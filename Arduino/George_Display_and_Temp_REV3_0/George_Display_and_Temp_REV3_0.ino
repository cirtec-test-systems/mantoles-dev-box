// George Science project
// Digital thermometer using the dallas 3 pin chip sensor DS18B20

#include <OneWire.h>
#include <DallasTemperature.h>
#include "SevSeg.h"
SevSeg sevenSegment; //Instantiate a seven segment controller object
// Data wire is plugged into port 2 on the Arduino
#define ONE_WIRE_BUS 2
// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);
// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

void setup() 
{
  byte numDigits = 4;
  byte digitPins[] = {2, 3, 4, 5};
  byte segmentPins[] = {6, 7, 8, 9, 10, 11, 12, 13};
  bool resistorsOnSegments = false; // 'false' means resistors are on digit pins
  byte hardwareConfig = COMMON_CATHODE;//ANODE; // See README.md for options
  bool updateWithDelays = true; // Default 'false' is Recommended  true is needed 
  bool leadingZeros = false; //true; // Use 'true' if you'd like to keep the leading zeros
  bool disableDecPoint = false; // Use 'true' if your decimal point doesn't exist or isn't connected
  sevenSegment.begin(hardwareConfig, numDigits, digitPins, segmentPins, resistorsOnSegments,updateWithDelays, leadingZeros, disableDecPoint);
  sevenSegment.setBrightness(90);
  Serial.begin(9600);
  Serial.println("Dallas Temperature IC Control Library Demo");
  // Start up the library
  sensors.begin();
} // end of setup

void loop() 
  {
  // call sensors.requestTemperatures() to issue a global temperature
  // request to all devices on the bus
  static unsigned long timer = millis();
  static int deciSeconds = 0;
  float tempC = 0.0;

  Serial.print("Requesting temperatures...");
  sensors.requestTemperatures(); // Send the command to get temperatures
  Serial.println("DONE");
  delay(1500);
  // We use the function ByIndex, and as an example get the temperature from the first sensor only.
  tempC = sensors.getTempCByIndex(0);
  // Check if reading was successful
  if (tempC != DEVICE_DISCONNECTED_C)
       {
    Serial.print("Temperature for the device 1 (index 0) is:   ");
    Serial.println(tempC);
    for (int i = 0; i < 100; i++)
      {
      //sevenSegment.setNumber(tempC,3);
      sevenSegment.setNumberF(tempC,1);
      delay(10);
      sevenSegment.refreshDisplay(); // Must run repeatedly
      }
       }
  else
    {
    Serial.println("Error: Could not read temperature data");
    } 
  } // end of loop

/// END ///
