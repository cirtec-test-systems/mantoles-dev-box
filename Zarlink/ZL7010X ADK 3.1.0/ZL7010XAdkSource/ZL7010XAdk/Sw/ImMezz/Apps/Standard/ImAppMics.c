/**************************************************************************
 * This file contains functions to interface to the MICS chip on the implant
 * mezzanine board.
 *
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Standard/String.h"          /* memset() */
#include "Adp/General.h"              /* UINT, RSTAT, BOOL, TRUE/FALSE, ... */
#include "Adp/Build/Build.h"          /* UD8, UD16, ... */
#include "Adp/AnyBoard/SysTimerLib.h" /* StDelayUs(), ... */
#include "Adk/AnyMezz/MicsHw.h"       /* IRQ_RADIOREADY, ... */
#include "Adk/AnyMezz/MicsLib.h"      /* MicsInit(), MicsAbort(), ... */
#include "Adk/AnyMezz/MicsGeneral.h"  /* MICS_LINK_CONFIG, ... */
#include "Adk/ImMezz/ImGeneral.h"     /* IM_IDLE, IM_MICS_CONFIG, ... */

/* local include shared by all source files for implant application */
#include "ImApp.h"

/**************************************************************************
 * Defines and Macros
 */
 
/* Private macro for this file to use to set the MICS state. This is needed
 * because ImPub.micsState is declared "const" so others can't modify it.
 */
#define IM_SET_MICS_STATE(newState) (*((UD8 *)&ImPub.micsState) = newState)

/* Number of milliseconds between each external strobe to trigger a 2.45 GHz
 * sniff, and the corresponding number of clocks. For best accuracy, this
 * should be a multiple of IM_MS_PER_CLOCK.
 */
#define IM_MS_PER_EXT_STROBE  1000
#define IM_CLOCKS_PER_EXT_STROBE  (IM_MS_PER_EXT_STROBE / IM_MS_PER_CLOCK)
 
/* Milliseconds between updates of the link quality statistics, and the 
 * corresponding number of clocks. For best accuracy, this should be a
 * multiple of IM_MS_PER_CLOCK.
 */
#define IM_MS_PER_LINK_QUAL_UPDATE  250
#define IM_CLOCKS_PER_LINK_QUAL_UPDATE \
    (IM_MS_PER_LINK_QUAL_UPDATE / IM_MS_PER_CLOCK)

/* Number of clocks between searches for a 400 MHz wakeup signal. For more
 * information, see IM_SEC_PER_WAKE_400_SEARCH in "ZL7010XAdk\Sw\Includes\
 * Adk\ImMezz\ImGeneral.h".
 */
#define IM_CLOCKS_PER_WAKE_400_SEARCH \
    (((UD32)IM_SEC_PER_WAKE_400_SEARCH * 1000) / IM_MS_PER_CLOCK)

/* Various settings for 400 MHz wakeup:
 * 
 * IM_NUM_BANDS:
 *     Number of bands (each band covers multiple channels).
 * IM_INIT_CONSEC_MISS_RESTART:
 *     The initial/reset value for "IM_WAKE_400_BAND.consecMissRestart".
 * IM_RSSI_MISS_DELTA:
 *     If an RSSI sample is < (maxRssi - IM_RSSI_MISS_DELTA), it's treated as
 *     a miss (see ImSearchForRssi() and ImProcRssi()). The value chosen is a
 *     balance between two conflicting requirements. On one hand, if the miss
 *     delta is too large, and some interference (any false signal) increases
 *     maxRssi, then stops, the implant is less likely to detect enough misses
 *     to trigger another ImCheckMaxRssi() to reset the maxRssi. Thus, if a
 *     real wakeup signal occurs at the same level later on, the RSSI's won't
 *     exceed maxRssi, so they won't trigger ImSearchBands(). On the other hand,
 *     if the miss delta is too small, and some low level interference raises
 *     maxRssi, then stops, the implant is more likely to detect enough misses
 *     to trigger another ImCheckMaxRssi() to reset maxRssi. Thus, if the
 *     interference keeps coming and going, the RSSI's will repeatedly exceed
 *     maxRssi, triggering ImSearchBands() and wasting power. The value chosen
 *     is the approximate amount by which the maximum RSSI for a real wakeup
 *     signal must exceed the maximum RSSI for ambient noise in order for a
 *     session to be established (i.e. the level at which the signal exceeds
 *     the noise enough for a session).
 * IM_NUM_SEARCH_RETRIES:
 *     This is the number of times to search a band again if the first search
 *     doesn't detect a wakeup signal for this implant. Note to save power, the
 *     retries are only done if the RSSI remains high enough to warrant it. If
 *     there's a real wakeup signal, the RSSI on the retry should never miss
 *     the signal (see ImGetRetryRssi() and ImProcRssi() for details). Thus,
 *     one retry should be enough, but if desired, this can be increased. The
 *     number of retries should be small to keep the power consumption low.
 */
#define IM_NUM_BANDS                  4
#define IM_INIT_CONSEC_MISS_RESTART   3
#define IM_RSSI_MISS_DELTA            3
#define IM_NUM_SEARCH_RETRIES         1
    
/* The times at which various steps are performed during a fast wakeup (for
 * 400 MHz wakeup - see ImFastWakeup()). Note these times can probably be
 * optimized to reduce power, but that hasn't been fully characterized.
 * 
 * IM_ENAB_RSSI_TIME_US: The time at which ImFastWakeup() enables the RSSI
 * blocks and receiver (relative to the time at which it enabled the synth).
 * 
 * IM_RSSI_READY_TIME_US: The time at which the ZL7010X will be ready to take
 * the first RSSI sample (relative to the time at which ImFastWakeup() enabled
 * the synthesizer).
 */
#if MICS_REV >= ZL70102
    #define IM_ENAB_RSSI_TIME_US   (1700)
    #define IM_RSSI_READY_TIME_US  (2000)
#else /* ZL70101 */
    #define IM_ENAB_RSSI_TIME_US   (1900)
    #define IM_RSSI_READY_TIME_US  (2200)
#endif

/* The amount of time ImListen() listens for a 400 MHz wakeup signal from the
 * base station. This is ~6.5 ms, which includes MICS_SHORT_SYNTH_LOCK_TIME_US
 * to ensure the synthesizer has time to lock, plus 930 us (the time it takes
 * the ZL7010X to transmit one 2FSK-FB header), plus ~3.63 ms to receive any
 * 400 MHz wakeup headers from the base station. Note the 930 us is needed
 * because when ImListen() first starts listening for a 400 MHz wakeup signal,
 * the ZL7010X always performs the steps to transmit a header, even though it
 * doesn't actually transmit the header (for more information, see INITCOM in
 * ImListen103(), and RESENDTIME and INITCOM in ImListen101And102()).
 *
 * When the base is using 400 MHz wakeup to start a session, it usually
 * transmits 400 MHz headers using the minimum resend time (930 us headers
 * with a 55 us gap between them). If the implant listens during that time,
 * it will see 3 headers from the base (3.63 / 0.985).
 *
 * For 6 ms out of each second, the base changes its resend time to ~1.28 ms
 * (930 us headers with a 1.28 ms gap between them) and listens for responses
 * between transmissions. The implant will rarely listen during one of those
 * 6 ms windows, and when it does, it will still see at least 1 header from
 * the base (3.63 / 2.21).
 *
 * Every 4th listening window on the base (approximately once every 4 seconds),
 * the base also stops transmitting for 10 ms to check if the channel is still
 * clear. If the implant listens during one of those 10 ms windows, it won't
 * see any headers from the base. This isn't critical because it will very
 * rarely happen, and if it does happen, the implant should detect the wakeup
 * signal when it retries (see ImGetRetryRssi() for details). Thus, it's not
 * worth listening longer on the implant to cover this 10 ms.
 *
 * When the base is searching for implants, its listening windows are
 * longer (~25 ms normally, and ~35 ms every 4th listening window). Thus, the
 * probability the implant will listen for ID's during that window is a little
 * higher. However, if that happens, the implant should still detect the wakeup
 * signal when it retries (see ImGetRetryRssi() for details), so this isn't
 * a concern.
 *
 * For more information on the timing between the base and implant during 400
 * MHz wakeup, see BSM_MS_PER_WAKE_400_LISTEN in "ZL7010XAdk/Sw/BsmMezz/Apps/
 * Standard/BsmAppMics.c".
 *
 * Note the 400 MHz wakeup algorithm tracks the maximum RSSI, etc. to avoid
 * searching the bands and listening too often (see ImSearchForRssi() and
 * ImProcRssi()). Thus, listening for ~6.5 ms is a reasonable compromise
 * between reliability (to detect a real wakeup signal), and power consumption
 * (when there are false signals).
 */
#define IM_WAKE_400_LISTEN_TIME_US  (MICS_SHORT_SYNTH_LOCK_TIME_US + 4500)

/* The normal resend time to use when sending wakeup responses and emergency
 * transmissions. If the power-on/reset value is used (RESENDTIME_RESET), the
 * MICS chip will send wakeup responses and emergency transmissions at random
 * intervals between ~9.829 to ~19.658 ms. Note if this is increased, and
 * you're using 400 MHz wakeup, you should also increase the listening window
 * on the base station when searching for implants (see BsmProcWake400() in
 * "ZL7010XAdk\Sw\BsmMezz\Apps\Standard\BsmAppMics.c"), and the timeout in
 * ImConfirmWake400() (later in this file).
 */
#define IM_RESEND_TIME  RESENDTIME_RESET

/* normal device mode */
#define IM_DEVICE_MODE  DISAB_WILD_COMPANY_ID

/* The setting to use for the ZL7010X's INTERFACE_MODE. This sets the ZL7010X's
 * maximum SPI clock to the smallest value that will work with the local SPI.
 */
#if IM_LSPI_CLK_FREQ <= 1000000UL
  #define IM_INTERFACE_MODE  MAX_SPI_CLOCK_1_MHZ
#elif IM_LSPI_CLK_FREQ <= 2000000UL
  #define IM_INTERFACE_MODE  MAX_SPI_CLOCK_2_MHZ
#else  
  #define IM_INTERFACE_MODE  MAX_SPI_CLOCK_4_MHZ
#endif

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* structure of private data for MICS interface */
typedef struct {
    
    /* Various flags for MICS interface. For bit definitions, see
     * IM_EMERGENCY, etc. This is declared volatile to prevent the
     * compiler from assuming it won't change during a section of code,
     * in case it is changed by interrupts.
     */
    volatile UD8 flags;
    
    /* number of clocks left until next external strobe for a 2.45 GHz sniff */
    UD8 clocksToExtStrobe;

    /* number of clocks left until link quality statistics are updated */
    UD8 clocksToLinkQualUpdate;
    
    /* number of clocks left until next search for a 400 MHz wakeup signal */
    UD8 clocksToWake400Search;
        
    /* If using a ZL70101, these are used to save SYNTH_CT, SYNTH_CTRIM, and
     * RXIFFILTERTUNE for ImFastWakeup101() to reference (for 400 MHz wakeup).
     */
    #if MICS_REV == ZL70101
    UD8 synthCt;
    UD8 synthCtrim;
    UD8 rxIfFilterTune;
    #endif
    
    /* If using a ZL70102/103, this is the current DEVICEMODE_102 setting. When
     * ImSetDeviceMode() is called to set DEVICEMODE_102, it also sets this so
     * ImMicsWakeup() can detect when the ZL70102/103 is in fast startup mode.
     * The software should always call ImSetDeviceMode() to set DEVICEMODE_102
     * (it's declared const so it will fail if you try to set it directly).
     */
    const UD8 deviceMode;
    
    UD8 spare1;
    
    /* Space we know is available in the MICS TX buf (see ImTransmitMics()),
     * and data we know is available in the MICS RX buf (see ImReceiveMics()).
     */
    UD16 txBytesAvail;
    UD16 rxBytesAvail;
    
    /* data test specifications (see ImStartDt()) */
    MICS_DT_SPECS dt;
    
    /* data for each band (400 MHz wakeup) */
    struct ImWake400Band {
        
        /* maximum RSSI detected for band */
        UD8 maxRssi;
            
        /* set to initiate retries (search band again) */
        UD8 retryCountdown;
            
        /* number of consecutive misses left until we re-check max RSSI */
        UD8 consecMissCountdown;
            
        /* restart value for "consecMissCountdown" */
        UD8 consecMissRestart;
        
    } band[IM_NUM_BANDS];
    
} IM_MICS_PRIV;
/*
 * Typedefs for structures nested in IM_MICS_PRIV (for convenience).
 */
typedef struct ImWake400Band  IM_WAKE_400_BAND;
/*
 * Defines for IM_MICS_PRIV.flags:
 * 
 * IM_EMERGENCY:
 *     ImSendEmergency() sets this flag when it's called to start
 *     sending emergency transmissions. When a session is established,
 *     ImProcMicsSession() can check this flag to determine if the session
 *     was established while sending emergency transmissions (i.e. it's an
 *     emergency session). This flag is cleared by ImMicsSleep().
 * IM_RX_WILD_TID:
 *     If using 400 MHz wakeup with a ZL70101 or ZL70102, ImListen101And102()
 *     sets this flag when it receives implant ID 0 (wildcard). That way, when
 *     ImListen101And102() calls ImConfirmWake400(), and it in turn asserts
 *     IRQ_LINKREADY to confirm the wakeup, ImLinkReadyIsr() will see this
 *     flag and ignore IRQ_LINKREADY. This is cleared by ImListen101And102()
 *     or ImMicsSleep().
 */
#define IM_EMERGENCY        (1 << 0)
#define IM_RX_WILD_TID  (1 << 1)

/* Structure used to specify the order in which to search the bands for a 400
 * MHz wakeup signal (see ImSearchBands()). 
 */
typedef struct {
    
    /* the bands to search (in desired order, terminated with 0xFF) */
    UD8 band[IM_NUM_BANDS + 1];
    
} IM_BAND_ORDER;
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/* private data for MICS interface */
static IM_MICS_PRIV  ImMicsPriv;

/* table containing bit flag associated with each band (indexed by band) */
static const UD8 ImBandFlag[IM_NUM_BANDS] = {BIT0, BIT1, BIT2, BIT3};

/* Table containing the order in which the implant searches the channels in
 * each band for a 400 MHz wakeup signal (see ImSearchBands()). For bands with
 * fewer than 3 channels, the last entry is 0xFF to indicate there are no more
 * channels in band. The first channel in each band is the band's primary
 * channel, which is the channel the implant uses to check if there's any RSSI
 * energy in the band (see ImSearchForRssi()).
 */
static const UD8 ImChanOrder[IM_NUM_BANDS][3] = {
    {0, 1, 0xFF},  /* band 0 */
    {3, 2, 4},     /* band 1 */
    {6, 5, 7},     /* band 2 */   
    {9, 8, 0xFF}   /* band 3 */
};
 
/**************************************************************************
 * Function Prototypes
 */
static void ImInitMicsPrivMem(void);
static void ImInitMicsConfigMem(void);
static UINT ImProcRssi(UINT band, UINT rssi);
static void ImCheckMaxRssi(UINT band);
static UINT ImGetRetryRssi(void);
static void ImGetBandOrder(UINT searchBands, IM_BAND_ORDER *retOrder);
static void ImSearchBands(UINT searchBands);
static BOOL ImListen(UINT chan);
static void ImResetBand(UINT band);
static void ImFastWakeup(void);
static void ImFastSleep(void);
static void ImProcDtRx(void);
/**/
__inline void ImSetTxBlockSize(UINT txBlockSize);
__inline void ImSetRxBlockSize(UINT rxBlockSize);
__inline void ImSearchForWake400(void);
__inline UINT ImSearchForRssi(void);
__inline void ImSetSyncPattern(UINT chan);
/**/
__inline void ImRadioReadyIsr(void);
__inline void ImLinkReadyIsr(void);
__inline void ImLinkQualIsr(void);
__inline void ImMicsRxNotEmptyIsr(void);
__inline void ImHkMessRegIsr(void);
__inline void ImHkUserDataIsr(void);
__inline void ImHkUserStatusIsr(void);
__inline void ImHkRemoteDoneIsr(void);
__inline void ImCommandDoneIsr(void);
__inline UINT ImConLostIsr(UINT irqAuxStat);
         UINT ImRadioFailIsr(UINT irqAuxStat);
/**/
#if MICS_REV >= ZL70102
    __inline void ImFastWakeup102(void);
    __inline void ImSetDeviceMode(UINT deviceMode);
#else
    __inline void ImFastWakeup101(void);
    __inline void ImInitInterfaceMode(void);
#endif
/**/
#if MICS_REV >= ZL70103
    __inline BOOL ImListen103(UINT chan);
#else
    __inline BOOL ImListen101And102(UINT chan);
    static BOOL ImConfirmWake400(void);
#endif

/**************************************************************************
 * Initialize the MICS chip and MICS interface on the implant.
 */
RSTAT
ImMicsInit(void)
{
    UINT micsRev;
    UD16 startMs;

    /* init fields in ImMicsPriv and ImPub.micsConfig */
    ImInitMicsPrivMem();
    ImInitMicsConfigMem();

    /* init MICS library */
    if (MicsInit()) return(-1);

    /* Set WU_EN pin to wake the ZL7010X, then wait for it to assert its
     * interrupt to ensure it finished waking up (IRQ_RADIOREADY). Note if
     * the WU_EN pin is high after power-on/reset, the ZL7010X will already be
     * waking up at this point, or finished. If it's finished, its interrupt
     * will already be asserted.
     *
     * Note if the MCU is power-cycled or reset (via a debugger, etc.),
     * but the ZL7010X remains awake and powered, the ZL7010X won't assert
     * IRQ_RADIOREADY, in which case this will just time out and continue.
     * Also, if the ZL7010X remains asleep and powered, then when this wakes
     * it, it's possible it will assert IRQ_CRCERR instead of IRQ_RADIOREADY.
     * If either of these cases occurs, it won't be an issue, because when
     * ImResetMics() is called later on, it will still reset the ZL7010X.
     */
    MICS_WR_WU_EN(TRUE);
    for(startMs = StMs();;) {
        if (MICS_INT_ASSERTED || StElapsedMs(startMs, 250)) break;
    }
    MICS_WR_WU_EN(FALSE);
    /*
     * Read the ZL7010X revision. Note if the SPI clock is > 1 MHz, this
     * temporarily reduces it in case the SPI clock is normally 4 MHz and the
     * chip is a ZL70101 (max SPI 2 MHz after sleep or POR), or the ZL7010X's
     * INTERFACE_MODE was somehow corrupted and only the MCU was power-cycled
     * or reset (via a debugger, etc.).
     */
    if (IM_LSPI_CLK_FREQ > 1000000UL) {
        ImConfigSpi(IM_LSPI_CLK_DIV_FOR_1_MHZ);
        micsRev = MicsRead(MICS_REVISION);
        ImConfigSpi(IM_LSPI_CLK_DIV);
    } else {
        micsRev = MicsRead(MICS_REVISION);
    }
    /* If the ZL7010X revision doesn't match the revision this software is
     * intended for (MICS_REV), fail.
     */
    if (micsRev != MICS_REV) {
        ImErr(IM_APP_ERR_WRONG_MICS_REV, "%u", micsRev);
        return(-1);
    }

    /* Reset and initialize the ZL7010X in case it remained powered while
     * the MCU was power-cycled or reset (via a debugger, etc.). Note if the
     * ZL7010X was also power-cycled, it's redundant to reset it again, but
     * do so anyway to be consistent, and because it's simpler.
     */
    ImResetMics();

    /* put ZL7010X to sleep */
    ImMicsSleep();
    
    return(0);
}

/* This resets and initializes the ZL7010X. Note that when this is called,
 * the ZL7010X must be awake, and when this returns, the ZL7010X is awake
 * and initialized.
 *
 * Note that this should never be called in an ISR (to avoid race conditions).
 */
void
ImResetMics(void)
{
    IM_MICS_CONFIG *mc = &ImPub.micsConfig;
    UINT synthCtrim;

    /* If the SPI clock is > 1MHz, temporarily reduce it to help ensure
     * we can write to the ZL7010X to reset it (in case the SPI clock is
     * normally 4 MHz and the ZL7010X was somehow awakened without adjusting
     * INTERFACE_MODE, or INTERFACE_MODE was somehow corrupted). Note the SPI
     * clock will be restored after MicsReset() is called later on.
     */
    if (IM_LSPI_CLK_FREQ > 1000000UL) ImConfigSpi(IM_LSPI_CLK_DIV_FOR_1_MHZ);
    /*
     * Ensure the ZL7010X watchdog is disabled, then disable all interrupts
     * in the ZL7010X. This ensures the ZL7010X won't go to sleep or assert
     * another interrupt while we're resetting the affected status/control
     * data and the ZL7010X. The ZL7010X won't assert another interrupt until
     * it re-wakes after the reset (IRQ_RADIOREADY).
     */
    MicsWrite(WDOG_CONTROL, WDOG_CLEAR_AND_DISAB);
    MicsWrite(IRQ_ENABLECLEAR1, 0xFF);
    MicsWrite(IRQ_ENABLECLEAR2, 0xFF);
    /*
     * Configure the MICS interrupt to latch on rising edge, initialize
     * the MICS interrupt flag, then enable the interrupt.
     */
    MICS_WR_INT_EDGE(INT_ON_RISING_EDGE);
    MICS_WR_INT_FLAG(FALSE);
    MICS_WR_INT_ENAB(TRUE);

    /* Ensure the session LED, TX 400 LED, and RX 400 LED are off. Note that we
     * can't just wait for the next ImPeriodicLedTasks() to turn off the TX/RX
     * 400 LEDs, because if the TX 400 LED is constantly on (e.g. for the 400
     * MHz TX carrier, or because an HK TX operation never received a reply
     * from the base), and/or the RX 400 LED is constantly on (e.g. for the
     * external RSSI), ImPeriodicLedTasks() wouldn't turn them off, so they
     * must be turned off here.
     */
    ImSessionLedOff();
    ImTx400LedOff();
    ImRx400LedOff();

    /* reset fields in ImMicsPriv and ImPub.micsConfig */
    ImInitMicsPrivMem();
    ImInitMicsConfigMem();

    /* clear any pending flags affected by ZL7010X reset */
    ImPub.pend &= ~(IM_MICS_SLEEP_PEND | IM_MICS_SESSION_PEND |
        IM_MICS_RX_PEND | IM_DT_TX_PEND);

    /* Reset ZL7010X. Note that this sets ImPub.micsState to IM_SLEEPING
     * before calling MicsReset() so when the ZL7010X re-wakes and asserts
     * IRQ_RADIOREADY after the reset, ImMicsIsr() will handle it correctly.
     */
    IM_SET_MICS_STATE(IM_SLEEPING);
    MicsReset();

    /* if needed, restore SPI clock to its normal rate */
    if (IM_LSPI_CLK_FREQ > 1000000UL) ImConfigSpi(IM_LSPI_CLK_DIV);

    /* If using a ZL70102/103, and the desired INTERFACE_MODE value is
     * different than the reset value, initialize INTERFACE_MODE. That way,
     * if the actual SPI clock is <= 2 MHz, the ZL70102/103's maximum SPI clock
     * rate will be reduced from 4 MHz (the reset value) to either 2 MHz or 1
     * MHz as appropriate. This is done to reduce the power consumed by the
     * ZL70102/103's SPI interface a little, even though the power saved is
     * probably insignificant relative to the overall power consumed by the
     * ZL70102/103.
     *
     * Note that the ZL70102/103 saves INTERFACE_MODE in its wakeup stack when
     * ImConfigMics() calls MicsCopyRegs() later on, so the ZL70102/103 will
     * restore INTERFACE_MODE each time it wakes up thereafter. Thus,
     * INTERFACE_MODE only needs to initialized after the ZL70102/103
     * is reset, not each time it wakes up.
     *
     * Note that for a ZL70101, INTERFACE_MODE isn't initialized here because
     * when the ZL70101 wakes up and asserts IRQ_RADIOREADY, ImMicsIsr() calls
     * ImInitInterfaceMode() to initialize INTERFACE_MODE as needed (see
     * ImInitInterfaceMode() for details).
     */
    if ((MICS_REV >= ZL70102) && (IM_INTERFACE_MODE != INTERFACE_MODE_RESET_102)) {
        MicsWrite(INTERFACE_MODE, IM_INTERFACE_MODE);
    }

    /* Override various ZL7010X power-on/reset defaults (to optimize, etc.).
     * Note some of these registers reside in the ZL7010X's wakeup block so it
     * will always preserve them while sleeping. For the rest, the ZL7010X will
     * save them in its wakeup stack when ImConfigMics() calls MicsCopyRegs()
     * later on, so the ZL7010X will restore them each time it wakes up.
     */
    if (IM_RESEND_TIME != RESENDTIME_RESET) {
        MicsWrite(RESENDTIME, IM_RESEND_TIME);
    }
    MicsWrite(TXRFPWRDEFAULTSET, 0x3F);
    /*
     * Set XO trim & RSSI offset trim to calibrated values from NVM.
     */
    MicsWrite(XO_TRIM, MicsFactNvm->xoTrim);
    #if MICS_REV >= ZL70102
        MicsWrite(RSSITRIM_102, MicsFactNvm->rssiOffsetTrim);
    #else
        MicsWrite(RSSITRIM_101, MicsFactNvm->rssiOffsetTrim);
    #endif
    /**/
    #if ((IM_MODEL == AIM200) || (IM_MODEL == AIM300))

        #if MICS_REV >= ZL70102
            MicsWrite(TXRFANTTUNE_TX_MODE_102, 0x00);
            MicsWrite(RXRFANTTUNE_RX_MODE_102, 0x00);
            MicsWrite(ANTMATCH1_102, 0x0C);
            MicsWrite(ANTMATCH2_102, 0x00);
        #endif

    #elif IM_HW == AIM100_REV_E

        MicsWrite(ANT245TUNE, 0x02);

        #if MICS_REV >= ZL70102
            MicsWrite(TXRFANTTUNE_TX_MODE_102, 0x0A);
            MicsWrite(RXRFANTTUNE_RX_MODE_102, 0x00);
        #else /* ZL70101 */
            MicsWrite(TXRFANTTUNE_101, 0x0A);
            MicsWrite(RXRFANTTUNE_101, 0x00);
        #endif

    #else /* all other AIM100 revisions */

        MicsWrite(ANT245TUNE, 0x00);

        #if MICS_REV >= ZL70102
            MicsWrite(TXRFANTTUNE_TX_MODE_102, 0x12);
            MicsWrite(RXRFANTTUNE_RX_MODE_102, 0x00);
        #else /* ZL70101 */
            MicsWrite(TXRFANTTUNE_101, 0x12);
            MicsWrite(RXRFANTTUNE_101, 0x00);
        #endif
    #endif
    /**/
    #if MICS_REV >= ZL70102

        MicsWrite(LNABIAS_102, 0x0F);
        MicsWrite(RX_EN_CNT_102, 5);

        /* cal 2 MHz oscillator frequency (for 2.45 GHz receiver) */
        MicsCal(CAL_2_MHZ_OSC_102);

        /* if needed, set device mode (e.g. to disable wildcard company ID) */
        if (IM_DEVICE_MODE != DEVICEMODE_RESET_102) {
            ImSetDeviceMode(IM_DEVICE_MODE);
        }

    #else /* ZL70101 */

        MicsWrite(LNABIAS_101, 0x0F);
        MicsWrite(LNAFREQ_101, 0x0D);

        /* Optimize the 2.45 GHz wakeup receiver. The zero level calibration
         * (CAL_245_GHZ_ZERO_LEV_101) sets DETZEROLEV_101, which the ZL70101
         * adds to DETSENSLONG_101 for the long range sensitivity, and
         * DETSENSSHORT_101 for the short range sensitivity. Note the
         * CAL_245_GHZ_ZERO_LEV_101 calibration depends on LNAFREQ_101,
         * LNABIAS_101, and ANT245TUNE so they must be set as desired
         * beforehand (they're set earlier in this function).
         */
        MicsWrite(DETSENSSHORT_101, 0x1F);
        MicsWrite(DETSENSLONG_101, 0x12);
        MicsCal(CAL_245_GHZ_ZERO_LEV_101);
    #endif
    /*
     * Optimize the wakeup strobe pulse width and period. Note the ZL7010X
     * calibrates the wakeup strobe oscillator (CAL_WAKEUP_STROBE_OSC) every
     * time it wakes up, so at this point, it's safe to optimize these values.
     * This sets the strobe period to ~1 second, and the pulse width to
     * 200 us nominal (190 us minimum) for a ZL70102, or 280 us nominal
     * (266 us minimum) for a ZL70101. These are the recommended pulse width
     * settings for the ZL70101 and ZL70102 respectively.
     */
    #if MICS_REV >= ZL70102
        MicsWrite(STROSCPWIDTH1_102, 8);
        MicsWrite(STROSCPERIOD1_102, 2777 & 0xFF);
        MicsWrite(STROSCPERIOD2_102, 2777 >> 8);
    #else /* ZL70101 */
        MicsWrite(STROSCPWIDTH_101, 6);
        MicsWrite(STROSCPERIOD1_101, 3571 & 0xFF);
        MicsWrite(STROSCPERIOD2_101, 3571 >> 8);
    #endif
    /*
     * Call MicsSynthCtrim() to trim SYNTH_CTRIM and set SYNTH_CTRIM_READY = 1
     * so the ZL7010X won't repeat this trim each time the synthesizer is
     * enabled in the future.
     *
     * If using a ZL70102/103, also copy the resulting SYNTH_CTRIM[5:1] to
     * RX_CTRIM_102[4:0] so the next time the ZL70102/103 sleeps, it will use
     * that value for the 2.45 GHz RX (since RX_CTRIM_102 is in the wakeup
     * block, the ZL70102/103 will preserve it while sleeping).
     *
     * Note the ZL7010X will save SYNTH_CTRIM in its wakeup stack when
     * ImConfigMics() calls MicsCopyRegs() later on, so the ZL7010X will
     * restore SYNTH_CTRIM each time it wakes up in the future. Note if
     * using a ZL70102/103, then each time it wakes up, it will also copy
     * SYNTH_CTRIM[5:1] to RX_CTRIM_102[4:0], and SYNTH_CTRIM[5:3] to
     * RAMP_CTRIM_102[5:3] (for the 400 MHz TX power ramping).
     */
    synthCtrim = MicsSynthCtrim();
    if (MICS_REV >= ZL70102) MicsWrite(RX_CTRIM_102, synthCtrim >> 1);

    /* Config ZL7010X (to override ZL7010X defaults affected by MICS config).
     * Note ImConfigMics() also calls MicsCopyRegs() to save registers in the
     * ZL7010X's wakeup stack, so if any registers included in the wakeup stack
     * were changed, the ZL7010X will restore them each time it wakes up.
     */
    ImConfigMics(mc);
}

static void
ImInitMicsPrivMem(void)
{
    IM_MICS_PRIV *im = &ImMicsPriv;

    /* clear ImMicsPriv and init any non-zero defaults */
    (void)memset(im, 0, sizeof(*im));
}

static void
ImInitMicsConfigMem(void)
{
    IM_MICS_CONFIG *mc = &ImPub.micsConfig;

    /* clear ImPub.micsConfig and init any non-zero defaults */
    (void)memset(mc, 0, sizeof(*mc));
    mc->companyId = 1;
    mc->imdTid.b1 = 1;
    mc->flags = IM_ENAB_HK_WRITE | IM_EXT_STROBE_FOR_245_GHZ_SNIFF;
    mc->normalLinkConfig.txBlockSize = 14;
    mc->normalLinkConfig.rxBlockSize = 14;
    mc->normalLinkConfig.maxBlocksPerTxPack = 31;
    mc->emergencyLinkConfig = mc->normalLinkConfig;
}

/* If the MICS chip is sleeping, use direct wakeup to wake it up. Note if the
 * chip is already awake, this will leave it awake. The implant will keep the
 * MICS chip awake until ImMicsSleep() is called to put it to sleep, or the
 * MICS watchdog timer expires (~4.37 seconds after it is awakened).
 */
void
ImMicsWakeup(void)
{
    IM_MICS_CONFIG *mc = &ImPub.micsConfig;
    #if MICS_REV >= ZL70102
        IM_MICS_PRIV *im = &ImMicsPriv;
    #endif
    
    /* if MICS chip is already awake, return (nothing to do) */
    if (ImPub.micsState != IM_SLEEPING) return;
    
    /* If using a ZL70102/103 and DEVICEMODE_102.FAST_STARTUP is currently set,
     * do a fast wakeup, clear DEVICEMODE_102.FASE_STARTUP, and put the chip
     * back to sleep. That way, when MicsWakeup() is called afterwards (see
     * below), the chip will do a normal startup instead of a fast startup.
     * This is needed because when 400 MHz wakeup is used with a ZL70102/103,
     * DEVICEMODE_102.FAST_STARTUP is set while the chip is sleeping so it
     * will wake up faster for each 400 MHz sniff (see ImMicsSleep(),
     * ImSearchForWake400(), and ImFastWakup()).
     */
    #if MICS_REV >= ZL70102
        if (im->deviceMode & FAST_STARTUP) {

            ImFastWakeup();
            ImSetDeviceMode(IM_DEVICE_MODE);
            ImFastSleep();
       }
    #endif

    /* use direct wakeup to wake the ZL7010X */
    MicsWakeup();
    
    /* Initialize the company ID and implant ID in the MAC block since they
     * default to 0 on a direct wakeup. That way, the correct values will be
     * reported to the PC in the link status (see IM_STAT), and they'll be
     * ready to send emergency (see ImSendEmergency()) or search for 400 MHz
     * wakeup (see ImSearchBands()).
     */
    MicsWrite(MAC_COMPANYID, mc->companyId);
    MicsWrite(MAC_IMDTRANSID1, mc->imdTid.b1);
    MicsWrite(MAC_IMDTRANSID2, mc->imdTid.b2);
    MicsWrite(MAC_IMDTRANSID3, mc->imdTid.b3);
    
    /* set flag to report change in link status to PC (see IM_STAT) */
    ImPub.statFlags |= IM_LINK_STAT_CHANGED;
}

/* Configure the MICS interface and MICS chip. Note this should only be called
 * when the MICS chip is awake and idle.
 */
void
ImConfigMics(const IM_MICS_CONFIG *newMc)
{
    IM_MICS_PRIV *im = &ImMicsPriv;
    IM_MICS_CONFIG *mc = &ImPub.micsConfig;
    
    /* if specified different MICS config, save new MICS config */
    if (newMc != mc) *mc = *newMc;
    
    /* init ImMicsPriv as appropriate when config is changed */
    im->clocksToLinkQualUpdate = IM_CLOCKS_PER_LINK_QUAL_UPDATE;
    
    /* Enable or disable housekeeping write access on the ZL7010X. Note when
     * the ZL7010X sleeps, it will disable housekeeping write access, but the
     * next time the chip wakes up and asserts IRQ_RADIOREADY, ImMicsIsr()
     * will check the config flags and enable it again if desired (if using
     * a ZL70102/103, see HK_MODE_102 in ImRadioReadyIsr(); if using a ZL70101,
     * see ImInitInterfaceMode()).
     */
    if (mc->flags & IM_ENAB_HK_WRITE) {
        if (MICS_REV >= ZL70102) {
            MicsWrite(HK_MODE_102, HK_WRITE_ENAB);
        } else {
            MicsWrite(INTERFACE_MODE, IM_INTERFACE_MODE | ENAB_HK_WRITE_101);
        }
    } else {
        if (MICS_REV >= ZL70102) {
            MicsWrite(HK_MODE_102, 0);
        } else {
            MicsWrite(INTERFACE_MODE, IM_INTERFACE_MODE);
        }
    }
    
    /* Set company ID and implant ID. Note these are also written to their
     * corresponding registers in the wakeup block for 2.45 GHz wakeup. This
     * also ensures the wakeup and MAC settings are always the same.
     */
    MicsWrite(MAC_COMPANYID, mc->companyId);
    MicsWrite(MAC_IMDTRANSID1, mc->imdTid.b1);
    MicsWrite(MAC_IMDTRANSID2, mc->imdTid.b2);
    MicsWrite(MAC_IMDTRANSID3, mc->imdTid.b3);
    MicsWrite(WAKEUP_COMPANYID, mc->companyId);
    MicsWrite(WAKEUP_IMDTRANSID1, mc->imdTid.b1);
    MicsWrite(WAKEUP_IMDTRANSID2, mc->imdTid.b2);
    MicsWrite(WAKEUP_IMDTRANSID3, mc->imdTid.b3);
    
    /* if specified 400 MHZ wakeup (instead of 2.45 GHz) */
    if (mc->flags & IM_400_MHZ_WAKEUP) {
        
        /* reset 400 MHz wakeup variables (see function for details) */
        ImResetWake400();
            
        /* Clear reg_wakeup_ctrl.ENAB_INT_STROBE_OSC to disable the internal
         * wakeup strobe oscillator to reduce power (since we're not using
         * 2.45 GHz wakeup, we don't need the ZL7010X's internal wakeup strobe
         * to trigger 2.45 GHz sniffs).
         */
        #if MICS_REV == ZL70101
            MicsWrite(WAKEUP_CTRL, WAKEUP_CTRL_RESET_101 & ~ENAB_INT_STROBE_OSC);
        #else    
            MicsWrite(WAKEUP_CTRL, WAKEUP_CTRL_RESET_102 & ~ENAB_INT_STROBE_OSC);
        #endif
        
        /* Perform the multi-channel coarse tune algorithm for the ZL7010X's
         * synthesizer. This finds a SYNTH_CT value that will work for all
         * channels and writes it back to SYNTH_CT with SYNTH_CT.SYNTH_CT_READY
         * = 1. That way, the synthesizer will start faster each time it's
         * enabled to search for 400 MHz wakeup (see ImSearchForWake400(),
         * ImFastWakeup(), and ImListen()). Note the ZL7010X will save SYNTH_CT
         * in its wakeup stack when ImConfigMics() calls MicsCopyRegs() later
         * on, so the ZL7010X will restore SYNTH_CT each time it wakes up.
         *
         * Note when this is called, SYNTH_CTRIM must already be set with
         * SYNTH_CTRIM.SYNTH_CTRIM_READY = 1, which is done by calling
         * MicsSynthCtrim() during power-on/reset initialization (see
         * ImResetMics()).
         */
        MicsMultiChanSynthCoarseTune();
        /*
         * If using ZL70101, do the following:
         * 
         * - Save SYNTH_CT and SYNTH_CTRIM for ImFastWakeup101() to reference
         *   later (for 400 MHz wakeup). This is needed even though the ZL70101
         *   saves these in its wakeup stack, because ImFastWakeup101() needs
         *   to restore these before the ZL70101 restores its wakeup stack.
         * 
         * - Save RXIFFILTERTUNE (reg_rf_rxiffiltertune). This is saved now so
         *   we save the value after a normal wakeup with default calibrations.
         *   For more information, see RXIFFILTERTUNE in ImFastWakeup101().
         *   Note the ZL70102/103 doesn't need this because it saves
         *   RXIFFILTERTUNE in its wakeup stack.
         */
        #if MICS_REV == ZL70101
            im->synthCt = MicsRead(SYNTH_CT);
            im->synthCtrim = MicsRead(SYNTH_CTRIM);
            im->rxIfFilterTune = MicsRead(RXIFFILTERTUNE);
        #endif
    
    } else { /* else, specified 2.45 GHz wakeup */
        
        /* If specified use of external strobe for 2.45 GHz sniff, clear
         * reg_wakeup_ctrl.ENAB_INT_STROBE_OSC to disable the ZL7010X's
         * internal wakeup strobe oscillator to reduce power. Since we'll
         * use an external strobe to trigger each 2.45 GHz sniff, the internal
         * strobe isn't needed.
         */
        if (mc->flags & IM_EXT_STROBE_FOR_245_GHZ_SNIFF) {

            #if MICS_REV == ZL70101
                MicsWrite(WAKEUP_CTRL, WAKEUP_CTRL_RESET_101 & ~ENAB_INT_STROBE_OSC);
            #else
                MicsWrite(WAKEUP_CTRL, WAKEUP_CTRL_RESET_102 & ~ENAB_INT_STROBE_OSC);
            #endif

        } else { /* specified use of ZL7010X's internal wakeup strobe */

            /* Restore reg_wakeup_ctrl to its power-on/reset default in case
             * it was changed for a previous configuration setup. This sets
             * reg_wakeup_ctrl.ENAB_INT_STROBE so the ZL7010X will use its
             * internal wakeup strobe to trigger 2.45 GHz sniffs while it's
             * sleeping.
             */
            #if MICS_REV == ZL70101
                MicsWrite(WAKEUP_CTRL, WAKEUP_CTRL_RESET_101);
            #else
                MicsWrite(WAKEUP_CTRL, WAKEUP_CTRL_RESET_102);
            #endif
        }
        
        /* Clear SYNTH_CT.SYNTH_CT_READY so the ZL7010X will coarse tune
         * SYNTH_CT each time the synthesizer is enabled.
         */
        MicsWrite(SYNTH_CT, SYNTH_CT_RESET);
    }
    
    /* configure link for normal operation by default (not emergency) */
    ImConfigLink(&mc->normalLinkConfig);
    
    /* Save registers in the ZL7010X's wakeup stack, so if any registers
     * included in the wakeup stack were changed, the ZL7010X will restore
     * them each time it wakes up. MicsCopyRegs() also recalculates the CRC,
     * which is required whenever registers are changed in the wakeup stack
     * (RXBUFF_BSIZE, ...) or wakeup block (WAKEUP_COMPANYID, ...).
     */
    MicsCopyRegs();
}

/* This is called to configure the MICS link. To configure the link for
 * normal operation, pass a pointer to ImPub.micsConfig.normalLinkConfig,
 * and to configure the link for emergency operation, pass a pointer to
 * ImPub.micsConfig.emergencyLinkConfig. Note this should only be called
 * when the ZL7010X is awake and idle.
 */
void
ImConfigLink(const MICS_LINK_CONFIG *lc)
{
    UINT prevModUser;
    
    /* set various link config registers in ZL7010X */
    MicsWrite(MAC_CHANNEL, lc->chan);
    MicsWrite(TXBUFF_MAXPACKSIZE, lc->maxBlocksPerTxPack);
    ImSetTxBlockSize(lc->txBlockSize);
    ImSetRxBlockSize(lc->rxBlockSize);
    
    /* Set MAC_MODUSER (for TX/RX modulation, etc.), and if the new TX
     * modulation != its previous setting, recalibrate the TX IF oscillator
     * (in case it's being changed from 2FSK to 4FSK, or vice-versa).
     */
    prevModUser = MicsRead(MAC_MODUSER);
    MicsWrite(MAC_MODUSER, lc->modUser);
    if ((lc->modUser & TX_MOD_MASK) != (prevModUser & TX_MOD_MASK)) {
        MicsCal(CAL_TX_IF_OSC);
    }
    
    /* If using a ZL70101, set reg_mac_trainword to the best value for the
     * specified TX modulation mode.
     */
    #if MICS_REV == ZL70101
        if ((lc->modUser & TX_MOD_MASK) == TX_MOD_4FSK) {
            MicsWrite(TRAINWORD_101, TRAINWORD_4FSK);
        } else {
            MicsWrite(TRAINWORD_101, TRAINWORD_2FSK);
        }
    #endif
    
    /* set flag to report change in link status to PC (see IM_STAT) */
    ImPub.statFlags |= IM_LINK_STAT_CHANGED;
}

/* Tell the MICS chip to send emergency transmissions. Note if this is called
 * when the chip is active, it will abort the current operation, and if it's
 * called when the chip is sleeping, it will wake it up.
 */
void
ImSendEmergency(void)
{
    IM_MICS_PRIV *im = &ImMicsPriv;
    IM_MICS_CONFIG *mc = &ImPub.micsConfig;
    
    /* If MICS chip is active, put it to sleep (to abort), then make sure the
     * chip is awake (ImMicsWakeup() does nothing if already awake).
     */
    if (ImPub.micsState > IM_IDLE) ImMicsSleep();
    ImMicsWakeup();
    
    /* Disable MICS watchdog timer so we'll send emergency transmissions
     * indefinitely (until aborted or the base starts an emergency session).
     */
    MicsWrite(WDOG_CONTROL, WDOG_CLEAR_AND_DISAB);
    
    /* configure link to send emergency transmissions */
    ImConfigLink(&mc->emergencyLinkConfig);
    
    /* Update state and tell MICS chip to send emergency transmissions (note
     * emergency transmissions are the same as wakeup responses).
     */
    IM_SET_MICS_STATE(IM_SENDING_EMERGENCY);
    MicsWrite(INITCOM, 0);
    
    /* set flag for ImMicsSleep(), and maybe ImProcMicsSession() */
    im->flags |= IM_EMERGENCY;
}

/* This function starts a ZL7010X data test. If a session is already active,
 * the test will use the current session. Otherwise, this will put the ZL7010X
 * to sleep right away so it will be ready when the base tries to start a
 * session for the test.
 * 
 * PARAMETERS:
 *     specs:
 *         The data test specifications (for description, see MICS_DT_SPECS in
 *         "ZL7010XAdk\Sw\Includes\Adk\AnyMezz\MicsGeneral.h").
 * 
 * RETURN VALUES:
 *     None.
 */
void
ImStartDt(const MICS_DT_SPECS *specs)
{
    IM_MICS_PRIV *im = &ImMicsPriv;
    
    /* If not already in session, call ImMicsSleep() to make sure the ZL7010X
     * is sleeping. That way, it will be ready when the base tries to start a
     * session for the test. Note ImMicsSleep() is called before setting up the
     * test, because if it was called afterwards, it would clear the test setup.
     */
    if (ImPub.micsState != IM_IN_SESSION) ImMicsSleep();
    
    /* save data test specs */
    im->dt = *specs;
    
    /* If MICS_DT_TX is specified, set IM_DT_TX_PEND to tell ImProc() to
     * call ImProcDtTx() as often as possible (to transmit data).
     */
    if (specs->options & MICS_DT_TX) ImPub.pend |= IM_DT_TX_PEND;
}

/* Abort any MICS operation and put the MICS chip to sleep. Note this should
 * never be called in an ISR (to avoid race conditions).
 */
void
ImMicsSleep(void)
{
    IM_MICS_PRIV *im = &ImMicsPriv;
    #if MICS_REV >= ZL70102
        IM_MICS_CONFIG *mc = &ImPub.micsConfig;
    #endif
    
    /* if MICS chip is awake */
    if (ImPub.micsState != IM_SLEEPING) {
    
        /* If using a ZL70102/103 with 400 MHZ wakeup (instead of 2.45 GHz),
         * set DEVICEMODE_102.FAST_STARTUP before putting the chip to sleep.
         * That way, each time ImSearchForWake400() calls ImFastWakeup() to
         * wake the chip do a 400 MHz sniff, the chip will do a fast startup
         * instead of a normal startup.
         */
        #if MICS_REV >= ZL70102
            if (mc->flags & IM_400_MHZ_WAKEUP) {
                ImSetDeviceMode(IM_DEVICE_MODE | FAST_STARTUP);
            }
        #endif
        
        /* abort any MICS operation and put MICS chip to sleep */
        MicsSleep();
    }
        
    /* Make sure the IM_MICS_SLEEP_PEND flag is clear (so ImProc() won't keep
     * calling ImMicsSleep()), and clear any other pending flags that are
     * invalid when the chip is sleeping (to be safe).
     */
    ImPub.pend &= ~(IM_MICS_SLEEP_PEND | IM_MICS_SESSION_PEND |
        IM_MICS_RX_PEND | IM_DT_TX_PEND);
    
    /* Ensure the session LED, TX 400 LED, and RX 400 LED are off. Note that we
     * can't just wait for the next ImPeriodicLedTasks() to turn off the TX/RX
     * 400 LEDs, because if we did, it would try to access the ZL7010X while
     * it's sleeping. Also, if the TX 400 LED is constantly on (e.g. for the
     * 400 MHz TX carrier, or because an HK TX operation never received a reply
     * from the base), and/or the RX 400 LED is constantly on (e.g. for the
     * external RSSI), ImPeriodicLedTasks() wouldn't turn them off, so they
     * must be turned off here.
     */
    ImSessionLedOff();
    ImTx400LedOff();
    ImRx400LedOff();
    
    /* clear counts for ImTransmitMics() & ImReceiveMics() */
    im->txBytesAvail = 0;
    im->rxBytesAvail = 0;
    
    /* restart number of clocks to next external strobe for 2.45 GHz sniff */
    im->clocksToExtStrobe = IM_CLOCKS_PER_EXT_STROBE;

    /* clear any flags affected by sleep (in case they're set) */
    im->flags &= ~(IM_EMERGENCY | IM_RX_WILD_TID);
    
    /* clear data test option flags (in case data test was active) */
    im->dt.options = 0;
    
    /* update state to indicate ZL7010X is sleeping */
    IM_SET_MICS_STATE(IM_SLEEPING);
}

/* If "start" is true, this starts transmitting the 400 MHz TX carrier
 * (continuous wave). Otherwise, it stops the carrier. Note this is a test
 * mode, not a normal operational mode. The carrier should only be started
 * when the MICS chip is awake and idle, and should be stopped before starting
 * any other MICS operation (so it won't affect the operation, or vice-versa).
 */
void
ImStartTx400Carrier(BOOL start, UINT chan)
{
    /* if specified, start transmitting 400 MHz TX carrier (continuous wave) */
    if (start) {
        
        /* set channel & flag to report change to PC (see IM_STAT) */
        MicsWrite(MAC_CHANNEL, chan);
        ImPub.statFlags |= IM_LINK_STAT_CHANGED;
        
        /* if using ZL70102/103 */
        #if MICS_REV >= ZL70102
        
            /* start 400 MHz TX carrier (continuous wave) */
            if ((MicsRead(MAC_MODUSER) & TX_MOD_MASK) == TX_MOD_4FSK) {
                MicsWrite(TXIFFREQ_102, TX_IF_FREQ_OUT_SEL_102 | 0x08);
            } else {
                MicsWrite(TXIFFREQ_102, TX_IF_FREQ_OUT_SEL_102 | 0x0C);
            }
            MicsWrite(RF_GENENABLES, TX_IF | TX_RF | SYNTH);
            
        #else /* ZL70101 */
        
            /* start 400 MHz TX carrier (continuous wave) */
            MicsWrite(TXIFCTRL, TX_IF_FREQ_OUT_SEL_101 | TX_IF_BLOCK_ENAB);
            /**/
            if ((MicsRead(MAC_MODUSER) & TX_MOD_MASK) == TX_MOD_4FSK) {
                MicsWrite(TXIFFREQ_101, 0x08);
            } else {
                MicsWrite(TXIFFREQ_101, 0x0C);
            }
            MicsWrite(RF_GENENABLES, TX_IF | TX_RF | SYNTH);
            
        #endif
        
        /* turn on 400 MHz TX LED & tell ImPeriodicLedTasks() to leave it on */
        ImTx400LedOn(IM_NO_AUTO_OFF);
        
    } else {
        
        /* if using ZL70102/103 */
        #if MICS_REV >= ZL70102
        
            /* stop 400 MHz TX carrier */
            MicsWrite(RF_GENENABLES, 0);
            MicsWrite(TXIFFREQ_102, 0);
            
        #else /* ZL70101 */
            
            /* stop 400 MHz TX carrier */
            MicsWrite(TXIFCTRL, 0);
            MicsWrite(RF_GENENABLES, 0);
            
        #endif
        
        /* turn off 400 MHz TX LED */
        ImTx400LedOff();
    }
}

/* This performs an internal RSSI. This samples the RX signal level for 10 ms
 * and returns the average RSSI. Note that if needed, this automatically enables
 * the ZL7010X's internal RSSI, receiver, and internal ADC, and restores their
 * previous settings when done.
 *
 * Note this should only be called when the ZL7010X is awake and idle.
 *
 * RETURN VALUES:
 *     Returns the average RSSI over 10 ms. The returned value is always >= 0.
 */
UINT
ImIntRssi(void)
{
    UINT adcSum, adcCount, aveRssi, prevRxGenCtrl;
    BOOL prevAdcEnab, enabledRx;
    UD16 startMs;

    /* init variables referenced during cleanup */
    enabledRx = FALSE;

    /* ensure the ZL7010X's internal RSSI is enabled */
    prevRxGenCtrl = MicsRead(RXGENCTRL);
    MicsWrite(RXGENCTRL, prevRxGenCtrl | RX_INT_RSSI);

    /* If the ZL7010X's receiver is off, enable synth & RX, wait to give synth
     * time to coarse tune and lock, and set flag for cleanup logic later in
     * this function.
     */
    if (!MICS_RX_MODE) {

        MicsWrite(RF_GENENABLES, RX_RF | RX_IF | SYNTH);
        StDelayUs(MICS_LONG_SYNTH_LOCK_TIME_US);
        enabledRx = TRUE;
    }

    /* ensure the ZL7010X's internal ADC is enabled */
    prevAdcEnab = MicsEnabAdc(TRUE);

    /* take samples for 10 ms and sum them all */
    adcSum = adcCount = 0;
    for(startMs = StMs();;) {

        adcSum += MicsAdc(ADC_INPUT_400_MHZ_RX_RSSI);
        ++adcCount;

        /* if it's been long enough, stop */
        if (StElapsedMs(startMs, 10)) break;
    }

    /* calc average */
    aveRssi = adcSum / adcCount;

    /* restore the previous enable setting for the ZL7010X's internal ADC */
    (void)MicsEnabAdc(prevAdcEnab);

    /* if we enabled the ZL7010X's receiver, disable it */
    if (enabledRx) MicsWrite(RF_GENENABLES, 0);

    /* restore the previous enable setting for the ZL7010X's internal RSSI */
    MicsWrite(RXGENCTRL, prevRxGenCtrl);

    /* return average RSSI */
    return(aveRssi);
}

/* Process the start of a session. When a session starts (IRQ_LINKREADY),
 * ImLinkReadyIsr() sets the IM_MICS_SESSION_PEND flag to tell ImProc() to
 * call this function. By default, this function doesn't do anything when a
 * session starts. Rather, the implant just reports the state change to the
 * application on the PC (see IM_STAT). If desired, this function can be
 * customized to communicate with the base station. Note if the session was
 * established while sending emergency transmissions, IM_EMERGENCY will be set
 * in ImMicsPriv.flags (see ImSendEmergency()). This flag can be referenced to
 * determine if it's an emergency session.
 */
void
ImProcMicsSession(void)
{
    /* clear flag so ImProc() won't keep calling this function */
    ImPub.pend &= ~IM_MICS_SESSION_PEND;
}

/* Process received data. When the MICS chip receives data (IRQ_RXNOTEMPTY),
 * ImMicsRxNotEmptyIsr() sets the IM_MICS_RX_PEND flag to tell ImProc() to
 * call this function.
 * 
 * By default, this function just leaves the data in the MICS RX buffer for
 * the PC to read. If desired, this function can be customized to read and
 * process the data. Note if this function reads the data from the MICS RX
 * buffer, it should re-enable IRQ_RXNOTEMPTY before returning, so if more
 * data is received, IRQ_RXNOTEMPTY will recur and set IM_MICS_RX_PEND again.
 */
void
ImProcMicsRx(void)
{
    IM_MICS_PRIV *im = &ImMicsPriv;
    BOOL enabRxInt = FALSE;
    
    /* if data test RX is active */
    if (im->dt.options & MICS_DT_RX) {
        
        /* indicate we want to re-enable IRQ_RXNOTEMPTY when done */
        enabRxInt = TRUE;
        
        /* process received data */ 
        ImProcDtRx();
    }
    
    /* Clear IM_MICS_RX_PEND so ImProc() won't keep calling this function,
     * then re-enable IRQ_RXNOTEMPTY if needed. Note if IRQ_RXNOTEMPTY was
     * already re-enabled (by ImReceiveMics()), and it occurs before we clear
     * IM_MICS_RX_PEND here, it will recur when we re-enable it here, so it
     * will still set IM_MICS_RX_PEND again.
     */
    ImPub.pend &= ~IM_MICS_RX_PEND;
    if (enabRxInt) {
        /* clear IRQ_RXNOTEMPTY so it won't recur if RX buf is now empty */
        MicsWrite(IRQ_RAWSTATUS1, ~IRQ_RXNOTEMPTY);
        MicsWrite(IRQ_ENABLESET1, IRQ_RXNOTEMPTY);
    }
}

/* When the data test RX is active, ImProcMicsRx() calls this to process the
 * received data. This reads the data contained in the ZL7010X's RX buffer,
 * and validates it if desired.
 */
static void
ImProcDtRx(void)
{
    IM_MICS_PRIV *im = &ImMicsPriv;
    UINT rxBlockSize = ImPub.rxBlockSize;
    UD8 buf[RXBUFF_BSIZE_MAX];
    UINT i, n, nMax, dataInc;
    UD8 data;
    
    /* get data we expect the next block to start with */
    data = im->dt.rxData;
    
    /* get value to increment the expected data by (0 or 1) */
    dataInc = (im->dt.options & MICS_DT_INC_RX) ? 1 : 0;
    
    /* Init number of blocks received this time (n) and the max (nMax). The
     * max ensures we won't remain in ImProcDtRx() too long if it can't keep up
     * with the RX data, so the implant can still report status to the PC, etc.
     */
    n = 0; nMax = RXBUFF_USED_MAX;
    
    while(1) {
        
        /* if no data block is available (ZL7010X RX buf empty), stop */
        if (ImReceiveMics(buf, rxBlockSize, 0) == 0) break;
        
        /* if we want to validate the received data, do so */ 
        if (im->dt.options & MICS_DT_VALIDATE) {

            /* if RX block size = max, check bit 0 of first byte */
            if (rxBlockSize == RXBUFF_BSIZE_MAX) {
                if ((buf[0] ^ data) & 1) ++ImPub.dataStat.dataErrCount;
                data += dataInc;
                i = 1;
            } else {
                i = 0;
            }
            for(; i < rxBlockSize; ++i) {
                if (buf[i] != data) ++ImPub.dataStat.dataErrCount;
                data += dataInc;
            }
        }
        
        /* if we've received the max number of blocks, stop */
        if (++n >= nMax) break;
    }
    
    /* save the data we expect the next block to start with */
    im->dt.rxData = data;
    
    /* update data status and set flag to report change to PC (see IM_STAT) */
    ImPub.dataStat.rxBlockCount += n;
    ImPub.statFlags |= IM_DATA_STAT_CHANGED;
}

/* When the data test TX is active, ImProc() calls this as often as possible
 * to transmit data. This checks if there is any space available in the
 * ZL7010X's TX buffer, and if so, writes data to it.
 */
void
ImProcDtTx(void)
{
    IM_MICS_PRIV *im = &ImMicsPriv;
    UINT txBlockSize = ImPub.txBlockSize;
    UD8 buf[TXBUFF_BSIZE_MAX];
    UINT i, n, nMax;
    UD8 data;
    
    /* if not in session, don't try to transmit any test data */
    if (ImPub.micsState != IM_IN_SESSION) return;
    
    /* init data to transmit in next block */
    data = im->dt.txData;
    if (im->dt.options & MICS_DT_INC_TX) {
        for(i = 0; i < txBlockSize; ++i) buf[i] = data++;
    } else {
        for(i = 0; i < txBlockSize; ++i) buf[i] = data;
    }
    
    /* Init number of blocks transmitted this time (n) and the max (nMax). If
     * im->dt.txBlockCount is 0 (transmit until stopped) or > TXBUFF_USED_MAX,
     * set nMax = TXBUFF_USED_MAX. This ensures we won't remain in ImProcDtTx()
     * too long if it can't keep up with the ZL7010X TX, so the implant can
     * still report status to the PC, etc.
     */
    n = 0; 
    nMax = im->dt.txBlockCount;
    if ((nMax == 0) || (nMax > TXBUFF_USED_MAX)) nMax = TXBUFF_USED_MAX;
    
    while(1) {
        
        /* if ZL7010X TX buf is full, save data to start next block & stop */
        if (ImTransmitMics(buf, txBlockSize, 0) == 0) {
            im->dt.txData = buf[0];
            break;
        }
        
        /* if max transmitted, save data to start next block & stop */
        if (++n >= nMax) {
            im->dt.txData = data;
            break;
        }
        
        /* if incrementing data, update data for next block */
        if (im->dt.options & MICS_DT_INC_TX) {
            for(i = 0; i < txBlockSize; ++i) buf[i] = data++;
        }
    }
    
    /* if no more data was written to TX buf, skip remaining logic */
    if (n == 0) return;
    
    /* if we're not transmitting forever, update blocks left to transmit */
    if (im->dt.txBlockCount) {
        
        /* if all blocks have been transmitted, stop data test TX */
        if ((im->dt.txBlockCount -= n) == 0) ImPub.pend &= ~IM_DT_TX_PEND;
    }
    
    /* update data status and set flag to report change to PC (see IM_STAT) */
    ImPub.dataStat.txBlockCount += n;
    ImPub.statFlags |= IM_DATA_STAT_CHANGED;
}

/* This function writes data to the ZL7010X's TX buffer, which will transmit
 * the data as soon as possible. If there's not enough space in the TX buffer,
 * this function will loop writing the data as space becomes available. If the
 * specified timeout expires or the ZL7010X asserts IRQ_CLOST before all of the
 * data has been written, it stops and returns the number of bytes written.
 * 
 * Note the specified number of bytes must be a multiple of the TX block
 * size (TXBUFF_BSIZE setting). Otherwise, it has not been determined how the
 * ZL7010X will behave.
 * 
 * Note for this function to work properly, MicsWriteTxBuf() must not be
 * called directly (only through this function), and ImSetTxBlockSize() must
 * be called to change TXBUFF_BSIZE (instead of calling MicsWrite() directly).
 * Otherwise, this function won't track the space available in the TX buffer
 * correctly, and won't use the correct TX block size.
 * 
 * PARAMETERS:
 *     data:
 *         The data to transmit.
 *     nBytes:
 *         The number of data bytes. Note this must be a multiple of the TX
 *         block size (TXBUFF_BSIZE setting). Otherwise, it has not been
 *         determined how the ZL7010X will behave.
 *     timeoutMs:
 *         The maximum time to wait for space in the ZL7010X's TX buffer. If the
 *         specified timeout expires or IRQ_WDOG occurs before all of the data
 *         has been written to the TX buffer, this function will stop and
 *         return the number of bytes that were written. If the timeout is 0,
 *         it will write as much data as the TX buffer currently has space for
 *         and return immediately.
 *
 * RETURN VALUES:
 *     This function returns the number of bytes written to the ZL7010X TX buf.
 */
UINT
ImTransmitMics(const void *data, UINT nBytes, UINT timeoutMs)
{
    IM_MICS_PRIV *im = &ImMicsPriv;
    UINT nLeft, nAvail;
    UD16 startMs;
    
    /* init bytes left to write, bytes available in TX buf, & start time */
    if ((nLeft = nBytes) == 0) return(0);
    nAvail = im->txBytesAvail;
    startMs = StMs();
    
    /* Turn on the 400 MHz TX LED & tell ImPeriodicLedTasks() to turn it off
     * automatically when the ZL7010X's TX buffer becomes empty. ImProc()
     * periodically calls ImPeriodicLedTasks() to update the LED's.
     */
    ImTx400LedOn(IM_AUTO_OFF);
    
    /* until the specified number of bytes have been written to TX buf */
    while(1) {
        
        /* if we've used all the bytes we know were available in the TX buf */
        if (nAvail == 0) {
                
            /* wait for more space in the TX buf */
            while((nAvail = (TXBUFF_USED_MAX - MicsRead(TXBUFF_USED))) == 0) {
                
                /* If it's been too long, or the ZL7010X asserted IRQ_CLOST,
                 * return the number of bytes written to the TX buf.
                 */
                if ((timeoutMs == 0) || StElapsedMs(startMs, timeoutMs) ||
                    (MicsPub.irqStat1Latch & IRQ_CLOST)) {
                    
                    im->txBytesAvail = 0;
                    return(nBytes - nLeft);
                }
                
                /* Wait ~1 ms to allow the ZL7010X to transmit some blocks from
                 * its TX buf. The TX buf holds 64 blocks, and the ZL7010X will
                 * take at least ~194 us to transmit each block (155 bits /
                 * 800000 bps). Thus, there's no need to recheck TXBUFF_USED
                 * more than once a millisecond, and the extra SPI bus activity
                 * would just waste power.
                 */
                StDelayMs(1);
            }
            /* convert blocks available to bytes available */
            nAvail *= ImPub.txBlockSize;
        }
        
        /* if bytes left <= bytes available, write bytes left and return */
        if (nLeft <= nAvail) {
            
            MicsWriteTxBuf(data, nLeft);
            im->txBytesAvail = nAvail - nLeft;
            return(nBytes);
            
        } else {
        
            /* write to available space, then update data pointer & counts */
            MicsWriteTxBuf(data, nAvail);
            data = (UD8 *)data + nAvail;
            nLeft -= nAvail;
            nAvail = 0;
        }
    }
}

/* This function reads data from the ZL7010X's RX buffer. If there's not enough
 * data available, this function will loop reading data as it becomes available.
 * If the specified timeout expires or the ZL7010X asserts IRQ_CLOST before all
 * of the requested data has been read, it stops and returns the number of bytes
 * read. In that case, it also clears IRQ_RXNOTEMPTY in MicsPub.irqStat1Latch
 * and re-enables the interrupt.
 * 
 * Note the specified number of bytes must be a multiple of the RX block
 * size (RXBUFF_BSIZE setting). Otherwise, it has not been determined how the
 * ZL7010X will behave.
 * 
 * Note for this function to work properly, MicsReadRxBuf() must not be
 * called directly (only through this function), and ImSetRxBlockSize() must
 * be called to change RXBUFF_BSIZE (instead of calling MicsWrite() directly).
 * Otherwise, this function won't track the number of bytes available in the
 * RX buffer correctly, and won't use the correct RX block size.
 * 
 * PARAMETERS:
 *     buf:
 *         The buffer to put the received data in.
 *     nBytes:
 *         The number of bytes to receive. Note this must be a multiple of the
 *         RX block size (RXBUFF_BSIZE setting). Otherwise, it has not been
 *         determined how the ZL7010X will behave. It should also be <= the
 *         size of the specified buffer.
 *     timeoutMs:
 *         The maximum time to wait for data in the ZL7010X's RX buffer. If
 *         the specified timeout expires or IRQ_WDOG occurs before all of the
 *         requested data has been read, this function will stop and return the
 *         number of bytes read (if the timeout is 0, it will read any data in
 *         the RX buffer and return immediately). In that case, it also clears
 *         IRQ_RXNOTEMPTY in MicsPub.irqStat1Latch and re-enables the interrupt.
 *
 * RETURN VALUES:
 *     This function returns the number of bytes read from the ZL7010X RX buf.
 */
UINT
ImReceiveMics(void *buf, UINT nBytes, UINT timeoutMs)
{
    IM_MICS_PRIV *im = &ImMicsPriv;
    UINT nLeft, nAvail;
    UD16 startMs;
    
    /* init bytes left to read, bytes available in RX buf, & start time */
    if ((nLeft = nBytes) == 0) return(0);
    nAvail = im->rxBytesAvail;
    startMs = StMs();
    
    /* until the specified number of bytes have been read from the RX buf */
    while(1) {
        
        /* if we've read all the data we know was available in the RX buf */
        if (nAvail == 0) {
            
            /* wait for more data in the RX buf */
            while((nAvail = MicsRead(RXBUFF_USED)) == 0) {
                
                /* Clear IRQ_RXNOTEMPTY in irqStat1Latch, re-enable it,
                 * then wait for it to occur. Note if IRQ_RXNOTEMPTY is
                 * already enabled at this point, and it occurs before we
                 * clear irqStat1Latch.IRQ_RXNOTEMPTY here, it will recur
                 * when we re-enable it here, so it will still set
                 * irqStat1Latch.IRQ_RXNOTEMPTY again.
                 */
                MicsPub.irqStat1Latch &= ~IRQ_RXNOTEMPTY;
                MicsWrite(IRQ_ENABLESET1, IRQ_RXNOTEMPTY);
                /**/
                while((MicsPub.irqStat1Latch & IRQ_RXNOTEMPTY) == 0) {
            
                    /* If it's been too long, or the ZL7010X asserted IRQ_CLOST,
                     * return the number of bytes read from the RX buf.
                     */
                    if ((timeoutMs == 0) || StElapsedMs(startMs, timeoutMs) ||
                        (MicsPub.irqStat1Latch & IRQ_CLOST)) {
                    
                        im->rxBytesAvail = 0;
                        return(nBytes - nLeft);
                    }
                }
            }
            /* convert blocks available to bytes available */
            nAvail *= ImPub.rxBlockSize;
        }
        
        /* if bytes left <= bytes available, read bytes left and return */
        if (nLeft <= nAvail) {
            
            MicsReadRxBuf(buf, nLeft);
            im->rxBytesAvail = nAvail - nLeft;
            return(nBytes);
            
        } else {
        
            /* read available bytes, then update buf pointer & counts */
            MicsReadRxBuf(buf, nAvail);
            buf = (UD8 *)buf + nAvail;
            nLeft -= nAvail;
            nAvail = 0;
        }
    }
}

/* ImProc() calls this each clock tick (see IM_MS_PER_CLOCK) to perform
 * periodic MICS tasks, such as checking for link quality errors and searching
 * for a 400 MHz wakeup signal.
 */
void
ImPeriodicMicsTasks(BOOL clockOverdue)
{
    IM_MICS_CONFIG *mc = &ImPub.micsConfig;
    IM_MICS_PRIV *im = &ImMicsPriv;
    
    /* If configured for 400 MHz wakeup and the ZL7010X is sleeping, check if
     * it's time to search for a 400 MHz wakeup signal. Note this is done even
     * if the clock was overdue. That way, "clocksToWake400Search" will always
     * be updated, so the timing of the 400 MHz wakeup won't permanently shift
     * relative to the base station (for more, see BSM_MS_PER_WAKE_400_LISTEN
     * in "ZL7010XAdk/Sw/BsmMezz/Apps/Standard/BsmAppMics.c").
     */
    if ((mc->flags & IM_400_MHZ_WAKEUP) && (ImPub.micsState == IM_SLEEPING)) {
        
        /* if it's time to search for a 400 MHz wakeup signal, do so */
        if (--im->clocksToWake400Search == 0) {
            
            /* restart number of clocks to next search */
            im->clocksToWake400Search = IM_CLOCKS_PER_WAKE_400_SEARCH;
            ImSearchForWake400();
        }
    }
    
    /* If the clock is overdue, skip the remaining tasks so we'll never do
     * them multiple times in rapid succession (it's ok if the timing for
     * the remaining tasks sometimes shifts).
     */
    if (clockOverdue) return;
    
    /* if ZL7010X is active */
    if (ImPub.micsState > IM_IDLE) {
            
        /* if it's time to update the link quality statistics, do so */
        if (--im->clocksToLinkQualUpdate == 0) {
            
            /* restart number of clocks to next link quality update */
            im->clocksToLinkQualUpdate = IM_CLOCKS_PER_LINK_QUAL_UPDATE;
            ImUpdateLinkQual();
        }
    }

    /* If configured for 2.45 GHz wakeup using an external strobe to trigger
     * each sniff, and the ZL7010X is sleeping, check if it's time to strobe
     * the ZL7010X's WU_EN pin to trigger a 2.45 GHz sniff.
     */
    if (!(mc->flags & IM_400_MHZ_WAKEUP) &&
        (mc->flags & IM_EXT_STROBE_FOR_245_GHZ_SNIFF) &&
        (ImPub.micsState == IM_SLEEPING)) {

        /* if it's time to generate an external strobe for a 2.45 GHz sniff */
        if (--im->clocksToExtStrobe == 0) {

            /* restart number of clocks to next external strobe */
            im->clocksToExtStrobe = IM_CLOCKS_PER_EXT_STROBE;

            /* strobe the ZL7010X's WU_EN pin to trigger a 2.45 GHz sniff */
            MICS_WR_WU_EN(TRUE);
            #if MICS_REV >= ZL70102
                StDelayUs(200);
            #else /* ZL70101 */
                StDelayUs(280);
            #endif
            MICS_WR_WU_EN(FALSE);
        }
    }
}

/* When using 400 MHz wakeup, ImPeriodicMicsTasks() periodically calls this
 * function to search for a 400 MHz wakeup signal.
 */
__inline void
ImSearchForWake400(void)
{
    UINT searchBands;
    
    /* wake the ZL7010X (using fast wakeup) to search for a 400 MHz signal */
    ImFastWakeup();
    
    /* search bands for signal (returns bands for which a signal is detected) */
    searchBands = ImSearchForRssi();
    
    /* if a signal was present for any band, search bands for wakeup headers */
    if (searchBands) {
        
        /* Clear DEVICEMODE_102.FAST_STARTUP (ZL70102/103 only), put the
         * ZL7010X back to sleep, do a normal direct wakeup, then search the
         * indicated bands for a wakeup signal. Note if ImSearchBands() doesn't
         * detect a wakeup signal, it will put the ZL7010X back to sleep.
         */
        #if MICS_REV >= ZL70102
            ImSetDeviceMode(IM_DEVICE_MODE);
        #endif
        ImFastSleep();
        ImMicsWakeup();
        ImSearchBands(searchBands);
        
    } else { /* else, just put the ZL7010X back to sleep */
        
        ImFastSleep();
    }
}

/* When using 400 MHz wakeup, ImSearchForWake400() calls this function to
 * search for an increased signal strength (RSSI) in any of the 400 MHz wakeup
 * bands. It returns a mask indicating which bands need to be searched for a
 * 400 MHz wakeup signal.
 */
__inline UINT
ImSearchForRssi(void)
{
    IM_MICS_PRIV *im = &ImMicsPriv;
    UINT band, nextChan, rssi;
    UD16 chanTimeCount;
    UINT searchBands;
    
    /* check for a signal (RSSI) in each band of channels */
    for(band = 0, nextChan = 0, searchBands = 0;;) {
        
        /* if we've seen enough consecutive misses, re-check the max RSSI */
        if (im->band[band].consecMissCountdown == 0) ImCheckMaxRssi(band);
        
        /* take a single RSSI sample for the band */
        MicsWrite(ADCCTRL, ADC_INPUT_400_MHZ_RX_RSSI | ADC_ENAB | ADC_START);
        while(((rssi = MicsRead(ADCOUTPUT)) & ADC_COMPLETE) == 0);
        MicsWrite(ADCCTRL, ADC_INPUT_400_MHZ_RX_RSSI | ADC_ENAB);
        /* 
         * If a retry is pending (to search band again), take RSSI samples for
         * 1 ms (see ImGetRetryRssi() for explanation).
         */
        if (im->band[band].retryCountdown) rssi = ImGetRetryRssi();
        
        /* if there's another band to sample, set channel for next band */
        if ((nextChan += 3) <= MAX_MICS_CHAN) {
            MicsWrite(SYNTH_CHANNEL, nextChan);
            chanTimeCount = StCount();
        }
        rssi &= ADC_RESULT_MASK; /* make sure ADC_COMPLETE is clear */
        
        /* While we're waiting for the synthesizer to settle on the next
         * channel, process the RSSI sample taken for the current band
         * (determines whether to search the channels in the band).
         */
        searchBands |= ImProcRssi(band, rssi);
        
        /* if there are no more bands to sample, stop */
        if (++band >= IM_NUM_BANDS) break;
        
        /* Wait long enough to ensure the ZL7010X has settled on the next
         * channel before taking the next RSSI sample.
         */
        StWaitUs(chanTimeCount, MICS_CHAN_SETTLING_TIME_US);
    }

    return(searchBands);
}

/* If using 400 MHz wakeup, ImSearchForWake400() calls this function to wake the
 * ZL7010X and prepare it to search for an RSSI signal (see ImSearchForRssi()).
 * This does a special fast wakeup to prepare the ZL7010X to do RSSI's (less
 * power than a normal direct wakeup). Note before doing anything else with
 * the ZL7010X, ImFastSleep() must be called to put it back to sleep, and then
 * ImMicsWakeup() to do a normal direct wakeup.
 */
static void
ImFastWakeup(void)
{
    #if MICS_REV >= ZL70102
        ImFastWakeup102();
    #else
        ImFastWakeup101();
    #endif
}

/* If using 400 MHz wakeup with a ZL70102/103, ImFastWakeup() calls this
 * function to wake the ZL70102/103 and prepare it to search for an RSSI
 * signal (see ImSearchForRssi()). Note this should only be called when the
 * ZL70102/103 device mode is set for fast startup (ImMicsSleep() sets the
 * device mode before it puts the ZL70102/103 to sleep). This does a special
 * fast wakeup to prepare the ZL70102/103 to do RSSI's (less power than a
 * normal direct wakeup). Note before doing anything else with the ZL70102/103,
 * ImFastSleep() must be called to put it back to sleep, and then ImMicsWakeup()
 * to do a normal direct wakeup.
 */
#if MICS_REV >= ZL70102
__inline void
ImFastWakeup102(void)
{
    UD16 startCount;
    
    /* disable ZL7010X interrupt (not used to do RSSI's for 400 MHz wakeup) */
    MICS_WR_INT_ENAB(FALSE);
    
    /* Set WU_EN to wakeup the ZL70102/103, then wait for it to assert the
     * interrupt to ensure it finished waking up. Note since we configured the
     * ZL70102/103 to do a fast startup, it won't check the CRC, so it won't
     * assert IRQ_CRCERR. Also, since we don't start the synthesizer until after
     * the wakeup (later in this function), it won't assert IRQ_SYNTHLOCKFAIL.
     * Thus, the first interrupt must be IRQ_RADIOREADY.
     */
    MICS_WR_WU_EN(TRUE);
    while(!MICS_INT_ASSERTED);
    
    /* set channel to 0 (defaults to 5 for direct wakeup) */
    MicsWrite(SYNTH_CHANNEL, 0);
    
    /* enable synthesizer & save time at which it was enabled */
    MicsWrite(RF_GENENABLES, SYNTH);
    startCount = StCount();
    /*
     * If using a ZL70103 or later, select the wide RX bandwidth so the RSSI
     * for each 400 MHz sniff will always cover 3 channels.
     */
    if (MICS_REV >= ZL70103) MicsWrite(MAC_OVERRIDE_103, RX_BANDWIDTH_WIDE);
    /*
     * Wait until it's time to enable the internal RSSI blocks & receiver,
     * then enable them.
     */
    StWaitUs(startCount, IM_ENAB_RSSI_TIME_US);
    MicsWrite(RF_GENENABLES, RSSI_SNIFF_102 | RX_RF | SYNTH);
    /*
     * Wait until it's time to enable the ADC, then enable it.
     */
    StWaitUs(startCount, IM_RSSI_READY_TIME_US - MICS_ADC_START_TIME_US);
    MicsWrite(ADCCTRL, ADC_INPUT_400_MHZ_RX_RSSI | ADC_ENAB);
    /*
     * Wait until the ZL70102/103 is ready to do the first RSSI.
     */
    StWaitUs(startCount, IM_RSSI_READY_TIME_US);
     
    /* if IRQ_RADIOFAIL, handle it & set error to report to PC if needed */
    if (MicsRead(IRQ_STATUS2) & IRQ_RADIOFAIL) ImRadioFailIsr(0);
}
#endif

/* If using 400 MHz wakeup with a ZL70101 or later, ImFastWakeup() calls
 * this function to wake the ZL70101 and prepare it to search for an RSSI
 * signal (see ImSearchForRssi()). This does a special fast wakeup to prepare
 * the ZL70101 to do RSSI's (less power than a normal direct wakeup). Note
 * before doing anything else with the ZL70101, ImFastSleep() must be called to
 * put it back to sleep, and then ImMicsWakeup() to do a normal direct wakeup.
 */
#if MICS_REV == ZL70101
__inline void
ImFastWakeup101(void)
{
    IM_MICS_PRIV *im = &ImMicsPriv;
    UD16 startCount;
    
    /* If the SPI clock is > 2 MHz, temporarily reduce it so it will work with
     * the ZL70101 in its default state (2 MHz max). Note it will be restored
     * later in this function.
     */
    if (IM_LSPI_CLK_FREQ > 2000000UL) {
        ImConfigSpi(IM_LSPI_CLK_DIV_FOR_2_MHZ);
    }
    
    /* disable MICS interrupt (not used to do RSSI's for 400 MHz wakeup) */
    MICS_WR_INT_ENAB(FALSE);
    
    /* Set WU_EN to wakeup the MICS chip, then wait for it to initialize its
     * SPI interface. For 400 MHz wakeup, we need to communicate with the chip
     * as soon as possible so we can reduce the startup power. Thus, instead
     * of waiting for IRQ_RADIOREADY, wait until we can read the INTERFACE_MODE
     * register via SPI. Note this uses INTERFACE_MODE so MicsRead() won't try
     * to do any paging, since that won't work until the SPI interface is up.
     */
    MICS_WR_WU_EN(TRUE);
    while(MicsRead(INTERFACE_MODE) != INTERFACE_MODE_RESET_101);
    /*
     * Set MAC_CTRL.SKIP_CRC as soon as possible to tell the ZL70101 to skip the
     * CRC check. The CRC is calculated during the XOSU time (reg_wakeup_xosu).
     * If the calculation hasn't finished when the XOSU time ends, and SKIP_CRC
     * is 0, the ZL70101 will wait 0.85 ms to allow the calculation to finish,
     * then check the CRC. If SKIP_CRC is 1, the ZL70101 won't wait, shortening
     * the startup time by ~0.85 ms (to save power).
     * 
     * Note if the CRC calculation finishes before the XOSU time ends, the
     * ZL70101 won't wait 0.85 ms before checking the CRC, so setting SKIP_CRC
     * won't make any difference.
     * 
     * Note if for some reason the ZL70101 already checked the CRC and detected
     * a CRC error at this point, setting SKIP_CRC will just cause the ZL70101
     * to ignore the CRC error and continue with the startup, so the end result
     * will be the same. If desired, the implant can periodically instigate a
     * normal wakeup to detect and correct any CRC error.
     */
    MicsWrite(MAC_CTRL, SKIP_CRC);
    
    /* Enable the synthesizer and save the time at which it was enabled (so we
     * can check how much time has elapsed later on). This is done first because
     * the synthesizer takes the most time to start. Before enabling the
     * synthesizer, set SYNTH_CT and SYNTH_CTRIM to their predetermined values
     * (im->synthCt and im->synthCtrim) so the synthesizer will lock faster.
     * Note the ZL70101 will restore these registers from its wakeup stack,
     * but at this point, it might not have restored them yet, so we must set
     * them directly before enabling the synthesizer.
     */
    MicsWrite(SYNTH_CT, im->synthCt);
    MicsWrite(SYNTH_CTRIM, im->synthCtrim);
    MicsWrite(RF_GENENABLES, SYNTH);
    startCount = StCount();
    
    /* Set WAKEUP_CTRL. At this point, we know the MAC has started, so it will
     * draw >= 200 uA, ensuring the VDDD regulator is stable. Thus, clear
     * WAKEUP_CTRL.ADD_MIN_VDDD_LOAD to eliminate the minimum load for the VDDD
     * regulator (to save power). Note ImFastSleep() will set this bit again
     * before putting the ZL70101 back to sleep, to ensure the VDDD regulator
     * is stable the next time it wakes up, and so the ZL70101 won't detect a
     * CRC error and assert IRQ_RADIOFAIL the next time it wakes up.
     * 
     * Note WAKEUP_CTRL.ADD_MIN_VDDA_LOAD isn't cleared because even though
     * the synthesizer is enabled, we're not certain it will draw more than
     * 200 uA at this point.
     * 
     * Note if this changes WAKEUP_CTRL before the ZL70101 finishes calculating
     * the CRC, it will affect the CRC calculation (WAKEUP_CTRL is included in
     * the CRC), but since we set MAC_CTRL.SKIP_CRC earlier, the ZL70101 won't
     * check the CRC after the calculation is finished, so it won't matter.
     */
    MicsWrite(WAKEUP_CTRL, ADD_MIN_VDDA_LOAD);
    
    /* Various other register settings for the 400 MHz wakeup RSSI:
     * 
     * CALSELECT1_101: Prevent the default calibrations (to save power). These
     * calibrations aren't needed to perform RSSI's for 400 MHz wakeup.
     * 
     * RXBIASTR: Bias current settings. Note it may be possible to use
     * RF_LNA_BIAS_2 and RF_MIXER_BIAS_2, or even RF_LNA_BIAS_1 and
     * RF_MIXER_BIAS_1. This is still under test. It's also being determined
     * if it's better to do this here, or if the power used is the same if it's
     * done just before the RSSI. In any case, since we're waiting for the
     * synthesizer to power up, there's time to do it here.
     * 
     * RXIFFILTERTUNE: Restore the calibrated setting for RXIFFILTERTUNE
     * (reg_rf_rxiffiltertune). During normal wakeup, the MICS chip 
     * automatically performs this calibration, but since the calibrations are
     * bypassed here, we need to restore the saved value directly to improve
     * the RSSI's. ImConfigMics() saves the calibrated value when it configures
     * the MICS chip for 400 MHz wakeup. The application may also want to
     * do a normal wakeup every now and then to update the calibrated value.
     */
    MicsWrite(CALSELECT1_101, CAL_NONE);
    MicsWrite(RXBIASTR, IF_FILT_BIAS_0 | IF_DET_FILT_BIAS_0 |
        RF_LNA_BIAS_3 | RF_MIXER_BIAS_3);
    MicsWrite(RXIFFILTERTUNE, im->rxIfFilterTune);
    
    /* If the SPI clock is > 2 MHz, increase the ZL0101's max SPI clock
     * setting, then restore the normal SPI clock (it was reduced earlier so
     * it would work with the ZL70101 in its default state after waking up).
     * If the SPI clock is <= 1 MHz, lower the ZL70101's max SPI clock
     * setting (to reduce power).
     */
    if (IM_LSPI_CLK_FREQ > 2000000UL) {
        
        MicsWrite(INTERFACE_MODE, MAX_SPI_CLOCK_4_MHZ);
        ImConfigSpi(IM_LSPI_CLK_DIV);
        
    } else if (IM_LSPI_CLK_FREQ <= 1000000UL) {
        
        MicsWrite(INTERFACE_MODE, MAX_SPI_CLOCK_1_MHZ);
    }
    
    /* Wait for ZL70101 to assert interrupt to ensure it finished waking up.
     * Note the first interrupt should normally be IRQ_RADIOREADY. It shouldn't
     * be IRQ_CRCERR because we set MAC_CTRL.SKIP_CRC above. It might be
     * IRQ_SYNTHLOCKFAIL if the synthesizer fails to lock. Whatever the case,
     * we'll just continue here (shouldn't hurt anything), and any IRQ_CRCERR
     * or IRQ_SYNTHLOCKFAIL will be detected later in this function when it
     * checks IRQ_STATUS2 for IRQ_RADIOFAIL.
     */
    while(!MICS_INT_ASSERTED);
            
    /* Wait until it's time to enable the internal RSSI blocks & receiver,
     * then enable them. The RXTESTENABLE test register is used to enable the
     * RX_IF blocks needed to do RSSI's, and RF_GENENABLES is used to enable
     * the RX_RF block (note RX_IF isn't set in RF_GENENABLES because that
     * would enable all of the RX_IF blocks).
     */
    StWaitUs(startCount, IM_ENAB_RSSI_TIME_US);
    MicsWrite(RXGENCTRL, RX_INT_RSSI | RX_NORMAL_INPUT | RX_DC_REMOVAL);
    MicsWrite(RXTESTENABLE, 0xF0);
    MicsWrite(RF_GENENABLES, RX_RF | SYNTH);
    /*
     * Wait until it's time to enable the ADC, then enable it.
     */
    StWaitUs(startCount, IM_RSSI_READY_TIME_US - MICS_ADC_START_TIME_US);
    MicsWrite(ADCCTRL, ADC_INPUT_400_MHZ_RX_RSSI | ADC_ENAB);
    /*
     * Wait until the ZL70101 is ready to do the first RSSI.
     */
    StWaitUs(startCount, IM_RSSI_READY_TIME_US);
    
    /* if IRQ_RADIOFAIL, handle it & set error to report to PC if needed */
    if (MicsRead(IRQ_STATUS2) & IRQ_RADIOFAIL) ImRadioFailIsr(0);
}
#endif

/* When using 400 MHz wakeup, this is called to put the ZL7010X back to sleep
 * after a fast wakeup (see ImFastWakeup()).
 */
static void
ImFastSleep(void)
{
    /* If using a ZL70101, restore WAKEUP_CTRL (modified by ImFastWakeup101()).
     * This is needed so the ADD_MIN_VDDD_LOAD bit will be set the next time
     * the ZL70101 wakes up (to ensure the VDDD voltage regulator will be
     * stable). This also prevents the ZL70101 from detecting a CRC error the
     * next time it wakes up (the ZL70101 includes WAKEUP_CTRL in its CRC, and
     * ImFastWakeup101() doesn't update the CRC after it changes WAKEUP_CTRL).
     */
    #if MICS_REV == ZL70101
        MicsWrite(WAKEUP_CTRL, WAKEUP_CTRL_RESET_101 & ~ENAB_INT_STROBE_OSC);
    #endif
    /*
     * Tell the ZL7010X to sleep, wait for it to de-assert the interrupt so
     * we know it finished going to sleep (it must be asserting the interrupt
     * at this point because we never cleared IRQ_RADIOREADY), then clear the
     * MICS interrupt flag and re-enable the interrupt.
     */
    MicsSleep(); 
    while(MICS_INT_ASSERTED);
    MICS_WR_INT_FLAG(FALSE); 
    MICS_WR_INT_ENAB(TRUE);
}

/* When using 400 MHz wakeup, ImSearchForRssi() calls this function to process
 * the RSSI reading for the specified band. If it determines the band needs to
 * be searched for a 400 MHz wakeup signal, it returns the search mask for the
 * band (ImBandFlag[band]). Otherwise, it returns 0.
 */
static UINT
ImProcRssi(UINT band, UINT rssi)
{
    IM_WAKE_400_BAND *b = &ImMicsPriv.band[band];
    
    /* If RSSI < (maxRssi - IM_RSSI_MISS_DELTA), it's a miss (for more
     * information, see the comment for IM_RSSI_MISS_DELTA).
     */
    if ((rssi + IM_RSSI_MISS_DELTA) < b->maxRssi) {
        
        /* Decrement the consecutive miss countdown. If this gets to 0, then
         * the next time ImSearchForRssi() is called to search for RSSI's, it
         * will call ImCheckMaxRssi(), which will sample for 10 ms to check the
         * band's maximum RSSI and reduce it if needed.
         */
        if (--b->consecMissCountdown == 0) {
            
            /* Increase "consecMissRestart" so if ImCheckMaxRssi() finds the
             * maximum RSSI still holds, more consecutive misses will be
             * required to trigger the next check (to save power).
             */
            if (b->consecMissRestart <= 127) {
                b->consecMissRestart *= 2;
            } else {
                b->consecMissRestart = 255;
            }
        }
        
        /* if any retries are pending, decrement retry countdown */
        if (b->retryCountdown) --b->retryCountdown;
        
    } else { /* else, it's a hit */
        
        /* restart consecutive miss countdown */
        b->consecMissCountdown = b->consecMissRestart;
        
        /* if retry is pending, process the retry RSSI (see ImGetRetryRssi()) */
        if (b->retryCountdown) {
            
            /* Process the retry RSSI as follows:
             *
             * - Decrement the retry countdown (so we won't keep retrying).
             * 
             * - If the RSSI is > the previous max, save new max. This helps
             *   limit the number of times we search the band (to save power).
             * 
             * - If RSSI >= (previous max - 1), return flag so we'll retry
             *   (i.e. search the channels in the band again). Otherwise,
             *   don't bother retrying. This helps limit the number of times we
             *   search the band (to save power). If it's a real wakeup signal,
             *   the retry RSSI will very likely be >= (previous max - 1). For
             *   more information, see ImGetRetryRssi().
             */
            --b->retryCountdown;
            if (rssi > b->maxRssi) b->maxRssi = rssi;
            if ((rssi + 1) >= b->maxRssi) return(ImBandFlag[band]);
            
        } else if (rssi > b->maxRssi) { /* else, if RSSI is new max */ 
            
            /* Since the RSSI is the maximum detected for the band up to now,
             * there might be a wakeup signal for this implant, since that will
             * usually be stronger. Thus, do the following:
             * 
             * - Save the new max RSSI so a higher RSSI will be required to
             *   trigger future searches (to avoid wasting power).
             * 
             * - Reset "consecMissCountdown" and "consecMissRestart" so if the
             *   signal decreases, we'll detect it sooner and call
             *   ImCheckMaxRssi() to re-check the maximum RSSI.
             * 
             * - Set "retryCountdown" so if no wakeup signal is detected for
             *   this implant when the band is searched (see ImSearchBands()),
             *   we'll retry the next time the implant searches for a wakeup
             *   signal (provided the RSSI remains high enough). See
             *   ImGetRetryRssi() for details.
             * 
             * - Return flag so we'll search the channels in the band.
             */
            b->maxRssi = rssi;
            b->consecMissCountdown = IM_INIT_CONSEC_MISS_RESTART;
            b->consecMissRestart = IM_INIT_CONSEC_MISS_RESTART;
            b->retryCountdown = IM_NUM_SEARCH_RETRIES;
            
            return(ImBandFlag[band]);
        }
    }
    
    /* return 0 (this band doesn't need to be searched) */
    return(0);
}

/* When using 400 MHz wakeup, ImSearchForRssi() calls this function when
 * needed to re-check the current maximum RSSI saved for the specified band.
 */
static void
ImCheckMaxRssi(UINT band)
{
    IM_WAKE_400_BAND *b = &ImMicsPriv.band[band];
    UINT rssi, newMaxRssi;
    UD16 startCount;
    UD32 remCount;
    
    /* restart consecutive miss countdown */
    b->consecMissCountdown = b->consecMissRestart;
    
    /* take RSSI samples for 10 ms to check if the signal has decreased */
    for(startCount = StCount(), remCount = StUsToCount(10000), newMaxRssi=0;;) {
            
        /* take RSSI sample */
        MicsWrite(ADCCTRL, ADC_INPUT_400_MHZ_RX_RSSI | ADC_ENAB | ADC_START);
        while(((rssi = MicsRead(ADCOUTPUT)) & ADC_COMPLETE) == 0);
        MicsWrite(ADCCTRL, ADC_INPUT_400_MHZ_RX_RSSI | ADC_ENAB);
        rssi &= ADC_RESULT_MASK;
        
        /* if RSSI > max loop has seen up to now */
        if (rssi > newMaxRssi) {
            
            /* save new max */
            newMaxRssi = rssi;
            
            /* If the RSSI is > (maxRssi - IM_RSSI_MISS_DELTA), stop (don't
             * reset maxRssi). The delta is included so we're more likely to
             * keep the previous maximum RSSI. That way, random ambient noise
             * and low level interference (any false signal) won't cause the
             * implant to keep resetting the maximum RSSI, then exceeding it,
             * searching the band, resetting it again, etc.
             */
            if ((rssi + IM_RSSI_MISS_DELTA) > b->maxRssi) break;
        }
        
        /* If we've sampled long enough, the max RSSI must have decreased, so
         * update the band's wakeup variables.
         */
        if (StElapsedCount(&startCount, &remCount)) {
            
            b->maxRssi = newMaxRssi;
            b->consecMissCountdown = IM_INIT_CONSEC_MISS_RESTART;
            b->consecMissRestart = IM_INIT_CONSEC_MISS_RESTART;
            b->retryCountdown = 0;
            break;
        }
    }
}

/* When using 400 MHz wakeup, ImSearchForRssi() calls this function to get
 * the RSSI for a retry (i.e. when searching a band again because the first
 * attempt didn't detect a wakeup signal for this implant). This takes RSSI
 * samples for 1 ms to get something closer to the real maximum RSSI for the
 * band. That way, variations in the band's RSSI due to noise are less likely
 * to trigger additional searches and retries, saving power.
 * 
 * Note if there's a real wakeup signal, sampling for 1 ms also ensures we'll
 * detect the signal on the retry, because the gap between wakeup packets is
 * only 55 us, and a retry should never occur while the base station is in a
 * listening window (if the first search of the band occurs during a base
 * station listening window and misses the wakeup signal, then by the time the
 * retry occurs, the base's listening window will have shifted away from the
 * retry). For more information on the timing between the base and implant
 * during 400 MHz wakeup, see BSM_MS_PER_WAKE_400_LISTEN in "ZL7010XAdk/Sw/
 * BsmMezz/Apps/Standard/BsmAppMics.c".
 */
static UINT
ImGetRetryRssi(void)
{
    UD32 remCount;
    UD16 startCount;
    UINT rssi, maxRssi;
    
    /* take RSSI samples for 1 ms and return the max detected */
    for(startCount = StCount(), remCount = StUsToCount(1000), maxRssi = 0;;) {
            
        MicsWrite(ADCCTRL, ADC_INPUT_400_MHZ_RX_RSSI | ADC_ENAB | ADC_START);
        while(((rssi = MicsRead(ADCOUTPUT)) & ADC_COMPLETE) == 0);
        MicsWrite(ADCCTRL, ADC_INPUT_400_MHZ_RX_RSSI | ADC_ENAB);
        rssi &= ADC_RESULT_MASK;
        
        if (rssi > maxRssi) maxRssi = rssi; 
        
        if (StElapsedCount(&startCount, &remCount)) break;
    }
    
    return(maxRssi);
}

/* When using 400 MHz wakeup, ImSearchForWake400() calls this function to
 * search the specified bands for a 400 MHz wakeup signal (i.e. listen for
 * the implant's ID on each channel in the specified bands).
 */
static void
ImSearchBands(UINT searchBands)
{
    IM_MICS_PRIV *im = &ImMicsPriv;
    MICS_LINK_CONFIG *nlc = &ImPub.micsConfig.normalLinkConfig;
    UINT i, j, band, chan, prevChan;
    IM_BAND_ORDER bandOrder;
    
    /* get order in which to search bands (from highest to lowest max RSSI )*/
    ImGetBandOrder(searchBands, &bandOrder);
    
    /* Set the following registers before searching the channels:
     *
     * TXBUFF_MAXPACKSIZE, TXBUFF_BSIZE, and RXBUFF_BSIZE: If using a ZL70103,
     * set these for a normal session. This ensures they'll always be ready for
     * a normal session when the ZL70103 is awakened via 400 MHz wakeup (in
     * case their emergency settings were copied to the ZL70103's wakeup stack
     * during an earlier emergency operation, and were therefore restored when
     * the ZL70103 woke up). Note for a ZL70101 or ZL70102, these registers are
     * set by ImListen101And102() later on.
     * 
     * ImSetRxBlockSize(): If using a ZL70101 or ZL70102, set RX block size
     * (RXBUFF_BSIZE) to 3. While ImListen101And102() is listening for the
     * implant's ID, each ID received in the RX buffer is 3 bytes.
     * 
     * TXRFPWRDEFAULTSET: If using a ZL70101, set the minimum TX power so
     * when the ZL70101 tries to transmit a wakeup response when we listen
     * for 400 MHz wakeup (see ImListen101And102()) and confirm the wakeup
     * signal (see ImConfirmWake400()), it won't generate an RF signal that's
     * detectable outside of the body (at the minimum TX power, the ZL70101
     * outputs approximately -60 dBm, and the body attenuates approximately
     * 40 dBm more). Note this should be verified in the end product.
     * 
     * MAC_MODUSER: If needed, set MAC_MODUSER for the RX modulation while
     * listening for 400 MHz wakeup (see ImListen()), and for the TX modulation
     * in case ImListen() tells the ZL7010X to respond to the 400 MHz wakeup.
     * Note if the TX modulation is 4FSK, and ImListen() tells the ZL7010X to
     * respond to the 400 MHz wakeup later on, ImListen() will rerun the
     * CAL_TX_IF_OSC calibration at that time to optimize it for 4FSK.
     */
    if (MICS_REV >= ZL70103) {
        MicsWrite(TXBUFF_MAXPACKSIZE, nlc->maxBlocksPerTxPack);
        ImSetTxBlockSize(nlc->txBlockSize);
        ImSetRxBlockSize(nlc->rxBlockSize);
    }
    if (MICS_REV <= ZL70102) ImSetRxBlockSize(3);
    if (MICS_REV == ZL70101) MicsWrite(TXRFPWRDEFAULTSET, 0);
    if (nlc->modUser != MAC_MODUSER_RESET) MicsWrite(MAC_MODUSER, nlc->modUser);
    
    /* search the specified bands (in order from highest to lowest RSSI) */
    for(i = 0, prevChan = 0; i < IM_NUM_BANDS; ++i) {
        
        /* get next band to search from the search order */
        if ((band = bandOrder.band[i]) == 0xFF) break;
        
        /* search the band's channels in the order specified in ImChanOrder */
        for(j = 0; j < 3; ++j) {
            
            /* get next channel to search from the band's channel order */
            if ((chan = ImChanOrder[band][j]) == 0xFF) break;
            
            /* If using a ZL70101, and switching from an even to an odd
             * channel, or vice-versa, change the sync pattern accordingly
             * (see ImSetSyncPattern() for explanation).
             */
            if (MICS_REV == ZL70101) {
                if ((chan ^ prevChan) & 1) ImSetSyncPattern(chan);
                prevChan = chan;
            }
            
            /* Call ImListen() to listen for a 400 MHz wakeup signal for
             * this implant on the specified channel, and if it returns TRUE
             * (detected wakeup signal for this implant), reset the wakeup
             * variables and return (stop searching). The wakeup variables
             * determine what will happen when the ZL7010X is later put
             * back to sleep and ImPeriodicMicsTasks() starts calling
             * ImSearchForWake400() again to search for another wakeup signal.
             */
            if (ImListen(chan)) {
                
                /* Clear the "retryCountdown" for the other bands (so the next
                 * ImSearchForWake400() won't do any retries that were pending
                 * for the other bands), then reset the band's wakeup variables
                 * Note ImResetBand() must be done last because it sets the
                 * band's "retryCountdown".
                 */
                for(i = 0; i < IM_NUM_BANDS; ++i) {
                    im->band[i].retryCountdown = 0;
                }
                ImResetBand(band);
                
                /* Set "clocksToWake400Search" = 2 so ImPeriodicMicsTasks()
                 * will call ImSearchForWake400() on the second clock after the
                 * MICS chip is put to sleep. This ensures ImSearchForWake400()
                 * won't be called for at least IM_MS_PER_CLOCK ms after the
                 * chip is put to sleep. That way, if the base station aborts
                 * a session, it will finish transmitting the HK abort link
                 * signal before ImSearchForWake400() is called again, so the
                 * signal won't affect the next RSSI sample on the implant.
                 */
                im->clocksToWake400Search = 2;
                
                /* return (stop searching) */
                return;
            }
        }
    }
    
    /* put MICS chip back to sleep (no wakeup detected for this implant) */
    ImMicsSleep();
}

/* When using 400 MHz wakeup, ImSearchBands() calls this function to listen
 * for a 400 MHz wakeup signal for this implant on the specified channel. If
 * this function detects a 400 MHz wakeup signal for this implant, it tells
 * the ZL7010X to send responses and returns TRUE. Otherwise, it stops
 * listening and returns FALSE.
 */
static BOOL
ImListen(UINT chan)
{
    #if MICS_REV >= ZL70103
        return(ImListen103(chan));
    #else
        return(ImListen101And102(chan));
    #endif
}

/* If using a ZL70103 or later, ImListen() calls this function to listen for a
 * 400 MHz wakeup signal for this implant on the specified channel. For more
 * information, see ImListen().
 */
#if MICS_REV >= ZL70103
__inline BOOL
ImListen103(UINT chan)
{
    MICS_LINK_CONFIG *nlc = &ImPub.micsConfig.normalLinkConfig;
    UD16 startCount;
    UD32 remCount;

    /* set channel to use to listen for 400 MHz wakeup */
    MicsWrite(MAC_CHANNEL, chan);

    /* Clear IRQ_LINKREADY in the IRQ status latch (so we can wait for
     * ImMicsIsr() to set it again), update state, then tell the ZL70103 to
     * listen for wakeup packets using the "send wakeup responses" mode. In
     * this mode, the ZL70103 will set reg_mac_head_det[0] if it receives a
     * valid 400 MHz wakeup header (a header containing the correct implant ID,
     * company ID, and channel). If the header also contains PID = 0 (to invite
     * the implant to start a session), the ZL70103 will also assert
     * IRQ_LINKREADY.
     *
     * Note when the ZL70103 first enters this mode, it will try to transmit
     * one wakeup response, and if it remains in this mode longer than the
     * resend time, it will try to transmit another wakeup response each time
     * the resend timer triggers. To ensure this won't generate an RF signal,
     * INITCOM.DISAB_400_MHZ_TX_102 is set here so the ZL70103 won't generate
     * any RF signal at all.
     */
    MicsPub.irqStat2Latch &= ~IRQ_LINKREADY;
    IM_SET_MICS_STATE(IM_SENDING_WAKEUP_RESPONSES);
    MicsWrite(INITCOM, DISAB_400_MHZ_TX_102);
    /*
     * Listen for ID's for ~6.5 ms (for an explanation, see
     * IM_WAKE_400_LISTEN_TIME_US).
     */
    startCount = StCount();
    remCount = StUsToCount(IM_WAKE_400_LISTEN_TIME_US);
    while(1) {

        /* If the ZL70103 asserted IRQ_LINKREADY, or set the HEADER_RECEIVED
         * flag, stop (detected 400 MHz wakeup signal for this implant).
         */
        if ((MicsPub.irqStat2Latch & IRQ_LINKREADY) ||
            (MicsRead(HEAD_DET_102) & HEADER_RECEIVED)) {
            break;
        }

        /* If it's been too long, tell the ZL70103 to abort, then return
         * false (no 400 MHz wakeup signal detected for this implant). Note
         * the MICS_SET_MIN_RESEND option is passed to MicsAbort() to help
         * ensure the ZL70103 will execute the abort as soon as possible,
         * and the normal resend time is restored after MicsAbort().
         */
        if (StElapsedCount(&startCount, &remCount)) {

            MicsAbort(MICS_SET_MIN_RESEND);
            MicsWrite(RESENDTIME, IM_RESEND_TIME);
            IM_SET_MICS_STATE(IM_IDLE);
            return(FALSE);
        }
    }

    /* If using 4FSK to transmit responses, recalibrate the TX IF oscillator.
     * Note ImSearchBands() wrote nlc->modUser to MAC_MODUSER earlier, so we
     * can just reference nlc->modUser (instead of reading MAC_MODUSER).
     */
    if ((nlc->modUser & TX_MOD_MASK) == TX_MOD_4FSK) MicsCal(CAL_TX_IF_OSC);

    /* Clear INITCOM.DISAB_400_MHZ_TX_102 so the ZL70103 will start
     * transmitting responses.
     */
    MicsWrite(INITCOM, 0);

    /* set flag to report change in link status to PC (see IM_STAT) */
    ImPub.statFlags |= IM_LINK_STAT_CHANGED;

    /* return true (detected wakeup signal for this implant) */
    return(TRUE);
}
#endif

/* If using a ZL70101 or ZL70102, ImListen() calls this function to listen for
 * a 400 MHz wakeup signal for this implant on the specified channel. For more
 * information, see ImListen101And102().
 */
#if MICS_REV <= ZL70102
__inline BOOL
ImListen101And102(UINT chan)
{
    IM_MICS_PRIV *im = &ImMicsPriv;
    IM_MICS_CONFIG *mc = &ImPub.micsConfig;
    MICS_LINK_CONFIG *nlc = &ImPub.micsConfig.normalLinkConfig;
    MICS_IMD_TID imdTid;
    UD16 startCount;
    UD32 remCount;
    
    /* set channel to use to listen for 400 MHz wakeup */
    MicsWrite(MAC_CHANNEL, chan);
    
    /* Disable the resend timer so the ZL7010X won't try to transmit 400 MHz
     * packets (other than the first) while this function is listening for
     * 400 MHz wakeup packets from the base station (later on). This is needed
     * because if the ZL7010X tries to transmit packets while this function is
     * listening, it might miss some wakeup packets. Also, the implant shouldn't
     * transmit anything until it has confirmed any wakeup packets that are
     * received (see ImConfirmWake400()). Note the ZL7010X will still try to
     * transmit one packet when this function first starts listening. To ensure
     * this won't generate an RF signal, different steps are taken for the
     * ZL70101 and ZL70102. For more information, see the comment where
     * INITCOM is set later in this function.
     *
     * Note the resend timer normally can't be disabled on an implant, but this
     * function sets INITCOM.IBS_FLAG = 1 (base station mode) while it's
     * listening for wakeup packets, so it can disable the resend timer.
     */
    MicsWrite(RESENDTIME, RESENDTIME_DISAB);
    
    /* Clear IRQ_RXNOTEMPTY in irqStat1Latch, then make sure it's enabled (so
     * the loop later in this function can check it).
     */
    MicsPub.irqStat1Latch &= ~IRQ_RXNOTEMPTY;
    MicsWrite(IRQ_ENABLESET1, IRQ_RXNOTEMPTY);
    
    /* Tell the ZL7010X to receive 400 MHz packets & place their implant ID's
     * in its RX buffer. Note ENAB_RESPONSES_WITH_WRONG_ADDR is specified so
     * the chip will accept regular packets with PID = 0 (including "start
     * session" packets from the base station). That way, the base station can
     * do a regular "start session" for 400 MHz wakeup (instead of having to
     * send wakeup responses like an implant).
     * 
     * Note when the ZL7010X first enters this mode, it will try to
     * transmit one 400 MHz packet. To ensure this won't generate an RF
     * signal, different steps are taken for the ZL70101 and ZL70102. For
     * a ZL70101, ImSearchBands() sets the minimum TX power before this
     * function is called, so the RF signal shouldn't be detectable outside
     * of the body (for details, see TXRFPWRDEFAULTSET in ImSearchBands()).
     * For a ZL70102, INITCOM.DISAB_400_MHZ_TX_102 is set here so the ZL70102
     * won't generate any RF signal at all.
     */
    #if MICS_REV == ZL70101
        MicsWrite(INITCOM, IBS_FLAG | RX_WAKEUP_RESPONSES |
            ENAB_RESPONSES_WITH_WRONG_ADDR);
    #else /* ZL70102 */
        MicsWrite(INITCOM, IBS_FLAG | RX_WAKEUP_RESPONSES |
            ENAB_RESPONSES_WITH_WRONG_ADDR | DISAB_400_MHZ_TX_102);
    #endif
    /*
     * Listen for ID's for ~6.5 ms (for an explanation, see
     * IM_WAKE_400_LISTEN_TIME_US).
     * 
     * Note if an unknown ID is received (i.e. not the implant's ID or the
     * wildcard 0), the loop still continues to listen up to the timeout. An
     * unknown ID doesn't necessarily mean the channel is being used by another
     * implant, because when the base station is searching for implants, this
     * implant may receive wakeup responses from other implants.
     */
    startCount = StCount();
    remCount = StUsToCount(IM_WAKE_400_LISTEN_TIME_US);
    while(1) {
        
        /* If ZL7010X asserted IRQ_RXNOTEMPTY, read next ID from RX buffer.
         * Note we could just call ImReceiveMics() to check for an ID, but that
         * would poll the chip via SPI, which would consume more power.
         */
        if (MicsPub.irqStat1Latch & IRQ_RXNOTEMPTY) {
            
            /* If an ID is read from the ZL7010X RX buffer, process it. Note if
             * the buffer is empty, ImReceiveMics() will clear IRQ_RXNOTEMPTY
             * in irqStat1Latch, re-enable it, and return 0.
             */
            if (ImReceiveMics(&imdTid, sizeof(imdTid), 0)) {
                
                /* if it's implant's own ID, stop listening loop & process it */
                if ((imdTid.b1 == mc->imdTid.b1) &&
                    (imdTid.b2 == mc->imdTid.b2) &&
                    (imdTid.b3 == mc->imdTid.b3)) {
                                    
                    break;
                }
                /* if it's ID 0 (wildcard), stop listening loop & process it */
                if ((imdTid.b1 == 0) &&
                    (imdTid.b2 == 0) &&
                    (imdTid.b3 == 0)) {
                                    
                    im->flags |= IM_RX_WILD_TID;
                    break;
                }
            }
        }
        /* if it's been too long, go abort and return FALSE */
        if (StElapsedCount(&startCount, &remCount)) goto abort;
    }
            
    /* Abort listen, then clear IM_MICS_RX_PEND (set by ImMicsRxNotEmptyIsr()).
     * Specify MICS_FLUSH_RX to flush any ID's remaining in the ZL7010X RX
     * buffer, and MICS_SET_MIN_RESEND because the resend timer is disabled
     * while listening and the ZL7010X won't execute abort with the resend
     * timer disabled.
     */
    MicsAbort(MICS_FLUSH_RX | MICS_SET_MIN_RESEND);
    ImPub.pend &= ~IM_MICS_RX_PEND;
    
    /* Configure TX (for session if it's started):
     * 
     * - If using a ZL70101, and the TX modulation is 2FSK, set TRAINWORD_101
     *   to 0xAA (best for 2FSK modes). Otherwise, just keep the default (0xD8,
     *   best for 4FSK). This isn't needed for a ZL70102/103 because it has
     *   separate training word registers for 2FSK and 4FSK.
     * 
     * - Set TXBUFF_MAXPACKSIZE and TXBUFF_BSIZE for a normal session. This
     *   ensures they'll always be ready for a normal session when the ZL7010X
     *   is awakened via 400 MHz wakeup (in case their emergency settings
     *   were copied to the ZL7010X's wakeup stack during an earlier emergency
     *   operation, and were therefore restored when the ZL7010X woke up). 
     */
    #if MICS_REV == ZL70101
        if ((nlc->modUser & TX_MOD_MASK) != TX_MOD_4FSK) {
            MicsWrite(TRAINWORD_101, TRAINWORD_2FSK);
        }
    #endif
    MicsWrite(TXBUFF_MAXPACKSIZE, nlc->maxBlocksPerTxPack);
    ImSetTxBlockSize(nlc->txBlockSize);
    
    /* if we received implant ID 0 (wildcard) */
    if (im->flags & IM_RX_WILD_TID) {
            
        /* Temporarily set the implant's ID to 0 (wildcard value), then call
         * ImConfirmWake400() to confirm the wakeup packets are for this
         * implant (see ImConfirmWake400() for explanation).
         */
        MicsWrite(MAC_IMDTRANSID1, 0);
        MicsWrite(MAC_IMDTRANSID2, 0);
        MicsWrite(MAC_IMDTRANSID3, 0);
        /**/
        if (ImConfirmWake400() == FALSE) goto abort;
        
        /* Abort wildcard confirmation, then restore implant's ID, resend time
         * (for wakeup responses), and RX block size (for session if started).
         */
        MicsAbort(MICS_SET_MIN_RESEND);
        MicsWrite(MAC_IMDTRANSID1, mc->imdTid.b1);
        MicsWrite(MAC_IMDTRANSID2, mc->imdTid.b2);
        MicsWrite(MAC_IMDTRANSID3, mc->imdTid.b3);
        MicsWrite(RESENDTIME, IM_RESEND_TIME);
        ImSetRxBlockSize(nlc->rxBlockSize);

        /* Clear IM_RX_WILD_TID so ImLinkReadyIsr() won't ignore IRQ_LINKREADY
         * if the base switches from search to "start session" while the
         * implant is still sending wake-up responses.
         */
        im->flags &= ~IM_RX_WILD_TID;

        /* if using a ZL70101, restore TX power for wakeup responses */
        #if MICS_REV == ZL70101
            MicsWrite(TXRFPWRDEFAULTSET, TXRFPWRDEFAULTSET_RESET_101);
        #endif    

        /* update state, then tell ZL7010X to send wakeup responses */
        IM_SET_MICS_STATE(IM_SENDING_WAKEUP_RESPONSES);
        MicsWrite(INITCOM, 0);
            
    } else { /* else, received implant's own ID (not the wildcard 0) */
        
        /* Call ImConfirmWake400() to confirm the wakeup packets are for
         * this implant (see ImConfirmWake400() for explanation). Note
         * ImConfirmWake400() waits for IRQ_LINKREADY, which will set
         * ImPub.micsState to IM_IN_SESSION (see ImLinkReadyIsr()). Thus,
         * there's no need to set ImPub.micsState again here.
         */
        if (ImConfirmWake400() == FALSE) goto abort;
        
        /* Restore the normal resend time and RX block size. Note since the
         * session already started (IRQ_LINKREADY during ImConfirmWake400()),
         * the ZL7010X won't actually use the resend time, but it's still
         * restored now in case registers are copied to the ZL7010X's wakeup
         * stack during the session (see MicsCopyRegs()).
         */
        MicsWrite(RESENDTIME, IM_RESEND_TIME);
        ImSetRxBlockSize(nlc->rxBlockSize);
        
        /* If using a ZL70101, restore the TX power. If using a ZL70102, clear
         * INITCOM.DISAB_400_MHZ_TX_102 (set by ImConfirmWake400()) so the
         * ZL70102 will start transmitting responses.
         */
        if (MICS_REV == ZL70101) {
            MicsWrite(TXRFPWRDEFAULTSET, TXRFPWRDEFAULTSET_RESET_101);
        } else { /* ZL70102 */
            MicsWrite(INITCOM, 0);
        }
    }
    
    /* If using 4FSK to transmit responses, recalibrate the TX IF oscillator.
     * Note ImSearchBands() wrote nlc->modUser to MAC_MODUSER earlier, so we
     * can just reference nlc->modUser (instead of reading MAC_MODUSER).
     */
    if ((nlc->modUser & TX_MOD_MASK) == TX_MOD_4FSK) MicsCal(CAL_TX_IF_OSC);
        
    /* set flag to report change in link status to PC (see IM_STAT) */
    ImPub.statFlags |= IM_LINK_STAT_CHANGED;
                
    /* return true (detected wakeup signal for this implant) */
    return(TRUE);
    
abort:
    /* Tell ZL7010X to abort, then make sure IM_MICS_RX_PEND is clear (in case
     * ImMicsRxNotEmptyIsr() set it). Specify MICS_FLUSH_RX to flush any ID's
     * remaining in the ZL7010X RX buffer, and MICS_SET_MIN_RESEND because the
     * resend timer might be disabled at this point and the ZL7010X won't
     * execute abort with the resend timer disabled.
     */
    MicsAbort(MICS_FLUSH_RX | MICS_SET_MIN_RESEND);
    ImPub.pend &= ~IM_MICS_RX_PEND;
    /* 
     * If implant ID 0 (wildcard) was received, restore the implant's own ID
     * (in case it was changed to confirm the wildcard ID). Note this is done
     * after the abort so the ZL7010X is inactive when it occurs.
     */
    if (im->flags & IM_RX_WILD_TID) {
        MicsWrite(MAC_IMDTRANSID1, mc->imdTid.b1);
        MicsWrite(MAC_IMDTRANSID2, mc->imdTid.b2);
        MicsWrite(MAC_IMDTRANSID3, mc->imdTid.b3);
        im->flags &= ~IM_RX_WILD_TID;
    }
    /* return false (no wakeup signal for this implant) */
    return(FALSE);
}
#endif

/* If using a ZL70101 or ZL70102, ImListen101And102() calls this function to
 * confirm the received wakeup packets are actually for this implant. This
 * function returns TRUE if the wakeup packets are confirmed, and FALSE
 * otherwise. This is done for the following reasons:
 * 
 * ZL70102 implant: If the base also has a ZL70102 (or a ZL70103), the wakeup
 * packets will contain the company ID and channel, but when the implant is
 * listening for ID's, its ZL70102 doesn't check the company ID or channel.
 * Thus, the implant might receive an implant ID (or the wildcard 0) that is
 * actually for a different company, and it might receive the wakeup packets
 * on an adjacent channel. To resolve these issues, ImListen101And102() calls
 * this function to confirm the company ID and channel before sending responses.
 * To do so, this function configures the ZL70102 to send wakeup responses, then
 * waits for IRQ_LINKREADY, which will only happen if the company ID and channel
 * match, as well as the implant ID. Note the ZL70102 won't actually send
 * wakeup responses because this function sets INITCOM.DISAB_400_MHZ_TX_102.
 * Also, the implant ID (or the wildcard 0) was already confirmed by
 * ImListen101And102(), but it's ok to re-confirm it here.
 * 
 * ZL70101 implant: Due to a bug in the ZL70101, if something is in session
 * and transferring data on a channel while the ZL70101 is listening for
 * implant ID's, the ZL70101 will place the session data in its RX buffer.
 * Thus, the implant might receive false implant ID's. To resolve this,
 * ImListen101And102() calls this function to confirm the received implant
 * ID before sending responses. To do so, this function configures the ZL70101
 * to send wakeup responses, then waits for IRQ_LINKREADY, which will only
 * happen if the implant ID matches. Note before this function is called,
 * ImSearchBands() sets the minimum TX power, so the wakeup responses
 * transmitted during this function should be too weak to be detected outside
 * of the body (for details, see TXRFPWRDEFAULTSET in ImSearchBands()).
 *
 * Note if either the base or the implant has a ZL70101, neither side will
 * check the company ID or channel in the 400 MHz packets. This applies
 * regardless of which side has the ZL70101, or if they both have a ZL70101.
 * As a result, this function will return TRUE even if the base and implant
 * actually have different company IDs, or if the implant is receiving the
 * wake-up packets on an adjacent channel. To resolve this, the base and
 * implant should both alternate the sync pattern for adjacent channels
 * (see ImSetSyncPattern()), and should negotiate after the session starts
 * to check the company ID.
 */
#if MICS_REV <= ZL70102
static BOOL
ImConfirmWake400(void)
{
    UD16 startMs;

    /* Set the resend time to its maximum (~54 ms) so the ZL7010X won't try
     * to transmit wakeup responses (other than the first) while this function
     * is listening for wakeup packets from the base station (later on). This
     * is needed because if the ZL7010X tries to transmit wakeup responses while
     * this function is listening, it might miss some wakeup packets. Also, the
     * implant shouldn't transmit anything until it has confirmed the wakeup
     * packets. Note the ZL7010X will still try to transmit one wakeup response
     * when this function first starts listening, and if this function ever
     * listens longer than the resend time for some reason, the ZL7010X will
     * also try to transmit another wakeup response each time the resend timer
     * triggers. To ensure this won't generate an RF signal, different steps
     * are taken for the ZL70101 and ZL70102. For more information, see the
     * comment where INITCOM is set later in this function.
     *
     * Note the resend timer can't be disabled here because when this function
     * listens for wakeup packets later on, the ZL7010X is in implant mode,
     * and in implant mode, the resend timer can't be disabled.
     */
    MicsWrite(RESENDTIME, RESENDTIME_MAX);
    
    /* Clear IRQ_LINKREADY in the IRQ status latch (so we can wait for
     * ImMicsIsr() to set it again), then tell the ZL7010X to listen for
     * wakeup packets using the "send wakeup responses" mode. In this mode,
     * if using a ZL70102, it will assert IRQ_LINKREADY if it receives a
     * packet containing the correct implant ID, company ID, and channel.
     * If using a ZL70101, it will assert IRQ_LINKREADY if it receives a
     * packet containing the correct implant ID. For more information, see
     * the prolog for this function.
     *
     * Note when the ZL7010X first enters this mode, it will try to transmit
     * one wakeup response, and if it remains in this mode longer than the
     * resend time, it will try to transmit another wakeup response each time
     * the resend timer triggers. To ensure this won't generate an RF signal,
     * different steps are taken for the ZL70101 and ZL70102. For a ZL70101,
     * ImSearchBands() sets the minimum TX power before this function is
     * called, so the RF signal shouldn't be detectable outside of the body
     * (for details, see TXRFPWRDEFAULTSET in ImSearchBands()). For a
     * ZL70102, INITCOM.DISAB_400_MHZ_TX_102 is set here so the ZL70102
     * won't generate any RF signal at all.
     */
    MicsPub.irqStat2Latch &= ~IRQ_LINKREADY;
    #if MICS_REV == ZL70101
        MicsWrite(INITCOM, 0);
    #else /* ZL70102 */
        MicsWrite(INITCOM, DISAB_400_MHZ_TX_102);
    #endif
    
    /* Wait up to 40 ms for the ZL7010X to assert IRQ_LINKREADY. This is
     * long enough to ensure at least one header is received from the base,
     * because during 400 MHz wakeup, the longest time the base goes without
     * transmitting any headers is ~35 ms (when searching for implants,
     * BsmProcWake400() listens ~25 ms for responses, plus ~10 ms for
     * BsmCheckWake400Rssi() to check if the channel is still clear).
     * 
     * Note if using a ZL70102, this will only wait the full timeout if
     * ImListen101And102() received this implant's ID (or the wildcard 0), but
     * the wakeup packets contain a different company ID. If using a ZL70101,
     * this will only wait the full timeout if ImListen101And102() received
     * implant ID 0 (wildcard), or a false ID that matched this implant's ID.
     * Since these cases should rarely happen, the length of the timeout
     * isn't a big issue.
     */
    for(startMs = StMs();;) {
                        
        /* if ZL7010X asserted IRQ_LINKREADY, return TRUE (ID confirmed) */
        if (MicsPub.irqStat2Latch & IRQ_LINKREADY) return(TRUE);
            
        /* if it's been too long, return FALSE (not confirmed) */
        if (StElapsedMs(startMs, 40)) return(FALSE);
    }
}
#endif

/* When using 400 MHz wakeup, ImSearchBands() calls this to get the bands to
 * search for 400 MHz wakeup in order from the band with the highest RSSI to
 * the band with the lowest RSSI. This is done for the following reasons:
 * 
 * - If there's a real wakeup signal, it will have the highest RSSI, so the
 *   implant might as well check the band with the highest RSSI first. That
 *   way, when there's a real wakeup signal, the implant won't waste power
 *   checking the other bands. Note this probably won't have much of an
 *   affect on the overall power required to support 400 MHz wakeup.
 *
 * - If the wakeup signal appears in different bands due to RF images, and the
 *   implant checks those bands first, it might respond on the wrong channel.
 *   To prevent this, the implant checks the band with the highest RSSI first,
 *   since the real wakeup band will have a higher RSSI than any images. Note
 *   when the implant is inside a body, images shouldn't be strong enough to
 *   work, so this probably won't be an issue. Also, if the base station and
 *   implant are both using a ZL70102/103, this isn't an issue, because the
 *   ZL70102/103 includes the channel in 400 MHz packets, and the implant
 *   confirms the channel before responding to the 400 MHz wakeup (see
 *   ImListen103() for the ZL70103, and ImConfirmWake400() for the ZL70102).
 */
static void
ImGetBandOrder(UINT searchBands, IM_BAND_ORDER *retOrder)
{ 
    IM_WAKE_400_BAND *b;
    UINT maxRssi, maxRssiBand;
    UINT i, band, bands;
    
    /* order bands from highest to lowest RSSI */
    for(i = 0, bands = searchBands; i < IM_NUM_BANDS; ++i) {
        
        /* search remaining bands for the one with the highest RSSI */
        for(band = 0, maxRssi = 0; band < IM_NUM_BANDS; ++band) {
            
            /* get pointer to data for band */
            b = &ImMicsPriv.band[band];
            
            /* if band is still in search and its RSSI > max, save new max */
            if ((bands & ImBandFlag[band]) && (b->maxRssi > maxRssi)) {
                maxRssi = b->maxRssi;
                maxRssiBand = band;
            }
        }
        /* if no band found, stop (no more bands to add to search order) */
        if (maxRssi == 0) break;
        
        /* set next band in search order */
        retOrder->band[i] = maxRssiBand;
        
        /* clear flag so we won't check this band again */
        bands &= ~ImBandFlag[maxRssiBand];
        
        /* if no more bands to check, stop */
        if (bands == 0) break;
    }
    
    /* terminate search list */
    retOrder->band[++i] = 0xFF;
}

/* If using 400 MHz wakeup and the implant has a ZL70101, ImSearchBands() calls
 * this function to set the sync pattern as required for the specified channel.
 * The odd channels use a different sync pattern than the even channels to
 * ensure the implant won't receive the wakeup packets on an adjacent channel.
 * This is needed because the ZL70101 doesn't include the channel in 400 MHz
 * packets, or check it in received 400 MHz packets, so if either the base or
 * the implant has a ZL70101, the implant might receive the 400 MHz wakeup
 * packets on an adjacent channel.
 *
 * Note if the implant has a ZL70102/103, it's assumed the base also has
 * a ZL70102/103, so the base will include the channel (and company ID) in
 * the 400 MHz packets, and the implant will confirm the channel before
 * responding to the 400 MHz wakeup (see ImListen103() for the ZL70103,
 * and ImConfirmWake400() for the ZL70102). Thus, the same sync pattern can
 * be used for all channels, and this function is never called.
 *
 * Note if you want to use a ZL70101 base with a ZL70102/103 implant, you'll
 * need to change the ZL70102/103 implant software so it also alternates the
 * sync pattern. Otherwise, 400 MHz wake-up won't work on the odd channels.
 */
__inline void
ImSetSyncPattern(UINT chan)
{
    /* If channel is odd, set sync pattern for odd channel. Otherwise, set
     * it for even channel (the power-on/reset default).
     */
    if (chan % 2) {
        MicsWrite(SYNC1, 0x6A);
        MicsWrite(SYNC2, 0xE3);
        MicsWrite(SYNC3, 0x48);
        MicsWrite(SYNC4, 0xC5);
        MicsWrite(SYNC5, 0xE6);
    } else {
        MicsWrite(SYNC1, SYNC1_RESET);
        MicsWrite(SYNC2, SYNC2_RESET);
        MicsWrite(SYNC3, SYNC3_RESET);
        MicsWrite(SYNC4, SYNC4_RESET);
        MicsWrite(SYNC5, SYNC5_RESET);
    }
}

/* When using 400 MHz wakeup, this is called to reset the wakeup variables.
 * This determines what will happen when the MICS chip is put to sleep and
 * ImPeriodicMicsTasks() starts calling ImSearchForWake400() to search for
 * a wakeup signal. In effect, this resets the 400 MHz wakeup algorithm.
 */
void
ImResetWake400(void)
{
    IM_MICS_PRIV *im = &ImMicsPriv;
    UINT band;
    
    /* reset wakeup variables for each band (see ImResetBand() for details) */
    for(band = 0; band < IM_NUM_BANDS; ++band) ImResetBand(band);
    
    /* Set "clocksToWake400Search" = 1 so ImPeriodicMicsTasks() will call
     * ImSearchForWake400() on the first clock after the MICS chip is put to
     * sleep (note the clock could occur anywhere from ~0 to IM_MS_PER_CLOCK ms
     * after the chip is put to sleep).
     */
    im->clocksToWake400Search = 1;
}

/* When using 400 MHz wakeup, this is called to reset the wakeup variables for
 * the specified band. This determines what will happen when the MICS chip is
 * put to sleep and ImPeriodicMicsTasks() starts calling ImSearchForWake400()
 * to search for a wakeup signal.
 */
static void
ImResetBand(UINT band)
{
    IM_MICS_PRIV *im = &ImMicsPriv;
    
    /* Reset the band's wakeup variables as follows:
     * 
     * - Reset "consecMissCountdown" and "consecMissRestart".
     * 
     * - Set "retryCountdown" = 2 so when the MICS chip is put to sleep and
     *   ImPeriodicMicsTasks() starts calling ImSearchForWake400() to search
     *   for a wakeup signal, it will do two retries. That way, if the base
     *   station is transmitting a wakeup signal, the implant will detect it.
     *   Note the first retry might occur during the base's listening window
     *   and miss the wakeup signal, but in that case, the second retry should
     *   never miss the wakeup signal (see ImGetRetryRssi() for details). Also
     *   note the second retry will only occur if the RSSI remains high enough
     *   to warrant it (see ImProcRssi() and ImGetRetryRssi()).
     * 
     * - Set "maxRssi" = 0 so it will be set to the new RSSI on the first
     *   retry, establishing a new "maxRssi" level. That way, if the wakeup
     *   signal ends before the retries, then starts again afterwards, it will
     *   trigger another search, ensuring it will be detected again.
     */
    im->band[band].consecMissCountdown = IM_INIT_CONSEC_MISS_RESTART;
    im->band[band].consecMissRestart = IM_INIT_CONSEC_MISS_RESTART;
    im->band[band].retryCountdown = 2;
    im->band[band].maxRssi = 0;        
}

/* When the ZL7010X is active (not idle or sleeping), ImPeriodicMicsTasks()
 * periodically calls this function to update the link quality statistics (so
 * the counters in the ZL7010X won't overflow). This is also called when the
 * PC queries the implant status so the PC will get the latest link quality
 * statistics (see ImCmdGetStatChanges()).
 */
void
ImUpdateLinkQual(void)
{
    UINT crcErrs, errCorBlocks, irqAuxStat;
    
    /* if using a ZL70102/103 */
    if (MICS_REV >= ZL70102) {
        
        /* stop error counters, read them, then clear and restart them */
        MicsWrite(ERRCLR_102, STOP_ECCORR | STOP_CRCERR);
        crcErrs = MicsRead(CRCERR);
        errCorBlocks = MicsRead(ECCORR);
        MicsWrite(ERRCLR_102, 0);
        
        /* if there were any link quality errors */
        if (crcErrs || errCorBlocks) {
                        
            /* update link quality & set flag to report change to PC */
            ImPub.linkQual.crcErrs += crcErrs;
            ImPub.linkQual.errCorBlocks += errCorBlocks;
            ImPub.statFlags |= IM_LINK_QUAL_CHANGED;
        }
        
    } else { /* MICS_REV == ZL70101 */
    
        /* if there were any link quality errors */
        crcErrs = MicsRead(CRCERR);
        errCorBlocks = MicsRead(ECCORR);
        if (crcErrs || errCorBlocks) {
                        
            /* clear error counters in ZL70101 */
            MicsWrite(ERRCLR_101, CLEAR_ECCORR | CLEAR_CRCERR);
            MicsWrite(ERRCLR_101, 0);
            
            /* update link quality & set flag to report change to PC */
            ImPub.linkQual.crcErrs += crcErrs;
            ImPub.linkQual.errCorBlocks += errCorBlocks;
            ImPub.statFlags |= IM_LINK_QUAL_CHANGED;
        }
    }
    
    /* if the ZL7010X asserted IRQ_LINKQUALITY */
    if (MicsPub.irqStat2Latch & IRQ_LINKQUALITY) {
        
        /* get IRQ_AUXSTATUS */
        irqAuxStat = MicsRead(IRQ_AUXSTATUS);
    
        /* If IRQ_MAXBERR: set ImPub.linkQual.maxBErrInts; set flag to report
         * change in link quality to PC; clear IRQ_MAXBERR in IRQ_AUXSTATUS.
         */
        if (irqAuxStat & IRQ_MAXBERR) {
            
            ImPub.linkQual.maxBErrInts = TRUE;
            ImPub.statFlags |= IM_LINK_QUAL_CHANGED;
            MicsWrite(IRQ_AUXSTATUS, ~IRQ_MAXBERR);
        }
        
        /* if IRQ_MAXRETRIES, just clear retry counter (meaningless on implant) */
        if (irqAuxStat & IRQ_MAXRETRIES) {
            
            MicsWrite(MAXRETRIES, CLEAR_RETRY_COUNTER | MAXRETRIES_RESET);
            MicsWrite(MAXRETRIES, MAXRETRIES_RESET);
            MicsWrite(IRQ_AUXSTATUS, ~IRQ_MAXRETRIES);
        }
        
        /* clear IRQ_LINKQUALITY in irqStat2Latch, then re-enable it */
        MicsPub.irqStat2Latch &= ~IRQ_LINKQUALITY;
        MicsWrite(IRQ_ENABLESET2, IRQ_LINKQUALITY);
    }
}

/* If using a ZL70102/103, this is called to set the device mode
 * (DEVICEMODE_102) instead of calling MicsWrite() directly. This sets
 * ImMicsPriv.deviceMode for ImMicsWakeup() to reference, so it can detect
 * when the ZL70102/103 is in fast startup mode.
 */
#if MICS_REV >= ZL70102
__inline void
ImSetDeviceMode(UINT deviceMode)
{
    #if MICS_REV >= ZL70102
        MicsWrite(DEVICEMODE_102, deviceMode);
        *((UD8 *)&ImMicsPriv.deviceMode) = (UD8)deviceMode;
    #endif
}
#endif

/* This is called to set the TX block size (TXBUFF_BSIZE) instead of calling
 * MicsWrite() directly. This sets ImPub.txBlockSize so ImTransmitMics() and
 * other functions don't have to read TXBUFF_BSIZE.
 */
__inline void
ImSetTxBlockSize(UINT txBlockSize)
{
    MicsWrite(TXBUFF_BSIZE, txBlockSize);
    *((UD8 *)&ImPub.txBlockSize) = (UD8)txBlockSize;
}

/* This is called to set the RX block size (RXBUFF_BSIZE) instead of calling
 * MicsWrite() directly. This sets ImPub.rxBlockSize so ImReceiveMics() and
 * other functions don't have to read RXBUFF_BSIZE.
 */
__inline void
ImSetRxBlockSize(UINT rxBlockSize)
{
    #if MICS_REV == ZL70101
        MicsWrite(RXBUFF_BSIZE, rxBlockSize);
    #else
        MicsWrite(RXBUFF_BSIZE, rxBlockSize | IMPROVE_ERR_COR_102);
    #endif
    *((UD8 *)&ImPub.rxBlockSize) = (UD8)rxBlockSize;
}

/* This is called to turn on the TX 400 LED when starting an HK TX. MicsReadR()
 * and MicsWriteR() call this via MICS_APP_HOOK_FOR_START_HK_TX().
 */
void ImAppHookForStartHkTx(void)
{
    ImTx400LedOn(IM_NO_AUTO_OFF);
}

/* This is the MICS (ZL7010X) interrupt service routine.
 */
void
ImMicsIsr(void)
{
    UINT irqStat1, irqStat2, irqAuxStat;
    
    /* If using a ZL70101 and it was sleeping, call ImInitInterfaceMode()
     * to initialize INTERFACE_MODE as needed. This initializes the ZL70101's
     * maximum SPI clock rate, which must be done before reading the ZL70101's
     * interrupt statuses to ensure the SPI works properly (see
     * ImInitInterfaceMode() for details).
     */
    #if MICS_REV == ZL70101
        if (ImPub.micsState == IM_SLEEPING) ImInitInterfaceMode();
    #endif
    
    /* Process MICS interrupts until the chip de-asserts the interrupt line.
     * That way, if an interrupt condition occurs after we read the statuses,
     * it will be detected and processed the next time through the loop. This
     * also ensures there will always be a rising edge if an interrupt occurs
     * after we exit the ISR, so the MSP430 will always detect it and set
     * MICS_INT_FLAG again (the interrupt is edge triggered).
     * 
     * Note the interrupt flag is cleared first outside of the loop in case
     * MICS_INT_ASSERTED is false (should never happen, but it's best to
     * ensure the flag is cleared so the interrupt won't keep recurring).
     */
    MICS_WR_INT_FLAG(FALSE);
    while(MICS_INT_ASSERTED) {
        
        /* Get status for interrupt groups 1 & 2 from MICS chip, and init
         * auxiliary status to 0 (if an interrupt flagged in group 1 or 2 has
         * an auxiliary status, it will be read from the MICS chip later).
         */
        irqStat1 = MicsRead(IRQ_STATUS1); 
        irqStat2 = MicsRead(IRQ_STATUS2);
        irqAuxStat = 0;
        
        /* if no interrupts are flagged, set error (shouldn't happen) */
        if ((irqStat1 | irqStat2) == 0) {
            ImErr(IM_APP_ERR_MICS_INT_WITH_NO_STAT, NULL);
        }

        /* if a group 1 interrupt is flagged */
        if (irqStat1) {
    
            /* RX buf not empty */
            if (irqStat1 & IRQ_RXNOTEMPTY) ImMicsRxNotEmptyIsr();
        
            /* remote side accessed HK_USERDATA */
            if (irqStat1 & IRQ_HKUSERDATA) ImHkUserDataIsr();
            
            /* remote side accessed HK_USERSTATUS */
            if (irqStat1 & IRQ_HKUSERSTATUS) ImHkUserStatusIsr();
            
            /* remote side responded to HK access */
            if (irqStat1 & IRQ_HKREMOTEDONE) ImHkRemoteDoneIsr();
        
            /* connection lost (IRQ_WDOG or IRQ_HKABORTLINK) */
            if (irqStat1 & IRQ_CLOST) irqAuxStat = ImConLostIsr(irqAuxStat);
            
            /* set bits in the IRQ latch for the MICS library functions */
            MicsPub.irqStat1Latch |= irqStat1;
            
            /* clear any group 1 interrupts that were set in the status */
            MicsWrite(IRQ_RAWSTATUS1, ~irqStat1);
        }
    
        /* if a group 2 interrupt is flagged */
        if (irqStat2) {
        
            /* radio fail (IRQ_CRCERR or IRQ_SYNTHLOCKFAIL) */
            if (irqStat2 & IRQ_RADIOFAIL) irqAuxStat = ImRadioFailIsr(irqAuxStat);
        
            /* radio ready (at end of wakeup, or after abort link) */
            if (irqStat2 & IRQ_RADIOREADY) ImRadioReadyIsr();
        
            /* link ready (session established) */
            if (irqStat2 & IRQ_LINKREADY) ImLinkReadyIsr();
            
            /* link quality (IRQ_MAXRETRIES or IRQ_MAXBERR) */
            if (irqStat2 & IRQ_LINKQUALITY) ImLinkQualIsr();
        
            /* remote side accessed local register via housekeeping */
            if (irqStat2 & IRQ_HKMESSREG) ImHkMessRegIsr();
        
            /* MAC_CTRL command done */
            if (irqStat2 & IRQ_COMMANDDONE) ImCommandDoneIsr();
        
            /* set bits in the IRQ latch for the MICS library functions */
            MicsPub.irqStat2Latch |= irqStat2;
            
            /* clear any group 2 interrupts that were set in the status */
            MicsWrite(IRQ_RAWSTATUS2, ~irqStat2);
        }
        
        /* set bits in the IRQ latch for the MICS library functions */
        MicsPub.irqAuxStatLatch |= irqAuxStat;
        
        /* ensure interrupt flag is clear so it won't recur when ISR exits */
        MICS_WR_INT_FLAG(FALSE);
    }
}

/* If using a ZL70101, then each time the ZL70101 asserts IRQ_RADIOREADY when
 * it wakes up, ImMicsIsr() calls this function to initialize INTERFACE_MODE
 * as needed. This is required because each time the ZL70101 is sleep cycled,
 * it resets INTERFACE_MODE, which resets the ZL70101's maximum SPI clock
 * rate to 2 MHz. If the actual SPI clock is > 2 MHz, this function adjusts
 * INTERFACE_MODE to increase the ZL70101's maximum SPI clock rate to 4 MHz,
 * which is required in order for the SPI to work. If the actual SPI clock
 * is <= 1 MHz, this function adjusts INTERFACE_MODE to decrease the ZL70101's
 * maximum SPI clock rate to 1 MHz in order to reduce the power consumed by the
 * ZL70101's SPI interface a little, even though the power saved is probably
 * insignificant relative to the overall power consumed by the ZL70101.
 *
 * Note that for a ZL70102/103, ImMicsIsr() does not need to call this
 * function to initialize INTERFACE_MODE. Instead, each time ImResetMics()
 * is called to reset the ZL70102/103, it initializes INTERFACE_MODE (see
 * ImResetMics() for details).
 */
#if MICS_REV == ZL70101
__inline void
ImInitInterfaceMode(void)
{
    UINT interfaceMode;
        
    /* if configured to enable housekeeping write access, do so */
    if (ImPub.micsConfig.flags & IM_ENAB_HK_WRITE) {
        interfaceMode = IM_INTERFACE_MODE | ENAB_HK_WRITE_101;
    } else {
        interfaceMode = IM_INTERFACE_MODE;
    }

    /* if the desired INTERFACE_MODE value != reset value, set it */
    if (interfaceMode != INTERFACE_MODE_RESET_101) {

        /* If the local SPI bus clock is normally > 2 MHz, reduce it to 2
         * MHz so we can talk to the ZL70101 in its default state (2 MHz
         * max), then restore it afterwards.
         */
        if (IM_LSPI_CLK_FREQ > 2000000UL) {

            ImConfigSpi(IM_LSPI_CLK_DIV_FOR_2_MHZ);
            MicsWrite(INTERFACE_MODE, interfaceMode);
            ImConfigSpi(IM_LSPI_CLK_DIV);

        } else {
            MicsWrite(INTERFACE_MODE, interfaceMode);
        }
    }
}
#endif

UINT
ImRadioFailIsr(UINT irqAuxStat)
{
    /* if auxiliary status hasn't been read yet, read it */     
    if (irqAuxStat == 0) irqAuxStat = MicsRead(IRQ_AUXSTATUS);
    
    /* If CRC error interrupt, handle it. Normally, an implant application
     * should restore the relevant register settings in the MICS chip from
     * non-volatile memory, then invoke MicsCopyRegs() to save the registers
     * in the ZL7010X's wakeup stack and recalculate the CRC. For now, just set
     * an error to report to the PC application and tell the MICS chip to skip
     * the CRC. Note the implant can be power-cycled to clear the CRC error and
     * return the MICS chip to default register settings (leave it off for a
     * few moments to make sure power is completely drained).
     */
    if (irqAuxStat & IRQ_CRCERR) {
        
        ImErr(IM_APP_ERR_MICS_CRC_ERR, NULL);
        MicsWrite(MAC_CTRL, SKIP_CRC);
        MicsWrite(IRQ_AUXSTATUS, ~IRQ_CRCERR);
    }
    
    /* if synth lock fail, clear bit in auxiliary status */
    if (irqAuxStat & IRQ_SYNTHLOCKFAIL) {
        
        MicsWrite(IRQ_AUXSTATUS, ~IRQ_SYNTHLOCKFAIL);
        ImErr(IM_APP_ERR_MICS_SYNTH_FAILED_TO_LOCK, NULL);
    }
    
    /* if using ZL70102/103 */
    #if MICS_REV >= ZL70102
    
        /* if IRQ_VREGFAIL_102, re-write VDDA & VDDD trims to clear it */
        if (irqAuxStat & IRQ_VREGFAIL_102) {
            
            MicsWrite(TESTCTRL1_102, ENAB_VDD_WRITE);
            MicsWrite(VDDATRIM_102, VDDATRIM_RESET_102);
            MicsWrite(VDDDTRIM_102, VDDDTRIM_RESET_102);
            MicsWrite(TESTCTRL1_102, 0);
        }
    #endif
    
    /* return auxiliary status read from MICS chip */
    return(irqAuxStat);
}

/* Note if the MICS chip asserted IRQ_RADIOREADY at the end of its wakeup 
 * sequence, the watchdog timer will be enabled (default wakeup setting).
 * In that case, the watchdog is left enabled so it will timeout in 4.37
 * seconds and assert IRQ_WDOG (see ImConLostIsr()), which will set the
 * IM_MICS_SLEEP_PEND flag to tell ImProc() to put the MICS chip to sleep
 * (see ImMicsSleep()).
 */
__inline void
ImRadioReadyIsr(void)
{
    MICS_LINK_CONFIG *nlc = &ImPub.micsConfig.normalLinkConfig;
    #if MICS_REV >= ZL70102
        IM_MICS_CONFIG *mc = &ImPub.micsConfig;
    #endif
    
    /* if ZL7010X was sleeping (IRQ_RADIOREADY is for wakeup, not an abort) */
    if (ImPub.micsState == IM_SLEEPING) {
        
        /* Initialize registers that need to be set every time the ZL7010X
         * wakes up (because its "copy registers" command doesn't save them
         * in its wakeup stack):
         *
         * IRQ_ENABLESET1/2 (reg_irq_enable1/2):
         *     Enable MICS interrupts needed by this application and the MICS
         *     library functions. Note the ZL7010X enables the following
         *     interrupts by default: IRQ_RADIOFAIL, IRQ_RADIOREADY,
         *     IRQ_LINKREADY, IRQ_CLOST, IRQ_HKUSERDATA, IRQ_HKUSERSTATUS,
         *     and IRQ_HKREMOTEDONE. These don't have to be re-enabled here,
         *     but they are for the sake of clarity. If any of the default
         *     interrupts aren't needed, they should be disabled here.
         * HK_MODE_102:
         *     If using a ZL70102/103, and the implant is configured to
         *     enable housekeeping write access, set HK_MODE_102.HK_WRITE_ENAB.
         *     This is needed because the ZL70102/103 doesn't save this in its
         *     wakeup stack.
         */
        MicsWrite(IRQ_ENABLESET1, IRQ_HKUSERDATA | IRQ_HKUSERSTATUS |
            IRQ_HKREMOTEDONE | IRQ_CLOST);
        MicsWrite(IRQ_ENABLESET2, IRQ_RADIOREADY | IRQ_RADIOFAIL |
            IRQ_LINKREADY | IRQ_HKMESSREG | IRQ_COMMANDDONE);
        #if MICS_REV >= ZL70102
            if (mc->flags & IM_ENAB_HK_WRITE) {
                MicsWrite(HK_MODE_102, HK_WRITE_ENAB);
            }
        #endif   
        
        /* If the NVM contains a valid value for RXIFADCDECLEV (determined by
         * a manual factory calibration procedure for the RX ADC trim), write
         * it to RXIFADCDECLEV. Otherwise, just use the existing value for
         * RXIFADCDECLEV (determined by the automated RX ADC trim that the
         * ZL7010X performs each time it wakes up). Note that the manual
         * factory calibration is only required for 4FSK, but can also be
         * used for 2FSK and 2FSK-FB.
         */
        if (MicsFactNvm->rxIfAdcDecLev <= 0x1F) {
            MicsWrite(RXIFADCDECLEV, MicsFactNvm->rxIfAdcDecLev);
        }

        /* Set source for programmable output 2 (PO2) to RX_MODE. This is
         * connected to a micro-controller input pin (MICS_RX_MODE) so the
         * software can determine when the ZL7010X is in receive mode without
         * having to access the ZL7010X over the SPI bus.
         */
        MicsWrite(PO2, PO2_RX_MODE);
        
        /* if IRQ_RADIOREADY is for direct wakeup (MicsWakeup()) */
        if (MICS_WU_EN) {
            
            /* indicate ZL7010X is awake & idle */
            IM_SET_MICS_STATE(IM_IDLE);
            
        } else { /* else, IRQ_RADIOREADY is for 2.45 GHz wakeup */
            
            /* Indicate ZL7010X is sending wakeup responses (after 2.45 GHz
             * wakeup, the ZL7010X automatically starts sending responses).
             */
            IM_SET_MICS_STATE(IM_SENDING_WAKEUP_RESPONSES);
            
            /* Set TXBUFF_MAXPACKSIZE, TXBUFF_BSIZE, and RXBUFF_BSIZE for a
             * normal session. This ensures they'll always be ready for a
             * normal session when the ZL7010X is awakened via 2.45 GHz (in
             * case their emergency settings were copied to the ZL7010X's
             * wakeup stack during an earlier emergency operation, and were
             * therefore restored when the ZL7010X woke up).
             */
            MicsWrite(TXBUFF_MAXPACKSIZE, nlc->maxBlocksPerTxPack);
            ImSetTxBlockSize(nlc->txBlockSize);
            ImSetRxBlockSize(nlc->rxBlockSize);
            
            /* If using a ZL70101, do the following:
             * 
             * - Copy WAKEUP_CHANNEL to MAC_CHANNEL so the link status will
             *   report the channel extracted from the 2.45 GHz wakeup (see
             *   ImCmdGetLinkStat()). Otherwise, the link status would report
             *   channel 0 (the default ZL70101 MAC_CHANNEL). Note the ZL70101
             *   manual says it will copy WAKEUP_CHANNEL to MAC_CHANNEL for
             *   2.45 GHz wakeup, but it doesn't. The ZL70102/103 does.
             * 
             * - If the TX modulation extracted from the 2.45 GHz signal is
             *   2FSK, set TRAINWORD_101 to 0xAA (best for 2FSK modes).
             *   Otherwise, keep the default (0xD8, best for 4FSK). This isn't
             *   needed for the ZL70102/103 because it has separate training
             *   word registers for 2FSK and 4FSK.
             */
            #if MICS_REV == ZL70101
                /**/
                MicsWrite(MAC_CHANNEL, MicsRead(WAKEUP_CHANNEL));
                /**/
                if ((MicsRead(MAC_MODUSER) & TX_MOD_MASK) != TX_MOD_4FSK) {
                    MicsWrite(TRAINWORD_101, TRAINWORD_2FSK);
                }
            #endif
        }
            
        /* Since the ZL7010X was sleeping, MAC_CHANNEL and MAC_MODUSER may
         * have changed (either the value extracted from the 2.45 GHz wakeup
         * signal, or the default value for direct wakeup). Thus, set flag to
         * report change in link status to PC (see IM_STAT).
         */
        ImPub.statFlags |= IM_LINK_STAT_CHANGED;
    }
}

__inline void
ImLinkReadyIsr(void)
{
    IM_MICS_PRIV *im = &ImMicsPriv;
    
    /* If IM_RX_WILD_TID is set, we must be using 400 MHz wakeup, and
     * ImListen101And102() must have called ImConfirmWake400() to confirm
     * a wakeup signal containing implant ID 0 (wildcard). In that case,
     * we don't want IRQ_LINKREADY to do anything except update
     * MicsPub.irqStat2Latch for ImConfirmWake400() to reference. Since
     * MicsPub.irqStat2Latch is updated in ImMicsIsr(), there's nothing
     * more to do here, so just return.
     */
    if (im->flags & IM_RX_WILD_TID) return;
        
    /* turn session LED on */
    ImSessionLedOn();
    
    /* Ensure MICS watchdog timer is enabled (in case it was disabled to send
     * emergency transmissions). This is needed so if the link is lost, the
     * MICS watchdog will time out and assert IRQ_WDOG (see ImConLostIsr()).
     * Otherwise, the implant may never abort the session and go to sleep.
     */
    MicsWrite(WDOG_CONTROL, WDOG_ENAB);
    
    /* enable IRQ_RXNOTEMPTY (for data received during session) */
    MicsWrite(IRQ_ENABLESET1, IRQ_RXNOTEMPTY);
    
    /* enable IRQ_LINKQUALITY (for link quality statistics during session) */
    MicsWrite(IRQ_ENABLESET2, IRQ_LINKQUALITY);
    
    /* update state to indicate session started */
    IM_SET_MICS_STATE(IM_IN_SESSION);
    
    /* set flag to tell ImProc() to call ImProcMicsSession() */
    ImPub.pend |= IM_MICS_SESSION_PEND;
}

__inline void
ImLinkQualIsr(void)
{
    /* Disable IRQ_LINKQUALITY until the next time ImUpdateLinkQual() is
     * called. ImUpdateLinkQual() is called periodically to update the link
     * quality (see IM_MS_PER_LINK_QUAL_UPDATE). When it's called, it will
     * detect IRQ_LINKQUALITY in MicsPub.irqStat2Latch, update the link
     * quality as needed, then re-enable IRQ_LINKQUALITY. IRQ_LINKQUALITY
     * is handled this way for the following reason: if the ZL7010X asserts
     * IRQ_LINKQUALITY for IRQ_MAXBERR, then never receives another packet
     * afterwards (as can happen if the RF link is poor), it will continue
     * to assert IRQ_LINKQUALITY, even if IRQ_MAXBERR is cleared in
     * IRQ_AUXSTATUS. To ensure this won't monopolize the micro-controller
     * and cause a lock-up condition, IRQ_LINKQUALITY is disabled when it
     * occurs, and the next ImUpdateLinkQual() handles it.
     */
    MicsWrite(IRQ_ENABLECLEAR2, IRQ_LINKQUALITY);
}

__inline void
ImMicsRxNotEmptyIsr(void)
{
    /* disable MICS RX interrupt until data in RX buf is read */
    MicsWrite(IRQ_ENABLECLEAR1, IRQ_RXNOTEMPTY);
    
    /* Turn on the 400 MHz RX LED & tell ImPeriodicLedTasks() turn it off
     * automatically when the RX buffer on the MICS chip becomes empty.
     * ImProc() periodically calls ImPeriodicLedTasks() to update the LED's.
     */
    ImRx400LedOn(IM_AUTO_OFF);
    
    /* set flag to tell ImProc() to call ImProcMicsRx() */
    ImPub.pend |= IM_MICS_RX_PEND;
}

__inline void
ImHkMessRegIsr(void)
{
    /* Turn on the 400 MHz RX & TX LED's to indicate a remote HK access was
     * received and replied to, & let ImPeriodicLedTasks() turn them off
     * automatically. ImProc() periodically calls ImPeriodicLedTasks() to
     * update the LED's.
     */
    ImRx400LedOn(IM_AUTO_OFF);
    ImTx400LedOn(IM_AUTO_OFF);
    
    /* If the base changed a register, it might affect the link status, so set
     * flag to report change in link status to PC (see IM_STAT).
     */
    if ((MicsRead(HK_LASTADDRESS) & MICS_READ) == 0) {
        ImPub.statFlags |= IM_LINK_STAT_CHANGED;
    }
}

__inline void
ImHkUserDataIsr(void)
{
    /* Turn on the 400 MHz RX & TX LED's to indicate a remote HK access was
     * received and replied to, & let ImPeriodicLedTasks() turn them off
     * automatically. ImProc() periodically calls ImPeriodicLedTasks() to
     * update the LED's.
     */
    ImRx400LedOn(IM_AUTO_OFF);
    ImTx400LedOn(IM_AUTO_OFF);
}

__inline void
ImHkUserStatusIsr(void)
{
    /* Turn on the 400 MHz RX & TX LED's to indicate a remote HK access was
     * received and replied to, & let ImPeriodicLedTasks() turn them off
     * automatically. ImProc() periodically calls ImPeriodicLedTasks() to
     * update the LED's.
     */
    ImRx400LedOn(IM_AUTO_OFF);
    ImTx400LedOn(IM_AUTO_OFF);
}

__inline void
ImHkRemoteDoneIsr(void)
{
    /* Turn on the 400 MHz RX LED to indicate a reply was received for a remote
     * HK access, and let ImPeriodicLedTasks() turn it off automatically. Also,
     * the 400 MHz TX LED is already on for the HK TX, so keep it on, but switch
     * it to IM_AUTO_OFF so ImPeriodicLedTasks() will turn it off if there's no
     * data in the ZL7010X's TX buffer. ImProc() periodically calls
     * ImPeriodicLedTasks() to update the LED's.
     */
    ImRx400LedOn(IM_AUTO_OFF);
    ImTx400LedOn(IM_AUTO_OFF);
}

__inline void
ImCommandDoneIsr(void)
{
    IM_MICS_PRIV *im = &ImMicsPriv;
    
    /* Clear txBytesAvail in case the command flushed the TX buffer in the
     * MICS chip. That way, the next time ImTransmitMics() is called, it will
     * re-read TXBUFF_USED and recalculate txBytesAvail.
     * 
     * Clear rxBytesAvail in case the command flushed the RX buffer in the MICS
     * chip. That way, the next time ImReceiveMics() is called, it will re-read
     * RXBUFF_BUSED and detect if the RX buffer is still empty.
     */
    im->txBytesAvail = 0;
    im->rxBytesAvail = 0;
}

__inline UINT
ImConLostIsr(UINT irqAuxStat)
{
    /* if auxiliary status hasn't been read yet, read it */     
    if (irqAuxStat == 0) irqAuxStat = MicsRead(IRQ_AUXSTATUS);
    
    /* If received abort link from base (via housekeeping or ID 0xFFFFFF),
     * clear IRQ_HKABORTLINK bit in IRQ_AUXSTATUS. Also turn on the 400 MHz RX
     * LED to indicate abort link was received. Note IM_AUTO_OFF is specified to
     * tell ImPeriodicLedTasks() to turn the LED off automatically, but ImProc()
     * will most likely call ImMicsSleep() before ImPeriodicLedTasks(), and
     * ImMicsSleep() will also turn it off.
     */
    if (irqAuxStat & IRQ_HKABORTLINK) {
        
        MicsWrite(IRQ_AUXSTATUS, ~IRQ_HKABORTLINK);
        ImRx400LedOn(IM_AUTO_OFF);
    }
    
    /* If MICS watchdog timer, clear it (clears interrupt source), then clear
     * the bit in the auxiliary status. Clearing the watchdog also ensures it
     * won't finish counting to ~5 seconds and force the MICS chip to sleep
     * without the software knowing about it (if it did, the software would
     * think the chip was awake when it was actually sleeping, which could
     * cause problems).
     * 
     * Note if DEBUG is defined, the watchdog is also disabled. That way, if
     * you're using a debugger, the watchdog won't force the chip back to
     * sleep while you're stepping through code. If DEBUG isn't defined, the
     * watchdog is left enabled. That way, if the micro-controller gets lost
     * and never puts the MICS chip back to sleep, it will go back to sleep on
     * its own (to help ensure it won't drain the battery).
     */
    if (irqAuxStat & IRQ_WDOG) {
        
        #ifdef DEBUG
            MicsWrite(WDOG_CONTROL, WDOG_CLEAR_AND_DISAB);
        #else
            MicsWrite(WDOG_CONTROL, WDOG_CLEAR);
        #endif
        MicsWrite(IRQ_AUXSTATUS, ~IRQ_WDOG);
    }
    
    /* Set IM_MICS_SLEEP_PEND flag to tell ImProc() to put the MICS chip back
     * to sleep. ImProc() will detect this flag and call ImMicsSleep(), which in
     * turn will call MicsSleep() to put the chip to sleep. ImMicsSleep() isn't
     * called here in the ISR because that could cause race conditions if the
     * software is doing something with the MICS chip when IRQ_CLOST occurs.
     * 
     * Also, disable IRQ_CLOST for the following reasons:
     * 
     * - If IRQ_CLOST was left enabled, then every time the ZL7010X asserted
     *   IRQ_WDOG (after ~4.37 seconds), this ISR would clear the watchdog
     *   timer. As a result, the watchdog timer could never reach ~5 seconds
     *   and automatically put the ZL7010X back to sleep. Note the watchdog
     *   should never reach 5 seconds anyway, because ImProc() should always
     *   detect IM_MICS_SLEEP_PEND and put the ZL7010X to sleep in well under
     *   5 seconds. However, if something goes wrong and ImProc() never puts
     *   the ZL7010X back to sleep, the watchdog will do it automatically,
     *   ensuring the ZL7010X won't drain too much power.
     * 
     * - If the ZL7010X receives ID 0xFFFFFF, it will keep asserting
     *   IRQ_HKABORTLINK (and therefore IRQ_CLOST) until it goes to sleep or
     *   is reset (even after IRQ_HKABORTLINK is cleared in IRQ_AUXSTATUS).
     *   Disabling IRQ_CLOST ensures this won't happen.
     * 
     * Note the ZL7010X will re-enable IRQ_CLOST the next time it wakes up.
     */
    ImPub.pend |= IM_MICS_SLEEP_PEND;
    MicsWrite(IRQ_ENABLECLEAR1, IRQ_CLOST);
    
    /* return auxiliary status read from MICS chip */
    return(irqAuxStat);
}
