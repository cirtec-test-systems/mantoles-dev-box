/**************************************************************************
 * This file contains functions for the communication interface on the
 * implant mezzanine board.
 *
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Standard/String.h"           /* memset() */
#include "Standard/StdIo.h"            /* sprintf() */
#include "Adp/General.h"               /* UINT, RSTAT, TRUE/FALSE, ... */
#include "Adp/Build/Build.h"           /* UD8, UD16, ... */ 
#include "Adp/AnyBoard/Com.h"          /* COM_PACK_HDR, ... */
#include "Adp/AnyBoard/AdpSpiHw.h"     /* ASPI_TX_BUF, ... */
#include "Adp/AnyBoard/AdpSpiLib.h"    /* ASpiInit(), ... */
#include "Adp/AnyBoard/SysTimerLib.h"  /* StElapsedMs(), ... */ 
#include "Adk/AnyMezz/MicsHw.h"        /* HK_USERSTATUS, ... */
#include "Adk/AnyMezz/MicsLib.h"       /* MicsRead(), ... */
#include "Adk/ImMezz/ImCom.h"          /* IM_LOCAL_ADDR */

/* local include shared by all source files for implant application */
#include "ImApp.h"

/**************************************************************************
 * Defines and Macros
 */
 
/* Communication interface identifiers:
 * 
 * IM_ADP_SPI_INTERFACE: SPI interface between implant mezzanine & ADP board.
 * IM_MICS_HK_INTERFACE: MICS housekeeping interface.
 * IM_MICS_DATA_INTERFACE: MICS data interface.
 */
#define IM_ADP_SPI_INTERFACE    0
#define IM_MICS_HK_INTERFACE    1
#define IM_MICS_DATA_INTERFACE  2

/* Set communications timeout (milliseconds). If an expected byte isn't
 * received or a byte can't be transmitted within this timeout, the
 * communication interface will abort the packet being received or
 * transmitted and report the error.
 */
#define IM_COM_TIMEOUT_MS  2000

/* Default error message for errors that occur on an implant. Note
 * this does not end with a period because ImSendErrReply() appends
 * ": <ErrGroup>.<ErrCode>" to it.
 */
#define IM_ERR_MSG  "Error on implant"

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* structure of private data for communication interface */
typedef struct {
    
    /* Various flags for the communication interface. For bit definitions,
     * see IM_ENAB_ERR_REPLY, etc. This is declared volatile to prevent the
     * compiler from assuming it won't change during a section of code, in
     * case it is changed by interrupts.
     */
    volatile UD8 flags;
    
    UD8 rxInterface;
    
    UD8 micsInterface;
    
    UD8 repSrcAndDest;
    
} IM_COM_PRIV;
/*
 * Defines for bits in IM_COM_PRIV.flags:
 * 
 * IM_ENAB_ERR_REPLY: ImReceiveAndProcCmdPack() sets this before receiving and
 * processing a command so it will send an error reply if the command fails. If
 * needed, this is cleared while the command is being received and processed so
 * ImReceiveAndProcCmdPack() won't try to send an error reply.
 * 
 * IM_FORCE_SPI_RX_FULL: If ASPI_RX_FULL is clear when ImProcAdpSpiRx() is
 * called, it sets this flag to indicate the SPI RX buffer contains a byte even
 * though ASPI_RX_FULL is clear. This is needed because a SPI RX interrupt
 * occurs for the first byte of each packet (see ImAdpSpiRxIsr()), which
 * automatically clears ASPI_RX_FULL on some MSP430 variants.
 */
#define IM_ENAB_ERR_REPLY     (1 << 0)
#define IM_FORCE_SPI_RX_FULL  (1 << 1)
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/* private data for communication interface */
static IM_COM_PRIV ImComPriv;

/**************************************************************************
 * Function Prototypes
 */
 
static RSTAT ImSendSpi(const void *data, UINT len);
static RSTAT ImSendMicsHk(const void *data, UINT len);
static RSTAT ImReceiveAndProcPack(int thisBoardAddr);
/**/
__inline RSTAT ImReceiveAndProcCmdPack(const COM_PACK_HDR *packHdr, UINT packLen);
__inline RSTAT ImReceiveAndForwardPack(const COM_PACK_HDR *packHdr, UINT packLen);
__inline RSTAT ImReceiveSpi(void *buf, UINT len);
__inline RSTAT ImReceiveMicsHk(void *buf, UINT len);
__inline RSTAT ImReceiveAndForwardToSpi(UINT len);
__inline RSTAT ImReceiveAndForwardToMicsHk(UINT len);
/**/
__interrupt void ImAdpSpiRxIsr(void);

/**************************************************************************
 * Initialize the communication interface on the implant.
 */
RSTAT
ImComInit(void)
{
    IM_COM_PRIV *ic =&ImComPriv;
    
    /* clear data and init any non-zero defaults */
    (void)memset(ic, 0, sizeof(*ic));
    ic->micsInterface = IM_MICS_HK_INTERFACE;
    
    /* init ADP SPI interface (configures SPI for slave mode) */
    if (ASpiInit()) return(-1);
    
    return(0);
}

void
ImProcAdpSpiRx(void)
{
    IM_COM_PRIV *ic =&ImComPriv;
    
    /* clear IM_ADP_SPI_RX_PEND flag for ImProc() */
    ImPub.pend &= ~IM_ADP_SPI_RX_PEND;
    
    /* set interface we're receiving packet from */
    ic->rxInterface = IM_ADP_SPI_INTERFACE;
    
    /* If ASPI_RX_FULL is clear, set flag to indicate the SPI RX buffer
     * contains a byte even though ASPI_RX_FULL is clear. This is needed
     * because a SPI RX interrupt occurs for the first byte of each packet
     * (see ImAdpSpiRxIsr()), which automatically clears ASPI_RX_FULL on
     * some MSP430 variants.
     */
    if (!ASPI_RX_FULL) ic->flags |= IM_FORCE_SPI_RX_FULL;
    
    /* Receive packet via SPI and process it. Note for a packet received via
     * SPI, this board is the local implant mezzanine board, so use
     * IM_LOCAL_ADDR for this board's address.
     */
    (void)ImReceiveAndProcPack(IM_LOCAL_ADDR);
    
    /* re-enable ADP SPI RX interrupt for first byte of next packet */
    ASPI_WR_RX_INT_ENAB(TRUE);
}

static RSTAT
ImReceiveAndProcPack(int thisBoardAddr)
{
    COM_PACK_HDR packHdr;
    UINT destAddr;
    UD16 packLen;
    
    /* receive packet header */
    if (ImReceive(&packHdr, sizeof(packHdr))) goto error;
    
    /* get packet length from header (note it may be 0) */
    packLen = ((UD16)packHdr.lenMsb << 8) | packHdr.lenLsb;
    
    /* If the destination address is 0, communication may not be synchronized
     * on packet boundaries, so receive & discard bytes until it times out.
     */
    if ((destAddr = (packHdr.srcAndDest & 0x0F)) == 0) {
        ImErr(IM_APP_ERR_RECEIVED_PACK_WITH_DEST_0, NULL);
        ImReceiveAndDiscard(COM_MAX_PACK_LEN);
        goto error;
    }
    
    /* if packet is for us, receive and process command; else forward packet */
    if (destAddr == thisBoardAddr) {
        if (ImReceiveAndProcCmdPack(&packHdr, packLen)) goto error;
    } else {
        if (ImReceiveAndForwardPack(&packHdr, packLen)) goto error;
    }
    
    /* If the SPI interface was switched to master mode (to forward a packet
     * to SPI, or to reply to a command), configure it for slave mode again.
     */
    if (ASPI_MASTER) ASpiSetSlave();
    return(0);
    
error:
    /* reset SPI interface in case it's out of sync (shouldn't happen) */
    ASpiSetSlave();
    
    return(-1);
}

__inline RSTAT
ImReceiveAndProcCmdPack(const COM_PACK_HDR *packHdr, UINT packLen)
{
    IM_COM_PRIV *ic =&ImComPriv;
    const ERR_INFO *ei;
    RSTAT rStat;
    
    /* set reply source and destination (swap command source and destination) */
    ic->repSrcAndDest = (packHdr->srcAndDest << 4) | (packHdr->srcAndDest >> 4);
    
    /* enable error reply in case command fails (cleared later if needed) */
    ic->flags |= IM_ENAB_ERR_REPLY;
    /*
     * Receive remainder of command packet, process it, and send reply. If
     * that fails, and it's still ok to send an error reply, do so. An error
     * reply is a packet header with type COM_ERR_REPLY_TYPE followed by an
     * error ID string.
     */
    rStat = ImReceiveAndProcCmd(packHdr->type, packLen);
    if (rStat && (ic->flags & IM_ENAB_ERR_REPLY)) {
            
        /* If the error is sent successfully, and it was the first error since
         * ErrClearFirst() was called, clear the first error. That way, it
         * won't be reported in the status, which would be redundant.
         */
        ei = ErrGet();
        if (ImSendErrReply(COM_ERR_REPLY_TYPE, ei) == 0) {
            if (ErrIsFirst(ei->errGroup, ei->errCode)) ErrClearFirst();
        }
    }
    return(rStat);
}

__inline RSTAT
ImReceiveAndForwardPack(const COM_PACK_HDR *packHdr, UINT packLen)
{
    IM_COM_PRIV *ic =&ImComPriv;
    
    /* TODO: The implant does not currently support the forwarding of command
     * and reply packets via MICS housekeeping (it didn't work in preliminary
     * tests, and hasn't yet been debugged). Thus, just receive & discard the
     * packet (to keep communication synchronized on packet boundaries).
     */
    if (packHdr != NULL) {
        ImReceiveAndDiscard(packLen);
        return(0);
    }
    
    /* if receiving packet via ADP SPI, forward it to MICS */
    if (ic->rxInterface == IM_ADP_SPI_INTERFACE) {
        
        /* if using housekeeping for MICS communication interface */
        if (ic->micsInterface == IM_MICS_HK_INTERFACE) {
            
            /* Force IRQ_HKUSERSTATUS to 1 so ImSendMicsHk() will send the
             * first byte of the packet without waiting for the remote side to
             * write to HK_USERSTATUS on the local MICS chip (to acknowledge
             * previous byte).
             */
            MicsPub.irqStat1Latch |= IRQ_HKUSERSTATUS;
            
            /* Forward packet header to MICS housekeeping, and if it fails,
             * receive & discard remainder of packet (to keep communication
             * synchronized on packet boundaries).
             */
            if (ImSendMicsHk(packHdr, sizeof(*packHdr))) {
                ImReceiveAndDiscard(packLen);
                return(-1);
            }
            
            /* forward remainder of packet to MICS housekeeping interface */
            if (packLen) return(ImReceiveAndForwardToMicsHk(packLen));
            
        } else { /* else forward packet from SPI to MICS data interface */
            
            /* Receive and discard remainder of packet (to keep communication
             * synchronized on packet boundaries).
             */
            ImErr(IM_APP_ERR_MICS_DATA_NOT_YET_SUPPORTED, NULL);
            ImReceiveAndDiscard(packLen);
            return(-1);
        }
        
    } else { /* else forward packet from MICS to SPI */
    
        /* configure SPI interface for master mode */
        ASpiSetMaster();
            
        /* Forward packet header to SPI, and if it fails, receive and discard
         * remainder of packet (to keep communication synchronized).
         */
        if (ImSendSpi(packHdr, sizeof(*packHdr))) {
            ImReceiveAndDiscard(packLen);
            return(-1);
        }
                
        /* forward remainder of packet to SPI */
        if (packLen) return(ImReceiveAndForwardToSpi(packLen));
    }
    
    return(0);
}

RSTAT
ImReceive(void *buf, UINT len)
{
    IM_COM_PRIV *ic =&ImComPriv;
    RSTAT rStat;
    
    /* receive bytes from interface we are receiving a packet from */
    switch(ic->rxInterface) {
    case IM_ADP_SPI_INTERFACE:
    
        rStat = ImReceiveSpi(buf, len);
        break;
        
    case IM_MICS_HK_INTERFACE:
    
        rStat = ImReceiveMicsHk(buf, len);
        break;
        
    default: /* assume receive interface is MICS data */
    
        ImErr(IM_APP_ERR_MICS_DATA_NOT_YET_SUPPORTED, NULL);
        rStat = -1;
        break;
    }
    /* If an error occurred, clear IM_ENAB_ERR_REPLY. That way, if the error
     * occurred while receiving a command packet, ImReceiveAndProcCmdPack()
     * won't try to send an error reply for the command (it's best not to).
     */
    if (rStat) ic->flags &= ~IM_ENAB_ERR_REPLY;
    
    return(rStat);
}

void
ImReceiveAndDiscard(UINT len)
{
    UINT n;
    UD8 buf[8];
    
    ErrLock(TRUE);
    while(len != 0) {
        
        /* receive more bytes and update length remaining */
        n = (len < sizeof(buf)) ? len : sizeof(buf);
        if (ImReceive(buf, n)) break;
        len -= n;
    }
    ErrLock(FALSE);
}

__inline RSTAT
ImReceiveSpi(void *buf, UINT len)
{
    IM_COM_PRIV *ic =&ImComPriv;
    UD16 startMs;
    UD8 *next = buf;
    
    while(len != 0) {
        
        /* while SPI RX buf is empty, wait for it */
        startMs = StMs();
        while(!ASPI_RX_FULL) {
                    
            /* If we know the first byte of a packet is ready, don't wait for
             * ASPI_RX_FULL. This is done because a SPI RX interrupt occurs for
             * the first byte, which automatically clears ASPI_RX_FULL on some
             * MSP430 variants.
             */
            if (ic->flags & IM_FORCE_SPI_RX_FULL) {
                ic->flags &= ~IM_FORCE_SPI_RX_FULL;
                break;
            }
            if (StElapsedMs(startMs, IM_COM_TIMEOUT_MS)) {
                ImErr(IM_APP_ERR_ADP_SPI_RX_TIMEOUT, NULL);
                return(-1);
            }
        }
                
        /* Read byte from SPI RX buf and save it in the specified buf (also
         * clears ASPI_RX_FULL), then invoke ASPI_STROBE_SLAVE_READY() to tell
         * the transmitting side (master) the byte was received.
         */
        *next++ = ASPI_RX_BUF;
        ASPI_STROBE_SLAVE_READY();
        
        /* decrement length remaining */
        --len;
    }
    
    return(0);
}

__inline RSTAT
ImReceiveMicsHk(void *buf, UINT len)
{
    UD16 startMs;
    UD8 *next = buf;
    
    while(len != 0) {
        
        /* Wait for the remote side to write the next byte to the HK_USERDATA
         * register on the local MICS chip. Note for the first byte of a
         * packet, we know IRQ_HKUSERDATA occurred when the first byte was
         * received in HK_USERDATA, so we know IRQ_HKUSERDATA must be set in
         * MicsPub.irqStat1Latch.
         */
        startMs = StMs();
        while((MicsPub.irqStat1Latch & IRQ_HKUSERDATA) == 0) {
            
            if (StElapsedMs(startMs, IM_COM_TIMEOUT_MS)) {
                ImErr(IM_APP_ERR_MICS_HK_RX_TIMEOUT, NULL);
                return(-1);
            }
        }
        MicsPub.irqStat1Latch &= ~IRQ_HKUSERDATA;
                
        /* save received byte in specified buf */
        *next++ = MicsRead(HK_USERDATA);
        
        /* Write a byte to the HK_USERSTATUS register on the remote MICS chip.
         * This tells the remote side the previous byte has been received. Note
         * it doesn't matter what the value of the byte is, so there's no need
         * to set HK_TXDATA first.
         */
        #if MICS_REV == ZL70101
            /* workaround for ZL70101 issue */
            if (MicsWriteHkTxAddr(HK_USERSTATUS, 100)) return(-1);
        #else
            MicsWrite(HK_TXADDR, HK_USERSTATUS);
        #endif
        
        /* decrement length remaining */
        --len;
    }
    
    return(0);
}

__inline RSTAT
ImReceiveAndForwardToMicsHk(UINT len)
{
    UD8 buf[8];
    UINT n;
    
    while(len != 0) {
        
        /* receive more bytes and update length remaining */
        n = (len < sizeof(buf)) ? len : sizeof(buf);
        if (ImReceive(buf, n)) return(-1);
        len -= n;
        
        /* Forward bytes to MICS housekeeping interface, and if it fails,
         * receive and discard remainder of packet (to keep communication
         * synchronized on packet boundaries).
         */
        if (ImSendMicsHk(buf, n)) {
            ImReceiveAndDiscard(len);
            return(-1);
        }
    }
    return(0);
}

__inline RSTAT
ImReceiveAndForwardToSpi(UINT len)
{
    UD8 buf[8];
    UINT n;
    
    while(len != 0) {
        
        /* receive more bytes and update length remaining */
        n = (len < sizeof(buf)) ? len : sizeof(buf);
        if (ImReceive(buf, n)) return(-1);
        len -= n;
        
        /* Forward bytes to SPI, and if it fails, receive and discard remainder
         * of packet (to keep communication synchronized on packet boundaries).
         */
        if (ImSendSpi(buf, n)) {
            ImReceiveAndDiscard(len);
            return(-1);
        }
    }
    return(0);
}

RSTAT
ImSendReply(const void *rep, UINT repLen)
{
    /* send reply packet header for successful command */
    if (ImSendReplyHdr(COM_OK_REPLY_TYPE, repLen)) return(-1);
    
    /* send reply data (note structure of reply depends on command) */
    if (repLen != 0) {
        return(ImSendReplyData(rep, repLen));
    }
    return(0);
}

/* This is called to send an error reply (i.e. a packet header with reply type
 * COM_ERR_REPLY_TYPE followed by an error ID string).
 */
RSTAT
ImSendErrReply(UD8 repType, const ERR_INFO *ei)
{
    UINT packLen, sErrCodeLen, errGroupLen, errArgsLen;
    char sErrCode[6];
    
    /* Build error code string. Max length = 5 (max string length for 16 bit
     * error code in decimal) + '\0' = 6.
     */
    (void)sprintf(sErrCode, "%d", ei->errCode);
    
    /* get string lengths (excluding terminating '\0') */
    errGroupLen = strlen(ei->errGroup);
    sErrCodeLen = strlen(sErrCode);
    errArgsLen = strlen(ei->errArgs);
    
    /* Calc length of error reply packet (error ID string + '\0' + error
     * arguments string + '\0') and send reply packet header.
     */
    packLen = errGroupLen + IM_LEN(".") + sErrCodeLen + IM_LEN(": ") +
        IM_LEN(IM_ERR_MSG) + IM_LEN(": ") + errGroupLen + IM_LEN(".") +
        sErrCodeLen + IM_LEN("\0") + errArgsLen + IM_LEN("\0");
        
    if (ImSendReplyHdr(repType, packLen)) return(-1);
    
    /* Send error ID string followed by '\0'. The string is:
     * 
     * <ErrGroup>.<ErrCode>: <IM_ERR_MSG>: <ErrGroup>.<ErrCode>
     */
    if (ImSendReplyData(ei->errGroup, errGroupLen)) return(-1);
    if (ImSendReplyData(".", IM_LEN("."))) return(-1);
    if (ImSendReplyData(sErrCode, sErrCodeLen)) return(-1);
    if (ImSendReplyData(": ", IM_LEN(": "))) return(-1);
    if (ImSendReplyData(IM_ERR_MSG, IM_LEN(IM_ERR_MSG))) return(-1);
    if (ImSendReplyData(": ", IM_LEN(": "))) return(-1);
    if (ImSendReplyData(ei->errGroup, errGroupLen)) return(-1);
    if (ImSendReplyData(".", IM_LEN("."))) return(-1);
    if (ImSendReplyData(sErrCode, sErrCodeLen)) return(-1);
    if (ImSendReplyData("\0", IM_LEN("\0"))) return(-1);
    
    /* send error arguments string followed by '\0' */
    if (ImSendReplyData(ei->errArgs, errArgsLen + 1)) return(-1);
    
    return(0);
}

RSTAT
ImSendReplyHdr(UD8 repType, UINT repLen)
{
    COM_PACK_HDR packHdr;
    IM_COM_PRIV *ic =&ImComPriv;
    
    /* Disable error reply so if an error is encountered after the reply is
     * started, ImReceiveAndProcCmdPack() won't try to send an error reply.
     */
    ic->flags &= ~IM_ENAB_ERR_REPLY;
    
    /* Build reply packet header. Note the reply type must be COM_OK_REPLY_TYPE
     * or COM_ERR_REPLY_TYPE. If any other value is specified for the reply
     * type, COM_ERR_REPLY_TYPE will be used instead.
     */
    packHdr.srcAndDest = ic->repSrcAndDest;
    packHdr.lenLsb = (UD8)(repLen);
    packHdr.lenMsb = (UD8)(repLen >> 8);
    if (repType == COM_OK_REPLY_TYPE) {
        packHdr.type = COM_OK_REPLY_TYPE;
    } else {    
        packHdr.type = COM_ERR_REPLY_TYPE;
    }
    
    /* send reply packet header to interface we received command from */
    switch(ic->rxInterface) {
    case IM_ADP_SPI_INTERFACE:
    
        /* configure ADP SPI interface for master mode */
        ASpiSetMaster();
        
        return(ImSendSpi(&packHdr, sizeof(packHdr)));
        
    case IM_MICS_HK_INTERFACE:  
    
        /* Force IRQ_HKUSERSTATUS to 1 so ImSendMicsHk() will send the first
         * byte of the packet without waiting for the remote side to write to
         * HK_USERSTATUS on the local MICS chip (to acknowledge previous byte).
         */
        MicsPub.irqStat1Latch |= IRQ_HKUSERSTATUS;
    
        return(ImSendMicsHk(&packHdr, sizeof(packHdr)));
        
    default: /* assume command received from MICS data */
        
        ImErr(IM_APP_ERR_MICS_DATA_NOT_YET_SUPPORTED, NULL);
        return(-1);
    }
}

RSTAT
ImSendReplyData(const void *data, UINT len)
{
    IM_COM_PRIV *ic =&ImComPriv;
    
    /* send reply data to interface we received command from */
    switch(ic->rxInterface) {
    case IM_ADP_SPI_INTERFACE:
    
        return(ImSendSpi(data, len));
        
    case IM_MICS_HK_INTERFACE:  
    
        return(ImSendMicsHk(data, len));
    
    default: /* assume command received from MICS data */
        
        ImErr(IM_APP_ERR_MICS_DATA_NOT_YET_SUPPORTED, NULL);
        return(-1);
    }
}

static RSTAT
ImSendSpi(const void *data, UINT len)
{
    UD16 startMs;
    const UD8 *next = data;
    
    while(len != 0) {
        
        /* while slave hasn't received the previous byte, wait for it */
        startMs = StMs();
        while(!ASPI_SLAVE_READY) {
            
            if (StElapsedMs(startMs, IM_COM_TIMEOUT_MS)) {
                ImErr(IM_APP_ERR_ADP_SPI_TX_TIMEOUT, NULL);
                return(-1);
            }
        }
        /* clear latched ASPI_SLAVE_READY before transmitting the next byte */
        ASPI_WR_SLAVE_READY(0);
        
        /* write next byte to SPI TX buf */
        ASPI_TX_BUF = *next++;
        
        /* decrement length remaining */
        --len;
    }
    
    return(0);
}

static RSTAT
ImSendMicsHk(const void *data, UINT len)
{
    const UD8 *next = data;
    UD16 startMs;
    
    while(len != 0) {
        
        /* Wait for the remote side to acknowledge receipt of the previous
         * byte. The remote side does this by writing to the HK_USERSTATUS
         * register on the local MICS chip. Note for the first byte of a 
         * packet, IRQ_HKUSERSTATUS is set before this function is called,
         * so the first byte will always be transmitted without waiting.
         */
        startMs = StMs();
        while((MicsPub.irqStat1Latch & IRQ_HKUSERSTATUS) == 0) {
            
            if (StElapsedMs(startMs, IM_COM_TIMEOUT_MS)) {
                ImErr(IM_APP_ERR_MICS_HK_TX_TIMEOUT, NULL);
                return(-1);
            }
        }
        MicsPub.irqStat1Latch &= ~IRQ_HKUSERSTATUS;
                
        /* write next byte to HK_USERDATA register on remote MICS chip */
        MicsWrite(HK_TXDATA, *next++);
        #if MICS_REV == ZL70101
            /* workaround for ZL70101 issue */
            if (MicsWriteHkTxAddr(HK_USERDATA, 100)) return(-1);
        #else
            MicsWrite(HK_TXADDR, HK_USERDATA);
        #endif
        
        /* decrement length remaining */
        --len;
    }
    
    return(0);
}

#pragma vector=ASPI_RX_VECTOR
__interrupt void
ImAdpSpiRxIsr(void)
{
    /* disable SPI RX interrupt */
    ASPI_WR_RX_INT_ENAB(FALSE);
        
    /* set IM_ADP_SPI_RX_PEND flag for ImProc() */
    ImPub.pend |= IM_ADP_SPI_RX_PEND;
    
#ifdef TODO
    /* Since we set a pending flag for ImProc(), adjust MCU's status register
     * on stack so we won't re-enter low power mode when ISR returns.
     */
    LPM4_EXIT;
#endif    
}
