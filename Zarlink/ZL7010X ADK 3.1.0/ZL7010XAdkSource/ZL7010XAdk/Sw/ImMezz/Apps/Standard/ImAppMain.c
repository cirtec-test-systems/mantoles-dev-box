/**************************************************************************
 * This file contains the main processing functions for the application
 * on the implant mezzanine board (main entry point, initialization, and
 * processing loop).
 *
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Standard/String.h"          /* memset() */
#include "Adp/General.h"              /* RSTAT, BOOL, TRUE/FALSE, ... */
#include "Adp/Build/Build.h"          /* UD16, WDTCTL, P1OUT_INIT, ... */
#include "Adp/AnyBoard/ErrLib.h"      /* ErrInit(), ... */
#include "Adp/AnyBoard/SysTimerLib.h" /* StInit(), ... */
#include "Adp/AnyBoard/LocalSpiLib.h" /* LSpiInit(), LSpiConfig() */
#include "Adp/AnyBoard/NvmLib.h"      /* NvmInit(), ... */

/* local include shared by all source files for implant application */
#include "ImApp.h"

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* structure of private data for main processing */
typedef struct {
    
    /* The time at which the last clock occurred. ImProc() uses this to
     * determine if it's time for the next clock. At each clock, ImProc()
     * checks if any periodic tasks need to be performed and performs them
     * as needed.
     */
    UD16 clockTimeMs;
    
} IM_MAIN_PRIV;
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/* public data for implant application */
IM_PUB  ImPub;

/* private data for main processing */
static IM_MAIN_PRIV ImMainPriv;

/* name of error group for implant application */
const char *ImAppErr = IM_APP_ERR;

/* Pointer to factory settings for ZL7010X radio in nonvolatile memory (NVM)
 * on both a bsm and an im. Note that the application must initialize this
 * before calling anything that references it.
 */
const IM_MICS_FACT_NVM *MicsFactNvm;

/**************************************************************************
 * Function Prototypes
 */
static void ImInit(void);
static RSTAT ImNvmInit(void);
__inline void ImInitMcIo(void);
__inline void ImInitMcClocks(void);
__inline void ImProc(void);
/* __inline void ImSleep(void); */

/**************************************************************************
 * Main entry point for application on implant mezzanine board.
 */
int
main(void)
{
    /* initialization */
    ImInit();
    
    /* main processing loop */
    while(1) { ImProc(); }

    #pragma diag_suppress 112
    return 0;
    #pragma diag_default 112
}

static void
ImInit(void)
{
    BOOL comInit = FALSE;
    
    /* stop watchdog timer */
    WDTCTL = WDTPW + WDTHOLD;
    
    /* init micro-controller I/O ports */
    ImInitMcIo();
    
    /* init micro-controller clocks (must be called before StInit()) */
    ImInitMcClocks();
    
    /* Clear public & private data and init any non-zero defaults. Note
     * this initializes ImPub.micsState to 0 (IM_SLEEPING).
     */
    (void)memset(&ImPub, 0, sizeof(ImPub));
    (void)memset(&ImMainPriv, 0, sizeof(ImMainPriv));
    
    /* init LED interface (turns all LED's off) */
    ImLedInit();

    /* init error lib */
    if (ErrInit()) goto error;
    
    /* init system timer and enable interrupts so it will start counting */
    if (StInit()) goto error;
    _enable_interrupts();
    
    /* init communication before remaining init so PC can get any error */
    if (ImComInit()) goto error;
    ImCmdInit();
    comInit = TRUE;
    
    /* Init local SPI bus interface to talk to MICS chip, etc. (for more
     * information, see IM_LSPI_CLK_FREQ in "ImApp.h").
     */
    if (LSpiInit(IM_LSPI_SPECS, IM_LSPI_CLK_DIV)) goto error;
    
    /* init interface for nonvolatile memory */
    if(ImNvmInit()) goto error;

    /* init interface for MICS (ZL7010X); must be done after ImNvmInit() */
    if (ImMicsInit()) goto error;
    ImPub.flags |= IM_MICS_INIT;

    /* done (init successful) */
    return;
    
error:    
    /* If the communication interface wasn't initialized, there's no way
     * to report the initialization error to the PC, so just loop forever
     * (shouldn't happen). Otherwise, tell the LED interface to flash all
     * LED's, and set IM_INIT_ERR to tell ImReceiveAndProcCmd() to report
     * the error to the PC.
     */
    if (comInit == FALSE) {
        while(1) {/* ImSleep(); */};
    }
    ImOverrideLeds(IM_FLASH_ALL_LEDS);
    ImPub.flags |= IM_INIT_ERR;
}

__inline void
ImInitMcIo(void)
{
    /* Init output ports. Note unused ports should be initialized as outputs
     * to prevent floating inputs and reduce power (recommended in MSP430
     * manual). It's also best to drive them low in case they are accidently
     * shorted to ground during testing. This also applies to test points.
     */
    P1OUT = P1OUT_INIT;
    P1DIR = P1DIR_INIT;
    /**/
    P2OUT = P2OUT_INIT;
    P2DIR = P2DIR_INIT;
    /**/
    P3OUT = P3OUT_INIT;
    P3DIR = P3DIR_INIT;
    /**/
    P4OUT = P4OUT_INIT;
    P4DIR = P4DIR_INIT;
    /**/
    P5OUT = P5OUT_INIT;
    P5DIR = P5DIR_INIT;
    /**/
    P6OUT = P6OUT_INIT;
    P6DIR = P6DIR_INIT;
}

/* Initialize the micro-controller clocks. Note this configures the system
 * timer in a special mode, so it must be called before StInit() (otherwise,
 * it would clobber the normal system timer settings).
 */
__inline void
ImInitMcClocks(void)
{
    /* Init BCSCTL1 (LFXT1 = 8 MHz, no XT2, and keep default resistor setting
     * so DCO frequency = ~800 KHz). Note after this, ACLK will source LFXT1
     * (8MHz), but MCLK and SMCLK will still source the DCO (~800 KHz).
     */
    BCSCTL1 = XTS | XT2OFF | RSEL2;
    
    /* Temporarily configure the system timer so we can use it to wait ~100 us
     * in the loop that follows. Note the MSP430 manual says if a timer clock
     * is asynchronous to MCLK, and the CPU tries to read the timer while it's
     * running, it might not read the correct value. Thus, this configures the
     * system timer to use the DCO (via SMCLK) so it's synchronous to MCLK.
     * 
     * ST_CC0_COUNT:
     *     The count at which the timer will set the CCIFG interrupt flag.
     *     This is calculated for ~100 us (100 * CountPerSec/UsPerSec =
     *     100 * SMCLK_FREQ/1000000).
     * ST_CONTROL:
     *   - Clock source = SMCLK (~800 KHz from DCO after power-on/reset).
     *   - Clock input divider = 1 (ST_DIV_1).
     *   - Mode control = MC_1 (count to ST_CC0_COUNT then restart from 0).
     */
    ST_CC0_COUNT = (UD16)((100 * SMCLK_FREQ) / (UD32)1000000UL);
    ST_CONTROL = ST_SMCLK | ST_DIV_1 | MC_1;
    /*
     * Switch MCLK source to LFXT1 divided by 2 (4 MHz). This procedure is
     * described in the MPS430 manual.
     */
    while(1) {
        
        IFG1 &= ~OFIFG;
        
        /* wait ~100 us (the MSP430 manual says to wait at least 50 us) */
        ST_COUNTER = 0;
        ST_CC0_CONTROL &= ~CCIFG;
        while(!(ST_CC0_CONTROL & CCIFG));
        
        if ((IFG1 & OFIFG) == 0) break;
    }
    BCSCTL2 = SELM_3 | DIVM_1;  /* set DIVM_1 so MCLK = (8 MHz / 2) */
    
    /* stop system timer */
    ST_CONTROL = 0;
}

__inline void
ImProc(void)
{
    IM_MAIN_PRIV *im = &ImMainPriv;
    BOOL clockOverdue;
    
    /* if any tasks are pending */
    if (ImPub.pend) {
        
        /* ADP SPI RX (set by ImAdpSpiRxIsr()) */
        if (ImPub.pend & IM_ADP_SPI_RX_PEND) ImProcAdpSpiRx();
        
        /* MICS session established (set by ImLinkReadyIsr()) */
        if (ImPub.pend & IM_MICS_SESSION_PEND) ImProcMicsSession();
        
        /* MICS data RX (set by ImMicsRxNotEmptyIsr()) */
        if (ImPub.pend & IM_MICS_RX_PEND) ImProcMicsRx();
        
        /* MICS abort (set by ImConLostIsr()) */
        if (ImPub.pend & IM_MICS_SLEEP_PEND) ImMicsSleep();
        
        /* MICS is transmitting test data (set by ImStartDt()) */
        if (ImPub.pend & IM_DT_TX_PEND) ImProcDtTx();
        
    } else {
        /* ImSleep(); */
    }
    
    /* if a clock period elapsed, do any periodic tasks that are pending */
    if (StElapsedMs(im->clockTimeMs, IM_MS_PER_CLOCK)) {
        
        /* update time at which the last clock occurred */
        im->clockTimeMs += IM_MS_PER_CLOCK;
        
        /* If the clock is overdue, set "clockOverdue" to tell the periodic
         * tasks. That way, if the micro-controller was busy doing something
         * for longer than IM_MS_PER_CLOCK, the periodic tasks can avoid
         * performing tasks multiple times in rapid succession (if needed).
         */
        if (StElapsedMs(im->clockTimeMs, IM_MS_PER_CLOCK)) {
            clockOverdue = TRUE;
        } else {
            clockOverdue = FALSE;
        }
        
        /* perform any periodic tasks required by MICS interface */
        if (ImPub.flags & IM_MICS_INIT) ImPeriodicMicsTasks(clockOverdue);
        
        /* perform any periodic tasks required by LED interface */ 
        ImPeriodicLedTasks(clockOverdue);
    }
}

#ifdef TODO
__inline void
ImSleep(void)
{
    /* disable interrupts so they won't set any pending flags */
    _disable_interrupts();
            
    /* If an interrupt occurred and set a pending flag before we disabled
     * interrupts, re-enable interrupts and continue processing loop.
     * Otherwise, switch the micro-controller to low power mode (stops
     * execution and enters low power mode until next interrupt).
     */
    if (ImPub.pend) {
         _enable_interrupts();
    } else {
        _bis_SR_register(LPM4_bits | GIE);
    }
}
#endif

void
ImConfigSpi(UINT clockDiv)
{
    UINT gie;

    /* Save current GIE setting (general interrupt enable), then disable all
     * interrupts so no ISR will try to use the local SPI bus.
     */
    gie = _get_SR_register() & GIE;
    _disable_interrupts();

    LSpiConfig(IM_LSPI_SPECS, clockDiv);

    /* if interrupts were enabled, re-enable them */
    _bis_SR_register(gie);
}

void
ImErr(UD16 imAppErrCode, const char *argFormats, ...)
{
    va_list va;

    /* init variable arguments list */
    va_start(va, argFormats);

    ErrSetVa(ImAppErr, imAppErrCode, argFormats, va);

    /* clean up variable args list */
    va_end(va);
}


/* This initializes the interface for nonvolatile memory.
 */
static RSTAT
ImNvmInit(void)
{
    const IM_NVM_HEAD *h;

    /* get pointer to NVM header */
    h = (IM_NVM_HEAD *)NVM_ADDR;

    /* init library used to manage nonvolatile memory */
    if (NvmInit(&h->nvmInfo, sizeof(IM_NVM),NVM_TYPE_10X_ADK_IM)) {
        return(-1);
    }

    /* init global pointers to factory settings for ZL7010X radio */
    MicsFactNvm = (IM_MICS_FACT_NVM *)((UD8 *)h + h->micsFactNvmOffset);

    return(0);
}
