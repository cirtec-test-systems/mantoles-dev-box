/**************************************************************************
 * This file contains functions to receive and process command packets on the
 * the implant mezzanine board.
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Standard/String.h"          /* memset() */
#include "Adp/General.h"              /* UINT, RSTAT, BOOL, TRUE/FALSE, ... */
#include "Adp/Build/Build.h"          /* UD8, UD16, ... */
#include "Adp/AnyBoard/Com.h"         /* COM_MAX_PACK_LEN */
#include "Adp/AnyBoard/ErrLib.h"      /* ErrGetFirst() */
#include "Adp/AnyBoard/TraceLib.h"    /* TRACE_ENAB, TraceGetNext(), ... */
#include "Adp/AnyBoard/SysTimerLib.h" /* StElapsedMs(), ... */
#include "Adk/AdkVer.h"               /* ADK_VER */
#include "Adk/AnyMezz/MicsHw.h"       /* INTERFACE_MODE, MAC_CHANNEL, ... */
#include "Adk/AnyMezz/MicsLib.h"      /* MicsRead(), MICS_SLEEP, ... */
#include "Adk/AnyMezz/MicsGeneral.h"  /* MICS_LINK_STAT, MICS_DATA_STAT, ...*/
#include "Adk/ImMezz/ImCom.h"         /* IM_WAKEUP_CMD, ... */
#include "Adk/ImMezz/ImGeneral.h"     /* IM_IDLE, IM_MODEL, AIM100, ...*/

/* local include shared by all source files for implant application */
#include "ImApp.h"

/**************************************************************************
 * Defines and Macros
 */
 
/* model name for implant mezzanine board */
#if IM_MODEL == AIM100
    #define IM_MODEL_NAME  AIM100_NAME
#elif IM_MODEL == AIM200
    #define IM_MODEL_NAME  AIM200_NAME
#elif IM_MODEL == AIM300
    #define IM_MODEL_NAME  AIM300_NAME
#else
    #error "Invalid IM_MODEL (make sure the build include defines IM_HW)."
#endif
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* structure of private data for command processing */
typedef struct {
    
    /* Buffer used to hold a received command while the command is being
     * processed. This is a union because it is only used to hold one command
     * at a time.
     */
    union {
        IM_WAKEUP_CMD wakeupCmd;
        IM_READ_MICS_REG_CMD readMicsRegCmd;
        IM_RECEIVE_MICS_CMD receiveMicsCmd;
        IM_WRITE_MICS_REG_CMD writeMicsRegCmd;
        IM_MICS_CAL_CMD micsCalCmd;
        IM_MICS_ADC_CMD micsAdcCmd;
        IM_START_TX_400_CARRIER_CMD startTx400CarrierCmd;
        IM_INT_RSSI_CMD intRssiCmd;
        IM_ENAB_EXT_RSSI_CMD enabExtRssiCmd;
        IM_ENAB_HK_WRITE_CMD enabHkWriteCmd;
        IM_GET_STAT_CHANGES_CMD getStatChangesCmd;
        IM_START_DATA_TEST_CMD startDataTestCmd;
    } cmdBuf;
    
    /* Temporary buffer used to hold data for a reply, and to hold data for the
     * MICS chip while receiving a command or sending a reply. This is a union
     * because it is only used for one thing at a time.
     */
    union {
        IM_STAT genStat;
        MICS_LINK_STAT linkStat;
        IM_READ_MICS_REG_REPLY readMicsRegReply;
        IM_MICS_ADC_REPLY micsAdcReply;
        IM_INT_RSSI_REPLY intRssiReply;
        IM_GET_MICS_FACT_NVM_REPLY getFactNvmReply;
        UD8 data[TXBUFF_BSIZE_MAX];
    } tempBuf;
    
} IM_CMD_PRIV;

/**************************************************************************
 * Global and Static Definitions
 */
 
/* private data for command processing */
static IM_CMD_PRIV ImCmdPriv;
 
/**************************************************************************
 * Function Prototypes
 */
static void ImGetGenStat(IM_STAT *gs);
static void ImGetLinkStat(MICS_LINK_STAT *ls);
static RSTAT ImSendDataWithLen(const void *data, UD8 len);
static void ImChangeChan(UINT chan);
static RSTAT ImCmdLenErr(void);
/**/ 
static RSTAT ImCmdGetDevType(UINT cmdLen);
static RSTAT ImCmdGetDevName(UINT cmdLen);
static RSTAT ImCmdGetVer(UINT cmdLen);
static RSTAT ImCmdGetErr(UINT cmdLen);
static RSTAT ImCmdGetStat(UINT cmdLen);
static RSTAT ImCmdGetMicsConfig(UINT cmdLen);
static RSTAT ImCmdSetMicsConfig(UINT cmdLen);
static RSTAT ImCmdWakeup(UINT cmdLen);
static RSTAT ImCmdSleep(UINT cmdLen);
static RSTAT ImCmdSendEmergency(UINT cmdLen);
static RSTAT ImCmdReadMicsReg(UINT cmdLen);
static RSTAT ImCmdWriteMicsReg(UINT cmdLen);
static RSTAT ImCmdReceiveMics(UINT cmdLen);
static RSTAT ImCmdTransmitMics(UINT cmdLen);
static RSTAT ImCmdMicsCal(UINT cmdLen);
static RSTAT ImCmdMicsAdc(UINT cmdLen);
static RSTAT ImCmdStartTx400Carrier(UINT cmdLen);
static RSTAT ImCmdIntRssi(UINT cmdLen);
static RSTAT ImCmdEnabExtRssi(UINT cmdLen);
static RSTAT ImCmdGetLinkStat(UINT cmdLen);
static RSTAT ImCmdGetLinkQual(UINT cmdLen);
static RSTAT ImCmdEnabHkWrite(UINT cmdLen);
static RSTAT ImCmdGetTraceMsg(UINT cmdLen);
static RSTAT ImCmdCopyMicsRegs(UINT cmdLen);
static RSTAT ImCmdGetStatChanges(UINT cmdLen);
static RSTAT ImCmdStartDataTest(UINT cmdLen);
static RSTAT ImCmdResetMics(UINT cmdLen);
static RSTAT ImCmdCalcMicsCrc(UINT cmdLen);
static RSTAT ImCmdGetFactNvm(UINT cmdLen);

/**************************************************************************
 * Initialize the command interface on the implant.
 */
void
ImCmdInit(void)
{
    IM_CMD_PRIV *ic = &ImCmdPriv;
    
    /* clear data and init any non-zero defaults */
    (void)memset(ic, 0, sizeof(*ic));
}

/* Receive and process the remainder of the command packet for the specified
 * command type, and send a reply.
 */
RSTAT
ImReceiveAndProcCmd(UINT cmdType, UINT packLen)
{
    RSTAT rStat;
    
    /* If ImInit() encountered an initialization error, report the error
     * instead of trying to process the command from the PC (that might not
     * be safe since the initialization failed). Note some basic commands are
     * still allowed so the PC can find the implant (to open a connection).
     */
    if (ImPub.flags & IM_INIT_ERR) {
        
        switch(cmdType) {
        case COM_GET_DEV_TYPE_CMD_TYPE:
        case COM_GET_DEV_NAME_CMD_TYPE:
            break;
        default:
            ImReceiveAndDiscard(packLen);
            (void)ImSendErrReply(COM_ERR_REPLY_TYPE, ErrGetFirst());
            return(-1);
        }
    }
    
    /* receive and process remainder of command packet, & send reply */
    switch(cmdType) {
    case COM_GET_DEV_TYPE_CMD_TYPE:
        rStat = ImCmdGetDevType(packLen); break;
    case COM_GET_DEV_NAME_CMD_TYPE:
        rStat = ImCmdGetDevName(packLen); break;
    case COM_GET_DEV_VER_CMD_TYPE:
        rStat = ImCmdGetVer(packLen); break;
    case COM_GET_DEV_ERR_CMD_TYPE:
        rStat = ImCmdGetErr(packLen); break;
    case IM_GET_STAT_CMD_TYPE:
        rStat = ImCmdGetStat(packLen); break;
    case IM_GET_MICS_CONFIG_CMD_TYPE:
        rStat = ImCmdGetMicsConfig(packLen); break;
    case IM_SET_MICS_CONFIG_CMD_TYPE:
        rStat = ImCmdSetMicsConfig(packLen); break;
    case IM_WAKEUP_CMD_TYPE:
        rStat = ImCmdWakeup(packLen); break;
    case IM_SLEEP_CMD_TYPE:
        rStat = ImCmdSleep(packLen); break;
    case IM_SEND_EMERGENCY_CMD_TYPE:
        rStat = ImCmdSendEmergency(packLen); break;
    case IM_READ_MICS_REG_CMD_TYPE:
        rStat = ImCmdReadMicsReg(packLen); break;
    case IM_RECEIVE_MICS_CMD_TYPE:
        rStat = ImCmdReceiveMics(packLen); break;
    case IM_WRITE_MICS_REG_CMD_TYPE:
        rStat = ImCmdWriteMicsReg(packLen); break;
    case IM_TRANSMIT_MICS_CMD_TYPE:
        rStat = ImCmdTransmitMics(packLen); break;
    case IM_MICS_CAL_CMD_TYPE:
        rStat = ImCmdMicsCal(packLen); break;
    case IM_MICS_ADC_CMD_TYPE:
        rStat = ImCmdMicsAdc(packLen); break;
    case IM_START_TX_400_CARRIER_CMD_TYPE:
        rStat = ImCmdStartTx400Carrier(packLen); break;
    case IM_INT_RSSI_CMD_TYPE:
        rStat = ImCmdIntRssi(packLen); break;
    case IM_ENAB_EXT_RSSI_CMD_TYPE:
        rStat = ImCmdEnabExtRssi(packLen); break;
    case IM_GET_LINK_STAT_CMD_TYPE:
        rStat = ImCmdGetLinkStat(packLen); break;
    case IM_GET_LINK_QUAL_CMD_TYPE:
        rStat = ImCmdGetLinkQual(packLen); break;
    case IM_ENAB_HK_WRITE_CMD_TYPE:
        rStat = ImCmdEnabHkWrite(packLen); break;
    case IM_GET_TRACE_MSG_CMD_TYPE:
        rStat = ImCmdGetTraceMsg(packLen); break;
    case IM_COPY_MICS_REGS_CMD_TYPE:
        rStat = ImCmdCopyMicsRegs(packLen); break;
    case IM_GET_STAT_CHANGES_CMD_TYPE:
        rStat = ImCmdGetStatChanges(packLen); break;
    case IM_START_DATA_TEST_CMD_TYPE:
        rStat = ImCmdStartDataTest(packLen); break;
    case IM_RESET_MICS_CMD_TYPE:
        rStat = ImCmdResetMics(packLen); break;
    case IM_CALC_MICS_CRC_CMD_TYPE:
        rStat = ImCmdCalcMicsCrc(packLen); break;
    case IM_GET_MICS_FACT_NVM_CMD_TYPE:
        rStat = ImCmdGetFactNvm(packLen); break;
    default:
        /* Since command was invalid, communication might not be synchronized on
         * packet boundaries, so receive and discard bytes until it times out.
         */
        ImErr(IM_APP_ERR_RECEIVED_CMD_WITH_INVALID_CMD_TYPE, "%u", cmdType);
        ImReceiveAndDiscard(COM_MAX_PACK_LEN);
        return(-1);
    }
    return(rStat);
}

static RSTAT
ImCmdGetDevType(UINT cmdLen)
{
    /* check command packet length */
    if (cmdLen != 0) return(ImCmdLenErr());
    
    /* send reply for successful command (device type string including '\0') */
    return(ImSendReply(IM_AIM_DEV_TYPE, sizeof(IM_AIM_DEV_TYPE)));
}

static RSTAT
ImCmdGetDevName(UINT cmdLen)
{
    /* check command packet length */
    if (cmdLen != 0) return(ImCmdLenErr());
    
    /* Send reply for successful command (device name including '\0'). The
     * implant doesn't currently provide an interface to set its device
     * name, so it just returns an empty string.
     */
    return(ImSendReply("", sizeof("")));
}

static RSTAT
ImCmdGetVer(UINT cmdLen)
{
    UINT packLen;
    
    /* check command packet length */
    if (cmdLen != 0) return(ImCmdLenErr());
    
    /* Calculate the length of the reply packet for the implant's version
     * string, then send the packet header followed by version string.
     */
    packLen = IM_LEN(IM_MODEL_NAME) + IM_LEN("; ") + IM_LEN(ADK_VER) +
        IM_LEN("\0");
    /**/    
    if (ImSendReplyHdr(COM_OK_REPLY_TYPE, packLen)) return(-1);
    if (ImSendReplyData(IM_MODEL_NAME, IM_LEN(IM_MODEL_NAME))) return(-1);
    if (ImSendReplyData("; ", IM_LEN("; "))) return(-1);
    if (ImSendReplyData(ADK_VER, IM_LEN(ADK_VER))) return(-1);
    if (ImSendReplyData("\0", IM_LEN("\0"))) return(-1);
    
    return(0);
}

static RSTAT
ImCmdGetErr(UINT cmdLen)
{
    /* check command packet length */
    if (cmdLen != 0) return(ImCmdLenErr());
    
    /* Send reply containing information for latched error (if any). Note if
     * no error is latched, the error code will be 0, the error group will be
     * a default string, and the error arguments will an empty string.
     */
    if (ImSendErrReply(COM_OK_REPLY_TYPE, ErrGetFirst())) return(-1);
    
    /* clear any latched error information */
    ErrClearFirst();
    
    return(0);
}

/* Kept for backwards compatibility (now queried via ImCmdGetStatChanges()).
 */
static RSTAT
ImCmdGetStat(UINT cmdLen)
{
    ImGetGenStat(&ImCmdPriv.tempBuf.genStat);
    return(ImSendReply(&ImCmdPriv.tempBuf.genStat, sizeof(IM_STAT)));
}

static RSTAT
ImCmdGetMicsConfig(UINT cmdLen)
{
    /* check command packet length */
    if (cmdLen != 0) return(ImCmdLenErr());
    
    /* send reply for successful command (MICS config) */
    return(ImSendReply(&ImPub.micsConfig, sizeof(ImPub.micsConfig)));
}

static RSTAT
ImCmdSetMicsConfig(UINT cmdLen)
{
    /* Check command packet length. Note the command length can be less than
     * the current size of ImPub.micsConfig (IM_MICS_CONFIG). That way, if
     * new fields are added to the end of IM_MICS_CONFIG, this interface will
     * remain compatible with older software on the PC (the fields added to the
     * end of the config will not be affected by the command, and will simply
     * be left as is).
     */
    if (cmdLen > sizeof(ImPub.micsConfig)) return(ImCmdLenErr());
    
    /* If MICS chip is active, put it to sleep (to abort), then make sure the
     * chip is awake (ImMicsWakeup() does nothing if already awake). Note this
     * is done before receiving the new MICS configuration to ensure the MICS
     * chip is idle while ImPub.micsConfig is overwritten.
     */
    if (ImPub.micsState > IM_IDLE) ImMicsSleep();
    ImMicsWakeup();
    
    /* receive new MICS config (up to command length) */
    if (ImReceive(&ImPub.micsConfig, cmdLen)) return(-1);
    
    /* update MICS config */
    ImConfigMics(&ImPub.micsConfig);
    
    /* send reply for successful command */
    return(ImSendReply(NULL, 0));
}

static RSTAT
ImCmdWakeup(UINT cmdLen)
{
    IM_WAKEUP_CMD *cmd;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(ImCmdLenErr());
    
    /* set pointer to command buf and receive rest of command packet */
    cmd = &ImCmdPriv.cmdBuf.wakeupCmd;
    if (ImReceive(cmd, sizeof(*cmd))) return(-1);
    
    /* If the ZL7010X is sleeping, wake it up and configure it for normal
     * link operation. That way, whenever the PC application requests a direct
     * wakeup, the ZL7010X will be configured with the normal link settings
     * (for consistency and convenience).
     */
    if (ImPub.micsState == IM_SLEEPING) {
    
        ImMicsWakeup();
        ImConfigLink(&ImPub.micsConfig.normalLinkConfig);
    }

    /* If specified "stay awake", disable the MICS watchdog timer so it won't
     * timeout and assert IRQ_WDOG (see ImConLostIsr()). That way, the ISR
     * won't set IM_MICS_SLEEP_PEND to tell ImProc() to put the MICS chip
     * to sleep, so the MICS chip will remain awake until the MICS watchdog
     * is re-enabled (when a session is started, etc.), or ImMicsSleep() is
     * called to put the chip back to sleep.
     */
    if (cmd->stayAwake) {
        
        MicsWrite(WDOG_CONTROL, WDOG_CLEAR_AND_DISAB);
        ImPub.pend &= ~IM_MICS_SLEEP_PEND;
    }

    /* send reply for successful command */
    return(ImSendReply(NULL, 0));
}

static RSTAT
ImCmdSleep(UINT cmdLen)
{
    IM_MICS_CONFIG *mc = &ImPub.micsConfig;
    
    /* check command packet length */
    if (cmdLen != 0) return(ImCmdLenErr());
    
    /* abort and put MICS chip to sleep (if it's not already sleeping) */
    ImMicsSleep();
    
    /* If configured to use 400 MHZ wakeup (instead of 2.45 GHz), reset the
     * wakeup search algorithm (see ImResetWake400() for details), so the user
     * can do a manual wakeup/sleep to reset the algorithm (for test/debug).
     */
    if (mc->flags & IM_400_MHZ_WAKEUP) ImResetWake400();
    
    /* send reply for successful command */
    return(ImSendReply(NULL, 0));
}

static RSTAT
ImCmdSendEmergency(UINT cmdLen)
{
    /* check command packet length */
    if (cmdLen != 0) return(ImCmdLenErr());
    
    /* start sending emergency transmissions (wakes MICS chip if sleeping) */
    ImSendEmergency();
    
    /* send reply for successful command */
    return(ImSendReply(NULL, 0));
}

static RSTAT
ImCmdReadMicsReg(UINT cmdLen)
{
    IM_READ_MICS_REG_CMD *cmd;
    IM_READ_MICS_REG_REPLY *rep;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(ImCmdLenErr());
    
    /* set pointer to command buf & receive rest of command */
    cmd = &ImCmdPriv.cmdBuf.readMicsRegCmd;
    if (ImReceive(cmd, cmdLen)) return(-1);
    
    /* set pointer to reply buf */
    rep = &ImCmdPriv.tempBuf.readMicsRegReply;
    
    /* make sure MICS chip is awake (does nothing if already awake) */
    ImMicsWakeup();
    
    /* read specified register in MICS chip */
    rep->regVal = (UD8)MicsRead(cmd->regAddr);

    /* send reply for successful command */
    return(ImSendReply(rep, sizeof(*rep)));
}

static RSTAT
ImCmdWriteMicsReg(UINT cmdLen)
{
    IM_WRITE_MICS_REG_CMD *cmd;
    UINT regAddr;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(ImCmdLenErr());
    
    /* set pointer to command buf & receive rest of command */
    cmd = &ImCmdPriv.cmdBuf.writeMicsRegCmd;
    if (ImReceive(cmd, cmdLen)) return(-1);
    
    /* make sure MICS chip is awake (does nothing if already awake) */
    ImMicsWakeup();
    
    /* If changing register that may affect the link status, set status flag
     * so change will be reported to application on PC (see IM_STAT).
     */
    regAddr = cmd->regAddr;
    if (((regAddr >= RXBUFF_BSIZE) && (regAddr <= TXBUFF_MAXPACKSIZE)) ||
        ((regAddr >= MAC_IMDTRANSID1) && (regAddr <= MAC_CHANNEL))) {
            
        ImPub.statFlags |= IM_LINK_STAT_CHANGED;
    }
    
    /* write to specified register in MICS chip */
    MicsWrite(cmd->regAddr, cmd->regVal);
    
    /* send reply for successful command */
    return(ImSendReply(NULL, 0));
}

static RSTAT
ImCmdTransmitMics(UINT cmdLen)
{
    UINT blockSize, bytesLeft;
    
    /* make sure MICS chip is awake (does nothing if already awake) */
    ImMicsWakeup();

    /* get current TX block size (set via ImSetTxBlockSize()) */
    blockSize = ImPub.txBlockSize;
    
    /* init number of bytes to transmit */
    bytesLeft = cmdLen;
    
    /* receive data from command interface and write it to MICS TX buf */
    while(bytesLeft >= blockSize) {
        
        /* receive a block from command interface & update bytes left to do */
        if (ImReceive(ImCmdPriv.tempBuf.data, blockSize)) return(-1);
        bytesLeft -= blockSize;
            
        /* Write block to MICS TX buf and fail if it times out (also receive
         * and discard remainder of packet to keep communication synchronized).
         */
        if (ImTransmitMics(ImCmdPriv.tempBuf.data, blockSize, 4000) !=
            blockSize) {
            
            ImErr(IM_APP_ERR_MICS_DATA_TX_TIMEOUT, NULL);
            ImReceiveAndDiscard(bytesLeft);
            return(-1);
        }
        /* update data status and set flag to report it to PC (see IM_STAT) */
        ++ImPub.dataStat.txBlockCount;
        ImPub.statFlags |= IM_DATA_STAT_CHANGED;
    }
    
    /* If length wasn't a multiple of TX block size, fail (also receive and
     * discard remainder of packet to keep communication synchronized).
     */
    if (bytesLeft != 0) {
        ImErr(IM_APP_ERR_INVALID_TX_LEN, "%d\f%d", cmdLen, blockSize);
        ImReceiveAndDiscard(bytesLeft);
        return(-1);
    }
    
    /* send reply for successful command */
    return(ImSendReply(NULL, 0));
}

static RSTAT
ImCmdReceiveMics(UINT cmdLen)
{
    IM_RECEIVE_MICS_CMD *cmd;
    UINT blockSize, bytesAvail, bytesLeft;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(ImCmdLenErr());
    
    /* set pointer to command buf and receive rest of command packet */
    cmd = &ImCmdPriv.cmdBuf.receiveMicsCmd;
    if (ImReceive(cmd, sizeof(*cmd))) return(-1);
    
    /* if MICS chip is sleeping, send successful reply with no data */
    if (ImPub.micsState == IM_SLEEPING) return(ImSendReply(NULL, 0));
    
    /* get current RX block size (set via ImSetRxBlockSize()) */
    blockSize = ImPub.rxBlockSize;
    
    /* get number of bytes available in MICS RX buf */
    bytesAvail = MicsRead(RXBUFF_USED) * blockSize;
    
    /* If specified maximum length is < bytes available, prepare to read the
     * next smaller multiple of the block size (note this usually shouldn't
     * happen since the maximum length will usually be > bytes available).
     */
    if (cmd->maxLen < bytesAvail) {
        bytesLeft = (cmd->maxLen / blockSize) * blockSize;
    } else {
        bytesLeft = bytesAvail;
    }
    
    /* send reply packet header for successful command (replyLen = bytesLeft) */
    if (ImSendReplyHdr(COM_OK_REPLY_TYPE, bytesLeft)) return(-1);
    
    /* read data from MICS chip and send it to reply interface */
    while(bytesLeft >= blockSize) {
        
        /* Read a block from from MICS chip & update bytes left to do (no need
         * to check the return value since we know the bytes are available).
         */
        (void)ImReceiveMics(ImCmdPriv.tempBuf.data, blockSize, 0);
        bytesLeft -= blockSize;
        
        /* send block to reply interface (continuation of reply data) */
        if (ImSendReplyData(ImCmdPriv.tempBuf.data, blockSize)) return(-1);
        
        /* update data status and set flag to report it to PC (see IM_STAT) */
        ++ImPub.dataStat.rxBlockCount;
        ImPub.statFlags |= IM_DATA_STAT_CHANGED;
    }
    
    /* If all of the available data was read, re-enable IRQ_RXNOTEMPTY. That
     * way, if ImPeriodicLedTasks() automatically turns off the 400 MHz RX
     * LED the next time it's called, and more data is received after that,
     * IRQ_RXNOTEMPTY will recur and turn the LED back on again (see
     * ImMicsRxNotEmptyIsr()). Note if the ZL7010X has already received more
     * data, it will immediately reassert IRQ_RXNOTEMPTY, which is ok.
     */
    if (cmd->maxLen >= bytesAvail) {
        /* clear IRQ_RXNOTEMPTY so it won't recur if RX buf is now empty */
        MicsWrite(IRQ_RAWSTATUS1, ~IRQ_RXNOTEMPTY);
        MicsWrite(IRQ_ENABLESET1, IRQ_RXNOTEMPTY);
    }
    
    return(0);
}

static RSTAT
ImCmdMicsCal(UINT cmdLen)
{
    IM_MICS_CAL_CMD *cmd = &ImCmdPriv.cmdBuf.micsCalCmd;
    UINT cals;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) { 
        
        /* if only missing calSelect2, use 0 (for backwards compatibility) */
        if (cmdLen == (sizeof(*cmd) - 1)) {
            cmd->calSelect2 = 0;
        } else {
            return(ImCmdLenErr());
        }
    }
        
    /* receive rest of command packet */
    if (ImReceive(cmd, cmdLen)) return(-1);
    
    /* form calibration selection */
    cals = (cmd->calSelect2 << 8) | cmd->calSelect1;
    
    /* make sure MICS chip is awake (does nothing if already awake) */
    ImMicsWakeup();
    
    /* if doing CAL_400_MHZ_ANT and a channel is specified */
    if ((cals & CAL_400_MHZ_ANT) && (cmd->chan >= 0)) {
        
        /* if specified new channel, abort any MICS operation & set chan */
        if (cmd->chan != MicsRead(MAC_CHANNEL)) ImChangeChan(cmd->chan);
    }
    
    /* perform specified calibrations on MICS chip */
    MicsCal(cals);
    
    /* send reply for successful command */
    return(ImSendReply(NULL, 0));
}

static RSTAT
ImCmdMicsAdc(UINT cmdLen)
{
    IM_MICS_ADC_CMD *cmd;
    IM_MICS_ADC_REPLY *rep;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(ImCmdLenErr());
    
    /* set pointer to command buf and receive rest of command packet */
    cmd = &ImCmdPriv.cmdBuf.micsAdcCmd;
    if (ImReceive(cmd, sizeof(*cmd))) return(-1);
    
    /* make sure MICS chip is awake (does nothing if already awake) */
    ImMicsWakeup();
    
    /* set pointer to reply buf */
    rep = &ImCmdPriv.tempBuf.micsAdcReply;
    
    /* perform A/D conversion */
    rep->adcResult = (UD8)MicsAdc(cmd->adcInput);
    
    /* send reply for successful command */
    return(ImSendReply(rep, sizeof(*rep)));
}

static RSTAT
ImCmdStartTx400Carrier(UINT cmdLen)
{
    IM_START_TX_400_CARRIER_CMD *cmd;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(ImCmdLenErr());
    
    /* set pointer to command buf and receive rest of command packet */
    cmd = &ImCmdPriv.cmdBuf.startTx400CarrierCmd;
    if (ImReceive(cmd, sizeof(*cmd))) return(-1);
    
    /* If MICS chip is active, put it to sleep (to abort), then make sure the
     * chip is awake (ImMicsWakeup() does nothing if already awake).
     */
    if (ImPub.micsState > IM_IDLE) ImMicsSleep();
    ImMicsWakeup();
    
    /* start or stop transmitting 400 MHz TX carrier (continuous wave) */
    ImStartTx400Carrier(cmd->start, cmd->chan);
    
    /* send reply for successful command */
    return(ImSendReply(NULL, 0));
}

static RSTAT
ImCmdIntRssi(UINT cmdLen)
{
    IM_INT_RSSI_CMD *cmd;
    IM_INT_RSSI_REPLY *rep;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(ImCmdLenErr());
    
    /* set pointer to command buf and receive rest of command packet */
    cmd = &ImCmdPriv.cmdBuf.intRssiCmd;
    if (ImReceive(cmd, sizeof(*cmd))) return(-1);
    
    /* set pointer to reply buf */
    rep = &ImCmdPriv.tempBuf.intRssiReply;
    
    /* make sure MICS chip is awake (does nothing if already awake) */
    ImMicsWakeup();
    
    /* if specified new channel, abort any MICS op & change it */
    if (cmd->chan != MicsRead(MAC_CHANNEL)) ImChangeChan(cmd->chan);
    
    /* perform internal RSSI */
    rep->rssi = (UD8)ImIntRssi();
    
    /* send reply for successful command */
    return(ImSendReply(rep, sizeof(*rep)));
}

static RSTAT
ImCmdEnabExtRssi(UINT cmdLen)
{
    IM_ENAB_EXT_RSSI_CMD *cmd;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(ImCmdLenErr());
    
    /* set pointer to command buf and receive rest of command packet */
    cmd = &ImCmdPriv.cmdBuf.enabExtRssiCmd;
    if (ImReceive(cmd, sizeof(*cmd))) return(-1);
    
    /* make sure MICS chip is awake (does nothing if already awake) */
    ImMicsWakeup();
    
    /* If enabling external RSSI, do so. Note there is no external RSSI
     * hardware on an implant, but it can still be enabled to route the RF
     * signals in the MICS chip to its TESTIO[5,6] pins for monitoring &
     * measurement purposes (test & evaluation).
     */
    if (cmd->enab) {
        
        /* turn on 400 MHz RX LED & tell ImPeriodicLedTasks() to leave it on */
        ImRx400LedOn(IM_NO_AUTO_OFF);
        
        /* if specified new channel, abort any MICS op & change it */
        if (cmd->chan != MicsRead(MAC_CHANNEL)) ImChangeChan(cmd->chan);
        
        /* if ZL7010X idle, enable synth & RX, then give synth time to lock */
        if (ImPub.micsState == IM_IDLE) {
            MicsWrite(RF_GENENABLES, RX_RF | RX_IF | SYNTH);
            StDelayUs(MICS_LONG_SYNTH_LOCK_TIME_US);
        }
        
        /* enable external RSSI */
        (void)MicsEnabExtRssi(TRUE);
        
    } else {
        
        /* turn off 400 MHz RX LED */
        ImRx400LedOff();
        
        /* if MICS chip is idle, disable RX */
        if (ImPub.micsState == IM_IDLE) MicsWrite(RF_GENENABLES, 0);
        
        /* disable external RSSI */
        (void)MicsEnabExtRssi(FALSE);
    }
    
    /* send reply for successful command */
    return(ImSendReply(NULL, 0));
}

/* Kept for backwards compatibility (now queried via ImCmdGetStatChanges()).
 */
static RSTAT
ImCmdGetLinkStat(UINT cmdLen)
{
    /* make sure MICS chip is awake (does nothing if already awake) */
    ImMicsWakeup();

    ImPub.statFlags &= ~IM_LINK_STAT_CHANGED;
    ImGetLinkStat(&ImCmdPriv.tempBuf.linkStat);
    return(ImSendReply(&ImCmdPriv.tempBuf.linkStat, sizeof(MICS_LINK_STAT)));
}

/* Kept for backwards compatibility (now queried via ImCmdGetStatChanges()).
 */
static RSTAT
ImCmdGetLinkQual(UINT cmdLen)
{
    if (ImPub.micsState > IM_IDLE) ImUpdateLinkQual();
    ImPub.statFlags &= ~IM_LINK_QUAL_CHANGED;
    if (ImSendReply(&ImPub.linkQual, sizeof(ImPub.linkQual))) return(-1);
    ImPub.linkQual.crcErrs = 0;
    ImPub.linkQual.errCorBlocks = 0;
    ImPub.linkQual.maxBErrInts = 0;
    return(0);
}

/* Note in an end product, communication with an implant should be secured.
 * This command in particular is a security concern because it enables a base
 * station to write to the implant's registers via housekeeping.
 */
static RSTAT
ImCmdEnabHkWrite(UINT cmdLen)
{
    IM_ENAB_HK_WRITE_CMD *cmd;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(ImCmdLenErr());
    
    /* set pointer to command buf and receive rest of command packet */
    cmd = &ImCmdPriv.cmdBuf.enabHkWriteCmd;
    if (ImReceive(cmd, sizeof(*cmd))) return(-1);
    
    /* make sure MICS chip is awake (does nothing if already awake) */
    ImMicsWakeup();
    
    /* Enable or disable housekeeping write access on the ZL7010X. Note when the
     * ZL7010X sleeps, it will disable housekeeping write access, but the next
     * time the chip wakes up and asserts IRQ_RADIOREADY, ImRadioReadyIsr()
     * will check the config flags and enable it again if needed.
     */
    if (cmd->enab) {
        if (MICS_REV >= ZL70102) {
            MicsWriteBit(HK_MODE_102, HK_WRITE_ENAB, 1);
        } else {
            MicsWriteBit(INTERFACE_MODE, ENAB_HK_WRITE_101, 1);
        }
        ImPub.micsConfig.flags |= IM_ENAB_HK_WRITE;
    } else {
        if (MICS_REV >= ZL70102) {
            MicsWriteBit(HK_MODE_102, HK_WRITE_ENAB, 0);
        } else {
            MicsWriteBit(INTERFACE_MODE, ENAB_HK_WRITE_101, 0);
        }
        ImPub.micsConfig.flags &= ~IM_ENAB_HK_WRITE;
    }
    
    /* send reply for successful command */
    return(ImSendReply(NULL, 0));
}

static RSTAT
ImCmdGetTraceMsg(UINT cmdLen)
{
    const char *msg;
    UINT msgSize;
    
    /* check command packet length */
    if (cmdLen != 0) return(ImCmdLenErr());
    
    /* if tracing is enabled, get next trace message */
    if (TRACE_ENAB) {
        if ((msg = TraceGetNext()) == NULL) msg = "";
        msgSize = strlen(msg) + 1;
    } else {
        msg = "";
        msgSize = sizeof("");
    }
    /* send reply for successful command (message string including '\0') */
    return(ImSendReply(msg, msgSize));
}

static RSTAT
ImCmdCopyMicsRegs(UINT cmdLen)
{
    /* check command packet length */
    if (cmdLen != 0) return(ImCmdLenErr());
    
    /* make sure MICS chip is awake (does nothing if already awake) */
    ImMicsWakeup();
    
    /* Copy registers to the wakeup stack in the ZL7010X, so it will preserve
     * them while it's sleeping.
     */
    MicsCopyRegs();
    
    /* send reply for successful command */
    return(ImSendReply(NULL, 0));
}

static RSTAT
ImCmdGetStatChanges(UINT cmdLen)
{
    IM_CMD_PRIV *ic = &ImCmdPriv;
    IM_GET_STAT_CHANGES_CMD *cmd;
    UINT packLen, statFlags;
    
    /* set pointer to command buf */
    cmd = &ImCmdPriv.cmdBuf.getStatChangesCmd;
    
    /* If command packet includes any argument(s), check command length and
     * receive rest of command packet. Otherwise, use defaults.
     */
    if (cmdLen) {
        if (cmdLen != sizeof(*cmd)) return(ImCmdLenErr());
        if (ImReceive(cmd, sizeof(*cmd))) return(-1);
    } else {
        cmd->force = FALSE;
    }
    
    /* if ZL7010X is active, update link quality so we'll report the latest */
    if (ImPub.micsState > IM_IDLE) ImUpdateLinkQual();
    
    /* If specified "force", force status flags so we'll also report sections
     * of the status that haven't changed. Note that when ImGetGenStat() is
     * called below, it clears IM_LINK_STAT_CHANGED if the ZL7010X is sleeping
     * since the link status isn't available (so we won't report an invalid
     * link status to the PC).
     */
    if (cmd->force) {
        ImPub.statFlags |= (IM_LINK_STAT_CHANGED | IM_LINK_QUAL_CHANGED |
            IM_DATA_STAT_CHANGED);
    }
    /* Get general status in temp buf, then make copy of status flags (so it
     * will remain valid after temp buf is reused later in this function).
     */
    ImGetGenStat(&ic->tempBuf.genStat);
    statFlags = ic->tempBuf.genStat.flags;
    
    /* calc reply packet length and send reply packet header */
    packLen = (1 + sizeof(IM_STAT));
    if (statFlags & IM_LINK_STAT_CHANGED) {
        packLen += (1 + sizeof(MICS_LINK_STAT));
    }
    if (statFlags & IM_LINK_QUAL_CHANGED) {
        packLen += (1 + sizeof(MICS_LINK_QUAL));
    }
    if (statFlags & IM_DATA_STAT_CHANGED) {
        packLen += (1 + sizeof(MICS_DATA_STAT));
    }
    if (ImSendReplyHdr(COM_OK_REPLY_TYPE, packLen)) return(-1);
    
    /* send general status */
    if (ImSendDataWithLen(&ic->tempBuf.genStat, sizeof(IM_STAT))) return(-1);
    
    /* if needed, send link status */
    if (statFlags & IM_LINK_STAT_CHANGED) {
        
        /* Clear IM_LINK_STAT_CHANGED to indicate the link status hasn't
         * changed since the last time it was reported.
         */
        ImPub.statFlags &= ~IM_LINK_STAT_CHANGED;
        
        /* send link status */
        ImGetLinkStat(&ic->tempBuf.linkStat);
        if (ImSendDataWithLen(&ic->tempBuf.linkStat, sizeof(MICS_LINK_STAT))) {
            return(-1);
        }
    }
    
    /* if needed, send link quality */
    if (statFlags & IM_LINK_QUAL_CHANGED) {
        
        /* Clear IM_LINK_QUAL_CHANGED to indicate the link quality hasn't
         * changed since the last time it was reported.
         */
        ImPub.statFlags &= ~IM_LINK_QUAL_CHANGED;
        
        /* send link quality, then clear it */
        if (ImSendDataWithLen(&ImPub.linkQual, sizeof(MICS_LINK_QUAL))) return(-1);
        ImPub.linkQual.crcErrs = 0;
        ImPub.linkQual.errCorBlocks = 0;
        ImPub.linkQual.maxBErrInts = 0;
    }
    
    /* if needed, send data status */
    if (statFlags & IM_DATA_STAT_CHANGED) {
        
        /* Clear IM_DATA_STAT_CHANGED to indicate the data status hasn't
         * changed since the last time it was reported.
         */
        ImPub.statFlags &= ~IM_DATA_STAT_CHANGED;
        
        /* send data status, then clear it */
        if (ImSendDataWithLen(&ImPub.dataStat, sizeof(MICS_DATA_STAT))) return(-1);
        ImPub.dataStat.txBlockCount = 0;
        ImPub.dataStat.rxBlockCount = 0;
        ImPub.dataStat.dataErrCount = 0;
    }
    
    return(0);
}

static RSTAT
ImCmdStartDataTest(UINT cmdLen)
{
    IM_START_DATA_TEST_CMD *cmd;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(ImCmdLenErr());
    
    /* set pointer to command buf and receive rest of command packet */
    cmd = &ImCmdPriv.cmdBuf.startDataTestCmd;
    if (ImReceive(cmd, sizeof(*cmd))) return(-1);
    
    /* start data test */
    ImStartDt(cmd);
        
    /* send reply for successful command */
    return(ImSendReply(NULL, 0));
}

static RSTAT
ImCmdResetMics(UINT cmdLen)
{
    /* check command packet length */
    if (cmdLen != 0) return(ImCmdLenErr());

    /* make sure MICS chip is awake (does nothing if already awake) */
    ImMicsWakeup();

    /* reset and re-initialize ZL7010X */
    ImResetMics();

    /* send reply for successful command */
    return(ImSendReply(NULL, 0));
}

static RSTAT
ImCmdCalcMicsCrc(UINT cmdLen)
{
    /* check command packet length */
    if (cmdLen != 0) return(ImCmdLenErr());

    /* make sure MICS chip is awake (does nothing if already awake) */
    ImMicsWakeup();

    /* calculate CRC for ZL7010X */
    MicsCalcCrc();

    /* send reply for successful command */
    return(ImSendReply(NULL, 0));
}

static void
ImGetGenStat(IM_STAT *gs)
{
    /* get current state of ZL7010X */
    gs->micsState = ImPub.micsState;
    
    /* If the ZL7010X is sleeping, the link status (MICS_LINK_STAT) isn't
     * available, so make sure IM_LINK_STAT_CHANGED is clear so we won't report
     * an invalid link status to the PC. For example, when ImSearchBands() is
     * called to search for 400 MHz wakeup, it calls ImMicsWakeup() to wake
     * the ZL7010X, which sets IM_LINK_STAT_CHANGED, but if no wakeup signal
     * is found, ImSearchBands() just puts the ZL7010X back to sleep, in which
     * case there's no need to report the link status.
     */
    if (gs->micsState == IM_SLEEPING) ImPub.statFlags &= ~IM_LINK_STAT_CHANGED;
        
    /* Get the status flags to report to the PC, then clear any status flags
     * corresponding to LED's that have been turned off. The "LED on" functions
     * set status flags when the LED's are turned on (see ImAppLed.c), and the
     * flags remain set until they're reported to the PC (to ensure the GUI
     * will turn on its LED's). If the LED's have subsequently been turned off,
     * the status flags are cleared here so they'll be clear the next time the
     * PC gets the status (so the GUI will turn its LED's off).
     */
    gs->flags = ImPub.statFlags;
    /**/
    if (ImTx400LedIsOff()) ImPub.statFlags &= ~IM_400_MHZ_TX_ACTIVE;
    if (ImRx400LedIsOff()) ImPub.statFlags &= ~IM_400_MHZ_RX_ACTIVE;
    /* 
     * If an error is lacthed, set error flag.
     */
    if ((ErrGetFirst())->errCode) gs->flags |= IM_ERR_OCCURRED;
}

static void
ImGetLinkStat(MICS_LINK_STAT *ls)
{
    /* get link status */
    ls->companyId = MicsRead(MAC_COMPANYID);
    ls->imdTid.b1 = MicsRead(MAC_IMDTRANSID1);
    ls->imdTid.b2 = MicsRead(MAC_IMDTRANSID2);
    ls->imdTid.b3 = MicsRead(MAC_IMDTRANSID3);
    ls->chan = MicsRead(MAC_CHANNEL);
    ls->modUser = MicsRead(MAC_MODUSER); /* RF modulation (RX & TX), etc */
    ls->txBlockSize = MicsRead(TXBUFF_BSIZE);
    ls->rxBlockSize = MicsRead(RXBUFF_BSIZE) & RXBUFF_BSIZE_MASK;
    ls->maxBlocksPerTxPack = MicsRead(TXBUFF_MAXPACKSIZE);
}

static RSTAT
ImSendDataWithLen(const void *data, UD8 len)
{
    /* send length of data first */
    if (ImSendReplyData(&len, 1)) return(-1);
    
    /* send data */ 
    return(ImSendReplyData(data, len));
}

static void
ImChangeChan(UINT chan)
{
    /* if MICS chip is active, sleep (to abort operation), then rewake chip */
    if (ImPub.micsState > IM_IDLE) {
        ImMicsSleep();
        ImMicsWakeup();
    }
    
    /* set channel and status flag so it will be reported to the PC */
    MicsWrite(MAC_CHANNEL, chan);
    ImPub.statFlags |= IM_LINK_STAT_CHANGED;
}

static RSTAT
ImCmdGetFactNvm(UINT cmdLen)
{
    /* check command packet length */
    if (cmdLen != 0) return(ImCmdLenErr());

    /* reply with contents of factory NVM */
    return(ImSendReply(MicsFactNvm, sizeof(IM_MICS_FACT_NVM)));
}

static RSTAT
ImCmdLenErr(void)
{
    /* Since the received command packet length didn't match the expected
     * length, the communication may not be synchronized on packet boundaries.
     * To get it synchronized again, receive and discard bytes until it
     * times out. Note the timeout will also disable the error reply (see
     * IM_ENAB_ERR_REPLY in ImAppCom.c), so ImReceiveAndProcCmdPack() won't
     * send an error reply for the command (it's best not to send an
     * error reply if the communication is out of sync).
     */
    ImErr(IM_APP_ERR_RECEIVED_CMD_WITH_INVALID_LEN, NULL);
    ImReceiveAndDiscard(COM_MAX_PACK_LEN);
    return(-1);
}
