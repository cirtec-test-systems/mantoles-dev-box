/**************************************************************************
 * This include file contains things that are shared by all of the source files
 * for the implant application. Note this is private to the implant application,
 * and should not be included by anything outside of the implant application.
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
/* ensure this file is only included once */
#ifndef ImApp_h
#define ImApp_h

#include "Standard/String.h"         /* NULL (ImErr() args) */
#include "Standard/StdArg.h"         /* va_list (ImErr() args) */
#include "Adp/General.h"             /* UINT, RSTAT, BOOL, TRUE/FALSE, ... */
#include "Adp/Build/Build.h"         /* UD8, UD16, ... */
#include "Adp/AnyBoard/ErrLib.h"     /* ERR_INFO */
#include "Adp/AnyBoard/TraceLib.h"   /* Trace0(), ... (for test/debug use) */
#include "Adp/AnyBoard/LocalSpiHw.h" /* LSPI_SPEC_..., LSPI_CLK_FREQ, ... */
#include "Adk/AnyMezz/MicsGeneral.h" /* MICS_LINK_QUAL, MICS_DATA_STAT, ... */
#include "Adk/ImMezz/ImGeneral.h"    /* IM_MICS_CONFIG, ... */
#include "Adk/ImMezz/ImAppErrs.h"    /* ImAppErr, IM_APP_ERR_... */

/**************************************************************************
 * Defines and Macros
 */
 
/* Milliseconds per clock. This determines how often ImProc() checks if
 * any periodic tasks need to be performed, and the minimum time period and
 * resolution for all periodic tasks, so it should be set accordingly.
 */
#define IM_MS_PER_CLOCK  50
 
/* macro to get length of a constant string (excluding the '\0' terminator) */
#define IM_LEN(constStr)  (sizeof(constStr) - 1)

/* defines for "autoOff" argument passed to some "LED on" functions */
#define IM_NO_AUTO_OFF  FALSE
#define IM_AUTO_OFF     TRUE

/* defines for override mode passed to ImOverrideLeds() */
#define IM_NO_LED_OVERRIDE  0
#define IM_ALL_LEDS_OFF     1
#define IM_ALL_LEDS_ON      2
#define IM_FLASH_ALL_LEDS   3

/* Clock frequency, dividers, and specs for local SPI bus. Note if using a
 * ZL70101, its SPI interface resets to 2 MHz max whenever it's awakened or
 * reset. Thus, if the local SPI clock is normally 4 MHz, the implant must
 * temporarily change it to 2 MHz, configure the ZL70101 for 4 MHz, then 
 * restore the SPI clock to 4 MHz again. This is handled in the ZL70101
 * interrupt service routine (see ImMicsIsr() and ImInitInterfaceMode()).
 */
#define IM_LSPI_CLK_FREQ  4000000UL
#define IM_LSPI_CLK_DIV   (UD8)(LSPI_CLK_FREQ / IM_LSPI_CLK_FREQ)
/* clock divider for 1 & 2 MHz */
#define IM_LSPI_CLK_DIV_FOR_1_MHZ  (UD8)(LSPI_CLK_FREQ / (UD32)1000000UL)
#define IM_LSPI_CLK_DIV_FOR_2_MHZ  (UD8)(LSPI_CLK_FREQ / (UD32)2000000UL)
/* specs (for specs argument passed LSpiInit() and LSpiConfig()) */
#define IM_LSPI_SPECS  (LSPI_SPEC_INACTIVE_LOW_WITH_SAMP_ON_RISING_EDGE | \
    LSPI_SPEC_MASTER | LSPI_SPEC_8_BIT | LSPI_SPEC_MSB_FIRST)

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* structure of data shared by all source files for implant application */
typedef struct {
    
    /* Flags used to indicate tasks are pending & require processing. This is
     * declared volatile to prevent the compiler from assuming it won't change
     * during a section of code, since it may be changed by interrupts. For
     * bit definitions, see IM_ADP_SPI_RX_PEND, etc.
     */
    volatile UD8 pend;
    
    /* Various flags shared by all of the source files for the implant
     * application. For bit definitions, see IM_INIT_ERR, etc. This is declared
     * volatile to prevent the compiler from assuming it won't change during a
     * section of code, in case it is changed by interrupts.
     */
    volatile UD8 flags;
    
    /* Status flags for IM_STAT.flags (see IM_STAT, IM_ERR_OCCURRED, etc. in
     * "ZL7010XAdk\Sw\Includes\Adk\ImMezz\ImGeneral.h"). This is declared
     * volatile to prevent the compiler from assuming it won't change during
     * a section of code, since it may be changed by interrupts.
     */
    volatile UD8 statFlags;
    
    /* Current MICS operational state (see IM_SLEEPING, IM_IDLE, etc. in
     * "ZL7010XAdk\Sw\Includes\Adk\ImMezz\ImGeneral.h"). Note this is declared
     * "const" because it's managed by ImAppMics.c, which is the only thing
     * that should modify it. It is also declared volatile to prevent the
     * compiler from assuming it won't change during a section of code, since
     * the MICS interrupts change it.
     */
    volatile const UD8 micsState;
    
    /* current MICS config (see ImAppMics.c) */
    IM_MICS_CONFIG micsConfig;
    
    /* The current block size settings in TXBUFF_BSIZE and RXBUFF_BSIZE.
     * To set TXBUFF_BSIZE and RXBUFF_BSIZE, the software should always call
     * ImSetTxBlockSize() and ImSetRxBlockSize() so these block sizes are
     * also updated. That way, the software can reference the current block
     * sizes without having to read TXBUFF_BSIZE and RXBUFF_BSIZE (see
     * ImTransmitMics() and ImReceiveMics()).
     */
    const UD8 txBlockSize;
    const UD8 rxBlockSize;
    
    /* link quality statistics (see ImAppMics.c) */
    MICS_LINK_QUAL linkQual;
    
    /* data status (see ImAppMics.c) */
    MICS_DATA_STAT dataStat;
    
} IM_PUB;
/*
 * Defines for bits in IM_PUB.pend (task pending flags for ImProc()):
 * 
 * IM_ADP_SPI_RX_PEND:
 *     ImAdpSpiRxIsr() sets this to tell ImProc() to call ImProcAdpSpiRx()
 *     (receive and process a packet via the ADP SPI interface).
 * IM_MICS_SESSION_PEND:
 *     ImLinkReadyIsr() sets this to tell ImProc() to call ImProcMicsSession()
 *     (MICS session started).
 * IM_MICS_RX_PEND:
 *     ImMicsRxNotEmptyIsr() sets this to tell ImProc() to call ImProcMicsRx()
 *     (data received via MICS). 
 * IM_MICS_SLEEP_PEND:
 *     ImConLostIsr() sets this to tell ImProc() to call ImMicsSleep() (puts
 *     MICS chip back to sleep after it asserts IRQ_WDOG or IRQ_HKABORTLINK).
 * IM_DT_TX_PEND:
 *     ImStartDt() sets this to tell ImProc() to call ImProcDtTx() as often as
 *     possible (to transmit test data). This flag is cleared when ImMicsSleep()
 *     is called to stop the data test.
 */
#define IM_ADP_SPI_RX_PEND      (1 << 0)
#define IM_MICS_SESSION_PEND    (1 << 1)
#define IM_MICS_RX_PEND         (1 << 2)
#define IM_MICS_SLEEP_PEND      (1 << 3)
#define IM_DT_TX_PEND           (1 << 4)
/*
 * Defines for bits in IM_PUB.flags:
 * 
 * IM_INIT_ERR:
 *     If an error is encountered during initialization, ImInit() sets this
 *     to tell ImReceiveAndProcCmd() to report the error to the PC (instead
 *     of trying to process the command from the PC).
 * IM_MICS_INIT:
 *     ImInit() sets this to tell ImProc() the MICS (ZL7010X) interface
 *     has been initialized, so ImProc() will call ImPeriodicMicsTasks()
 *     to perform any periodic tasks required by the MICS interface.
 */
#define IM_INIT_ERR   (1 << 0)
#define IM_MICS_INIT  (1 << 1)

/**************************************************************************
 * Global Declarations
 */
 
/* data shared by all source files for implant application */
extern IM_PUB  ImPub;

/**************************************************************************
 * External Function Prototypes
 */
 
/* shared functions defined in ImAppMain.c */
extern void ImConfigSpi(UINT clockDiv);
extern void ImErr(UD16 imAppErrCode, const char *argFormats, ...);
 
/* shared functions defined in ImAppCom.c */
extern RSTAT ImComInit(void); 
extern void ImProcAdpSpiRx(void);
extern void ImReceiveAndDiscard(UINT len);
extern RSTAT ImReceive(void *buf, UINT len);
extern RSTAT ImSendReply(const void *rep, UINT repLen);
extern RSTAT ImSendReplyHdr(UD8 repType, UINT repLen);
extern RSTAT ImSendReplyData(const void *data, UINT len);
extern RSTAT ImSendErrReply(UD8 repType, const ERR_INFO *ei);

/* shared functions defined in ImAppCmd.c */
extern void ImCmdInit(void);
extern RSTAT ImReceiveAndProcCmd(UINT cmdType, UINT packLen);

/* shared functions defined in ImAppMics.c */
extern RSTAT ImMicsInit(void); 
extern void ImResetMics(void);
extern void ImMicsWakeup(void);
extern void ImMicsSleep(void);
extern void ImConfigMics(const IM_MICS_CONFIG *mc);
extern void ImConfigLink(const MICS_LINK_CONFIG *lc);
extern void ImSendEmergency(void);
extern void ImStartDt(const MICS_DT_SPECS *specs);
extern void ImStartTx400Carrier(BOOL start, UINT chan);
extern UINT ImIntRssi(void);
extern void ImProcMicsSession(void);
extern void ImProcMicsRx(void);
extern void ImProcDtTx(void);
extern UINT ImTransmitMics(const void *data, UINT nBytes, UINT timeoutMs);
extern UINT ImReceiveMics(void *buf, UINT nBytes, UINT timeoutMs);
extern void ImPeriodicMicsTasks(BOOL clockOverdue);
extern void ImResetWake400(void);
extern void ImUpdateLinkQual(void);
extern void ImMicsIsr(void);

/* shared functions defined in ImAppLed.c */
extern void ImLedInit(void);
extern void ImTx400LedOn(BOOL autoOff);
extern void ImTx400LedOff(void);
extern BOOL ImTx400LedIsOff(void);
extern void ImRx400LedOn(BOOL autoOff);
extern void ImRx400LedOff(void);
extern BOOL ImRx400LedIsOff(void);
extern void ImSessionLedOn(void);
extern void ImSessionLedOff(void);
extern void ImOverrideLeds(UINT mode);
extern void ImPeriodicLedTasks(BOOL clockOverdue);

#endif /* ensure this file is only included once */
