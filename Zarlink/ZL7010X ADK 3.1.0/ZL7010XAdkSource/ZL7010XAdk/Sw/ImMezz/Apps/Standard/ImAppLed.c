/**************************************************************************
 * This file contains functions to manage to the LED's on the implant
 * mezzanine board.
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

#include "Adp/General.h"         /* UINT, RSTAT, BOOL, WR_BITS(), ... */
#include "Adp/Build/Build.h"     /* UD8, TX_400_LED, LED_P(), ... */
#include "Adk/AnyMezz/MicsHw.h"  /* TXBUFF_USED, RXBUFF_USED */
#include "Adk/AnyMezz/MicsLib.h" /* MicsRead() */

/* local include shared by all source files for implant application */
#include "ImApp.h"

/**************************************************************************
 * Defines and Macros
 */
 
/* Milliseconds between updates of the LED's, and the corresponding number of
 * clocks. For best accuracy, this should be a multiple of IM_MS_PER_CLOCK.
 */
#define IM_MS_PER_LED_UPDATE  200
#define IM_CLOCKS_PER_LED_UPDATE \
    (IM_MS_PER_LED_UPDATE / IM_MS_PER_CLOCK)
    
/* Milliseconds between heartbeats (i.e. flash the heartbeat LED), and the
 * corresponding number of LED updates. For best accuracy, this should be a
 * multiple of IM_MS_PER_LED_UPDATE.
 */
#define IM_MS_PER_HEARTBEAT  3000
#define IM_LED_UPDATES_PER_HEARTBEAT \
    (IM_MS_PER_HEARTBEAT / IM_MS_PER_LED_UPDATE)
    
/* all LED's */
#define IM_LEDS  (TX_400_LED | RX_400_LED | SESSION_LED | HEARTBEAT_LED)
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* structure of private data for LED interface on implant */
typedef struct {
    
    /* Various flags for the LED's. For bit definitions, see
     * IM_TX_400_LED_AUTO_OFF, etc. This is declared volatile to prevent
     * the compiler from assuming it won't change during a section of code,
     * in case it's changed by interrupts.
     */
    volatile UD8 flags;
    
    /* Number of clocks left until ImPeriodicLedTasks() will update the LED's
     * (ImProc() periodically calls ImPeriodicLedTasks()).
     */
    UD8 clocksToLedUpdate;
    
    /* Number of LED updates left until ImPeriodicLedTasks() will flash the
     * heartbeat LED (ImProc() periodically calls ImPeriodicLedTasks() to
     * update the LED's).
     */
    UD8 ledUpdatesToHeartbeat;
    
    /* These are used to manage the on/off state of the LED's:
     * 
     * ledsOff: The desired state of the LED's. If a bit is set, it indicates
     * the application wants the corresponding LED off. ImPeriodicLedTasks()
     * will turn the LED off after it's been on for >= one update period.
     * ImProc() periodically calls ImPeriodicLedTasks() to update the LED's.
     * 
     * ledsOffForNextUpdate: LED's ImPeriodicLedTasks() will turn off during
     * the next update (ImProc() periodically calls ImPeriodicLedTasks() to
     * update the LED's). This is used to wait one update period before turning
     * off the LED's, to ensure they'll remain on long enough to be seen.
     */
    UD8 ledsOff;
    UD8 ledsOffForNextUpdate;
    
    /* override mode: IM_NO_LED_OVERRIDE (0), IM_ALL_LEDS_OFF, ... */
    UD8 overrideMode;
    
} IM_LED_PRIV;
/*
 * Defines for bits in IM_LED_PRIV.flags:
 * 
 * IM_TX_400_LED_AUTO_OFF: True if we want ImPeriodicLedTasks() to automatically
 * turn off the 400 MHz TX LED when the TX buffer on the ZL7010X is empty.
 * ImProc() periodically calls ImPeriodicLedTasks() to update the LED's.
 * 
 * IM_RX_400_LED_AUTO_OFF: True if we want ImPeriodicLedTasks() to automatically
 * turn off the the 400 MHz RX LED when the RX buffer on the ZL7010X is empty.
 * ImProc() periodically calls ImPeriodicLedTasks() to update the LED's.
 */
#define IM_TX_400_LED_AUTO_OFF     (1 << 0)
#define IM_RX_400_LED_AUTO_OFF     (1 << 1)
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/* private data for LED interface on implant */
static IM_LED_PRIV ImLedPriv;
 
/**************************************************************************
 * Function Prototypes
 */
 
/**************************************************************************
 * Initialize the LED interface on the implant.
 */
void
ImLedInit(void)
{
    IM_LED_PRIV *il = &ImLedPriv;
    
    /* clear data and init any non-zero defaults */
    (void)memset(il, 0, sizeof(*il));
    il->clocksToLedUpdate = IM_CLOCKS_PER_LED_UPDATE;
    il->ledUpdatesToHeartbeat = IM_LED_UPDATES_PER_HEARTBEAT;
    il->ledsOff = IM_LEDS;
    
    /* make sure all LED's are initially off */
    LED_P(OUT) |= IM_LEDS;
}

/**************************************************************************
 * Functions to turn LED's on and off. Note these functions perform additional
 * tasks such as checking for LED override, and managing LED status & control
 * flags. These functions work together with ImPeriodicLedTasks() to ensure
 * the LED's stay on long enough to be seen (ImProc() periodically calls
 * ImPeriodicLedTasks() to update the LED's).
 */
void
ImTx400LedOn(BOOL autoOff)
{
    /* if LED override is off, turn LED on */
    if (!ImLedPriv.overrideMode) LED_P(OUT) &= ~TX_400_LED;
    
    /* indicate LED is on */
    ImLedPriv.ledsOff &= ~TX_400_LED;
    
    /* make sure ImPeriodicLedTasks() won't turn LED off during next update */
    ImLedPriv.ledsOffForNextUpdate &= ~TX_400_LED;
    
    /* tell ImPeriodicLedTasks() to turn LED off when MICS TX buf is empty */
    if (autoOff) ImLedPriv.flags |= IM_TX_400_LED_AUTO_OFF;
    
    /* set status flag so it will be reported to PC */
    ImPub.statFlags |= IM_400_MHZ_TX_ACTIVE;
}

void
ImTx400LedOff(void)
{
    /* indicate caller wants LED off (ImPeriodicLedTasks() will turn it off) */
    ImLedPriv.ledsOff |= TX_400_LED;
    
    /* ensure the LED's "auto off" flag is clear since it's no longer needed */
    ImLedPriv.flags &= ~IM_TX_400_LED_AUTO_OFF;
}

BOOL
ImTx400LedIsOff(void)
{
    return(ImLedPriv.ledsOff & TX_400_LED);
}

void
ImRx400LedOn(BOOL autoOff)
{
    /* if LED override is off, turn LED on */
    if (!ImLedPriv.overrideMode) LED_P(OUT) &= ~RX_400_LED;
    
    /* indicate LED is on */
    ImLedPriv.ledsOff &= ~RX_400_LED;
    
    /* make sure ImPeriodicLedTasks() won't turn LED off during next update */
    ImLedPriv.ledsOffForNextUpdate &= ~RX_400_LED;
    
    /* tell ImPeriodicLedTasks() to turn LED off when MICS RX buf is empty */
    if (autoOff) ImLedPriv.flags |= IM_RX_400_LED_AUTO_OFF;
    
    /* set status flag so it will be reported to PC */
    ImPub.statFlags |= IM_400_MHZ_RX_ACTIVE;
}

void
ImRx400LedOff(void)
{
    /* indicate caller wants LED off (ImPeriodicLedTasks() will turn it off) */
    ImLedPriv.ledsOff |= RX_400_LED;
    
    /* ensure the LED's "auto off" flag is clear since it's no longer needed */
    ImLedPriv.flags &= ~IM_RX_400_LED_AUTO_OFF;
}

BOOL
ImRx400LedIsOff(void)
{
    return(ImLedPriv.ledsOff & RX_400_LED);
}

void
ImSessionLedOn(void)
{
    /* if LED override is off, turn LED on */
    if (!ImLedPriv.overrideMode) LED_P(OUT) &= ~SESSION_LED;
    
    /* indicate LED is on */
    ImLedPriv.ledsOff &= ~SESSION_LED;
    
    /* make sure ImPeriodicLedTasks() won't turn LED off during next update */
    ImLedPriv.ledsOffForNextUpdate &= ~SESSION_LED;
}
void
ImSessionLedOff(void)
{
    /* indicate caller wants LED off (ImPeriodicLedTasks() will turn it off) */
    ImLedPriv.ledsOff |= SESSION_LED;
}

/* Set LED override mode. The following modes can be specified:
 * 
 * IM_NO_LED_OVERRIDE:
 *     LED override is shut off and each LED is restored to its desired state.
 * IM_ALL_LEDS_OFF:
 *     All LED's are turned off and will remain off.
 * IM_ALL_LEDS_ON:
 *     All LED's are turned on and will remain on.
 * IM_FLASH_ALL_LEDS:
 *     All LED's are flashed.
 */
void
ImOverrideLeds(UINT mode)
{
    switch(mode) {
    case IM_NO_LED_OVERRIDE:
    
        /* restore each LED to its desired state */
        WR_BITS(LED_P(OUT), IM_LEDS, ImLedPriv.ledsOff);
        break;
    
    case IM_ALL_LEDS_OFF:

        /* turn off all LED's */
        LED_P(OUT) |= IM_LEDS;
        break;
        
    case IM_ALL_LEDS_ON:
    case IM_FLASH_ALL_LEDS:

        /* turn on all LED's */
        LED_P(OUT) &= ~IM_LEDS;
        break;
        
    default:
        /* specified invalid mode, so do nothing (shouldn't happen) */
        return;
    }
    
    /* save new LED override mode */
    ImLedPriv.overrideMode = mode;
}

/* ImProc() calls this each clock tick (see IM_MS_PER_CLOCK) to do any
 * periodic tasks required by the LED's on the implant.
 */
void
ImPeriodicLedTasks(BOOL clockOverdue)
{
    IM_LED_PRIV *il = &ImLedPriv;
    
    /* If the clock is overdue, don't update the LED's. That way, we'll never
     * update the LED's multiple times in rapid succession (it's ok if the
     * timing for LED updates sometimes shifts).
     */
    if (clockOverdue) return;
    
    /* if it's time to update the LED's, do so */
    if (--il->clocksToLedUpdate == 0) {
        
        /* restart number of clocks to next LED update */
        il->clocksToLedUpdate = IM_CLOCKS_PER_LED_UPDATE;
        
        /* if it's time to flash the heartbeat LED */
        if (--il->ledUpdatesToHeartbeat == 0) {
            
            /* restart number of LED updates to next heartbeat */
            il->ledUpdatesToHeartbeat = IM_LED_UPDATES_PER_HEARTBEAT;
            
            /* turn heartbeat LED on */
            if (!ImLedPriv.overrideMode) LED_P(OUT) &= ~HEARTBEAT_LED;
    
            /* make ImPeriodicLedTasks() leave LED on for one update period */
            ImLedPriv.ledsOffForNextUpdate &= ~HEARTBEAT_LED;
        }
        
        /* If desired, check if the TX buffer on the MICS chip is empty, and
         * if so, automatically turn off the 400 MHz TX LED.
         */
        if (il->flags & IM_TX_400_LED_AUTO_OFF) {
            if (MicsRead(TXBUFF_USED) == 0) ImTx400LedOff();
        }
        /* If desired, check if the RX buffer on the MICS chip is empty, and
         * if so, automatically turn off the 400 MHz RX LED.
         */
        if (il->flags & IM_RX_400_LED_AUTO_OFF) {
            if (MicsRead(RXBUFF_USED) == 0) ImRx400LedOff();
        }
        
        /* If LED override is off, set the output pin for each LED we want to
         * turn off during this update, then save the LED's we want the next
         * update to turn off. This is done to wait one update period before
         * turning off the LED's, to ensure they'll remain on long enough to
         * be seen.
         */
        if (!il->overrideMode) LED_P(OUT) |= il->ledsOffForNextUpdate;
        il->ledsOffForNextUpdate = il->ledsOff;
        
        /* if override mode is set to flash all LED's, do so */
        if (il->overrideMode == IM_FLASH_ALL_LEDS) {
            INVERT_BITS(LED_P(OUT), IM_LEDS);
        }
    }
}
