/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Adp/General.h"              /* UINT, BOOL, TRUE/FALSE, ... */
#include "Adp/Build/Build.h"          /* UD16, UD32, ... */  
#include "Adp/AnyBoard/SysTimerLib.h" /* StElapsedMs(), ... */ 
#include "Adk/AnyMezz/MicsHw.h"       /* ADC_INPUT_EXT_RSSI, ... */
#include "Adk/AnyMezz/MicsLib.h"      /* public include for MicsLib */
 
/* local includes that are private to the MICS library */
#include "MicsPriv.h"  /* MicsPriv, MicsErr(), ... */

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/**************************************************************************
 * Function Prototypes
 */
 
/* Enable/disable external RSSI. This configures the ZL7010X and the external
 * hardware as needed to perform RSSI measurements using an external ADC. Note
 * this does not enable/disable the receiver in the ZL7010X, so that must be 
 * done separately before taking RSSI samples. When the ZL7010X is idle, its
 * receiver can be enabled by setting RF_GENENABLES = RX_RF | RX_IF | SYNTH
 * and waiting MICS_LONG_SYNTH_LOCK_TIME_US for the synthesizer to lock. When
 * the ZL7010X is active, RSSI samples should only be taken when chip enters
 * receive mode on its own.
 * 
 * Note there is no external RSSI hardware on an implant, but this function can
 * still be called to route the RF signals in the ZL7010X to its TESTIO[5,6]
 * pins for monitoring & measurement purposes (test & evaluation).
 */
BOOL
MicsEnabExtRssi(BOOL enab)
{
    UINT tioEnabMask;
    
    /* Build mask for the TESTIO pins required for external RSSI (to enable
     * and disable internal pull-downs). This includes the TESTIO output pin(s)
     * required by the external RSSI circuitry (see TEST_IO_EXT_RSSI_BUF_ENAB),
     * the internal porting of RF signals to TESTIO[5,6] (TEST_IO_RF_ENAB), and
     * the ADC input pin for the external RSSI (so the internal ADC in the MICS
     * chip can be used to sample the external RSSI signal, if desired).
     */
    tioEnabMask = TEST_IO_EXT_RSSI_BUF_ENAB | TEST_IO_RF_ENAB;
    switch(ADC_INPUT_EXT_RSSI) {
    case ADC_INPUT_TEST_IO_1: tioEnabMask |= TEST_IO_1_ENAB; break;
    case ADC_INPUT_TEST_IO_2: tioEnabMask |= TEST_IO_2_ENAB; break;
    case ADC_INPUT_TEST_IO_3: tioEnabMask |= TEST_IO_3_ENAB; break;
    case ADC_INPUT_TEST_IO_4: tioEnabMask |= TEST_IO_4_ENAB; break;
    default: break;
    }
    
    /* if external RSSI is currently enabled */
    if (MicsPriv.flags & MICS_EXT_RSSI_ENAB) {
    
        /* if caller wants to disable external RSSI, disable it */
        if (!enab) {
            
            /* disable TESTIO output buf(s) that were enabled for ext RSSI */
            MicsWrite(TESTIOBUFEN, MicsRead(TESTIOBUFEN) &
                ~TEST_IO_EXT_RSSI_BUF_ENAB);
                
            /* disable TESTIO pins that were enabled for external RSSI */
            MicsWrite(TESTIOCLAMP, MicsRead(TESTIOCLAMP) & ~tioEnabMask);
        
            /* if base station, disable external RSSI circuitry */
            #ifdef BASE_STATION
                MICS_WR_RSSI_ENAB(FALSE);
            #endif    
            
            /* disable IF filter outputs to TESTIO[5,6] */
            MicsWrite(RXGENCTRL, MicsRead(RXGENCTRL) & ~RX_EXT_RSSI);
        
            /* clear flag to indicate external RSSI is disabled */
            MicsPriv.flags &= ~MICS_EXT_RSSI_ENAB;
        }
        
        /* indicate the external RSSI was previously enabled */
        return(TRUE);
    
    } else { /* else, external RSSI is currently disabled */       
        
        /* if caller wants to enable external RSSI, enable it */
        if (enab) {
            
            /* enable TESTIO output buf(s) required for external RSSI */
            MicsWrite(TESTIOBUFEN, MicsRead(TESTIOBUFEN) |
                TEST_IO_EXT_RSSI_BUF_ENAB);
                
            /* enable TESTIO pins required for external RSSI */
            MicsWrite(TESTIOCLAMP, MicsRead(TESTIOCLAMP) | tioEnabMask);
                
            /* if base station, enable external RSSI circuitry */
            #ifdef BASE_STATION
                MICS_WR_RSSI_ENAB(TRUE);
            #endif    
            
            /* enable IF filter outputs to TESTIO[5,6] for external RSSI */
            MicsWrite(RXGENCTRL, MicsRead(RXGENCTRL) | RX_EXT_RSSI);
        
            /* set flag to indicate external RSSI is enabled */
            MicsPriv.flags |= MICS_EXT_RSSI_ENAB;
        }
            
        /* indicate the external RSSI was previously disabled */
        return(FALSE);
    }
}
