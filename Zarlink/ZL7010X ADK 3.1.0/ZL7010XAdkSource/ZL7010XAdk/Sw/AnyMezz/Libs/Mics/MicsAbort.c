/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Adp/General.h"              /* BOOL, ... */
#include "Adp/Build/Build.h"          /* UD16, ... */
#include "Adp/AnyBoard/SysTimerLib.h" /* StElapsedMs(), ... */
#include "Adk/AnyMezz/MicsHw.h"       /* INITCOM, ... */
#include "Adk/AnyMezz/MicsLib.h"      /* public include for MicsLib */

/* local includes that are private to the MICS library */
#include "MicsPriv.h"  /* MicsPriv, MicsErr(), MicsMacCmd(), ... */
 
/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/**************************************************************************
 * Function Prototypes
 */
 
/**************************************************************************
 * Abort the current ZL7010X operation, but keep the ZL7010X awake (to abort
 * and put the ZL7010X to sleep, see MicsSleep()). For the "options" argument,
 * see MICS_FLUSH, etc. in "ZL7010XAdk\Sw\Includes\Adk\AnyMezz\MicsLib.h".
 * Note this should never be called in an ISR (to avoid race conditions).
 * 
 * Note for a ZL70101 in implant mode (INIT_COM.IBS_FLAG = 0), the application
 * should avoid calling MicsAbort(), because if no packets are received during
 * MicsAbort() (because the RF link is lost, or the base station aborts the
 * session), the ZL70101 will never execute the "abort to idle" command,
 * causing MicsAbort() to time out. Thus, for a ZL70101 in implant mode,
 * it's best to call MicsSleep() to abort an operation, not MicsAbort().
 *
 * Note for a ZL70101 in base station mode (INIT_COM.IBS_FLAG = 1), if no
 * packets are received during MicsAbort(), the ZL70101 won't execute the
 * "abort to idle" until its resend timer expires, which can be up to ~10 ms
 * for the default resend time. If that is a concern, the MICS_SET_MIN_RESEND
 * option can be specified so the ZL70101 will execute the "abort to idle"
 * command sooner.
 *
 * Note if MicsAbort() is called while the resend timer is disabled, the
 * MICS_SET_MIN_RESEND option should be specified. Otherwise, the ZL7010X
 * might never execute the "abort to idle" command, causing MicsAbort() to
 * time out. This applies to all ZL7010X revisions (ZL70101, ZL70102, and
 * ZL70103) in both implant mode (INITCOM_IBS_FLAG = 0) and base station mode
 * (INITCOM_IBS_FLAG = 1).
 * 
 * Note MicsAbort() might not return right away, because it waits for the
 * ZL7010X to execute the "abort to idle" command, and depending on the current
 * operation and configuration, the ZL7010X might not execute the "abort to
 * idle" command right away. However, it should always be well under 1 second,
 * so if the ZL7010X doesn't execute the command within 1 second, MicsAbort()
 * will time out and return.
 *
 * Note when the MICS_SET_MIN_RESEND option is specified, MicsAbort() will not
 * restore the resend time afterwards, so the caller must restore it if needed
 * before starting another ZL7010X operation.
 */
void
MicsAbort(UINT options)
{
    UD16 startMs;
    
    /* Clear IRQ_RADIOREADY in the IRQ status latch (so we can wait for the ISR
     * to set it again below), clear bits that are no longer needed in INITCOM,
     * then execute ABORT_LINK and wait for it to finish (IRQ_RADIOREADY). The
     * IBS_FLAG is set in INITCOM so the ZL7010X will return to the "check
     * command idle" state when it executes ABORT_LINK (note this applies to an
     * implant as well as a base station). When the ZL7010DX finishes executing
     * ABORT_LINK, it will assert IRQ_RADIOREADY, at which point the ISR must
     * set the IRQ_RADIOREADY bit in MicsPub.irqStat2Latch.
     */
    MicsPub.irqStat2Latch &= ~IRQ_RADIOREADY;
    MicsWrite(INITCOM, IBS_FLAG);
    MicsWrite(MAC_CTRL, ABORT_LINK);
    /*
     * If the caller specified MICS_SET_MIN_RESEND, set the minimum resend
     * time (see the function prolog for more information). Note this is done
     * after setting MAC_CTRL.ABORT_LINK in case the resend timer was disabled,
     * to ensure the ABORT_LINK is the only thing that will happen when the
     * resend timer expires (i.e. to ensure the ZL70101 won't try to transmit
     * a 400 MHz packet).
     */
    if (options & MICS_SET_MIN_RESEND) MicsWrite(RESENDTIME, RESENDTIME_MIN);
    /*
     * Wait for ZL7010X to finish executing ABORT_LINK (assert IRQ_RADIOREADY).
     */
    for(startMs = StMs();;) {
            
        /* if ZL7010x asserted IRQ_RADIOREADY, stop (abort finished) */
        if (MicsPub.irqStat2Latch & IRQ_RADIOREADY) break;
                
        /* If it's been too long, set error and stop (shouldn't happen).
         * Depending on the current operation and configuration, the ZL7010X
         * might not execute the "abort to idle" command right away, but it
         * should always be well under 1 second.
         */
        if (StElapsedMs(startMs, 1000)) {
            MicsErr(MICS_LIB_ERR_ABORT_LINK_TIMEOUT, NULL);
            break;
        }
    }
    
    /* clear ABORT_LINK bit in MAC_CTRL (it's not self-clearing) */
    MicsWrite(MAC_CTRL, 0);
    
    /* if specified, flush TX buf on local ZL7010X */
    if (options & MICS_FLUSH_TX) MicsMacCmd(FLUSH_LOCAL_TX_BUF);
    
    /* If specified, flush the RX buffer on the local ZL7010X. This also
     * clears IRQ_RXNOTEMPTY because the flush won't clear it. If we didn't
     * clear it here, the ZL7010X would assert it again the next time it's
     * enabled, even if the RX buffer is still empty. This wouldn't hurt
     * anything if the software always checks to make sure there's data in
     * the RX buffer, but it's still best not to assert the interrupt after
     * the buffer is flushed.
     */
    if (options & MICS_FLUSH_RX) {
        
        MicsMacCmd(FLUSH_LOCAL_RX_BUF);
        MicsWrite(IRQ_RAWSTATUS1, ~IRQ_RXNOTEMPTY);
    }
    
    /* Clear the IRQ_CLOST interrupts in the interrupt status latches in case
     * they're set (so they'll be ready for the software to reference during
     * the next ZL7010X operation, if needed).
     */
    MicsPub.irqStat1Latch &= ~IRQ_CLOST;
    MicsPub.irqAuxStatLatch &= ~(IRQ_WDOG | IRQ_HKABORTLINK);

    /* If base station, clear MicsPriv.hkPageOnImplant to tell MicsReadR(),
     * MicsWriteR(), and MicsSetHkPageonImplant() that MicsPriv.hkPageOnImplant
     * is no longer known and needs to be initialized (for ZL70102/103 paging).
     */
    #ifdef BASE_STATION
        MicsPriv.hkPageOnImplant = 0;
    #endif
}
