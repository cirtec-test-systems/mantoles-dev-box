/**************************************************************************
 * This is the private include file for the MICS library. This should not
 * be included by anything outside of the MICS library.
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
/* ensure this file is only included once */
#ifndef MicsPriv_h
#define MicsPriv_h

#include "Standard/String.h"     /* NULL (for MicsErr() args) */
#include "Standard/StdArg.h"     /* va_list (for MicsErr() args) */
#include "Adp/General.h"         /* UINT, RSTAT, BOOL, ... */
#include "Adp/Build/Build.h"     /* UD8, UD16, ... */
#include "Adk/AnyMezz/MicsLib.h" /* public include for MicsLib */

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* structure of private data for MICS library */
typedef struct {
    
    /* Various flags for the MICS interface. For bit definitions, see
     * MICS_ADC_ENAB, MICS_EXT_RSSI_ENAB, etc. This is declared volatile to
     * prevent the compiler from assuming it won't change during a section of
     * code, since it may be changed by MICS interrupts.
     */
    volatile UD8 flags;
    
    /* These are used to track which register page is currently selected in the
     * ZL7010X, and the current value of the INTERFACE_MODE register. MicsRead()
     * and MicsWrite() maintain these through MicsSetPage() so they don't have
     * to select the page every time they access a register in the ZL7010X.
     * MicsInit(), MicsSleep(), and MicsReset() set the page field to 0 to
     * tell MicsRead(), MicsWrite, and MicsSetPage() that these are not yet
     * known and need to be initialized.
     */
    UD8 page;
    UD8 iMode;
    
    /* For a base station, this is used to track the current HK page selected
     * on the remote implant (for implants with ZL70102/103). MicsReadR() and
     * MicsWriteR() maintain this so they don't have to select the HK page
     * each time they access a register on the remote implant. MicsInit(),
     * MicsAbort(), MicsReset(), and MicsSleep() set this to 0 to tell
     * MicsReadR(), MicsWriteR(), and MicsSetHkPageOnImplant() that it's
     * not yet known and needs to be initialized.
     */
    #ifdef BASE_STATION
    UD8 hkPageOnImplant;
    #endif
    
    /* For a base station, this indicates the revision of the MICS chip on the
     * remote implant. The revisions are defined in "Adk/AnyMezz/MicsHw.h"
     * (ZL70100, ZL70101, ZL70102, or ZL70103). By default, MicsInit()
     * initializes this to be the same as the MICS revision on the base,
     * but MicsRevOnImplant() can be called to change it if needed.
     */
    #ifdef BASE_STATION
    UD8 micsRevOnImplant;
    #endif

} MICS_PRIV;
/*
 * Defines for bits in MICS_PRIV.flags:
 * 
 * MICS_ADC_ENAB:
 *     True if the ADC on the local MICS chip is enabled (see MicsEnabAdc()).
 * MICS_EXT_RSSI_ENAB:
 *     True if external RSSI is enabled (see MicsEnabExtRssi()). Note there is
 *     no external RSSI hardware on an implant, but this can still be enabled
 *     to route the RF signals in the MICS chip to its TESTIO[5,6] pins for
 *     monitoring & measurement purposes (test & evaluation).
 */
#define MICS_ADC_ENAB            (1 << 0)
#define MICS_EXT_RSSI_ENAB       (1 << 1)

/**************************************************************************
 * Global Declarations
 */
 
/* private data for MICS library */
extern MICS_PRIV  MicsPriv;
 
/**************************************************************************
 * External Function Prototypes
 */
 
/* Declarations for private MICS library functions. These should not be
 * called by anything outside of the MICS library.
 */
 
/* functions private to the MICS library */
extern void MicsErr(UD16 micsLibErrCode, const char *argFormats, ...);
extern void MicsMacCmd(UINT macCmd);
extern void MicsSetPage(UINT page);
#ifdef BASE_STATION
    extern RSTAT MicsSetHkPageOnImplant(UINT hkPageOnImplant, UD16 timeoutMs);
#endif    

#endif /* ensure this file is only included once */
