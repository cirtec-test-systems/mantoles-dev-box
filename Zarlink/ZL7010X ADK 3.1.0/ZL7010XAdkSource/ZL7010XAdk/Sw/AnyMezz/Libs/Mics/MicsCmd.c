/**************************************************************************
 * This file contains functions for various MICS operations and commands.
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Adp/General.h"              /* UINT, ... */
#include "Adp/Build/Build.h"          /* UD16, ... */
#include "Adp/AnyBoard/SysTimerLib.h" /* StElapsedMs(), ... */ 
#include "Adk/AnyMezz/MicsHw.h"       /* MAC_CTRL, ... */
#include "Adk/AnyMezz/MicsLib.h"      /* public include for MicsLib */

/* local includes that are private to the MICS library */
#include "MicsPriv.h"  /* MicsPriv, MicsErr(), ... */

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/**************************************************************************
 * Function Prototypes
 */
static __inline RSTAT MicsMacCmdDef(UINT macCmd, BOOL remote);
static __inline RSTAT MicsCalcCrcDef(BOOL remote);
/**/
#ifdef BASE_STATION
    static RSTAT MicsMacCmdR(UINT macCmd, BOOL remote);
#endif    
 
/**************************************************************************
 * Tell the ZL7010X to execute a MAC_CTRL command and wait for it to finish.
 * 
 * Note if executing PERFORM_CALS on a ZL70102/103 with the CAL_400_MZ_TX_102
 * and/or CAL_400_MHZ_RX_102 calibrations selected, the PERFORM_CALS could
 * take up to 13 seconds depending on how these calibrations are configured.
 * For this reason, MicsMacCmd() uses a 15 second timeout when executing
 * PERFORM_CALS on a ZL70102/103. In all other cases, MicsMacCmd() uses a
 * 100 ms timeout. If a command times out, MicsMacCmd() will set the global
 * error and return (shouldn't happen).
 * 
 * Note this function should not be used to execute ABORT_LINK or SKIP_CRC
 * because they never assert IRQ_COMMANDDONE and they aren't self-clearing.
 */
void
MicsMacCmd(UINT macCmd)
{
    #ifdef BASE_STATION
        (void)MicsMacCmdR(macCmd, FALSE); /* should never fail */
    #else
        (void)MicsMacCmdDef(macCmd, FALSE); /* should never fail */
    #endif
}

#ifdef BASE_STATION
static RSTAT
MicsMacCmdR(UINT macCmd, BOOL remote)
{
    return(MicsMacCmdDef(macCmd, remote));
}
#endif

static __inline RSTAT
MicsMacCmdDef(UINT macCmd, BOOL remote)
{
    UD16 startMs, timeoutMs;
    int val;
        
    /* if executing command on remote ZL7010X */
    if (remote) {
        
        /* start command on remote ZL7010X */
        if (MicsWriteR(MAC_CTRL, macCmd, TRUE, 250)) return(-1);
        
    } else {    
        
        /* clear IRQ_COMMANDDONE bit in IRQ status latch */
        MicsPub.irqStat2Latch &= ~IRQ_COMMANDDONE;
    
        /* start command */
        MicsWrite(MAC_CTRL, macCmd);
    }
    
    /* If executing PERFORM_CALS on a ZL70102/103, use 15 second timeout (if
     * CAL_400_MZ_TX_102 and/or CAL_400_MHZ_RX_102 are selected, PERFORM_CALS
     * could take up to 13 seconds). Otherwise, use 100 ms timeout.
     */
    if ((MICS_REV >= ZL70102) && (macCmd & PERFORM_CALS)) {
        timeoutMs = 15000;
    } else {        
        timeoutMs = 100;
    }
        
    /* wait for command to finish */
    for(startMs = StMs();;) {
        
        /* if executing command on remote ZL7010X */
        if (remote) {
            
            /* if remote ZL7010X cleared bit in MAC_CTRL, stop waiting (done) */
            if ((val = MicsReadR(MAC_CTRL, TRUE, 250)) < 0) return(-1);
            if (val == 0) break;
            
        } else {
            
            /* if ZL7010X asserted IRQ_COMMANDDONE, stop waiting (done) */
            if (MicsPub.irqStat2Latch & IRQ_COMMANDDONE) break;
        }
            
        /* if it's been too long, set error and stop (shouldn't happen) */
        if (StElapsedMs(startMs, timeoutMs)) {
            MicsErr(MICS_LIB_ERR_MAC_CMD_TIMEOUT, "0x%02x", macCmd);
            break;
        }
    }
    
    return(0);
}

/* This tells the ZL7010X to copy registers to its wakeup stack so they'll be
 * preserved while it's sleeping, and to recalculate its CRC. For registers
 * included in the wakeup stack, see the ZL7010X Design Manual.
 * 
 * Note on an implant, if the COPY_REGS command is executed during a session,
 * and the RF link is still good, the COPY_REGS will work. However, if the base
 * has aborted the session, or the RF link has been lost, COPY_REGS will time
 * out (due to a ZL7010X bug). Thus, if COPY_REGS is executed during a session,
 * there's always a chance it won't work. Note it doesn't matter if COPY_REGS
 * is executed before or after the ZL7010X asserts IRQ_WDOG - it will still
 * time out either way.
 * 
 * If the implant has a ZL70102/103, this can be avoided altogether by waiting
 * until the session ends (after IRQ_WDOG), calling MicsAbort() to return the
 * ZL70102/103 to the idle state, then executing COPY_REGS. In that case, the
 * COPY_REGS will always work.
 * 
 * If the implant has a ZL70101, MicsAbort() won't work after the session ends,
 * so there's no way to avoid the COPY_REGS issue altogether. To minimize the
 * probability, it's best to execute COPY_REGS when there's a good chance the
 * RF link is still good. For example, whenever the base modifies something on
 * the implant that the ZL70101 includes in its wakeup stack, the base can
 * also execute COPY_REGS on the implant (see MicsCopyRegsR()). That way, the
 * COPY_REGS will only occur when the RF link is good.
 */
void
MicsCopyRegs(void)
{
    /* copy registers to the wakeup stack, then recalc CRC */
    MicsMacCmd(COPY_REGS);
    MicsCalcCrc();
}

/* If "remote" is true, this tells the ZL7010X on the remote implant to copy
 * registers to its wakeup stack so they'll be preserved while it's sleeping,
 * and to recalculate its CRC. Note for this to work, the base must be in
 * session with the implant. If "remote" is false, this does the same thing
 * as MicsCopyRegs() (see MicsCopyRegs() for details).
 * 
 * For registers included in the wakeup stack, see the ZL7010X Design Manual.
 * 
 * This returns 0 if successful, non-zero if it fails.
 */
#ifdef BASE_STATION
RSTAT
MicsCopyRegsR(BOOL remote)
{
    /* copy registers to the wakeup stack, then recalc CRC */
    if (MicsMacCmdR(COPY_REGS, remote)) return(-1);
    if (MicsCalcCrcR(remote)) return(-1);
    
    return(0);
}
#endif

void
MicsCalcCrc(void)
{
    #ifdef BASE_STATION
        (void)MicsCalcCrcR(FALSE); /* should never fail */
    #else
        (void)MicsCalcCrcDef(FALSE); /* should never fail */
    #endif
}

#ifdef BASE_STATION
RSTAT
MicsCalcCrcR(BOOL remote)
{
    return(MicsCalcCrcDef(remote));
}
#endif

static __inline RSTAT
MicsCalcCrcDef(BOOL remote)
{
    UINT countdown;
    int val;
    
    /* start CRC calc */
    if (remote) {
        if (MicsWriteR(CRCCTRL, CALC_CRC, remote, 250)) return(-1);
    } else {
        MicsWrite(CRCCTRL, CALC_CRC);
    }
    
    /* wait for CRC calc to finish */
    for(countdown = 20;;) {
        
        /* wait 1 ms */
        StDelayUs(1000);
        
        /* read CRCCTRL */
        if (remote) {
            if ((val = MicsReadR(CRCCTRL, TRUE, 250)) < 0) return(-1);
        } else {
            val = MicsRead(CRCCTRL); 
        }
        
        /* if CRC calc is done, stop */
        if (val & CALC_CRC_DONE) break;
        
        /* if it's been too long, set error and stop (shouldn't happen) */
        if (--countdown == 0) {
            MicsErr(MICS_LIB_ERR_CALC_CRC_TIMEOUT, NULL);
            break;
        }
    }
    /* clear CRCCTRL.CALC_CRC (not self-clearing) */
    if (remote) {
        if (MicsWriteR(CRCCTRL, 0, remote, 250)) return(-1);
    } else {        
        MicsWrite(CRCCTRL, 0);
    }
    
    return(0);
}
