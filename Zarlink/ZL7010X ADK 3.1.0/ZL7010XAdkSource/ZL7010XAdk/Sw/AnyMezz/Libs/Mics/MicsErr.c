/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Standard/StdArg.h"         /* va_list (MicsErr() args) */
#include "Adp/Build/Build.h"         /* UD16, ... */
#include "Adp/AnyBoard/ErrLib.h"     /* ErrSetVa() */
#include "Adk/AnyMezz/MicsLib.h"     /* public include for MicsLib */

/* local includes that are private to the MICS library */
#include "MicsPriv.h"  /* MicsPriv, MicsErr(), ... */

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/* constant global string for MICS library error group */
const char *MicsLibErr = MICS_LIB_ERR;
 
/**************************************************************************
 * Function Prototypes
 */
 
/**************************************************************************
 * Set global error information for a MICS library error.
 */
void
MicsErr(UD16 micsLibErrCode, const char *argFormats, ...)
{
    va_list va;

    /* init variable arguments list */
    va_start(va, argFormats);

    ErrSetVa(MicsLibErr, micsLibErrCode, argFormats, va);

    /* clean up variable args list */
    va_end(va);
}
