/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Adp/General.h"              /* UINT, BOOL, ... */
#include "Adp/Build/Build.h"          /* UD16, _disable_interrupts(), ... */
#include "Adp/AnyBoard/LocalSpiLib.h" /* LSpiRead(), ... */
#include "Adp/AnyBoard/SysTimerLib.h" /* StElapsedMs(), ... */ 
#include "Adk/AnyMezz/MicsHw.h"       /* MICS_WR_SPI_CS(), ... */
#include "Adk/AnyMezz/MicsLib.h"      /* public include for MicsLib */

/* local includes that are private to the MICS library */
#include "MicsPriv.h"  /* MicsPriv, MicsErr(), ... */

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/**************************************************************************
 * Function Prototypes
 */
 
/**************************************************************************
 * Read a register in the MICS chip.
 */
UINT
MicsRead(UINT regAddr)
{
    UINT gie, val, page;
    
    /* Save current GIE setting (general interrupt enable), then disable all
     * interrupts so no ISR will try to use the SPI bus.
     */
    gie = _get_SR_register() & GIE;
    _disable_interrupts();
    
    /* get the register page (also detect 0x8000 for backwards compatibility) */
    if (regAddr & (MICS_PAGE_2 | 0x8000)) {
        regAddr &= 0x7F;
        page = 2;
    } else {
        page = 1;
    }
    /* If the desired page isn't already selected, and we're not reading
     * INTERFACE_MODE, select the page. There's no need to select the page
     * for INTERFACE_MODE because it exists in both pages, and could cause
     * infinite nesting of MicsRead() and MicsWrite() through MicsSetPage().
     */
    if ((page != MicsPriv.page) && (regAddr != INTERFACE_MODE)) {
        MicsSetPage(page);
    }
    
    /* select MICS chip on SPI bus */
    MICS_WR_SPI_CS(MICS_SPI_ON);
    
    /* transmit register address and receive register value */
    val = LSpiRead(MICS_READ | regAddr);
        
    /* de-select MICS chip on SPI bus */
    MICS_WR_SPI_CS(MICS_SPI_OFF);
    
    /* if interrupts were enabled, re-enable them */
    _bis_SR_register(gie);
    
    /* return register value received from MICS chip */
    return(val);
}

int
MicsReadR(UINT regAddr, BOOL remote, UD16 timeoutMs)
{
    UD16 startMs;
    #ifdef BASE_STATION
        UINT hkPageOnImplant;
    #endif
    
    /* if not reading a remote register, call function to read local */
    if (!remote) return(MicsRead(regAddr));
    
    /* for backwards compatibility (used to set 0x8000 for page 2 registers) */
    if (regAddr & 0x8000) {
        regAddr &= ~0x8000;
        regAddr |= MICS_PAGE_2;
    }
    
    /* if implant */
    #ifdef IMPLANT
    
        /* if trying to access a register in page 2 on remote base, fail */
        if (regAddr & MICS_PAGE_2) {
            MicsErr(MICS_LIB_ERR_ATTEMPT_TO_ACCESS_REMOTE_PAGE_2_VIA_HK, NULL);
            return(-1);
        }
        
    #else /* base station */
    
        /* if the remote implant has a ZL70101 */
        if (MicsPriv.micsRevOnImplant <= ZL70101) {
            
            /* if reading page 2 on remote ZL70101, fail (not supported) */
            if (regAddr & MICS_PAGE_2) {
                MicsErr(MICS_LIB_ERR_ATTEMPT_TO_ACCESS_REMOTE_PAGE_2_VIA_HK, NULL);
                return(-1);
            }
            
        } else { /* else, the remote implant has a ZL70102/103 */
            
            /* get the register page to access on the remote implant */
            hkPageOnImplant = (regAddr & MICS_PAGE_2) ? 2 : 1;
            regAddr &= 0x7F;
            
            /* If not reading HK_MODE_102 or MICS_REVISION on the remote
             * implant, call MicsSetHkPageOnImplant() to set the register
             * page to access on the remote implant. There's no need to call
             * MicsSetHkPageOnImplant() for HK_MODE_102 or MICS_REVISION
             * because they exist in both pages. MicsSetHkPageOnImplant()
             * isn't called for HK_MODE_102 because that could cause infinite
             * nesting of MicsReadR() through MicsSetHkPageOnImplant(), and
             * MicsSetHkPageOnImplant() isn't called for MICS_REVISION so the
             * base station can read MICS_REVISION on the remote implant to
             * confirm the session is still alive without incurring the
             * overhead of MicsSetHkPageOnImplant().
             *
             * Note that if the desired page is already selected on the
             * remote implant, MicsSetHkPageOnImplant() doesn't do anything.
             */
            if ((regAddr != HK_MODE_102) && (regAddr != MICS_REVISION)) {

                if (MicsSetHkPageOnImplant(hkPageOnImplant, timeoutMs)) {
                    return(-1);
                }
            }
        }
        
    #endif /* base station */        
        
    /* clear IRQ_HKREMOTEDONE bit in IRQ status latch */
    MicsPub.irqStat1Latch &= ~IRQ_HKREMOTEDONE;
            
    /* tell local MICS chip to read specified register on remote chip */
    #if MICS_REV == ZL70101 
        /* workaround for ZL70101 issue */
        if (MicsWriteHkTxAddr(MICS_READ | regAddr, timeoutMs)) return(-1);
    #else        
        MicsWrite(HK_TXADDR, MICS_READ | regAddr);
    #endif

    /* If the application defined a hook to call when starting an HK TX
     * (e.g. to turn on an LED), call it.
     */
    #ifdef MICS_APP_HOOK_FOR_START_HK_TX
        MICS_APP_HOOK_FOR_START_HK_TX();
    #endif
    
    /* wait for local MICS chip to receive reply from remote MICS chip */
    startMs = StMs();
    while((MicsPub.irqStat1Latch & IRQ_HKREMOTEDONE) == 0) {
        
        if ((StElapsedMs(startMs, timeoutMs)) || (MicsPub.irqStat1Latch & IRQ_CLOST)) {
            MicsErr(MICS_LIB_ERR_REMOTE_HK_TIMEOUT, NULL);
            return(-1);
        }
    }
    
    /* return the remote read reply (the remote register value) */
    return(MicsRead(HK_TXREPLY));
}
 
void
MicsReadRxBuf(void *buf, UINT len)
{
    UINT gie;
    
    /* Save current GIE setting (general interrupt enable), then disable all
     * interrupts so no ISR will try to use the SPI bus.
     */
    gie = _get_SR_register() & GIE;
    _disable_interrupts();
    
    /* select MICS chip on SPI bus */
    MICS_WR_SPI_CS(MICS_SPI_ON);
    
    /* transmit MICS RX buf address and receive specified number of bytes */
    LSpiReadMulti(MICS_READ | TXRXBUFF, buf, len);
    
    /* de-select MICS chip on SPI bus */
    MICS_WR_SPI_CS(MICS_SPI_OFF);
    
    /* if interrupts were enabled, re-enable them */
    _bis_SR_register(gie);
}
