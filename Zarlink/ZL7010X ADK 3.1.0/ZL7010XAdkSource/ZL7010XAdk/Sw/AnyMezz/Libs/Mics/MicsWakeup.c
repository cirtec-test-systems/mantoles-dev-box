/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Adp/General.h"              /* UINT, RSTAT, BOOL, WR_BITS(), ... */
#include "Adp/Build/Build.h"          /* UD16, BASE_STATION, ... */
#include "Adp/AnyBoard/SysTimerLib.h" /* StElapsedMs(), ... */ 
#include "Adk/AnyMezz/MicsHw.h"       /* INITCOM, MICS_WR_IBS(), ... */
#include "Adk/AnyMezz/MicsLib.h"      /* public include for MicsLib */

/* local includes that are private to the MICS library */
#include "MicsPriv.h"  /* MicsPriv, MicsErr(), ... */

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/**************************************************************************
 * Function Prototypes
 */
 
static void MicsPrepForSleepOrReset(void);
 
/**************************************************************************
 * Wake the ZL7010X MICS chip (no affect if it's already awake). Note this
 * should never be called in an ISR (to avoid race conditions, and because it
 * depends on the IRQ_RADIOREADY interrupt from the chip).
 * 
 * Note this references MicsPub.irqStat2Latch.IRQ_RADIOREADY to detect when the
 * ZL7010X is awake. MicsSleep() clears this bit so MicsWakeup() can poll it to
 * detect when the chip finishes waking up. MicsAbort() and MicsReset() also
 * clear this bit, but only temporarily (the bit is set again before they
 * return). If anything else clears this bit while the chip is awake, it should
 * set the bit again when done.
 */
void
MicsWakeup(void)
{
    UD16 startMs;
    
    #ifdef BASE_STATION
        /* set MICS IBS input so if ZL7010X is sleeping, it will wake up */
        MICS_WR_IBS(TRUE);
    #else
        /* set MICS WU_EN input so if ZL7010X is sleeping, it will wake up */
        MICS_WR_WU_EN(TRUE);
    #endif

    /* wait for ZL7010X to wakeup */
    for(startMs = StMs();;) {
        
        /* if ZL7010X is awake (asserted IRQ_RADIOREADY), stop */
        if (MicsPub.irqStat2Latch & IRQ_RADIOREADY) break;
        
        /* if it's been too long, set error & stop waiting (shouldn't happen) */
        if (StElapsedMs(startMs, 250)) {
            MicsErr(MICS_LIB_ERR_WAKEUP_TIMEOUT, NULL);
            break;
        }
    }
    
    #ifdef IMPLANT
        /* clear WU_EN so the ZL7010X won't re-wake the next time it sleeps */
        MICS_WR_WU_EN(FALSE);
    #endif    
}

/* Put the ZL7010X to sleep. This should never be called in an ISR (to
 * avoid race conditions). This disables all interrupts in the ZL7010X to
 * ensure no ZL7010X ISR will try to access the ZL7010X until the next time
 * it's awakened (i.e. the next IRQ_RADIOREADY). Note before the application
 * calls MicsSleep(), it must also ensure nothing else will try to access the
 * ZL7010X until the next time it's awakened.
 * 
 * Note this clears MicsPub.irqStat2Latch.IRQ_RADIOREADY so the next time
 * MicsWakeup() is called, it can poll it to detect when the chip finishes
 * waking up.
 */
void
MicsSleep(void)
{
    /* Prepare to put the ZL7010X to sleep. Note this disables all interrupts
     * in the ZL7010X (see explanation in function prolog).
     */
    MicsPrepForSleepOrReset();

    #ifdef BASE_STATION
        /* clear IBS input to ZL7010X so it will go to sleep */
        MICS_WR_IBS(FALSE);
    #else
        /* Ensure WU_EN is low so the ZL7010X won't keep waking up. Note if
         * MicsWakeup() was used to wake the chip, WU_EN will already be low
         * since MicsWakeup() clears it after the chip wakes up, but it's
         * harmless to clear it again.
         */
        MICS_WR_WU_EN(FALSE);
        /*
         * In INITCOM, clear IBS_FLAG so the ZL7010X will go to sleep when it
         * executes ABORT_LINK, and clear all other bits to tell it to stop any
         * other operations. Next, tell the ZL7010X to execute ABORT_LINK. Note
         * the chip will go to sleep right away. There's no need to clear the
         * ABORT_LINK bit afterwards because it's cleared when it goes to sleep.
         */
        MicsWrite(INITCOM, 0);
        MicsWrite(MAC_CTRL, ABORT_LINK);
    #endif
}

/* Reset the ZL7010X. When this is called, the ZL7010X must be awake, and
 * it will re-wake the ZL7010X before returning. This should never be called
 * in an ISR (to avoid race conditions). This disables all interrupts in the
 * ZL7010X to ensure no ZL7010X ISR will try to access the ZL7010X until it
 * re-wakes after the reset (i.e. the next IRQ_RADIOREADY). Note before the
 * application calls MicsReset(), it must also ensure nothing else will try
 * to access the ZL7010X until it re-wakes after the reset.
 *
 * Note this clears MicsPub.irqStat2Latch.IRQ_RADIOREADY so the next time
 * MicsWakeup() is called, it can poll it to detect when the chip finishes
 * waking up.
 */
void
MicsReset(void)
{
    /* Prepare to reset the ZL7010X. Note this disables all interrupts in the
     * ZL7010X (see explanation in function prolog).
     */
    MicsPrepForSleepOrReset();

    /* initiate ZL7010X reset */
    MicsWrite(CHIP_RESET_ADDRESS, CHIP_RESET);
    
    /* re-wake the ZL7010X (implant) or wait for it to finish resetting (base) */
    MicsWakeup();
}

/* Prepare to put the ZL7010X to sleep or reset it.
 */
static void
MicsPrepForSleepOrReset(void)
{
    /* Disable all interrupts in the ZL7010X to ensure no ZL7010X ISR will
     * try to access the ZL7010X until the next IRQ_RADIOREADY. Note we don't
     * disable the ZL7010X micro-controller interrupt for this because we'd
     * have to re-enable it after initiating the sleep or reset (for
     * IRQ_RADIOREADY), and if we re-enabled it before the ZL7010X
     * actually finished resetting or going to sleep, the ZL7010X might
     * assert some other interrupt.
     */
    MicsWrite(IRQ_ENABLECLEAR1, 0xFF);
    MicsWrite(IRQ_ENABLECLEAR2, 0xFF);

    /* clear flags affected by sleep or reset */
    MicsPriv.flags &= ~(MICS_ADC_ENAB | MICS_EXT_RSSI_ENAB);

    /* Clear MicsPriv.page to tell MicsRead(), MicsWrite(), and MicsSetPage()
     * that MicsPriv.page and MicsPriv.iMode are no longer known and need to be
     * initialized (for ZL7010X paging).
     */
    MicsPriv.page = 0;

    /* If base station, clear MicsPriv.hkPageOnImplant to tell MicsReadR(),
     * MicsWriteR(), and MicsSetHkPageonImplant() that MicsPriv.hkPageOnImplant
     * is no longer known and needs to be initialized (for ZL70102/103 paging).
     */
    #ifdef BASE_STATION
        MicsPriv.hkPageOnImplant = 0;
    #endif

    /* Clear irqStat2Latch.IRQ_RADIOREADY (for MicsWakeup() after sleep or
     * reset), and clear the IRQ_CLOST interrupts in the interrupt status
     * latches in case they're set (so they'll be ready for the software to
     * reference during the next ZL7010X RF operation, if needed).
     */
    MicsPub.irqStat2Latch &= ~IRQ_RADIOREADY;
    MicsPub.irqStat1Latch &= ~IRQ_CLOST;
    MicsPub.irqAuxStatLatch &= ~(IRQ_WDOG | IRQ_HKABORTLINK);
}
