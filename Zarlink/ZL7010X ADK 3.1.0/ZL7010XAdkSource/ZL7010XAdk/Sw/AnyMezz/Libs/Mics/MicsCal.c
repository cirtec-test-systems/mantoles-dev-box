/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Adp/General.h"              /* UINT, RSTAT, BOOL, TRUE/FALSE, ... */
#include "Adp/Build/Build.h"          /* MICS_REV, ... */
#include "Adp/AnyBoard/SysTimerLib.h" /* StDelayUs(), ... */ 
#include "Adk/AnyMezz/MicsHw.h"       /* ZL70101, CALSELECT1_102, ... */
#include "Adk/AnyMezz/MicsLib.h"      /* public include for MicsLib */

/* local includes that are private to the MICS library */
#include "MicsPriv.h"  /* MicsPriv, MicsErr(), ... */

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/**************************************************************************
 * Function Prototypes
 */
 
static RSTAT MicsCal245GHzZeroLev101(BOOL remote);
 
/**************************************************************************
 * This performs the specified calibrations on the local ZL7010X. The "cals"
 * argument selects the calibrations to perform. The lower 8 bits are written
 * to the calselect1 register (ZL70101/102/103), and the upper 8 bits are
 * written to the calselect2 register (ZL70102/103 only).
 * 
 * Note for CAL_400_MHZ_ANT, the calibration uses the current settings for the
 * following registers, so they should be set as desired beforehand:
 * 
 *   - MICS channel: selected via MAC_CHANNEL.
 * 
 *   - PD gain: controlled by TXPD_GAIN_CTRL_102 for the ZL70102/103, and
 *     TXIFAMP[6:4] for the ZL70101.
 * 
 *   - TX power output trimming: controlled by TXRFPWRTUNEANTSET_102 for the
 *     ZL70102/103, and TXRFPWRTUNEANTSET_101 for the ZL70101.
 * 
 *   - ANTMATCHSEL: this selects which capacitor and peak detector are used
 *     (TX, match2, or match2). The default is CAP_AND_PEAK_DET_RF_TX, so if
 *     it's never changed, CAL_400_MHZ_ANT will calibrate the 400 MHz TX.
 */
void
MicsCal(UINT cals)
{
    #ifdef BASE_STATION
    
        /* do specified cals on local ZL7010X (should never fail) */
        (void)MicsCalR(cals, FALSE);
        
    #else /* implant */
    
        /* if using a ZL70101 */
        if (MICS_REV == ZL70101) {
        
            /* if specified 2.45 GHz zero level cal, do separately */
            if (cals& CAL_245_GHZ_ZERO_LEV_101) {
                
                /* do cal on local ZL70101 (should never fail) */
                (void)MicsCal245GHzZeroLev101(FALSE);
            }
            /* if no other cals are specified, return (done) */
            if ((cals &= ~CAL_245_GHZ_ZERO_LEV_101) == 0) return;
            
            /* set remaining calibrations for ZL70101 to run */
            MicsWrite(CALSELECT1_101, cals);
            
        } else {   
            
            /* set calibrations for ZL70102/103 to run */
            MicsWrite(CALSELECT1_102, (UD8)cals);
            MicsWrite(CALSELECT2_102, cals >> 8);
        }
    
        /* run calibrations on ZL7010X */
        MicsMacCmd(PERFORM_CALS);
        
    #endif /* implant */
}

/* This performs the specified calibrations on the local ZL7010X on the base
 * station, or the remote ZL7010X on the implant. The "cals" argument selects
 * the calibrations to perform. The lower 8 bits are written to the calselect1
 * register (ZL70101/102/103), and the upper 8 bits are written to the
 * calselect2 register (ZL70102/103 only).
 * 
 * For notes about CAL_400_MHZ_ANT, see the function prolog for MicsCal().
 */
#ifdef BASE_STATION
RSTAT
MicsCalR(UINT cals, BOOL remote)
{
    /* if running cals on a ZL70101 (local or remote) */
    if ((!remote && (MICS_REV == ZL70101)) ||
        (remote && (MicsPriv.micsRevOnImplant == ZL70101))) {
        
        /* if specified 2.45 GHz zero level cal, do separately */
        if (cals & CAL_245_GHZ_ZERO_LEV_101) {
            if (MicsCal245GHzZeroLev101(remote)) return(-1);
        }
        /* if no other cals are specified, return (done) */
        if ((cals &= ~CAL_245_GHZ_ZERO_LEV_101) == 0) return(0);
        
        /* set remaining calibrations to run on ZL70101 */
        MicsWriteR(CALSELECT1_101, cals, remote, 250);
        
    } else {
        
        /* set calibrations to run on ZL70102/103 */
        MicsWriteR(CALSELECT1_102, (UD8)cals, remote, 250);
        MicsWriteR(CALSELECT2_102, cals >> 8, remote, 250);
    }
    
    /* Run calibrations on ZL7010X. Note for a remote calibration,
     * MicsWriteR() is called to set PERFORM_CALS in the remote MAC_CTRL
     * register, which uses housekeeping. In that case, the remote ZL7010X
     * won't send the housekeeping reply until the calibrations are done, so
     * MicsWriteR() won't return until the calibrations are done.
     */
    if (remote) {
        if (MicsWriteR(MAC_CTRL, PERFORM_CALS, TRUE, 1000)) return(-1);
    } else {
        MicsMacCmd(PERFORM_CALS); /* run cals on local ZL7010X */
    }
    
    return(0);
}
#endif /* #ifdef BASE_STATION */

/* This performs the 2.45 GHz zero level calibration (CAL_245_GHZ_ZERO_LEV) on
 * a ZL70101 (local or remote). The ZL70102/103 doesn't support this calibration.
 */
static RSTAT
MicsCal245GHzZeroLev101(BOOL remote)
{
    UINT i, total, detZeroLev;
    
    /* set calibration for MICS chip to perform (local or remote) */
    MicsWriteR(CALSELECT1_101, CAL_245_GHZ_ZERO_LEV_101, remote, 250);
    
    /* do 20 cals & calc average */
    for(total = 0, i = 0; i < 20; ++i) {
        
        /* if remote, perform calibrations on remote MICS chip */
        if (remote) { 
            if (MicsWriteR(MAC_CTRL, PERFORM_CALS, TRUE, 250)) return(-1);
        } else {
            MicsMacCmd(PERFORM_CALS); /* perform cals on local MICS chip */
        }
        
        /* read calibration result (local or remote) & add to total */
        total += MicsReadR(DETZEROLEV_101, remote, 250);
    }
    
    /* Calculate the average. Note 10 (half of 20) is added to the total before
     * dividing by 20 so the integer result will be as close as possible to the
     * actual average, as shown in the following table:
     * 
     *     Total    Actual Average   (Total+10)/20
     *     00-09      0.00-0.45           0 
     *     10-29      0.50-1.45           1
     *     30-49      1.50-2.45           2
     *     50-69      2.50-3.45           3
     *     ...
     */
    detZeroLev = (total + 10) / 20;
    
    /* write result to DETZEROLEV_101 register (local or remote) */
    if (MicsWriteR(DETZEROLEV_101, detZeroLev, remote, 250)) return(-1);
    
    return(0);
}

/* This performs the multi-channel coarse tune algorithm for the ZL7010X's
 * synthesizer, which finds a SYNTH_CT value that will work for all channels
 * and writes it back to SYNTH_CT with SYNTH_CT.SYNTH_CT_READY = 1. That way,
 * each time the synthesizer is enabled thereafter, the ZL7010X will use that
 * SYNTH_CT value instead of doing its internal synthesizer coarse tune, so
 * the synthesizer will lock within MICS_SHORT_SYNTH_LOCK_TIME_US instead of
 * MICS_LONG_SYNTH_LOCK_TIME_US. Note the following:
 * 
 * - Before this function is called, MicsSynthCtrim() must be called to
 *   trim SYNTH_CTRIM and set SYNTH_CTRIM.SYNTH_CTRIM_READY. Otherwise, each
 *   time the synthesizer is enabled, the ZL7010X will also trim SYNTH_CTRIM,
 *   which will increase the synthesizer startup time by ~1 ms, and that would
 *   cause problems in this function.
 *
 * - This function should only be called when the ZL7010X is idle.
 * 
 * - If you want the ZL7010X to preserve the resulting SYNTH_CT while sleeping
 *   and restore it each time it wakes up, you must copy registers to the
 *   ZL7010X's wakeup stack after this function is called (see MicsCopyRegs()).
 */
void
MicsMultiChanSynthCoarseTune(void)
{
    UINT prevMicsIntEnab, failed;
    UINT chan, prevChan, synthCt;
    int prevChange;

    /* Save current setting for ZL7010X interrupt enable, then disable it
     * (this function doesn't need it for anything, and does its own special
     * handling for IRQ_SYNTHLOCKFAIL).
     */
    prevMicsIntEnab = MICS_INT_ENAB;
    MICS_WR_INT_ENAB(FALSE);

    /* save previous channel */
    prevChan = MicsRead(MAC_CHANNEL);

    /* If SYNTH_CTRIM.SYNTH_CTRIM_READY has not yet been set by calling
     * MicsSynthCtrim(), set error for PC (software error).
     */
    if ((MicsRead(SYNTH_CTRIM) & SYNTH_CTRIM_READY) == 0) {
        MicsErr(MICS_LIB_ERR_SYNTH_CTRIM_READY_NOT_SET, NULL);
        goto cleanup;
    }

    /* Make the ZL7010X do its internal synthesizer coarse tune using channel 3.
     * To do so, select channel 3, clear SYNTH_CT.SYNTH_CT_READY (in case it's
     * set), enable the synthesizer, then wait MICS_LONG_SYNTH_LOCK_TIME_US to
     * ensure the ZL7010X has time to perform the internal coarse tune and set
     * SYNTH_CT.SYNTH_CT_READY. Note this also waits an additional 100 us to
     * ensure we won't read SYNTH_CT just before the ZL7010X finishes and sets
     * SYNTH_CT.SYNTH_CT_READY.
     */
    MicsWrite(MAC_CHANNEL, 3);
    MicsWrite(SYNTH_CT, SYNTH_CT_RESET);
    MicsWrite(RF_GENENABLES, SYNTH);
    StDelayUs(MICS_LONG_SYNTH_LOCK_TIME_US + 100);
    /*
     * Read the resulting SYNTH_CT (with SYNTH_CT.SYNTH_CT_READY = 1) and
     * rewrite it so the ZL7010X won't repeat its internal coarse tune each
     * time the synthesizer is enabled in the future (so it will lock faster).
     * Note this reads and rewrites SYNTH_CT before it disables the synthesizer,
     * because if it disabled the synthesizer first, the ZL7010X would clear
     * SYNTH_CT_READY, after which SYNTH_CT would return an invalid value.
     */
    MicsWrite(SYNTH_CT, synthCt = MicsRead(SYNTH_CT));
    MicsWrite(RF_GENENABLES, 0);
    /*
     * If SYNTH_CT.SYNTH_CT_READY isn't set, set error for PC. This should
     * never happen, and is a catastrophic ZL7010X failure.
     */
    if ((synthCt & SYNTH_CT_READY) == 0) {
        MicsErr(MICS_LIB_ERR_ZL7010X_DID_NOT_SET_SYNTH_CT_READY, NULL);
        goto cleanup;
    }
    
    /* ensure IRQ_SYNTHLOCKFAIL is clear */
    MicsWrite(IRQ_AUXSTATUS, ~IRQ_SYNTHLOCKFAIL);

    /* check if synth locks for other channels & adjust SYNTH_CT if needed */
    for(prevChange = 0;;) {
        
        /* for each channel (except channel 3) */
        for(chan = failed = 0; chan <= 9; ++chan) {
            
            /* if channel 3, skip it (already done earlier in this function) */
            if (chan == 3) chan = 4;
            
            /* Enable the synthesizer & check if it locks for this channel.
             * Note SYNTH_CT.SYNTH_CT_READY and SYNTH_CTRIM.SYNTH_CTRIM_READY
             * are still set at this point so the synthesizer should lock
             * within MICS_SHORT_SYNTH_LOCK_TIME_US (if it hasn't locked by
             * that time, the ZL7010X will assert IRQ_SYNTHLOCKFAIL). This also
             * waits an additional 100 us to ensure we won't read IRQ_AUXSTATUS
             * just before the ZL7010X sets IRQ_SYNTHLOCKFAIL.
             */
            MicsWrite(MAC_CHANNEL, chan);
            MicsWrite(RF_GENENABLES, SYNTH);
            StDelayUs(MICS_SHORT_SYNTH_LOCK_TIME_US + 100);
            /**/ 
            if (MicsRead(IRQ_AUXSTATUS) & IRQ_SYNTHLOCKFAIL) {
                
                MicsWrite(IRQ_AUXSTATUS, ~IRQ_SYNTHLOCKFAIL);
                failed |= (1 << chan);
            }
            MicsWrite(RF_GENENABLES, 0);
        }
        
        /* If chan 0, 1, or 2 failed and 4 to 9 were all ok, ++SYNTH_CT. Else
         * if chan 8 or 9 failed and 0 to 2 were all ok, --SYNTH_CT. Note this
         * won't affect SYNTH_CT.SYNTH_CT_READY, which will remain set.
         * 
         * Note this also checks the previous change (+1/-1) and stops if it
         * reversed direction to ensure this can never loop forever.
         */
        if ((failed & 0x0007) && !(failed & 0x03F0)) {
            
            if (prevChange < 0) break;
            prevChange = +1;
            ++synthCt;
            
        } else if ((failed & 0x0300) && !(failed & 0x0007)) {
            
            if (prevChange > 0) break;
            prevChange = -1;
            --synthCt;
            
        } else { /* else, failed = 0 (done) or an unexpected value, so stop */
            
            break;
        }
        /* set SYNTH_CT to new value */
        MicsWrite(SYNTH_CT, synthCt);
    }
    
    /* if any channel failed to lock, set error for PC (shouldn't happen) */
    if (failed) {
        MicsErr(MICS_LIB_ERR_SYNTH_DID_NOT_LOCK_FOR_SOME_CHAN, NULL);
        goto cleanup;
    }
    
cleanup:
    /* restore previous channel */
    MicsWrite(MAC_CHANNEL, prevChan);

    /* if ZL7010X interrupt was enabled, re-enable it */
    if (prevMicsIntEnab) MICS_WR_INT_ENAB(TRUE);
}

/* This is called to trim SYNTH_CTRIM and set SYNTH_CTRIM_READY = 1 so the
 * ZL7010X won't repeat this trim each time the synthesizer is enabled in
 * the future. Note if you want the ZL7010X to preserve SYNTH_CTRIM while
 * sleeping and restore it each time it wakes up, you must copy registers
 * to the ZL7010X's wakeup stack (see MicsCopyRegs()). This returns the
 * resulting SYNTH_CTRIM value.
 */
UINT
MicsSynthCtrim(void)
{
    UINT synthCtrim;
    
    /* Start synthesizer so it will trim SYNTH_CTRIM, wait 2 ms to ensure it's
     * done, then read and rewrite SYNTH_CTRIM (with SYNTH_CTRIM_READY = 1).
     * That way, the ZL7010X won't repeat this trim each time the synthesizer
     * is enabled in the future. Note this reads and rewrites SYNTH_CTRIM
     * before it disables the synthesizer, because if it disabled the
     * synthesizer first, the ZL7010X would clear SYNTH_CTRIM_READY,
     * after which SYNTH_CTRIM would return an invalid value.
     */
    MicsWrite(SYNTH_CTRIM, SYNTH_CTRIM_RESET);
    MicsWrite(RF_GENENABLES, SYNTH);
    StDelayUs(2000);
    MicsWrite(SYNTH_CTRIM, synthCtrim = MicsRead(SYNTH_CTRIM));
    MicsWrite(RF_GENENABLES, 0);
    /*
     * If SYNTH_CTRIM_READY isn't set, set error for PC (shouldn't happen).
     */
    if ((synthCtrim & SYNTH_CTRIM_READY) == 0) {
        MicsErr(MICS_LIB_ERR_ZL7010X_DID_NOT_SET_SYNTH_CTRIM_READY, NULL);
    }
    return(synthCtrim);
}
