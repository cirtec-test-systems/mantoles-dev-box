/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Adp/General.h"              /* RSTAT, UINT, BOOL, ... */
#include "Adp/Build/Build.h"          /* UD16, _disable_interrupts(), ... */
#include "Adp/AnyBoard/LocalSpiLib.h" /* LSpiWrite(), ... */
#include "Adp/AnyBoard/SysTimerLib.h" /* StElapsedMs(), ... */ 
#include "Adk/AnyMezz/MicsHw.h"       /* MICS_WR_SPI_CS(), ... */
#include "Adk/AnyMezz/MicsLib.h"      /* public include for MicsLib */
 
/* local includes that are private to the MICS library */
#include "MicsPriv.h"  /* MicsPriv, MicsErr(), ... */

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/**************************************************************************
 * Function Prototypes
 */
 
/**************************************************************************
 * Write to a register in the MICS chip.
 */
void
MicsWrite(UINT regAddr, UINT val)
{
    UINT gie, page;
    
    /* Save current GIE setting (general interrupt enable), then disable all
     * interrupts so no ISR will try to use the SPI bus.
     */
    gie = _get_SR_register() & GIE;
    _disable_interrupts();
    
    /* get the register page (also detect 0x8000 for backwards compatibility) */
    if (regAddr & (MICS_PAGE_2 | 0x8000)) {
        regAddr &= 0x7F;
        page = 2;
    } else {
        page = 1;
    }
    /* If writing to INTERFACE_MODE, update MicsPriv.iMode and MicsPriv.page.
     * Otherwise, if the desired page isn't already selected, select it.
     * There's no need to select the page for INTERFACE_MODE because it exists
     * in both pages, and could cause infinite nesting of MicsRead() and
     * MicsWrite() through MicsSetPage().
     */
    if (regAddr == INTERFACE_MODE) {
            
        MicsPriv.iMode = val;
        MicsPriv.page = (val & ACCESS_PAGE_2) ? 2 : 1;
            
    } else if (page != MicsPriv.page) {
           
        MicsSetPage(page);
    }
    
    /* select MICS chip on SPI bus */
    MICS_WR_SPI_CS(MICS_SPI_ON);
    
    /* transmit register address and value */
    LSpiWrite(regAddr, val);
        
    /* de-select MICS chip on SPI bus */
    MICS_WR_SPI_CS(MICS_SPI_OFF);
    
    /* if interrupts were enabled, re-enable them */
    _bis_SR_register(gie);
}

RSTAT
MicsWriteR(UINT regAddr, UINT val, BOOL remote, UD16 timeoutMs)
{
    UD16 startMs;
    #ifdef BASE_STATION
        UINT hkPageOnImplant;
    #endif
    
    /* if not writing to remote register, call function to write local */
    if (!remote) {
        MicsWrite(regAddr, val);
        return(0);
    }
    
    /* for backwards compatibility (used to set 0x8000 for page 2 registers) */
    if (regAddr & 0x8000) {
        regAddr &= ~0x8000;
        regAddr |= MICS_PAGE_2;
    }
    
    /* if implant */
    #ifdef IMPLANT
    
        /* if trying to access a register in page 2 on remote base, fail */
        if (regAddr & MICS_PAGE_2) {
            MicsErr(MICS_LIB_ERR_ATTEMPT_TO_ACCESS_REMOTE_PAGE_2_VIA_HK, NULL);
            goto error;
        }
        
    #else /* base station */
    
        /* if the remote implant has a ZL70101 */
        if (MicsPriv.micsRevOnImplant <= ZL70101) {
            
            /* if writing to page 2 on remote ZL70101, fail (not supported) */
            if (regAddr & MICS_PAGE_2) {
                MicsErr(MICS_LIB_ERR_ATTEMPT_TO_ACCESS_REMOTE_PAGE_2_VIA_HK, NULL);
                goto error;
            }
            
        } else { /* else, the remote implant has a ZL70102/103 */
            
            /* get the register page to access on the remote implant */
            hkPageOnImplant = (regAddr & MICS_PAGE_2) ? 2 : 1;
            regAddr &= 0x7F;
            
            /* If writing to HK_MODE_102 on the remote implant,
             * update MicsPriv.hkPageOnImplant. Otherwise, call
             * MicsSetHkPageOnImplant() to set the register page to
             * access on the remote implant. There's no need to call
             * MicsSetHkPageOnImplant() for HK_MODE_102 because it exists
             * in both pages. MicsSetHkPageOnImplant() isn't called for
             * HK_MODE_102 because that could cause infinite nesting of
             * MicsWriteR() through MicsSetHkPageOnImplant().
             *
             * Note that if the desired page is already selected on the
             * remote implant, MicsSetHkPageOnImplant() doesn't do anything.
             */
            if (regAddr == HK_MODE_102) {
                MicsPriv.hkPageOnImplant = (val & HK_ACCESS_PAGE_2) ? 2 : 1;
            } else {    
                if (MicsSetHkPageOnImplant(hkPageOnImplant, timeoutMs)) {
                    goto error;
                }
            }
        }
        
        /* If writing to INTERFACE_MODE on remote implant, ensure ACCESS_PAGE_2
         * isn't set. That would cause a problem on the implant if it tried to
         * access page 1 afterwards (e.g. for a MICS interrupt status register).
         */
        if (regAddr == INTERFACE_MODE) val &= ~ACCESS_PAGE_2;
        
    #endif /* base station */
    
    /* clear IRQ_HKREMOTEDONE bit in IRQ status latch */
    MicsPub.irqStat1Latch &= ~IRQ_HKREMOTEDONE;
        
    /* tell local MICS chip to write to specified register on remote chip */
    MicsWrite(HK_TXDATA, val);
    #if MICS_REV == ZL70101
        /* workaround for ZL70101 issue */
        if (MicsWriteHkTxAddr(regAddr, timeoutMs)) goto error;
    #else    
        MicsWrite(HK_TXADDR, regAddr);
    #endif

    /* If the application defined a hook to call when starting an HK TX
     * (e.g. to turn on an LED), call it.
     */
    #ifdef MICS_APP_HOOK_FOR_START_HK_TX
        MICS_APP_HOOK_FOR_START_HK_TX();
    #endif
    
    /* wait for local MICS chip to receive reply from remote MICS chip */
    startMs = StMs();
    while((MicsPub.irqStat1Latch & IRQ_HKREMOTEDONE) == 0) {
        
        if ((StElapsedMs(startMs, timeoutMs)) ||
            (MicsPub.irqStat1Latch & IRQ_CLOST)) {

            MicsErr(MICS_LIB_ERR_REMOTE_HK_TIMEOUT, NULL);
            goto error;
        }
    }
    
    /* if reply from remote MICS indicates remote writes disabled, fail */
    if (MicsRead(HK_TXREPLY) != (UD8)val) {
        MicsErr(MICS_LIB_ERR_REMOTE_HK_WRITES_DISABLED, NULL);
        goto error;
    }
    
    /* return success */
    return(0);
    
error:    
    /* If base station, clear MicsPriv.hkPageOnImplant to tell MicsReadR(),
     * MicsWriteR(), and MicsSetHkPageonImplant() that MicsPriv.hkPageOnImplant
     * is no longer known and needs to be initialized. This is done in case
     * the caller was writing to HK_MODE_102 on the remote implant (to select
     * the HK page).
     */
    #ifdef BASE_STATION
        MicsPriv.hkPageOnImplant = 0;
    #endif
    
    return(-1);
}

/* Write to a bit in a ZL7010X register. Note if the value passed to
 * MicsWriteBit() is 0 (FALSE), it will clear the bit, and if the value
 * is non-zero (1, TRUE, or the bit's mask), it will set the bit.
 */
void
MicsWriteBit(UINT regAddr, UINT mask, UINT val)
{
    UINT gie, newVal;

    /* Save current GIE setting (general interrupt enable), then disable all
     * interrupts to ensure no ISR will modify the register between the time
     * we read it and the time we write it back (to be safe).
     */
    gie = _get_SR_register() & GIE;
    _disable_interrupts();

    newVal = MicsRead(regAddr);
    WR_BIT(newVal, mask, val);
    MicsWrite(regAddr, newVal);

    /* if interrupts were enabled, re-enable them */
    _bis_SR_register(gie);
}

/* Write to multiple bits in a ZL7010X register.
 */
void
MicsWriteBits(UINT regAddr, UINT mask, UINT val)
{
    UINT gie, newVal;

    /* Save current GIE setting (general interrupt enable), then disable all
     * interrupts to ensure no ISR will modify the register between the time
     * we read it and the time we write it back (to be safe).
     */
    gie = _get_SR_register() & GIE;
    _disable_interrupts();

    newVal = MicsRead(regAddr);
    WR_BITS(newVal, mask, val);
    MicsWrite(regAddr, newVal);

    /* if interrupts were enabled, re-enable them */
    _bis_SR_register(gie);
}

RSTAT
MicsWriteBitR(UINT regAddr, UINT mask, UINT val, BOOL remote, UD16 timeoutMs)
{
    int newVal;

    /* if not writing to a remote register, call function to write local */
    if (!remote) {
        MicsWriteBit(regAddr, mask, val);
        return(0);
    }

    if ((newVal = MicsReadR(regAddr, TRUE, timeoutMs)) < 0) return(-1);
    WR_BIT(newVal, mask, val);
    if (MicsWriteR(regAddr, newVal, TRUE, timeoutMs)) return(-1);

    return(0);
}

RSTAT
MicsWriteBitsR(UINT regAddr, UINT mask, UINT val, BOOL remote, UD16 timeoutMs)
{
    int newVal;
    
    /* if not writing to a remote register, call function to write local */
    if (!remote) {
        MicsWriteBits(regAddr, mask, val);
        return(0);
    }

    if ((newVal = MicsReadR(regAddr, TRUE, timeoutMs)) < 0) return(-1);
    WR_BITS(newVal, mask, val);
    if (MicsWriteR(regAddr, newVal, TRUE, timeoutMs)) return(-1);
    
    return(0);
}
 
void
MicsWriteTxBuf(const void *data, UINT len)
{
    UINT gie;
    
    /* Save current GIE setting (general interrupt enable), then disable all
     * interrupts so no ISR will try to use the SPI bus.
     */
    gie = _get_SR_register() & GIE;
    _disable_interrupts();
    
    /* select MICS chip on SPI bus */
    MICS_WR_SPI_CS(MICS_SPI_ON);
    
    /* transmit MICS RX buf address followed by specified data bytes */
    LSpiWriteMulti(TXRXBUFF, data, len);
    
    /* de-select MICS chip on SPI bus */
    MICS_WR_SPI_CS(MICS_SPI_OFF);
    
    /* if interrupts were enabled, re-enable them */
    _bis_SR_register(gie);
}

/* If the local ZL7010X is a ZL70101, this function is called to write to
 * HK_TXADDR (instead of calling MicsWrite() directly). This function waits
 * for the ZL70101 to enter TX mode before writing to HK_TXADDR, ensuring the
 * software will never write to HK_TXADDR when the ZL70101 is switching from
 * RX to TX mode (workaround for a ZL70101 issue). Writing to HK_TXADDR tells
 * the ZL7010X to use housekeeping to access the specified register on the
 * remote ZL7010X.
 */ 
#if MICS_REV == ZL70101
RSTAT
MicsWriteHkTxAddr(UINT val, UD16 timeoutMs)
{
    RSTAT rStat = 0;
    UD16 startMs;
    UINT gie;
    
    /* save current GIE setting (interrupt enable) */
    gie = _get_SR_register() & GIE;

    /* loop until we've written to HK_TXADDR, or we time out */
    startMs = StMs();
    while(1) {

        /* if the ZL7010X is in TX mode */
        if (!MICS_RX_MODE) {

            /* Disable all interrupts, then check if the ZL7010X is still
             * in TX mode, and if so, write to HK_TXADDR and stop (done).
             * Otherwise, restore the previous GIE (to re-enable interrupts)
             * and continue waiting to wait for the ZL7010X to enter TX mode
             * again. This is done to ensure that the write to HK_TXADDR
             * always occurs immediately after this software detects the
             * ZL7010X is in TX mode (without any intervening interrupt).
             */
            _disable_interrupts();
            if (!MICS_RX_MODE) {

                MicsWrite(HK_TXADDR, val);
                break;
            }
            _bis_SR_register(gie);
        }

        /* if it's been too long, or the ZL7010X asserted IRQ_CLOST, fail */
        if (StElapsedMs(startMs, timeoutMs) ||
            (MicsPub.irqStat1Latch & IRQ_CLOST)) {

            MicsErr(MICS_LIB_ERR_TIMED_OUT_WAITING_FOR_TX_MODE, NULL);
            rStat = -1;
            break;
        }
    }

    /* restore previous GIE setting (interrupt enable) */
    _bis_SR_register(gie);
    return(rStat);
}
#endif /* #if MICS_REV == ZL70101 */
