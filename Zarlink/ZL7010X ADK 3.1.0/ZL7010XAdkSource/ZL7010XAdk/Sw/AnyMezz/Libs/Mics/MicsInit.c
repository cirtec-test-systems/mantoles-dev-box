/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Standard/String.h"     /* memset() */
#include "Adp/General.h"         /* UINT, RSTAT, BOOL, ... */
#include "Adp/Build/Build.h"     /* MICS_REV, BASE_STATION, ... */
#include "Adk/AnyMezz/MicsHw.h"  /* ZL70101, ZL70102, ... */
#include "Adk/AnyMezz/MicsLib.h" /* public include for MicsLib */

/* local includes that are private to the MICS library */
#include "MicsPriv.h"  /* MICS_PRIV, MicsErr(), ... */

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/* private data for MICS library */
MICS_PRIV  MicsPriv;

/* public data for MICS library */ 
MICS_PUB MicsPub;

/**************************************************************************
 * Function Prototypes
 */
 
/**************************************************************************
 * Initialize the MICS library (note this doesn't initialize the MICS chip
 * itself). This clears the interrupt status latches (MicsPub.irqStat1Latch,
 * etc.), so it must be called before the MICS interrupt is enabled. Note for
 * the MICS library functions to work properly, the MICS interrupt service
 * routine must update the IRQ status latches when a MICS interrupt occurs
 * (i.e. if bits are set in the IRQ statuses read from the MICS chip, the
 * corresponding bits must be set in the latches).
 */
RSTAT
MicsInit(void)
{
    /* clear data and init any non-zero defaults */
    (void)memset(&MicsPriv, 0, sizeof(MicsPriv));
    (void)memset(&MicsPub, 0, sizeof(MicsPub));
    
    /* by default, assume MICS revision on remote implant is same as base */
    #ifdef BASE_STATION
        MicsPriv.micsRevOnImplant = MICS_REV;
    #endif
    
    return(0);
}

/* MicsRead() and MicsWrite() call this to select the page in the ZL7010X.
 */
void 
MicsSetPage(UINT page)
{
    /* if needed, initialize MicsPriv.page and MicsPriv.iMode */
    if (MicsPriv.page == 0) {
        MicsPriv.iMode = MicsRead(INTERFACE_MODE);
        MicsPriv.page = (MicsPriv.iMode & ACCESS_PAGE_2) ? 2 : 1;
    }
    
    /* If the specified page isn't already selected, select it. Note the
     * MicsWrite() will update MicsPriv.page.
     */
    if (page != MicsPriv.page) {
    
        if (page == 2) {
            MicsPriv.iMode |= ACCESS_PAGE_2;
        } else {
            MicsPriv.iMode &= ~ACCESS_PAGE_2;
        }
        MicsWrite(INTERFACE_MODE, MicsPriv.iMode);
    }
}

/* For a base station, MicsReadR() and MicsWriteR() call this to select the
 * page to access on the remote implant via housekeeping, but only if the
 * remote implant has a ZL70102/103 (not supported by ZL70101 implant).
 */
#ifdef BASE_STATION
RSTAT
MicsSetHkPageOnImplant(UINT hkPageOnImplant, UD16 timeoutMs)
{
    int val;
    
    /* if needed, initialize MicsPriv.hkPageOnImplant */
    if (MicsPriv.hkPageOnImplant == 0) {
        
        if ((val = MicsReadR(HK_MODE_102, TRUE, timeoutMs)) < 0) return(-1);
        
        MicsPriv.hkPageOnImplant = (val & HK_ACCESS_PAGE_2) ? 2 : 1;
    }
    
    /* If the specified HK page isn't already selected on the remote implant,
     * select it. Note the MicsWriteR() will update MicsPriv.hkPageOnImplant.
     */
    if (hkPageOnImplant != MicsPriv.hkPageOnImplant) {
    
        if (hkPageOnImplant == 2) {
            val = HK_WRITE_ENAB | HK_ACCESS_PAGE_2;
        } else {
            val = HK_WRITE_ENAB;
        }
        if (MicsWriteR(HK_MODE_102, val, TRUE, timeoutMs) < 0) return(-1);
    }
    
    return(0);
}
#endif /* #ifdef BASE_STATION */

/* For a base station, this can be called to tell the MICS library the revision
 * of the MICS chip on the remote implant. The revisions are defined in
 * "Adk/AnyMezz/MicsHw.h" (ZL70100, ZL70101, ZL70102, or ZL70103). By default,
 * MicsInit() initializes this to be the same as the MICS revision on the base,
 * but this function can be called to change it if needed.
 */
#ifdef BASE_STATION
void
MicsRevOnImplant(UINT micsRevOnImplant)
{
    MicsPriv.micsRevOnImplant = micsRevOnImplant;
}
#endif /* #ifdef BASE_STATION */
