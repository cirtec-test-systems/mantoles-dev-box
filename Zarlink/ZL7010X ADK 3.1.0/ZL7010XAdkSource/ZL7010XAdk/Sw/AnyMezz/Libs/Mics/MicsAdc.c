/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Adp/General.h"              /* UINT, BOOL, TRUE/FALSE, ... */
#include "Adp/Build/Build.h"          /* UD16, ... */
#include "Adp/AnyBoard/SysTimerLib.h" /* StElapsedMs(), ... */ 
#include "Adk/AnyMezz/MicsHw.h"       /* ADCCTRL, ... */
#include "Adk/AnyMezz/MicsLib.h"      /* public include for MicsLib */

/* local includes that are private to the MICS library */
#include "MicsPriv.h"  /* MicsPriv, MicsErr(), MICS_ADC_ENAB, ... */

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/**************************************************************************
 * Function Prototypes
 */
 
/**************************************************************************
 * Perform an A/D conversion using the ADC in the MICS chip.
 */
UINT
MicsAdc(UINT adcInput)
{
    UD16 startMs;
    UINT prevAdcEnab;
    int adcResult;
    
    /* save ADC enable setting so we can restore it later */
    prevAdcEnab = (MicsPriv.flags & MICS_ADC_ENAB) ? ADC_ENAB : 0;
    
    /* Enable ADC, set input, and clear ADC_START to prepare for conversion.
     * Also, if the ADC was not previously enabled, give it time to start.
     */
    MicsWrite(ADCCTRL, ADC_ENAB | adcInput);
    if (prevAdcEnab == 0) StDelayUs(MICS_ADC_START_TIME_US);
    
    /* start conversion and wait for it to complete */
    MicsWrite(ADCCTRL, ADC_ENAB | ADC_START | adcInput);
    StDelayUs(2);
    for(startMs = StMs();;) {
        
        /* if ADC is complete, stop (done) */
        if ((adcResult = MicsRead(ADCOUTPUT)) & ADC_COMPLETE) {
            adcResult &= ADC_RESULT_MASK;
            break;
        }
        
        /* if it's been too long, set error and stop (should never happen) */
        if (StElapsedMs(startMs, 100)) {
            MicsErr(MICS_LIB_ERR_ADC_TIMEOUT, NULL);
            adcResult = 0;
            break;
        }
    }
    
    /* clear ADC_START and disable ADC if it was previously disabled */
    MicsWrite(ADCCTRL, prevAdcEnab);
    
    return(adcResult);
}

BOOL
MicsEnabAdc(BOOL enab)
{
    /* if ADC is currently enabled */
    if (MicsPriv.flags & MICS_ADC_ENAB) {
        
        /* if caller wants to disable ADC, do so */
        if (!enab) {
            MicsWrite(ADCCTRL, 0);
            MicsPriv.flags &= ~MICS_ADC_ENAB;
        }
        /* indicate the ADC was previously enabled */
        return(TRUE);
        
    } else { /* else, ADC is currently disabled */
        
        /* if caller wants to enable ADC, do so */
        if (enab) {
            MicsWrite(ADCCTRL, ADC_ENAB);
            MicsPriv.flags |= MICS_ADC_ENAB;
            StDelayUs(MICS_ADC_START_TIME_US);
        }
        /* indicate the ADC was previously disabled */
        return(FALSE);
    }
}

#ifdef BASE_STATION
int
MicsAdcR(UINT adcInput, BOOL remote)
{
    UD16 startMs;
    int adcResult;
    
    /* if not remote ADC, call function to do local ADC */
    if (!remote) return(MicsAdc(adcInput));
    
    /* Enable remote ADC, set input, and clear ADC_START to prepare for
     * conversion. Also wait MICS_ADC_START_TIME_US to ensure the ADC has time
     * to power up. Note MicsWriteR() will fail if no session is active, or if
     * the remote Implant does not allow remote writes. 
     */
    if (MicsWriteR(ADCCTRL, ADC_ENAB|adcInput, TRUE, 250)) return(-1);
    StDelayUs(MICS_ADC_START_TIME_US);
    
    /* start conversion on remote ADC and wait for it to complete */
    if (MicsWriteR(ADCCTRL, ADC_ENAB|ADC_START|adcInput, TRUE, 250)) return(-1);
    StDelayUs(2);
    for(startMs = StMs();;) {
        
        /* read remote ADC output */
        if ((adcResult = MicsReadR(ADCOUTPUT, TRUE, 250)) < 0) return(-1);
        
        /* if conversion is complete, stop (done) */
        if (adcResult & ADC_COMPLETE) {
            adcResult &= ADC_RESULT_MASK;
            break;
        }
        /* if it's been too long, stop (should never happen) */
        if (StElapsedMs(startMs, 100)) {
            MicsErr(MICS_LIB_ERR_REMOTE_ADC_TIMEOUT, NULL);
            return(-1);
        }
    }
    
    /* clear ADC_START and disable remote ADC */
    if (MicsWriteR(ADCCTRL, 0, TRUE, 250)) return(-1);
    
    return(adcResult);
}
#endif /* #ifdef BASE_STATION */
