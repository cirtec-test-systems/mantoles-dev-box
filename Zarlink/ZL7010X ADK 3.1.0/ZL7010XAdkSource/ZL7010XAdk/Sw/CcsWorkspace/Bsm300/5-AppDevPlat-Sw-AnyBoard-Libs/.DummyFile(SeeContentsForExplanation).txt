
This file was created manually, not by CCS. It is a dummy file that is included
in the CCS package in SOS so when SOS populates the files contained in the
package, it also creates the folder that contains this file. In CCS, this
folder contains links to source files that are included in the corresponding
project, and if the folder doesn't exist, CCS won't include those links in the
project. This dummy file is needed to force SOS to create the folder, because
the folder doesn't contain any other files, and when SOS populates the files
contained in a package, it only creates folders that contain files.

