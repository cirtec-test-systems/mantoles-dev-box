/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

#include "Standard/String.h"    /* strchr(), ... */
#include "Adk/AdkVer.h"         /* ADK_VER */
#include "Adk/AnyMezz/MicsHw.h" /* ZL70101, MICS_PAGE_2, ... */
#include "Adk/Pc/AdkLib.h"      /* public include for AdkLib */
 
/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * 
 * Global and Static Definitions
 */
 
/**************************************************************************
 * Function Prototypes
 */
 
/**************************************************************************
 * FUNCTION: AdkGetApiVer
 * 
 * This function gets the version string for this library, DLL, or API (the
 * Application Programming Interface for the ZL7010X Application Development
 * Kit). The caller may pass this to VerIsCompat() to check if it's compatible
 * with the API version required by the caller. For the format and content of
 * version strings, see VerIsCompat() in "AppDevPlat\Sw\Pc\Libs\VerLib.c".
 *
 * PARAMETERS:
 *     None.
 *
 * RETURN VALUES:
 *     This function returns the version string for this API (never NULL).
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function.
 *
 * VISIBILITY: Customer
 */
const char *
AdkGetApiVer(void)
{
    return(ADK_VER);
}

/**************************************************************************
 * FUNCTION: AdkIsStackReg
 * 
 * This can be called to check if a ZL7010X MICS chip includes the specified
 * register in its wakeup stack. When the ZL7010X "copy registers" command is
 * executed (MAC_CTRL.COPY_REGS), it will copy these registers to the wakeup
 * stack in the ZL7010X, so the ZL7010X will preserve them while it's sleeping
 * and restore them the next time it is awakened.
 * 
 * PARAMETERS:
 *     regAddr:
 *         The address of the register in the ZL7010X. For page 2 registers,
 *         specify MICS_PAGE_2 (0x80) in the register address (note 0x8000 will
 *         also still work so this remains backwards compatible).
 *     micsRev:
 *         The revision of the ZL7010X (see ZL70101, ZL70102, etc. in
 *         "ZL7010XAdk\Sw\Includes\Adk\AnyMezz\MicsHw.h").
 *
 * RETURN VALUES:
 *     Returns TRUE if the ZL7010X includes the specified register in its
 *     wakeup stack, FALSE otherwise.
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function.
 *
 * VISIBILITY: Customer
 */
BOOL
AdkIsStackReg(UD32 regAddr, UD32 micsRev)
{
    /* for backwards compatibility (used to set 0x8000 for page 2 registers) */
    if (regAddr & 0x8000) {
        regAddr &= ~0x8000;
        regAddr |= MICS_PAGE_2;
    }
    
    if (micsRev == ZL70101) {
        if (strchr(MICS_STACK_REGS_101, regAddr)) return(TRUE);
    } else {
        if (strchr(MICS_STACK_REGS_102, regAddr)) return(TRUE);
    }
    
    return(FALSE);
}

/**************************************************************************
 * FUNCTION: AdkIsCrcReg
 *
 * This can be called to check if a ZL7010X MICS chip includes the specified
 * register in its CRC. Note the ZL7010X also includes its wakeup stack in the
 * CRC.
 *
 * PARAMETERS:
 *     regAddr:
 *         The address of the register in the ZL7010X. For page 2 registers,
 *         specify MICS_PAGE_2 (0x80) in the register address (note 0x8000 will
 *         also still work so this remains backwards compatible).
 *     micsRev:
 *         The revision of the ZL7010X (see ZL70101, ZL70102, etc. in
 *         "ZL7010XAdk\Sw\Includes\Adk\AnyMezz\MicsHw.h").
 *
 * RETURN VALUES:
 *     Returns TRUE if the ZL7010X includes the specified register in its
 *     CRC, FALSE otherwise.
 *
 * THREAD SAFE:
 *     Multiple threads may call this function.
 *
 * VISIBILITY: Customer
 */
BOOL
AdkIsCrcReg(UD32 regAddr, UD32 micsRev)
{
    /* for backwards compatibility (used to set 0x8000 for page 2 registers) */
    if (regAddr & 0x8000) {
        regAddr &= ~0x8000;
        regAddr |= MICS_PAGE_2;
    }

    if (micsRev == ZL70101) {
        if (strchr(MICS_CRC_REGS_101, regAddr)) return(TRUE);
    } else {
        if (strchr(MICS_CRC_REGS_102, regAddr)) return(TRUE);
    }

    return(FALSE);
}
