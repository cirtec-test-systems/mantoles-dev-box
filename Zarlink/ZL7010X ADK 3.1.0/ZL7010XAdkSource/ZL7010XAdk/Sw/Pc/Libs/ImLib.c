/**************************************************************************
 * FILE: ImLib.c
 * 
 * This file contains functions to communicate with a ZL7010X ADK implant
 * mezzanine board. For usage examples, see "ZL7010XAdk\Sw\Pc\Examples\
 * ImExample.c"
 *
 * NOTES:
 *     None.
 * 
 * VISIBILITY: Customer
 *
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Standard/String.h"         /* memset(), NULL */
#include "Standard/StdLib.h"         /* calloc() */
#include "Standard/Malloc.h"         /* calloc() */
#include "Adp/General.h"             /* RSTAT, BOOL, TRUE/FALSE, ... */
#include "Adp/Build/Build.h"         /* UD8, UD32, BOOL8, ... */
#include "Adp/Pc/ErrInfoLib.h"       /* EI, EiSet() */
#include "Adp/Pc/ComLib.h"           /* ComCmdAndReply(), ... */ 
#include "Adp/Pc/VerLib.h"           /* VerIsCompat() */
#include "Adk/AdkVer.h"              /* ADK_VER */
#include "Adk/AnyMezz/MicsHw.h"      /* MICS_PAGE_2, ... */
#include "Adk/AnyMezz/MicsLib.h"     /* MICS_LIB_ERR, ... */
#include "Adk/AnyMezz/MicsGeneral.h" /* MICS_LINK_STAT, MICS_LINK_QUAL, ... */
#include "Adk/ImMezz/ImCom.h"        /* IM_LOCAL_ADDR, ... */
#include "Adk/ImMezz/ImGeneral.h"    /* IM_STAT, IM_MICS_CONFIG, AIM100, ... */
#include "Adk/ImMezz/ImAppErrs.h"    /* IM_APP_ERR, ... */
#include "Adk/Pc/ImLib.h"            /* ImLib public include */

/**************************************************************************
 * Defines and Macros
 */
 
/* magic number used to validate data structures (ascii "IM  ") */
#define IM_MAGIC  0x494D2020

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* Structure of private data for implant library.
 */
typedef struct {
    
    /* magic number used to validate data structure */
    volatile UD32 magic;
    
    /* Communication interface to communicate with the local ADP board (the
     * ADP board connected to the PC via USB).
     * 
     * If IM_PRIV.addr is IM_LOCAL_ADDR, this is the ADP board attached to the
     * local implant mezzanine board. In this case, the ADP board forwards
     * commands to the mezzanine board via SPI, and the mezzanine board
     * processes the commands.
     * 
     * If IM_PRIV.addr is IM_REMOTE_ADDR, this is the ADP board attached to the
     * local base station mezzanine board. In this case, the ADP board forwards
     * commands to the base station mezzanine board via SPI, which forwards
     * them to the remote implant via the MICS wireless link, and the remote
     * implant processes the commands. Note before attempting to communicate
     * with the remote implant, you must tell the base station to start a
     * session with it (otherwise, the communication will just time out).
     */
    COM_ID comId;
    
    /* Address of the implant mezzanine board to communicate with (IM_LOCAL_ADDR
     * for the local implant, or IM_REMOTE_ADDR for the remote implant).
     */
    UD8 addr;
    
    /* if true, swap multi-byte fields sent to & received from implant */
    BOOL swap;
    
    /* revision of ZL7010X on implant */
    int micsRev;

} IM_PRIV;
 
/**************************************************************************
 * 
 * Global and Static Definitions
 */
 
/**************************************************************************
 * Function Prototypes
 */
static RSTAT ImIdErr(EI e);
static RSTAT ImSizeErr(EI e);
static RSTAT ImConvertErr(EI e);
static void ImSwapGenStat(IM_STAT *genStat);
static void ImSwapLinkStat(MICS_LINK_STAT *ls);
static void ImSwapLinkQual(MICS_LINK_QUAL *lq);

/**************************************************************************
 * FUNCTION: ImInitSpecs
 * 
 * This function initializes a IM_SPECS structure with default values. The
 * structure may then be modified as desired and passed to ImOpen().
 *
 * PARAMETERS:
 *     specs:
 *         The IM_SPECS structure to initialize. IM_SPECS is defined in
 *         "ZL7010XAdk\Sw\Includes\Adk\Pc\ImLib.h".
 *     specsSize:
 *         The size of the specified IM_SPECS structure (bytes). Note
 *         ImInitSpecs() won't write to anything in the structure beyond the
 *         specified size. That way, if new fields are added to the end of
 *         IM_SPECS, this function will remain compatible with older
 *         applications that don't use the new fields.
 *
 * RETURN VALUES:
 *     None.
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate IM_SPECS structure.
 *
 * VISIBILITY: Customer
 */
void
ImInitSpecs(IM_SPECS *specs, UD32 specsSize)
{
    IM_SPECS defSpecs;
    
    /* init default specs with default values for everything */
    (void)memset(&defSpecs, 0, sizeof(defSpecs));
    defSpecs.magic = IM_MAGIC;
    
    /* copy default specs into caller's specs (up to specified size) */
    if (specsSize <= sizeof(defSpecs)) {
        (void)memcpy(specs, &defSpecs, specsSize);
    } else {
        (void)memset(specs, 0, specsSize);
        (void)memcpy(specs, &defSpecs, sizeof(defSpecs));
    }
}
 
/**************************************************************************
 * FUNCTION: ImOpen
 * 
 * This function opens a connection to a ZL7010X ADK implant mezzanine board.
 * It returns an IM_ID that may be passed to the other functions in this
 * library to communicate with the implant.
 * 
 * Note only one connection should be opened for each implant, but it can
 * be shared by multiple threads if desired (semaphores are used to coordinate
 * communication so the threads won't conflict).
 *
 * PARAMETERS:
 *     specs:
 *         Specifications for the connection (see IM_SPECS in "ZL7010XAdk\Sw\
 *         Includes\Adk\Pc\ImLib.h"), or NULL to use defaults. Note before
 *         passing a IM_SPECS structure to ImOpen(), it must be initialized
 *         using ImInitSpecs() and modified as desired.
 *     specsSize:
 *         The size of the specified IM_SPECS structure (bytes). Note ImOpen()
 *         won't reference anything in the structure beyond the specified size.
 *         That way, if new fields are added to the end of IM_SPECS, this
 *         function will remain compatible with older applications that don't
 *         use the new fields.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns a non-NULL ID for the connection.
 *     Otherwise, it fills the specified error object and returns NULL (to get
 *     the error information, see EiGetMsg(), etc. in "AppDevPlat\Sw\Pc\Libs\
 *     ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but only one connection should
 *     be opened for each implant (the connection may be shared by multiple
 *     threads if desired). Each thread should also specify its own separate
 *     error object.
 *
 * VISIBILITY: Customer
 */
IM_ID
ImOpen(const IM_SPECS *specs, UD32 specsSize, EI e)
{
    char imMezzVer[IM_MEZZ_VER_BUF_SIZE];
    IM_SPECS specsBuf;
    IM_PRIV *im;
    IM_ID imId;
    
    /* init resources referenced in error cleanup */
    im = NULL;
    imId = NULL;
    
    /* Init local specs buf with default values, then overwrite the defaults
     * with the passed specs (up to the specified size). That way, if fields
     * are added to the specs in the future, and the calling application isn't
     * updated, the new fields will still have valid default values.
     */
    ImInitSpecs(&specsBuf, sizeof(specsBuf));
    if (specs != NULL) {
        
        /* if passed specs not initialized by ImInitSpecs() or too big, fail */
        if ((specs->magic != IM_MAGIC) || (specsSize > sizeof(specsBuf))) {
            EiSet(e, IM_LIB_EID_INVALID_SPECS, NULL);
            goto error;
        }
        (void)memcpy(&specsBuf, specs, specsSize);
    }
    specs = &specsBuf;
    
    /* Allocate memory for private data and clear all resources in it so
     * the free function can check if each resource was allocated before trying
     * to free it. This way the free function can be called to clean up if
     * an error is detected during the remainder of the creation.
     */
    if ((im = calloc(1, sizeof(*im))) == NULL) {
        EiSet(e, IM_LIB_EID_FAILED_TO_ALLOCATE_MEM, "%d", sizeof(*im));
        goto error;
    }
    im->magic = IM_MAGIC;
    imId = (IM_ID)im;
    
    /* if no local ADP board connection specified, fail */
    if (specs->localAdpId == NULL) {
        EiSet(e, IM_LIB_EID_NO_LOCAL_ADP_ID, NULL);
        goto error;
    }
    /* get communication interface for local ADP board */
    if ((im->comId = AdpGetComId(specs->localAdpId, e)) == NULL) goto error;
    
    /* if specified, set address to communicate with remote implant mezzanine */
    if (specs->remote) {
        im->addr = IM_REMOTE_ADDR;
    } else {
        im->addr = IM_LOCAL_ADDR;
    }
    
    /* get version string from implant mezzanine board */
    if (ImGetMezzVer(imId, imMezzVer, sizeof(imMezzVer), e)) goto error;
    /*
     * Check if the version string contains a board model supported by this
     * library. This also verifies we're talking to an implant.
     */
    if ((strstr(imMezzVer, AIM100_NAME) == NULL) &&
        (strstr(imMezzVer, AIM200_NAME) == NULL) &&
        (strstr(imMezzVer, AIM300_NAME) == NULL)) {
        EiSet(e, IM_LIB_EID_INCOMPATIBLE_BOARD_MODEL, "%s", imMezzVer);
        goto error;
    }
    /* Check if the implant mezzanine board version is compatible with the
     * version required by this library (ADK_VER).
     */
    if (VerIsCompat(imMezzVer, ADK_VER) == FALSE) {
        EiSet(e, IM_LIB_EID_INCOMPATIBLE_IM_MEZZ_VER, "%s\f%s",
            imMezzVer, ADK_VER);
        goto error;
    }
    
    /* read revision from ZL7010X */
    if ((im->micsRev = ImReadMicsReg(imId, MICS_REVISION, e)) < 0) {
        goto error;
    }
    
    return(imId);
    
error:
    /* free any resources allocated up to where the error occurred */
    if (im != NULL) (void)ImClose((IM_ID)im, NULL);
    
    return(NULL);
}

/**************************************************************************
 * FUNCTION: ImClose
 * 
 * This function closes a connection that was opened by ImOpen(), freeing any
 * memory and other resources allocated for it. Note this won't close the local
 * ADP board connection that was passed to ImOpen() (see IM_SPECS.localAdpId in
 * "ZL7010XAdk\Sw\Includes\Adk\Pc\ImLib.h"). Thus, the local ADP board
 * connection must be closed separately if desired (see AdpClose() in
 * "AppDevPlat\Sw\Pc\Libs\AdpLib.c").
 *
 * PARAMETERS:
 *     imId:
 *         The ID of the connection to close (an ID returned by ImOpen()).
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     A connection should only be closed once by a single thread, and after
 *     it's closed, the connection ID should no longer be used by any thread
 *     (if it is, the called function will detect it and return an error).
 *     Each thread should also specify its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
ImClose(IM_ID imId, EI e)
{
    IM_PRIV *im = (IM_PRIV *)imId;
    RSTAT rStat = 0;
    
    /* if specified id is invalid, don't try to close it */
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));
    
    /* Invalidate the magic number so we'll detect any attempt to use the ID
     * after it has been closed.
     */
    im->magic = 0;
    free(im);
    return(rStat);
}

/**************************************************************************
 * FUNCTION: ImGetMezzVer
 * 
 * This function gets the version string for the implant mezzanine board. For
 * the format of the version string, see the notes for each release in the
 * "ZL7010X ADK Release Notes" (included with the ADK documentation).
 *
 * PARAMETERS:
 *     imId:
 *         A connection ID returned by ImOpen().
 *     buf:
 *         Buffer in which to return the version string.
 *     bufSize:
 *         The size of the specified buffer (bytes). To ensure the version
 *         string will fit in the buffer, the buffer should be at least
 *         IM_MEZZ_VER_BUF_SIZE bytes (defined in "ZL7010XAdk\Sw\Includes\Adk\
 *         Pc\ImLib.h"). If the version string won't fit in the buffer, it
 *         will be truncated (note it will still be NULL-terminated).
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function puts the NULL-terminated version string
 *     in the specified buffer and returns 0. If not successful, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate version string buffer and error object.
 *
 * VISIBILITY: Customer
 */
RSTAT
ImGetMezzVer(IM_ID imId, char *buf, UD32 bufSize, EI e)
{
    IM_PRIV *im = (IM_PRIV *)imId;
    
    /* if specified id is invalid, fail */
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));
    
    if (ComGetDevVer(im->comId, im->addr, buf, bufSize, e)) {
        return(ImConvertErr(e));
    }
    return(0);
}

/* This has been kept for backwards compatibility - use ImGetStatChanges()
 * instead. This gets just the general status from the implant (IM_STAT).
 */
RSTAT
ImGetStat(IM_ID imId, IM_STAT *genStat, UD32 genStatSize, EI e)
{
    IM_PRIV *im = (IM_PRIV *)imId;
    UD8 repBuf[sizeof(IM_STAT) + 50];
    SD32 repLen;
    
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));
    
    if ((repLen = ComCmdAndVarReply(im->comId, im->addr,
        IM_GET_STAT_CMD_TYPE, NULL, 0, repBuf, sizeof(repBuf), e)) < 0) {
        return(ImConvertErr(e));
    }
    /* if needed, swap multi-byte fields */
    if (im->swap) ImSwapGenStat((IM_STAT *)repBuf);
    
    if (genStatSize > (UD32)repLen) return(ImSizeErr(e));
    (void)memcpy(genStat, repBuf, genStatSize);
    
    /* if error occurred on board, get error & set error info for caller */
    if (genStat->flags & IM_ERR_OCCURRED) {
        if (ComGetDevErr(im->comId, im->addr, e)) return(-1);
        (void)ImConvertErr(e);
    }
    
    return(0);
}

/**************************************************************************
 * FUNCTION: ImGetMicsConfig
 * 
 * This function gets the current MICS configuration from the implant.
 *
 * PARAMETERS:
 *     imId:
 *         A connection ID returned by ImOpen().
 *     mc:
 *         Buffer in which to return the MICS configuration. IM_MICS_CONFIG is
 *         defined in "ZL7010XAdk\Sw\Includes\Adk\ImMezz\ImGeneral.h".
 *     mcSize:
 *         The size of the specified buffer (bytes). Note ImGetMicsConfig()
 *         won't write beyond the specified size. That way, if new fields are
 *         added to the end of IM_MICS_CONFIG, this function will remain
 *         compatible with older applications that don't use the new fields.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function puts the MICS configuration in the
 *     specified buffer and returns 0. If not successful, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate IM_MICS_CONFIG buffer and error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
ImGetMicsConfig(IM_ID imId, IM_MICS_CONFIG *mc, UD32 mcSize, EI e)
{
    IM_PRIV *im = (IM_PRIV *)imId;
    UD8 mcBuf[256];
    SD32 mcLen;
    
    /* if specified id is invalid, fail */
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));
    
    /* Get MICS config from implant mezzanine board. Note mcBuf should always
     * be larger than the IM_MICS_CONFIG structure, so this will always read
     * the full config available from the board, whatever the board's version.
     * If IM_MICS_CONFIG ever outgrows mcBuf, the worst that will happen is
     * ComCmdAndVarReply() will fail during development and mcBuf will have
     * to be increased.
     */
    if ((mcLen = ComCmdAndVarReply(im->comId, im->addr,
        IM_GET_MICS_CONFIG_CMD_TYPE, NULL, 0, mcBuf, sizeof(mcBuf), e)) < 0) {
        return(ImConvertErr(e));
    }
    /* If the caller expects a larger config than is available, fail. It's ok
     * if the caller expects a smaller config (fewer fields). That way, if new
     * fields are added to the end of IM_MICS_CONFIG, this interface will
     * remain compatible with older software on the PC - since the caller
     * doesn't expect the new fields that were added to the end of
     * IM_MICS_CONFIG, those fields simply won't be returned to the caller.
     */
    if (mcSize > (UD32)mcLen) return(ImSizeErr(e));
    (void)memcpy(mc, mcBuf, mcSize);
    
    return(0);
}

/**************************************************************************
 * FUNCTION: ImSetMicsConfig
 * 
 * This function sets the MICS configuration on the implant. Note every field
 * must be specified in the configuration. Usually, an application will call
 * ImGetMicsConfig() to get the current MICS configuration, modify it as
 * desired, then pass it to ImSetMicsConfig().
 * 
 * Note if the ZL7010X is sleeping, this will wake it up, and if a ZL7010X
 * operation is active, this will put the ZL7010X to sleep and rewake it (to
 * abort the operation). The ZL7010X will remain awake until its watchdog timer
 * expires (~4.37 seconds after it is awakened), or ImSleep() is called.
 *
 * PARAMETERS:
 *     imId:
 *         A connection ID returned by ImOpen().
 *     mc:
 *         The new MICS configuration. IM_MICS_CONFIG is defined in
 *         "ZL7010XAdk\Sw\Includes\Adk\ImMezz\ImGeneral.h".
 *     mcSize:
 *         The size of the specified IM_MICS_CONFIG structure (bytes). Note
 *         ImSetMicsConfig() won't reference anything in the structure beyond
 *         the specified size. That way, if new fields are added to the end of
 *         IM_MICS_CONFIG, this function will remain compatible with older
 *         applications that don't specify the new fields (defaults will be
 *         used for the new fields).
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
ImSetMicsConfig(IM_ID imId, const IM_MICS_CONFIG *mc, UD32 mcSize, EI e)
{
    IM_PRIV *im = (IM_PRIV *)imId;
    
    /* if specified id is invalid, fail */
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));
    
    /* Check specified config size. Note the specified size can be less than
     * the current size of IM_MICS_CONFIG. That way, if new fields are added
     * to the end of IM_MICS_CONFIG, this interface will remain compatible with
     * older software on the PC (the fields added to the end of the config will
     * not be affected by the command, and will simply be left as is).
     */
    if (mcSize > sizeof(IM_MICS_CONFIG)) return(ImSizeErr(e));
    
    /* send command and receive reply */
    if (ComCmdAndReply(im->comId, im->addr, IM_SET_MICS_CONFIG_CMD_TYPE,
        mc, mcSize, NULL, 0, e)) {
        return(ImConvertErr(e));
    }
    return(0);
}

/**************************************************************************
 * FUNCTION: ImWakeup
 * 
 * If the ZL7010X is sleeping, this function will wake it up (if it's already
 * awake, it will remain awake). If "stayAwake" is false, the ZL7010X will
 * remain awake until its watchdog timer expires (~4.37 seconds after it is
 * awakened), or ImSleep() is called. If "stayAwake" is true, the ZL7001X
 * watchdog timer is disabled so the ZL7010X will remain awake indefinitely.
 * Note the "stay awake" feature is intended for test/debug, not for use in an
 * end product, because it risks draining the implant's battery.
 * 
 * PARAMETERS:
 *     imId:
 *         A connection ID returned by ImOpen().
 *     stayAwake:
 *         If true, the ZL7010X watchdog timer is disabled so the ZL7010X will
 *         remain awake indefinitely. Note this is intended for test/debug, not
 *         for use in an end product, becaue it risks draining the battery.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
ImWakeup(IM_ID imId, BOOL stayAwake, EI e)
{ 
    IM_PRIV *im = (IM_PRIV *)imId;
    IM_WAKEUP_CMD cmd;
    
    /* if specified id is invalid, fail */
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));
    
    /* build command */
    cmd.stayAwake = (BOOL8)stayAwake;
    
    /* send command and receive reply */
    if (ComCmdAndReply(im->comId, im->addr, IM_WAKEUP_CMD_TYPE,
        &cmd, sizeof(cmd), NULL, 0, e)) {
        return(ImConvertErr(e));
    }
    return(0);
}

/**************************************************************************
 * FUNCTION: ImSleep
 * 
 * This function tells the implant to abort any ZL7010X operation that is
 * currently active and put the ZL7010X to sleep. If the ZL7010X is already
 * sleeping, it will remain asleep.
 *
 * PARAMETERS:
 *     imId:
 *         A connection ID returned by ImOpen().
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
ImSleep(IM_ID imId, EI e)
{
    IM_PRIV *im = (IM_PRIV *)imId;
    
    /* if specified id is invalid, fail */
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));
    
    /* send command and receive reply */
    if (ComCmdAndReply(im->comId, im->addr, IM_SLEEP_CMD_TYPE,
        NULL, 0, NULL, 0, e)) {
        return(ImConvertErr(e));
    }
    return(0);
}

/* This has been kept for backwards compatibility - call ImSleep() instead.
 */
RSTAT
ImAbortMics(IM_ID imId, EI e)
{
    return(ImSleep(imId, e));
}

/**************************************************************************
 * FUNCTION: ImSendEmergency
 * 
 * This function tells the implant to start sending emergency transmissions.
 * 
 * Note if the ZL7010X is sleeping, this will wake it up, and if a ZL7010X
 * operation is active, this will put the ZL7010X to sleep and rewake it (to
 * abort the operation). This also disables the ZL7010X watchdog so the ZL7010X
 * will continue to send emergency transmissions until a session starts, or
 * ImSleep() is called.
 * 
 * PARAMETERS:
 *     imId:
 *         A connection ID returned by ImOpen().
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
ImSendEmergency(IM_ID imId, EI e)
{
    IM_PRIV *im = (IM_PRIV *)imId;
    
    /* if specified id is invalid, fail */
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));
    
    /* send command and receive reply */
    if (ComCmdAndReply(im->comId, im->addr, IM_SEND_EMERGENCY_CMD_TYPE,
        NULL, 0, NULL, 0, e)) {
        return(ImConvertErr(e));
    }
    return(0);
}

/**************************************************************************
 * FUNCTION: ImReadMicsReg
 * 
 * This function reads a register in the ZL7010X on the implant.
 * 
 * The addresses for many commonly used registers are defined in
 * "ZL7010XAdk\Sw\Includes\Adk\AnyMezz\MicsHw.h". For a page 2 register,
 * specify MICS_PAGE_2 (0x80) in the register address (note 0x8000 will also
 * still work so this remains backwards compatible). The ADK software manages
 * the page selection automatically.
 * 
 * Note if the ZL7010X is sleeping, this will wake it up. The ZL7010X will
 * remain awake until its watchdog timer expires (~4.37 seconds after it is
 * awakened), or ImSleep() is called.
 * 
 * PARAMETERS:
 *     imId:
 *         A connection ID returned by ImOpen().
 *     regAddr:
 *         The address of the register to read in the ZL7010X. For a page 2
 *         register, specify MICS_PAGE_2 (0x80) in the register address (note
 *         0x8000 will also still work so this remains backwards compatible).
 *         For more information, see the function prolog.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns the value of the register (always
 *     >= 0 because each register is only 8 bits). If not successful, this
 *     function fills the specified error object and returns -1 (to get the
 *     error, see EiGetMsg(), etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
SD32
ImReadMicsReg(IM_ID imId, UD32 regAddr, EI e)
{ 
    IM_PRIV *im = (IM_PRIV *)imId;
    IM_READ_MICS_REG_CMD cmd;
    IM_READ_MICS_REG_REPLY rep;
    
    /* if specified id is invalid, fail */
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));
    
    /* for backwards compatibility (used to set 0x8000 for page 2 registers) */
    if (regAddr & 0x8000) {
        regAddr &= ~0x8000;
        regAddr |= MICS_PAGE_2;
    }
    
    /* build command */
    cmd.regAddr = (UD16)regAddr;
    
    /* if needed, swap multi-byte fields */
    if (im->swap) cmd.regAddr = SWAP16(cmd.regAddr);
    
    /* send command and receive reply */
    if (ComCmdAndReply(im->comId, im->addr, IM_READ_MICS_REG_CMD_TYPE,
        &cmd, sizeof(cmd), &rep, sizeof(rep), e)) {
        return(ImConvertErr(e));
    }
    
    /* return register value */
    return(rep.regVal);
}

/* This has been kept for backwards compatibility - new software can just call
 * ImReadMicsReg() with MICS_PAGE_2 (0x80) specified in the register address.
 */
SD32
ImReadMicsTestReg(IM_ID imId, UD32 testRegAddr, EI e)
{ 
    return(ImReadMicsReg(imId, MICS_PAGE_2 | testRegAddr, e));
}

/**************************************************************************
 * FUNCTION: ImWriteMicsReg
 * 
 * This function writes to a register in the ZL7010X on the implant.
 * 
 * The addresses for many commonly used registers are defined in
 * "ZL7010XAdk\Sw\Includes\Adk\AnyMezz\MicsHw.h". For a page 2 register,
 * specify MICS_PAGE_2 (0x80) in the register address (note 0x8000 will also
 * still work so this remains backwards compatible). The ADK software manages
 * the page selection automatically.
 * 
 * Note if the ZL7010X is sleeping, this will wake it up. The ZL7010X will
 * remain awake until its watchdog timer expires (~4.37 seconds after it is
 * awakened), or ImSleep() is called.
 * 
 * PARAMETERS:
 *     imId:
 *         A connection ID returned by ImOpen().
 *     regAddr:
 *         The address of the register to write to in the ZL7010X. For a page 2
 *         register, specify MICS_PAGE_2 (0x80) in the register address (note
 *         0x8000 will also still work so this remains backwards compatible).
 *         For more information, see the function prolog.
 *     regVal:
 *         The value to write to the register.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the 
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
ImWriteMicsReg(IM_ID imId, UD32 regAddr, UD32 regVal, EI e)
{ 
    IM_PRIV *im = (IM_PRIV *)imId;
    IM_WRITE_MICS_REG_CMD cmd;
    int curVal;
    
    /* if specified id is invalid, fail */
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));
    
    /* for backwards compatibility (used to set 0x8000 for page 2 registers) */
    if (regAddr & 0x8000) {
        regAddr &= ~0x8000;
        regAddr |= MICS_PAGE_2;
    }
    
    /* If trying to set INTERFACE_MODE.ACCESS_PAGE_2 (register page), fail. To
     * access a page 2 register, set MICS_PAGE_2 (0x80) in the address (the
     * firmware manages the page selection automatically). It actually wouldn't
     * hurt anything to set ACCESS_PAGE_2 because the implant would be aware of
     * it, but it wouldn't serve any purpose, and isn't the intended usage.
     */
    if (((regAddr & 0x7F) == INTERFACE_MODE) && (regVal & ACCESS_PAGE_2)) {
        EiSet(e, IM_LIB_EID_ATTEMPT_TO_SELECT_PAGE_2, NULL);
        return(-1);
    }
    /* If ZL70102 and trying to change HK_MODE_102.HK_ACCESS_PAGE_2 (register
     * page for housekeeping accesses by the remote base), fail. This isn't safe
     * because it might not match the page expected by the base.
     */
    if ((im->micsRev > ZL70101) && ((regAddr & 0x7F) == HK_MODE_102)) {
        
        if ((curVal = ImReadMicsReg(imId, HK_MODE_102, e)) < 0) {
            return(ImConvertErr(e));
        }
        if ((curVal & HK_ACCESS_PAGE_2) != (int)(regVal & HK_ACCESS_PAGE_2)) {
            EiSet(e, IM_LIB_EID_ATTEMPT_TO_CHANGE_HK_PAGE, NULL);
            return(-1);
        }
    }
    
    /* build command */
    cmd.regAddr = (UD16)regAddr;
    cmd.regVal = (UD8)regVal;
    
    /* if needed, swap multi-byte fields */
    if (im->swap) cmd.regAddr = SWAP16(cmd.regAddr);
    
    /* send command and receive reply */
    if (ComCmdAndReply(im->comId, im->addr, IM_WRITE_MICS_REG_CMD_TYPE,
        &cmd, sizeof(cmd), NULL, 0, e)) {
        return(ImConvertErr(e));
    }
    return(0);
}

/* This has been kept for backwards compatibility - new software can just call
 * ImWriteMicsReg() with MICS_PAGE_2 (0x80) specified in the register address.
 */
RSTAT
ImWriteMicsTestReg(IM_ID imId, UD32 testRegAddr, UD32 testRegVal, EI e)
{ 
    return(ImWriteMicsReg(imId, MICS_PAGE_2 | testRegAddr, testRegVal, e));
}

/**************************************************************************
 * FUNCTION: ImReceiveMics
 * 
 * This function reads any data contained in the RX buffer on the ZL7010X.
 * 
 * Note if the ZL7010X is sleeping, this will return 0 bytes of data.
 *
 * PARAMETERS:
 *     imId:
 *         A connection ID returned by ImOpen().
 *     buf:
 *         The buffer in which to put any data that is read.
 *     bufSize:
 *         The size of the specified buffer (bytes). Note ImReceiveMics()
 *         will read any data available in the ZL7010X, but only up to the
 *         specified buffer size. Note the implant can only read complete data
 *         blocks from the ZL7010X, so the specified buffer size must always
 *         be >= the current RX block size, and ImReceiveMics() will always
 *         read a multiple of the RX block size.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function puts any data that it reads in the
 *     specified buffer and returns the number of bytes read. Note this will
 *     always be a multiple of the current RX block size, or 0 if no data is
 *     available. If not successful, this function fills the specified error
 *     object and returns -1 (to get the error, see EiGetMsg(), etc. in
 *     "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate data buffer and error object.
 * 
 * VISIBILITY: Customer
 */
SD32
ImReceiveMics(IM_ID imId, void *buf, UD32 bufSize, EI e)
{ 
    IM_PRIV *im = (IM_PRIV *)imId;
    IM_RECEIVE_MICS_CMD cmd;
    UD32 maxLen;
    SD32 repLen;
    
    /* if specified id is invalid, fail */
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));
    
    /* don't try to read more data than one reply packet can hold */
    maxLen = bufSize < COM_MAX_PACK_LEN ? bufSize : COM_MAX_PACK_LEN;
    
    /* build command */
    cmd.maxLen = (UD16)(maxLen);
    
    /* if needed, swap multi-byte fields */
    if (im->swap) cmd.maxLen = SWAP16(cmd.maxLen);
    
    /* send command and receive variable length reply */
    if ((repLen = ComCmdAndVarReply(im->comId, im->addr,
        IM_RECEIVE_MICS_CMD_TYPE, &cmd, sizeof(cmd), buf, maxLen, e)) < 0) {
        return(ImConvertErr(e));
    }
    
    /* return number of bytes read (note this may be 0) */
    return(repLen);
}

/**************************************************************************
 * FUNCTION: ImTransmitMics
 * 
 * This function writes data to the ZL7010X's TX buffer, which will transmit
 * the data as soon as possible. If there's not enough space in the TX buffer,
 * this will wait up to 4 seconds, writing data as space becomes available. If
 * 4 seconds elapses or the ZL7010X asserts IRQ_CLOST before all of the data
 * has been written, it will time out and return an error.
 * 
 * Note if the ZL7010X is sleeping, this will wake it up. The ZL7010X will
 * remain awake until its watchdog timer expires (~4.37 seconds after it is
 * awakened), or ImSleep() is called.
 *
 * PARAMETERS:
 *     imId:
 *         A connection ID returned by ImOpen().
 *     data:
 *         The data to transmit.
 *     nBytes:
 *         The number of data bytes. Note the implant can only write complete
 *         data blocks to the ZL7010X, so the specified number of bytes must
 *         always be a multiple of the current TX block size (otherwise, this
 *         function will return an error).
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If all of the data is written to the TX buffer on the ZL7010X, this
 *     function returns 0. Otherwise, it fills the specified error object and
 *     returns -1 (to get the error, see EiGetMsg(), etc. in "AppDevPlat\Sw\
 *     Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
ImTransmitMics(IM_ID imId, const void *data, UD32 nBytes, EI e)
{ 
    IM_PRIV *im = (IM_PRIV *)imId;
    
    /* if specified id is invalid, fail */
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));
    
    /* send command and receive reply */
    if (ComCmdAndReply(im->comId, im->addr, IM_TRANSMIT_MICS_CMD_TYPE,
        data, nBytes, NULL, 0, e)) {
        return(ImConvertErr(e));
    }
    return(0);        
}

/**************************************************************************
 * FUNCTION: ImMicsCal
 * 
 * This performs one or more MICS (ZL7010X) calibrations on the implant.
 * 
 * Note if the ZL7010X is sleeping, this will wake it up. The ZL7010X
 * will remain awake until its watchdog timer expires (~4.37 seconds after
 * it is awakened), or ImSleep() is called.
 * 
 * Note if CAL_400_MHZ_ANT is specified, "remote" is false, and the
 * specified channel is >= 0 and != the current channel (MAC_CHANNEL), this
 * will abort any active ZL7010X operation before changing the channel (by
 * putting the ZL7010X to sleep and rewaking it).
 * 
 * PARAMETERS:
 *     imId:
 *         A connection ID returned by ImOpen().
 *     cals:
 *         The calibrations to perform. Each bit corresponds to a calibration
 *         in the CALSELECT1 or CALSELECT2 registers (defined in "ZL7010XAdk\
 *         Sw\Includes\Adk\AnyMezz\MicsHw.h"). The lowest 8 bits are written to
 *         CALSELECT1, and the next 8 bits are written to CALSELECT2 (ZL70102
 *         or later). To see which calibrations are supported, refer to
 *         IM_101_CALS or IM_102_CALS in "ZL7010XAdk\Sw\Includes\Adk\ImMezz\
 *         ImGeneral.h". If an unsupported calibration is specified, this
 *         function will return an error.
 *     chan:
 *         The channel to use for the CAL_400_MHZ_ANT calibration, or -1 to
 *         use the current channel (MAC_CHANNEL). Note this is only used if
 *         CAL_400_MHZ_ANT is specified.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
ImMicsCal(IM_ID imId, UD32 cals, SD32 chan, EI e)
{
    IM_PRIV *im = (IM_PRIV *)imId;
    UINT timeoutMs = COM_DEF_TIMEOUT_MS;
    UINT supportedCals, badCals;
    IM_MICS_CAL_CMD cmd;
    
    /* if specified id is invalid, fail */
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));
    
    /* get supported calibrations and override timeout if needed */ 
    if (im->micsRev == ZL70101) {
        
        supportedCals = IM_101_CALS;
        
    } else { /* ZL70102 or later */
        
        supportedCals = IM_102_CALS;
        /*
         * If CAL_400_MZ_TX_102 and/or CAL_400_MHZ_RX_102 are specified, the
         * calibration can take up to 13 seconds depending on how these
         * calibrations are configured, so set timeout to 15 seconds.
         */
        if (cals & (CAL_400_MHZ_TX_102 | CAL_400_MHZ_RX_102)) timeoutMs = 15000;
    }
    
    /* if an unsupported calibration is specified, fail */
    if ((badCals = (cals & ~supportedCals)) != 0) {
        EiSet(e, IM_LIB_EID_UNSUPPORTED_MICS_CAL, "0x%02x", badCals);
        return(-1);
    }
    
    /* build command */
    cmd.calSelect1 = (UD8)(cals);
    cmd.calSelect2 = (UD8)(cals >> 8);
    cmd.chan = (SD8)chan;
    
    /* send command and receive reply */
    if (ComCmdAndReplyT(im->comId, im->addr, IM_MICS_CAL_CMD_TYPE,
        &cmd, sizeof(cmd), NULL, 0, timeoutMs, e)) {
        return(ImConvertErr(e));
    }
    return(0);
}

/**************************************************************************
 * FUNCTION: ImMicsAdc
 * 
 * This function does an A/D conversion on the implant using the internal
 * ADC in the ZL7010X.
 * 
 * Note if the ZL7010X is sleeping, this will wake it up. The ZL7010X will
 * remain awake until its watchdog timer expires (~4.37 seconds after it is
 * awakened), or ImSleep() is called.
 * 
 * PARAMETERS:
 *     imId:
 *         A connection ID returned by ImOpen().
 *     adcInput:
 *         The A/D input to sample (see ADC_INPUT_TEST_IO_1, etc. in
 *         "ZL7010XAdk\Sw\Includes\Adk\AnyMezz\MicsHw.h").
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns the ADC (always >= 0). Otherwise,
 *     it fills the specified error object and returns -1 (to get the error,
 *     see EiGetMsg(), etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
SD32
ImMicsAdc(IM_ID imId, UD32 adcInput, EI e)
{
    IM_PRIV *im = (IM_PRIV *)imId;
    IM_MICS_ADC_CMD cmd;
    IM_MICS_ADC_REPLY rep;
    
    /* if specified id is invalid, fail */
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));
    
    /* build command */
    cmd.adcInput = (UD8)adcInput;
    
    /* send command and receive reply */
    if (ComCmdAndReply(im->comId, im->addr, IM_MICS_ADC_CMD_TYPE,
        &cmd, sizeof(cmd), &rep, sizeof(rep), e)) {
        return(ImConvertErr(e));
    }
    
    /* return result */
    return(rep.adcResult);
}

/**************************************************************************
 * FUNCTION: ImStartTx400Carrier
 * 
 * This function tells the implant to start or stop transmitting a 400 MHz
 * TX carrier (continuous wave).
 * 
 * Note if the ZL7010X is sleeping, this will wake it up, and if a ZL7010X
 * operation is active, this will put the ZL7010X to sleep and rewake it (to
 * abort the operation). The ZL7010X will remain awake until its watchdog timer
 * expires (~4.37 seconds after it is awakened), or ImSleep() is called. If you
 * want the ZL7010X to continue to transmit the carrier indefinitely, invoke
 * ImWakeup() with "stayAwake" = true before starting the carrier.
 * 
 * Note this is a test mode, not a normal operational mode. The carrier
 * should be stopped before starting any other ZL7010X operation (so it won't
 * affect the operation).
 * 
 * PARAMETERS:
 *     imId:
 *         A connection ID returned by ImOpen().
 *     start:
 *         If true, the carrier is started. If false, it's stopped.
 *     chan:
 *         The MICS channel to use to transmit the carrier.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
ImStartTx400Carrier(IM_ID imId, BOOL start, UD32 chan, EI e)
{
    IM_PRIV *im = (IM_PRIV *)imId;
    IM_START_TX_400_CARRIER_CMD cmd;
    
    /* if specified id is invalid, fail */
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));
    
    /* build command */
    cmd.start = (UD8)start;
    cmd.chan = (UD8)chan;
    
    /* send command and receive reply */
    if (ComCmdAndReply(im->comId, im->addr,
        IM_START_TX_400_CARRIER_CMD_TYPE, &cmd, sizeof(cmd), NULL, 0, e)) {
        return(ImConvertErr(e));
    }
    
    return(0);
}

/**************************************************************************
 * FUNCTION: ImIntRssi
 * 
 * This function does an RSSI (received signal strength indicator) using the
 * internal ADC in the ZL7010X.
 * 
 * Note if the ZL7010X is sleeping, this will wake it up. Also, if a
 * ZL7010X operation is active, and the specified channel is different than
 * the current channel (MAC_CHANNEL register), this will put the ZL7010X to
 * sleep and rewake it (to abort the operation before changing the channel).
 * The ZL7010X will remain awake until its watchdog timer expires (~4.37
 * seconds after it is awakened), or ImSleep() is called.
 * 
 * PARAMETERS:
 *     imId:
 *         A connection ID returned by ImOpen().
 *     chan:
 *         The MICS channel to do the RSSI for (0 to 9).
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns the RSSI (always >= 0). If not
 *     successful, it fills the specified error object and returns -1 (to get
 *     the error, see EiGetMsg(), etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
SD32
ImIntRssi(IM_ID imId, UD32 chan, EI e)
{
    IM_PRIV *im = (IM_PRIV *)imId;
    IM_INT_RSSI_CMD cmd;
    IM_INT_RSSI_REPLY rep;
    
    /* if specified id is invalid, fail */
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));
    
    /* build command */
    cmd.chan = (UD8)chan;
    
    /* send command and receive reply */
    if (ComCmdAndReply(im->comId, im->addr, IM_INT_RSSI_CMD_TYPE,
        &cmd, sizeof(cmd), &rep, sizeof(rep), e)) {
        return(ImConvertErr(e));
    }
    
    /* return result */
    return(rep.rssi);
}

/**************************************************************************
 * FUNCTION: ImEnabExtRssi
 * 
 * This function tells the implant to enable or disable the external RSSI.
 * Note there is no external RSSI hardware on an implant, but this function
 * can still be called to route the RF signals in the ZL7010X to its
 * TESTIO[5,6] pins for test and evaluation purposes. If the ZL7010X is idle,
 * this will also enable its receiver and synthesizer and wait to give them
 * time to start.
 * 
 * Note if the ZL7010X is sleeping, this will wake it up. If a ZL7010X
 * operation is active, and the specified channel is different than the current
 * channel (MAC_CHANNEL register), this will put the ZL7010X to sleep and rewake
 * it (to abort the operation before changing the channel). The ZL7010X will
 * remain awake until its watchdog timer expires (~4.37 seconds after it is
 * awakened), or ImSleep() is called. If you want the external RSSI to remain
 * enabled indefinitely, invoke ImWakeup() with "stayAwake" = true before
 * enabling the external RSSI.
 * 
 * Note this is a test mode, not a normal operational mode. The external
 * RSSI should normally be disabled because it consumes more power.
 * 
 * PARAMETERS:
 *     imId:
 *         A connection ID returned by ImOpen().
 *     enab:
 *         If true, enable the external RSSI. If false, disable it.
 *     chan:
 *         The MICS channel to use for the external RSSI.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
ImEnabExtRssi(IM_ID imId, BOOL enab, UD32 chan, EI e)
{
    IM_PRIV *im = (IM_PRIV *)imId;
    IM_ENAB_EXT_RSSI_CMD cmd;
    
    /* if specified id is invalid, fail */
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));
    
    /* build command */
    cmd.enab = (UD8)enab;
    cmd.chan = (UD8)chan;
    
    /* send command and receive reply */
    if (ComCmdAndReply(im->comId, im->addr, IM_ENAB_EXT_RSSI_CMD_TYPE,
        &cmd, sizeof(cmd), NULL, 0, e)) {
        return(ImConvertErr(e));
    }
    
    return(0);
}

/* This has been kept for backwards compatibility - use ImGetStatChanges()
 * instead. This gets just the link status from the implant (MICS_LINK_STAT).
 */
RSTAT
ImGetLinkStat(IM_ID imId, MICS_LINK_STAT *ls, UD32 lsSize, EI e)
{
    IM_PRIV *im = (IM_PRIV *)imId;
    UD8 repBuf[sizeof(MICS_LINK_STAT) + 50];
    SD32 repLen;
    
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));
    
    if ((repLen = ComCmdAndVarReply(im->comId, im->addr,
        IM_GET_LINK_STAT_CMD_TYPE, NULL, 0, repBuf, sizeof(repBuf), e)) < 0) {
        return(ImConvertErr(e));
    }
    /* if needed, swap multi-byte fields */
    if (im->swap) ImSwapLinkStat((MICS_LINK_STAT *)repBuf);
    
    if (lsSize > (UD32)repLen) return(ImSizeErr(e));
    (void)memcpy(ls, repBuf, lsSize);
        
        
    return(0);
}

/* This has been kept for backwards compatibility - use ImGetStatChanges()
 * instead. This gets just the link quality from the implant (MICS_LINK_QUAL).
 */
RSTAT
ImGetLinkQual(IM_ID imId, MICS_LINK_QUAL *lq, UD32 lqSize, EI e)
{
    IM_PRIV *im = (IM_PRIV *)imId;
    UD8 repBuf[sizeof(MICS_LINK_QUAL) + 50];
    SD32 repLen;
    
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));
    
    if ((repLen = ComCmdAndVarReply(im->comId, im->addr,
        IM_GET_LINK_QUAL_CMD_TYPE, NULL, 0, repBuf, sizeof(repBuf), e)) < 0) {
        return(ImConvertErr(e));
    }
    /* if needed, swap multi-byte fields */
    if (im->swap) ImSwapLinkQual((MICS_LINK_QUAL *)repBuf);
    
    if (lqSize > (UD32)repLen) return(ImSizeErr(e));
    (void)memcpy(lq, repBuf, lqSize);
    
    return(0);
}

/* This has been kept for backwards compatibility - use ImGetStatChanges()
 * instead. This gets just the link quality from the implant (MICS_LINK_QUAL).
 */
RSTAT
ImGetLinkIntegrity(IM_ID imId, MICS_LINK_QUAL *lq, UD32 lqSize, EI e)
{
    return(ImGetLinkQual(imId, lq, lqSize, e));
}

/**************************************************************************
 * FUNCTION: ImEnabHkWrite
 * 
 * This function tells the implant to enable or disable housekeeping write
 * access. Note in an end product, communication with an implant should be
 * secured. This command in particular is a security concern because it enables
 * a base station to write to the implant's registers via housekeeping.
 * 
 * Note if the ZL7010X is sleeping, this will wake it up. The ZL7010X will
 * remain awake until its watchdog timer expires (~4.37 seconds after it is
 * awakened), or ImSleep() is called.
 * 
 * PARAMETERS:
 *     imId:
 *         A connection ID returned by ImOpen().
 *     enab:
 *         If true, enable housekeeping write access. If false, disable it.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
ImEnabHkWrite(IM_ID imId, BOOL enab, EI e)
{
    IM_PRIV *im = (IM_PRIV *)imId;
    IM_ENAB_HK_WRITE_CMD cmd;
    
    /* if specified id is invalid, fail */
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));
    
    /* build command */
    cmd.enab = (UD8)enab;
    
    /* send command and receive reply */
    if (ComCmdAndReply(im->comId, im->addr, IM_ENAB_HK_WRITE_CMD_TYPE,
        &cmd, sizeof(cmd), NULL, 0, e)) {
        return(ImConvertErr(e));
    }
    
    return(0);
}

/**************************************************************************
 * FUNCTION: ImStartBerRx
 * 
 * This configures the implant as needed for the bit error rate test
 * (receive side), and optionally enables the receiver to start the test.
 * Note this is an ADK factory test that requires a specific setup with
 * specific test equipment, and is not intended for customer use.
 * 
 * Note this is a test mode, not a normal operational mode. This should
 * only be called when the ZL7010X is idle. When done, you should reset the
 * implant to restore the ZL7010X registers for normal operation.
 * 
 * PARAMETERS:
 *     imId:
 *         A connection ID returned by ImOpen().
 *     start:
 *         If true, the receiver is enabled. If false, it's disabled.
 *     chan:
 *         The MICS channel to use.
 *     rxMod:
 *         The RX modulation to use (see RX_MOD_2FSK_FB, etc. in
 *         "ZL7010XAdk\Sw\Includes\Adk\AnyMezz\MicsHw.h").
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
ImStartBerRx(IM_ID imId, BOOL start, UD32 chan, UD32 rxMod, EI e)
{
    IM_PRIV *im = (IM_PRIV *)imId;
    UD32 clkRecCtrlAddr;
    
    /* init variables that depend on ZL7010X revision */
    if (im->micsRev == ZL70101) {
        clkRecCtrlAddr = CLKRECCTRL_101;
    } else {
        clkRecCtrlAddr = CLKRECCTRL_102;
    }
    
    /* disable RX (in case it's enabled) */
    if (ImWriteMicsReg(imId, RF_GENENABLES, 0, e)) return(-1);
    
    /* set channel and RX modulation */
    if (ImWriteMicsReg(imId, MAC_CHANNEL, chan, e)) return(-1);
    if (ImWriteMicsReg(imId, MAC_MODUSER, rxMod, e)) return(-1);
    
    switch(rxMod & RX_MOD_MASK) {
    case RX_MOD_2FSK_FB:
    case RX_MOD_BARKER_103: {
        
        if (ImWriteMicsReg(imId, SYNC1, 0x6A, e)) return(-1);
        if (ImWriteMicsReg(imId, SYNC2, 0xE3, e)) return(-1);
        if (ImWriteMicsReg(imId, SYNC3, 0x48, e)) return(-1);
        if (ImWriteMicsReg(imId, SYNC4, 0xC5, e)) return(-1);
        if (ImWriteMicsReg(imId, SYNC5, 0xE6, e)) return(-1);
        if (ImWriteMicsReg(imId, MICS_PAGE_2 | 11, 0x03, e)) return(-1);
        if (ImWriteMicsReg(imId, MICS_PAGE_2 | 13, 0x40, e)) return(-1);
        if (ImWriteMicsReg(imId, PO0, 0, e)) return(-1);
        if (ImWriteMicsReg(imId, PO2, 0, e)) return(-1);
        if (ImWriteMicsReg(imId, PO3, 0, e)) return(-1);
        if (ImWriteMicsReg(imId, clkRecCtrlAddr, 0x0D, e)) return(-1);
        if (ImWriteMicsReg(imId, RXGENCTRL, 0x03, e)) return(-1);
        break;
        
    } case RX_MOD_2FSK: {
        
        if (ImWriteMicsReg(imId, SYNC1, 0x6A, e)) return(-1);
        if (ImWriteMicsReg(imId, SYNC2, 0xE3, e)) return(-1);
        if (ImWriteMicsReg(imId, SYNC3, 0x48, e)) return(-1);
        if (ImWriteMicsReg(imId, SYNC4, 0xC5, e)) return(-1);
        if (ImWriteMicsReg(imId, SYNC5, 0xE6, e)) return(-1);
        if (ImWriteMicsReg(imId, MICS_PAGE_2 | 11, 0x03, e)) return(-1);
        if (ImWriteMicsReg(imId, MICS_PAGE_2 | 13, 0x40, e)) return(-1);
        if (ImWriteMicsReg(imId, PO0, 0, e)) return(-1);
        if (ImWriteMicsReg(imId, PO2, 0, e)) return(-1);
        if (ImWriteMicsReg(imId, PO3, 0, e)) return(-1);
        if (ImWriteMicsReg(imId, clkRecCtrlAddr, 0x0D, e)) return(-1);
        if (ImWriteMicsReg(imId, RXGENCTRL, 0x03, e)) return(-1);
        break;
    
    } case RX_MOD_4FSK: {
        
        if (ImWriteMicsReg(imId, SYNC1, 0x36, e)) return(-1);
        if (ImWriteMicsReg(imId, SYNC2, 0x8E, e)) return(-1);
        if (ImWriteMicsReg(imId, SYNC3, 0x54, e)) return(-1);
        if (ImWriteMicsReg(imId, SYNC4, 0x6C, e)) return(-1);
        if (ImWriteMicsReg(imId, SYNC5, 0x3E, e)) return(-1);
        if (ImWriteMicsReg(imId, MICS_PAGE_2 | 11, 0x07, e)) return(-1);
        if (ImWriteMicsReg(imId, MICS_PAGE_2 | 13, 0x40, e)) return(-1);
        if (ImWriteMicsReg(imId, PO1, 0, e)) return(-1);
        if (ImWriteMicsReg(imId, PO2, 0, e)) return(-1);
        if (ImWriteMicsReg(imId, PO3, 0, e)) return(-1);
        if (ImWriteMicsReg(imId, clkRecCtrlAddr, 0x0D, e)) return(-1);
        if (ImWriteMicsReg(imId, RXGENCTRL, 0x0B, e)) return(-1);
        break;
        
    } default: {
        
        EiSet(e, IM_LIB_EID_INVALID_RX_MOD, "0x%x", rxMod);
        return(-1);
    }}
        
    /* if specified, enable RX */
    if (start) {
        if (ImWriteMicsReg(imId, RF_GENENABLES, SYNTH | RX_RF | RX_IF, e)) {
            return(-1);
        }
    }
    
    /* return success */
    return(0);
}

/**************************************************************************
 * FUNCTION: ImGetTraceMsg
 * 
 * This function gets the the next message in the trace buffer on the implant.
 * Note trace messages are generally intended for test/debug, so they're
 * normally disabled, but they can be enabled if desired. For more information
 * on tracing, see "AppDevPlat\Sw\Includes\Adp\AnyBoard\TraceLib.h".
 *
 * PARAMETERS:
 *     imId:
 *         A connection ID returned by ImOpen().
 *     buf:
 *         The buffer in which to return the next trace message (note if
 *         no more trace messages are available, the message will be a
 *         NULL-terminated empty string).
 *     bufSize:
 *         The size of the specified buffer (bytes). If the trace message
 *         won't fit in the buffer, it will be truncated (note it will still
 *         be NULL-terminated). The maximum size of a trace message depends
 *         on the TRACE_MSG_BUF_SIZE defined in "AppDevPlat\Sw\Includes\Adp\
 *         AnyBoard\TraceLib.h". It's recommended the specified buffer is >=
 *         256 bytes to ensure this will get the whole message.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function puts the NULL-terminated trace message in
 *     the specified buffer and returns 0 (note if no more trace messages are
 *     available, the message will be a NULL-terminated empty string). If not
 *     successful, it fills the specified error object and returns -1 (to get
 *     the error, see EiGetMsg(), etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate buffer and error object.
 *
 * VISIBILITY: Customer
 */
RSTAT
ImGetTraceMsg(IM_ID imId, char *buf, UD32 bufSize, EI e)
{
    IM_PRIV *im = (IM_PRIV *)imId;
    SD32 repLen;
    
    /* if specified id is invalid, fail */
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));
    
    /* send command and receive variable length reply (trace message string) */
    if ((repLen = ComCmdAndVarReply(im->comId, im->addr,
        IM_GET_TRACE_MSG_CMD_TYPE, NULL, 0, buf, bufSize, e)) < 0) {
        return(ImConvertErr(e));
    }
    
    /* make sure string is terminated (just to be safe) */
    if (repLen) {
        buf[repLen - 1] = '\0';
    } else {
        buf[0] = '\0';
    }
    
    return(0);
}

/**************************************************************************
 * FUNCTION: ImCopyMicsRegs
 * 
 * This function executes the ZL7010X "copy registers" command on the implant.
 * This copies a subset of the ZL7010X's registers to its wakeup stack so it
 * will preserve them while it's sleeping and restore them every time it's
 * awakened. This also recalculates the ZL7010X's CRC.
 * 
 * PARAMETERS:
 *     imId:
 *         A connection ID returned by ImOpen().
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
ImCopyMicsRegs(IM_ID imId, EI e)
{
    IM_PRIV *im = (IM_PRIV *)imId;
    
    /* if specified id is invalid, fail */
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));
    
    /* send command and receive reply */
    if (ComCmdAndReply(im->comId, im->addr, IM_COPY_MICS_REGS_CMD_TYPE,
        NULL, 0, NULL, 0, e)) {
        return(ImConvertErr(e));
    }
    return(0);
}

/**************************************************************************
 * FUNCTION: ImGetStatChanges
 * 
 * This function gets changes in the status for the implant. Typically, an
 * application will call this periodically, such as once a second.
 * 
 * Note some sections in the status (sc) will only be updated if they changed
 * since the last time this function was called. If "force" is specified, this
 * function will also update sections that haven't changed (except if the
 * ZL7010X is sleeping, in which case it still won't update sc->linkStat since
 * the link status isn't available). Note using "force" is inefficient, so it
 * should only be used to get an initial status. The status flags
 * (sc->genStat.flags) indicate which sections were updated, as follows:
 * 
 * IM_LINK_STAT_CHANGED:
 *     The sc->linkStat section was updated.
 * IM_LINK_QUAL_CHANGED:
 *     The sc->linkQual section was updated.
 * IM_DATA_STAT_CHANGED:
 *     The sc->dataStat section was updated.
 * IM_ERR_OCCURRED:
 *     This indicates an error occurred in the implant. In this case, the
 *     function will return 0 since it got the status ok, but it will fill the
 *     specified error object (e) with the error information from the implant.
 *     To access get the error information, see EiGetMsg(), etc. in
 *     "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c".
 * 
 * PARAMETERS:
 *     imId:
 *         A connection ID returned by ImOpen().
 *     force:
 *         If true, also update sections of the status that haven't changed.
 *         (except if the ZL7010X is sleeping, in which case it still won't
 *         update sc->linkStat since the link status isn't available). Note
 *         using "force" is inefficient, so it should only be used to get an
 *         initial status.
 *     sc:
 *         Buffer in which to return the status changes. IM_STAT_CHANGES is
 *         defined in "ZL7010XAdk\Sw\Includes\Adk\ImMezz\ImGeneral.h".
 *     scSize:
 *         The size of the specified IM_STAT_CHANGES buffer (bytes). This
 *         is used to determine if the caller is using an older version of
 *         IM_STAT_CHANGES, so it can be handled accordingly. That way,
 *         ImGetStatChanges() will remain backwards compatible with previous
 *         verions of IM_STAT_CHANGES.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If this function is successful, it puts the status in the specified
 *     buffer and returns 0. Otherwise, it fills the specified error object
 *     and returns -1 (to get the error, see EiGetMsg(), etc. in "AppDevPlat\
 *     Sw\Pc\Libs\ErrInfoLib.c").
 *
 *     Note if the implant returns IM_ERR_OCCURRED in sc->genStat.flags, this
 *     function will fill the specified error object with the error information
 *     from the implant, but in this case, the function will still return 0
 *     since it got the status ok. To access the error information, see
 *     EiGetMsg(), etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c".
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate IM_STAT_CHANGES buffer and error object.
 *
 * VISIBILITY: Customer
 */
#pragma warning(disable:4701) /* don't warn that "ls", etc. might not be init */
RSTAT
ImGetStatChanges(IM_ID imId, BOOL32 force, IM_STAT_CHANGES *sc, UD32 scSize, EI e)
{
    IM_PRIV *im = (IM_PRIV *)imId;
    IM_GET_STAT_CHANGES_CMD cmd;
    UINT len, cmdLen, offset;
    MICS_LINK_QUAL lq;
    MICS_LINK_STAT ls;
    MICS_DATA_STAT dt;
    IM_STAT gs;
    /*
     * Buffer to hold the status reply from the implant. Note this should
     * be larger than the maximum size anticipated for the status reply now
     * and in the future, so it will always hold the full reply, even if it's
     * from a newer version of the implant. That way, older versions of this
     * software will remain compatible with newer versions of the implant. If
     * an actual reply ever exceeds repBuf, ComCmdAndVarReply() will fail and
     * repBuf will have to be increased.
     */
    UD8 repBuf[sizeof(IM_STAT_CHANGES) + 200];

    /* if specified id is invalid, fail */
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));
    
    /* build command */
    if (force) {
        cmd.force = (BOOL8)force;
        cmdLen = sizeof(cmd);
    } else {
        cmdLen = 0;
    }
    
    /* get status changes from implant */
    if (ComCmdAndVarReply(im->comId, im->addr, IM_GET_STAT_CHANGES_CMD_TYPE,
        &cmd, cmdLen, repBuf, sizeof(repBuf), e) < 0) {
        return(ImConvertErr(e));
    }
    
    /* extract each section of the status that's included in the reply */
    offset = 0;
    len = repBuf[offset++];
    if (len < sizeof(gs)) return(ImSizeErr(e));
    (void)memcpy(&gs, &repBuf[offset], sizeof(gs));
    if (im->swap) ImSwapGenStat(&gs);
    offset += len;
    /**/ 
    if (gs.flags & IM_LINK_STAT_CHANGED) {
        
        len = repBuf[offset++];
        if (len < sizeof(ls)) return(ImSizeErr(e));
        (void)memcpy(&ls, &repBuf[offset], sizeof(ls));
        if (im->swap) ImSwapLinkStat(&ls);
        offset += len;
    }
    if (gs.flags & IM_LINK_QUAL_CHANGED) {
        
        len = repBuf[offset++];
        if (len < sizeof(lq)) return(ImSizeErr(e));
        (void)memcpy(&lq, &repBuf[offset], sizeof(lq));
        if (im->swap) ImSwapLinkQual(&lq);
        offset += len;
    }
    if (gs.flags & IM_DATA_STAT_CHANGED) {
        
        len = repBuf[offset++];
        if (len < sizeof(dt)) return(ImSizeErr(e));
        (void)memcpy(&dt, &repBuf[offset], sizeof(dt));
        if (im->swap) {
            dt.txBlockCount = SWAP16(dt.txBlockCount);
            dt.rxBlockCount = SWAP16(dt.rxBlockCount);
            dt.dataErrCount = SWAP16(dt.dataErrCount);
        }
        offset += len;
    }

    /* Copy each section of the status that changed into the corresponding
     * section of the specified buffer (IM_STAT_CHANGES). Note for each section,
     * if the caller expects more than what's available, fail. It's ok if the
     * caller expects less (fewer fields). That way, if new fields are added
     * to the end of a section, ImGetStatChanges() will remain compatible with
     * older software on the PC (since the caller doesn't expect the new
     * fields that were added to the end of the section, those fields simply
     * won't be returned to the caller).
     */
    #pragma warning(disable:4127) /* don't warn about if(constant expression) */
    switch(scSize) {
    case 26:
        if (sizeof(gs) < 2) return(ImSizeErr(e));
        (void)memcpy((UD8 *)sc, &gs, 2);
        /**/
        if (gs.flags & IM_LINK_STAT_CHANGED) {
            if (sizeof(ls) < 9) return(ImSizeErr(e));
            (void)memcpy((UD8 *)sc + 2, &ls, 9);
        }
        if (gs.flags & IM_LINK_QUAL_CHANGED) {
            if (sizeof(lq) < 8) return(ImSizeErr(e));
            (void)memcpy((UD8 *)sc + 12, &lq, 8);
        }
        if (gs.flags & IM_DATA_STAT_CHANGED) {
            if (sizeof(dt) < 6) return(ImSizeErr(e));
            (void)memcpy((UD8 *)sc + 20, &dt, 6);
        }
        break;
    default:
        return(ImSizeErr(e));
    }
    #pragma warning(default:4127)

    /* if error occurred on board, get error & set error info for caller */
    if (gs.flags & IM_ERR_OCCURRED) {
        if (ComGetDevErr(im->comId, im->addr, e)) return(-1);
        (void)ImConvertErr(e);
    }
    
    return(0);
}
#pragma warning(default:4701)

/**************************************************************************
 * FUNCTION: ImStartDataTest
 * 
 * This function tells the implant to start a ZL7010X data test. Note if a
 * session is already active, the test will use the current session. Otherwise,
 * this will put the ZL7010X to sleep right away so it will be ready when the
 * base tries to start a session for the test.
 * 
 * PARAMETERS:
 *     imId:
 *         A connection ID returned by ImOpen().
 *     specs:
 *         The data test specifications (for description, see MICS_DT_SPECS in
 *         "ZL7010XAdk\Sw\Includes\Adk\AnyMezz\MicsGeneral.h").
 *     specsSize:
 *         The size of the specified MICS_DT_SPECS structure (bytes). Note
 *         ImStartDataTest() won't reference anything in the structure beyond
 *         the specified size. That way, if new fields are added to the end of
 *         MICS_DT_SPECS, this function will remain compatible with older
 *         applications that don't specify the new fields (defaults will be
 *         used for the new fields).
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
ImStartDataTest(IM_ID imId, const MICS_DT_SPECS *specs, UD32 specsSize, EI e)
{
    IM_PRIV *im = (IM_PRIV *)imId;
    IM_START_DATA_TEST_CMD cmd;
    UINT cmdLen;
    
    /* if specified id is invalid, fail */
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));
    
    /* Check size of passed specifications. Note the specified size can be
     * less than the current size of MICS_DT_SPECS. That way, if new fields
     * are added to the end of MICS_DT_SPECS, this interface will remain
     * compatible with older software on the PC.
     */
    if (specsSize > sizeof(cmd)) return(ImSizeErr(e));
    
    /* build command and swap multi-byte fields (if needed) */
    memcpy(&cmd, specs, specsSize);
    cmdLen = specsSize;
    if (im->swap) {
        cmd.txBlockCount = SWAP16(cmd.txBlockCount);
    }
    
    /* send command and receive reply */
    if (ComCmdAndReply(im->comId, im->addr, IM_START_DATA_TEST_CMD_TYPE,
        &cmd, cmdLen, NULL, 0, e)) {
        return(ImConvertErr(e));
    }
    return(0);
}

/**************************************************************************
 * FUNCTION: ImResetMics
 *
 * This function resets and re-initializes the ZL7010X on the implant.
 *
 * PARAMETERS:
 *     imId:
 *         A connection ID returned by ImOpen().
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 *
 * VISIBILITY: Customer
 */
RSTAT
ImResetMics(IM_ID imId, EI e)
{
    IM_PRIV *im = (IM_PRIV *)imId;

    /* if specified id is invalid, fail */
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));

    /* send command and receive reply */
    if (ComCmdAndReply(im->comId, im->addr, IM_RESET_MICS_CMD_TYPE,
        NULL, 0, NULL, 0, e)) {
        return(ImConvertErr(e));
    }
    return(0);
}

/**************************************************************************
 * FUNCTION: ImCalcMicsCrc
 *
 * This function calculates the CRC for the ZL7010X on the implant.
 *
 * PARAMETERS:
 *     imId:
 *         A connection ID returned by ImOpen().
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 *
 * VISIBILITY: Customer
 */
RSTAT
ImCalcMicsCrc(IM_ID imId, EI e)
{
    IM_PRIV *im = (IM_PRIV *)imId;

    /* if specified id is invalid, fail */
    if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));

    /* send command and receive reply */
    if (ComCmdAndReply(im->comId, im->addr, IM_CALC_MICS_CRC_CMD_TYPE,
        NULL, 0, NULL, 0, e)) {
        return(ImConvertErr(e));
    }
    return(0);
}

/**************************************************************************
* FUNCTION: ImGetMicsFactNvm
*
* This function gets the current factory NVM data from the implant.
*
* PARAMETERS:
*     imId:
*         A connection ID returned by ImOpen().
*     fn:
*         Buffer in which to return the MICS_FACT_NVM structure. IM_MICS_FACT_NVM
*         is defined in "ZL7010XAdk\Sw\Includes\Adk\ImMezz\ImGeneral.h".
*     fnSize:
*         The size of the specified buffer (bytes). Note ImGetMicsFactNvm()
*         won't write beyond the specified size. That way, if new fields are
*         added to the end of IM_MICS_FACT_NVM, this function will remain
*         compatible with older applications that don't use the new fields.
*     e:
*         Error object to use to return error information if the function
*         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
*
* RETURN VALUES:
*     If successful, this function puts the NVM structure in the
*     specified buffer and returns 0. If not successful, it fills the
*     specified error object and returns -1 (to get the error, see EiGetMsg(),
*     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
*
* THREAD SAFE:
*     Multiple threads may call this function, but each thread should specify
*     its own separate IM_MICS_FACT_NVM buffer and error object.
*
* VISIBILITY: Customer
*/
RSTAT
ImGetMicsFactNvm(IM_ID imId, IM_MICS_FACT_NVM *fn, UD32 fnSize, EI e)
{
	IM_PRIV *im = (IM_PRIV *)imId;
	SD32 fnLen;

	union {
		IM_MICS_FACT_NVM fn;
		UD8 buf[64];
	} fnBuf;

	/* if specified id is invalid, fail */
	if ((im == NULL) || (im->magic != IM_MAGIC)) return(ImIdErr(e));

	/* Get the factory NVM from the implant. Note fnBuf should always be
	* larger than the IM_MICS_FACT_NVM structure, so this will always read the
	* full structure available from the board, whatever the board's version. If
	* IM_MICS_FACT_NVM ever outgrows fnBuf, the worst that will happen is
	* ComCmdAndVarReply() will fail during development and fnBuf will have
	* to be increased.
	*/
	if ((fnLen = ComCmdAndVarReply(im->comId, IM_LOCAL_ADDR,
		IM_GET_MICS_FACT_NVM_CMD_TYPE, NULL, 0, &fnBuf, sizeof(fnBuf), e)) < 0) {
		return(ImConvertErr(e));
	}

	/* If the caller expects a larger config than is available, fail. It's ok
	* if the caller expects a smaller config (fewer fields). That way, if new
	* fields are added to the end of IM_MICS_FACT_NVM, this interface will
	* remain compatible with older software on the PC - since the caller
	* doesn't expect the new fields that were added to the end of
	* IM_MICS_FACT_NVM, those fields simply won't be returned to the caller.
	*/
	if (fnSize > (UD32)fnLen) return(ImSizeErr(e));
	(void)memcpy(fn, &fnBuf, fnSize);

	return(0);
}

/**************************************************************************
* FUNCTION: ImCheckNvm
*
* This checks if the specified im NVM ("nvm") is valid, and if it is the
* same version (size) as the current IM_NVM structure for the host (PC)
* software. If so, it returns 0. Otherwise, it fills the specified error
* object and returns a negative value to indicate the cause of the error
* (see RETURN VALUES below).
*
* PARAMETERS:
*     nvm:
*         The im NVM to check. Note that this is a void pointer instead
*         of an IM_NVM pointer because the specified NVM might be an older
*         (smaller) or a newer (larger) version that is not the same size as
*         the current IM_NVM structure for the host (PC) software.
*     e:
*         Error object to use to return error information if the function
*         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
*
* RETURN VALUES:
*     If the specified NVM is valid, and is the same version (size) as the
*     current IM_NVM structure for the host (PC) software, this function
*     returns 0 (IM_NVM_IS_VALID). If the specified NVM is an older (smaller)
*     version of the structure, this function fills the specified error object
*     and returns IM_NVM_IS_OLDER_VER. If any other error occurs (e.g. the
*     specified NVM is invalid, or is a newer (larger) version that the host
*     (PC) software does not support), this function fills the specified error
*     object and returns IM_NVM_OTHER_ERR. To get the error information,
*     see EiGetMsg(), etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c".
*
* THREAD SAFE:
*     Multiple threads may call this function, but each thread should specify
*     it's own separate error object, or protect it with a semaphore to ensure
*     only one thread uses it at a time.
*/
#pragma warning(disable:4305) /* don't warn about cast in IM_NVM_OFFSET() */
IM_NVM_RSTAT
ImCheckNvm(void *nvm, EI e)
{
    IM_NVM_HEAD *h;
    UD16 nvmSize, sectSize, sectOffset;

    /* init pointer to NVM header */
    h = (IM_NVM_HEAD *)nvm;

    /* if the NVM header is invalid, fail */
    if (GET_UD32(&h->nvmInfo.nvmInitKey) != NVM_INIT_KEY) {
        EiSet(e, IM_LIB_EID_NVM_IS_NOT_INIT, NULL);
        return(IM_NVM_OTHER_ERR);
    }
    if (GET_UD16(&h->nvmInfo.nvmType) != NVM_TYPE_10X_ADK_IM) {
        EiSet(e, IM_LIB_EID_NVM_IS_WRONG_TYPE, NULL);
        return(IM_NVM_OTHER_ERR);
    }
    if ((nvmSize = GET_UD16(&h->nvmInfo.nvmSize)) > sizeof(IM_NVM)) {
        EiSet(e, IM_LIB_EID_NVM_IS_NEWER_VER, NULL);
        return(IM_NVM_OTHER_ERR);
    }
    if ((h->nvmHeadSize == 0) || (h->nvmHeadSize >= nvmSize) ||
        (h->nvmHeadSize > sizeof(IM_NVM_HEAD))) {
        EiSet(e, IM_LIB_EID_NVM_HEAD_IS_INVALID, NULL);
        return(IM_NVM_OTHER_ERR);
    }

    /* if NVM contains micsFactNvm section, check its size & offset */
    if (h->nvmHeadSize > IM_NVM_OFFSET(head.micsFactNvmOffset)) {

        /* get offset & size for micsFactNvm section from NVM header */
        sectOffset = h->micsFactNvmOffset;
        sectSize = h->micsFactNvmSize;

        /* if size or offset for micsFactNvm section is invalid, fail */
        if ((sectSize == 0) || (sectSize > sizeof(IM_MICS_FACT_NVM)) ||
            (sectOffset < h->nvmHeadSize) || ((sectOffset + sectSize) > nvmSize)) {

            EiSet(e, IM_LIB_EID_NVM_HEAD_IS_INVALID, NULL);
            return(IM_NVM_OTHER_ERR);
        }
    }

    /* If the NVM is an older (smaller) version, return a unique error value
    * so the caller can detect it if desired.
    */
    if (nvmSize < sizeof(IM_NVM)) {
        EiSet(e, IM_LIB_EID_NVM_IS_OLDER_VER, NULL);
        return(IM_NVM_IS_OLDER_VER);
    }

    return(IM_NVM_IS_VALID);
}

/*****************************************************************************
* FUNCTION ImTransferNvm
*
* This transfers the settings from the specified previous NVM ("prevNvm")
* to the specified IM_NVM structure ("nvm"). Note that the caller must
* initialize everything in the IM_NVM structure before passing it to this
* function, including the NVM headers, and default values for all settings.
* If the previous NVM is an older (smaller) version of the IM_NVM structure
* that does not contain all of the settings that exist in the current IM_NVM
* structure, this function leaves the new settings in the IM_NVM structure
* at the default values initialized by the caller. If an error occurs (e.g.
* one of the NVMs is invalid, or the previous NVM is a newer (larger) version
* that the host (PC) software doesn't support), this fills the specified error
* object and returns -1.
*
* PARAMETERS:
*     prevNvm:
*         The previous NVM. Note that this is a void pointer instead of a
*         IM_NVM pointer because the previous NVM might be an older (smaller)
*         or a newer (larger) version that is not the same size as the current
*         IM_NVM structure for the host (PC) software.
*     nvm:
*         The IM_NVM structure to transfer the previous NVM into.
*     nvmSize:
*         The size of the specified IM_NVM structure (bytes).
*     e:
*         Error object to use to return error information if the function
*         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
*
* RETURN VALUES:
*     If successful, this function returns 0. If an error occurs (e.g. one
*     of the NVMs is invalid, or the previous NVM is a newer (larger) version
*     that the host (PC) software does not support), this function fills the
*     specified error object and returns -1 (to get the error information,
*     see EiGetMsg(), etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
*
* THREAD SAFE:
*     Multiple threads may call this function, but each thread should specify
*     it's own separate IM_NVM structure and error object, or protect them
*     with a semaphore to ensure only one thread uses them at a time.
*/
#pragma warning(disable:4305) /* don't warn about cast in IM_NVM_OFFSET() */
RSTAT
ImTransferNvm(void *prevNvm, IM_NVM *nvm, UD32 nvmSize, EI e)
{
    IM_NVM_HEAD *prevH;
    IM_NVM_RSTAT nnrStat;
    UD16 prevSectSize;
    void *prevSect;

    /* if the NVM size for the caller & this API don't match, fail */
    if (nvmSize != sizeof(IM_NVM)) return(ImSizeErr(e));

    /* if previous NVM isn't valid and isn't an older version, fail */
    if (((nnrStat = ImCheckNvm(prevNvm, e)) != IM_NVM_IS_VALID) &&
        (nnrStat != IM_NVM_IS_OLDER_VER)) {
        return(-1);
    }

    /* if new NVM isn't valid, fail */
    if (ImCheckNvm(nvm, e)) return(-1);

    /* init pointer to previous NVM header */
    prevH = (IM_NVM_HEAD *)prevNvm;

    /* if previous NVM contains micsFactNvm section, copy it to new NVM */
    if (prevH->nvmHeadSize > IM_NVM_OFFSET(head.micsFactNvmOffset)) {

        prevSect = (UD8 *)prevNvm + prevH->micsFactNvmOffset;
        prevSectSize = prevH->micsFactNvmSize;
        /**/
        if (prevSectSize > sizeof(nvm->micsFactNvm)) {
            EiSet(e, IM_LIB_EID_NVM_HEAD_IS_INVALID, NULL);
            return(-1);
        }
        (void)memcpy(&nvm->micsFactNvm, prevSect, prevSectSize);
    }

    return(0);
}

#pragma warning(disable:4100) /* don't warn that "genStat" isn't referenced */
static void
ImSwapGenStat(IM_STAT *genStat)
{
    /* currently, IM_STAT doesn't have any multi-byte fields to swap */
    return;
}
#pragma warning(default:4100)

#pragma warning(disable:4100) /* don't warn that "ls" isn't referenced */
static void
ImSwapLinkStat(MICS_LINK_STAT *ls)
{
    /* currently, MICS_LINK_STAT doesn't have any multi-byte fields to swap */
    return;
}
#pragma warning(default:4100)

static void
ImSwapLinkQual(MICS_LINK_QUAL *lq)
{
    lq->maxBErrInts = SWAP16(lq->maxBErrInts);
    lq->maxRetriesInts = SWAP16(lq->maxRetriesInts);
    lq->errCorBlocks = SWAP16(lq->errCorBlocks);
    lq->crcErrs = SWAP16(lq->crcErrs);
}

static RSTAT
ImSizeErr(EI e)
{
    EiSet(e, IM_LIB_EID_INVALID_STRUCT_SIZE, NULL);
    return(-1);
}

static RSTAT
ImIdErr(EI e)
{
    EiSet(e, IM_LIB_EID_INVALID_IM_ID, NULL);
    return(-1);
}

/* If the specified error is from the implant, this will convert the error
 * so it contains a specific error message (errors from the implant only
 * contain a generic message). Note this will only change the error's message,
 * not the error group or code. This always returns -1 so it can be passed to
 * a return statement, if desired.
 */
static RSTAT
ImConvertErr(EI e)
{
    const char *eid;

    /* get original error ID string */
    eid = EiGetId(e);

    /* If it's an error from the implant, convert it to a new error ID string
     * that contains a specific error message.
     */
    if (EiGroupIs(e, IM_APP_ERR)) {

        /* extract error code from original error ID string and convert it */
        switch (atoi(&eid[sizeof(IM_APP_ERR)])) {
        case IM_APP_ERR_MICS_DATA_NOT_YET_SUPPORTED:
            eid = IM_APP_EID_MICS_DATA_NOT_YET_SUPPORTED; break;
        case IM_APP_ERR_RECEIVED_CMD_WITH_INVALID_CMD_TYPE:
            eid = IM_APP_EID_RECEIVED_CMD_WITH_INVALID_CMD_TYPE; break;
        case IM_APP_ERR_RECEIVED_CMD_WITH_INVALID_LEN:
            eid = IM_APP_EID_RECEIVED_CMD_WITH_INVALID_LEN; break;
        case IM_APP_ERR_MICS_DATA_RX_TIMEOUT:
            eid = IM_APP_EID_MICS_DATA_RX_TIMEOUT; break;
        case IM_APP_ERR_MICS_DATA_TX_TIMEOUT:
            eid = IM_APP_EID_MICS_DATA_TX_TIMEOUT; break;
        case IM_APP_ERR_MICS_HK_RX_TIMEOUT:
            eid = IM_APP_EID_MICS_HK_RX_TIMEOUT; break;
        case IM_APP_ERR_MICS_HK_TX_TIMEOUT:
            eid = IM_APP_EID_MICS_HK_TX_TIMEOUT; break;
        case IM_APP_ERR_ADP_SPI_RX_TIMEOUT:
            eid = IM_APP_EID_ADP_SPI_RX_TIMEOUT; break;
        case IM_APP_ERR_ADP_SPI_TX_TIMEOUT:
            eid = IM_APP_EID_ADP_SPI_TX_TIMEOUT; break;
        case IM_APP_ERR_RECEIVED_PACK_WITH_DEST_0:
            eid = IM_APP_EID_RECEIVED_PACK_WITH_DEST_0; break;
        case IM_APP_ERR_MICS_INT_WITH_NO_STAT:
            eid = IM_APP_EID_MICS_INT_WITH_NO_STAT; break;
        case IM_APP_ERR_MICS_CRC_ERR:
            eid = IM_APP_EID_MICS_CRC_ERR; break;
        case IM_APP_ERR_INVALID_TX_LEN:
            eid = IM_APP_EID_INVALID_TX_LEN; break;
        case IM_APP_ERR_MICS_SYNTH_FAILED_TO_LOCK:
            eid = IM_APP_EID_MICS_SYNTH_FAILED_TO_LOCK; break;
        case IM_APP_ERR_MICS_WAKEUP_TIMEOUT:
            eid = IM_APP_EID_MICS_WAKEUP_TIMEOUT; break;
        case IM_APP_ERR_WRONG_MICS_REV:
            eid = IM_APP_EID_WRONG_MICS_REV; break;
        default:
            eid = NULL; break; /* don't convert error */
        }

    } else if (EiGroupIs(e, MICS_LIB_ERR)) {

        /* extract error code from original error ID string and convert it */
        switch (atoi(&eid[sizeof(MICS_LIB_ERR)])) {
        case MICS_LIB_ERR_REMOTE_HK_TIMEOUT:
            eid = MICS_LIB_EID_REMOTE_HK_TIMEOUT; break;
        case MICS_LIB_ERR_REMOTE_HK_WRITES_DISABLED:
            eid = MICS_LIB_EID_REMOTE_HK_WRITES_DISABLED; break;
        case MICS_LIB_ERR_REMOTE_ADC_TIMEOUT:
            eid = MICS_LIB_EID_REMOTE_ADC_TIMEOUT; break;
        case MICS_LIB_ERR_ABORT_LINK_TIMEOUT:
            eid = MICS_LIB_EID_ABORT_LINK_TIMEOUT; break;
        case MICS_LIB_ERR_CALC_CRC_TIMEOUT:
            eid = MICS_LIB_EID_CALC_CRC_TIMEOUT; break;
        case MICS_LIB_ERR_MAC_CMD_TIMEOUT:
            eid = MICS_LIB_EID_MAC_CMD_TIMEOUT; break;
        case MICS_LIB_ERR_ADC_TIMEOUT:
            eid = MICS_LIB_EID_ADC_TIMEOUT; break;
        case MICS_LIB_ERR_WAKEUP_TIMEOUT:
            eid = MICS_LIB_EID_MICS_WAKEUP_TIMEOUT; break;
        case MICS_LIB_ERR_ATTEMPT_TO_ACCESS_REMOTE_PAGE_2_VIA_HK:
            eid = MICS_LIB_EID_ATTEMPT_TO_ACCESS_REMOTE_PAGE_2_VIA_HK; break;
        case MICS_LIB_ERR_TIMED_OUT_WAITING_FOR_TX_MODE:
            eid = MICS_LIB_EID_TIMED_OUT_WAITING_FOR_TX_MODE; break;
        case MICS_LIB_ERR_ZL7010X_DID_NOT_SET_SYNTH_CT_READY:
            eid = MICS_LIB_EID_ZL7010X_DID_NOT_SET_SYNTH_CT_READY; break;
        case MICS_LIB_ERR_SYNTH_DID_NOT_LOCK_FOR_SOME_CHAN:
            eid = MICS_LIB_EID_SYNTH_DID_NOT_LOCK_FOR_SOME_CHAN; break;
        case MICS_LIB_ERR_ZL7010X_DID_NOT_SET_SYNTH_CTRIM_READY:
            eid = MICS_LIB_EID_ZL7010X_DID_NOT_SET_SYNTH_CTRIM_READY; break;
        case MICS_LIB_ERR_SYNTH_CTRIM_READY_NOT_SET:
            eid = MICS_LIB_EID_SYNTH_CTRIM_READY_NOT_SET; break;
        default:
            eid = NULL; break; /* don't convert error */
        }

    } else { /* unknown error group */

        eid = NULL; /* don't convert error */
    }
    /* If there's a new error ID string for the error, call EiSet() to update
     * the error information. Note the error arguments string (returned by
     * EiGetArgs()) is passed as a single string via "%s". This won't change
     * the arguments string, so the new error information will still contain
     * the same arguments string.
     */
    if (eid != NULL) EiSet(e, eid, "%s", EiGetArgs(e));

    return(-1);
}
