/**************************************************************************
 * FILE: BsmLib.c
 * 
 * This file contains functions to communicate with a ZL7010X ADK base station
 * mezzanine board. For usage examples, see "ZL7010XAdk\Sw\Pc\Examples\
 * BsmExample.c"
 *
 * NOTES:
 *     None.
 * 
 * VISIBILITY: Customer
 *
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Standard/String.h"         /* memset(), NULL */
#include "Standard/StdLib.h"         /* calloc(), atoi(), ... */
#include "Standard/Malloc.h"         /* calloc() */
#include "Adp/General.h"             /* UINT, RSTAT, BOOL, TRUE/FALSE, ... */
#include "Adp/Build/Build.h"         /* UD8, UD32, ... */
#include "Adp/Pc/ErrInfoLib.h"       /* EI, EiSet() */
#include "Adp/Pc/ComLib.h"           /* ComCmdAndReply(), ... */ 
#include "Adp/Pc/VerLib.h"           /* VerIsCompat() */
#include "Adk/AdkVer.h"              /* ADK_VER */
#include "Adk/AnyMezz/MicsHw.h"      /* MAX_MICS_CHAN, MICS_PAGE_2, ... */
#include "Adk/AnyMezz/MicsLib.h"     /* MICS_LIB_ERR, ... */
#include "Adk/AnyMezz/MicsGeneral.h" /* MICS_LINK_STAT, MICS_LINK_QUAL, ... */
#include "Adk/BsmMezz/BsmCom.h"      /* BSM_LOCAL_ADDR, ... */
#include "Adk/BsmMezz/BsmGeneral.h"  /* BSM_STAT, BSM_MICS_CONFIG, BSM100, ... */
#include "Adk/BsmMezz/BsmAppErrs.h"  /* BSM_APP_ERR, ... */
#include "Adk/Pc/BsmLib.h"           /* BsmLib public include */

/**************************************************************************
 * Defines and Macros
 */
 
/* magic number used to validate data structures (ascii "BSM ") */
#define BSM_MAGIC  0x42534D20

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* Structure of private data for the base station library.
 */
typedef struct {
    
    /* magic number used to validate data structure */
    volatile UD32 magic;
    
    /* Communication interface to use to communicate with the base station
     * mezzanine board. This is the connection to the local ADP board attached
     * to the base station mezzanine board (returned by AdpGetComId()).
     */
    COM_ID comId;
    
    /* if true, swap multi-byte fields sent to & received from base station */
    BOOL swap;
    
    /* revision of ZL7010X on base station */
    int micsRev;
    
    /* revision of ZL7010X on remote implant (ZL70101, ZL70102, ...) */
    int micsRevOnImplant;
    
} BSM_PRIV;
 
/**************************************************************************
 * 
 * Global and Static Definitions
 */
 
/**************************************************************************
 * Function Prototypes
 */
    
static RSTAT BsmIdErr(EI e);
static RSTAT BsmSizeErr(EI e);
static RSTAT BsmConvertErr(EI e);
static void BsmSwapGenStat(BSM_STAT *genStat);
static void BsmSwapLinkStat(MICS_LINK_STAT *ls);
static void BsmSwapLinkQual(MICS_LINK_QUAL *lq);

/**************************************************************************
 * FUNCTION: BsmInitSpecs
 * 
 * This function initializes a BSM_SPECS structure with default values. The
 * structure may then be modified as desired and passed to BsmOpen().
 *
 * PARAMETERS:
 *     specs:
 *         The BSM_SPECS structure to initialize. BSM_SPECS is defined in
 *         "ZL7010XAdk\Sw\Includes\Adk\Pc\BsmLib.h".
 *     specsSize:
 *         The size of the specified BSM_SPECS structure (bytes). Note
 *         BsmInitSpecs() won't write to anything in the structure beyond the
 *         specified size. That way, if new fields are added to the end of
 *         BSM_SPECS, this function will remain compatible with older
 *         applications that don't use the new fields.
 *
 * RETURN VALUES:
 *     None.
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate BSM_SPECS structure.
 *
 * VISIBILITY: Customer
 */
void
BsmInitSpecs(BSM_SPECS *specs, UD32 specsSize)
{
    BSM_SPECS defSpecs;
    
    /* init default specs with default values for everything */
    (void)memset(&defSpecs, 0, sizeof(defSpecs));
    defSpecs.magic = BSM_MAGIC;
    
    /* copy default specs into caller's specs (up to specified size) */
    if (specsSize <= sizeof(defSpecs)) {
        (void)memcpy(specs, &defSpecs, specsSize);
    } else {
        (void)memset(specs, 0, specsSize);
        (void)memcpy(specs, &defSpecs, sizeof(defSpecs));
    }
}

/**************************************************************************
 * FUNCTION: BsmOpen
 * 
 * This function opens a connection to a ZL7010X ADK base station mezzanine
 * board. It returns an BSM_ID that may be passed to the other functions in
 * this library to communicate with the base station.
 * 
 * Note only one connection should be opened for each base station, but it
 * can be shared by multiple threads if desired (semaphores are used to
 * coordinate communication so the threads won't conflict).
 *
 * PARAMETERS:
 *     specs:
 *         Specifications for the connection (see BSM_SPECS in "ZL7010XAdk\Sw\
 *         Includes\Adk\Pc\BsmLib.h"), or NULL to use defaults. Note before
 *         passing a BSM_SPECS structure to BsmOpen(), it must be initialized
 *         using BsmInitSpecs() and modified as desired.
 *     specsSize:
 *         The size of the specified BSM_SPECS structure (bytes). Note BsmOpen()
 *         won't reference anything in the structure beyond the specified size.
 *         That way, if new fields are added to the end of BSM_SPECS, this
 *         function will remain compatible with older applications that don't
 *         use the new fields.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns a non-NULL ID for the connection.
 *     Otherwise, it fills the specified error object and returns NULL (to get
 *     the error information, see EiGetMsg(), etc. in "AppDevPlat\Sw\Pc\Libs\
 *     ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but only one connection should
 *     be opened for each base station (the connection may be shared by multiple
 *     threads if desired). Each thread should also specify its own separate
 *     error object.
 *
 * VISIBILITY: Customer
 */
BSM_ID
BsmOpen(const BSM_SPECS *specs, UD32 specsSize, EI e)
{
    char bsmMezzVer[BSM_MEZZ_VER_BUF_SIZE];
    BSM_SPECS specsBuf;
    BSM_PRIV *bsm;
    BSM_ID bsmId;
    
    /* init resources referenced in error cleanup */
    bsm = NULL;
    bsmId = NULL;
    
    /* Init local specs buf with default values, then overwrite the defaults
     * with the passed specs (up to the specified size). That way, if fields
     * are added to the specs in the future, and the calling application isn't
     * updated, the new fields will still have valid default values.
     */
    BsmInitSpecs(&specsBuf, sizeof(specsBuf));
    if (specs != NULL) {
        
        /* if passed specs not initialized by BsmInitSpecs() or too big, fail */
        if ((specs->magic != BSM_MAGIC) || (specsSize > sizeof(specsBuf))) {
            EiSet(e, BSM_LIB_EID_INVALID_SPECS, NULL);
            goto error;
        }
        (void)memcpy(&specsBuf, specs, specsSize);
    }
    specs = &specsBuf;
    
    /* Allocate memory for private data and clear all resources in it so
     * the free function can check if each resource was allocated before trying
     * to free it. This way the free function can be called to clean up if
     * an error is detected during the remainder of the creation.
     */
    if ((bsm = calloc(1, sizeof(*bsm))) == NULL) {
        EiSet(e, BSM_LIB_EID_FAILED_TO_ALLOCATE_MEM, "%d", sizeof(*bsm));
        goto error;
    }
    bsm->magic = BSM_MAGIC;
    bsmId = (BSM_ID)bsm;
    
    /* if no local ADP board connection specified, fail */ 
    if (specs->localAdpId == NULL) {
        EiSet(e, BSM_LIB_EID_NO_LOCAL_ADP_ID, NULL);
        goto error;
    }
    /* get communication interface for local ADP board */
    if ((bsm->comId = AdpGetComId(specs->localAdpId, e)) == NULL) goto error;
    
    /* get version string from base station mezzanine board */
    if (BsmGetMezzVer(bsmId, bsmMezzVer, sizeof(bsmMezzVer), e)) goto error;
    /*
     * Check if the version string contains a board model supported by this
     * library. This also verifies we're talking to a base station.
     */
    if ((strstr(bsmMezzVer, BSM100_NAME) == NULL) &&
        (strstr(bsmMezzVer, BSM200_NAME) == NULL) &&
        (strstr(bsmMezzVer, BSM300_NAME) == NULL)) {
        EiSet(e, BSM_LIB_EID_INCOMPATIBLE_BOARD_MODEL, "%s", bsmMezzVer);
        goto error;
    }
    /* Check if the base station mezzanine board version is compatible with the
     * version required by this library (ADK_VER).
     */
    if (VerIsCompat(bsmMezzVer, ADK_VER) == FALSE) {
        EiSet(e, BSM_LIB_EID_INCOMPATIBLE_BSM_MEZZ_VER, "%s\f%s",
            bsmMezzVer, ADK_VER);
        goto error;
    }
    
    /* read revision from ZL7010X */
    if ((bsm->micsRev = BsmReadMicsReg(bsmId, MICS_REVISION, FALSE, e)) < 0) {
        goto error;
    }
    
    /* set default for revision of ZL7010X on remote implant */
    bsm->micsRevOnImplant = ZL70102;
        
    return(bsmId);
    
error:
    /* free any resources allocated up to where the error occurred */
    if (bsm != NULL) (void)BsmClose((BSM_ID)bsm, NULL);
    
    return(NULL);
}

/**************************************************************************
 * FUNCTION: BsmClose
 * 
 * This function closes a connection that was opened by BsmOpen(), freeing any
 * memory and other resources allocated for it. Note this won't close the local
 * ADP board connection that was passed to BsmOpen() (see BSM_SPECS.localAdpId
 * in "ZL7010XAdk\Sw\Includes\Adk\Pc\BsmLib.h"). Thus, the local ADP board
 * connection must be closed separately if desired (see AdpClose() in
 * "AppDevPlat\Sw\Pc\Libs\AdpLib.c").
 *
 * PARAMETERS:
 *     bsmId:
 *         The ID of the connection to close (an ID returned by BsmOpen()).
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     A connection should only be closed once by a single thread, and after
 *     it's closed, the connection ID should no longer be used by any thread
 *     (if it is, the called function will detect it and return an error).
 *     Each thread should also specify its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
BsmClose(BSM_ID bsmId, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    RSTAT rStat = 0;
    
    /* if specified id is invalid, don't try to close it */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    /* Invalidate the magic number so we'll detect any attempt to use the ID
     * after it has been closed.
     */
    bsm->magic = 0;
    free(bsm);
    return(rStat);
}

/**************************************************************************
 * FUNCTION: BsmGetMezzVer
 * 
 * This function gets the version string for the base station mezzanine board.
 * For the format of the version string, see the notes for each release in the
 * "ZL7010X ADK Release Notes" (included with the ADK documentation).
 *
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     buf:
 *         Buffer in which to return the version string.
 *     bufSize:
 *         The size of the specified buffer (bytes). To ensure the version
 *         string will fit in the buffer, the buffer should be at least
 *         BSM_MEZZ_VER_BUF_SIZE bytes (defined in "ZL7010XAdk\Sw\Includes\Adk\
 *         Pc\BsmLib.h"). If the version string won't fit in the buffer, it
 *         will be truncated (note it will still be NULL-terminated).
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function puts the NULL-terminated version string
 *     in the specified buffer and returns 0. If not successful, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate version string buffer and error object.
 *
 * VISIBILITY: Customer
 */
RSTAT
BsmGetMezzVer(BSM_ID bsmId, char *buf, UD32 bufSize, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    
    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    if (ComGetDevVer(bsm->comId, BSM_LOCAL_ADDR, buf, bufSize, e)) {
        return(BsmConvertErr(e));
    }
    return(0);
}

/* This has been kept for backwards compatibility - use BsmGetStatChanges()
 * instead. This gets just the general status from the base station (BSM_STAT).
 */
RSTAT
BsmGetStat(BSM_ID bsmId, BSM_STAT *genStat, UD32 genStatSize, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    UD8 repBuf[sizeof(BSM_STAT) + 50];
    SD32 repLen;
    
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    if ((repLen = ComCmdAndVarReply(bsm->comId, BSM_LOCAL_ADDR,
        BSM_GET_STAT_CMD_TYPE, NULL, 0, repBuf, sizeof(repBuf), e)) < 0) {
        return(BsmConvertErr(e));
    }
    /* if needed, swap multi-byte fields */
    if (bsm->swap) BsmSwapGenStat((BSM_STAT *)repBuf);
    
    if (genStatSize > (UD32)repLen) return(BsmSizeErr(e));
    (void)memcpy(genStat, repBuf, genStatSize);
    
    /* if error occurred on board, get error & set error info for caller */
    if (genStat->flags & BSM_ERR_OCCURRED) {
        if (ComGetDevErr(bsm->comId, BSM_LOCAL_ADDR, e)) return(-1);
        (void)BsmConvertErr(e);
    }
    
    return(0);
}

/**************************************************************************
 * FUNCTION: BsmGetMicsConfig
 * 
 * This function gets the current MICS configuration from the base station.
 *
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     mc:
 *         Buffer in which to return the MICS configuration. BSM_MICS_CONFIG is
 *         defined in "ZL7010XAdk\Sw\Includes\Adk\BsmMezz\BsmGeneral.h".
 *     mcSize:
 *         The size of the specified buffer (bytes). Note BsmGetMicsConfig()
 *         won't write beyond the specified size. That way, if new fields are
 *         added to the end of BSM_MICS_CONFIG, this function will remain
 *         compatible with older applications that don't use the new fields.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function puts the MICS configuration in the
 *     specified buffer and returns 0. If not successful, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate BSM_MICS_CONFIG buffer and error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
BsmGetMicsConfig(BSM_ID bsmId, BSM_MICS_CONFIG *mc, UD32 mcSize, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    SD32 mcLen;
    union {
        BSM_MICS_CONFIG mc;
        UD8 buf[256];
    } mcBuf;
    
    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    /* Get the MICS config from the base station. Note mcBuf should always be
     * larger than the BSM_MICS_CONFIG structure, so this will always read the
     * full config available from the board, whatever the board's version. If
     * BSM_MICS_CONFIG ever outgrows mcBuf, the worst that will happen is
     * ComCmdAndVarReply() will fail during development and mcBuf will have
     * to be increased.
     */
    if ((mcLen = ComCmdAndVarReply(bsm->comId, BSM_LOCAL_ADDR,
        BSM_GET_MICS_CONFIG_CMD_TYPE, NULL, 0, &mcBuf, sizeof(mcBuf), e)) < 0) {
        return(BsmConvertErr(e));
    }
    /* If the caller expects a larger config than is available, fail. It's ok
     * if the caller expects a smaller config (fewer fields). That way, if new
     * fields are added to the end of BSM_MICS_CONFIG, this interface will
     * remain compatible with older software on the PC - since the caller
     * doesn't expect the new fields that were added to the end of
     * BSM_MICS_CONFIG, those fields simply won't be returned to the caller.
     */
    if (mcSize > (UD32)mcLen) return(BsmSizeErr(e));
    (void)memcpy(mc, &mcBuf, mcSize);
    
    /* save revision of ZL7010X on remote implant for this lib to reference */
    bsm->micsRevOnImplant = mc->micsRevOnImplant;
    
    return(0);
}

/**************************************************************************
 * FUNCTION: BsmSetMicsConfig
 * 
 * This function sets the MICS configuration on the base station.
 * 
 * Note every field must be specified in the configuration. Usually,
 * an application will call BsmGetMicsConfig() to get the current MICS
 * configuration, modify it as desired, then pass it to BsmSetMicsConfig().
 * 
 * Note when the MICS configuration is changed, the base station will
 * automatically stop any MICS operation that is active.
 *
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     mc:
 *         The new MICS configuration. BSM_MICS_CONFIG is defined in
 *         "ZL7010XAdk\Sw\Includes\Adk\BsmMezz\BsmGeneral.h".
 *     mcSize:
 *         The size of the specified BSM_MICS_CONFIG structure (bytes). Note
 *         BsmSetMicsConfig() won't reference anything in the structure beyond
 *         the specified size. That way, if new fields are added to the end of
 *         BSM_MICS_CONFIG, this function will remain compatible with older
 *         applications that don't specify the new fields (defaults will be
 *         used for the new fields).
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
BsmSetMicsConfig(BSM_ID bsmId, const BSM_MICS_CONFIG *mc, UD32 mcSize, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    BSM_MICS_CONFIG mcBuf;
    
    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    /* Check specified config size. Note the specified size can be less than
     * the current size of BSM_MICS_CONFIG. That way, if new fields are added
     * to the end of BSM_MICS_CONFIG, this interface will remain compatible
     * with older software on the PC (the fields added to the end of the config
     * will not be affected by the command, and will simply be left as is).
     */
    if (mcSize > sizeof(BSM_MICS_CONFIG)) return(BsmSizeErr(e));
    
    /* save revision of ZL7010X on remote implant for this lib to reference */
    bsm->micsRevOnImplant = mc->micsRevOnImplant;
    
    /* if we need to swap multi-byte fields, copy config & swap the fields */
    if (bsm->swap) {

        (void)memcpy(&mcBuf, mc, mcSize);
        mc = &mcBuf;

        /* Add logic to swap any multi-byte fields here (there weren't any
         * multi-byte fields as of this writing).
         */
    }

    /* send command and receive reply */
    if (ComCmdAndReply(bsm->comId, BSM_LOCAL_ADDR,
        BSM_SET_MICS_CONFIG_CMD_TYPE, mc, mcSize, NULL, 0, e)) {
        return(BsmConvertErr(e));
    }
    
    return(0);
}

/**************************************************************************
 * FUNCTION: BsmStartSession
 * 
 * This function tells the base station to start a session with an implant.
 * When this is called, the base station will abort any ZL7010X operation that
 * is active, configure the ZL7010X with the normal link configuration (see
 * BsmSetMicsConfig()), then try to start a session with the implant (i.e.
 * transmit the wakeup signal and start a session if a response is received).
 * 
 * Note if using 400 MHz wakeup and either the base or the implant has a
 * ZL70101, neither side will check the company ID in the 400 MHz packets.
 * This applies regardless of which side has the ZL70101, or if they both
 * have a ZL70101. As a result, if multiple implants from different companies
 * are in range, and they use the same implant ID, they will all respond to
 * the 400 MHz wakeup and try to start a session. In this case, the implants
 * might interfere with one another, making it difficult to start a session.
 * Also, after the session stars, the base and implant should negotiate to
 * confirm the company ID.
 * 
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     enabWatchdog:
 *         If true, the base station will enable the ZL7010X watchdog timer
 *         while it's waiting for the session to start. That way, the ZL7010X
 *         will assert IRQ_WDOG if the session doesn't start within ~4.37
 *         seconds, causing the base station to abort the "start session".
 *
 *         When using 2.45 GHz wakeup, "enabWatchdog" should normally be
 *         true to ensure the wakeup won't occupy the MICS channel for more
 *         than 5 seconds. In contrast, when using 400 MHz wakeup, it should
 *         normally be false because it may take longer than 4.37 seconds for
 *         the implant to wake up (the base station will periodically check if
 *         the channel is still clear and automatically switch if needed).
 *
 *         Note if "timeoutSec" > 0, the watchdog isn't really needed, because
 *         you can just use the timeout to control when the "start session" is
 *         aborted. If both are specified, the first to occur will abort the
 *         "start session".
 *
 *         Note if the session starts, the base station will enable the ZL7010X
 *         watchdog timer, even if it was disabled while starting the session.
 *         That way, if the link is lost for more than ~4.37 seconds, the base
 *         station will abort the session.
 *     timeoutSec:
 *         If this is 0, the function will return right away without waiting
 *         for the session to start. In that case, the application may poll the
 *         base station status to detect when the session starts or is aborted
 *         (see BsmGetStat() and BSM_STAT.micsState in "ZL7010XAdk\Sw\Includes\
 *         Adk\BsmMezz\BsmGeneral.h"). If "timeoutSec" > 0, this function will
 *         wait for the session to start (up to the specified timeout, or until
 *         the ZL7010X watchdog asserts IRQ_WDOG, whichever occurs first). Note
 *         while this function is waiting, nothing else will be able to
 *         communicate with the base station, so a long timeout is not
 *         recommended. Also, the maximum timeout supported by the base
 *         station is ~60 seconds.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c"). If a non-zero timeout is
 *     specified, a timeout is treated as an error.
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
BsmStartSession(BSM_ID bsmId, BOOL enabWatchdog, UD32 timeoutSec, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    BSM_START_SESSION_CMD cmd;
    UD32 comTimeoutMs;
    
    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    /* build command */
    cmd.enabWatchdog = (BOOL8)enabWatchdog;
    cmd.timeoutSec = (UD8)timeoutSec;
    
    /* Calc timeout for communication interface. Note this adds 5 seconds to
     * the specified "start session" timeout to ensure the communication
     * interface won't time out before the "start session" times out.
     */
    comTimeoutMs = (timeoutSec + 5) * 1000;

    /* send command and receive reply */
    if (ComCmdAndReplyT(bsm->comId, BSM_LOCAL_ADDR, BSM_START_SESSION_CMD_TYPE,
        &cmd, sizeof(cmd), NULL, 0, comTimeoutMs, e)) {
        return(BsmConvertErr(e));
    }
    return(0);
}

/**************************************************************************
 * FUNCTION: BsmSearchForImplant
 * 
 * This function tells the base station to start searching for implants.
 * When this is called, the base station will stop any ZL7010X operation that
 * is active, configure the ZL7010X with the normal link configuration (see
 * BsmSetMicsConfig()), transmit the wakeup signal, receive any wakeup
 * responses, and place their ID's in the ZL7010X's RX buffer.
 * 
 * Note each implant ID is 3 bytes, so during the search, the base
 * station sets the RX block size to 3. The application can periodically call
 * BsmReceiveMics() to get any ID's that are received. Note since the RX block
 * size is 3, the application should read a multiple of 3 bytes. Also, since
 * each implant repeatedly transmits its ID, each ID may be received numerous
 * times. When the search is stopped, the base station will restore the RX block
 * size specified in the normal link configuration (see BsmSetMicsConfig()).
 * 
 * Note if using 400 MHz wakeup and either the base or the implant has a
 * ZL70101, neither side will check the company ID in the 400 MHz packets.
 * This applies regardless of which side has the ZL70101, or if they both
 * have a ZL70101. As a result, if multiple implants from different companies
 * are in range, and they use the same implant ID, they will all respond to
 * the 400 MHz wakeup. In this case, if a session is subsequently started,
 * the base and implant should negotiate to confirm the company ID.
 *
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     enabWatchdog:
 *         If true, the base station will enable the ZL7010X watchdog timer
 *         during the search. That way, the ZL7010X will assert IRQ_WDOG
 *         after ~4.37 seconds, at which point the search will be stopped.
 *
 *         When using 2.45 GHz wakeup, "enabWatchdog" should normally be
 *         true to ensure the search won't occupy the MICS channel for more
 *         than 5 seconds. In contrast, when using 400 MHz wakeup, it should
 *         normally be false because it may take longer than 4.37 seconds for
 *         the implant to wake up. During 400 MHz wakeup, the base station will
 *         periodically check if the channel is still clear and automatically
 *         switch if needed, so the watchdog isn't needed.
 *     anyCompany:
 *         If using 2.45 GHz wakeup, and this is true, the company ID is set
 *         to 0 (wildcard) during the search so the search will wake up
 *         implants for any company (provided the implant ID also matches).
 *
 *         If using 400 MHz wakeup, and both the base and the implant have a
 *         ZL70102 or later, searching with company ID 0 (wildcard) is not
 *         supported, so this option should be false.
 *
 *         If using 400 MHz wakeup, and either the base or the implant has a
 *         ZL70101, the company ID will not be screened, so this option will
 *         always effectively be true (since the ZL70101 doesn't include or
 *         check the company ID in 400 MHz packets, there's no way to screen
 *         the company ID).
 *     anyImplant:
 *         If this is true, the base station sets the implant ID to 0 (wildcard)
 *         during the search, so it will wake up any implant (provided the
 *         company ID also matches).
 *
 *         Note if using 400 MHz wakeup and the implant has a ZL70101 or
 *         ZL70102, implant ID 0 (wildcard) must always be used to search
 *         for implants, so this option must be true. This is necessary
 *         because the way 400 MHz search is implemented on a ZL70101/102
 *         implant, the implant would start a session if the base station
 *         transmitted the implant's own unique ID.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
BsmSearchForImplant(BSM_ID bsmId, BOOL enabWatchdog, BOOL anyCompany,
    BOOL anyImplant, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    BSM_SEARCH_FOR_IMPLANT_CMD cmd;
    
    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    /* build command */
    cmd.enabWatchdog = (BOOL8)enabWatchdog;
    cmd.anyCompany = (BOOL8)anyCompany;
    cmd.anyImplant = (BOOL8)anyImplant;
    
    /* send command and receive reply */
    if (ComCmdAndReply(bsm->comId, BSM_LOCAL_ADDR,
        BSM_SEARCH_FOR_IMPLANT_CMD_TYPE, &cmd, sizeof(cmd), NULL, 0, e)) {
        return(BsmConvertErr(e));
    }
    return(0);
}

/**************************************************************************
 * FUNCTION: BsmListenForEmergency
 * 
 * This function tells the base station to start listening for emergency
 * transmissions. When this is called, the base station will stop any ZL7010X
 * operation that is active, configure the ZL7010X with the emergency link
 * configuration (see BsmSetMicsConfig()), start receiving emergency
 * transmissions, and place their implant ID's in the ZL7010X's RX buffer.
 * 
 * If "startSession" is true, the base station will read any ID's that
 * are received, and if one matches the implant ID specified in the MICS
 * configuration (see BsmSetMicsConfig), it will start an emergency
 * session with that implant.
 * 
 * Note each implant ID is 3 bytes, so while listening, the base station
 * sets the RX block size to 3. If "startSession" is false, the application
 * can periodically call BsmReceiveMics() to get any ID's that are received.
 * Note since the RX block size is 3, the application should read a multiple
 * of 3 bytes. Also, since the implant repeatedly transmits its ID, the ID may
 * be received numerous times. When the listen is stopped, or the emergency
 * session is started, the base station will restore the RX block size
 * specified in the emergency link configuration (see BsmSetMicsConfig()).
 * 
 * Note the base station disables the ZL7010X watchdog timer while
 * listening, so it will continue to listen until it either starts an emergency
 * session with the implant (see the "startSession" argument), or the listen is
 * stopped (see BsmAbortMics()). Listening indefinitely won't violate the
 * MICS specification because nothing is transmitted.
 * 
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     startSession: 
 *         If true, the base station will read any ID's that are received, and
 *         if one matches the implant ID specified in the MICS configuration
 *         (see BsmSetMicsConfig()), it will start an emergency session with
 *         that implant.
 *
 *         If false, the base station will leave any ID's that are received in
 *         the RX buffer on the ZL7010X for the application to read (for more
 *         information, see the function description above).
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
BsmListenForEmergency(BSM_ID bsmId, BOOL startSession, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    BSM_LISTEN_FOR_EMERGENCY_CMD cmd;
    
    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    /* build command */
    cmd.startSession = (BOOL8)startSession;
    
    /* send command and receive reply */
    if (ComCmdAndReply(bsm->comId, BSM_LOCAL_ADDR,
        BSM_LISTEN_FOR_EMERGENCY_CMD_TYPE, &cmd, sizeof(cmd), NULL, 0, e)) {
        return(BsmConvertErr(e));
    }
    return(0);
}

/**************************************************************************
 * FUNCTION: BsmAbortMics
 * 
 * This function tells the base station to abort any ZL7010X operation that is
 * currently active.
 *
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
BsmAbortMics(BSM_ID bsmId, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    
    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    /* send command and receive reply */
    if (ComCmdAndReply(bsm->comId, BSM_LOCAL_ADDR, BSM_ABORT_MICS_CMD_TYPE,
        NULL, 0, NULL, 0, e)) {
        return(BsmConvertErr(e));
    }
    return(0);
}

/**************************************************************************
 * FUNCTION: BsmReadMicsReg
 * 
 * This function can be used to read a register in the ZL7010X on the base
 * station or the remote implant (via housekeeping).
 * 
 * The addresses for many commonly used registers are defined in
 * "ZL7010XAdk\Sw\Includes\Adk\AnyMezz\MicsHw.h". For a page 2 register,
 * specify MICS_PAGE_2 (0x80) in the register address (note 0x8000 will also
 * still work so this remains backwards compatible). The ADK software manages
 * the page selection automatically. Note this function can't be used to read
 * a page 2 register on a remote ZL70101 implant, because the ZL70101 doesn't
 * support page 2 access via housekeeping.
 * 
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     regAddr:
 *         The address of the register to read in the ZL7010X. For a page 2
 *         register, specify MICS_PAGE_2 (0x80) in the register address (note
 *         0x8000 will also still work so this remains backwards compatible).
 *         For more information, see the function prolog.
 *     remote:
 *         If false, this function will read the specified register in the
 *         ZL7010X on the base station.
 *
 *         If true, the base will use housekeeping to read the specified
 *         register in the ZL7010X on the remote implant. Note the base must
 *         be in session with the implant (otherwise, the remote read will
 *         time out and this function will return an error).
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns the value of the register (always
 *     >= 0 because each register is only 8 bits). If not successful, this
 *     function fills the specified error object and returns -1 (to get the
 *     error, see EiGetMsg(), etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
SD32
BsmReadMicsReg(BSM_ID bsmId, UD32 regAddr, BOOL remote, EI e)
{ 
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    BSM_READ_MICS_REG_CMD cmd;
    BSM_READ_MICS_REG_REPLY rep;
    
    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    /* for backwards compatibility (used to set 0x8000 for page 2 registers) */
    if (regAddr & 0x8000) {
        regAddr &= ~0x8000;
        regAddr |= MICS_PAGE_2;
    }
    
    /* build command */
    cmd.regAddr = (UD16)regAddr;
    cmd.remote = (BOOL8)remote;
    
    /* if needed, swap multi-byte fields */
    if (bsm->swap) cmd.regAddr = SWAP16(cmd.regAddr);
    
    /* send command and receive reply */
    if (ComCmdAndReply(bsm->comId, BSM_LOCAL_ADDR, BSM_READ_MICS_REG_CMD_TYPE,
        &cmd, sizeof(cmd), &rep, sizeof(rep), e)) {
        return(BsmConvertErr(e));
    }
    
    /* return register value */
    return(rep.regVal);
}

/* This has been kept for backwards compatibility - new software can just call
 * BsmReadMicsReg() with MICS_PAGE_2 (0x80) specified in the register address.
 */
SD32
BsmReadMicsTestReg(BSM_ID bsmId, UD32 testRegAddr, EI e)
{ 
    return(BsmReadMicsReg(bsmId, MICS_PAGE_2 | testRegAddr, FALSE, e));
}

/**************************************************************************
 * FUNCTION: BsmWriteMicsReg
 * 
 * This function can be used to write to a register in the ZL7010X on the base
 * station or the remote implant (via housekeeping).
 * 
 * The addresses for many commonly used registers are defined in
 * "ZL7010XAdk\Sw\Includes\Adk\AnyMezz\MicsHw.h". For a page 2 register,
 * specify MICS_PAGE_2 (0x80) in the register address (note 0x8000 will also
 * still work so this remains backwards compatible). The ADK software manages
 * the page selection automatically. Note this function can't be used to write
 * to a page 2 register on a remote ZL70101 implant, because the ZL70101 doesn't
 * support page 2 access via housekeeping.
 * 
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     regAddr:
 *         The address of the register to write to in the ZL7010X. For a page 2
 *         register, specify MICS_PAGE_2 (0x80) in the register address (note
 *         0x8000 will also still work so this remains backwards compatible).
 *         For more information, see the function prolog.
 *     regVal:
 *         The value to write to the register.
 *     remote:
 *         If false, this function will write to the specified register in the
 *         ZL7010X on the base station.
 *
 *         If true, the base will use housekeeping to write to the specified
 *         register in the ZL7010X on the remote implant. Note the base must
 *         be in session with the implant (otherwise, the remote write will
 *         time out and this function will return an error).
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the 
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
BsmWriteMicsReg(BSM_ID bsmId, UD32 regAddr, UD32 regVal, BOOL remote, EI e)
{ 
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    BSM_WRITE_MICS_REG_CMD cmd;
    
    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    /* for backwards compatibility (used to set 0x8000 for page 2 registers) */
    if (regAddr & 0x8000) {
        regAddr &= ~0x8000;
        regAddr |= MICS_PAGE_2;
    }
    
    /* If trying to set INTERFACE_MODE.ACCESS_PAGE_2 (register page 2), fail.
     * This is done for the reasons noted below. To access a page 2 register,
     * specify MICS_PAGE_2 (0x80) in the address (the firmware manages the
     * page selection automatically).
     * 
     * If changing INTERFACE_MODE on the base, it actually wouldn't hurt
     * anything to set ACCESS_PAGE_2 because the base firmware would be aware
     * of it, but it wouldn't serve any purpose, and isn't the intended usage.
     * 
     * If changing INTERFACE_MODE on the remote implant, setting ACCESS_PAGE_2 
     * would cause a problem if the implant tried to access page 1 afterwards,
     * such as a ZL7010X interrupt status register.
     */
    if (((regAddr & 0x7F) == INTERFACE_MODE) && (regVal & ACCESS_PAGE_2)) {
        EiSet(e, BSM_LIB_EID_ATTEMPT_TO_SELECT_PAGE_2, NULL);
        return(-1);
    }
    /* If trying to set HK_MODE_102.HK_ACCESS_PAGE_2 on a ZL70102, fail. This
     * is done for the following reasons:
     * 
     * If changing HK_MODE on base, there's no reason to set HK_ACCESS_PAGE_2
     * because the ADK doesn't allow the implant to access page 2 registers on
     * the base, and even if it did, changing HK_ACCESS_PAGE_2 wouldn't be safe
     * because it might not match the page expected by the implant.
     * 
     * If changing HK_MODE on the remote implant, it actually wouldn't hurt
     * anything to set HK_ACCESS_PAGE_2 because the base firmware would be aware
     * of it, but it wouldn't serve any purpose, and isn't the intended usage.
     */
    if ((!remote && (bsm->micsRev > ZL70101)) ||
        (remote && (bsm->micsRevOnImplant > ZL70101))) {
            
        if (((regAddr & 0x7F) == HK_MODE_102) && (regVal & HK_ACCESS_PAGE_2)) {
            
            EiSet(e, BSM_LIB_EID_ATTEMPT_TO_SELECT_HK_PAGE_2, NULL);
            return(-1);
        }
    }
     
    /* build command */
    cmd.regAddr = (UD16)regAddr;
    cmd.regVal = (UD8)regVal;
    cmd.remote = (BOOL8)remote;
    
    /* if needed, swap multi-byte fields */
    if (bsm->swap) cmd.regAddr = SWAP16(cmd.regAddr);
    
    /* send command and receive reply */
    if (ComCmdAndReply(bsm->comId, BSM_LOCAL_ADDR, BSM_WRITE_MICS_REG_CMD_TYPE,
        &cmd, sizeof(cmd), NULL, 0, e)) {
        return(BsmConvertErr(e));
    }
    return(0);
}

/* This has been kept for backwards compatibility - new software can just call
 * BsmWriteMicsReg() with MICS_PAGE_2 (0x80) specified in the register address.
 */
RSTAT
BsmWriteMicsTestReg(BSM_ID bsmId, UD32 testRegAddr, UD32 testRegVal, EI e)
{ 
    return(BsmWriteMicsReg(bsmId, MICS_PAGE_2 | testRegAddr, testRegVal, FALSE, e));
}

/**************************************************************************
 * FUNCTION: BsmReceiveMics
 * 
 * This function reads any data contained in the RX buffer on the ZL7010X.
 *
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     buf:
 *         The buffer in which to put any data that is read.
 *     bufSize:
 *         The size of the specified buffer (bytes). Note BsmReceiveMics()
 *         will read any data available in the ZL7010X, but only up to the
 *         specified buffer size. Note the base station can only read complete
 *         data blocks from the ZL7010X, so the specified buffer size must
 *         always be >= the current RX block size, and BsmReceiveMics() will
 *         always read a multiple of the RX block size.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function puts any data that it reads in the
 *     specified buffer and returns the number of bytes read. Note this will
 *     always be a multiple of the current RX block size, or 0 if no data is
 *     available. If not successful, this function fills the specified error
 *     object and returns -1 (to get the error, see EiGetMsg(), etc. in
 *     "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate data buffer and error object.
 * 
 * VISIBILITY: Customer
 */
SD32
BsmReceiveMics(BSM_ID bsmId, void *buf, UD32 bufSize, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    BSM_RECEIVE_MICS_CMD cmd;
    UD32 maxLen;
    SD32 repLen;
    
    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    /* don't try to read more data than one reply packet can hold */
    maxLen = bufSize < COM_MAX_PACK_LEN ? bufSize : COM_MAX_PACK_LEN;
    
    /* build command */
    cmd.maxLen = (UD16)(maxLen);
    
    /* if needed, swap multi-byte fields */
    if (bsm->swap) cmd.maxLen = SWAP16(cmd.maxLen);
    
    /* send command and receive variable length reply */
    if ((repLen = ComCmdAndVarReply(bsm->comId, BSM_LOCAL_ADDR,
        BSM_RECEIVE_MICS_CMD_TYPE, &cmd, sizeof(cmd), buf, maxLen, e)) < 0) {
        return(BsmConvertErr(e));
    }
    
    /* return number of bytes read (note this may be 0) */
    return(repLen);
}

/**************************************************************************
 * FUNCTION: BsmTransmitMics
 * 
 * This function writes data to the ZL7010X's TX buffer, which will transmit
 * the data as soon as possible. If there's not enough space in the TX buffer,
 * this will wait up to 4 seconds, writing data as space becomes available. If
 * 4 seconds elapses or the ZL7010X asserts IRQ_CLOST before all of the data
 * has been written, it will time out and return an error.
 * 
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     data:
 *         The data to transmit.
 *     nBytes:
 *         The number of data bytes. Note the base station can only write
 *         complete data blocks to the ZL7010X, so the specified number of
 *         bytes must always be a multiple of the current TX block size
 *         (otherwise, this function will return an error).
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If all of the data is written to the TX buffer on the ZL7010X, this
 *     function returns 0. Otherwise, it fills the specified error object and
 *     returns -1 (to get the error, see EiGetMsg(), etc. in "AppDevPlat\Sw\
 *     Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
BsmTransmitMics(BSM_ID bsmId, const void *data, UD32 nBytes, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    
    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    /* send command and receive reply */
    if (ComCmdAndReply(bsm->comId, BSM_LOCAL_ADDR, BSM_TRANSMIT_MICS_CMD_TYPE,
        data, nBytes, NULL, 0, e)) {
        return(BsmConvertErr(e));
    }
    return(0);
}

/**************************************************************************
 * FUNCTION: BsmMicsCal
 * 
 * This function performs one or more MICS (ZL7010X) calibrations on the
 * base station, or on the remote implant (via housekeeping).
 * 
 * Note if CAL_400_MHZ_ANT is specified, "remote" is false, and the
 * specified channel is >= 0 and != the current channel (MAC_CHANNEL), this
 * will abort any active ZL7010X operation before changing the channel.
 * 
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     cals:
 *         The calibrations to perform. Each bit corresponds to a calibration
 *         in the CALSELECT1 or CALSELECT2 registers (defined in "ZL7010XAdk\
 *         Sw\Includes\Adk\AnyMezz\MicsHw.h"). The lowest 8 bits are written to
 *         CALSELECT1, and the next 8 bits are written to CALSELECT2 (ZL70102
 *         or later). To see which calibrations are supported, refer to
 *         BSM_LOCAL_MICS_CALS if "remote" is false, and BSM_REMOTE_101_CALS
 *         or BSM_REMOTE_102_CALS if "remote" is true (defined in "ZL7010XAdk\
 *         Sw\Includes\Adk\BsmMezz\BsmGeneral.h"). If an unsupported
 *         calibration is specified, this function will return an error.
 *     chan:
 *         The channel to use for the CAL_400_MHZ_ANT calibration, or -1 to
 *         use the current channel (MAC_CHANNEL). Note this is only used if
 *         CAL_400_MHZ_ANT is specified and "remote" is false (the channel
 *         can't be changed on the remote implant, because that would break
 *         the wireless link).
 *     remote:
 *         If false, this function will invoke the specified calibrations in
 *         the ZL7010X on the base station.
 *
 *         If true, the base station will use housekeeping to invoke the
 *         specified calibrations in the ZL7010X on the remote implant. Note
 *         the base station must be in session with the implant (otherwise, the
 *         housekeeping will time out and this function will return an error).
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
BsmMicsCal(BSM_ID bsmId, UD32 cals, SD32 chan, BOOL remote, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    UINT supportedCals, badCals;
    BSM_MICS_CAL_CMD cmd;
    
    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    /* if performing calibration(s) on remote implant */
    if (remote) {
        
        /* get supported calibrations */ 
        if (bsm->micsRevOnImplant == ZL70101) {
            supportedCals = BSM_REMOTE_101_CALS;
        } else {
            supportedCals = BSM_REMOTE_102_CALS;
        }
        /* if an unsupported calibration is specified, fail */
        if ((badCals = (cals & ~supportedCals)) != 0) {
            EiSet(e, BSM_LIB_EID_UNSUPPORTED_REMOTE_MICS_CAL, "0x%02x", badCals);
            return(-1);
        }
    } else {
        
        /* if an unsupported calibration is specified, fail */
        if ((badCals = (cals & ~BSM_LOCAL_MICS_CALS)) != 0) {
            EiSet(e, BSM_LIB_EID_UNSUPPORTED_LOCAL_MICS_CAL, "0x%02x", badCals);
            return(-1);
        }
    }
    
    /* build command */
    cmd.calSelect1 = (UD8)(cals);
    cmd.calSelect2 = (UD8)(cals >> 8);
    cmd.chan = (SD8)chan;
    cmd.remote = (BOOL8)remote;
    
    /* send command and receive reply */
    if (ComCmdAndReply(bsm->comId, BSM_LOCAL_ADDR, BSM_MICS_CAL_CMD_TYPE,
        &cmd, sizeof(cmd), NULL, 0, e)) {
        return(BsmConvertErr(e));
    }
    return(0);
}

/**************************************************************************
 * FUNCTION: BsmMicsAdc
 * 
 * This function does an A/D conversion on the base station, or on the remote
 * implant (via housekeeping). It uses the internal ADC in the ZL7010X.
 * 
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     adcInput:
 *         The A/D input to sample (see ADC_INPUT_TEST_IO_1, etc. in
 *         "ZL7010XAdk\Sw\Includes\Adk\AnyMezz\MicsHw.h").
 *     remote:
 *         If false, this function will do the ADC on the base station.
 *
 *         If true, the base station will use housekeeping to do the ADC on the
 *         remote implant. Note the base station must be in session with the
 *         implant (otherwise, the housekeeping will time out and this function
 *         will return an error).
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns the ADC (always >= 0). Otherwise,
 *     it fills the specified error object and returns -1 (to get the error,
 *     see EiGetMsg(), etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
SD32
BsmMicsAdc(BSM_ID bsmId, UD32 adcInput, BOOL remote, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    BSM_MICS_ADC_CMD cmd;
    BSM_MICS_ADC_REPLY rep;
    
    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    /* build command */
    cmd.adcInput = (UD8)adcInput;
    cmd.remote = (BOOL8)remote;
    
    /* send command and receive reply */
    if (ComCmdAndReply(bsm->comId, BSM_LOCAL_ADDR, BSM_MICS_ADC_CMD_TYPE,
        &cmd, sizeof(cmd), &rep, sizeof(rep), e)) {
        return(BsmConvertErr(e));
    }
    
    /* return result */
    return(rep.adcResult);
}

/**************************************************************************
 * FUNCTION: BsmSetTx245Freq
 *
 * This function sets the value of the CC25XX's frequency control registers
 * (FREQ2, FREQ1, and FREQ0) for the 2.45 GHz TX. Note that if the CC25XX TX
 * is active when this is called, it will be stopped while the frequency is
 * changed, then restarted afterwards.
 *
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     ccFreq:
 *         The new value for the CC25XX frequency control registers, with FREQ0
 *         in the least significant byte, FREQ1 in the second byte, FREQ2 in the
 *         third byte, and 0 in the most significant byte.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 *
 * VISIBILITY: Customer
 */
RSTAT
BsmSetTx245Freq(BSM_ID bsmId, UD32 ccFreq, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    BSM_SET_TX_245_FREQ_CMD cmd;

    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));

    /* build command */
    cmd.ccFreq0 = (UD8)ccFreq;
    cmd.ccFreq1 = (UD8)(ccFreq >> 8);
    cmd.ccFreq2 = (UD8)(ccFreq >> 16);

    /* send command and receive reply */
    if (ComCmdAndReply(bsm->comId, BSM_LOCAL_ADDR,
        BSM_SET_TX_245_FREQ_CMD_TYPE, &cmd, sizeof(cmd), NULL, 0, e)) {
        return(BsmConvertErr(e));
    }

    return(0);
}

/**************************************************************************
 * FUNCTION: BsmSetTx245Power
 * 
 * This function sets the power level for the 2.45 GHz TX carrier (for the
 * 2.45 GHz wakeup signal).
 * 
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     ccPa:
 *         This value is written to the CC25XX PA register. When properly
 *         calibrated, it determines the TX power.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
BsmSetTx245Power(BSM_ID bsmId, UINT ccPa, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    BSM_SET_TX_245_POWER_CMD cmd;
    
    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    /* build command */
    cmd.ccPa = (UD8)ccPa;
    
    /* send command and receive reply */
    if (ComCmdAndReply(bsm->comId, BSM_LOCAL_ADDR,
        BSM_SET_TX_245_POWER_CMD_TYPE, &cmd, sizeof(cmd), NULL, 0, e)) {
        return(BsmConvertErr(e));
    }
    
    return(0);
}

/**************************************************************************
 * FUNCTION: BsmStartTx245Carrier
 * 
 * This function tells the base station to start or stop transmitting a
 * 2.45 GHz TX carrier (continuous wave with no on/off keying).
 * 
 * Note this is a test mode, not a normal operational mode. If a ZL7010X
 * operation is active, it will be aborted before the carrier is started. The
 * carrier should be stopped before starting any other ZL7010X operation (so it
 * won't affect the operation).
 * 
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     start:
 *         If true, the carrier is started. If false, it's stopped.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
BsmStartTx245Carrier(BSM_ID bsmId, BOOL start, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    BSM_START_TX_245_CARRIER_CMD cmd;
    
    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    /* build command */
    cmd.start = (UD8)start;
    
    /* send command and receive reply */
    if (ComCmdAndReply(bsm->comId, BSM_LOCAL_ADDR,
        BSM_START_TX_245_CARRIER_CMD_TYPE, &cmd, sizeof(cmd), NULL, 0, e)) {
        return(BsmConvertErr(e));
    }
    
    return(0);
}

/**************************************************************************
 * FUNCTION: BsmStartTx400Carrier
 * 
 * This function tells the base station to start or stop transmitting a
 * 400 MHz TX carrier (continuous wave).
 * 
 * Note this is a test mode, not a normal operational mode. If a ZL7010X
 * operation is active, it will be aborted before the carrier is started. The
 * carrier should be stopped before starting any other ZL7010X operation (so it
 * won't affect the operation).
 * 
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     start:
 *         If true, the carrier is started. If false, it's stopped.
 *     chan:
 *         The MICS channel to use to transmit the carrier.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
BsmStartTx400Carrier(BSM_ID bsmId, BOOL start, UD32 chan, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    BSM_START_TX_400_CARRIER_CMD cmd;
    
    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    /* build command */
    cmd.start = (UD8)start;
    cmd.chan = (UD8)chan;
    
    /* send command and receive reply */
    if (ComCmdAndReply(bsm->comId, BSM_LOCAL_ADDR,
        BSM_START_TX_400_CARRIER_CMD_TYPE, &cmd, sizeof(cmd), NULL, 0, e)) {
        return(BsmConvertErr(e));
    }
    
    return(0);
}

/**************************************************************************
 * FUNCTION: BsmRssi
 * 
 * This function performs an RSSI (received signal strength indicator). Note that
 * if a ZL7010X operation is active, this will abort it before performing the RSSI.
 * 
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     useIntRssi:
 *         If true, the internal RSSI is used (factory test/debug use only), in
 *         which case the "average" argument has no effect because the internal
 *         RSSI always averages over 10 ms.
 *     chan:
 *         The MICS channel to do the RSSI for (0 to 9).
 *     average:
 *         If true, the average RSSI over 10 ms is returned instead of the
 *         maximum RSSI detected over 10 ms. Note that if "useIntRssi" is true,
 *         the "average" argument has no effect because the internal RSSI always
 *         averages over 10 ms.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns the RSSI (always >= 0). If not
 *     successful, it fills the specified error object and returns -1 (to get
 *     the error, see EiGetMsg(), etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
SD32
BsmRssi(BSM_ID bsmId, BOOL useIntRssi, UD32 chan, BOOL average, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    BSM_RSSI_CMD cmd;
    BSM_RSSI_REPLY rep;
    
    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    /* build command */
    cmd.useIntRssi = (UD8)useIntRssi;
    cmd.chan = (UD8)chan;
    cmd.average = (BOOL8)average;
    
    /* send command and receive reply */
    if (ComCmdAndReply(bsm->comId, BSM_LOCAL_ADDR, BSM_RSSI_CMD_TYPE,
        &cmd, sizeof(cmd), &rep, sizeof(rep), e)) {
        return(BsmConvertErr(e));
    }
    
    /* if needed, swap multi-byte fields */
    if (bsm->swap) rep.rssi = SWAP16(rep.rssi);
    
    /* return result */
    return(rep.rssi);
}

/**************************************************************************
 * FUNCTION: BsmEnabExtRssi
 * 
 * This function tells the base station to enable or disable the external RSSI.
 * This configures the ZL7010X and the external hardware as needed to perform
 * RSSI measurements using an external ADC. If the ZL7010X is idle, this will
 * also enable its receiver and synthesizer and wait to give them time to start.
 * 
 * Note if a ZL7010X operation is active, and the specified channel is
 * different than the current channel (MAC_CHANNEL register), this will abort
 * the operation before changing the channel.
 * 
 * Note this is a test mode, not a normal operational mode. The external
 * RSSI should normally be left disabled because it consumes more power. The
 * base station automatically enables it as needed to perform RSSI's.
 * 
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     enab:
 *         If true, enable the external RSSI. If false, disable it.
 *     chan:
 *         The MICS channel to use for the external RSSI.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
BsmEnabExtRssi(BSM_ID bsmId, BOOL enab, UD32 chan, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    BSM_ENAB_EXT_RSSI_CMD cmd;
    
    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    /* build command */
    cmd.enab = (UD8)enab;
    cmd.chan = (UD8)chan;
    
    /* send command and receive reply */
    if (ComCmdAndReply(bsm->comId, BSM_LOCAL_ADDR, BSM_ENAB_EXT_RSSI_CMD_TYPE,
        &cmd, sizeof(cmd), NULL, 0, e)) {
        return(BsmConvertErr(e));
    }
    
    return(0);
}

/**************************************************************************
 * FUNCTION: BsmCca
 * 
 * This function performs a clear channel assessment (CCA). Note that if a ZL7010X
 * operation is active, this will abort it before performing the CCA (so it can
 * change the channel), and will restore the previous channel when done.
 * 
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     average:
 *         If true, the CCA uses the average RSSI over 10 ms instead of the
 *         maximum RSSI detected over 10 ms.
 *     data:
 *         Buffer in which to return the CCA data (NULL for none). BSM_CCA_DATA
 *         is defined in "ZL7010XAdk\Sw\Includes\Adk\BsmMezz\BsmGeneral.h".
 *     dataSize:
 *         If "data" != NULL, this must be the size of the specified buffer
 *         (bytes), which must be >= the size of BSM_CCA_DATA (otherwise, the
 *         function will return an error).
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns the clearest channel (0 to 9).
 *     If not successful, this function fills the specified error object and
 *     returns -1 (to get the error, see EiGetMsg(), etc. in "AppDevPlat\Sw\
 *     Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
SD32
BsmCca(BSM_ID bsmId, BOOL average, BSM_CCA_DATA *data, UD32 dataSize, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    BSM_CCA_CMD cmd;
    BSM_CCA_REPLY rep;
    UINT chan;
    
    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    /* build command */
    cmd.average = (BOOL8)average;
    
    /* send command and receive reply */
    if (ComCmdAndReply(bsm->comId, BSM_LOCAL_ADDR, BSM_CCA_CMD_TYPE,
        &cmd, sizeof(cmd), &rep, sizeof(rep), e)) {
        return(BsmConvertErr(e));
    }
    
    /* if a buffer was specified to return the CCA data */
    if (data != NULL) {
        
        /* if specified buffer is too small, fail */
        if (dataSize < sizeof(rep.data)) return(BsmSizeErr(e));
        
        /* copy CCA data into specified buffer */
        *data = rep.data;
        
        /* if needed, swap multi-byte fields */
        if (bsm->swap) {
            for(chan = 0; chan <= MAX_MICS_CHAN; ++chan) {
                data->rssi[chan] = SWAP16(data->rssi[chan]);
            }
        }
    }
    
    /* return clearest channel */
    return(rep.clearChan);
}

/* This has been kept for backwards compatibility - use BsmGetStatChanges()
 * instead. This gets just the link status from the base (MIC_LINK_STAT).
 */
RSTAT
BsmGetLinkStat(BSM_ID bsmId, MICS_LINK_STAT *ls, UD32 lsSize, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    UD8 repBuf[sizeof(MICS_LINK_STAT) + 50];
    SD32 repLen;
    
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    if ((repLen = ComCmdAndVarReply(bsm->comId, BSM_LOCAL_ADDR,
        BSM_GET_LINK_STAT_CMD_TYPE, NULL, 0, repBuf, sizeof(repBuf), e)) < 0) {
        return(BsmConvertErr(e));
    }
    /* if needed, swap multi-byte fields */
    if (bsm->swap) BsmSwapLinkStat((MICS_LINK_STAT *)repBuf);
    
    if (lsSize > (UD32)repLen) return(BsmSizeErr(e));
    (void)memcpy(ls, repBuf, lsSize);
        
        
    return(0);
}

/* This has been kept for backwards compatibility - use BsmGetStatChanges()
 * instead. This gets just the link quality from the base (MICS_LINK_QUAL).
 */
RSTAT
BsmGetLinkQual(BSM_ID bsmId, MICS_LINK_QUAL *lq, UD32 lqSize, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    UD8 repBuf[sizeof(MICS_LINK_QUAL) + 50];
    SD32 repLen;
    
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    if ((repLen = ComCmdAndVarReply(bsm->comId, BSM_LOCAL_ADDR,
        BSM_GET_LINK_QUAL_CMD_TYPE, NULL, 0, repBuf, sizeof(repBuf), e)) < 0) {
        return(BsmConvertErr(e));
    }
    /* if needed, swap multi-byte fields */
    if (bsm->swap) BsmSwapLinkQual((MICS_LINK_QUAL *)repBuf);
    
    if (lqSize > (UD32)repLen) return(BsmSizeErr(e));
    (void)memcpy(lq, repBuf, lqSize);
    
    return(0);
}

/* This has been kept for backwards compatibility - use BsmGetStatChanges()
 * instead. This gets just the link quality from the base (MICS_LINK_QUAL).
 */
RSTAT
BsmGetLinkIntegrity(BSM_ID bsmId, MICS_LINK_QUAL *lq, UD32 lqSize, EI e)
{
    return(BsmGetLinkQual(bsmId, lq, lqSize, e));
}

/**************************************************************************
 * FUNCTION: BsmStartBerRx
 * 
 * This configures the base station as needed for the bit error rate test
 * (receive side), and optionally enables the receiver to start the test.
 * Note this is an ADK factory test that requires a specific setup with
 * specific test equipment, and is not intended for customer use.
 * 
 * Note this is a test mode, not a normal operational mode. This should
 * only be called when the ZL7010X is idle. When done, you should reset the
 * base station to restore the ZL7010X registers for normal operation.
 * 
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     start:
 *         If true, the receiver is enabled. If false, it's disabled.
 *     chan:
 *         The MICS channel to use.
 *     rxMod:
 *         The RX modulation to use (see RX_MOD_2FSK_FB, etc. in
 *         "ZL7010XAdk\Sw\Includes\Adk\AnyMezz\MicsHw.h").
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
BsmStartBerRx(BSM_ID bsmId, BOOL start, UD32 chan, UD32 rxMod, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    UD32 clkRecCtrlAddr;
    
    /* init variables that depend on ZL7010X revision */
    if (bsm->micsRev == ZL70101) {
        clkRecCtrlAddr = CLKRECCTRL_101;
    } else {
        clkRecCtrlAddr = CLKRECCTRL_102;
    }
    
    /* disable RX (in case it's enabled) */
    if (BsmWriteMicsReg(bsmId, RF_GENENABLES, 0, FALSE, e)) return(-1);
    
    /* set channel and RX modulation */
    if (BsmWriteMicsReg(bsmId, MAC_CHANNEL, chan, FALSE, e)) return(-1);
    if (BsmWriteMicsReg(bsmId, MAC_MODUSER, rxMod, FALSE, e)) return(-1);
    
    switch(rxMod & RX_MOD_MASK) {
    case RX_MOD_2FSK_FB:
    case RX_MOD_BARKER_103: {
        
        if (BsmWriteMicsReg(bsmId, SYNC1, 0x6A, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, SYNC2, 0xE3, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, SYNC3, 0x48, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, SYNC4, 0xC5, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, SYNC5, 0xE6, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, MICS_PAGE_2 | 11, 0x03, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, MICS_PAGE_2 | 13, 0x40, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, PO0, 0, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, PO2, 0, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, PO3, 0, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, clkRecCtrlAddr, 0x0D, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, RXGENCTRL, 0x03, FALSE, e)) return(-1);
        break;
        
    } case RX_MOD_2FSK: {
        
        if (BsmWriteMicsReg(bsmId, SYNC1, 0x6A, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, SYNC2, 0xE3, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, SYNC3, 0x48, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, SYNC4, 0xC5, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, SYNC5, 0xE6, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, MICS_PAGE_2 | 11, 0x03, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, MICS_PAGE_2 | 13, 0x40, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, PO0, 0, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, PO2, 0, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, PO3, 0, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, clkRecCtrlAddr, 0x0D, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, RXGENCTRL, 0x03, FALSE, e)) return(-1);
        break;
    
    } case RX_MOD_4FSK: {
        
        if (BsmWriteMicsReg(bsmId, SYNC1, 0x36, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, SYNC2, 0x8E, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, SYNC3, 0x54, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, SYNC4, 0x6C, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, SYNC5, 0x3E, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, MICS_PAGE_2 | 11, 0x07, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, MICS_PAGE_2 | 13, 0x40, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, PO1, 0, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, PO2, 0, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, PO3, 0, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, clkRecCtrlAddr, 0x0D, FALSE, e)) return(-1);
        if (BsmWriteMicsReg(bsmId, RXGENCTRL, 0x0B, FALSE, e)) return(-1);
        break;
        
    } default: {
        
        EiSet(e, BSM_LIB_EID_INVALID_RX_MOD, "0x%x", rxMod);
        return(-1);
    }}
        
    /* if specified, enable RX */
    if (start) {
        if (BsmWriteMicsReg(bsmId, RF_GENENABLES, SYNTH | RX_RF | RX_IF,
            FALSE, e)) {
            return(-1);
        }
    }
    
    /* return success */
    return(0);
}

/**************************************************************************
 * FUNCTION: BsmGetTraceMsg
 * 
 * This function gets the the next message in the trace buffer on the base
 * station. Note trace messages are generally intended for test/debug, so
 * they're normally disabled, but they can be enabled if desired. For more
 * information on tracing, see "AppDevPlat\Sw\Includes\Adp\AnyBoard\TraceLib.h".
 *
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     buf:
 *         The buffer in which to return the next trace message (note if
 *         no more trace messages are available, the message will be a
 *         NULL-terminated empty string).
 *     bufSize:
 *         The size of the specified buffer (bytes). If the trace message
 *         won't fit in the buffer, it will be truncated (note it will still
 *         be NULL-terminated). The maximum size of a trace message depends
 *         on the TRACE_MSG_BUF_SIZE defined in "AppDevPlat\Sw\Includes\Adp\
 *         AnyBoard\TraceLib.h". It's recommended the specified buffer is >=
 *         256 bytes to ensure this will get the whole message.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function puts the NULL-terminated trace message in
 *     the specified buffer and returns 0 (note if no more trace messages are
 *     available, the message will be a NULL-terminated empty string). If not
 *     successful, it fills the specified error object and returns -1 (to get
 *     the error, see EiGetMsg(), etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate buffer and error object.
 *
 * VISIBILITY: Customer
 */
RSTAT
BsmGetTraceMsg(BSM_ID bsmId, char *buf, UD32 bufSize, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    SD32 repLen;
    
    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    /* send command and receive variable length reply (trace message string) */
    if ((repLen = ComCmdAndVarReply(bsm->comId, BSM_LOCAL_ADDR,
        BSM_GET_TRACE_MSG_CMD_TYPE, NULL, 0, buf, bufSize, e)) < 0) {
        return(BsmConvertErr(e));
    }
    
    /* make sure string is terminated (just to be safe) */
    if (repLen) {
        buf[repLen - 1] = '\0';
    } else {
        buf[0] = '\0';
    }
    
    return(0);
}

/**************************************************************************
 * FUNCTION: BsmCopyMicsRegs
 * 
 * This function executes the ZL7010X "copy registers" command on the base or
 * the remote implant (via housekeeping). This copies a subset of the ZL7010X's
 * registers to its wakeup stack so it will preserve them while it's sleeping
 * and restore them every time it's awakened. This also recalculates the
 * ZL7010X's CRC.
 * 
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     remote:
 *         If true, the base will use housekeeping to execute the "copy
 *         registers" command on the remote implant's ZL7010X. Note the base
 *         must be in session with the implant (otherwise, the housekeeping
 *         will time out and this function will return an error).
 *
 *         If false, this function will execute the "copy registers" command
 *         on the base. Note if the ZL7010X on the base never sleeps, this
 *         isn't needed, but it won't hurt anything.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
BsmCopyMicsRegs(BSM_ID bsmId, BOOL remote, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    BSM_COPY_MICS_REGS_CMD cmd;
    RSTAT rStat = -1;
    BOOL enabIrq;
    int regVal;
    
    /* init resources referenced in cleanup logic */
    enabIrq = FALSE;
    
    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) {
        (void)BsmIdErr(e);
        goto cleanup;
    }
    
    /* if executing "copy registers" on the remote implant */
    if (remote) {
        
        /* If IRQ_HKMESSREG is enabled in the ZL7010X on the remote implant,
         * temporarily disable it. This is done for the following reason: if the
         * implant is running ADK 2.0.0 or earlier, it monitors IRQ_HKMESSREG
         * to detect if the base changes a register, and if so, it immediately
         * executes "copy registers" on its own. If that happens, it will cause
         * the "copy registers" executed later in this function to fail. To
         * avoid this, the base disables IRQ_HKMESSREG on the implant before
         * executing "copy registers" on the implant, and re-enables it
         * afterwards. Note if the implant is running ADK 2.1.0 or later,
         * this isn't necessary, but it's harmless.
         */
        if ((regVal = BsmReadMicsReg(bsmId, IRQ_ENABLE2, TRUE, e)) < 0) goto cleanup;
        /**/    
        if (regVal & IRQ_HKMESSREG) {
            
            if (BsmWriteMicsReg(bsmId, IRQ_ENABLECLEAR2, IRQ_HKMESSREG, TRUE, e)) goto cleanup;
            enabIrq = TRUE;
        }
    }
    
    /* build command */
    cmd.remote = (BOOL8)remote;
    
    /* send command and receive reply */
    if (ComCmdAndReply(bsm->comId, BSM_LOCAL_ADDR, BSM_COPY_MICS_REGS_CMD_TYPE,
        &cmd, sizeof(cmd), NULL, 0, e)) {
        goto cleanup;
    }
    
    /* indicate success */
    rStat = 0;
    
cleanup:    
    /* if an error occurred, convert error (if it's from base) */
    if (rStat) (void)BsmConvertErr(e);
    
    /* if we need to re-enable IRQ_HKMESSREG on the remote implant, do so */ 
    if (enabIrq) {
        if (BsmWriteMicsReg(bsmId, IRQ_ENABLESET2, IRQ_HKMESSREG, TRUE, e)) rStat = -1;
    }
    
    return(rStat);
}

/**************************************************************************
 * FUNCTION: BsmGetStatChanges
 * 
 * This function gets changes in the status for the base station. Typically, an
 * application will call this periodically, such as once a second.
 * 
 * Note some sections in the status (sc) will only be updated if they changed
 * since the last time this function was called. If "force" is specified, this
 * function will also update sections that haven't changed. Note using "force"
 * is inefficient, so it should only be used to get an initial status. The
 * status flags (sc->genStat.flags) indicate which sections were updated,
 * as follows:
 * 
 * BSM_LINK_STAT_CHANGED:
 *     The sc->linkStat section was updated.
 * BSM_LINK_QUAL_CHANGED:
 *     The sc->linkQual section was updated.
 * BSM_DATA_STAT_CHANGED:
 *     The sc->dataStat section was updated.
 * BSM_ERR_OCCURRED:
 *     This indicates an error occurred in the base station. In this case, the
 *     function will return 0 since it got the status ok, but it will fill the
 *     specified error object (e) with the error information from the base.
 *     To access get the error information, see EiGetMsg(), etc. in
 *     "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c".
 * 
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     force:
 *         If true, also update sections of the status that haven't changed.
 *         Note using "force" is inefficient, so it should only be used to get
 *         an initial status.
 *     sc:
 *         Buffer in which to return the status changes. BSM_STAT_CHANGES is
 *         defined in "ZL7010XAdk\Sw\Includes\Adk\BsmMezz\BsmGeneral.h".
 *     scSize:
 *         The size of the specified BSM_STAT_CHANGES buffer (bytes). This
 *         is used to determine if the caller is using an older version of
 *         BSM_STAT_CHANGES, so it can be handled accordingly. That way,
 *         BsmGetStatChanges() will remain backwards compatible with previous
 *         verions of BSM_STAT_CHANGES.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If this function is successful, it puts the status in the specified
 *     buffer and returns 0. Otherwise, it fills the specified error object
 *     and returns -1 (to get the error, see EiGetMsg(), etc. in "AppDevPlat\
 *     Sw\Pc\Libs\ErrInfoLib.c").
 *
 *     Note if the base returns BSM_ERR_OCCURRED in sc->genStat.flags, this
 *     function will fill the specified error object with the error information
 *     from the base, but in this case, the function will still return 0
 *     since it got the status ok. To access the error information, see
 *     EiGetMsg(), etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c".
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate BSM_STAT_CHANGES buffer and error object.
 *
 * VISIBILITY: Customer
 */
#pragma warning(disable:4701) /* don't warn that "ls", etc. might not be init */
RSTAT
BsmGetStatChanges(BSM_ID bsmId, BOOL32 force, BSM_STAT_CHANGES *sc, UD32 scSize, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    BSM_GET_STAT_CHANGES_CMD cmd;
    UINT len, cmdLen, offset;
    MICS_LINK_QUAL lq;
    MICS_LINK_STAT ls;
    MICS_DATA_STAT dt;
    BSM_STAT gs;
    /*
     * Buffer to hold the status reply from the base station. Note this should
     * be larger than the maximum size anticipated for the status reply now
     * and in the future, so it will always hold the full reply, even if it's
     * from a newer version of the base. That way, older versions of this
     * software will remain compatible with newer versions of the base. If
     * an actual reply ever exceeds repBuf, ComCmdAndVarReply() will fail and
     * repBuf will have to be increased.
     */
    UD8 repBuf[sizeof(BSM_STAT_CHANGES) + 200];
    
    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    /* build command */
    if (force) {
        cmd.force = (BOOL8)force;
        cmdLen = sizeof(cmd);
    } else {
        cmdLen = 0;
    }
    
    /* get status changes from base station */
    if (ComCmdAndVarReply(bsm->comId, BSM_LOCAL_ADDR, BSM_GET_STAT_CHANGES_CMD_TYPE,
        &cmd, cmdLen, repBuf, sizeof(repBuf), e) < 0) {
        return(BsmConvertErr(e));
    }
    
    /* extract each section of the status that's included in the reply */
    offset = 0;
    len = repBuf[offset++];
    if (len < sizeof(gs)) return(BsmSizeErr(e));
    (void)memcpy(&gs, &repBuf[offset], sizeof(gs));
    if (bsm->swap) BsmSwapGenStat(&gs);
    offset += len;
    /**/ 
    if (gs.flags & BSM_LINK_STAT_CHANGED) {
        
        len = repBuf[offset++];
        if (len < sizeof(ls)) return(BsmSizeErr(e));
        (void)memcpy(&ls, &repBuf[offset], sizeof(ls));
        if (bsm->swap) BsmSwapLinkStat(&ls);
        offset += len;
    }
    if (gs.flags & BSM_LINK_QUAL_CHANGED) {
        
        len = repBuf[offset++];
        if (len < sizeof(lq)) return(BsmSizeErr(e));
        (void)memcpy(&lq, &repBuf[offset], sizeof(lq));
        if (bsm->swap) BsmSwapLinkQual(&lq);
        offset += len;
    }
    if (gs.flags & BSM_DATA_STAT_CHANGED) {
        
        len = repBuf[offset++];
        if (len < sizeof(dt)) return(BsmSizeErr(e));
        (void)memcpy(&dt, &repBuf[offset], sizeof(dt));
        if (bsm->swap) {
            dt.txBlockCount = SWAP16(dt.txBlockCount);
            dt.rxBlockCount = SWAP16(dt.rxBlockCount);
            dt.dataErrCount = SWAP16(dt.dataErrCount);
        }
        offset += len;
    }
    
    /* Copy each section of the status that changed into the corresponding
     * section of the specified buffer (BSM_STAT_CHANGES). Note for each section,
     * if the caller expects more than what's available, fail. It's ok if the
     * caller expects less (fewer fields). That way, if new fields are added
     * to the end of a section, BsmGetStatChanges() will remain compatible with
     * older software on the PC (since the caller doesn't expect the new
     * fields that were added to the end of the section, those fields simply
     * won't be returned to the caller).
     */
    #pragma warning(disable:4127) /* don't warn about if(constant expression) */
    switch(scSize) {
    case 26:
        if (sizeof(gs) < 2) return(BsmSizeErr(e));
        (void)memcpy((UD8 *)sc, &gs, 2);
        /**/
        if (gs.flags & BSM_LINK_STAT_CHANGED) {
            if (sizeof(ls) < 9) return(BsmSizeErr(e));
            (void)memcpy((UD8 *)sc + 2, &ls, 9);
        }
        if (gs.flags & BSM_LINK_QUAL_CHANGED) {
            if (sizeof(lq) < 8) return(BsmSizeErr(e));
            (void)memcpy((UD8 *)sc + 12, &lq, 8);
        }
        if (gs.flags & BSM_DATA_STAT_CHANGED) {
            if (sizeof(dt) < 6) return(BsmSizeErr(e));
            (void)memcpy((UD8 *)sc + 20, &dt, 6);
        }
        break;
    default:
        return(BsmSizeErr(e));
    }
    #pragma warning(default:4127)
        
    /* if error occurred on board, get error & set error info for caller */
    if (gs.flags & BSM_ERR_OCCURRED) {
        if (ComGetDevErr(bsm->comId, BSM_LOCAL_ADDR, e)) return(-1);
        (void)BsmConvertErr(e);
    }
    
    return(0);
}
#pragma warning(default:4701)

/**************************************************************************
 * FUNCTION: BsmStartDataTest
 * 
 * This function tells the base station to start a ZL7010X data test. Note if
 * a session is already active with an implant, the test will use the current
 * session. Otherwise, it will start a new session.
 * 
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     specs:
 *         The data test specifications (for description, see MICS_DT_SPECS in
 *         "ZL7010XAdk\Sw\Includes\Adk\AnyMezz\MicsGeneral.h").
 *     specsSize:
 *         The size of the specified MICS_DT_SPECS structure (bytes). Note
 *         BsmStartDataTest() won't reference anything in the structure beyond
 *         the specified size. That way, if new fields are added to the end of
 *         MICS_DT_SPECS, this function will remain compatible with older
 *         applications that don't specify the new fields (defaults will be
 *         used for the new fields).
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
BsmStartDataTest(BSM_ID bsmId, const MICS_DT_SPECS *specs, UD32 specsSize, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    BSM_START_DATA_TEST_CMD cmd;
    UINT cmdLen;
    
    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));
    
    /* Check size of passed specifications. Note the specified size can be
     * less than the current size of MICS_DT_SPECS. That way, if new fields
     * are added to the end of MICS_DT_SPECS, this interface will remain
     * compatible with older software on the PC.
     */
    if (specsSize > sizeof(cmd)) return(BsmSizeErr(e));
    
    /* build command and swap multi-byte fields (if needed) */
    memcpy(&cmd, specs, specsSize);
    cmdLen = specsSize;
    if (bsm->swap) {
        cmd.txBlockCount = SWAP16(cmd.txBlockCount);
    }
    
    /* send command and receive reply */
    if (ComCmdAndReply(bsm->comId, BSM_LOCAL_ADDR, BSM_START_DATA_TEST_CMD_TYPE,
        &cmd, cmdLen, NULL, 0, e)) {
        return(BsmConvertErr(e));
    }
    return(0);
}

/**************************************************************************
 * FUNCTION: BsmResetMics
 *
 * This function resets and re-initializes the ZL7010X on the base station.
 *
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 *
 * VISIBILITY: Customer
 */
RSTAT
BsmResetMics(BSM_ID bsmId, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;

    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));

    /* send command and receive reply */
    if (ComCmdAndReply(bsm->comId, BSM_LOCAL_ADDR, BSM_RESET_MICS_CMD_TYPE,
        NULL, 0, NULL, 0, e)) {
        return(BsmConvertErr(e));
    }

    return(0);
}

/**************************************************************************
 * FUNCTION: BsmCalcMicsCrc
 *
 * This function calculates the CRC for the ZL7010X on the base or the remote
 * implant (via housekeeping).
 *
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     remote:
 *         If true, the base will use housekeeping to calculate the CRC for
 *         the ZL7010X on the remote implant. Note the base must be in session
 *         with the implant (otherwise, the housekeeping will time out and
 *         this function will return an error).
 *
 *         If false, this function will calculate the CRC for the ZL7010X on
 *         the base. Note if the ZL7010X on the base never sleeps, this
 *         isn't needed, but it won't hurt anything.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 *
 * VISIBILITY: Customer
 */
RSTAT
BsmCalcMicsCrc(BSM_ID bsmId, BOOL remote, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    BSM_CALC_MICS_CRC_CMD cmd;
    RSTAT rStat = -1;
    BOOL enabIrq;
    int regVal;

    /* init resources referenced in cleanup logic */
    enabIrq = FALSE;

    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) {
        (void)BsmIdErr(e);
        goto cleanup;
    }

    /* if calculating CRC for ZL7010X on remote implant */
    if (remote) {

        /* If IRQ_HKMESSREG is enabled in the ZL7010X on the remote implant,
         * temporarily disable it. This is done for the following reason: if the
         * implant is running ADK 2.0.0 or earlier, it monitors IRQ_HKMESSREG
         * to detect if the base changes a register, and if so, it immediately
         * executes "copy registers" on its own. If that happens, it might
         * cause the CRC calculation executed later in this function to fail.
         * To avoid this, the base disables IRQ_HKMESSREG on the implant before
         * calculating the CRC on the implant, and re-enables it afterwards.
         * Note if the implant is running ADK 2.1.0 or later, this isn't
         * necessary, but it's harmless.
         */
        if ((regVal = BsmReadMicsReg(bsmId, IRQ_ENABLE2, TRUE, e)) < 0) goto cleanup;
        /**/
        if (regVal & IRQ_HKMESSREG) {

            if (BsmWriteMicsReg(bsmId, IRQ_ENABLECLEAR2, IRQ_HKMESSREG, TRUE, e)) goto cleanup;
            enabIrq = TRUE;
        }
    }

    /* build command */
    cmd.remote = (BOOL8)remote;

    /* send command and receive reply */
    if (ComCmdAndReply(bsm->comId, BSM_LOCAL_ADDR, BSM_CALC_MICS_CRC_CMD_TYPE,
        &cmd, sizeof(cmd), NULL, 0, e)) {
        goto cleanup;
    }

    /* indicate success */
    rStat = 0;

cleanup:
    /* if an error occurred, convert error (if it's from base) */
    if (rStat) (void)BsmConvertErr(e);

    /* if we need to re-enable IRQ_HKMESSREG on the remote implant, do so */
    if (enabIrq) {
        if (BsmWriteMicsReg(bsmId, IRQ_ENABLESET2, IRQ_HKMESSREG, TRUE, e)) rStat = -1;
    }

    return(rStat);
}

/**************************************************************************
 * FUNCTION: BsmWriteCcReg
 *
 * This function can be used to write to a register in the CC25XX chip used to
 * transmit 2.45 GHz wake-up. The addresses for many commonly used registers
 * are defined in "ZL7010XAdk\Sw\Includes\Adk\BsmMezz\CC25XXHw.h".
 *
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     regAddr:
 *         The address of the register to write to in the CC25XX.
 *     regVal:
 *         The value to write to the register.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 *
 * VISIBILITY: Factory
 */
RSTAT
BsmWriteCcReg(BSM_ID bsmId, UD32 regAddr, UD32 regVal, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    BSM_WRITE_CC_REG_CMD cmd;

    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));

    /* build command */
    cmd.regAddr = (UD8)regAddr;
    cmd.regVal  = (UD8)regVal;

    /* send command and receive reply */
    if (ComCmdAndReply(bsm->comId, BSM_LOCAL_ADDR, BSM_WRITE_CC_REG_CMD_TYPE,
        &cmd, sizeof(cmd), NULL, 0, e)) {
        return(BsmConvertErr(e));
    }
    return(0);
}

/**************************************************************************
 * FUNCTION: BsmReadCcReg
 *
 * This function can be used to read a register in the CC25XX radio on the
 * base station.
 *
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     regAddr:
 *         The address of the register to read in the CC25XX.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns the value of the register (always
 *     >= 0 because each register is only 8 bits). If not successful, this
 *     function fills the specified error object and returns -1 (to get the
 *     error, see EiGetMsg(), etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 *
 * VISIBILITY: Factory
 */
SD32
BsmReadCcReg(BSM_ID bsmId, UD32 regAddr, EI e)
{
    BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
    BSM_READ_CC_REG_CMD cmd;
    BSM_READ_CC_REG_REPLY rep;

    /* if specified id is invalid, fail */
    if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));

    /* build command */
    cmd.regAddr = (UD8)regAddr;

    /* send command and receive reply */
    if (ComCmdAndReply(bsm->comId, BSM_LOCAL_ADDR, BSM_READ_CC_REG_CMD_TYPE,
        &cmd, sizeof(cmd), &rep, sizeof(rep), e)) {
        return(BsmConvertErr(e));
    }

    /* return register value */
    return(rep.regVal);
}

/**************************************************************************
* FUNCTION: BsmWakeup
*
* This function tells the base station to Wakeup the MICS chip.
*
* PARAMETERS:
*     bsmId:
*         A connection ID returned by BsmOpen().
*
*     e:
*         Error object to use to return error information if the function
*         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
*
* RETURN VALUES:
*     If successful, this function returns 0. Otherwise, it fills the
*     specified error object and returns -1 (to get the error, see EiGetMsg(),
*     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
*
* THREAD SAFE:
*     Multiple threads may call this function, but each thread should specify
*     its own separate error object.
*
* VISIBILITY: Customer
*/
RSTAT
BsmWakeup(BSM_ID bsmId, EI e)
{
	BSM_PRIV *bsm = (BSM_PRIV *)bsmId;

	/* if specified id is invalid, fail */
	if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));

	/* send command and receive reply */
	if (ComCmdAndReply(bsm->comId, BSM_LOCAL_ADDR, BSM_WAKEUP_CMD_TYPE,
		NULL, 0, NULL, 0, e)) {
		return(BsmConvertErr(e));
	}

	return(0);
}

/**************************************************************************
 * FUNCTION: BsmSleep
 *
 * This function tells the base station to Sleep the MICS chip.
 *
 * PARAMETERS:
 *     bsmId:
 *         A connection ID returned by BsmOpen().
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 *
 * VISIBILITY: Customer
 */
RSTAT
BsmSleep(BSM_ID bsmId, EI e)
{
	BSM_PRIV *bsm = (BSM_PRIV *)bsmId;

	/* if specified id is invalid, fail */
	if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));

	/* send command and receive reply */
	if (ComCmdAndReply(bsm->comId, BSM_LOCAL_ADDR, BSM_SLEEP_CMD_TYPE,
		NULL, 0, NULL, 0, e)) {
		return(BsmConvertErr(e));
	}

	return(0);
}

/**************************************************************************
* FUNCTION: BsmGetMicsFactNvm
*
* This function gets the current factory NVM data from the base station.
*
* PARAMETERS:
*     bsmId:
*         A connection ID returned by BsmOpen().
*     fn:
*         Buffer in which to return the MICS_FACT_NVM structure. BSM_MICS_FACT_NVM
*         is defined in "ZL7010XAdk\Sw\Includes\Adk\BsmMezz\BsmGeneral.h".
*     fnSize:
*         The size of the specified buffer (bytes). Note BsmGetMicsFactNvm()
*         won't write beyond the specified size. That way, if new fields are
*         added to the end of BSM_MICS_FACT_NVM, this function will remain
*         compatible with older applications that don't use the new fields.
*     e:
*         Error object to use to return error information if the function
*         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
*
* RETURN VALUES:
*     If successful, this function puts the NVM structure in the
*     specified buffer and returns 0. If not successful, it fills the
*     specified error object and returns -1 (to get the error, see EiGetMsg(),
*     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
*
* THREAD SAFE:
*     Multiple threads may call this function, but each thread should specify
*     its own separate BSM_MICS_FACT_NVM buffer and error object.
*
* VISIBILITY: Customer
*/
RSTAT
BsmGetMicsFactNvm(BSM_ID bsmId, BSM_MICS_FACT_NVM *fn, UD32 fnSize, EI e)
{
	BSM_PRIV *bsm = (BSM_PRIV *)bsmId;
	SD32 fnLen;

	union {
		BSM_MICS_FACT_NVM fn;
		UD8 buf[64];
	} fnBuf;

	/* if specified id is invalid, fail */
	if ((bsm == NULL) || (bsm->magic != BSM_MAGIC)) return(BsmIdErr(e));

	/* Get the factory NVM from the base station. Note fnBuf should always be
	* larger than the BSM_MICS_FACT_NVM structure, so this will always read the
	* full structure available from the board, whatever the board's version. If
	* BSM_MICS_FACT_NVM ever outgrows fnBuf, the worst that will happen is
	* ComCmdAndVarReply() will fail during development and fnBuf will have
	* to be increased.
	*/
	if ((fnLen = ComCmdAndVarReply(bsm->comId, BSM_LOCAL_ADDR,
		BSM_GET_MICS_FACT_NVM_CMD_TYPE, NULL, 0, &fnBuf, sizeof(fnBuf), e)) < 0) {
		return(BsmConvertErr(e));
	}

	/* If the caller expects a larger config than is available, fail. It's ok
	* if the caller expects a smaller config (fewer fields). That way, if new
	* fields are added to the end of BSM_MICS_FACT_NVM, this interface will
	* remain compatible with older software on the PC - since the caller
	* doesn't expect the new fields that were added to the end of
	* BSM_MICS_FACT_NVM, those fields simply won't be returned to the caller.
	*/
	if (fnSize > (UD32)fnLen) return(BsmSizeErr(e));
	(void)memcpy(fn, &fnBuf, fnSize);

	return(0);
}

/**************************************************************************
 * FUNCTION: BsmCheckNvm
 *
 * This checks if the specified node NVM ("nvm") is valid, and if it is the
 * same version (size) as the current BSM_NVM structure for the host (PC)
 * software. If so, it returns 0. Otherwise, it fills the specified error
 * object and returns a negative value to indicate the cause of the error
 * (see RETURN VALUES below).
 *
 * PARAMETERS:
 *     nvm:
 *         The node NVM to check. Note that this is a void pointer instead
 *         of a BSM_NVM pointer because the specified NVM might be an older
 *         (smaller) or a newer (larger) version that is not the same size as
 *         the current BSM_NVM structure for the host (PC) software.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c."
 *
 * RETURN VALUES:
 *     If the specified NVM is valid, and is the same version (size) as the
 *     current BSM_NVM structure for the host (PC) software, this function
 *     returns 0 (BSM_NVM_IS_VALID). If the specified NVM is an older (smaller)
 *     version of the structure, this function fills the specified error object
 *     and returns BSM_NVM_IS_OLDER_VER. If any other error occurs (e.g. the
 *     specified NVM is invalid, or is a newer (larger) version that the host
 *     (PC) software does not support), this function fills the specified error
 *     object and returns BSM_NVM_OTHER_ERR. To get the error information,
 *     see EiGetMsg(), etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c".
 *
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     it's own separate error object, or protect it with a semaphore to ensure
 *     only one thread uses it at a time.
 */
#pragma warning(disable:4305) /* don't warn about cast in BSM_NVM_OFFSET() */
BSM_NVM_RSTAT
BsmCheckNvm(void *nvm, EI e)
{
    BSM_NVM_HEAD *h;
    UD16 nvmSize, sectSize, sectOffset;

    /* init pointer to NVM header */
    h = (BSM_NVM_HEAD *)nvm;

    /* if the NVM header is invalid, fail */
    if (GET_UD32(&h->nvmInfo.nvmInitKey) != NVM_INIT_KEY) {
        EiSet(e, BSM_LIB_EID_NVM_IS_NOT_INIT, NULL);
        return(BSM_NVM_OTHER_ERR);
    }
    if (GET_UD16(&h->nvmInfo.nvmType) != NVM_TYPE_10X_ADK_BSM) {
        EiSet(e, BSM_LIB_EID_NVM_IS_WRONG_TYPE, NULL);
        return(BSM_NVM_OTHER_ERR);
    }
    if ((nvmSize = GET_UD16(&h->nvmInfo.nvmSize)) > sizeof(BSM_NVM)) {
        EiSet(e, BSM_LIB_EID_NVM_IS_NEWER_VER, NULL);
        return(BSM_NVM_OTHER_ERR);
    }
    if ((h->nvmHeadSize == 0) || (h->nvmHeadSize >= nvmSize) ||
        (h->nvmHeadSize > sizeof(BSM_NVM_HEAD))) {
        EiSet(e, BSM_LIB_EID_NVM_HEAD_IS_INVALID, NULL);
        return(BSM_NVM_OTHER_ERR);
    }

    /* if NVM contains micsFactNvm section, check its size & offset */
    if (h->nvmHeadSize > BSM_NVM_OFFSET(head.micsFactNvmOffset)) {

        /* get offset & size for micsFactNvm section from NVM header */
        sectOffset = h->micsFactNvmOffset;
        sectSize = h->micsFactNvmSize;

        /* if size or offset for micsFactNvm section is invalid, fail */
        if ((sectSize == 0) || (sectSize > sizeof(BSM_MICS_FACT_NVM)) ||
            (sectOffset < h->nvmHeadSize) || ((sectOffset + sectSize) > nvmSize)) {

            EiSet(e, BSM_LIB_EID_NVM_HEAD_IS_INVALID, NULL);
            return(BSM_NVM_OTHER_ERR);
        }
    }

   /* If the NVM is an older (smaller) version, return a unique error value
    * so the caller can detect it if desired.
    */
    if (nvmSize < sizeof(BSM_NVM)) {
        EiSet(e, BSM_LIB_EID_NVM_IS_OLDER_VER, NULL);
        return(BSM_NVM_IS_OLDER_VER);
    }

    return(BSM_NVM_IS_VALID);
}

/*****************************************************************************
 * FUNCTION BsmTransferNvm
 *
 * This transfers the settings from the specified previous NVM ("prevNvm")
 * to the specified BSM_NVM structure ("nvm"). Note that the caller must
 * initialize everything in the BSM_NVM structure before passing it to this
 * function, including the NVM headers, and default values for all settings.
 * If the previous NVM is an older (smaller) version of the BSM_NVM structure
 * that does not contain all of the settings that exist in the current BSM_NVM
 * structure, this function leaves the new settings in the BSM_NVM structure
 * at the default values initialized by the caller. If an error occurs (e.g.
 * one of the NVMs is invalid, or the previous NVM is a newer (larger) version
 * that the host (PC) software doesn't support), this fills the specified error
 * object and returns -1.
 *
 * PARAMETERS:
 *     prevNvm:
 *         The previous NVM. Note that this is a void pointer instead of a
 *         BSM_NVM pointer because the previous NVM might be an older (smaller)
 *         or a newer (larger) version that is not the same size as the current
 *         BSM_NVM structure for the host (PC) software.
 *     nvm:
 *         The BSM_NVM structure to transfer the previous NVM into.
 *     nvmSize:
 *         The size of the specified BSM_NVM structure (bytes).
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. If an error occurs (e.g. one
 *     of the NVMs is invalid, or the previous NVM is a newer (larger) version
 *     that the host (PC) software does not support), this function fills the
 *     specified error object and returns -1 (to get the error information,
 *     see EiGetMsg(), etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     it's own separate BSM_NVM structure and error object, or protect them
 *     with a semaphore to ensure only one thread uses them at a time.
 */
#pragma warning(disable:4305) /* don't warn about cast in BSM_NVM_OFFSET() */
RSTAT
BsmTransferNvm(void *prevNvm, BSM_NVM *nvm, UD32 nvmSize, EI e)
{
    BSM_NVM_HEAD *prevH;
    BSM_NVM_RSTAT nnrStat;
    UD16 prevSectSize;
    void *prevSect;

    /* if the NVM size for the caller & this API don't match, fail */
    if (nvmSize != sizeof(BSM_NVM)) return(BsmSizeErr(e));

    /* if previous NVM isn't valid and isn't an older version, fail */
    if (((nnrStat = BsmCheckNvm(prevNvm, e)) != BSM_NVM_IS_VALID) &&
        (nnrStat != BSM_NVM_IS_OLDER_VER)) {
        return(-1);
    }

    /* if new NVM isn't valid, fail */
    if (BsmCheckNvm(nvm, e)) return(-1);

    /* init pointer to previous NVM header */
    prevH = (BSM_NVM_HEAD *)prevNvm;

    /* if previous NVM contains micsFactNvm section, copy it to new NVM */
    if (prevH->nvmHeadSize > BSM_NVM_OFFSET(head.micsFactNvmOffset)) {

        prevSect = (UD8 *)prevNvm + prevH->micsFactNvmOffset;
        prevSectSize = prevH->micsFactNvmSize;
        /**/
        if (prevSectSize > sizeof(nvm->micsFactNvm)) {
            EiSet(e, BSM_LIB_EID_NVM_HEAD_IS_INVALID, NULL);
            return(-1);
        }
        (void)memcpy(&nvm->micsFactNvm, prevSect, prevSectSize);
    }

    return(0);
}
#pragma warning(default:4305)

#pragma warning(disable:4100) /* don't warn that "genStat" isn't referenced */
/**/
static void
BsmSwapGenStat(BSM_STAT *genStat)
{
    /* currently, BSM_STAT doesn't have any multi-byte fields to swap */
    return;
}
#pragma warning(default:4100)

#pragma warning(disable:4100) /* don't warn that "ls" isn't referenced */
static void
BsmSwapLinkStat(MICS_LINK_STAT *ls)
{
    /* currently, MICS_LINK_STAT doesn't have any multi-byte fields to swap */
    return;
}
#pragma warning(default:4100)

static void
BsmSwapLinkQual(MICS_LINK_QUAL *lq)
{
    lq->maxBErrInts = SWAP16(lq->maxBErrInts);
    lq->maxRetriesInts = SWAP16(lq->maxRetriesInts);
    lq->errCorBlocks = SWAP16(lq->errCorBlocks);
    lq->crcErrs = SWAP16(lq->crcErrs);
}

static RSTAT
BsmSizeErr(EI e)
{
    EiSet(e, BSM_LIB_EID_INVALID_STRUCT_SIZE, NULL);
    return(-1);
}

static RSTAT
BsmIdErr(EI e)
{
    EiSet(e, BSM_LIB_EID_INVALID_BSM_ID, NULL);
    return(-1);
}

/* If the specified error is from the base station, this will convert the error
 * so it contains a specific error message (errors from the base station only
 * contain a generic message). Note this will only change the error's message,
 * not the error group or code. This always returns -1 so it can be passed to
 * a return statement, if desired.
 */
static RSTAT
BsmConvertErr(EI e)
{
    const char *eid;

    /* get original error ID string */
    eid = EiGetId(e);

    /* If it's an error from the base station, convert it to a new error ID
     * string that contains a specific error message.
     */
    if (EiGroupIs(e, BSM_APP_ERR)) {

        /* extract error code from original error ID string and convert it */
        switch(atoi(&eid[sizeof(BSM_APP_ERR)])) {
        case BSM_APP_ERR_MICS_DATA_NOT_YET_SUPPORTED:
            eid = BSM_APP_EID_MICS_DATA_NOT_YET_SUPPORTED; break;
        case BSM_APP_ERR_RECEIVED_CMD_WITH_INVALID_CMD_TYPE:
            eid = BSM_APP_EID_RECEIVED_CMD_WITH_INVALID_CMD_TYPE; break;
        case BSM_APP_ERR_RECEIVED_CMD_WITH_INVALID_LEN:
            eid = BSM_APP_EID_RECEIVED_CMD_WITH_INVALID_LEN; break;
        case BSM_APP_ERR_MICS_DATA_RX_TIMEOUT:
            eid = BSM_APP_EID_MICS_DATA_RX_TIMEOUT; break;
        case BSM_APP_ERR_MICS_DATA_TX_TIMEOUT:
            eid = BSM_APP_EID_MICS_DATA_TX_TIMEOUT; break;
        case BSM_APP_ERR_MICS_HK_RX_TIMEOUT:
            eid = BSM_APP_EID_MICS_HK_RX_TIMEOUT; break;
        case BSM_APP_ERR_MICS_HK_TX_TIMEOUT:
            eid = BSM_APP_EID_MICS_HK_TX_TIMEOUT; break;
        case BSM_APP_ERR_ADP_SPI_RX_TIMEOUT:
            eid = BSM_APP_EID_ADP_SPI_RX_TIMEOUT; break;
        case BSM_APP_ERR_ADP_SPI_TX_TIMEOUT:
            eid = BSM_APP_EID_ADP_SPI_TX_TIMEOUT; break;
        case BSM_APP_ERR_RECEIVED_PACK_WITH_DEST_0:
            eid = BSM_APP_EID_RECEIVED_PACK_WITH_DEST_0; break;
        case BSM_APP_ERR_MICS_INT_WITH_NO_STAT:
            eid = BSM_APP_EID_MICS_INT_WITH_NO_STAT; break;
        case BSM_APP_ERR_MICS_CRC_ERR:
            eid = BSM_APP_EID_MICS_CRC_ERR; break;
        case BSM_APP_ERR_INVALID_TX_LEN:
            eid = BSM_APP_EID_INVALID_TX_LEN; break;
        case BSM_APP_ERR_MICS_SYNTH_FAILED_TO_LOCK:
            eid = BSM_APP_EID_MICS_SYNTH_FAILED_TO_LOCK; break;
        case BSM_APP_ERR_START_SESSION_TIMEOUT:
            eid = BSM_APP_EID_START_SESSION_TIMEOUT; break;
        case BSM_APP_ERR_WRONG_MICS_REV_ON_BASE:
            eid = BSM_APP_EID_WRONG_MICS_REV_ON_BASE; break;
        case BSM_APP_ERR_WRONG_MICS_REV_ON_IMPLANT:
            eid = BSM_APP_EID_WRONG_MICS_REV_ON_IMPLANT; break;
        case BSM_APP_ERR_NOT_ALLOWED_TO_CHANGE_COMPANY_ID:
            eid = BSM_APP_EID_NOT_ALLOWED_TO_CHANGE_COMPANY_ID; break;
        default:
            eid = NULL; break; /* don't convert error */
        }

    } else if (EiGroupIs(e, MICS_LIB_ERR)) {

        /* extract error code from original error ID string and convert it */
        switch(atoi(&eid[sizeof(MICS_LIB_ERR)])) {
        case MICS_LIB_ERR_REMOTE_HK_TIMEOUT:
            eid = MICS_LIB_EID_REMOTE_HK_TIMEOUT; break;
        case MICS_LIB_ERR_REMOTE_HK_WRITES_DISABLED:
            eid = MICS_LIB_EID_REMOTE_HK_WRITES_DISABLED; break;
        case MICS_LIB_ERR_REMOTE_ADC_TIMEOUT:
            eid = MICS_LIB_EID_REMOTE_ADC_TIMEOUT; break;
        case MICS_LIB_ERR_ABORT_LINK_TIMEOUT:
            eid = MICS_LIB_EID_ABORT_LINK_TIMEOUT; break;
        case MICS_LIB_ERR_CALC_CRC_TIMEOUT:
            eid = MICS_LIB_EID_CALC_CRC_TIMEOUT; break;
        case MICS_LIB_ERR_MAC_CMD_TIMEOUT:
            eid = MICS_LIB_EID_MAC_CMD_TIMEOUT; break;
        case MICS_LIB_ERR_ADC_TIMEOUT:
            eid = MICS_LIB_EID_ADC_TIMEOUT; break;
        case MICS_LIB_ERR_WAKEUP_TIMEOUT:
            eid = MICS_LIB_EID_MICS_WAKEUP_TIMEOUT; break;
        case MICS_LIB_ERR_ATTEMPT_TO_ACCESS_REMOTE_PAGE_2_VIA_HK:
            eid = MICS_LIB_EID_ATTEMPT_TO_ACCESS_REMOTE_PAGE_2_VIA_HK; break;
        case MICS_LIB_ERR_TIMED_OUT_WAITING_FOR_TX_MODE:
            eid = MICS_LIB_EID_TIMED_OUT_WAITING_FOR_TX_MODE; break;
        case MICS_LIB_ERR_ZL7010X_DID_NOT_SET_SYNTH_CT_READY:
            eid = MICS_LIB_EID_ZL7010X_DID_NOT_SET_SYNTH_CT_READY; break;
        case MICS_LIB_ERR_SYNTH_DID_NOT_LOCK_FOR_SOME_CHAN:
            eid = MICS_LIB_EID_SYNTH_DID_NOT_LOCK_FOR_SOME_CHAN; break;
        case MICS_LIB_ERR_ZL7010X_DID_NOT_SET_SYNTH_CTRIM_READY:
            eid = MICS_LIB_EID_ZL7010X_DID_NOT_SET_SYNTH_CTRIM_READY; break;
        case MICS_LIB_ERR_SYNTH_CTRIM_READY_NOT_SET:
            eid = MICS_LIB_EID_SYNTH_CTRIM_READY_NOT_SET; break;
        default:
            eid = NULL; break; /* don't convert error */
        }

    } else { /* unknown error group */

        eid = NULL; /* don't convert error */
    }

    /* If there's a new error ID string for the error, call EiSet() to update
     * the error information. Note the error arguments string (returned by
     * EiGetArgs()) is passed as a single string via "%s". This won't change
     * the arguments string, so the new error information will still contain
     * the same arguments string.
     */
    if (eid != NULL) EiSet(e, eid, "%s", EiGetArgs(e));

    return(-1);
}
