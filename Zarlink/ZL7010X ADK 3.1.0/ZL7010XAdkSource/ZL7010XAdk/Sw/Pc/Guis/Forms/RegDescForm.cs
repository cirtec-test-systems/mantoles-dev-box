//
//      This file contains the public class RegDescForm.
// 
// CLASS: RegDescForm
//      Displays information about register that was double-clicked.
//      Information includes:
//          Address
//          Name
//          Type - either read, write, or both
//          Bits - how many active bits are in the register
//          Block
//          Subblock
//          Crc
//          Reset Value
//          Usage
//          Description
//          UsageComments
// 
// NOTES:
//      Current design limitations:
//
// Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2012. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.

using System.Windows.Forms;
using Zarlink.Adk.Api;

namespace Zarlink.Adk.Forms
{
    public partial class RegDescForm : Form
    {
        public RegDescForm(GroupBox p, RegInfo ri)
        {
            RegInfo.REG_INFO[,] regInfo = ri.regInfo;
            uint page = (((uint)((int)p.Tag) & CommonApi.P2) != 0) ? 1U : 0U;
            uint reg = ((uint)((int)p.Tag)) & 0x7F;
            
            InitializeComponent();

            txtRegAddr.Text = regInfo[page, reg].Addr.ToString() + ", 0x" +
                ri.regInfo[page, reg].Addr.ToString("X2");
            txtRegName.Text = regInfo[page, reg].Name;
            txtRegBits.Text = regInfo[page, reg].Bits.ToString();
            txtRegType.Text = regInfo[page, reg].Type;
            txtRegCrc.Text = regInfo[page, reg].Crc;
            txtRegReset.Text = regInfo[page, reg].ResetVal;
            txtRegCategory.Text = regInfo[page, reg].Category;
            txtRegDesc.Text = regInfo[page, reg].Description;
        }
    }
}
