//
// This file contains the public class AdkTraceViewForm, which provides the
// trace view for the ZL7010X ADK implant and base station modules.
//
// Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2012. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using Zarlink.Adk.Api;

namespace Zarlink.Adk.Forms
{
    public partial class AdkTraceViewForm : Form
    {
        [DllImport("user32.dll")]
        public static extern Int32 LockWindowUpdate(Int32 hwndLock);
        [DllImport("shlwapi.dll", CharSet = CharSet.Auto)]
        public static extern bool PathRemoveFileSpec([In, Out] StringBuilder pszPath);
        
        // ADK form (used to access parent form for implant or base station)
        private AdkForm adkForm = null;
        
        // true if monitoring trace messages from an implant
        private bool implant = false;
        
        // selection for board to get trace messages from (ADP or mezzanine)
        private int boardIndex = 0;

        // used to write trace messages to log file (null if log disabled)
        private StreamWriter logStream = null;
        
        // time for previous trace message (used to calc time delta)
        private int prevTime = -1; 
        
        // used to build the next line to write to the log file
        private string nextLogLine = String.Empty;
        
        // true when we've already reported the polling has been suspended
        private bool reportedSuspend = false;
        
        // current poll rate selection
        private int pollRateIndex = 0;
        
        // max number of lines in the trace window
        private int maxLinesInTraceWin = 1000;
        
        public AdkTraceViewForm(bool implant, AdkForm adkForm)
        {
            InitializeComponent();
            
            // indicate if trace view is for implant or base station
            this.implant = implant;

            // save ADK form (used to access parent form for implant or base)
            this.adkForm = adkForm;

            // In the title bar, indicate if this trace view is for an implant
            // or base station (to avoid confusion if both are up).
            //
            if (implant) {
                Text = "Trace Window for Implant Module";
            } else  {
                Text = "Trace Window for Base Station Module";
            }

            // Build path to log file (locate in same folder as executable).
            //
            StringBuilder sb = new StringBuilder(Application.ExecutablePath);
            //
            bool b = PathRemoveFileSpec(sb);
            //
            if (implant) {
                sb.Append(@"\ImplantTraceLog.txt");
                btnEraseLogFile.Text = "Erase Log File (ImplantTraceLog.txt)";
            } else {
                sb.Append(@"\BaseTraceLog.txt");
                btnEraseLogFile.Text = "Erase Log File (BaseTraceLog.txt)"; 
            }
            traceDirTextBox.Text = sb.ToString();
        }
        
        private void AdkTraceViewForm_Load(object sender, EventArgs e)
        {
            // set initial indexes to display in combo boxes
            cmbTracePollFreq.SelectedIndex = pollRateIndex = 0;  // poll once
            cmbWhichDeviceToTrace.SelectedIndex = 1;  // mezzanine board
            cmbMaxTraceTextBoxLines.SelectedIndex = 3;  // max lines = 1000
        }
        
        // Called when the trace start/stop button is clicked.
        //
        private void btnStartRdTraceStop_Click(object sender, EventArgs e)
        {
            if (btnStartRdTraceStop.Text == "Start Reading Trace Data") {
            
                // if "poll once" is selected
                if (cmbTracePollFreq.SelectedIndex == 0) {
                
                    // get available messages from board
                    getMessages();
                    
                } else {
                
                    showMessage("\nPolling started ...\n");
                
                    // update poll rate to start polling
                    updatePollRate();

                    // toggle the button text
                    btnStartRdTraceStop.Text = "Stop Reading Trace Data";
                }

            } else  {
            
                // stop trace polling (also toggles button text) 
                stopPolling();
            }
        }

        // This updates the trace polling rate to the currently selected value.
        //
        private void updatePollRate()
        {
            // save current poll rate selection
            this.pollRateIndex = cmbTracePollFreq.SelectedIndex;
            
            // if "poll once" is selected, make sure polling is stopped
            if (pollRateIndex == 0) {
            
                // stop trace polling (does nothing if already stopped)
                stopPolling();
                
            } else {
            
                // set timer based on the frequency from the list
                double sec = 1.0 / pollRateIndex;

                // timer wants ms for interval
                timerTracePolling.Interval = Convert.ToInt32(sec * 1000.0);
                
                // turn on timer events
                timerTracePolling.Enabled = true;

                // report the interval on the screen for information
                labCurTimerInterval.Text = Convert.ToString(timerTracePolling.Interval) + " ms";
            }
        }
        
        // The polling timer calls this periodically to poll the board for
        // trace messages. The frequency depends on the selected polling rate.
        //
        private void timerTracePolling_Tick(object sender, EventArgs e)
        {
            // If the user selected a different poll rate, update rate. Note
            // if the drop-down list is displayed, the user is still making a
            // choice, so don't update the rate. That way, the rate won't keep
            // changing as the user moves the mouse over the list.
            //
            if ((cmbTracePollFreq.SelectedIndex != pollRateIndex) &&
                (cmbTracePollFreq.DroppedDown == false)) {
                
                updatePollRate();
            }
            
            // if parent GUI has been closed, don't try to poll (suspended)
            if ((implant && (adkForm.imForm == null)) ||
                (!implant && (adkForm.bsmForm == null))) {
                
                // if we haven't reported the suspend, do so
                if (reportedSuspend == false) {
                    showMessage("\nPolling suspended because parent GUI was closed ...\n");
                    reportedSuspend = true;
                }
                return;
            }

            // if we reported the polling was suspended, report it's resumed
            if (reportedSuspend) {
            
                showMessage("\nPolling resumed ...\n");
                reportedSuspend = false;
            }
            
            // get available trace messages from board
            getMessages();
        }

        private void stopPolling()
        {
            // allow this to be called even when polling is off by checking first
            if (timerTracePolling.Enabled == true)
            {
                // no more timer events
                timerTracePolling.Enabled = false;
                
                // fix button caption since we are essentially turning polling off
                btnStartRdTraceStop.Text = "Start Reading Trace Data";

                // report the interval on the screen for information
                labCurTimerInterval.Text = "Polling Stopped";
                
                // clear this in case it was set while polling
                reportedSuspend = false; 
                
                showMessage("\nPolling stopped.\n");
            }
        }
        
        // Get available trace messages from board. Returns 0 if there were
        // messages, -1 if not.
        //
        private void getMessages()
        {
            // while there are messages to get, continue to get them
            while(getMessage() == 0);

            // remove any excess old lines in trace window
            limitTraceWinLines();
        }

        // Get next trace message from board. Returns 0 if it gets a message,
        // or -1 if there are no more messages at this time, or we're unable
        // to get a message for some reason.
        //
        private int getMessage()
        {
            string msg = String.Empty;
            string getTraceErr = null;

            // if parent GUI is open, get trace messages
            if (implant && (adkForm.imForm != null)) {
                
                if (boardIndex == 0) {
                    if (adkForm.imForm.adkMod.getAdpTraceString(ref msg) != 0) {
                        getTraceErr = msg;
                    }
                } else { 
                    if (adkForm.imForm.imMod.getTraceString(ref msg) != 0) {
                        getTraceErr = msg;
                    }
                }
                
            } else if (!implant && (adkForm.bsmForm != null)) {
            
                if (boardIndex == 0) {
                    if (adkForm.bsmForm.adkMod.getAdpTraceString(ref msg) != 0) {
                        getTraceErr = msg;
                    }
                } else {
                    if (adkForm.bsmForm.bsmMod.getTraceString(ref msg) != 0) {
                        getTraceErr = msg;
                    }
                }
                    
            } else { // else, parent GUI isn't open
            
                showMessage("\nCan't get trace messages because parent GUI is closed.\n");
                
                // tell caller we didn't get a message
                return(-1);
            }
            
            // if failed to get trace message
            if (getTraceErr != null) {
            
                // extend error message
                string err = "Failed to get trace message: " + getTraceErr;
            
                // show error in trace window & write to log file if open
                showMessage("\n" + err + "\n");
            
                // stop trace polling
                stopPolling();
                
                // display error in a message box
                MessageBox.Show(err);
                
                // tell caller we didn't get a message
                return(-1);
            }
            
            // if no message, tell caller there's no more messages at this time 
            if (msg == String.Empty) return(-1);
            
            // show message in trace window & write to log file if open
            showMessage(msg);
            
            // tell caller we got a message
            return(0);
        }
        
        // Show message in trace window. This also writes the message to the
        // log file if it's open.
        //
        private void showMessage(string msg)
        {
            // if empty messsage, do nothing
            if (msg == String.Empty) return;
            
            // If we want to display the delta time between messages, and
            // the message begins with a time stamp, do so. A time stamped
            // message begins with "Ms " or "Us ", followed by a 5 digit
            // time, followed by ": ". For example:
            //  
            // "Ms 34126: This is a trace message".
            //
            if (chbShowDeltaT.Checked && (msg.Length >= 10) &&
                ((msg[0] == 'U') || (msg[0] == 'M')) && (msg[1] == 's') &&
                (msg[2] == ' ') && (msg[8] == ':') && (msg[9] == ' ')) {
                    
                // extract time from message
                string sTime = msg.Substring(3, 5);
                int time = Convert.ToInt32(sTime);
                    
                // Calc time delta between this message and previous
                // message. Note this handles the case in which the time
                // wrapped between messages, but it can only detect one
                // wrap. If the time wraps more than once between messages,
                // the delta time won't reflect the full amount of time.
                //
                int deltaTime;
                if (this.prevTime == -1) {
                    deltaTime = 0;
                } else if ((deltaTime = time - this.prevTime) < 0) {
                    deltaTime = (time + 65535) - this.prevTime;
                }
                    
                // extract time prefix and message without time stamp
                string timePrefix = msg.Substring(0, 8);
                string msgSuffix = msg.Substring(10, msg.Length - 10);
                    
                // add delta time to message
                msg = timePrefix + deltaTime.ToString(" (+000000): ") + msgSuffix;
                    
                this.prevTime = time;
            }
            
            // replace any "\n" not already preceded by "\r" with "\r\n"
            msg = msg.Replace("\r\n", "\n");
            msg = msg.Replace("\n", "\r\n");
            
            // if log file is open, write message to log file
            if (logStream != null) {
            
                // if message ends in "\n", write next line to log file
                if (msg[msg.Length - 1] == '\n') {
                
                    // strip "\r\n" at end of message and append to log line
                    nextLogLine += msg.Remove(msg.Length - 2);
                
                    // write next line to log file
                    logStream.WriteLine("{0}", nextLogLine);
                    nextLogLine = String.Empty;
                    logStream.Flush();
                    
                } else { // else, append message to end of next line for log file
                
                    nextLogLine += msg;
                }
            }
                
            // show message in trace window
            tbTraceDataDisplay.AppendText(msg);
        }
        
        // If the number of lines in the trace window exceeds the maximum,
        // this removes enough old lines to bring it back down to the maximum.
        //
        private void limitTraceWinLines()
        {
            // get number of lines currently in trace window
            int nLines = tbTraceDataDisplay.Lines.Length;

            // if there too many lines, remove old lines
            if (nLines > maxLinesInTraceWin) {
            
                // calc number of old lines to remove
                int nRemove = nLines - maxLinesInTraceWin;

                // create a new empty array
                string[] newLines = new string[nLines - nRemove];

                // copy the newer lines into the new array 
                int i = 0; int j = 0;
                foreach(string line in tbTraceDataDisplay.Lines) {
                    if (i++ >= nRemove) newLines[j++] = line;
                }

                // I noticed a visual flashing which could be observed in the
                // following situation: the first 500 lines in the textbox are
                // long lines, the next lines are short lines.  When the total
                // gets to > 1000 I could clearly see the longer lines briefly
                // flashing on in the text box and then quickly they would
                // be replaced with the more recent short lines.  I deduced that
                // this was caused by the screen being updated after the
                // assignment to the new (shortened) buffer (which still has the
                // long lines at the front) and before the scroll to the end
                // where the shorter lines were. I dug out this as a potential
                // solution and it seemed to work.
                //
                // There is a bizzare behavior when this window is minimized
                // and I try to use LockWindowUpdate():  the icons on the desktop
                // flash every time the polling happened (but only when the trace 
                // view form is minimized).  Here I will test for minimized, and
                // only call LockWindowUpdate if the form is not minimized.
                //
                bool winUpdateLocked = false;
                if (this.WindowState != FormWindowState.Minimized) {
                
                    LockWindowUpdate(this.Handle.ToInt32());
                    winUpdateLocked = true;
                }
                
                // update lines in trace window
                tbTraceDataDisplay.Lines = newLines;
                
                // put cursor at end of trace window
                tbTraceDataDisplay.SelectionStart = tbTraceDataDisplay.Text.Length;
                tbTraceDataDisplay.ScrollToCaret();
                
                // if we locked the window update, unlock it
                if (winUpdateLocked) LockWindowUpdate(0);
            }
        }

        private void btnClearTraceTextBox_Click(object sender, EventArgs e)
        {
            tbTraceDataDisplay.Text = String.Empty;
        }

        private void cmbWhichDeviceToTrace_SelectedIndexChanged(object sender, EventArgs e)
        {
            // save board to get trace messages from
            boardIndex = cmbWhichDeviceToTrace.SelectedIndex;
        }

        private void labCurTimerInterval_Click(object sender, EventArgs e)
        {
        }

        private void AdkTraceViewForm_Resize(object sender, EventArgs e)
        {
            // get the graphic object from the handle
            Graphics g = Graphics.FromHwnd(this.Handle);
            
            // get the bounds of your box
            RectangleF formRect = g.VisibleClipBounds;

            // Note this gutter (between the left edge of the TextBox and
            // the left edge of the form) and make the right and bottom 
            // gutters the same.
            //
            int curLeftGutter = tbTraceDataDisplay.Location.X;

            // Try to make the gutter on the right side of this textbox 
            // the same as the gutter on the left...we need the *2 since we
            // are going from a *Location* to a *width* (think about it!).
            //
            System.Drawing.Size tSize = tbTraceDataDisplay.Size;
            tSize.Width = (int)formRect.Width - (2 * curLeftGutter);

            // We cannot go too far wrong by making the *bottom* gutter
            // the same as the left and right gutters
            //
            tSize.Height = (int)formRect.Height - tbTraceDataDisplay.Location.Y - curLeftGutter;

            // this becomes the new TextBox size
            tbTraceDataDisplay.Size = tSize;

            // also directory string box
            tSize = traceDirTextBox.Size;
            tSize.Width = (int)formRect.Width - traceDirTextBox.Location.X - curLeftGutter;
            traceDirTextBox.Size = tSize;
        }

        private void btnBrowseForTraceLogFile_Click(object sender, EventArgs e)
        {
        }

        private void btnReportTraceTextboxStats_Click(object sender, EventArgs e)
        {
            string nLines = tbTraceDataDisplay.Lines.Length.ToString();

            lblTraceTextBoxStats.Text = "Lines = " + nLines;
        }

        private void traceTextBox_TextChanged(object sender, EventArgs e)
        {
        }

        private void cbLogTraceDataToFile_CheckedChanged(object sender, EventArgs e)
        {
            if (cbLogTraceDataToFile.Checked) {
                openLogFile();
            } else {
                closeLogFile();
            }
        }
        
        private void openLogFile()
        {
            int i, n;
            
            // if log file is already open, do nothing (shouldn't happen)
            if (logStream != null) return;
                
            // open log file
            try {
                logStream = File.AppendText(traceDirTextBox.Text);
            } catch (Exception ex) {    
                MessageBox.Show("Failed to open log file: " + ex.Message);
                cbLogTraceDataToFile.Checked = false;
                return;
            }

            // write date and time the log file was opened
            string s = "\r\nLog opened: " + DateTime.Now.ToLongTimeString() + " " +
                DateTime.Now.ToLongDateString() + "\r\n";
            n = s.Length - 4;
            for(i = 0; i < n; ++i) s += "-";
            logStream.WriteLine("{0}", s);

            // for each line in trace window, write it to log file
            n = tbTraceDataDisplay.Lines.Length;
            foreach(string line in tbTraceDataDisplay.Lines) {
            
                // if it's the last empty line, don't write it to log file
                if ((--n == 0) && (line == String.Empty)) break;
                
                logStream.WriteLine("{0}", line + "\r\n");
            }
            logStream.Flush(); 
        }

        private void closeLogFile()
        {
            // if log file is already closed, do nothing
            if (logStream == null) return;
            
            // if we've been building a line for the log file, write it
            if (nextLogLine != String.Empty) {
                
                logStream.WriteLine("{0}", nextLogLine + "\r\n");
                nextLogLine = String.Empty;
            }
            
            // make sure the underlying log file is updated
            logStream.Flush();
            
            // close log file
            logStream.Close();
            logStream = null;
        }

        private void lblTraceTextBoxStats_Click(object sender, EventArgs e)
        {
        }

        private void lblLastError_Click(object sender, EventArgs e)
        {
        }

        private void btnEraseLogFile_Click(object sender, EventArgs e)
        {
            // if log file is open, close it so we can delete file
            if (logStream != null) closeLogFile();
            
            // delete log file
            try {
                File.Delete(traceDirTextBox.Text);
            } catch (Exception ex) {    
                MessageBox.Show("Failed to delete log file: " + ex.Message);
            }
            
            // if log file is enabled, re-open it
            if (cbLogTraceDataToFile.Checked) openLogFile();
        }

        private void cmbMaxTraceTextBoxLines_SelectedIndexChanged(object sender, EventArgs e)
        {
            // update max number of lines for trace window
            int i = cmbMaxTraceTextBoxLines.SelectedIndex;
            maxLinesInTraceWin = Convert.ToInt32(cmbMaxTraceTextBoxLines.Items[i]);
            
            // remove any excess old lines in trace window
            limitTraceWinLines();
        }

        private void AdkTraceViewForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            closeLogFile();
        }

        private void AdkTraceViewForm_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void label2_Click(object sender, EventArgs e)
        {
        }

        private void AdkTraceViewForm_Activated(object sender, EventArgs e)
        {
            // Make the TextBox cover Up the debug controls I want To keep but
            // do not want an ordinary operator to see

            // Now we have to also update the HEIGHT to increase it by the difference
            // between the original top Y of the TextBox and the top Y of the
            // control we are obscuring
            //
            int origHeight = tbTraceDataDisplay.Height;
            int newHeight = origHeight + (tbTraceDataDisplay.Location.Y - labDirOfExe.Location.Y);

            tbTraceDataDisplay.SetBounds(0, labDirOfExe.Location.Y, 0, 0, BoundsSpecified.Y);
            tbTraceDataDisplay.SetBounds(0, 0, 0, newHeight, BoundsSpecified.Height);

            // make the debug controls invisible 
            labDirOfExe.Visible = false;
            traceDirTextBox.Visible = false;
            btnBrowseForTraceLogFile.Visible = false;
            btnReportTraceTextboxStats.Visible = false;
            lblTraceTextBoxStats.Visible = false;
            lblLinesNowInTraceTextbox.Visible = false;
            lblLastError.Visible = false;
            lblLinesNowInTraceTextbox.Visible = false;
        }

        private void chbShowDeltaT_CheckedChanged(object sender, EventArgs e)
        {
        }
    }
}
