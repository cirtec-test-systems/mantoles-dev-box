//
// This file contains the public class AnyModForm, which provides the main GUI
// for the ZL7010X ADK base station module (BSM) and implant module (IM). When
// this form is instantiated, it configures itself for the module type it is
// created for. When this form is viewed using the form designer in Visual
// Studio, the display components for both modules are shown. If different
// components occupy the same screen space, the components are stacked on top
// of each other. When the form is instantiated, the components are enabled/
// disabled and made visible/invisible as appropriate for the module the form
// is for. Note that for the implant module, some components are also moved at
// runtime. The form uses the BsmApi and ImApi classes to communicate with the
// base station an implant modules (see the BsmApi and ImApi classes for more
// information).
// 
// Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2012. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.

using System;
using System.IO; // for file reads
using System.Data;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.Globalization; // for NumberStyles
using Zarlink.Adp.General;
using Zarlink.Adk.Api;
using Zarlink.Adp.Forms;

namespace Zarlink.Adk.Forms
{
    public partial class AnyModForm : Form
    {
        // The GUI state maintained by the status polling. This always
        // represents the state of the GUI that resulted from the latest
        // status poll. The status polling uses this to determine what needs
        // to be done each time the GUI state changes as a result of the status
        // polling (gray/ungray display components, etc.), which is handled
        // by updateGuiStateToMatchPolledMicsState().
        //
        // Note that if the status polling is disabled, "guiState" isn't
        // updated, so it doesn't reflect the current GUI state. Thus, nothing
        // except the status polling should reference "guiState" to determine
        // the current GUI state.
        // 
        // Note that some of the GUI states apply to both the base station and
        // the implant GUIs, and some apply only to one or the other.
        //
        private GUI_STATE guiState = GUI_STATE.UNKNOWN;
        //
        private enum GUI_STATE : int
        {
            SLEEP,                  // base & implant
            IDLE,                   // base & implant 
            SEARCH_FOR_IMPLANT,     // base only
            START_SESSION,          // base only
            SESSION,                // base & implant
            LISTEN_FOR_EMER,        // base only
            SEND_RESPONSES,         // implant only
            SEND_EMER,              // implant only
            CONTINUOUS_CCA_OR_RSSI, // base (CCA) & implant (RSSI)
            UNRECOGNIZED,           // base & implant
            UNKNOWN                 // base & implant
        }
        
        // Flags used to indicate when various states/activities are active.
        // These are used to determine what needs to be done each time the
        // state changes (gray/ungray display components, etc.) as a result of
        // a user action or the status polling. Note that some of theses apply
        // to both the base station and the implant GUIs, and some apply only
        // to one or the other.
        //
        private bool inStartSession = false;        // base only
        private bool inSession = false;             // base & implant
        private bool inSearchForImplant = false;   // base only
        private bool inListenForEmer = false;       // base only
        private bool inSendEmer = false;            // implant only
        private bool inTx245Test = false;           // base only
        private bool inTx400Test = false;           // base & implant
        private bool inRx400Test = false;           // base & implant
        private bool inTransmitData = false;        // base & implant
        private bool inContinuousCcaOrRssi = false; // base (CCA) & implant (RSSI)
        private bool inDataTest = false;            // base GUI only (always false on implant GUI)
        
        // The base station GUI uses this flag to indicate when it is connected
        // to a local implant via USB.
        //
        private bool connectedToLocalImplant = false;
        
        // Strings displayed in the "Operational State" field. Note that some of
        // these apply to both the base station and the implant, and some apply
        // only to one or the other.
        //
        private const string OP_STATE_SLEEP = "Sleep";                               // base & implant   
        private const string OP_STATE_IDLE = "Idle";                                 // base & implant
        private const string OP_STATE_SEARCH_FOR_IMPLANT = "Search for Implant(s)"; // base only
        private const string OP_STATE_START_SESSION = "Start Session";               // base only
        private const string OP_STATE_SESSION = "In Session";                        // base & implant   
        private const string OP_STATE_LISTEN_FOR_EMER = "Listen for Emergency";      // base only
        private const string OP_STATE_SEND_EMER = "Send Emergency";                  // implant only   
        private const string OP_STATE_SEND_RESPONSES = "Send Responses";             // implant only
        private const string OP_STATE_PERFORM_CCA = "Perform CCA";                   // base only
        private const string OP_STATE_PERFORM_RSSI = "Perform RSSI";                 // implant only
        private const string OP_STATE_UNRECOGNIZED = "Unrecognized State";           // base & implant 
        
        // string displayed for invalid or unknown values
        private const string INVALID_VALUE = "****";
        
        // header displayed for ADC column for external and internal RSSIs
        private const string EXT_ADC = "Ext ADC";
        private const string INT_ADC = "Int ADC";
        
        private struct RX_IMD_TID_ENTRY
        {
            public uint imdTid;
            public uint count;
        }

        // this indicates if the GUI is for the implant (true) or base (false)
        private bool isImForm = false;

        // if true, processing of the next event change is bypassed
        private bool ignoreChangedEvent = false;
        private bool ignoreTransmitChangedEvent = false;
        private bool ignoreRcvChangedEvent = false;

        // mezzanine board model (AIM100, AIM200, AIM300, BSM100, BSM200, or BSM300)
        private string mezzModel = null;

        // set to force status polling to update all status displays
        private bool forceStat = false;

        // set to tell status polling to add "(CCA)" to displayed chan
        private bool addCcaToStatChan = false;

        // Bits in MICS_DT_SPECS.options. For descriptions, see MICS_DT_TX,
        // etc. in "ZL7010XAdk\Sw\Includes\Adk\AnyMezz\MicsGeneral.h".
        //
        private byte bsmDtTx = CommonApi.MICS_DT_TX;
        private byte bsmDtRx;
        private byte bsmDtValidate;
        private byte bsmDtIncrementTx;
        private byte bsmDtIncrementRx;
        private byte imDtTx;
        private byte imDtRx = CommonApi.MICS_DT_RX;
        private byte imDtValidate;
        private byte imDtIncrementTx;
        private byte imDtIncrementRx;
        private byte bsmData;
        private byte imData;

        // used to indicate when an invalid key (character) is entered
        private bool isInvalidKey = false;

        // The rate (interval) of the background task timer used to perform background
        // tasks, and the counters used to determine when the background task timer
        // should perform each of the periodic tasks.
        //
        private const int BACKGROUND_TASK_INTERVAL = 100; // 100 ms
        //
        private int backgroundTaskTimerCount = 0;
        private int txDataIntervalCount = 1;
        private int rxDataPollIntervalCount = 1;
        private int ccaIntervalCount = 10;
        private int imResponsePollIntervalCount = 1;

        private int dgvImInfoRowCount = 0;

        // used to hold total number invalid bytes detected during data test
        private uint totalBsmDataErrCount = 0;
        private uint totalImDataErrCount = 0;

        // Buffer used to hold TX data to write to the ZL7010X for transmission.
        // Note the maximum number of TX bytes that can be specified depends on
        // the text box size. Index checks aren't performed in the code.
        //
        private const int TX_BUF_SIZE = 1500;
        private byte[] txBuf = new byte[TX_BUF_SIZE];
        private int txLen = 0;  // the number of bytes specified in txBuf
        //
        // Buffer used to hold RX data read from the ZL7010X. This is big
        // enough to hold the maximum number of bytes contained in the
        // ZL7010X's RX buffer when it's full. That way, each time we poll
        // the ZL7010X for RX data, we'll always read everything contained in
        // the ZL7010X's RX buffer. The ZL7010X's RX buffer can contain up to
        // 2 maximum-size packets of data (2 Packets * 32 MaxBlocksPerPacket *
        // 15 MaxBytesPerBlock).
        //
        private const int RX_BUF_SIZE = 2 * 32 * 15;
        private byte[] rxBuf = new byte[RX_BUF_SIZE];
        //
        // The ZL7010X data block size reported by the status polling (see
        // tmrStatPoll_Tick()). Note this should only be referenced when the
        // status polling is active. For example, it's safe to reference this
        // when "inSession" is true because only the status polling sets
        // "inSession", ensuring the status polling is active.
        //
        private int blockSizeFromStat = 0;

        // List of IMD TIDs received from implants that either responded to a
        // search or sent an emergency message (limited to 100 implants).
        //
        private const int MAX_NUM_ENTRIES_IN_RX_IMD_TID_LIST = 100;
        private RX_IMD_TID_ENTRY[] rxImdTidList = new RX_IMD_TID_ENTRY[MAX_NUM_ENTRIES_IN_RX_IMD_TID_LIST];
        private int numEntriesInRxImdTidList = 0;

        // The following objects are used to interface to the API (DLL) for the
        // ADK module. The adkMod object is used to access methods and properties
        // common to all module types, whereas the bsmMod and imMod objects are
        // used to access methods and properties specific to the module type
        // (base station or implant). 
        //
        public CommonApi adkMod = null;
        public BsmApi bsmMod = null;
        public ImApi imMod = null;

        // to capture a reference to the form that launched us
        private AdkForm adkForm;

        // trace viewer
        public AdkTraceViewForm adkTraceViewForm = null;

        // power monitor
        public AdpPowMonForm adpPowMonForm = null;

        // register form objects
        private RegistersForm bsmRegsForm = null;
        private RegistersForm imRegsForm = null;

        // link quality form objects (only valid on base station)
        private LinkQualityForm linkQualForm = null;

        private DataSet imInfoDataSet;
        private StreamWriter sw;
        private bool streamingData = false;

        // store previous values of VSUP in case an invalid number is entered
        private decimal prevVsup1;
        private decimal prevVsup2;

        // flags for high level tuning algorithm
        private const byte TX_TUNE_CAP = (1 << 0);
        private const byte MATCH1_TUNE_CAP = (1 << 1);
        private const byte MATCH2_TUNE_CAP = (1 << 2);

        private const byte TUNE_CH0 = (1 << 0);
        private const byte TUNE_CH1 = (1 << 1);
        private const byte TUNE_CH2 = (1 << 2);
        private const byte TUNE_CH3 = (1 << 3);
        private const byte TUNE_CH4 = (1 << 4);
        private const byte TUNE_CH5 = (1 << 5);
        private const byte TUNE_CH6 = (1 << 6);
        private const byte TUNE_CH7 = (1 << 7);
        private const byte TUNE_CH8 = (1 << 0);
        private const byte TUNE_CH9 = (1 << 1);

        // anttunesel1
        private byte highLevelTuneTxCap = TX_TUNE_CAP;
        private byte highLevelTuneMatch1Cap = MATCH1_TUNE_CAP;
        private byte highLevelTuneMatch2Cap = MATCH2_TUNE_CAP;

        // anttunesel2
        private byte highLevelTuneCh0;
        private byte highLevelTuneCh1;
        private byte highLevelTuneCh2;
        private byte highLevelTuneCh3;
        private byte highLevelTuneCh4;
        private byte highLevelTuneCh5;
        private byte highLevelTuneCh6;
        private byte highLevelTuneCh7;

        // anttunesel3
        private byte highLevelTuneCh8;
        private byte highLevelTuneCh9;
        private byte highLevelExtAlg;

        private byte anttunesel1 = 0;
        private byte anttunesel2 = 0;
        private byte anttunesel3 = 0;

        // The list of 2.45 GHz TX power selections (dBm) for the CC2500 on the
        // BSM300 (used to populate the dropdown list), and tables containing
        // the CC2500 PA values to use for each power selection. Which table
        // is used depends on whether the currently selected 2.45 GHz frequency
        // is in the low band (2405 - 2430 MHz), middle band (2435 - 2460 MHz),
        // or high band (2465 - 2485 MHz).
        //
        String[] cc2500PowList = new [] { "-65", "0", "1", "2", "3", "4", "5",
            "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17",
            "18", "19", "20", "21", "22", "22+" };
        private readonly byte[] cc2500PaValsForLowFreqBand = new byte[] {
            0x00, 0x63, 0x6C, 0x91, 0xC3, 0xB2, 0xA3, 0x85, 0x87, 0x65, 0x67,
            0x96, 0x97, 0xCF, 0x79, 0x7E, 0x9B, 0xE6, 0xE7, 0xDE, 0xBE, 0xAF,
            0xF9, 0xEB, 0xFF};
        private readonly byte[] cc2500PaValsForMidFreqBand = new byte[] {
            0x00, 0x8C, 0x47, 0xC2, 0xA1, 0xB3, 0xD2, 0x87, 0xE3, 0x5A, 0xC5,
            0x97, 0xCA, 0x69, 0x7D, 0x9E, 0x7F, 0xE6, 0xD9, 0xAD, 0xAB, 0xBF,
            0xF9, 0xFE, 0xFF};
        private readonly byte[] cc2500PaValsForHighFreqBand = new byte[] {
            0x00, 0x47, 0xC1, 0x93, 0xB2, 0x56, 0x86, 0xE2, 0x89, 0x5B, 0xC7,
            0xC9, 0xCF, 0x6E, 0x6B, 0x9F, 0xE5, 0xE7, 0xB9, 0xBE, 0xAF, 0xEA,
            0xEB, 0xFB, 0xFF};
            
        // The list of 2.45 GHz TX power selections (dBm) for the CC2550 on the
        // BSM200 and BSM100 (used to populate the dropdown list), and the table
        // containing the CC2550 PA value to use for each power selection.
        //
        String[] cc2550PowList = new [] { "-29", "-4", "-2", "0", "2", "4", "6",
            "8", "10", "12", "14", "16", "18", "20", "22", "24", "26", "27" };
        private readonly byte[] cc2550PaVals = new byte[] {
            0x00, 0x44, 0x41, 0x43, 0x84, 0x82, 0x47, 0xC8, 0x85, 0x59,
            0xC6, 0x97, 0xD6, 0x7F, 0xA9, 0xBF, 0xEE, 0xFF};

        // NVM display
        private NvmForm frmNvm = null;

        // copies of the contents of the MICS factory NVM on the base station and implant
        private BsmApi.BsmMicsFactNvm bsmMicsFactNvm = new BsmApi.BsmMicsFactNvm();
        private ImApi.ImMicsFactNvm imMicsFactNvm = new ImApi.ImMicsFactNvm();

        // state variables for btnCalReq_Click()
        private enum CAL_REQ_STATE : int
        {
            TX_400_OFF = 0,
            WAIT_TX_400_ON,
            TX_400_ON,
            TX_400_BACK_OFF
        }
        private CAL_REQ_STATE calReqOn = CAL_REQ_STATE.TX_400_OFF;
        private bool calReqRemote = false;
        
        // enum for channel passed to performRssi()
        private enum RSSI_CHAN : uint
        {
            CHAN_0 = 0,
            CHAN_1 = 1,
            CHAN_2 = 2,
            CHAN_3 = 3,
            CHAN_4 = 4,
            CHAN_5 = 5,
            CHAN_6 = 6,
            CHAN_7 = 7,
            CHAN_8 = 8,
            CHAN_9 = 9,
            ALL_CHANS = 10
        }
        
        // Arrays used to access display components for each channel (indexed
        // by channel number). These allow each channel's display components to
        // be accessed by indexing into these arrays using the channel number.
        //
        private TextBox[] txtRssi;
        private TextBox[] txtRssiDbm;

        // fonts used to display RSSI values
        Font boldFont = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold,
            GraphicsUnit.Point, ((byte)(0)));
        Font regularFont = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Regular,
            GraphicsUnit.Point, ((byte)(0)));
        
        public AnyModForm()
        {
            InitializeComponent();
            isImForm = false;
        }

        public AnyModForm(bool isImForm, AdkForm adkForm)
        {
            InitializeComponent();
            this.isImForm = isImForm;

            // save a reference to the parent form that launched this form
            this.adkForm = adkForm;
        }

        public void AnyModForm_Load(object sender, EventArgs e)
        {
            string err = "";
            int adkModInitStat;

            tmrBackgroundTasks.Interval = BACKGROUND_TASK_INTERVAL;
            
            // comment this out to use the factory test page
            bsmTabs.TabPages.Remove(tabFactoryUse);
            
            // if implant GUI, do init for implant GUI
            if (isImForm) {
            
                grpImStat.Text = "IMD Status + Control (AIM)";
                grpImInfo.Enabled = false;
                grpImInfo.Visible = false;
                
                // change "CCA & Cal" tab to "RSSI & Cal" and related
                this.tabCcaOrRssiAndCal.Text = "RSSI & Cal";
                grpCcaOrRssi.Text = "RSSI & CAL";
                grpCcaOrRssi.Text = "RSSI Measurements";
                btnPerformCcaOrRssi.Text = "Perform RSSI";
                lblExtOrIntAdc.Text = INT_ADC;
                //
                // These aren't pertinent to the implant because the only RSSI it
                // supports is the internal RSSI which always averages over 10 ms.
                //
                chbUseIntRssiOnBsm.Visible = false;
                chbUseAveRssiForCca.Visible = false;
                
                this.bsmTabs.Controls.Remove(this.tabRemote);
                btnStartSession.Visible = false;
                btnStartListen.Visible = false;
                btnStartImSearch.Visible = false;
                label140.Visible = false;
                chbAutoCca.Visible = false;
                chbAnyImSearch.Visible = false;
                chbAutoListen.Visible = false;
                grpWakeupMode.Visible = true;
                grpRxImdTidList.Visible = false;
                grpBsmStat.Visible = false;

                label60.Visible = false;
                cmbTestTx245Chan.Visible = false;
                btnTestTx245Enab.Visible = false;
                cmbTestTx245Pow.Visible = false;
                lblCcPa.Visible = false;
                txtCcPa.Visible = false;
                btnCcPaRead.Visible = false;
                btnCcPaWrite.Visible = false;

                this.tabRemote.Visible = false;
                btnWakeupIm.Visible = true;
                btnSendEmer.Visible = true;

                // hide 2.45 GHz wakeup section in link setup (not applicable to implant GUI)
                label22.Visible = false;
                label20.Visible = false;
                label25.Visible = false;
                label140.Visible = false;
                cmbWake245Country.Visible = false;
                cmbWake245UserData.Visible = false;
                cmbImplantType.Visible = false;
                grpImCommandInterface.Visible = false;

                // Display the correct Implant Status + Control group
                grpImStatOnBsm.Visible = false;
                grpImStat.Visible = true;
                grpImStat.Location = new Point(464, 16);

                // remove Data Test tab (not supported on implant form)
                bsmTabs.TabPages.Remove(tabDataTest);

                // hide BSM sleep button/checkbox
                btnWakeupBsm.Visible = false;
                chbResetMicsOnWakeup.Visible = false;
                
                grpReceiveData.Text = "Receive Data (Downlink)";
                grpTransmitData.Text = "Transmit Data (Uplink)";

                // init the API interface used to communicate with the module
                adkMod = imMod = new ImApi();
                if ((adkModInitStat = adkMod.init(ref err)) < 0) {
                
                    Util.displayError(txtSysMsg, "Failed to initialize interface to implant: " + err);
                        
                    // set default "mezzModel" (for demo mode)
                    this.mezzModel = "AIM300";
                    
                } else { // adkMod.init() was successful
                
                    // init "mezzModel" for reference during remaining init
                    string mezzVer = adkMod.getMezzVer();
                    if (mezzVer.Contains("AIM300")) {
                        mezzModel = "AIM300";
                    } else if (mezzVer.Contains("AIM200")) {
                        mezzModel = "AIM200";
                    } else if (mezzVer.Contains("AIM100")) {
                        mezzModel = "AIM100";
                    } else {
                        mezzModel = "Unrecognized Implant Model";
                    }
                }
                
                // init title, etc. for ZL70101, ZL70102, or ZL70103
                if (adkMod.MicsRev > CommonApi.MICS_REV.ZL70102) { // ZL70103
                
                    this.Text = "ZL70103 Implant (" + mezzModel + ")";
                    btnImRegs.Text = "ZL70103 Registers";
                    grpHighLevelTuning.Visible = true;

                } else if (adkMod.MicsRev > CommonApi.MICS_REV.ZL70101) { // ZL70102 

                    this.Text = "ZL70102 Implant (" + mezzModel + ")";
                    btnImRegs.Text = "ZL70102 Registers";
                    grpHighLevelTuning.Visible = true;
                    
                } else { // ZL70101
                
                    this.Text = "ZL70101 Implant (" + mezzModel + ")";
                    btnImRegs.Text = "ZL70101 Registers";
                    grpHighLevelTuning.Visible = false;
                }
                
                // enable display components in implant status section
                txtImVsup1.Enabled = true;
                txtImVsup2.Enabled = true;
                btnImPowMon.Enabled = true;
                
            } else { // do init for base station GUI
            
                // load implant information from XML file
                loadImInfo();
                
                chbEnabHkWrite.Visible = false;
                
                // Disable GUI items that can be used to change the ZL7010X
                // company ID on the base station. This is done so the ADK as
                // shipped can't be used to wake implants for other companys.
                //
                txtCompanyId1.Enabled = false;
                txtCompanyId2.Enabled = false;
                
                // disable implant cals
                lblImVsup1.Enabled = false;
                lblImVsup2.Enabled = false;
                udSetVsup1.Enabled = false;
                udSetVsup2.Enabled = false;
                chbVsupTrack.Enabled = false;
                txtImVsup1.Enabled = false;
                txtImVsup2.Enabled = false;
                lblVsupMeasured.Enabled = false;
                lblVsupSetting.Enabled = false;
                btnImLinkQual.Visible = false;
                btnImPowMon.Visible = false;

                // Display the correct Implant Status + Control group
                grpImStat.Visible = false;
                grpImStatOnBsm.Visible = true;

                // select no remote connection by default
                cmbImConnection.SelectedIndex = 0;
                
                grpHighLevelTuning.Visible = false;
                lblCalReqRem.Visible = false;
                btnCalReqRem.Visible = false;
                
                // Hide "Ext Strobe" checkbox (option for 2.45 GHz wakeup)
                // because this option doesn't apply to the base station. Also
                // reduce the size of the "Wake-up Mode" group box accordingly
                // (so it won't overlap what's below in the base station GUI).
                //
                chbExtStrobeFor245Sniff.Visible = false;
                grpWakeupMode.Height = 40;
                
                // disable implant registers button (enabled only during session)
                btnImRegsOnBsm.Enabled = false;

                // init the API interface used to communicate with the module
                adkMod = bsmMod = new BsmApi();
                if ((adkModInitStat = adkMod.init(ref err)) < 0) {
                
                    Util.displayError(txtSysMsg, "Failed to initialize interface to base station: " + err);
                        
                    // set default "mezzModel" (for demo mode)
                    this.mezzModel = "BSM300";
                    
                } else { // adkMod.init() was successful
                
                    // init "mezzModel" for reference during remaining init
                    string mezzVer = adkMod.getMezzVer();
                    if (mezzVer.Contains("BSM300")) {
                        mezzModel = "BSM300";
                    } else if (mezzVer.Contains("BSM200")) {
                        mezzModel = "BSM200";
                    } else if (mezzVer.Contains("BSM100")) {
                        mezzModel = "BSM100";
                    } else {
                        mezzModel = "Unrecognized Base Station Model";
                    }
                }
                
                // init title, etc. for ZL70101, ZL70102, or ZL70103
                if (adkMod.MicsRev > CommonApi.MICS_REV.ZL70102) { // ZL70103
                
                    this.Text = "ZL70103 Base Station (" + mezzModel + ")";
                    btnBsmRegs.Text = "ZL70103 Registers";
                        
                    // hide calibrations box (no cals for ZL70102 on base)
                    grpCal.Visible = false;

                } else if (adkMod.MicsRev > CommonApi.MICS_REV.ZL70101) { // ZL70102

                    this.Text = "ZL70102 Base Station (" + mezzModel + ")";
                    btnBsmRegs.Text = "ZL70102 Registers";

                    // hide calibrations box (no cals for ZL70102 on base)
                    grpCal.Visible = false;
                    
                } else { // ZL70101
                
                    this.Text = "ZL70101 Base Station (" + mezzModel + ")";
                    btnBsmRegs.Text = "ZL70101 Registers";
                }
                
                // init dropdown list used to select 2.45 GHz TX power
                if (mezzModel == "BSM300") {
                    cmbTestTx245Pow.Items.AddRange(cc2500PowList);
                } else { // BSM200 or BSM100
                    cmbTestTx245Pow.Items.AddRange(cc2550PowList);
                }
                
                // disable display components in implant status section
                txtImVsup1.Enabled = false;
                txtImVsup2.Enabled = false;
                btnImPowMon.Enabled = false;
            }
            
            grpTransmitData.Enabled = false;
            grpReceiveData.Enabled = false;
            cmbTestRx400Chan.SelectedIndex = 0;
            cmbTestTx400Chan.SelectedIndex = 0;
            cmbBerChan.SelectedIndex = 0;
            tabRemote.Enabled = false;
            cmbReceiveDataPollRate.SelectedIndex = 4;
            cmbTransmitDataRate.SelectedIndex = 4;
            btnImTx400.Enabled = false;
            lblImTx400.Enabled = false;
            btnImRx400.Enabled = false;
            lblImRx400.Enabled = false;
            lblCalReq.Visible = false;
            btnCalReq.Visible = false;
            setStatusInvalid(true);
            
            btnBsmTx400.BackColor = Color.White;
            btnImTx400.BackColor = Color.White;
            btnBsmRx400.BackColor = Color.White;
            btnImRx400.BackColor = Color.White;
            btnBsmTx245.BackColor = Color.White;

            // remove items from implant connection combo box (not yet implemented)
            cmbImConnection.Items.Remove("ZL7010X HK User Data (RF)");
            cmbImConnection.Items.Remove("ZL7010X HK User Status (RF)");
            cmbImConnection.Items.Remove("ZL7010X Data Blocks (RF)");
            
            // Init the arrays used to access the RSSI components for each
            // channel (indexed by channel number). 
            //
            txtRssi = new TextBox[] {txtRssi0, txtRssi1, txtRssi2, txtRssi3,
                txtRssi4, txtRssi5, txtRssi6, txtRssi7, txtRssi8, txtRssi9};
            txtRssiDbm = new TextBox[] {txtRssiDbm0, txtRssiDbm1, txtRssiDbm2,
                txtRssiDbm3, txtRssiDbm4, txtRssiDbm5, txtRssiDbm6, txtRssiDbm7,
                txtRssiDbm8, txtRssiDbm9};
            
            // Init the link setup tab. Note that this is called after
            // adkMod.init() so it references the adkMod.MicsRev value
            // returned by the board. Also, it must be called before setting
            // cmbNormTxMod.SelectedIndex to adkMod.CfgNormTxMod
            // and cmbNormRxMod.SelectedIndex to adkMod.CfgNormRxMod
            // (the settings obtained from the board). Otherwise, if either
            // adkMod.CfgNormTxMod or adkMod.CfgNormRxMod contains a Barker
            // mode, the GUI throws an exception.
            //
            initLinkSetupTab();
            
            // If adkMod.init() was successful, do remaining init that should
            // only be done if adkMod.init() was successful.
            //
            if (adkModInitStat == 0) {
            
                // Adjust the displayed config values, etc. to match the config
                // values read from the module.
                //
                if (isImForm) {
                
                    chbEnabHkWrite.Checked = imMod.CfgEnabHkWrite;
                    udSetVsup1.Value = (decimal)(adkMod.CfgVsup1Mv) / 1000;
                    udSetVsup2.Value = (decimal)(adkMod.CfgVsup2Mv) / 1000;
                    prevVsup1 = udSetVsup1.Value;
                    prevVsup2 = udSetVsup2.Value;
                    
                    chbExtStrobeFor245Sniff.Checked = imMod.CfgExtStrobeFor245GHzSniff;
                    
                } else { // base station GUI
                
                    // set implant type selection to match current module config
                    switch(bsmMod.CfgMicsRevOnImplant) {
                    case CommonApi.MICS_REV.ZL70100:
                        cmbImplantType.SelectedIndex = 0; break;
                    case CommonApi.MICS_REV.ZL70101:
                        cmbImplantType.SelectedIndex = 1; break;
                    case CommonApi.MICS_REV.ZL70102:
                        cmbImplantType.SelectedIndex = 2; break;
                    default: // ZL70103
                        cmbImplantType.SelectedIndex = 3; break;
                    }
                    
                    chbAutoCca.Checked = bsmMod.CfgAutoCcaEnab;
                    chbAutoListen.Checked = bsmMod.CfgAutoListenEnab;
                    chbResetMicsOnWakeup.Checked = bsmMod.CfgResetMicsOnWakeup;
                    
                    // Init the 2.45 GHz TX power and frequency. Note that
                    // the GUI does not initialize these to match the current
                    // settings on the base station because doing so would be
                    // more complex that it's worth. Instead, the GUI just uses
                    // default settings and writes them to the base station to
                    // ensure the settings on the base station match the
                    // displayed settings.
                    //
                    if (mezzModel == "BSM300") {
                        cmbTestTx245Pow.SelectedIndex = 21; // ~20 dBm
                    } else { // BSM200 or BSM100
                        cmbTestTx245Pow.SelectedIndex = 13; // ~20 dBm
                    }
                    cmbTestTx245Chan.SelectedIndex = 9; // channel 9 = 2450 MHz
                }
                rad400MHzWakeup.Checked = adkMod.Cfg400MHzWakeup;
                txtCompanyId1.Text = ((adkMod.CfgCompanyId >> 4) & 0x0f).ToString("X1");
                txtCompanyId2.Text = (adkMod.CfgCompanyId & 0x0f).ToString("X1");
                txtImdTid1.Text = ((adkMod.CfgImdTid >> 20) & 0x0f).ToString("X1");
                txtImdTid2.Text = ((adkMod.CfgImdTid >> 16) & 0x0f).ToString("X1");
                txtImdTid3.Text = ((adkMod.CfgImdTid >> 12) & 0x0f).ToString("X1");
                txtImdTid4.Text = ((adkMod.CfgImdTid >> 8) & 0x0f).ToString("X1");
                txtImdTid5.Text = ((adkMod.CfgImdTid >> 4) & 0x0f).ToString("X1");
                txtImdTid6.Text = ((adkMod.CfgImdTid) & 0x0f).ToString("X1");
                cmbNormChan.SelectedIndex = adkMod.CfgNormChan;
                //
                // Set the normal TX & RX modulations, and then if it's the base
                // station GUI, set the user data for 2.45 GHz wakeup. Note that
                // the modulations are set first so cmdWake245UserData.Enabled
                // will be false if a Barker mode is selected.
                //
                cmbNormTxMod.SelectedIndex = (int)adkMod.CfgNormTxMod;
                cmbNormRxMod.SelectedIndex = (int)adkMod.CfgNormRxMod;
                if (!isImForm) {
                    if (cmbWake245UserData.Enabled) {
                        cmbWake245UserData.SelectedIndex = bsmMod.CfgWake245UserData;
                    } else {
                        cmbWake245UserData.SelectedIndex = 0;
                    }
                }
                cmbNormBytesPerBlock.SelectedIndex = adkMod.CfgNormBlockSize - 2;  // range 2 - 15
                cmbNormMaxBlocksPerPack.SelectedIndex = adkMod.CfgNormMaxBlocksPerTxPack - 1;    // range 1 - 31
                cmbEmerChan.SelectedIndex = adkMod.CfgEmerChan;
                cmbEmerRxMod.SelectedIndex = (int)adkMod.CfgEmerRxMod;
                cmbEmerTxMod.SelectedIndex = (int)adkMod.CfgEmerTxMod;
                cmbEmerBytesPerBlock.SelectedIndex = adkMod.CfgEmerBlockSize - 2;  // range 2 - 15
                cmbEmerMaxBlocksPerPack.SelectedIndex = adkMod.CfgEmerMaxBlocksPerTxPack - 1;  // range 1 - 31
                
                // read contents of MICS factory NVM from module
                if (isImForm) {

                    if (imMod.getMicsFactNvm(imMicsFactNvm, ref err) < 0) {
                        Util.displayError(txtSysMsg, "Failed to get the MICS factory NVM from the implant: " + err);
                    }

                } else {

                    if (bsmMod.getMicsFactNvm(bsmMicsFactNvm, ref err) < 0) {
                        Util.displayError(txtSysMsg, "Failed to get the MICS factory NVM from the base station: " + err);
                    }
                }

                // Now that the GUI has finished initializing its display
                // components to match the configuration read from the module,
                // set adkMod.CfgWriteEachChangeToMod so from now on, each time
                // a configuration variable in the module's API is changed, the
                // API will write the new value to the module. This is initially
                // false so as the GUI initializes its display components to
                // match the current configuration values read from the module,
                // and the event handlers for the display components in turn
                // write the same values back to the configuration variables in
                // the API, the API does not write the values to the module.
                // Otherwise, each event handler invoked during initialization
                // would write the configuration values back to the module for
                // no reason, which would be inefficient, and would abort any
                // operation on the module.
                //
                adkMod.CfgWriteEachChangeToMod = true;
                //
                // Enable the timers for the periodic tasks. Note that this
                // should always be the last thing done by the initialization
                // to ensure that everything is initialized before the periodic
                // tasks are started. This also sets "forceStat" to force the
                // first status poll to update all status displays.
                //
                forceStat = true;
                this.tmrStatPoll.Enabled = true;
                this.tmrBackgroundTasks.Enabled = true;
                //
                // Note: Don't add anything here or later in this method (add
                // it earlier instead). For an explanation, see the comment for
                // "tmrStatPoll.Enabled = true" above.
            }
            // Note: Don't add anything here or later in this method (add
            // it earlier instead). For an explanation, see the comment for
            // "tmrStatPoll.Enabled = true" above.
        }
        
        // Sets the fields displayed in the link status to indicate all values
        // are unknown or invalid. Note that the "Operational State" field is
        // set only if "includeOpState" is true.
        //
        private void setStatusInvalid(bool includeOpState)
        {
            txtStatCompanyId.Text = INVALID_VALUE;
            txtStatImdTid.Text = INVALID_VALUE;
            txtStatTxMod.Text = INVALID_VALUE;
            txtStatRxMod.Text = INVALID_VALUE;
            txtStatChan.Text = INVALID_VALUE;
            txtStatBytesPerBlock.Text = INVALID_VALUE;
            txtStatMaxBlocksPerPack.Text = INVALID_VALUE;
            if (includeOpState) txtStatOpState.Text = INVALID_VALUE;
        }

        private void initLinkSetupTab()
        {
            // Add the TX modulation options for normal operation. Note
            // these must be added in the specified order so they match the
            // corresponding indexes in the cmbNormTxMod dropdown list.
            //
            cmbNormTxMod.Items.Add(CommonApi.MOD_2FSK_FB_STR);
            cmbNormTxMod.Items.Add(CommonApi.MOD_2FSK_STR);
            cmbNormTxMod.Items.Add(CommonApi.MOD_4FSK_STR);
            if (adkMod.MicsRev > CommonApi.MICS_REV.ZL70102) { // ZL70103
            
                cmbNormTxMod.Items.Add(CommonApi.MOD_BARKER_5_STR);
                
                // If base station, add option for Barker 11 TX. Note Barker
                // 11 TX is not allowed on the implant because Barker 11
                // won't work for the uplink (implant TX to base station RX).
                // 
                if (!isImForm) {
                    cmbNormTxMod.Items.Add(CommonApi.MOD_BARKER_11_STR);
                }
            }
            
            // Add the RX modulation options for normal operation. Note
            // these must be added in the specified order so they match the
            // corresponding indexes in the cmbNormTxMod dropdown list.
            //
            cmbNormRxMod.Items.Add(CommonApi.MOD_2FSK_FB_STR);
            cmbNormRxMod.Items.Add(CommonApi.MOD_2FSK_STR);
            cmbNormRxMod.Items.Add(CommonApi.MOD_4FSK_STR);
            if (adkMod.MicsRev > CommonApi.MICS_REV.ZL70102) {  // ZL70103
            
                cmbNormRxMod.Items.Add(CommonApi.MOD_BARKER_5_STR);
                
                // If implant, add option for Barker 11 RX. Note Barker 11 RX
                // is not allowed on the base station because Barker 11 won't
                // work for the uplink (implant TX to base station RX).
                // 
                if (isImForm) {
                    cmbNormRxMod.Items.Add(CommonApi.MOD_BARKER_11_STR);
                }
            }
            
            // Add the TX modulation options for emergency operation. Note
            // these must be added in the specified order so they match the
            // corresponding indexes in the cmbNormTxMod dropdown list.
            //
            cmbEmerTxMod.Items.Add(CommonApi.MOD_2FSK_FB_STR);
            cmbEmerTxMod.Items.Add(CommonApi.MOD_2FSK_STR);
            cmbEmerTxMod.Items.Add(CommonApi.MOD_4FSK_STR);
            if (adkMod.MicsRev > CommonApi.MICS_REV.ZL70102) {  // ZL70103
            
                cmbEmerTxMod.Items.Add(CommonApi.MOD_BARKER_5_STR);
                
                // If base station, add option for Barker 11 TX. Note Barker
                // 11 TX is not allowed on the implant because Barker 11
                // won't work for the uplink (implant TX to base station RX).
                // 
                if (!isImForm) {
                    cmbEmerTxMod.Items.Add(CommonApi.MOD_BARKER_11_STR);
                }
            }
            
            // Add the RX modulation options for emergency operation. Note
            // these must be added in the specified order so they match the
            // corresponding indexes in the cmbNormTxMod dropdown list.
            //
            cmbEmerRxMod.Items.Add(CommonApi.MOD_2FSK_FB_STR);
            cmbEmerRxMod.Items.Add(CommonApi.MOD_2FSK_STR);
            cmbEmerRxMod.Items.Add(CommonApi.MOD_4FSK_STR);
            if (adkMod.MicsRev > CommonApi.MICS_REV.ZL70102) {  // ZL70103
            
                cmbEmerRxMod.Items.Add(CommonApi.MOD_BARKER_5_STR);
                
                // If implant, add option for Barker 11 RX. Note Barker 11 RX
                // is not allowed on the base station because Barker 11 won't
                // work for the uplink (implant TX to base station RX).
                // 
                if (isImForm) {
                    cmbEmerRxMod.Items.Add(CommonApi.MOD_BARKER_11_STR);
                }
            }
            
            cmbWake245Country.SelectedIndex = 0;  // USA
        }

        // This enables/disables the display components on the test tab.
        //
        private void enabTestTabComponents(bool enab)
        {
            if (enab) {
            
                cmbTestTx400Chan.Enabled = true;
                btnTestTx400Enab.Enabled = true;
                btnTestTx400ReadPow.Enabled = true;
                btnTestTx400WritePow.Enabled = true;

                cmbTestRx400Chan.Enabled = true;
                btnTestRx400Enab.Enabled = true;
                btnTestRx400ReadLna.Enabled = true;
                btnTestRx400WriteLna.Enabled = true;
                txtTestRx400Lna.Enabled = true;
                label53.Enabled = true;
                label59.Enabled = true;
                label79.Enabled = true;
                label89.Enabled = true;

                cmbTestTx245Chan.Enabled = true;
                cmbTestTx245Pow.Enabled = true;
                btnTestTx245Enab.Enabled = true;
                txtTestTx400Pow.Enabled = true;
                label60.Enabled = true;
                label58.Enabled = true;
                label56.Enabled = true;
                label57.Enabled = true;
                label61.Enabled = true;
                
                lblCcPa.Enabled = true;
                txtCcPa.Enabled = true;
                btnCcPaRead.Enabled = true;
                btnCcPaWrite.Enabled = true;

            } else { // disable
            
                cmbTestTx400Chan.Enabled = false;
                btnTestTx400Enab.Enabled = false;
                btnTestTx400ReadPow.Enabled = false;
                btnTestTx400WritePow.Enabled = false;

                cmbTestRx400Chan.Enabled = false;
                btnTestRx400Enab.Enabled = false;
                btnTestRx400ReadLna.Enabled = false;
                btnTestRx400WriteLna.Enabled = false;
                txtTestRx400Lna.Enabled = false;
                label53.Enabled = false;
                label59.Enabled = false;
                label79.Enabled = false;
                label89.Enabled = false;

                cmbTestTx245Chan.Enabled = false;
                cmbTestTx245Pow.Enabled = false;
                btnTestTx245Enab.Enabled = false;
                txtTestTx400Pow.Enabled = false;
                label60.Enabled = false;
                label58.Enabled = false;
                label56.Enabled = false;
                label57.Enabled = false;
                label61.Enabled = false;
                
                lblCcPa.Enabled = false;
                txtCcPa.Enabled = false;
                btnCcPaRead.Enabled = false;
                btnCcPaWrite.Enabled = false;
            }
        }

        // This changes the accessibility of form controls for the sleep state.
        //
        private void stateChangeSleep()
        {
            // components that are only on the base station GUI
            btnWakeupBsm.Enabled = true;
            btnStartSession.Enabled = false;
            btnStartImSearch.Enabled = false;
            btnStartListen.Enabled = false;
            chbResetMicsOnWakeup.Enabled = false;
            chbAutoCca.Enabled = false;
            chbAnyImSearch.Enabled = false;
            chbAutoListen.Enabled = false;
            btnBsmRegs.Enabled = false;
            btnBsmLinkQual.Enabled = false;
            btnBsmTx400.Enabled = false;
            lblBsmTx400.Enabled = false;
            btnBsmRx400.Enabled = false;
            lblBsmRx400.Enabled = false;
            btnBsmTx245.Enabled = false;
            lblBsmTx245.Enabled = false;
            grpDataTest.Enabled = false; 
            grpRxImdTidList.Enabled = false;
            btnImRegsOnBsm.Enabled = false;
            if (bsmRegsForm != null) bsmRegsForm.regFormEnable(false);
            
            // components that are only on the implant GUI
            btnWakeupIm.Enabled = true;
            btnSendEmer.Enabled = true;
            chbEnabHkWrite.Enabled = false;
            btnImRegs.Enabled = false;
            btnImLinkQual.Enabled = false;
            btnImTx400.Enabled = false;
            lblImTx400.Enabled = false;
            btnImRx400.Enabled = false;
            lblImRx400.Enabled = false;
            
            // components that are on both the base station and implant GUIs
            grpLinkSetup.Enabled = false;
            grpCcaOrRssi.Enabled = false;
            grpCal.Enabled = false;
            grpTransmitData.Enabled = false;
            grpReceiveData.Enabled = false;
            enabTestTabComponents(false);
            grpWakeupMode.Enabled = false;
            grpBerSetup.Enabled = false;
            if (imRegsForm != null) imRegsForm.regFormEnable(false);
            setStatusInvalid(false);
        }

        // This changes the accessibility of form controls for the idle state.
        //
        private void stateChangeIdle()
        {
            // components that are only on the base station GUI
            btnWakeupBsm.Enabled = true;
            btnStartSession.Enabled = true;
            btnStartImSearch.Enabled = true;
            btnStartListen.Enabled = true;
            chbResetMicsOnWakeup.Enabled = true;
            chbResetMicsOnWakeup.Enabled = true;
            chbAutoCca.Enabled = true;
            chbAutoListen.Enabled = true;
            btnBsmRegs.Enabled = true;
            btnBsmLinkQual.Enabled = true;
            btnBsmTx400.Enabled = true;
            lblBsmTx400.Enabled = true;
            btnBsmRx400.Enabled = true;
            lblBsmRx400.Enabled = true;
            btnBsmTx245.Enabled = true;
            lblBsmTx245.Enabled = true;
            grpDataTest.Enabled = true; 
            tabRemote.Enabled = false;
            if (bsmRegsForm != null) bsmRegsForm.regFormEnable(true);
            
            // components that are only on the implant GUI
            btnWakeupIm.Enabled = true;
            chbEnabHkWrite.Enabled = true;
            btnSendEmer.Enabled = false;

            // components that are on both the base station and implant GUIs
            grpLinkSetup.Enabled = true;
            grpCcaOrRssi.Enabled = true;
            grpCal.Enabled = true;
            grpTransmitData.Enabled = false;
            grpReceiveData.Enabled = false;
            enabTestTabComponents(true);
            grpBerSetup.Enabled = true;
            stateChange400MHzWakeup(rad400MHzWakeup.Checked);
            grpWakeupMode.Enabled = true;
            //
            // Components that are on both the base station and implant GUIs,
            // but are handled differently on each GUI.
            //
            if (isImForm) {
            
                // Enable components on the implant GUI that are available when
                // the implant is awake.
                //
                btnImRegs.Enabled = true;
                btnImTx400.Enabled = true;
                lblImTx400.Enabled = true;
                btnImRx400.Enabled = true;
                lblImRx400.Enabled = true;
                btnImLinkQual.Enabled = true;
                if (imRegsForm != null) imRegsForm.regFormEnable(true);
                
            } else { // base station GUI
            
                // Disable components on the base station GUI that aren't
                // available when the base station is idle (because they're
                // only available when it's in session).
                //
                lblImVsup1.Enabled = false;
                lblImVsup2.Enabled = false;
                btnImRegsOnBsm.Enabled = false;
                btnImTx400.Enabled = false;
                lblImTx400.Enabled = false;
                btnImRx400.Enabled = false;
                lblImRx400.Enabled = false;
                btnImLinkQual.Enabled = false;
                if (imRegsForm != null) imRegsForm.regFormEnable(false);
            }
        }

        private void stateChange400MHzWakeup(bool in400MHzWakeup)
        {
            // if implant GUI
            if (isImForm) {
            
                if (in400MHzWakeup) {
                    chbExtStrobeFor245Sniff.Enabled = false;
                } else {
                    chbExtStrobeFor245Sniff.Enabled = true;
                }
                
            } else { // base station GUI
            
                if (in400MHzWakeup) {
                    
                    // If the implant has a ZL70103 or later, allow the user
                    // to select the "any implant" option as desired.
                    //
                    if (bsmMod.CfgMicsRevOnImplant > CommonApi.MICS_REV.ZL70102) {
                    
                        chbAnyImSearch.Enabled = true;
                        
                    } else { // implant has a ZL70101 or ZL70102
                    
                        chbAnyImSearch.Checked = true;
                        chbAnyImSearch.Enabled = false;
                    }
                    
                } else {
                
                    chbAnyImSearch.Enabled = true;
                }
            }
        }

        // This changes the accessibility of form controls for the CCA/RSSI state.
        //
        private void stateChangeContinuousCcaOrRssi()
        {
            grpWakeupMode.Enabled = false;
            
            // implant
            btnWakeupIm.Enabled = false;
            btnSendEmer.Enabled = false;
            
            // base
            btnWakeupBsm.Enabled = false;
            btnStartSession.Enabled = false;
            btnStartImSearch.Enabled = false;
            btnStartListen.Enabled = false;
            chbResetMicsOnWakeup.Enabled = false;
            chbAutoCca.Enabled = false;
            chbAnyImSearch.Enabled = false;
            chbAutoListen.Enabled = false;
            chbContinuousCcaOrRssi.Enabled = false;
            chbEnabHkWrite.Enabled = false;
            grpLinkSetup.Enabled = false;
            grpCal.Enabled = false;
            grpTransmitData.Enabled = false;
            grpReceiveData.Enabled = false;
            enabTestTabComponents(false);
        }

        // This changes the accessibility of form controls for the search state.
        //
        private void stateChangeSearchForImplant()
        {
            grpRxImdTidList.Enabled = true;
            grpWakeupMode.Enabled = false;
            
            // base
            btnWakeupBsm.Enabled = false;
            btnStartSession.Enabled = false;
            btnStartListen.Enabled = false;
            chbResetMicsOnWakeup.Enabled = false;
            chbAutoCca.Enabled = false;
            chbAnyImSearch.Enabled = false;
            chbAutoListen.Enabled = false;
            chbEnabHkWrite.Enabled = false;

            grpLinkSetup.Enabled = false;
            grpCcaOrRssi.Enabled = false;
            grpCal.Enabled = false;
            grpTransmitData.Enabled = false;
            grpReceiveData.Enabled = false;
            enabTestTabComponents(false);
            tabRemote.Enabled = false;
        }

        // This changes the accessibility of form controls for the listen state.
        //
        private void stateChangeListenForEmer()
        {
            grpRxImdTidList.Enabled = true;
            grpWakeupMode.Enabled = false;
            
            // base
            btnWakeupBsm.Enabled = false;
            btnStartSession.Enabled = false;
            btnStartImSearch.Enabled = false;
            chbResetMicsOnWakeup.Enabled = false;
            chbAutoCca.Enabled = false;
            chbAnyImSearch.Enabled = false;
            chbAutoListen.Enabled = false;
            chbEnabHkWrite.Enabled = false;

            grpLinkSetup.Enabled = false;
            grpCcaOrRssi.Enabled = false;
            grpCal.Enabled = false;
            grpTransmitData.Enabled = false;
            grpReceiveData.Enabled = false;
            enabTestTabComponents(false);
            tabRemote.Enabled = false;
        }

        // This changes the accessibility of form controls for the "in session" state.
        //
        private void stateChangeSession()
        {
            grpRxImdTidList.Enabled = false;
            grpWakeupMode.Enabled = false;
            grpTransmitData.Enabled = true;
            grpReceiveData.Enabled = true;
            tabRemote.Enabled = true;
            
            if (isImForm) {
            
                btnImRegs.Enabled = true;
                btnImTx400.Enabled = true;
                lblImTx400.Enabled = true;
                btnImRx400.Enabled = true;
                lblImRx400.Enabled = true;
                txtImVsup1.Enabled = true;
                txtImVsup2.Enabled = true;
                lblImVsup1.Enabled = true;
                lblImVsup2.Enabled = true;
                btnImLinkQual.Enabled = true;
                
            } else { // base station GUI
            
                // if implant isn't a ZL70100, enable implant registers button 
                if (bsmMod.CfgMicsRevOnImplant > CommonApi.MICS_REV.ZL70100) {
                    btnImRegsOnBsm.Enabled = true;
                }
                btnImTx400.Enabled = false;
                lblImTx400.Enabled = false;
                btnImRx400.Enabled = false;
                lblImRx400.Enabled = false;
                txtImVsup1.Enabled = false;
                txtImVsup2.Enabled = false;
                lblImVsup1.Enabled = false;
                lblImVsup2.Enabled = false;
                btnImLinkQual.Enabled = false;
                txtImVsup1.Text = INVALID_VALUE;
                txtImVsup2.Text = INVALID_VALUE;
            }

            if (imRegsForm != null) imRegsForm.regFormEnable(true);
        }

        // This changes the accessibility of form controls for the "start session" state.
        //
        private void stateChangeStartSession()
        {
            btnSendEmer.Enabled = false;
            btnWakeupBsm.Enabled = false;
            btnStartImSearch.Enabled = false;
            btnStartListen.Enabled = false;
            chbResetMicsOnWakeup.Enabled = false;
            chbAutoCca.Enabled = false;
            chbAnyImSearch.Enabled = false;
            chbAutoListen.Enabled = false;
            grpRxImdTidList.Enabled = false;
            grpWakeupMode.Enabled = false;
            grpLinkSetup.Enabled = false;
            grpCcaOrRssi.Enabled = false;
            grpCal.Enabled = false;
            enabTestTabComponents(false);
        }

        // This changes the accessibility of form controls for the "send emergency" state.
        //
        private void stateChangeSendEmer()
        {
            grpRxImdTidList.Enabled = false;
            grpWakeupMode.Enabled = false;
            btnWakeupIm.Enabled = false;

            grpLinkSetup.Enabled = false;
            grpCcaOrRssi.Enabled = false;
            grpCal.Enabled = false;
            grpTransmitData.Enabled = false;
            grpReceiveData.Enabled = false;
            enabTestTabComponents(false);
            
            if (isImForm) {
            
                btnImRegs.Enabled = true;
                btnImTx400.Enabled = true;
                lblImTx400.Enabled = true;
                btnImRx400.Enabled = true;
                lblImRx400.Enabled = true;
                if (imRegsForm != null) imRegsForm.regFormEnable(true);
            }
        }

        private void activitiesDisableSession(bool isInSession)
        {
            if (isInSession) {
            
                btnStartSession.Text = "Stop Session";
                
            } else {
            
                btnStartSession.Text = "Start Session";
                
                // clean up "in session" only activities
                btnTransmitData.Text = "Transmit Data";
                chbTransmitDataForever.Enabled = true;
                inTransmitData = false;
                tabRemote.Enabled = false;
            }
        }

        private void activitiesDisableSearchForImplant(bool isInSearchForImplant)
        {
            if (isInSearchForImplant) {
                btnStartImSearch.Text = "Stop Search";
                grpRxImdTidList.Text = "Wake-up Responses";
            } else {
                btnStartImSearch.Text = "Search Implant(s)";
                grpRxImdTidList.Text = "Wake-up Responses or Emergency Messages";
            }
        }

        private void activitiesDisableListenForEmer(bool isInListenForEmer)
        {
            if (isInListenForEmer) {
                btnStartListen.Text = "Stop Listening";
                grpRxImdTidList.Text = "Emergency Messages";
            } else {
                btnStartListen.Text = "Listen Emergency";
                grpRxImdTidList.Text = "Wake-up Responses or Emergency Messages";
            }
        }

        private void activitiesDisableSendEmer(bool isInSend)
        {
            if (isInSend) {
                btnSendEmer.Text = "Stop Emergency";
            } else {
                btnSendEmer.Text = "Send Emergency";
            }
        }

        private void activitiesDisableTest(bool isInTest, Button p, string id, Button btnToDisable)
        {
            if (isInTest) {
                p.Text = id + " DIS";
                p.BackColor = Color.LimeGreen;
                if (btnToDisable != null) btnToDisable.Enabled = false;
            } else {
                p.Text = id + " EN";
                p.BackColor = Color.Transparent;
                if (btnToDisable != null) btnToDisable.Enabled = true;
            }
        }

        private void btnStartSession_Click(object sender, EventArgs e)
        {
            Button p = (Button)sender;
            
            if (p.Text.Contains("Start")) {
                startSession();
            } else {
                stopSession();
            }
        }
        
        private void startSession()
        {
            string err = "";
            
            if (bsmMod.startSession(0, ref err) < 0) {
            
                Util.displayError(txtSysMsg, "Failed to start session: " + err);
                
            } else {
            
                activitiesDisableAll(GUI_STATE.START_SESSION);
                activitiesDisableSession(true);
                stateChangeStartSession();
                inStartSession = true;
                    
                // if auto-CCA enabled, tell status to add "(CCA)" to displayed chan
                addCcaToStatChan = chbAutoCca.Checked;
            }
        }
        
        private void stopSession()
        {
            string err = "";
            
            if (bsmMod.micsAbort(ref err) < 0) {
            
                Util.displayError(txtSysMsg, "Failed to stop session: " + err);
                
            } else {
            
                activitiesDisableSession(false);
                stateChangeIdle();
                inStartSession = false;
            } 
        }
        
        private void btnStartImSearch_Click(object sender, EventArgs e)
        {
            Button p = (Button)sender;
            
            if (p.Text.Contains("Implant")) {
                startSearchForImplant();
            } else {
                stopSearchForImplant();
            }
        }
        
        private void startSearchForImplant()
        {
            string err = "";
            
            clearRxImdTidList();

            if (bsmMod.searchForImplant(false, chbAnyImSearch.Checked, ref err) < 0) {
            
                Util.displayError(txtSysMsg, "Failed to start implant search: " + err);
                
            } else {
            
                activitiesDisableSearchForImplant(true);
                stateChangeSearchForImplant();
                inSearchForImplant = true;
                    
                // if auto-CCA enabled, tell status to add "(CCA)" to displayed chan
                addCcaToStatChan = chbAutoCca.Checked;
            }
        }
        
        private void stopSearchForImplant()
        {
            string err = "";
            
            if (bsmMod.micsAbort(ref err) < 0) {
                Util.displayError(txtSysMsg, "Failed to stop implant search: " + err);
            } 
            
            // Stop the search in the GUI. Note this is done even if bsmMod.micsAbort()
            // failed so the GUI will stop polling the base station for implant responses,
            // since that would just repeatedly fail (see tmrBackgroundTasks_Tick()).
            //
            activitiesDisableSearchForImplant(false);
            stateChangeIdle();
            inSearchForImplant = false;
        }

        private void btnStartListen_Click(object sender, EventArgs e)
        {
            Button p = (Button)sender;
            
            if (p.Text.Contains("Stop")) {
                stopListenForEmer();
            } else {
                startListenForEmer();
            }
        }
        
        private void startListenForEmer()
        {
            string err = "";
            
            clearRxImdTidList();

            if (bsmMod.listenForEmergency(false, ref err) < 0) {
            
                Util.displayError(txtSysMsg, "Failed to start listening for emergency: " + err);
                
            } else {
            
                activitiesDisableAll(GUI_STATE.LISTEN_FOR_EMER);
                activitiesDisableListenForEmer(true);
                stateChangeListenForEmer();
                inListenForEmer = true;
            }
        }
        
        private void stopListenForEmer()
        {
            string err = "";
            
            if (bsmMod.micsAbort(ref err) < 0) {
                Util.displayError(txtSysMsg, "Failed to stop listening for emergency: " + err);
            }
            
            // Stop the listen in the GUI. Note this is done even if bsmMod.micsAbort()
            // failed so the GUI will stop polling the base station for implant emergencies,
            // since that would just repeatedly fail (see tmrBackgroundTasks_Tick()).
            //
            activitiesDisableAll(GUI_STATE.UNKNOWN);
            activitiesDisableListenForEmer(false);
            stateChangeIdle();
            inListenForEmer = false;
        }
        
        private void clearRxImdTidList()
        {
            // clear the display for the list of received IMD TIDs
            dgvRxImdTidList.Rows.Clear();
            
            // clear the list of received IMD TIDs
            for(int i = 0; i < numEntriesInRxImdTidList; i++) {
                rxImdTidList[i].imdTid = 0;
                rxImdTidList[i].count = 0;
            }
            numEntriesInRxImdTidList = 0;
        }

        private void cmbNormTxMod_SelectedIndexChanged(object sender, EventArgs e)
        {
            CommonApi.MOD txMod = (CommonApi.MOD)cmbNormTxMod.SelectedIndex;
            CommonApi.MOD rxMod = (CommonApi.MOD)cmbNormRxMod.SelectedIndex;
            
            // Update GUI components that are affected by the new selection. Note
            // that this is done even if the initialization isn't complete.
            //
            // If selected a Barker mode (ZL70103 or later), disable the user data for 2.45 GHz
            // wakeup because the Barker modes use the same register bits (reg_mac_moduser[6:4]).
            //
            if ((txMod == CommonApi.MOD.MOD_BARKER_5) || (txMod == CommonApi.MOD.MOD_BARKER_11) ||
                (rxMod == CommonApi.MOD.MOD_BARKER_5) || (rxMod == CommonApi.MOD.MOD_BARKER_11)) {
                cmbWake245UserData.Enabled = false;
            } else {
                cmbWake245UserData.Enabled = true;
            }
            
            // if selected a Barker mode (ZL70103 or later)
            if ((txMod == CommonApi.MOD.MOD_BARKER_5) || (txMod == CommonApi.MOD.MOD_BARKER_11)) {
            
                // if 400 MHz wake-up mode is selected, report error and undo Barker selection
                if (rad400MHzWakeup.Checked) {
                
                    MessageBox.Show("The 400 MHz wake-up mode is currently selected, which does not support Barker modes.");
                    cmbNormTxMod.SelectedIndex = (int)CommonApi.MOD.MOD_2FSK_FB;
                    return;
                }
                // if base station GUI & selected implant type is ZL70100/101/102, report error and undo Barker selection
                if (!isImForm && (bsmMod.CfgMicsRevOnImplant < CommonApi.MICS_REV.ZL70103)) {
                
                    MessageBox.Show("The selected implant type is currently ZL70100/101/102, which does not support Barker modes.");
                    cmbNormTxMod.SelectedIndex = (int)CommonApi.MOD.MOD_2FSK_FB;
                    return;
                }
            }
            
            // set normal TX modulation in ADK module config (also sends config to board)
            adkMod.CfgNormTxMod = (CommonApi.MOD)cmbNormTxMod.SelectedIndex;
        }

        private void chbAutoCca_CheckedChanged(object sender, EventArgs e)
        {
            bsmMod.CfgAutoCcaEnab = ((CheckBox)sender).Checked;
        }

        // This disables everything in the GUI that is related to the current
        // activity unless the current activity corresponds to the specified
        // "preserveState", in which case this does nothing.
        //
        private void activitiesDisableAll(GUI_STATE preserveState)
        {
            if (inRx400Test || inTx400Test || inTx245Test) {
            
                activitiesDisableTest(false, btnTestRx400Enab, "RX", btnTestTx400Enab);
                activitiesDisableTest(false, btnTestTx400Enab, "TX", btnTestRx400Enab);
                activitiesDisableTest(false, btnTestTx245Enab, "TX", null);
                inRx400Test = false;
                inTx400Test = false;
                inTx245Test = false;
            }
            
            if (inListenForEmer && (preserveState != GUI_STATE.LISTEN_FOR_EMER)) {
                activitiesDisableListenForEmer(false);
                inListenForEmer = false;
            }
            
            if (inSearchForImplant && (preserveState != GUI_STATE.SEARCH_FOR_IMPLANT)) {
                activitiesDisableSearchForImplant(false);
                inSearchForImplant = false;
            }
            
            if (inSendEmer && (preserveState != GUI_STATE.SEND_EMER)) {
                activitiesDisableSendEmer(false);
                inSendEmer = false;
            }
            
            if (inStartSession && (preserveState != GUI_STATE.START_SESSION)) {
                activitiesDisableSession(false);
                inStartSession = false;
            }
            
            if (inSession && (preserveState != GUI_STATE.SESSION)) {
            
                if (inTransmitData) {
                
                    // clean up "in session" only activities
                    btnTransmitData.Text = "Transmit Data";
                    chbTransmitDataForever.Enabled = true;
                    tabRemote.Enabled = false;
                    inTransmitData = false;
                }
                activitiesDisableSession(false);
                inSession = false;
            }
            
            if (inContinuousCcaOrRssi && (preserveState != GUI_STATE.CONTINUOUS_CCA_OR_RSSI)) {
            
                if (isImForm) {
                    btnPerformCcaOrRssi.Text = "Perform RSSI";
                } else {
                    btnPerformCcaOrRssi.Text = "Perform CCA";
                }
                chbContinuousCcaOrRssi.Enabled = true;
                inContinuousCcaOrRssi = false;
            }
        }

        private void updateGuiStateToMatchPolledMicsState(CommonApi.MICS_STATE micsState)
        {
            switch (micsState) {
            case CommonApi.MICS_STATE.SLEEPING:
            
                if (guiState != GUI_STATE.SLEEP) {
                
                    txtStatOpState.Text = OP_STATE_SLEEP;
                    txtStatOpState.BackColor = Color.White;
                    
                    activitiesDisableAll(GUI_STATE.UNKNOWN);
                    stateChangeSleep();
                    
                    if (isImForm) {
                        btnWakeupIm.Text = "Direct Wake-up";
                    } else {
                        btnWakeupBsm.Text = "Direct Wake-up";
                    }
                    guiState = GUI_STATE.SLEEP;
                }
                break;
                
            case CommonApi.MICS_STATE.IDLE:
            
                if (inContinuousCcaOrRssi) {
                
                    if (guiState != GUI_STATE.CONTINUOUS_CCA_OR_RSSI) {
                        txtStatOpState.BackColor = Color.Yellow;
                        if (isImForm) {
                            txtStatOpState.Text = OP_STATE_PERFORM_RSSI;
                        } else {
                            txtStatOpState.Text = OP_STATE_PERFORM_CCA;
                        }
                        guiState = GUI_STATE.CONTINUOUS_CCA_OR_RSSI;
                    }
                    
                } else if (inRx400Test || inTx400Test || inTx245Test) {
                
                    // do nothing
                    
                } else if (guiState != GUI_STATE.IDLE) {
                
                    txtStatOpState.BackColor = Color.White;
                    txtStatOpState.Text = OP_STATE_IDLE;
                    
                    activitiesDisableAll(GUI_STATE.UNKNOWN);
                    stateChangeIdle();
                    
                    guiState = GUI_STATE.IDLE;
                }
                break;
                
            case CommonApi.MICS_STATE.SEARCHING_FOR_IMPLANT:
            
                if (guiState != GUI_STATE.SEARCH_FOR_IMPLANT) {
                
                    txtStatOpState.BackColor = Color.Yellow;
                    txtStatOpState.Text = OP_STATE_SEARCH_FOR_IMPLANT;
                    activitiesDisableAll(GUI_STATE.SEARCH_FOR_IMPLANT);
                    
                    if (inSearchForImplant == false) {
                        activitiesDisableSearchForImplant(true);
                        stateChangeSearchForImplant();
                        inSearchForImplant = true;
                    }
                    guiState = GUI_STATE.SEARCH_FOR_IMPLANT;
                }
                break;
                
            case CommonApi.MICS_STATE.STARTING_SESSION:
            
                if (guiState != GUI_STATE.START_SESSION) {
                
                    txtStatOpState.BackColor = Color.Yellow;
                    txtStatOpState.Text = OP_STATE_START_SESSION;
                    
                    if (inStartSession == false) {
                    
                        activitiesDisableAll(GUI_STATE.START_SESSION);
                        stateChangeStartSession();
                        activitiesDisableSession(true);
                        inStartSession = true;
                    }
                    guiState = GUI_STATE.START_SESSION;
                }
                break;
                
            case CommonApi.MICS_STATE.LISTENING_FOR_EMERGENCY:
            
                if (guiState != GUI_STATE.LISTEN_FOR_EMER) {
                
                    txtStatOpState.BackColor = Color.Yellow;
                    txtStatOpState.Text = OP_STATE_LISTEN_FOR_EMER;
                    
                    // if listening due to base station GUI command
                    if (inListenForEmer) {
                    
                        activitiesDisableAll(GUI_STATE.LISTEN_FOR_EMER);
                        activitiesDisableListenForEmer(true);
                        stateChangeListenForEmer();
                        
                    } else { // listening due to auto-listen or unknown startup condition
                    
                        if ((guiState == GUI_STATE.UNKNOWN) && (!chbAutoListen.Checked)) {
                        
                            activitiesDisableListenForEmer(true);
                            stateChangeListenForEmer();
                            inListenForEmer = true;
                            
                        } else { // assume base is automatically listening for emergencies
                        
                            // while listening automatically, everything else is enabled
                            activitiesDisableAll(GUI_STATE.UNKNOWN);
                            stateChangeIdle();
                        }
                    }
                    guiState = GUI_STATE.LISTEN_FOR_EMER;
                }
                break;
                
            case CommonApi.MICS_STATE.SENDING_EMERGENCY:
            
                if (guiState != GUI_STATE.SEND_EMER) {
                
                    txtStatOpState.BackColor = Color.Yellow;
                    txtStatOpState.Text = OP_STATE_SEND_EMER;
                    activitiesDisableAll(GUI_STATE.SEND_EMER);
                    
                    if (inSendEmer == false) {
                        activitiesDisableSendEmer(true);
                        stateChangeSendEmer();
                        inSendEmer = true;
                    }
                    guiState = GUI_STATE.SEND_EMER;
                }
                break;

            case CommonApi.MICS_STATE.IN_SESSION:
            
                if (guiState != GUI_STATE.SESSION) {
                
                    txtStatOpState.BackColor = Color.LightGreen;
                    txtStatOpState.Text = OP_STATE_SESSION;
                    activitiesDisableAll(GUI_STATE.SESSION);
                    
                    if (inSession == false) {
                    
                        // All of these must be called to ensure the state of
                        // the GUI is changed correctly for all cases.
                        //
                        activitiesDisableSession(true);
                        stateChangeStartSession();
                        stateChangeSession();
                        inSession = true;
                    }
                    guiState = GUI_STATE.SESSION;
                }
                break;
                
            case CommonApi.MICS_STATE.SENDING_WAKEUP_RESPONSES:
            
                if (guiState != GUI_STATE.SEND_RESPONSES) {
                
                    txtStatOpState.Text = OP_STATE_SEND_RESPONSES;
                    txtStatOpState.BackColor = Color.Yellow;
                    guiState = GUI_STATE.SEND_RESPONSES;
                }
                break;
                    
            default:
            
                if (guiState != GUI_STATE.UNRECOGNIZED) {
                
                    txtStatOpState.Text = OP_STATE_UNRECOGNIZED;
                    guiState = GUI_STATE.UNRECOGNIZED;
                    
                    if (isImForm) {
                        Util.displayError(txtSysMsg, "The implant reported an unrecognized state.");
                    } else {
                        Util.displayError(txtSysMsg, "The base station reported an unrecognized state.");
                    }
                }
                break;
            }
            
            // If not starting session, in session, searching, or idle, set
            // "addCcaToStatChan" to false to ensure the status polling won't
            // add "(CCA)" to the displayed channel. Note that this is done
            // last so the GUI state is updated before this checks it.
            //
            if ((guiState != GUI_STATE.START_SESSION) && (guiState != GUI_STATE.SESSION) &&
                (guiState != GUI_STATE.SEARCH_FOR_IMPLANT)) {
                
                addCcaToStatChan = false;
            }
        }

        // called on status poll timer object tick to acquire and process module status
        private void tmrStatPoll_Tick(object sender, EventArgs e)
        {
            if (chbStatPoll.Checked == false) return;
            
            CommonApi.AdpStatChanges asc = null;
            CommonApi.StatChanges sc = null;
            BsmApi.BsmStatChanges bsc = null;
            ImApi.ImStatChanges isc = null;
            string vsup1 = "";
            string vsup2 = "";
            string err = "";

            // get status changes for ADP board
            if ((asc = adkMod.getAdpStatChanges(forceStat, ref err)) == null) {
                reportGetStatErr(ref err);
                return;
            }

            // if connected to base, get status changes for base
            if (bsmMod != null) {
            
                if ((bsc = bsmMod.getStatChanges(forceStat, ref err)) == null) {
                    reportGetStatErr(ref err);
                    return;
                }
                
                // set "sc" to use base status (for base GUI)
                sc = bsc;
            }

            // if connected to implant, get status changes for implant
            if (imMod != null) {
            
                if ((isc = imMod.getStatChanges(forceStat, ref err)) == null) {
                    reportGetStatErr(ref err);
                    return;
                }
                
                // if implant GUI, set "sc" to use implant status
                if (isImForm) sc = isc;
            }
            
            // if ADP board reported an error, display it
            if (asc.adpErr != null) {
                Util.displayError(txtSysMsg, asc.adpErr);
            }
            
            // update display, etc. as needed for current MICS state
            updateGuiStateToMatchPolledMicsState(sc.micsState);
            
            // if link status changed, update display, etc.
            if (sc.linkStatChanged) {
            
                txtStatCompanyId.Text = "0x" + sc.linkStat.companyId.ToString("X2");
                txtStatImdTid.Text = "0x" + sc.linkStat.imdTid.ToString("X6");
                txtStatTxMod.Text = adkMod.modStrings[(int)sc.linkStat.txMod];
                txtStatRxMod.Text = adkMod.modStrings[(int)sc.linkStat.rxMod];
                txtStatBytesPerBlock.Text = sc.linkStat.rxBlockSize.ToString();
                txtStatMaxBlocksPerPack.Text = sc.linkStat.maxBlocksPerTxPack.ToString();

                // update displayed chan and add "(CCA)" if desired
                if (addCcaToStatChan) {
                    txtStatChan.Text = sc.linkStat.chan.ToString() + " (CCA)";
                } else {
                    txtStatChan.Text = sc.linkStat.chan.ToString();
                }
                
                // update reported data block size for reference elsewhere
                this.blockSizeFromStat = sc.linkStat.rxBlockSize;
            }

            // if state of 400 MHz TX changed, update display
            if (sc.tx400ActiveChanged) {
            
                if (sc.tx400Active) {
                    if (isImForm) {
                        btnImTx400.BackColor = Color.LimeGreen;
                    } else {
                        btnBsmTx400.BackColor = Color.LimeGreen;
                    }
                } else {
                    btnBsmTx400.BackColor = Color.White;
                    btnImTx400.BackColor = Color.White;
                }
            }
    
            // if state of 400 MHz RX changed, update display
            if (sc.rx400ActiveChanged) {
            
                if (sc.rx400Active) {
                    if (isImForm) {
                        btnImRx400.BackColor = Color.LimeGreen;
                    } else {
                        btnBsmRx400.BackColor = Color.LimeGreen;
                    }
                } else {
                    btnBsmRx400.BackColor = Color.White;
                    btnImRx400.BackColor = Color.White;
                }
            }
            
            // for base status only
            if (bsc != null) {
            
                // If link quality is displayed, update for base. Note this references
                // this.blockSizeFromStat, which the status polling updates above (see
                // sc.linkStatChanged), so it's safe to reference.
                //
                if (linkQualForm != null) {
                    linkQualForm.updateBsmStats(bsc, this.blockSizeFromStat);
                }
                
                // if data status changed for base & data test is active
                if (bsc.dataStatChanged && inDataTest) {
                    totalBsmDataErrCount += bsc.dataStat.dataErrCount;
                    txtDtTotalRxByteErrsBsm.Text = totalBsmDataErrCount.ToString();
                }
                
                // if base reported an error, display it
                if (bsc.bsmErr != null) {
                    Util.displayError(txtSysMsg, bsc.bsmErr);
                }
            
                // if state of 2.45 GHz TX changed, update display
                if (bsc.tx245ActiveChanged) {
                    if (bsc.tx245Active) {
                        btnBsmTx245.BackColor = Color.LimeGreen;
                    } else {
                        btnBsmTx245.BackColor = Color.White;
                    }
                }
            }
    
            // for implant status only
            if (isc != null) {
            
                // If link quality is displayed, update for implant. Note this references
                // this.blockSizeFromStat, which the status polling updates above (see
                // sc.linkStatChanged), so it's safe to reference.
                //
                if (linkQualForm != null) {
                    linkQualForm.updateImStats(isc, this.blockSizeFromStat);
                }
                
                // if data status changed for implant & data test is active
                if (isc.dataStatChanged && inDataTest) {
                    totalImDataErrCount += isc.dataStat.dataErrCount;
                    txtDtTotalRxByteErrsIm.Text = totalImDataErrCount.ToString();
                }
                
                // if implant reported an error, display it
                if (isc.imErr != null) {
                    Util.displayError(txtSysMsg, isc.imErr);
                }
            }

            // if implant GUI
            if (isImForm) {
            
                // If VSUP1 or VSUP2 changed, update display but a value of 0
                // indicates the ADC12 is in use by power monitoring, so do not
                // try to display.
                //
                if (asc.vsup1MvChanged && (asc.vsup1Mv != 0)) {
                
                    vsup1 = asc.vsup1Mv.ToString();
                    
                    // Note that this formatting will fail with an exception if
                    // the vsup1 result of "0" was attempted.
                    //
                    txtImVsup1.Text = vsup1.Substring(0, 1) + "." + vsup1.Substring(1, 3) + " V";
                }
                if (asc.vsup2MvChanged && (asc.vsup2Mv != 0)) {
                    vsup2 = asc.vsup2Mv.ToString();
                    txtImVsup2.Text = vsup2.Substring(0, 1) + "." + vsup2.Substring(1, 3) + " V";
                }
            }
            
            // now that the status has been udpated, clear this for next poll
            forceStat = false;
        }

        private void reportGetStatErr(ref string err)
        {
            Util.displayError(txtSysMsg, "Status polling disabled due to error: " + err);
            chbStatPoll.Checked = false;
        }

        private void cmbNormRxMod_SelectedIndexChanged(object sender, EventArgs e)
        {
            CommonApi.MOD rxMod = (CommonApi.MOD)cmbNormRxMod.SelectedIndex;
            CommonApi.MOD txMod = (CommonApi.MOD)cmbNormTxMod.SelectedIndex;
            
            // Update the GUI components that are affected by the new selection.
            // If selected a Barker mode (ZL70103 or later), disable the user data
            // for 2.45 GHz wakeup because the Barker modes use the same register
            // bits (reg_mac_moduser[6:4]).
            //
            if ((txMod == CommonApi.MOD.MOD_BARKER_5) || (txMod == CommonApi.MOD.MOD_BARKER_11) ||
                (rxMod == CommonApi.MOD.MOD_BARKER_5) || (rxMod == CommonApi.MOD.MOD_BARKER_11)) {
                cmbWake245UserData.Enabled = false;
            } else {
                cmbWake245UserData.Enabled = true;
            }
            
            // if selected a Barker mode (ZL70103 or later)
            if ((rxMod == CommonApi.MOD.MOD_BARKER_5) || (rxMod == CommonApi.MOD.MOD_BARKER_11)) {
            
                // if 400 MHz wake-up mode is selected, report error and undo Barker selection
                if (rad400MHzWakeup.Checked) {
                
                    MessageBox.Show("The 400 MHz wake-up mode is currently selected, which does not support Barker modes.");
                    cmbNormRxMod.SelectedIndex = (int)CommonApi.MOD.MOD_2FSK_FB;
                    return;
                }
                // if base station GUI & selected implant type is ZL70100/101/102, report error and undo Barker selection
                if (!isImForm && (bsmMod.CfgMicsRevOnImplant < CommonApi.MICS_REV.ZL70103)) {
                
                    MessageBox.Show("The selected implant type is currently ZL70100/101/102, which does not support Barker modes.");
                    cmbNormRxMod.SelectedIndex = (int)CommonApi.MOD.MOD_2FSK_FB;
                    return;
                }
            }
            
            // set normal RX modulation in ADK module config (also sends config to board)
            adkMod.CfgNormRxMod = (CommonApi.MOD)cmbNormRxMod.SelectedIndex;
        }
        
        private void bsmRegs_FormClosed(object sender, EventArgs e)
        {
            bsmRegsForm = null;
        }
        
        private void regIm_FormClosed(object sender, EventArgs e)
        {
            imRegsForm = null;
        }
        
        // This function is not intended for customer use (factory tab only)
        // so use by customers is not supported.
        //
        private void adkTraceViewForm_FormClosed(object sender, EventArgs e)
        {
            // Set this to null so the next time the "Start Trace Viewer"
            // button on the factory tab is clicked, btnStartTraceView_Click()
            // will re-open the trace viewer. That way, the trace viewer can
            // be restarted without having to restart the whole GUI.
            //
            adkTraceViewForm = null;
        }

        private void adpPowMonForm_FormClosed(object sender, EventArgs e)
        {
            // Set this to null so the next time the "Power Monitor" button for
            // the implant is clicked, btnImPowMon_Click() will re-open the
            // power monitor. That way, the power monitor can be restarted
            // without having to restart the whole GUI.
            //
            adpPowMonForm = null;
        }

        private void btnBsmRegs_Click(object sender, EventArgs e)
        {
            if (bsmRegsForm == null) {
            
                bsmRegsForm = new RegistersForm(false, bsmMod, false, txtSysMsg, bsmMod.MicsRev);
                
                // set title displayed in registers form
                if (bsmMod.MicsRev > CommonApi.MICS_REV.ZL70102) {  // ZL70103
                    bsmRegsForm.Text = "ZL70103 Registers (" + mezzModel + ")";
                } else if (bsmMod.MicsRev > CommonApi.MICS_REV.ZL70101) { // ZL70102
                    bsmRegsForm.Text = "ZL70102 Registers (" + mezzModel + ")";
                } else { // ZL70101
                    bsmRegsForm.Text = "ZL70101 Registers (" + mezzModel + ")";
                }
                
                bsmRegsForm.FormClosed += new System.Windows.Forms.FormClosedEventHandler(bsmRegs_FormClosed);
                bsmRegsForm.Show();
                
            } else {
                bsmRegsForm.Focus();
            }
        }

        private void btnImRegs_Click(object sender, EventArgs e)
        {
            if (imRegsForm == null) {
                
                imRegsForm = new RegistersForm(true, imMod, false, txtSysMsg, imMod.MicsRev);
                    
                // set title displayed in registers form
                if (imMod.MicsRev > CommonApi.MICS_REV.ZL70102) {  // ZL70103
                    imRegsForm.Text = "ZL70103 Registers (" + mezzModel + ")";
                } else if (imMod.MicsRev > CommonApi.MICS_REV.ZL70101) {  // ZL70102
                    imRegsForm.Text = "ZL70102 Registers (" + mezzModel + ")";
                } else {  // ZL70101
                    imRegsForm.Text = "ZL70101 Registers (" + mezzModel + ")";
                }
                    
                imRegsForm.FormClosed += new System.Windows.Forms.FormClosedEventHandler(regIm_FormClosed);
                imRegsForm.Show();
                    
            } else {
                
                imRegsForm.Focus();
            }                              
        }

        private void btnImRegsOnBsm_Click(object sender, EventArgs e)
        {
            if (imRegsForm == null) {

                imRegsForm = new RegistersForm(true, bsmMod, true, txtSysMsg, bsmMod.CfgMicsRevOnImplant);

                // set title displayed in registers form
                if (bsmMod.CfgMicsRevOnImplant > CommonApi.MICS_REV.ZL70102) {  // ZL70103
                    imRegsForm.Text = "ZL70103 Registers (Remote Implant)";
                } else if (bsmMod.CfgMicsRevOnImplant > CommonApi.MICS_REV.ZL70101) {  // ZL70102
                    imRegsForm.Text = "ZL70102 Registers (Remote Implant)";
                } else {  // ZL70101
                    imRegsForm.Text = "ZL70101 Registers (Remote Implant)";
                }

                imRegsForm.FormClosed += new System.Windows.Forms.FormClosedEventHandler(regIm_FormClosed);
                imRegsForm.Show();

            } else {
                imRegsForm.Focus();
            }
    }

        private void cmbNormChan_SelectedIndexChanged(object sender, EventArgs e)
        {
            adkMod.CfgNormChan = (byte)((ComboBox)sender).SelectedIndex;
        }

        private void cmbNormBytesPerBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            adkMod.CfgNormBlockSize = (byte)(((ComboBox)sender).SelectedIndex + 2);
        }

        private void cmbNormMaxBlocksPerPack_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox p = (ComboBox)sender;
            
            if (p.Text != p.SelectedIndex.ToString()) {
                p.SelectedIndex = Int32.Parse(p.Text, NumberStyles.Integer) - 1;
            }
            adkMod.CfgNormMaxBlocksPerTxPack = (byte)(p.SelectedIndex + 1);
        }

        private void cmbEmerTxMod_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox p = (ComboBox)sender;
            CommonApi.MOD txMod = (CommonApi.MOD)p.SelectedIndex;
            
            // if selected a Barker mode (ZL70103 or later)
            if ((txMod == CommonApi.MOD.MOD_BARKER_5) || (txMod == CommonApi.MOD.MOD_BARKER_11)) {
            
                // if base station GUI & selected implant type is ZL70100/101/102, report error and undo Barker selection
                if (!isImForm && (bsmMod.CfgMicsRevOnImplant < CommonApi.MICS_REV.ZL70103)) {
                
                    MessageBox.Show("The selected implant type is currently ZL70100/101/102, which does not support Barker modes.");
                    cmbEmerTxMod.SelectedIndex = (int)CommonApi.MOD.MOD_2FSK_FB;
                    return;
                }
            }
            
            // set emergency TX modulation in ADK module config (also sends config to board)
            adkMod.CfgEmerTxMod = (CommonApi.MOD)p.SelectedIndex;
        }

        private void chbAutoListen_CheckedChanged(object sender, EventArgs e)
        {
            bsmMod.CfgAutoListenEnab = ((CheckBox)sender).Checked;
        }

        private void txtImdTid_TextChanged(object sender, EventArgs e)
        {
            adkMod.CfgImdTid = Int32.Parse(txtImdTid1.Text + txtImdTid2.Text +
                txtImdTid3.Text + txtImdTid4.Text + txtImdTid5.Text +
                txtImdTid6.Text, NumberStyles.AllowHexSpecifier);
        }

        private void txtCompanyId_TextChanged(object sender, EventArgs e)
        {
            adkMod.CfgCompanyId = (byte)Int32.Parse((txtCompanyId1.Text + txtCompanyId2.Text),
                NumberStyles.AllowHexSpecifier);
        }

        private void cmbEmerRxMod_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox p = (ComboBox)sender;
            CommonApi.MOD rxMod = (CommonApi.MOD)p.SelectedIndex;
            
            // if selected a Barker mode (ZL70103 or later)
            if ((rxMod == CommonApi.MOD.MOD_BARKER_5) || (rxMod == CommonApi.MOD.MOD_BARKER_11)) {
            
                // if base station GUI & selected implant type is ZL70100/101/102, report error and undo Barker selection
                if (!isImForm && (bsmMod.CfgMicsRevOnImplant < CommonApi.MICS_REV.ZL70103)) {
                
                    MessageBox.Show("The selected implant type is currently ZL70100/101/102, which does not support Barker modes.");
                    cmbEmerRxMod.SelectedIndex = (int)CommonApi.MOD.MOD_2FSK_FB;
                    return;
                }
            }
            
            // set emergency RX modulation in ADK module config (also sends config to board)
            adkMod.CfgEmerRxMod = (CommonApi.MOD)p.SelectedIndex;
        }

        private void cmbEmerChan_SelectedIndexChanged(object sender, EventArgs e)
        {
            adkMod.CfgEmerChan = (byte)((ComboBox)sender).SelectedIndex;
        }

        private void cmbWake245Country_SelectedIndexChanged(object sender, EventArgs e)
        {
            // since only one option is supported (USA), there's nothing to do
        }

        private void btnTransmitData_Click(object sender, EventArgs e)
        {
            Button p = (Button)sender;
            
            if (inTransmitData == false) {
            
                if (txtTransmitData.Text.Length == 0) return;
                
                if (chbTransmitDataForever.Checked) {
                
                    p.Text = "Stop Transmit Data";
                    chbTransmitDataForever.Enabled = false;
                    grpTransmitDataType.Enabled = false;
                    txtTransmitData.Enabled = false;
                }
                
                if (radTransmitDataHex.Checked) {
                
                    txLen = convertStrOfHexCharsToBinBytes(txtTransmitData.Text, txtTransmitData.Text.Length, txBuf);
                    
                    // Fill remainder of last block with 0. Note this references
                    // this.blockSizeFromStat, which is updated by the status polling.
                    // At this point, the status polling must be active, because data
                    // can only be transmitted while "inSession" is true, and only the
                    // status polling sets "inSession".
                    //
                    while((txLen % this.blockSizeFromStat) != 0) txBuf[txLen++] = 0;
                    
                } else {
                
                    byte[] asciiBytes = Util.convertStrToAsciiBytes(txtTransmitData.Text);
                    txLen = asciiBytes.Length;
                    for(int i = 0; i < txLen; i++) txBuf[i] = asciiBytes[i];
                    
                    // Fill remainder of last block with '.'. Note this references
                    // this.blockSizeFromStat, which is updated by the status polling.
                    // At this point, the status polling must be active, because data
                    // can only be transmitted while "inSession" is true, and only the
                    // status polling sets "inSession".
                    //
                    while((txLen % this.blockSizeFromStat) != 0) txBuf[txLen++] = (byte)'.';
                }
                inTransmitData = true;
                
            } else {
            
                p.Text = "Transmit Data";
                chbTransmitDataForever.Enabled = true;
                inTransmitData = false;
                grpTransmitDataType.Enabled = true;
                txtTransmitData.Enabled = true;
            }
        }
        
        // This converts the specified string of hexadecimal characters (strOfHexChars)
        // into binary data bytes and places the binary data bytes into the byte array
        // specified by "binBytes". Every two hexadecimal characters in the string are
        // converted into one binary data byte in the byte array. The number of hexadecimal
        // characters to convert is specified by "nHexCharsToConvert". This returns the
        // number of binary data bytes placed in the byte array.
        //
        private int convertStrOfHexCharsToBinBytes(string strOfHexChars,
            int nHexCharsToConvert, byte[] binBytes)
        {
            int i, j, nBytes;
            char[] newLine = Environment.NewLine.ToCharArray();
            
            if ((nHexCharsToConvert % 2) != 0) {
            
                strOfHexChars += "0";
                ++nHexCharsToConvert;
            }
            
            for(i = 0, j = 0, nBytes = 0; i < nHexCharsToConvert; i++) {
            
                if (i == strOfHexChars.IndexOfAny(newLine, i)) continue;
                
                string sHexChar = strOfHexChars.Substring(i, 1);
                
                if (j == 0) {
                    binBytes[nBytes] = (byte)(Byte.Parse(sHexChar, NumberStyles.HexNumber) << 4);
                    j = 1;
                } else {
                    binBytes[nBytes++] |= Byte.Parse(sHexChar, NumberStyles.HexNumber);
                    j = 0;
                }
            }
            return(nBytes);
        }
        
        // This performs an RSSI for the specified channel, or for all channels
        // if the specified channel is RSSI_CHAN.ALL_CHANS. Returns 0 if
        // successful, or -1 for error.
        //
        private int performRssi(RSSI_CHAN chanArg)
        {
            uint startChan, endChan;
            int rssi;
            float rssiDbm;
            string err = "";

            // if caller specified all channels, prepare to do every channel
            if (chanArg == RSSI_CHAN.ALL_CHANS) {
                startChan = 0;
                endChan = 9;
            } else {
                startChan = endChan = (uint)chanArg;
            }

            // for each specified channel, get & display RSSI
            for(uint chan = startChan; chan <= endChan; ++chan) {
            
                // if implant GUI
                if (isImForm) {               
                
                    // get internal RSSI for channel (always average over 10 ms)
                    if ((rssi = imMod.rssi(chan, ref err)) < 0) {
                        Util.displayError(txtSysMsg, "Failed to get RSSI: " + err);
                        return(-1);
                    }
                    
                    // convert internal RSSI to dBm
                    rssiDbm = imIntRssiToDbm(rssi);
                    
                    // display internal RSSI
                    txtRssi[chan].Text = rssi.ToString();
                    txtRssiDbm[chan].Text = rssiDbm.ToString("###.0");
                    
                } else { // base station GUI
                
                    // Get RSSI using the selected RSSI mode (internal or external).
                    // For an external RSSI, bsmMod.rssi() gets either the maximum RSSI
                    // detected over 10 ms (if chbUseAveRssiForCca is checked), or the
                    // average RSSI over 10 ms (if chbUseAveRssiForCca is unchecked).
                    // For an internal RSSI, bsmMod.rssi() ignores the chbUseAveRssiForCca
                    // argument and always gets the average RSSI over 10 ms.
                    //
                    if ((rssi = bsmMod.rssi(chbUseIntRssiOnBsm.Checked, chan,
                        chbUseAveRssiForCca.Checked, ref err)) < 0) {

                        Util.displayError(txtSysMsg, "Failed to get RSSI: " + err);
                        return(-1);
                    }
                    
                    // Display the RSSI. If "use internal RSSI" is selected (for
                    // factory test/debug use only), it's an internal RSSI, so display
                    // only the ADC value for the RSSI because there's no calibrated
                    // dBm conversion for the internal RSSI on the base station.
                    //
                    if (chbUseIntRssiOnBsm.Checked) {
                    
                        txtRssi[chan].Text = rssi.ToString();
                        txtRssiDbm[chan].Text = "***";
                        
                    } else {
                    
                        // convert external RSSI to dBm
                        rssiDbm = bsmExtRssiToDbm(rssi, chan);
                    
                        // display external RSSI
                        txtRssi[chan].Text = rssi.ToString();
                        txtRssiDbm[chan].Text = rssiDbm.ToString("###.0");
                    }
                }
            }          
            
            return(0);
        }

        // This performs a CCA. Returns 0 if successful, or -1 for error.
        //
        private int performCca()
        {
            BsmApi.BSM_CCA_DATA ccaData;
            int rssi;
            float rssiDbm;
            string err = "";
            int clearChan;
            
            // If "use internal RSSI" is selected on the base station GUI (for
            // factory test/debug use only), just call performRssi() to perform
            // an internal RSSI for each channel (don't perform a CCA because
            // the CCA can't use the internal RSSI).
            //
            if (chbUseIntRssiOnBsm.Checked) {
            
                if (performRssi(RSSI_CHAN.ALL_CHANS) < 0) return(-1);
                
                return(0);
            }

            // Perform CCA. This gets the external RSSI for each channel (max
            // RSSI detected over 10 ms, or average RSSI over 10 ms) and returns
            // the clearest channel.
            //
            if ((clearChan = bsmMod.cca(chbUseAveRssiForCca.Checked, out ccaData,
                ref err)) < 0) {
                
                Util.displayError(txtSysMsg, "Failed CCA: " + err);
                return(-1);
            }
            
            // display external RSSI for each channel
            for(uint chan = 0; chan < 10; ++chan) {
            
                // get external RSSI for channel
                unsafe { rssi = ccaData.rssi[chan]; }
                
                // convert external RSSI to dBm
                rssiDbm = bsmExtRssiToDbm(rssi, chan);

                // display external RSSI
                txtRssi[chan].Text = rssi.ToString();
                txtRssiDbm[chan].Text = rssiDbm.ToString("###.0");

                // use bold font for the clearest channel
                if (chan == clearChan) {
                    txtRssi[chan].Font = boldFont;
                    txtRssiDbm[chan].Font = boldFont;
                } else {
                    txtRssi[chan].Font = regularFont;
                    txtRssiDbm[chan].Font = regularFont;
                }
            }

            return(0);
        }

        // These methods convert an RSSI measurement to an approximate signal
        // level in dBm. The bsmExtRssiToDbm() method converts an external RSSI
        // for the base station, and the imIntRssiToDbm() method converts an
        // internal RSSI for the implant. In both cases, the following formula
        // is used to do the conversion:
        //
        //     dBm = -96.0 + ((rssi - rssiForNeg96Dbm) / rssiPerDbm)
        //  
        // The components in this equation are as follows:
        // 
        // -96.0:
        //     The approximate input signal level (in dBm) represented by the
        //     RSSI-to-dBm calibration value stored in the NVM on the module
        //     (base station or implant). This is the input signal level used
        //     for the RSSI-to-dBm calibration.
        // rssi:
        //     The RSSI measurement to convert to dBm (external RSSI for base
        //     station, or internal RSSI for implant).
        // rssiForNeg96Dbm:
        //     The RSSI measured for a -96 dBm input signal, which is the
        //     RSSI-to-dBm calibration value stored in the NVM on the module
        //     (base station or implant). This calibration is performed separately
        //     for each channel, and the result for each channel is stored in the
        //     NVM on the module.
        // rssiPerDbm:
        //     The amount by which the measured RSSI changes for each 1 dBm
        //     change in the input signal level. This is a hard-coded constant
        //     that was determined by charaterizing a number of modules in the
        //     lab. All base stations use one hard-coded constant, and all
        //     implants use another hard-coded constant.
        //
        private float bsmExtRssiToDbm(int extRssi, uint chan)
        {
            int rssiForNeg96Dbm = getBsmRssiToDbmCal(chan);
            const float rssiPerDbm = 40.0F;
                
            float rssiDbm = -96.0F + ((extRssi - rssiForNeg96Dbm) / rssiPerDbm);
            
            return(rssiDbm);
        }
        private float imIntRssiToDbm(int intRssi)
        {
            int rssiForNeg96Dbm = imMicsFactNvm.rssiToDbmCal.Val;
            const float rssiPerDbm = 0.553724054F;
            
            float rssiDbm = -96.0F + ((intRssi - rssiForNeg96Dbm) / rssiPerDbm);
            
            return(rssiDbm);
        }
        
        int
        getBsmRssiToDbmCal(uint chan)
        {
            switch(chan) {
            case 0: return(bsmMicsFactNvm.rssiToDbmCalForChan0.Val);
            case 1: return(bsmMicsFactNvm.rssiToDbmCalForChan1.Val);
            case 2: return(bsmMicsFactNvm.rssiToDbmCalForChan2.Val);
            case 3: return(bsmMicsFactNvm.rssiToDbmCalForChan3.Val);
            case 4: return(bsmMicsFactNvm.rssiToDbmCalForChan4.Val);
            case 5: return(bsmMicsFactNvm.rssiToDbmCalForChan5.Val);
            case 6: return(bsmMicsFactNvm.rssiToDbmCalForChan6.Val);
            case 7: return(bsmMicsFactNvm.rssiToDbmCalForChan7.Val);
            case 8: return(bsmMicsFactNvm.rssiToDbmCalForChan8.Val);
            case 9: return(bsmMicsFactNvm.rssiToDbmCalForChan9.Val);
            default: return(0);
            }
        }

        private void btnPerformCcaOrRssi_Click(object sender, EventArgs e)
        {
            Button p = (Button)sender;
            
            if (chbContinuousCcaOrRssi.Checked) {
            
                if (p.Text.Contains("Perform")) {
                    startContinuousCcaOrRssi();
                } else {
                    stopContinuousCcaOrRssi();
                }
                
            } else {
            
                if (isImForm) {
                    performRssi(RSSI_CHAN.ALL_CHANS);
                } else {
                    performCca();
                }
            }
        }
        
        private void startContinuousCcaOrRssi()
        {
            inContinuousCcaOrRssi = true;
            
            if (isImForm) {
                btnPerformCcaOrRssi.Text = "Stop RSSI";
            } else {
                btnPerformCcaOrRssi.Text = "Stop CCA";
            }
            stateChangeContinuousCcaOrRssi();
        }
        
        private void stopContinuousCcaOrRssi()
        {
            inContinuousCcaOrRssi = false;
            
            if (isImForm) {
                btnPerformCcaOrRssi.Text = "Perform RSSI";
            } else {
                btnPerformCcaOrRssi.Text = "Perform CCA";
            }
            chbContinuousCcaOrRssi.Enabled = true;
            stateChangeIdle();
        }
        
        private void btnReadMaxOrAveRssi0_Click(object sender, EventArgs e)
        {
            performRssi(RSSI_CHAN.CHAN_0);
        }

        private void btnReadMaxOrAveRssi1_Click(object sender, EventArgs e)
        {
            performRssi(RSSI_CHAN.CHAN_1);
        }

        private void btnReadMaxOrAveRssi2_Click(object sender, EventArgs e)
        {
            performRssi(RSSI_CHAN.CHAN_2);
        }

        private void btnReadMaxOrAveRssi3_Click(object sender, EventArgs e)
        {
            performRssi(RSSI_CHAN.CHAN_3);
        }

        private void btnReadMaxOrAveRssi4_Click(object sender, EventArgs e)
        {
            performRssi(RSSI_CHAN.CHAN_4);
        }

        private void btnReadMaxOrAveRssi5_Click(object sender, EventArgs e)
        {
            performRssi(RSSI_CHAN.CHAN_5);
        }

        private void btnReadMaxOrAveRssi6_Click(object sender, EventArgs e)
        {
            performRssi(RSSI_CHAN.CHAN_6);
        }

        private void btnReadMaxOrAveRssi7_Click(object sender, EventArgs e)
        {
            performRssi(RSSI_CHAN.CHAN_7);
        }

        private void btnReadMaxOrAveRssi8_Click(object sender, EventArgs e)
        {
            performRssi(RSSI_CHAN.CHAN_8);
        }

        private void btnReadMaxOrAveRssi9_Click(object sender, EventArgs e)
        {
            performRssi(RSSI_CHAN.CHAN_9);
        }

        private void btnTestRx400Enab_Click(object sender, EventArgs e)
        {
            Button p = (Button)sender;
            string err = "";

            if (inRx400Test == true) {
            
                activitiesDisableTest(false, p, "RX", btnTestTx400Enab);

                if (adkMod.enabExtRssi(false, (uint)cmbTestRx400Chan.SelectedIndex, ref err) < 0) {
                    Util.displayError(txtSysMsg, "Failed to disable external RSSI: " + err);
                }
                inRx400Test = false;
                
            } else {
            
                activitiesDisableTest(true, p, "RX", btnTestTx400Enab);
                
                if (adkMod.enabExtRssi(true, (uint)cmbTestRx400Chan.SelectedIndex, ref err) < 0) {
                    Util.displayError(txtSysMsg, "Failed to enable external RSSI: " + err);
                }
                inRx400Test = true;
            }
        }

        private void btnTestTx400Enab_Click(object sender, EventArgs e)
        {
            Button p = (Button)sender;
            string err = "";
            
            if (inTx400Test == true) {
            
                activitiesDisableTest(false, p, "TX", btnTestRx400Enab);
                
                if (adkMod.startTx400Carrier(false, (uint)cmbTestTx400Chan.SelectedIndex, ref err) < 0) {
                    Util.displayError(txtSysMsg, "Failed to stop 400 MHz TX carrier: " + err);
                }
                inTx400Test = false;
                
            } else {
            
                activitiesDisableTest(true, p, "TX", btnTestRx400Enab);
                
                if (adkMod.startTx400Carrier(true, (uint)cmbTestTx400Chan.SelectedIndex, ref err) < 0) {
                    Util.displayError(txtSysMsg, "Failed to start 400 MHz TX carrier: " + err);
                }
                inTx400Test = true;
            }
        }

        private void chbStatPoll_CheckedChanged(object sender, EventArgs e)
        {
            if (chbStatPoll.Checked) {
            
                // force status polling to update all status displays
                forceStat = true;
                
            } else {
            
                setStatusInvalid(true);
                txtImVsup1.Text = INVALID_VALUE;
                txtImVsup2.Text = INVALID_VALUE;
                txtStatOpState.BackColor = Color.White;
                
                // Set "guiState" to "unknown" since it's not updated while the
                // status polling is disabled, and so when the status polling is
                // re-enabled, the first status poll will see that "guiState"
                // doesn't match the polled MICS state, and will therefore
                // update the display accordingly.
                //
                guiState = GUI_STATE.UNKNOWN;
            }
        }

        private void btnHighLevelCal_Click(object sender, EventArgs e)
        {
            byte REG_MAC_CTL = 0x29;
            uint REG_MAC_ANTTUNESEL1 = CommonApi.P2 | 0x17;
            uint REG_MAC_ANTTUNESEL2 = CommonApi.P2 | 0x18;
            uint REG_MAC_ANTTUNESEL3 = CommonApi.P2 | 0x19;
            uint temp_reg;
            string err = "";

            btnHighLevelCal.Enabled = false;

            anttunesel1 = (byte) (highLevelTuneMatch2Cap | highLevelTuneMatch1Cap | highLevelTuneTxCap);
            anttunesel2 = (byte) (highLevelTuneCh7 | highLevelTuneCh6 | highLevelTuneCh5 | highLevelTuneCh4 | 
                                  highLevelTuneCh3 | highLevelTuneCh2 | highLevelTuneCh1 | highLevelTuneCh0);
            anttunesel3 = (byte) (highLevelExtAlg | highLevelTuneCh9 | highLevelTuneCh8);

            // set anttunsel1 register
            temp_reg = (uint)adkMod.micsRead(REG_MAC_ANTTUNESEL1, false, ref err);
            temp_reg = temp_reg & 0xF8;
            temp_reg = temp_reg | anttunesel1;
            adkMod.micsWrite(REG_MAC_ANTTUNESEL1, (temp_reg), false, ref err);

            // set anttunesel2 register
            adkMod.micsWrite(REG_MAC_ANTTUNESEL2, (anttunesel2), false, ref err);

            // set anttunesel3 register
            temp_reg = (uint)adkMod.micsRead(REG_MAC_ANTTUNESEL3, false, ref err);
            temp_reg = temp_reg & 0xE0;
            temp_reg = temp_reg | anttunesel3;
            adkMod.micsWrite(REG_MAC_ANTTUNESEL3, (temp_reg), false, ref err);

            if (chbHighLevelCalTx.Checked) {
                calibrate(false, CommonApi.MICS_CALS.CAL_400_MHZ_TX_102);
            }

            if (chbHighLevelCalRx.Checked) {
                calibrate(false, CommonApi.MICS_CALS.CAL_400_MHZ_RX_102);
            }

            // wait for calibration to finish
            do {
                temp_reg = (uint)adkMod.micsRead(REG_MAC_CTL, false, ref err);
            } while(temp_reg != 0x00);

            btnHighLevelCal.Enabled = true;
        }

        private void txtHighLevelCalChan0_Click(object sender, EventArgs e)
        {
            // toggle the textbox value
            if (txtHighLevelCalChan0.Text == "0") {
                txtHighLevelCalChan0.Text = "1";
                highLevelTuneCh0 = TUNE_CH0;
            } else {
                txtHighLevelCalChan0.Text = "0";
                highLevelTuneCh0 = 0;
            }
        }

        private void txtHighLevelCalChan1_Click(object sender, EventArgs e)
        {
            // toggle the textbox value
            if (txtHighLevelCalChan1.Text == "0") {
                txtHighLevelCalChan1.Text = "1";
                highLevelTuneCh1 = TUNE_CH1;
            } else {
                txtHighLevelCalChan1.Text = "0";
                highLevelTuneCh1 = 0;
            }
        }

        private void txtHighLevelCalChan2_Click(object sender, EventArgs e)
        {
            // toggle the textbox value
            if (txtHighLevelCalChan2.Text == "0") {
                txtHighLevelCalChan2.Text = "1";
                highLevelTuneCh2 = TUNE_CH2;
            } else {
                txtHighLevelCalChan2.Text = "0";
                highLevelTuneCh2 = 0;
            }
        }

        private void txtHighLevelCalChan3_Click(object sender, EventArgs e)
        {
            // toggle the textbox value
            if (txtHighLevelCalChan3.Text == "0") {
                txtHighLevelCalChan3.Text = "1";
                highLevelTuneCh3 = TUNE_CH3;
            } else {
                txtHighLevelCalChan3.Text = "0";
                highLevelTuneCh3 = 0;
            }
        }

        private void txtHighLevelCalChan4_Click(object sender, EventArgs e)
        {
            // toggle the textbox value
            if (txtHighLevelCalChan4.Text == "0") {
                txtHighLevelCalChan4.Text = "1";
                highLevelTuneCh4 = TUNE_CH4;
            } else {
                txtHighLevelCalChan4.Text = "0";
                highLevelTuneCh4 = 0;
            }
        }

        private void txtHighLevelCalChan5_Click(object sender, EventArgs e)
        {
            // toggle the textbox value
            if (txtHighLevelCalChan5.Text == "0") {
                txtHighLevelCalChan5.Text = "1";
                highLevelTuneCh5 = TUNE_CH5;
            } else {
                txtHighLevelCalChan5.Text = "0";
                highLevelTuneCh5 = 0;
            }
        }

        private void txtHighLevelCalChan6_Click(object sender, EventArgs e)
        {
            // toggle the textbox value
            if (txtHighLevelCalChan6.Text == "0") {
                txtHighLevelCalChan6.Text = "1";
                highLevelTuneCh6 = TUNE_CH6;
            } else {
                txtHighLevelCalChan6.Text = "0";
                highLevelTuneCh6 = 0;
            }
        }

        private void txtHighLevelCalChan7_Click(object sender, EventArgs e)
        {
            // toggle the textbox value
            if (txtHighLevelCalChan7.Text == "0") {
                txtHighLevelCalChan7.Text = "1";
                highLevelTuneCh7 = TUNE_CH7;
            } else {
                txtHighLevelCalChan7.Text = "0";
                highLevelTuneCh7 = 0;
            }
        }

        private void txtHighLevelCalChan8_Click(object sender, EventArgs e)
        {
            // toggle the textbox value
            if (txtHighLevelCalChan8.Text == "0") {
                txtHighLevelCalChan8.Text = "1";
                highLevelTuneCh8 = TUNE_CH8;
            } else {
                txtHighLevelCalChan8.Text = "0";
                highLevelTuneCh8 = 0;
            }
        }

        private void txtHighLevelCalChan9_Click(object sender, EventArgs e)
        {
            // toggle the textbox value
            if (txtHighLevelCalChan9.Text == "0") {
                txtHighLevelCalChan9.Text = "1";
                highLevelTuneCh9 = TUNE_CH9;
            } else {
                txtHighLevelCalChan9.Text = "0";
                highLevelTuneCh9 = 0;
            }
        }

        private void chbMatch1TuneCap_CheckedChanged(object sender, EventArgs e)
        {
            if (chbMatch1TuneCap.Checked) {
                highLevelTuneMatch1Cap = MATCH1_TUNE_CAP;
            } else {
                highLevelTuneMatch1Cap = 0;
            }
        }

        private void chbMatch2TuneCap_CheckedChanged(object sender, EventArgs e)
        {
            if (chbMatch2TuneCap.Checked) {
                highLevelTuneMatch2Cap = MATCH2_TUNE_CAP;
            } else {
                highLevelTuneMatch2Cap = 0;
            }
        }

        private void chbTxTuneCap_CheckedChanged(object sender, EventArgs e)
        {
            if (chbTxTuneCap.Checked) {
                highLevelTuneTxCap = TX_TUNE_CAP;
            } else {
                highLevelTuneTxCap = 0;
            }    
        }

        private void chbHighLevelCalExtAlg_CheckedChanged(object sender, EventArgs e)
        {
            if (chbHighLevelCalExtAlg.Checked) {
                highLevelExtAlg = 0x1C;
            } else {
                highLevelExtAlg = 0;
            }
        }

        private void btn24CrystOsc_Click(object sender, EventArgs e)
        {
            calibrate(false, CommonApi.MICS_CALS.CAL_24_MHZ_CRYSTAL_OSC);
        }

        private void btnTestTx245Enab_Click(object sender, EventArgs e)
        {
            Button p = (Button)sender;
            string err = "";

            if (inTx245Test == true) {
            
                activitiesDisableTest(false, p, "TX", null);
                
                if (bsmMod.startTx245Carrier(false, ref err) < 0) {
                    Util.displayError(txtSysMsg, "Failed to stop 2.45 GHz TX carrier: " + err);
                }
                inTx245Test = false;
                
            } else {
            
                activitiesDisableTest(true, p, "TX", null);
                
                if (bsmMod.startTx245Carrier(true, ref err) < 0) {
                    Util.displayError(txtSysMsg, "Failed to start 2.45 GHz TX carrier: " + err);
                }
                inTx245Test = true;
            }
        }

        private void tmrBackgroundTasks_Tick(object sender, EventArgs e)
        {
            string err = "";

            backgroundTaskTimerCount++;

            if (inContinuousCcaOrRssi == true) {
            
                // if it's time to do CCA/RSSI, do so
                if ((backgroundTaskTimerCount % ccaIntervalCount) == 0) {
                
                    if (isImForm) {
                    
                        if (performRssi(RSSI_CHAN.ALL_CHANS) < 0) {
                        
                            // stop continuous RSSI so it won't keep failing
                            stopContinuousCcaOrRssi();
                            Util.displayError(txtSysMsg, "Continuous RSSI stopped due to error.");
                        }
                        
                    } else {
                    
                        if (performCca() < 0) {
                        
                            // stop continuous CCA so it won't keep failing
                            stopContinuousCcaOrRssi();
                            Util.displayError(txtSysMsg, "Continuous CCA stopped due to error.");
                        }
                    }
                }
            }

            // If searching for implants or listening for emergency messages from
            // implants, poll the base station for any received IMD TIDs.
            //
            if (inSearchForImplant || inListenForEmer) {
            
                // if it's time to poll the base for any received IMD TIDs, do so
                if ((backgroundTaskTimerCount % imResponsePollIntervalCount) == 0) {
                
                    int len = bsmMod.micsReceive(rxBuf, RX_BUF_SIZE, ref err);
                    if (len < 0) {
                    
                        if (inSearchForImplant) {
                            stopSearchForImplant();
                            Util.displayError(txtSysMsg, "Stopped implant search due to error: " + err);
                        }
                        if (inListenForEmer) {
                            stopListenForEmer();
                            Util.displayError(txtSysMsg, "Stopped listening for emergency due to error: " + err);
                        }
                        
                    } else {
                    
                        // add each IMD TID in the RX buf to the list of received IMD TIDs
                        for(int i = 0; i < len;) {
                        
                            // If the last IMD TID in the RX buffer is < 3 bytes, it's invalid,
                            // so don't process it (shouldn't happen).
                            //
                            if ((len - i) < 3) break;
                            
                            // get the next IMD TID from the RX buf
                            uint rxImdTid = (uint)((rxBuf[i++] << 16) + (rxBuf[i++] << 8) + (rxBuf[i++]));
                            
                            // Search for the IMD TID in the list of received IMD TIDs, and if
                            // it's already in the list, just increment its count.
                            //
                            bool alreadyInList = false;
                            for(int j = 0; j < numEntriesInRxImdTidList; ++j) {
                            
                                if (rxImdTidList[j].imdTid == rxImdTid) {
                                
                                    alreadyInList = true; 
                                    rxImdTidList[j].count++;
                                    break;
                                }
                            }
                            // if the IMD TID is not already in the list, add it
                            if (!alreadyInList) {
                            
                                if (numEntriesInRxImdTidList < MAX_NUM_ENTRIES_IN_RX_IMD_TID_LIST) {
                                
                                    rxImdTidList[numEntriesInRxImdTidList].imdTid = rxImdTid;
                                    rxImdTidList[numEntriesInRxImdTidList].count = 1;
                                    ++numEntriesInRxImdTidList;
                                }
                            }
                        }
                        
                        // clear the display for the list of received IMD TIDs
                        dgvRxImdTidList.Rows.Clear();
                        
                        // for each entry in the list of recevied IMD TIDs, display the entry
                        for(int i = 0; i < numEntriesInRxImdTidList; ++i) {
                        
                            // get the received IMD TID and its RX count
                            string rxImdTid = rxImdTidList[i].imdTid.ToString("X6");
                            string rxImdTidCount = rxImdTidList[i].count.ToString();
                            
                            // get the company ID specified in MICS configuration
                            string cfgCompanyId = adkMod.CfgCompanyId.ToString("X2");
                            
                            // search the implant info for a matching entry
                            bool foundImInfo = false;
                            for(int j = 0; j < (dgvImInfo.Rows.Count - 1); ++j) {
                            
                                DataGridViewRow imInfoRow = dgvImInfo.Rows[j];
                                
                                string imInfoCompanyId = imInfoRow.Cells[0].Value.ToString();
                                string imInfoImdTid = imInfoRow.Cells[1].Value.ToString();
                                string imInfoCompanyName = imInfoRow.Cells[2].Value.ToString();
                                string imInfoImplantDescription = imInfoRow.Cells[3].Value.ToString();
                                
                                // If the IMD TID in the implant info matches the received IMD TID,
                                // and the company ID in the implant info matches the company ID in
                                // the MICS configuration for the base station (that is, the base
                                // station's company ID), then display the implant info together with
                                // the received IMD TID and its RX count. This checks the company ID
                                // so the GUI displays the implant info only if the company ID in the
                                // implant info matches the company ID for the implant that sent the
                                // wake-up response / emergency message. Since there's no way to obtain
                                // the company ID from the implant's wake-up responses / emergency
                                // messages, it's assumed the implant's company ID is the same as the
                                // base station's company ID for the following reasons:
                                //
                                // - If the base station and implant both have a ZL70102/103, then 
                                //   the implant's ZL70102/103 includes its company ID in its wake-up
                                //   responses / emergency messages, and the base station's ZL70102/103
                                //   only receives wake-up responses / emergency messages that contain
                                //   the base station's company ID. Thus, if the base station receives
                                //   a wake-up response / emergency message from the implant, the implant
                                //   must have the same company ID as the base station.
                                //
                                // - If either the base station or the implant has a ZL70101, then the
                                //   ZL70101/102/103 on the base station will receive wake-up responses /
                                //   emergency messages from the implant even if the implant has a different
                                //   company ID than the base station. However, this is very unlikely to
                                //   occur for the following reasons. First, when the base station is
                                //   searching for implant(s), it sends the company ID in the wake-up signal,
                                //   and only implants with that company ID respond to the wake-up signal.
                                //   Second, when the base station is listening for emergency messages, it's
                                //   very unlikely that an implant for a different company will be in range,
                                //   have the same IMD TID, and send an emergency message all at the same time.
                                //
                                if (imInfoImdTid.Contains(rxImdTid) && (imInfoCompanyId == cfgCompanyId)) {
                                
                                    string[] displayStrings = new string[] { imInfoImdTid, rxImdTidCount,
                                        imInfoCompanyName, imInfoImplantDescription};
                                        
                                    dgvRxImdTidList.Rows.Add(displayStrings);
                                    
                                    // indicate we found the implant info
                                    foundImInfo = true;
                                    
                                    // stop searching for implant info
                                    break;
                                }
                            }
                            
                            // if we didn't find the implant info, display the received IMD TID and its RX count
                            if (!foundImInfo) {
                            
                                string[] displayStrings = new string[] { rxImdTid, rxImdTidCount, "********", "********" };
                                dgvRxImdTidList.Rows.Add(displayStrings);
                            }
                        }
                    }
                }
            }

            if (inSession) {
            
                // If data gathering is enabled and a data test isn't active,
                // poll for any received data. 
                //
                if ((chbDataGat.Checked == true) && (!inDataTest)) {
                
                    // if it's time to poll for any received data, do so
                    if ((backgroundTaskTimerCount % rxDataPollIntervalCount) == 0) {
                    
                        int len = adkMod.micsReceive(rxBuf, RX_BUF_SIZE, ref err);
                        if (len < 0) {
                        
                            Util.displayError(txtSysMsg, "Data gather disabled due to error: " + err);
                            chbDataGat.Checked = false;
                            
                        } else if (len > 0) {
                        
                            int charsPerBlock;
                            string text;
                            
                            // Convert the received data to a text string. Note this references
                            // this.blockSizeFromStat, which is updated by the status polling.
                            // At this point, the status polling must be active, because this
                            // is only invoked if "inSession" is true, and only the status
                            // polling sets "inSession".
                            //
                            if (radReceiveDataHex.Checked) {
                                charsPerBlock = this.blockSizeFromStat * 2;
                                text = convertBinBytesToStrOfHexChars(rxBuf, len);
                            } else {
                                charsPerBlock = this.blockSizeFromStat;
                                text = Util.convertAsciiBytesToStr(rxBuf, 0, len);
                            }

                            // if desired, write data to file
                            if (streamingData) {
                                sw.WriteLine(text);
                            } else {
                            
                                // display each block of data on a separate line
                                for(int i = 0; i < text.Length; i += charsPerBlock) {
                                
                                    // if last block is too short, prepare to display a shorter line
                                    if ((i + charsPerBlock) >= text.Length) charsPerBlock = text.Length - i;
                                    
                                    // display next data block (terminated with newline)
                                    txtReceiveData.Text += text.Substring(i, charsPerBlock) + Environment.NewLine;
                                }
                            }
                        }
                    }
                }

                if (inTransmitData) {
                
                    // if it's time to transmit data, do so
                    if ((backgroundTaskTimerCount % txDataIntervalCount) == 0) {
                    
                        unsafe {
                        
                            fixed(byte* fTxBuf = txBuf) {
                            
                                if (adkMod.micsTransmit(fTxBuf, (uint)txLen, ref err) < 0) {
                                
                                    btnTransmitData.Text = "Transmit Data";
                                    chbTransmitDataForever.Enabled = true;
                                    inTransmitData = false;
                                    Util.displayError(txtSysMsg, "Data transmission stopped due to error: " + err);
                                }
                            }
                        }
                        if (chbTransmitDataForever.Checked == false) {
                            inTransmitData = false;
                            chbTransmitDataForever.Enabled = true;
                        }
                    }
                }
            }
        }
        
        // This converts the binary data bytes in the specified byte array (binBytes)
        // into a string of hexadecimal characters. Each binary data byte in the byte
        // array is converted into two hexadecimal characters in the resulting string.
        // The number of binary data bytes to convert is specified by "nBytesToConvert".
        // This returns the resulting string of hexadecimal characters.
        //
        private string convertBinBytesToStrOfHexChars(byte[] binBytes, int nBytesToConvert)
        {
            string strOfHexChars = "";
            
            for(int i = 0; i < nBytesToConvert; ++i) {
                strOfHexChars += binBytes[i].ToString("X2");
            }
            return(strOfHexChars);
        }

        private void calibrate(bool remote, CommonApi.MICS_CALS cal)
        {
            string err = "";

            if (cal == CommonApi.MICS_CALS.CAL_24_MHZ_CRYSTAL_OSC) {
            
                if (calReqOn == CAL_REQ_STATE.TX_400_OFF) {
                
                    calReqRemote = remote;
                    lblCalReq.Text = "Turn on 402.15 MHz carrier.";
                    lblCalReq.Visible = true;
                    btnCalReq.Visible = true;
                    calReqOn = CAL_REQ_STATE.WAIT_TX_400_ON;
                    return;
                    
                } else if (calReqOn != CAL_REQ_STATE.TX_400_ON) {
                    return;
                }
            }

            // If the calibration fails, report the error. Otherwise, if it was
            // performed on an implant (local or remote), copy registers to the
            // wakeup stack in the ZL7010X on the implant. That way, if the
            // calibration affected any registers the ZL7010X includes in its
            // wakeup stack, the ZL7010X will preserve them while it's sleeping.
            //
            if (adkMod.micsCal(cal, -1, remote, ref err) < 0) {
            
                Util.displayError(txtSysMsg, "Failed calibration: " + err);
                    
            } else if (isImForm || remote) {
                
                if (adkMod.copyMicsRegs(remote, ref err) < 0) {
                    Util.displayError(txtSysMsg, "Failed to copy registers: " + err);
                }
            }
        }
        
        private void cmbTestTx400Chan_SelectedIndexChanged(object sender, EventArgs e)
        {
            string err = "";
            
            // if in test, stop test and restart with new channel
            if (inTx400Test == true) {
            
                if (adkMod.startTx400Carrier(true, (uint)cmbTestTx400Chan.SelectedIndex, ref err) < 0) {
                    Util.displayError(txtSysMsg, "Failed to start 400 MHz TX carrier: " + err);
                }
            }
        }

        private void cmbTestTx245Chan_SelectedIndexChanged(object sender, EventArgs e)
        {
            string err = "";
            uint freqMHz, ccFreq;
            
            // calc frequency (in MHz) corresponding to selected index
            freqMHz = 2405 + ((uint)cmbTestTx245Chan.SelectedIndex * 5);
            
            // Calculate the corresponding CC25XX frequency control value. Note
            // that the divisor (26 or 24) is the CC25XX crystal frequency in
            // MHz, which is different for the BSM100 and BSM200/300.
            //
            if (mezzModel == "BSM100") {
                ccFreq = (freqMHz * 65536) / 26;
            } else { // BSM200 or BSM300  
                ccFreq = (freqMHz * 65536) / 24;
            }

            if (bsmMod.setTx245Freq(ccFreq, ref err) < 0) {
                Util.displayError(txtSysMsg, "Failed to set 2.45 GHz TX frequency: " + err);
            }

            // update TX power setting (may be needed when the frequency is changed)
            setTx245Power((uint)cmbTestTx245Chan.SelectedIndex, (uint)cmbTestTx245Pow.SelectedIndex);
        }
        
        private void cmbTestTx245Pow_SelectedIndexChanged(object sender, EventArgs e)
        {
            setTx245Power((uint)cmbTestTx245Chan.SelectedIndex, (uint)cmbTestTx245Pow.SelectedIndex);
        }
        
        private void setTx245Power(uint freqSelect, uint powSelect)
        {
            string err = ""; 
            byte ccPa;
            
            // if BSM300 (has CC2500)
            if (mezzModel == "BSM300") {
            
                // PA setting depends on frequency */
                if (freqSelect > 11) {
                    ccPa = cc2500PaValsForHighFreqBand[powSelect];
                } else if (freqSelect < 6) {
                    ccPa = cc2500PaValsForLowFreqBand[powSelect];
                } else {
                    ccPa = cc2500PaValsForMidFreqBand[powSelect];
                }
                
            } else { /* BSM200 or BSM100 (has CC2550) */

                // PA setting is independent of frequency
                ccPa = cc2550PaVals[powSelect];
            }
            
            if (bsmMod.setTx245Power(ccPa, ref err) < 0) {
                Util.displayError(txtSysMsg, "Failed to set the 2.45 GHZ TX power: " + err);
            }
        }

        private void cmbTestRx400Chan_SelectedIndexChanged(object sender, EventArgs e)
        {
            string err = "";
            
            if (inRx400Test == true) {
            
                if (adkMod.enabExtRssi(true, (uint)cmbTestRx400Chan.SelectedIndex, ref err) < 0) {
                    Util.displayError(txtSysMsg, "Failed to enable external RSSI: " + err);
                }
            }
        }
        
        private void btnWakeupBsm_Click(object sender, EventArgs e)
        {
            Button p = (Button)sender;
            string err = "";
            
            if (p.Text.Contains("Wake-up")) {
            
                if (bsmMod.wakeup(ref err) < 0) {
                    Util.displayError(txtSysMsg, "Failed to wake base station: " + err);
                    return;
                }
                activitiesDisableAll(GUI_STATE.UNKNOWN);
                stateChangeIdle();
                p.Text = "Sleep";
                
            } else {
            
                if (bsmMod.sleep(ref err) < 0) {
                    Util.displayError(txtSysMsg, "Failed to put base station to sleep: " + err);
                    return;
                }
                activitiesDisableAll(GUI_STATE.UNKNOWN);
                stateChangeSleep();
                p.Text = "Direct Wake-up";
            }
        }

        private void btnWakeupIm_Click(object sender, EventArgs e)
        {
            Button p = (Button)sender;
            string err = "";
            
            if (p.Text.Contains("Wake-up")) {
            
                if (imMod.wakeup(true, ref err) < 0) {
                    Util.displayError(txtSysMsg, "Failed to wake implant: " + err);
                    return;
                }
                activitiesDisableAll(GUI_STATE.UNKNOWN);
                stateChangeIdle();
                p.Text = "Sleep";
                
            } else {
            
                if (imMod.sleep(ref err) < 0) {
                    Util.displayError(txtSysMsg, "Failed to put implant to sleep: " + err);
                    return;
                }
                activitiesDisableAll(GUI_STATE.UNKNOWN);
                stateChangeSleep();
                p.Text = "Direct Wake-up";
            }
        }

        private void btnSendEmer_Click(object sender, EventArgs e)
        {
            Button p = (Button)sender;
            string err = "";
            
            if (!inSendEmer) {
            
                if (imMod.sendEmergency(ref err) < 0) {
                    Util.displayError(txtSysMsg, "Failed to start sending emergency: " + err);
                } else {
                    activitiesDisableAll(GUI_STATE.SEND_EMER);
                    activitiesDisableSendEmer(true);
                    stateChangeSendEmer();
                    inSendEmer = true;
                }
                
            } else {
            
                if (adkMod.micsAbort(ref err) < 0) {
                    Util.displayError(txtSysMsg, "Failed to stop sending emergency: " + err);
                } else {
                    activitiesDisableSendEmer(false);
                    stateChangeSleep();
                    inSendEmer = false;
                }
            }
        }

        private void btnReceiveDataClear_Click(object sender, EventArgs e)
        {
            txtReceiveData.Text = "";
        }

        private void AnyModForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            string err = "";
            
            inSession = false;
            inSendEmer = false;
            inSearchForImplant = false;
            inListenForEmer = false;
            inRx400Test = false;
            inTx400Test = false;
            inTx245Test = false;
            inContinuousCcaOrRssi = false;
            forceStat = false;

            // Close any API/DLL interfaces that were opened by the ADK module
            // interface. This frees any resources allocated by the API
            // functions, and closes any board connections so they're
            // available to be re-opened later if desired.
            //
            if (isImForm) {
            
                // if connected to implant, close it */
                if (imMod != null) imMod.close(ref err);
                
            } else {
            
                // if connected to base station, close it */
                if (bsmMod != null) bsmMod.close(ref err);
                
                // if connected to local implant via USB, close it
                if (imMod != null) imMod.close(ref err);
            }
        }

        public void setStatPollRate(int selectedIndex)
        {
            tmrStatPoll.Interval = convertRate(selectedIndex);
        }
        
        public void setCcaRate(int selectedIndex)
        {
            ccaIntervalCount = convertRate(selectedIndex) / BACKGROUND_TASK_INTERVAL;
        }
        
        public void setImResponsePollRate(int selectedIndex)
        {
            imResponsePollIntervalCount = convertRate(selectedIndex) / BACKGROUND_TASK_INTERVAL;
        }

        private void radTransmitDataAscii_CheckedChanged(object sender, EventArgs e)
        {
            if (ignoreTransmitChangedEvent) {
                ignoreTransmitChangedEvent = false;
                return;
            }

            if (radTransmitDataAscii.Checked) {
                radTransmitDataHex.Checked = false;
            } else {
                radTransmitDataHex.Checked = true;
            }
            
            txtTransmitData.Text = "";
            ignoreTransmitChangedEvent = true;
        }

        private void radTransmitDataHex_CheckedChanged(object sender, EventArgs e)
        {
            if (ignoreTransmitChangedEvent) {
                ignoreTransmitChangedEvent = false;
                return;
            }

            if (radTransmitDataHex.Checked) {
                radTransmitDataAscii.Checked = false;
            } else {
                radTransmitDataAscii.Checked = true;
            }
            
            txtTransmitData.Text = "";
            ignoreTransmitChangedEvent = true;
        }

        private void radReceiveDataHex_CheckedChanged(object sender, EventArgs e)
        {
            if (ignoreRcvChangedEvent) {
                ignoreRcvChangedEvent = false;
                return;
            }

            if (radReceiveDataHex.Checked) {
                radReceiveDataAscii.Checked = false;
            } else {
                radReceiveDataAscii.Checked = true;
            }
            
            txtReceiveData.Text = "";
            ignoreRcvChangedEvent = true;
        }

        private void radReceiveDataAscii_CheckedChanged(object sender, EventArgs e)
        {
            if (ignoreRcvChangedEvent) {
                ignoreRcvChangedEvent = false;
                return;
            }

            if (radReceiveDataAscii.Checked) {
                radReceiveDataHex.Checked = false;
            } else {
                radReceiveDataHex.Checked = true;
            }
            
            txtReceiveData.Text = "";
            ignoreRcvChangedEvent = true;
        }

        private void txtHex_KeyDown(object sender, KeyEventArgs e)
        {
            isInvalidKey = Util.isInvalidHexKey(e, true);
        }

        private void txtHex_KeyPress(object sender, KeyPressEventArgs e)
        {
            // check for the flag being set in the KeyDown event
            if (isInvalidKey == true) {
            
                // stop the character from being entered into the control since it is non-numerical
                e.Handled = true;
                isInvalidKey = false;
            }
        }

        private void txtTransmitData_KeyDown(object sender, KeyEventArgs e)
        {
            isInvalidKey = false;

            if (radTransmitDataHex.Checked) {
                isInvalidKey = Util.isInvalidHexKey(e, true);
            }
        }

        // This event occurs after the KeyDown event and can be used to prevent
        // characters from entering the control.
        //
        private void txtTransmitData_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (radTransmitDataHex.Checked) {
            
                // check for the flag being set in the KeyDown event
                if (isInvalidKey == true) {
                
                    // stop the character from being entered into the control since it is non-numerical
                    e.Handled = true;
                    isInvalidKey = false;
                }
            }
        }
        
        private void txtTestTx400Pow_TextChanged(object sender, EventArgs e)
        {
            TextBox p = (TextBox)sender;
            string valStr = p.Text;
            
            if (valStr.Contains("*")) return;

            if (ignoreChangedEvent) {
                ignoreChangedEvent = false;
                return;
            }
            
            if (valStr == "") {
                ignoreChangedEvent = true;
                valStr = "0";
            }

            if (valStr.Contains("0x")) {
                valStr = valStr.Replace("0x", "0");
            }
            if (valStr.Contains("0X")) {
                valStr = valStr.Replace("0X", "0");
            }

            if ((uint)Int32.Parse(valStr, NumberStyles.HexNumber) > 0x0ff) {
                p.Text = "0x0";
            }

            if (valStr.Contains("abc")) {
            
                bsmTabs.TabPages.Add(tabFactoryUse);
                
                // if base station GUI
                if (!isImForm) {
                
                    grpCcReg.Visible = true;
                    btnNonvolatileMemory.Visible = true;
                    grp245Cal.Visible = false;
                }
                
            } else if (valStr.Contains("cba")) {
            
                bsmTabs.TabPages.Remove(tabFactoryUse);
            }
        }

        private void btnTestTx400ReadPow_Click(object sender, EventArgs e)
        {
            int val = 0;
            string err = "";

            if ((val = adkMod.micsRead(99, false, ref err)) < 0) {
                Util.displayError(txtSysMsg, "Failed to read ZL7010X register: " + err);
            } else {
                txtTestTx400Pow.Text = "0x" + val.ToString("X2");
            }
        }

        private void btnTestRx400ReadLna_Click(object sender, EventArgs e)
        {
            int val = 0;
            string err = "";
            
            if ((val = adkMod.micsRead(104, false, ref err)) < 0) {
                Util.displayError(txtSysMsg, "Failed to read ZL7010X register: " + err);
            } else {
                txtTestRx400Lna.Text = "0x" + val.ToString("X2");
            }
        }

        private void chbEnabHkWrite_CheckedChanged(object sender, EventArgs e)
        {
            imMod.CfgEnabHkWrite = ((CheckBox)sender).Checked;
        }

        private void ignoreEntry_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        // This function assumes the following rate selections:
        // 
        //     0.5
        //     1
        //     2
        //     5
        //     10
        //
        private int convertRate(int selectedIndex)
        {
            int interval;
            
            switch (selectedIndex) {
            case 0:
                interval = 2000;  // in ms
                break;
            case 1:
                interval = 1000;  // in ms
                break;
            case 2:
                interval = 500;  // in ms
                break;
            case 3:
                interval = 200;  // in ms
                break;
            case 4:
                interval = 100;  // in ms
                break;
            default:
                interval = 100; // in ms
                break;
            }
            return(interval);
        }

        private void cmbTransmitDataRate_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox p = (ComboBox)sender;
            txDataIntervalCount = convertRate(p.SelectedIndex) / BACKGROUND_TASK_INTERVAL;
        }

        private void cmbReceiveDataPollRate_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox p = (ComboBox)sender;
            rxDataPollIntervalCount = convertRate(p.SelectedIndex) / BACKGROUND_TASK_INTERVAL;
        }

        // base station GUI only
        private void cmbWake245UserData_SelectedIndexChanged(object sender, EventArgs e)
        {
            bsmMod.CfgWake245UserData = (byte)((ComboBox)sender).SelectedIndex;
        }

        private void cmbImplantType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox p = (ComboBox)sender;
            
            // Update GUI components that are affected by the new selection.
            // Note that this is done even if the initialization isn't complete.
            //
            switch(p.SelectedIndex) {
            case 0:
                btnImRegsOnBsm.Visible = false;
                grpRemHighLevelTuning.Visible = false;
                break;
            case 1:
                btnImRegsOnBsm.Text = "ZL70101 Registers";
                btnImRegsOnBsm.Visible = true;
                grpRemHighLevelTuning.Visible = false;
                break;
            case 2:
                btnImRegsOnBsm.Text = "ZL70102 Registers";
                btnImRegsOnBsm.Visible = true;
                grpRemHighLevelTuning.Visible = true;
                break;
            case 3:
                btnImRegsOnBsm.Text = "ZL70103 Registers";
                btnImRegsOnBsm.Visible = true;
                grpRemHighLevelTuning.Visible = true;
                break;
            default:
                btnImRegsOnBsm.Text = "ZL70103 Registers";
                btnImRegsOnBsm.Visible = true;
                grpRemHighLevelTuning.Visible = true;
                break;
            }
            
            // if selected implant type is <= ZL70102
            if (p.SelectedIndex <= 2) {
            
                // if a Barker mode is selected, report error and undo implant type selection
                CommonApi.MOD nTxMod = (CommonApi.MOD)cmbNormTxMod.SelectedIndex;
                CommonApi.MOD nRxMod = (CommonApi.MOD)cmbNormRxMod.SelectedIndex;
                CommonApi.MOD eTxMod = (CommonApi.MOD)cmbEmerTxMod.SelectedIndex;
                CommonApi.MOD eRxMod = (CommonApi.MOD)cmbEmerRxMod.SelectedIndex;
                //     
                if ((nTxMod == CommonApi.MOD.MOD_BARKER_5) || (nTxMod == CommonApi.MOD.MOD_BARKER_11) ||
                    (nRxMod == CommonApi.MOD.MOD_BARKER_5) || (nRxMod == CommonApi.MOD.MOD_BARKER_11) ||
                    (eTxMod == CommonApi.MOD.MOD_BARKER_5) || (eTxMod == CommonApi.MOD.MOD_BARKER_11) ||
                    (eRxMod == CommonApi.MOD.MOD_BARKER_5) || (eRxMod == CommonApi.MOD.MOD_BARKER_11)) {
                
                    MessageBox.Show("A Barker mode is currently selected, which is not supported by ZL70100/101/102 implants.");
                    p.SelectedIndex = 3;  // select ZL70103 instead
                    return;
                }
            }
            
            // set implant type in ADK module config (also sends config to board)
            switch(p.SelectedIndex) {
            case 0:
                bsmMod.CfgMicsRevOnImplant = CommonApi.MICS_REV.ZL70100; break;
            case 1:
                bsmMod.CfgMicsRevOnImplant = CommonApi.MICS_REV.ZL70101; break;
            case 2:
                bsmMod.CfgMicsRevOnImplant = CommonApi.MICS_REV.ZL70102; break;
            case 3:
                bsmMod.CfgMicsRevOnImplant = CommonApi.MICS_REV.ZL70103; break;
            default:
                bsmMod.CfgMicsRevOnImplant = CommonApi.MICS_REV.ZL70103; break;
            }
            // Update state affected by new selection. Note this must be called after
            // updating bsmMod.CfgMicsRevOnImplant because this references it.
            //
            stateChange400MHzWakeup(rad400MHzWakeup.Checked);
        }

        private void btnTestTx400WritePow_Click(object sender, EventArgs e)
        {
            int result = 0;
            string err = "";
            uint val = 0;
            
            string valStr = txtTestTx400Pow.Text;
            if (valStr.Contains("*")) return;
            if (valStr == "") return;
            if (valStr.Contains("0x")) {
                valStr = valStr.Replace("0x", "0");
            }

            val = (uint)Int32.Parse(valStr, NumberStyles.HexNumber);
            
            if ((result = adkMod.micsWrite((uint)99, val, false, ref err)) < 0) {
                Util.displayError(txtSysMsg, "Failed to write to ZL7010X register: " + err);
                txtTestTx400Pow.Text = INVALID_VALUE;
            } else {
                txtTestTx400Pow.Text = "0x" + val.ToString("X2");
            }
        }

        private void btnTestRx400WriteLna_Click(object sender, EventArgs e)
        {
            int result = 0;
            string err = "";
            uint val = 0;
            
            string valStr = txtTestRx400Lna.Text;
            if (valStr.Contains("*")) return;
            if (valStr == "") return;
            if (valStr.Contains("0x")) {
                valStr = valStr.Replace("0x", "0");
            }

            val = (uint)Int32.Parse(valStr, NumberStyles.HexNumber);
            
            if ((result = adkMod.micsWrite((uint)104, val, false, ref err)) < 0) {
                Util.displayError(txtSysMsg, "Failed to write to ZL7010X register: " + err);
                txtTestRx400Lna.Text = INVALID_VALUE;
            } else {
                txtTestRx400Lna.Text = "0x" + val.ToString("X2");
            }
        }

        private void btnClearSysMsg_Click(object sender, EventArgs e)
        {
            txtSysMsg.Clear();
        }

        private void txtTestRx400Lna_TextChanged(object sender, EventArgs e)
        {
            TextBox p = (TextBox)sender;
            string valStr = p.Text;
            
            if (valStr.Contains("*")) return;

            if (ignoreChangedEvent) {
                ignoreChangedEvent = false;
                return;
            }
            if (valStr == "") {
                ignoreChangedEvent = true;
                valStr = "0";
            }

            if (valStr.Contains("0x")) {
                valStr = valStr.Replace("0x", "0");
            }
            if (valStr.Contains("0X")) {
                valStr = valStr.Replace("0X", "0");
            }

            if ((uint)Int32.Parse(valStr, NumberStyles.HexNumber) > 0x0FF) {
                p.Text = "0x0";
            }
        }

        private void cmbEmerBytesPerBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            adkMod.CfgEmerBlockSize = (byte)(((ComboBox)sender).SelectedIndex + 2);
        }

        private void cmbEmerMaxBlocksPerPack_SelectedIndexChanged(object sender, EventArgs e)
        {
            adkMod.CfgEmerMaxBlocksPerTxPack = (byte)(((ComboBox)sender).SelectedIndex + 1);
        }

        private void btnCalReq_Click(object sender, EventArgs e)
        {
            switch (calReqOn) {
            case CAL_REQ_STATE.TX_400_OFF:
                break;
            case CAL_REQ_STATE.WAIT_TX_400_ON:
                calReqOn = CAL_REQ_STATE.TX_400_ON;
                calibrate(calReqRemote, CommonApi.MICS_CALS.CAL_24_MHZ_CRYSTAL_OSC);
                lblCalReq.Text = "Turn off 402.15 MHz carrier.";
                break;
            case CAL_REQ_STATE.TX_400_ON:
                lblCalReq.Visible = false;
                btnCalReq.Visible = false;
                calReqOn = CAL_REQ_STATE.TX_400_OFF;
                break;
            }
        }

        private void btnStoreImInfo_Click(object sender, EventArgs e)
        {
            for(int i = 0; i < dgvImInfo.Rows.Count - 1; i++) {

                DataGridViewRow imInfoRow = dgvImInfo.Rows[i];
                
                string imInfoCompanyId = imInfoRow.Cells[0].Value.ToString();
                string imInfoImdTid = imInfoRow.Cells[1].Value.ToString();

                // add '0's to front of company ID to make it 2 chars
                for(int j = imInfoCompanyId.Length; j < 2; ++j) {
                    imInfoCompanyId = "0" + imInfoCompanyId;
                }
                imInfoRow.Cells[0].Value = imInfoCompanyId;

                // add '0's to front of IMD transceiver ID to make it 6 chars
                for(int j = imInfoImdTid.Length; j < 6; ++j) {
                    imInfoImdTid = "0" + imInfoImdTid;
                }    
                imInfoRow.Cells[1].Value = imInfoImdTid;
            }
            
            imInfoDataSet.WriteXml(".\\ImdDatabase.xml", XmlWriteMode.IgnoreSchema);
        }

        private void rad400MHzWakeup_CheckedChanged(object sender, EventArgs e)
        {
            // if selected 400 MHz wake-up mode
            if (rad400MHzWakeup.Checked) {
            
                // if a Barker mode is selected, report error and undo 400 MHz wakeup selection
                CommonApi.MOD txMod = (CommonApi.MOD)cmbNormTxMod.SelectedIndex;
                CommonApi.MOD rxMod = (CommonApi.MOD)cmbNormRxMod.SelectedIndex;
                //
                if ((txMod == CommonApi.MOD.MOD_BARKER_5) || (txMod == CommonApi.MOD.MOD_BARKER_11) ||
                    (rxMod == CommonApi.MOD.MOD_BARKER_5) || (rxMod == CommonApi.MOD.MOD_BARKER_11)) {
                
                    MessageBox.Show("A Barker mode is currently selected for normal operation, which 400 MHz wake-up does not support.");
                    rad245GHzWakeup.Checked = true;
                    return;
                }
            }
            
            // set wake-up mode in ADK module config (also sends config to board)
            adkMod.Cfg400MHzWakeup = rad400MHzWakeup.Checked;
            
            // update state affected by new selection
            stateChange400MHzWakeup(rad400MHzWakeup.Checked);
        }
        
        private void chbExtStrobeFor245Sniff_CheckedChanged(object sender, EventArgs e)
        {
            imMod.CfgExtStrobeFor245GHzSniff = ((CheckBox)sender).Checked;
        }

        private void btnLoadImInfo_Click(object sender, EventArgs e)
        {
            loadImInfo();
        }

        private void loadImInfo()
        {
            imInfoDataSet = new DataSet("Implant Information");
            imInfoDataSet.ReadXml(".\\ImdDatabase.xml", XmlReadMode.Auto);
            imInfoDataSet.Locale = System.Globalization.CultureInfo.CurrentUICulture;
            dgvImInfo.AutoGenerateColumns = false;
            dgvImInfo.DataSource = imInfoDataSet;
            dgvImInfo.DataMember = "ImdInfo";
            dgvImInfoRowCount = dgvImInfo.Rows.Count - 1;
        }

        private void udSetVsup1_ValueChanged(object sender, EventArgs e)
        {
            uint temp = 0;
            temp = (uint)(udSetVsup1.Value * 1000);
            
            if (chbVsupTrack.Checked) {
                adkMod.CfgVsup2Mv = (ushort)temp;
                adkMod.CfgVsup1Mv = (ushort)temp;
                return;
            }
            
            if (temp < adkMod.CfgVsup2Mv - 100) { // added 100 to leave some margin
                Util.displayError(txtSysMsg, "Error: VSUP2 must be less than or equal to VSUP1 plus 0.1V");
                udSetVsup1.Value = prevVsup1;
            } else {
                adkMod.CfgVsup1Mv = (ushort)temp;
                prevVsup1 = udSetVsup1.Value;
            }
        }

        private void udSetVsup2_ValueChanged(object sender, EventArgs e)
        {
            uint temp = 0;
            temp = (uint)(udSetVsup2.Value * 1000);
            
            if (temp > adkMod.CfgVsup1Mv + 100) { // added 100 to leave some margin
                Util.displayError(txtSysMsg, "Error: VSUP2 must be less than or equal to VSUP1 plus 0.1V");
                udSetVsup2.Value = prevVsup2;
            } else {
                adkMod.CfgVsup2Mv = (ushort)temp;
                prevVsup2 = udSetVsup2.Value;
            }
        }

        private void chbVsupTrack_CheckedChanged(object sender, EventArgs e)
        {
            if (chbVsupTrack.Checked) {
                udSetVsup2.Enabled = false;
            } else {
                udSetVsup2.Enabled = true;
                udSetVsup2.Value = udSetVsup1.Value;
            }
        }

        private void btnStartDataTest_Click(object sender, EventArgs e)
        {
            string err = "";

            CommonApi.MICS_DT_SPECS specsBsm;
            CommonApi.MICS_DT_SPECS specsIm;

            if (btnStartDataTest.Text.Contains("Start")) {
            
                inDataTest = true;

                // open the link quality stats window if not already open
                if (linkQualForm == null) {
                
                    linkQualForm = new LinkQualityForm(false, connectedToLocalImplant);
                    linkQualForm.Text = "Link Quality Statistics";
                    linkQualForm.FormClosed += new System.Windows.Forms.FormClosedEventHandler(linkQualForm_FormClosed);
                    linkQualForm.Show();
                } else {
                    linkQualForm.Focus();
                }
                linkQualForm.resetStats();
                
                totalBsmDataErrCount = 0;
                txtDtTotalRxByteErrsBsm.Text = "0";
                
                totalImDataErrCount = 0;
                txtDtTotalRxByteErrsIm.Text = "0";
                
                btnStartDataTest.BackColor = Color.LimeGreen;
                btnStartDataTest.Text = "Stop Data Test";
                
                // if connected to local implant via USB, start data test on implant
                if (connectedToLocalImplant) {
                
                    specsIm.txData = imData;
                    specsIm.rxData = bsmData;
                    specsIm.options = (byte)(imDtTx | imDtRx | imDtValidate | imDtIncrementTx | imDtIncrementRx);
                    specsIm.txBlockCount = ushort.Parse(txtDtTxBlockCountIm.Text);
                    //
                    if (imMod.startDataTest(ref specsIm, ref err) < 0) {
                        Util.displayError(txtSysMsg, "Failed to start data test on implant: " + err);
                    }
                }
                
                // start data test on base
                specsBsm.txData = bsmData;
                specsBsm.rxData = imData;
                specsBsm.options = (byte)(bsmDtTx | bsmDtRx | bsmDtValidate | bsmDtIncrementTx | bsmDtIncrementRx);
                specsBsm.txBlockCount = ushort.Parse(txtDtTxBlockCountBsm.Text);
                //
                if (bsmMod.startDataTest(ref specsBsm, ref err) < 0) {
                    Util.displayError(txtSysMsg, "Failed to start data test on base station: " + err);
                }

                cmbImConnection.Enabled = false;
                grpDtConfigBsm.Enabled = false;
                grpDtConfigIm.Enabled = false;
                
            } else {
            
                btnStartDataTest.Text = "Start Data Test";
                btnStartDataTest.BackColor = Color.Transparent;
                inDataTest = false;

                // abort data test on base station (also aborts session)
                if (bsmMod.micsAbort(ref err) < 0) {
                    Util.displayError(txtSysMsg, "Failed to stop data test on base station: " + err);
                }

                cmbImConnection.Enabled = true;
                grpDtConfigBsm.Enabled = true;
                grpDtConfigIm.Enabled = true;
            }
        }

        private void btnBsmLinkQual_Click(object sender, EventArgs e)
        {
            if (linkQualForm == null) {
            
                linkQualForm = new LinkQualityForm(false, connectedToLocalImplant);
                linkQualForm.Text = "Link Quality Statistics";
                linkQualForm.FormClosed += new System.Windows.Forms.FormClosedEventHandler(linkQualForm_FormClosed);
                linkQualForm.Show();
                
            } else {
                linkQualForm.Focus();
            }
        }

        private void linkQualForm_FormClosed(object sender, EventArgs e)
        {
            linkQualForm = null;
        }

        private void txtDtDataBsm_TextChanged(object sender, EventArgs e)
        {
            TextBox p = (TextBox)sender;
            string valStr = p.Text;
            
            if (valStr.Contains("*")) return;

            if (ignoreChangedEvent) {
                ignoreChangedEvent = false;
                return;
            }
            if (valStr == "") {
                ignoreChangedEvent = true;
                valStr = "0";
            }

            if (valStr.Contains("0x")) {
                valStr = valStr.Replace("0x", "0");
            }
            if (valStr.Contains("0X")) {
                valStr = valStr.Replace("0X", "0");
            }

            if ((uint)Int32.Parse(valStr, NumberStyles.HexNumber) > 0x0ff) {
                p.Text = "0x0";
            }

            bsmData = byte.Parse(valStr, NumberStyles.HexNumber);
        }

        private void chbDtTxBsm_CheckedChanged(object sender, EventArgs e)
        {
            if (chbDtTxBsm.Checked) {
                bsmDtTx = CommonApi.MICS_DT_TX;
            } else {
                bsmDtTx = 0;
            }
        }

        private void chbDtRxIm_CheckedChanged(object sender, EventArgs e)
        {
            if (chbDtRxIm.Checked) {
                imDtRx = CommonApi.MICS_DT_RX;
            } else {
                imDtRx = 0;
            }
        }

        private void chbDtValidateRxDataIm_CheckedChanged(object sender, EventArgs e)
        {
            if (chbDtValidateRxDataIm.Checked) {
                imDtValidate = CommonApi.MICS_DT_VALIDATE;
                txtDtTotalRxByteErrsIm.Enabled = true;
            } else {
                imDtValidate = 0;
                txtDtTotalRxByteErrsIm.Enabled = false;
            }
        }

        private void chbDtIncDataBsm_CheckedChanged(object sender, EventArgs e)
        {
            if (chbDtIncDataBsm.Checked) {
                bsmDtIncrementTx = CommonApi.MICS_DT_INC_TX;
                imDtIncrementRx = CommonApi.MICS_DT_INC_RX;
            } else {
                bsmDtIncrementTx = 0;
                imDtIncrementRx = 0;
            }
        }

        private void chbDtTxIm_CheckedChanged(object sender, EventArgs e)
        {
            if (chbDtTxIm.Checked) {
                imDtTx = CommonApi.MICS_DT_TX;
            } else {
                imDtTx = 0;
            }
        }

        private void chbDtRxBsm_CheckedChanged(object sender, EventArgs e)
        {
            if (chbDtRxBsm.Checked) {
                bsmDtRx = CommonApi.MICS_DT_RX;
            } else {
                bsmDtRx = 0;
            }
        }

        private void chbDtValidateRxDataBsm_CheckedChanged(object sender, EventArgs e)
        {
            if (chbDtValidateRxDataBsm.Checked) {
                bsmDtValidate = CommonApi.MICS_DT_VALIDATE;
                txtDtTotalRxByteErrsBsm.Enabled = true;
            } else {
                bsmDtValidate = 0;
                txtDtTotalRxByteErrsBsm.Enabled = false;
            }
        }

        private void chbDtIncDataIm_CheckedChanged(object sender, EventArgs e)
        {
            if (chbDtIncDataIm.Checked) {
                imDtIncrementTx = CommonApi.MICS_DT_INC_TX;
                bsmDtIncrementRx = CommonApi.MICS_DT_INC_RX;
            } else {
                imDtIncrementTx = 0;
                bsmDtIncrementRx = 0;
            }
        }

        private void cmbImConnection_SelectedIndexChanged(object sender, EventArgs e)
        {
            string err = "";

            switch(cmbImConnection.SelectedIndex) {
            case 1:
                if (imMod == null) {
                
                    imMod = new ImApi();
                    if (imMod.init(ref err) < 0) {
                    
                        Util.displayError(txtSysMsg, "Failed to initialize interface to implant: " + err);
                        inDataTest = false;
                        cmbImConnection.SelectedIndex = 0;
                        return;
                    }
                    connectedToLocalImplant = true;
                    grpDataTestIm.Enabled = true;
                }
                if (linkQualForm != null) linkQualForm.enableImStats();
                break;
                
            default:
                if (imMod != null) imMod.close(ref err);
                imMod = null;
                grpDataTestIm.Enabled = false;
                connectedToLocalImplant = false;
                if (linkQualForm != null) linkQualForm.disableImStats();
                break;
            }
        }

        private void txtDtDataIm_TextChanged(object sender, EventArgs e)
        {
            TextBox p = (TextBox)sender;
            string valStr = p.Text;
            
            if (valStr.Contains("*")) return;

            if (ignoreChangedEvent) {
                ignoreChangedEvent = false;
                return;
            }
            if (valStr == "") {
                ignoreChangedEvent = true;
                valStr = "0";
            }

            if (valStr.Contains("0x")) {
                valStr = valStr.Replace("0x", "0");
            }
            if (valStr.Contains("0X")) {
                valStr = valStr.Replace("0X", "0");
            }

            if ((uint)Int32.Parse(valStr, NumberStyles.HexNumber) > 0x0ff) {
                p.Text = "0x0";
            }

            imData = byte.Parse(valStr, NumberStyles.HexNumber);
        }

        private void btnImLinkQual_Click(object sender, EventArgs e)
        {
            if (linkQualForm == null) {
            
                linkQualForm = new LinkQualityForm(true, true);
                linkQualForm.Text = "Link Quality Statistics";
                linkQualForm.FormClosed += new System.Windows.Forms.FormClosedEventHandler(linkQualForm_FormClosed);
                linkQualForm.Show();
                
            } else {
                linkQualForm.Focus();
            }
        }

        private void btnStartStream_Click(object sender, EventArgs e)
        {
            Button p = (Button)sender;

            if (p.Text.Contains("Start")) {
            
                sfdStreamData.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                sfdStreamData.FilterIndex = 1;
                sfdStreamData.RestoreDirectory = true;
                sfdStreamData.ShowDialog();
                if (sfdStreamData.FileName.ToString() == "") return;
                p.Text = "Stop Stream";
                streamingData = true;
                
                if (isImForm) {
                    sw = new StreamWriter(sfdStreamData.FileName);
                    sw.WriteLine(DateTime.Now);
                } else {
                    sw = new StreamWriter(sfdStreamData.FileName);
                    sw.WriteLine(DateTime.Now);
                }
                
            } else {
                p.Text = "Start Stream";
                sw.Close();
                streamingData = false;
            }
        }

        // This function is not intended for customer use (factory tab only)
        // so use by customers is not supported.
        //
        private void btnStartBer_Click(object sender, EventArgs e)
        {
            string err = "";

            // write channel
            //adkMod.micsWrite(25, (uint) cmbBerChan.SelectedIndex, false, ref err);

            if (btnStartBer.Text == "Start") {
            
                btnStartBer.Text = "Stop";
                btnStartBer.BackColor = Color.LimeGreen;
                
                if (radBer200kbps.Checked) {
                
                    if (adkMod.startBerRx(true, (uint)cmbBerChan.SelectedIndex, CommonApi.MOD.MOD_2FSK_FB, ref err) < 0) {
                        Util.displayError(txtSysMsg, "Failed to start 200 Kbps BER test: " + err);
                    }
                    
                } else if (radBer400kbps.Checked) {
                
                    if (adkMod.startBerRx(true, (uint)cmbBerChan.SelectedIndex, CommonApi.MOD.MOD_2FSK, ref err) < 0) {
                        Util.displayError(txtSysMsg, "Failed to start 400 Kbps BER test: " + err);
                    }
                    
                } else { // 800 Kbps
                
                    if (adkMod.startBerRx(true, (uint)cmbBerChan.SelectedIndex, CommonApi.MOD.MOD_4FSK, ref err) < 0) {
                        Util.displayError(txtSysMsg, "Failed to start 800 Kbps BER test: " + err);
                    }
                }
                
            } else {
            
                btnStartBer.Text = "Start";
                btnStartBer.BackColor = Color.Transparent;
                if (adkMod.startBerRx(false, (uint)cmbBerChan.SelectedIndex, CommonApi.MOD.MOD_2FSK_FB, ref err) < 0) {
                    Util.displayError(txtSysMsg, "Failed to stop BER test: " + err);
                }
            }
        }

        // 2.45 GHz receiver calibration. Note this function is not intended for
        // customer use (factory tab only) so use by customers is not supported.
        //
        private void btn245Cal_Click(object sender, EventArgs e)
        {
            string err = "";
            double adc_average1 = 0;
            byte REG_GENEN = 0x6E;     //110
            byte REG_MAC_CTL = 0x29;
            byte REG_WAKEUP_ANT245TUNE = 0x4D;
            byte REG_WAKEUP_WK_RX_CTRL1 = 0x48;
            byte REG_WAKEUP_WK_RX_RSSIGAIN = 0x50;
            byte REG_WAKEUP_LNAFREQ1 = 0x51;
            byte REG_SYNTH_CTRIM = 0x78;
            byte REG_WAKEUP_WK_RX_CTRIM = 0x4F;
            uint temp_reg;
            uint temp_reg2;
            uint REG_CALSELECT1 = CommonApi.P2 | 0x14; 
            uint REG_CALSELECT2 = CommonApi.P2 | 0x15;
            uint REG_WAKEUP_LNABIAS = CommonApi.P2 | 0x49;
            uint REG_WAKEUP_WK_RX_CTRL2 = CommonApi.P2 | 0x56;
            uint REG_WAKEUP_WK_RX_TESTCTRL1 = CommonApi.P2 | 0x46;
            uint REG_WAKEUP_WK_RX_DET_IOSTRIM1 = CommonApi.P2 | 0x54;
            uint REG_WAKEUP_WK_RX_RSSIVOSTRIM = CommonApi.P2 | 0x57;
            uint REG_WAKEUP_WK_RX_LNA_NEGTRIM1 = CommonApi.P2 | 0x5E;
            uint REG_WAKEUP_RX_EN_CNT = CommonApi.P2 | 0x41;
            double max_result = 0;
            uint j = 0;

            btn245Cal.BackColor = Color.LimeGreen;
            adkMod.micsWrite(REG_GENEN, 0x01, false, ref err); //Start synthesizer
            Thread.Sleep(5); //sleep 2ms

            // 10.12.4?
            // copy synth_ctrim[5:1] to wakeup_wk_rx_ctrim
            temp_reg = (uint)adkMod.micsRead(REG_SYNTH_CTRIM, false, ref err);
            temp_reg = temp_reg & 0x3E;
            adkMod.micsWrite(REG_WAKEUP_WK_RX_CTRIM, temp_reg, false, ref err); //Start synthesizer

            // 10.12.5?
            adkMod.micsWrite(REG_GENEN, 0x00, false, ref err); //Stop synthesizer
            adkMod.micsWrite(REG_CALSELECT1, 0x00, false, ref err); //Disable Calibrations
            adkMod.micsWrite(REG_CALSELECT2, 0x01, false, ref err); //Enable Calibration
            adkMod.micsWrite(REG_MAC_CTL, 0x08, false, ref err); //Launch Calibration

            // wait for calibration to finish
            do {
                temp_reg = (uint)adkMod.micsRead(REG_MAC_CTL, false, ref err);
            } while(temp_reg != 0x00);
            temp_reg = (uint)adkMod.micsRead(REG_WAKEUP_WK_RX_CTRL2, false, ref err);
            adkMod.micsWrite(REG_WAKEUP_WK_RX_CTRL2, (temp_reg & 0xFD), false, ref err); //select trim set 1
            adkMod.micsWrite(REG_WAKEUP_LNABIAS, 0x0F, false, ref err); //Max sensitivity and current consumption

            //---------- 10.12.8 Detector and RSSI Offset Calibration --------------------
            adkMod.micsWrite(REG_WAKEUP_WK_RX_TESTCTRL1, 0x01, false, ref err); // step b
            adkMod.micsWrite(REG_WAKEUP_WK_RX_CTRL1, 0x0D, false, ref err); // step c
            adkMod.micsWrite(REG_WAKEUP_WK_RX_RSSIGAIN, 0x01, false, ref err); // step d
            adkMod.micsWrite(REG_WAKEUP_WK_RX_RSSIVOSTRIM, 0x10, false, ref err); // step e
            adkMod.micsWrite(REG_WAKEUP_WK_RX_DET_IOSTRIM1, 0x3F, false, ref err); // step f
             
            performAdc(ref adc_average1);
            temp_reg = (uint)adkMod.micsRead(REG_WAKEUP_WK_RX_DET_IOSTRIM1, false, ref err);
            while(adc_average1 > 0.1 && temp_reg > 0) {
                temp_reg = (uint)adkMod.micsRead(REG_WAKEUP_WK_RX_DET_IOSTRIM1, false, ref err);
                adkMod.micsWrite(REG_WAKEUP_WK_RX_DET_IOSTRIM1, (temp_reg - 1), false, ref err);
                performAdc(ref adc_average1);
            }
            temp_reg = (uint)adkMod.micsRead(REG_WAKEUP_WK_RX_DET_IOSTRIM1, false, ref err);
            adkMod.micsWrite(REG_WAKEUP_WK_RX_DET_IOSTRIM1, temp_reg + 1, false, ref err);  //increase value by 1

            //set to min value
            adkMod.micsWrite(REG_WAKEUP_WK_RX_RSSIVOSTRIM, 0x00, false, ref err);
            temp_reg = (uint)adkMod.micsRead(REG_WAKEUP_WK_RX_RSSIVOSTRIM, false, ref err);
            performAdc(ref adc_average1);
            while(adc_average1 > 1 && temp_reg < 31) {
                temp_reg = (uint)adkMod.micsRead(REG_WAKEUP_WK_RX_RSSIVOSTRIM, false, ref err);
                adkMod.micsWrite(REG_WAKEUP_WK_RX_RSSIVOSTRIM, (temp_reg + 1), false, ref err);
                performAdc(ref adc_average1);
            }

            Util.displayError(txtSysMsg, "WK_RX_RSSIVOSTRIM Register = " + temp_reg.ToString());

            //---------- 10.12.9 LNA Freq Calibration --------------------------------------
            adkMod.micsWrite(REG_WAKEUP_LNAFREQ1, 0x00, false, ref err);
            //adkMod.micsWrite(REG_WAKEUP_WK_RX_LNA_NEGTRIM1, 0x14, false, ref err);
            adkMod.micsWrite(REG_WAKEUP_WK_RX_LNA_NEGTRIM1, 0x20, false, ref err); // Brian C.
            adkMod.micsWrite(REG_WAKEUP_WK_RX_TESTCTRL1, 0x01, false, ref err);
            adkMod.micsWrite(REG_WAKEUP_WK_RX_CTRL1, 0x0F, false, ref err);
            adkMod.micsWrite(REG_WAKEUP_WK_RX_RSSIGAIN, 0x01, false, ref err);
            temp_reg = (uint)adkMod.micsRead(REG_WAKEUP_WK_RX_LNA_NEGTRIM1, false, ref err);
            performAdc(ref adc_average1);
            while(adc_average1 < 5  && temp_reg < 63) {
                temp_reg = (uint)adkMod.micsRead(REG_WAKEUP_WK_RX_LNA_NEGTRIM1, false, ref err);
                adkMod.micsWrite(REG_WAKEUP_WK_RX_LNA_NEGTRIM1, (temp_reg + 1), false, ref err);
                performAdc(ref adc_average1);
            }
            temp_reg = (uint)adkMod.micsRead(REG_WAKEUP_WK_RX_LNA_NEGTRIM1, false, ref err);
            Util.displayError(txtSysMsg, "REG_WAKEUP_WK_RX_LNA_NEGTRIM1 Register Before Backoff = " + temp_reg.ToString());
            //adkMod.micsWrite(REG_WAKEUP_WK_RX_LNA_NEGTRIM1, (temp_reg - 2), false, ref err); //reduce by 2 steps
            if(txtLnaNegTrimBackoff.Text == "") {
                MessageBox.Show("LNA Neg Trim Backoff must contain a value");
                goto test_end;
            }
            adkMod.micsWrite(REG_WAKEUP_WK_RX_LNA_NEGTRIM1, (uint)(temp_reg - Int32.Parse(txtLnaNegTrimBackoff.Text, NumberStyles.Integer)), false, ref err); //reduce by 2 steps
            temp_reg = (uint)adkMod.micsRead(REG_WAKEUP_WK_RX_LNA_NEGTRIM1, false, ref err);

            Util.displayError(txtSysMsg, "REG_WAKEUP_WK_RX_LNA_NEGTRIM1 Register After Backoff = " + temp_reg.ToString());

            j = 0;
            // step a
            adkMod.micsWrite(REG_WAKEUP_LNAFREQ1, j, false, ref err);
            // step b
            switch (MessageBox.Show("Apply 2.45GHz carrier wave to input at -45dBm", "", MessageBoxButtons.OKCancel)) {
            case DialogResult.Cancel:
                goto test_end;
            case DialogResult.OK:
                goto stepC;
            }

            // step c
            stepC:
            
            performAdc(ref adc_average1);
            Util.displayError(txtSysMsg, "ADC Average = " + adc_average1.ToString());
            MessageBox.Show("LNA Freq Cal Step C: ADC Average = " + adc_average1.ToString() + " LNA Freq = " + j.ToString());
            // step d
            if (adc_average1 > 27.0) {
                switch(MessageBox.Show("Reduce amplitude by 1dBm", "", MessageBoxButtons.OKCancel)) {
                case DialogResult.Cancel:
                    goto test_end; 
                case DialogResult.OK: 
                    goto stepC;
                }
                //goto stepC;
            }

            // step e
            j++;
            if (j <= 31) {
                adkMod.micsWrite(REG_WAKEUP_LNAFREQ1, j, false, ref err);
                goto stepC;
            }

            // restore registers
            adkMod.micsWrite(REG_WAKEUP_WK_RX_TESTCTRL1, 0x00, false, ref err);
            adkMod.micsWrite(REG_WAKEUP_WK_RX_CTRL1, 0x00, false, ref err);

            adkMod.micsWrite(REG_CALSELECT2, 0x02, false, ref err);
            adkMod.micsWrite(REG_MAC_CTL, 0x08, false, ref err);
            // wait for calibration to finish
            do {
                temp_reg = (uint)adkMod.micsRead(REG_MAC_CTL, false, ref err);
            } while(temp_reg != 0x00);
            temp_reg = (uint)adkMod.micsRead(REG_WAKEUP_LNAFREQ1, false, ref err);

            MessageBox.Show("Turn off 2.45GHz carrier wave");

            //----------------- Define Negative Resistance in LNA ----------------
            adkMod.micsWrite(REG_WAKEUP_WK_RX_LNA_NEGTRIM1, 0x14, false, ref err);
            adkMod.micsWrite(REG_WAKEUP_WK_RX_TESTCTRL1, 0x01, false, ref err);
            adkMod.micsWrite(REG_WAKEUP_WK_RX_CTRL1, 0x0F, false, ref err);
            adkMod.micsWrite(REG_WAKEUP_WK_RX_RSSIGAIN, 0x01, false, ref err);
            temp_reg = (uint)adkMod.micsRead(REG_WAKEUP_WK_RX_LNA_NEGTRIM1, false, ref err);
            performAdc(ref adc_average1);
            while(adc_average1 < 5  && temp_reg < 63) {
                temp_reg = (uint)adkMod.micsRead(REG_WAKEUP_WK_RX_LNA_NEGTRIM1, false, ref err);
                adkMod.micsWrite(REG_WAKEUP_WK_RX_LNA_NEGTRIM1, (temp_reg + 1), false, ref err);
                performAdc(ref adc_average1);
            }
            temp_reg = (uint)adkMod.micsRead(REG_WAKEUP_WK_RX_LNA_NEGTRIM1, false, ref err);
            //adkMod.micsWrite(REG_WAKEUP_WK_RX_LNA_NEGTRIM1, (temp_reg - 2), false, ref err);  //reduce by 2 steps
            adkMod.micsWrite(REG_WAKEUP_WK_RX_LNA_NEGTRIM1, (uint)(temp_reg - Int32.Parse(txtLnaNegTrimBackoff.Text, NumberStyles.Integer)), false, ref err); //reduce by 2 steps
            temp_reg = (uint)adkMod.micsRead(REG_WAKEUP_WK_RX_LNA_NEGTRIM1, false, ref err);
            Util.displayError(txtSysMsg, "WK_RX_LNA_NEGTRIM1 Register = " + temp_reg.ToString());

            //------------ Fine Trim LNA Frequency (10.13.11) -----------------------------
            max_result = 0.0;
            temp_reg2 = 0;
            temp_reg = (uint)adkMod.micsRead(REG_WAKEUP_LNAFREQ1, false, ref err);  //read LNAFREQ1
            adkMod.micsWrite(REG_WAKEUP_LNAFREQ1, (temp_reg - 1), false, ref err);  //decrease LNAFREQ1 by 1
            MessageBox.Show("Apply 2.45GHz carrier wave to input at -45dBm");
            
            step4:
            
            performAdc(ref adc_average1);
            Util.displayError(txtSysMsg, "ADC Average = " + adc_average1.ToString());
            MessageBox.Show("Fine Trim LNA Freq (10.13.11) Step 4: ADC Average = " + adc_average1.ToString() + " LNA Freq = " + temp_reg.ToString() + " - 1");
            if (adc_average1 > 30.0) {
                switch (MessageBox.Show("Reduce amplitude by 1dBm", "", MessageBoxButtons.OKCancel)) {
                case DialogResult.Cancel:
                    goto test_end; 
                case DialogResult.OK:
                    temp_reg2 = temp_reg;
                    goto step4;
                }
                //temp_reg2 = temp_reg;
                //goto step4;
            } else if (adc_average1 > max_result) {
                max_result = adc_average1;
                temp_reg2 = temp_reg;
            }

            temp_reg = temp_reg + 1;
            adkMod.micsWrite(REG_WAKEUP_LNAFREQ1, (temp_reg), false, ref err);  //increase LNAFREQ1 by 1
            
            step5:
            
            performAdc(ref adc_average1);
            Util.displayError(txtSysMsg, "ADC Average = " + adc_average1.ToString());
            MessageBox.Show("Fine Trim LNA Freq (10.13.11) Step 5: ADC Average = " + adc_average1.ToString() + " LNA Freq = " + temp_reg.ToString());
            if (adc_average1 > 30.0) {
                switch (MessageBox.Show("Reduce amplitude by 1dBm", "", MessageBoxButtons.OKCancel)) {
                case DialogResult.Cancel:
                    goto test_end; 
                case DialogResult.OK:
                    temp_reg2 = temp_reg;
                    goto step5;
                }
            } else if (adc_average1 > max_result) {
                max_result = adc_average1;
                temp_reg2 = temp_reg;
            }

            temp_reg = temp_reg + 1;
            adkMod.micsWrite(REG_WAKEUP_LNAFREQ1, (temp_reg), false, ref err);  //increase LNAFREQ1 by 1
            
            step6:
      
            performAdc(ref adc_average1);
            Util.displayError(txtSysMsg, "ADC Average = " + adc_average1.ToString());
            MessageBox.Show("Fine Trim LNA Freq (10.13.11) Step 6: ADC Average = " + adc_average1.ToString() + " LNA Freq = " + temp_reg.ToString());
            if (adc_average1 > 30.0) {
                switch (MessageBox.Show("Reduce amplitude by 1dBm", "", MessageBoxButtons.OKCancel)) {
                case DialogResult.Cancel:
                    goto test_end; 
                case DialogResult.OK:
                    temp_reg2 = temp_reg;
                    goto step6;
                }
                //temp_reg2 = temp_reg;
                //goto step6;
            } else if (adc_average1 > max_result) {
                max_result = adc_average1;
                temp_reg2 = temp_reg;
            }

            temp_reg = temp_reg + 1;
            adkMod.micsWrite(REG_WAKEUP_LNAFREQ1, (temp_reg), false, ref err);  //increase LNAFREQ1 by 1
            
            step7:
            
            performAdc(ref adc_average1);
            MessageBox.Show("Fine Trim LNA Freq (10.13.11) Step 7: ADC Average = " + adc_average1.ToString());
            if (adc_average1 > 30.0) {
                //MessageBox.Show("Reduce amplitude by 1dBm");
                switch (MessageBox.Show("Reduce amplitude by 1dBm", "", MessageBoxButtons.OKCancel)) {
                case DialogResult.Cancel:
                    goto test_end; 
                case DialogResult.OK:
                    temp_reg2 = temp_reg;
                    goto step7;
                }
                //temp_reg2 = temp_reg;
                //goto step7;
            } else if (adc_average1 > max_result) {
                max_result = adc_average1;
                temp_reg2 = temp_reg;
            }

            temp_reg = temp_reg + 1;
            adkMod.micsWrite(REG_WAKEUP_LNAFREQ1, (temp_reg), false, ref err);  //increase LNAFREQ1 by 1
            
            step8:
            
            performAdc(ref adc_average1);
            Util.displayError(txtSysMsg, "ADC Average = " + adc_average1.ToString());
            MessageBox.Show("Fine Trim LNA Freq (10.13.11) Step 8: ADC Average = " + adc_average1.ToString());
            if (adc_average1 > 30.0) {
                switch (MessageBox.Show("Reduce amplitude by 1dBm", "", MessageBoxButtons.OKCancel)) {
                    case DialogResult.Cancel:
                        goto test_end; 
                    case DialogResult.OK:
                        temp_reg2 = temp_reg;
                        goto step8;
                }
                //temp_reg2 = temp_reg;
                //goto step8;
            } else if (adc_average1 > max_result) {
                max_result = adc_average1;
                temp_reg2 = temp_reg;
            }

            adkMod.micsWrite(REG_WAKEUP_LNAFREQ1, temp_reg2, false, ref err);  //increase LNAFREQ1 by 1
            MessageBox.Show("Turn off 2.45GHz carrier wave");
        
            Util.displayError(txtSysMsg, "WAKEUP_LNAFREQ1 Register = " + temp_reg2.ToString());

            //----------- 2.45GHz Antenna Tuning (10.13.12) --------------------
            adkMod.micsWrite(REG_WAKEUP_WK_RX_TESTCTRL1, 0x01, false, ref err);
            adkMod.micsWrite(REG_WAKEUP_WK_RX_CTRL1, 0x0F, false, ref err);
            adkMod.micsWrite(REG_WAKEUP_WK_RX_RSSIGAIN, 0x01, false, ref err);
            adkMod.micsWrite(REG_WAKEUP_ANT245TUNE, 0x00, false, ref err);
            MessageBox.Show("Apply 2.45GHz carrier wave to input at -45dBm");
       
            max_result = 0.0;
            for(uint i = 0; i <= 15; i++) {
            
                antTuneLoop:
                
                adkMod.micsWrite(REG_WAKEUP_ANT245TUNE, i, false, ref err);
                performAdc(ref adc_average1);
                Util.displayError(txtSysMsg, "ADC Average = " + adc_average1.ToString());
                MessageBox.Show("2.45 Antenna Tuning (10.13.12): ADC Average = " + adc_average1.ToString());
                
                if (adc_average1 > 30.0) {
                
                    switch (MessageBox.Show("Reduce amplitude by 1dBm", "", MessageBoxButtons.OKCancel)) {
                    case DialogResult.Cancel:
                        goto test_end;
                    case DialogResult.OK:
                        temp_reg = i;
                        goto antTuneLoop;
                    }
                    //temp_reg = i;
                    //goto antTuneLoop;
                } else if (adc_average1 > max_result) {
                    max_result = adc_average1;
                    temp_reg = i;
                }
                
            }
            
            adkMod.micsWrite(REG_WAKEUP_ANT245TUNE, temp_reg, false, ref err);
            Util.displayError(txtSysMsg, "WAKEUP_ANT245TUNE Register = " + temp_reg.ToString());
            MessageBox.Show("Turn off 2.45GHz carrier wave");
            
            adkMod.micsWrite(REG_WAKEUP_RX_EN_CNT, 0x05, false, ref err);  //allow 5 retries

            temp_reg = (uint)adkMod.micsRead(REG_WAKEUP_WK_RX_DET_IOSTRIM1, false, ref err);
            adkMod.micsWrite(REG_WAKEUP_WK_RX_DET_IOSTRIM1, temp_reg+7, false, ref err);
            temp_reg = (uint)adkMod.micsRead(REG_WAKEUP_WK_RX_DET_IOSTRIM1, false, ref err);
            Util.displayError(txtSysMsg, "WK_RX_DET_IOSTRIM1 Register = " + temp_reg.ToString());
            
            test_end:
            
            adkMod.micsWrite(REG_WAKEUP_WK_RX_TESTCTRL1, 0x00, false, ref err);
            adkMod.micsWrite(REG_WAKEUP_WK_RX_CTRL1, 0x00, false, ref err);

            // restore calibration registers
            adkMod.micsWrite(REG_CALSELECT1, 0x0F, false, ref err);
            adkMod.micsWrite(REG_CALSELECT2, 0x00, false, ref err);

            btn245Cal.BackColor = Color.Transparent;
        }

        // This function is not intended for customer use (factory tab only)
        // so use by customers is not supported.
        //
        private void performAdc(ref double adc_average)
        {
            string err = "";
            uint adc_result = 0;
            uint adc_total = 0;
            byte REG_ADCCTRL = 0x74;   //116
            byte REG_ADCOUTPUT = 0x75; //117

            // ADC MEASUREMENT PROCEDURE
            for(int i = 0; i < 20; i++) {
            
                // setup ADC
                adkMod.micsWrite((uint)REG_ADCCTRL, 0x1D, false, ref err);  //select wk_rx_rssi on ADC input

                // wait 10ms
                Thread.Sleep(1);

                adkMod.micsWrite((uint)REG_ADCCTRL, 0x1F, false, ref err); //start conversion

                adc_result = (byte)adkMod.micsRead(REG_ADCOUTPUT, false, ref err);
                adc_result = (byte)(adc_result & 0x1F);
                adc_total = adc_total + adc_result;
            }
            adc_average = (double) (adc_total) / 20.0;
        }
        
        // This function is not intended for customer use (factory tab only)
        // so use by customers is not supported.
        //
        private void btnStartTraceView_Click(object sender, EventArgs e)
        {
            if (adkTraceViewForm == null) {
            
                // create the trace view form
                adkTraceViewForm = new AdkTraceViewForm(isImForm, adkForm);

                // add an event handler to catch when this form is closed
                adkTraceViewForm.FormClosed += new System.Windows.Forms.FormClosedEventHandler(adkTraceViewForm_FormClosed);

                adkTraceViewForm.Show();
                
			} else {
            
                adkTraceViewForm.Focus();
            }
        }

        private void btnImPowMon_Click(object sender, EventArgs e)
        {
            if (isImForm) {
            
                openPowMonForm(mezzModel, sender, e);
                
            } else { // base station
            
                // this has not been implimented yet
                MessageBox.Show("Remote power monitoring is not yet implemented.\r",
                    "btnImPowMon_Click() on base station GUI:", MessageBoxButtons.OK);
            }
        }

        private void openPowMonForm(string titleOfMezz, object sender, EventArgs e)
        {
            // if power monitor isn't already open, open it
            if (adpPowMonForm == null) {
            
                try {
                    unsafe {
                        adpPowMonForm = new AdpPowMonForm(titleOfMezz, adkMod.adpId);
                    }
                } catch (ArgumentException) {
                
                    // If AdpPowMonForm() throws this exception, it indicates
                    // the user declined to create a private connection to the
                    // ADP board. This only happens if AdpPowMonForm() can't
                    // find the mezzanine board, which should only happen when
                    // the user is calibrating an ADP board. Since this exception
                    // indicates the user chose not to continue, just ignore the
                    // exception and return without opening the power monitor.
                    //
                    return;
                }
 
                // add event handler to call when power monitor is closed
                adpPowMonForm.FormClosed += new System.Windows.Forms.FormClosedEventHandler(adpPowMonForm_FormClosed);
 
                adpPowMonForm.Show();
                
            } else { // power monitor for implant is already open
            
                adpPowMonForm.Focus();
            }
        }

        private void txtCcRegAddr_TextChanged(object sender, EventArgs e)
        {
            TextBox p = (TextBox)sender;
            
            string valStr = p.Text;
            if (valStr.Contains("*")) return;

            if (ignoreChangedEvent)  {
                ignoreChangedEvent = false;
                return;
            }

            if (valStr == "")  {
                ignoreChangedEvent = true;
                valStr = "0";
            }

            if (valStr.Contains("0x")) {
                valStr = valStr.Replace("0x", "0");
            }

            if (valStr.Contains("0X"))  {
                valStr = valStr.Replace("0X", "0");
            }

            // validate the input
            if ((uint)Int32.Parse(valStr, NumberStyles.HexNumber) > 0x03F) {
                p.Text = "0x0";
                MessageBox.Show("Register address must be less than or equal to 0x3F");
            }
        }

        private void txtCcRegVal_TextChanged(object sender, EventArgs e)
        {
            TextBox p = (TextBox)sender;
            string valStr = p.Text;
            if (valStr.Contains("*")) return;

            if (ignoreChangedEvent) {
                ignoreChangedEvent = false;
                return;
            }

            if (valStr == "") {
                ignoreChangedEvent = true;
                valStr = "0";
            }

            if (valStr.Contains("0x")) {
                valStr = valStr.Replace("0x", "0");
            }

            if (valStr.Contains("0X")) {
                valStr = valStr.Replace("0X", "0");
            }

            if ((uint)Int32.Parse(valStr, NumberStyles.HexNumber) > 0x0FF) {
                p.Text = "0x0";
            }
        }
        
        private void btnCcRegRead_Click(object sender, EventArgs e)
        {
            int val;
            string err = "";
            
            string addrStr = txtCcRegAddr.Text;
            if (addrStr == "") return;
            if (addrStr.Contains("*")) return;
            if (addrStr.Contains("0x")) addrStr = addrStr.Replace("0x", "0");

            uint addr = (uint)Int32.Parse(addrStr, NumberStyles.HexNumber);
 
            if ((val = bsmMod.readCcReg(addr, ref err)) < 0) {
                Util.displayError(txtSysMsg, "Failed to read CC25XX register: " + err);
            } else {
                txtCcRegVal.Text = "0x" + val.ToString("X2");
            }
        }

        private void btnCcRegWrite_Click(object sender, EventArgs e)
        {
            string err = "";

            string valStr = txtCcRegVal.Text;
            if (valStr == "") return;
            if (valStr.Contains("*")) return;
            if (valStr.Contains("0x")) valStr = valStr.Replace("0x", "0");
            
            string addrStr = txtCcRegAddr.Text;
            if (addrStr == "") return;
            if (addrStr.Contains("*")) return;
            if (addrStr.Contains("0x")) addrStr = addrStr.Replace("0x", "0");
            
            uint addr = (uint)Int32.Parse(addrStr, NumberStyles.HexNumber);
            uint val = (uint)Int32.Parse(valStr, NumberStyles.HexNumber);
            
            // validate the inputs
            if (addr > 0x3f) {
                MessageBox.Show("Register address must be less than or equal to 0x3F");
                return;
            }
            if (val > 0xff) {
                MessageBox.Show("Register value must be less than or equal to 0xFF");
                return;
            }

            if (bsmMod.writeCcReg(addr, val, ref err) < 0) {
                Util.displayError(txtSysMsg, "Failed to write to CC25XX register: " + err);
            }
        }
        
        private void txtCcPa_TextChanged(object sender, EventArgs e)
        {
            TextBox p = (TextBox)sender;
            string valStr = p.Text;
            
            if (valStr.Contains("*")) return;

            if (ignoreChangedEvent) {
                ignoreChangedEvent = false;
                return;
            }

            if (valStr == "") {
                ignoreChangedEvent = true;
                valStr = "0";
            }

            if (valStr.Contains("0x")) {
                valStr = valStr.Replace("0x", "0");
            }

            if (valStr.Contains("0X")) {
                valStr = valStr.Replace("0X", "0");
            }

            if ((uint)Int32.Parse(valStr, NumberStyles.HexNumber) > 0x0FF) {
                p.Text = "0x0";
            }
        }

        private void btnCcPaRead_Click(object sender, EventArgs e)
        {
            int val;
            string err = "";
            
            // read 1st entry in PA table
            if ((val = bsmMod.readCcReg(0x3E, ref err)) < 0) {
                Util.displayError(txtSysMsg, "Failed to read CC25XX PA table: " + err);
            } else {
                txtCcPa.Text = "0x" + val.ToString("X2");
            }
        }

        private void btnCcPaWrite_Click(object sender, EventArgs e)
        {
            uint ccPa;
            string err = ""; 
            
            string valStr = txtCcPa.Text;
            if (valStr.Contains("*")) return;
            if (valStr == "") return;
            if (valStr.Contains("0x")) valStr = valStr.Replace("0x", "0");

            ccPa = (uint)int.Parse(valStr, NumberStyles.HexNumber);
            if (ccPa > 0xff) {
                MessageBox.Show("Register value must be less than or equal to 0xFF");
                return;
            }
            
            if (bsmMod.setTx245Power(ccPa, ref err) < 0) {
                Util.displayError(txtSysMsg, "Failed to set the 2.45 GHZ TX power: " + err);
            }
        }

        private void frmNvm_FormClosed(object sender, EventArgs ea)
        {
            frmNvm = null;
        }

        private void btnNonvolatileMemory_Click(object sender, EventArgs ea)
        {
            try {
                if (frmNvm == null) {
                    frmNvm = new NvmForm(this.bsmMod);
                    frmNvm.Text = "Nonvolatile Memory";
                    frmNvm.FormClosed += new System.Windows.Forms.FormClosedEventHandler(frmNvm_FormClosed);
                    frmNvm.Show();
                } else {
                    frmNvm.Focus();
                }
                
            } catch (Exception e) {
            
                MessageBox.Show(Util.eMsg(e));
            }
        }

        private void chbResetMicsOnWakeup_CheckedChanged(object sender, EventArgs e)
        {
            bsmMod.CfgResetMicsOnWakeup = ((CheckBox)sender).Checked;
        }

        private void btnShowRssiToDbmCalsFromNvm_Click(object sender, EventArgs e)
        {
            uint chan;
            
            for(chan = 0; chan < 10; ++chan) {
            
                if (isImForm) {
                    txtRssi[chan].Text = imMicsFactNvm.rssiToDbmCal.Val.ToString();
                } else {
                    txtRssi[chan].Text = getBsmRssiToDbmCal(chan).ToString();
                }
                txtRssi[chan].Font = regularFont;
                
                txtRssiDbm[chan].Text = INVALID_VALUE;
                txtRssiDbm[chan].Font = regularFont;
            }
        }

        private void chbUseIntRssiOnBsm_CheckedChanged(object sender, EventArgs e)
        {
            // Clear the RSSI displayed for every channel and ensure they're all
            // displayed using the regular font since the clearest channel is no
            // longer known.
            //
            for(uint chan = 0; chan < 10; ++chan) {
            
                txtRssi[chan].Text = INVALID_VALUE;
                txtRssi[chan].Font = regularFont;
                txtRssiDbm[chan].Text = INVALID_VALUE;
                txtRssiDbm[chan].Font = regularFont;
            }
        
            // If "use internal RSSI" is selected on the base station GUI (for factory
            // test/debug purposes only), change the title of the RSSI column to "Int",
            // and hide the "Average" checkbox for the CCA. This is done because when
            // "use internal RSSI" is selected, the "Perform CCA" button just performs
            // an internal RSSI for each channel (not a CCA), and the "R" buttons on
            // the CCA display also perform an internal RSSI (not an external RSSI).
            // Since the internal RSSI always averages over 10 ms, the "Average"
            // checkbox has no effect.
            //
            if (chbUseIntRssiOnBsm.Checked) {
                lblExtOrIntAdc.Text = INT_ADC;
                chbUseAveRssiForCca.Visible = false;
            } else {
                lblExtOrIntAdc.Text = EXT_ADC;
                chbUseAveRssiForCca.Visible = true;
            }
        }
    }
}
