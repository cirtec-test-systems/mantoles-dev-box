//
// Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2012. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.

namespace Zarlink.Adk.Forms
{
    partial class RegDescForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegDescForm));
            this.grpRegDesc = new System.Windows.Forms.GroupBox();
            this.txtRegDesc = new System.Windows.Forms.TextBox();
            this.grpDescAddr = new System.Windows.Forms.GroupBox();
            this.txtRegAddr = new System.Windows.Forms.TextBox();
            this.grpRegName = new System.Windows.Forms.GroupBox();
            this.txtRegName = new System.Windows.Forms.TextBox();
            this.grpRegType = new System.Windows.Forms.GroupBox();
            this.txtRegType = new System.Windows.Forms.TextBox();
            this.grpRegBits = new System.Windows.Forms.GroupBox();
            this.txtRegBits = new System.Windows.Forms.TextBox();
            this.grpRegReset = new System.Windows.Forms.GroupBox();
            this.txtRegReset = new System.Windows.Forms.TextBox();
            this.grpRegCrc = new System.Windows.Forms.GroupBox();
            this.txtRegCrc = new System.Windows.Forms.TextBox();
            this.grpRegUsage = new System.Windows.Forms.GroupBox();
            this.txtRegCategory = new System.Windows.Forms.TextBox();
            this.grpRegDesc.SuspendLayout();
            this.grpDescAddr.SuspendLayout();
            this.grpRegName.SuspendLayout();
            this.grpRegType.SuspendLayout();
            this.grpRegBits.SuspendLayout();
            this.grpRegReset.SuspendLayout();
            this.grpRegCrc.SuspendLayout();
            this.grpRegUsage.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpRegDesc
            // 
            this.grpRegDesc.Controls.Add(this.txtRegDesc);
            this.grpRegDesc.Location = new System.Drawing.Point(14, 122);
            this.grpRegDesc.Name = "grpRegDesc";
            this.grpRegDesc.Size = new System.Drawing.Size(605, 363);
            this.grpRegDesc.TabIndex = 1;
            this.grpRegDesc.TabStop = false;
            this.grpRegDesc.Text = "Description";
            // 
            // txtRegDesc
            // 
            this.txtRegDesc.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegDesc.Location = new System.Drawing.Point(11, 19);
            this.txtRegDesc.Multiline = true;
            this.txtRegDesc.Name = "txtRegDesc";
            this.txtRegDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRegDesc.Size = new System.Drawing.Size(588, 333);
            this.txtRegDesc.TabIndex = 2;
            this.txtRegDesc.TabStop = false;
            this.txtRegDesc.Text = "Description of register.";
            // 
            // grpDescAddr
            // 
            this.grpDescAddr.Controls.Add(this.txtRegAddr);
            this.grpDescAddr.Location = new System.Drawing.Point(12, 12);
            this.grpDescAddr.Name = "grpDescAddr";
            this.grpDescAddr.Size = new System.Drawing.Size(117, 49);
            this.grpDescAddr.TabIndex = 5;
            this.grpDescAddr.TabStop = false;
            this.grpDescAddr.Text = "Address";
            // 
            // txtRegAddr
            // 
            this.txtRegAddr.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtRegAddr.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRegAddr.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegAddr.Location = new System.Drawing.Point(6, 19);
            this.txtRegAddr.Multiline = true;
            this.txtRegAddr.Name = "txtRegAddr";
            this.txtRegAddr.Size = new System.Drawing.Size(105, 20);
            this.txtRegAddr.TabIndex = 2;
            this.txtRegAddr.TabStop = false;
            this.txtRegAddr.Text = "addr";
            // 
            // grpRegName
            // 
            this.grpRegName.Controls.Add(this.txtRegName);
            this.grpRegName.Location = new System.Drawing.Point(135, 12);
            this.grpRegName.Name = "grpRegName";
            this.grpRegName.Size = new System.Drawing.Size(484, 49);
            this.grpRegName.TabIndex = 6;
            this.grpRegName.TabStop = false;
            this.grpRegName.Text = "Name";
            // 
            // txtRegName
            // 
            this.txtRegName.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtRegName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRegName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegName.Location = new System.Drawing.Point(6, 19);
            this.txtRegName.Multiline = true;
            this.txtRegName.Name = "txtRegName";
            this.txtRegName.Size = new System.Drawing.Size(461, 20);
            this.txtRegName.TabIndex = 0;
            this.txtRegName.TabStop = false;
            this.txtRegName.Text = "name";
            // 
            // grpRegType
            // 
            this.grpRegType.Controls.Add(this.txtRegType);
            this.grpRegType.Location = new System.Drawing.Point(12, 67);
            this.grpRegType.Name = "grpRegType";
            this.grpRegType.Size = new System.Drawing.Size(90, 49);
            this.grpRegType.TabIndex = 7;
            this.grpRegType.TabStop = false;
            this.grpRegType.Text = "Type";
            // 
            // txtRegType
            // 
            this.txtRegType.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtRegType.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRegType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegType.Location = new System.Drawing.Point(2, 19);
            this.txtRegType.Multiline = true;
            this.txtRegType.Name = "txtRegType";
            this.txtRegType.Size = new System.Drawing.Size(86, 20);
            this.txtRegType.TabIndex = 3;
            this.txtRegType.TabStop = false;
            this.txtRegType.Text = "type";
            this.txtRegType.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // grpRegBits
            // 
            this.grpRegBits.Controls.Add(this.txtRegBits);
            this.grpRegBits.Location = new System.Drawing.Point(135, 67);
            this.grpRegBits.Name = "grpRegBits";
            this.grpRegBits.Size = new System.Drawing.Size(55, 49);
            this.grpRegBits.TabIndex = 8;
            this.grpRegBits.TabStop = false;
            this.grpRegBits.Text = "Bits";
            // 
            // txtRegBits
            // 
            this.txtRegBits.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtRegBits.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRegBits.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegBits.Location = new System.Drawing.Point(6, 19);
            this.txtRegBits.Multiline = true;
            this.txtRegBits.Name = "txtRegBits";
            this.txtRegBits.Size = new System.Drawing.Size(43, 20);
            this.txtRegBits.TabIndex = 4;
            this.txtRegBits.TabStop = false;
            this.txtRegBits.Text = "bits";
            this.txtRegBits.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // grpRegReset
            // 
            this.grpRegReset.Controls.Add(this.txtRegReset);
            this.grpRegReset.Location = new System.Drawing.Point(200, 67);
            this.grpRegReset.Name = "grpRegReset";
            this.grpRegReset.Size = new System.Drawing.Size(63, 49);
            this.grpRegReset.TabIndex = 11;
            this.grpRegReset.TabStop = false;
            this.grpRegReset.Text = "Reset";
            // 
            // txtRegReset
            // 
            this.txtRegReset.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtRegReset.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRegReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegReset.Location = new System.Drawing.Point(6, 19);
            this.txtRegReset.Multiline = true;
            this.txtRegReset.Name = "txtRegReset";
            this.txtRegReset.Size = new System.Drawing.Size(52, 20);
            this.txtRegReset.TabIndex = 7;
            this.txtRegReset.TabStop = false;
            this.txtRegReset.Text = "reset";
            this.txtRegReset.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // grpRegCrc
            // 
            this.grpRegCrc.Controls.Add(this.txtRegCrc);
            this.grpRegCrc.Location = new System.Drawing.Point(269, 67);
            this.grpRegCrc.Name = "grpRegCrc";
            this.grpRegCrc.Size = new System.Drawing.Size(53, 49);
            this.grpRegCrc.TabIndex = 12;
            this.grpRegCrc.TabStop = false;
            this.grpRegCrc.Text = "CRC";
            // 
            // txtRegCrc
            // 
            this.txtRegCrc.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtRegCrc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRegCrc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegCrc.Location = new System.Drawing.Point(6, 19);
            this.txtRegCrc.Multiline = true;
            this.txtRegCrc.Name = "txtRegCrc";
            this.txtRegCrc.Size = new System.Drawing.Size(41, 20);
            this.txtRegCrc.TabIndex = 4;
            this.txtRegCrc.TabStop = false;
            this.txtRegCrc.Text = "crc";
            this.txtRegCrc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // grpRegUsage
            // 
            this.grpRegUsage.Controls.Add(this.txtRegCategory);
            this.grpRegUsage.Location = new System.Drawing.Point(328, 67);
            this.grpRegUsage.Name = "grpRegUsage";
            this.grpRegUsage.Size = new System.Drawing.Size(92, 49);
            this.grpRegUsage.TabIndex = 8;
            this.grpRegUsage.TabStop = false;
            this.grpRegUsage.Text = "Category";
            // 
            // txtRegCategory
            // 
            this.txtRegCategory.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtRegCategory.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRegCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegCategory.Location = new System.Drawing.Point(6, 19);
            this.txtRegCategory.Multiline = true;
            this.txtRegCategory.Name = "txtRegCategory";
            this.txtRegCategory.Size = new System.Drawing.Size(80, 29);
            this.txtRegCategory.TabIndex = 5;
            this.txtRegCategory.TabStop = false;
            this.txtRegCategory.Text = "cat";
            this.txtRegCategory.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // RegDescForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(633, 496);
            this.Controls.Add(this.grpRegUsage);
            this.Controls.Add(this.grpRegCrc);
            this.Controls.Add(this.grpRegReset);
            this.Controls.Add(this.grpRegBits);
            this.Controls.Add(this.grpRegType);
            this.Controls.Add(this.grpRegName);
            this.Controls.Add(this.grpDescAddr);
            this.Controls.Add(this.grpRegDesc);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RegDescForm";
            this.Text = "Register Description";
            this.grpRegDesc.ResumeLayout(false);
            this.grpRegDesc.PerformLayout();
            this.grpDescAddr.ResumeLayout(false);
            this.grpDescAddr.PerformLayout();
            this.grpRegName.ResumeLayout(false);
            this.grpRegName.PerformLayout();
            this.grpRegType.ResumeLayout(false);
            this.grpRegType.PerformLayout();
            this.grpRegBits.ResumeLayout(false);
            this.grpRegBits.PerformLayout();
            this.grpRegReset.ResumeLayout(false);
            this.grpRegReset.PerformLayout();
            this.grpRegCrc.ResumeLayout(false);
            this.grpRegCrc.PerformLayout();
            this.grpRegUsage.ResumeLayout(false);
            this.grpRegUsage.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpRegDesc;
        private System.Windows.Forms.GroupBox grpDescAddr;
        private System.Windows.Forms.GroupBox grpRegName;
        private System.Windows.Forms.GroupBox grpRegType;
        private System.Windows.Forms.GroupBox grpRegBits;
        private System.Windows.Forms.GroupBox grpRegReset;
        private System.Windows.Forms.GroupBox grpRegCrc;
        private System.Windows.Forms.GroupBox grpRegUsage;
        private System.Windows.Forms.TextBox txtRegName;
        private System.Windows.Forms.TextBox txtRegAddr;
        private System.Windows.Forms.TextBox txtRegType;
        private System.Windows.Forms.TextBox txtRegBits;
        private System.Windows.Forms.TextBox txtRegReset;
        private System.Windows.Forms.TextBox txtRegCrc;
        private System.Windows.Forms.TextBox txtRegDesc;
        private System.Windows.Forms.TextBox txtRegCategory;

    }
}