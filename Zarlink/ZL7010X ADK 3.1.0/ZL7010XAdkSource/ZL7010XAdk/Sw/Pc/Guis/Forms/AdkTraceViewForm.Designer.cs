//
// Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2012. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.

namespace Zarlink.Adk.Forms
{
    partial class AdkTraceViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdkTraceViewForm));
            this.tbTraceDataDisplay = new System.Windows.Forms.TextBox();
            this.cmbTracePollFreq = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.cmbWhichDeviceToTrace = new System.Windows.Forms.ComboBox();
            this.chbShowDeltaT = new System.Windows.Forms.CheckBox();
            this.lblLastError = new System.Windows.Forms.Label();
            this.btnStartRdTraceStop = new System.Windows.Forms.Button();
            this.timerTracePolling = new System.Windows.Forms.Timer(this.components);
            this.btnClearTraceTextBox = new System.Windows.Forms.Button();
            this.labCurTimerInterval = new System.Windows.Forms.Label();
            this.labDirOfExe = new System.Windows.Forms.Label();
            this.traceDirTextBox = new System.Windows.Forms.TextBox();
            this.btnReportTraceTextboxStats = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnBrowseForTraceLogFile = new System.Windows.Forms.Button();
            this.cbLogTraceDataToFile = new System.Windows.Forms.CheckBox();
            this.lblTraceTextBoxStats = new System.Windows.Forms.Label();
            this.btnEraseLogFile = new System.Windows.Forms.Button();
            this.cmbMaxTraceTextBoxLines = new System.Windows.Forms.ComboBox();
            this.lblLinesNowInTraceTextbox = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbTraceDataDisplay
            // 
            this.tbTraceDataDisplay.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTraceDataDisplay.Location = new System.Drawing.Point(12, 251);
            this.tbTraceDataDisplay.MaxLength = 500;
            this.tbTraceDataDisplay.Multiline = true;
            this.tbTraceDataDisplay.Name = "tbTraceDataDisplay";
            this.tbTraceDataDisplay.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbTraceDataDisplay.Size = new System.Drawing.Size(684, 284);
            this.tbTraceDataDisplay.TabIndex = 2;
            this.tbTraceDataDisplay.TextChanged += new System.EventHandler(this.traceTextBox_TextChanged);
            // 
            // cmbTracePollFreq
            // 
            this.cmbTracePollFreq.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTracePollFreq.FormattingEnabled = true;
            this.cmbTracePollFreq.Items.AddRange(new object[] {
            "Once",
            "1 Hz",
            "2 Hz",
            "3 Hz",
            "4 Hz",
            "5 Hz",
            "6 Hz",
            "7 Hz",
            "8 Hz",
            "9 Hz",
            "10 Hz"});
            this.cmbTracePollFreq.Location = new System.Drawing.Point(215, 24);
            this.cmbTracePollFreq.Name = "cmbTracePollFreq";
            this.cmbTracePollFreq.Size = new System.Drawing.Size(76, 21);
            this.cmbTracePollFreq.TabIndex = 16;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(375, 15);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(116, 13);
            this.label26.TabIndex = 15;
            this.label26.Text = "Which Board To Trace";
            // 
            // cmbWhichDeviceToTrace
            // 
            this.cmbWhichDeviceToTrace.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWhichDeviceToTrace.FormattingEnabled = true;
            this.cmbWhichDeviceToTrace.Items.AddRange(new object[] {
            "Local ADP",
            "Local Mezz"});
            this.cmbWhichDeviceToTrace.Location = new System.Drawing.Point(378, 31);
            this.cmbWhichDeviceToTrace.MaxDropDownItems = 2;
            this.cmbWhichDeviceToTrace.Name = "cmbWhichDeviceToTrace";
            this.cmbWhichDeviceToTrace.Size = new System.Drawing.Size(115, 21);
            this.cmbWhichDeviceToTrace.TabIndex = 14;
            this.cmbWhichDeviceToTrace.SelectedIndexChanged += new System.EventHandler(this.cmbWhichDeviceToTrace_SelectedIndexChanged);
            // 
            // chbShowDeltaT
            // 
            this.chbShowDeltaT.AutoSize = true;
            this.chbShowDeltaT.Location = new System.Drawing.Point(215, 84);
            this.chbShowDeltaT.Name = "chbShowDeltaT";
            this.chbShowDeltaT.Size = new System.Drawing.Size(107, 17);
            this.chbShowDeltaT.TabIndex = 13;
            this.chbShowDeltaT.Text = "Show Time Delta";
            this.chbShowDeltaT.UseVisualStyleBackColor = true;
            this.chbShowDeltaT.CheckedChanged += new System.EventHandler(this.chbShowDeltaT_CheckedChanged);
            // 
            // lblLastError
            // 
            this.lblLastError.Location = new System.Drawing.Point(21, 122);
            this.lblLastError.Name = "lblLastError";
            this.lblLastError.Size = new System.Drawing.Size(537, 28);
            this.lblLastError.TabIndex = 17;
            this.lblLastError.Text = "No Errors Yet";
            this.lblLastError.Click += new System.EventHandler(this.lblLastError_Click);
            // 
            // btnStartRdTraceStop
            // 
            this.btnStartRdTraceStop.Location = new System.Drawing.Point(12, 8);
            this.btnStartRdTraceStop.Name = "btnStartRdTraceStop";
            this.btnStartRdTraceStop.Size = new System.Drawing.Size(173, 26);
            this.btnStartRdTraceStop.TabIndex = 18;
            this.btnStartRdTraceStop.Text = "Start Reading Trace Data";
            this.btnStartRdTraceStop.UseVisualStyleBackColor = true;
            this.btnStartRdTraceStop.Click += new System.EventHandler(this.btnStartRdTraceStop_Click);
            // 
            // timerTracePolling
            // 
            this.timerTracePolling.Tick += new System.EventHandler(this.timerTracePolling_Tick);
            // 
            // btnClearTraceTextBox
            // 
            this.btnClearTraceTextBox.Location = new System.Drawing.Point(12, 40);
            this.btnClearTraceTextBox.Name = "btnClearTraceTextBox";
            this.btnClearTraceTextBox.Size = new System.Drawing.Size(173, 28);
            this.btnClearTraceTextBox.TabIndex = 19;
            this.btnClearTraceTextBox.Text = "Clear Trace Data Window";
            this.btnClearTraceTextBox.UseVisualStyleBackColor = true;
            this.btnClearTraceTextBox.Click += new System.EventHandler(this.btnClearTraceTextBox_Click);
            // 
            // labCurTimerInterval
            // 
            this.labCurTimerInterval.AutoSize = true;
            this.labCurTimerInterval.Location = new System.Drawing.Point(219, 48);
            this.labCurTimerInterval.Name = "labCurTimerInterval";
            this.labCurTimerInterval.Size = new System.Drawing.Size(0, 13);
            this.labCurTimerInterval.TabIndex = 20;
            this.labCurTimerInterval.Click += new System.EventHandler(this.labCurTimerInterval_Click);
            // 
            // labDirOfExe
            // 
            this.labDirOfExe.AutoSize = true;
            this.labDirOfExe.Location = new System.Drawing.Point(5, 166);
            this.labDirOfExe.Name = "labDirOfExe";
            this.labDirOfExe.Size = new System.Drawing.Size(110, 13);
            this.labDirOfExe.TabIndex = 21;
            this.labDirOfExe.Text = "Trace Log File to use:";
            // 
            // traceDirTextBox
            // 
            this.traceDirTextBox.Location = new System.Drawing.Point(121, 163);
            this.traceDirTextBox.Name = "traceDirTextBox";
            this.traceDirTextBox.Size = new System.Drawing.Size(571, 20);
            this.traceDirTextBox.TabIndex = 22;
            // 
            // btnReportTraceTextboxStats
            // 
            this.btnReportTraceTextboxStats.Location = new System.Drawing.Point(170, 189);
            this.btnReportTraceTextboxStats.Name = "btnReportTraceTextboxStats";
            this.btnReportTraceTextboxStats.Size = new System.Drawing.Size(148, 27);
            this.btnReportTraceTextboxStats.TabIndex = 23;
            this.btnReportTraceTextboxStats.Text = "Report Trace Textbox Stats";
            this.btnReportTraceTextboxStats.UseVisualStyleBackColor = true;
            this.btnReportTraceTextboxStats.Click += new System.EventHandler(this.btnReportTraceTextboxStats_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.CheckFileExists = false;
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnBrowseForTraceLogFile
            // 
            this.btnBrowseForTraceLogFile.Location = new System.Drawing.Point(8, 189);
            this.btnBrowseForTraceLogFile.Name = "btnBrowseForTraceLogFile";
            this.btnBrowseForTraceLogFile.Size = new System.Drawing.Size(147, 27);
            this.btnBrowseForTraceLogFile.TabIndex = 24;
            this.btnBrowseForTraceLogFile.Text = "Browse for TraceLogFile";
            this.btnBrowseForTraceLogFile.UseVisualStyleBackColor = true;
            this.btnBrowseForTraceLogFile.Click += new System.EventHandler(this.btnBrowseForTraceLogFile_Click);
            // 
            // cbLogTraceDataToFile
            // 
            this.cbLogTraceDataToFile.AutoSize = true;
            this.cbLogTraceDataToFile.Location = new System.Drawing.Point(522, 33);
            this.cbLogTraceDataToFile.Name = "cbLogTraceDataToFile";
            this.cbLogTraceDataToFile.Size = new System.Drawing.Size(136, 17);
            this.cbLogTraceDataToFile.TabIndex = 25;
            this.cbLogTraceDataToFile.Text = "Log Trace Data To File";
            this.cbLogTraceDataToFile.UseVisualStyleBackColor = true;
            this.cbLogTraceDataToFile.CheckedChanged += new System.EventHandler(this.cbLogTraceDataToFile_CheckedChanged);
            // 
            // lblTraceTextBoxStats
            // 
            this.lblTraceTextBoxStats.AutoSize = true;
            this.lblTraceTextBoxStats.Location = new System.Drawing.Point(330, 200);
            this.lblTraceTextBoxStats.Name = "lblTraceTextBoxStats";
            this.lblTraceTextBoxStats.Size = new System.Drawing.Size(108, 13);
            this.lblTraceTextBoxStats.TabIndex = 26;
            this.lblTraceTextBoxStats.Text = "lblTraceTextBoxStats";
            this.lblTraceTextBoxStats.Click += new System.EventHandler(this.lblTraceTextBoxStats_Click);
            // 
            // btnEraseLogFile
            // 
            this.btnEraseLogFile.Location = new System.Drawing.Point(12, 74);
            this.btnEraseLogFile.Name = "btnEraseLogFile";
            this.btnEraseLogFile.Size = new System.Drawing.Size(173, 27);
            this.btnEraseLogFile.TabIndex = 27;
            this.btnEraseLogFile.Text = "Erase Logfile";
            this.btnEraseLogFile.UseVisualStyleBackColor = true;
            this.btnEraseLogFile.Click += new System.EventHandler(this.btnEraseLogFile_Click);
            // 
            // cmbMaxTraceTextBoxLines
            // 
            this.cmbMaxTraceTextBoxLines.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMaxTraceTextBoxLines.FormattingEnabled = true;
            this.cmbMaxTraceTextBoxLines.Items.AddRange(new object[] {
            "100",
            "200",
            "500",
            "1000",
            "2000",
            "5000"});
            this.cmbMaxTraceTextBoxLines.Location = new System.Drawing.Point(378, 89);
            this.cmbMaxTraceTextBoxLines.Name = "cmbMaxTraceTextBoxLines";
            this.cmbMaxTraceTextBoxLines.Size = new System.Drawing.Size(115, 21);
            this.cmbMaxTraceTextBoxLines.TabIndex = 28;
            this.cmbMaxTraceTextBoxLines.SelectedIndexChanged += new System.EventHandler(this.cmbMaxTraceTextBoxLines_SelectedIndexChanged);
            // 
            // lblLinesNowInTraceTextbox
            // 
            this.lblLinesNowInTraceTextbox.AutoSize = true;
            this.lblLinesNowInTraceTextbox.Location = new System.Drawing.Point(577, 122);
            this.lblLinesNowInTraceTextbox.Name = "lblLinesNowInTraceTextbox";
            this.lblLinesNowInTraceTextbox.Size = new System.Drawing.Size(115, 13);
            this.lblLinesNowInTraceTextbox.TabIndex = 29;
            this.lblLinesNowInTraceTextbox.Text = "[Lines In TextBox now]";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(375, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 13);
            this.label1.TabIndex = 30;
            this.label1.Text = "Maximum Lines In Window";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(212, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "Poll Rate";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // AdkTraceViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(712, 547);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblLinesNowInTraceTextbox);
            this.Controls.Add(this.cmbMaxTraceTextBoxLines);
            this.Controls.Add(this.btnEraseLogFile);
            this.Controls.Add(this.lblTraceTextBoxStats);
            this.Controls.Add(this.cbLogTraceDataToFile);
            this.Controls.Add(this.btnBrowseForTraceLogFile);
            this.Controls.Add(this.btnReportTraceTextboxStats);
            this.Controls.Add(this.traceDirTextBox);
            this.Controls.Add(this.labDirOfExe);
            this.Controls.Add(this.labCurTimerInterval);
            this.Controls.Add(this.btnClearTraceTextBox);
            this.Controls.Add(this.btnStartRdTraceStop);
            this.Controls.Add(this.lblLastError);
            this.Controls.Add(this.cmbTracePollFreq);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.cmbWhichDeviceToTrace);
            this.Controls.Add(this.chbShowDeltaT);
            this.Controls.Add(this.tbTraceDataDisplay);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AdkTraceViewForm";
            this.Text = "AdkTraceViewForm";
            this.Load += new System.EventHandler(this.AdkTraceViewForm_Load);
            this.Activated += new System.EventHandler(this.AdkTraceViewForm_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AdkTraceViewForm_FormClosed);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdkTraceViewForm_FormClosing);
            this.Resize += new System.EventHandler(this.AdkTraceViewForm_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbTraceDataDisplay;
        private System.Windows.Forms.ComboBox cmbTracePollFreq;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox cmbWhichDeviceToTrace;
        private System.Windows.Forms.CheckBox chbShowDeltaT;
        private System.Windows.Forms.Label lblLastError;
        private System.Windows.Forms.Button btnStartRdTraceStop;
        private System.Windows.Forms.Timer timerTracePolling;
        private System.Windows.Forms.Button btnClearTraceTextBox;
        private System.Windows.Forms.Label labCurTimerInterval;
        private System.Windows.Forms.Label labDirOfExe;
        private System.Windows.Forms.TextBox traceDirTextBox;
        private System.Windows.Forms.Button btnReportTraceTextboxStats;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnBrowseForTraceLogFile;
        private System.Windows.Forms.CheckBox cbLogTraceDataToFile;
        private System.Windows.Forms.Label lblTraceTextBoxStats;
        private System.Windows.Forms.Button btnEraseLogFile;
        private System.Windows.Forms.ComboBox cmbMaxTraceTextBoxLines;
        private System.Windows.Forms.Label lblLinesNowInTraceTextbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}