//
// This file contains the public class RegistersForm, which provides the GUI
// for direct access to ZL7010X registers.
// 
// The form itself contains one pre-drawn register control block for development
// purposes only. This control is not visible when running  the actual application.
// All register controls used during the application execution are drawn dynamically
// and based upon text files which describe the registers, as follows:
//
//     ZL70101 page 1 registers: ZL70101RegistersPage1.txt
//     ZL70101 page 2 registers: ZL70101RegistersPage2.txt
//     ZL70102 page 1 registers: ZL70102RegistersPage1.txt
//     ZL70102 page 2 registers: ZL70102RegistersPage2.txt
//     ZL70103 page 1 registers: ZL70103RegistersPage1.txt
//     ZL70103 page 2 registers: ZL70103RegistersPage2.txt
// 
// Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2012. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;                        
using System.Globalization;             
using System.Runtime.InteropServices;
using Zarlink.Adp.General;
using Zarlink.Adk.Api;

namespace Zarlink.Adk.Forms
{
    public partial class RegistersForm : Form
    {
        // if ture, the character pressed is ignored in the key down event
        private bool isInvalidKey = false;
        
        // if true, processing of the next event change is bypassed
        private bool ignoreChangedEvent = false;
        
        // true if displaying registers for an implant (not base station)
        private bool isImplant = false;
        //
        // True if displaying registers for a remote implant being accessed via
        // the radio (ZL7010X).
        //
        private bool remote = false;
        
        private CommonApi.MICS_REV micsRev = CommonApi.MICS_REV.ZL70102;

        private RegDescForm regDescForm = null;

        private CommonApi adkMod;

        private bool initialized = false;

        private const int MAX_REGISTERS = 128;
        private const int MAX_PAGES = 2;
        // 
        // Current value table for register values. Stores the register value before a write
        // is selected and after a read.
        //
        private byte[,] registers = new byte[MAX_PAGES, MAX_REGISTERS];

        private bool ignoreTextChangedEvent = false;

        // used to hold register info read from register description file(s)
        private RegInfo regInfo;

        private TextBox txtErrBox = null;

        // stream writer used to save register settings to a file
        private StreamWriter regFileWriter = null;

        private REG_FORM[,] regForm = new REG_FORM[MAX_PAGES, MAX_REGISTERS];
        
        // Represents a "collection" of form controls for modifying register values.
        //
        private struct REG_FORM
        {
            public string regName;
            public GroupBox regGroup;
            public GroupBox regBitsGroup;
            public TextBox[] regBits;
            public TextBox regVal;
            public Button regWr;
            public Button regRd;

            public REG_FORM(string name)
            {
                regName = name;
                regGroup = new System.Windows.Forms.GroupBox();
                regBitsGroup = new System.Windows.Forms.GroupBox();
                regBits = new TextBox[8]; 

                // create one textbox representing each of the eight bits of a register
                for(int i = 0; i < 8; i++) {
                    regBits[i] = new System.Windows.Forms.TextBox();
                }

                regVal = new System.Windows.Forms.TextBox();
                regWr = new System.Windows.Forms.Button();
                regRd = new System.Windows.Forms.Button();
            }
        }

        public RegistersForm(bool isImplant, CommonApi adkMod, bool remote, TextBox txtErrBox, CommonApi.MICS_REV micsRev)
        {
            this.remote = remote;
            this.txtErrBox = txtErrBox;
            this.isImplant = isImplant;
            this.adkMod = adkMod;
            this.micsRev = micsRev;
            
            this.regInfo = new RegInfo(micsRev);

            InitializeComponent();

            drawRegisters();

            if (adkMod != null) initialized = true;
            if (remote) btnRegWrAll.Enabled = false;
        }

        // Creates the controls used to modify the register values
        //
        private void drawRegisters()
        {
            RegInfo.REG_INFO[,] regInfo = this.regInfo.regInfo;
            const int spacer = 8;
            const int startX = 8;
            const int startY = 12;
            
            // Init the model to use to create the display for each register.
            // In the Visual Studio display designer, the model appears on the
            // first tab in the "RegistersForm" form (object "grpRegModel").
            // During development, the display designer can be used to modify
            // the model visually, and during runtime, the display for each
            // register is based on it (the model itself is hidden).
            //
            REG_FORM model = new REG_FORM("model");
            model.regGroup = grpRegModel;
            model.regBitsGroup = grpRegBitsModel;
            model.regBits[0] = txtRegBit0Model;
            model.regBits[1] = txtRegBit1Model;
            model.regBits[2] = txtRegBit2Model;
            model.regBits[3] = txtRegBit3Model;
            model.regBits[4] = txtRegBit4Model;
            model.regBits[5] = txtRegBit5Model;
            model.regBits[6] = txtRegBit6Model;
            model.regBits[7] = txtRegBit7Model;
            model.regVal = txtRegValModel;
            model.regRd = btnRegRdModel;
            model.regWr = btnRegWrModel;
            
            System.Windows.Forms.Control.ControlCollection tab = this.grpRegMac1.Controls;

            // Iterate through registers across all pages and create REG_FORM struct to
            // represent register form controls.
            //
            for(int page = 0, reg = 0; page < MAX_PAGES; reg++) {
            
                regForm[page, reg] = new REG_FORM("register" + reg.ToString());
                
                if (reg == (MAX_REGISTERS-1)) {
                    reg = -1;
                    page++;
                }
            }

            // if ZL70101
            if (micsRev <= CommonApi.MICS_REV.ZL70101) {
            
                // remove tabs not needed for ZL70101
                this.tabReg.Controls.RemoveAt(7);
                this.tabReg.Controls.RemoveAt(6);
                
                this.tabRegP2Mac1.Text = "(0-69) Test";
                this.tabRegP2Mac2.Text = "(96-115) Test";
            }

            this.tabReg.SuspendLayout();
            this.tabRegMac1.SuspendLayout();
            this.tabRegMac2.SuspendLayout();
            this.tabRegWake.SuspendLayout();
            this.tabRegRF.SuspendLayout();
            this.tabRegP2Mac1.SuspendLayout();
            this.tabRegP2Mac2.SuspendLayout();
            this.tabRegP2Wake.SuspendLayout();
            this.tabRegP2RF.SuspendLayout();
            
            this.grpRegMac1.SuspendLayout();
            this.grpRegMac2.SuspendLayout();
            this.grpRegWake.SuspendLayout();
            this.grpRegRF.SuspendLayout();
            this.grpRegP2Mac1.SuspendLayout();
            this.grpRegP2Mac2.SuspendLayout();
            this.grpRegP2Wake.SuspendLayout();
            this.grpRegP2RF.SuspendLayout();

            for(int page = 0, reg = 0, c = 0, u = startX, v = startY; 
                page < MAX_PAGES; reg++, v+=42, c++) {
                
                if (c == 8) {
                    u += model.regGroup.Size.Width + spacer; v = startY; c = 0;
                }

                if (page == 0) {
                
                    switch (reg) {
                    case 0:
                        break;
                    case 32:
                        tab = this.grpRegMac2.Controls;
                        u = startX; v = startY;
                        break;
                    case 64:
                        tab = this.grpRegWake.Controls;
                        u = startX; v = startY;
                        break;
                    case 96:
                        tab = this.grpRegRF.Controls;
                        u = startX; v = startY;
                        break;
                    case 128:
                        tab = this.grpRegP2Mac1.Controls;
                        u = startX; v = startY;
                        page = 1;
                        //
                        // if for remote ZL70101, skip test registers (can't access remotely)
                        if (remote && (micsRev <= CommonApi.MICS_REV.ZL70101)) {
                            page = MAX_PAGES;
                        }
                        reg = 0;
                        break;
                    default:
                        break;
                    }
                    
                } else { // page 1
                
                    // if ZL70102/103
                    if (micsRev > CommonApi.MICS_REV.ZL70101) {
                    
                        switch (reg) {
                        case 0:
                            break;
                        case 32:
                            tab = this.grpRegP2Mac2.Controls;
                            u = startX; v = startY;
                            break;
                        case 64:
                            tab = this.grpRegP2Wake.Controls;
                            u = startX; v = startY;
                            break;
                        case 96:
                            tab = this.grpRegP2RF.Controls;
                            u = startX; v = startY;
                            break;
                        case 128:
                            page = MAX_PAGES; // no more pages
                            break;
                        default:
                            break;
                        }
                        
                    } else { // ZL70101
                    
                        switch (reg) {
                        case 0:
                            break;
                        case 15:
                            reg = 43;
                            break;
                        case 44:
                            reg = 64;
                            break;
                        case 70:
                            tab = this.grpRegP2Mac2.Controls;
                            u = startX; v = startY; reg = 96; c = 0;
                            break;
                        case 116:
                            page = MAX_PAGES; // no more pages
                            break;
                        default:
                            break;
                        }
                    }
                }

                // if no more pages, stop
                if (page >= MAX_PAGES) break;

                regForm[page, reg].regGroup.SuspendLayout();
                regForm[page, reg].regBitsGroup.SuspendLayout();
                regForm[page, reg].regGroup.SuspendLayout();
                this.SuspendLayout();

                // add register controls to the grouping container
                tab.Add(regForm[page, reg].regGroup);
                regForm[page, reg].regGroup.Controls.Add(regForm[page, reg].regBitsGroup);
                regForm[page, reg].regGroup.Controls.Add(regForm[page, reg].regVal);
                regForm[page, reg].regGroup.Controls.Add(regForm[page, reg].regWr);
                regForm[page, reg].regGroup.Controls.Add(regForm[page, reg].regRd);
                regForm[page, reg].regGroup.Location = new System.Drawing.Point(u, v);
                regForm[page, reg].regGroup.Margin = model.regGroup.Margin;
                regForm[page, reg].regGroup.Name = regForm[page, reg].regName;
                regForm[page, reg].regGroup.Padding = model.regGroup.Padding;
                regForm[page, reg].regGroup.Size = model.regGroup.Size;
                regForm[page, reg].regGroup.TabIndex = 0;
                regForm[page, reg].regGroup.TabStop = false;
                regForm[page, reg].regGroup.Text = regForm[page, reg].regName;

                regForm[page, reg].regGroup.Tag = new int();
                if (page == 0) {
                    regForm[page, reg].regGroup.Tag = reg;
                } else {
                    regForm[page, reg].regGroup.Tag = (int)(CommonApi.P2 | reg);
                }
                regForm[page, reg].regGroup.DoubleClick += new System.EventHandler(this.txtReg_DoubleClick);
                regForm[page, reg].regGroup.Text = reg.ToString() + " " + regInfo[page, reg].Name;

                for(int j = 0; j < 8; j++) {
                    regForm[page, reg].regBitsGroup.Controls.Add(regForm[page, reg].regBits[j]);
                }

                regForm[page, reg].regBitsGroup.Location = model.regBitsGroup.Location;
                regForm[page, reg].regBitsGroup.Margin = model.regBitsGroup.Margin;
                regForm[page, reg].regBitsGroup.Name = "grpRegBits" + reg.ToString();
                regForm[page, reg].regBitsGroup.Padding = model.regBitsGroup.Padding;
                regForm[page, reg].regBitsGroup.Size = model.regBitsGroup.Size;
                regForm[page, reg].regBitsGroup.Font = model.regBitsGroup.Font;
                regForm[page, reg].regBitsGroup.TabIndex = 0;
                regForm[page, reg].regBitsGroup.TabStop = false;

                // Iterate through the set of all textboxes corresponding to register bits
                // and set textbox properties.
                //
                for(int j = 0; j < 8; j++) {
                
                    TextBox tmpRegBit = regForm[page, reg].regBits[j];
                    
                    tmpRegBit.Font = model.regBits[j].Font;
                    tmpRegBit.ForeColor = System.Drawing.SystemColors.WindowText;
                    tmpRegBit.Location = model.regBits[j].Location;
                    tmpRegBit.Margin = model.regBits[j].Margin;
                    tmpRegBit.MaxLength = model.regBits[j].MaxLength;
                    tmpRegBit.Name = "txtRegBit" + reg.ToString();
                    tmpRegBit.Size = model.regBits[j].Size;
                    tmpRegBit.TabIndex = 0;
                    tmpRegBit.Text = "0";
                    tmpRegBit.Click += new System.EventHandler(this.txtRegBit_Click);
                    tmpRegBit.Enabled = (j < (8 - regInfo[page, reg].Bits)) ? false : true;

                    // prevent direct editing of bit register textboxes
                    tmpRegBit.KeyPress += new KeyPressEventHandler(ignoreEntry_KeyPress);    
                }

                // txtRegVal
                // 
                regForm[page, reg].regVal.CharacterCasing = model.regVal.CharacterCasing;
                regForm[page, reg].regVal.Font = model.regVal.Font;
                regForm[page, reg].regVal.Location = model.regVal.Location;
                regForm[page, reg].regVal.Margin = model.regVal.Margin;
                regForm[page, reg].regVal.MaxLength = model.regVal.MaxLength;
                regForm[page, reg].regVal.Name = "txtRegVal" + reg.ToString();
                regForm[page, reg].regVal.Size = model.regVal.Size;
                regForm[page, reg].regVal.TabIndex = 8;
                regForm[page, reg].regVal.Text = "00";
                regForm[page, reg].regVal.TextAlign = model.regVal.TextAlign;
                regForm[page, reg].regVal.Enter += new System.EventHandler(this.txtRegVal_Enter);
                regForm[page, reg].regVal.Leave += new System.EventHandler(this.txtRegVal_Leave);
                regForm[page, reg].regVal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_KeyDown);
                regForm[page, reg].regVal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_KeyPress);

                // btnRegRd
                // 
                regForm[page, reg].regRd.Font = model.regRd.Font;
                regForm[page, reg].regRd.Location = model.regRd.Location;
                regForm[page, reg].regRd.Margin = model.regRd.Margin;
                regForm[page, reg].regRd.Name = "btnRegRd";
                regForm[page, reg].regRd.Size = model.regRd.Size;
                regForm[page, reg].regRd.TabIndex = 9;
                regForm[page, reg].regRd.Text = "R";
                regForm[page, reg].regRd.UseVisualStyleBackColor = true;
                regForm[page, reg].regRd.Click += new System.EventHandler(this.btnRegRd_Click);
                regForm[page, reg].regRd.Enabled = regInfo[page, reg].read;

                // btnRegWr
                // 
                regForm[page, reg].regWr.Font = model.regWr.Font;
                regForm[page, reg].regWr.Location = model.regWr.Location;
                regForm[page, reg].regWr.Margin = model.regWr.Margin;
                regForm[page, reg].regWr.Name = "btnRegWr";
                regForm[page, reg].regWr.Size = model.regWr.Size;
                regForm[page, reg].regWr.TabIndex = 10;
                regForm[page, reg].regWr.Text = "W";
                regForm[page, reg].regWr.UseVisualStyleBackColor = true;
                regForm[page, reg].regWr.Click += new System.EventHandler(this.btnRegWr_Click);
                regForm[page, reg].regWr.Enabled = regInfo[page, reg].write;
                
                // If it's the base station company ID register, override the
                // register's normal display settings (derived from the register
                // description file) to disable the register's bit text boxes,
                // value text box, and write button. This is done so the ADK as
                // shipped can't be used to wake implants for other companys.
                //
                if (!isImplant && (page == 0) && (reg == 22)) {
                    for(int j = 0; j < 8; j++) regForm[page, reg].regBits[j].Enabled = false;
                    regForm[page, reg].regVal.Enabled = false;
                    regForm[page, reg].regWr.Enabled = false;
                }
            }

            this.tabReg.ResumeLayout(false);
            this.tabRegMac1.ResumeLayout(false);
            this.tabRegMac2.ResumeLayout(false);
            this.tabRegWake.ResumeLayout(false);
            this.tabRegRF.ResumeLayout(false);
            this.tabRegP2Mac1.ResumeLayout(false);
            this.tabRegP2Mac2.ResumeLayout(false);
            this.tabRegP2Wake.ResumeLayout(false);
            this.tabRegP2RF.ResumeLayout(false);
            this.grpRegMac1.ResumeLayout(false);
            this.grpRegMac2.ResumeLayout(false);
            this.grpRegWake.ResumeLayout(false);
            this.grpRegRF.ResumeLayout(false);
            this.grpRegP2Mac1.ResumeLayout(false);
            this.grpRegP2Mac2.ResumeLayout(false);
            this.grpRegP2Wake.ResumeLayout(false);
            this.grpRegP2RF.ResumeLayout(false);
            this.grpRegRadix.ResumeLayout(false);
            this.grpRegRadix.PerformLayout();
            this.grpRegControls.ResumeLayout(false);
            this.grpRegControls.PerformLayout();
            this.grpRegAccess.ResumeLayout(false);
            this.grpRegAccess.PerformLayout();
            this.ResumeLayout(false);
        }

        private void txt_KeyDown(object sender, KeyEventArgs e)
        {
            isInvalidKey = false;
            
            if (btnRegHex.Checked) {
                isInvalidKey = Util.isInvalidHexKey(e, false);
            } else {
                isInvalidKey = Util.isInvalidIntegerKey(e, false);
            }
        }

        private void txt_KeyPress(object sender, KeyPressEventArgs e)
        {
            // check for the flag being set in the KeyDown event
            if (isInvalidKey == true) {
            
                // stop the character from being entered into the control since it is non-numerical
                e.Handled = true;
                isInvalidKey = false;
            }
        }
        
        private void txtHex_KeyDown(object sender, KeyEventArgs e)
        {
            isInvalidKey = false;

            isInvalidKey = Util.isInvalidHexKey(e, false);
        }

        private void txtInteger_KeyDown(object sender, KeyEventArgs e)
        {
            isInvalidKey = false;

            isInvalidKey = Util.isInvalidIntegerKey(e, false);
        }

        // This modifies the textboxes that show each bit value for a register
        // when the register value changes. The "val" parameter is the new
        // register value.
        //
        private void regDisplay(GroupBox grpRegs, byte val, Color color)
        {
            int i = 7;
            string newVal;

            foreach (object valBox in grpRegs.Controls) {
            
                if (valBox is GroupBox) {
                
                    foreach (TextBox txtBox in ((GroupBox)valBox).Controls) {
                    
                        newVal = ((val & (byte)(1 << i--)) > 0) ? "1" : "0";
                        
                        if (txtBox.Text != newVal) {
                            txtBox.Text = newVal;
                            txtBox.ForeColor = color;
                        }
                    }
                }
                
                if (valBox is TextBox) {
                
                    if (btnRegHex.Checked) {
                        ((TextBox)valBox).Text = val.ToString("X2"); 
                    } else {
                        ((TextBox)valBox).Text = val.ToString();
                    }
                    ((TextBox)valBox).ForeColor = color;
                }
            }
        }

        // This modifies the textboxes representing bit values when the R or W
        // button is clicked.
        //
        private void colorRegsOnButtonPush(Button p, Color c)
        {
            GroupBox grpRegs = (GroupBox)p.Parent;

            foreach (object valBox in grpRegs.Controls) {
            
                if (valBox is GroupBox) {
                
                    foreach (TextBox txtBox in ((GroupBox)valBox).Controls) {
                        txtBox.ForeColor = c;
                    }
                }
                
                if (valBox is TextBox) {
                    ((TextBox)valBox).ForeColor = c;
                }
            }
        }

        // This hides/displays form controls.
        //
        public void regFormEnable(bool enable)
        {
            if (enable) {
                tabReg.Enabled = true;
                grpRegControls.Enabled = true;
            } else {
                tabReg.Enabled = false;
                grpRegControls.Enabled = false;
            }
        }

        // This retrieves the value of the specified register.
        //
        public byte getRegister(uint register)
        {
            uint page = ((register & CommonApi.P2) != 0) ? 1U : 0U;
            uint reg = register & 0x7F;
            
            return(registers[page, reg]);
        }

        // This sets the specified register to the specified value.
        //
        public void setRegister(uint register, byte val)
        {
            uint page = ((register & CommonApi.P2) != 0) ? 1U : 0U;
            uint reg = register & 0x7F;
            
            if (regFileWriter != null) {
            
                if (page == 0) {
                    regFileWriter.WriteLine("Page1(dec): " + reg.ToString() +
                        ", Data(hex): 0x" + val.ToString("X2"));
                } else {
                    regFileWriter.WriteLine("Page2(dec): " + reg.ToString() +
                        ", Data(hex): 0x" + val.ToString("X2"));
                }
            }
            registers[page, reg] = val;
        }

        // This modifies the register value when a bit textbox value is toggled.
        //
        private byte regBinaryChange(TextBox p)
        {
            GroupBox grpBits = (GroupBox) p.Parent;
            GroupBox grpRegs = (GroupBox) grpBits.Parent;

            byte val = 0;
            int i = 7;

            // toggle the textbox value
            if (p.Text == "0") {
                p.Text = "1";
            } else {
                p.Text = "0";
            }

            p.ForeColor = Color.Red;

            // set the register value based on the value of all bit textboxes for the register            
            foreach (TextBox txtBox in grpBits.Controls) {
            
                if (txtBox.Text == "1") {
                    val += (byte) (1 << i);
                }
                i--;
            }

            // set the hex/dec value of the register value textbox
            foreach (object valBox in grpRegs.Controls) {
            
                if (valBox is TextBox) {
                
                    if (btnRegHex.Checked) {
                        ((TextBox)valBox).Text = val.ToString("X2");
                    } else {
                        ((TextBox)valBox).Text = val.ToString();
                    }
                    ((TextBox)valBox).ForeColor = Color.Red;
                }
            }

            return(val);
        }
        
        // This modifies the register value when a bit textbox value is toggled,
        // and sets the underlying value of register.
        //
        private void regBitChange(object sender, uint register)
        {
            TextBox p = (TextBox)sender;

            setRegister(register, regBinaryChange(p));
        }

        // This converts displayed decimal values to hexadecimal.
        //
        private void switchHex()
        {
            Color c;
            int val;

            TextBox tmpRegVal = null;

            // iterate through all registers
            foreach (REG_FORM register in regForm) {
            
                tmpRegVal = register.regVal;

                if (tmpRegVal.Text == "") continue;

                // store the font color momentarily
                c = tmpRegVal.ForeColor;

                tmpRegVal.MaxLength = 2;
                val = (int)Int32.Parse(tmpRegVal.Text, NumberStyles.Integer);

                // convert the decimal text to hexadecimal
                tmpRegVal.Text = val.ToString("X2");

                // TextChanged event caused by above will change the color to red,
                // so revert to correct color.
                //
                tmpRegVal.ForeColor = c;
            }
        }

        // This converts displayed hexadecimal values to decimal.
        //
        private void switchDec()
        {
            Color c;
            uint val = 0;

            TextBox tmpRegVal = null;

            // iterate through all registers
            foreach (REG_FORM register in regForm) {
            
                tmpRegVal = register.regVal;

                if (tmpRegVal.Text == "") continue;

                // store the font color momentarily
                c = tmpRegVal.ForeColor;

                tmpRegVal.MaxLength = 3;
                val = (uint)Int32.Parse(tmpRegVal.Text, NumberStyles.HexNumber);
                tmpRegVal.Text = val.ToString();

                // TextChanged event caused by above will change to color to red,
                // so revert to correct color.
                //
                tmpRegVal.ForeColor = c;
            }
        }

        // This reads/writes the abstract register values from/to the hardware
        // registers. If "write" is true, it writes to the hardware registers,
        // and if "write" is false, it reads from the hardware registers.
        //
        private int registerRdWr(bool write, GroupBox tabGrp)
        {
            Button btn;
            TextBox t;
            
            foreach (GroupBox grpBox in tabGrp.Controls) {
            
                foreach (object obj in grpBox.Controls) {
                
                    if (obj is Button) {
                    
                        btn = (Button)obj;
                        if (write) {
                        
                            if ((btn.Name == "btnRegWr") & btn.Enabled) {
                            
                                foreach (object o in grpBox.Controls) {
                                
                                    if (o is TextBox) {
                                    
                                        t = (TextBox) o;
                                        if (t.ForeColor == Color.Red) {
                                        
                                            if (regWriteButtonProcessing(btn, (uint)(((int)btn.Parent.Tag))) < 0) {
                                                return(-1);
                                            }
                                        }
                                    }
                                }
                            }
                            
                        } else {
                        
                            if ((btn.Name == "btnRegRd") & btn.Enabled) {
                            
                                if (regReadButtonProcessing(btn, (uint)(((int)btn.Parent.Tag))) < 0) {
                                    return(-1);
                                }
                            }
                        }
                    }
                }
            }
            
            return(0);
        }

        // This reads value from the specified hardware register and assigns the
        // value to the abstract register.
        //
        private int regReadButtonProcessing(Button p, uint register)
        {
            int val;
            string err = "";

            // if unable to read register, return error
            if ((val = adkMod.micsRead(register, remote, ref err)) < 0) {
            
                Util.displayError(txtErrBox, err);
                return(-1);
            }
            
            colorRegsOnButtonPush(p, Color.Black);
            
            // update register value in GUI
            setRegister(register, (byte)val);
            regDisplay((GroupBox)p.Parent, (byte)val, Color.Black);
            
            return(0);
        }

        // This writes the value from the specified abstract register to the
        // hardware register.
        //
        private int regWriteButtonProcessing(Button p, uint register)
        {
            byte val = 0;
            string err = "";

            // if unable to write new value to register, return error
            val = getRegister(register);
            if (adkMod.micsWrite(register, val, remote, ref err) < 0) {
            
                Util.displayError(txtErrBox, err);
                return(-1);
            }
            
            colorRegsOnButtonPush(p, Color.Black);
                
            // if modified register is on implant
            if (isImplant) {
            
                // If the ZL7010X includes the register in its wakeup stack,
                // copy registers to the wakeup stack so the ZL7010X will
                // preserve the register while it's sleeping. Else, if the
                // ZL7010X includes the register in its CRC, recalculate CRC.
                //
                if (adkMod.isStackReg(register, micsRev)) {
                
                    if (adkMod.copyMicsRegs(remote, ref err) < 0) {
                    
                        Util.displayError(txtErrBox, err);
                        return(-1);
                    }
                    
                } else if (adkMod.isCrcReg(register, micsRev)) {
                    
                    if (adkMod.calcMicsCrc(remote, ref err) < 0) {
                    
                        Util.displayError(txtErrBox, err);
                        return(-1);
                    }
                }
            }
            
            return(0);
        }

        private void btnRegHex_CheckedChanged(object sender, EventArgs e)
        {
            if (ignoreTextChangedEvent) {
                ignoreTextChangedEvent = false;
                return;
            }

            if (btnRegHex.Checked) {
                btnRegDec.Checked = false;
                switchHex();
            } else {
                btnRegDec.Checked = true;
                switchDec();
            }

            ignoreTextChangedEvent = true;
        }

        private void btnRegDec_CheckedChanged(object sender, EventArgs e)
        {
            if (ignoreTextChangedEvent) {
                ignoreTextChangedEvent = false;
                return;
            }
            
            if (btnRegDec.Checked) {
                btnRegHex.Checked = false;
                switchDec();
            } else {
                btnRegHex.Checked = true;
                switchHex();
            }

            ignoreTextChangedEvent = true;
        }

        private void btnRegAccessRead_Click(object sender, EventArgs e)
        {
            Button p = (Button)sender;
            string err = "";
            if (!initialized) return;

            uint addr = (uint)Int32.Parse(txtRegAccessAddr.Text, NumberStyles.Integer);
            int val = 0;

            val = adkMod.micsRead(addr, remote, ref err);
            if (val < 0) {
                Util.displayError(txtErrBox, err);
            } else {
                txtRegAccessValue.Text = val.ToString("X2");
            }
        }

        private void btnRegAccessWrite_Click(object sender, EventArgs e)
        {
            Button p = (Button)sender;
            string err = "";
            if (!initialized) return;

            uint addr = (uint)Int32.Parse(txtRegAccessAddr.Text, NumberStyles.Integer);
            uint val = 0;
            
            if (txtRegAccessValue.Text == "") {
            
                MessageBox.Show("Register Access Value entry is invalid. No write performed.");
                
            } else {
            
                val = (uint)Int32.Parse(txtRegAccessValue.Text, NumberStyles.HexNumber);

                // if the write fails, report error
                if (adkMod.micsWrite(addr, val, remote, ref err) < 0) {
                
                    Util.displayError(txtErrBox, err);
                    
                } else if (isImplant) { // else, if modified register is on implant
                
                    // If the ZL7010X includes the register in its wakeup stack,
                    // copy registers to the wakeup stack so the ZL7010X will
                    // preserve the register while it's sleeping. Else, if the
                    // ZL7010X includes the register in its CRC, recalculate CRC.
                    //
                    if (adkMod.isStackReg(addr, micsRev)) {
                    
                        if (adkMod.copyMicsRegs(remote, ref err) < 0) {
                            Util.displayError(txtErrBox, err);
                        }
                        
                    } else if (adkMod.isCrcReg(addr, micsRev)) {
                    
                        if (adkMod.calcMicsCrc(remote, ref err) < 0) {
                            Util.displayError(txtErrBox, err);
                        }
                    }
                }
            }
        }

        private void txtRegAccessAddr_TextChanged(object sender, EventArgs e)
        {
            TextBox p = (TextBox)sender;

            if (ignoreChangedEvent) {
                ignoreChangedEvent = false;
                return;
            }
            
            if (p.Text == "") {
                ignoreChangedEvent = true;
                p.Text = "0";
            }
            int addr = Int32.Parse(p.Text, NumberStyles.Integer);

            if ((addr > 127) || (addr < 1)) {
                MessageBox.Show("Warning: Address appears out of range");
            }
        }

        private void regDesc_FormClosed(object sender, EventArgs e)
        {
            regDescForm = null;
        }

        private void txtReg_DoubleClick(object sender, EventArgs e)
        {
            GroupBox p = (GroupBox)sender;
            regDescForm = new RegDescForm(p, this.regInfo);
            regDescForm.FormClosed += new System.Windows.Forms.FormClosedEventHandler(regDesc_FormClosed);
            regDescForm.Show();   
        }

        private void txtRegBit_Click(object sender, EventArgs e)
        {
            TextBox p = (TextBox)sender;
            if (!initialized) return;
            regBitChange(p, (uint)((int)p.Parent.Parent.Tag));
        }

        private void ignoreEntry_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void txtRegVal_Enter(object sender, EventArgs e)
        {
            TextBox p = (TextBox)sender;
            regValueProcessing_Enter(p, (uint)((int)p.Parent.Tag));
        }

        private void txtRegVal_Leave(object sender, EventArgs e)
        {
            TextBox p = (TextBox)sender;
            regValueProcessing_Leave(p, (uint)((int)p.Parent.Tag));
        }

        private void btnRegWr_Click(object sender, EventArgs e)
        {
            Button p = (Button)sender;
            if (!initialized) return;

            regWriteButtonProcessing(p, (uint)(((int)p.Parent.Tag)));
        }

        private void btnRegRd_Click(object sender, EventArgs e)
        {
            Button p = (Button)sender;
            if (!initialized) return;

            regReadButtonProcessing(p, (uint)(((int)p.Parent.Tag)));
        }

        private void btnRegRdAll_Click(object sender, EventArgs e)
        {
            if (!initialized) return;
            
            if (registerRdWr(false, grpRegMac1) < 0) return;
            if (registerRdWr(false, grpRegMac2) < 0) return;
            if (registerRdWr(false, grpRegWake) < 0) return;
            if (registerRdWr(false, grpRegRF) < 0) return;
            if (registerRdWr(false, grpRegP2Mac1) < 0) return;
            if (registerRdWr(false, grpRegP2Mac2) < 0) return;
            if (registerRdWr(false, grpRegP2Wake) < 0) return;
            if (registerRdWr(false, grpRegP2RF) < 0) return;

            // if "Save Registers to File" is checked, automatically uncheck it
            if (chbRegSave.Checked) chbRegSave.Checked = false;
        }

        private void btnRegWrAll_Click(object sender, EventArgs e)
        {
            if (!initialized) return;
            
            if (registerRdWr(true, grpRegMac1) < 0) return;
            if (registerRdWr(true, grpRegMac2) < 0) return;
            if (registerRdWr(true, grpRegWake) < 0) return;
            if (registerRdWr(true, grpRegRF) < 0) return;
            if (registerRdWr(true, grpRegP2Mac1) < 0) return;
            if (registerRdWr(true, grpRegP2Mac2) < 0) return;
            if (registerRdWr(true, grpRegP2Wake) < 0) return;
            if (registerRdWr(true, grpRegP2RF) < 0) return;
        }

        private void regValueProcessing_Enter(TextBox p, uint register)
        {
            p.Text = "";
        }

        private void regValueProcessing_Leave(TextBox p, uint register)
        {
            RegInfo.REG_INFO[,] regInfo = this.regInfo.regInfo;
            uint page = ((register & CommonApi.P2) != 0) ? 1U : 0U;
            uint reg = register & 0x7F;
            int maxVal = 0;
            int val = 0;

            maxVal = ((int)Math.Pow(2, regInfo[page, reg].Bits)) - 1;
            
            if (p.Text == "") {
                MessageBox.Show("A value must be entered, setting to zero.");
                p.Text = "0";
            }
            
            p.ForeColor = Color.Red;
            if (btnRegHex.Checked) {
            
                val = Int32.Parse(p.Text, NumberStyles.HexNumber);

                // prevent entering a value larger than the max for the register
                if (val > maxVal) {
                
                    string decMaxValText = maxVal.ToString();
                    string hexMaxValText = Int32.Parse(decMaxValText, NumberStyles.HexNumber).ToString();

                    MessageBox.Show("Value for this register must not exceed " + decMaxValText + ".");
                    val = 0;
                }
                
            } else {
            
                val = Int32.Parse(p.Text, NumberStyles.Integer);

                // prevent entering a value larger than the max for the register
                if (val > maxVal) {
                
                    string decMaxValText = maxVal.ToString();
                    string hexMaxValText = Int32.Parse(decMaxValText, NumberStyles.HexNumber).ToString();

                    MessageBox.Show("Value for this register must not exceed " + decMaxValText + ".");
                    val = 0;
                }
            }

            regDisplay((GroupBox)p.Parent, (byte)val, Color.Red);
            setRegister(register, (byte)val);
        }
        
        private void chbRegSave_CheckedChanged(object sender, EventArgs e)
        {
            if (chbRegSave.Checked) {
            
                if (isImplant) {
                    regFileWriter = new StreamWriter("AimRegisterDump.txt");
                } else {
                    regFileWriter = new StreamWriter("BsmRegisterDump.txt");
                }
                regFileWriter.AutoFlush = true;
                regFileWriter.WriteLine(DateTime.Now);
                
            } else if (regFileWriter != null) {
            
                regFileWriter.Close();
                regFileWriter = null;
            }
        }

        private void RegistersForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (regFileWriter != null) {
                regFileWriter.Close();
                regFileWriter = null;
            }
        }
        
        private void txtRegSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13) {

                string s;
                string regPage1;
                string regPage2;
                List<string> regFound = new List<string>();

                bool found = false;

                char[] regDelimiter = new char[] { '#' };
                char[] fieldDelimiter = new char[] { '@' };

                if (micsRev > CommonApi.MICS_REV.ZL70102) { // ZL70103
                    regPage1 = "ZL70103RegistersPage1.txt";  
                    regPage2 = "ZL70103RegistersPage2.txt"; 
                }  else if (micsRev > CommonApi.MICS_REV.ZL70101) { // ZL70102
                    regPage1 = "ZL70102RegistersPage1.txt";
                    regPage2 = "ZL70102RegistersPage2.txt";
                } else {  // ZL70101
                    regPage1 = "ZL70101RegistersPage1.txt";
                    regPage2 = "ZL70101RegistersPage2.txt";
                }

                for(int j = 1; j < 3; j++) {
                
                    if (j < 2) {
                    
                        try {
                            s = File.ReadAllText(regPage1);
                        } catch (FileNotFoundException) {
                            MessageBox.Show("The register description file (" + regPage1 + ") was not found.");
                            return;
                        }
                        
                    } else {
                    
                        try {
                            s = File.ReadAllText(regPage2);
                        } catch (FileNotFoundException) {
                            MessageBox.Show("The register description file (" + regPage2 + ") was not found.");
                            return;
                        }
                    }

                    // eliminate initial "#" character
                    if (s.StartsWith("#")) s = s.Remove(0, 1);

                    // split string into an array of register info strings
                    String[] regs = s.Split(regDelimiter);

                    for(int i = 0; i < regs.GetLength(0); i++) {
                    
                        // split register info string into an array of field info strings
                        String[] fields = regs[i].Split(fieldDelimiter);

                        if (fields[3].Contains(txtRegSearch.Text)) {
                        
                            // Remove the /n and /r characters at the end of these fields
                            // so it will display correct in the message box.
                            //
                            fields[3] = fields[3].Remove(fields[3].Length - 2, 2);
                            fields[0] = fields[0].Remove(fields[0].Length - 2, 2);
                            regFound.Add("Page " + j.ToString() + " : Addr " + fields[0] + " : " + fields[3]);
                            found = true;
                        }
                    }
                }

                if (found) {
                
                    string toDisplay = string.Join(Environment.NewLine, regFound);

                    Form registerSearch = new Form();
                    TextBox searchResultTextBox = new System.Windows.Forms.TextBox();

                    searchResultTextBox.Multiline = true;
                    searchResultTextBox.ScrollBars = ScrollBars.Vertical;
                    searchResultTextBox.Size = new Size(300, 300);
                    searchResultTextBox.Text = toDisplay;
                    registerSearch.Controls.Add(searchResultTextBox);
                    registerSearch.Text = "Reg Search Result";
                    registerSearch.Size = new Size(318, 340);
                    registerSearch.Show();
                    searchResultTextBox.DeselectAll();
                    
                } else {
                    MessageBox.Show("Register not found.  Verify portion of register is spelled correct");
                }

             }
        }

        private void txtRegSearch_Click(object sender, EventArgs e)
        {
            txtRegSearch.SelectAll();
        }
    }

    // Used to hold the information read from register description file(s).
    //
    public class RegInfo
    {
        // Structure used to hold the information for a register.
        //
        public struct REG_INFO
        {
            public int Addr;
            public String Name;
            public String Description;
            public String Type;
            public bool read;
            public bool write;
            public int Bits;
            public string ResetVal;
            public string Crc;
            public string Category;

            public REG_INFO(String[] fields)
            {
                Addr = Int32.Parse(fields[0]);
                Name = fields[3];
                Description = fields[4];
                Type = fields[5];
                read = fields[5].Contains("R");
                write = fields[5].Contains("W");
                Bits = Int32.Parse(fields[6]);
                ResetVal = fields[9];
                Crc = fields[10];
                Category = fields[11];
            }
        }

        // array containing a REG_INFO structure for each register
        public REG_INFO[,] regInfo = new REG_INFO[2, 150];

        public RegInfo(CommonApi.MICS_REV micsRev)
        {
            if (micsRev > CommonApi.MICS_REV.ZL70102) {  //ZL70103
                initRegInfo(0, "ZL70103RegistersPage1.txt");
                initRegInfo(1, "ZL70103RegistersPage2.txt");
            } else if (micsRev > CommonApi.MICS_REV.ZL70101) {  //ZL70102
                initRegInfo(0, "ZL70102RegistersPage1.txt");
                initRegInfo(1, "ZL70102RegistersPage2.txt");
            } else { // ZL70101
                initRegInfo(0, "ZL70101RegistersPage1.txt");
                initRegInfo(1, "ZL70101RegistersPage2.txt");
            }
        }
        
        private void initRegInfo(int page, string filename)
        {
            string s;
            char[] regDelimiter = new char[] { '#' };
            char[] fieldDelimiter = new char[] { '@' };
        
            // read register description info from file
            try {
                s = File.ReadAllText(filename);
                
            } catch (FileNotFoundException) {
                MessageBox.Show("The register description file (" + filename + ") was not found.");
                return;
            }

            // eliminate initial "#" character
            if (s.StartsWith("#")) s = s.Remove(0, 1);

            // split string into an array of register info strings
            String[] regs = s.Split(regDelimiter);

            // for each register info string
            for(int i = 0; i < regs.GetLength(0); i++) {
            
                // split register info string into an array of field info strings
                String[] fields = regs[i].Split(fieldDelimiter);

                // create structure containing info for register
                REG_INFO ri = new REG_INFO(fields);
                this.regInfo[page, ri.Addr] = ri;
            }
        }
    }
}
