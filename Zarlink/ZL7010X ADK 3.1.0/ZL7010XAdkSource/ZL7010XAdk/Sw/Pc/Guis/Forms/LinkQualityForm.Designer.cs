//
// Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2012. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.

namespace Zarlink.Adk.Forms
{
    partial class LinkQualityForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LinkQualityForm));
            this.grpImLinkQual = new System.Windows.Forms.GroupBox();
            this.txtImDataBpsT = new System.Windows.Forms.TextBox();
            this.btnImMaxBerrIntsT = new System.Windows.Forms.Button();
            this.btnImMaxBerrIntsP = new System.Windows.Forms.Button();
            this.txtImRxBlockCountT = new System.Windows.Forms.TextBox();
            this.txtImTxBlockCountT = new System.Windows.Forms.TextBox();
            this.txtImEccBlocksT = new System.Windows.Forms.TextBox();
            this.txtImCrcErrorsT = new System.Windows.Forms.TextBox();
            this.lblImTotal = new System.Windows.Forms.Label();
            this.lblImPollPeriod = new System.Windows.Forms.Label();
            this.lblImDataBps = new System.Windows.Forms.Label();
            this.txtImDataBpsP = new System.Windows.Forms.TextBox();
            this.lblImRxBlockCount = new System.Windows.Forms.Label();
            this.txtImRxBlockCountP = new System.Windows.Forms.TextBox();
            this.lblImTxBlockCount = new System.Windows.Forms.Label();
            this.txtImTxBlockCountP = new System.Windows.Forms.TextBox();
            this.lblImEccBlocks = new System.Windows.Forms.Label();
            this.txtImEccBlocksP = new System.Windows.Forms.TextBox();
            this.lblImCrcErrors = new System.Windows.Forms.Label();
            this.txtImCrcErrorsP = new System.Windows.Forms.TextBox();
            this.lblImMaxBerrInts = new System.Windows.Forms.Label();
            this.grpBsmLinkQual = new System.Windows.Forms.GroupBox();
            this.txtBsmDataBpsT = new System.Windows.Forms.TextBox();
            this.btnBsmMaxRetriesT = new System.Windows.Forms.Button();
            this.btnBsmMaxRetriesP = new System.Windows.Forms.Button();
            this.btnBsmMaxBerrIntsT = new System.Windows.Forms.Button();
            this.btnBsmMaxBerrIntsP = new System.Windows.Forms.Button();
            this.lblBsmMaxRetries = new System.Windows.Forms.Label();
            this.txtBsmRxBlockCountT = new System.Windows.Forms.TextBox();
            this.txtBsmTxBlockCountT = new System.Windows.Forms.TextBox();
            this.txtBsmEccBlocksT = new System.Windows.Forms.TextBox();
            this.txtBsmCrcErrorsT = new System.Windows.Forms.TextBox();
            this.lblBsmTotal = new System.Windows.Forms.Label();
            this.lblBsmPollPeriod = new System.Windows.Forms.Label();
            this.lblBsmDataBps = new System.Windows.Forms.Label();
            this.txtBsmDataBpsP = new System.Windows.Forms.TextBox();
            this.lblBsmRxBlockCount = new System.Windows.Forms.Label();
            this.txtBsmRxBlockCountP = new System.Windows.Forms.TextBox();
            this.lblBsmTxBlockCount = new System.Windows.Forms.Label();
            this.txtBsmTxBlockCountP = new System.Windows.Forms.TextBox();
            this.lblBsmEccBlocks = new System.Windows.Forms.Label();
            this.txtBsmEccBlocksP = new System.Windows.Forms.TextBox();
            this.lblBsmCrcErrors = new System.Windows.Forms.Label();
            this.txtBsmCrcErrorsP = new System.Windows.Forms.TextBox();
            this.lblBsmMaxBerrInts = new System.Windows.Forms.Label();
            this.btnResetStats = new System.Windows.Forms.Button();
            this.grpImLinkQual.SuspendLayout();
            this.grpBsmLinkQual.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpImLinkQual
            // 
            this.grpImLinkQual.Controls.Add(this.txtImDataBpsT);
            this.grpImLinkQual.Controls.Add(this.btnImMaxBerrIntsT);
            this.grpImLinkQual.Controls.Add(this.btnImMaxBerrIntsP);
            this.grpImLinkQual.Controls.Add(this.txtImRxBlockCountT);
            this.grpImLinkQual.Controls.Add(this.txtImTxBlockCountT);
            this.grpImLinkQual.Controls.Add(this.txtImEccBlocksT);
            this.grpImLinkQual.Controls.Add(this.txtImCrcErrorsT);
            this.grpImLinkQual.Controls.Add(this.lblImTotal);
            this.grpImLinkQual.Controls.Add(this.lblImPollPeriod);
            this.grpImLinkQual.Controls.Add(this.lblImDataBps);
            this.grpImLinkQual.Controls.Add(this.txtImDataBpsP);
            this.grpImLinkQual.Controls.Add(this.lblImRxBlockCount);
            this.grpImLinkQual.Controls.Add(this.txtImRxBlockCountP);
            this.grpImLinkQual.Controls.Add(this.lblImTxBlockCount);
            this.grpImLinkQual.Controls.Add(this.txtImTxBlockCountP);
            this.grpImLinkQual.Controls.Add(this.lblImEccBlocks);
            this.grpImLinkQual.Controls.Add(this.txtImEccBlocksP);
            this.grpImLinkQual.Controls.Add(this.lblImCrcErrors);
            this.grpImLinkQual.Controls.Add(this.txtImCrcErrorsP);
            this.grpImLinkQual.Controls.Add(this.lblImMaxBerrInts);
            this.grpImLinkQual.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpImLinkQual.Location = new System.Drawing.Point(299, 12);
            this.grpImLinkQual.Name = "grpImLinkQual";
            this.grpImLinkQual.Size = new System.Drawing.Size(271, 248);
            this.grpImLinkQual.TabIndex = 1;
            this.grpImLinkQual.TabStop = false;
            this.grpImLinkQual.Text = "Implant Link Quality Statistics";
            // 
            // txtImDataBpsT
            // 
            this.txtImDataBpsT.Location = new System.Drawing.Point(185, 205);
            this.txtImDataBpsT.Name = "txtImDataBpsT";
            this.txtImDataBpsT.Size = new System.Drawing.Size(76, 20);
            this.txtImDataBpsT.TabIndex = 28;
            this.txtImDataBpsT.Text = "0";
            this.txtImDataBpsT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnImMaxBerrIntsT
            // 
            this.btnImMaxBerrIntsT.Location = new System.Drawing.Point(212, 45);
            this.btnImMaxBerrIntsT.Name = "btnImMaxBerrIntsT";
            this.btnImMaxBerrIntsT.Size = new System.Drawing.Size(20, 20);
            this.btnImMaxBerrIntsT.TabIndex = 21;
            this.btnImMaxBerrIntsT.UseVisualStyleBackColor = true;
            // 
            // btnImMaxBerrIntsP
            // 
            this.btnImMaxBerrIntsP.Location = new System.Drawing.Point(125, 45);
            this.btnImMaxBerrIntsP.Name = "btnImMaxBerrIntsP";
            this.btnImMaxBerrIntsP.Size = new System.Drawing.Size(20, 20);
            this.btnImMaxBerrIntsP.TabIndex = 20;
            this.btnImMaxBerrIntsP.UseVisualStyleBackColor = true;
            // 
            // txtImRxBlockCountT
            // 
            this.txtImRxBlockCountT.Location = new System.Drawing.Point(185, 180);
            this.txtImRxBlockCountT.Name = "txtImRxBlockCountT";
            this.txtImRxBlockCountT.Size = new System.Drawing.Size(76, 20);
            this.txtImRxBlockCountT.TabIndex = 18;
            this.txtImRxBlockCountT.Text = "0";
            this.txtImRxBlockCountT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtImTxBlockCountT
            // 
            this.txtImTxBlockCountT.Location = new System.Drawing.Point(185, 155);
            this.txtImTxBlockCountT.Name = "txtImTxBlockCountT";
            this.txtImTxBlockCountT.Size = new System.Drawing.Size(76, 20);
            this.txtImTxBlockCountT.TabIndex = 17;
            this.txtImTxBlockCountT.Text = "0";
            this.txtImTxBlockCountT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtImEccBlocksT
            // 
            this.txtImEccBlocksT.Location = new System.Drawing.Point(185, 130);
            this.txtImEccBlocksT.Name = "txtImEccBlocksT";
            this.txtImEccBlocksT.Size = new System.Drawing.Size(76, 20);
            this.txtImEccBlocksT.TabIndex = 16;
            this.txtImEccBlocksT.Text = "0";
            this.txtImEccBlocksT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtImCrcErrorsT
            // 
            this.txtImCrcErrorsT.Location = new System.Drawing.Point(185, 105);
            this.txtImCrcErrorsT.Name = "txtImCrcErrorsT";
            this.txtImCrcErrorsT.Size = new System.Drawing.Size(76, 20);
            this.txtImCrcErrorsT.TabIndex = 15;
            this.txtImCrcErrorsT.Text = "0";
            this.txtImCrcErrorsT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblImTotal
            // 
            this.lblImTotal.AutoSize = true;
            this.lblImTotal.Location = new System.Drawing.Point(203, 20);
            this.lblImTotal.Name = "lblImTotal";
            this.lblImTotal.Size = new System.Drawing.Size(36, 13);
            this.lblImTotal.TabIndex = 13;
            this.lblImTotal.Text = "Total";
            // 
            // lblImPollPeriod
            // 
            this.lblImPollPeriod.AutoSize = true;
            this.lblImPollPeriod.Location = new System.Drawing.Point(105, 20);
            this.lblImPollPeriod.Name = "lblImPollPeriod";
            this.lblImPollPeriod.Size = new System.Drawing.Size(68, 13);
            this.lblImPollPeriod.TabIndex = 12;
            this.lblImPollPeriod.Text = "Poll Period";
            // 
            // lblImDataBps
            // 
            this.lblImDataBps.AutoSize = true;
            this.lblImDataBps.Location = new System.Drawing.Point(10, 208);
            this.lblImDataBps.Name = "lblImDataBps";
            this.lblImDataBps.Size = new System.Drawing.Size(87, 13);
            this.lblImDataBps.TabIndex = 11;
            this.lblImDataBps.Text = "Data Bits/Sec";
            // 
            // txtImDataBpsP
            // 
            this.txtImDataBpsP.Location = new System.Drawing.Point(110, 205);
            this.txtImDataBpsP.Name = "txtImDataBpsP";
            this.txtImDataBpsP.Size = new System.Drawing.Size(53, 20);
            this.txtImDataBpsP.TabIndex = 10;
            this.txtImDataBpsP.Text = "0";
            this.txtImDataBpsP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblImRxBlockCount
            // 
            this.lblImRxBlockCount.AutoSize = true;
            this.lblImRxBlockCount.Location = new System.Drawing.Point(10, 183);
            this.lblImRxBlockCount.Name = "lblImRxBlockCount";
            this.lblImRxBlockCount.Size = new System.Drawing.Size(97, 13);
            this.lblImRxBlockCount.TabIndex = 9;
            this.lblImRxBlockCount.Text = "RX Block Count";
            // 
            // txtImRxBlockCountP
            // 
            this.txtImRxBlockCountP.Location = new System.Drawing.Point(110, 180);
            this.txtImRxBlockCountP.Name = "txtImRxBlockCountP";
            this.txtImRxBlockCountP.Size = new System.Drawing.Size(53, 20);
            this.txtImRxBlockCountP.TabIndex = 8;
            this.txtImRxBlockCountP.Text = "0";
            this.txtImRxBlockCountP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblImTxBlockCount
            // 
            this.lblImTxBlockCount.AutoSize = true;
            this.lblImTxBlockCount.Location = new System.Drawing.Point(10, 158);
            this.lblImTxBlockCount.Name = "lblImTxBlockCount";
            this.lblImTxBlockCount.Size = new System.Drawing.Size(96, 13);
            this.lblImTxBlockCount.TabIndex = 7;
            this.lblImTxBlockCount.Text = "TX Block Count";
            // 
            // txtImTxBlockCountP
            // 
            this.txtImTxBlockCountP.Location = new System.Drawing.Point(110, 155);
            this.txtImTxBlockCountP.Name = "txtImTxBlockCountP";
            this.txtImTxBlockCountP.Size = new System.Drawing.Size(53, 20);
            this.txtImTxBlockCountP.TabIndex = 6;
            this.txtImTxBlockCountP.Text = "0";
            this.txtImTxBlockCountP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblImEccBlocks
            // 
            this.lblImEccBlocks.AutoSize = true;
            this.lblImEccBlocks.Location = new System.Drawing.Point(10, 133);
            this.lblImEccBlocks.Name = "lblImEccBlocks";
            this.lblImEccBlocks.Size = new System.Drawing.Size(73, 13);
            this.lblImEccBlocks.TabIndex = 5;
            this.lblImEccBlocks.Text = "ECC Blocks";
            // 
            // txtImEccBlocksP
            // 
            this.txtImEccBlocksP.Location = new System.Drawing.Point(110, 130);
            this.txtImEccBlocksP.Name = "txtImEccBlocksP";
            this.txtImEccBlocksP.Size = new System.Drawing.Size(53, 20);
            this.txtImEccBlocksP.TabIndex = 4;
            this.txtImEccBlocksP.Text = "0";
            this.txtImEccBlocksP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblImCrcErrors
            // 
            this.lblImCrcErrors.AutoSize = true;
            this.lblImCrcErrors.Location = new System.Drawing.Point(10, 108);
            this.lblImCrcErrors.Name = "lblImCrcErrors";
            this.lblImCrcErrors.Size = new System.Drawing.Size(69, 13);
            this.lblImCrcErrors.TabIndex = 3;
            this.lblImCrcErrors.Text = "CRC Errors";
            // 
            // txtImCrcErrorsP
            // 
            this.txtImCrcErrorsP.Location = new System.Drawing.Point(110, 105);
            this.txtImCrcErrorsP.Name = "txtImCrcErrorsP";
            this.txtImCrcErrorsP.Size = new System.Drawing.Size(53, 20);
            this.txtImCrcErrorsP.TabIndex = 2;
            this.txtImCrcErrorsP.Text = "0";
            this.txtImCrcErrorsP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblImMaxBerrInts
            // 
            this.lblImMaxBerrInts.AutoSize = true;
            this.lblImMaxBerrInts.Location = new System.Drawing.Point(10, 47);
            this.lblImMaxBerrInts.Name = "lblImMaxBerrInts";
            this.lblImMaxBerrInts.Size = new System.Drawing.Size(74, 13);
            this.lblImMaxBerrInts.TabIndex = 1;
            this.lblImMaxBerrInts.Text = "MaxBerrInts";
            // 
            // grpBsmLinkQual
            // 
            this.grpBsmLinkQual.Controls.Add(this.txtBsmDataBpsT);
            this.grpBsmLinkQual.Controls.Add(this.btnBsmMaxRetriesT);
            this.grpBsmLinkQual.Controls.Add(this.btnBsmMaxRetriesP);
            this.grpBsmLinkQual.Controls.Add(this.btnBsmMaxBerrIntsT);
            this.grpBsmLinkQual.Controls.Add(this.btnBsmMaxBerrIntsP);
            this.grpBsmLinkQual.Controls.Add(this.lblBsmMaxRetries);
            this.grpBsmLinkQual.Controls.Add(this.txtBsmRxBlockCountT);
            this.grpBsmLinkQual.Controls.Add(this.txtBsmTxBlockCountT);
            this.grpBsmLinkQual.Controls.Add(this.txtBsmEccBlocksT);
            this.grpBsmLinkQual.Controls.Add(this.txtBsmCrcErrorsT);
            this.grpBsmLinkQual.Controls.Add(this.lblBsmTotal);
            this.grpBsmLinkQual.Controls.Add(this.lblBsmPollPeriod);
            this.grpBsmLinkQual.Controls.Add(this.lblBsmDataBps);
            this.grpBsmLinkQual.Controls.Add(this.txtBsmDataBpsP);
            this.grpBsmLinkQual.Controls.Add(this.lblBsmRxBlockCount);
            this.grpBsmLinkQual.Controls.Add(this.txtBsmRxBlockCountP);
            this.grpBsmLinkQual.Controls.Add(this.lblBsmTxBlockCount);
            this.grpBsmLinkQual.Controls.Add(this.txtBsmTxBlockCountP);
            this.grpBsmLinkQual.Controls.Add(this.lblBsmEccBlocks);
            this.grpBsmLinkQual.Controls.Add(this.txtBsmEccBlocksP);
            this.grpBsmLinkQual.Controls.Add(this.lblBsmCrcErrors);
            this.grpBsmLinkQual.Controls.Add(this.txtBsmCrcErrorsP);
            this.grpBsmLinkQual.Controls.Add(this.lblBsmMaxBerrInts);
            this.grpBsmLinkQual.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBsmLinkQual.Location = new System.Drawing.Point(12, 12);
            this.grpBsmLinkQual.Name = "grpBsmLinkQual";
            this.grpBsmLinkQual.Size = new System.Drawing.Size(271, 248);
            this.grpBsmLinkQual.TabIndex = 20;
            this.grpBsmLinkQual.TabStop = false;
            this.grpBsmLinkQual.Text = "Base Station Link Quality Statistics";
            // 
            // txtBsmDataBpsT
            // 
            this.txtBsmDataBpsT.Location = new System.Drawing.Point(185, 205);
            this.txtBsmDataBpsT.Name = "txtBsmDataBpsT";
            this.txtBsmDataBpsT.Size = new System.Drawing.Size(76, 20);
            this.txtBsmDataBpsT.TabIndex = 27;
            this.txtBsmDataBpsT.Text = "0";
            this.txtBsmDataBpsT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnBsmMaxRetriesT
            // 
            this.btnBsmMaxRetriesT.Location = new System.Drawing.Point(212, 75);
            this.btnBsmMaxRetriesT.Name = "btnBsmMaxRetriesT";
            this.btnBsmMaxRetriesT.Size = new System.Drawing.Size(20, 20);
            this.btnBsmMaxRetriesT.TabIndex = 25;
            this.btnBsmMaxRetriesT.UseVisualStyleBackColor = true;
            // 
            // btnBsmMaxRetriesP
            // 
            this.btnBsmMaxRetriesP.Location = new System.Drawing.Point(125, 75);
            this.btnBsmMaxRetriesP.Name = "btnBsmMaxRetriesP";
            this.btnBsmMaxRetriesP.Size = new System.Drawing.Size(20, 20);
            this.btnBsmMaxRetriesP.TabIndex = 24;
            this.btnBsmMaxRetriesP.UseVisualStyleBackColor = true;
            // 
            // btnBsmMaxBerrIntsT
            // 
            this.btnBsmMaxBerrIntsT.Location = new System.Drawing.Point(212, 45);
            this.btnBsmMaxBerrIntsT.Name = "btnBsmMaxBerrIntsT";
            this.btnBsmMaxBerrIntsT.Size = new System.Drawing.Size(20, 20);
            this.btnBsmMaxBerrIntsT.TabIndex = 23;
            this.btnBsmMaxBerrIntsT.UseVisualStyleBackColor = true;
            // 
            // btnBsmMaxBerrIntsP
            // 
            this.btnBsmMaxBerrIntsP.Location = new System.Drawing.Point(125, 45);
            this.btnBsmMaxBerrIntsP.Name = "btnBsmMaxBerrIntsP";
            this.btnBsmMaxBerrIntsP.Size = new System.Drawing.Size(20, 20);
            this.btnBsmMaxBerrIntsP.TabIndex = 22;
            this.btnBsmMaxBerrIntsP.UseVisualStyleBackColor = true;
            // 
            // lblBsmMaxRetries
            // 
            this.lblBsmMaxRetries.AutoSize = true;
            this.lblBsmMaxRetries.Location = new System.Drawing.Point(10, 77);
            this.lblBsmMaxRetries.Name = "lblBsmMaxRetries";
            this.lblBsmMaxRetries.Size = new System.Drawing.Size(74, 13);
            this.lblBsmMaxRetries.TabIndex = 21;
            this.lblBsmMaxRetries.Text = "Max Retries";
            // 
            // txtBsmRxBlockCountT
            // 
            this.txtBsmRxBlockCountT.Location = new System.Drawing.Point(185, 180);
            this.txtBsmRxBlockCountT.Name = "txtBsmRxBlockCountT";
            this.txtBsmRxBlockCountT.Size = new System.Drawing.Size(76, 20);
            this.txtBsmRxBlockCountT.TabIndex = 18;
            this.txtBsmRxBlockCountT.Text = "0";
            this.txtBsmRxBlockCountT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBsmTxBlockCountT
            // 
            this.txtBsmTxBlockCountT.Location = new System.Drawing.Point(185, 155);
            this.txtBsmTxBlockCountT.Name = "txtBsmTxBlockCountT";
            this.txtBsmTxBlockCountT.Size = new System.Drawing.Size(76, 20);
            this.txtBsmTxBlockCountT.TabIndex = 17;
            this.txtBsmTxBlockCountT.Text = "0";
            this.txtBsmTxBlockCountT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBsmEccBlocksT
            // 
            this.txtBsmEccBlocksT.Location = new System.Drawing.Point(185, 130);
            this.txtBsmEccBlocksT.Name = "txtBsmEccBlocksT";
            this.txtBsmEccBlocksT.Size = new System.Drawing.Size(76, 20);
            this.txtBsmEccBlocksT.TabIndex = 16;
            this.txtBsmEccBlocksT.Text = "0";
            this.txtBsmEccBlocksT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBsmCrcErrorsT
            // 
            this.txtBsmCrcErrorsT.Location = new System.Drawing.Point(185, 105);
            this.txtBsmCrcErrorsT.Name = "txtBsmCrcErrorsT";
            this.txtBsmCrcErrorsT.Size = new System.Drawing.Size(76, 20);
            this.txtBsmCrcErrorsT.TabIndex = 15;
            this.txtBsmCrcErrorsT.Text = "0";
            this.txtBsmCrcErrorsT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblBsmTotal
            // 
            this.lblBsmTotal.AutoSize = true;
            this.lblBsmTotal.Location = new System.Drawing.Point(203, 20);
            this.lblBsmTotal.Name = "lblBsmTotal";
            this.lblBsmTotal.Size = new System.Drawing.Size(36, 13);
            this.lblBsmTotal.TabIndex = 13;
            this.lblBsmTotal.Text = "Total";
            // 
            // lblBsmPollPeriod
            // 
            this.lblBsmPollPeriod.AutoSize = true;
            this.lblBsmPollPeriod.Location = new System.Drawing.Point(105, 20);
            this.lblBsmPollPeriod.Name = "lblBsmPollPeriod";
            this.lblBsmPollPeriod.Size = new System.Drawing.Size(68, 13);
            this.lblBsmPollPeriod.TabIndex = 12;
            this.lblBsmPollPeriod.Text = "Poll Period";
            // 
            // lblBsmDataBps
            // 
            this.lblBsmDataBps.AutoSize = true;
            this.lblBsmDataBps.Location = new System.Drawing.Point(10, 208);
            this.lblBsmDataBps.Name = "lblBsmDataBps";
            this.lblBsmDataBps.Size = new System.Drawing.Size(87, 13);
            this.lblBsmDataBps.TabIndex = 11;
            this.lblBsmDataBps.Text = "Data Bits/Sec";
            // 
            // txtBsmDataBpsP
            // 
            this.txtBsmDataBpsP.Location = new System.Drawing.Point(110, 205);
            this.txtBsmDataBpsP.Name = "txtBsmDataBpsP";
            this.txtBsmDataBpsP.Size = new System.Drawing.Size(53, 20);
            this.txtBsmDataBpsP.TabIndex = 10;
            this.txtBsmDataBpsP.Text = "0";
            this.txtBsmDataBpsP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblBsmRxBlockCount
            // 
            this.lblBsmRxBlockCount.AutoSize = true;
            this.lblBsmRxBlockCount.Location = new System.Drawing.Point(10, 183);
            this.lblBsmRxBlockCount.Name = "lblBsmRxBlockCount";
            this.lblBsmRxBlockCount.Size = new System.Drawing.Size(97, 13);
            this.lblBsmRxBlockCount.TabIndex = 9;
            this.lblBsmRxBlockCount.Text = "RX Block Count";
            // 
            // txtBsmRxBlockCountP
            // 
            this.txtBsmRxBlockCountP.Location = new System.Drawing.Point(110, 180);
            this.txtBsmRxBlockCountP.Name = "txtBsmRxBlockCountP";
            this.txtBsmRxBlockCountP.Size = new System.Drawing.Size(53, 20);
            this.txtBsmRxBlockCountP.TabIndex = 8;
            this.txtBsmRxBlockCountP.Text = "0";
            this.txtBsmRxBlockCountP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblBsmTxBlockCount
            // 
            this.lblBsmTxBlockCount.AutoSize = true;
            this.lblBsmTxBlockCount.Location = new System.Drawing.Point(10, 158);
            this.lblBsmTxBlockCount.Name = "lblBsmTxBlockCount";
            this.lblBsmTxBlockCount.Size = new System.Drawing.Size(96, 13);
            this.lblBsmTxBlockCount.TabIndex = 7;
            this.lblBsmTxBlockCount.Text = "TX Block Count";
            // 
            // txtBsmTxBlockCountP
            // 
            this.txtBsmTxBlockCountP.Location = new System.Drawing.Point(110, 155);
            this.txtBsmTxBlockCountP.Name = "txtBsmTxBlockCountP";
            this.txtBsmTxBlockCountP.Size = new System.Drawing.Size(53, 20);
            this.txtBsmTxBlockCountP.TabIndex = 6;
            this.txtBsmTxBlockCountP.Text = "0";
            this.txtBsmTxBlockCountP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblBsmEccBlocks
            // 
            this.lblBsmEccBlocks.AutoSize = true;
            this.lblBsmEccBlocks.Location = new System.Drawing.Point(10, 133);
            this.lblBsmEccBlocks.Name = "lblBsmEccBlocks";
            this.lblBsmEccBlocks.Size = new System.Drawing.Size(73, 13);
            this.lblBsmEccBlocks.TabIndex = 5;
            this.lblBsmEccBlocks.Text = "ECC Blocks";
            // 
            // txtBsmEccBlocksP
            // 
            this.txtBsmEccBlocksP.Location = new System.Drawing.Point(110, 130);
            this.txtBsmEccBlocksP.Name = "txtBsmEccBlocksP";
            this.txtBsmEccBlocksP.Size = new System.Drawing.Size(53, 20);
            this.txtBsmEccBlocksP.TabIndex = 4;
            this.txtBsmEccBlocksP.Text = "0";
            this.txtBsmEccBlocksP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblBsmCrcErrors
            // 
            this.lblBsmCrcErrors.AutoSize = true;
            this.lblBsmCrcErrors.Location = new System.Drawing.Point(10, 108);
            this.lblBsmCrcErrors.Name = "lblBsmCrcErrors";
            this.lblBsmCrcErrors.Size = new System.Drawing.Size(69, 13);
            this.lblBsmCrcErrors.TabIndex = 3;
            this.lblBsmCrcErrors.Text = "CRC Errors";
            // 
            // txtBsmCrcErrorsP
            // 
            this.txtBsmCrcErrorsP.Location = new System.Drawing.Point(110, 105);
            this.txtBsmCrcErrorsP.Name = "txtBsmCrcErrorsP";
            this.txtBsmCrcErrorsP.Size = new System.Drawing.Size(53, 20);
            this.txtBsmCrcErrorsP.TabIndex = 2;
            this.txtBsmCrcErrorsP.Text = "0";
            this.txtBsmCrcErrorsP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblBsmMaxBerrInts
            // 
            this.lblBsmMaxBerrInts.AutoSize = true;
            this.lblBsmMaxBerrInts.Location = new System.Drawing.Point(10, 47);
            this.lblBsmMaxBerrInts.Name = "lblBsmMaxBerrInts";
            this.lblBsmMaxBerrInts.Size = new System.Drawing.Size(74, 13);
            this.lblBsmMaxBerrInts.TabIndex = 1;
            this.lblBsmMaxBerrInts.Text = "MaxBerrInts";
            // 
            // btnResetStats
            // 
            this.btnResetStats.Location = new System.Drawing.Point(254, 274);
            this.btnResetStats.Name = "btnResetStats";
            this.btnResetStats.Size = new System.Drawing.Size(75, 23);
            this.btnResetStats.TabIndex = 21;
            this.btnResetStats.Text = "Reset Stats";
            this.btnResetStats.UseVisualStyleBackColor = true;
            this.btnResetStats.Click += new System.EventHandler(this.btnResetStats_Click);
            // 
            // LinkQualityForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(589, 309);
            this.Controls.Add(this.btnResetStats);
            this.Controls.Add(this.grpBsmLinkQual);
            this.Controls.Add(this.grpImLinkQual);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LinkQualityForm";
            this.Text = "LinkQualityForm";
            this.grpImLinkQual.ResumeLayout(false);
            this.grpImLinkQual.PerformLayout();
            this.grpBsmLinkQual.ResumeLayout(false);
            this.grpBsmLinkQual.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpImLinkQual;
        private System.Windows.Forms.Label lblImTxBlockCount;
        private System.Windows.Forms.TextBox txtImTxBlockCountP;
        private System.Windows.Forms.Label lblImEccBlocks;
        private System.Windows.Forms.TextBox txtImEccBlocksP;
        private System.Windows.Forms.Label lblImCrcErrors;
        private System.Windows.Forms.TextBox txtImCrcErrorsP;
        private System.Windows.Forms.Label lblImMaxBerrInts;
        private System.Windows.Forms.Label lblImTotal;
        private System.Windows.Forms.Label lblImPollPeriod;
        private System.Windows.Forms.Label lblImDataBps;
        private System.Windows.Forms.TextBox txtImDataBpsP;
        private System.Windows.Forms.Label lblImRxBlockCount;
        private System.Windows.Forms.TextBox txtImRxBlockCountP;
        private System.Windows.Forms.TextBox txtImRxBlockCountT;
        private System.Windows.Forms.TextBox txtImTxBlockCountT;
        private System.Windows.Forms.TextBox txtImEccBlocksT;
        private System.Windows.Forms.TextBox txtImCrcErrorsT;
        private System.Windows.Forms.GroupBox grpBsmLinkQual;
        private System.Windows.Forms.Label lblBsmMaxRetries;
        private System.Windows.Forms.TextBox txtBsmRxBlockCountT;
        private System.Windows.Forms.TextBox txtBsmTxBlockCountT;
        private System.Windows.Forms.TextBox txtBsmEccBlocksT;
        private System.Windows.Forms.TextBox txtBsmCrcErrorsT;
        private System.Windows.Forms.Label lblBsmTotal;
        private System.Windows.Forms.Label lblBsmPollPeriod;
        private System.Windows.Forms.Label lblBsmDataBps;
        private System.Windows.Forms.TextBox txtBsmDataBpsP;
        private System.Windows.Forms.Label lblBsmRxBlockCount;
        private System.Windows.Forms.TextBox txtBsmRxBlockCountP;
        private System.Windows.Forms.Label lblBsmTxBlockCount;
        private System.Windows.Forms.TextBox txtBsmTxBlockCountP;
        private System.Windows.Forms.Label lblBsmEccBlocks;
        private System.Windows.Forms.TextBox txtBsmEccBlocksP;
        private System.Windows.Forms.Label lblBsmCrcErrors;
        private System.Windows.Forms.TextBox txtBsmCrcErrorsP;
        private System.Windows.Forms.Label lblBsmMaxBerrInts;
        private System.Windows.Forms.Button btnImMaxBerrIntsP;
        private System.Windows.Forms.Button btnImMaxBerrIntsT;
        private System.Windows.Forms.Button btnBsmMaxBerrIntsT;
        private System.Windows.Forms.Button btnBsmMaxBerrIntsP;
        private System.Windows.Forms.Button btnBsmMaxRetriesT;
        private System.Windows.Forms.Button btnBsmMaxRetriesP;
        private System.Windows.Forms.Button btnResetStats;
        private System.Windows.Forms.TextBox txtImDataBpsT;
        private System.Windows.Forms.TextBox txtBsmDataBpsT;
    }
}