//
// Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2012. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Zarlink.Adk.Api;

namespace Zarlink.Adk.Forms
{
    public partial class LinkQualityForm : Form
    {
        // used to save total counts
        private long totalBsmTxBlocks = 0;
        private long totalBsmRxBlocks = 0;
        private long totalBsmCrcErrs = 0;
        private long totalBsmEccBlocks = 0;
        private long totalImTxBlocks = 0;
        private long totalImRxBlocks = 0;
        private long totalImCrcErrs = 0;
        private long totalImEccBlocks = 0;
        
        // stopwatch used to measure time for data rate calcs
        private Stopwatch stopwatch = Stopwatch.StartNew();
        
        // used to calc data rates since previous update
        private TimeSpan prevUpdateTimeForBsm;
        private TimeSpan prevUpdateTimeForIm;
        
        // used to calc total data rates since starting
        private long rxBlocksForTotalBsmDataRate = -1;
        private long rxBlocksForTotalImDataRate = -1;
        private TimeSpan startTimeForTotalBsmDataRate;
        private TimeSpan startTimeForTotalImDataRate;
        
        public LinkQualityForm(bool isImplant, bool connectedToLocalImplant)
        {
            InitializeComponent();
            if (isImplant) {
            
                grpBsmLinkQual.Visible = false;
                this.Size = new System.Drawing.Size (303, 343);
                grpImLinkQual.Location = new System.Drawing.Point(12, 12);
                btnResetStats.Location = new System.Drawing.Point(110, 274);
                
            } else {
            
                if (connectedToLocalImplant) {
                    grpImLinkQual.Enabled = true;
                } else {
                    grpImLinkQual.Enabled = false;
                }
            }
        }
        
        public void updateBsmStats(BsmApi.BsmStatChanges bsc, int blockSize)
        {
            // get current time
            TimeSpan now = stopwatch.Elapsed;
            
            updateBsmLinkQualStats(bsc);
            updateBsmDataStats(bsc, blockSize, now);
            
            // save previous update time for base
            prevUpdateTimeForBsm = now;
        }
        
        public void updateImStats(ImApi.ImStatChanges isc, int blockSize)
        { 
            // get current time
            TimeSpan now = stopwatch.Elapsed;
            
            updateImLinkQualStats(isc);
            updateImDataStats(isc, blockSize, now);
            
            // save previous update time for implant
            prevUpdateTimeForIm = now;
        }

        private void updateBsmLinkQualStats(BsmApi.BsmStatChanges bsc)
        {
            // if link quality changed for base
            if (bsc.linkQualChanged) {
            
                txtBsmCrcErrorsP.Text = bsc.linkQual.crcErrs.ToString();
                txtBsmEccBlocksP.Text = bsc.linkQual.errCorBlocks.ToString();
    
                totalBsmCrcErrs = bsc.linkQual.crcErrs + totalBsmCrcErrs;
                totalBsmEccBlocks = bsc.linkQual.errCorBlocks + totalBsmEccBlocks;
                txtBsmCrcErrorsT.Text = totalBsmCrcErrs.ToString();
                txtBsmEccBlocksT.Text = totalBsmEccBlocks.ToString();
    
                if (bsc.linkQual.maxBErrInts) {
                    btnBsmMaxBerrIntsP.BackColor = Color.Red;
                    btnBsmMaxBerrIntsT.BackColor = Color.Red;
                } else {
                    btnBsmMaxBerrIntsP.BackColor = Color.Transparent;
                }
                
                if (bsc.linkQual.maxRetriesInts) {
                    btnBsmMaxRetriesP.BackColor = Color.Red;
                    btnBsmMaxRetriesT.BackColor = Color.Red;
                } else {
                    btnBsmMaxRetriesP.BackColor = Color.Transparent;
                }
                
            } else { // link quality didn't change for base
            
                // display 0 for these stats
                txtBsmCrcErrorsP.Text = "0";
                txtBsmEccBlocksP.Text = "0";
                btnBsmMaxBerrIntsP.BackColor = Color.Transparent;
                btnBsmMaxRetriesP.BackColor = Color.Transparent;
            }
        }
            
        private void updateImLinkQualStats(ImApi.ImStatChanges isc)
        {
            // if link quality changed for implant
            if (isc.linkQualChanged) {
            
                txtImCrcErrorsP.Text = isc.linkQual.crcErrs.ToString();
                txtImEccBlocksP.Text = isc.linkQual.errCorBlocks.ToString();
    
                totalImCrcErrs = isc.linkQual.crcErrs + totalImCrcErrs;
                totalImEccBlocks = isc.linkQual.errCorBlocks + totalImEccBlocks;
                txtImCrcErrorsT.Text = totalImCrcErrs.ToString();
                txtImEccBlocksT.Text = totalImEccBlocks.ToString();
                
                if (isc.linkQual.maxBErrInts) {
                    btnImMaxBerrIntsP.BackColor = Color.Red;
                    btnImMaxBerrIntsT.BackColor = Color.Red;
                } else {
                    btnImMaxBerrIntsP.BackColor = Color.Transparent;
                }
                
            } else { // link quality didn't change for implant
            
                // display 0 for these stats
                txtImCrcErrorsP.Text = "0";
                txtImEccBlocksP.Text = "0";
                btnImMaxBerrIntsP.BackColor = Color.Transparent;
            }
        }

        private void updateBsmDataStats(BsmApi.BsmStatChanges bsc, int blockSize, TimeSpan now)
        {
            uint dataRate;
            TimeSpan timeDiff;
            
            // if data status for base changed and RX block count != 0
            if (bsc.dataStatChanged && (bsc.dataStat.rxBlockCount != 0)) {
            
                // update RX block counts
                totalBsmRxBlocks += bsc.dataStat.rxBlockCount;
                txtBsmRxBlockCountT.Text = totalBsmRxBlocks.ToString();
                txtBsmRxBlockCountP.Text = bsc.dataStat.rxBlockCount.ToString();
                
                // If first RX data since stats were reset, restart time & RX
                // block count used to calc total data rate.
                //
                if (rxBlocksForTotalBsmDataRate < 0) {
                
                    startTimeForTotalBsmDataRate = now;
                    rxBlocksForTotalBsmDataRate = 0;
                    
                } else {
                
                    // update RX block count used to calc total data rate
                    rxBlocksForTotalBsmDataRate += bsc.dataStat.rxBlockCount;
                    
                    // calc bits per block
                    double bitsPerBlock = (blockSize == 15) ? ((14 * 8) + 1) : (blockSize * 8);
                    
                    // update total data rate since starting
                    timeDiff = now - startTimeForTotalBsmDataRate;
                    dataRate = (uint)((rxBlocksForTotalBsmDataRate * bitsPerBlock) /
                        timeDiff.TotalSeconds);
                    txtBsmDataBpsT.Text = dataRate.ToString();
                    
                    // update data rate since previous update
                    timeDiff = now - prevUpdateTimeForBsm;
                    dataRate = (uint)((bsc.dataStat.rxBlockCount * bitsPerBlock) /
                        timeDiff.TotalSeconds);
                    txtBsmDataBpsP.Text = dataRate.ToString();
                }
                
            } else { // data status didn't change or RX block count == 0
            
                // display 0 for these counts
                txtBsmRxBlockCountP.Text = "0";
                txtBsmDataBpsP.Text = "0";
                txtBsmDataBpsT.Text = "0";
                
                // restart total data rate calc
                rxBlocksForTotalBsmDataRate = -1;
            }
            
            // if data status for base changed, update TX block counts
            if (bsc.dataStatChanged) {
                totalBsmTxBlocks += bsc.dataStat.txBlockCount;
                txtBsmTxBlockCountT.Text = totalBsmTxBlocks.ToString();
                txtBsmTxBlockCountP.Text = bsc.dataStat.txBlockCount.ToString();
            } else {
                // display 0 for these counts
                txtBsmTxBlockCountP.Text = "0";
            }
        }
        
        private void updateImDataStats(ImApi.ImStatChanges isc, int blockSize, TimeSpan now)
        {
            uint dataRate;
            TimeSpan timeDiff;
            
            // if data status for implant changed and RX block count != 0
            if (isc.dataStatChanged && (isc.dataStat.rxBlockCount != 0)) {
            
                // update RX block counts
                totalImRxBlocks += isc.dataStat.rxBlockCount;
                txtImRxBlockCountT.Text = totalImRxBlocks.ToString();
                txtImRxBlockCountP.Text = isc.dataStat.rxBlockCount.ToString();
                
                // If first RX data since stats were reset, restart time & RX
                // block count used to calc total data rate.
                //
                if (rxBlocksForTotalImDataRate < 0) {
                
                    startTimeForTotalImDataRate = now;
                    rxBlocksForTotalImDataRate = 0;
                    
                } else {
                
                    // update RX block count used to calc total data rate
                    rxBlocksForTotalImDataRate += isc.dataStat.rxBlockCount;
                    
                    // calc bits per block
                    double bitsPerBlock = (blockSize == 15) ? ((14 * 8) + 1) : (blockSize * 8);
                    
                    // update total data rate since starting
                    timeDiff = now - startTimeForTotalImDataRate;
                    dataRate = (uint)((rxBlocksForTotalImDataRate * bitsPerBlock) /
                        timeDiff.TotalSeconds);
                    txtImDataBpsT.Text = dataRate.ToString();
                    
                    // update data rate since previous update
                    timeDiff = now - prevUpdateTimeForIm;
                    dataRate = (uint)((isc.dataStat.rxBlockCount * bitsPerBlock) /
                        timeDiff.TotalSeconds);
                    txtImDataBpsP.Text = dataRate.ToString();
                }
                
            } else { // data status didn't change or RX block count == 0
            
                // display 0 for these counts
                txtImRxBlockCountP.Text = "0";
                txtImDataBpsP.Text = "0";
                txtImDataBpsT.Text = "0";
                
                // restart total data rate calc
                rxBlocksForTotalImDataRate = -1;
            }
            
            // if data status for implant changed, update TX block counts
            if (isc.dataStatChanged) {
                totalImTxBlocks += isc.dataStat.txBlockCount;
                txtImTxBlockCountT.Text = totalImTxBlocks.ToString();
                txtImTxBlockCountP.Text = isc.dataStat.txBlockCount.ToString();
            } else {
                // display 0 for these counts
                txtImTxBlockCountP.Text = "0";
            }
        }
        
        public void resetStats()
        {
            txtBsmCrcErrorsP.Text = "0";
            txtBsmEccBlocksP.Text = "0";
            txtImCrcErrorsP.Text = "0";
            txtImEccBlocksP.Text = "0";
            txtBsmTxBlockCountP.Text = "0";
            txtBsmRxBlockCountP.Text = "0";
            txtImTxBlockCountP.Text = "0";
            txtImRxBlockCountP.Text = "0";
            btnBsmMaxBerrIntsP.BackColor = Color.Transparent;
            btnBsmMaxRetriesP.BackColor = Color.Transparent;
            btnImMaxBerrIntsP.BackColor = Color.Transparent;
            txtBsmCrcErrorsT.Text = "0";
            txtBsmEccBlocksT.Text = "0";
            txtImCrcErrorsT.Text = "0";
            txtImEccBlocksT.Text = "0";
            txtBsmTxBlockCountT.Text = "0";
            txtBsmRxBlockCountT.Text = "0";
            txtImTxBlockCountT.Text = "0";
            txtImRxBlockCountT.Text = "0";
            txtBsmDataBpsP.Text = "0";
            txtBsmDataBpsT.Text = "0";
            txtImDataBpsP.Text = "0";
            txtImDataBpsT.Text = "0";

            btnBsmMaxBerrIntsT.BackColor = Color.Transparent;
            btnBsmMaxRetriesT.BackColor = Color.Transparent;
            btnImMaxBerrIntsT.BackColor = Color.Transparent;
                
            totalBsmTxBlocks = 0;
            totalBsmRxBlocks = 0;
            totalImTxBlocks = 0;
            totalImRxBlocks = 0;
            totalBsmCrcErrs = 0;
            totalBsmEccBlocks = 0;
            totalImCrcErrs = 0;
            totalImEccBlocks = 0;
            
            rxBlocksForTotalBsmDataRate = -1;
            rxBlocksForTotalImDataRate = -1;
        }

        public void enableImStats()
        {
            grpImLinkQual.Enabled = true;
        }
        
        public void disableImStats()
        { 
            grpImLinkQual.Enabled = false;
        }

        private void btnResetStats_Click(object sender, EventArgs e)
        {
            resetStats();
        }
    }
}
