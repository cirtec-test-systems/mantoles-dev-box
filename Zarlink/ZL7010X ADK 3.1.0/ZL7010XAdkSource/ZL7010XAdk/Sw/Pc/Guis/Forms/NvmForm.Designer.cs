using System;

namespace Zarlink.Adk.Forms
{
    partial class NvmForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpBsmNvmSettings = new System.Windows.Forms.GroupBox();
            this.btnCalRssi = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBsmNvmCh1 = new System.Windows.Forms.TextBox();
            this.txtBsmNvmCh0 = new System.Windows.Forms.TextBox();
            this.txtBsmNvmCh3 = new System.Windows.Forms.TextBox();
            this.txtBsmNvmCh2 = new System.Windows.Forms.TextBox();
            this.txtBsmNvmCh5 = new System.Windows.Forms.TextBox();
            this.txtBsmNvmCh4 = new System.Windows.Forms.TextBox();
            this.txtBsmNvmCh7 = new System.Windows.Forms.TextBox();
            this.txtBsmNvmCh6 = new System.Windows.Forms.TextBox();
            this.txtBsmNvmCh8 = new System.Windows.Forms.TextBox();
            this.txtBsmNvmCh9 = new System.Windows.Forms.TextBox();
            this.lblBsmNvmParm2 = new System.Windows.Forms.Label();
            this.lblBsmNvmParm1 = new System.Windows.Forms.Label();
            this.txtBsmNvmParm2 = new System.Windows.Forms.TextBox();
            this.btnBsmLoadIntelHex = new System.Windows.Forms.Button();
            this.btnBsmSaveIntelHex = new System.Windows.Forms.Button();
            this.grpImNvmSettings = new System.Windows.Forms.GroupBox();
            this.lblImNvmParm4 = new System.Windows.Forms.Label();
            this.txtImNvmParm4 = new System.Windows.Forms.TextBox();
            this.lblImNvmParm3 = new System.Windows.Forms.Label();
            this.lblImNvmParm2 = new System.Windows.Forms.Label();
            this.lblImNvmParm1 = new System.Windows.Forms.Label();
            this.txtImNvmParm3 = new System.Windows.Forms.TextBox();
            this.txtImNvmParm2 = new System.Windows.Forms.TextBox();
            this.txtImNvmParm1 = new System.Windows.Forms.TextBox();
            this.btnImLoadIntelHex = new System.Windows.Forms.Button();
            this.btnImSaveIntelHex = new System.Windows.Forms.Button();
            this.grpBsmNvmSettings.SuspendLayout();
            this.grpImNvmSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpBsmNvmSettings
            // 
            this.grpBsmNvmSettings.Controls.Add(this.btnCalRssi);
            this.grpBsmNvmSettings.Controls.Add(this.label10);
            this.grpBsmNvmSettings.Controls.Add(this.label9);
            this.grpBsmNvmSettings.Controls.Add(this.label8);
            this.grpBsmNvmSettings.Controls.Add(this.label7);
            this.grpBsmNvmSettings.Controls.Add(this.label6);
            this.grpBsmNvmSettings.Controls.Add(this.label5);
            this.grpBsmNvmSettings.Controls.Add(this.label4);
            this.grpBsmNvmSettings.Controls.Add(this.label3);
            this.grpBsmNvmSettings.Controls.Add(this.label2);
            this.grpBsmNvmSettings.Controls.Add(this.label1);
            this.grpBsmNvmSettings.Controls.Add(this.txtBsmNvmCh1);
            this.grpBsmNvmSettings.Controls.Add(this.txtBsmNvmCh0);
            this.grpBsmNvmSettings.Controls.Add(this.txtBsmNvmCh3);
            this.grpBsmNvmSettings.Controls.Add(this.txtBsmNvmCh2);
            this.grpBsmNvmSettings.Controls.Add(this.txtBsmNvmCh5);
            this.grpBsmNvmSettings.Controls.Add(this.txtBsmNvmCh4);
            this.grpBsmNvmSettings.Controls.Add(this.txtBsmNvmCh7);
            this.grpBsmNvmSettings.Controls.Add(this.txtBsmNvmCh6);
            this.grpBsmNvmSettings.Controls.Add(this.txtBsmNvmCh8);
            this.grpBsmNvmSettings.Controls.Add(this.txtBsmNvmCh9);
            this.grpBsmNvmSettings.Controls.Add(this.lblBsmNvmParm2);
            this.grpBsmNvmSettings.Controls.Add(this.lblBsmNvmParm1);
            this.grpBsmNvmSettings.Controls.Add(this.txtBsmNvmParm2);
            this.grpBsmNvmSettings.Controls.Add(this.btnBsmLoadIntelHex);
            this.grpBsmNvmSettings.Controls.Add(this.btnBsmSaveIntelHex);
            this.grpBsmNvmSettings.Location = new System.Drawing.Point(12, 12);
            this.grpBsmNvmSettings.Name = "grpBsmNvmSettings";
            this.grpBsmNvmSettings.Size = new System.Drawing.Size(273, 426);
            this.grpBsmNvmSettings.TabIndex = 1;
            this.grpBsmNvmSettings.TabStop = false;
            this.grpBsmNvmSettings.Text = "Base Station NVM ";
            // 
            // btnCalRssi
            // 
            this.btnCalRssi.Location = new System.Drawing.Point(172, 301);
            this.btnCalRssi.Name = "btnCalRssi";
            this.btnCalRssi.Size = new System.Drawing.Size(95, 23);
            this.btnCalRssi.TabIndex = 34;
            this.btnCalRssi.Text = "Calibrate RSSI";
            this.btnCalRssi.UseVisualStyleBackColor = true;
            this.btnCalRssi.Click += new System.EventHandler(this.btnCalRssi_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(88, 278);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 33;
            this.label10.Text = "Channel 9";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(88, 253);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 32;
            this.label9.Text = "Channel 8";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(88, 228);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 31;
            this.label8.Text = "Channel 7";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(88, 203);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 30;
            this.label7.Text = "Channel 6";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(88, 178);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 29;
            this.label6.Text = "Channel 5";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(88, 153);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 28;
            this.label5.Text = "Channel 4";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(88, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "Channel 3";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(88, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 26;
            this.label3.Text = "Channel 2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(88, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "Channel 1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(88, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "Channel 0";
            // 
            // txtBsmNvmCh1
            // 
            this.txtBsmNvmCh1.Location = new System.Drawing.Point(172, 75);
            this.txtBsmNvmCh1.Name = "txtBsmNvmCh1";
            this.txtBsmNvmCh1.Size = new System.Drawing.Size(74, 20);
            this.txtBsmNvmCh1.TabIndex = 23;
            // 
            // txtBsmNvmCh0
            // 
            this.txtBsmNvmCh0.Location = new System.Drawing.Point(172, 50);
            this.txtBsmNvmCh0.Name = "txtBsmNvmCh0";
            this.txtBsmNvmCh0.Size = new System.Drawing.Size(74, 20);
            this.txtBsmNvmCh0.TabIndex = 22;
            // 
            // txtBsmNvmCh3
            // 
            this.txtBsmNvmCh3.Location = new System.Drawing.Point(172, 125);
            this.txtBsmNvmCh3.Name = "txtBsmNvmCh3";
            this.txtBsmNvmCh3.Size = new System.Drawing.Size(74, 20);
            this.txtBsmNvmCh3.TabIndex = 21;
            // 
            // txtBsmNvmCh2
            // 
            this.txtBsmNvmCh2.Location = new System.Drawing.Point(172, 100);
            this.txtBsmNvmCh2.Name = "txtBsmNvmCh2";
            this.txtBsmNvmCh2.Size = new System.Drawing.Size(74, 20);
            this.txtBsmNvmCh2.TabIndex = 20;
            // 
            // txtBsmNvmCh5
            // 
            this.txtBsmNvmCh5.Location = new System.Drawing.Point(172, 175);
            this.txtBsmNvmCh5.Name = "txtBsmNvmCh5";
            this.txtBsmNvmCh5.Size = new System.Drawing.Size(74, 20);
            this.txtBsmNvmCh5.TabIndex = 19;
            // 
            // txtBsmNvmCh4
            // 
            this.txtBsmNvmCh4.Location = new System.Drawing.Point(172, 150);
            this.txtBsmNvmCh4.Name = "txtBsmNvmCh4";
            this.txtBsmNvmCh4.Size = new System.Drawing.Size(74, 20);
            this.txtBsmNvmCh4.TabIndex = 18;
            // 
            // txtBsmNvmCh7
            // 
            this.txtBsmNvmCh7.Location = new System.Drawing.Point(172, 225);
            this.txtBsmNvmCh7.Name = "txtBsmNvmCh7";
            this.txtBsmNvmCh7.Size = new System.Drawing.Size(74, 20);
            this.txtBsmNvmCh7.TabIndex = 17;
            // 
            // txtBsmNvmCh6
            // 
            this.txtBsmNvmCh6.Location = new System.Drawing.Point(172, 200);
            this.txtBsmNvmCh6.Name = "txtBsmNvmCh6";
            this.txtBsmNvmCh6.Size = new System.Drawing.Size(74, 20);
            this.txtBsmNvmCh6.TabIndex = 16;
            // 
            // txtBsmNvmCh8
            // 
            this.txtBsmNvmCh8.Location = new System.Drawing.Point(172, 250);
            this.txtBsmNvmCh8.Name = "txtBsmNvmCh8";
            this.txtBsmNvmCh8.Size = new System.Drawing.Size(74, 20);
            this.txtBsmNvmCh8.TabIndex = 15;
            // 
            // txtBsmNvmCh9
            // 
            this.txtBsmNvmCh9.Location = new System.Drawing.Point(172, 275);
            this.txtBsmNvmCh9.Name = "txtBsmNvmCh9";
            this.txtBsmNvmCh9.Size = new System.Drawing.Size(74, 20);
            this.txtBsmNvmCh9.TabIndex = 14;
            // 
            // lblBsmNvmParm2
            // 
            this.lblBsmNvmParm2.AutoSize = true;
            this.lblBsmNvmParm2.Location = new System.Drawing.Point(11, 345);
            this.lblBsmNvmParm2.Name = "lblBsmNvmParm2";
            this.lblBsmNvmParm2.Size = new System.Drawing.Size(132, 13);
            this.lblBsmNvmParm2.TabIndex = 13;
            this.lblBsmNvmParm2.Text = "RX IF ADC Decision Level";
            // 
            // lblBsmNvmParm1
            // 
            this.lblBsmNvmParm1.AutoSize = true;
            this.lblBsmNvmParm1.Location = new System.Drawing.Point(23, 30);
            this.lblBsmNvmParm1.Name = "lblBsmNvmParm1";
            this.lblBsmNvmParm1.Size = new System.Drawing.Size(120, 13);
            this.lblBsmNvmParm1.TabIndex = 12;
            this.lblBsmNvmParm1.Text = "RSSI-to-dBm Calibration";
            // 
            // txtBsmNvmParm2
            // 
            this.txtBsmNvmParm2.Location = new System.Drawing.Point(172, 342);
            this.txtBsmNvmParm2.Name = "txtBsmNvmParm2";
            this.txtBsmNvmParm2.Size = new System.Drawing.Size(74, 20);
            this.txtBsmNvmParm2.TabIndex = 10;
            // 
            // btnBsmLoadIntelHex
            // 
            this.btnBsmLoadIntelHex.Location = new System.Drawing.Point(148, 375);
            this.btnBsmLoadIntelHex.Name = "btnBsmLoadIntelHex";
            this.btnBsmLoadIntelHex.Size = new System.Drawing.Size(119, 23);
            this.btnBsmLoadIntelHex.TabIndex = 8;
            this.btnBsmLoadIntelHex.Text = "Load from Intel Hex";
            this.btnBsmLoadIntelHex.UseVisualStyleBackColor = true;
            this.btnBsmLoadIntelHex.Click += new System.EventHandler(this.btnBsmLoadIntelHex_Click);
            // 
            // btnBsmSaveIntelHex
            // 
            this.btnBsmSaveIntelHex.Location = new System.Drawing.Point(11, 375);
            this.btnBsmSaveIntelHex.Name = "btnBsmSaveIntelHex";
            this.btnBsmSaveIntelHex.Size = new System.Drawing.Size(119, 23);
            this.btnBsmSaveIntelHex.TabIndex = 7;
            this.btnBsmSaveIntelHex.Text = "Save to Intel Hex";
            this.btnBsmSaveIntelHex.UseVisualStyleBackColor = true;
            this.btnBsmSaveIntelHex.Click += new System.EventHandler(this.btnBsmSaveIntelHex_Click);
            // 
            // grpImNvmSettings
            // 
            this.grpImNvmSettings.Controls.Add(this.lblImNvmParm4);
            this.grpImNvmSettings.Controls.Add(this.txtImNvmParm4);
            this.grpImNvmSettings.Controls.Add(this.lblImNvmParm3);
            this.grpImNvmSettings.Controls.Add(this.lblImNvmParm2);
            this.grpImNvmSettings.Controls.Add(this.lblImNvmParm1);
            this.grpImNvmSettings.Controls.Add(this.txtImNvmParm3);
            this.grpImNvmSettings.Controls.Add(this.txtImNvmParm2);
            this.grpImNvmSettings.Controls.Add(this.txtImNvmParm1);
            this.grpImNvmSettings.Controls.Add(this.btnImLoadIntelHex);
            this.grpImNvmSettings.Controls.Add(this.btnImSaveIntelHex);
            this.grpImNvmSettings.Location = new System.Drawing.Point(312, 12);
            this.grpImNvmSettings.Name = "grpImNvmSettings";
            this.grpImNvmSettings.Size = new System.Drawing.Size(273, 229);
            this.grpImNvmSettings.TabIndex = 2;
            this.grpImNvmSettings.TabStop = false;
            this.grpImNvmSettings.Text = "Implant NVM ";
            // 
            // lblImNvmParm4
            // 
            this.lblImNvmParm4.AutoSize = true;
            this.lblImNvmParm4.Location = new System.Drawing.Point(23, 138);
            this.lblImNvmParm4.Name = "lblImNvmParm4";
            this.lblImNvmParm4.Size = new System.Drawing.Size(86, 13);
            this.lblImNvmParm4.TabIndex = 17;
            this.lblImNvmParm4.Text = "RSSI Offset Trim";
            // 
            // txtImNvmParm4
            // 
            this.txtImNvmParm4.Location = new System.Drawing.Point(172, 134);
            this.txtImNvmParm4.Name = "txtImNvmParm4";
            this.txtImNvmParm4.Size = new System.Drawing.Size(74, 20);
            this.txtImNvmParm4.TabIndex = 16;
            // 
            // lblImNvmParm3
            // 
            this.lblImNvmParm3.AutoSize = true;
            this.lblImNvmParm3.Location = new System.Drawing.Point(23, 102);
            this.lblImNvmParm3.Name = "lblImNvmParm3";
            this.lblImNvmParm3.Size = new System.Drawing.Size(45, 13);
            this.lblImNvmParm3.TabIndex = 15;
            this.lblImNvmParm3.Text = "XO Trim";
            // 
            // lblImNvmParm2
            // 
            this.lblImNvmParm2.AutoSize = true;
            this.lblImNvmParm2.Location = new System.Drawing.Point(23, 66);
            this.lblImNvmParm2.Name = "lblImNvmParm2";
            this.lblImNvmParm2.Size = new System.Drawing.Size(132, 13);
            this.lblImNvmParm2.TabIndex = 14;
            this.lblImNvmParm2.Text = "RX IF ADC Decision Level";
            this.lblImNvmParm2.UseMnemonic = false;
            // 
            // lblImNvmParm1
            // 
            this.lblImNvmParm1.AutoSize = true;
            this.lblImNvmParm1.Location = new System.Drawing.Point(23, 30);
            this.lblImNvmParm1.Name = "lblImNvmParm1";
            this.lblImNvmParm1.Size = new System.Drawing.Size(120, 13);
            this.lblImNvmParm1.TabIndex = 13;
            this.lblImNvmParm1.Text = "RSSI-to-dBm Calibration";
            // 
            // txtImNvmParm3
            // 
            this.txtImNvmParm3.Location = new System.Drawing.Point(172, 98);
            this.txtImNvmParm3.Name = "txtImNvmParm3";
            this.txtImNvmParm3.Size = new System.Drawing.Size(74, 20);
            this.txtImNvmParm3.TabIndex = 11;
            // 
            // txtImNvmParm2
            // 
            this.txtImNvmParm2.Location = new System.Drawing.Point(172, 62);
            this.txtImNvmParm2.Name = "txtImNvmParm2";
            this.txtImNvmParm2.Size = new System.Drawing.Size(74, 20);
            this.txtImNvmParm2.TabIndex = 10;
            // 
            // txtImNvmParm1
            // 
            this.txtImNvmParm1.Location = new System.Drawing.Point(172, 26);
            this.txtImNvmParm1.Name = "txtImNvmParm1";
            this.txtImNvmParm1.Size = new System.Drawing.Size(74, 20);
            this.txtImNvmParm1.TabIndex = 9;
            // 
            // btnImLoadIntelHex
            // 
            this.btnImLoadIntelHex.Location = new System.Drawing.Point(148, 184);
            this.btnImLoadIntelHex.Name = "btnImLoadIntelHex";
            this.btnImLoadIntelHex.Size = new System.Drawing.Size(119, 23);
            this.btnImLoadIntelHex.TabIndex = 8;
            this.btnImLoadIntelHex.Text = "Load from Intel Hex";
            this.btnImLoadIntelHex.UseVisualStyleBackColor = true;
            this.btnImLoadIntelHex.Click += new System.EventHandler(this.btnImLoadIntelHex_Click);
            // 
            // btnImSaveIntelHex
            // 
            this.btnImSaveIntelHex.Location = new System.Drawing.Point(13, 184);
            this.btnImSaveIntelHex.Name = "btnImSaveIntelHex";
            this.btnImSaveIntelHex.Size = new System.Drawing.Size(119, 23);
            this.btnImSaveIntelHex.TabIndex = 7;
            this.btnImSaveIntelHex.Text = "Save to Intel Hex";
            this.btnImSaveIntelHex.UseVisualStyleBackColor = true;
            this.btnImSaveIntelHex.Click += new System.EventHandler(this.btnImSaveIntelHex_Click);
            // 
            // NvmForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(605, 450);
            this.Controls.Add(this.grpImNvmSettings);
            this.Controls.Add(this.grpBsmNvmSettings);
            this.Name = "NvmForm";
            this.Text = "Nonvolatile Memory";
            this.grpBsmNvmSettings.ResumeLayout(false);
            this.grpBsmNvmSettings.PerformLayout();
            this.grpImNvmSettings.ResumeLayout(false);
            this.grpImNvmSettings.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion
        private System.Windows.Forms.GroupBox grpBsmNvmSettings;
        private System.Windows.Forms.Button btnBsmLoadIntelHex;
        private System.Windows.Forms.Button btnBsmSaveIntelHex;
        private System.Windows.Forms.TextBox txtBsmNvmParm2;
        private System.Windows.Forms.GroupBox grpImNvmSettings;
        private System.Windows.Forms.TextBox txtImNvmParm3;
        private System.Windows.Forms.TextBox txtImNvmParm2;
        private System.Windows.Forms.TextBox txtImNvmParm1;
        private System.Windows.Forms.Button btnImLoadIntelHex;
        private System.Windows.Forms.Button btnImSaveIntelHex;
        private System.Windows.Forms.Label lblBsmNvmParm2;
        private System.Windows.Forms.Label lblBsmNvmParm1;
        private System.Windows.Forms.Label lblImNvmParm3;
        private System.Windows.Forms.Label lblImNvmParm2;
        private System.Windows.Forms.Label lblImNvmParm1;
        private System.Windows.Forms.Label lblImNvmParm4;
        private System.Windows.Forms.TextBox txtImNvmParm4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBsmNvmCh1;
        private System.Windows.Forms.TextBox txtBsmNvmCh0;
        private System.Windows.Forms.TextBox txtBsmNvmCh3;
        private System.Windows.Forms.TextBox txtBsmNvmCh2;
        private System.Windows.Forms.TextBox txtBsmNvmCh5;
        private System.Windows.Forms.TextBox txtBsmNvmCh4;
        private System.Windows.Forms.TextBox txtBsmNvmCh7;
        private System.Windows.Forms.TextBox txtBsmNvmCh6;
        private System.Windows.Forms.TextBox txtBsmNvmCh8;
        private System.Windows.Forms.TextBox txtBsmNvmCh9;
        private System.Windows.Forms.Button btnCalRssi;
    }
}