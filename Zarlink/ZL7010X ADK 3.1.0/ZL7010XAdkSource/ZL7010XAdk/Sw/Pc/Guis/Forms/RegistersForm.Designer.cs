//
// Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2012. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.

namespace Zarlink.Adk.Forms
{
    partial class RegistersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RegistersForm));
            this.lblRegInfo = new System.Windows.Forms.Label();
            this.grpRegRadix = new System.Windows.Forms.GroupBox();
            this.btnRegDec = new System.Windows.Forms.RadioButton();
            this.btnRegHex = new System.Windows.Forms.RadioButton();
            this.chbRegSave = new System.Windows.Forms.CheckBox();
            this.btnRegWrAll = new System.Windows.Forms.Button();
            this.btnRegRdAll = new System.Windows.Forms.Button();
            this.grpRegControls = new System.Windows.Forms.GroupBox();
            this.grpRegSearch = new System.Windows.Forms.GroupBox();
            this.txtRegSearch = new System.Windows.Forms.TextBox();
            this.grpRegAccess = new System.Windows.Forms.GroupBox();
            this.btnRegAccessWrite = new System.Windows.Forms.Button();
            this.btnRegAccessRead = new System.Windows.Forms.Button();
            this.lblRegAccessValue = new System.Windows.Forms.Label();
            this.txtRegAccessValue = new System.Windows.Forms.TextBox();
            this.lblRegAccessAddr = new System.Windows.Forms.Label();
            this.txtRegAccessAddr = new System.Windows.Forms.TextBox();
            this.tipRegFrm = new System.Windows.Forms.ToolTip(this.components);
            this.tabRegP2RF = new System.Windows.Forms.TabPage();
            this.grpRegP2RF = new System.Windows.Forms.GroupBox();
            this.tabRegP2Wake = new System.Windows.Forms.TabPage();
            this.grpRegP2Wake = new System.Windows.Forms.GroupBox();
            this.tabRegP2Mac2 = new System.Windows.Forms.TabPage();
            this.grpRegP2Mac2 = new System.Windows.Forms.GroupBox();
            this.tabRegP2Mac1 = new System.Windows.Forms.TabPage();
            this.grpRegP2Mac1 = new System.Windows.Forms.GroupBox();
            this.tabRegRF = new System.Windows.Forms.TabPage();
            this.grpRegRF = new System.Windows.Forms.GroupBox();
            this.tabRegWake = new System.Windows.Forms.TabPage();
            this.grpRegWake = new System.Windows.Forms.GroupBox();
            this.tabRegMac2 = new System.Windows.Forms.TabPage();
            this.grpRegMac2 = new System.Windows.Forms.GroupBox();
            this.tabRegMac1 = new System.Windows.Forms.TabPage();
            this.grpRegMac1 = new System.Windows.Forms.GroupBox();
            this.grpRegModel = new System.Windows.Forms.GroupBox();
            this.grpRegBitsModel = new System.Windows.Forms.GroupBox();
            this.txtRegBit0Model = new System.Windows.Forms.TextBox();
            this.txtRegBit1Model = new System.Windows.Forms.TextBox();
            this.txtRegBit2Model = new System.Windows.Forms.TextBox();
            this.txtRegBit3Model = new System.Windows.Forms.TextBox();
            this.txtRegBit4Model = new System.Windows.Forms.TextBox();
            this.txtRegBit5Model = new System.Windows.Forms.TextBox();
            this.txtRegBit6Model = new System.Windows.Forms.TextBox();
            this.txtRegBit7Model = new System.Windows.Forms.TextBox();
            this.btnRegWrModel = new System.Windows.Forms.Button();
            this.btnRegRdModel = new System.Windows.Forms.Button();
            this.txtRegValModel = new System.Windows.Forms.TextBox();
            this.tabReg = new System.Windows.Forms.TabControl();
            this.grpRegRadix.SuspendLayout();
            this.grpRegControls.SuspendLayout();
            this.grpRegSearch.SuspendLayout();
            this.grpRegAccess.SuspendLayout();
            this.tabRegP2RF.SuspendLayout();
            this.tabRegP2Wake.SuspendLayout();
            this.tabRegP2Mac2.SuspendLayout();
            this.tabRegP2Mac1.SuspendLayout();
            this.tabRegRF.SuspendLayout();
            this.tabRegWake.SuspendLayout();
            this.tabRegMac2.SuspendLayout();
            this.tabRegMac1.SuspendLayout();
            this.grpRegMac1.SuspendLayout();
            this.grpRegModel.SuspendLayout();
            this.grpRegBitsModel.SuspendLayout();
            this.tabReg.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblRegInfo
            // 
            this.lblRegInfo.AutoSize = true;
            this.lblRegInfo.Location = new System.Drawing.Point(29, 47);
            this.lblRegInfo.Name = "lblRegInfo";
            this.lblRegInfo.Size = new System.Drawing.Size(227, 13);
            this.lblRegInfo.TabIndex = 119;
            this.lblRegInfo.Text = "Double click register name for more information";
            // 
            // grpRegRadix
            // 
            this.grpRegRadix.Controls.Add(this.btnRegDec);
            this.grpRegRadix.Controls.Add(this.btnRegHex);
            this.grpRegRadix.Location = new System.Drawing.Point(643, 8);
            this.grpRegRadix.Name = "grpRegRadix";
            this.grpRegRadix.Size = new System.Drawing.Size(116, 60);
            this.grpRegRadix.TabIndex = 118;
            this.grpRegRadix.TabStop = false;
            this.grpRegRadix.Text = "Radix";
            // 
            // btnRegDec
            // 
            this.btnRegDec.AutoSize = true;
            this.btnRegDec.Location = new System.Drawing.Point(16, 36);
            this.btnRegDec.Name = "btnRegDec";
            this.btnRegDec.Size = new System.Drawing.Size(63, 17);
            this.btnRegDec.TabIndex = 117;
            this.btnRegDec.Text = "Decimal";
            this.btnRegDec.UseVisualStyleBackColor = true;
            this.btnRegDec.CheckedChanged += new System.EventHandler(this.btnRegDec_CheckedChanged);
            // 
            // btnRegHex
            // 
            this.btnRegHex.AutoSize = true;
            this.btnRegHex.Checked = true;
            this.btnRegHex.Location = new System.Drawing.Point(16, 16);
            this.btnRegHex.Name = "btnRegHex";
            this.btnRegHex.Size = new System.Drawing.Size(86, 17);
            this.btnRegHex.TabIndex = 116;
            this.btnRegHex.TabStop = true;
            this.btnRegHex.Text = "Hexadecimal";
            this.btnRegHex.UseVisualStyleBackColor = true;
            this.btnRegHex.CheckedChanged += new System.EventHandler(this.btnRegHex_CheckedChanged);
            // 
            // chbRegSave
            // 
            this.chbRegSave.AutoSize = true;
            this.chbRegSave.Location = new System.Drawing.Point(172, 20);
            this.chbRegSave.Name = "chbRegSave";
            this.chbRegSave.Size = new System.Drawing.Size(129, 17);
            this.chbRegSave.TabIndex = 114;
            this.chbRegSave.Text = "Save Registers to File";
            this.chbRegSave.UseVisualStyleBackColor = true;
            this.chbRegSave.CheckedChanged += new System.EventHandler(this.chbRegSave_CheckedChanged);
            // 
            // btnRegWrAll
            // 
            this.btnRegWrAll.Location = new System.Drawing.Point(8, 13);
            this.btnRegWrAll.Name = "btnRegWrAll";
            this.btnRegWrAll.Size = new System.Drawing.Size(76, 28);
            this.btnRegWrAll.TabIndex = 113;
            this.btnRegWrAll.Text = "Write All";
            this.btnRegWrAll.UseVisualStyleBackColor = true;
            this.btnRegWrAll.Click += new System.EventHandler(this.btnRegWrAll_Click);
            // 
            // btnRegRdAll
            // 
            this.btnRegRdAll.Location = new System.Drawing.Point(90, 13);
            this.btnRegRdAll.Name = "btnRegRdAll";
            this.btnRegRdAll.Size = new System.Drawing.Size(76, 28);
            this.btnRegRdAll.TabIndex = 112;
            this.btnRegRdAll.Text = "Read All";
            this.btnRegRdAll.UseVisualStyleBackColor = true;
            this.btnRegRdAll.Click += new System.EventHandler(this.btnRegRdAll_Click);
            // 
            // grpRegControls
            // 
            this.grpRegControls.Controls.Add(this.grpRegSearch);
            this.grpRegControls.Controls.Add(this.grpRegAccess);
            this.grpRegControls.Controls.Add(this.lblRegInfo);
            this.grpRegControls.Controls.Add(this.grpRegRadix);
            this.grpRegControls.Controls.Add(this.chbRegSave);
            this.grpRegControls.Controls.Add(this.btnRegWrAll);
            this.grpRegControls.Controls.Add(this.btnRegRdAll);
            this.grpRegControls.Location = new System.Drawing.Point(3, 401);
            this.grpRegControls.Name = "grpRegControls";
            this.grpRegControls.Size = new System.Drawing.Size(775, 73);
            this.grpRegControls.TabIndex = 3;
            this.grpRegControls.TabStop = false;
            this.grpRegControls.Text = "Register Controls";
            // 
            // grpRegSearch
            // 
            this.grpRegSearch.Controls.Add(this.txtRegSearch);
            this.grpRegSearch.Location = new System.Drawing.Point(307, 8);
            this.grpRegSearch.Name = "grpRegSearch";
            this.grpRegSearch.Size = new System.Drawing.Size(147, 60);
            this.grpRegSearch.TabIndex = 122;
            this.grpRegSearch.TabStop = false;
            this.grpRegSearch.Text = "Register Name Search";
            // 
            // txtRegSearch
            // 
            this.txtRegSearch.Location = new System.Drawing.Point(4, 25);
            this.txtRegSearch.Margin = new System.Windows.Forms.Padding(2);
            this.txtRegSearch.Name = "txtRegSearch";
            this.txtRegSearch.Size = new System.Drawing.Size(140, 20);
            this.txtRegSearch.TabIndex = 49;
            this.txtRegSearch.Text = "Enter Reg Name (Hit Enter)";
            this.txtRegSearch.Click += new System.EventHandler(this.txtRegSearch_Click);
            this.txtRegSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRegSearch_KeyPress);
            // 
            // grpRegAccess
            // 
            this.grpRegAccess.Controls.Add(this.btnRegAccessWrite);
            this.grpRegAccess.Controls.Add(this.btnRegAccessRead);
            this.grpRegAccess.Controls.Add(this.lblRegAccessValue);
            this.grpRegAccess.Controls.Add(this.txtRegAccessValue);
            this.grpRegAccess.Controls.Add(this.lblRegAccessAddr);
            this.grpRegAccess.Controls.Add(this.txtRegAccessAddr);
            this.grpRegAccess.Location = new System.Drawing.Point(459, 8);
            this.grpRegAccess.Margin = new System.Windows.Forms.Padding(2);
            this.grpRegAccess.Name = "grpRegAccess";
            this.grpRegAccess.Padding = new System.Windows.Forms.Padding(2);
            this.grpRegAccess.Size = new System.Drawing.Size(180, 60);
            this.grpRegAccess.TabIndex = 121;
            this.grpRegAccess.TabStop = false;
            this.grpRegAccess.Text = "Register Access";
            // 
            // btnRegAccessWrite
            // 
            this.btnRegAccessWrite.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegAccessWrite.Location = new System.Drawing.Point(127, 36);
            this.btnRegAccessWrite.Name = "btnRegAccessWrite";
            this.btnRegAccessWrite.Size = new System.Drawing.Size(50, 20);
            this.btnRegAccessWrite.TabIndex = 48;
            this.btnRegAccessWrite.Text = "Write";
            this.btnRegAccessWrite.UseVisualStyleBackColor = true;
            this.btnRegAccessWrite.Click += new System.EventHandler(this.btnRegAccessWrite_Click);
            // 
            // btnRegAccessRead
            // 
            this.btnRegAccessRead.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegAccessRead.Location = new System.Drawing.Point(127, 15);
            this.btnRegAccessRead.Name = "btnRegAccessRead";
            this.btnRegAccessRead.Size = new System.Drawing.Size(50, 20);
            this.btnRegAccessRead.TabIndex = 47;
            this.btnRegAccessRead.Text = "Read";
            this.btnRegAccessRead.UseVisualStyleBackColor = true;
            this.btnRegAccessRead.Click += new System.EventHandler(this.btnRegAccessRead_Click);
            // 
            // lblRegAccessValue
            // 
            this.lblRegAccessValue.AutoSize = true;
            this.lblRegAccessValue.Location = new System.Drawing.Point(6, 38);
            this.lblRegAccessValue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblRegAccessValue.Name = "lblRegAccessValue";
            this.lblRegAccessValue.Size = new System.Drawing.Size(63, 13);
            this.lblRegAccessValue.TabIndex = 3;
            this.lblRegAccessValue.Text = "Value (hex):";
            // 
            // txtRegAccessValue
            // 
            this.txtRegAccessValue.Location = new System.Drawing.Point(85, 35);
            this.txtRegAccessValue.Margin = new System.Windows.Forms.Padding(2);
            this.txtRegAccessValue.Name = "txtRegAccessValue";
            this.txtRegAccessValue.Size = new System.Drawing.Size(37, 20);
            this.txtRegAccessValue.TabIndex = 2;
            this.txtRegAccessValue.Text = "0";
            this.txtRegAccessValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblRegAccessAddr
            // 
            this.lblRegAccessAddr.AutoSize = true;
            this.lblRegAccessAddr.Location = new System.Drawing.Point(6, 18);
            this.lblRegAccessAddr.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblRegAccessAddr.Name = "lblRegAccessAddr";
            this.lblRegAccessAddr.Size = new System.Drawing.Size(75, 13);
            this.lblRegAccessAddr.TabIndex = 1;
            this.lblRegAccessAddr.Text = "Address (dec):";
            // 
            // txtRegAccessAddr
            // 
            this.txtRegAccessAddr.Location = new System.Drawing.Point(85, 15);
            this.txtRegAccessAddr.Margin = new System.Windows.Forms.Padding(2);
            this.txtRegAccessAddr.Name = "txtRegAccessAddr";
            this.txtRegAccessAddr.Size = new System.Drawing.Size(37, 20);
            this.txtRegAccessAddr.TabIndex = 0;
            this.txtRegAccessAddr.Text = "0";
            this.txtRegAccessAddr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRegAccessAddr.TextChanged += new System.EventHandler(this.txtRegAccessAddr_TextChanged);
            // 
            // tabRegP2RF
            // 
            this.tabRegP2RF.Controls.Add(this.grpRegP2RF);
            this.tabRegP2RF.Location = new System.Drawing.Point(4, 22);
            this.tabRegP2RF.Name = "tabRegP2RF";
            this.tabRegP2RF.Size = new System.Drawing.Size(763, 368);
            this.tabRegP2RF.TabIndex = 7;
            this.tabRegP2RF.Text = "(P2: 96-127) RF";
            this.tabRegP2RF.UseVisualStyleBackColor = true;
            // 
            // grpRegP2RF
            // 
            this.grpRegP2RF.Location = new System.Drawing.Point(0, 3);
            this.grpRegP2RF.Name = "grpRegP2RF";
            this.grpRegP2RF.Size = new System.Drawing.Size(760, 348);
            this.grpRegP2RF.TabIndex = 7;
            this.grpRegP2RF.TabStop = false;
            this.grpRegP2RF.Text = "Page 2: RF Registers";
            // 
            // tabRegP2Wake
            // 
            this.tabRegP2Wake.Controls.Add(this.grpRegP2Wake);
            this.tabRegP2Wake.Location = new System.Drawing.Point(4, 22);
            this.tabRegP2Wake.Name = "tabRegP2Wake";
            this.tabRegP2Wake.Size = new System.Drawing.Size(763, 368);
            this.tabRegP2Wake.TabIndex = 6;
            this.tabRegP2Wake.Text = "(P2: 64-95) Wake-up";
            this.tabRegP2Wake.UseVisualStyleBackColor = true;
            // 
            // grpRegP2Wake
            // 
            this.grpRegP2Wake.Location = new System.Drawing.Point(0, 3);
            this.grpRegP2Wake.Name = "grpRegP2Wake";
            this.grpRegP2Wake.Size = new System.Drawing.Size(760, 348);
            this.grpRegP2Wake.TabIndex = 6;
            this.grpRegP2Wake.TabStop = false;
            this.grpRegP2Wake.Text = "Page 2: Wake-up Registers";
            // 
            // tabRegP2Mac2
            // 
            this.tabRegP2Mac2.Controls.Add(this.grpRegP2Mac2);
            this.tabRegP2Mac2.Location = new System.Drawing.Point(4, 22);
            this.tabRegP2Mac2.Name = "tabRegP2Mac2";
            this.tabRegP2Mac2.Size = new System.Drawing.Size(763, 368);
            this.tabRegP2Mac2.TabIndex = 5;
            this.tabRegP2Mac2.Text = "(P2: 32-63) MAC";
            this.tabRegP2Mac2.UseVisualStyleBackColor = true;
            // 
            // grpRegP2Mac2
            // 
            this.grpRegP2Mac2.Location = new System.Drawing.Point(0, 3);
            this.grpRegP2Mac2.Name = "grpRegP2Mac2";
            this.grpRegP2Mac2.Size = new System.Drawing.Size(760, 348);
            this.grpRegP2Mac2.TabIndex = 5;
            this.grpRegP2Mac2.TabStop = false;
            this.grpRegP2Mac2.Text = "Page 2: MAC Registers";
            // 
            // tabRegP2Mac1
            // 
            this.tabRegP2Mac1.Controls.Add(this.grpRegP2Mac1);
            this.tabRegP2Mac1.Location = new System.Drawing.Point(4, 22);
            this.tabRegP2Mac1.Name = "tabRegP2Mac1";
            this.tabRegP2Mac1.Size = new System.Drawing.Size(763, 368);
            this.tabRegP2Mac1.TabIndex = 4;
            this.tabRegP2Mac1.Text = "(P2: 0-31) MAC";
            this.tabRegP2Mac1.UseVisualStyleBackColor = true;
            // 
            // grpRegP2Mac1
            // 
            this.grpRegP2Mac1.Location = new System.Drawing.Point(0, 3);
            this.grpRegP2Mac1.Name = "grpRegP2Mac1";
            this.grpRegP2Mac1.Size = new System.Drawing.Size(760, 348);
            this.grpRegP2Mac1.TabIndex = 4;
            this.grpRegP2Mac1.TabStop = false;
            this.grpRegP2Mac1.Text = "Page 2: MAC Registers";
            // 
            // tabRegRF
            // 
            this.tabRegRF.Controls.Add(this.grpRegRF);
            this.tabRegRF.Location = new System.Drawing.Point(4, 22);
            this.tabRegRF.Name = "tabRegRF";
            this.tabRegRF.Size = new System.Drawing.Size(763, 368);
            this.tabRegRF.TabIndex = 3;
            this.tabRegRF.Text = "(96-127) RF";
            this.tabRegRF.UseVisualStyleBackColor = true;
            // 
            // grpRegRF
            // 
            this.grpRegRF.Location = new System.Drawing.Point(0, 3);
            this.grpRegRF.Name = "grpRegRF";
            this.grpRegRF.Size = new System.Drawing.Size(760, 348);
            this.grpRegRF.TabIndex = 3;
            this.grpRegRF.TabStop = false;
            this.grpRegRF.Text = "RF Registers";
            // 
            // tabRegWake
            // 
            this.tabRegWake.Controls.Add(this.grpRegWake);
            this.tabRegWake.Location = new System.Drawing.Point(4, 22);
            this.tabRegWake.Name = "tabRegWake";
            this.tabRegWake.Size = new System.Drawing.Size(763, 368);
            this.tabRegWake.TabIndex = 2;
            this.tabRegWake.Text = "(64-95) Wake-up";
            this.tabRegWake.UseVisualStyleBackColor = true;
            // 
            // grpRegWake
            // 
            this.grpRegWake.Location = new System.Drawing.Point(0, 3);
            this.grpRegWake.Name = "grpRegWake";
            this.grpRegWake.Size = new System.Drawing.Size(760, 348);
            this.grpRegWake.TabIndex = 2;
            this.grpRegWake.TabStop = false;
            this.grpRegWake.Text = "Wake-up Registers";
            // 
            // tabRegMac2
            // 
            this.tabRegMac2.Controls.Add(this.grpRegMac2);
            this.tabRegMac2.Location = new System.Drawing.Point(4, 22);
            this.tabRegMac2.Name = "tabRegMac2";
            this.tabRegMac2.Padding = new System.Windows.Forms.Padding(3);
            this.tabRegMac2.Size = new System.Drawing.Size(763, 368);
            this.tabRegMac2.TabIndex = 1;
            this.tabRegMac2.Text = "(32-63) MAC";
            this.tabRegMac2.UseVisualStyleBackColor = true;
            // 
            // grpRegMac2
            // 
            this.grpRegMac2.Location = new System.Drawing.Point(0, 3);
            this.grpRegMac2.Name = "grpRegMac2";
            this.grpRegMac2.Size = new System.Drawing.Size(760, 348);
            this.grpRegMac2.TabIndex = 1;
            this.grpRegMac2.TabStop = false;
            this.grpRegMac2.Text = "MAC Registers";
            // 
            // tabRegMac1
            // 
            this.tabRegMac1.Controls.Add(this.grpRegMac1);
            this.tabRegMac1.Location = new System.Drawing.Point(4, 22);
            this.tabRegMac1.Name = "tabRegMac1";
            this.tabRegMac1.Padding = new System.Windows.Forms.Padding(3);
            this.tabRegMac1.Size = new System.Drawing.Size(763, 368);
            this.tabRegMac1.TabIndex = 0;
            this.tabRegMac1.Text = "(0-31) MAC";
            this.tabRegMac1.UseVisualStyleBackColor = true;
            // 
            // grpRegMac1
            // 
            this.grpRegMac1.Controls.Add(this.grpRegModel);
            this.grpRegMac1.Location = new System.Drawing.Point(0, 3);
            this.grpRegMac1.Name = "grpRegMac1";
            this.grpRegMac1.Size = new System.Drawing.Size(760, 348);
            this.grpRegMac1.TabIndex = 0;
            this.grpRegMac1.TabStop = false;
            this.grpRegMac1.Text = "MAC Registers";
            // 
            // grpRegModel
            // 
            this.grpRegModel.Controls.Add(this.grpRegBitsModel);
            this.grpRegModel.Controls.Add(this.btnRegWrModel);
            this.grpRegModel.Controls.Add(this.btnRegRdModel);
            this.grpRegModel.Controls.Add(this.txtRegValModel);
            this.grpRegModel.Enabled = false;
            this.grpRegModel.Location = new System.Drawing.Point(8, 12);
            this.grpRegModel.Margin = new System.Windows.Forms.Padding(4);
            this.grpRegModel.Name = "grpRegModel";
            this.grpRegModel.Padding = new System.Windows.Forms.Padding(4);
            this.grpRegModel.Size = new System.Drawing.Size(180, 40);
            this.grpRegModel.TabIndex = 0;
            this.grpRegModel.TabStop = false;
            this.grpRegModel.Text = "4 - txbuff_bsize";
            this.grpRegModel.Visible = false;
            // 
            // grpRegBitsModel
            // 
            this.grpRegBitsModel.Controls.Add(this.txtRegBit0Model);
            this.grpRegBitsModel.Controls.Add(this.txtRegBit1Model);
            this.grpRegBitsModel.Controls.Add(this.txtRegBit2Model);
            this.grpRegBitsModel.Controls.Add(this.txtRegBit3Model);
            this.grpRegBitsModel.Controls.Add(this.txtRegBit4Model);
            this.grpRegBitsModel.Controls.Add(this.txtRegBit5Model);
            this.grpRegBitsModel.Controls.Add(this.txtRegBit6Model);
            this.grpRegBitsModel.Controls.Add(this.txtRegBit7Model);
            this.grpRegBitsModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 0.1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpRegBitsModel.Location = new System.Drawing.Point(5, 13);
            this.grpRegBitsModel.Margin = new System.Windows.Forms.Padding(4);
            this.grpRegBitsModel.Name = "grpRegBitsModel";
            this.grpRegBitsModel.Padding = new System.Windows.Forms.Padding(4);
            this.grpRegBitsModel.Size = new System.Drawing.Size(104, 24);
            this.grpRegBitsModel.TabIndex = 0;
            this.grpRegBitsModel.TabStop = false;
            this.grpRegBitsModel.Tag = "000001";
            // 
            // txtRegBit0Model
            // 
            this.txtRegBit0Model.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegBit0Model.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtRegBit0Model.Location = new System.Drawing.Point(4, 2);
            this.txtRegBit0Model.Margin = new System.Windows.Forms.Padding(4);
            this.txtRegBit0Model.MaxLength = 1;
            this.txtRegBit0Model.Name = "txtRegBit0Model";
            this.txtRegBit0Model.Size = new System.Drawing.Size(13, 20);
            this.txtRegBit0Model.TabIndex = 0;
            this.txtRegBit0Model.Text = "0";
            // 
            // txtRegBit1Model
            // 
            this.txtRegBit1Model.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegBit1Model.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtRegBit1Model.Location = new System.Drawing.Point(16, 2);
            this.txtRegBit1Model.Margin = new System.Windows.Forms.Padding(4);
            this.txtRegBit1Model.MaxLength = 1;
            this.txtRegBit1Model.Name = "txtRegBit1Model";
            this.txtRegBit1Model.Size = new System.Drawing.Size(13, 20);
            this.txtRegBit1Model.TabIndex = 1;
            this.txtRegBit1Model.Text = "0";
            // 
            // txtRegBit2Model
            // 
            this.txtRegBit2Model.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegBit2Model.Location = new System.Drawing.Point(28, 2);
            this.txtRegBit2Model.Margin = new System.Windows.Forms.Padding(4);
            this.txtRegBit2Model.MaxLength = 1;
            this.txtRegBit2Model.Name = "txtRegBit2Model";
            this.txtRegBit2Model.Size = new System.Drawing.Size(13, 20);
            this.txtRegBit2Model.TabIndex = 2;
            this.txtRegBit2Model.Text = "0";
            // 
            // txtRegBit3Model
            // 
            this.txtRegBit3Model.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegBit3Model.Location = new System.Drawing.Point(40, 2);
            this.txtRegBit3Model.Margin = new System.Windows.Forms.Padding(4);
            this.txtRegBit3Model.MaxLength = 1;
            this.txtRegBit3Model.Name = "txtRegBit3Model";
            this.txtRegBit3Model.Size = new System.Drawing.Size(13, 20);
            this.txtRegBit3Model.TabIndex = 3;
            this.txtRegBit3Model.Text = "0";
            // 
            // txtRegBit4Model
            // 
            this.txtRegBit4Model.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegBit4Model.Location = new System.Drawing.Point(52, 2);
            this.txtRegBit4Model.Margin = new System.Windows.Forms.Padding(4);
            this.txtRegBit4Model.MaxLength = 1;
            this.txtRegBit4Model.Name = "txtRegBit4Model";
            this.txtRegBit4Model.Size = new System.Drawing.Size(13, 20);
            this.txtRegBit4Model.TabIndex = 4;
            this.txtRegBit4Model.Text = "0";
            // 
            // txtRegBit5Model
            // 
            this.txtRegBit5Model.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegBit5Model.Location = new System.Drawing.Point(64, 2);
            this.txtRegBit5Model.Margin = new System.Windows.Forms.Padding(4);
            this.txtRegBit5Model.MaxLength = 1;
            this.txtRegBit5Model.Name = "txtRegBit5Model";
            this.txtRegBit5Model.Size = new System.Drawing.Size(13, 20);
            this.txtRegBit5Model.TabIndex = 5;
            this.txtRegBit5Model.Text = "0";
            // 
            // txtRegBit6Model
            // 
            this.txtRegBit6Model.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegBit6Model.Location = new System.Drawing.Point(76, 2);
            this.txtRegBit6Model.Margin = new System.Windows.Forms.Padding(4);
            this.txtRegBit6Model.MaxLength = 1;
            this.txtRegBit6Model.Name = "txtRegBit6Model";
            this.txtRegBit6Model.Size = new System.Drawing.Size(13, 20);
            this.txtRegBit6Model.TabIndex = 6;
            this.txtRegBit6Model.Text = "0";
            // 
            // txtRegBit7Model
            // 
            this.txtRegBit7Model.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegBit7Model.Location = new System.Drawing.Point(88, 2);
            this.txtRegBit7Model.Margin = new System.Windows.Forms.Padding(4);
            this.txtRegBit7Model.MaxLength = 1;
            this.txtRegBit7Model.Name = "txtRegBit7Model";
            this.txtRegBit7Model.Size = new System.Drawing.Size(13, 20);
            this.txtRegBit7Model.TabIndex = 7;
            this.txtRegBit7Model.Text = "0";
            this.txtRegBit7Model.WordWrap = false;
            // 
            // btnRegWrModel
            // 
            this.btnRegWrModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegWrModel.Location = new System.Drawing.Point(162, 15);
            this.btnRegWrModel.Name = "btnRegWrModel";
            this.btnRegWrModel.Size = new System.Drawing.Size(14, 20);
            this.btnRegWrModel.TabIndex = 10;
            this.btnRegWrModel.Text = "W";
            this.btnRegWrModel.UseVisualStyleBackColor = true;
            // 
            // btnRegRdModel
            // 
            this.btnRegRdModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegRdModel.Location = new System.Drawing.Point(147, 15);
            this.btnRegRdModel.Name = "btnRegRdModel";
            this.btnRegRdModel.Size = new System.Drawing.Size(14, 20);
            this.btnRegRdModel.TabIndex = 9;
            this.btnRegRdModel.Text = "R";
            this.btnRegRdModel.UseVisualStyleBackColor = true;
            // 
            // txtRegValModel
            // 
            this.txtRegValModel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRegValModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegValModel.Location = new System.Drawing.Point(113, 15);
            this.txtRegValModel.Margin = new System.Windows.Forms.Padding(4);
            this.txtRegValModel.MaxLength = 2;
            this.txtRegValModel.Name = "txtRegValModel";
            this.txtRegValModel.Size = new System.Drawing.Size(30, 20);
            this.txtRegValModel.TabIndex = 8;
            this.txtRegValModel.Text = "00";
            this.txtRegValModel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tabReg
            // 
            this.tabReg.Controls.Add(this.tabRegMac1);
            this.tabReg.Controls.Add(this.tabRegMac2);
            this.tabReg.Controls.Add(this.tabRegWake);
            this.tabReg.Controls.Add(this.tabRegRF);
            this.tabReg.Controls.Add(this.tabRegP2Mac1);
            this.tabReg.Controls.Add(this.tabRegP2Mac2);
            this.tabReg.Controls.Add(this.tabRegP2Wake);
            this.tabReg.Controls.Add(this.tabRegP2RF);
            this.tabReg.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabReg.Location = new System.Drawing.Point(0, 0);
            this.tabReg.Name = "tabReg";
            this.tabReg.SelectedIndex = 0;
            this.tabReg.Size = new System.Drawing.Size(771, 394);
            this.tabReg.TabIndex = 2;
            // 
            // RegistersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(771, 476);
            this.Controls.Add(this.grpRegControls);
            this.Controls.Add(this.tabReg);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RegistersForm";
            this.Text = "ZL70102 Registers";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RegistersForm_FormClosing);
            this.grpRegRadix.ResumeLayout(false);
            this.grpRegRadix.PerformLayout();
            this.grpRegControls.ResumeLayout(false);
            this.grpRegControls.PerformLayout();
            this.grpRegSearch.ResumeLayout(false);
            this.grpRegSearch.PerformLayout();
            this.grpRegAccess.ResumeLayout(false);
            this.grpRegAccess.PerformLayout();
            this.tabRegP2RF.ResumeLayout(false);
            this.tabRegP2Wake.ResumeLayout(false);
            this.tabRegP2Mac2.ResumeLayout(false);
            this.tabRegP2Mac1.ResumeLayout(false);
            this.tabRegRF.ResumeLayout(false);
            this.tabRegWake.ResumeLayout(false);
            this.tabRegMac2.ResumeLayout(false);
            this.tabRegMac1.ResumeLayout(false);
            this.grpRegMac1.ResumeLayout(false);
            this.grpRegModel.ResumeLayout(false);
            this.grpRegModel.PerformLayout();
            this.grpRegBitsModel.ResumeLayout(false);
            this.grpRegBitsModel.PerformLayout();
            this.tabReg.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        // Register 000
        private System.Windows.Forms.CheckBox chbRegSave;
        private System.Windows.Forms.Button btnRegWrAll;
        private System.Windows.Forms.Button btnRegRdAll;
        private System.Windows.Forms.GroupBox grpRegRadix;
        private System.Windows.Forms.RadioButton btnRegDec;
        private System.Windows.Forms.RadioButton btnRegHex;
        private System.Windows.Forms.Label lblRegInfo;
        private System.Windows.Forms.GroupBox grpRegControls;
        private System.Windows.Forms.GroupBox grpRegAccess;
        private System.Windows.Forms.Button btnRegAccessWrite;
        private System.Windows.Forms.Button btnRegAccessRead;
        private System.Windows.Forms.Label lblRegAccessValue;
        private System.Windows.Forms.TextBox txtRegAccessValue;
        private System.Windows.Forms.Label lblRegAccessAddr;
        private System.Windows.Forms.TextBox txtRegAccessAddr;
        private System.Windows.Forms.ToolTip tipRegFrm;
        private System.Windows.Forms.TabPage tabRegP2RF;
        private System.Windows.Forms.TabPage tabRegP2Wake;
        private System.Windows.Forms.GroupBox grpRegP2Wake;
        private System.Windows.Forms.TabPage tabRegP2Mac2;
        private System.Windows.Forms.GroupBox grpRegP2Mac2;
        private System.Windows.Forms.TabPage tabRegP2Mac1;
        private System.Windows.Forms.GroupBox grpRegP2Mac1;
        private System.Windows.Forms.TabPage tabRegRF;
        private System.Windows.Forms.GroupBox grpRegRF;
        private System.Windows.Forms.TabPage tabRegWake;
        private System.Windows.Forms.GroupBox grpRegWake;
        private System.Windows.Forms.TabPage tabRegMac2;
        private System.Windows.Forms.GroupBox grpRegMac2;
        private System.Windows.Forms.TabPage tabRegMac1;
        private System.Windows.Forms.GroupBox grpRegMac1;
        private System.Windows.Forms.GroupBox grpRegModel;
        private System.Windows.Forms.GroupBox grpRegBitsModel;
        private System.Windows.Forms.TextBox txtRegBit0Model;
        private System.Windows.Forms.TextBox txtRegBit1Model;
        private System.Windows.Forms.TextBox txtRegBit2Model;
        private System.Windows.Forms.TextBox txtRegBit3Model;
        private System.Windows.Forms.TextBox txtRegBit4Model;
        private System.Windows.Forms.TextBox txtRegBit5Model;
        private System.Windows.Forms.TextBox txtRegBit6Model;
        private System.Windows.Forms.TextBox txtRegBit7Model;
        private System.Windows.Forms.Button btnRegWrModel;
        private System.Windows.Forms.Button btnRegRdModel;
        private System.Windows.Forms.TextBox txtRegValModel;
        private System.Windows.Forms.TabControl tabReg;
        private System.Windows.Forms.GroupBox grpRegP2RF;
        private System.Windows.Forms.TextBox txtRegSearch;
        private System.Windows.Forms.GroupBox grpRegSearch;
    }
}
