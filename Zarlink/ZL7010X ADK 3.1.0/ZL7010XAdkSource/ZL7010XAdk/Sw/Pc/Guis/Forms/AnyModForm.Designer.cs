//
// Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2012. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.

namespace Zarlink.Adk.Forms
{
    partial class AnyModForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AnyModForm));
            this.txtRssiDbm5 = new System.Windows.Forms.TextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.bsmTabs = new System.Windows.Forms.TabControl();
            this.tabLinkSetup = new System.Windows.Forms.TabPage();
            this.grpImInfo = new System.Windows.Forms.GroupBox();
            this.btnLoadImInfo = new System.Windows.Forms.Button();
            this.btnStoreImInfo = new System.Windows.Forms.Button();
            this.dgvImInfo = new System.Windows.Forms.DataGridView();
            this.companyId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imdTransId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.companyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imdDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpLinkSetup = new System.Windows.Forms.GroupBox();
            this.cmbImplantType = new System.Windows.Forms.ComboBox();
            this.cmbWake245UserData = new System.Windows.Forms.ComboBox();
            this.label146 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.cmbEmerMaxBlocksPerPack = new System.Windows.Forms.ComboBox();
            this.cmbEmerBytesPerBlock = new System.Windows.Forms.ComboBox();
            this.txtCompanyId2 = new System.Windows.Forms.TextBox();
            this.txtCompanyId1 = new System.Windows.Forms.TextBox();
            this.label140 = new System.Windows.Forms.Label();
            this.txtImdTid6 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtImdTid5 = new System.Windows.Forms.TextBox();
            this.txtImdTid4 = new System.Windows.Forms.TextBox();
            this.cmbWake245Country = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtImdTid3 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.cmbEmerChan = new System.Windows.Forms.ComboBox();
            this.txtImdTid2 = new System.Windows.Forms.TextBox();
            this.cmbEmerRxMod = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbEmerTxMod = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtImdTid1 = new System.Windows.Forms.TextBox();
            this.cmbNormMaxBlocksPerPack = new System.Windows.Forms.ComboBox();
            this.cmbNormBytesPerBlock = new System.Windows.Forms.ComboBox();
            this.cmbNormChan = new System.Windows.Forms.ComboBox();
            this.cmbNormRxMod = new System.Windows.Forms.ComboBox();
            this.cmbNormTxMod = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabCcaOrRssiAndCal = new System.Windows.Forms.TabPage();
            this.grpCal = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnCalReq = new System.Windows.Forms.Button();
            this.lblCalReq = new System.Windows.Forms.Label();
            this.btn24CrystOsc = new System.Windows.Forms.Button();
            this.grpHighLevelTuning = new System.Windows.Forms.GroupBox();
            this.btnHighLevelCal = new System.Windows.Forms.Button();
            this.chbTxTuneCap = new System.Windows.Forms.CheckBox();
            this.chbMatch2TuneCap = new System.Windows.Forms.CheckBox();
            this.chbMatch1TuneCap = new System.Windows.Forms.CheckBox();
            this.chbHighLevelCalExtAlg = new System.Windows.Forms.CheckBox();
            this.chbHighLevelCalRx = new System.Windows.Forms.CheckBox();
            this.chbHighLevelCalTx = new System.Windows.Forms.CheckBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.txtHighLevelCalChan0 = new System.Windows.Forms.TextBox();
            this.txtHighLevelCalChan1 = new System.Windows.Forms.TextBox();
            this.txtHighLevelCalChan2 = new System.Windows.Forms.TextBox();
            this.txtHighLevelCalChan3 = new System.Windows.Forms.TextBox();
            this.txtHighLevelCalChan4 = new System.Windows.Forms.TextBox();
            this.txtHighLevelCalChan5 = new System.Windows.Forms.TextBox();
            this.txtHighLevelCalChan6 = new System.Windows.Forms.TextBox();
            this.txtHighLevelCalChan7 = new System.Windows.Forms.TextBox();
            this.txtHighLevelCalChan8 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtHighLevelCalChan9 = new System.Windows.Forms.TextBox();
            this.grpCcaOrRssi = new System.Windows.Forms.GroupBox();
            this.chbUseAveRssiForCca = new System.Windows.Forms.CheckBox();
            this.btnReadMaxOrAveRssi9 = new System.Windows.Forms.Button();
            this.btnReadMaxOrAveRssi8 = new System.Windows.Forms.Button();
            this.chbContinuousCcaOrRssi = new System.Windows.Forms.CheckBox();
            this.btnReadMaxOrAveRssi7 = new System.Windows.Forms.Button();
            this.btnReadMaxOrAveRssi6 = new System.Windows.Forms.Button();
            this.btnPerformCcaOrRssi = new System.Windows.Forms.Button();
            this.btnReadMaxOrAveRssi5 = new System.Windows.Forms.Button();
            this.btnReadMaxOrAveRssi4 = new System.Windows.Forms.Button();
            this.btnReadMaxOrAveRssi3 = new System.Windows.Forms.Button();
            this.btnReadMaxOrAveRssi2 = new System.Windows.Forms.Button();
            this.btnReadMaxOrAveRssi1 = new System.Windows.Forms.Button();
            this.btnReadMaxOrAveRssi0 = new System.Windows.Forms.Button();
            this.txtRssiDbm9 = new System.Windows.Forms.TextBox();
            this.txtRssi9 = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtRssiDbm8 = new System.Windows.Forms.TextBox();
            this.txtRssi8 = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.txtRssiDbm7 = new System.Windows.Forms.TextBox();
            this.txtRssi7 = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.txtRssiDbm6 = new System.Windows.Forms.TextBox();
            this.txtRssi6 = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.txtRssi5 = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.txtRssiDbm4 = new System.Windows.Forms.TextBox();
            this.txtRssi4 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txtRssiDbm3 = new System.Windows.Forms.TextBox();
            this.txtRssi3 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtRssiDbm2 = new System.Windows.Forms.TextBox();
            this.txtRssi2 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtRssiDbm1 = new System.Windows.Forms.TextBox();
            this.txtRssi1 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtRssiDbm0 = new System.Windows.Forms.TextBox();
            this.txtRssi0 = new System.Windows.Forms.TextBox();
            this.lblRssiDbm = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblExtOrIntAdc = new System.Windows.Forms.Label();
            this.tabData = new System.Windows.Forms.TabPage();
            this.grpReceiveData = new System.Windows.Forms.GroupBox();
            this.btnStartStream = new System.Windows.Forms.Button();
            this.cmbReceiveDataPollRate = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.grpReceiveDataType = new System.Windows.Forms.GroupBox();
            this.radReceiveDataAscii = new System.Windows.Forms.RadioButton();
            this.radReceiveDataHex = new System.Windows.Forms.RadioButton();
            this.btnReceiveDataClear = new System.Windows.Forms.Button();
            this.txtReceiveData = new System.Windows.Forms.TextBox();
            this.grpTransmitData = new System.Windows.Forms.GroupBox();
            this.btnTransmitData = new System.Windows.Forms.Button();
            this.cmbTransmitDataRate = new System.Windows.Forms.ComboBox();
            this.chbTransmitDataForever = new System.Windows.Forms.CheckBox();
            this.label21 = new System.Windows.Forms.Label();
            this.grpTransmitDataType = new System.Windows.Forms.GroupBox();
            this.radTransmitDataAscii = new System.Windows.Forms.RadioButton();
            this.radTransmitDataHex = new System.Windows.Forms.RadioButton();
            this.txtTransmitData = new System.Windows.Forms.TextBox();
            this.tabTest = new System.Windows.Forms.TabPage();
            this.grpTestRx = new System.Windows.Forms.GroupBox();
            this.btnTestRx400WriteLna = new System.Windows.Forms.Button();
            this.btnTestRx400ReadLna = new System.Windows.Forms.Button();
            this.txtTestRx400Lna = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.btnTestRx400Enab = new System.Windows.Forms.Button();
            this.cmbTestRx400Chan = new System.Windows.Forms.ComboBox();
            this.grpTestTx = new System.Windows.Forms.GroupBox();
            this.btnCcPaWrite = new System.Windows.Forms.Button();
            this.btnCcPaRead = new System.Windows.Forms.Button();
            this.lblCcPa = new System.Windows.Forms.Label();
            this.txtCcPa = new System.Windows.Forms.TextBox();
            this.cmbTestTx245Pow = new System.Windows.Forms.ComboBox();
            this.btnTestTx400WritePow = new System.Windows.Forms.Button();
            this.btnTestTx400ReadPow = new System.Windows.Forms.Button();
            this.txtTestTx400Pow = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.cmbTestTx245Chan = new System.Windows.Forms.ComboBox();
            this.btnTestTx245Enab = new System.Windows.Forms.Button();
            this.label56 = new System.Windows.Forms.Label();
            this.btnTestTx400Enab = new System.Windows.Forms.Button();
            this.cmbTestTx400Chan = new System.Windows.Forms.ComboBox();
            this.tabDataTest = new System.Windows.Forms.TabPage();
            this.grpDataTest = new System.Windows.Forms.GroupBox();
            this.grpDataTestIm = new System.Windows.Forms.GroupBox();
            this.grpDtStatIm = new System.Windows.Forms.GroupBox();
            this.txtDtTotalRxByteErrsIm = new System.Windows.Forms.TextBox();
            this.lblDtTotalRxByteErrsIm = new System.Windows.Forms.Label();
            this.grpDtConfigIm = new System.Windows.Forms.GroupBox();
            this.chbDtIncDataIm = new System.Windows.Forms.CheckBox();
            this.txtDtTxBlockCountIm = new System.Windows.Forms.TextBox();
            this.chbDtRxIm = new System.Windows.Forms.CheckBox();
            this.chbDtValidateRxDataIm = new System.Windows.Forms.CheckBox();
            this.lblDtDataIm = new System.Windows.Forms.Label();
            this.txtDtDataIm = new System.Windows.Forms.TextBox();
            this.chbDtTxIm = new System.Windows.Forms.CheckBox();
            this.lblDtTxBlockCountIm = new System.Windows.Forms.Label();
            this.grpDataTestBsm = new System.Windows.Forms.GroupBox();
            this.grpDtConfigBsm = new System.Windows.Forms.GroupBox();
            this.chbDtIncDataBsm = new System.Windows.Forms.CheckBox();
            this.txtDtTxBlockCountBsm = new System.Windows.Forms.TextBox();
            this.lblDtDataBsm = new System.Windows.Forms.Label();
            this.txtDtDataBsm = new System.Windows.Forms.TextBox();
            this.chbDtTxBsm = new System.Windows.Forms.CheckBox();
            this.lblDtTxBlockCountBsm = new System.Windows.Forms.Label();
            this.chbDtRxBsm = new System.Windows.Forms.CheckBox();
            this.chbDtValidateRxDataBsm = new System.Windows.Forms.CheckBox();
            this.grpDtStatBsm = new System.Windows.Forms.GroupBox();
            this.txtDtTotalRxByteErrsBsm = new System.Windows.Forms.TextBox();
            this.lblDtTotalRxByteErrsBsm = new System.Windows.Forms.Label();
            this.btnStartDataTest = new System.Windows.Forms.Button();
            this.tabRemote = new System.Windows.Forms.TabPage();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.label120 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.button56 = new System.Windows.Forms.Button();
            this.comboBox17 = new System.Windows.Forms.ComboBox();
            this.checkBox51 = new System.Windows.Forms.CheckBox();
            this.textBox61 = new System.Windows.Forms.TextBox();
            this.label116 = new System.Windows.Forms.Label();
            this.comboBox16 = new System.Windows.Forms.ComboBox();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.label132 = new System.Windows.Forms.Label();
            this.comboBox18 = new System.Windows.Forms.ComboBox();
            this.checkBox52 = new System.Windows.Forms.CheckBox();
            this.label117 = new System.Windows.Forms.Label();
            this.button57 = new System.Windows.Forms.Button();
            this.textBox60 = new System.Windows.Forms.TextBox();
            this.label115 = new System.Windows.Forms.Label();
            this.comboBox11 = new System.Windows.Forms.ComboBox();
            this.grpRemoteCals = new System.Windows.Forms.GroupBox();
            this.grpRemHighLevelTuning = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnCalReqRem = new System.Windows.Forms.Button();
            this.lblCalReqRem = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.tabFactoryUse = new System.Windows.Forms.TabPage();
            this.btnShowRssiToDbmCalsFromNvmOnCcaTab = new System.Windows.Forms.Button();
            this.grp245Cal = new System.Windows.Forms.GroupBox();
            this.btn245Cal = new System.Windows.Forms.Button();
            this.lblLnaNegTrimBackoff = new System.Windows.Forms.Label();
            this.txtLnaNegTrimBackoff = new System.Windows.Forms.TextBox();
            this.chbUseIntRssiOnBsm = new System.Windows.Forms.CheckBox();
            this.btnNonvolatileMemory = new System.Windows.Forms.Button();
            this.grpCcReg = new System.Windows.Forms.GroupBox();
            this.txtCcRegAddr = new System.Windows.Forms.TextBox();
            this.btnCcRegWrite = new System.Windows.Forms.Button();
            this.btnCcRegRead = new System.Windows.Forms.Button();
            this.label78 = new System.Windows.Forms.Label();
            this.txtCcRegVal = new System.Windows.Forms.TextBox();
            this.btnStartTraceView = new System.Windows.Forms.Button();
            this.grpBerSetup = new System.Windows.Forms.GroupBox();
            this.lblBerChan = new System.Windows.Forms.Label();
            this.cmbBerChan = new System.Windows.Forms.ComboBox();
            this.btnStartBer = new System.Windows.Forms.Button();
            this.grpBerDataRate = new System.Windows.Forms.GroupBox();
            this.radBer800kbps = new System.Windows.Forms.RadioButton();
            this.radBer400kbps = new System.Windows.Forms.RadioButton();
            this.radBer200kbps = new System.Windows.Forms.RadioButton();
            this.grpStat = new System.Windows.Forms.GroupBox();
            this.grpImStatOnBsm = new System.Windows.Forms.GroupBox();
            this.btnImRegsOnBsm = new System.Windows.Forms.Button();
            this.grpRxImdTidList = new System.Windows.Forms.GroupBox();
            this.dgvRxImdTidList = new System.Windows.Forms.DataGridView();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpWakeupMode = new System.Windows.Forms.GroupBox();
            this.rad245GHzWakeup = new System.Windows.Forms.RadioButton();
            this.rad400MHzWakeup = new System.Windows.Forms.RadioButton();
            this.chbExtStrobeFor245Sniff = new System.Windows.Forms.CheckBox();
            this.grpSysMsg = new System.Windows.Forms.GroupBox();
            this.btnClearSysMsg = new System.Windows.Forms.Button();
            this.txtSysMsg = new System.Windows.Forms.TextBox();
            this.grpStatSession = new System.Windows.Forms.GroupBox();
            this.btnStartListen = new System.Windows.Forms.Button();
            this.chbResetMicsOnWakeup = new System.Windows.Forms.CheckBox();
            this.chbDataGat = new System.Windows.Forms.CheckBox();
            this.chbStatPoll = new System.Windows.Forms.CheckBox();
            this.chbAutoListen = new System.Windows.Forms.CheckBox();
            this.chbAutoCca = new System.Windows.Forms.CheckBox();
            this.chbAnyImSearch = new System.Windows.Forms.CheckBox();
            this.btnStartImSearch = new System.Windows.Forms.Button();
            this.chbEnabHkWrite = new System.Windows.Forms.CheckBox();
            this.btnWakeupIm = new System.Windows.Forms.Button();
            this.btnWakeupBsm = new System.Windows.Forms.Button();
            this.btnStartSession = new System.Windows.Forms.Button();
            this.btnSendEmer = new System.Windows.Forms.Button();
            this.grpStat400Stat = new System.Windows.Forms.GroupBox();
            this.txtStatOpState = new System.Windows.Forms.TextBox();
            this.txtStatCompanyId = new System.Windows.Forms.TextBox();
            this.label139 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.txtStatMaxBlocksPerPack = new System.Windows.Forms.TextBox();
            this.txtStatBytesPerBlock = new System.Windows.Forms.TextBox();
            this.txtStatImdTid = new System.Windows.Forms.TextBox();
            this.txtStatTxMod = new System.Windows.Forms.TextBox();
            this.txtStatChan = new System.Windows.Forms.TextBox();
            this.txtStatRxMod = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.grpBsmStat = new System.Windows.Forms.GroupBox();
            this.btnBsmLinkQual = new System.Windows.Forms.Button();
            this.btnBsmRx400 = new System.Windows.Forms.Button();
            this.btnBsmTx245 = new System.Windows.Forms.Button();
            this.btnBsmTx400 = new System.Windows.Forms.Button();
            this.lblBsmTx245 = new System.Windows.Forms.Label();
            this.lblBsmRx400 = new System.Windows.Forms.Label();
            this.pbrBsmLinkQual = new System.Windows.Forms.ProgressBar();
            this.label19 = new System.Windows.Forms.Label();
            this.btnBsmRegs = new System.Windows.Forms.Button();
            this.lblBsmTx400 = new System.Windows.Forms.Label();
            this.grpImCommandInterface = new System.Windows.Forms.GroupBox();
            this.cmbImConnection = new System.Windows.Forms.ComboBox();
            this.grpImStat = new System.Windows.Forms.GroupBox();
            this.btnImLinkQual = new System.Windows.Forms.Button();
            this.chbVsupTrack = new System.Windows.Forms.CheckBox();
            this.lblVsupSetting = new System.Windows.Forms.Label();
            this.lblVsupMeasured = new System.Windows.Forms.Label();
            this.udSetVsup2 = new System.Windows.Forms.NumericUpDown();
            this.udSetVsup1 = new System.Windows.Forms.NumericUpDown();
            this.btnImRx400 = new System.Windows.Forms.Button();
            this.btnImTx400 = new System.Windows.Forms.Button();
            this.lblImTx400 = new System.Windows.Forms.Label();
            this.lblImVsup1 = new System.Windows.Forms.Label();
            this.txtImVsup1 = new System.Windows.Forms.TextBox();
            this.txtImVsup2 = new System.Windows.Forms.TextBox();
            this.btnImRegs = new System.Windows.Forms.Button();
            this.btnImPowMon = new System.Windows.Forms.Button();
            this.lblImVsup2 = new System.Windows.Forms.Label();
            this.lblImRx400 = new System.Windows.Forms.Label();
            this.tmrStatPoll = new System.Windows.Forms.Timer(this.components);
            this.tmrBackgroundTasks = new System.Windows.Forms.Timer(this.components);
            this.sfdStreamData = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.bsmTabs.SuspendLayout();
            this.tabLinkSetup.SuspendLayout();
            this.grpImInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvImInfo)).BeginInit();
            this.grpLinkSetup.SuspendLayout();
            this.tabCcaOrRssiAndCal.SuspendLayout();
            this.grpCal.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.grpHighLevelTuning.SuspendLayout();
            this.grpCcaOrRssi.SuspendLayout();
            this.tabData.SuspendLayout();
            this.grpReceiveData.SuspendLayout();
            this.grpReceiveDataType.SuspendLayout();
            this.grpTransmitData.SuspendLayout();
            this.grpTransmitDataType.SuspendLayout();
            this.tabTest.SuspendLayout();
            this.grpTestRx.SuspendLayout();
            this.grpTestTx.SuspendLayout();
            this.tabDataTest.SuspendLayout();
            this.grpDataTest.SuspendLayout();
            this.grpDataTestIm.SuspendLayout();
            this.grpDtStatIm.SuspendLayout();
            this.grpDtConfigIm.SuspendLayout();
            this.grpDataTestBsm.SuspendLayout();
            this.grpDtConfigBsm.SuspendLayout();
            this.grpDtStatBsm.SuspendLayout();
            this.tabRemote.SuspendLayout();
            this.groupBox24.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.groupBox25.SuspendLayout();
            this.grpRemoteCals.SuspendLayout();
            this.grpRemHighLevelTuning.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabFactoryUse.SuspendLayout();
            this.grp245Cal.SuspendLayout();
            this.grpCcReg.SuspendLayout();
            this.grpBerSetup.SuspendLayout();
            this.grpBerDataRate.SuspendLayout();
            this.grpStat.SuspendLayout();
            this.grpImStatOnBsm.SuspendLayout();
            this.grpRxImdTidList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRxImdTidList)).BeginInit();
            this.grpWakeupMode.SuspendLayout();
            this.grpSysMsg.SuspendLayout();
            this.grpStatSession.SuspendLayout();
            this.grpStat400Stat.SuspendLayout();
            this.grpBsmStat.SuspendLayout();
            this.grpImCommandInterface.SuspendLayout();
            this.grpImStat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udSetVsup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udSetVsup1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtRssiDbm5
            // 
            this.txtRssiDbm5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRssiDbm5.Location = new System.Drawing.Point(121, 138);
            this.txtRssiDbm5.Name = "txtRssiDbm5";
            this.txtRssiDbm5.Size = new System.Drawing.Size(48, 20);
            this.txtRssiDbm5.TabIndex = 27;
            this.txtRssiDbm5.Text = "****";
            this.txtRssiDbm5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRssiDbm5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.bsmTabs);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.grpStat);
            this.splitContainer1.Size = new System.Drawing.Size(713, 746);
            this.splitContainer1.SplitterDistance = 328;
            this.splitContainer1.TabIndex = 0;
            // 
            // bsmTabs
            // 
            this.bsmTabs.Controls.Add(this.tabLinkSetup);
            this.bsmTabs.Controls.Add(this.tabCcaOrRssiAndCal);
            this.bsmTabs.Controls.Add(this.tabData);
            this.bsmTabs.Controls.Add(this.tabTest);
            this.bsmTabs.Controls.Add(this.tabDataTest);
            this.bsmTabs.Controls.Add(this.tabRemote);
            this.bsmTabs.Controls.Add(this.tabFactoryUse);
            this.bsmTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bsmTabs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bsmTabs.Location = new System.Drawing.Point(0, 0);
            this.bsmTabs.Name = "bsmTabs";
            this.bsmTabs.SelectedIndex = 0;
            this.bsmTabs.Size = new System.Drawing.Size(713, 328);
            this.bsmTabs.TabIndex = 0;
            // 
            // tabLinkSetup
            // 
            this.tabLinkSetup.BackColor = System.Drawing.Color.White;
            this.tabLinkSetup.Controls.Add(this.grpImInfo);
            this.tabLinkSetup.Controls.Add(this.grpLinkSetup);
            this.tabLinkSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabLinkSetup.Location = new System.Drawing.Point(4, 22);
            this.tabLinkSetup.Name = "tabLinkSetup";
            this.tabLinkSetup.Padding = new System.Windows.Forms.Padding(3);
            this.tabLinkSetup.Size = new System.Drawing.Size(705, 302);
            this.tabLinkSetup.TabIndex = 0;
            this.tabLinkSetup.Text = "Link Setup";
            this.tabLinkSetup.UseVisualStyleBackColor = true;
            // 
            // grpImInfo
            // 
            this.grpImInfo.Controls.Add(this.btnLoadImInfo);
            this.grpImInfo.Controls.Add(this.btnStoreImInfo);
            this.grpImInfo.Controls.Add(this.dgvImInfo);
            this.grpImInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpImInfo.Location = new System.Drawing.Point(11, 209);
            this.grpImInfo.Name = "grpImInfo";
            this.grpImInfo.Size = new System.Drawing.Size(688, 96);
            this.grpImInfo.TabIndex = 17;
            this.grpImInfo.TabStop = false;
            this.grpImInfo.Text = "Implant Information";
            // 
            // btnLoadImInfo
            // 
            this.btnLoadImInfo.Location = new System.Drawing.Point(634, 19);
            this.btnLoadImInfo.Name = "btnLoadImInfo";
            this.btnLoadImInfo.Size = new System.Drawing.Size(50, 32);
            this.btnLoadImInfo.TabIndex = 36;
            this.btnLoadImInfo.Text = "Load";
            this.btnLoadImInfo.UseVisualStyleBackColor = true;
            this.btnLoadImInfo.Click += new System.EventHandler(this.btnLoadImInfo_Click);
            // 
            // btnStoreImInfo
            // 
            this.btnStoreImInfo.Location = new System.Drawing.Point(634, 55);
            this.btnStoreImInfo.Name = "btnStoreImInfo";
            this.btnStoreImInfo.Size = new System.Drawing.Size(50, 32);
            this.btnStoreImInfo.TabIndex = 35;
            this.btnStoreImInfo.Text = "Store";
            this.btnStoreImInfo.UseVisualStyleBackColor = true;
            this.btnStoreImInfo.Click += new System.EventHandler(this.btnStoreImInfo_Click);
            // 
            // dgvImInfo
            // 
            this.dgvImInfo.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvImInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvImInfo.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvImInfo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvImInfo.ColumnHeadersHeight = 40;
            this.dgvImInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.companyId,
            this.imdTransId,
            this.companyName,
            this.imdDescription});
            this.dgvImInfo.Location = new System.Drawing.Point(18, 19);
            this.dgvImInfo.Name = "dgvImInfo";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvImInfo.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvImInfo.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvImInfo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvImInfo.Size = new System.Drawing.Size(612, 68);
            this.dgvImInfo.TabIndex = 21;
            // 
            // companyId
            // 
            this.companyId.DataPropertyName = "companyId";
            this.companyId.HeaderText = "Company ID (hex)";
            this.companyId.MinimumWidth = 20;
            this.companyId.Name = "companyId";
            this.companyId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // imdTransId
            // 
            this.imdTransId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.imdTransId.DataPropertyName = "imdTransId";
            this.imdTransId.FillWeight = 85.10638F;
            this.imdTransId.HeaderText = "IMD TID (hex)";
            this.imdTransId.MinimumWidth = 50;
            this.imdTransId.Name = "imdTransId";
            this.imdTransId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // companyName
            // 
            this.companyName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.companyName.DataPropertyName = "companyName";
            this.companyName.FillWeight = 107.4468F;
            this.companyName.HeaderText = "Company Name";
            this.companyName.MinimumWidth = 50;
            this.companyName.Name = "companyName";
            this.companyName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // imdDescription
            // 
            this.imdDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.imdDescription.DataPropertyName = "imdDescription";
            this.imdDescription.DividerWidth = 10;
            this.imdDescription.HeaderText = "Implant Description";
            this.imdDescription.MinimumWidth = 100;
            this.imdDescription.Name = "imdDescription";
            this.imdDescription.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // grpLinkSetup
            // 
            this.grpLinkSetup.Controls.Add(this.cmbImplantType);
            this.grpLinkSetup.Controls.Add(this.cmbWake245UserData);
            this.grpLinkSetup.Controls.Add(this.label146);
            this.grpLinkSetup.Controls.Add(this.label145);
            this.grpLinkSetup.Controls.Add(this.cmbEmerMaxBlocksPerPack);
            this.grpLinkSetup.Controls.Add(this.cmbEmerBytesPerBlock);
            this.grpLinkSetup.Controls.Add(this.txtCompanyId2);
            this.grpLinkSetup.Controls.Add(this.txtCompanyId1);
            this.grpLinkSetup.Controls.Add(this.label140);
            this.grpLinkSetup.Controls.Add(this.txtImdTid6);
            this.grpLinkSetup.Controls.Add(this.label25);
            this.grpLinkSetup.Controls.Add(this.txtImdTid5);
            this.grpLinkSetup.Controls.Add(this.txtImdTid4);
            this.grpLinkSetup.Controls.Add(this.cmbWake245Country);
            this.grpLinkSetup.Controls.Add(this.label23);
            this.grpLinkSetup.Controls.Add(this.label20);
            this.grpLinkSetup.Controls.Add(this.txtImdTid3);
            this.grpLinkSetup.Controls.Add(this.label22);
            this.grpLinkSetup.Controls.Add(this.cmbEmerChan);
            this.grpLinkSetup.Controls.Add(this.txtImdTid2);
            this.grpLinkSetup.Controls.Add(this.cmbEmerRxMod);
            this.grpLinkSetup.Controls.Add(this.label6);
            this.grpLinkSetup.Controls.Add(this.cmbEmerTxMod);
            this.grpLinkSetup.Controls.Add(this.label24);
            this.grpLinkSetup.Controls.Add(this.txtImdTid1);
            this.grpLinkSetup.Controls.Add(this.cmbNormMaxBlocksPerPack);
            this.grpLinkSetup.Controls.Add(this.cmbNormBytesPerBlock);
            this.grpLinkSetup.Controls.Add(this.cmbNormChan);
            this.grpLinkSetup.Controls.Add(this.cmbNormRxMod);
            this.grpLinkSetup.Controls.Add(this.cmbNormTxMod);
            this.grpLinkSetup.Controls.Add(this.label12);
            this.grpLinkSetup.Controls.Add(this.label11);
            this.grpLinkSetup.Controls.Add(this.label10);
            this.grpLinkSetup.Controls.Add(this.label7);
            this.grpLinkSetup.Controls.Add(this.label8);
            this.grpLinkSetup.Controls.Add(this.label4);
            this.grpLinkSetup.Controls.Add(this.label5);
            this.grpLinkSetup.Controls.Add(this.label2);
            this.grpLinkSetup.Controls.Add(this.label3);
            this.grpLinkSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpLinkSetup.Location = new System.Drawing.Point(11, 6);
            this.grpLinkSetup.Name = "grpLinkSetup";
            this.grpLinkSetup.Size = new System.Drawing.Size(688, 197);
            this.grpLinkSetup.TabIndex = 5;
            this.grpLinkSetup.TabStop = false;
            this.grpLinkSetup.Text = "Link Setup";
            // 
            // cmbImplantType
            // 
            this.cmbImplantType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbImplantType.FormattingEnabled = true;
            this.cmbImplantType.Items.AddRange(new object[] {
            "ZL70100",
            "ZL70101",
            "ZL70102",
            "ZL70103"});
            this.cmbImplantType.Location = new System.Drawing.Point(574, 78);
            this.cmbImplantType.Name = "cmbImplantType";
            this.cmbImplantType.Size = new System.Drawing.Size(96, 21);
            this.cmbImplantType.TabIndex = 33;
            this.cmbImplantType.SelectedIndexChanged += new System.EventHandler(this.cmbImplantType_SelectedIndexChanged);
            this.cmbImplantType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // cmbWake245UserData
            // 
            this.cmbWake245UserData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbWake245UserData.FormattingEnabled = true;
            this.cmbWake245UserData.Items.AddRange(new object[] {
            "0 - 000",
            "1 - 001",
            "2 - 010",
            "3 - 011",
            "4 - 100",
            "5 - 101",
            "6 - 110",
            "7 - 111"});
            this.cmbWake245UserData.Location = new System.Drawing.Point(574, 55);
            this.cmbWake245UserData.Name = "cmbWake245UserData";
            this.cmbWake245UserData.Size = new System.Drawing.Size(96, 21);
            this.cmbWake245UserData.TabIndex = 32;
            this.cmbWake245UserData.SelectedIndexChanged += new System.EventHandler(this.cmbWake245UserData_SelectedIndexChanged);
            this.cmbWake245UserData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label146.Location = new System.Drawing.Point(262, 128);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(104, 13);
            this.label146.TabIndex = 28;
            this.label146.Text = "Max. Blocks/Packet";
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label145.Location = new System.Drawing.Point(262, 105);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(65, 13);
            this.label145.TabIndex = 27;
            this.label145.Text = "Bytes/Block";
            // 
            // cmbEmerMaxBlocksPerPack
            // 
            this.cmbEmerMaxBlocksPerPack.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEmerMaxBlocksPerPack.FormattingEnabled = true;
            this.cmbEmerMaxBlocksPerPack.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"});
            this.cmbEmerMaxBlocksPerPack.Location = new System.Drawing.Point(371, 124);
            this.cmbEmerMaxBlocksPerPack.Name = "cmbEmerMaxBlocksPerPack";
            this.cmbEmerMaxBlocksPerPack.Size = new System.Drawing.Size(96, 21);
            this.cmbEmerMaxBlocksPerPack.TabIndex = 26;
            this.cmbEmerMaxBlocksPerPack.SelectedIndexChanged += new System.EventHandler(this.cmbEmerMaxBlocksPerPack_SelectedIndexChanged);
            this.cmbEmerMaxBlocksPerPack.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // cmbEmerBytesPerBlock
            // 
            this.cmbEmerBytesPerBlock.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEmerBytesPerBlock.FormattingEnabled = true;
            this.cmbEmerBytesPerBlock.Items.AddRange(new object[] {
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15 (LSB only)"});
            this.cmbEmerBytesPerBlock.Location = new System.Drawing.Point(371, 101);
            this.cmbEmerBytesPerBlock.Name = "cmbEmerBytesPerBlock";
            this.cmbEmerBytesPerBlock.Size = new System.Drawing.Size(96, 21);
            this.cmbEmerBytesPerBlock.TabIndex = 25;
            this.cmbEmerBytesPerBlock.SelectedIndexChanged += new System.EventHandler(this.cmbEmerBytesPerBlock_SelectedIndexChanged);
            this.cmbEmerBytesPerBlock.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // txtCompanyId2
            // 
            this.txtCompanyId2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCompanyId2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCompanyId2.Location = new System.Drawing.Point(220, 32);
            this.txtCompanyId2.MaxLength = 1;
            this.txtCompanyId2.Name = "txtCompanyId2";
            this.txtCompanyId2.Size = new System.Drawing.Size(16, 20);
            this.txtCompanyId2.TabIndex = 2;
            this.txtCompanyId2.Text = "1";
            this.txtCompanyId2.TextChanged += new System.EventHandler(this.txtCompanyId_TextChanged);
            this.txtCompanyId2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHex_KeyDown);
            this.txtCompanyId2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHex_KeyPress);
            // 
            // txtCompanyId1
            // 
            this.txtCompanyId1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCompanyId1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCompanyId1.Location = new System.Drawing.Point(204, 32);
            this.txtCompanyId1.MaxLength = 1;
            this.txtCompanyId1.Name = "txtCompanyId1";
            this.txtCompanyId1.Size = new System.Drawing.Size(16, 20);
            this.txtCompanyId1.TabIndex = 1;
            this.txtCompanyId1.Text = "0";
            this.txtCompanyId1.TextChanged += new System.EventHandler(this.txtCompanyId_TextChanged);
            this.txtCompanyId1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHex_KeyDown);
            this.txtCompanyId1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHex_KeyPress);
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label140.Location = new System.Drawing.Point(495, 82);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(68, 13);
            this.label140.TabIndex = 24;
            this.label140.Text = "Implant Type";
            // 
            // txtImdTid6
            // 
            this.txtImdTid6.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtImdTid6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImdTid6.Location = new System.Drawing.Point(220, 55);
            this.txtImdTid6.MaxLength = 1;
            this.txtImdTid6.Name = "txtImdTid6";
            this.txtImdTid6.Size = new System.Drawing.Size(16, 20);
            this.txtImdTid6.TabIndex = 8;
            this.txtImdTid6.Text = "1";
            this.txtImdTid6.TextChanged += new System.EventHandler(this.txtImdTid_TextChanged);
            this.txtImdTid6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHex_KeyDown);
            this.txtImdTid6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHex_KeyPress);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(495, 59);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(55, 13);
            this.label25.TabIndex = 17;
            this.label25.Text = "User Data";
            // 
            // txtImdTid5
            // 
            this.txtImdTid5.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtImdTid5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImdTid5.Location = new System.Drawing.Point(204, 55);
            this.txtImdTid5.MaxLength = 1;
            this.txtImdTid5.Name = "txtImdTid5";
            this.txtImdTid5.Size = new System.Drawing.Size(16, 20);
            this.txtImdTid5.TabIndex = 7;
            this.txtImdTid5.Text = "0";
            this.txtImdTid5.TextChanged += new System.EventHandler(this.txtImdTid_TextChanged);
            this.txtImdTid5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHex_KeyDown);
            this.txtImdTid5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHex_KeyPress);
            // 
            // txtImdTid4
            // 
            this.txtImdTid4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtImdTid4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImdTid4.Location = new System.Drawing.Point(188, 55);
            this.txtImdTid4.MaxLength = 1;
            this.txtImdTid4.Name = "txtImdTid4";
            this.txtImdTid4.Size = new System.Drawing.Size(16, 20);
            this.txtImdTid4.TabIndex = 6;
            this.txtImdTid4.Text = "0";
            this.txtImdTid4.TextChanged += new System.EventHandler(this.txtImdTid_TextChanged);
            this.txtImdTid4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHex_KeyDown);
            this.txtImdTid4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHex_KeyPress);
            // 
            // cmbWake245Country
            // 
            this.cmbWake245Country.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbWake245Country.FormattingEnabled = true;
            this.cmbWake245Country.Items.AddRange(new object[] {
            "0 - USA"});
            this.cmbWake245Country.Location = new System.Drawing.Point(574, 32);
            this.cmbWake245Country.Name = "cmbWake245Country";
            this.cmbWake245Country.Size = new System.Drawing.Size(96, 21);
            this.cmbWake245Country.TabIndex = 17;
            this.cmbWake245Country.SelectedIndexChanged += new System.EventHandler(this.cmbWake245Country_SelectedIndexChanged);
            this.cmbWake245Country.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Cursor = System.Windows.Forms.Cursors.Default;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(16, 36);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(91, 13);
            this.label23.TabIndex = 2;
            this.label23.Text = "Company ID (hex)";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(495, 36);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(43, 13);
            this.label20.TabIndex = 3;
            this.label20.Text = "Country";
            // 
            // txtImdTid3
            // 
            this.txtImdTid3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtImdTid3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImdTid3.Location = new System.Drawing.Point(172, 55);
            this.txtImdTid3.MaxLength = 1;
            this.txtImdTid3.Name = "txtImdTid3";
            this.txtImdTid3.Size = new System.Drawing.Size(16, 20);
            this.txtImdTid3.TabIndex = 5;
            this.txtImdTid3.Text = "0";
            this.txtImdTid3.TextChanged += new System.EventHandler(this.txtImdTid_TextChanged);
            this.txtImdTid3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHex_KeyDown);
            this.txtImdTid3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHex_KeyPress);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(487, 16);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(115, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "2.45 GHz Wake-up";
            // 
            // cmbEmerChan
            // 
            this.cmbEmerChan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEmerChan.FormattingEnabled = true;
            this.cmbEmerChan.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.cmbEmerChan.Location = new System.Drawing.Point(371, 78);
            this.cmbEmerChan.Name = "cmbEmerChan";
            this.cmbEmerChan.Size = new System.Drawing.Size(96, 21);
            this.cmbEmerChan.TabIndex = 16;
            this.cmbEmerChan.SelectedIndexChanged += new System.EventHandler(this.cmbEmerChan_SelectedIndexChanged);
            this.cmbEmerChan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // txtImdTid2
            // 
            this.txtImdTid2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtImdTid2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImdTid2.Location = new System.Drawing.Point(156, 55);
            this.txtImdTid2.MaxLength = 1;
            this.txtImdTid2.Name = "txtImdTid2";
            this.txtImdTid2.Size = new System.Drawing.Size(16, 20);
            this.txtImdTid2.TabIndex = 4;
            this.txtImdTid2.Text = "0";
            this.txtImdTid2.TextChanged += new System.EventHandler(this.txtImdTid_TextChanged);
            this.txtImdTid2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHex_KeyDown);
            this.txtImdTid2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHex_KeyPress);
            // 
            // cmbEmerRxMod
            // 
            this.cmbEmerRxMod.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEmerRxMod.FormattingEnabled = true;
            this.cmbEmerRxMod.Location = new System.Drawing.Point(371, 55);
            this.cmbEmerRxMod.Name = "cmbEmerRxMod";
            this.cmbEmerRxMod.Size = new System.Drawing.Size(96, 21);
            this.cmbEmerRxMod.TabIndex = 15;
            this.cmbEmerRxMod.SelectedIndexChanged += new System.EventHandler(this.cmbEmerRxMod_SelectedIndexChanged);
            this.cmbEmerRxMod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(262, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "RX Modulation";
            // 
            // cmbEmerTxMod
            // 
            this.cmbEmerTxMod.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEmerTxMod.FormattingEnabled = true;
            this.cmbEmerTxMod.Location = new System.Drawing.Point(371, 32);
            this.cmbEmerTxMod.Name = "cmbEmerTxMod";
            this.cmbEmerTxMod.Size = new System.Drawing.Size(96, 21);
            this.cmbEmerTxMod.TabIndex = 14;
            this.cmbEmerTxMod.SelectedIndexChanged += new System.EventHandler(this.cmbEmerTxMod_SelectedIndexChanged);
            this.cmbEmerTxMod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(262, 36);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(76, 13);
            this.label24.TabIndex = 17;
            this.label24.Text = "TX Modulation";
            // 
            // txtImdTid1
            // 
            this.txtImdTid1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtImdTid1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImdTid1.Location = new System.Drawing.Point(140, 55);
            this.txtImdTid1.MaxLength = 1;
            this.txtImdTid1.Name = "txtImdTid1";
            this.txtImdTid1.Size = new System.Drawing.Size(16, 20);
            this.txtImdTid1.TabIndex = 3;
            this.txtImdTid1.Text = "0";
            this.txtImdTid1.TextChanged += new System.EventHandler(this.txtImdTid_TextChanged);
            this.txtImdTid1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHex_KeyDown);
            this.txtImdTid1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHex_KeyPress);
            // 
            // cmbNormMaxBlocksPerPack
            // 
            this.cmbNormMaxBlocksPerPack.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbNormMaxBlocksPerPack.FormattingEnabled = true;
            this.cmbNormMaxBlocksPerPack.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"});
            this.cmbNormMaxBlocksPerPack.Location = new System.Drawing.Point(140, 170);
            this.cmbNormMaxBlocksPerPack.Name = "cmbNormMaxBlocksPerPack";
            this.cmbNormMaxBlocksPerPack.Size = new System.Drawing.Size(96, 21);
            this.cmbNormMaxBlocksPerPack.TabIndex = 13;
            this.cmbNormMaxBlocksPerPack.SelectedIndexChanged += new System.EventHandler(this.cmbNormMaxBlocksPerPack_SelectedIndexChanged);
            this.cmbNormMaxBlocksPerPack.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // cmbNormBytesPerBlock
            // 
            this.cmbNormBytesPerBlock.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbNormBytesPerBlock.FormattingEnabled = true;
            this.cmbNormBytesPerBlock.Items.AddRange(new object[] {
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15 (LSB only)"});
            this.cmbNormBytesPerBlock.Location = new System.Drawing.Point(140, 147);
            this.cmbNormBytesPerBlock.Name = "cmbNormBytesPerBlock";
            this.cmbNormBytesPerBlock.Size = new System.Drawing.Size(96, 21);
            this.cmbNormBytesPerBlock.TabIndex = 12;
            this.cmbNormBytesPerBlock.SelectedIndexChanged += new System.EventHandler(this.cmbNormBytesPerBlock_SelectedIndexChanged);
            this.cmbNormBytesPerBlock.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // cmbNormChan
            // 
            this.cmbNormChan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbNormChan.FormattingEnabled = true;
            this.cmbNormChan.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.cmbNormChan.Location = new System.Drawing.Point(140, 124);
            this.cmbNormChan.MaxLength = 8;
            this.cmbNormChan.Name = "cmbNormChan";
            this.cmbNormChan.Size = new System.Drawing.Size(96, 21);
            this.cmbNormChan.TabIndex = 11;
            this.cmbNormChan.SelectedIndexChanged += new System.EventHandler(this.cmbNormChan_SelectedIndexChanged);
            this.cmbNormChan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // cmbNormRxMod
            // 
            this.cmbNormRxMod.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbNormRxMod.FormattingEnabled = true;
            this.cmbNormRxMod.Location = new System.Drawing.Point(140, 101);
            this.cmbNormRxMod.Name = "cmbNormRxMod";
            this.cmbNormRxMod.Size = new System.Drawing.Size(96, 21);
            this.cmbNormRxMod.TabIndex = 10;
            this.cmbNormRxMod.SelectedIndexChanged += new System.EventHandler(this.cmbNormRxMod_SelectedIndexChanged);
            this.cmbNormRxMod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // cmbNormTxMod
            // 
            this.cmbNormTxMod.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbNormTxMod.FormattingEnabled = true;
            this.cmbNormTxMod.Location = new System.Drawing.Point(140, 78);
            this.cmbNormTxMod.Name = "cmbNormTxMod";
            this.cmbNormTxMod.Size = new System.Drawing.Size(96, 21);
            this.cmbNormTxMod.TabIndex = 9;
            this.cmbNormTxMod.SelectedIndexChanged += new System.EventHandler(this.cmbNormTxMod_SelectedIndexChanged);
            this.cmbNormTxMod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(16, 174);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(104, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Max. Blocks/Packet";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(16, 151);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Bytes/Block";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(16, 128);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Channel";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(262, 82);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Channel";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(253, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(190, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "400 MHz - Emergency Operation";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(16, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "TX Modulation";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(16, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "RX Modulation";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(167, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "400 MHz - Normal Operation";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "IMD Transceiver ID (hex)";
            // 
            // tabCcaOrRssiAndCal
            // 
            this.tabCcaOrRssiAndCal.Controls.Add(this.grpCal);
            this.tabCcaOrRssiAndCal.Controls.Add(this.grpCcaOrRssi);
            this.tabCcaOrRssiAndCal.Location = new System.Drawing.Point(4, 22);
            this.tabCcaOrRssiAndCal.Name = "tabCcaOrRssiAndCal";
            this.tabCcaOrRssiAndCal.Padding = new System.Windows.Forms.Padding(3);
            this.tabCcaOrRssiAndCal.Size = new System.Drawing.Size(705, 302);
            this.tabCcaOrRssiAndCal.TabIndex = 1;
            this.tabCcaOrRssiAndCal.Text = "CCA & Cal";
            this.tabCcaOrRssiAndCal.UseVisualStyleBackColor = true;
            // 
            // grpCal
            // 
            this.grpCal.Controls.Add(this.groupBox3);
            this.grpCal.Controls.Add(this.grpHighLevelTuning);
            this.grpCal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpCal.Location = new System.Drawing.Point(360, 6);
            this.grpCal.Name = "grpCal";
            this.grpCal.Size = new System.Drawing.Size(336, 292);
            this.grpCal.TabIndex = 59;
            this.grpCal.TabStop = false;
            this.grpCal.Text = "Calibration";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnCalReq);
            this.groupBox3.Controls.Add(this.lblCalReq);
            this.groupBox3.Controls.Add(this.btn24CrystOsc);
            this.groupBox3.Location = new System.Drawing.Point(9, 204);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(241, 81);
            this.groupBox3.TabIndex = 103;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "24MHz Crystal Osc Tuning";
            // 
            // btnCalReq
            // 
            this.btnCalReq.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalReq.Location = new System.Drawing.Point(188, 50);
            this.btnCalReq.Name = "btnCalReq";
            this.btnCalReq.Size = new System.Drawing.Size(40, 20);
            this.btnCalReq.TabIndex = 0;
            this.btnCalReq.Text = "OK";
            this.btnCalReq.UseVisualStyleBackColor = true;
            this.btnCalReq.Click += new System.EventHandler(this.btnCalReq_Click);
            // 
            // lblCalReq
            // 
            this.lblCalReq.AutoSize = true;
            this.lblCalReq.BackColor = System.Drawing.Color.CornflowerBlue;
            this.lblCalReq.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCalReq.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCalReq.Location = new System.Drawing.Point(12, 52);
            this.lblCalReq.Name = "lblCalReq";
            this.lblCalReq.Size = new System.Drawing.Size(142, 15);
            this.lblCalReq.TabIndex = 1;
            this.lblCalReq.Text = "Turn on 402.15 MHz carrier.";
            // 
            // btn24CrystOsc
            // 
            this.btn24CrystOsc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn24CrystOsc.Location = new System.Drawing.Point(94, 20);
            this.btn24CrystOsc.Name = "btn24CrystOsc";
            this.btn24CrystOsc.Size = new System.Drawing.Size(40, 20);
            this.btn24CrystOsc.TabIndex = 94;
            this.btn24CrystOsc.Text = "Cal";
            this.btn24CrystOsc.UseVisualStyleBackColor = true;
            this.btn24CrystOsc.Click += new System.EventHandler(this.btn24CrystOsc_Click);
            // 
            // grpHighLevelTuning
            // 
            this.grpHighLevelTuning.Controls.Add(this.btnHighLevelCal);
            this.grpHighLevelTuning.Controls.Add(this.chbTxTuneCap);
            this.grpHighLevelTuning.Controls.Add(this.chbMatch2TuneCap);
            this.grpHighLevelTuning.Controls.Add(this.chbMatch1TuneCap);
            this.grpHighLevelTuning.Controls.Add(this.chbHighLevelCalExtAlg);
            this.grpHighLevelTuning.Controls.Add(this.chbHighLevelCalRx);
            this.grpHighLevelTuning.Controls.Add(this.chbHighLevelCalTx);
            this.grpHighLevelTuning.Controls.Add(this.label65);
            this.grpHighLevelTuning.Controls.Add(this.label55);
            this.grpHighLevelTuning.Controls.Add(this.label62);
            this.grpHighLevelTuning.Controls.Add(this.label63);
            this.grpHighLevelTuning.Controls.Add(this.label64);
            this.grpHighLevelTuning.Controls.Add(this.label54);
            this.grpHighLevelTuning.Controls.Add(this.label51);
            this.grpHighLevelTuning.Controls.Add(this.label50);
            this.grpHighLevelTuning.Controls.Add(this.label47);
            this.grpHighLevelTuning.Controls.Add(this.txtHighLevelCalChan0);
            this.grpHighLevelTuning.Controls.Add(this.txtHighLevelCalChan1);
            this.grpHighLevelTuning.Controls.Add(this.txtHighLevelCalChan2);
            this.grpHighLevelTuning.Controls.Add(this.txtHighLevelCalChan3);
            this.grpHighLevelTuning.Controls.Add(this.txtHighLevelCalChan4);
            this.grpHighLevelTuning.Controls.Add(this.txtHighLevelCalChan5);
            this.grpHighLevelTuning.Controls.Add(this.txtHighLevelCalChan6);
            this.grpHighLevelTuning.Controls.Add(this.txtHighLevelCalChan7);
            this.grpHighLevelTuning.Controls.Add(this.txtHighLevelCalChan8);
            this.grpHighLevelTuning.Controls.Add(this.label27);
            this.grpHighLevelTuning.Controls.Add(this.label9);
            this.grpHighLevelTuning.Controls.Add(this.txtHighLevelCalChan9);
            this.grpHighLevelTuning.Location = new System.Drawing.Point(9, 20);
            this.grpHighLevelTuning.Name = "grpHighLevelTuning";
            this.grpHighLevelTuning.Size = new System.Drawing.Size(242, 177);
            this.grpHighLevelTuning.TabIndex = 102;
            this.grpHighLevelTuning.TabStop = false;
            this.grpHighLevelTuning.Text = "400 MHz High Level Tuning Algorithm";
            // 
            // btnHighLevelCal
            // 
            this.btnHighLevelCal.BackColor = System.Drawing.Color.Transparent;
            this.btnHighLevelCal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHighLevelCal.Location = new System.Drawing.Point(162, 120);
            this.btnHighLevelCal.Name = "btnHighLevelCal";
            this.btnHighLevelCal.Size = new System.Drawing.Size(66, 36);
            this.btnHighLevelCal.TabIndex = 74;
            this.btnHighLevelCal.Text = "Start Calibration";
            this.btnHighLevelCal.UseVisualStyleBackColor = true;
            this.btnHighLevelCal.Click += new System.EventHandler(this.btnHighLevelCal_Click);
            // 
            // chbTxTuneCap
            // 
            this.chbTxTuneCap.AutoSize = true;
            this.chbTxTuneCap.Checked = true;
            this.chbTxTuneCap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbTxTuneCap.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbTxTuneCap.Location = new System.Drawing.Point(118, 60);
            this.chbTxTuneCap.Name = "chbTxTuneCap";
            this.chbTxTuneCap.Size = new System.Drawing.Size(90, 17);
            this.chbTxTuneCap.TabIndex = 73;
            this.chbTxTuneCap.Text = "TX Tune Cap";
            this.chbTxTuneCap.UseVisualStyleBackColor = true;
            this.chbTxTuneCap.CheckedChanged += new System.EventHandler(this.chbTxTuneCap_CheckedChanged);
            // 
            // chbMatch2TuneCap
            // 
            this.chbMatch2TuneCap.AutoSize = true;
            this.chbMatch2TuneCap.Checked = true;
            this.chbMatch2TuneCap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbMatch2TuneCap.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbMatch2TuneCap.Location = new System.Drawing.Point(118, 24);
            this.chbMatch2TuneCap.Name = "chbMatch2TuneCap";
            this.chbMatch2TuneCap.Size = new System.Drawing.Size(115, 17);
            this.chbMatch2TuneCap.TabIndex = 72;
            this.chbMatch2TuneCap.Text = "Match 2 Tune Cap";
            this.chbMatch2TuneCap.UseVisualStyleBackColor = true;
            this.chbMatch2TuneCap.CheckedChanged += new System.EventHandler(this.chbMatch2TuneCap_CheckedChanged);
            // 
            // chbMatch1TuneCap
            // 
            this.chbMatch1TuneCap.AutoSize = true;
            this.chbMatch1TuneCap.Checked = true;
            this.chbMatch1TuneCap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbMatch1TuneCap.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbMatch1TuneCap.Location = new System.Drawing.Point(118, 42);
            this.chbMatch1TuneCap.Name = "chbMatch1TuneCap";
            this.chbMatch1TuneCap.Size = new System.Drawing.Size(115, 17);
            this.chbMatch1TuneCap.TabIndex = 71;
            this.chbMatch1TuneCap.Text = "Match 1 Tune Cap";
            this.chbMatch1TuneCap.UseVisualStyleBackColor = true;
            this.chbMatch1TuneCap.CheckedChanged += new System.EventHandler(this.chbMatch1TuneCap_CheckedChanged);
            // 
            // chbHighLevelCalExtAlg
            // 
            this.chbHighLevelCalExtAlg.AutoSize = true;
            this.chbHighLevelCalExtAlg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbHighLevelCalExtAlg.Location = new System.Drawing.Point(12, 84);
            this.chbHighLevelCalExtAlg.Name = "chbHighLevelCalExtAlg";
            this.chbHighLevelCalExtAlg.Size = new System.Drawing.Size(101, 17);
            this.chbHighLevelCalExtAlg.TabIndex = 70;
            this.chbHighLevelCalExtAlg.Text = "Ext. Alg. Enable";
            this.chbHighLevelCalExtAlg.UseVisualStyleBackColor = true;
            this.chbHighLevelCalExtAlg.CheckedChanged += new System.EventHandler(this.chbHighLevelCalExtAlg_CheckedChanged);
            // 
            // chbHighLevelCalRx
            // 
            this.chbHighLevelCalRx.AutoSize = true;
            this.chbHighLevelCalRx.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbHighLevelCalRx.Location = new System.Drawing.Point(12, 66);
            this.chbHighLevelCalRx.Name = "chbHighLevelCalRx";
            this.chbHighLevelCalRx.Size = new System.Drawing.Size(77, 17);
            this.chbHighLevelCalRx.TabIndex = 69;
            this.chbHighLevelCalRx.Text = "RX Enable";
            this.chbHighLevelCalRx.UseVisualStyleBackColor = true;
            // 
            // chbHighLevelCalTx
            // 
            this.chbHighLevelCalTx.AutoSize = true;
            this.chbHighLevelCalTx.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbHighLevelCalTx.Location = new System.Drawing.Point(12, 24);
            this.chbHighLevelCalTx.Name = "chbHighLevelCalTx";
            this.chbHighLevelCalTx.Size = new System.Drawing.Size(106, 17);
            this.chbHighLevelCalTx.TabIndex = 68;
            this.chbHighLevelCalTx.Text = "TX Enable ---------";
            this.chbHighLevelCalTx.UseVisualStyleBackColor = true;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(25, 152);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(13, 13);
            this.label65.TabIndex = 67;
            this.label65.Text = "8";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(39, 152);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(13, 13);
            this.label55.TabIndex = 66;
            this.label55.Text = "7";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(53, 152);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(13, 13);
            this.label62.TabIndex = 65;
            this.label62.Text = "6";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(67, 152);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(13, 13);
            this.label63.TabIndex = 64;
            this.label63.Text = "5";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(81, 152);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(13, 13);
            this.label64.TabIndex = 63;
            this.label64.Text = "4";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(95, 152);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(13, 13);
            this.label54.TabIndex = 62;
            this.label54.Text = "3";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(109, 152);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(13, 13);
            this.label51.TabIndex = 61;
            this.label51.Text = "2";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(123, 152);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(13, 13);
            this.label50.TabIndex = 60;
            this.label50.Text = "1";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(30, 109);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(103, 17);
            this.label47.TabIndex = 21;
            this.label47.Text = "Channel Select";
            // 
            // txtHighLevelCalChan0
            // 
            this.txtHighLevelCalChan0.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHighLevelCalChan0.Location = new System.Drawing.Point(137, 129);
            this.txtHighLevelCalChan0.Name = "txtHighLevelCalChan0";
            this.txtHighLevelCalChan0.Size = new System.Drawing.Size(14, 20);
            this.txtHighLevelCalChan0.TabIndex = 20;
            this.txtHighLevelCalChan0.Text = "0";
            this.txtHighLevelCalChan0.Click += new System.EventHandler(this.txtHighLevelCalChan0_Click);
            // 
            // txtHighLevelCalChan1
            // 
            this.txtHighLevelCalChan1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHighLevelCalChan1.Location = new System.Drawing.Point(123, 129);
            this.txtHighLevelCalChan1.Name = "txtHighLevelCalChan1";
            this.txtHighLevelCalChan1.Size = new System.Drawing.Size(14, 20);
            this.txtHighLevelCalChan1.TabIndex = 19;
            this.txtHighLevelCalChan1.Text = "0";
            this.txtHighLevelCalChan1.Click += new System.EventHandler(this.txtHighLevelCalChan1_Click);
            // 
            // txtHighLevelCalChan2
            // 
            this.txtHighLevelCalChan2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHighLevelCalChan2.Location = new System.Drawing.Point(109, 129);
            this.txtHighLevelCalChan2.Name = "txtHighLevelCalChan2";
            this.txtHighLevelCalChan2.Size = new System.Drawing.Size(14, 20);
            this.txtHighLevelCalChan2.TabIndex = 18;
            this.txtHighLevelCalChan2.Text = "0";
            this.txtHighLevelCalChan2.Click += new System.EventHandler(this.txtHighLevelCalChan2_Click);
            // 
            // txtHighLevelCalChan3
            // 
            this.txtHighLevelCalChan3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHighLevelCalChan3.Location = new System.Drawing.Point(95, 129);
            this.txtHighLevelCalChan3.Name = "txtHighLevelCalChan3";
            this.txtHighLevelCalChan3.Size = new System.Drawing.Size(14, 20);
            this.txtHighLevelCalChan3.TabIndex = 17;
            this.txtHighLevelCalChan3.Text = "0";
            this.txtHighLevelCalChan3.Click += new System.EventHandler(this.txtHighLevelCalChan3_Click);
            // 
            // txtHighLevelCalChan4
            // 
            this.txtHighLevelCalChan4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHighLevelCalChan4.Location = new System.Drawing.Point(81, 129);
            this.txtHighLevelCalChan4.Name = "txtHighLevelCalChan4";
            this.txtHighLevelCalChan4.Size = new System.Drawing.Size(14, 20);
            this.txtHighLevelCalChan4.TabIndex = 16;
            this.txtHighLevelCalChan4.Text = "0";
            this.txtHighLevelCalChan4.Click += new System.EventHandler(this.txtHighLevelCalChan4_Click);
            // 
            // txtHighLevelCalChan5
            // 
            this.txtHighLevelCalChan5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHighLevelCalChan5.Location = new System.Drawing.Point(67, 129);
            this.txtHighLevelCalChan5.Name = "txtHighLevelCalChan5";
            this.txtHighLevelCalChan5.Size = new System.Drawing.Size(14, 20);
            this.txtHighLevelCalChan5.TabIndex = 15;
            this.txtHighLevelCalChan5.Text = "0";
            this.txtHighLevelCalChan5.Click += new System.EventHandler(this.txtHighLevelCalChan5_Click);
            // 
            // txtHighLevelCalChan6
            // 
            this.txtHighLevelCalChan6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHighLevelCalChan6.Location = new System.Drawing.Point(53, 129);
            this.txtHighLevelCalChan6.Name = "txtHighLevelCalChan6";
            this.txtHighLevelCalChan6.Size = new System.Drawing.Size(14, 20);
            this.txtHighLevelCalChan6.TabIndex = 14;
            this.txtHighLevelCalChan6.Text = "0";
            this.txtHighLevelCalChan6.Click += new System.EventHandler(this.txtHighLevelCalChan6_Click);
            // 
            // txtHighLevelCalChan7
            // 
            this.txtHighLevelCalChan7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHighLevelCalChan7.Location = new System.Drawing.Point(39, 129);
            this.txtHighLevelCalChan7.Name = "txtHighLevelCalChan7";
            this.txtHighLevelCalChan7.Size = new System.Drawing.Size(14, 20);
            this.txtHighLevelCalChan7.TabIndex = 13;
            this.txtHighLevelCalChan7.Text = "0";
            this.txtHighLevelCalChan7.Click += new System.EventHandler(this.txtHighLevelCalChan7_Click);
            // 
            // txtHighLevelCalChan8
            // 
            this.txtHighLevelCalChan8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHighLevelCalChan8.Location = new System.Drawing.Point(25, 129);
            this.txtHighLevelCalChan8.Name = "txtHighLevelCalChan8";
            this.txtHighLevelCalChan8.Size = new System.Drawing.Size(14, 20);
            this.txtHighLevelCalChan8.TabIndex = 12;
            this.txtHighLevelCalChan8.Text = "0";
            this.txtHighLevelCalChan8.Click += new System.EventHandler(this.txtHighLevelCalChan8_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(11, 152);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(13, 13);
            this.label27.TabIndex = 11;
            this.label27.Text = "9";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(137, 152);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "0";
            // 
            // txtHighLevelCalChan9
            // 
            this.txtHighLevelCalChan9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHighLevelCalChan9.Location = new System.Drawing.Point(11, 129);
            this.txtHighLevelCalChan9.Name = "txtHighLevelCalChan9";
            this.txtHighLevelCalChan9.Size = new System.Drawing.Size(14, 20);
            this.txtHighLevelCalChan9.TabIndex = 0;
            this.txtHighLevelCalChan9.Text = "0";
            this.txtHighLevelCalChan9.Click += new System.EventHandler(this.txtHighLevelCalChan9_Click);
            // 
            // grpCcaOrRssi
            // 
            this.grpCcaOrRssi.Controls.Add(this.chbUseAveRssiForCca);
            this.grpCcaOrRssi.Controls.Add(this.btnReadMaxOrAveRssi9);
            this.grpCcaOrRssi.Controls.Add(this.btnReadMaxOrAveRssi8);
            this.grpCcaOrRssi.Controls.Add(this.chbContinuousCcaOrRssi);
            this.grpCcaOrRssi.Controls.Add(this.btnReadMaxOrAveRssi7);
            this.grpCcaOrRssi.Controls.Add(this.btnReadMaxOrAveRssi6);
            this.grpCcaOrRssi.Controls.Add(this.btnPerformCcaOrRssi);
            this.grpCcaOrRssi.Controls.Add(this.btnReadMaxOrAveRssi5);
            this.grpCcaOrRssi.Controls.Add(this.btnReadMaxOrAveRssi4);
            this.grpCcaOrRssi.Controls.Add(this.btnReadMaxOrAveRssi3);
            this.grpCcaOrRssi.Controls.Add(this.btnReadMaxOrAveRssi2);
            this.grpCcaOrRssi.Controls.Add(this.btnReadMaxOrAveRssi1);
            this.grpCcaOrRssi.Controls.Add(this.btnReadMaxOrAveRssi0);
            this.grpCcaOrRssi.Controls.Add(this.txtRssiDbm9);
            this.grpCcaOrRssi.Controls.Add(this.txtRssi9);
            this.grpCcaOrRssi.Controls.Add(this.label42);
            this.grpCcaOrRssi.Controls.Add(this.txtRssiDbm8);
            this.grpCcaOrRssi.Controls.Add(this.txtRssi8);
            this.grpCcaOrRssi.Controls.Add(this.label41);
            this.grpCcaOrRssi.Controls.Add(this.txtRssiDbm7);
            this.grpCcaOrRssi.Controls.Add(this.txtRssi7);
            this.grpCcaOrRssi.Controls.Add(this.label40);
            this.grpCcaOrRssi.Controls.Add(this.txtRssiDbm6);
            this.grpCcaOrRssi.Controls.Add(this.txtRssi6);
            this.grpCcaOrRssi.Controls.Add(this.label39);
            this.grpCcaOrRssi.Controls.Add(this.txtRssiDbm5);
            this.grpCcaOrRssi.Controls.Add(this.txtRssi5);
            this.grpCcaOrRssi.Controls.Add(this.label38);
            this.grpCcaOrRssi.Controls.Add(this.txtRssiDbm4);
            this.grpCcaOrRssi.Controls.Add(this.txtRssi4);
            this.grpCcaOrRssi.Controls.Add(this.label37);
            this.grpCcaOrRssi.Controls.Add(this.txtRssiDbm3);
            this.grpCcaOrRssi.Controls.Add(this.txtRssi3);
            this.grpCcaOrRssi.Controls.Add(this.label36);
            this.grpCcaOrRssi.Controls.Add(this.txtRssiDbm2);
            this.grpCcaOrRssi.Controls.Add(this.txtRssi2);
            this.grpCcaOrRssi.Controls.Add(this.label35);
            this.grpCcaOrRssi.Controls.Add(this.txtRssiDbm1);
            this.grpCcaOrRssi.Controls.Add(this.txtRssi1);
            this.grpCcaOrRssi.Controls.Add(this.label34);
            this.grpCcaOrRssi.Controls.Add(this.txtRssiDbm0);
            this.grpCcaOrRssi.Controls.Add(this.txtRssi0);
            this.grpCcaOrRssi.Controls.Add(this.lblRssiDbm);
            this.grpCcaOrRssi.Controls.Add(this.label30);
            this.grpCcaOrRssi.Controls.Add(this.label15);
            this.grpCcaOrRssi.Controls.Add(this.lblExtOrIntAdc);
            this.grpCcaOrRssi.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpCcaOrRssi.Location = new System.Drawing.Point(8, 6);
            this.grpCcaOrRssi.Name = "grpCcaOrRssi";
            this.grpCcaOrRssi.Size = new System.Drawing.Size(336, 292);
            this.grpCcaOrRssi.TabIndex = 58;
            this.grpCcaOrRssi.TabStop = false;
            this.grpCcaOrRssi.Text = "Clear Channel Assessment (CCA)";
            // 
            // chbUseAveRssiForCca
            // 
            this.chbUseAveRssiForCca.AutoSize = true;
            this.chbUseAveRssiForCca.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbUseAveRssiForCca.Location = new System.Drawing.Point(124, 264);
            this.chbUseAveRssiForCca.Name = "chbUseAveRssiForCca";
            this.chbUseAveRssiForCca.Size = new System.Drawing.Size(66, 17);
            this.chbUseAveRssiForCca.TabIndex = 58;
            this.chbUseAveRssiForCca.Text = "Average";
            this.chbUseAveRssiForCca.UseVisualStyleBackColor = true;
            // 
            // btnReadMaxOrAveRssi9
            // 
            this.btnReadMaxOrAveRssi9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReadMaxOrAveRssi9.Location = new System.Drawing.Point(176, 218);
            this.btnReadMaxOrAveRssi9.Name = "btnReadMaxOrAveRssi9";
            this.btnReadMaxOrAveRssi9.Size = new System.Drawing.Size(20, 20);
            this.btnReadMaxOrAveRssi9.TabIndex = 55;
            this.btnReadMaxOrAveRssi9.Text = "R";
            this.btnReadMaxOrAveRssi9.UseVisualStyleBackColor = true;
            this.btnReadMaxOrAveRssi9.Click += new System.EventHandler(this.btnReadMaxOrAveRssi9_Click);
            // 
            // btnReadMaxOrAveRssi8
            // 
            this.btnReadMaxOrAveRssi8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReadMaxOrAveRssi8.Location = new System.Drawing.Point(176, 198);
            this.btnReadMaxOrAveRssi8.Name = "btnReadMaxOrAveRssi8";
            this.btnReadMaxOrAveRssi8.Size = new System.Drawing.Size(20, 20);
            this.btnReadMaxOrAveRssi8.TabIndex = 54;
            this.btnReadMaxOrAveRssi8.Text = "R";
            this.btnReadMaxOrAveRssi8.UseVisualStyleBackColor = true;
            this.btnReadMaxOrAveRssi8.Click += new System.EventHandler(this.btnReadMaxOrAveRssi8_Click);
            // 
            // chbContinuousCcaOrRssi
            // 
            this.chbContinuousCcaOrRssi.AutoSize = true;
            this.chbContinuousCcaOrRssi.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbContinuousCcaOrRssi.Location = new System.Drawing.Point(124, 248);
            this.chbContinuousCcaOrRssi.Name = "chbContinuousCcaOrRssi";
            this.chbContinuousCcaOrRssi.Size = new System.Drawing.Size(79, 17);
            this.chbContinuousCcaOrRssi.TabIndex = 28;
            this.chbContinuousCcaOrRssi.Text = "Continuous";
            this.chbContinuousCcaOrRssi.UseVisualStyleBackColor = true;
            // 
            // btnReadMaxOrAveRssi7
            // 
            this.btnReadMaxOrAveRssi7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReadMaxOrAveRssi7.Location = new System.Drawing.Point(176, 178);
            this.btnReadMaxOrAveRssi7.Name = "btnReadMaxOrAveRssi7";
            this.btnReadMaxOrAveRssi7.Size = new System.Drawing.Size(20, 20);
            this.btnReadMaxOrAveRssi7.TabIndex = 53;
            this.btnReadMaxOrAveRssi7.Text = "R";
            this.btnReadMaxOrAveRssi7.UseVisualStyleBackColor = true;
            this.btnReadMaxOrAveRssi7.Click += new System.EventHandler(this.btnReadMaxOrAveRssi7_Click);
            // 
            // btnReadMaxOrAveRssi6
            // 
            this.btnReadMaxOrAveRssi6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReadMaxOrAveRssi6.Location = new System.Drawing.Point(176, 158);
            this.btnReadMaxOrAveRssi6.Name = "btnReadMaxOrAveRssi6";
            this.btnReadMaxOrAveRssi6.Size = new System.Drawing.Size(20, 20);
            this.btnReadMaxOrAveRssi6.TabIndex = 52;
            this.btnReadMaxOrAveRssi6.Text = "R";
            this.btnReadMaxOrAveRssi6.UseVisualStyleBackColor = true;
            this.btnReadMaxOrAveRssi6.Click += new System.EventHandler(this.btnReadMaxOrAveRssi6_Click);
            // 
            // btnPerformCcaOrRssi
            // 
            this.btnPerformCcaOrRssi.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPerformCcaOrRssi.Location = new System.Drawing.Point(28, 246);
            this.btnPerformCcaOrRssi.Name = "btnPerformCcaOrRssi";
            this.btnPerformCcaOrRssi.Size = new System.Drawing.Size(88, 36);
            this.btnPerformCcaOrRssi.TabIndex = 44;
            this.btnPerformCcaOrRssi.Text = "Perform CCA";
            this.btnPerformCcaOrRssi.UseVisualStyleBackColor = true;
            this.btnPerformCcaOrRssi.Click += new System.EventHandler(this.btnPerformCcaOrRssi_Click);
            // 
            // btnReadMaxOrAveRssi5
            // 
            this.btnReadMaxOrAveRssi5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReadMaxOrAveRssi5.Location = new System.Drawing.Point(176, 138);
            this.btnReadMaxOrAveRssi5.Name = "btnReadMaxOrAveRssi5";
            this.btnReadMaxOrAveRssi5.Size = new System.Drawing.Size(20, 20);
            this.btnReadMaxOrAveRssi5.TabIndex = 51;
            this.btnReadMaxOrAveRssi5.Text = "R";
            this.btnReadMaxOrAveRssi5.UseVisualStyleBackColor = true;
            this.btnReadMaxOrAveRssi5.Click += new System.EventHandler(this.btnReadMaxOrAveRssi5_Click);
            // 
            // btnReadMaxOrAveRssi4
            // 
            this.btnReadMaxOrAveRssi4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReadMaxOrAveRssi4.Location = new System.Drawing.Point(176, 118);
            this.btnReadMaxOrAveRssi4.Name = "btnReadMaxOrAveRssi4";
            this.btnReadMaxOrAveRssi4.Size = new System.Drawing.Size(20, 20);
            this.btnReadMaxOrAveRssi4.TabIndex = 50;
            this.btnReadMaxOrAveRssi4.Text = "R";
            this.btnReadMaxOrAveRssi4.UseVisualStyleBackColor = true;
            this.btnReadMaxOrAveRssi4.Click += new System.EventHandler(this.btnReadMaxOrAveRssi4_Click);
            // 
            // btnReadMaxOrAveRssi3
            // 
            this.btnReadMaxOrAveRssi3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReadMaxOrAveRssi3.Location = new System.Drawing.Point(176, 98);
            this.btnReadMaxOrAveRssi3.Name = "btnReadMaxOrAveRssi3";
            this.btnReadMaxOrAveRssi3.Size = new System.Drawing.Size(20, 20);
            this.btnReadMaxOrAveRssi3.TabIndex = 49;
            this.btnReadMaxOrAveRssi3.Text = "R";
            this.btnReadMaxOrAveRssi3.UseVisualStyleBackColor = true;
            this.btnReadMaxOrAveRssi3.Click += new System.EventHandler(this.btnReadMaxOrAveRssi3_Click);
            // 
            // btnReadMaxOrAveRssi2
            // 
            this.btnReadMaxOrAveRssi2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReadMaxOrAveRssi2.Location = new System.Drawing.Point(176, 78);
            this.btnReadMaxOrAveRssi2.Name = "btnReadMaxOrAveRssi2";
            this.btnReadMaxOrAveRssi2.Size = new System.Drawing.Size(20, 20);
            this.btnReadMaxOrAveRssi2.TabIndex = 48;
            this.btnReadMaxOrAveRssi2.Text = "R";
            this.btnReadMaxOrAveRssi2.UseVisualStyleBackColor = true;
            this.btnReadMaxOrAveRssi2.Click += new System.EventHandler(this.btnReadMaxOrAveRssi2_Click);
            // 
            // btnReadMaxOrAveRssi1
            // 
            this.btnReadMaxOrAveRssi1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReadMaxOrAveRssi1.Location = new System.Drawing.Point(176, 58);
            this.btnReadMaxOrAveRssi1.Name = "btnReadMaxOrAveRssi1";
            this.btnReadMaxOrAveRssi1.Size = new System.Drawing.Size(20, 20);
            this.btnReadMaxOrAveRssi1.TabIndex = 47;
            this.btnReadMaxOrAveRssi1.Text = "R";
            this.btnReadMaxOrAveRssi1.UseVisualStyleBackColor = true;
            this.btnReadMaxOrAveRssi1.Click += new System.EventHandler(this.btnReadMaxOrAveRssi1_Click);
            // 
            // btnReadMaxOrAveRssi0
            // 
            this.btnReadMaxOrAveRssi0.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReadMaxOrAveRssi0.Location = new System.Drawing.Point(176, 38);
            this.btnReadMaxOrAveRssi0.Name = "btnReadMaxOrAveRssi0";
            this.btnReadMaxOrAveRssi0.Size = new System.Drawing.Size(20, 20);
            this.btnReadMaxOrAveRssi0.TabIndex = 45;
            this.btnReadMaxOrAveRssi0.Text = "R";
            this.btnReadMaxOrAveRssi0.UseVisualStyleBackColor = true;
            this.btnReadMaxOrAveRssi0.Click += new System.EventHandler(this.btnReadMaxOrAveRssi0_Click);
            // 
            // txtRssiDbm9
            // 
            this.txtRssiDbm9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRssiDbm9.Location = new System.Drawing.Point(121, 218);
            this.txtRssiDbm9.Name = "txtRssiDbm9";
            this.txtRssiDbm9.Size = new System.Drawing.Size(48, 20);
            this.txtRssiDbm9.TabIndex = 43;
            this.txtRssiDbm9.Text = "****";
            this.txtRssiDbm9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRssiDbm9.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // txtRssi9
            // 
            this.txtRssi9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRssi9.Location = new System.Drawing.Point(65, 218);
            this.txtRssi9.Name = "txtRssi9";
            this.txtRssi9.Size = new System.Drawing.Size(48, 20);
            this.txtRssi9.TabIndex = 41;
            this.txtRssi9.Text = "****";
            this.txtRssi9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtRssi9.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(25, 222);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(13, 13);
            this.label42.TabIndex = 40;
            this.label42.Text = "9";
            // 
            // txtRssiDbm8
            // 
            this.txtRssiDbm8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRssiDbm8.Location = new System.Drawing.Point(121, 198);
            this.txtRssiDbm8.Name = "txtRssiDbm8";
            this.txtRssiDbm8.Size = new System.Drawing.Size(48, 20);
            this.txtRssiDbm8.TabIndex = 39;
            this.txtRssiDbm8.Text = "****";
            this.txtRssiDbm8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRssiDbm8.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // txtRssi8
            // 
            this.txtRssi8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRssi8.Location = new System.Drawing.Point(65, 198);
            this.txtRssi8.Name = "txtRssi8";
            this.txtRssi8.Size = new System.Drawing.Size(48, 20);
            this.txtRssi8.TabIndex = 37;
            this.txtRssi8.Text = "****";
            this.txtRssi8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtRssi8.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(25, 202);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(13, 13);
            this.label41.TabIndex = 36;
            this.label41.Text = "8";
            // 
            // txtRssiDbm7
            // 
            this.txtRssiDbm7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRssiDbm7.Location = new System.Drawing.Point(121, 178);
            this.txtRssiDbm7.Name = "txtRssiDbm7";
            this.txtRssiDbm7.Size = new System.Drawing.Size(48, 20);
            this.txtRssiDbm7.TabIndex = 35;
            this.txtRssiDbm7.Text = "****";
            this.txtRssiDbm7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRssiDbm7.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // txtRssi7
            // 
            this.txtRssi7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRssi7.Location = new System.Drawing.Point(65, 178);
            this.txtRssi7.Name = "txtRssi7";
            this.txtRssi7.Size = new System.Drawing.Size(48, 20);
            this.txtRssi7.TabIndex = 33;
            this.txtRssi7.Text = "****";
            this.txtRssi7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtRssi7.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(25, 182);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(13, 13);
            this.label40.TabIndex = 32;
            this.label40.Text = "7";
            // 
            // txtRssiDbm6
            // 
            this.txtRssiDbm6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRssiDbm6.Location = new System.Drawing.Point(121, 158);
            this.txtRssiDbm6.Name = "txtRssiDbm6";
            this.txtRssiDbm6.Size = new System.Drawing.Size(48, 20);
            this.txtRssiDbm6.TabIndex = 31;
            this.txtRssiDbm6.Text = "****";
            this.txtRssiDbm6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRssiDbm6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // txtRssi6
            // 
            this.txtRssi6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRssi6.Location = new System.Drawing.Point(65, 158);
            this.txtRssi6.Name = "txtRssi6";
            this.txtRssi6.Size = new System.Drawing.Size(48, 20);
            this.txtRssi6.TabIndex = 29;
            this.txtRssi6.Text = "****";
            this.txtRssi6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtRssi6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(25, 162);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(13, 13);
            this.label39.TabIndex = 28;
            this.label39.Text = "6";
            // 
            // txtRssi5
            // 
            this.txtRssi5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRssi5.Location = new System.Drawing.Point(65, 138);
            this.txtRssi5.Name = "txtRssi5";
            this.txtRssi5.Size = new System.Drawing.Size(48, 20);
            this.txtRssi5.TabIndex = 25;
            this.txtRssi5.Text = "****";
            this.txtRssi5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtRssi5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(25, 142);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(13, 13);
            this.label38.TabIndex = 24;
            this.label38.Text = "5";
            // 
            // txtRssiDbm4
            // 
            this.txtRssiDbm4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRssiDbm4.Location = new System.Drawing.Point(121, 118);
            this.txtRssiDbm4.Name = "txtRssiDbm4";
            this.txtRssiDbm4.Size = new System.Drawing.Size(48, 20);
            this.txtRssiDbm4.TabIndex = 23;
            this.txtRssiDbm4.Text = "****";
            this.txtRssiDbm4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRssiDbm4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // txtRssi4
            // 
            this.txtRssi4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRssi4.Location = new System.Drawing.Point(65, 118);
            this.txtRssi4.Name = "txtRssi4";
            this.txtRssi4.Size = new System.Drawing.Size(48, 20);
            this.txtRssi4.TabIndex = 21;
            this.txtRssi4.Text = "****";
            this.txtRssi4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtRssi4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(25, 122);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(13, 13);
            this.label37.TabIndex = 20;
            this.label37.Text = "4";
            // 
            // txtRssiDbm3
            // 
            this.txtRssiDbm3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRssiDbm3.Location = new System.Drawing.Point(121, 98);
            this.txtRssiDbm3.Name = "txtRssiDbm3";
            this.txtRssiDbm3.Size = new System.Drawing.Size(48, 20);
            this.txtRssiDbm3.TabIndex = 19;
            this.txtRssiDbm3.Text = "****";
            this.txtRssiDbm3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRssiDbm3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // txtRssi3
            // 
            this.txtRssi3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRssi3.Location = new System.Drawing.Point(65, 98);
            this.txtRssi3.Name = "txtRssi3";
            this.txtRssi3.Size = new System.Drawing.Size(48, 20);
            this.txtRssi3.TabIndex = 17;
            this.txtRssi3.Text = "****";
            this.txtRssi3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtRssi3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(25, 102);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(13, 13);
            this.label36.TabIndex = 16;
            this.label36.Text = "3";
            // 
            // txtRssiDbm2
            // 
            this.txtRssiDbm2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRssiDbm2.Location = new System.Drawing.Point(121, 78);
            this.txtRssiDbm2.Name = "txtRssiDbm2";
            this.txtRssiDbm2.Size = new System.Drawing.Size(48, 20);
            this.txtRssiDbm2.TabIndex = 15;
            this.txtRssiDbm2.Text = "****";
            this.txtRssiDbm2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRssiDbm2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // txtRssi2
            // 
            this.txtRssi2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRssi2.Location = new System.Drawing.Point(65, 78);
            this.txtRssi2.Name = "txtRssi2";
            this.txtRssi2.Size = new System.Drawing.Size(48, 20);
            this.txtRssi2.TabIndex = 13;
            this.txtRssi2.Text = "****";
            this.txtRssi2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtRssi2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(25, 82);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(13, 13);
            this.label35.TabIndex = 12;
            this.label35.Text = "2";
            // 
            // txtRssiDbm1
            // 
            this.txtRssiDbm1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRssiDbm1.Location = new System.Drawing.Point(121, 58);
            this.txtRssiDbm1.Name = "txtRssiDbm1";
            this.txtRssiDbm1.Size = new System.Drawing.Size(48, 20);
            this.txtRssiDbm1.TabIndex = 11;
            this.txtRssiDbm1.Text = "****";
            this.txtRssiDbm1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRssiDbm1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // txtRssi1
            // 
            this.txtRssi1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRssi1.Location = new System.Drawing.Point(65, 58);
            this.txtRssi1.Name = "txtRssi1";
            this.txtRssi1.Size = new System.Drawing.Size(48, 20);
            this.txtRssi1.TabIndex = 9;
            this.txtRssi1.Text = "****";
            this.txtRssi1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtRssi1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(25, 62);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(13, 13);
            this.label34.TabIndex = 8;
            this.label34.Text = "1";
            // 
            // txtRssiDbm0
            // 
            this.txtRssiDbm0.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRssiDbm0.Location = new System.Drawing.Point(121, 38);
            this.txtRssiDbm0.Name = "txtRssiDbm0";
            this.txtRssiDbm0.Size = new System.Drawing.Size(48, 20);
            this.txtRssiDbm0.TabIndex = 7;
            this.txtRssiDbm0.Text = "****";
            this.txtRssiDbm0.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtRssiDbm0.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // txtRssi0
            // 
            this.txtRssi0.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRssi0.Location = new System.Drawing.Point(65, 38);
            this.txtRssi0.Name = "txtRssi0";
            this.txtRssi0.Size = new System.Drawing.Size(48, 20);
            this.txtRssi0.TabIndex = 5;
            this.txtRssi0.Text = "****";
            this.txtRssi0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtRssi0.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // lblRssiDbm
            // 
            this.lblRssiDbm.AutoSize = true;
            this.lblRssiDbm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRssiDbm.Location = new System.Drawing.Point(130, 20);
            this.lblRssiDbm.Name = "lblRssiDbm";
            this.lblRssiDbm.Size = new System.Drawing.Size(31, 13);
            this.lblRssiDbm.TabIndex = 4;
            this.lblRssiDbm.Text = "dBm";
            this.lblRssiDbm.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(6, 20);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(53, 13);
            this.label30.TabIndex = 1;
            this.label30.Text = "Channel";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(25, 42);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(13, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "0";
            // 
            // lblExtOrIntAdc
            // 
            this.lblExtOrIntAdc.AutoSize = true;
            this.lblExtOrIntAdc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExtOrIntAdc.Location = new System.Drawing.Point(63, 20);
            this.lblExtOrIntAdc.Name = "lblExtOrIntAdc";
            this.lblExtOrIntAdc.Size = new System.Drawing.Size(54, 13);
            this.lblExtOrIntAdc.TabIndex = 2;
            this.lblExtOrIntAdc.Text = "Ext ADC";
            this.lblExtOrIntAdc.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tabData
            // 
            this.tabData.Controls.Add(this.grpReceiveData);
            this.tabData.Controls.Add(this.grpTransmitData);
            this.tabData.Location = new System.Drawing.Point(4, 22);
            this.tabData.Name = "tabData";
            this.tabData.Size = new System.Drawing.Size(705, 302);
            this.tabData.TabIndex = 2;
            this.tabData.Text = "Data";
            this.tabData.UseVisualStyleBackColor = true;
            // 
            // grpReceiveData
            // 
            this.grpReceiveData.Controls.Add(this.btnStartStream);
            this.grpReceiveData.Controls.Add(this.cmbReceiveDataPollRate);
            this.grpReceiveData.Controls.Add(this.label29);
            this.grpReceiveData.Controls.Add(this.grpReceiveDataType);
            this.grpReceiveData.Controls.Add(this.btnReceiveDataClear);
            this.grpReceiveData.Controls.Add(this.txtReceiveData);
            this.grpReceiveData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpReceiveData.Location = new System.Drawing.Point(371, 9);
            this.grpReceiveData.Name = "grpReceiveData";
            this.grpReceiveData.Size = new System.Drawing.Size(320, 292);
            this.grpReceiveData.TabIndex = 6;
            this.grpReceiveData.TabStop = false;
            this.grpReceiveData.Text = "Receive Data (Uplink)";
            // 
            // btnStartStream
            // 
            this.btnStartStream.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartStream.Location = new System.Drawing.Point(110, 208);
            this.btnStartStream.Name = "btnStartStream";
            this.btnStartStream.Size = new System.Drawing.Size(72, 36);
            this.btnStartStream.TabIndex = 67;
            this.btnStartStream.Text = "Start Stream";
            this.btnStartStream.UseVisualStyleBackColor = true;
            this.btnStartStream.Click += new System.EventHandler(this.btnStartStream_Click);
            // 
            // cmbReceiveDataPollRate
            // 
            this.cmbReceiveDataPollRate.FormattingEnabled = true;
            this.cmbReceiveDataPollRate.Items.AddRange(new object[] {
            "0.5",
            "1",
            "2",
            "5",
            "10"});
            this.cmbReceiveDataPollRate.Location = new System.Drawing.Point(114, 267);
            this.cmbReceiveDataPollRate.Name = "cmbReceiveDataPollRate";
            this.cmbReceiveDataPollRate.Size = new System.Drawing.Size(54, 21);
            this.cmbReceiveDataPollRate.TabIndex = 66;
            this.cmbReceiveDataPollRate.SelectedIndexChanged += new System.EventHandler(this.cmbReceiveDataPollRate_SelectedIndexChanged);
            this.cmbReceiveDataPollRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(30, 271);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(78, 13);
            this.label29.TabIndex = 65;
            this.label29.Text = "Data Rate (Hz)";
            // 
            // grpReceiveDataType
            // 
            this.grpReceiveDataType.Controls.Add(this.radReceiveDataAscii);
            this.grpReceiveDataType.Controls.Add(this.radReceiveDataHex);
            this.grpReceiveDataType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpReceiveDataType.Location = new System.Drawing.Point(192, 205);
            this.grpReceiveDataType.Name = "grpReceiveDataType";
            this.grpReceiveDataType.Size = new System.Drawing.Size(102, 62);
            this.grpReceiveDataType.TabIndex = 5;
            this.grpReceiveDataType.TabStop = false;
            this.grpReceiveDataType.Text = "Data Type";
            // 
            // radReceiveDataAscii
            // 
            this.radReceiveDataAscii.AutoSize = true;
            this.radReceiveDataAscii.Location = new System.Drawing.Point(12, 35);
            this.radReceiveDataAscii.Name = "radReceiveDataAscii";
            this.radReceiveDataAscii.Size = new System.Drawing.Size(52, 17);
            this.radReceiveDataAscii.TabIndex = 4;
            this.radReceiveDataAscii.Text = "ASCII";
            this.radReceiveDataAscii.UseVisualStyleBackColor = true;
            this.radReceiveDataAscii.CheckedChanged += new System.EventHandler(this.radReceiveDataAscii_CheckedChanged);
            // 
            // radReceiveDataHex
            // 
            this.radReceiveDataHex.AutoSize = true;
            this.radReceiveDataHex.Checked = true;
            this.radReceiveDataHex.Location = new System.Drawing.Point(12, 15);
            this.radReceiveDataHex.Name = "radReceiveDataHex";
            this.radReceiveDataHex.Size = new System.Drawing.Size(86, 17);
            this.radReceiveDataHex.TabIndex = 3;
            this.radReceiveDataHex.TabStop = true;
            this.radReceiveDataHex.Text = "Hexadecimal";
            this.radReceiveDataHex.UseVisualStyleBackColor = true;
            this.radReceiveDataHex.CheckedChanged += new System.EventHandler(this.radReceiveDataHex_CheckedChanged);
            // 
            // btnReceiveDataClear
            // 
            this.btnReceiveDataClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReceiveDataClear.Location = new System.Drawing.Point(25, 208);
            this.btnReceiveDataClear.Name = "btnReceiveDataClear";
            this.btnReceiveDataClear.Size = new System.Drawing.Size(72, 36);
            this.btnReceiveDataClear.TabIndex = 2;
            this.btnReceiveDataClear.Text = "Clear Display";
            this.btnReceiveDataClear.UseVisualStyleBackColor = true;
            this.btnReceiveDataClear.Click += new System.EventHandler(this.btnReceiveDataClear_Click);
            // 
            // txtReceiveData
            // 
            this.txtReceiveData.AcceptsReturn = true;
            this.txtReceiveData.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReceiveData.Location = new System.Drawing.Point(16, 20);
            this.txtReceiveData.Multiline = true;
            this.txtReceiveData.Name = "txtReceiveData";
            this.txtReceiveData.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtReceiveData.Size = new System.Drawing.Size(288, 176);
            this.txtReceiveData.TabIndex = 1;
            this.txtReceiveData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // grpTransmitData
            // 
            this.grpTransmitData.Controls.Add(this.btnTransmitData);
            this.grpTransmitData.Controls.Add(this.cmbTransmitDataRate);
            this.grpTransmitData.Controls.Add(this.chbTransmitDataForever);
            this.grpTransmitData.Controls.Add(this.label21);
            this.grpTransmitData.Controls.Add(this.grpTransmitDataType);
            this.grpTransmitData.Controls.Add(this.txtTransmitData);
            this.grpTransmitData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpTransmitData.Location = new System.Drawing.Point(17, 9);
            this.grpTransmitData.Name = "grpTransmitData";
            this.grpTransmitData.Size = new System.Drawing.Size(320, 292);
            this.grpTransmitData.TabIndex = 3;
            this.grpTransmitData.TabStop = false;
            this.grpTransmitData.Text = "Transmit Data (Downlink)";
            // 
            // btnTransmitData
            // 
            this.btnTransmitData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTransmitData.Location = new System.Drawing.Point(22, 206);
            this.btnTransmitData.Name = "btnTransmitData";
            this.btnTransmitData.Size = new System.Drawing.Size(72, 36);
            this.btnTransmitData.TabIndex = 2;
            this.btnTransmitData.Text = "Transmit \r\nData";
            this.btnTransmitData.UseVisualStyleBackColor = true;
            this.btnTransmitData.Click += new System.EventHandler(this.btnTransmitData_Click);
            // 
            // cmbTransmitDataRate
            // 
            this.cmbTransmitDataRate.FormattingEnabled = true;
            this.cmbTransmitDataRate.Items.AddRange(new object[] {
            "0.5",
            "1",
            "2",
            "5",
            "10"});
            this.cmbTransmitDataRate.Location = new System.Drawing.Point(109, 267);
            this.cmbTransmitDataRate.Name = "cmbTransmitDataRate";
            this.cmbTransmitDataRate.Size = new System.Drawing.Size(54, 21);
            this.cmbTransmitDataRate.TabIndex = 64;
            this.cmbTransmitDataRate.SelectedIndexChanged += new System.EventHandler(this.cmbTransmitDataRate_SelectedIndexChanged);
            this.cmbTransmitDataRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // chbTransmitDataForever
            // 
            this.chbTransmitDataForever.AutoSize = true;
            this.chbTransmitDataForever.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbTransmitDataForever.Location = new System.Drawing.Point(34, 250);
            this.chbTransmitDataForever.Name = "chbTransmitDataForever";
            this.chbTransmitDataForever.Size = new System.Drawing.Size(105, 17);
            this.chbTransmitDataForever.TabIndex = 46;
            this.chbTransmitDataForever.Text = "Transmit Forever";
            this.chbTransmitDataForever.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(30, 271);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(78, 13);
            this.label21.TabIndex = 63;
            this.label21.Text = "Data Rate (Hz)";
            // 
            // grpTransmitDataType
            // 
            this.grpTransmitDataType.Controls.Add(this.radTransmitDataAscii);
            this.grpTransmitDataType.Controls.Add(this.radTransmitDataHex);
            this.grpTransmitDataType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpTransmitDataType.Location = new System.Drawing.Point(194, 201);
            this.grpTransmitDataType.Name = "grpTransmitDataType";
            this.grpTransmitDataType.Size = new System.Drawing.Size(102, 62);
            this.grpTransmitDataType.TabIndex = 5;
            this.grpTransmitDataType.TabStop = false;
            this.grpTransmitDataType.Text = "Data Type";
            // 
            // radTransmitDataAscii
            // 
            this.radTransmitDataAscii.AutoSize = true;
            this.radTransmitDataAscii.Location = new System.Drawing.Point(12, 35);
            this.radTransmitDataAscii.Name = "radTransmitDataAscii";
            this.radTransmitDataAscii.Size = new System.Drawing.Size(52, 17);
            this.radTransmitDataAscii.TabIndex = 4;
            this.radTransmitDataAscii.Text = "ASCII";
            this.radTransmitDataAscii.UseVisualStyleBackColor = true;
            this.radTransmitDataAscii.CheckedChanged += new System.EventHandler(this.radTransmitDataAscii_CheckedChanged);
            // 
            // radTransmitDataHex
            // 
            this.radTransmitDataHex.AutoSize = true;
            this.radTransmitDataHex.Checked = true;
            this.radTransmitDataHex.Location = new System.Drawing.Point(12, 15);
            this.radTransmitDataHex.Name = "radTransmitDataHex";
            this.radTransmitDataHex.Size = new System.Drawing.Size(86, 17);
            this.radTransmitDataHex.TabIndex = 3;
            this.radTransmitDataHex.TabStop = true;
            this.radTransmitDataHex.Text = "Hexadecimal";
            this.radTransmitDataHex.UseVisualStyleBackColor = true;
            this.radTransmitDataHex.CheckedChanged += new System.EventHandler(this.radTransmitDataHex_CheckedChanged);
            // 
            // txtTransmitData
            // 
            this.txtTransmitData.AcceptsReturn = true;
            this.txtTransmitData.AllowDrop = true;
            this.txtTransmitData.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTransmitData.Location = new System.Drawing.Point(16, 20);
            this.txtTransmitData.MaxLength = 465;
            this.txtTransmitData.Multiline = true;
            this.txtTransmitData.Name = "txtTransmitData";
            this.txtTransmitData.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtTransmitData.Size = new System.Drawing.Size(288, 176);
            this.txtTransmitData.TabIndex = 1;
            this.txtTransmitData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTransmitData_KeyDown);
            this.txtTransmitData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTransmitData_KeyPress);
            // 
            // tabTest
            // 
            this.tabTest.Controls.Add(this.grpTestRx);
            this.tabTest.Controls.Add(this.grpTestTx);
            this.tabTest.Location = new System.Drawing.Point(4, 22);
            this.tabTest.Name = "tabTest";
            this.tabTest.Size = new System.Drawing.Size(705, 302);
            this.tabTest.TabIndex = 0;
            this.tabTest.Text = " Test";
            this.tabTest.UseVisualStyleBackColor = true;
            // 
            // grpTestRx
            // 
            this.grpTestRx.Controls.Add(this.btnTestRx400WriteLna);
            this.grpTestRx.Controls.Add(this.btnTestRx400ReadLna);
            this.grpTestRx.Controls.Add(this.txtTestRx400Lna);
            this.grpTestRx.Controls.Add(this.label53);
            this.grpTestRx.Controls.Add(this.label89);
            this.grpTestRx.Controls.Add(this.label79);
            this.grpTestRx.Controls.Add(this.label59);
            this.grpTestRx.Controls.Add(this.btnTestRx400Enab);
            this.grpTestRx.Controls.Add(this.cmbTestRx400Chan);
            this.grpTestRx.Location = new System.Drawing.Point(14, 105);
            this.grpTestRx.Name = "grpTestRx";
            this.grpTestRx.Size = new System.Drawing.Size(491, 76);
            this.grpTestRx.TabIndex = 102;
            this.grpTestRx.TabStop = false;
            this.grpTestRx.Text = "RX Carrier Test (RSSI)";
            // 
            // btnTestRx400WriteLna
            // 
            this.btnTestRx400WriteLna.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTestRx400WriteLna.Location = new System.Drawing.Point(314, 41);
            this.btnTestRx400WriteLna.Name = "btnTestRx400WriteLna";
            this.btnTestRx400WriteLna.Size = new System.Drawing.Size(20, 20);
            this.btnTestRx400WriteLna.TabIndex = 103;
            this.btnTestRx400WriteLna.Text = "W";
            this.btnTestRx400WriteLna.UseVisualStyleBackColor = true;
            this.btnTestRx400WriteLna.Click += new System.EventHandler(this.btnTestRx400WriteLna_Click);
            // 
            // btnTestRx400ReadLna
            // 
            this.btnTestRx400ReadLna.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTestRx400ReadLna.Location = new System.Drawing.Point(294, 41);
            this.btnTestRx400ReadLna.Name = "btnTestRx400ReadLna";
            this.btnTestRx400ReadLna.Size = new System.Drawing.Size(20, 20);
            this.btnTestRx400ReadLna.TabIndex = 102;
            this.btnTestRx400ReadLna.Text = "R";
            this.btnTestRx400ReadLna.UseVisualStyleBackColor = true;
            this.btnTestRx400ReadLna.Click += new System.EventHandler(this.btnTestRx400ReadLna_Click);
            // 
            // txtTestRx400Lna
            // 
            this.txtTestRx400Lna.Location = new System.Drawing.Point(243, 41);
            this.txtTestRx400Lna.Name = "txtTestRx400Lna";
            this.txtTestRx400Lna.Size = new System.Drawing.Size(44, 20);
            this.txtTestRx400Lna.TabIndex = 96;
            this.txtTestRx400Lna.Text = "**";
            this.txtTestRx400Lna.TextChanged += new System.EventHandler(this.txtTestRx400Lna_TextChanged);
            this.txtTestRx400Lna.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHex_KeyDown);
            this.txtTestRx400Lna.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHex_KeyPress);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(248, 11);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(40, 26);
            this.label53.TabIndex = 95;
            this.label53.Text = "LNA\r\nSetting\r\n";
            this.label53.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.Location = new System.Drawing.Point(173, 24);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(46, 13);
            this.label89.TabIndex = 95;
            this.label89.Text = "RX Only";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(77, 24);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(77, 13);
            this.label79.TabIndex = 101;
            this.label79.Text = "Freq (MHz)/Ch";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(12, 44);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(50, 13);
            this.label59.TabIndex = 98;
            this.label59.Text = "400 MHz";
            // 
            // btnTestRx400Enab
            // 
            this.btnTestRx400Enab.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTestRx400Enab.Location = new System.Drawing.Point(167, 41);
            this.btnTestRx400Enab.Name = "btnTestRx400Enab";
            this.btnTestRx400Enab.Size = new System.Drawing.Size(58, 22);
            this.btnTestRx400Enab.TabIndex = 97;
            this.btnTestRx400Enab.Text = "RX EN";
            this.btnTestRx400Enab.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnTestRx400Enab.UseVisualStyleBackColor = true;
            this.btnTestRx400Enab.Click += new System.EventHandler(this.btnTestRx400Enab_Click);
            // 
            // cmbTestRx400Chan
            // 
            this.cmbTestRx400Chan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTestRx400Chan.FormattingEnabled = true;
            this.cmbTestRx400Chan.Items.AddRange(new object[] {
            "402.15 / 0",
            "402.45 / 1",
            "402.75 / 2",
            "403.05 / 3",
            "403.35 / 4",
            "403.65 / 5",
            "403.95 / 6",
            "404.25 / 7",
            "404.55 / 8",
            "404.85 / 9"});
            this.cmbTestRx400Chan.Location = new System.Drawing.Point(68, 40);
            this.cmbTestRx400Chan.Name = "cmbTestRx400Chan";
            this.cmbTestRx400Chan.Size = new System.Drawing.Size(94, 21);
            this.cmbTestRx400Chan.TabIndex = 96;
            this.cmbTestRx400Chan.SelectedIndexChanged += new System.EventHandler(this.cmbTestRx400Chan_SelectedIndexChanged);
            this.cmbTestRx400Chan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // grpTestTx
            // 
            this.grpTestTx.Controls.Add(this.btnCcPaWrite);
            this.grpTestTx.Controls.Add(this.btnCcPaRead);
            this.grpTestTx.Controls.Add(this.lblCcPa);
            this.grpTestTx.Controls.Add(this.txtCcPa);
            this.grpTestTx.Controls.Add(this.cmbTestTx245Pow);
            this.grpTestTx.Controls.Add(this.btnTestTx400WritePow);
            this.grpTestTx.Controls.Add(this.btnTestTx400ReadPow);
            this.grpTestTx.Controls.Add(this.txtTestTx400Pow);
            this.grpTestTx.Controls.Add(this.label61);
            this.grpTestTx.Controls.Add(this.label60);
            this.grpTestTx.Controls.Add(this.label58);
            this.grpTestTx.Controls.Add(this.label57);
            this.grpTestTx.Controls.Add(this.cmbTestTx245Chan);
            this.grpTestTx.Controls.Add(this.btnTestTx245Enab);
            this.grpTestTx.Controls.Add(this.label56);
            this.grpTestTx.Controls.Add(this.btnTestTx400Enab);
            this.grpTestTx.Controls.Add(this.cmbTestTx400Chan);
            this.grpTestTx.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpTestTx.Location = new System.Drawing.Point(14, 3);
            this.grpTestTx.Name = "grpTestTx";
            this.grpTestTx.Size = new System.Drawing.Size(491, 96);
            this.grpTestTx.TabIndex = 88;
            this.grpTestTx.TabStop = false;
            this.grpTestTx.Text = "TX Carrier Test ";
            // 
            // btnCcPaWrite
            // 
            this.btnCcPaWrite.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCcPaWrite.Location = new System.Drawing.Point(446, 67);
            this.btnCcPaWrite.Name = "btnCcPaWrite";
            this.btnCcPaWrite.Size = new System.Drawing.Size(20, 20);
            this.btnCcPaWrite.TabIndex = 102;
            this.btnCcPaWrite.Text = "W";
            this.btnCcPaWrite.UseVisualStyleBackColor = true;
            this.btnCcPaWrite.Click += new System.EventHandler(this.btnCcPaWrite_Click);
            // 
            // btnCcPaRead
            // 
            this.btnCcPaRead.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCcPaRead.Location = new System.Drawing.Point(424, 67);
            this.btnCcPaRead.Name = "btnCcPaRead";
            this.btnCcPaRead.Size = new System.Drawing.Size(20, 20);
            this.btnCcPaRead.TabIndex = 101;
            this.btnCcPaRead.Text = "R";
            this.btnCcPaRead.UseVisualStyleBackColor = true;
            this.btnCcPaRead.Click += new System.EventHandler(this.btnCcPaRead_Click);
            // 
            // lblCcPa
            // 
            this.lblCcPa.AutoSize = true;
            this.lblCcPa.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCcPa.Location = new System.Drawing.Point(370, 49);
            this.lblCcPa.Name = "lblCcPa";
            this.lblCcPa.Size = new System.Drawing.Size(106, 13);
            this.lblCcPa.TabIndex = 100;
            this.lblCcPa.Text = "CC25XX PA Register";
            // 
            // txtCcPa
            // 
            this.txtCcPa.Location = new System.Drawing.Point(373, 67);
            this.txtCcPa.Name = "txtCcPa";
            this.txtCcPa.Size = new System.Drawing.Size(45, 20);
            this.txtCcPa.TabIndex = 99;
            this.txtCcPa.Text = "**";
            this.txtCcPa.TextChanged += new System.EventHandler(this.txtCcPa_TextChanged);
            this.txtCcPa.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHex_KeyDown);
            this.txtCcPa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHex_KeyPress);
            // 
            // cmbTestTx245Pow
            // 
            this.cmbTestTx245Pow.FormattingEnabled = true;
            this.cmbTestTx245Pow.Location = new System.Drawing.Point(243, 65);
            this.cmbTestTx245Pow.Name = "cmbTestTx245Pow";
            this.cmbTestTx245Pow.Size = new System.Drawing.Size(44, 21);
            this.cmbTestTx245Pow.TabIndex = 98;
            this.cmbTestTx245Pow.SelectedIndexChanged += new System.EventHandler(this.cmbTestTx245Pow_SelectedIndexChanged);
            this.cmbTestTx245Pow.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // btnTestTx400WritePow
            // 
            this.btnTestTx400WritePow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTestTx400WritePow.Location = new System.Drawing.Point(314, 44);
            this.btnTestTx400WritePow.Name = "btnTestTx400WritePow";
            this.btnTestTx400WritePow.Size = new System.Drawing.Size(20, 20);
            this.btnTestTx400WritePow.TabIndex = 96;
            this.btnTestTx400WritePow.Text = "W";
            this.btnTestTx400WritePow.UseVisualStyleBackColor = true;
            this.btnTestTx400WritePow.Click += new System.EventHandler(this.btnTestTx400WritePow_Click);
            // 
            // btnTestTx400ReadPow
            // 
            this.btnTestTx400ReadPow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTestTx400ReadPow.Location = new System.Drawing.Point(294, 44);
            this.btnTestTx400ReadPow.Name = "btnTestTx400ReadPow";
            this.btnTestTx400ReadPow.Size = new System.Drawing.Size(20, 20);
            this.btnTestTx400ReadPow.TabIndex = 95;
            this.btnTestTx400ReadPow.Text = "R";
            this.btnTestTx400ReadPow.UseVisualStyleBackColor = true;
            this.btnTestTx400ReadPow.Click += new System.EventHandler(this.btnTestTx400ReadPow_Click);
            // 
            // txtTestTx400Pow
            // 
            this.txtTestTx400Pow.Location = new System.Drawing.Point(243, 42);
            this.txtTestTx400Pow.Name = "txtTestTx400Pow";
            this.txtTestTx400Pow.Size = new System.Drawing.Size(44, 20);
            this.txtTestTx400Pow.TabIndex = 80;
            this.txtTestTx400Pow.Text = "**";
            this.txtTestTx400Pow.TextChanged += new System.EventHandler(this.txtTestTx400Pow_TextChanged);
            this.txtTestTx400Pow.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHex_KeyDown);
            this.txtTestTx400Pow.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHex_KeyPress);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(246, 13);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(42, 26);
            this.label61.TabIndex = 57;
            this.label61.Text = "TX Pwr\r\nSetting\r\n";
            this.label61.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(6, 68);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(56, 13);
            this.label60.TabIndex = 90;
            this.label60.Text = "2450 MHz";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(12, 45);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(50, 13);
            this.label58.TabIndex = 57;
            this.label58.Text = "400 MHz";
            // 
            // label57
            // 
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label57.Location = new System.Drawing.Point(174, 11);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(45, 28);
            this.label57.TabIndex = 57;
            this.label57.Text = "Carrier\r\nWave\r\n";
            this.label57.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmbTestTx245Chan
            // 
            this.cmbTestTx245Chan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTestTx245Chan.FormattingEnabled = true;
            this.cmbTestTx245Chan.Items.AddRange(new object[] {
            "2405.000 / 0",
            "2410.000 / 1",
            "2415.000 / 2",
            "2420.000 / 3",
            "2425.000 / 4",
            "2430.000 / 5",
            "2435.000 / 6",
            "2440.000 / 7",
            "2445.000 / 8",
            "2450.000 / 9",
            "2455.000 / 10",
            "2460.000 / 11",
            "2465.000 / 12",
            "2470.000 / 13",
            "2475.000 / 14",
            "2480.000 / 15",
            "2485.000 / 16",
            "2490.000 / 17",
            "2495.000 / 18"});
            this.cmbTestTx245Chan.Location = new System.Drawing.Point(68, 64);
            this.cmbTestTx245Chan.Name = "cmbTestTx245Chan";
            this.cmbTestTx245Chan.Size = new System.Drawing.Size(94, 21);
            this.cmbTestTx245Chan.TabIndex = 88;
            this.cmbTestTx245Chan.SelectedIndexChanged += new System.EventHandler(this.cmbTestTx245Chan_SelectedIndexChanged);
            this.cmbTestTx245Chan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // btnTestTx245Enab
            // 
            this.btnTestTx245Enab.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTestTx245Enab.Location = new System.Drawing.Point(167, 65);
            this.btnTestTx245Enab.Name = "btnTestTx245Enab";
            this.btnTestTx245Enab.Size = new System.Drawing.Size(58, 22);
            this.btnTestTx245Enab.TabIndex = 85;
            this.btnTestTx245Enab.Text = "TX EN ";
            this.btnTestTx245Enab.UseVisualStyleBackColor = true;
            this.btnTestTx245Enab.Click += new System.EventHandler(this.btnTestTx245Enab_Click);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(77, 24);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(77, 13);
            this.label56.TabIndex = 80;
            this.label56.Text = "Freq (MHz)/Ch";
            // 
            // btnTestTx400Enab
            // 
            this.btnTestTx400Enab.BackColor = System.Drawing.Color.Transparent;
            this.btnTestTx400Enab.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTestTx400Enab.Location = new System.Drawing.Point(167, 41);
            this.btnTestTx400Enab.Name = "btnTestTx400Enab";
            this.btnTestTx400Enab.Size = new System.Drawing.Size(58, 21);
            this.btnTestTx400Enab.TabIndex = 80;
            this.btnTestTx400Enab.Text = "TX EN";
            this.btnTestTx400Enab.UseVisualStyleBackColor = false;
            this.btnTestTx400Enab.Click += new System.EventHandler(this.btnTestTx400Enab_Click);
            // 
            // cmbTestTx400Chan
            // 
            this.cmbTestTx400Chan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTestTx400Chan.FormattingEnabled = true;
            this.cmbTestTx400Chan.Items.AddRange(new object[] {
            "402.15 / 0",
            "402.45 / 1",
            "402.75 / 2",
            "403.05 / 3",
            "403.35 / 4",
            "403.65 / 5",
            "403.95 / 6",
            "404.25 / 7",
            "404.55 / 8",
            "404.85 / 9"});
            this.cmbTestTx400Chan.Location = new System.Drawing.Point(68, 40);
            this.cmbTestTx400Chan.Name = "cmbTestTx400Chan";
            this.cmbTestTx400Chan.Size = new System.Drawing.Size(94, 21);
            this.cmbTestTx400Chan.TabIndex = 58;
            this.cmbTestTx400Chan.SelectedIndexChanged += new System.EventHandler(this.cmbTestTx400Chan_SelectedIndexChanged);
            this.cmbTestTx400Chan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // tabDataTest
            // 
            this.tabDataTest.Controls.Add(this.grpDataTest);
            this.tabDataTest.Location = new System.Drawing.Point(4, 22);
            this.tabDataTest.Name = "tabDataTest";
            this.tabDataTest.Size = new System.Drawing.Size(705, 302);
            this.tabDataTest.TabIndex = 6;
            this.tabDataTest.Text = "Data Test";
            this.tabDataTest.UseVisualStyleBackColor = true;
            // 
            // grpDataTest
            // 
            this.grpDataTest.Controls.Add(this.grpDataTestIm);
            this.grpDataTest.Controls.Add(this.grpDataTestBsm);
            this.grpDataTest.Controls.Add(this.btnStartDataTest);
            this.grpDataTest.Location = new System.Drawing.Point(5, 3);
            this.grpDataTest.Name = "grpDataTest";
            this.grpDataTest.Size = new System.Drawing.Size(697, 296);
            this.grpDataTest.TabIndex = 104;
            this.grpDataTest.TabStop = false;
            this.grpDataTest.Text = "Data Transfer Test";
            // 
            // grpDataTestIm
            // 
            this.grpDataTestIm.Controls.Add(this.grpDtStatIm);
            this.grpDataTestIm.Controls.Add(this.grpDtConfigIm);
            this.grpDataTestIm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpDataTestIm.Location = new System.Drawing.Point(322, 16);
            this.grpDataTestIm.Name = "grpDataTestIm";
            this.grpDataTestIm.Size = new System.Drawing.Size(280, 225);
            this.grpDataTestIm.TabIndex = 106;
            this.grpDataTestIm.TabStop = false;
            this.grpDataTestIm.Text = "Implant";
            // 
            // grpDtStatIm
            // 
            this.grpDtStatIm.Controls.Add(this.txtDtTotalRxByteErrsIm);
            this.grpDtStatIm.Controls.Add(this.lblDtTotalRxByteErrsIm);
            this.grpDtStatIm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpDtStatIm.Location = new System.Drawing.Point(11, 151);
            this.grpDtStatIm.Name = "grpDtStatIm";
            this.grpDtStatIm.Size = new System.Drawing.Size(257, 63);
            this.grpDtStatIm.TabIndex = 108;
            this.grpDtStatIm.TabStop = false;
            this.grpDtStatIm.Text = "Status";
            // 
            // txtDtTotalRxByteErrsIm
            // 
            this.txtDtTotalRxByteErrsIm.Enabled = false;
            this.txtDtTotalRxByteErrsIm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDtTotalRxByteErrsIm.Location = new System.Drawing.Point(125, 23);
            this.txtDtTotalRxByteErrsIm.Name = "txtDtTotalRxByteErrsIm";
            this.txtDtTotalRxByteErrsIm.Size = new System.Drawing.Size(53, 20);
            this.txtDtTotalRxByteErrsIm.TabIndex = 9;
            this.txtDtTotalRxByteErrsIm.Text = "0";
            this.txtDtTotalRxByteErrsIm.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblDtTotalRxByteErrsIm
            // 
            this.lblDtTotalRxByteErrsIm.AutoSize = true;
            this.lblDtTotalRxByteErrsIm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDtTotalRxByteErrsIm.Location = new System.Drawing.Point(10, 27);
            this.lblDtTotalRxByteErrsIm.Name = "lblDtTotalRxByteErrsIm";
            this.lblDtTotalRxByteErrsIm.Size = new System.Drawing.Size(103, 13);
            this.lblDtTotalRxByteErrsIm.TabIndex = 7;
            this.lblDtTotalRxByteErrsIm.Text = "Total RX Byte Errors";
            // 
            // grpDtConfigIm
            // 
            this.grpDtConfigIm.Controls.Add(this.chbDtIncDataIm);
            this.grpDtConfigIm.Controls.Add(this.txtDtTxBlockCountIm);
            this.grpDtConfigIm.Controls.Add(this.chbDtRxIm);
            this.grpDtConfigIm.Controls.Add(this.chbDtValidateRxDataIm);
            this.grpDtConfigIm.Controls.Add(this.lblDtDataIm);
            this.grpDtConfigIm.Controls.Add(this.txtDtDataIm);
            this.grpDtConfigIm.Controls.Add(this.chbDtTxIm);
            this.grpDtConfigIm.Controls.Add(this.lblDtTxBlockCountIm);
            this.grpDtConfigIm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpDtConfigIm.Location = new System.Drawing.Point(11, 19);
            this.grpDtConfigIm.Name = "grpDtConfigIm";
            this.grpDtConfigIm.Size = new System.Drawing.Size(257, 126);
            this.grpDtConfigIm.TabIndex = 108;
            this.grpDtConfigIm.TabStop = false;
            this.grpDtConfigIm.Text = "Config";
            // 
            // chbDtIncDataIm
            // 
            this.chbDtIncDataIm.AutoSize = true;
            this.chbDtIncDataIm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbDtIncDataIm.Location = new System.Drawing.Point(68, 97);
            this.chbDtIncDataIm.Name = "chbDtIncDataIm";
            this.chbDtIncDataIm.Size = new System.Drawing.Size(73, 17);
            this.chbDtIncDataIm.TabIndex = 11;
            this.chbDtIncDataIm.Text = "Increment";
            this.chbDtIncDataIm.UseVisualStyleBackColor = true;
            this.chbDtIncDataIm.CheckedChanged += new System.EventHandler(this.chbDtIncDataIm_CheckedChanged);
            // 
            // txtDtTxBlockCountIm
            // 
            this.txtDtTxBlockCountIm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDtTxBlockCountIm.Location = new System.Drawing.Point(114, 20);
            this.txtDtTxBlockCountIm.Name = "txtDtTxBlockCountIm";
            this.txtDtTxBlockCountIm.Size = new System.Drawing.Size(51, 20);
            this.txtDtTxBlockCountIm.TabIndex = 105;
            this.txtDtTxBlockCountIm.Text = "0";
            this.txtDtTxBlockCountIm.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // chbDtRxIm
            // 
            this.chbDtRxIm.AutoSize = true;
            this.chbDtRxIm.Checked = true;
            this.chbDtRxIm.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbDtRxIm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbDtRxIm.Location = new System.Drawing.Point(7, 45);
            this.chbDtRxIm.Name = "chbDtRxIm";
            this.chbDtRxIm.Size = new System.Drawing.Size(78, 17);
            this.chbDtRxIm.TabIndex = 2;
            this.chbDtRxIm.Text = "Implant RX";
            this.chbDtRxIm.UseVisualStyleBackColor = true;
            this.chbDtRxIm.CheckedChanged += new System.EventHandler(this.chbDtRxIm_CheckedChanged);
            // 
            // chbDtValidateRxDataIm
            // 
            this.chbDtValidateRxDataIm.AutoSize = true;
            this.chbDtValidateRxDataIm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbDtValidateRxDataIm.Location = new System.Drawing.Point(115, 45);
            this.chbDtValidateRxDataIm.Name = "chbDtValidateRxDataIm";
            this.chbDtValidateRxDataIm.Size = new System.Drawing.Size(108, 17);
            this.chbDtValidateRxDataIm.TabIndex = 3;
            this.chbDtValidateRxDataIm.Text = "Validate RX Data";
            this.chbDtValidateRxDataIm.UseVisualStyleBackColor = true;
            this.chbDtValidateRxDataIm.CheckedChanged += new System.EventHandler(this.chbDtValidateRxDataIm_CheckedChanged);
            // 
            // lblDtDataIm
            // 
            this.lblDtDataIm.AutoSize = true;
            this.lblDtDataIm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDtDataIm.Location = new System.Drawing.Point(7, 80);
            this.lblDtDataIm.Name = "lblDtDataIm";
            this.lblDtDataIm.Size = new System.Drawing.Size(134, 13);
            this.lblDtDataIm.TabIndex = 104;
            this.lblDtDataIm.Text = "Data (Implant -> Base)";
            // 
            // txtDtDataIm
            // 
            this.txtDtDataIm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDtDataIm.Location = new System.Drawing.Point(9, 96);
            this.txtDtDataIm.Name = "txtDtDataIm";
            this.txtDtDataIm.Size = new System.Drawing.Size(53, 20);
            this.txtDtDataIm.TabIndex = 103;
            this.txtDtDataIm.Text = "0x00";
            this.txtDtDataIm.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtDtDataIm.TextChanged += new System.EventHandler(this.txtDtDataIm_TextChanged);
            // 
            // chbDtTxIm
            // 
            this.chbDtTxIm.AutoSize = true;
            this.chbDtTxIm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbDtTxIm.Location = new System.Drawing.Point(7, 20);
            this.chbDtTxIm.Name = "chbDtTxIm";
            this.chbDtTxIm.Size = new System.Drawing.Size(77, 17);
            this.chbDtTxIm.TabIndex = 0;
            this.chbDtTxIm.Text = "Implant TX";
            this.chbDtTxIm.UseVisualStyleBackColor = true;
            this.chbDtTxIm.CheckedChanged += new System.EventHandler(this.chbDtTxIm_CheckedChanged);
            // 
            // lblDtTxBlockCountIm
            // 
            this.lblDtTxBlockCountIm.AutoSize = true;
            this.lblDtTxBlockCountIm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDtTxBlockCountIm.Location = new System.Drawing.Point(171, 24);
            this.lblDtTxBlockCountIm.Name = "lblDtTxBlockCountIm";
            this.lblDtTxBlockCountIm.Size = new System.Drawing.Size(82, 13);
            this.lblDtTxBlockCountIm.TabIndex = 10;
            this.lblDtTxBlockCountIm.Text = "TX Block Count";
            // 
            // grpDataTestBsm
            // 
            this.grpDataTestBsm.Controls.Add(this.grpDtConfigBsm);
            this.grpDataTestBsm.Controls.Add(this.grpDtStatBsm);
            this.grpDataTestBsm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpDataTestBsm.Location = new System.Drawing.Point(14, 16);
            this.grpDataTestBsm.Name = "grpDataTestBsm";
            this.grpDataTestBsm.Size = new System.Drawing.Size(280, 225);
            this.grpDataTestBsm.TabIndex = 96;
            this.grpDataTestBsm.TabStop = false;
            this.grpDataTestBsm.Text = "Base Station ";
            // 
            // grpDtConfigBsm
            // 
            this.grpDtConfigBsm.Controls.Add(this.chbDtIncDataBsm);
            this.grpDtConfigBsm.Controls.Add(this.txtDtTxBlockCountBsm);
            this.grpDtConfigBsm.Controls.Add(this.lblDtDataBsm);
            this.grpDtConfigBsm.Controls.Add(this.txtDtDataBsm);
            this.grpDtConfigBsm.Controls.Add(this.chbDtTxBsm);
            this.grpDtConfigBsm.Controls.Add(this.lblDtTxBlockCountBsm);
            this.grpDtConfigBsm.Controls.Add(this.chbDtRxBsm);
            this.grpDtConfigBsm.Controls.Add(this.chbDtValidateRxDataBsm);
            this.grpDtConfigBsm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpDtConfigBsm.Location = new System.Drawing.Point(11, 19);
            this.grpDtConfigBsm.Name = "grpDtConfigBsm";
            this.grpDtConfigBsm.Size = new System.Drawing.Size(257, 126);
            this.grpDtConfigBsm.TabIndex = 107;
            this.grpDtConfigBsm.TabStop = false;
            this.grpDtConfigBsm.Text = "Config";
            // 
            // chbDtIncDataBsm
            // 
            this.chbDtIncDataBsm.AutoSize = true;
            this.chbDtIncDataBsm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbDtIncDataBsm.Location = new System.Drawing.Point(68, 97);
            this.chbDtIncDataBsm.Name = "chbDtIncDataBsm";
            this.chbDtIncDataBsm.Size = new System.Drawing.Size(73, 17);
            this.chbDtIncDataBsm.TabIndex = 11;
            this.chbDtIncDataBsm.Text = "Increment";
            this.chbDtIncDataBsm.UseVisualStyleBackColor = true;
            this.chbDtIncDataBsm.CheckedChanged += new System.EventHandler(this.chbDtIncDataBsm_CheckedChanged);
            // 
            // txtDtTxBlockCountBsm
            // 
            this.txtDtTxBlockCountBsm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDtTxBlockCountBsm.Location = new System.Drawing.Point(114, 20);
            this.txtDtTxBlockCountBsm.Name = "txtDtTxBlockCountBsm";
            this.txtDtTxBlockCountBsm.Size = new System.Drawing.Size(51, 20);
            this.txtDtTxBlockCountBsm.TabIndex = 105;
            this.txtDtTxBlockCountBsm.Text = "0";
            this.txtDtTxBlockCountBsm.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblDtDataBsm
            // 
            this.lblDtDataBsm.AutoSize = true;
            this.lblDtDataBsm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDtDataBsm.Location = new System.Drawing.Point(7, 80);
            this.lblDtDataBsm.Name = "lblDtDataBsm";
            this.lblDtDataBsm.Size = new System.Drawing.Size(134, 13);
            this.lblDtDataBsm.TabIndex = 104;
            this.lblDtDataBsm.Text = "Data (Base -> Implant)";
            // 
            // txtDtDataBsm
            // 
            this.txtDtDataBsm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDtDataBsm.Location = new System.Drawing.Point(9, 96);
            this.txtDtDataBsm.Name = "txtDtDataBsm";
            this.txtDtDataBsm.Size = new System.Drawing.Size(53, 20);
            this.txtDtDataBsm.TabIndex = 103;
            this.txtDtDataBsm.Text = "0x00";
            this.txtDtDataBsm.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtDtDataBsm.TextChanged += new System.EventHandler(this.txtDtDataBsm_TextChanged);
            // 
            // chbDtTxBsm
            // 
            this.chbDtTxBsm.AutoSize = true;
            this.chbDtTxBsm.Checked = true;
            this.chbDtTxBsm.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbDtTxBsm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbDtTxBsm.Location = new System.Drawing.Point(7, 20);
            this.chbDtTxBsm.Name = "chbDtTxBsm";
            this.chbDtTxBsm.Size = new System.Drawing.Size(103, 17);
            this.chbDtTxBsm.TabIndex = 0;
            this.chbDtTxBsm.Text = "Base Station TX";
            this.chbDtTxBsm.UseVisualStyleBackColor = true;
            this.chbDtTxBsm.CheckedChanged += new System.EventHandler(this.chbDtTxBsm_CheckedChanged);
            // 
            // lblDtTxBlockCountBsm
            // 
            this.lblDtTxBlockCountBsm.AutoSize = true;
            this.lblDtTxBlockCountBsm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDtTxBlockCountBsm.Location = new System.Drawing.Point(171, 24);
            this.lblDtTxBlockCountBsm.Name = "lblDtTxBlockCountBsm";
            this.lblDtTxBlockCountBsm.Size = new System.Drawing.Size(82, 13);
            this.lblDtTxBlockCountBsm.TabIndex = 10;
            this.lblDtTxBlockCountBsm.Text = "TX Block Count";
            // 
            // chbDtRxBsm
            // 
            this.chbDtRxBsm.AutoSize = true;
            this.chbDtRxBsm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbDtRxBsm.Location = new System.Drawing.Point(7, 45);
            this.chbDtRxBsm.Name = "chbDtRxBsm";
            this.chbDtRxBsm.Size = new System.Drawing.Size(104, 17);
            this.chbDtRxBsm.TabIndex = 2;
            this.chbDtRxBsm.Text = "Base Station RX";
            this.chbDtRxBsm.UseVisualStyleBackColor = true;
            this.chbDtRxBsm.CheckedChanged += new System.EventHandler(this.chbDtRxBsm_CheckedChanged);
            // 
            // chbDtValidateRxDataBsm
            // 
            this.chbDtValidateRxDataBsm.AutoSize = true;
            this.chbDtValidateRxDataBsm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbDtValidateRxDataBsm.Location = new System.Drawing.Point(115, 45);
            this.chbDtValidateRxDataBsm.Name = "chbDtValidateRxDataBsm";
            this.chbDtValidateRxDataBsm.Size = new System.Drawing.Size(108, 17);
            this.chbDtValidateRxDataBsm.TabIndex = 3;
            this.chbDtValidateRxDataBsm.Text = "Validate RX Data";
            this.chbDtValidateRxDataBsm.UseVisualStyleBackColor = true;
            this.chbDtValidateRxDataBsm.CheckedChanged += new System.EventHandler(this.chbDtValidateRxDataBsm_CheckedChanged);
            // 
            // grpDtStatBsm
            // 
            this.grpDtStatBsm.Controls.Add(this.txtDtTotalRxByteErrsBsm);
            this.grpDtStatBsm.Controls.Add(this.lblDtTotalRxByteErrsBsm);
            this.grpDtStatBsm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpDtStatBsm.Location = new System.Drawing.Point(11, 151);
            this.grpDtStatBsm.Name = "grpDtStatBsm";
            this.grpDtStatBsm.Size = new System.Drawing.Size(257, 63);
            this.grpDtStatBsm.TabIndex = 107;
            this.grpDtStatBsm.TabStop = false;
            this.grpDtStatBsm.Text = "Status";
            // 
            // txtDtTotalRxByteErrsBsm
            // 
            this.txtDtTotalRxByteErrsBsm.Enabled = false;
            this.txtDtTotalRxByteErrsBsm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDtTotalRxByteErrsBsm.Location = new System.Drawing.Point(125, 23);
            this.txtDtTotalRxByteErrsBsm.Name = "txtDtTotalRxByteErrsBsm";
            this.txtDtTotalRxByteErrsBsm.Size = new System.Drawing.Size(53, 20);
            this.txtDtTotalRxByteErrsBsm.TabIndex = 9;
            this.txtDtTotalRxByteErrsBsm.Text = "0";
            this.txtDtTotalRxByteErrsBsm.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblDtTotalRxByteErrsBsm
            // 
            this.lblDtTotalRxByteErrsBsm.AutoSize = true;
            this.lblDtTotalRxByteErrsBsm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDtTotalRxByteErrsBsm.Location = new System.Drawing.Point(10, 27);
            this.lblDtTotalRxByteErrsBsm.Name = "lblDtTotalRxByteErrsBsm";
            this.lblDtTotalRxByteErrsBsm.Size = new System.Drawing.Size(103, 13);
            this.lblDtTotalRxByteErrsBsm.TabIndex = 7;
            this.lblDtTotalRxByteErrsBsm.Text = "Total RX Byte Errors";
            // 
            // btnStartDataTest
            // 
            this.btnStartDataTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartDataTest.Location = new System.Drawing.Point(271, 250);
            this.btnStartDataTest.Name = "btnStartDataTest";
            this.btnStartDataTest.Size = new System.Drawing.Size(76, 36);
            this.btnStartDataTest.TabIndex = 102;
            this.btnStartDataTest.Text = "Start Data Test";
            this.btnStartDataTest.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnStartDataTest.UseVisualStyleBackColor = true;
            this.btnStartDataTest.Click += new System.EventHandler(this.btnStartDataTest_Click);
            // 
            // tabRemote
            // 
            this.tabRemote.BackColor = System.Drawing.Color.Transparent;
            this.tabRemote.Controls.Add(this.groupBox24);
            this.tabRemote.Controls.Add(this.grpRemoteCals);
            this.tabRemote.ForeColor = System.Drawing.SystemColors.ControlText;
            this.tabRemote.Location = new System.Drawing.Point(4, 22);
            this.tabRemote.Name = "tabRemote";
            this.tabRemote.Size = new System.Drawing.Size(705, 302);
            this.tabRemote.TabIndex = 4;
            this.tabRemote.Text = "Remote Implant (HK)";
            this.tabRemote.UseVisualStyleBackColor = true;
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.groupBox26);
            this.groupBox24.Controls.Add(this.groupBox25);
            this.groupBox24.Enabled = false;
            this.groupBox24.Location = new System.Drawing.Point(312, 11);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(368, 288);
            this.groupBox24.TabIndex = 36;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "Remote Test Functions";
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this.label120);
            this.groupBox26.Controls.Add(this.label119);
            this.groupBox26.Controls.Add(this.button56);
            this.groupBox26.Controls.Add(this.comboBox17);
            this.groupBox26.Controls.Add(this.checkBox51);
            this.groupBox26.Controls.Add(this.textBox61);
            this.groupBox26.Controls.Add(this.label116);
            this.groupBox26.Controls.Add(this.comboBox16);
            this.groupBox26.Location = new System.Drawing.Point(16, 156);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(336, 112);
            this.groupBox26.TabIndex = 84;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "Measure RSSI";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label120.Location = new System.Drawing.Point(268, 28);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(55, 13);
            this.label120.TabIndex = 82;
            this.label120.Text = "IMD RSSI";
            this.label120.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label119.Location = new System.Drawing.Point(168, 28);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(77, 13);
            this.label119.TabIndex = 82;
            this.label119.Text = "Freq (MHz)/Ch";
            // 
            // button56
            // 
            this.button56.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button56.Location = new System.Drawing.Point(12, 64);
            this.button56.Name = "button56";
            this.button56.Size = new System.Drawing.Size(40, 36);
            this.button56.TabIndex = 82;
            this.button56.Text = "Send\r\nCmd\r\n";
            this.button56.UseVisualStyleBackColor = true;
            // 
            // comboBox17
            // 
            this.comboBox17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox17.FormattingEnabled = true;
            this.comboBox17.Items.AddRange(new object[] {
            "402.15 / 0",
            "402.45 / 1",
            "402.75 / 2",
            "403.05 / 3",
            "403.35 / 4",
            "403.65 / 5",
            "403.95 / 6",
            "404.25 / 7",
            "404.55 / 8",
            "404.85 / 9"});
            this.comboBox17.Location = new System.Drawing.Point(164, 72);
            this.comboBox17.Name = "comboBox17";
            this.comboBox17.Size = new System.Drawing.Size(92, 21);
            this.comboBox17.TabIndex = 42;
            // 
            // checkBox51
            // 
            this.checkBox51.AutoSize = true;
            this.checkBox51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox51.Location = new System.Drawing.Point(76, 76);
            this.checkBox51.Name = "checkBox51";
            this.checkBox51.Size = new System.Drawing.Size(85, 17);
            this.checkBox51.TabIndex = 28;
            this.checkBox51.Text = "Base TX EN";
            this.checkBox51.UseVisualStyleBackColor = true;
            // 
            // textBox61
            // 
            this.textBox61.Location = new System.Drawing.Point(268, 44);
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new System.Drawing.Size(56, 20);
            this.textBox61.TabIndex = 36;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label116.Location = new System.Drawing.Point(64, 48);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(96, 13);
            this.label116.TabIndex = 35;
            this.label116.Text = "400 MHz RX RSSI";
            // 
            // comboBox16
            // 
            this.comboBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox16.FormattingEnabled = true;
            this.comboBox16.Items.AddRange(new object[] {
            "402.15 / 0",
            "402.45 / 1",
            "402.75 / 2",
            "403.05 / 3",
            "403.35 / 4",
            "403.65 / 5",
            "403.95 / 6",
            "404.25 / 7",
            "404.55 / 8",
            "404.85 / 9"});
            this.comboBox16.Location = new System.Drawing.Point(164, 44);
            this.comboBox16.Name = "comboBox16";
            this.comboBox16.Size = new System.Drawing.Size(92, 21);
            this.comboBox16.TabIndex = 34;
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this.label132);
            this.groupBox25.Controls.Add(this.comboBox18);
            this.groupBox25.Controls.Add(this.checkBox52);
            this.groupBox25.Controls.Add(this.label117);
            this.groupBox25.Controls.Add(this.button57);
            this.groupBox25.Controls.Add(this.textBox60);
            this.groupBox25.Controls.Add(this.label115);
            this.groupBox25.Controls.Add(this.comboBox11);
            this.groupBox25.Location = new System.Drawing.Point(16, 24);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(336, 112);
            this.groupBox25.TabIndex = 83;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "Transmit Carrier Wave (20 Sec)";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label132.Location = new System.Drawing.Point(168, 28);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(77, 13);
            this.label132.TabIndex = 81;
            this.label132.Text = "Freq (MHz)/Ch";
            // 
            // comboBox18
            // 
            this.comboBox18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox18.FormattingEnabled = true;
            this.comboBox18.Items.AddRange(new object[] {
            "402.15 / 0",
            "402.45 / 1",
            "402.75 / 2",
            "403.05 / 3",
            "403.35 / 4",
            "403.65 / 5",
            "403.95 / 6",
            "404.25 / 7",
            "404.55 / 8",
            "404.85 / 9"});
            this.comboBox18.Location = new System.Drawing.Point(164, 72);
            this.comboBox18.Name = "comboBox18";
            this.comboBox18.Size = new System.Drawing.Size(92, 21);
            this.comboBox18.TabIndex = 44;
            // 
            // checkBox52
            // 
            this.checkBox52.AutoSize = true;
            this.checkBox52.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox52.Location = new System.Drawing.Point(76, 76);
            this.checkBox52.Name = "checkBox52";
            this.checkBox52.Size = new System.Drawing.Size(78, 17);
            this.checkBox52.TabIndex = 43;
            this.checkBox52.Text = "Base RSSI";
            this.checkBox52.UseVisualStyleBackColor = true;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label117.Location = new System.Drawing.Point(268, 56);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(59, 13);
            this.label117.TabIndex = 38;
            this.label117.Text = "Base RSSI";
            this.label117.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button57
            // 
            this.button57.BackColor = System.Drawing.Color.Transparent;
            this.button57.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button57.Location = new System.Drawing.Point(12, 64);
            this.button57.Name = "button57";
            this.button57.Size = new System.Drawing.Size(40, 36);
            this.button57.TabIndex = 37;
            this.button57.Text = "Send\r\nCmd\r\n";
            this.button57.UseVisualStyleBackColor = false;
            // 
            // textBox60
            // 
            this.textBox60.Location = new System.Drawing.Point(268, 72);
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new System.Drawing.Size(56, 20);
            this.textBox60.TabIndex = 31;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label115.Location = new System.Drawing.Point(64, 48);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(88, 13);
            this.label115.TabIndex = 33;
            this.label115.Text = "400 MHz TX CW";
            // 
            // comboBox11
            // 
            this.comboBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox11.FormattingEnabled = true;
            this.comboBox11.Items.AddRange(new object[] {
            "402.15 / 0",
            "402.45 / 1",
            "402.75 / 2",
            "403.05 / 3",
            "403.35 / 4",
            "403.65 / 5",
            "403.95 / 6",
            "404.25 / 7",
            "404.55 / 8",
            "404.85 / 9"});
            this.comboBox11.Location = new System.Drawing.Point(164, 44);
            this.comboBox11.Name = "comboBox11";
            this.comboBox11.Size = new System.Drawing.Size(92, 21);
            this.comboBox11.TabIndex = 32;
            // 
            // grpRemoteCals
            // 
            this.grpRemoteCals.Controls.Add(this.grpRemHighLevelTuning);
            this.grpRemoteCals.Controls.Add(this.groupBox1);
            this.grpRemoteCals.Enabled = false;
            this.grpRemoteCals.Location = new System.Drawing.Point(18, 11);
            this.grpRemoteCals.Name = "grpRemoteCals";
            this.grpRemoteCals.Size = new System.Drawing.Size(272, 288);
            this.grpRemoteCals.TabIndex = 31;
            this.grpRemoteCals.TabStop = false;
            this.grpRemoteCals.Text = "Remote Calibration";
            // 
            // grpRemHighLevelTuning
            // 
            this.grpRemHighLevelTuning.Controls.Add(this.button1);
            this.grpRemHighLevelTuning.Controls.Add(this.checkBox1);
            this.grpRemHighLevelTuning.Controls.Add(this.checkBox2);
            this.grpRemHighLevelTuning.Controls.Add(this.checkBox3);
            this.grpRemHighLevelTuning.Controls.Add(this.checkBox4);
            this.grpRemHighLevelTuning.Controls.Add(this.checkBox5);
            this.grpRemHighLevelTuning.Controls.Add(this.checkBox6);
            this.grpRemHighLevelTuning.Controls.Add(this.label46);
            this.grpRemHighLevelTuning.Controls.Add(this.label48);
            this.grpRemHighLevelTuning.Controls.Add(this.label66);
            this.grpRemHighLevelTuning.Controls.Add(this.label67);
            this.grpRemHighLevelTuning.Controls.Add(this.label68);
            this.grpRemHighLevelTuning.Controls.Add(this.label69);
            this.grpRemHighLevelTuning.Controls.Add(this.label70);
            this.grpRemHighLevelTuning.Controls.Add(this.label71);
            this.grpRemHighLevelTuning.Controls.Add(this.label72);
            this.grpRemHighLevelTuning.Controls.Add(this.textBox1);
            this.grpRemHighLevelTuning.Controls.Add(this.textBox2);
            this.grpRemHighLevelTuning.Controls.Add(this.textBox3);
            this.grpRemHighLevelTuning.Controls.Add(this.textBox4);
            this.grpRemHighLevelTuning.Controls.Add(this.textBox5);
            this.grpRemHighLevelTuning.Controls.Add(this.textBox6);
            this.grpRemHighLevelTuning.Controls.Add(this.textBox7);
            this.grpRemHighLevelTuning.Controls.Add(this.textBox8);
            this.grpRemHighLevelTuning.Controls.Add(this.textBox9);
            this.grpRemHighLevelTuning.Controls.Add(this.label73);
            this.grpRemHighLevelTuning.Controls.Add(this.label74);
            this.grpRemHighLevelTuning.Controls.Add(this.textBox10);
            this.grpRemHighLevelTuning.Location = new System.Drawing.Point(15, 19);
            this.grpRemHighLevelTuning.Name = "grpRemHighLevelTuning";
            this.grpRemHighLevelTuning.Size = new System.Drawing.Size(242, 177);
            this.grpRemHighLevelTuning.TabIndex = 106;
            this.grpRemHighLevelTuning.TabStop = false;
            this.grpRemHighLevelTuning.Text = "400 MHz High Level Tuning Algorithm";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(162, 120);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(66, 36);
            this.button1.TabIndex = 74;
            this.button1.Text = "Start Calibration";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(118, 60);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(90, 17);
            this.checkBox1.TabIndex = 73;
            this.checkBox1.Text = "TX Tune Cap";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Checked = true;
            this.checkBox2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox2.Location = new System.Drawing.Point(118, 24);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(115, 17);
            this.checkBox2.TabIndex = 72;
            this.checkBox2.Text = "Match 2 Tune Cap";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Checked = true;
            this.checkBox3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox3.Location = new System.Drawing.Point(118, 42);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(115, 17);
            this.checkBox3.TabIndex = 71;
            this.checkBox3.Text = "Match 1 Tune Cap";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox4.Location = new System.Drawing.Point(12, 84);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(101, 17);
            this.checkBox4.TabIndex = 70;
            this.checkBox4.Text = "Ext. Alg. Enable";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox5.Location = new System.Drawing.Point(12, 66);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(77, 17);
            this.checkBox5.TabIndex = 69;
            this.checkBox5.Text = "RX Enable";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox6.Location = new System.Drawing.Point(12, 24);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(106, 17);
            this.checkBox6.TabIndex = 68;
            this.checkBox6.Text = "TX Enable ---------";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(25, 152);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(13, 13);
            this.label46.TabIndex = 67;
            this.label46.Text = "8";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(39, 152);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(13, 13);
            this.label48.TabIndex = 66;
            this.label48.Text = "7";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(53, 152);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(13, 13);
            this.label66.TabIndex = 65;
            this.label66.Text = "6";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(67, 152);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(13, 13);
            this.label67.TabIndex = 64;
            this.label67.Text = "5";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(81, 152);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(13, 13);
            this.label68.TabIndex = 63;
            this.label68.Text = "4";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(95, 152);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(13, 13);
            this.label69.TabIndex = 62;
            this.label69.Text = "3";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(109, 152);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(13, 13);
            this.label70.TabIndex = 61;
            this.label70.Text = "2";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(123, 152);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(13, 13);
            this.label71.TabIndex = 60;
            this.label71.Text = "1";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(30, 109);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(103, 17);
            this.label72.TabIndex = 21;
            this.label72.Text = "Channel Select";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(137, 129);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(14, 20);
            this.textBox1.TabIndex = 20;
            this.textBox1.Text = "0";
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(123, 129);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(14, 20);
            this.textBox2.TabIndex = 19;
            this.textBox2.Text = "0";
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(109, 129);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(14, 20);
            this.textBox3.TabIndex = 18;
            this.textBox3.Text = "0";
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(95, 129);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(14, 20);
            this.textBox4.TabIndex = 17;
            this.textBox4.Text = "0";
            // 
            // textBox5
            // 
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.Location = new System.Drawing.Point(81, 129);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(14, 20);
            this.textBox5.TabIndex = 16;
            this.textBox5.Text = "0";
            // 
            // textBox6
            // 
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(67, 129);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(14, 20);
            this.textBox6.TabIndex = 15;
            this.textBox6.Text = "0";
            // 
            // textBox7
            // 
            this.textBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.Location = new System.Drawing.Point(53, 129);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(14, 20);
            this.textBox7.TabIndex = 14;
            this.textBox7.Text = "0";
            // 
            // textBox8
            // 
            this.textBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox8.Location = new System.Drawing.Point(39, 129);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(14, 20);
            this.textBox8.TabIndex = 13;
            this.textBox8.Text = "0";
            // 
            // textBox9
            // 
            this.textBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox9.Location = new System.Drawing.Point(25, 129);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(14, 20);
            this.textBox9.TabIndex = 12;
            this.textBox9.Text = "0";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(11, 152);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(13, 13);
            this.label73.TabIndex = 11;
            this.label73.Text = "9";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(137, 152);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(13, 13);
            this.label74.TabIndex = 10;
            this.label74.Text = "0";
            // 
            // textBox10
            // 
            this.textBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox10.Location = new System.Drawing.Point(11, 129);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(14, 20);
            this.textBox10.TabIndex = 0;
            this.textBox10.Text = "0";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnCalReqRem);
            this.groupBox1.Controls.Add(this.lblCalReqRem);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Location = new System.Drawing.Point(15, 204);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(241, 78);
            this.groupBox1.TabIndex = 105;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "24MHz Crystal Osc Tuning";
            // 
            // btnCalReqRem
            // 
            this.btnCalReqRem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalReqRem.Location = new System.Drawing.Point(188, 54);
            this.btnCalReqRem.Name = "btnCalReqRem";
            this.btnCalReqRem.Size = new System.Drawing.Size(40, 20);
            this.btnCalReqRem.TabIndex = 0;
            this.btnCalReqRem.Text = "OK";
            this.btnCalReqRem.UseVisualStyleBackColor = true;
            // 
            // lblCalReqRem
            // 
            this.lblCalReqRem.AutoSize = true;
            this.lblCalReqRem.BackColor = System.Drawing.Color.CornflowerBlue;
            this.lblCalReqRem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCalReqRem.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCalReqRem.Location = new System.Drawing.Point(12, 56);
            this.lblCalReqRem.Name = "lblCalReqRem";
            this.lblCalReqRem.Size = new System.Drawing.Size(142, 15);
            this.lblCalReqRem.TabIndex = 1;
            this.lblCalReqRem.Text = "Turn on 402.15 MHz carrier.";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(94, 24);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(40, 20);
            this.button2.TabIndex = 94;
            this.button2.Text = "Cal";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // tabFactoryUse
            // 
            this.tabFactoryUse.Controls.Add(this.btnShowRssiToDbmCalsFromNvmOnCcaTab);
            this.tabFactoryUse.Controls.Add(this.grp245Cal);
            this.tabFactoryUse.Controls.Add(this.chbUseIntRssiOnBsm);
            this.tabFactoryUse.Controls.Add(this.btnNonvolatileMemory);
            this.tabFactoryUse.Controls.Add(this.grpCcReg);
            this.tabFactoryUse.Controls.Add(this.btnStartTraceView);
            this.tabFactoryUse.Controls.Add(this.grpBerSetup);
            this.tabFactoryUse.Location = new System.Drawing.Point(4, 22);
            this.tabFactoryUse.Name = "tabFactoryUse";
            this.tabFactoryUse.Size = new System.Drawing.Size(705, 302);
            this.tabFactoryUse.TabIndex = 5;
            this.tabFactoryUse.Text = "Factory Use";
            this.tabFactoryUse.UseVisualStyleBackColor = true;
            // 
            // btnShowRssiToDbmCalsFromNvmOnCcaTab
            // 
            this.btnShowRssiToDbmCalsFromNvmOnCcaTab.AutoSize = true;
            this.btnShowRssiToDbmCalsFromNvmOnCcaTab.Location = new System.Drawing.Point(273, 65);
            this.btnShowRssiToDbmCalsFromNvmOnCcaTab.Name = "btnShowRssiToDbmCalsFromNvmOnCcaTab";
            this.btnShowRssiToDbmCalsFromNvmOnCcaTab.Size = new System.Drawing.Size(155, 38);
            this.btnShowRssiToDbmCalsFromNvmOnCcaTab.TabIndex = 27;
            this.btnShowRssiToDbmCalsFromNvmOnCcaTab.Text = "Show NVM RSSI-to-dBm\r\nCals on CCA/RSSI Tab";
            this.btnShowRssiToDbmCalsFromNvmOnCcaTab.UseVisualStyleBackColor = true;
            this.btnShowRssiToDbmCalsFromNvmOnCcaTab.Click += new System.EventHandler(this.btnShowRssiToDbmCalsFromNvm_Click);
            // 
            // grp245Cal
            // 
            this.grp245Cal.Controls.Add(this.btn245Cal);
            this.grp245Cal.Controls.Add(this.lblLnaNegTrimBackoff);
            this.grp245Cal.Controls.Add(this.txtLnaNegTrimBackoff);
            this.grp245Cal.Location = new System.Drawing.Point(460, 12);
            this.grp245Cal.Name = "grp245Cal";
            this.grp245Cal.Size = new System.Drawing.Size(162, 120);
            this.grp245Cal.TabIndex = 29;
            this.grp245Cal.TabStop = false;
            this.grp245Cal.Text = "2.45 GHz Calibration";
            // 
            // btn245Cal
            // 
            this.btn245Cal.BackColor = System.Drawing.Color.Transparent;
            this.btn245Cal.Location = new System.Drawing.Point(29, 23);
            this.btn245Cal.Name = "btn245Cal";
            this.btn245Cal.Size = new System.Drawing.Size(100, 35);
            this.btn245Cal.TabIndex = 3;
            this.btn245Cal.Text = "2.45 GHz Calibration";
            this.btn245Cal.UseVisualStyleBackColor = false;
            this.btn245Cal.Click += new System.EventHandler(this.btn245Cal_Click);
            // 
            // lblLnaNegTrimBackoff
            // 
            this.lblLnaNegTrimBackoff.AutoSize = true;
            this.lblLnaNegTrimBackoff.Location = new System.Drawing.Point(9, 75);
            this.lblLnaNegTrimBackoff.Name = "lblLnaNegTrimBackoff";
            this.lblLnaNegTrimBackoff.Size = new System.Drawing.Size(130, 13);
            this.lblLnaNegTrimBackoff.TabIndex = 5;
            this.lblLnaNegTrimBackoff.Text = "LNA NegTrim Backoff";
            // 
            // txtLnaNegTrimBackoff
            // 
            this.txtLnaNegTrimBackoff.Location = new System.Drawing.Point(29, 91);
            this.txtLnaNegTrimBackoff.Name = "txtLnaNegTrimBackoff";
            this.txtLnaNegTrimBackoff.Size = new System.Drawing.Size(100, 20);
            this.txtLnaNegTrimBackoff.TabIndex = 4;
            // 
            // chbUseIntRssiOnBsm
            // 
            this.chbUseIntRssiOnBsm.AutoSize = true;
            this.chbUseIntRssiOnBsm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbUseIntRssiOnBsm.Location = new System.Drawing.Point(275, 156);
            this.chbUseIntRssiOnBsm.Name = "chbUseIntRssiOnBsm";
            this.chbUseIntRssiOnBsm.Size = new System.Drawing.Size(111, 17);
            this.chbUseIntRssiOnBsm.TabIndex = 25;
            this.chbUseIntRssiOnBsm.Text = "Use Internal RSSI";
            this.chbUseIntRssiOnBsm.UseVisualStyleBackColor = true;
            this.chbUseIntRssiOnBsm.CheckedChanged += new System.EventHandler(this.chbUseIntRssiOnBsm_CheckedChanged);
            // 
            // btnNonvolatileMemory
            // 
            this.btnNonvolatileMemory.Location = new System.Drawing.Point(273, 112);
            this.btnNonvolatileMemory.Name = "btnNonvolatileMemory";
            this.btnNonvolatileMemory.Size = new System.Drawing.Size(100, 35);
            this.btnNonvolatileMemory.TabIndex = 21;
            this.btnNonvolatileMemory.Text = "Nonvolatile Memory";
            this.btnNonvolatileMemory.UseVisualStyleBackColor = true;
            this.btnNonvolatileMemory.Visible = false;
            this.btnNonvolatileMemory.Click += new System.EventHandler(this.btnNonvolatileMemory_Click);
            // 
            // grpCcReg
            // 
            this.grpCcReg.Controls.Add(this.txtCcRegAddr);
            this.grpCcReg.Controls.Add(this.btnCcRegWrite);
            this.grpCcReg.Controls.Add(this.btnCcRegRead);
            this.grpCcReg.Controls.Add(this.label78);
            this.grpCcReg.Controls.Add(this.txtCcRegVal);
            this.grpCcReg.Location = new System.Drawing.Point(8, 143);
            this.grpCcReg.Name = "grpCcReg";
            this.grpCcReg.Size = new System.Drawing.Size(227, 91);
            this.grpCcReg.TabIndex = 18;
            this.grpCcReg.TabStop = false;
            this.grpCcReg.Text = "CC25XX Register";
            this.grpCcReg.Visible = false;
            // 
            // txtCcRegAddr
            // 
            this.txtCcRegAddr.Location = new System.Drawing.Point(6, 19);
            this.txtCcRegAddr.Name = "txtCcRegAddr";
            this.txtCcRegAddr.Size = new System.Drawing.Size(100, 20);
            this.txtCcRegAddr.TabIndex = 12;
            this.txtCcRegAddr.TextChanged += new System.EventHandler(this.txtCcRegAddr_TextChanged);
            this.txtCcRegAddr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHex_KeyDown);
            this.txtCcRegAddr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHex_KeyPress);
            // 
            // btnCcRegWrite
            // 
            this.btnCcRegWrite.Location = new System.Drawing.Point(169, 19);
            this.btnCcRegWrite.Name = "btnCcRegWrite";
            this.btnCcRegWrite.Size = new System.Drawing.Size(35, 23);
            this.btnCcRegWrite.TabIndex = 8;
            this.btnCcRegWrite.Text = "W";
            this.btnCcRegWrite.UseVisualStyleBackColor = true;
            this.btnCcRegWrite.Click += new System.EventHandler(this.btnCcRegWrite_Click);
            // 
            // btnCcRegRead
            // 
            this.btnCcRegRead.Location = new System.Drawing.Point(131, 19);
            this.btnCcRegRead.Name = "btnCcRegRead";
            this.btnCcRegRead.Size = new System.Drawing.Size(35, 23);
            this.btnCcRegRead.TabIndex = 9;
            this.btnCcRegRead.Text = "R";
            this.btnCcRegRead.UseVisualStyleBackColor = true;
            this.btnCcRegRead.Click += new System.EventHandler(this.btnCcRegRead_Click);
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(13, 45);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(39, 13);
            this.label78.TabIndex = 15;
            this.label78.Text = "Value";
            // 
            // txtCcRegVal
            // 
            this.txtCcRegVal.Location = new System.Drawing.Point(6, 63);
            this.txtCcRegVal.Name = "txtCcRegVal";
            this.txtCcRegVal.Size = new System.Drawing.Size(100, 20);
            this.txtCcRegVal.TabIndex = 13;
            this.txtCcRegVal.TextChanged += new System.EventHandler(this.txtCcRegVal_TextChanged);
            this.txtCcRegVal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHex_KeyDown);
            this.txtCcRegVal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHex_KeyPress);
            // 
            // btnStartTraceView
            // 
            this.btnStartTraceView.Location = new System.Drawing.Point(273, 19);
            this.btnStartTraceView.Name = "btnStartTraceView";
            this.btnStartTraceView.Size = new System.Drawing.Size(134, 38);
            this.btnStartTraceView.TabIndex = 2;
            this.btnStartTraceView.Text = "Start Trace Viewer";
            this.btnStartTraceView.UseVisualStyleBackColor = true;
            this.btnStartTraceView.Click += new System.EventHandler(this.btnStartTraceView_Click);
            // 
            // grpBerSetup
            // 
            this.grpBerSetup.Controls.Add(this.lblBerChan);
            this.grpBerSetup.Controls.Add(this.cmbBerChan);
            this.grpBerSetup.Controls.Add(this.btnStartBer);
            this.grpBerSetup.Controls.Add(this.grpBerDataRate);
            this.grpBerSetup.Location = new System.Drawing.Point(8, 12);
            this.grpBerSetup.Name = "grpBerSetup";
            this.grpBerSetup.Size = new System.Drawing.Size(227, 124);
            this.grpBerSetup.TabIndex = 0;
            this.grpBerSetup.TabStop = false;
            this.grpBerSetup.Text = "Bit Error Rate Setup";
            // 
            // lblBerChan
            // 
            this.lblBerChan.AutoSize = true;
            this.lblBerChan.Location = new System.Drawing.Point(128, 23);
            this.lblBerChan.Name = "lblBerChan";
            this.lblBerChan.Size = new System.Drawing.Size(76, 13);
            this.lblBerChan.TabIndex = 60;
            this.lblBerChan.Text = "Chan Select";
            // 
            // cmbBerChan
            // 
            this.cmbBerChan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBerChan.FormattingEnabled = true;
            this.cmbBerChan.Items.AddRange(new object[] {
            "402.15 / 0",
            "402.45 / 1",
            "402.75 / 2",
            "403.05 / 3",
            "403.35 / 4",
            "403.65 / 5",
            "403.95 / 6",
            "404.25 / 7",
            "404.55 / 8",
            "404.85 / 9"});
            this.cmbBerChan.Location = new System.Drawing.Point(125, 40);
            this.cmbBerChan.Name = "cmbBerChan";
            this.cmbBerChan.Size = new System.Drawing.Size(81, 21);
            this.cmbBerChan.TabIndex = 59;
            // 
            // btnStartBer
            // 
            this.btnStartBer.Location = new System.Drawing.Point(129, 74);
            this.btnStartBer.Name = "btnStartBer";
            this.btnStartBer.Size = new System.Drawing.Size(75, 37);
            this.btnStartBer.TabIndex = 1;
            this.btnStartBer.Text = "Start";
            this.btnStartBer.UseVisualStyleBackColor = true;
            this.btnStartBer.Click += new System.EventHandler(this.btnStartBer_Click);
            // 
            // grpBerDataRate
            // 
            this.grpBerDataRate.Controls.Add(this.radBer800kbps);
            this.grpBerDataRate.Controls.Add(this.radBer400kbps);
            this.grpBerDataRate.Controls.Add(this.radBer200kbps);
            this.grpBerDataRate.Location = new System.Drawing.Point(6, 19);
            this.grpBerDataRate.Name = "grpBerDataRate";
            this.grpBerDataRate.Size = new System.Drawing.Size(100, 92);
            this.grpBerDataRate.TabIndex = 0;
            this.grpBerDataRate.TabStop = false;
            this.grpBerDataRate.Text = "Data Rate";
            // 
            // radBer800kbps
            // 
            this.radBer800kbps.AutoSize = true;
            this.radBer800kbps.Location = new System.Drawing.Point(6, 67);
            this.radBer800kbps.Name = "radBer800kbps";
            this.radBer800kbps.Size = new System.Drawing.Size(78, 17);
            this.radBer800kbps.TabIndex = 2;
            this.radBer800kbps.Text = "800 Kbps";
            this.radBer800kbps.UseVisualStyleBackColor = true;
            // 
            // radBer400kbps
            // 
            this.radBer400kbps.AutoSize = true;
            this.radBer400kbps.Location = new System.Drawing.Point(6, 44);
            this.radBer400kbps.Name = "radBer400kbps";
            this.radBer400kbps.Size = new System.Drawing.Size(78, 17);
            this.radBer400kbps.TabIndex = 1;
            this.radBer400kbps.Text = "400 Kbps";
            this.radBer400kbps.UseVisualStyleBackColor = true;
            // 
            // radBer200kbps
            // 
            this.radBer200kbps.AutoSize = true;
            this.radBer200kbps.Checked = true;
            this.radBer200kbps.Location = new System.Drawing.Point(6, 21);
            this.radBer200kbps.Name = "radBer200kbps";
            this.radBer200kbps.Size = new System.Drawing.Size(78, 17);
            this.radBer200kbps.TabIndex = 0;
            this.radBer200kbps.TabStop = true;
            this.radBer200kbps.Text = "200 Kbps";
            this.radBer200kbps.UseVisualStyleBackColor = true;
            // 
            // grpStat
            // 
            this.grpStat.Controls.Add(this.grpImStatOnBsm);
            this.grpStat.Controls.Add(this.grpRxImdTidList);
            this.grpStat.Controls.Add(this.grpWakeupMode);
            this.grpStat.Controls.Add(this.grpSysMsg);
            this.grpStat.Controls.Add(this.grpStatSession);
            this.grpStat.Controls.Add(this.grpStat400Stat);
            this.grpStat.Controls.Add(this.grpBsmStat);
            this.grpStat.Controls.Add(this.grpImCommandInterface);
            this.grpStat.Controls.Add(this.grpImStat);
            this.grpStat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpStat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpStat.ForeColor = System.Drawing.Color.Black;
            this.grpStat.Location = new System.Drawing.Point(0, 0);
            this.grpStat.Name = "grpStat";
            this.grpStat.Size = new System.Drawing.Size(713, 414);
            this.grpStat.TabIndex = 40;
            this.grpStat.TabStop = false;
            this.grpStat.Text = "System Status and Control";
            // 
            // grpImStatOnBsm
            // 
            this.grpImStatOnBsm.Controls.Add(this.btnImRegsOnBsm);
            this.grpImStatOnBsm.Location = new System.Drawing.Point(464, 189);
            this.grpImStatOnBsm.Name = "grpImStatOnBsm";
            this.grpImStatOnBsm.Size = new System.Drawing.Size(241, 65);
            this.grpImStatOnBsm.TabIndex = 111;
            this.grpImStatOnBsm.TabStop = false;
            this.grpImStatOnBsm.Text = "Implant Status + Control (AIM)";
            // 
            // btnImRegsOnBsm
            // 
            this.btnImRegsOnBsm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImRegsOnBsm.Location = new System.Drawing.Point(8, 20);
            this.btnImRegsOnBsm.Name = "btnImRegsOnBsm";
            this.btnImRegsOnBsm.Size = new System.Drawing.Size(69, 35);
            this.btnImRegsOnBsm.TabIndex = 37;
            this.btnImRegsOnBsm.Text = "ZL70103 Registers";
            this.btnImRegsOnBsm.UseVisualStyleBackColor = true;
            this.btnImRegsOnBsm.Click += new System.EventHandler(this.btnImRegsOnBsm_Click);
            // 
            // grpRxImdTidList
            // 
            this.grpRxImdTidList.Controls.Add(this.dgvRxImdTidList);
            this.grpRxImdTidList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpRxImdTidList.Location = new System.Drawing.Point(6, 237);
            this.grpRxImdTidList.Name = "grpRxImdTidList";
            this.grpRxImdTidList.Size = new System.Drawing.Size(442, 107);
            this.grpRxImdTidList.TabIndex = 39;
            this.grpRxImdTidList.TabStop = false;
            this.grpRxImdTidList.Text = "Wake-up Responses or Emergency Messages";
            // 
            // dgvRxImdTidList
            // 
            this.dgvRxImdTidList.AllowUserToAddRows = false;
            this.dgvRxImdTidList.AllowUserToDeleteRows = false;
            this.dgvRxImdTidList.AllowUserToResizeColumns = false;
            this.dgvRxImdTidList.AllowUserToResizeRows = false;
            this.dgvRxImdTidList.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvRxImdTidList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvRxImdTidList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvRxImdTidList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvRxImdTidList.ColumnHeadersHeight = 20;
            this.dgvRxImdTidList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvRxImdTidList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column1});
            this.dgvRxImdTidList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRxImdTidList.Location = new System.Drawing.Point(3, 16);
            this.dgvRxImdTidList.MultiSelect = false;
            this.dgvRxImdTidList.Name = "dgvRxImdTidList";
            this.dgvRxImdTidList.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvRxImdTidList.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvRxImdTidList.RowHeadersWidth = 20;
            this.dgvRxImdTidList.RowTemplate.Height = 24;
            this.dgvRxImdTidList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvRxImdTidList.Size = new System.Drawing.Size(436, 88);
            this.dgvRxImdTidList.TabIndex = 19;
            this.dgvRxImdTidList.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column2.FillWeight = 85.10638F;
            this.Column2.HeaderText = "IMD TID";
            this.Column2.MinimumWidth = 50;
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column2.Width = 70;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column3.FillWeight = 107.4468F;
            this.Column3.HeaderText = "Count";
            this.Column3.MinimumWidth = 25;
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column3.Width = 55;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column4.FillWeight = 107.4468F;
            this.Column4.HeaderText = "Company";
            this.Column4.MinimumWidth = 50;
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column4.Width = 120;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.DividerWidth = 10;
            this.Column1.HeaderText = "Description";
            this.Column1.MinimumWidth = 100;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // grpWakeupMode
            // 
            this.grpWakeupMode.Controls.Add(this.rad245GHzWakeup);
            this.grpWakeupMode.Controls.Add(this.rad400MHzWakeup);
            this.grpWakeupMode.Controls.Add(this.chbExtStrobeFor245Sniff);
            this.grpWakeupMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpWakeupMode.Location = new System.Drawing.Point(6, 194);
            this.grpWakeupMode.Name = "grpWakeupMode";
            this.grpWakeupMode.Size = new System.Drawing.Size(216, 60);
            this.grpWakeupMode.TabIndex = 28;
            this.grpWakeupMode.TabStop = false;
            this.grpWakeupMode.Text = "Wake-up Mode";
            // 
            // rad245GHzWakeup
            // 
            this.rad245GHzWakeup.AutoSize = true;
            this.rad245GHzWakeup.Checked = true;
            this.rad245GHzWakeup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rad245GHzWakeup.Location = new System.Drawing.Point(16, 16);
            this.rad245GHzWakeup.Name = "rad245GHzWakeup";
            this.rad245GHzWakeup.Size = new System.Drawing.Size(70, 17);
            this.rad245GHzWakeup.TabIndex = 29;
            this.rad245GHzWakeup.TabStop = true;
            this.rad245GHzWakeup.Text = "2.45 GHz";
            this.rad245GHzWakeup.UseVisualStyleBackColor = true;
            // 
            // rad400MHzWakeup
            // 
            this.rad400MHzWakeup.AutoSize = true;
            this.rad400MHzWakeup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rad400MHzWakeup.Location = new System.Drawing.Point(121, 16);
            this.rad400MHzWakeup.Name = "rad400MHzWakeup";
            this.rad400MHzWakeup.Size = new System.Drawing.Size(68, 17);
            this.rad400MHzWakeup.TabIndex = 30;
            this.rad400MHzWakeup.Text = "400 MHz";
            this.rad400MHzWakeup.UseVisualStyleBackColor = true;
            this.rad400MHzWakeup.CheckedChanged += new System.EventHandler(this.rad400MHzWakeup_CheckedChanged);
            // 
            // chbExtStrobeFor245Sniff
            // 
            this.chbExtStrobeFor245Sniff.AutoSize = true;
            this.chbExtStrobeFor245Sniff.Checked = true;
            this.chbExtStrobeFor245Sniff.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbExtStrobeFor245Sniff.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbExtStrobeFor245Sniff.Location = new System.Drawing.Point(16, 39);
            this.chbExtStrobeFor245Sniff.Name = "chbExtStrobeFor245Sniff";
            this.chbExtStrobeFor245Sniff.Size = new System.Drawing.Size(98, 17);
            this.chbExtStrobeFor245Sniff.TabIndex = 31;
            this.chbExtStrobeFor245Sniff.Text = "External Strobe";
            this.chbExtStrobeFor245Sniff.UseVisualStyleBackColor = true;
            this.chbExtStrobeFor245Sniff.CheckedChanged += new System.EventHandler(this.chbExtStrobeFor245Sniff_CheckedChanged);
            // 
            // grpSysMsg
            // 
            this.grpSysMsg.Controls.Add(this.btnClearSysMsg);
            this.grpSysMsg.Controls.Add(this.txtSysMsg);
            this.grpSysMsg.Location = new System.Drawing.Point(6, 347);
            this.grpSysMsg.Name = "grpSysMsg";
            this.grpSysMsg.Size = new System.Drawing.Size(699, 65);
            this.grpSysMsg.TabIndex = 46;
            this.grpSysMsg.TabStop = false;
            this.grpSysMsg.Text = "System Messages";
            // 
            // btnClearSysMsg
            // 
            this.btnClearSysMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearSysMsg.Location = new System.Drawing.Point(645, 18);
            this.btnClearSysMsg.Name = "btnClearSysMsg";
            this.btnClearSysMsg.Size = new System.Drawing.Size(49, 41);
            this.btnClearSysMsg.TabIndex = 47;
            this.btnClearSysMsg.Text = "Clear";
            this.btnClearSysMsg.UseVisualStyleBackColor = true;
            this.btnClearSysMsg.Click += new System.EventHandler(this.btnClearSysMsg_Click);
            // 
            // txtSysMsg
            // 
            this.txtSysMsg.Location = new System.Drawing.Point(3, 18);
            this.txtSysMsg.Margin = new System.Windows.Forms.Padding(2);
            this.txtSysMsg.Multiline = true;
            this.txtSysMsg.Name = "txtSysMsg";
            this.txtSysMsg.ReadOnly = true;
            this.txtSysMsg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtSysMsg.Size = new System.Drawing.Size(638, 42);
            this.txtSysMsg.TabIndex = 46;
            // 
            // grpStatSession
            // 
            this.grpStatSession.Controls.Add(this.btnStartListen);
            this.grpStatSession.Controls.Add(this.chbResetMicsOnWakeup);
            this.grpStatSession.Controls.Add(this.chbDataGat);
            this.grpStatSession.Controls.Add(this.chbStatPoll);
            this.grpStatSession.Controls.Add(this.chbAutoListen);
            this.grpStatSession.Controls.Add(this.chbAutoCca);
            this.grpStatSession.Controls.Add(this.chbAnyImSearch);
            this.grpStatSession.Controls.Add(this.btnStartImSearch);
            this.grpStatSession.Controls.Add(this.chbEnabHkWrite);
            this.grpStatSession.Controls.Add(this.btnWakeupIm);
            this.grpStatSession.Controls.Add(this.btnWakeupBsm);
            this.grpStatSession.Controls.Add(this.btnStartSession);
            this.grpStatSession.Controls.Add(this.btnSendEmer);
            this.grpStatSession.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpStatSession.Location = new System.Drawing.Point(6, 16);
            this.grpStatSession.Name = "grpStatSession";
            this.grpStatSession.Size = new System.Drawing.Size(217, 176);
            this.grpStatSession.TabIndex = 45;
            this.grpStatSession.TabStop = false;
            this.grpStatSession.Text = "Session Control";
            // 
            // btnStartListen
            // 
            this.btnStartListen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartListen.Location = new System.Drawing.Point(6, 112);
            this.btnStartListen.Name = "btnStartListen";
            this.btnStartListen.Size = new System.Drawing.Size(105, 28);
            this.btnStartListen.TabIndex = 33;
            this.btnStartListen.Text = "Listen Emergency";
            this.btnStartListen.UseVisualStyleBackColor = true;
            this.btnStartListen.Click += new System.EventHandler(this.btnStartListen_Click);
            // 
            // chbResetMicsOnWakeup
            // 
            this.chbResetMicsOnWakeup.AutoSize = true;
            this.chbResetMicsOnWakeup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbResetMicsOnWakeup.Location = new System.Drawing.Point(120, 21);
            this.chbResetMicsOnWakeup.Name = "chbResetMicsOnWakeup";
            this.chbResetMicsOnWakeup.Size = new System.Drawing.Size(70, 30);
            this.chbResetMicsOnWakeup.TabIndex = 38;
            this.chbResetMicsOnWakeup.Text = "Reset on\r\nWake-up";
            this.chbResetMicsOnWakeup.UseVisualStyleBackColor = true;
            this.chbResetMicsOnWakeup.CheckedChanged += new System.EventHandler(this.chbResetMicsOnWakeup_CheckedChanged);
            // 
            // chbDataGat
            // 
            this.chbDataGat.AutoSize = true;
            this.chbDataGat.Checked = true;
            this.chbDataGat.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbDataGat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbDataGat.Location = new System.Drawing.Point(119, 149);
            this.chbDataGat.Name = "chbDataGat";
            this.chbDataGat.Size = new System.Drawing.Size(84, 17);
            this.chbDataGat.TabIndex = 28;
            this.chbDataGat.Text = "Data Gather";
            this.chbDataGat.UseVisualStyleBackColor = true;
            // 
            // chbStatPoll
            // 
            this.chbStatPoll.AutoSize = true;
            this.chbStatPoll.Checked = true;
            this.chbStatPoll.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbStatPoll.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbStatPoll.Location = new System.Drawing.Point(14, 149);
            this.chbStatPoll.Name = "chbStatPoll";
            this.chbStatPoll.Size = new System.Drawing.Size(90, 17);
            this.chbStatPoll.TabIndex = 27;
            this.chbStatPoll.Text = "Status Polling";
            this.chbStatPoll.UseVisualStyleBackColor = true;
            this.chbStatPoll.CheckedChanged += new System.EventHandler(this.chbStatPoll_CheckedChanged);
            // 
            // chbAutoListen
            // 
            this.chbAutoListen.AutoSize = true;
            this.chbAutoListen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbAutoListen.Location = new System.Drawing.Point(120, 119);
            this.chbAutoListen.Name = "chbAutoListen";
            this.chbAutoListen.Size = new System.Drawing.Size(79, 17);
            this.chbAutoListen.TabIndex = 25;
            this.chbAutoListen.Text = "Auto Listen";
            this.chbAutoListen.UseVisualStyleBackColor = true;
            this.chbAutoListen.CheckedChanged += new System.EventHandler(this.chbAutoListen_CheckedChanged);
            // 
            // chbAutoCca
            // 
            this.chbAutoCca.AutoSize = true;
            this.chbAutoCca.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbAutoCca.Location = new System.Drawing.Point(120, 59);
            this.chbAutoCca.Name = "chbAutoCca";
            this.chbAutoCca.Size = new System.Drawing.Size(72, 17);
            this.chbAutoCca.TabIndex = 22;
            this.chbAutoCca.Text = "Auto CCA";
            this.chbAutoCca.UseVisualStyleBackColor = true;
            this.chbAutoCca.CheckedChanged += new System.EventHandler(this.chbAutoCca_CheckedChanged);
            // 
            // chbAnyImSearch
            // 
            this.chbAnyImSearch.AutoSize = true;
            this.chbAnyImSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbAnyImSearch.Location = new System.Drawing.Point(120, 89);
            this.chbAnyImSearch.Name = "chbAnyImSearch";
            this.chbAnyImSearch.Size = new System.Drawing.Size(81, 17);
            this.chbAnyImSearch.TabIndex = 24;
            this.chbAnyImSearch.Text = "Any Implant";
            this.chbAnyImSearch.UseVisualStyleBackColor = true;
            // 
            // btnStartImSearch
            // 
            this.btnStartImSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartImSearch.Location = new System.Drawing.Point(6, 82);
            this.btnStartImSearch.Name = "btnStartImSearch";
            this.btnStartImSearch.Size = new System.Drawing.Size(105, 28);
            this.btnStartImSearch.TabIndex = 32;
            this.btnStartImSearch.Text = "Search Implant(s)";
            this.btnStartImSearch.UseVisualStyleBackColor = true;
            this.btnStartImSearch.Click += new System.EventHandler(this.btnStartImSearch_Click);
            // 
            // chbEnabHkWrite
            // 
            this.chbEnabHkWrite.AutoSize = true;
            this.chbEnabHkWrite.Checked = true;
            this.chbEnabHkWrite.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbEnabHkWrite.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbEnabHkWrite.Location = new System.Drawing.Point(14, 130);
            this.chbEnabHkWrite.Name = "chbEnabHkWrite";
            this.chbEnabHkWrite.Size = new System.Drawing.Size(143, 17);
            this.chbEnabHkWrite.TabIndex = 36;
            this.chbEnabHkWrite.Text = "Enable HK Write Access";
            this.chbEnabHkWrite.UseVisualStyleBackColor = true;
            this.chbEnabHkWrite.CheckedChanged += new System.EventHandler(this.chbEnabHkWrite_CheckedChanged);
            // 
            // btnWakeupIm
            // 
            this.btnWakeupIm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWakeupIm.Location = new System.Drawing.Point(6, 22);
            this.btnWakeupIm.Name = "btnWakeupIm";
            this.btnWakeupIm.Size = new System.Drawing.Size(105, 28);
            this.btnWakeupIm.TabIndex = 35;
            this.btnWakeupIm.Text = "Sleep";
            this.btnWakeupIm.UseVisualStyleBackColor = true;
            this.btnWakeupIm.Visible = false;
            this.btnWakeupIm.Click += new System.EventHandler(this.btnWakeupIm_Click);
            // 
            // btnWakeupBsm
            // 
            this.btnWakeupBsm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWakeupBsm.Location = new System.Drawing.Point(6, 22);
            this.btnWakeupBsm.Name = "btnWakeupBsm";
            this.btnWakeupBsm.Size = new System.Drawing.Size(105, 28);
            this.btnWakeupBsm.TabIndex = 37;
            this.btnWakeupBsm.Text = "Sleep";
            this.btnWakeupBsm.UseVisualStyleBackColor = true;
            this.btnWakeupBsm.Click += new System.EventHandler(this.btnWakeupBsm_Click);
            // 
            // btnStartSession
            // 
            this.btnStartSession.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartSession.Location = new System.Drawing.Point(6, 52);
            this.btnStartSession.Name = "btnStartSession";
            this.btnStartSession.Size = new System.Drawing.Size(105, 28);
            this.btnStartSession.TabIndex = 31;
            this.btnStartSession.Text = "Start Session";
            this.btnStartSession.UseVisualStyleBackColor = true;
            this.btnStartSession.Click += new System.EventHandler(this.btnStartSession_Click);
            // 
            // btnSendEmer
            // 
            this.btnSendEmer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSendEmer.Location = new System.Drawing.Point(6, 52);
            this.btnSendEmer.Name = "btnSendEmer";
            this.btnSendEmer.Size = new System.Drawing.Size(105, 28);
            this.btnSendEmer.TabIndex = 34;
            this.btnSendEmer.Text = "Send Emergency";
            this.btnSendEmer.UseVisualStyleBackColor = true;
            this.btnSendEmer.Visible = false;
            this.btnSendEmer.Click += new System.EventHandler(this.btnSendEmer_Click);
            // 
            // grpStat400Stat
            // 
            this.grpStat400Stat.Controls.Add(this.txtStatOpState);
            this.grpStat400Stat.Controls.Add(this.txtStatCompanyId);
            this.grpStat400Stat.Controls.Add(this.label139);
            this.grpStat400Stat.Controls.Add(this.label52);
            this.grpStat400Stat.Controls.Add(this.txtStatMaxBlocksPerPack);
            this.grpStat400Stat.Controls.Add(this.txtStatBytesPerBlock);
            this.grpStat400Stat.Controls.Add(this.txtStatImdTid);
            this.grpStat400Stat.Controls.Add(this.txtStatTxMod);
            this.grpStat400Stat.Controls.Add(this.txtStatChan);
            this.grpStat400Stat.Controls.Add(this.txtStatRxMod);
            this.grpStat400Stat.Controls.Add(this.label1);
            this.grpStat400Stat.Controls.Add(this.label13);
            this.grpStat400Stat.Controls.Add(this.label14);
            this.grpStat400Stat.Controls.Add(this.label16);
            this.grpStat400Stat.Controls.Add(this.label18);
            this.grpStat400Stat.Controls.Add(this.label17);
            this.grpStat400Stat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpStat400Stat.Location = new System.Drawing.Point(229, 16);
            this.grpStat400Stat.Name = "grpStat400Stat";
            this.grpStat400Stat.Size = new System.Drawing.Size(216, 218);
            this.grpStat400Stat.TabIndex = 37;
            this.grpStat400Stat.TabStop = false;
            this.grpStat400Stat.Text = "400 MHz Link Status";
            // 
            // txtStatOpState
            // 
            this.txtStatOpState.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStatOpState.Location = new System.Drawing.Point(82, 189);
            this.txtStatOpState.Name = "txtStatOpState";
            this.txtStatOpState.Size = new System.Drawing.Size(126, 20);
            this.txtStatOpState.TabIndex = 37;
            this.txtStatOpState.Text = "****";
            this.txtStatOpState.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // txtStatCompanyId
            // 
            this.txtStatCompanyId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStatCompanyId.Location = new System.Drawing.Point(141, 16);
            this.txtStatCompanyId.Name = "txtStatCompanyId";
            this.txtStatCompanyId.Size = new System.Drawing.Size(67, 20);
            this.txtStatCompanyId.TabIndex = 40;
            this.txtStatCompanyId.Text = "****";
            this.txtStatCompanyId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label139.Location = new System.Drawing.Point(12, 20);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(65, 13);
            this.label139.TabIndex = 39;
            this.label139.Text = "Company ID";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(12, 185);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(61, 26);
            this.label52.TabIndex = 38;
            this.label52.Text = "Operational\r\nState";
            this.label52.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txtStatMaxBlocksPerPack
            // 
            this.txtStatMaxBlocksPerPack.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStatMaxBlocksPerPack.Location = new System.Drawing.Point(141, 160);
            this.txtStatMaxBlocksPerPack.Name = "txtStatMaxBlocksPerPack";
            this.txtStatMaxBlocksPerPack.Size = new System.Drawing.Size(67, 20);
            this.txtStatMaxBlocksPerPack.TabIndex = 36;
            this.txtStatMaxBlocksPerPack.Text = "****";
            this.txtStatMaxBlocksPerPack.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // txtStatBytesPerBlock
            // 
            this.txtStatBytesPerBlock.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStatBytesPerBlock.Location = new System.Drawing.Point(141, 136);
            this.txtStatBytesPerBlock.Name = "txtStatBytesPerBlock";
            this.txtStatBytesPerBlock.Size = new System.Drawing.Size(67, 20);
            this.txtStatBytesPerBlock.TabIndex = 35;
            this.txtStatBytesPerBlock.Text = "****";
            this.txtStatBytesPerBlock.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // txtStatImdTid
            // 
            this.txtStatImdTid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStatImdTid.Location = new System.Drawing.Point(141, 40);
            this.txtStatImdTid.Name = "txtStatImdTid";
            this.txtStatImdTid.Size = new System.Drawing.Size(67, 20);
            this.txtStatImdTid.TabIndex = 31;
            this.txtStatImdTid.Text = "****";
            this.txtStatImdTid.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // txtStatTxMod
            // 
            this.txtStatTxMod.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStatTxMod.Location = new System.Drawing.Point(141, 64);
            this.txtStatTxMod.Name = "txtStatTxMod";
            this.txtStatTxMod.Size = new System.Drawing.Size(67, 20);
            this.txtStatTxMod.TabIndex = 32;
            this.txtStatTxMod.Text = "****";
            this.txtStatTxMod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // txtStatChan
            // 
            this.txtStatChan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStatChan.Location = new System.Drawing.Point(141, 112);
            this.txtStatChan.Name = "txtStatChan";
            this.txtStatChan.Size = new System.Drawing.Size(67, 20);
            this.txtStatChan.TabIndex = 34;
            this.txtStatChan.Text = "****";
            this.txtStatChan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // txtStatRxMod
            // 
            this.txtStatRxMod.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStatRxMod.Location = new System.Drawing.Point(141, 88);
            this.txtStatRxMod.Name = "txtStatRxMod";
            this.txtStatRxMod.Size = new System.Drawing.Size(67, 20);
            this.txtStatRxMod.TabIndex = 33;
            this.txtStatRxMod.Text = "****";
            this.txtStatRxMod.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 163);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "Max. Blocks/Packet";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(12, 140);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "Bytes/Block";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(12, 116);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(46, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "Channel";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(12, 68);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(76, 13);
            this.label16.TabIndex = 25;
            this.label16.Text = "TX Modulation";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(12, 44);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(100, 13);
            this.label18.TabIndex = 24;
            this.label18.Text = "IMD Transceiver ID";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(12, 92);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(77, 13);
            this.label17.TabIndex = 26;
            this.label17.Text = "RX Modulation";
            // 
            // grpBsmStat
            // 
            this.grpBsmStat.Controls.Add(this.btnBsmLinkQual);
            this.grpBsmStat.Controls.Add(this.btnBsmRx400);
            this.grpBsmStat.Controls.Add(this.btnBsmTx245);
            this.grpBsmStat.Controls.Add(this.btnBsmTx400);
            this.grpBsmStat.Controls.Add(this.lblBsmTx245);
            this.grpBsmStat.Controls.Add(this.lblBsmRx400);
            this.grpBsmStat.Controls.Add(this.pbrBsmLinkQual);
            this.grpBsmStat.Controls.Add(this.label19);
            this.grpBsmStat.Controls.Add(this.btnBsmRegs);
            this.grpBsmStat.Controls.Add(this.lblBsmTx400);
            this.grpBsmStat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBsmStat.Location = new System.Drawing.Point(464, 16);
            this.grpBsmStat.Name = "grpBsmStat";
            this.grpBsmStat.Size = new System.Drawing.Size(240, 108);
            this.grpBsmStat.TabIndex = 41;
            this.grpBsmStat.TabStop = false;
            this.grpBsmStat.Text = "Base Station Status + Control (BSM)";
            // 
            // btnBsmLinkQual
            // 
            this.btnBsmLinkQual.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBsmLinkQual.Location = new System.Drawing.Point(126, 64);
            this.btnBsmLinkQual.Name = "btnBsmLinkQual";
            this.btnBsmLinkQual.Size = new System.Drawing.Size(69, 35);
            this.btnBsmLinkQual.TabIndex = 107;
            this.btnBsmLinkQual.Text = "Link Quality Statistics";
            this.btnBsmLinkQual.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBsmLinkQual.UseVisualStyleBackColor = true;
            this.btnBsmLinkQual.Click += new System.EventHandler(this.btnBsmLinkQual_Click);
            // 
            // btnBsmRx400
            // 
            this.btnBsmRx400.BackColor = System.Drawing.Color.White;
            this.btnBsmRx400.Location = new System.Drawing.Point(124, 15);
            this.btnBsmRx400.Name = "btnBsmRx400";
            this.btnBsmRx400.Size = new System.Drawing.Size(18, 18);
            this.btnBsmRx400.TabIndex = 70;
            this.btnBsmRx400.UseVisualStyleBackColor = false;
            // 
            // btnBsmTx245
            // 
            this.btnBsmTx245.BackColor = System.Drawing.Color.White;
            this.btnBsmTx245.Location = new System.Drawing.Point(8, 38);
            this.btnBsmTx245.Name = "btnBsmTx245";
            this.btnBsmTx245.Size = new System.Drawing.Size(18, 18);
            this.btnBsmTx245.TabIndex = 69;
            this.btnBsmTx245.UseVisualStyleBackColor = false;
            // 
            // btnBsmTx400
            // 
            this.btnBsmTx400.BackColor = System.Drawing.Color.White;
            this.btnBsmTx400.Location = new System.Drawing.Point(8, 15);
            this.btnBsmTx400.Name = "btnBsmTx400";
            this.btnBsmTx400.Size = new System.Drawing.Size(18, 18);
            this.btnBsmTx400.TabIndex = 68;
            this.btnBsmTx400.UseVisualStyleBackColor = false;
            // 
            // lblBsmTx245
            // 
            this.lblBsmTx245.AutoSize = true;
            this.lblBsmTx245.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBsmTx245.Location = new System.Drawing.Point(24, 41);
            this.lblBsmTx245.Name = "lblBsmTx245";
            this.lblBsmTx245.Size = new System.Drawing.Size(95, 13);
            this.lblBsmTx245.TabIndex = 67;
            this.lblBsmTx245.Text = "2.45 GHz TX Data";
            // 
            // lblBsmRx400
            // 
            this.lblBsmRx400.AutoSize = true;
            this.lblBsmRx400.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBsmRx400.Location = new System.Drawing.Point(141, 18);
            this.lblBsmRx400.Name = "lblBsmRx400";
            this.lblBsmRx400.Size = new System.Drawing.Size(94, 13);
            this.lblBsmRx400.TabIndex = 65;
            this.lblBsmRx400.Text = "400 MHz RX Data";
            // 
            // pbrBsmLinkQual
            // 
            this.pbrBsmLinkQual.Enabled = false;
            this.pbrBsmLinkQual.Location = new System.Drawing.Point(125, 38);
            this.pbrBsmLinkQual.Name = "pbrBsmLinkQual";
            this.pbrBsmLinkQual.Size = new System.Drawing.Size(70, 19);
            this.pbrBsmLinkQual.Step = 25;
            this.pbrBsmLinkQual.TabIndex = 61;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Enabled = false;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(194, 41);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(30, 13);
            this.label19.TabIndex = 37;
            this.label19.Text = "QOS";
            // 
            // btnBsmRegs
            // 
            this.btnBsmRegs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBsmRegs.Location = new System.Drawing.Point(8, 64);
            this.btnBsmRegs.Name = "btnBsmRegs";
            this.btnBsmRegs.Size = new System.Drawing.Size(69, 35);
            this.btnBsmRegs.TabIndex = 34;
            this.btnBsmRegs.Text = "ZL70103 Registers";
            this.btnBsmRegs.UseVisualStyleBackColor = true;
            this.btnBsmRegs.Click += new System.EventHandler(this.btnBsmRegs_Click);
            // 
            // lblBsmTx400
            // 
            this.lblBsmTx400.AutoSize = true;
            this.lblBsmTx400.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBsmTx400.Location = new System.Drawing.Point(24, 18);
            this.lblBsmTx400.Name = "lblBsmTx400";
            this.lblBsmTx400.Size = new System.Drawing.Size(93, 13);
            this.lblBsmTx400.TabIndex = 63;
            this.lblBsmTx400.Text = "400 MHz TX Data";
            // 
            // grpImCommandInterface
            // 
            this.grpImCommandInterface.Controls.Add(this.cmbImConnection);
            this.grpImCommandInterface.Location = new System.Drawing.Point(464, 130);
            this.grpImCommandInterface.Name = "grpImCommandInterface";
            this.grpImCommandInterface.Size = new System.Drawing.Size(239, 55);
            this.grpImCommandInterface.TabIndex = 110;
            this.grpImCommandInterface.TabStop = false;
            this.grpImCommandInterface.Text = "PC -> Implant Command Interface";
            // 
            // cmbImConnection
            // 
            this.cmbImConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbImConnection.FormattingEnabled = true;
            this.cmbImConnection.Items.AddRange(new object[] {
            "None",
            "Local (USB)",
            "ZL7010X HK User Data (RF)",
            "ZL7010X HK User Status (RF)",
            "ZL7010X Data Blocks (RF)"});
            this.cmbImConnection.Location = new System.Drawing.Point(6, 21);
            this.cmbImConnection.Name = "cmbImConnection";
            this.cmbImConnection.Size = new System.Drawing.Size(219, 21);
            this.cmbImConnection.TabIndex = 108;
            this.cmbImConnection.SelectedIndexChanged += new System.EventHandler(this.cmbImConnection_SelectedIndexChanged);
            // 
            // grpImStat
            // 
            this.grpImStat.Controls.Add(this.btnImLinkQual);
            this.grpImStat.Controls.Add(this.chbVsupTrack);
            this.grpImStat.Controls.Add(this.lblVsupSetting);
            this.grpImStat.Controls.Add(this.lblVsupMeasured);
            this.grpImStat.Controls.Add(this.udSetVsup2);
            this.grpImStat.Controls.Add(this.udSetVsup1);
            this.grpImStat.Controls.Add(this.btnImRx400);
            this.grpImStat.Controls.Add(this.btnImTx400);
            this.grpImStat.Controls.Add(this.lblImTx400);
            this.grpImStat.Controls.Add(this.lblImVsup1);
            this.grpImStat.Controls.Add(this.txtImVsup1);
            this.grpImStat.Controls.Add(this.txtImVsup2);
            this.grpImStat.Controls.Add(this.btnImRegs);
            this.grpImStat.Controls.Add(this.btnImPowMon);
            this.grpImStat.Controls.Add(this.lblImVsup2);
            this.grpImStat.Controls.Add(this.lblImRx400);
            this.grpImStat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpImStat.Location = new System.Drawing.Point(464, 189);
            this.grpImStat.Name = "grpImStat";
            this.grpImStat.Size = new System.Drawing.Size(241, 152);
            this.grpImStat.TabIndex = 42;
            this.grpImStat.TabStop = false;
            this.grpImStat.Text = "Implant Status + Control (AIM)";
            // 
            // btnImLinkQual
            // 
            this.btnImLinkQual.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImLinkQual.Location = new System.Drawing.Point(82, 111);
            this.btnImLinkQual.Name = "btnImLinkQual";
            this.btnImLinkQual.Size = new System.Drawing.Size(69, 35);
            this.btnImLinkQual.TabIndex = 110;
            this.btnImLinkQual.Text = "Link Quality Statistics";
            this.btnImLinkQual.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnImLinkQual.UseVisualStyleBackColor = true;
            this.btnImLinkQual.Click += new System.EventHandler(this.btnImLinkQual_Click);
            // 
            // chbVsupTrack
            // 
            this.chbVsupTrack.AutoSize = true;
            this.chbVsupTrack.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbVsupTrack.Location = new System.Drawing.Point(156, 83);
            this.chbVsupTrack.Name = "chbVsupTrack";
            this.chbVsupTrack.Size = new System.Drawing.Size(54, 17);
            this.chbVsupTrack.TabIndex = 81;
            this.chbVsupTrack.Text = "Track";
            this.chbVsupTrack.UseVisualStyleBackColor = true;
            this.chbVsupTrack.CheckedChanged += new System.EventHandler(this.chbVsupTrack_CheckedChanged);
            // 
            // lblVsupSetting
            // 
            this.lblVsupSetting.AutoSize = true;
            this.lblVsupSetting.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVsupSetting.Location = new System.Drawing.Point(46, 39);
            this.lblVsupSetting.Name = "lblVsupSetting";
            this.lblVsupSetting.Size = new System.Drawing.Size(40, 13);
            this.lblVsupSetting.TabIndex = 80;
            this.lblVsupSetting.Text = "Setting";
            // 
            // lblVsupMeasured
            // 
            this.lblVsupMeasured.AutoSize = true;
            this.lblVsupMeasured.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVsupMeasured.Location = new System.Drawing.Point(92, 39);
            this.lblVsupMeasured.Name = "lblVsupMeasured";
            this.lblVsupMeasured.Size = new System.Drawing.Size(54, 13);
            this.lblVsupMeasured.TabIndex = 79;
            this.lblVsupMeasured.Text = "Measured";
            // 
            // udSetVsup2
            // 
            this.udSetVsup2.DecimalPlaces = 3;
            this.udSetVsup2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.udSetVsup2.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.udSetVsup2.Location = new System.Drawing.Point(45, 80);
            this.udSetVsup2.Maximum = new decimal(new int[] {
            35,
            0,
            0,
            65536});
            this.udSetVsup2.Minimum = new decimal(new int[] {
            18,
            0,
            0,
            65536});
            this.udSetVsup2.Name = "udSetVsup2";
            this.udSetVsup2.Size = new System.Drawing.Size(48, 20);
            this.udSetVsup2.TabIndex = 78;
            this.udSetVsup2.Value = new decimal(new int[] {
            33,
            0,
            0,
            65536});
            this.udSetVsup2.ValueChanged += new System.EventHandler(this.udSetVsup2_ValueChanged);
            // 
            // udSetVsup1
            // 
            this.udSetVsup1.DecimalPlaces = 3;
            this.udSetVsup1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.udSetVsup1.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.udSetVsup1.Location = new System.Drawing.Point(45, 55);
            this.udSetVsup1.Maximum = new decimal(new int[] {
            35,
            0,
            0,
            65536});
            this.udSetVsup1.Minimum = new decimal(new int[] {
            205,
            0,
            0,
            131072});
            this.udSetVsup1.Name = "udSetVsup1";
            this.udSetVsup1.Size = new System.Drawing.Size(48, 20);
            this.udSetVsup1.TabIndex = 77;
            this.udSetVsup1.Value = new decimal(new int[] {
            33,
            0,
            0,
            65536});
            this.udSetVsup1.ValueChanged += new System.EventHandler(this.udSetVsup1_ValueChanged);
            // 
            // btnImRx400
            // 
            this.btnImRx400.BackColor = System.Drawing.Color.White;
            this.btnImRx400.Location = new System.Drawing.Point(123, 16);
            this.btnImRx400.Name = "btnImRx400";
            this.btnImRx400.Size = new System.Drawing.Size(18, 18);
            this.btnImRx400.TabIndex = 76;
            this.btnImRx400.UseVisualStyleBackColor = false;
            // 
            // btnImTx400
            // 
            this.btnImTx400.BackColor = System.Drawing.Color.White;
            this.btnImTx400.Location = new System.Drawing.Point(6, 15);
            this.btnImTx400.Name = "btnImTx400";
            this.btnImTx400.Size = new System.Drawing.Size(18, 18);
            this.btnImTx400.TabIndex = 75;
            this.btnImTx400.UseVisualStyleBackColor = false;
            // 
            // lblImTx400
            // 
            this.lblImTx400.AutoSize = true;
            this.lblImTx400.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImTx400.Location = new System.Drawing.Point(24, 19);
            this.lblImTx400.Name = "lblImTx400";
            this.lblImTx400.Size = new System.Drawing.Size(93, 13);
            this.lblImTx400.TabIndex = 72;
            this.lblImTx400.Text = "400 MHz TX Data";
            // 
            // lblImVsup1
            // 
            this.lblImVsup1.AutoSize = true;
            this.lblImVsup1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImVsup1.Location = new System.Drawing.Point(5, 58);
            this.lblImVsup1.Name = "lblImVsup1";
            this.lblImVsup1.Size = new System.Drawing.Size(40, 13);
            this.lblImVsup1.TabIndex = 66;
            this.lblImVsup1.Text = "Vsup 1";
            // 
            // txtImVsup1
            // 
            this.txtImVsup1.BackColor = System.Drawing.SystemColors.Window;
            this.txtImVsup1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImVsup1.Location = new System.Drawing.Point(97, 55);
            this.txtImVsup1.Name = "txtImVsup1";
            this.txtImVsup1.ReadOnly = true;
            this.txtImVsup1.Size = new System.Drawing.Size(44, 20);
            this.txtImVsup1.TabIndex = 65;
            this.txtImVsup1.Text = "****";
            this.txtImVsup1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // txtImVsup2
            // 
            this.txtImVsup2.BackColor = System.Drawing.SystemColors.Window;
            this.txtImVsup2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImVsup2.Location = new System.Drawing.Point(97, 79);
            this.txtImVsup2.Name = "txtImVsup2";
            this.txtImVsup2.ReadOnly = true;
            this.txtImVsup2.Size = new System.Drawing.Size(44, 20);
            this.txtImVsup2.TabIndex = 69;
            this.txtImVsup2.Text = "****";
            this.txtImVsup2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // btnImRegs
            // 
            this.btnImRegs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImRegs.Location = new System.Drawing.Point(7, 111);
            this.btnImRegs.Name = "btnImRegs";
            this.btnImRegs.Size = new System.Drawing.Size(69, 35);
            this.btnImRegs.TabIndex = 36;
            this.btnImRegs.Text = "ZL70103 Registers";
            this.btnImRegs.UseVisualStyleBackColor = true;
            this.btnImRegs.Click += new System.EventHandler(this.btnImRegs_Click);
            // 
            // btnImPowMon
            // 
            this.btnImPowMon.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImPowMon.Location = new System.Drawing.Point(156, 111);
            this.btnImPowMon.Name = "btnImPowMon";
            this.btnImPowMon.Size = new System.Drawing.Size(69, 35);
            this.btnImPowMon.TabIndex = 37;
            this.btnImPowMon.Text = "Power Monitor";
            this.btnImPowMon.UseVisualStyleBackColor = true;
            this.btnImPowMon.Click += new System.EventHandler(this.btnImPowMon_Click);
            // 
            // lblImVsup2
            // 
            this.lblImVsup2.AutoSize = true;
            this.lblImVsup2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImVsup2.Location = new System.Drawing.Point(5, 83);
            this.lblImVsup2.Name = "lblImVsup2";
            this.lblImVsup2.Size = new System.Drawing.Size(40, 13);
            this.lblImVsup2.TabIndex = 70;
            this.lblImVsup2.Text = "Vsup 2";
            // 
            // lblImRx400
            // 
            this.lblImRx400.AutoSize = true;
            this.lblImRx400.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImRx400.Location = new System.Drawing.Point(141, 19);
            this.lblImRx400.Name = "lblImRx400";
            this.lblImRx400.Size = new System.Drawing.Size(94, 13);
            this.lblImRx400.TabIndex = 74;
            this.lblImRx400.Text = "400 MHz RX Data";
            // 
            // tmrStatPoll
            // 
            this.tmrStatPoll.Interval = 1000;
            this.tmrStatPoll.Tick += new System.EventHandler(this.tmrStatPoll_Tick);
            // 
            // tmrBackgroundTasks
            // 
            this.tmrBackgroundTasks.Tick += new System.EventHandler(this.tmrBackgroundTasks_Tick);
            // 
            // AnyModForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(713, 746);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AnyModForm";
            this.Text = "ZL70103 Base Station (BSM300)";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AnyModForm_FormClosing);
            this.Load += new System.EventHandler(this.AnyModForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.bsmTabs.ResumeLayout(false);
            this.tabLinkSetup.ResumeLayout(false);
            this.grpImInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvImInfo)).EndInit();
            this.grpLinkSetup.ResumeLayout(false);
            this.grpLinkSetup.PerformLayout();
            this.tabCcaOrRssiAndCal.ResumeLayout(false);
            this.grpCal.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.grpHighLevelTuning.ResumeLayout(false);
            this.grpHighLevelTuning.PerformLayout();
            this.grpCcaOrRssi.ResumeLayout(false);
            this.grpCcaOrRssi.PerformLayout();
            this.tabData.ResumeLayout(false);
            this.grpReceiveData.ResumeLayout(false);
            this.grpReceiveData.PerformLayout();
            this.grpReceiveDataType.ResumeLayout(false);
            this.grpReceiveDataType.PerformLayout();
            this.grpTransmitData.ResumeLayout(false);
            this.grpTransmitData.PerformLayout();
            this.grpTransmitDataType.ResumeLayout(false);
            this.grpTransmitDataType.PerformLayout();
            this.tabTest.ResumeLayout(false);
            this.grpTestRx.ResumeLayout(false);
            this.grpTestRx.PerformLayout();
            this.grpTestTx.ResumeLayout(false);
            this.grpTestTx.PerformLayout();
            this.tabDataTest.ResumeLayout(false);
            this.grpDataTest.ResumeLayout(false);
            this.grpDataTestIm.ResumeLayout(false);
            this.grpDtStatIm.ResumeLayout(false);
            this.grpDtStatIm.PerformLayout();
            this.grpDtConfigIm.ResumeLayout(false);
            this.grpDtConfigIm.PerformLayout();
            this.grpDataTestBsm.ResumeLayout(false);
            this.grpDtConfigBsm.ResumeLayout(false);
            this.grpDtConfigBsm.PerformLayout();
            this.grpDtStatBsm.ResumeLayout(false);
            this.grpDtStatBsm.PerformLayout();
            this.tabRemote.ResumeLayout(false);
            this.groupBox24.ResumeLayout(false);
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            this.grpRemoteCals.ResumeLayout(false);
            this.grpRemHighLevelTuning.ResumeLayout(false);
            this.grpRemHighLevelTuning.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabFactoryUse.ResumeLayout(false);
            this.tabFactoryUse.PerformLayout();
            this.grp245Cal.ResumeLayout(false);
            this.grp245Cal.PerformLayout();
            this.grpCcReg.ResumeLayout(false);
            this.grpCcReg.PerformLayout();
            this.grpBerSetup.ResumeLayout(false);
            this.grpBerSetup.PerformLayout();
            this.grpBerDataRate.ResumeLayout(false);
            this.grpBerDataRate.PerformLayout();
            this.grpStat.ResumeLayout(false);
            this.grpImStatOnBsm.ResumeLayout(false);
            this.grpRxImdTidList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRxImdTidList)).EndInit();
            this.grpWakeupMode.ResumeLayout(false);
            this.grpWakeupMode.PerformLayout();
            this.grpSysMsg.ResumeLayout(false);
            this.grpSysMsg.PerformLayout();
            this.grpStatSession.ResumeLayout(false);
            this.grpStatSession.PerformLayout();
            this.grpStat400Stat.ResumeLayout(false);
            this.grpStat400Stat.PerformLayout();
            this.grpBsmStat.ResumeLayout(false);
            this.grpBsmStat.PerformLayout();
            this.grpImCommandInterface.ResumeLayout(false);
            this.grpImStat.ResumeLayout(false);
            this.grpImStat.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udSetVsup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udSetVsup1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox grpWakeupMode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtStatImdTid;
        private System.Windows.Forms.TextBox txtStatTxMod;
        private System.Windows.Forms.TextBox txtStatMaxBlocksPerPack;
        private System.Windows.Forms.TextBox txtStatBytesPerBlock;
        private System.Windows.Forms.TextBox txtStatChan;
        private System.Windows.Forms.TextBox txtStatRxMod;
        private System.Windows.Forms.Button btnStartSession;
        private System.Windows.Forms.GroupBox grpStat400Stat;
        private System.Windows.Forms.GroupBox grpStat;
        private System.Windows.Forms.GroupBox grpBsmStat;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox grpImStat;
        private System.Windows.Forms.Button btnStartImSearch;
        private System.Windows.Forms.GroupBox grpStatSession;
        private System.Windows.Forms.Button btnBsmRegs;
        private System.Windows.Forms.TabControl bsmTabs;
        private System.Windows.Forms.TabPage tabLinkSetup;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox cmbWake245Country;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox grpLinkSetup;
        private System.Windows.Forms.TextBox txtImdTid6;
        private System.Windows.Forms.TextBox txtImdTid5;
        private System.Windows.Forms.TextBox txtImdTid4;
        private System.Windows.Forms.TextBox txtImdTid3;
        private System.Windows.Forms.ComboBox cmbEmerChan;
        private System.Windows.Forms.TextBox txtImdTid2;
        private System.Windows.Forms.ComboBox cmbEmerRxMod;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbEmerTxMod;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtImdTid1;
        private System.Windows.Forms.ComboBox cmbNormMaxBlocksPerPack;
        private System.Windows.Forms.ComboBox cmbNormBytesPerBlock;
        private System.Windows.Forms.ComboBox cmbNormChan;
        private System.Windows.Forms.ComboBox cmbNormRxMod;
        private System.Windows.Forms.ComboBox cmbNormTxMod;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage tabCcaOrRssiAndCal;
        private System.Windows.Forms.TabPage tabData;
        private System.Windows.Forms.GroupBox grpReceiveData;
        private System.Windows.Forms.GroupBox grpReceiveDataType;
        private System.Windows.Forms.RadioButton radReceiveDataAscii;
        private System.Windows.Forms.RadioButton radReceiveDataHex;
        private System.Windows.Forms.Button btnReceiveDataClear;
        private System.Windows.Forms.GroupBox grpTransmitData;
        private System.Windows.Forms.GroupBox grpTransmitDataType;
        private System.Windows.Forms.RadioButton radTransmitDataAscii;
        private System.Windows.Forms.RadioButton radTransmitDataHex;
        private System.Windows.Forms.Button btnTransmitData;
        private System.Windows.Forms.TextBox txtTransmitData;
        private System.Windows.Forms.TabPage tabRemote;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Button button56;
        private System.Windows.Forms.ComboBox comboBox17;
        private System.Windows.Forms.CheckBox checkBox51;
        private System.Windows.Forms.TextBox textBox61;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.ComboBox comboBox16;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.ComboBox comboBox18;
        private System.Windows.Forms.CheckBox checkBox52;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Button button57;
        private System.Windows.Forms.TextBox textBox60;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.ComboBox comboBox11;
        private System.Windows.Forms.GroupBox grpRemoteCals;
        private System.Windows.Forms.RadioButton rad400MHzWakeup;
        private System.Windows.Forms.RadioButton rad245GHzWakeup;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox txtStatOpState;
        private System.Windows.Forms.DataGridView dgvRxImdTidList;
        private System.Windows.Forms.GroupBox grpRxImdTidList;
        private System.Windows.Forms.ProgressBar pbrBsmLinkQual;
        private System.Windows.Forms.Label lblImVsup1;
        private System.Windows.Forms.TextBox txtImVsup1;
        private System.Windows.Forms.TextBox txtImVsup2;
        private System.Windows.Forms.Button btnImRegs;
        private System.Windows.Forms.Button btnImPowMon;
        private System.Windows.Forms.Label lblImVsup2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtReceiveData;
        private System.Windows.Forms.CheckBox chbTransmitDataForever;
        private System.Windows.Forms.TabPage tabTest;
        private System.Windows.Forms.CheckBox chbAnyImSearch;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.TextBox txtStatCompanyId;
        private System.Windows.Forms.CheckBox chbAutoCca;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.Button btnStartListen;
        private System.Windows.Forms.CheckBox chbAutoListen;
        private System.Windows.Forms.GroupBox grpImInfo;
        private System.Windows.Forms.DataGridView dgvImInfo;
        private System.Windows.Forms.CheckBox chbStatPoll;
        private System.Windows.Forms.TextBox txtCompanyId2;
        private System.Windows.Forms.TextBox txtCompanyId1;
        private System.Windows.Forms.DataGridViewTextBoxColumn companyId;
        private System.Windows.Forms.DataGridViewTextBoxColumn imdTransId;
        private System.Windows.Forms.DataGridViewTextBoxColumn companyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn imdDescription;
        private System.Windows.Forms.Timer tmrStatPoll;
        private System.Windows.Forms.GroupBox grpCal;
        private System.Windows.Forms.GroupBox grpCcaOrRssi;
        private System.Windows.Forms.Button btnReadMaxOrAveRssi9;
        private System.Windows.Forms.Button btnReadMaxOrAveRssi8;
        private System.Windows.Forms.CheckBox chbContinuousCcaOrRssi;
        private System.Windows.Forms.Button btnReadMaxOrAveRssi7;
        private System.Windows.Forms.Button btnReadMaxOrAveRssi6;
        private System.Windows.Forms.Button btnPerformCcaOrRssi;
        private System.Windows.Forms.Button btnReadMaxOrAveRssi5;
        private System.Windows.Forms.Button btnReadMaxOrAveRssi4;
        private System.Windows.Forms.Button btnReadMaxOrAveRssi3;
        private System.Windows.Forms.Button btnReadMaxOrAveRssi2;
        private System.Windows.Forms.Button btnReadMaxOrAveRssi1;
        private System.Windows.Forms.Button btnReadMaxOrAveRssi0;
        private System.Windows.Forms.TextBox txtRssiDbm9;
        private System.Windows.Forms.TextBox txtRssi9;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtRssiDbm8;
        private System.Windows.Forms.TextBox txtRssi8;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txtRssiDbm7;
        private System.Windows.Forms.TextBox txtRssi7;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtRssiDbm6;
        private System.Windows.Forms.TextBox txtRssi6;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txtRssi5;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txtRssiDbm4;
        private System.Windows.Forms.TextBox txtRssi4;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtRssiDbm3;
        private System.Windows.Forms.TextBox txtRssi3;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtRssiDbm2;
        private System.Windows.Forms.TextBox txtRssi2;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtRssiDbm1;
        private System.Windows.Forms.TextBox txtRssi1;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtRssiDbm0;
        private System.Windows.Forms.TextBox txtRssi0;
        private System.Windows.Forms.Label lblExtOrIntAdc;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtRssiDbm5;
        private System.Windows.Forms.Button btnSendEmer;
        private System.Windows.Forms.Timer tmrBackgroundTasks;
        private System.Windows.Forms.Button btn24CrystOsc;
        private System.Windows.Forms.CheckBox chbUseAveRssiForCca;
        private System.Windows.Forms.Button btnWakeupIm;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.ComboBox cmbEmerMaxBlocksPerPack;
        private System.Windows.Forms.ComboBox cmbEmerBytesPerBlock;
        private System.Windows.Forms.TextBox txtSysMsg;
        private System.Windows.Forms.GroupBox grpSysMsg;
        private System.Windows.Forms.CheckBox chbEnabHkWrite;
        private System.Windows.Forms.CheckBox chbDataGat;
        private System.Windows.Forms.ComboBox cmbTransmitDataRate;
        private System.Windows.Forms.ComboBox cmbReceiveDataPollRate;
        private System.Windows.Forms.ComboBox cmbWake245UserData;
        private System.Windows.Forms.ComboBox cmbImplantType;
        private System.Windows.Forms.Label lblBsmTx400;
        private System.Windows.Forms.Label lblBsmRx400;
        private System.Windows.Forms.Label lblBsmTx245;
        private System.Windows.Forms.Label lblImRx400;
        private System.Windows.Forms.Label lblImTx400;
        private System.Windows.Forms.GroupBox grpTestTx;
        private System.Windows.Forms.ComboBox cmbTestTx245Pow;
        private System.Windows.Forms.Button btnTestTx400WritePow;
        private System.Windows.Forms.Button btnTestTx400ReadPow;
        private System.Windows.Forms.TextBox txtTestTx400Pow;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.ComboBox cmbTestTx245Chan;
        private System.Windows.Forms.Button btnTestTx245Enab;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Button btnTestTx400Enab;
        private System.Windows.Forms.ComboBox cmbTestTx400Chan;
        private System.Windows.Forms.GroupBox grpTestRx;
        private System.Windows.Forms.Button btnTestRx400WriteLna;
        private System.Windows.Forms.Button btnTestRx400ReadLna;
        private System.Windows.Forms.TextBox txtTestRx400Lna;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Button btnTestRx400Enab;
        private System.Windows.Forms.ComboBox cmbTestRx400Chan;
        private System.Windows.Forms.Button btnClearSysMsg;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.Button btnBsmRx400;
        private System.Windows.Forms.Button btnBsmTx245;
        private System.Windows.Forms.Button btnBsmTx400;
        private System.Windows.Forms.Button btnImRx400;
        private System.Windows.Forms.Button btnImTx400;
        private System.Windows.Forms.Button btnStoreImInfo;
        private System.Windows.Forms.Button btnLoadImInfo;
        private System.Windows.Forms.Button btnStartStream;
        private System.Windows.Forms.SaveFileDialog sfdStreamData;
        private System.Windows.Forms.NumericUpDown udSetVsup2;
        private System.Windows.Forms.NumericUpDown udSetVsup1;
        private System.Windows.Forms.Label lblVsupSetting;
        private System.Windows.Forms.Label lblVsupMeasured;
        private System.Windows.Forms.TabPage tabFactoryUse;
        private System.Windows.Forms.GroupBox grpBerSetup;
        private System.Windows.Forms.ComboBox cmbBerChan;
        private System.Windows.Forms.Button btnStartBer;
        private System.Windows.Forms.GroupBox grpBerDataRate;
        private System.Windows.Forms.RadioButton radBer800kbps;
        private System.Windows.Forms.RadioButton radBer400kbps;
        private System.Windows.Forms.RadioButton radBer200kbps;
        private System.Windows.Forms.Label lblBerChan;
        private System.Windows.Forms.CheckBox chbVsupTrack;
        private System.Windows.Forms.Button btnStartTraceView;
        private System.Windows.Forms.TabPage tabDataTest;
        private System.Windows.Forms.GroupBox grpDataTest;
        private System.Windows.Forms.GroupBox grpDataTestIm;
        private System.Windows.Forms.GroupBox grpDataTestBsm;
        private System.Windows.Forms.TextBox txtDtTxBlockCountBsm;
        private System.Windows.Forms.Label lblDtDataBsm;
        private System.Windows.Forms.TextBox txtDtDataBsm;
        private System.Windows.Forms.Button btnStartDataTest;
        private System.Windows.Forms.CheckBox chbDtIncDataBsm;
        private System.Windows.Forms.Label lblDtTxBlockCountBsm;
        private System.Windows.Forms.TextBox txtDtTotalRxByteErrsBsm;
        private System.Windows.Forms.Label lblDtTotalRxByteErrsBsm;
        private System.Windows.Forms.CheckBox chbDtValidateRxDataIm;
        private System.Windows.Forms.CheckBox chbDtRxIm;
        private System.Windows.Forms.CheckBox chbDtTxBsm;
        private System.Windows.Forms.GroupBox grpDtStatBsm;
        private System.Windows.Forms.GroupBox grpDtConfigBsm;
        private System.Windows.Forms.GroupBox grpDtConfigIm;
        private System.Windows.Forms.TextBox txtDtTxBlockCountIm;
        private System.Windows.Forms.CheckBox chbDtTxIm;
        private System.Windows.Forms.CheckBox chbDtRxBsm;
        private System.Windows.Forms.Label lblDtTxBlockCountIm;
        private System.Windows.Forms.CheckBox chbDtValidateRxDataBsm;
        private System.Windows.Forms.GroupBox grpDtStatIm;
        private System.Windows.Forms.TextBox txtDtTotalRxByteErrsIm;
        private System.Windows.Forms.Label lblDtTotalRxByteErrsIm;
        private System.Windows.Forms.CheckBox chbDtIncDataIm;
        private System.Windows.Forms.TextBox txtDtDataIm;
        private System.Windows.Forms.Label lblDtDataIm;
        private System.Windows.Forms.Button btnBsmLinkQual;
        private System.Windows.Forms.ComboBox cmbImConnection;
        private System.Windows.Forms.Button btnImLinkQual;
        private System.Windows.Forms.Button btn245Cal;
        private System.Windows.Forms.GroupBox grpImCommandInterface;
        private System.Windows.Forms.GroupBox grpHighLevelTuning;
        private System.Windows.Forms.TextBox txtHighLevelCalChan9;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox txtHighLevelCalChan0;
        private System.Windows.Forms.TextBox txtHighLevelCalChan1;
        private System.Windows.Forms.TextBox txtHighLevelCalChan2;
        private System.Windows.Forms.TextBox txtHighLevelCalChan3;
        private System.Windows.Forms.TextBox txtHighLevelCalChan4;
        private System.Windows.Forms.TextBox txtHighLevelCalChan5;
        private System.Windows.Forms.TextBox txtHighLevelCalChan6;
        private System.Windows.Forms.TextBox txtHighLevelCalChan7;
        private System.Windows.Forms.TextBox txtHighLevelCalChan8;
        private System.Windows.Forms.CheckBox chbHighLevelCalExtAlg;
        private System.Windows.Forms.CheckBox chbHighLevelCalRx;
        private System.Windows.Forms.CheckBox chbHighLevelCalTx;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnCalReq;
        private System.Windows.Forms.Label lblCalReq;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCalReqRem;
        private System.Windows.Forms.Label lblCalReqRem;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox chbTxTuneCap;
        private System.Windows.Forms.CheckBox chbMatch2TuneCap;
        private System.Windows.Forms.CheckBox chbMatch1TuneCap;
        private System.Windows.Forms.Button btnHighLevelCal;
        private System.Windows.Forms.GroupBox grpRemHighLevelTuning;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label lblLnaNegTrimBackoff;
        private System.Windows.Forms.TextBox txtLnaNegTrimBackoff;
        private System.Windows.Forms.CheckBox chbExtStrobeFor245Sniff;
        private System.Windows.Forms.Button btnCcRegWrite;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.TextBox txtCcRegVal;
        private System.Windows.Forms.TextBox txtCcRegAddr;
        private System.Windows.Forms.Button btnCcRegRead;
        private System.Windows.Forms.GroupBox grpCcReg;
        private System.Windows.Forms.Button btnNonvolatileMemory;
        private System.Windows.Forms.CheckBox chbResetMicsOnWakeup;
        private System.Windows.Forms.Button btnWakeupBsm;
        private System.Windows.Forms.Label lblRssiDbm;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button btnCcPaWrite;
        private System.Windows.Forms.Button btnCcPaRead;
        private System.Windows.Forms.Label lblCcPa;
        private System.Windows.Forms.TextBox txtCcPa;
        private System.Windows.Forms.CheckBox chbUseIntRssiOnBsm;
        private System.Windows.Forms.Button btnShowRssiToDbmCalsFromNvmOnCcaTab;
        private System.Windows.Forms.GroupBox grp245Cal;
        private System.Windows.Forms.GroupBox grpImStatOnBsm;
        private System.Windows.Forms.Button btnImRegsOnBsm;
    }
}
