//
// This file contains the public class AdkForm, which provides the main GUI
// for the ZL7010X ADK. This provides an interface to start the base station
// and implant GUIs.
//
// Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2012. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.

using System;
using System.Windows.Forms;

namespace Zarlink.Adk.Forms
{
    public partial class AdkForm : Form
    {
        public AnyModForm bsmForm = null;
        public AnyModForm imForm = null;

        public AdkForm()
        {
            InitializeComponent();

            cmbCcaRate.SelectedIndex = 1; 
            cmbStatPollRate.SelectedIndex = 1;
            cmbImResponsePollRate.SelectedIndex = 3;
            grpSysConfig.Visible = false;
        }

        private void bsm_FormClosed(object sender, EventArgs e)
        {
            bsmForm = null;
        }

        private void btnBsmDev_Click(object sender, EventArgs e)
        {
            if (bsmForm == null) {
            
                bsmForm = new AnyModForm(false, this);
                bsmForm.FormClosed += new System.Windows.Forms.FormClosedEventHandler(bsm_FormClosed);
                setAllRates();
                bsmForm.Show();
                
            } else {
                bsmForm.Focus();
            }
        }

        private void im_FormClosed(object sender, EventArgs e)
        {
            imForm = null;
        }

        private void btnImDev_Click(object sender, EventArgs e)
        {
            if (imForm == null) {
            
                imForm = new AnyModForm(true, this);
                imForm.FormClosed += new System.Windows.Forms.FormClosedEventHandler(im_FormClosed);
                setAllRates();
                imForm.Show();
                
            } else {
                imForm.Focus();
            }
        }
        
        private void setAllRates()
        {
            setStatPollRate(cmbStatPollRate.SelectedIndex);
            setImResponsePollRate(cmbImResponsePollRate.SelectedIndex);  
            setCcaRate(cmbCcaRate.SelectedIndex);
        }

        private void cmbStatPollRate_SelectedIndexChanged(object sender, EventArgs e)
        {
             ComboBox p = (ComboBox)sender;
             setStatPollRate(p.SelectedIndex);
        }
        private void setStatPollRate(int selectedIndex)
        {
             if (bsmForm != null) bsmForm.setStatPollRate(selectedIndex);
             if (imForm != null) imForm.setStatPollRate(selectedIndex);
        }

        private void cmbCcaRate_SelectedIndexChanged(object sender, EventArgs e)
        {
             ComboBox p = (ComboBox)sender;
             setCcaRate(p.SelectedIndex);
        }
        private void setCcaRate(int selectedIndex)
        {
            if (bsmForm != null) bsmForm.setCcaRate(selectedIndex);
            if (imForm != null) imForm.setCcaRate(selectedIndex);
        }

        private void cmbImResponsePollRate_SelectedIndexChanged(object sender, EventArgs e)
        {
             ComboBox p = (ComboBox)sender;
             setImResponsePollRate(p.SelectedIndex);
        }
        private void setImResponsePollRate(int selectedIndex)
        {
            if (bsmForm != null) bsmForm.setImResponsePollRate(selectedIndex);
            if (imForm != null) imForm.setImResponsePollRate(selectedIndex);
        }

        private void ignoreEntry_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void btnSysSet_Click(object sender, EventArgs e)
        {
            if (grpSysConfig.Visible == false) {
                grpSysConfig.Visible = true;
            } else {
                grpSysConfig.Visible = false;
            }
        }
    }
}