//
// Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2012. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.

namespace Zarlink.Adk.Forms
{
    partial class AdkForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdkForm));
            this.btnBsmForm = new System.Windows.Forms.Button();
            this.btnImForm = new System.Windows.Forms.Button();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.hlp = new System.Windows.Forms.HelpProvider();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.cmbStatPollRate = new System.Windows.Forms.ComboBox();
            this.cmbCcaRate = new System.Windows.Forms.ComboBox();
            this.cmbImResponsePollRate = new System.Windows.Forms.ComboBox();
            this.grpSysConfig = new System.Windows.Forms.GroupBox();
            this.btnSysSet = new System.Windows.Forms.Button();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.grpSysConfig.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // btnBsmForm
            // 
            this.btnBsmForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBsmForm.Location = new System.Drawing.Point(106, 193);
            this.btnBsmForm.Margin = new System.Windows.Forms.Padding(2);
            this.btnBsmForm.Name = "btnBsmForm";
            this.btnBsmForm.Size = new System.Drawing.Size(109, 60);
            this.btnBsmForm.TabIndex = 0;
            this.btnBsmForm.Text = "Base Station (BSMX00)";
            this.btnBsmForm.UseVisualStyleBackColor = true;
            this.btnBsmForm.Click += new System.EventHandler(this.btnBsmDev_Click);
            // 
            // btnImForm
            // 
            this.btnImForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImForm.Location = new System.Drawing.Point(313, 193);
            this.btnImForm.Margin = new System.Windows.Forms.Padding(2);
            this.btnImForm.Name = "btnImForm";
            this.btnImForm.Size = new System.Drawing.Size(109, 60);
            this.btnImForm.TabIndex = 1;
            this.btnImForm.Text = "Implant (AIMX00)";
            this.btnImForm.UseVisualStyleBackColor = true;
            this.btnImForm.Click += new System.EventHandler(this.btnImDev_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.BackColor = System.Drawing.SystemColors.Control;
            this.lblTitle.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(22, 122);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(487, 26);
            this.lblTitle.TabIndex = 15;
            this.lblTitle.Text = "ZL7010X Application Development Kit (ADK)";
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.ForeColor = System.Drawing.Color.Black;
            this.lblVersion.Location = new System.Drawing.Point(195, 157);
            this.lblVersion.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(139, 24);
            this.lblVersion.TabIndex = 16;
            this.lblVersion.Text = "Version: 3.1.0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label2.Location = new System.Drawing.Point(200, 17);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 13);
            this.label2.TabIndex = 28;
            this.label2.Text = "Implant Response Poll Rate (Hz)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label1.Location = new System.Drawing.Point(32, 37);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "CCA/RSSI Rate (Hz)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label3.Location = new System.Drawing.Point(32, 17);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Status Poll Rate (Hz)";
            // 
            // cmbStatPollRate
            // 
            this.cmbStatPollRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbStatPollRate.ForeColor = System.Drawing.SystemColors.InfoText;
            this.cmbStatPollRate.FormattingEnabled = true;
            this.cmbStatPollRate.Items.AddRange(new object[] {
            ".5",
            "1",
            "2",
            "5",
            "10"});
            this.cmbStatPollRate.Location = new System.Drawing.Point(141, 14);
            this.cmbStatPollRate.Margin = new System.Windows.Forms.Padding(2);
            this.cmbStatPollRate.Name = "cmbStatPollRate";
            this.cmbStatPollRate.Size = new System.Drawing.Size(42, 21);
            this.cmbStatPollRate.TabIndex = 31;
            this.cmbStatPollRate.SelectedIndexChanged += new System.EventHandler(this.cmbStatPollRate_SelectedIndexChanged);
            this.cmbStatPollRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // cmbCcaRate
            // 
            this.cmbCcaRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCcaRate.ForeColor = System.Drawing.SystemColors.InfoText;
            this.cmbCcaRate.FormattingEnabled = true;
            this.cmbCcaRate.Items.AddRange(new object[] {
            "0.5",
            "1",
            "2",
            "5",
            "10"});
            this.cmbCcaRate.Location = new System.Drawing.Point(141, 33);
            this.cmbCcaRate.Margin = new System.Windows.Forms.Padding(2);
            this.cmbCcaRate.Name = "cmbCcaRate";
            this.cmbCcaRate.Size = new System.Drawing.Size(42, 21);
            this.cmbCcaRate.TabIndex = 32;
            this.cmbCcaRate.SelectedIndexChanged += new System.EventHandler(this.cmbCcaRate_SelectedIndexChanged);
            this.cmbCcaRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // cmbImResponsePollRate
            // 
            this.cmbImResponsePollRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbImResponsePollRate.ForeColor = System.Drawing.SystemColors.InfoText;
            this.cmbImResponsePollRate.FormattingEnabled = true;
            this.cmbImResponsePollRate.Items.AddRange(new object[] {
            "0.5",
            "1",
            "2",
            "5",
            "10"});
            this.cmbImResponsePollRate.Location = new System.Drawing.Point(362, 14);
            this.cmbImResponsePollRate.Margin = new System.Windows.Forms.Padding(2);
            this.cmbImResponsePollRate.Name = "cmbImResponsePollRate";
            this.cmbImResponsePollRate.Size = new System.Drawing.Size(42, 21);
            this.cmbImResponsePollRate.TabIndex = 33;
            this.cmbImResponsePollRate.SelectedIndexChanged += new System.EventHandler(this.cmbImResponsePollRate_SelectedIndexChanged);
            this.cmbImResponsePollRate.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ignoreEntry_KeyPress);
            // 
            // grpSysConfig
            // 
            this.grpSysConfig.Controls.Add(this.label3);
            this.grpSysConfig.Controls.Add(this.label1);
            this.grpSysConfig.Controls.Add(this.cmbImResponsePollRate);
            this.grpSysConfig.Controls.Add(this.cmbStatPollRate);
            this.grpSysConfig.Controls.Add(this.cmbCcaRate);
            this.grpSysConfig.Controls.Add(this.label2);
            this.grpSysConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSysConfig.ForeColor = System.Drawing.Color.Blue;
            this.grpSysConfig.Location = new System.Drawing.Point(46, 288);
            this.grpSysConfig.Margin = new System.Windows.Forms.Padding(2);
            this.grpSysConfig.Name = "grpSysConfig";
            this.grpSysConfig.Padding = new System.Windows.Forms.Padding(2);
            this.grpSysConfig.Size = new System.Drawing.Size(437, 61);
            this.grpSysConfig.TabIndex = 35;
            this.grpSysConfig.TabStop = false;
            this.grpSysConfig.Text = "System Settings";
            // 
            // btnSysSet
            // 
            this.btnSysSet.Location = new System.Drawing.Point(210, 259);
            this.btnSysSet.Margin = new System.Windows.Forms.Padding(2);
            this.btnSysSet.Name = "btnSysSet";
            this.btnSysSet.Size = new System.Drawing.Size(109, 25);
            this.btnSysSet.TabIndex = 37;
            this.btnSysSet.Text = "System Settings";
            this.btnSysSet.UseVisualStyleBackColor = true;
            this.btnSysSet.Click += new System.EventHandler(this.btnSysSet_Click);
            // 
            // picLogo
            // 
            this.picLogo.Image = global::Zarlink.Adk.Forms.Properties.Resources.MicrosemiLogo;
            this.picLogo.Location = new System.Drawing.Point(106, 20);
            this.picLogo.Margin = new System.Windows.Forms.Padding(2);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(315, 90);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLogo.TabIndex = 34;
            this.picLogo.TabStop = false;
            // 
            // AdkForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 358);
            this.Controls.Add(this.btnImForm);
            this.Controls.Add(this.btnSysSet);
            this.Controls.Add(this.btnBsmForm);
            this.Controls.Add(this.grpSysConfig);
            this.Controls.Add(this.picLogo);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.lblTitle);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "AdkForm";
            this.Text = "ZL7010X Application Development Kit (ADK)";
            this.grpSysConfig.ResumeLayout(false);
            this.grpSysConfig.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnBsmForm;
        private System.Windows.Forms.Button btnImForm;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.HelpProvider hlp;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ComboBox cmbStatPollRate;
        private System.Windows.Forms.ComboBox cmbCcaRate;
        private System.Windows.Forms.ComboBox cmbImResponsePollRate;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.GroupBox grpSysConfig;
        private System.Windows.Forms.Button btnSysSet;
    }
}

