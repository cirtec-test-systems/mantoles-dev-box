//
// This file contains the form for nonvolatile memory display. This display is
// used to modify the nonvolatile memory (NVM) on the base station and the implant.
//
// Copyright Microsemi Semiconductor (U.S.) Inc. 2016. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2015. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.

using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Globalization;
using System.IO;
using System.Collections.Generic;
using Zarlink.Adp.General;
using Zarlink.Adk.Api;
using Zarlink.Adk.Forms;

namespace Zarlink.Adk.Forms
{
    public partial class NvmForm : Form
    {
        private const string END_OF_FILE_INTEL_HEX = ":00000001FF";

        // List of text boxes to dispaly RSSI per channel calibrations
        private List<TextBox> chanTxtboxList = new List<TextBox>();

        public BsmApi nvmBsmMod = null;

        public NvmForm(BsmApi bsmMod)
        {
            InitializeComponent();

            // Initialize textbox list
            chanTxtboxList.Add(txtBsmNvmCh0);
            chanTxtboxList.Add(txtBsmNvmCh1);
            chanTxtboxList.Add(txtBsmNvmCh2);
            chanTxtboxList.Add(txtBsmNvmCh3);
            chanTxtboxList.Add(txtBsmNvmCh4);
            chanTxtboxList.Add(txtBsmNvmCh5);
            chanTxtboxList.Add(txtBsmNvmCh6);
            chanTxtboxList.Add(txtBsmNvmCh7);
            chanTxtboxList.Add(txtBsmNvmCh8);
            chanTxtboxList.Add(txtBsmNvmCh9);

            // Get the reference to the BsmApi module
            nvmBsmMod = bsmMod;

            // Enable the buttons for the BSM NVM and IM NVM
            // Intel Hex files Load/Save. 
            //
            btnBsmSaveIntelHex.Enabled = true;
            btnBsmLoadIntelHex.Enabled = true;

            btnImSaveIntelHex.Enabled = true;
            btnImLoadIntelHex.Enabled = true;

            // display default settings for BSM NVM
            displayBsmNvm(new BsmApi.BsmNvm());

            // display default settings for IM NVM
            displayImNvm(new ImApi.ImNvm());
        }


        private void btnBsmSaveIntelHex_Click(object sender, EventArgs ea)
        {
            string buttonName = "\"" + btnBsmSaveIntelHex.Text + "\"";
            
            // init resources referenced by cleanup logic in "finally"
            StreamWriter hexFileWriter = null;

            try {
                BsmApi.BsmNvm bsmNvm = new BsmApi.BsmNvm();
                
                // build bsm NVM from settings that are currently displayed
                buildBsmNvm(bsmNvm);

                SaveFileDialog sfdNvmHex = new SaveFileDialog();
                sfdNvmHex.Filter = "hex files (*.hex)|*.hex|All files (*.*)|*.*";
                sfdNvmHex.FilterIndex = 1;
                sfdNvmHex.RestoreDirectory = true;
                //
                DialogResult dlogResult = sfdNvmHex.ShowDialog();
                if (dlogResult == DialogResult.Cancel) throw new Exception("The file selection was canceled.");
                if (sfdNvmHex.FileName.ToString() == "") throw new Exception("No file name was input.");

                // create Intel hex file
                hexFileWriter = new StreamWriter(sfdNvmHex.FileName);
                hexFileWriter.AutoFlush = true;

                // marshall data from BsmFactNvm object to array
                int n = Marshal.SizeOf(typeof(BsmApi.BsmNvm));

                byte[] nvmFileData = new byte[n + 4];

                nvmFileData[0] = (byte)n;
                nvmFileData[1] = 0x10;  // address offset MSB
                nvmFileData[2] = 0x80;  // address offset LSB
                nvmFileData[3] = 0x00;  // record type

                for (byte i = 4; i < n + 4; i++) {
                    nvmFileData[i] = Marshal.ReadByte(bsmNvm, i - 4);  // data
                }

                // calculate 8-bit checksum and convert to 2's complement
                byte rowCheckSum = 0;
                foreach(byte b in nvmFileData) rowCheckSum += b;
                rowCheckSum = (byte)(~rowCheckSum + 1);

                // create Intel hex format .hex file
                hexFileWriter.Write(":");
                foreach(byte b in nvmFileData) hexFileWriter.Write(b.ToString("X2"));
                hexFileWriter.Write(rowCheckSum.ToString("X2") + Environment.NewLine);
                hexFileWriter.WriteLine(END_OF_FILE_INTEL_HEX);

            } catch (Exception e) {
                MessageBox.Show("Failed " + buttonName + ": " + Util.eMsg(e));
            } finally {
                if (hexFileWriter != null) hexFileWriter.Close();
            }
        }

        private void btnBsmLoadIntelHex_Click(object sender, EventArgs ea)
        {
            string buttonName = "\"" + btnBsmLoadIntelHex.Text + "\"";

            try {                
                readAndCheckBsmNvmFromIntelHexFile();
            } catch (Exception e) {
                MessageBox.Show("Failed " + buttonName + ": " + Util.eMsg(e));
            }
        }

        // This reads the settings for the bsm's nonvolatile memory (NVM) from
        // an Intel Hex file, checks if they're valid, and displays them.
        //
        private void readAndCheckBsmNvmFromIntelHexFile()
        {
            byte[] bsmNvmPrev = new byte[512];
            BsmApi.BSM_NVM_RSTAT nvmStat;
            string err;

            // read bsm NVM from Intel hex file
            readBsmNvmFromIntelHexFile(bsmNvmPrev);
            
            // check if bsm NVM is valid and inform the user accordingly
            if ((nvmStat = BsmApi.checkNvm(bsmNvmPrev, out err)) < 0) {

                if (nvmStat == BsmApi.BSM_NVM_RSTAT.NVM_IS_OLDER_VER) {
                
                    MessageBox.Show("The nonvolatile memory read from the file is " +
                        "an older version. The display will use default values for " +
                        "new settings that do not exist in the old BSM NVM.");
                } else {
                
                    MessageBox.Show("The nonvolatile memory read from the file is invalid: " + err +
                        "The display will use default values for all BSM NVM settings.");
                        
                    bsmNvmPrev = null;
                }
            }
            
            // create a new bsm NVM that contains default values
            BsmApi.BsmNvm bsmNvm = new BsmApi.BsmNvm();
            //
            // If the previous NVM read from an Intel hex file is valid, transfer the settings
            // from the previous NVM to the new NVM. Note that if the previous NVM is an
            // older (smaller) version of the BSM_NVM structure that does not contain all
            // of the settings that exist in the current BsmNvm object, transferNvm() leaves
            // the new settings in the BsmNvm object at the default values initialized by
            // the BsmNvm() constructor when the BsmNvm object was created above.
            //
            if (bsmNvmPrev != null) {
            
                if (BsmApi.transferNvm(bsmNvmPrev, bsmNvm, out err) < 0) {
                    throw new Exception("Failed to transfer settings from the previous NVM to the new NVM: " + err);
                }
            }
            
            // display bsm NVM settings
            displayBsmNvm(bsmNvm);
        }

        private void readBsmNvmFromIntelHexFile(byte[] nvmData)
        {
            string s;
            char[] lineDelimiter = new char[] { ':' };
            OpenFileDialog ofdNvmHex = new OpenFileDialog();

            ofdNvmHex.Filter = "hex files (*.hex)|*.hex|All files (*.*)|*.*";
            ofdNvmHex.FilterIndex = 1;
            ofdNvmHex.RestoreDirectory = true;
            //
            DialogResult dialogResult = ofdNvmHex.ShowDialog();
            if (dialogResult == DialogResult.Cancel) throw new Exception("The file selection was canceled.");
            if (ofdNvmHex.FileName.ToString() == "") throw new Exception("No file name was input.");

            s = File.ReadAllText(ofdNvmHex.FileName);

            // remove ':' at beginning of first line
            s = s.Remove(0, 1);

            // split string into an array
            String[] eeprom = s.Split(lineDelimiter);

            byte nvmDataSizeLsb = (byte)Int32.Parse(eeprom[0].Substring(20, 2), NumberStyles.HexNumber);
            byte nvmDataSizeMsb = (byte)Int32.Parse(eeprom[0].Substring(22, 2), NumberStyles.HexNumber);
            ushort nvmDataSize = (ushort)(nvmDataSizeMsb * 256 + nvmDataSizeLsb);
            int nvmDataIndex = 0;

            int maxNumRows = (nvmDataSize / 16) + 1;
            int j = 0;

            for(int i = 0; i < maxNumRows; i++) {

                byte numberByteInRow = (byte)Int32.Parse(eeprom[i].Substring(0, 2), NumberStyles.HexNumber);

                if (eeprom[i].Substring(0, 9) != "00000001F") {

                    j = 8;    
                    for(byte k = 0; k < numberByteInRow; k++) {
                    
                        nvmDataIndex = (i * 16) + k;
                        
                        if (nvmDataIndex < nvmDataSize) {
                            nvmData[nvmDataIndex] = (byte)Int32.Parse(eeprom[i].Substring(j, 2), NumberStyles.HexNumber);
                            j += 2;
                        } else {
                            break;
                        }
                    }
                    
                } else {
                    break;
                }
            }
        }
        
        private void displayBsmNvm(BsmApi.BsmNvm bsmNvm)
        {
            int chan = 0;

            // Display factory settings for bsm.
            //
            foreach (TextBox ch in chanTxtboxList) {
            
                switch(chan++) {
                case 0: ch.Text = bsmNvm.micsFactNvm.rssiToDbmCalForChan0.Val.ToString(); break;
                case 1: ch.Text = bsmNvm.micsFactNvm.rssiToDbmCalForChan1.Val.ToString(); break;
                case 2: ch.Text = bsmNvm.micsFactNvm.rssiToDbmCalForChan2.Val.ToString(); break;
                case 3: ch.Text = bsmNvm.micsFactNvm.rssiToDbmCalForChan3.Val.ToString(); break;
                case 4: ch.Text = bsmNvm.micsFactNvm.rssiToDbmCalForChan4.Val.ToString(); break;
                case 5: ch.Text = bsmNvm.micsFactNvm.rssiToDbmCalForChan5.Val.ToString(); break;
                case 6: ch.Text = bsmNvm.micsFactNvm.rssiToDbmCalForChan6.Val.ToString(); break;
                case 7: ch.Text = bsmNvm.micsFactNvm.rssiToDbmCalForChan7.Val.ToString(); break;
                case 8: ch.Text = bsmNvm.micsFactNvm.rssiToDbmCalForChan8.Val.ToString(); break;
                case 9: ch.Text = bsmNvm.micsFactNvm.rssiToDbmCalForChan9.Val.ToString(); break;
                default: break; 
                }
            }
            txtBsmNvmParm2.Text = bsmNvm.micsFactNvm.rxIfAdcDecLev.Val.ToString();
        }
        
        private void buildBsmNvm(BsmApi.BsmNvm bsmNvm)
        {
            int chan = 0;
            
            // Build factory settings for bsm.
            // 
            foreach(TextBox ch in chanTxtboxList) {
            
                switch(chan++) {
                case 0: bsmNvm.micsFactNvm.rssiToDbmCalForChan0.Val = ushort.Parse(ch.Text); break;
                case 1: bsmNvm.micsFactNvm.rssiToDbmCalForChan1.Val = ushort.Parse(ch.Text); break;
                case 2: bsmNvm.micsFactNvm.rssiToDbmCalForChan2.Val = ushort.Parse(ch.Text); break;
                case 3: bsmNvm.micsFactNvm.rssiToDbmCalForChan3.Val = ushort.Parse(ch.Text); break;
                case 4: bsmNvm.micsFactNvm.rssiToDbmCalForChan4.Val = ushort.Parse(ch.Text); break;
                case 5: bsmNvm.micsFactNvm.rssiToDbmCalForChan5.Val = ushort.Parse(ch.Text); break;
                case 6: bsmNvm.micsFactNvm.rssiToDbmCalForChan6.Val = ushort.Parse(ch.Text); break;
                case 7: bsmNvm.micsFactNvm.rssiToDbmCalForChan7.Val = ushort.Parse(ch.Text); break;
                case 8: bsmNvm.micsFactNvm.rssiToDbmCalForChan8.Val = ushort.Parse(ch.Text); break;
                case 9: bsmNvm.micsFactNvm.rssiToDbmCalForChan9.Val = ushort.Parse(ch.Text); break;
                default: break; 
                }
            }
            bsmNvm.micsFactNvm.rxIfAdcDecLev.Val = (byte)int.Parse(txtBsmNvmParm2.Text);
        }


        private void btnImSaveIntelHex_Click(object sender, EventArgs ea)
        {
            string buttonName = "\"" + btnImSaveIntelHex.Text + "\"";

            // init resources referenced by cleanup logic in "finally"
            StreamWriter hexFileWriter = null;

            try
            {
                ImApi.ImNvm imNvm = new ImApi.ImNvm();

                // build bsm NVM from settings that are currently displayed
                buildImNvm(imNvm);

                SaveFileDialog sfdNvmHex = new SaveFileDialog();
                sfdNvmHex.Filter = "hex files (*.hex)|*.hex|All files (*.*)|*.*";
                sfdNvmHex.FilterIndex = 1;
                sfdNvmHex.RestoreDirectory = true;
                //
                DialogResult dlogResult = sfdNvmHex.ShowDialog();
                if (dlogResult == DialogResult.Cancel) throw new Exception("The file selection was canceled.");
                if (sfdNvmHex.FileName.ToString() == "") throw new Exception("No file name was input.");

                // create Intel hex file
                hexFileWriter = new StreamWriter(sfdNvmHex.FileName);
                hexFileWriter.AutoFlush = true;

                // marshall data from ImFactNvm object to array
                int n = Marshal.SizeOf(typeof(ImApi.ImNvm));

                byte[] nvmFileData = new byte[n + 4];

                nvmFileData[0] = (byte)n;
                nvmFileData[1] = 0x10;  // address offset MSB
                nvmFileData[2] = 0x80;  // address offset LSB
                nvmFileData[3] = 0x00;  // record type

                for (byte i = 4; i < n + 4; i++)
                {
                    nvmFileData[i] = Marshal.ReadByte(imNvm, i - 4);  // data
                }

                // calculate 8-bit checksum and convert to 2's complement
                byte rowCheckSum = 0;
                foreach(byte b in nvmFileData) rowCheckSum += b;
                rowCheckSum = (byte)(~rowCheckSum + 1);

                // create Intel hex format .hex file
                hexFileWriter.Write(":");
                foreach (byte b in nvmFileData) hexFileWriter.Write(b.ToString("X2"));
                hexFileWriter.Write(rowCheckSum.ToString("X2") + Environment.NewLine);
                hexFileWriter.WriteLine(END_OF_FILE_INTEL_HEX);

            }
            catch (Exception e)
            {
                MessageBox.Show("Failed " + buttonName + ": " + Util.eMsg(e));
            }
            finally
            {
                if (hexFileWriter != null) hexFileWriter.Close();
            }
        }

        private void btnImLoadIntelHex_Click(object sender, EventArgs ea)
        {
            string buttonName = "\"" + btnImLoadIntelHex.Text + "\"";

            try
            {
                readAndCheckImNvmFromIntelHexFile();
            }
            catch (Exception e)
            {
                MessageBox.Show("Failed " + buttonName + ": " + Util.eMsg(e));
            }
        }

        // This reads the settings for the implant's nonvolatile memory (NVM) from
        // an Intel Hex file, checks if they're valid, and displays them.
        //
        private void readAndCheckImNvmFromIntelHexFile()
        {
            byte[] imNvmPrev = new byte[512];
            ImApi.IM_NVM_RSTAT nvmStat;
            string err;

            // read im NVM from Intel hex file
            readImNvmFromIntelHexFile(imNvmPrev);

            // check if im NVM is valid and inform the user accordingly
            if ((nvmStat = ImApi.checkNvm(imNvmPrev, out err)) < 0)
            {
                if (nvmStat == ImApi.IM_NVM_RSTAT.NVM_IS_OLDER_VER)
                {

                    MessageBox.Show("The nonvolatile memory read from the file is " +
                        "an older version. The display will use default values for " +
                        "new settings that do not exist in the old IM NVM.");
                }
                else
                {

                    MessageBox.Show("The nonvolatile memory read from the file is invalid: " + err +
                        "The display will use default values for all IM NVM settings.");

                    imNvmPrev = null;
                }
            }

            // create a new im NVM that contains default values
            ImApi.ImNvm imNvm = new ImApi.ImNvm();
            //
            // If the previous NVM read from an Intel hex file is valid, transfer the settings
            // from the previous NVM to the new NVM. Note that if the previous NVM is an
            // older (smaller) version of the IM_NVM structure that does not contain all
            // of the settings that exist in the current ImNvm object, transferNvm() leaves
            // the new settings in the ImNvm object at the default values initialized by
            // the ImNvm() constructor when the ImNvm object was created above.
            //
            if (imNvmPrev != null)
            {
                if (ImApi.transferNvm(imNvmPrev, imNvm, out err) < 0)
                {
                    throw new Exception("Failed to transfer settings from the previous NVM to the new NVM: " + err);
                }
            }

            // display im NVM settings
            displayImNvm(imNvm);
        }

        private void readImNvmFromIntelHexFile(byte[] nvmData)
        {
            string s;
            char[] lineDelimiter = new char[] { ':' };
            OpenFileDialog ofdNvmHex = new OpenFileDialog();

            ofdNvmHex.Filter = "hex files (*.hex)|*.hex|All files (*.*)|*.*";
            ofdNvmHex.FilterIndex = 1;
            ofdNvmHex.RestoreDirectory = true;
            //
            DialogResult dialogResult = ofdNvmHex.ShowDialog();
            if (dialogResult == DialogResult.Cancel) throw new Exception("The file selection was canceled.");
            if (ofdNvmHex.FileName.ToString() == "") throw new Exception("No file name was input.");

            s = File.ReadAllText(ofdNvmHex.FileName);

            // remove ':' at beginning of first line
            s = s.Remove(0, 1);

            // split string into an array
            String[] eeprom = s.Split(lineDelimiter);

            byte nvmDataSizeLsb = (byte)Int32.Parse(eeprom[0].Substring(20, 2), NumberStyles.HexNumber);
            byte nvmDataSizeMsb = (byte)Int32.Parse(eeprom[0].Substring(22, 2), NumberStyles.HexNumber);
            ushort nvmDataSize = (ushort)(nvmDataSizeMsb * 256 + nvmDataSizeLsb);
            int nvmDataIndex = 0;

            int maxNumRows = (nvmDataSize / 16) + 1;
            int j = 0;

            for (int i = 0; i < maxNumRows; i++)
            {
                byte numberByteInRow = (byte)Int32.Parse(eeprom[i].Substring(0, 2), NumberStyles.HexNumber);

                if (eeprom[i].Substring(0, 9) != "00000001F")
                {

                    j = 8;
                    for (byte k = 0; k < numberByteInRow; k++)
                    {
                        nvmDataIndex = (i * 16) + k;

                        if (nvmDataIndex < nvmDataSize)
                        {
                            nvmData[nvmDataIndex] = (byte)Int32.Parse(eeprom[i].Substring(j, 2), NumberStyles.HexNumber);
                            j += 2;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                else
                {
                    break;
                }
            }
        }

        private void displayImNvm(ImApi.ImNvm imNvm)
        {
            // Display factory settings for implant.          
            txtImNvmParm1.Text = imNvm.micsFactNvm.rssiToDbmCal.Val.ToString();
            txtImNvmParm2.Text = imNvm.micsFactNvm.rxIfAdcDecLev.Val.ToString();
            txtImNvmParm3.Text = imNvm.micsFactNvm.xoTrim.Val.ToString();
            txtImNvmParm4.Text = imNvm.micsFactNvm.rssiOffsetTrim.Val.ToString();
        }

        private void buildImNvm(ImApi.ImNvm imNvm)
        {
            // Build factory settings for implant.           
            imNvm.micsFactNvm.rssiToDbmCal.Val = (byte)int.Parse(txtImNvmParm1.Text);
            imNvm.micsFactNvm.rxIfAdcDecLev.Val = (byte)int.Parse(txtImNvmParm2.Text);
            imNvm.micsFactNvm.xoTrim.Val = (byte)int.Parse(txtImNvmParm3.Text);
            imNvm.micsFactNvm.rssiOffsetTrim.Val = (byte)int.Parse(txtImNvmParm4.Text);
        }

        // Perform a CCA on all channels and populate the
        // RSSI-to-dBm conversion column of the NVM form.
        private void btnCalRssi_Click(object sender, EventArgs e)
        {
            BsmApi.BSM_CCA_DATA ccaData;
            int rssi;
            string err = "";
            const bool useAvgRssiForCca = true;
            int chan = 0;

            if (nvmBsmMod == null) {
                MessageBox.Show("BSM must be attached for this measurement! ");
                return;
            }

            // Perform CCA. This gets the external RSSI for each channel
            // (average RSSI over 10 ms) and returns
            // the clearest channel (unused).
            //
            if ((nvmBsmMod.cca(useAvgRssiForCca, out ccaData, ref err)) < 0) {
                MessageBox.Show("Failed CCA: " + err);
                return;
            }

            // display external CCA for each channel
            foreach (TextBox ch in chanTxtboxList) {

                // get external RSSI for channel
                unsafe { rssi = ccaData.rssi[chan]; }

                // display on NVM form
                ch.Text = rssi.ToString();
                chan++;
            }

            return;
        }

    }
}