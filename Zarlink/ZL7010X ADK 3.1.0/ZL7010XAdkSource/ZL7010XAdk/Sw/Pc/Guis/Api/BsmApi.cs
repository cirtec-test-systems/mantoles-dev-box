//
// This file contains the BsmApi class. The ZL7010X ADK GUI uses this class to
// interface to base station library in the ZL7010X ADK C API (DLL), which
// communicates with the base station mezzanine board on the base station
// module (BSM) via USB.
//
// Note that some of the data types (enum, struct) in this class are passed to
// functions in the C API DLL. These must match the corresponding types in the
// C API, and must not be altered except as needed to reflect changes in the
// C API. For descriptions of these types, see the corresponding types in the
// C API. Note that the names used for these types are generally the same as
// the names used in the C API.
//
// Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2012. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.

using System.Windows.Forms;
using System.Runtime.InteropServices; // for DLL
using Zarlink.Adp.Api;

namespace Zarlink.Adk.Api
{
    unsafe public class BsmApi : CommonApi
    {
        // name of DLL that contains the C API functions
        private const string C_API_DLL = Build.BuildSettings.C_API_DLL;
        // 
        // Declarations for the unmanaged API/DLL functions. Each declaration
        // corresponds to a function in the API/DLL.
        //
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern void BsmInitSpecs(out BSM_SPECS specs, uint specsSize);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern void* BsmOpen(ref BSM_SPECS specs, uint specsSize, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmSetMicsConfig(void* bsmId, ref BSM_MICS_CONFIG mc, uint mcSize, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmGetMicsConfig(void* bsmId, out BSM_MICS_CONFIG mc, uint mcSize, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmGetMezzVer(void* bsmId, sbyte[] buf, uint bufSize, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmStartTx245Carrier(void* bsmId, bool start, void* e); 
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmEnabExtRssi(void* bsmId, bool enab, uint chan, void* e); 
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmStartTx400Carrier(void* bsmId, bool start, uint chan, void* e); 
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmStartSession(void* bsmId, bool enabWatchdog, uint timeoutSec, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmSearchForImplant(void* bsmId, bool enabWatchdog, bool anyCompany, bool anyImplant, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmListenForEmergency(void* bsmId, bool startSession, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmAbortMics(void* bsmId, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmResetMics(void* bsmId, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmReadMicsReg(void* bsmId, uint regAddr, bool remote, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmWriteMicsReg(void* bsmId, uint regAddr, uint regVal, bool remote, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmReceiveMics(void* bsmId, byte[] buf, uint bufSize, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmTransmitMics(void* bsmId, byte* data, uint len, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmRssi(void* bsmId, bool useIntRssi, uint chan, bool average, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmCca(void* bsmId, bool average, BSM_CCA_DATA *data, uint dataSize, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmMicsCal(void* bsmId, uint cals, int chan, bool remote, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmClose(void* bsmId, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmSetTx245Power(void* bsmId, uint ccPa, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmMicsAdc(void* bsmId, uint adcInput, bool remote, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmStartBerRx(void* bsmId, bool start, uint chan, uint rxMod, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmCopyMicsRegs(void* bsmId, bool remote, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmCalcMicsCrc(void* bsmId, bool remote, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmGetStatChanges(void* bsmId, bool force, out BSM_STAT_CHANGES sc, uint scSize, void * e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmStartDataTest(void* bsmId, ref MICS_DT_SPECS specs, uint specsSize, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmGetTraceMsg(void* bsmId, sbyte[] buf, uint bufSize, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmSetTx245Freq(void* bsmId, uint ccFreq, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmWriteCcReg(void* bsmId,  uint regAddr, uint regVal, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmReadCcReg(void* bsmId, uint regAddr, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmSleep(void* bsmId, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmWakeup(void* bsmId, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmCheckNvm(void* nvm, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmTransferNvm(void* prevNvm, [In, Out] BsmNvm nvm, uint nvmSize, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int BsmGetMicsFactNvm(void* bsmId, [Out] BsmMicsFactNvm fn, uint fnSize, void* e);

        private const string BSM_DEV_TYPE = "ZL7010X ADK Base Station";
        
        // Class for the header at the beginning of the nonvolatile memory
        // (NVM) on a BSM. For field descriptions, see BSM_NVM_HEAD in
        // "[SourceTree]\7010XAdk\Sw\Includes\Adk\BsmMezz\BsmGeneral.h".
        //
        [StructLayout(LayoutKind.Sequential)]
        public class BsmNvmHead
        {
            private readonly NvmInfo nvmInfo = new NvmInfo(NvmInfo.NVM_TYPE_10X_ADK_BSM,
                    (ushort)Marshal.SizeOf(typeof(BsmNvm)));

            private readonly byte nvmHeadSize = (byte)Marshal.SizeOf(typeof(BsmNvmHead));

            private readonly byte micsFactNvmOffset = (byte)Marshal.OffsetOf(typeof(BsmNvm), "micsFactNvm");
            private readonly byte micsFactNvmSize = (byte)Marshal.SizeOf(typeof(BsmMicsFactNvm));
        }
        // Class for the nonvolatile memory (NVM) on a BSM. For field descriptions,
        // see BSM_NVM in "[SourceTree]\7010XAdk\Sw\Includes\Adk\BsmMezz\BsmGeneral.h".
        //
        [StructLayout(LayoutKind.Sequential)]
        public class BsmNvm
        {
            private readonly BsmNvmHead head = new BsmNvmHead();
            public BsmMicsFactNvm micsFactNvm = new BsmMicsFactNvm();
        }
        
        // Structure for the section of the nonvolatile memory that contains the
        // factory settings for the ZL7010X radio on a base station. Note that this
        // structure should contain only fields that are applicable to all applications
        // for all base stations that use the ZL7010X radio. It should not contain
        // any fields that are applicable only to specific applications. For field
        // descriptions, see BSM_MICS_FACT_NVM in "[SourceTree]\ZL7010XAdk\Sw\
        // Includes\Adk\AnyMezz\MicsGeneral.h"
        //
        [StructLayout(LayoutKind.Sequential)]
        public class BsmMicsFactNvm
        {
            public Ud16Bytes rssiToDbmCalForChan0 = new Ud16Bytes(0);
            public Ud16Bytes rssiToDbmCalForChan1 = new Ud16Bytes(0);
            public Ud16Bytes rssiToDbmCalForChan2 = new Ud16Bytes(0);
            public Ud16Bytes rssiToDbmCalForChan3 = new Ud16Bytes(0);
            public Ud16Bytes rssiToDbmCalForChan4 = new Ud16Bytes(0);
            public Ud16Bytes rssiToDbmCalForChan5 = new Ud16Bytes(0);
            public Ud16Bytes rssiToDbmCalForChan6 = new Ud16Bytes(0);
            public Ud16Bytes rssiToDbmCalForChan7 = new Ud16Bytes(0);
            public Ud16Bytes rssiToDbmCalForChan8 = new Ud16Bytes(0);
            public Ud16Bytes rssiToDbmCalForChan9 = new Ud16Bytes(0);
            //
            public Ud8 rxIfAdcDecLev = new Ud8(0);
        }

        // Values for the return status for checkNvm(). Note that these must
        // match the corresponding values in "[SourceTree]\7010XAdk\Sw\Includes
        // \Pc\BsmLib.h".
        //
        public enum BSM_NVM_RSTAT : int
        {
            NVM_IS_VALID = 0,
            NVM_IS_OLDER_VER = -1,
            NVM_OTHER_ERR = -2
        }
        
        // The API/DLL uses this to hold the MICS configuration for a base
        // station. For field descriptions, see BSM_MICS_CONFIG in "ZL7010XAdk\
        // Sw\Includes\Adk\BsmMezz\BsmGeneral.h".
        //
        private struct BSM_MICS_CONFIG
        {
            public byte flags;  // see BSM_AUTO_LISTEN, etc. below
            public CommonApi.MICS_IMD_TID imdTid;
            public byte companyId;
            public CommonApi.MICS_LINK_CONFIG normalLinkConfig;
            public CommonApi.MICS_LINK_CONFIG emergencyLinkConfig;
            public byte micsRevOnImplant;  // see CommonApi.MICS_REV
        }
        // Bits in BSM_MICS_CONFIG.flags. For descriptions, see BSM_AUTO_LISTEN
        // in "ZL7010XAdk\Sw\Includes\Adk\BsmMezz\BsmGeneral.h".
        //
        private const byte BSM_AUTO_LISTEN          = (1 << 0);
        private const byte BSM_AUTO_CCA             = (1 << 2);
        private const byte BSM_400_MHZ_WAKEUP       = (1 << 3);
        private const byte BSM_RESET_MICS_ON_WAKEUP = (1 << 4);
        
        // The API/DLL uses this to hold the general status for a base station.
        // For field descriptions, see BSM_STAT in "ZL7010XAdk\Sw\Includes\
        // Adk\BsmMezz\BsmGeneral.h".
        //
        private struct BSM_STAT
        {
            public byte flags;  // see BSM_ERR_OCCURRED, etc. below
            public byte micsState;  
        }
        // Bits in BSM_STAT.flags. For descriptions, see BSM_ERR_OCCURRED, etc.
        // in "ZL7010XAdk\Sw\Includes\Adk\BsmMezz\BsmGeneral.h".
        //
        private const byte BSM_ERR_OCCURRED      = (1 << 0);
        private const byte BSM_LINK_STAT_CHANGED = (1 << 1);
        private const byte BSM_LINK_QUAL_CHANGED = (1 << 2);
        private const byte BSM_400_MHZ_TX_ACTIVE = (1 << 3);
        private const byte BSM_400_MHZ_RX_ACTIVE = (1 << 4);
        private const byte BSM_245_GHZ_TX_ACTIVE = (1 << 5);
        private const byte BSM_DATA_STAT_CHANGED = (1 << 6);
        
        // These are used to hold changes in the base station status. The API/DLL
        // uses BSM_STAT_CHANGES, which is converted to BsmStatChanges for public
        // reference (see BsmApi.getStatChanges()). For field descriptions, see
        // BSM_STAT_CHANGES in "ZL7010XAdk\Sw\Includes\Adk\BsmMezz\BsmGeneral.h".
        //
        private struct BSM_STAT_CHANGES
        {
            public BSM_STAT genStat;
            public CommonApi.MICS_LINK_STAT linkStat; 
            public CommonApi.MICS_LINK_QUAL linkQual; 
            public CommonApi.MICS_DATA_STAT dataStat; 
        }
        public class BsmStatChanges : CommonApi.StatChanges
        {
            // non-null if the base station detected an error
            public string bsmErr; 
            
            public bool tx245ActiveChanged;
            public bool tx245Active;
        }
        // buffer to hold status changes (returned by BsmApi.getStatChanges())
        private BsmStatChanges bsmStatChanges = new BsmStatChanges();
        
        private struct BSM_SPECS
        {
            public uint magic;
            public void *localAdpId;
        }
        
        public struct BSM_CCA_DATA
        {
            // result of RSSI for each channel 
            public fixed ushort rssi[10];
        }

        // Configuration settings unique to the base station.
        //
        private byte cfgWake245UserData;
        public byte CfgWake245UserData
        {
            get { return(cfgWake245UserData); }
            set {
                cfgWake245UserData = value; 
                configMezzAndReportAnyErr();
            }
        }

        private MICS_REV cfgMicsRevOnImplant;
        public MICS_REV CfgMicsRevOnImplant
        {
            get { return(cfgMicsRevOnImplant); }
            set { 
                cfgMicsRevOnImplant = value;
                configMezzAndReportAnyErr();
            }
        }
        private bool cfgAutoCcaEnab;
        public bool CfgAutoCcaEnab
        {
            get { return(cfgAutoCcaEnab); }
            set { 
                cfgAutoCcaEnab = value;
                configMezzAndReportAnyErr();
            }
        }
        private bool cfgAutoListenEnab;
        public bool CfgAutoListenEnab
        {
            get { return(cfgAutoListenEnab); }
            set {
                cfgAutoListenEnab = value; 
                configMezzAndReportAnyErr();
            }
        }
        private bool cfgResetMicsOnWakeup;
        public bool CfgResetMicsOnWakeup
        {
            get { return(cfgResetMicsOnWakeup); }
            set {
                cfgResetMicsOnWakeup = value;
                configMezzAndReportAnyErr();
            }
        }

        // connection to base station mezzanine board (returned by BsmOpen())
        private void* bsmId = null;
        
        public BsmApi()
        {
            mezzDevType = BSM_DEV_TYPE;
        }
        
        // This initializes the interface to the base station module (BsmApi).
        // Returns 0 for success, -1 for error.
        //
        public override int init(ref string err)
        {
            BSM_SPECS bsmSpecs;
            int val;

            // perform init common to both of the ADK module interfaces
            if (base.init(ref err) < 0) goto error;

            // connect to base station mezzanine board
            BsmInitSpecs(out bsmSpecs, (uint)sizeof(BSM_SPECS));
            bsmSpecs.localAdpId = this.adpId;
            //
            bsmId = BsmOpen(ref bsmSpecs, (uint)sizeof(BSM_SPECS), eiId);
            if (bsmId == null) {
                err = errReport("Failed BsmOpen(): ", eiId);
                goto error;
            }
            
            sbyte[] mezzVerBuf = new sbyte[125];
            if ((BsmGetMezzVer(bsmId, mezzVerBuf, 120, eiId)) < 0) {
                err = errReport("Failed BsmGetMezzVer(): ", eiId);
                goto error;
            }
            for(int i = 0; i < 125; i++) {
                if ((char)mezzVerBuf[i] == '\0') break;
                mezzVer += (char)mezzVerBuf[i];
            }
            
            // get MICS revision (ZL70101, ZL70102 or ZL70103)
            if ((val = BsmReadMicsReg(bsmId, 127, false, eiId)) < 0) {
                err = errReport("Failed BsmReadMicsReg(): ", eiId);
                goto error;
            }
            MicsRev = (MICS_REV)val;
 
            // get config from base station mezzanine board (init config properties)
            if (getConfigFromMezz(ref err) < 0) goto error;

            initialized = true;
            return(0);
error:
            // Close any API/DLL interfaces that were opened. This frees any
            // resources allocated by the API functions, and closes any board
            // connections so they're available to be re-opened later if desired.
            //
            string closeErr = "";
            close(ref closeErr);
            return(-1);
        }
        
        // This is called to close any API/DLL interfaces that were opened by
        // the base station API (BsmApi). This frees any resources allocated by
        // the API functions, and closes any board connections so they're available
        // to be re-opened later if desired. Returns 0 for success, -1 for error.
        //
        public override int close(ref string err)
        {
            int retVal = 0;
            
            if (bsmId != null) {
            
                if (BsmClose(bsmId, eiId) < 0) {
                    err = errReport("Failed BsmClose(): ", eiId);
                    retVal = -1;
                }
                bsmId = null;
            }
            
            // perform close tasks common to both of the ADK module interfaces
            if (base.close(ref err) < 0) retVal = -1;
            
            return(retVal);
        }

        // This gets the configuration from the base station mezzanine board
        // and uses it to initialize the configuration properties associated
        // with the board, so they reflect the board's current configuration.
        // Returns 0 if successful, -1 for error.
        //
        private int getConfigFromMezz(ref string err)
        {
            BSM_MICS_CONFIG mc;
            
            if (BsmGetMicsConfig(bsmId, out mc, (uint)sizeof(BSM_MICS_CONFIG), eiId) < 0) {
                err = errReport("Failed BsmGetMicsConfig(): ", eiId);
                return(-1);
            }
            
            CfgImdTid = (mc.imdTid.b3 << 16) + (mc.imdTid.b2 << 8) + mc.imdTid.b1;
            CfgCompanyId = mc.companyId;
            //
            CfgNormChan = mc.normalLinkConfig.chan;
            CfgNormTxMod = extractTxMod(mc.normalLinkConfig.modUser);
            CfgNormRxMod = extractRxMod(mc.normalLinkConfig.modUser);
            CfgNormBlockSize = mc.normalLinkConfig.rxBlockSize;
            CfgNormMaxBlocksPerTxPack = mc.normalLinkConfig.maxBlocksPerTxPack;
            //
            CfgEmerChan = mc.emergencyLinkConfig.chan;
            CfgEmerTxMod = extractTxMod(mc.emergencyLinkConfig.modUser);
            CfgEmerRxMod = extractRxMod(mc.emergencyLinkConfig.modUser);
            CfgEmerBlockSize = mc.emergencyLinkConfig.rxBlockSize;
            CfgEmerMaxBlocksPerTxPack = mc.emergencyLinkConfig.maxBlocksPerTxPack;
            // 
            CfgMicsRevOnImplant = (MICS_REV)mc.micsRevOnImplant;
            CfgWake245UserData = (byte)(mc.normalLinkConfig.modUser >> 4);
            
            if ((mc.flags & BSM_AUTO_LISTEN) != 0) {
                CfgAutoListenEnab = true;
            } else {
                CfgAutoListenEnab = false;
            }
            if ((mc.flags & BSM_AUTO_CCA) != 0) {
                CfgAutoCcaEnab = true;
            } else {
                CfgAutoCcaEnab = false;
            }
            if ((mc.flags & BSM_400_MHZ_WAKEUP) != 0) {
                Cfg400MHzWakeup = true;
            } else {
                Cfg400MHzWakeup = false;
            }
            if ((mc.flags & BSM_RESET_MICS_ON_WAKEUP) != 0) {
                CfgResetMicsOnWakeup = true;
            } else {
                CfgResetMicsOnWakeup = false;
            }
            
            return(0);
        }

        // This configures the base station mezzanine board with the
        // configuration properties associated with the board. First, it builds
        // a structure based on the current configuration properties, then sends
        // it to the board via the API/DLL. Returns 0 if successful, -1 for error.
        //
        protected override int configMezz(ref string err)
        {
            BSM_MICS_CONFIG mc;

            if (!initialized) return(0);
            
            mc.flags = 0;
            mc.normalLinkConfig.modUser = 0;
            mc.emergencyLinkConfig.modUser = 0;
            // 
            mc.companyId = CfgCompanyId;
            mc.imdTid.b3 = (byte)(CfgImdTid >> 16);
            mc.imdTid.b2 = (byte)(CfgImdTid >> 8);
            mc.imdTid.b1 = (byte)CfgImdTid;
            // 
            mc.normalLinkConfig.chan = CfgNormChan;
            mc.normalLinkConfig.modUser = buildModUser(CfgNormTxMod, CfgNormRxMod, CfgWake245UserData);
            mc.normalLinkConfig.txBlockSize = CfgNormBlockSize;
            mc.normalLinkConfig.rxBlockSize = CfgNormBlockSize;
            mc.normalLinkConfig.maxBlocksPerTxPack = CfgNormMaxBlocksPerTxPack;
            //
            mc.emergencyLinkConfig.chan = CfgEmerChan;
            mc.emergencyLinkConfig.modUser = buildModUser(CfgEmerTxMod, CfgEmerRxMod, CfgWake245UserData);
            mc.emergencyLinkConfig.txBlockSize = CfgEmerBlockSize;
            mc.emergencyLinkConfig.rxBlockSize = CfgEmerBlockSize;
            mc.emergencyLinkConfig.maxBlocksPerTxPack = CfgEmerMaxBlocksPerTxPack;
            //
            mc.micsRevOnImplant = (byte)CfgMicsRevOnImplant;

            if (cfgAutoCcaEnab) {
                mc.flags |= BSM_AUTO_CCA;
            } else {
                mc.flags &= (~BSM_AUTO_CCA & 0xFF);
            }
            if (cfgAutoListenEnab) {
                mc.flags |= BSM_AUTO_LISTEN;
            } else {
                mc.flags &= (~BSM_AUTO_LISTEN & 0xFF);
            }
            if (Cfg400MHzWakeup) {
                mc.flags |= BSM_400_MHZ_WAKEUP;
            } else {
                mc.flags &= (~BSM_400_MHZ_WAKEUP & 0xFF);
            }
            if (CfgResetMicsOnWakeup) {
                mc.flags |= BSM_RESET_MICS_ON_WAKEUP;
            } else {
                mc.flags &= (~BSM_RESET_MICS_ON_WAKEUP & 0xFF);
            }

            if (BsmSetMicsConfig(bsmId, ref mc, (uint)sizeof(BSM_MICS_CONFIG), eiId) < 0) {
                err = errReport("Failed BsmSetMicsConfig(): ", eiId);
                return(-1);
            }
            
            return(0);
        }
        
        // Returns -1 if value couldn't be read, otherwise, reg value is unsigned.
        //
        public override int micsRead(uint register, bool remote, ref string err)
        {
            int val;
            
            if (!initialized) {
                err = "Unable to read register: the board interface isn't initialized.";
                return(-1);
            }
            
            if ((val = BsmReadMicsReg(bsmId, register, remote, eiId)) < 0) {
                err = errReport("BsmReadMicsReg(): ", eiId);
                return(-1);
            }
            return(val);
        }

        public override int micsWrite(uint register, uint regVal, bool remote, ref string err)
        {
            if (!initialized) return(0); 
            
            // If caller is trying to change the ZL7010X company ID on the base
            // station to something other than 1, fail. This is done so the ADK
            // as shipped can't be used to wake implants for other companys.
            //
            if (!remote && ((register == 22) && (regVal != 1))) {
                    
                err = "An attempt was made to change the ZL7010X company ID " +
                    "on the base station to something other than 1, which " +
                    "the ADK does not allow.";
                return(-1);
            }
                
            if (BsmWriteMicsReg(bsmId, register, regVal, remote, eiId) < 0) {
                err = errReport("BsmWriteMicsReg(): ", eiId);
                return(-1);
            }
            
            return(0);
        }

        public override int micsReceive(byte[] buf, uint bufSize, ref string err)
        {
            int len;
            
            if ((len = BsmReceiveMics(bsmId, buf, bufSize, eiId)) < 0) {
                err = errReport("Failed BsmReceiveMics(): ", eiId);
                return(-1);
            }
            return(len);
        }

        public override int micsTransmit(byte *data, uint len, ref string err)
        {
            if (BsmTransmitMics(bsmId, data, len, eiId) < 0) {
                err = errReport("Failed BsmTransmitMics(): ", eiId);
                return(-1);
            }
            return(0);
        }

        public override int micsAbort(ref string err)
        {
            if (BsmAbortMics(bsmId, eiId) < 0) {
                err = errReport("Failed BsmAbortMics(): ", eiId);
                return(-1);
            }
            return(0);
        }
        
        public override int micsReset(ref string err)
        {
            if (BsmResetMics(bsmId, eiId) < 0) {
                err = errReport("Failed BsmResetMics(): ", eiId);
                return(-1);
            }
            return(0);
        }

        // If the timeout passed to BsmStartSession() is 0, it will initiate the 
        // operation and return immediately (i.e. it won't wait for the session 
        // to become ready). Thus, for the "Start Session" button, specify timeout 
        // 0 so it will return immediately with no error. For temporary sessions
        // started to perform an operation, recommended timeout is 2 seconds. 
        //
        public int startSession(uint timeout, ref string err)
        {
            // reconfigure mezzanine board to ensure it has the latest config
            if (configMezz(ref err) < 0) return(-1);

            if (BsmStartSession(bsmId, false, timeout, eiId) < 0) {
                err = errReport("Failed BsmStartSession(): ", eiId);
                return(-1);
            }
            return(0);
        }

        public int searchForImplant(bool anyCompany, bool anyImplant, ref string err)
        {
            // reconfigure mezzanine board to ensure it has the latest config
            if (configMezz(ref err) < 0) return(-1);

            if (BsmSearchForImplant(bsmId, false, anyCompany, anyImplant, eiId) < 0) {
                err = errReport("Failed BsmSearchForImplant(): ", eiId);
                return(-1);
            }
            return(0);
        }
        
        public int listenForEmergency(bool startSession, ref string err)
        {
            // reconfigure mezzanine board to ensure it has the latest config
            if (configMezz(ref err) < 0) return(-1);

            if (BsmListenForEmergency(bsmId, startSession, eiId) < 0) {
                err = errReport("Failed BsmListenForEmergency(): ", eiId);
                return(-1);
            }
            return(0);
        }

        public int rssi(bool useIntRssi, uint chan, bool average, ref string err)
        {
            int rssi;

            if ((rssi = BsmRssi(bsmId, useIntRssi, chan, average, eiId)) < 0) {
                err = errReport("Failed BsmRssi(): ", eiId);
                return(-1);
            }           
            return(rssi);
        }

        public int cca(bool average, out BSM_CCA_DATA data, ref string err)
        {
            int clearChan;
            
            fixed(BSM_CCA_DATA *fData = &data) {

                if ((clearChan = BsmCca(bsmId, average, fData,
                    (uint)sizeof(BSM_CCA_DATA), eiId)) < 0) {
                    
                    err = errReport("Failed BsmCca(): ", eiId);
                    return(-1);
                }
            }     
            return(clearChan);
        }

        public override int micsCal(MICS_CALS cal, int chan, bool remote, ref string err)
        {
            if (BsmMicsCal(bsmId, (uint)cal, chan, remote, eiId) < 0) {
                err = errReport("Failed BsmMicsCal(): ", eiId);
                return(-1);
            }
            return(0);
        }

        public override int micsAdc(uint adcInput, bool remote, ref string err)
        {
            int adcResult;
            
            if ((adcResult = BsmMicsAdc(bsmId, adcInput, remote, eiId)) < 0) { 
                err = errReport("Failed BsmMicsAdc(): ", eiId);
                return(-1);
            }
            return(adcResult);
        }
        
        public int startTx245Carrier(bool start, ref string err)
        {
            if (BsmStartTx245Carrier(bsmId, start, eiId) < 0) {
                err = errReport("Failed BsmStartTx245Carrier(): ", eiId);
                return(-1);
            }
            return(0);
        }

        public int setTx245Freq(uint ccFreq, ref string err)
        {
            if (BsmSetTx245Freq(bsmId, ccFreq, eiId) < 0) {
                err = errReport("Failed BsmSetTx245Freq(): ", eiId);
                return(-1);
            }
            return(0);
        }
        
        public int setTx245Power(uint ccPa, ref string err)
        {
            if (BsmSetTx245Power(bsmId, ccPa, eiId) < 0) {
                err = errReport("Failed BsmSetTx245Power(): ", eiId);
                return(-1);
            }
            return(0);
        }

        public int writeCcReg(uint regAddr, uint regVal, ref string err)
        {
            if (BsmWriteCcReg(bsmId, regAddr, regVal, eiId) < 0) {
                err = errReport("Failed BsmWriteCcReg(): ", eiId);
                return(-1);
            }
            return(0);
        }

        public int readCcReg(uint regAddr, ref string err)
        {
            int val;

            if ((val = BsmReadCcReg(bsmId, regAddr, eiId)) < 0) {
                err = errReport("Failed BsmReadCcReg(): ", eiId);
                return(-1);
            }
            return(val);
        }

        public override int startTx400Carrier(bool start, uint chan, ref string err)
        {
            if (BsmStartTx400Carrier(bsmId, start, chan, eiId) < 0) {
                err = errReport("Failed BsmStartTx400Carrier(): ", eiId);
                return(-1);
            }
            return(0);
        }

        public override int enabExtRssi(bool enab, uint chan, ref string err)
        {
            if (BsmEnabExtRssi(bsmId, enab, chan, eiId) < 0) {
                err = errReport("Failed BsmEnabExtRssi(): ", eiId);
                return(-1);
            }
            return(0);
        }

        public override int startBerRx(bool start, uint chan, MOD rxMod, ref string err)
        {
            uint rxModDll = buildModUser(MOD.MOD_2FSK_FB, rxMod, 0);
            
            if (BsmStartBerRx(bsmId, start, chan, rxModDll, eiId) < 0) {
                err = errReport("Failed BsmStartBerRx(): ", eiId);
                return(-1);
            }
            return(0);
        }
        
        public override int copyMicsRegs(bool remote, ref string err)
        {
            if (BsmCopyMicsRegs(bsmId, remote, eiId) < 0) {
                err = errReport("Failed BsmCopyMicsRegs(): ", eiId);
                return(-1);
            }
            return(0);
        }
        
        public override int calcMicsCrc(bool remote, ref string err)
        {
            if (BsmCalcMicsCrc(bsmId, remote, eiId) < 0) {
                err = errReport("Failed BsmCalcMicsCrc(): ", eiId);
                return(-1);
            }
            return(0);
        }
        
        public BsmStatChanges getStatChanges(bool force, ref string err)
        {
            BSM_STAT_CHANGES bscDll;  // status changes from API/DLL
            BsmStatChanges bsc = this.bsmStatChanges;
            bool boolVal;
            
            // get status changes from base station
            if (BsmGetStatChanges(bsmId, force, out bscDll, (uint)sizeof(BSM_STAT_CHANGES), eiIdStat) < 0) {
                err = errReport("Failed BsmGetStatChanges(): ", eiIdStat);
                return(null);
            }
            // if the base station reported an error, set error in status
            if ((bscDll.genStat.flags & BSM_ERR_OCCURRED) != 0) {
                bsc.bsmErr = errReport("Error on base station: ", eiIdStat);
            } else {
                bsc.bsmErr = null;
            }
            
            // update the MICS state in the status
            updateMicsState(force, (MICS_STATE)bscDll.genStat.micsState, bsc);
            
            // update the 400 MHz TX active flag in the status
            boolVal = ((bscDll.genStat.flags & BSM_400_MHZ_TX_ACTIVE) != 0);
            updateTx400Active(force, boolVal, bsc);
            
            // update the 400 MHz RX active flag in the status
            boolVal = ((bscDll.genStat.flags & BSM_400_MHZ_RX_ACTIVE) != 0);
            updateRx400Active(force, boolVal, bsc);
            
            // update the 2.45 GHz TX state in the status
            boolVal = ((bscDll.genStat.flags & BSM_245_GHZ_TX_ACTIVE) != 0);
            if ((boolVal != bsc.tx245Active) || force) {
                bsc.tx245Active = boolVal;
                bsc.tx245ActiveChanged = true;
            } else {
                bsc.tx245ActiveChanged = false;
            }
            
            // if the link status changed, update it in the status
            if ((bscDll.genStat.flags & BSM_LINK_STAT_CHANGED) != 0) {
                updateLinkStat(ref bscDll.linkStat, bsc.linkStat);
                bsc.linkStatChanged = true;
            } else {
                bsc.linkStatChanged = false;
            }
            
            // if the link qulity changed, update it in the status
            if ((bscDll.genStat.flags & BSM_LINK_QUAL_CHANGED) != 0) {
                updateLinkQual(ref bscDll.linkQual, bsc.linkQual);
                bsc.linkQualChanged = true;
            } else {
                bsc.linkQualChanged = false;
            }
            
            // if the data test status changed, update it in the status
            if ((bscDll.genStat.flags & BSM_DATA_STAT_CHANGED) != 0) {
                bsc.dataStat = bscDll.dataStat;
                bsc.dataStatChanged = true;
            } else {
                bsc.dataStatChanged = false;
            } 
            
            // return changes in status for base station
            return(bsc);
        }
        
        public override int startDataTest(ref MICS_DT_SPECS specs, ref string err)
        {
            if ((BsmStartDataTest(bsmId, ref specs, (uint)sizeof(MICS_DT_SPECS), eiId)) < 0) {
                err = errReport("Failed BsmStartDataTest(): ", eiId);
                return(-1);
            }
            return(0);
        }
        
        public int getTraceString(ref string traceStr)
        {
            if ((BsmGetTraceMsg(bsmId, traceBuf, 120, eiId)) < 0) {
                traceStr = errReport("Failed BsmGetTraceMsg(): ", eiId);
                return(-1);
            }
            
            for(int i = 0; i < 125; i++) {
                if ((char)traceBuf[i] == '\0') break;
                traceStr += (char)traceBuf[i];
            };

            return(0);
        }
        
        public int wakeup(ref string err)
        {
            int val = 0;

            if ((val = BsmWakeup(bsmId, eiId)) < 0) {
                err = errReport("Failed BsmWakeup(): ", eiId);
            }
            return(val);
        }

        public int sleep(ref string err)
        {
            int val = 0;

            if ((val = BsmSleep(bsmId, eiId)) < 0) {
                err = errReport("Failed BsmSleep(): ", eiId);
            }
            return(val);
        }

        // This checks if the specified node NVM ("nvm") is valid, and if it is
        // the same version (size) as the current BSM_NVM structure for the host
        // (PC) software. If so, it returns 0. If the specified NVM is an older
        // (smaller) version of the structure, it sets the specified "err" string
        // to an error message and returns BSM_NVM_RSTAT.NVM_IS_OLDER_VER. If any
        // other error occurs (e.g. the specified NVM is invalid, or is a newer
        // (larger) version that the host (PC) software does not support), it
        // sets the specified "err" string to an error message and returns
        // BSM_NVM_RSTAT.NVM_OTHER_ERR.
        //
        public static BSM_NVM_RSTAT checkNvm(byte[] nvm, out string err)
        {
            err = null;
            BSM_NVM_RSTAT nnrStat;

            fixed (void* fNvm = nvm)
            {
                if ((nnrStat = (BSM_NVM_RSTAT)BsmCheckNvm(fNvm, eiId)) < 0) {
                    err = errReport("Failed BsmCheckNvm(): ", eiId);
                }
                return(nnrStat);
            }
        }

        // This transfers the settings from the specified previous NVM ("prevNvm")
        // to the specified BsmNvm object ("nvm"). Note that the caller must initialize
        // everything in the BsmNvm object before passing it to this method, including
        // the NVM headers, and default values for all settings. If the previous NVM is
        // an older (smaller) version of the BSM_NVM structure that does not contain
        // all of the settings that exist in the current BsmNvm object, this method
        // leaves the new settings in the BsmNvm object at the default values initialized
        // by the caller. If an error occurs (e.g. one of the NVMs is invalid, or the
        // previous NVM is a newer (larger) version that the host (PC) software doesn't
        // support), this sets the specified "err" string to an error message and
        // returns -1.
        //
        public static int transferNvm(byte[] prevNvm, BsmNvm nvm, out string err)
        {
            err = null;

            fixed (void* fPrevNvm = prevNvm)
            {
                uint nvmSize = (uint)Marshal.SizeOf(nvm);

                if (BsmTransferNvm(fPrevNvm, nvm, nvmSize, eiId) < 0) {
                    err = errReport("Failed BsmTransferNvm(): ", eiId);
                    return(-1);
                }
                return(0);
            }
        }

        // This reads the MICS factory NVM from the base station.
        //
        public int getMicsFactNvm(BsmMicsFactNvm mfn, ref string err)
        {
            int size = Marshal.SizeOf(typeof(BsmMicsFactNvm));

            if (BsmGetMicsFactNvm(bsmId, mfn, (uint)size, eiId) < 0) {
                err = errReport("Failed BsmGetMicsFactNvm(): ", eiId);
                return(-1);
            }

            return(0);
        }
    }
}
