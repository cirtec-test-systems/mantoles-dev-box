//
// This file contains the CommonApi class which contains things that are
// common to the BsmApi and ImApi classes. The ZL7010X ADK GUI uses the BsmApi
// and ImApi classes to interface to base station library and implant library
// in the ZL7010X ADK C API (DLL), which in turn communicate with the mezzanine
// boards on the base station module (BSM) and implant module (IM) via USB.
// The ZL7010X ADK GUI also uses the CommonApi class to interface to the ADP
// board library in the ZL7010X ADK C API (DLL), which communicates with the
// ADP board on each module. Note that the CommonApi class is not instantiated.
// Rather, it is inherited by the module-specific BsmApi and ImApi classes
// which customize and extend it for each module.
//
// Note that some of the data types (enum, struct) in this class are passed to
// functions in the C API DLL. These must match the corresponding types in the
// C API, and must not be altered except as needed to reflect changes in the
// C API. For descriptions of these types, see the corresponding types in the
// C API. Note that the names used for these types are generally the same as
// the names used in the C API.
// 
// Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2012. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.
 
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Globalization; // for NumberStyles
using Zarlink.Adp.General;

namespace Zarlink.Adk.Api
{
    unsafe public abstract class CommonApi
    {
        // name of DLL that contains the C API functions
        private const string C_API_DLL = Build.BuildSettings.C_API_DLL;
        // 
        // Declarations for the unmanaged API/DLL functions. Each declaration
        // corresponds to a function in the API/DLL.
        //
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern sbyte* AdkGetApiVer();
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern bool AdkIsStackReg(uint regAddr, uint micsRev);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern bool AdkIsCrcReg(uint regAddr, uint micsRev);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern void EiInitSpecs(out EI_SPECS specs, int specsSize);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern void* EiOpen(ref EI_SPECS specs, int specsSize);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern sbyte* EiGetMsg(void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int EiClose(void *e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern void AdpInitSpecs(out ADP_SPECS specs, int specsSize);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern void* AdpOpen(ref ADP_SPECS specs, int specsSize, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int AdpClose(void* adpId, void *e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int AdpGetStat(void* adpId, out ADP_STAT adpStat, int adpStatSize, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int AdpGetBoardVer(void* adpId, sbyte[] ver, int size, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int AdpSetVsup(void* adpId, uint vsup1Mv, uint vsup2Mv, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int AdpGetConfig(void* adpId, out ADP_CONFIG ac, int acSize, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int AdpGetTraceMsg(void* adpId, sbyte[] buf, uint bufSize, void* e);
        
        // TX/RX modulation mode
        public enum MOD : byte
        {
            // note these must match the indexes in the GUI selection boxes
            MOD_2FSK_FB = 0,
            MOD_2FSK = 1,
            MOD_4FSK = 2,
            MOD_BARKER_5 = 3,  // ZL70103
            MOD_BARKER_11 = 4,  // ZL70103
            MOD_UNKNOWN = 5,
        }
        // strings to display for TX/RX modulations
        public const string MOD_2FSK_FB_STR = "2FSKFB";
        public const string MOD_2FSK_STR = "2FSK";
        public const string MOD_4FSK_STR = "4FSK";
        public const string MOD_BARKER_5_STR = "Barker 5";
        public const string MOD_BARKER_11_STR = "Barker 11";
        public const string MOD_UNKNOWN_STR = "Unknown";
        //
        public string[] modStrings = new string[] {MOD_2FSK_FB_STR, MOD_2FSK_STR,
            MOD_4FSK_STR, MOD_BARKER_5_STR, MOD_BARKER_11_STR, MOD_UNKNOWN_STR};
        //
        // Bits in reg_mac_moduser register. For descriptions, see defines
        // for MAC_MODUSER in "ZL7010XAdk\Sw\Includes\Adk\AnyMezz\MicsHw.h".
        //
        private const byte TX_MOD_MASK           = 0x03;
        private const byte TX_MOD_2FSK_FB        = 0x00;
        private const byte TX_MOD_BARKER_103     = 0x01;  // ZL70103
        private const byte TX_MOD_2FSK           = 0x02;
        private const byte TX_MOD_4FSK           = 0x03;
        //
        private const byte RX_MOD_MASK           = 0x0C;
        private const byte RX_MOD_2FSK_FB        = 0x00;
        private const byte RX_MOD_BARKER_103     = 0x04;  // ZL70103
        private const byte RX_MOD_2FSK           = 0x08;
        private const byte RX_MOD_4FSK           = 0x0C;
        //
        private const byte USER_WAKEUP_DATA_MASK = 0x70;
        private const byte BARKER_TYPE_MASK      = 0x70;  // ZL70103
        private const byte TX_BARKER_5_103       = 0x10;  // ZL70103
        private const byte RX_BARKER_5_103       = 0x20;  // ZL70103
        
        // MICS chip revision
        public enum MICS_REV : byte
        {
            ZL70100 = 0,
            ZL70101 = 1,
            ZL70102 = 4,
            ZL70103 = 5
        }
        public MICS_REV MicsRev
        {
            get { return(micsRev); }
            set { micsRev = value; }
        }
        private MICS_REV micsRev = MICS_REV.ZL70103;
        
        // bit to set in a MICS register address to access a page 2 register
        public const uint P2 = 0x80;

        // MICS state. For a description of each state for an implant, see
        // IM_SLEEPING, IM_IDLE, etc. in "ZL7010XAdk\Sw\Includes\Adk\ImMezz\
        // ImGeneral.h", and for a base, see BSM_IDLE, BSM_STARTING_SESSION,
        // etc. in "ZL7010XAdk\Sw\Includes\Adk\BsmMezz\BsmGeneral.h".
        //
        public enum MICS_STATE : byte
        {
            SLEEPING = 0,                      // base & implant
            IDLE = 1,                          // base & implant   
            SEARCHING_FOR_IMPLANT = 2,         // base only
            STARTING_SESSION = 3,              // base only
            LISTENING_FOR_EMERGENCY = 4,       // base only
            SENDING_EMERGENCY = 5,             // implant only
            IN_SESSION = 6,                    // base & implant 
            SENDING_WAKEUP_RESPONSES = 8       // implant only
        }

        public enum MICS_CALS: uint
        {
            // bits to select calibrations to perform
            CAL_WAKEUP_STROBE_OSC = 0x01,
            CAL_TX_IF_OSC = 0x02,
            CAL_FM_DETECT_AND_RX_IF = 0x04,
            CAL_RX_ADC = 0x08,
            CAL_400_MHZ_TX_102 = 0x10,
            CAL_245_GHZ_ANT = 0x10,
            CAL_400_MHZ_RX_102 = 0x20,
            CAL_245_GHZ_ZERO_LEV = 0x20,
            CAL_400_MHZ_ANT = 0x40,
            CAL_24_MHZ_CRYSTAL_OSC = 0x80,
            CAL_2_MHZ_OSC_102 = (0x01 << 8),
            CAL_LNA_FREQ_102 = (0x02 << 8)
        }

        // The API/DLL uses this to hold an IMD transceiver ID (implant ID).
        // For a description, see MICS_IMD_TID in "ZL7010XAdk\Sw\Includes\
        // Adk\AnyMezz\MicsGeneral.h".
        //
        protected struct MICS_IMD_TID
        {
            public byte b3;
            public byte b2;
            public byte b1;
        }
        
        // The API/DLL uses this to hold the link configuration for a base
        // or implant. For field descriptions, see MICS_LINK_CONFIG in
        // "ZL7010XAdk\Sw\Includes\Adk\AnyMezz\MicsGeneral.h".
        //
        protected struct MICS_LINK_CONFIG
        {
            public byte chan;
            public byte modUser;
            public byte txBlockSize;
            public byte rxBlockSize;
            public byte maxBlocksPerTxPack;
        }
        
        // These are used to hold the link status for a base or implant.
        // The API/DLL uses MICS_LINK_STAT, which is converted to MicsLinkStat
        // for public reference. For field descriptions, see MICS_LINK_STAT in
        // "ZL7010XAdk\Sw\Includes\Adk\AnyMezz\MicsGeneral.h".
        //
        public class MicsLinkStat
        {
            public uint imdTid;
            public byte companyId;
            public byte chan;
            public MOD txMod;
            public MOD rxMod;
            public byte txBlockSize;
            public byte rxBlockSize;
            public byte maxBlocksPerTxPack;
        }
        protected struct MICS_LINK_STAT
        {
            public MICS_IMD_TID imdTid;  // IMD transceiver ID
            public byte companyId;
            public byte chan;
            public byte modUser;
            public byte txBlockSize;
            public byte rxBlockSize;
            public byte maxBlocksPerTxPack;
        }
        
        // These are used to hold the link quality for a base or implant.
        // The API/DLL uses MICS_LINK_QUAL, which is converted to MicsLinkQual
        // for public reference. For field descriptions, see MICS_LINK_QUAL in
        // "ZL7010XAdk\Sw\Includes\Adk\AnyMezz\MicsGeneral.h".
        //
        public class MicsLinkQual
        {
            public bool maxBErrInts;
            public bool maxRetriesInts;
            public ushort errCorBlocks;
            public ushort crcErrs;
        }
        //protected struct MICS_LINK_QUAL
        public struct MICS_LINK_QUAL
        {
            public ushort maxBErrInts;     // boolean (true if != 0)
            public ushort maxRetriesInts;  // boolean (true if != 0)
            public ushort errCorBlocks;
            public ushort crcErrs;
        }
        
        // This is used to hold the MICS data test specifications. For a
        // description of the fields, see MICS_DT_SPECS in "ZL7010XAdk\Sw\
        // Includes\Adk\AnyMezz\MicsGeneral.h".
        //
        public struct MICS_DT_SPECS
        {
            public byte options;
            public byte txData;
            public ushort txBlockCount;
            public byte rxData;
        }
        // Bits in MICS_DT_SPECS.options. For descriptions, see MICS_DT_TX,
        // etc. in "ZL7010XAdk\Sw\Includes\Adk\AnyMezz\MicsGeneral.h".
        //
        public const byte MICS_DT_TX       = (1 << 0);
        public const byte MICS_DT_RX       = (1 << 1);
        public const byte MICS_DT_VALIDATE = (1 << 2);
        public const byte MICS_DT_INC_TX   = (1 << 3);
        public const byte MICS_DT_INC_RX   = (1 << 4);
        
        // Data test status for base or implant. For field descriptions, see
        // MICS_DATA_STAT in "ZL7010XAdk\Sw\Includes\Adk\AnyMezz\MicsGeneral.h".
        //
        public struct MICS_DATA_STAT
        {
            public ushort txBlockCount;
            public ushort rxBlockCount;
            public ushort dataErrCount;
        }
        
        // This is used to report changes in the status for a base station or
        // implant. This is extended by BsmApi.BsmStatChanges for a base station
        // (see BsmApi.getStatChanges()), and by ImApi.ImStatChanges for an
        // implant (see ImApi.getStatChanges()).
        //
        public class StatChanges {
        
            public bool micsStateChanged;
            public MICS_STATE micsState;
            
            public bool tx400ActiveChanged;
            public bool tx400Active;
            
            public bool rx400ActiveChanged;
            public bool rx400Active;
            
            public bool linkStatChanged;
            public MicsLinkStat linkStat = new MicsLinkStat();
            
            public bool linkQualChanged;
            public MicsLinkQual linkQual = new MicsLinkQual();
            
            public bool dataStatChanged;
            public MICS_DATA_STAT dataStat = new MICS_DATA_STAT();
        }

        private struct ADP_CONFIG
        {
            public byte flags;
            public byte unused1;
            public ADP_POW_CONFIG pow;
        }

        private struct ADP_POW_CONFIG
        {
            public ushort vsup1Mv;
            public ushort vsup2Mv;
        }

        private struct ADP_SPECS
        {
            uint magic;
            public bool remote;
            public byte* localMezzDevType;    
            public byte* localMezzDevName;
            public uint defTimeoutMs;
            public void* localAdpId;   
        }
        
        // The API/DLL uses this to hold the general status for an ADP board.
        // For field descriptions, see ADP_STAT in "AppDevPlat\Sw\Includes\
        // Adp\AdpBoard\AdpGeneral.h".
        //
        private struct ADP_STAT
        {
            public byte flags;  // see ADP_ERR_OCCURRED, etc. below
            public byte unused1;
            public ushort vsup1Adc;
            public ushort vsup2Adc;
        }
        // Bits in ADP_STAT.flags. For descriptions, see ADP_ERR_OCCURRED,
        // etc. in "AppDevPlat\Sw\Includes\Adp\AdpBoard\AdpGeneral.h".
        //
        private const byte ADP_ERR_OCCURRED = (1 << 0);
        
        // This is used to hold changes in the ADP board status (see
        // getAdpStatChanges()). For field descriptions, see ADP_STAT in
        // "AppDevPlat\Sw\Includes\Adp\AdpBoard\AdpGeneral.h".
        //
        public class AdpStatChanges
        {
            // non-null if the ADP board detected an error
            public string adpErr; 
            
            public bool vsup1MvChanged;
            public ushort vsup1Mv;
            
            public bool vsup2MvChanged;
            public ushort vsup2Mv;
        }
        // buffer to hold changes in ADP board status (returned by getAdpStatChanges())
        private AdpStatChanges adpStatChanges = new AdpStatChanges();

        private struct EI_SPECS
        {
            public uint magic;
        }
        
        // If CfgWriteEachChangeToMod is true, then each time the application
        // (GUI) changes a configuration setting, this API also writes it to
        // the module. This is initially false so as the application initializes
        // its display components to match the configuration read from the
        // module, and the display components' event handlers in turn write
        // the same configuration settings back to this API, this API does not
        // write the configuration settings back to the module. Otherwise,
        // each event handler invoked during initialization would write the
        // configuration settings back to the module for no reason, which would
        // be inefficient, and would abort any operation on the module. When
        // the application finishes initializing its display components, it
        // must set CfgWriteEachChangeToMod to true.
        //
        private bool cfgWriteEachChangeToMod = false;
        public bool CfgWriteEachChangeToMod
        {
            get { return(cfgWriteEachChangeToMod); }
            set { cfgWriteEachChangeToMod = value; }
        }
        // Configuration settings for the ADP board.
        //
        private ushort cfgVsup1Mv;
        public ushort CfgVsup1Mv
        {
            get { return(cfgVsup1Mv); }
            set {
                cfgVsup1Mv = value; 
                configVsupAndReportAnyErr();
            } 
        }
        private ushort cfgVsup2Mv;
        public ushort CfgVsup2Mv
        {
            get { return(cfgVsup2Mv); }
            set {
                cfgVsup2Mv = value;
                configVsupAndReportAnyErr();
            }
        }
        // Configuration settings for the base or implant mezzanine board
        // (the board attached to the ADP board).
        //
        private int cfgImdTid;  // IMD transceiver ID
        public int CfgImdTid
        {
            get { return(cfgImdTid); }
            set {
                cfgImdTid = value; 
                configMezzAndReportAnyErr();
            }
        }
        private byte cfgCompanyId;
        public byte CfgCompanyId
        {
            get { return(cfgCompanyId); }
            set { 
                cfgCompanyId = value;
                configMezzAndReportAnyErr();
            }
        }
        private byte cfgNormChan;
        public byte CfgNormChan
        {
            get { return(cfgNormChan); }
            set {
                cfgNormChan = value; 
                configMezzAndReportAnyErr();
            }
        }
        private MOD cfgNormTxMod;
        public MOD CfgNormTxMod
        {
            get { return(cfgNormTxMod); }
            set {
                cfgNormTxMod = value; 
                configMezzAndReportAnyErr();
            }
        }
        private MOD cfgNormRxMod;
        public MOD CfgNormRxMod
        {
            get { return(cfgNormRxMod); }
            set {
                cfgNormRxMod = value; 
                configMezzAndReportAnyErr();
            }
        }
        private byte cfgNormBlockSize;
        public byte CfgNormBlockSize
        {
            get { return(cfgNormBlockSize); }
            set {
                cfgNormBlockSize = value; 
                configMezzAndReportAnyErr();
            }
        }
        private byte cfgNormMaxBlocksPerTxPack;
        public byte CfgNormMaxBlocksPerTxPack
        {
            get { return(cfgNormMaxBlocksPerTxPack); }
            set {
                cfgNormMaxBlocksPerTxPack = value; 
                configMezzAndReportAnyErr();
            }
        }
        private byte cfgEmerChan;
        public byte CfgEmerChan
        {
            get { return(cfgEmerChan); }
            set {
                cfgEmerChan = value; 
                configMezzAndReportAnyErr();
            }
        }
        private MOD cfgEmerTxMod;
        public MOD CfgEmerTxMod
        {
            get { return(cfgEmerTxMod); }
            set {
                cfgEmerTxMod = value; 
                configMezzAndReportAnyErr();
            }
        }
        private MOD cfgEmerRxMod;
        public MOD CfgEmerRxMod
        {
            get { return(cfgEmerRxMod); }
            set {
                cfgEmerRxMod = value; 
                configMezzAndReportAnyErr();
            }
        }
        private byte cfgEmerBlockSize;
        public byte CfgEmerBlockSize
        {
            get { return(cfgEmerBlockSize); }
            set {
                cfgEmerBlockSize = value; 
                configMezzAndReportAnyErr();
            }
        }
        private byte cfgEmerMaxBlocksPerTxPack;
        public byte CfgEmerMaxBlocksPerTxPack
        {
            get { return(cfgEmerMaxBlocksPerTxPack); }
            set {
                cfgEmerMaxBlocksPerTxPack = value; 
                configMezzAndReportAnyErr();
            }
        }
        private bool cfg400MHzWakeup;
        public bool Cfg400MHzWakeup
        {
            get { return(cfg400MHzWakeup); }
            set {
                cfg400MHzWakeup = value;
                configMezzAndReportAnyErr();
            }
        }
        
        // set to true if this interface is successfully initialized
        protected bool initialized = false;
        
        // Error information objects for the API/DLL. These are passed to the
        // C API functions, which use them to return error information if an
        // error occurs. These objects are used as follows:
        // 
        //     eiId: This is used by all non-status-polling methods.
        // 
        //     eiIdStat: This is used by all status-polling methods in case the
        //     status polling runs in a separate asynchronous thread. That way,
        //     if an error happens to occur in both the main processing thread
        //     and the status-polling thread at the same time, the returned
        //     error information won't conflict.
        //
        protected static void* eiId;
        protected static void* eiIdStat;
        
        // connection to ADP board (returned by AdpOpen())
        public void* adpId = null;
        
        // version string read from ADP board
        private string adpVer = "";
        
        // The device type for the mezzanine board attached to the ADP board.
        // This is initialized by the child class (BsmApi or ImApi) when it's
        // created, and is later passed to AdpOpen() to connect to the ADP
        // board. AdpOpen() connects to the first ADP board it finds that has
        // the specified type of mezzanine board attached to it.
        //
        protected string mezzDevType = "";
        
        // version string read from mezzanine board
        protected string mezzVer = "";
        
        // buffer to hold a trace message
        protected sbyte[] traceBuf = new sbyte[125];
        
        // This static constructor performs one-time initialization for the
        // CommonApi class. This is called automatically the first time the
        // CommonApi class is referenced.
        //
        static CommonApi()
        {
            EI_SPECS eiSpecs;
            
            // Call a function in the C API DLL to confirm that the C API DLL
            // and any other DLL's it depends on all exist.
            try {
                AdkGetApiVer();
                
            } catch (DllNotFoundException) {
            
                MessageBox.Show("One of the required DLL's could not be found. " +
                    "Make sure the ZL7010X ADK DLL exists and the USB driver was installed " +
                    "properly the first time an ADK module was connected to the PC.");
                return;    
            }
            
            // Create error information objects for the API/DLL (for more 
            // information, see the comments for eiId and eiIdStat). Note that
            // this is done after checking for the required C API DLLs above,
            // because if a DLL is missing, the logic above displays a helpful
            // error message, whereas this logic would throw a cryptic
            // exception with no indication that a DLL is missing.
            //
            EiInitSpecs(out eiSpecs, sizeof(EI_SPECS));
            if ((eiId = EiOpen(ref eiSpecs, sizeof(EI_SPECS))) == null) {
                throw new Exception("Failed EiOpen().");
            }
            if ((eiIdStat = EiOpen(ref eiSpecs, sizeof(EI_SPECS))) == null) {
                throw new Exception("Failed EiOpen().");
            }
        }

        public CommonApi()
        {
        }
        
        // This is called to perform initialization common to the base station
        // and implant module interfaces (BsmApi and ImApi). Returns 0 for
        // success, -1 for error.
        //
        public virtual int init(ref string err)
        {
            ADP_SPECS adpSpecs;
            ADP_CONFIG adpConfig;

            // init specs to pass to AdpOpen() with default values
            AdpInitSpecs(out adpSpecs, sizeof(ADP_SPECS));
            //
            // Connect to ADP board. "adpSpecs.localMezzDevType" is set to the
            // device type (this.mezzDevType) to tell AdpOpen() the type of
            // mezzanine board attached to the ADP board (base or implant).
            // AdpOpen() will search for the first ADP board that has the
            // specified type of mezzanine board attached to it.
            //
            byte[] mezzDevTypeBuf = Util.convertStrToAsciiBytes(mezzDevType); 
            fixed(byte *fMezzDevTypeBuf = &mezzDevTypeBuf[0]) {
                adpSpecs.localMezzDevType = fMezzDevTypeBuf;
                adpId = AdpOpen(ref adpSpecs, sizeof(ADP_SPECS), eiId);
            }
            if (adpId == null) {
                err = errReport("Failed AdpOpen(): ", eiId);
                goto error;
            }
            
            // get ADP board version
            sbyte[] adpVerBuf = new sbyte[125];
            if ((AdpGetBoardVer(adpId, adpVerBuf, 120, eiId)) < 0) {
                err = errReport("Failed AdpGetBoardVer(): ", eiId);
                goto error;
            }
            for(int i = 0; i<125; i++) {
                if ((char)adpVerBuf[i] == '\0') break;
                adpVer += (char)adpVerBuf[i];
            };

            // get config from ADP board and init config properties
            if ((AdpGetConfig(adpId, out adpConfig, sizeof(ADP_CONFIG), eiId)) < 0) {
                err = errReport("Failed AdpGetConfig(): ", eiId);
                goto error;
            }
            CfgVsup1Mv = adpConfig.pow.vsup1Mv;
            CfgVsup2Mv = adpConfig.pow.vsup2Mv;

            return(0);
error:
            // Close any API/DLL interfaces that were opened. This frees any
            // resources allocated by the API functions, and closes any board
            // connections so they're available to be re-opened later if desired.
            //
            string closeErr = "";
            close(ref closeErr);
            return(-1);
        }
        
        // This is called to close any API/DLL interfaces that were opened by
        // the common API (CommonApi). This frees any resources allocated by the
        // API functions, and closes any board connections so they're available
        // to be re-opened later if desired. Returns 0 for success, -1 for error.
        //
        public virtual int close(ref string err)
        {
            int retVal = 0;
            
            if (adpId != null) {
            
                if (AdpClose(adpId, eiId) < 0) {
                    err = errReport("Failed AdpClose(): ", eiId);
                    retVal = -1;
                }
                adpId = null;
            }
            
            initialized = false;
            
            return(retVal);
        }
        
        // Build value for reg_mac_moduser register.
        //
        protected byte buildModUser(MOD txMod, MOD rxMod, byte wake245UserData)
        {
            byte modUser = 0;
            bool barker = false;
            
            // set TX modulation in modUser
            switch(txMod) {
            case MOD.MOD_2FSK_FB:
                modUser |= TX_MOD_2FSK_FB; break;
            case MOD.MOD_2FSK:
                modUser |= TX_MOD_2FSK; break;
            case MOD.MOD_4FSK:
                modUser |= TX_MOD_4FSK; break;
            case MOD.MOD_BARKER_5:  // ZL70103
                modUser |= (TX_MOD_BARKER_103 | TX_BARKER_5_103);
                barker = true;
                break;
            case MOD.MOD_BARKER_11: // ZL70103
                modUser |= TX_MOD_BARKER_103;
                barker = true;
                break;
            default:
                break;
            }
            
            // set RX modulation in modUser
            switch(rxMod) {
            case MOD.MOD_2FSK_FB:
                modUser |= RX_MOD_2FSK_FB; break;
            case MOD.MOD_2FSK:
                modUser |= RX_MOD_2FSK; break;
            case MOD.MOD_4FSK:
                modUser |= RX_MOD_4FSK; break;
            case MOD.MOD_BARKER_5:  // ZL70103
                modUser |= (RX_MOD_BARKER_103 | RX_BARKER_5_103);
                barker = true;
                break;
            case MOD.MOD_BARKER_11: // ZL70103
                modUser |= RX_MOD_BARKER_103;
                barker = true;
                break;
            default:
                break;
            }
            
            // if not using Barker mode, set user data for 2.45 GHz wakeup
            if (!barker) modUser |= (byte)(wake245UserData << 4);
            
            return(modUser);
        }

        // Extract TX modulation from specified reg_mac_moduser register value.
        //
        protected MOD extractTxMod(byte modUser)
        {
            switch(modUser & TX_MOD_MASK) {
            case TX_MOD_2FSK_FB:
                return(MOD.MOD_2FSK_FB);
            case TX_MOD_2FSK:
                return(MOD.MOD_2FSK);
            case TX_MOD_4FSK:
                return(MOD.MOD_4FSK);
            case TX_MOD_BARKER_103:
                if ((modUser & TX_BARKER_5_103) != 0) {
                    return(MOD.MOD_BARKER_5);
                } else {
                    return(MOD.MOD_BARKER_11);
                }
            default:    
                return(MOD.MOD_UNKNOWN);
            }
        }
        
        // Extract RX modulation from specified reg_mac_moduser register value.
        //
        protected MOD extractRxMod(byte modUser)
        {
            switch(modUser & RX_MOD_MASK) {
            case RX_MOD_2FSK_FB:
                return(MOD.MOD_2FSK_FB);
            case RX_MOD_2FSK:
                return(MOD.MOD_2FSK);
            case RX_MOD_4FSK:
                return(MOD.MOD_4FSK);
            case RX_MOD_BARKER_103:
                if ((modUser & RX_BARKER_5_103) != 0) {
                    return(MOD.MOD_BARKER_5);
                } else {
                    return(MOD.MOD_BARKER_11);
                }
            default:    
                return(MOD.MOD_UNKNOWN);
            }
        }
        
        public AdpStatChanges getAdpStatChanges(bool force, ref string err)
        {
            AdpStatChanges asc = this.adpStatChanges;
            ADP_STAT asDll;  // ADP board status from API/DLL
            ushort vsup1Mv, vsup2Mv;
        
            // get status from ADP board via API/DLL
            if (AdpGetStat(adpId, out asDll, sizeof(ADP_STAT), eiIdStat) < 0) {
                err = errReport("Failed AdpGetStat(): ", eiIdStat);
                return(null);
            }
            // if the ADP board reported an error, set error in status
            if ((asDll.flags & ADP_ERR_OCCURRED) != 0) {
                asc.adpErr = errReport("Error on ADP board: ", eiIdStat);
            } else {
                asc.adpErr = null;
            }
            
            // Convert the ADC values for VSUP1 and VSUP2 to millivolts, then
            // update them in the status. In the equations, 2.5 is the internal
            // ADC reference voltage, 1.43 is the external voltage divider used
            // to scale VSup1&2 down to 2.5 volts, and 0x0FFF is the ADC value
            // at the maximum input voltage (12 bit ADC).
            //
            vsup1Mv = (ushort)((asDll.vsup1Adc * 2.5 * 1.43 * 1000) / 0xFFF);
            vsup2Mv = (ushort)((asDll.vsup2Adc * 2.5 * 1.43 * 1000) / 0xFFF);
            //
            if ((vsup1Mv != asc.vsup1Mv) || force) {
                asc.vsup1Mv = vsup1Mv;
                asc.vsup1MvChanged = true;
            } else {
                asc.vsup1MvChanged = false;
            }
            if ((vsup2Mv != asc.vsup2Mv) || force) {
                asc.vsup2Mv = vsup2Mv;
                asc.vsup2MvChanged = true;
            } else {
                asc.vsup2MvChanged = false;
            }
            
            // return changes in status for ADP board
            return(asc);
        }
        
        protected void updateMicsState(bool force, MICS_STATE micsState, StatChanges sc)
        {
            if ((micsState != sc.micsState) || force) {
                sc.micsState = micsState;
                sc.micsStateChanged = true;
            } else {
                sc.micsStateChanged = false;
            }
        }
        
        protected void updateTx400Active(bool force, bool tx400Active, StatChanges sc)
        {
            if ((tx400Active != sc.tx400Active) || force) {
                sc.tx400Active = tx400Active;
                sc.tx400ActiveChanged = true;
            } else {
                sc.tx400ActiveChanged = false;
            }
        }
        
        protected void updateRx400Active(bool force, bool rx400Active, StatChanges sc)
        {
            if ((rx400Active != sc.rx400Active) || force) {
                sc.rx400Active = rx400Active;
                sc.rx400ActiveChanged = true;
            } else {
                sc.rx400ActiveChanged = false;
            }
        }
        
        // This is called to convert a MICS_LINK_STAT returned by the API/DLL
        // to a MicsLinkStat for public reference.
        //
        protected void updateLinkStat(ref MICS_LINK_STAT lsDll, MicsLinkStat ls)
        {
            ls.chan = lsDll.chan;
            ls.companyId = lsDll.companyId;
            ls.imdTid = (uint)((lsDll.imdTid.b3 << 16) + (lsDll.imdTid.b2 << 8) + lsDll.imdTid.b1);
            ls.maxBlocksPerTxPack = lsDll.maxBlocksPerTxPack;
            ls.rxMod = extractRxMod(lsDll.modUser);
            ls.txMod = extractTxMod(lsDll.modUser);
            ls.txBlockSize = lsDll.txBlockSize;
            ls.rxBlockSize = lsDll.rxBlockSize;
        }
        
        // This is called to convert a MICS_LINK_QUAL returned by the API/DLL
        // to a MicsLinkQual for public reference.
        //
        protected void updateLinkQual(ref MICS_LINK_QUAL lqDll, MicsLinkQual lq)
        {
            lq.maxBErrInts = (lqDll.maxBErrInts != 0) ? true : false;
            lq.maxRetriesInts = (lqDll.maxRetriesInts != 0) ? true : false;
            lq.errCorBlocks = lqDll.errCorBlocks;
            lq.crcErrs = lqDll.crcErrs;
        }
        
        // Whenever a configuration property for the mezzanine board is
        // changed, its "set" method calls this to reconfigure the mezzanine
        // board (base station or implant).
        //
        protected void configMezzAndReportAnyErr()
        {
            string err = "";
            
            if (!initialized) return;
            if (!cfgWriteEachChangeToMod) return;
            
            if (configMezz(ref err) < 0) {
                MessageBox.Show("Failed to configure ZL7010X mezzanine board: " + err);
            }
        }
        // Whenever CfgVsup1Mv or CfgVsup2Mv are changed, their "set" methods
        // call this to reconfigure VSUP1 and VSUP2 on the ADP board.
        //
        private void configVsupAndReportAnyErr()
        {
            if (!initialized) return;
            if (!cfgWriteEachChangeToMod) return;
            
            if (AdpSetVsup(adpId, cfgVsup1Mv, cfgVsup2Mv, eiId) < 0) {
            
                string err = errReport("Failed AdpSetVsup(): ", eiId);
                MessageBox.Show("Failed to set VSUP1/VSUP2 on ADP board: " + err);
            }
        }
        
        public int getAdpTraceString(ref string traceStr)
        {
            if ((AdpGetTraceMsg(adpId, traceBuf, 120, eiId)) < 0) {
                traceStr = errReport("Failed AdpGetTraceMsg(): ", eiId);
                return(-1);
            }
            
            for(int i = 0; i < 125; i++) {
                if ((char)traceBuf[i] == '\0') break;
                traceStr += (char)traceBuf[i];
            };

            return(0);
        }
        
        public bool isStackReg(uint register, CommonApi.MICS_REV micsRev)
        {
            return(AdkIsStackReg(register, (uint)micsRev));
        }
        
        public bool isCrcReg(uint register, CommonApi.MICS_REV micsRev)
        {
            return(AdkIsCrcReg(register, (uint)micsRev));
        }
        
        protected static string errReport(string area, void *eiId)
        {
            string err;

            if (eiId != null) {
                // err = area + new string(EiGetMsg(eiId)); // for debugging
                err = new string(EiGetMsg(eiId));
            } else {
                err = area;
            }
            return(err);
        }

        public string getMezzVer()
        {
            return(mezzVer);
        }
        
        // These methods are unique for each type of ADK module, so they must
        // be overridden by the module-specific class (BsmApi or ImApi).
        //
        protected abstract int configMezz(ref string err);
        public abstract int micsAdc(uint adcInput, bool remote, ref string err);
        public abstract int micsCal(MICS_CALS cal, int chan, bool remote, ref string err);
        public abstract int micsRead(uint register, bool remote, ref string err);
        public abstract int micsWrite(uint register, uint regVal, bool remote, ref string err);
        public abstract int micsAbort(ref string err);
        public abstract int micsReset(ref string err);
        public abstract int micsTransmit(byte* data, uint len, ref string err);
        public abstract int micsReceive(byte[] data, uint len, ref string err);
        public abstract int startTx400Carrier(bool start, uint chan, ref string err);
        public abstract int enabExtRssi(bool enab, uint chan, ref string err);
        public abstract int startBerRx(bool start, uint chan, MOD rxMod, ref string err);
        public abstract int copyMicsRegs(bool remote, ref string err);
        public abstract int calcMicsCrc(bool remote, ref string err);
        public abstract int startDataTest(ref MICS_DT_SPECS specs, ref string err);
    }
}
