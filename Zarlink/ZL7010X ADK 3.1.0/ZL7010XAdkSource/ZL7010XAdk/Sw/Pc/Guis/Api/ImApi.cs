//
// This file contains the ImApi class. The ZL7010X ADK GUI uses this class
// to interface to implant library in the ZL7010X ADK C API (DLL), which
// communicates with the implant mezzanine board on the implant module (IM)
// via USB.
//
// Note that some of the data types (enum, struct) in this class are passed to
// functions in the C API DLL. These must match the corresponding types in the
// C API, and must not be altered except as needed to reflect changes in the
// C API. For descriptions of these types, see the corresponding types in the
// C API. Note that the names used for these types are generally the same as
// the names used in the C API.
//
// Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2012. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.

using System.Windows.Forms;
using System.Runtime.InteropServices; // for DLL
using Zarlink.Adp.Api;

namespace Zarlink.Adk.Api
{
    unsafe public class ImApi : CommonApi
    {
        // name of DLL that contains the C API functions
        private const string C_API_DLL = Build.BuildSettings.C_API_DLL;
        // 
        // Declarations for the unmanaged API/DLL functions. Each declaration
        // corresponds to a function in the API/DLL.
        //
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern void ImInitSpecs(out IM_SPECS specs, int specsSize);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern void* ImOpen(ref IM_SPECS specs, int specsSize, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImEnabHkWrite(void* imId, bool enab, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImSetMicsConfig(void* imId, ref IM_MICS_CONFIG mc, int mcSize, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImGetMicsConfig(void* imId, out IM_MICS_CONFIG mc, int mcSize, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImGetMezzVer(void* imId, sbyte[] buf, int bufSize, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImStartTx400Carrier(void* imId, bool start, uint chan, void* e); 
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImSendEmergency(void* imId, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImWakeup(void* imId, bool stayAwake, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImSleep(void* imId, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImAbortMics(void* imId, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImResetMics(void* imId, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImReadMicsReg(void* imId, uint regAddr, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImWriteMicsReg(void* imId, uint regAddr, uint regVal, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImReceiveMics(void* imId, byte[] buf, uint bufSize, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImTransmitMics(void* imId, byte* data, uint len, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImIntRssi(void* imId, uint chan, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImMicsCal(void* imId, uint cals, int chan, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImEnabExtRssi(void* imId, bool enab, uint chan, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImClose(void* imId, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImMicsAdc(void* imId, uint adcInput, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImStartBerRx(void* imId, bool start, uint chan, uint rxMod, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImCopyMicsRegs(void* imId, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImCalcMicsCrc(void* imId, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImGetStatChanges(void* imId, bool force, out IM_STAT_CHANGES isc, int iscSize, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImStartDataTest(void* imId, ref MICS_DT_SPECS specs, int specsSize, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImGetTraceMsg(void* imId, sbyte[] buf, uint bufSize, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImCheckNvm(void* nvm, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImTransferNvm(void* prevNvm, [In, Out] ImNvm nvm, uint nvmSize, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ImGetMicsFactNvm(void* ImId, [Out] ImMicsFactNvm fn, uint fnSize, void* e);

        private const string IM_AIM_DEV_TYPE = "ZL7010X ADK Implant";
        
        // Class for the header at the beginning of the nonvolatile memory
        // (NVM) on an IM. For field descriptions, see IM_NVM_HEAD in
        // "[SourceTree]\7010XAdk\Sw\Includes\Adk\ImMezz\ImGeneral.h".
        //
        [StructLayout(LayoutKind.Sequential)]
        public class ImNvmHead
        {
            private readonly NvmInfo nvmInfo = new NvmInfo(NvmInfo.NVM_TYPE_10X_ADK_IM,
                    (ushort)Marshal.SizeOf(typeof(ImNvm)));

            private readonly byte nvmHeadSize = (byte)Marshal.SizeOf(typeof(ImNvmHead));

            private readonly byte micsFactNvmOffset = (byte)Marshal.OffsetOf(typeof(ImNvm), "micsFactNvm");
            private readonly byte micsFactNvmSize = (byte)Marshal.SizeOf(typeof(ImMicsFactNvm));

        }
        
        // Class for the nonvolatile memory (NVM) on a IM. For field descriptions,
        // see IM_NVM in "[SourceTree]\7010XAdk\Sw\Includes\Adk\ImMezz\ImGeneral.h".
        //
        [StructLayout(LayoutKind.Sequential)]
        public class ImNvm
        {
            private readonly ImNvmHead head = new ImNvmHead();
            public ImMicsFactNvm micsFactNvm = new ImMicsFactNvm();
        }

        // Structure for the section of the nonvolatile memory that contains
        // the factory settings for the ZL7010X radio on an implant. Note that
        // this structure should contain only fields that are applicable to all
        // applications for all implants that use the ZL7010X radio. It should
        // not contain any fields that are applicable only to specific applications.
        // For field descriptions, see IM_MICS_FACT_NVM in "[SourceTree]\
        // ZL7010XAdk\Sw\Includes\Adk\AnyMezz\MicsGeneral.h"
        //
        [StructLayout(LayoutKind.Sequential)]
        public class ImMicsFactNvm
        {
            public Ud8 rssiToDbmCal = new Ud8(0);
            public Ud8 rxIfAdcDecLev = new Ud8(0);
            public Ud8 xoTrim = new Ud8(0);
            public Ud8 rssiOffsetTrim = new Ud8(0);
        }

        // Values for the return status for checkNvm(). Note that these must
        // match the corresponding values in "[SourceTree]\7010XAdk\Sw\Includes
        // \Pc\ImLib.h".
        //
        public enum IM_NVM_RSTAT : int
        {
            NVM_IS_VALID = 0,
            NVM_IS_OLDER_VER = -1,
            NVM_OTHER_ERR = -2
        }
        
        // The API/DLL uses this to hold the MICS configuration for an implant.
        // For field descriptions, see IM_MICS_CONFIG in "ZL7010XAdk\Sw\
        // Includes\Adk\ImMezz\ImGeneral.h".
        //
        private struct IM_MICS_CONFIG
        {
            public byte flags;  // see IM_ENAB_HK_WRITE, etc. below
            public CommonApi.MICS_IMD_TID imdTid;
            public byte companyId;
            public CommonApi.MICS_LINK_CONFIG normalLinkConfig;
            public CommonApi.MICS_LINK_CONFIG emergencyLinkConfig;
        }
        // Bits in IM_MICS_CONFIG.flags. For descriptions, see IM_ENAB_HK_WRITE,
        // etc. in "ZL7010XAdk\Sw\Includes\Adk\ImMezz\ImGeneral.h".
        //
        private const byte IM_ENAB_HK_WRITE                = (1 << 0);
        private const byte IM_400_MHZ_WAKEUP               = (1 << 1);
        private const byte IM_EXT_STROBE_FOR_245_GHZ_SNIFF = (1 << 2);
        
        // The API/DLL uses this to hold the general status for an implant.
        // For field descriptions, see IM_STAT in "ZL7010XAdk\Sw\Includes\
        // Adk\ImMezz\ImGeneral.h".
        //
        private struct IM_STAT
        {
            public byte flags;  // see IM_ERR_OCCURRED, etc. below
            public byte micsState;  
        }
        // Bits in IM_STAT.flags. For descriptions, see IM_ERR_OCCURRED, etc.
        // in "ZL7010XAdk\Sw\Includes\Adk\ImMezz\ImGeneral.h".
        //
        private const byte IM_ERR_OCCURRED         = (1 << 0);
        private const byte IM_LINK_STAT_CHANGED    = (1 << 1);
        private const byte IM_LINK_QUAL_CHANGED    = (1 << 2);
        private const byte IM_400_MHZ_TX_ACTIVE    = (1 << 3);
        private const byte IM_400_MHZ_RX_ACTIVE    = (1 << 4);
        private const byte IM_RESERVED_STAT_FLAG_5 = (1 << 5);
        private const byte IM_DATA_STAT_CHANGED    = (1 << 6);
        
        // These are used to hold changes in the implant status. The API/DLL
        // uses IM_STAT_CHANGES, which is converted to ImStatChanges for public
        // reference (see ImApi.getStatChanges()). For field descriptions, see
        // IM_STAT_CHANGES in "ZL7010XAdk\Sw\Includes\Adk\ImMezz\ImGeneral.h".
        //
        private struct IM_STAT_CHANGES
        {
            public IM_STAT genStat;
            public CommonApi.MICS_LINK_STAT linkStat; 
            public CommonApi.MICS_LINK_QUAL linkQual; 
            public CommonApi.MICS_DATA_STAT dataStat; 
        }
        public class ImStatChanges : CommonApi.StatChanges
        {
            // non-null if the implant detected an error
            public string imErr; 
        }
        // buffer to hold status changes (returned by ImApi.getStatChanges())
        private ImStatChanges imStatChanges = new ImStatChanges();
        
        private struct IM_SPECS
        {
            public uint magic;
            public void *localAdpId;
            public bool remote;
        }

        // Configuration settings unique to the implant.
        //
        private bool cfgEnabHkWrite;
        public bool CfgEnabHkWrite
        {
            get { return(cfgEnabHkWrite); }
            set {
                cfgEnabHkWrite = value;
                if (initialized)
                {
                    if (ImEnabHkWrite(imId, value, eiId) < 0)
                    {
                        string err = errReport("Failed ImEnabHkWrite(): ", eiId);
                        MessageBox.Show("Failed to change HK write enable: " + err);
                    }
                }
            }
        }
        private bool cfgExtStrobeFor245GHzSniff;
        public bool CfgExtStrobeFor245GHzSniff
        {
            get { return(cfgExtStrobeFor245GHzSniff); }
            set {
                cfgExtStrobeFor245GHzSniff = value;
                configMezzAndReportAnyErr();
            }
        }

        // connection to implant mezzanine board (returned by ImOpen())
        private void* imId = null;
        
        public ImApi()
        {
            mezzDevType = IM_AIM_DEV_TYPE;
        }
        
        // This initializes the interface to the implant module (ImApi).
        // Returns 0 for success, -1 for error.
        //
        public override int init(ref string err)
        {
            IM_SPECS imSpecs;
            int val;

            // perform init common to both of the ADK module interfaces
            if (base.init(ref err) < 0) goto error;

            // connect to implant mezzanine board
            ImInitSpecs(out imSpecs, sizeof(IM_SPECS));
            imSpecs.localAdpId = this.adpId;
            //
            imId = ImOpen(ref imSpecs, sizeof(IM_SPECS), eiId);
            if (imId == null) {
                err = errReport("Failed ImOpen(): ", eiId);
                goto error;
            }
            
            sbyte[] mezzVerBuf = new sbyte[125];
            if ((ImGetMezzVer(imId, mezzVerBuf, 120, eiId)) < 0)  {
                err = errReport("Failed ImGetMezzVer(): ", eiId);
                goto error;
            }
            for(int i = 0; i < 125; i++) {
                if ((char)mezzVerBuf[i] == '\0') break;
                mezzVer += (char)mezzVerBuf[i];
            }
            
            // get MICS revision (ZL70101, ZL70102 or ZL70103)
            if ((val = ImReadMicsReg(imId, 127, eiId)) < 0) {
                err = errReport("Failed ImReadMicsReg(): ", eiId);
                goto error;
            }
            MicsRev = (MICS_REV)val;
            
            // get config from base station mezzanine board (init config properties)
            if (getConfigFromMezz(ref err) < 0) goto error;
            
            initialized = true;
            return(0);
error:
            // Close any API/DLL interfaces that were opened. This frees any
            // resources allocated by the API functions, and closes any board
            // connections so they're available to be re-opened later if desired.
            //
            string closeErr = "";
            close(ref closeErr);
            return(-1);
        }
        
        // This is called to close any API/DLL interfaces that were opened by
        // the implant API (ImApi). This frees any resources allocated by the
        // API functions, and closes any board connections so they're available
        // to be re-opened later if desired. Returns 0 for success, -1 for error.
        //
        public override int close(ref string err)
        {
            int retVal = 0;
            
            if (imId != null) {
            
                if (ImClose(imId, eiId) < 0) {
                    err = errReport("Failed ImClose(): ", eiId);
                    retVal = -1;
                }
                imId = null;
            }
            
            // perform close tasks common to both of the ADK module interfaces
            if (base.close(ref err) < 0) retVal = -1;
            
            return(retVal);
        }
        
        // This gets the configuration from the implant mezzanine board and
        // uses it to initialize the configuration properties associated with
        // the board, so they reflect the board's current configuration.
        // Returns 0 if successful, -1 for error.
        //
        private int getConfigFromMezz(ref string err)
        {
            IM_MICS_CONFIG mc;
            
            if (ImGetMicsConfig(imId, out mc, sizeof(IM_MICS_CONFIG), eiId) < 0) {
                err = errReport("Failed ImGetMicsConfig(): ", eiId);
                return(-1);
            }
            
            CfgImdTid = (mc.imdTid.b3 << 16) + (mc.imdTid.b2 << 8) + mc.imdTid.b1;
            CfgCompanyId = mc.companyId;
            //
            CfgNormChan = mc.normalLinkConfig.chan;
            CfgNormTxMod = extractTxMod(mc.normalLinkConfig.modUser);
            CfgNormRxMod = extractRxMod(mc.normalLinkConfig.modUser);
            CfgNormBlockSize = mc.normalLinkConfig.rxBlockSize;
            CfgNormMaxBlocksPerTxPack = mc.normalLinkConfig.maxBlocksPerTxPack;
            //
            CfgEmerChan = mc.emergencyLinkConfig.chan;
            CfgEmerTxMod = extractTxMod(mc.emergencyLinkConfig.modUser);
            CfgEmerRxMod = extractRxMod(mc.emergencyLinkConfig.modUser);
            CfgEmerBlockSize = mc.emergencyLinkConfig.rxBlockSize;
            CfgEmerMaxBlocksPerTxPack = mc.emergencyLinkConfig.maxBlocksPerTxPack;
            
            if ((mc.flags & IM_ENAB_HK_WRITE) != 0) {
                CfgEnabHkWrite = true;
            } else {
                CfgEnabHkWrite = false;
            }
            if ((mc.flags & IM_400_MHZ_WAKEUP) != 0) {
                Cfg400MHzWakeup = true;
            } else {
                Cfg400MHzWakeup = false;
            }
            if ((mc.flags & IM_EXT_STROBE_FOR_245_GHZ_SNIFF) != 0) {
                CfgExtStrobeFor245GHzSniff = true;
            } else {
                CfgExtStrobeFor245GHzSniff = false;
            }
            
            return(0);
        }

        // This configures the implant mezzanine board with the configuration
        // properties associated with the board. First, it builds a structure
        // based on the current configuration properties, then sends it to the
        // board via the API/DLL. Returns 0 if successful, -1 for error.
        //
        protected override int configMezz(ref string err)
        {
            IM_MICS_CONFIG mc;
            
            if (!initialized) return(0);
            
            mc.flags = 0;
            mc.normalLinkConfig.modUser = 0;
            mc.emergencyLinkConfig.modUser = 0;
            // 
            mc.companyId = CfgCompanyId;
            mc.imdTid.b3 = (byte)(CfgImdTid >> 16);
            mc.imdTid.b2 = (byte)(CfgImdTid >> 8);
            mc.imdTid.b1 = (byte)CfgImdTid;
            //
            mc.normalLinkConfig.chan = CfgNormChan;
            mc.normalLinkConfig.modUser = buildModUser(CfgNormTxMod, CfgNormRxMod, 0);
            mc.normalLinkConfig.txBlockSize = CfgNormBlockSize;
            mc.normalLinkConfig.rxBlockSize = CfgNormBlockSize;
            mc.normalLinkConfig.maxBlocksPerTxPack = CfgNormMaxBlocksPerTxPack;
            //
            mc.emergencyLinkConfig.chan = CfgEmerChan;
            mc.emergencyLinkConfig.modUser = buildModUser(CfgEmerTxMod, CfgEmerRxMod, 0);
            mc.emergencyLinkConfig.txBlockSize = CfgEmerBlockSize;
            mc.emergencyLinkConfig.rxBlockSize = CfgEmerBlockSize;
            mc.emergencyLinkConfig.maxBlocksPerTxPack = CfgEmerMaxBlocksPerTxPack;
            //
            if (CfgEnabHkWrite) {
                mc.flags |= IM_ENAB_HK_WRITE;
            } else {
                mc.flags &= (~IM_ENAB_HK_WRITE & 0xFF);
            }
            if (Cfg400MHzWakeup) {
                mc.flags |= IM_400_MHZ_WAKEUP;
            } else {
                mc.flags &= (~IM_400_MHZ_WAKEUP & 0xFF);
            }
            if (CfgExtStrobeFor245GHzSniff) {
                mc.flags |= IM_EXT_STROBE_FOR_245_GHZ_SNIFF;
            } else {
                mc.flags &= (~IM_EXT_STROBE_FOR_245_GHZ_SNIFF & 0xFF);
            }

            if (ImSetMicsConfig(imId, ref mc, sizeof(IM_MICS_CONFIG), eiId) < 0) {
                err = errReport("Failed ImSetMicsConfig(): ", eiId);
                return(-1);
            }
            
            return(0);
        }

        // This reads the value of the specified register in the ZL7010X. If
        // successful, it returns the register value (8 bits, always >= 0). If
        // an error occurs, it sets "err" to the error message and returns -1. 
        //
        public override int micsRead(uint register, bool remote, ref string err)
        {
            int val = 0;
            
            if (!initialized) {
                err = "Unable to read register: the board interface isn't initialized.";
                return(-1);
            }
            
            if ((val = ImReadMicsReg(imId, register, eiId)) < 0) {
                err = errReport("ImReadMicsReg(): ", eiId);
                return(-1);
            }
            return(val);
        }

        public override int micsWrite(uint register, uint regVal, bool remote, ref string err)
        {
            if (initialized) {
            
                if (ImWriteMicsReg(imId, register, regVal, eiId) < 0) {
                    err = errReport("ImWriteMicsReg(): ", eiId);
                    return(-1);
                }
            } 
            return(0);
        }
       
        public override int micsReceive(byte[] buf, uint bufSize, ref string err)
        {
            int len;
            
            if ((len = ImReceiveMics(imId, buf, bufSize, eiId)) < 0) {
                err = errReport("Failed ImReceiveMics(): ", eiId);
                return(-1);
            }
            return(len);
        }

        public override int micsTransmit(byte *data, uint len, ref string err)
        {
            if (ImTransmitMics(imId, data, len, eiId) < 0) {
                err = errReport("Failed ImTransmitMics(): ", eiId);
                return(-1);
            }
            return(0);
        }

        public override int micsAbort(ref string err)
        {
            if (ImAbortMics(imId, eiId) < 0) {
                err = errReport("Failed ImAbortLink(): ", eiId);
                return(-1);
            }
            return(0);
        }
        
        public override int micsReset(ref string err)
        {
            if (ImResetMics(imId, eiId) < 0) {
                err = errReport("Failed ImResetMics(): ", eiId);
                return(-1);
            }
            return(0);
        }

        public int sendEmergency(ref string err)
        {
            // reconfigure mezzanine board to ensure it has the latest config
            if (configMezz(ref err) < 0) return(-1);

            if (ImSendEmergency(imId, eiId) < 0) {
                err = errReport("Failed ImSendEmergency(): ", eiId);
                return(-1);
            }
            return(0);
        }

        public int wakeup(bool stayAwake, ref string err)
        {
            if (ImWakeup(imId, stayAwake, eiId) < 0) {
                err = errReport("Failed ImWakeup(): ", eiId);
                return(-1);
            }
            return(0);
        }

        public int sleep(ref string err)
        {
            if (ImSleep(imId, eiId) < 0) {
                err = errReport("Failed ImSleep(): ", eiId);
                return(-1);
            }
            return(0);
        }

        // This performs an RSSI using the internal ADC in the ZL7010X. If
        // successful, it returns the RSSI value (always >= 0). If an error
        // occurs, it sets "err" to the error message and returns -1. 
        //
        public int rssi(uint chan, ref string err)
        {
            int rssi;
            
            if ((rssi = ImIntRssi(imId, chan, eiId)) < 0) {
                err = errReport("Failed ImIntRssi(): ", eiId);
                return(-1);
            }
            return(rssi);
        }

        public override int micsCal(MICS_CALS cal, int chan, bool remote, ref string err)
        {
            if (ImMicsCal(imId, (uint)cal, chan, eiId) < 0) {
                err = errReport("Failed ImMicsCal(): ", eiId);
                return(-1);
            }
            return(0);
        }

        public override int micsAdc(uint adcInput, bool remote, ref string err)
        {
            int adcResult;
            
            if ((adcResult = ImMicsAdc(imId, adcInput, eiId)) < 0) {
                err = errReport("Failed ImMicsAdc(): ", eiId);
                return(-1);
            }
            return(adcResult);
        }

        public override int startTx400Carrier(bool start, uint chan, ref string err)
        {
            if (ImStartTx400Carrier(imId, start, chan, eiId) < 0) {
                err = errReport("Failed ImStartTx400Carrier(): ", eiId);
                return(-1);
            }
            return(0);
        }

        public override int enabExtRssi(bool enab, uint chan, ref string err)
        {
            if (ImEnabExtRssi(imId, enab, chan, eiId) < 0) {
                err = errReport("Failed ImEnabExtRssi(): ", eiId);
                return(-1);
            }
            return(0);
        }

        public override int startBerRx(bool start, uint chan, MOD rxMod, ref string err)
        {
            uint rxModDll = buildModUser(MOD.MOD_2FSK_FB, rxMod, 0);
            
            if (ImStartBerRx(imId, start, chan, rxModDll, eiId) < 0) {
                err = errReport("Failed ImStartBerRx(): ", eiId);
                return(-1);
            }
            return(0);
        }
        
        public override int copyMicsRegs(bool remote, ref string err)
        {
            if (ImCopyMicsRegs(imId, eiId) < 0) {
                err = errReport("Failed ImCopyMicsRegs(): ", eiId);
                return(-1);
            }
            return(0);
        }
        
        public override int calcMicsCrc(bool remote, ref string err)
        {
            if (ImCalcMicsCrc(imId, eiId) < 0) {
                err = errReport("Failed ImCalcMicsCrc(): ", eiId);
                return(-1);
            }
            return(0);
        }
        
        public ImStatChanges getStatChanges(bool force, ref string err)
        {
            IM_STAT_CHANGES iscDll;  // status changes from API/DLL
            ImStatChanges isc = this.imStatChanges;
            bool boolVal;
            
            // get status changes from implant
            if (ImGetStatChanges(imId, force, out iscDll, sizeof(IM_STAT_CHANGES), eiIdStat) < 0) {
                err = errReport("ImGetStatChanges(): ", eiIdStat);
                return(null);
            }
            // if the implant reported an error, set error in status
            if ((iscDll.genStat.flags & IM_ERR_OCCURRED) != 0) {
                isc.imErr = errReport("Error on implant: ", eiIdStat);
            } else {
                isc.imErr = null;
            }
            
            // update the MICS state in the status
            updateMicsState(force, (MICS_STATE)iscDll.genStat.micsState, isc);
            
            // update the 400 MHz TX active flag in the status
            boolVal = ((iscDll.genStat.flags & IM_400_MHZ_TX_ACTIVE) != 0);
            updateTx400Active(force, boolVal, isc);
            
            // update the 400 MHz RX active flag in the status
            boolVal = ((iscDll.genStat.flags & IM_400_MHZ_RX_ACTIVE) != 0);
            updateRx400Active(force, boolVal, isc);
            
            // if the link status changed, update it in the status
            if ((iscDll.genStat.flags & IM_LINK_STAT_CHANGED) != 0) {
                updateLinkStat(ref iscDll.linkStat, isc.linkStat);
                isc.linkStatChanged = true;
            } else {
                isc.linkStatChanged = false;
            }
            
            // if the link qulity changed, update it in the status
            if ((iscDll.genStat.flags & IM_LINK_QUAL_CHANGED) != 0) {
                updateLinkQual(ref iscDll.linkQual, isc.linkQual);
                isc.linkQualChanged = true;
            } else {
                isc.linkQualChanged = false;
            }
            
            // if the data test status changed, update it in the status
            if ((iscDll.genStat.flags & IM_DATA_STAT_CHANGED) != 0) {
                isc.dataStat = iscDll.dataStat;
                isc.dataStatChanged = true;
            } else {
                isc.dataStatChanged = false;
            } 
            
            // return changes in status for implant
            return(isc);
        }
        
        public override int startDataTest(ref MICS_DT_SPECS specs, ref string err)
        {
            if ((ImStartDataTest(imId, ref specs, sizeof(MICS_DT_SPECS), eiId)) < 0) {
                err = errReport("Failed ImStartDataTest(): ", eiId);
                return(-1);
            }
            return(0);
        }
        
        public int getTraceString(ref string traceStr)
        {
            if ((ImGetTraceMsg(imId, traceBuf, 120, eiId)) < 0) {
                traceStr = errReport("Failed ImGetTraceMsg(): ", eiId);
                return(-1);
            }
            
            for(int i = 0; i < 125; i++) {
                if ((char)traceBuf[i] == '\0') break;
                traceStr += (char)traceBuf[i];
            };

            return(0);
        }

        // This checks if the specified node NVM ("nvm") is valid, and if it is
        // the same version (size) as the current IM_NVM structure for the host
        // (PC) software. If so, it returns 0. If the specified NVM is an older
        // (smaller) version of the structure, it sets the specified "err" string
        // to an error message and returns IM_NVM_RSTAT.NVM_IS_OLDER_VER. If any
        // other error occurs (e.g. the specified NVM is invalid, or is a newer
        // (larger) version that the host (PC) software does not support), it
        // sets the specified "err" string to an error message and returns
        // IM_NVM_RSTAT.NVM_OTHER_ERR.
        //
        public static IM_NVM_RSTAT checkNvm(byte[] nvm, out string err)
        {
            err = null;
            IM_NVM_RSTAT nnrStat;

            fixed (void* fNvm = nvm)
            {
                if ((nnrStat = (IM_NVM_RSTAT)ImCheckNvm(fNvm, eiId)) < 0)
                {
                    err = errReport("Failed ImCheckNvm(): ", eiId);
                }
                return(nnrStat);
            }
        }

        // This transfers the settings from the specified previous NVM ("prevNvm")
        // to the specified ImNvm object ("nvm"). Note that the caller must initialize
        // everything in the ImNvm object before passing it to this method, including
        // the NVM headers, and default values for all settings. If the previous NVM is
        // an older (smaller) version of the IM_NVM structure that does not contain
        // all of the settings that exist in the current ImNvm object, this method
        // leaves the new settings in the ImNvm object at the default values initialized
        // by the caller. If an error occurs (e.g. one of the NVMs is invalid, or the
        // previous NVM is a newer (larger) version that the host (PC) software doesn't
        // support), this sets the specified "err" string to an error message and
        // returns -1.
        //
        public static int transferNvm(byte[] prevNvm, ImNvm nvm, out string err)
        {
            err = null;

            fixed (void* fPrevNvm = prevNvm)
            {
                uint nvmSize = (uint)Marshal.SizeOf(nvm);

                if (ImTransferNvm(fPrevNvm, nvm, nvmSize, eiId) < 0)
                {
                    err = errReport("Failed ImTransferNvm(): ", eiId);
                    return(-1);
                }
                return(0);
            }
        }

        // This reads the MICS factory NVM from the implant.
        //
        public int getMicsFactNvm(ImMicsFactNvm mfn, ref string err)
        {
            int size = Marshal.SizeOf(typeof(ImMicsFactNvm));

            if (ImGetMicsFactNvm(imId, mfn, (uint)size, eiId) < 0) {
                err = errReport("Failed ImGetMicsFactNvm(): ", eiId);
                return(-1);
            }
            return(0);
        }
    }
}
