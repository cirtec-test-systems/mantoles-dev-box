//
// This file contains the build settings for the ZL7010X ADK. These are
// settings that vary depending on what is being built, but must have a
// known value at compile-time (i.e. they can't be run-time variables).
//
// Note generic names are used for the "Build" namespace and "BuildSettings"
// class so they can be referenced by software that is shared by multiple
// projects, not just the ZL7010X ADK. For example, the software included in
// the "Application Development Platform" is shared by the ZL7010X ADK and
// other projects, so it must work with all of the projects. If each project
// provides its own "Build.BuildSettings", the shared software can reference
// "Build.BuildSettings" to get project-specific settings that must have a
// known value at compile-time.
//
namespace Build
{
    // This class contains the build settings for the ZL7010X ADK. These are
    // settings that vary depending on what is being built, but must have a
    // known value at compile-time (i.e. they can't be run-time variables).
    //
    public static class BuildSettings
    {
        // the name of the DLL that contains the C API functions
        public const string C_API_DLL = "ZL7010XAdkDll_3_1_X.dll";
    }
}
