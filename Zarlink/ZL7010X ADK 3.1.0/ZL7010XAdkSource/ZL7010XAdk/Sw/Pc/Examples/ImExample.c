/**************************************************************************
 * This file provides an example of how to connect the implant and perform
 * perform various operations using the API (DLL) included with the ADK. For
 * more DLL functions, see "ZL7010XAdk\Sw\Pc\Libs\ImLib.c".
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Standard/String.h"      /* NULL */
#include "Standard/StdIo.h"       /* printf(), ... */ 
#include "Adp/Pc/ErrInfoLib.h"    /* EiOpen(), EiGetMsg(), ... */
#include "Adp/Pc/AdpLib.h"        /* AdpOpen(), ADP_SPECS, ... */
#include "Adk/AnyMezz/MicsHw.h"   /* SYNC1, TXBUFF_BSIZE_MAX, ... */
#include "Adk/ImMezz/ImGeneral.h" /* IM_STAT, IM_AIM_DEV_TYPE, ... */
#include "Adk/Pc/ImLib.h"         /* ImOpen(), IM_SPECS, ... */

void
main(void)
{
    char imVer[IM_MEZZ_VER_BUF_SIZE];
    ADP_ID localAdpId;
    ADP_SPECS adpSpecs;
    IM_SPECS imSpecs;
    IM_MICS_CONFIG imc;
    IM_STAT imStat;
    IM_ID imId;
    int i, n, rssi, regVal;
    UD8 buf[TXBUFF_BSIZE_MAX];
    EI e;
    
    /* init resources referenced in cleanup */
    e = localAdpId = imId = NULL;
    
    /* create an error interface to pass to DLL functions for error messages */
    if ((e = EiOpen(NULL, 0)) == NULL) {
        printf("Failed EiOpen()\n");
        goto cleanup;
    }
    
    /* connect to the ADP board attached to the implant mezzanine board */
    AdpInitSpecs(&adpSpecs, sizeof(adpSpecs));
    adpSpecs.localMezzDevType = IM_AIM_DEV_TYPE;
    if ((localAdpId = AdpOpen(&adpSpecs, sizeof(adpSpecs), e)) == NULL) {
        printf("Failed AdpOpen(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    
    /* connect to the implant mezzanine board (through the ADP board) */
    ImInitSpecs(&imSpecs, sizeof(imSpecs));
    imSpecs.localAdpId = localAdpId;
    if ((imId = ImOpen(&imSpecs, sizeof(imSpecs), e)) == NULL) {
        printf("Failed ImOpen(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    
    /* get version string from implant mezzanine board */
    if (ImGetMezzVer(imId, imVer, sizeof(imVer), e)) {
        printf("Failed ImGetMezzVer(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    printf("Implant version = \"%s\".\n", imVer);
    
    /* Check implant status. This is something an application on the PC will
     * generally do periodically, such as once a second. For more information,
     * see IM_STAT in "ZL7010XAdk\Sw\Includes\Adk\ImMezz\ImGeneral.h".
     */
    if (ImGetStat(imId, &imStat, sizeof(imStat), e)) {
        printf("Failed ImGetStat(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    if (imStat.flags & IM_ERR_OCCURRED) {
        printf("Implant reported error: %s\n", EiGetMsg(e));
    }
    
    /* get current implant configuration */
    if (ImGetMicsConfig(imId, &imc, sizeof(imc), e)) {
        printf("Failed ImGetMicsConfig(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    printf("Company ID = %d, IMD Transceiver ID = 0x%02x%02x%02x\n",
        imc.companyId, imc.imdTid.b3, imc.imdTid.b2, imc.imdTid.b1);
        
    /* write same config back to implant (could modify first if desired) */
    if (ImSetMicsConfig(imId, &imc, sizeof(imc), e)) {
        printf("Failed ImSetMicsConfig(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    
    /* do internal RSSI for channel 0 (received signal strength indicator) */
    if ((rssi = ImIntRssi(imId, 0, e)) < 0) {
        printf("Failed ImRssi(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    printf("Internal RSSI for channel 0 = %d\n", rssi);
    
    /* read a register in the MICS chip (on implant) */
    if ((regVal = ImReadMicsReg(imId, SYNC1, e)) < 0) {
        printf("Failed ImReadMicsReg(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    /* write to a register in the MICS chip (on implant) */
    if (ImWriteMicsReg(imId, SYNC1, regVal, e) < 0) {
        printf("Failed ImWriteMicsReg(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    
    /* Read any received data (any data in RX buf on MICS chip). Note this
     * returns the number of bytes read, which will always be a multiple of
     * the RX block size. Normally this would only be called when in session
     * with a base station. It is only called here as an example.
     */
    if ((n = ImReceiveMics(imId, buf, sizeof(buf), e)) < 0) {
        printf("Failed ImReceiveMics(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    printf("Read %d bytes of received data ...\n", n);
    
    /* Transmit a block of data (write it to the TX buf on MICS chip). Note
     * the data written must always be a multiple of the TX block size.
     * Normally this would only be called when in session with a base station.
     * It is only called here as an example.
     */
    printf("Transmitting data ...\n"); 
    n = imc.normalLinkConfig.txBlockSize;
    for(i = 0; i < sizeof(buf); ++i) buf[i] = (UD8)i;
    if (ImTransmitMics(imId, buf, n, e) < 0) {
        printf("Failed ImTransmitMics(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    
    /* start sending emergency transmissions */
    printf("Sending emergency transmissions ...\n");
    if (ImSendEmergency(imId, e) < 0) {
        printf("Failed ImSendEmergency(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    
    /* stop sending emergency and put MICS chip to sleep */
    if (ImSleep(imId, e) < 0) {
        printf("Failed ImSleep(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    printf("Stopped sending emergency transmissions.\n");

cleanup:
    if (imId != NULL) {
        if (ImClose(imId, e)) {
            printf("Failed ImClose(): %s\n", EiGetMsg(e));
        }
    }
    if (localAdpId != NULL) {
        if (AdpClose(localAdpId, e)) {
            printf("Failed AdpClose(): %s\n", EiGetMsg(e));
        }
    }
    if (e != NULL) EiClose(e);
    
    printf("\nEnter any character to exit ...\n");
    getchar();
}
