/**************************************************************************
 * This file provides an example of how to connect the the base station and
 * perform various operations using the API (DLL) included with the ADK. For
 * more DLL functions, see "ZL7010XAdk\Sw\Pc\Libs\BsmLib.c".
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Standard/String.h"        /* NULL */
#include "Standard/StdIo.h"         /* printf(), ... */ 
#include "Adp/Pc/ErrInfoLib.h"      /* EiOpen(), EiGetMsg(), ... */
#include "Adp/Pc/AdpLib.h"          /* AdpOpen(), ADP_SPECS, ... */
#include "Adk/AnyMezz/MicsHw.h"     /* SYNC1, TXBUFF_BSIZE_MAX, ... */
#include "Adk/BsmMezz/BsmGeneral.h" /* BSM_STAT, BSM_DEV_TYPE, ... */
#include "Adk/Pc/BsmLib.h"          /* BsmOpen(), BSM_SPECS, ... */

void
main(void)
{
    char bsmVer[BSM_MEZZ_VER_BUF_SIZE];
    ADP_ID localAdpId;
    ADP_SPECS adpSpecs;
    BSM_SPECS bsmSpecs;
    BSM_CCA_DATA ccaData;
    BSM_MICS_CONFIG bmc;
    BSM_STAT bsmStat;
    BSM_ID bsmId;
    int i, n, rssi, ccaChan, regVal;
    UD8 buf[TXBUFF_BSIZE_MAX];
    EI e;
    
    /* init resources referenced in cleanup */
    e = localAdpId = bsmId = NULL;
    
    /* create an error interface to pass to DLL functions for error messages */
    if ((e = EiOpen(NULL, 0)) == NULL) {
        printf("Failed EiOpen()\n");
        goto cleanup;
    }
    
    /* connect to the ADP board attached to base station mezzanine board */
    AdpInitSpecs(&adpSpecs, sizeof(adpSpecs));
    adpSpecs.localMezzDevType = BSM_DEV_TYPE;
    if ((localAdpId = AdpOpen(&adpSpecs, sizeof(adpSpecs), e)) == NULL) {
        printf("Failed AdpOpen(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    
    /* connect to the base station mezzanine board (through the ADP board) */
    BsmInitSpecs(&bsmSpecs, sizeof(bsmSpecs));
    bsmSpecs.localAdpId = localAdpId;
    if ((bsmId = BsmOpen(&bsmSpecs, sizeof(bsmSpecs), e)) == NULL) {
        printf("Failed BsmOpen(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    
    /* get version string from base station mezzanine board */
    if (BsmGetMezzVer(bsmId, bsmVer, sizeof(bsmVer), e)) {
        printf("Failed BsmGetMezzVer(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    printf("Base station version = \"%s\".\n", bsmVer);
    
    /* Check base station status. This is something an application on the
     * PC will generally do periodically, such as once a second. For more
     * information, see BSM_STAT in "ZL7010XAdk\Sw\Includes\Adk\BsmMezz\
     * BsmGeneral.h".
     */
    if (BsmGetStat(bsmId, &bsmStat, sizeof(bsmStat), e)) {
        printf("Failed BsmGetStat(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    if (bsmStat.flags & BSM_ERR_OCCURRED) {
        printf("Base station reported error: %s\n", EiGetMsg(e));
    }
    
    /* get current base station configuration */
    if (BsmGetMicsConfig(bsmId, &bmc, sizeof(bmc), e)) {
        printf("Failed BsmGetMicsConfig(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    printf("Company ID = %d, IMD Transceiver ID = 0x%02x%02x%02x\n",
        bmc.companyId, bmc.imdTid.b3, bmc.imdTid.b2, bmc.imdTid.b1);
        
    /* config base station to do CCA when starting session, etc. */
    bmc.flags |= BSM_AUTO_CCA;
    if (BsmSetMicsConfig(bsmId, &bmc, sizeof(bmc), e)) {
        printf("Failed BsmSetMicsConfig(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    
    /* do RSSI for channel 0 (received signal strength indicator) */
    if ((rssi = BsmRssi(bsmId, FALSE, 0, FALSE, e)) < 0) {
        printf("Failed BsmRssi(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    printf("RSSI for channel 0 = %d\n", rssi);
    
    /* do CCA (clear channel assessment) */
    if ((ccaChan = BsmCca(bsmId, FALSE, &ccaData, sizeof(ccaData), e)) < 0) {
        printf("Failed BsmCca(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    printf("CCA channel = %d\n", ccaChan);
    
    /* read a register in local MICS chip (on base station) */
    if ((regVal = BsmReadMicsReg(bsmId, SYNC1, FALSE, e)) < 0) {
        printf("Failed BsmReadMicsReg(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    /* write to a register in local MICS chip (on base station) */
    if (BsmWriteMicsReg(bsmId, SYNC1, regVal, FALSE, e) < 0) {
        printf("Failed BsmWriteMicsReg(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    
    /* Start a session with the remote implant. Note that TRUE is specified so
     * the ZL7010X watchdog is enabled while attempting to start the session.
     * That way, if the session doesn't start within ~4.37 seconds, the base
     * station's ZL7010X will assert IRQ_WDOG, causing the base station
     * software to abort the "start session" operation. For more information,
     * see BsmStartSession() in "ZL7010XAdk\Sw\Pc\Libs\BsmLib.c".
     */
    printf("Starting session ...\n"); 
    if (BsmStartSession(bsmId, TRUE, 5, e) < 0) {
        printf("Failed BsmStartSession(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    
    /* read a register in the remote MICS chip (on implant) using HK */
    if ((regVal = BsmReadMicsReg(bsmId, SYNC1, TRUE, e)) < 0) {
        printf("Failed BsmReadMicsReg(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    /* write to a register in the remote MICS chip (on implant) using HK */
    if (BsmWriteMicsReg(bsmId, SYNC1, regVal, TRUE, e) < 0) {
        printf("Failed BsmWriteMicsReg(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    
    /* Transmit a block of data (write it to the TX buf on MICS chip). Note
     * the data written must always be a multiple of the TX block size.
     */
    printf("Transmitting data ...\n"); 
    n = bmc.normalLinkConfig.txBlockSize;
    for(i = 0; i < sizeof(buf); ++i) buf[i] = (UD8)i;
    if (BsmTransmitMics(bsmId, buf, n, e) < 0) {
        printf("Failed BsmTransmitMics(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    
    /* Read any received data (any data in RX buf on MICS chip). Note this
     * returns the number of bytes read, which will always be a multiple of
     * the RX block size.
     */
    if ((n = BsmReceiveMics(bsmId, buf, sizeof(buf), e)) < 0) {
        printf("Failed BsmReceiveMics(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    printf("Read %d bytes of received data ...\n", n);
    
    /* stop session */
    if (BsmAbortMics(bsmId, e) < 0) {
        printf("Failed BsmAbort(): %s\n", EiGetMsg(e));
        goto cleanup;
    }
    printf("Session stopped ...\n");

cleanup:
    if (bsmId != NULL) {
        if (BsmClose(bsmId, e)) {
            printf("Failed BsmClose(): %s\n", EiGetMsg(e));
        }
    }
    if (localAdpId != NULL) {
        if (AdpClose(localAdpId, e)) {
            printf("Failed AdpClose(): %s\n", EiGetMsg(e));
        }
    }
    if (e != NULL) EiClose(e);
    
    printf("\nEnter any character to exit ...\n");
    getchar();
}
