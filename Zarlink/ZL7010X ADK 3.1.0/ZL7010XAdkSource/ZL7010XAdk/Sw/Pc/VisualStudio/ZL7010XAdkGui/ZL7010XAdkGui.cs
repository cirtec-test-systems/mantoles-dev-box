//
// NAMESPACE: Zarlink.Adk.Forms
//      ZL7010XAdkGui class
// 
// FILE: ZL7010XAdkGui.cs
// 
//      This file contains the public static class ZL7010XAdkGui.
// 
// CLASS: Util
//      Provides support main method for the GUI application.
// 
// NOTES:
//      None
//      
// Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2012. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.

using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Zarlink.Adk.Forms
{
    static class ZL7010XAdkGui
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new AdkForm());
        }
    }
}