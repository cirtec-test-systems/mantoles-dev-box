/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
/* ensure this file is only included once */
#ifndef Adk_AdkVer_h
#define Adk_AdkVer_h

/**************************************************************************
 * Defines and Macros
 */
 
/* Previous ADK versions.
 */
#define ADK_VER_1_0_0  "ZL70101 Application Development Kit 1.0.0"
#define ADK_VER_1_0_1  "ZL70101 Application Development Kit 1.0.1"
#define ADK_VER_1_0_2  "ZL70101 Application Development Kit 1.0.2"
#define ADK_VER_1_1_0  "ZL70101 Application Development Kit 1.1.0"
#define ADK_VER_1_2_0  "ZL70101 Application Development Kit 1.2.0"
#define ADK_VER_1_2_1  "ZL70101 Application Development Kit 1.2.1"
#define ADK_VER_2_0_0  "ZL7010X ADK 2.0.0"
#define ADK_VER_2_1_0  "ZL7010X ADK 2.1.0"
#define ADK_VER_2_1_1  "ZL7010X ADK 2.1.1"
#define ADK_VER_2_2_0  "ZL7010X ADK 2.2.0"
#define ADK_VER_2_3_0  "ZL7010X ADK 2.3.0"
#define ADK_VER_3_0_0  "ZL7010X ADK 3.0.0"
#define ADK_VER_3_1_0  "ZL7010X ADK 3.1.0"
/*
 * Current ADK version. The ADK API (DLL) and mezzanine boards (base station
 * and implant) return this in their version string (see AdkGetApiVer(),
 * BsmGetMezzVer(), and ImGetMezzVer()). Note BsmGetMezzVer() and ImGetMezzVer()
 * also return the mezzanine board's hardware model in the version string.
 * 
 * Note the Application Development Platform maintains its own separate
 * version (see APP_DEV_PLAT_VER in "Adp/AppDevPlatVer.h"). That way, different
 * ADK versions can all use the same standard version of the Application
 * Development Platform, including the ADP board, so the ADP board won't have
 * to be reprogrammed for each release of the ADK.
 */
#define ADK_VER  ADK_VER_3_1_0

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global Declarations
 */
 
/**************************************************************************
 * External Function Prototypes
 */
 
#endif /* ensure this file is only included once */
