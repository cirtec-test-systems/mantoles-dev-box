/**************************************************************************
 * This is the include file for the CC25XX library, which provides functions
 * to interface to the CC25XX chip (CC2500 or CC2550).
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adk_BsmMezz_CC25XXLib_h
#define Adk_BsmMezz_CC25XXLib_h

#include "Adp/General.h"          /* UINT, RSTAT, ... */
#include "Adk/BsmMezz/CC25XXHw.h" /* CC_STX, ... */

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global Declarations
 */
 
/**************************************************************************
 * External Function Prototypes
 */
 
extern RSTAT CcInit(void);
extern UINT CcRead(UINT regAddr);
extern void CcReadBurst(UINT startRegAddr, void *buf, UINT len);
extern void CcWrite(UINT regAddr, UINT regVal);
extern void CcWriteBurst(UINT startRegAddr, const void *data, UINT len);
extern void CcCommand(UINT command);
extern UINT CcChipStat(void);

#endif /* ensure this file is only included once */
