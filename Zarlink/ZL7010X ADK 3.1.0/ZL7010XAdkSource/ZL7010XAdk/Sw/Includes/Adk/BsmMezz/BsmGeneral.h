/**************************************************************************
 * This include file contains various defines and structures for status and
 * control of the base station mezzanine board.
 *
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
/* ensure this file is only included once */
#ifndef Adk_BsmMezz_BsmGeneral_h
#define Adk_BsmMezz_BsmGeneral_h

#include "Adp/Build/Build.h"         /* UD8, UD16, SD8, ... */
#include "Adk/AnyMezz/MicsHw.h"      /* CAL_400_MHZ_ANT, MAX_MICS_CHAN, ... */
#include "Adk/AnyMezz/MicsGeneral.h" /* MICS_IMD_TID, MICS_LINK_CONFIG, ... */
#include "Adp/AnyBoard/NvmLib.h"     /* NVM_INFO, ... */

/**************************************************************************
 * Defines and Macros
 */
 
/* The device type for the base station mezzanine board. To connect to a base
 * station mezzanine board (see BsmOpen() in "ZL7010XAdk\Sw\Pc\Libs\BsmLib.c"),
 * you must first connect to the ADP board attached to the mezzanine board
 * (see AdpOpen() in "AppDevPlat\Sw\Pc\Libs\AdpLib.c", and ADP_SPECS in
 * "AppDevPlat\Sw\Includes\Adp\Pc\AdpLib.h"). For ADP_SPECS.localMezzDevType,
 * specify BSM_DEV_TYPE.
 */
#define BSM_DEV_TYPE  "ZL7010X ADK Base Station"

/* Defines to use for BSM_HW to identify the base station mezzanine board
 * hardware. BSM_HW must be defined in the build include (for a description of
 * build includes and how they're used, see "Adp/Build/Build.h"). For hardware
 * revisions that require a special build, BSM_HW should be defined to one
 * of the specific revisions below (BSM100_REV_E, ...). Otherwise, BSM_HW
 * can be defined to one of the model numbers below with no revision
 * information (BSM100, ...).
 * 
 * To reference only the model number contained in BSM_HW, you can use
 * BSM_MODEL. This masks out any revision information provided in BSM_HW.
 * 
 * Note BsmGetMezzVer() returns the board's model name in its version string,
 * along with its firmware version (see ADK_VER in "Adk/AdkVer.h").
 */
#define BSM100         (100UL << 16)
#define BSM100_NAME    "BSM100"
#define BSM200         (200UL << 16)
#define BSM200_NAME    "BSM200"
#define BSM300         (300UL << 16)
#define BSM300_NAME    "BSM300"
/**/
#define BSM100_REV_B   (BSM100 | ('B' << 8) | 0)
#define BSM100_REV_C   (BSM100 | ('C' << 8) | 0)
#define BSM100_REV_D   (BSM100 | ('D' << 8) | 0)
#define BSM100_REV_D1  (BSM100 | ('D' << 8) | 1)
#define BSM100_REV_D2  (BSM100 | ('D' << 8) | 2)
#define BSM100_REV_E   (BSM100 | ('E' << 8) | 0)
#define BSM200_REV_A   (BSM200 | ('A' << 8) | 0)
#define BSM300_REV_A   (BSM300 | ('A' << 8) | 0)
/**/
#define BSM_MODEL      (BSM_HW & 0xFFFF0000UL)

/* Defines for the base station operational state (BSM_STAT.micsState). Note
 * to maintain backwards compatibility with existing software, the values of
 * existing states should never be changed. If a new state is added that is
 * unique to the base station, it should not use any of the values used for
 * for the implant states (in "ZL7010XAdk\Sw\Includes\Adk\ImMezz\ImGeneral.h").
 * If a new state is added that is also used by the implant, the state should
 * be added to both include files (for the base station and implant), and they
 * should have the same value.
 * 
 * BSM_SLEEPING:
 *     The MICS chip is either sleeping or starting up (before IRQ_RADIOREADY).
 *     Note on the ADK base station, this is only used once during startup,
 *     because the base station never puts the MICS chip back to sleep.
 * BSM_IDLE:
 *     The base station is idle (MICS chip awake).
 * BSM_SEARCHING_FOR_IMPLANT:
 *     Searching for implant(s) (see BsmSearchForImplant()). In this state,
 *     the base station transmits a wakeup signal (2.45 GHz or 400 MHz),
 *     receives 400 MHz wakeup responses, and places their implant ID's in
 *     the RX buffer on the MICS chip (for the application to read).
 * BSM_STARTING_SESSION:
 *     Starting a session with an implant (see BsmStartSession()). In this
 *     state, the base station transmits a wakeup signal (2.45 GHz or 400 MHz)
 *     and starts a session if a 400 MHz wakeup response is received.
 * BSM_LISTENING_FOR_EMERGENCY:
 *     Listening for emergency transmissions (see BsmListenForEmergency()).
 *     In this state, the base station listens for emergency transmissions and 
 *     places their implant ID's in the RX buf on the MICS chip (for the
 *     application to read). It doesn't transmit anything.
 * BSM_IN_SESSION:
 *     In session (link ready). In this state, the base station has established
 *     an RF link with the remote implant, and the link is ready to transmit
 *     and receive data and housekeeping messages.
 */
#define BSM_SLEEPING                 0
#define BSM_IDLE                     1
#define BSM_SEARCHING_FOR_IMPLANT    2
#define BSM_STARTING_SESSION         3
#define BSM_LISTENING_FOR_EMERGENCY  4
#define BSM_IN_SESSION               6
/*
 * These are old names used for various base station states (provided to
 * maintain backwards compatibility with existing software). Note some of these
 * are also defined in the include file for the implant states ("ZL7010XAdk\Sw\
 * Includes\Adk\ImMezz\ImGeneral.h"). Thus, they are undefined first, so if
 * both files are included, it won't cause compiler errors.
 */
#undef MICS_SLEEPING
#undef MICS_IDLE
#undef MICS_IN_SESSION
/**/
#define MICS_SLEEPING                 BSM_SLEEPING
#define MICS_IDLE                     BSM_IDLE
#define MICS_SEARCHING_FOR_IMPLANT    BSM_SEARCHING_FOR_IMPLANT
#define MICS_STARTING_SESSION         BSM_STARTING_SESSION
#define MICS_LISTENING_FOR_EMERGENCY  BSM_LISTENING_FOR_EMERGENCY
#define MICS_IN_SESSION               BSM_IN_SESSION

/* MICS calibrations supported by the base station:
 * 
 * BSM_LOCAL_MICS_CALS: Calibrations the base can run on its own ZL7010X.
 * 
 * BSM_REMOTE_102_CALS: Calibrations the base can run on a remote ZL70102/103
 * implant via housekeeping.
 * 
 * BSM_REMOTE_101_CALS: Calibrations the base can run on a remote ZL70101
 * implant via housekeeping.
 */
#define BSM_LOCAL_MICS_CALS  (CAL_TX_IF_OSC | CAL_FM_DETECT_AND_RX_IF | \
    CAL_RX_ADC | CAL_400_MHZ_ANT | CAL_24_MHZ_CRYSTAL_OSC)
/**/    
#define BSM_REMOTE_102_CALS  (CAL_WAKEUP_STROBE_OSC | CAL_TX_IF_OSC | \
    CAL_FM_DETECT_AND_RX_IF | CAL_RX_ADC | CAL_400_MHZ_ANT | \
    CAL_2_MHZ_OSC_102 | CAL_LNA_FREQ_102)
/**/    
#define BSM_REMOTE_101_CALS  (CAL_WAKEUP_STROBE_OSC | CAL_TX_IF_OSC | \
    CAL_FM_DETECT_AND_RX_IF | CAL_RX_ADC | CAL_245_GHZ_ZERO_LEV_101 | \
    CAL_400_MHZ_ANT)
    
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* Structure for the configuration for the MICS interface on the base station
 * (used to configure the ZL7010X and CC25XX).
 */
typedef struct {
    
    /* flags for MICS configuration (see BSM_AUTO_LISTEN, ...) */
    UD8 flags;
    
    /* IMD transceiver ID (implant ID; 3 bytes). Note this should never be set
     * to the wildcard value in the MICS configuration. Some operations (i.e.
     * "search for implant") provide a separate interface to use the wildcard
     * if desired, so it will only be used where appropriate.
     */
    MICS_IMD_TID imdTid;
        
    /* Company ID. Note this should never be set to 0 (wildcard) in the MICS
     * configuration. Some operations (i.e. "search for implant" and "listen
     * for emergency" provide a separate interface to use the wildcard if
     * desired, so it will only be used where appropriate.
     */
    UD8 companyId;
    
    /* link config for normal communication (400 MHz) */
    MICS_LINK_CONFIG normalLinkConfig;
        
    /* link config for emergency communication (400 MHz) */
    MICS_LINK_CONFIG emergencyLinkConfig;
    
    /* The revision of the MICS chip on the remote implant (ZL70100, ZL70101,
     * ZL70102, or ZL70103, which are defined in "Adk/AnyMezz/MicsHw.h"). The
     * base station will configure its MICS chip as required to communicate
     * with the MICS chip on the remote implant.
     */
    UD8 micsRevOnImplant;
    
} BSM_MICS_CONFIG;
/*
 * Defines for BSM_MICS_CONFIG.flags:
 * 
 * BSM_AUTO_LISTEN:
 *     Automatically listen for emergency transmissions when idle.
 * BSM_AUTO_CCA:
 *     Automatically perform a CCA when starting wakeup or starting a session.
 * BSM_400_MHZ_WAKEUP:
 *     Wakeup implant via 400 MHz instead of 2.45 GHz.
 * BSM_RESET_MICS_ON_WAKEUP:
 *     If this is set, then each time BsmMicsWakeup() is called to wake the
 *     ZL7010X, it also resets and reinitializes the ZL7010X. Note that
 *     BsmMicsWakeup() does not change the current MICS configuration
 *     (BSM_MICS_CONFIG) on the base station. Rather, after BsmMicsWakeup()
 *     resets and reinitializes the ZL7010X, it restores the ZL7010X settings
 *     specified in the current MICS configuration on the base station.
 */
#define BSM_AUTO_LISTEN             (1 << 0)
#define BSM_OBSOLETE_CONFIG_FLAG_1  (1 << 1)  /* no longer supported */
#define BSM_AUTO_CCA                (1 << 2)
#define BSM_400_MHZ_WAKEUP          (1 << 3)
#define BSM_RESET_MICS_ON_WAKEUP    (1 << 4)

/* Structure for general base station status.
 */
typedef struct {
    
    /* flags (see BSM_ERR_OCCURRED, BSM_LINK_STAT_CHANGED, ...) */
    UD8 flags;
    
    /* current operational state (see BSM_IDLE, BSM_IN_SESSION, etc. above) */
    UD8 micsState;

} BSM_STAT;
/*
 * Defines for BSM_STAT.flags:
 * 
 * BSM_ERR_OCCURRED:
 *     This is set when the base station detects an error, and cleared when
 *     the PC retrieves the error information (see BsmGetStatChanges() in
 *     "ZL7010XAdk\Sw\Pc\Libs\BsmLib.c").
 * BSM_LINK_STAT_CHANGED:
 *     This is set whenever the link status changes (or may have changed),
 *     and cleared when the PC gets the link status (see BsmGetStatChanges()
 *     in "ZL7010XAdk\Sw\Pc\Libs\BsmLib.c").
 * BSM_LINK_QUAL_CHANGED:
 *     This is set whenever the link quality statistics change, and cleared
 *     when the PC gets the link quality statistics (see BsmGetStatChanges()
 *     in "ZL7010XAdk\Sw\Pc\Libs\BsmLib.c").
 * BSM_400_MHZ_TX_ACTIVE:
 *     True when transmitting on 400 MHz.
 * BSM_400_MHZ_RX_ACTIVE:
 *     True when receiving on 400 MHz.
 * BSM_245_GHZ_TX_ACTIVE:
 *     True when transmitting on 2.45 GHz.
 * BSM_DATA_STAT_CHANGED:
 *     This is set whenever the data status changes, and cleared when the
 *     PC gets the data status (see BsmGetStatChanges() in "ZL7010XAdk\Sw\
 *     Pc\Libs\BsmLib.c").
 */
#define BSM_ERR_OCCURRED       (1 << 0)
#define BSM_LINK_STAT_CHANGED  (1 << 1)
#define BSM_LINK_QUAL_CHANGED  (1 << 2)
#define BSM_400_MHZ_TX_ACTIVE  (1 << 3)
#define BSM_400_MHZ_RX_ACTIVE  (1 << 4)  
#define BSM_245_GHZ_TX_ACTIVE  (1 << 5)
#define BSM_DATA_STAT_CHANGED  (1 << 6)

/* Structure used to hold changes in the base station status (see
 * BsmGetStatChanges() in "ZL7010XAdk\Sw\Pc\Libs\BsmLib.c").
 */
typedef struct {
    
    /* general status */
    BSM_STAT genStat;
    
    /* link status */
    MICS_LINK_STAT linkStat;
    
    /* link quality */
    MICS_LINK_QUAL linkQual;
    
    /* data status */
    MICS_DATA_STAT dataStat;

} BSM_STAT_CHANGES;

/* Structure for RSSI data returned by BsmCca().
 */
typedef struct {
    
    /* result of RSSI for each channel (indexed by channel) */
    UD16 rssi[MAX_MICS_CHAN + 1];
    
} BSM_CCA_DATA;

/* These are provided for backwards compatibility - use MICS_LINK_CONFIG,
 * MICS_LINK_STAT, and MICS_LINK_QUAL instead (defined in "ZL7010XAdk\Sw\
 * Includes\Adk\AnyMezz\MicsGeneral.h").
 */
typedef MICS_LINK_CONFIG  BSM_LINK_CONFIG;
typedef MICS_LINK_STAT    BSM_LINK_STAT;
typedef MICS_LINK_QUAL    BSM_LINK_QUAL;

/* Structure for the header at the beginning of the nonvolatile memory (NVM)
 * on the base station.
 */
typedef struct
{
    /* info that appears at the beginning of all NVMs */
    const NVM_INFO nvmInfo;

    /* the size of this header, including the NVM info */
    const UD8 nvmHeadSize;

    /* The offset and size of the NVM section that contains the factory
     * settings for the ZL7010X radio (see the BSM_MICS_FACT_NVM structure below).
     * The offset is the offset from the beginning of the NVM, and the size is the
     * size of the BSM_MICS_FACT_NVM section.
     */
    const UD8 micsFactNvmOffset;
    const UD8 micsFactNvmSize;

} BSM_NVM_HEAD;

/* Structure for the nonvolatile memory (NVM) on the base station. Note that
 * the firmware should not use this structure to access fields in the nonvolatile
 * memory, for the following reason. If the base station is updated with a new
 * firmware version and a new NVM, and the base station is later reprogrammed
 * with old firmware again, the contents of the new NVM on the base station won't
 * match the old BSM_NVM structure expected by the old firmware. To ensure old
 * firmware will work with a newer NVM, the firmware should access each section
 * of the NVM (e.g. micsFactNvm) only through global pointers that are
 * initialized by the application (e.g. MicsFactNvm).
 */
typedef struct {

    /* header for NVM on base station */
    BSM_NVM_HEAD head;

    /* section containing factory settings for ZL7010X radio */
    BSM_MICS_FACT_NVM micsFactNvm;

} BSM_NVM;

/**************************************************************************
 * Global Declarations
 */

/**************************************************************************
 * External Function Prototypes
 */
 
#endif /* ensure this file is only included once */
