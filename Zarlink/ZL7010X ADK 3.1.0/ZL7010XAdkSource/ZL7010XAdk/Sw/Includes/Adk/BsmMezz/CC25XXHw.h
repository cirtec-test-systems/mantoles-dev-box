/**************************************************************************
 * This file contains defines and macros to interface to the CC25XX chip
 * (CC2500 or CC2550).
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adk_BsmMezz_CC25XXHw_h
#define Adk_BsmMezz_CC25XXHw_h

#include "Adp/General.h"     /* WR_BIT(), ... */
#include "Adp/Build/Build.h" /* CC_SPI_CS_BIT, CC_SPI_CS_P(), ... */

/**************************************************************************
 * Defines and Macros
 */
 
/* defines for CC_SPI_CS_BIT I/O pin (SPI chip select to CC25XX) */
#define CC_WR_SPI_CS(val)  WR_BIT(CC_SPI_CS_P(OUT), CC_SPI_CS_BIT, val)
#define CC_SPI_ON          0 /* enable SPI interface on CC25XX */
#define CC_SPI_OFF         1 /* disable SPI interface on CC25XX */
                           
/**************************************************************************
 * Defines for registers in CC25XX (register addresses & related).
 */
 
/* This must be OR'ed with a register address to read a register in the
 * CC25XX chip. It sets the most significant bit of the address byte.
 */
#define CC_READ  0x80

/* This must be OR'ed with a register address to perform a burst access
 * (read or write consecutive registers in the CC25XX chip), or to read
 * a status register in the CC25XX chip.
 */
#define CC_BURST 0x40

/* Addresses for configuration registers in CC25XX chip (see CC25XX manual for
 * register descriptions).
 */
#define CC_IOCFG1    0x01
#define CC_IOCFG0    0x02
#define CC_FIFOTHR   0x03
#define CC_SYNC1     0x04
#define CC_SYNC0     0x05
#define CC_PKTLEN    0x06
#define CC_PKTCTRL0  0x08
#define CC_CHANNR    0x0A
#define CC_FREQ2     0x0D
#define CC_FREQ1     0x0E
#define CC_FREQ0     0x0F
#define CC_MDMCFG4   0x10
#define CC_MDMCFG3   0x11
#define CC_MDMCFG2   0x12
#define CC_MDMCFG1   0x13
#define CC_MDMCFG0   0x14
#define CC_DEVIATN   0x15
#define CC_MCSM1     0x17
#define CC_MCSM0     0x18
#define CC_FREND0    0x22
#define CC_FSCAL3    0x23
#define CC_FSCAL2    0x24
#define CC_FSCAL1    0x25
#define CC_FSCAL0    0x26
#define CC_FSTEST    0x29
#define CC_PTEST     0x2A
#define CC_TEST2     0x2C
#define CC_TEST1     0x2D
#define CC_TEST0     0x2E

/* Commands for CC25XX chip (command strobes). To tell the CC25XX to execute a
 * command, pass the command to CcCommand().
 * 
 * CC_SRES:
 *     Reset chip.
 * CC_SFSTXON:
 *     Enable and calibrate frequency synthesizer (if MCSM0.FS_AUTOCAL=1).
 * CC_SXOFF:
 *     Turn off crystal oscillator.
 * CC_SCAL:
 *     Calibrate frequency synthesizer and turn it off (enables quick start).
 *     SCAL can be strobed in IDLE state without setting manual calibration
 *     mode (MCSM0.FS_AUTOCAL=0).
 * CC_STX:
 *     Enable TX. Perform calibration first if MCSM0.FS_AUTOCAL=1.
 * CC_SIDLE:
 *     Exit TX and turn off frequency synthesizer.
 * CC_SPWD:
 *     Enter power down mode when CSn goes high.
 * CC_SFTX:
 *     Flush the TX FIFO buffer.
 * CC_SNOP:
 *     No operation. May be used to pad strobe commands to two bytes for
 *     simpler software.
 */
#define CC_SRES     0x30
#define CC_SFSTXON  0x31
#define CC_SXOFF    0x32
#define CC_SCAL     0x33
#define CC_STX      0x35
#define CC_SIDLE    0x36
#define CC_SPWD     0x39
#define CC_SFTX     0x3B
#define CC_SNOP     0x3D

/* Status registers in CC25XX chip. To read a status register, pass the status
 * register address to CcRead(). Note status registers can only be read one at
 * a time, so CcReadBurst() can't be used to read status registers. Also,
 * status registers are read-only, so they can't be written. 
 * 
 * CC_PARTNUM:
 *     CC25XX part number.
 * CC_VERSION:
 *     Current version number.
 * CC_MARCSTATE:
 *     Control state machine state.
 * CC_PKTSTATUS:
 *     Current GDOx status and packet status.
 * CC_VCO_VC_DAC:
 *     Current setting from PLL calibration module.
 * CC_TXBYTES:
 *     Underflow and number of bytes in the TX FIFO.
 */
#define CC_PARTNUM     (CC_BURST | 0x30)
#define CC_VERSION     (CC_BURST | 0x31) 
#define CC_MARCSTATE   (CC_BURST | 0x35) 
#define CC_PKTSTATUS   (CC_BURST | 0x38) 
#define CC_VCO_VC_DAC  (CC_BURST | 0x39) 
#define CC_TXBYTES     (CC_BURST | 0x3A) 
 
/* Address to access PATABLE (power amplifier table). To access just the first
 * byte in the PATABLE, you may use CcRead() and CcWrite(). To access more
 * than the first byte, you must use CcReadBurst() and CcWriteBurst().
 */
#define CC_PATABLE      0x3E

/* Address to access TX FIFO (transmit FIFO). To write one byte into the TX
 * FIFO, you may use CcWrite(). To write more than one byte, you must use
 * CcWriteBurst(). The TX FIFO is write-only, so it may not be read.
 */
#define CC_TXFIFO       0x3F
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global Declarations
 */

/**************************************************************************
 * External Function Prototypes
 */
 
#endif /* ensure this file is only included once */
