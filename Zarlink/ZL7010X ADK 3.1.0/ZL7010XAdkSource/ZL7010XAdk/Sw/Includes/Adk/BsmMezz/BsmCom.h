/**************************************************************************
 * This include file contains defines and structures used to communicate
 * with the base station mezzanine board.
 *
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
/* ensure this file is only included once */
#ifndef Adk_BsmMezz_BsmCom_h
#define Adk_BsmMezz_BsmCom_h

#include "Adp/Build/Build.h"         /* UD8, UD16, SD8, BOOL8, ... */ 
#include "Adp/AnyBoard/Com.h"        /* COM_LOCAL_MEZZ_ADDR, ... */ 
#include "Adk/AnyMezz/MicsGeneral.h" /* MICS_LINK_STAT, MICS_LINK_QUAL, ... */
#include "Adk/BsmMezz/BsmGeneral.h"  /* BSM_STAT, BSM_MICS_CONFIG, ... */

/**************************************************************************
 * Defines and Macros
 */
 
/* Command types for base station. The command and reply structures for each of
 * these are defined later on. Note the base station also supports the commands
 * that are common to all devices (see COM_GET_DEV_TYPE_CMD_TYPE, etc. in
 * "AppDevPlat\Sw\Includes\Adp\AnyBoard\Com.h").
 */
#define BSM_GET_STAT_CMD_TYPE                  1   /* 0x01 (legacy) */
#define BSM_RESERVED_CMD_TYPE_2                2   /* 0x02 (future use) */
#define BSM_RESERVED_CMD_TYPE_3                3   /* 0x03 (future use) */
#define BSM_GET_MICS_CONFIG_CMD_TYPE           4   /* 0x04 */ 
#define BSM_SET_MICS_CONFIG_CMD_TYPE           5   /* 0x05 */
#define BSM_START_SESSION_CMD_TYPE             6   /* 0x06 */
#define BSM_SEARCH_FOR_IMPLANT_CMD_TYPE        7   /* 0x07 */
#define BSM_LISTEN_FOR_EMERGENCY_CMD_TYPE      8   /* 0x08 */
#define BSM_ABORT_MICS_CMD_TYPE                9   /* 0x09 */
#define BSM_READ_MICS_REG_CMD_TYPE             10  /* 0x0A */
#define BSM_RESERVED_CMD_TYPE_11               11  /* 0x0B (obsolete) */
#define BSM_RECEIVE_MICS_CMD_TYPE              12  /* 0x0C */
#define BSM_WRITE_MICS_REG_CMD_TYPE            13  /* 0x0D */
#define BSM_RESERVED_CMD_TYPE_14               14  /* 0x0E (obsolete) */
#define BSM_TRANSMIT_MICS_CMD_TYPE             15  /* 0x0F */
#define BSM_MICS_CAL_CMD_TYPE                  16  /* 0x10 */
#define BSM_MICS_ADC_CMD_TYPE                  17  /* 0x11 */
#define BSM_SET_TX_245_POWER_CMD_TYPE          18  /* 0x12 */
#define BSM_START_TX_245_CARRIER_CMD_TYPE      19  /* 0x13 */
#define BSM_START_TX_400_CARRIER_CMD_TYPE      20  /* 0x14 */
#define BSM_RSSI_CMD_TYPE                      21  /* 0x15 */
#define BSM_ENAB_EXT_RSSI_CMD_TYPE             22  /* 0x16 */
#define BSM_CCA_CMD_TYPE                       23  /* 0x17 */
#define BSM_GET_LINK_STAT_CMD_TYPE             24  /* 0x18 (legacy) */
#define BSM_GET_LINK_QUAL_CMD_TYPE             25  /* 0x19 (legacy) */
#define BSM_GET_TRACE_MSG_CMD_TYPE             26  /* 0x1A */
#define BSM_COPY_MICS_REGS_CMD_TYPE            27  /* 0x1B */
#define BSM_GET_STAT_CHANGES_CMD_TYPE          28  /* 0x1C */
#define BSM_START_DATA_TEST_CMD_TYPE           29  /* 0x1D */
#define BSM_SET_TX_245_FREQ_CMD_TYPE           30  /* 0x1E */
#define BSM_RESET_MICS_CMD_TYPE                31  /* 0x1F */
#define BSM_CALC_MICS_CRC_CMD_TYPE             32  /* 0x20 */
#define BSM_WRITE_CC_REG_CMD_TYPE              33  /* 0x21 */
#define BSM_READ_CC_REG_CMD_TYPE               34  /* 0x22 */
#define BSM_RESERVED_CMD_TYPE_35               35  /* 0x23 (obsolete) */
#define BSM_SLEEP_CMD_TYPE                     36  /* 0x24 */
#define BSM_WAKEUP_CMD_TYPE                    37  /* 0x25 */
#define BSM_GET_MICS_FACT_NVM_CMD_TYPE         38  /* 0x26 */

/* Address for the local base station mezzanine board. This is the base station
 * on the local side of the wireless link (i.e. the one connected to the local
 * ADP board, which in turn is connected to the local PC via USB). This address
 * is used for the source & destination in communication packet headers (see
 * COM_PACK_HDR.srcAndDest in "AppDevPlat/Sw/Includes/Adp/AnyBoard/Com.h").
 */
#define BSM_LOCAL_ADDR  COM_LOCAL_MEZZ_ADDR

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/******************************************************************
 * Structures for BSM_GET_STAT_CMD_TYPE:
 */
typedef BSM_STAT  BSM_GET_STAT_REPLY;

/******************************************************************
 * Structures for BSM_GET_MICS_CONFIG_CMD_TYPE:
 */
typedef BSM_MICS_CONFIG  BSM_GET_MICS_CONFIG_REPLY;

/******************************************************************
 * Structures for BSM_SET_MICS_CONFIG_CMD_TYPE:
 */
typedef BSM_MICS_CONFIG  BSM_SET_MICS_CONFIG_CMD;
    
/******************************************************************
 * Structures for BSM_START_SESSION_CMD_TYPE.
 */
typedef struct {
    
    /* If true, the MICS watchdog timer will be enabled, so the start session
     * will be aborted if the session doesn't start within ~4.37 seconds.
     */
    BOOL8 enabWatchdog;
    
    /* Time to wait for the session to start (seconds) before replying. If 0,
     * the base station will configure the MICS chip to start a session and
     * reply immediately without waiting (not an error). Note if "timeoutSec"
     * and "enabWatchdog" are both specified, and "timeoutSec" is > 4.37, the
     * actual timeout will be ~4.37 seconds (MICS watchdog timeout).
     */
    UD8 timeoutSec;
    
} BSM_START_SESSION_CMD;

/******************************************************************
 * Structures for BSM_SEARCH_FOR_IMPLANT_CMD_TYPE.
 */
typedef struct {
    
    /* If true, the MICS watchdog timer will be enabled, so the search will be
     * aborted after ~4.37 seconds.
     */
    BOOL8 enabWatchdog;
    
    /* if true, wakeup implant for any company */
    BOOL8 anyCompany;
    
    /* if true, wakeup any implant (provided the company ID matches) */
    BOOL8 anyImplant;
    
} BSM_SEARCH_FOR_IMPLANT_CMD;

/******************************************************************
 * Structures for BSM_LISTEN_FOR_EMERGENCY_CMD_TYPE.
 */
typedef struct {
    
    /* If this is true, the base station will monitor the received emergency
     * transmissions, and if any match the implant ID specified in the MICS
     * config (see BSM_SET_MICS_CONFIG_CMD), the base station will start an
     * emergency session with the implant. If this is false, the base station
     * will leave the implant ID's in the RX buffer for the application to read.
     */
    BOOL8 startSession;
    
} BSM_LISTEN_FOR_EMERGENCY_CMD;

/******************************************************************
 * Structures for BSM_READ_MICS_REG_CMD_TYPE:
 */
typedef struct {
    
    /* address of register to read in MICS chip */
    UD16 regAddr;
    
    /* if true, use housekeeping to read register on remote MICS chip */
    BOOL8 remote;

} BSM_READ_MICS_REG_CMD;

typedef struct {
    
    /* value of register read from MICS chip */
    UD8 regVal;

} BSM_READ_MICS_REG_REPLY;

/******************************************************************
 * Structures for BSM_RECEIVE_MICS_CMD_TYPE:
 */
typedef struct {
    
    /* maximum length to read from MICS RX buf */
    UD16 maxLen;

} BSM_RECEIVE_MICS_CMD;

/******************************************************************
 * Structures for BSM_WRITE_MICS_REG_CMD_TYPE:
 */
typedef struct {
    
    /* address of register to write to in MICS chip */
    UD16 regAddr;
    
    /* value to write to register */
    UD8 regVal;
    
    /* if true, use housekeeping to write to register on remote MICS chip */
    BOOL8 remote;

} BSM_WRITE_MICS_REG_CMD;

/******************************************************************
 * Structures for BSM_MICS_CAL_CMD_TYPE:
 */
typedef struct {
    
    /* The group 1 calibrations to perform. Each bit corresponds to a bit in
     * the CALSELECT1 register on the MICS chip (defined in "ZL7010XAdk\Sw\
     * Includes\Adk\AnyMezz\MicsHw.h").
     */
    UD8 calSelect1;
    
    /* The channel to use for the CAL_400_MHZ_ANT calibration on the local
     * MICS chip. Note this is ignored for all other calibrations, includng
     * the CAL_400_MHZ_ANT on the remote MICS chip because that must use the
     * current channel for the active session.
     */
    SD8 chan;
    
    /* If true, the MICS housekeeping interface is used to perform the
     * calibrations on the remote implant instead of the base station.
     */
    BOOL8 remote;
    
    /* The group 2 calibrations to perform. Each bit corresponds to a bit in
     * the CALSELECT2 register on the ZL70102/103 (defined in "ZL7010XAdk\Sw\
     * Includes\Adk\AnyMezz\MicsHw.h"). This has no meaning for a ZL70101.
     */
    UD8 calSelect2;
    
} BSM_MICS_CAL_CMD;

/******************************************************************
 * Structures for BSM_MICS_ADC_CMD_TYPE:
 */
typedef struct {
    
    /* The ADC input to use on the MICS chip (see ADC_INPUT_TEST_IO_1,
     * ADC_INPUT_VSUP, etc. in "Adk/AnyMezz/MicsHw.h").
     */
    UD8 adcInput;
    
    /* If true, the MICS housekeeping interface is used to perform the
     * A/D conversion on the remote MICS chip instead of the local MICS chip.
     */
    BOOL8 remote;
    
} BSM_MICS_ADC_CMD;

typedef struct {
    
    /* result of A/D conversion */
    UD8 adcResult;

} BSM_MICS_ADC_REPLY;

/******************************************************************
 * Structures for BSM_SET_TX_245_POWER_CMD_TYPE:
 */
typedef struct {
    
    /* This value is written to the CC25XX PA register. When properly
     * calibrated, it determines the TX power.
     */
    UD8 ccPa;
    
} BSM_SET_TX_245_POWER_CMD;

/******************************************************************
 * Structures for BSM_START_TX_245_CARRIER_CMD_TYPE:
 */
typedef struct {
    
    /* start if true, stop if false */
    BOOL8 start;
    
} BSM_START_TX_245_CARRIER_CMD;

/******************************************************************
 * Structures for BSM_START_TX_400_CARRIER_CMD_TYPE:
 */
typedef struct {
    
    /* start if true, stop if false */
    BOOL8 start;
    
    /* MICS channel to transmit 400 MHz TX carrier for */
    UD8 chan;
    
} BSM_START_TX_400_CARRIER_CMD;

/******************************************************************
 * Structures for BSM_RSSI_CMD_TYPE:
 */
typedef struct {
    
    /* If true, the internal RSSI is used (factory test/debug use only), in
     * which case the "average" option has no effect because the internal RSSI
     * always averages over 10 ms.
     */
    BOOL8 useIntRssi;

    /* MICS channel to do RSSI for */
    UD8 chan;
    
    /* if true, the average RSSI is calculated instead of the max */
    BOOL8 average;
    
} BSM_RSSI_CMD;

typedef struct {
    
    /* result of RSSI */
    UD16 rssi;

} BSM_RSSI_REPLY;

/******************************************************************
 * Structures for BSM_ENAB_EXT_RSSI_CMD_TYPE:
 */
typedef struct {
    
    /* enable if true, disable if false */
    BOOL8 enab;
    
    /* MICS channel to use for external RSSI */
    UD8 chan;
    
} BSM_ENAB_EXT_RSSI_CMD;

/******************************************************************
 * Structures for BSM_CCA_CMD_TYPE:
 */
typedef struct {
    
    /* if true, the average RSSI is used for the CCA instead of the max */
    BOOL8 average;
    
} BSM_CCA_CMD;

typedef struct {
    
    /* clearest channel */
    UD8 clearChan;
    
    /* CCA data (RSSI results for each MICS channel) */
    BSM_CCA_DATA data;

} BSM_CCA_REPLY;

/******************************************************************
 * Structures for BSM_GET_LINK_STAT_CMD_TYPE:
 */
typedef MICS_LINK_STAT  BSM_GET_LINK_STAT_REPLY;

/******************************************************************
 * Structures for BSM_GET_LINK_QUAL_CMD_TYPE:
 */
typedef MICS_LINK_QUAL  BSM_GET_LINK_QUAL_REPLY;

/******************************************************************
 * Structures for BSM_COPY_MICS_REGS_CMD_TYPE:
 */
typedef struct {
    
    /* If true, the base will use MICS housekeeping to execute the "copy
     * registers" command on the remote implant instead of the base.
     */
    BOOL8 remote;
    
} BSM_COPY_MICS_REGS_CMD;

/******************************************************************
 * Structures for BSM_GET_STAT_CHANGES_CMD_TYPE:
 */
typedef struct {
    
    /* if true, also get sections of the status that haven't changed */
    BOOL8 force;
    
} BSM_GET_STAT_CHANGES_CMD;

/******************************************************************
 * Structures for BSM_START_DATA_TEST_CMD_TYPE
 */
typedef MICS_DT_SPECS  BSM_START_DATA_TEST_CMD;

/******************************************************************
 * Structures for BSM_SET_TX_245_FREQ_CMD_TYPE:
 */
typedef struct {
    
    /* the values for the CC25XX's FREQ0, FREQ1, and FREQ2 registers */
    UD8 ccFreq0;
    UD8 ccFreq1;
    UD8 ccFreq2;
    
} BSM_SET_TX_245_FREQ_CMD;

/******************************************************************
 * Structures for BSM_CALC_MICS_CRC_CMD_TYPE:
 */
typedef struct {

    /* If true, the base will use MICS housekeeping to execute the "calculate
     * CRC" command on the remote implant instead of the base.
     */
    BOOL8 remote;

} BSM_CALC_MICS_CRC_CMD;

/******************************************************************
 * Structures for BSM_WRITE_CC_REG_CMD_TYPE:
 */
typedef struct {
    
    /* address of register to write to in CC25XX */
    UD8 regAddr;
    
    /* value to write to register */
    UD8 regVal;

} BSM_WRITE_CC_REG_CMD;

/******************************************************************
 * Structures for BSM_READ_CC_REG_CMD_TYPE:
 */
typedef struct {

    /* address of register to write to in CC25XX */
    UD8 regAddr;

} BSM_READ_CC_REG_CMD;

typedef struct {

    /* value of register read from CC25XX */
    UD8 regVal;

} BSM_READ_CC_REG_REPLY;

/******************************************************************
 * Structures for BSM_GET_MICS_FACT_NVM_CMD_TYPE:
 */
typedef BSM_MICS_FACT_NVM BSM_GET_MICS_FACT_NVM_REPLY;

/******************************************************************
 * Global Declarations
 */

/**************************************************************************
 * External Function Prototypes
 */
 
#endif /* ensure this file is only included once */
