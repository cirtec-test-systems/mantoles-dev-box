/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adk_BsmMezz_BsmAppErrs_h
#define Adk_BsmMezz_BsmAppErrs_h

/**************************************************************************
 * Defines and Macros
 */
 
/* Error group and error codes for the application software on the base station.
 * Note for the error group, the firmware should always reference the global
 * variable BsmAppErr instead of BSM_APP_ERR, so all of the references will
 * share the same constant global string.
 * 
 * The error ID strings (EID) include a detailed message for each error. These
 * are provided for use on the host (PC). For the format and use of error ID
 * strings, see "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c". The firmware doesn't use
 * the error ID strings because they would require too much memory (the
 * firmware only uses error groups and error codes).
 */
extern const char *BsmAppErr;
#define BSM_APP_ERR "BsmAppErr"
/**/
#define BSM_APP_ERR_MICS_DATA_NOT_YET_SUPPORTED                              1
#define BSM_APP_EID_MICS_DATA_NOT_YET_SUPPORTED \
    "BsmAppErr.1: The base station does not yet support ADK command/reply " \
    "packet communication via ZL7010X session data."
#define BSM_APP_ERR_RECEIVED_CMD_WITH_INVALID_CMD_TYPE                       2
#define BSM_APP_EID_RECEIVED_CMD_WITH_INVALID_CMD_TYPE \
    "BsmAppErr.2: The base station received an ADK command packet with " \
    "an invalid command type ({0})."
#define BSM_APP_ERR_RECEIVED_CMD_WITH_INVALID_LEN                            3
#define BSM_APP_EID_RECEIVED_CMD_WITH_INVALID_LEN \
    "BsmAppErr.3: The base station received an ADK command packet with " \
    "an invalid length."
#define BSM_APP_ERR_MICS_DATA_RX_TIMEOUT                                     4
#define BSM_APP_EID_MICS_DATA_RX_TIMEOUT \
    "BsmAppErr.4: The base station timed out waiting for the ZL7010X to " \
    "receive session data."
#define BSM_APP_ERR_MICS_DATA_TX_TIMEOUT                                     5
#define BSM_APP_EID_MICS_DATA_TX_TIMEOUT \
    "BsmAppErr.5: The base station timed out waiting for the ZL7010X to " \
    "transmit session data."
#define BSM_APP_ERR_MICS_HK_RX_TIMEOUT                                       6
#define BSM_APP_EID_MICS_HK_RX_TIMEOUT \
    "BsmAppErr.6: The base station timed out waiting to receive an ADK " \
    "communication packet via the ZL7010X housekeeping interface."
#define BSM_APP_ERR_MICS_HK_TX_TIMEOUT                                       7
#define BSM_APP_EID_MICS_HK_TX_TIMEOUT \
    "BsmAppErr.7: The base station timed out waiting to transmit an ADK " \
    "communication packet via the ZL7010X housekeeping interface."
#define BSM_APP_ERR_ADP_SPI_RX_TIMEOUT                                       8
#define BSM_APP_EID_ADP_SPI_RX_TIMEOUT \
    "BsmAppErr.8: The base station timed out waiting to receive an ADK " \
    "communication packet from the ADP board via the ADP SPI interface."
#define BSM_APP_ERR_ADP_SPI_TX_TIMEOUT                                       9
#define BSM_APP_EID_ADP_SPI_TX_TIMEOUT \
    "BsmAppErr.9: The base station timed out waiting to transmit an ADK " \
    "communication packet to the ADP board via the ADP SPI interface."
#define BSM_APP_ERR_RECEIVED_PACK_WITH_DEST_0                                10
#define BSM_APP_EID_RECEIVED_PACK_WITH_DEST_0 \
    "BsmAppErr.10: The base station received an ADK communication packet " \
    "with destination address 0."
#define BSM_APP_ERR_MICS_INT_WITH_NO_STAT                                    11
#define BSM_APP_EID_MICS_INT_WITH_NO_STAT \
    "BsmAppErr.11: The ZL7010X on the base station asserted an interrupt, " \
    "but its reg_irq_status1 and 2 were both 0."
#define BSM_APP_ERR_MICS_CRC_ERR                                             12
#define BSM_APP_EID_MICS_CRC_ERR \
    "BsmAppErr.12: The ZL7010X on the base station asserted the " \
    "irq_crcerr interrupt."
#define BSM_APP_ERR_INVALID_TX_LEN                                           13
#define BSM_APP_EID_INVALID_TX_LEN \
    "BsmAppErr.13: The specified transmit length ({0}) was not a " \
    "multiple of the TX block size ({1})."
#define BSM_APP_ERR_14                                                       14
#define BSM_APP_EID_14 \
    "BsmAppErr.14: Old error code (should no longer occur)."
#define BSM_APP_ERR_MICS_SYNTH_FAILED_TO_LOCK                                15
#define BSM_APP_EID_MICS_SYNTH_FAILED_TO_LOCK \
    "BsmAppErr.15: The synthesizer on the base station's ZL7010X " \
    "failed to lock."
#define BSM_APP_ERR_START_SESSION_TIMEOUT                                    16
#define BSM_APP_EID_START_SESSION_TIMEOUT \
    "BsmAppErr.16: The base station timed out waiting for a session " \
    "to start (no response from the implant)."
#define BSM_APP_ERR_WRONG_MICS_REV_ON_BASE                                   17
#define BSM_APP_EID_WRONG_MICS_REV_ON_BASE \
    "BsmAppErr.17: The revision read from the ZL7010X on the base " \
    "station ({0}) was wrong. Make sure the base has the correct firmware."
#define BSM_APP_ERR_18                                                       18
#define BSM_APP_EID_18 \
    "BsmAppErr.18: Old error code (should no longer occur)."
#define BSM_APP_ERR_WRONG_MICS_REV_ON_IMPLANT                                19
#define BSM_APP_EID_WRONG_MICS_REV_ON_IMPLANT \
    "BsmAppErr.19: The revision read from the ZL7010X on the remote " \
    "implant ({0}) didn't match the expected value ({1}). If it's not " \
    "a compatible revision, it could cause serious problems."
#define BSM_APP_ERR_NOT_ALLOWED_TO_CHANGE_COMPANY_ID                         20
#define BSM_APP_EID_NOT_ALLOWED_TO_CHANGE_COMPANY_ID \
    "BsmAppErr.20: An attempt was made to change the ZL7010X " \
    "company ID on the base station to something other than 1, " \
    "which ADK base station does not allow."

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global Declarations
 */
 
/**************************************************************************
 * External Function Prototypes
 */
 
#endif /* ensure this file is only included once */
