/**************************************************************************
 * This is the build include for BSM300 base station mezzanine boards (with a
 * ZL70103). This contains various platform-dependent defines and macros. For a
 * description of build includes and how they're used, see "Adp/Build/Build.h".
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2014. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adk_Build_BuildBsm300_h
#define Adk_Build_BuildBsm300_h

/* indicate we're building for a BSM300 with a ZL70103 */
#define BSM_HW    BSM300
#define MICS_REV  ZL70103

/* include build file shared by all base station mezzanine boards */
#include "Adk/Build/BuildBsm.h"

#endif /* ensure this file is only included once */
