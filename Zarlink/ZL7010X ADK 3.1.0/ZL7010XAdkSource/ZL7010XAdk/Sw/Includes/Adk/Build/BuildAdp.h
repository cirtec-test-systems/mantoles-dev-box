/**************************************************************************
 * This is the build include for the ZL7010X ADK application on the ADP board.
 * It defines various platform-dependent defines and macros as required for the
 * ZL7010X ADK application on the ADP board. For a description of build
 * includes and how they're used, see "Adp/Build/Build.h".
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adk_Build_BuildAdp_h
#define Adk_Build_BuildAdp_h

/* Include the default build file for the ADP board (provided by the
 * Application Development Platform).
 */
#include "Adp/Build/BuildAdpBoard.h"

/* settings for VSUP1 & VSUP2 (millivolts) */
#define ADP_VSUP1_MV  3300
#define ADP_VSUP2_MV  3300

#endif /* ensure this file is only included once */
