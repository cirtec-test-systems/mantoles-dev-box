/**************************************************************************
 * This is the default build include for all implant mezzanine boards, which
 * contains various platform-dependent defines and macros. Note this is shared
 * by boards with a ZL70101, ZL70102, or ZL70103. For a description of build
 * includes and how they're used, see "Adp/Build/Build.h".
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adk_Build_BuildAim_h
#define Adk_Build_BuildAim_h

/* If debug, include "Standard/StdIo.h" so printf() can easily be invoked in
 * any file that includes this file (via "Adp/Build/Build.h").
 */
#ifdef DEBUG
#include "Standard/StdIo.h"
#endif

/* include file for micro-controller on implant (provided with compiler) */
#include "msp430f1611.h"  /* P1IN, P1OUT, BIT0, ... */

/* include the default build file for MSP430 platforms */
#include "Adp/Build/BuildMsp430.h" /* UD32, P1IN, BIT0, ... */

/**************************************************************************
 * Defines and Macros
 */

/* Define clock frequencies for the implant mezzanine board. Note these must
 * match the actual clock frequencies on the board, which depend on the
 * hardware (i.e. the crystal oscillator, etc.) and the way the firmware
 * configures the micro-controller.
 */
#define MCLK_FREQ   (UD32)4000000UL
#define ACLK_FREQ   (UD32)8000000UL
#define SMCLK_FREQ  (UD32)800000UL  /* approximate (DCOCLK with defaults) */

/* The starting address of the flash memory to use to store the implant's
 * nonvolatile settings (see the IM_NVM structure). The implant uses segment A
 * in the MSP430F1611's information memory.
 */
#define NVM_ADDR  0x1080

/* indicate we're building software for MICS implant */
#define IMPLANT

/* Define a hook to call when starting an HK TX (to turn on the TX 400 LED).
 * This hook is called by MicsReadR() and MicsWriteR().
 */
extern void ImAppHookForStartHkTx(void);
#define MICS_APP_HOOK_FOR_START_HK_TX()  ImAppHookForStartHkTx()

/* Set the number of milliseconds per system timer interrupt. This is
 * referenced by "Adp/AnyBoard/SysTimerHw.h", and determines the resolution
 * of the millisecond time (see StMs(), etc. in "Adp/AnyBoard/SysTimerLib.h").
 * If this isn't defined, it defaults to 1 ms. On an implant, it's set to 2 ms
 * to reduce the percentage of time spent servicing the system timer interrupt.
 * It takes a total of 21 clock cycles to service the system timer interrupt.
 * Thus, for an MSP430 running at 4MHz, the total time is 5.25 us (21/4000000),
 * and if ST_MS_PER_INT = 2, the percentage overhead is 0.2625% (5.25/2000).
 */
#define ST_MS_PER_INT  2

/* I/O port 1:
 * 
 * ADP_IO_0_BIT, ... (schematic ADP_IO<0>, ...):
 *     General purpose I/O bits connected to ADP board. When these aren't
 *     driven, they're pulled up by the level shifters on the ADP board.
 * ASPI_SLAVE_READY_BIT (schematic ADP_SPI_RDY_B):
 *     ADP SPI slave ready. When this isn't driven, it's pulled up by the level
 *     shifters on the ADP board. It must be initialized as an input, but the
 *     corresponding bit in P1OUT must also be initialized to 0 so it will be
 *     driven low when it's switched to an output. For more information, see
 *     ASPI_SLAVE_READY in "Adp/AnyBoard/AdpSpiHw.h".
 * P1DIR_INIT, ...:
 *     Initial I/O settings.
 * ..._P():
 *     Macros to form names of I/O port registers for each bit.
 */
#define ADP_IO_0_BIT          BIT0  /* ADP_IO<0> */
#define ADP_IO_1_BIT          BIT1  /* ADP_IO<1> */
#define ADP_IO_2_BIT          BIT2  /* ADP_IO<2> */
#define ADP_IO_3_BIT          BIT3  /* ADP_IO<3> */
#define ADP_IO_4_BIT          BIT4  /* ADP_IO<4> */
#define ADP_IO_5_BIT          BIT5  /* ADP_IO<5> */
#define ADP_IO_6_BIT          BIT6  /* ADP_IO<6> */
#define ASPI_SLAVE_READY_BIT  BIT7  /* ADP_SPI_RDY_B */
/**/
#define P1DIR_INIT  (0)
#define P1OUT_INIT  (0)
/**/
#define ADP_IO_P(suffix)            P1##suffix
#define ASPI_SLAVE_READY_P(suffix)  P1##suffix

/* I/O port 2:
 * 
 * MICS_INT_BIT (schematic ZL_IRQ):
 *     Interrupt request from MICS chip (ZL7010X). High = interrupt asserted.
 *     To manage this bit, use MICS_INT_FLAG, etc. defined in
 *     "Adk\AnyMezz\MicsHw.h".
 * UNUSED_2_1_BIT, ...:
 *     Unused bits (init as outputs to prevent floating & reduce power).
 * MICS_VDDD_BIT (schematic ZL_VDDD):
 *     VDDD from MICS chip (ZL7010X). To read current value, use MICS_VDDD
 *     defined in "Adk\AnyMezz\MicsHw.h".
 * TP_P2_BIT5, ... (schematic P2.5 test point, ...):
 *     Test points (init as outputs to prevent floating & reduce power).
 * P2DIR_INIT, ...:
 *     Initial I/O settings.
 * ..._P():
 *     Macros to form names of I/O port registers for each bit.
 */
#define MICS_INT_BIT    BIT0  /* ZL_IRQ */
#define UNUSED_P2_BIT1  BIT1
#define UNUSED_P2_BIT2  BIT2
#define MICS_VDDD_BIT   BIT3  /* ZL_VDDD */
#define UNUSED_P2_BIT4  BIT4
#define TP_P2_BIT5      BIT5  /* P2.5 test point */
#define TP_P2_BIT6      BIT6  /* P2.6 test point */
#define TP_P2_BIT7      BIT7  /* P2.7 test point */
/**/
#define P2DIR_INIT  (BIT1 | BIT2 | BIT4 | BIT5 | BIT6 | BIT7)
#define P2OUT_INIT  (0)
/**/
#define MICS_INT_P(suffix)   P2##suffix
#define MICS_VDDD_P(suffix)  P2##suffix
#define TP_P2_P(suffix)      P2##suffix

/* I/O port 3:
 * 
 * MICS_SPI_CS_BIT (schematic ZL_SPI_CS_B):
 *     SPI chip select to MICS chip (ZL7010X). Note 0 = enable SPI bus
 *     interface on chip. This is initialized high. To change it, use
 *     MICS_WR_SPI_CS() defined in "Adk\AnyMezz\MicsHw.h".
 * LSPI_CLK_BIT, ... (schematic SPI_CLK, ...):
 *     Local SPI interface used to transfer TX/RX data to/from the MICS chip
 *     (ZL7010X). For these bits, the USART function is selected, so the MSP430
 *     controls their direction automatically. For more information, see
 *     "AppDevPlat/Sw/AnyBoard/Libs/LocalSpiLib.c".
 * MICS_WU_EN_BIT (schematic WU_EN):
 *     MICS chip WU_EN input (wakeup enable). To manage this bit, use
 *     MICS_WU_EN and MICS_WR_WU_EN() defined in "Adk/AnyMezz/MicsHw.h".
 *     This is initialized high on an implant so the MICS chip will wake
 *     up in implant mode.
 * MICS_IBS_BIT (schematic IBS):
 *     MICS chip IBS input. To change this bit, use MICS_WR_IBS() defined in
 *     "Adk/AnyMezz/MicsHw.h". This is initialized low on an implant.
 * MICS_MODE0_BIT, MICS_MODE1_BIT (schematic MODE0 & MODE1):
 *     MICS chip MODE0 and MODE1 inputs. To change these bits, use
 *     MICS_WR_MODE0() and MICS_WR_MODE1 defined in "Adk/AnyMezz/MicsHw.h".
 *     These are initialized to 0.
 * P3DIR_INIT, ...:
 *     Initial I/O settings.
 * ..._P():
 *     Macros to form names of I/O port registers for each bit.
 */
#define MICS_SPI_CS_BIT  BIT0  /* ZL_SPI_CS_B */
#define LSPI_SIMO_BIT    BIT1  /* SPI_SDO */
#define LSPI_SOMI_BIT    BIT2  /* SPI_SDI */
#define LSPI_CLK_BIT     BIT3  /* SPI_CLK */
#define MICS_WU_EN_BIT   BIT4  /* WU_EN */
#define MICS_IBS_BIT     BIT5  /* IBS */
#define MICS_MODE0_BIT   BIT6  /* MODE0 */
#define MICS_MODE1_BIT   BIT7  /* MODE1 */
/**/
#define P3DIR_INIT  (BIT0 | BIT4 | BIT5 | BIT6 | BIT7)
#define P3OUT_INIT  (BIT0 | BIT4)
/**/
#define MICS_SPI_CS_P(suffix)  P3##suffix
#define LSPI_SIMO_P(suffix)    P3##suffix
#define LSPI_SOMI_P(suffix)    P3##suffix
#define LSPI_CLK_P(suffix)     P3##suffix
#define MICS_WU_EN_P(suffix)   P3##suffix
#define MICS_IBS_P(suffix)     P3##suffix
#define MICS_MODE0_P(suffix)   P3##suffix
#define MICS_MODE1_P(suffix)   P3##suffix

/* I/O port 4:
 * 
 * TX_400_LED, ... (schematic 400MHZ_TX, ...):
 *     Bits for LED's in bank A (0=on, 1=off).
 * UNUSED_P4_BIT3, ...:
 *     Unused bits (init as outputs to prevent floating & reduce power).
 * MICS_RX_MODE_BIT (schematic RX_EN):
 *     RX mode from MICS chip. This is an input from the MICS chip. It is 1
 *     while the receiver is enabled in the MICS chip (i.e. the RX_RF & RX_IF
 *     subsystems are enabled). To read this bit, use MICS_RX_MODE defined
 *     in "Adk/AnyMezz/MicsHw.h".
 * P4DIR_INIT, ...:
 *     Initial I/O settings.
 * ..._P():
 *     Macros to form names of I/O port registers for each bit.
 */
#define TX_400_LED        BIT0  /* 400MHZ_TX */
#define RX_400_LED        BIT1  /* 400MHZ_RX */
#define SESSION_LED       BIT2  /* SESSION */
#define UNUSED_P4_BIT3    BIT3
#define HEARTBEAT_LED     BIT4  /* HEARTBEAT */
#define MICS_RX_MODE_BIT  BIT5  /* RX_EN */
#define UNUSED_P4_BIT6    BIT6
#define UNUSED_P4_BIT7    BIT7
/**/
#define P4DIR_INIT  (BIT0 | BIT1 | BIT2 | BIT3 | BIT4 | BIT6 | BIT7)
#define P4OUT_INIT  (BIT0 | BIT1 | BIT2 | BIT4)
/**/
#define LED_P(suffix)           P4##suffix
#define MICS_RX_MODE_P(suffix)  P4##suffix

/* I/O port 5:
 * 
 * ASPI_STE_BIT, ... (schematic ADP_SPI_STE1, ...):
 *     ADP SPI interface signals. Note as of this writing, the ADP SPI
 *     interface only uses ASPI_SIMO_BIT and ASPI_CLK_BIT, so the USART function
 *     is only selected for those pins, and the MSP430 controls their direction
 *     automatically. The remaining pins are initialized as inputs and are
 *     pulled up by the level shifters on the ADP board. For more information,
 *     see "AppDevPlat/Sw/AnyBoard/Libs/AdpSpiLib.c".
 * UNUSED_P5_BIT4, ...:
 *     Unused bits (init as outputs to prevent floating & reduce power).
 * TP_P5_BIT6, ... (schematic P5.6 test point, ...):
 *     Test points (init as outputs to prevent floating & reduce power).
 * P5DIR_INIT, ...:
 *     Initial I/O settings.
 * ..._P():
 *     Macros to form names of I/O port registers for each bit.
 */
#define ASPI_STE_BIT    BIT0  /* ADP_SPI_STE1 */
#define ASPI_SIMO_BIT   BIT1  /* ADP_SPI_SIMO1 */
#define ASPI_SOMI_BIT   BIT2  /* ADP_SPI_SOMI1 */
#define ASPI_CLK_BIT    BIT3  /* ADP_SPI_CLK */
#define UNUSED_P5_BIT4  BIT4
#define UNUSED_P5_BIT5  BIT5
#define TP_P5_BIT6      BIT6  /* P5.6 test point */
#define TP_P5_BIT7      BIT7  /* P5.7 test point */
/**/
#define P5DIR_INIT  (BIT4 | BIT5 | BIT6 | BIT7)
#define P5OUT_INIT  (0)
/**/
#define ASPI_SIMO_P(suffix)  P5##suffix
#define ASPI_CLK_P(suffix)   P5##suffix
#define TP_P5_P(suffix)      P5##suffix

/* I/O port 6:
 * 
 * TP_P6_BIT0, ... (schematic P6.0 test point, ...):
 *     Test points (init as outputs to prevent floating & reduce power).
 * UNUSED_P6_BIT1, ...:
 *     Unused bits (init as outputs to prevent floating & reduce power).
 * P6DIR_INIT, ...:
 *     Initial I/O settings.
 * ..._P():
 *     Macros to form names of I/O port registers for each bit.
 */
#define TP_P6_BIT0      BIT0  /* P6.0 test point */
#define UNUSED_P6_BIT1  BIT1
#define UNUSED_P6_BIT2  BIT2
#define UNUSED_P6_BIT3  BIT3
#define UNUSED_P6_BIT4  BIT4
#define UNUSED_P6_BIT5  BIT5
#define TP_P6_BIT6      BIT6  /* P6.6 test point */
#define UNUSED_P6_BIT7  BIT7
/**/
#define P6DIR_INIT  (BIT0 | BIT1 | BIT2 | BIT3 | BIT4 | BIT5 | BIT6 | BIT7)
#define P6OUT_INIT  (0)
/**/
#define TP_P6_P(suffix)  P6##suffix

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global Declarations
 */

/**************************************************************************
 * External Function Prototypes
 */

#endif /* ensure this file is only included once */
