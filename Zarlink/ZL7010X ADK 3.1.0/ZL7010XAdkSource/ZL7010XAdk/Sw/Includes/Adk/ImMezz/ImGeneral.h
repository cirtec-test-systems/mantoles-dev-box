/**************************************************************************
 * This include file contains various defines and structures for status and
 * control of the implant mezzanine board.
 *
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
/* ensure this file is only included once */
#ifndef Adk_ImMezz_ImGeneral_h
#define Adk_ImMezz_ImGeneral_h

#include "Adp/Build/Build.h"         /* UD8, UD16, ... */
#include "Adk/AnyMezz/MicsHw.h"      /* CAL_400_MHZ_ANT, ... */
#include "Adk/AnyMezz/MicsGeneral.h" /* MICS_IMD_TID, MICS_LINK_CONFIG, ... */
#include "Adp/AnyBoard/NvmLib.h"     /* NVM_INFO, ... */

/**************************************************************************
 * Defines and Macros
 */
 
/* The device type for the implant mezzanine board. To connect to a local
 * implant mezzanine board (see ImOpen() in "ZL7010XAdk\Sw\Pc\Libs\ImLib.c"),
 * you must first connect to the local ADP board attached to the mezzanine
 * board (see AdpOpen() in "AppDevPlat\Sw\Pc\Libs\AdpLib.c", and ADP_SPECS in
 * "AppDevPlat\Sw\Includes\Adp\Pc\AdpLib.h"). For ADP_SPECS.localMezzDevType,
 * specify IM_AIM_DEV_TYPE.
 */
#define IM_AIM_DEV_TYPE  "ZL7010X ADK Implant"

/* Defines to use for IM_HW to identify the implant mezzanine board hardware.
 * IM_HW must be defined in the build include (for a description of build
 * includes and how they're used, see "Adp/Build/Build.h"). For hardware
 * revisions that require a special build, IM_HW should be defined to one
 * of the specific revisions below (AIM100_REV_B, ...). Otherwise, IM_HW
 * can be defined to one of the model numbers below with no revision
 * information (AIM100, ...).
 * 
 * To reference only the model number contained in IM_HW, you can use
 * IM_MODEL. This masks out any revision information provided in IM_HW.
 * 
 * Note BsmGetMezzVer() returns the board's model name in its version string,
 * along with its firmware version (see ADK_VER in "Adk/AdkVer.h").
 */
#define AIM100         (100UL << 16)
#define AIM100_NAME    "AIM100"
#define AIM200         (200UL << 16)
#define AIM200_NAME    "AIM200"
#define AIM300         (300UL << 16)
#define AIM300_NAME    "AIM300"
/**/
#define AIM100_REV_B   (AIM100 | ('B' << 8) | 0)
#define AIM100_REV_C   (AIM100 | ('C' << 8) | 0)
#define AIM100_REV_D   (AIM100 | ('D' << 8) | 0)
#define AIM100_REV_D1  (AIM100 | ('D' << 8) | 1)
#define AIM100_REV_E   (AIM100 | ('E' << 8) | 0)
#define AIM200_REV_A   (AIM200 | ('A' << 8) | 0)
#define AIM300_REV_A   (AIM300 | ('A' << 8) | 0)
/**/
#define IM_MODEL       (IM_HW & 0xFFFF0000UL)
 
/* Defines for the implant operational state (IM_STAT.micsState). Note to
 * maintain backwards compatibility with existing software, the values of
 * existing states should never be changed. If a new state is added that is
 * unique to the implant, it should not use any of the values used for the base
 * station states (in "ZL7010XAdk\Sw\Includes\Adk\BsmMezz\BsmGeneral.h"). If a
 * new state is added that is also used by the base station, the state should
 * be added to both include files (for the implant and base station), and they
 * should have the same value.
 * 
 * IM_SLEEPING:
 *     The MICS chip is either sleeping or starting up (before IRQ_RADIOREADY).
 * IM_IDLE:
 *     The implant is idle (MICS chip awake).
 * IM_SENDING_EMERGENCY:
 *     Sending emergency transmissions (see ImSendEmergency()). In this state,
 *     the implant sends emergency transmissions (special 400 MHz packets) and
 *     starts a session if a 400 MHz packet is received with PID = 0. Note
 *     wakeup responses are the same as emergency transmissions.
 * IM_IN_SESSION:
 *     In session (link ready). In this state, the implant has established an
 *     RF link with the base station, and the link is ready to transmit and
 *     receive data and housekeeping messages.
 * IM_SENDING_WAKEUP_RESPONSES:
 *     Sending wakeup responses (see ImListen() and ImRadioReadyIsr()). In this
 *     state, the implant transmits wakeup responses (special 400 MHz packets)
 *     and starts a session if a 400 MHz packet is received with PID = 0. Note
 *     wakeup responses are the same as emergency transmissions.
 */
#define IM_SLEEPING                  0
#define IM_IDLE                      1
#define IM_SENDING_EMERGENCY         5
#define IM_IN_SESSION                6
#define IM_SENDING_WAKEUP_RESPONSES  8
/*
 * These are old names used for various implant states (provided to maintain
 * backwards compatibility with existing software). Note some of these are also
 * defined in the include file for the base station states ("ZL7010XAdk\Sw\
 * Includes\Adk\ImMezz\BsmGeneral.h"). Thus, they are undefined first, so if
 * both files are included, it won't cause compiler errors.
 */
#undef MICS_SLEEPING
#undef MICS_IDLE
#undef MICS_IN_SESSION
/**/
#define MICS_SLEEPING                  IM_SLEEPING
#define MICS_IDLE                      IM_IDLE
#define MICS_SENDING_EMERGENCY         IM_SENDING_EMERGENCY
#define MICS_IN_SESSION                IM_IN_SESSION
#define MICS_SENDING_WAKEUP_RESPONSES  IM_SENDING_WAKEUP_RESPONSES

/* MICS calibrations supported by the implant:
 * 
 * IM_102_CALS:
 *     Calibrations a ZL70102/103 implant can run on its ZL70102/103.
 * IM_101_CALS:
 *     Calibrations a ZL70101 implant can run on its ZL70101. Note
 *     CAL_245_GHZ_ANT_101 isn't included because it doesn't work.
 */
#define IM_102_CALS  (CAL_WAKEUP_STROBE_OSC | CAL_TX_IF_OSC | \
    CAL_FM_DETECT_AND_RX_IF | CAL_RX_ADC | CAL_400_MHZ_TX_102 | \
    CAL_400_MHZ_RX_102 | CAL_400_MHZ_ANT | CAL_24_MHZ_CRYSTAL_OSC | \
    CAL_2_MHZ_OSC_102 | CAL_LNA_FREQ_102)
/**/    
#define IM_101_CALS  (CAL_WAKEUP_STROBE_OSC | CAL_TX_IF_OSC | \
    CAL_FM_DETECT_AND_RX_IF | CAL_RX_ADC | CAL_245_GHZ_ZERO_LEV_101 | \
    CAL_400_MHZ_ANT | CAL_24_MHZ_CRYSTAL_OSC)

/* When using 400 MHz wakeup, this is the number of seconds between each search
 * for a 400 MHz wakeup signal on the implant. Note the timing between the base
 * station and implant ensures the implant won't search during the base's
 * listening window twice in a row, so if the implant misses the wakeup signal
 * on the first search, it should always detect it on the retry. Thus, the
 * longest wakeup time for the implant is 2 x IM_SEC_PER_WAKE_400_SEARCH.
 * For details about the timing between the base and implant during 400 MHz
 * wakeup, see BSM_MS_PER_WAKE_400_LISTEN in "ZL7010XAdk/Sw/BsmMezz/Apps/
 * Standard/BsmAppMics.c").
 */
#define IM_SEC_PER_WAKE_400_SEARCH  5

/**************************************************************************
 * Data Structures and Typedefs
 */
        
/* MICS configuration for ZL7010X implant.
 */
typedef struct {
    
    /* MICS configuration flags (see IM_ENAB_HK_WRITE, ...) */
    UD8 flags;
    
    /* IMD transceiver ID (implant ID; 3 bytes). Note this should never be set
     * to the wildcard value in the MICS configuration.
     */
    MICS_IMD_TID imdTid;
        
    /* Company ID. Note this should never be set to 0 (wildcard) in the MICS
     * configuration.
     */
    UD8 companyId;
    
    /* link config for normal communication (400 MHz) */
    MICS_LINK_CONFIG normalLinkConfig;
    
    /* link config for emergency communication (400 MHz) */
    MICS_LINK_CONFIG emergencyLinkConfig;
    
} IM_MICS_CONFIG;
/*
 * Defines for IM_MICS_CONFIG.flags:
 * 
 * IM_ENAB_HK_WRITE:
 *     Enable housekeeping write access. If true, the implant will allow the
 *     base station to use housekeeping messages to write to registers in the
 *     MICS chip on the implant.
 * IM_400_MHZ_WAKEUP:
 *     Use 400 MHz wakeup instead of 2.45 GHz wakeup.
 * IM_EXT_STROBE_FOR_245_GHZ_SNIFF:
 *     Use an external strobe to trigger each 2.45 GHz sniff. Otherwise, the
 *     ZL7010X is configured to use its internal wakeup strobe oscillator to
 *     trigger each 2.45 sniff, which uses more power. Note this option only
 *     applies if using 2.45 GHz wakeup. It is ignored for 400 MHz wakeup.
 */
#define IM_ENAB_HK_WRITE                 (1 << 0)
#define IM_400_MHZ_WAKEUP                (1 << 1)
#define IM_EXT_STROBE_FOR_245_GHZ_SNIFF  (1 << 2)

/* General implant status.
 */
typedef struct {
    
    /* flags (see IM_ERR_OCCURRED, IM_LINK_STAT_CHANGED, ...) */
    UD8 flags;
    
    /* current operational state (see IM_IDLE, IM_IN_SESSION, etc. above) */
    UD8 micsState;
    
} IM_STAT;
/*
 * Defines for bits in IM_STAT.flags:
 * 
 * IM_ERR_OCCURRED:
 *     This is set when the implant detects an error, and cleared when the PC
 *     retrieves the error information (see ImGetStatChanges() in "ZL7010XAdk\
 *     Sw\Pc\Libs\ImLib.c").
 * IM_LINK_STAT_CHANGED:
 *     This is set whenever the link status changes (or may have changed),
 *     and cleared when the PC gets the link status (see ImGetStatChanges()
 *     in "ZL7010XAdk\Sw\Pc\Libs\ImLib.c").
 * IM_LINK_QUAL_CHANGED:
 *     This is set whenever the link quality statistics change, and cleared
 *     when the PC gets the link quality statistics (see ImGetStatChanges()
 *     in "ZL7010XAdk\Sw\Pc\Libs\ImLib.c").
 * IM_400_MHZ_TX_ACTIVE:
 *     This is set when transmitting on 400 MHz.
 * IM_400_MHZ_RX_ACTIVE:
 *     This is set when receiving on 400 MHz.
 * IM_RESERVED_STAT_FLAG_5:
 *     Reserved for possible future use.
 * IM_DATA_STAT_CHANGED:
 *     This is set whenever the data status changes, and cleared when the
 *     PC gets the data status (see ImGetStatChanges() in "ZL7010XAdk\Sw\
 *     Pc\Libs\ImLib.c").
 */
#define IM_ERR_OCCURRED          (1 << 0)
#define IM_LINK_STAT_CHANGED     (1 << 1)
#define IM_LINK_QUAL_CHANGED     (1 << 2)
#define IM_400_MHZ_TX_ACTIVE     (1 << 3)
#define IM_400_MHZ_RX_ACTIVE     (1 << 4)  
#define IM_RESERVED_STAT_FLAG_5  (1 << 5)
#define IM_DATA_STAT_CHANGED     (1 << 6)

/* Structure used to hold changes in the implant status (see
 * ImGetStatChanges() in "ZL7010XAdk\Sw\Pc\Libs\ImLib.c").
 */
typedef struct {
    
    /* general status */
    IM_STAT genStat;
    
    /* link status */
    MICS_LINK_STAT linkStat;
    
    /* link quality */
    MICS_LINK_QUAL linkQual;
    
    /* data status */
    MICS_DATA_STAT dataStat;

} IM_STAT_CHANGES;

/* These are provided for backwards compatibility - use MICS_LINK_CONFIG,
 * MICS_LINK_STAT, and MICS_LINK_QUAL instead (defined in "ZL7010XAdk\Sw\
 * Includes\Adk\AnyMezz\MicsGeneral.h").
 */
typedef MICS_LINK_CONFIG  IM_LINK_CONFIG;
typedef MICS_LINK_STAT    IM_LINK_STAT;
typedef MICS_LINK_QUAL    IM_LINK_QUAL;

/* Structure for the header at the beginning of the nonvolatile memory (NVM)
 * on the implant.
 */
typedef struct
{
    /* info that appears at the beginning of all NVMs */
    const NVM_INFO nvmInfo;

    /* the size of this header, including the NVM info */
    const UD8 nvmHeadSize;

    /* The offset and size of the NVM section that contains the factory
     * settings for the ZL7010X radio (see the IM_MICS_FACT_NVM structure below).
     * The offset is the offset from the beginning of the NVM, and the size is 
     * the size of the MICS_FACT_NVM section.
     */
    const UD8 micsFactNvmOffset;
    const UD8 micsFactNvmSize;

} IM_NVM_HEAD;

/* Structure for the nonvolatile memory (NVM) on the implant. Note that the
 * firmware should not use this structure to access fields in the nonvolatile
 * memory, for the following reason. If the implant is updated with a new firmware
 * version and a new NVM, and the implant is later reprogrammed with old firmware
 * again, the contents of the new NVM on the implant won't match the old IM_NVM
 * structure expected by the old firmware. To ensure old firmware will work with
 * with a newer NVM, the firmware should access each section of the NVM (e.g.
 * micsFactNvm) only through global pointers that are initialized by the
 * application (e.g. MicsFactNvm).
 */
typedef struct {

    /* header for NVM on implant */
    IM_NVM_HEAD head;

    /* section containing factory settings for ZL7010X radio */
    IM_MICS_FACT_NVM micsFactNvm;

} IM_NVM;

/**************************************************************************
 * Global Declarations
 */

/**************************************************************************
 * External Function Prototypes
 */
 
#endif /* ensure this file is only included once */
