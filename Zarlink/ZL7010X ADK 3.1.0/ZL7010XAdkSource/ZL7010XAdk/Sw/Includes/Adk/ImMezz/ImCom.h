/**************************************************************************
 * This include file contains defines and structures used to communicate
 * with the implant mezzanine boards.
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
/* ensure this file is only included once */
#ifndef Adk_ImMezz_ImCom_h
#define Adk_ImMezz_ImCom_h

#include "Adp/Build/Build.h"         /* UD8, UD16, SD8, BOOL8, ... */
#include "Adp/AnyBoard/Com.h"        /* COM_LOCAL_MEZZ_ADDR, ... */ 
#include "Adk/AnyMezz/MicsGeneral.h" /* MICS_LINK_STAT, MICS_LINK_QUAL, ... */
#include "Adk/ImMezz/ImGeneral.h"    /* IM_STAT, IM_MICS_CONFIG, ... */

/**************************************************************************
 * Defines and Macros
 */
 
/* Command types for implant. The command and reply structures for each of
 * these are defined later on. Note the implant also supports the commands
 * that are common to all devices (see COM_GET_DEV_TYPE_CMD_TYPE, etc. in
 * "AppDevPlat\Sw\Includes\Adp\AnyBoard\Com.h").
 */
#define IM_GET_STAT_CMD_TYPE                  1   /* 0x01 (legacy) */
#define IM_RESERVED_CMD_TYPE_2                2   /* 0x02 (future use) */
#define IM_RESERVED_CMD_TYPE_3                3   /* 0x03 (future use) */
#define IM_GET_MICS_CONFIG_CMD_TYPE           4   /* 0x04 */ 
#define IM_SET_MICS_CONFIG_CMD_TYPE           5   /* 0x05 */
#define IM_WAKEUP_CMD_TYPE                    6   /* 0x06 */
#define IM_SLEEP_CMD_TYPE                     7   /* 0x07 */
#define IM_SEND_EMERGENCY_CMD_TYPE            8   /* 0x08 */
#define IM_RESERVED_CMD_TYPE_9                9   /* 0x09 (obsolete) */
#define IM_READ_MICS_REG_CMD_TYPE             10  /* 0x0A */
#define IM_RESERVED_CMD_TYPE_11               11  /* 0x0B (obsolete) */
#define IM_RECEIVE_MICS_CMD_TYPE              12  /* 0x0C */
#define IM_WRITE_MICS_REG_CMD_TYPE            13  /* 0x0D */
#define IM_RESERVED_CMD_TYPE_14               14  /* 0x0E (obsolete) */
#define IM_TRANSMIT_MICS_CMD_TYPE             15  /* 0x0F */
#define IM_MICS_CAL_CMD_TYPE                  16  /* 0x10 */
#define IM_MICS_ADC_CMD_TYPE                  17  /* 0x11 */
#define IM_START_TX_400_CARRIER_CMD_TYPE      18  /* 0x12 */
#define IM_INT_RSSI_CMD_TYPE                  19  /* 0x13 */
#define IM_ENAB_EXT_RSSI_CMD_TYPE             20  /* 0x14 */
#define IM_GET_LINK_STAT_CMD_TYPE             21  /* 0x15 (legacy) */
#define IM_GET_LINK_QUAL_CMD_TYPE             22  /* 0x16 (legacy) */
#define IM_ENAB_HK_WRITE_CMD_TYPE             23  /* 0x17 */
#define IM_GET_TRACE_MSG_CMD_TYPE             24  /* 0x18 */
#define IM_COPY_MICS_REGS_CMD_TYPE            25  /* 0x19 */
#define IM_GET_STAT_CHANGES_CMD_TYPE          26  /* 0x1A */
#define IM_START_DATA_TEST_CMD_TYPE           27  /* 0x1B */
#define IM_RESET_MICS_CMD_TYPE                28  /* 0x1C */
#define IM_CALC_MICS_CRC_CMD_TYPE             29  /* 0x1D */
#define IM_GET_MICS_FACT_NVM_CMD_TYPE         30  /* 0x1E */

/* Addresses for the local and remote implant mezzanine boards. The local
 * board is the board on the local side of the wireless link (i.e. the implant
 * connected to the local ADP board, which in turn is connected to the local
 * PC via USB). The remote board is the board on the other side of the
 * wireless link. These addresses are used for the source & destination
 * in communication packet headers (see COM_PACK_HDR.srcAndDest in
 * "Adp/AnyBoard/Com.h").
 */
#define IM_LOCAL_ADDR   COM_LOCAL_MEZZ_ADDR
#define IM_REMOTE_ADDR  COM_REMOTE_MEZZ_ADDR

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/******************************************************************
 * Structures for IM_GET_STAT_CMD_TYPE:
 */
typedef IM_STAT  IM_GET_STAT_REPLY;

/******************************************************************
 * Structures for IM_GET_MICS_CONFIG_CMD_TYPE:
 */
typedef IM_MICS_CONFIG  IM_GET_MICS_CONFIG_REPLY;

/******************************************************************
 * Structures for IM_SET_MICS_CONFIG_CMD_TYPE:
 */
typedef IM_MICS_CONFIG  IM_SET_MICS_CONFIG_CMD;

/******************************************************************
 * Structures for IM_WAKEUP_CMD_TYPE:
 */
typedef struct {
    
    /* If true, the MICS chip will be kept awake. Otherwise, it will be put
     * back to sleep about 4.37 seconds after it is awakened, or when the sleep
     * command is issued (IM_SLEEP_CMD_TYPE).
     */
    BOOL8 stayAwake;
    
} IM_WAKEUP_CMD;
 
/******************************************************************
 * Structures for IM_READ_MICS_REG_CMD_TYPE:
 */
typedef struct {
    
    /* address of register to read in MICS chip */
    UD16 regAddr;
    
} IM_READ_MICS_REG_CMD;

typedef struct {
    
    /* value of register read from MICS chip */
    UD8 regVal;

} IM_READ_MICS_REG_REPLY;

/******************************************************************
 * Structures for IM_RECEIVE_MICS_CMD_TYPE:
 */
typedef struct {
    
    /* maximum length to read from MICS RX buf */
    UD16 maxLen;

} IM_RECEIVE_MICS_CMD;

/******************************************************************
 * Structures for IM_WRITE_MICS_REG_CMD_TYPE:
 */
typedef struct {
    
    /* address of register to write to in MICS chip */
    UD16 regAddr;
    
    /* value to write to register */
    UD8 regVal;
    
} IM_WRITE_MICS_REG_CMD;

/******************************************************************
 * Structures for IM_MICS_CAL_CMD_TYPE:
 */
typedef struct {
    
    /* The group 1 calibrations to perform. Each bit corresponds to a bit in
     * the CALSELECT1 register on the MICS chip (defined in "ZL7010XAdk\Sw\
     * Includes\Adk\AnyMezz\MicsHw.h").
     */
    UD8 calSelect1;
    
    /* The channel to use for the CAL_400_MHZ_ANT calibration on the local
     * MICS chip. Note this is ignored for all other calibrations.
     */
    SD8 chan;
    
    /* The group 2 calibrations to perform. Each bit corresponds to a bit in
     * the CALSELECT2 register on the ZL70102/103 (defined in "ZL7010XAdk\Sw\
     * Includes\Adk\AnyMezz\MicsHw.h"). The has no meaning for a ZL70101.
     */
    UD8 calSelect2;
    
} IM_MICS_CAL_CMD;

/******************************************************************
 * Structures for IM_MICS_ADC_CMD_TYPE:
 */
typedef struct {
    
    /* The ADC input to use on the MICS chip (see ADC_INPUT_TEST_IO_1,
     * ADC_INPUT_VSUP, etc. in "Adk/AnyMezz/MicsHw.h").
     */
    UD8 adcInput;
    
} IM_MICS_ADC_CMD;

typedef struct {
    
    /* result of A/D conversion */
    UD8 adcResult;

} IM_MICS_ADC_REPLY;

/******************************************************************
 * Structures for IM_START_TX_400_CARRIER_CMD_TYPE:
 */
typedef struct {
    
    /* start if true, stop if false */
    BOOL8 start;
    
    /* MICS channel to transmit 400 MHz TX carrier for */
    UD8 chan;
    
} IM_START_TX_400_CARRIER_CMD;

/******************************************************************
 * Structures for IM_INT_RSSI_CMD_TYPE:
 */
typedef struct {
    
    /* MICS channel to do internal RSSI for */
    UD8 chan;
    
} IM_INT_RSSI_CMD;

typedef struct {
    
    /* result of internal RSSI */
    UD8 rssi;

} IM_INT_RSSI_REPLY;

/******************************************************************
 * Structures for IM_GET_LINK_STAT_CMD_TYPE:
 */
typedef MICS_LINK_STAT  IM_GET_LINK_STAT_REPLY;

/******************************************************************
 * Structures for IM_GET_LINK_QUAL_CMD_TYPE:
 */
typedef MICS_LINK_QUAL  IM_GET_LINK_QUAL_REPLY;

/******************************************************************
 * Structures for IM_ENAB_HK_WRITE_CMD_TYPE:
 */
typedef struct {
    
    /* enable if true, disable if false */
    BOOL8 enab;
    
} IM_ENAB_HK_WRITE_CMD;

/******************************************************************
 * Structures for IM_ENAB_EXT_RSSI_CMD_TYPE:
 */
typedef struct {
    
    /* enable if true, disable if false */
    BOOL8 enab;
    
    /* MICS channel to use for external RSSI */
    UD8 chan;
    
} IM_ENAB_EXT_RSSI_CMD;

/******************************************************************
 * Structures for IM_GET_STAT_CHANGES_CMD_TYPE:
 */
typedef struct {
    
    /* if true, also get sections of the status that haven't changed */
    BOOL8 force;
    
} IM_GET_STAT_CHANGES_CMD;

/******************************************************************
 * Structures for IM_START_DATA_TEST_CMD_TYPE
 */
typedef MICS_DT_SPECS  IM_START_DATA_TEST_CMD;

/******************************************************************
 * Structures for IM_READ_NVM_CMD_TYPE:
 */
typedef IM_MICS_FACT_NVM IM_GET_MICS_FACT_NVM_REPLY;


/**************************************************************************
 * Global Declarations
 */

/**************************************************************************
 * External Function Prototypes
 */
 
#endif /* ensure this file is only included once */
