/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adk_ImMezz_ImAppErrs_h
#define Adk_ImMezz_ImAppErrs_h

/**************************************************************************
 * Defines and Macros
 */
 
/* Error group and error codes for the application software on the implant.
 * Note for the error group, the firmware should always reference the global
 * variable ImAppErr instead of IM_APP_ERR, so all of the references will share
 * the same constant global string.
 * 
 * The error ID strings (EID) include a detailed message for each error. These
 * are provided for use on the host (PC). For the format and use of error ID
 * strings, see "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c". The firmware doesn't use
 * the error ID strings because they would require too much memory (the
 * firmware only uses error groups and error codes).
 */
extern const char *ImAppErr;
#define IM_APP_ERR "ImAppErr"
/**/
#define IM_APP_ERR_MICS_DATA_NOT_YET_SUPPORTED                               1 
#define IM_APP_EID_MICS_DATA_NOT_YET_SUPPORTED \
    "ImAppErr.1: The implant does not yet support ADK command/reply " \
    "packet communication via ZL7010X session data."
#define IM_APP_ERR_RECEIVED_CMD_WITH_INVALID_CMD_TYPE                        2 
#define IM_APP_EID_RECEIVED_CMD_WITH_INVALID_CMD_TYPE \
    "ImAppErr.2: The implant received an ADK command packet with an " \
    "invalid command type ({0})."
#define IM_APP_ERR_RECEIVED_CMD_WITH_INVALID_LEN                             3
#define IM_APP_EID_RECEIVED_CMD_WITH_INVALID_LEN \
    "ImAppErr.3: The implant received an ADK command packet with an " \
    "invalid length."
#define IM_APP_ERR_MICS_DATA_RX_TIMEOUT                                      4
#define IM_APP_EID_MICS_DATA_RX_TIMEOUT \
    "ImAppErr.4: The implant timed out waiting for the ZL7010X to " \
    "receive session data."
#define IM_APP_ERR_MICS_DATA_TX_TIMEOUT                                      5
#define IM_APP_EID_MICS_DATA_TX_TIMEOUT \
    "ImAppErr.5: The implant timed out waiting for the ZL7010X to " \
    "transmit session data."
#define IM_APP_ERR_MICS_HK_RX_TIMEOUT                                        6
#define IM_APP_EID_MICS_HK_RX_TIMEOUT \
    "ImAppErr.6: The implant timed out waiting to receive an ADK " \
    "communication packet via the ZL7010X housekeeping interface."
#define IM_APP_ERR_MICS_HK_TX_TIMEOUT                                        7
#define IM_APP_EID_MICS_HK_TX_TIMEOUT \
    "ImAppErr.7: The implant timed out waiting to transmit an ADK " \
    "communication packet via the ZL7010X housekeeping interface."
#define IM_APP_ERR_ADP_SPI_RX_TIMEOUT                                        8
#define IM_APP_EID_ADP_SPI_RX_TIMEOUT \
    "ImAppErr.8: The implant timed out waiting to receive an ADK " \
    "communication packet from the ADP board via the ADP SPI interface."
#define IM_APP_ERR_ADP_SPI_TX_TIMEOUT                                        9
#define IM_APP_EID_ADP_SPI_TX_TIMEOUT \
    "IAppErr.9: The implant timed out waiting to transmit an ADK " \
    "communication packet to the ADP board via the ADP SPI interface."
#define IM_APP_ERR_RECEIVED_PACK_WITH_DEST_0                                 10
#define IM_APP_EID_RECEIVED_PACK_WITH_DEST_0 \
    "ImAppErr.10: The implant received an ADK communication packet with " \
    "destination address 0."
#define IM_APP_ERR_MICS_INT_WITH_NO_STAT                                     11
#define IM_APP_EID_MICS_INT_WITH_NO_STAT \
    "ImAppErr.11: The ZL7010X on the implant asserted an interrupt, but " \
    "its reg_irq_status1 and 2 were both 0."
#define IM_APP_ERR_MICS_CRC_ERR                                              12
#define IM_APP_EID_MICS_CRC_ERR \
    "ImAppErr.12: The ZL7010X on the implant asserted the irq_crcerr " \
    "interrupt."
#define IM_APP_ERR_INVALID_TX_LEN                                            13
#define IM_APP_EID_INVALID_TX_LEN \
    "ImAppErr.13: The specified transmit length ({0}) was not a " \
    "multiple of the TX block size ({1})."
#define IM_APP_ERR_14                                                        14
#define IM_APP_EID_14 \
    "ImAppErr.14: Old error code (should no longer occur)."
#define IM_APP_ERR_MICS_SYNTH_FAILED_TO_LOCK                                 15
#define IM_APP_EID_MICS_SYNTH_FAILED_TO_LOCK \
    "ImAppErr.15: The synthesizer on the implant's ZL7010X " \
    "failed to lock."
#define IM_APP_ERR_MICS_WAKEUP_TIMEOUT                                       16
#define IM_APP_EID_MICS_WAKEUP_TIMEOUT \
    "ImAppErr.16: The implant timed out waiting for its ZL7010X chip " \
    "to wake up."
#define IM_APP_ERR_WRONG_MICS_REV                                            17
#define IM_APP_EID_WRONG_MICS_REV \
    "ImAppErr.17: The revision read from the ZL7010X on the implant " \
    "({0}) was wrong. Make sure the implant has the correct firmware."

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global Declarations
 */

/**************************************************************************
 * External Function Prototypes
 */
 
#endif /* ensure this file is only included once */
