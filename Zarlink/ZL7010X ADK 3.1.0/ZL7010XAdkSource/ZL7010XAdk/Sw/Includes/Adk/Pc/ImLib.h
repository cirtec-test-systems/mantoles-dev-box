/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
/* ensure this file is only included once */
#ifndef Adk_Pc_ImLib_h
#define Adk_Pc_ImLib_h

#include "Adp/General.h"             /* RSTAT, BOOL, ... */
#include "Adp/Build/Build.h"         /* UD32, EXTC, ... */
#include "Adp/Pc/ErrInfoLib.h"       /* EI, EiSet() */
#include "Adp/Pc/AdpLib.h"           /* ADP_ID */
#include "Adp/AnyBoard/Com.h"        /* COM_DEV_VER_BUF_SIZE, ... */
#include "Adk/AnyMezz/MicsGeneral.h" /* MICS_LINK_STAT, MICS_DT_SPECS, ... */
#include "Adk/ImMezz/ImGeneral.h"    /* IM_MICS_CONFIG, IM_AIM_DEV_TYPE, ... */
#include "Adp/AnyBoard/NvmLib.h"     /* NVM_INIT_KET, NVM_INFO, ... */

/**************************************************************************
 * Defines and Macros
 */

 /* macro used to get the offset of fields in the IM_NVM structure */
#define IM_NVM_OFFSET(field)  ((UD16)(&(((IM_NVM *)0)->field)))

 /* Values for the return status for ImCheckNvm().
 */
typedef enum
{
    IM_NVM_IS_VALID = 0,
    IM_NVM_IS_OLDER_VER = -1,
    IM_NVM_OTHER_ERR = -2

} IM_NVM_RSTAT;

/* Error ID strings for the implant library. For the format and use of error
 * ID strings, see "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c".
 */
#define IM_LIB_EID_INVALID_SPECS \
    "ImLibErr.InvalidSpecs: Invalid IM_SPECS structure (it appears it " \
    "was not initialized by calling ImInitSpecs(), or is too big)."
#define IM_LIB_EID_FAILED_TO_ALLOCATE_MEM \
    "ImLibErr.FaliedToAllocateMem: Failed to allocate {0} bytes of memory."
#define IM_LIB_EID_NO_LOCAL_ADP_ID \
    "ImLibErr.NoLocalAdpId: No \"localAdpId\" was specified in the " \
    "IM_SPECS passed to ImOpen()."
#define IM_LIB_EID_INCOMPATIBLE_BOARD_MODEL \
    "ImLibErr.IncompatibleBoardModel: The implant mezzanine board version " \
    "({0}) does not contain a board model supported by the implant library."
#define IM_LIB_EID_INCOMPATIBLE_IM_MEZZ_VER \
    "ImLibErr.IncompatibleImMezzVer: The implant mezzanine board version " \
    "({0}) is not compatible with the version required by the implant " \
    "library ({1})."
#define IM_LIB_EID_INVALID_STRUCT_SIZE \
    "ImLibErr.InvalidStructSize: Specified structure was the wrong size."
#define IM_LIB_EID_INVALID_IM_ID \
    "ImLibErr.InvalidImId: Specified IM_ID was invalid (it may have been " \
    "closed - make sure nothing tries to use a IM_ID after it is closed)."
#define IM_LIB_EID_UNSUPPORTED_MICS_CAL \
    "ImLibErr.UnsupportedMicsCal: Specified unsupported calibration(s) " \
    "for the ZL7010X on the implant (reg_mac_calselect1 = {0})."
#define IM_LIB_EID_INVALID_RX_MOD \
    "ImLibErr.InvalidRxMod: Specified invalid RX modulation ({0})."
#define IM_LIB_EID_ATTEMPT_TO_SELECT_PAGE_2 \
    "ImLibErr.AttemptToSelectPage2: An attempt was made to select " \
    "register page 2 in the ZL7010X. To access a page 2 register, " \
    "specify MICS_PAGE_2 (h'80) in the register address (the ADK " \
    "software manages the page selection automatically)."
#define IM_LIB_EID_ATTEMPT_TO_CHANGE_HK_PAGE \
    "ImLibErr.AttemptToChangeHkPage: An attempt was made to change the " \
    "register page for housekeeping accesses by the base. This isn't safe " \
    "because it might not match the page expected by the base. To access a " \
    "page 2 register via housekeeping, specify MICS_PAGE_2 (h'80) in the " \
    "register address (the ADK software on the base manages the housekeeping " \
    "page selection on the implant automatically)."
#define IM_LIB_EID_MICS_REV_DOES_NOT_SUPPORT_REQUESTED_OP \
    "ImLibErr.MicsRevDoesNotSupportRequestedOp: The revision of the MICS " \
    "chip that is being used doesn't support the requested operation."
#define IM_LIB_EID_INVALID_STRUCT_SIZE \
    "ImLibErr.InvalidStructSize: Specified structure was the wrong size."
#define IM_LIB_EID_NVM_IS_NOT_INIT \
    "ImLibErr.NvmIsNotInit: The nonvolatile memory is not initialized."
#define IM_LIB_EID_NVM_IS_WRONG_TYPE \
    "ImLibErr.vmIsWrongType: The nonvolatile memory is the wrong type."
#define IM_LIB_EID_NVM_IS_NEWER_VER \
    "ImLibErr.NvmIsNewerVer: The nonvolatile memory appears to be a " \
    "newer (larger) version that this version of the host (PC) software " \
    "does not support."
#define IM_LIB_EID_NVM_IS_OLDER_VER \
    "ImLibErr.NvmIsOlderVer: The nonvolatile memory appears to be an " \
    "older (smaller) version."
#define IM_LIB_EID_NVM_HEAD_IS_INVALID \
    "ImLibErr.NvmHeadIsInvalid: The header in the nonvolatile memory " \
    "is invalid."

/* Size of buffer required to hold a maximum length version string for
 * implant mezzanine board (and its terminating '\0'). The buffer passed to
 * ImGetMezzVer() should be this size or larger to ensure it can hold the
 * full version string.
 */
#define IM_MEZZ_VER_BUF_SIZE  COM_DEV_VER_BUF_SIZE
#define IM_MEZZ_VER_MAX_LEN   COM_DEV_VER_MAX_LEN

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* type for ID returned by ImOpen() */
typedef const void * IM_ID;
 
/* Structure for specifications passed to ImOpen(). Note before passing
 * this to ImOpen(), you must call ImInitSpecs() to initialize it with
 * default values, then override any settings for which you do not want
 * the default value.
 */
typedef struct {

    /* Internal magic number set by ImInitSpecs() (used to verify the specs
     * were initialized by calling ImInitSpecs()).
     */
    UD32 magic;
    
    /* This must specify a connection to the ADP board for either an implant
     * or a base station. For how this connection is used, see IM_SPECS.remote.
     * To open a connection to the ADP board for an implant, invoke AdpOpen()
     * with ADP_SPECS.localMezzDevType set to IM_AIM_DEV_TYPE (defined in
     * "ZL7010XAdk\Sw\Includes\Adk\ImMezz\ImGeneral.h"), and to open a
     * connection to the ADP board for a base station, invoke AdpOpen() with
     * ADP_SPECS.localMezzDevType set to BSM_DEV_TYPE (defined in "ZL7010XAdk\
     * Sw\Includes\Adk\BsmMezz\BsmGeneral.h"). AdpOpen() is defined in
     * "AppDevPlat\Sw\Pc\Libs\AdpLib.c", and ADP_SPECS is defined in
     * "AppDevPlat\Sw\Includes\Adp\Pc\AdpLib.h".
     *
     * Note ImInitSpecs() sets this to NULL by default, so the calling
     * application must override it (otherwise, ImOpen() will fail).
     */
    ADP_ID localAdpId;
    
    /* If "remote" is false, IM_SPECS.localAdpId must specify a connection
     * to the ADP board for an implant, which will then be used to communicate
     * with the implant (the PC will send commands to the ADP board via USB,
     * which will forward them to the implant via SPI). For instructions on
     * how to open a connection to the ADP board for an implant, see
     * IM_SPECS.localAdpId.
     *
     * If "remote" is true, it's ignored (not yet supported). For future
     * reference, if "remote" is true, ImOpen() will connect to an implant
     * via a wireless link. In this case, IM_SPECS.localAdpId must specify a
     * connection to the ADP board for a base station, which will then be used
     * to communicate with the implant (the PC will send commands to the base
     * station's ADP board via USB, which will forward them to the base station
     * via SPI, which will forward them to the implant via the wireless link).
     * For instructions on how to open a connection to the ADP board for a base
     * station, see IM_SPECS.localAdpId. Note before attempting to communicate
     * with the implant, you must establish the wireless link (otherwise, it
     * will just time out).
     *
     * ImInitSpecs() sets this to false by default.
     */
    BOOL remote;

} IM_SPECS;

/**************************************************************************
 * Global Declarations
 */
 
/**************************************************************************
 * External Function Prototypes
 */
 
EXTC void ImInitSpecs(IM_SPECS *specs, UD32 specsSize);
 
EXTC IM_ID ImOpen(const IM_SPECS *specs, UD32 specsSize, EI e);

EXTC RSTAT ImGetMezzVer(IM_ID imId, char *buf, UD32 bufSize, EI e);

/* kept for backwards compatibility - use ImGetStatChanges() instead */
EXTC RSTAT ImGetStat(IM_ID imId, IM_STAT *genStat, UD32 genStatSize, EI e);

EXTC RSTAT ImGetMicsConfig(IM_ID imId, IM_MICS_CONFIG *mc,
    UD32 mcSize, EI e);
    
EXTC RSTAT ImSetMicsConfig(IM_ID imId, const IM_MICS_CONFIG *mc,
    UD32 mcSize, EI e);
    
EXTC RSTAT ImWakeup(IM_ID imId, BOOL stayAwake, EI e);

EXTC RSTAT ImSleep(IM_ID imId, EI e);

/* kept for backwards compatibility - use ImSleep() instead (same) */
EXTC RSTAT ImAbortMics(IM_ID imId, EI e);
    
EXTC RSTAT ImSendEmergency(IM_ID imId, EI e);
    
EXTC SD32 ImReadMicsReg(IM_ID imId, UD32 regAddr, EI e);

/* kept for backwards compatibility - can now use ImReadMicsReg() instead */
EXTC SD32 ImReadMicsTestReg(IM_ID imId, UD32 testRegAddr, EI e);

EXTC RSTAT ImWriteMicsReg(IM_ID imId, UD32 regAddr, UD32 regVal, EI e);

/* kept for backwards compatibility - can now use ImWriteMicsReg() instead */
EXTC RSTAT ImWriteMicsTestReg(IM_ID imId, UD32 testRegAddr, UD32 testRegVal, EI e);

EXTC SD32 ImReceiveMics(IM_ID imId, void *buf, UD32 bufSize, EI e);

EXTC RSTAT ImTransmitMics(IM_ID imId, const void *data, UD32 nBytes, EI e);
    
EXTC RSTAT ImMicsCal(IM_ID imId, UD32 cals, SD32 chan, EI e);

EXTC SD32 ImMicsAdc(IM_ID imId, UD32 adcInput, EI e);

EXTC RSTAT ImStartTx400Carrier(IM_ID imId, BOOL start, UD32 chan, EI e);

EXTC SD32 ImIntRssi(IM_ID imId, UD32 chan, EI e);

EXTC RSTAT ImEnabExtRssi(IM_ID imId, BOOL enab, UD32 chan, EI e);

/* kept for backwards compatibility - use ImGetStatChanges() instead */
EXTC RSTAT ImGetLinkStat(IM_ID imId, MICS_LINK_STAT *ls, UD32 lsSize, EI e);

/* kept for backwards compatibility - use ImGetStatChanges() instead */
EXTC RSTAT ImGetLinkQual(IM_ID imId, MICS_LINK_QUAL *lq, UD32 lqSize, EI e);

/* kept for backwards compatibility - use ImGetStatChanges() instead */
EXTC RSTAT ImGetLinkIntegrity(IM_ID imId, MICS_LINK_QUAL *lq, UD32 lqSize, EI e);
    
EXTC RSTAT ImEnabHkWrite(IM_ID imId, BOOL enab, EI e);

EXTC RSTAT ImStartBerRx(IM_ID imId, BOOL start, UD32 chan, UD32 rxMod, EI e);

EXTC RSTAT ImGetTraceMsg(IM_ID imId, char *buf, UD32 bufSize, EI e);

EXTC RSTAT ImCopyMicsRegs(IM_ID imId, EI e);

EXTC RSTAT ImGetStatChanges(IM_ID imId, BOOL32 force, IM_STAT_CHANGES *sc,
    UD32 scSize, EI e);
    
EXTC RSTAT ImStartDataTest(IM_ID imId, const MICS_DT_SPECS *specs,
    UD32 specsSize, EI e);

EXTC RSTAT ImResetMics(IM_ID imId, EI e);

EXTC RSTAT ImCalcMicsCrc(IM_ID imId, EI e);

EXTC RSTAT ImClose(IM_ID imId, EI e);

EXTC IM_NVM_RSTAT ImCheckNvm(void *nvm, EI e);

EXTC RSTAT ImTransferNvm(void *prevNvm, IM_NVM *nvm, UD32 nvmSize, EI e);

EXTC RSTAT ImGetMicsFactNvm(IM_ID imId, IM_MICS_FACT_NVM *fn, UD32 fnSize, EI e);

#endif /* ensure this file is only included once */
