/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adk_Pc_AdkLib_h
#define Adk_Pc_AdkLib_h

#include "Adp/Build/Build.h" /* EXTC, ... */

/**************************************************************************
 * Defines and Macros
 */

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global Declarations
 */
 
/**************************************************************************
 * External Function Prototypes
 */
 
EXTC const char *AdkGetApiVer(void);
EXTC BOOL AdkIsStackReg(UD32 regAddr, UD32 micsRev);
EXTC BOOL AdkIsCrcReg(UD32 regAddr, UD32 micsRev);
 
#endif /* ensure this file is only included once */
