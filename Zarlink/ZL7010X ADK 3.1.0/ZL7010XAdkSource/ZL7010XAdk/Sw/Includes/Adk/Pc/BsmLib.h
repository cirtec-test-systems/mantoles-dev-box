/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
/* ensure this file is only included once */
#ifndef Adk_Pc_BsmLib_h
#define Adk_Pc_BsmLib_h

#include "Adp/General.h"             /* RSTAT, BOOL, ... */
#include "Adp/Build/Build.h"         /* UD32, SD32, EXTC, ... */
#include "Adp/Pc/ErrInfoLib.h"       /* EI, EiSet() */
#include "Adp/Pc/AdpLib.h"           /* ADP_ID */
#include "Adp/AnyBoard/Com.h"        /* COM_DEV_VER_BUF_SIZE, ... */
#include "Adk/AnyMezz/MicsGeneral.h" /* MICS_LINK_STAT, MICS_DT_SPECS, ... */
#include "Adk/BsmMezz/BsmGeneral.h"  /* BSM_MICS_CONFIG, BSM_DEV_TYPE, ... */
#include "Adp/AnyBoard/NvmLib.h"     /* NVM_INIT_KET, NVM_INFO, ... */

/**************************************************************************
 * Defines and Macros
 */

 /* macro used to get the offset of fields in the BSM_NVM structure */
#define BSM_NVM_OFFSET(field)  ((UD16)(&(((BSM_NVM *)0)->field)))

 /* Values for the return status for BsmCheckNvm().
 */
typedef enum
{
    BSM_NVM_IS_VALID = 0,
    BSM_NVM_IS_OLDER_VER = -1,
    BSM_NVM_OTHER_ERR = -2

} BSM_NVM_RSTAT;

/* Error ID strings for the base station library. For the format and use of
 * error ID strings, see "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c".
 */
#define BSM_LIB_EID_INVALID_SPECS \
    "BsmLibErr.InvalidSpecs: Invalid BSM_SPECS structure (it appears it " \
    "was not initialized by calling BsmInitSpecs(), or is too big)."
#define BSM_LIB_EID_FAILED_TO_ALLOCATE_MEM \
    "BsmLibErr.FaliedToAllocateMem: Failed to allocate {0} bytes of memory."
#define BSM_LIB_EID_NO_LOCAL_ADP_ID \
    "BsmLibErr.NoLocalAdpId: No \"localAdpId\" was specified in the " \
    "BSM_SPECS passed to BsmOpen()."
#define BSM_LIB_EID_INCOMPATIBLE_BOARD_MODEL \
    "BsmLibErr.IncompatibleBoardModel: The base station mezzanine board " \
    "version ({0}) does not contain a board model supported by the base " \
    "station library."
#define BSM_LIB_EID_INCOMPATIBLE_BSM_MEZZ_VER \
    "BsmLibErr.IncompatibleBsmMezzVer: The base station mezzanine board " \
    "version ({0}) is not compatible with the version required by the base " \
    "station library ({1})."
#define BSM_LIB_EID_INVALID_STRUCT_SIZE \
    "BsmLibErr.InvalidStructSize: Specified structure was the wrong size."
#define BSM_LIB_EID_INVALID_BSM_ID \
    "BsmLibErr.InvalidBsmId: Specified BSM_ID was invalid (it may have been " \
    "closed - make sure nothing tries to use a BSM_ID after it is closed)."
#define BSM_LIB_EID_INVALID_245_GHZ_TX_POWER \
    "BsmLibErr.Invalid245GHzTxPower: Specified invalid 2.45 GHz TX power " \
    "level = {0} dBm (out of supported range)."
#define BSM_LIB_EID_UNSUPPORTED_LOCAL_MICS_CAL \
    "BsmLibErr.UnsupportedLocalMicsCal: Specified unsupported " \
    "calibration(s) for the ZL7010X on the base station " \
    "(reg_mac_calselect1 = {0})."
#define BSM_LIB_EID_UNSUPPORTED_REMOTE_MICS_CAL \
    "BsmLibErr.UnsupportedRemoteMicsCal: Specified unsupported " \
    "calibration(s) for the ZL7010X on the remote implant " \
    "(reg_mac_calselect1 = {0})."
#define BSM_LIB_EID_INVALID_RX_MOD \
    "BsmLibErr.InvalidRxMod: Specified invalid RX modulation ({0})."
#define BSM_LIB_EID_ATTEMPT_TO_SELECT_PAGE_2 \
    "BsmLibErr.AttemptToSelectPage2: An attempt was made to select " \
    "register page 2 in the ZL7010X. To access a page 2 register, " \
    "specify MICS_PAGE_2 (h'80) in the register address (the ADK " \
    "software manages the page selection automatically)."
#define BSM_LIB_EID_ATTEMPT_TO_SELECT_HK_PAGE_2 \
    "BsmLibErr.AttemptToSelectHkPage2: An attempt was made to select " \
    "register page 2 for housekeeping access. To access a page 2 register, " \
    "specify MICS_PAGE_2 (h'80) in the register address (the ADK " \
    "software manages the page selection automatically). Note the software " \
    "allows the base to use housekeeping to access page 2 registers in a " \
    "remote implant, but not the other way around." 
#define BSM_LIB_EID_INVALID_STRUCT_SIZE \
    "BsmLibErr.InvalidStructSize: Specified structure was the wrong size."
#define BSM_LIB_EID_NVM_IS_NOT_INIT \
    "BsmLibErr.NvmIsNotInit: The nonvolatile memory is not initialized."
#define BSM_LIB_EID_NVM_IS_WRONG_TYPE \
    "BsmLibErr.vmIsWrongType: The nonvolatile memory is the wrong type."
#define BSM_LIB_EID_NVM_IS_NEWER_VER \
    "BsmLibErr.NvmIsNewerVer: The nonvolatile memory appears to be a " \
    "newer (larger) version that this version of the host (PC) software " \
    "does not support."
#define BSM_LIB_EID_NVM_IS_OLDER_VER \
    "BsmLibErr.NvmIsOlderVer: The nonvolatile memory appears to be an " \
    "older (smaller) version."
#define BSM_LIB_EID_NVM_HEAD_IS_INVALID \
    "BsmLibErr.NvmHeadIsInvalid: The header in the nonvolatile memory " \
    "is invalid."

/* Size of buffer required to hold a maximum length version string for base
 * station mezzanine board (and its terminating '\0'). The buffer passed to
 * BsmGetMezzVer() should be this size or larger to ensure it can hold the
 * full version string.
 */
#define BSM_MEZZ_VER_BUF_SIZE  COM_DEV_VER_BUF_SIZE
#define BSM_MEZZ_VER_MAX_LEN   COM_DEV_VER_MAX_LEN

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* type for ID returned by BsmOpen() */
typedef const void * BSM_ID;
 
/* Structure for specifications passed to BsmOpen(). Note before passing
 * this to BsmOpen(), you must call BsmInitSpecs() to initialize it with
 * default values, then override any settings for which you do not want
 * the default value.
 */
typedef struct {

    /* Internal magic number set by BsmInitSpecs() (used to verify the specs
     * were initialized by calling BsmInitSpecs()).
     */
    UD32 magic;
    
    /* This must specify a connection to the ADP board for a base station,
     * which will then be used to communicate with the base station (the PC
     * will send commands to the ADP board via USB, which will forward them
     * to the base station via SPI). To open this connection, invoke AdpOpen()
     * with ADP_SPECS.localMezzDevType set to BSM_DEV_TYPE (defined in
     * "ZL7010XAdk\Sw\Includes\Adk\BsmMezz\BsmGeneral.h"). AdpOpen() is
     * defined in "AppDevPlat\Sw\Pc\Libs\AdpLib.c", and ADP_SPECS is defined
     * in "AppDevPlat\Sw\Includes\Adp\Pc\AdpLib.h".
     *
     * Note BsmInitSpecs() sets this to NULL by default, so the calling
     * application must override it (otherwise, BsmOpen() will fail).
     */
    ADP_ID localAdpId;

} BSM_SPECS;

/**************************************************************************
 * Global Declarations
 */
 
/**************************************************************************
 * External Function Prototypes
 */
 
EXTC void BsmInitSpecs(BSM_SPECS *specs, UD32 specsSize);
 
EXTC BSM_ID BsmOpen(const BSM_SPECS *specs, UD32 specsSize, EI e);

EXTC RSTAT BsmGetMezzVer(BSM_ID bsmId, char *buf, UD32 bufSize, EI e);

/* kept for backwards compatibility - use BsmGetStatChanges() instead */
EXTC RSTAT BsmGetStat(BSM_ID bsmId, BSM_STAT *genStat, UD32 genStatSize, EI e);

EXTC RSTAT BsmGetMicsConfig(BSM_ID bsmId, BSM_MICS_CONFIG *mc,
    UD32 mcSize, EI e);

EXTC RSTAT BsmSetMicsConfig(BSM_ID bsmId, const BSM_MICS_CONFIG *mc,
    UD32 mcSize, EI e);
    
EXTC RSTAT BsmStartSession(BSM_ID bsmId, BOOL enabWatchdog,
    UD32 timeoutSec, EI e);

EXTC RSTAT BsmSearchForImplant(BSM_ID bsmId, BOOL enabWatchdog,
    BOOL anyCompany, BOOL anyImplant, EI e);

EXTC RSTAT BsmListenForEmergency(BSM_ID bsmId, BOOL startSession, EI e);
    
EXTC RSTAT BsmAbortMics(BSM_ID bsmId, EI e);
    
EXTC SD32 BsmReadMicsReg(BSM_ID bsmId, UD32 regAddr, BOOL remote, EI e);

/* kept for backwards compatibility - can now use BsmReadMicsReg() instead */
EXTC SD32 BsmReadMicsTestReg(BSM_ID bsmId, UD32 testRegAddr, EI e);

EXTC RSTAT BsmWriteMicsReg(BSM_ID bsmId, UD32 regAddr, UD32 regVal,
    BOOL remote, EI e);

/* kept for backwards compatibility - can now use BsmWriteMicsReg() instead */
EXTC RSTAT BsmWriteMicsTestReg(BSM_ID bsmId, UD32 testRegAddr,
    UD32 testRegVal, EI e);
    
EXTC SD32 BsmReceiveMics(BSM_ID bsmId, void *buf, UD32 bufSize, EI e);

EXTC RSTAT BsmTransmitMics(BSM_ID bsmId, const void *data, UD32 nBytes, EI e);
    
EXTC RSTAT BsmMicsCal(BSM_ID bsmId, UD32 cals, SD32 chan, BOOL remote, EI e);

EXTC SD32 BsmMicsAdc(BSM_ID bsmId, UD32 adcInput, BOOL remote, EI e);

EXTC RSTAT BsmSetTx245Freq(BSM_ID bsmId, UD32 ccFreq, EI e);

EXTC RSTAT BsmSetTx245Power(BSM_ID bsmId, UINT ccPa, EI e);

EXTC RSTAT BsmStartTx245Carrier(BSM_ID bsmId, BOOL start, EI e);

EXTC RSTAT BsmStartTx400Carrier(BSM_ID bsmId, BOOL start, UD32 chan, EI e);

EXTC SD32 BsmRssi(BSM_ID bsmId, BOOL useIntRssi, UD32 chan, BOOL average, EI e);

EXTC RSTAT BsmEnabExtRssi(BSM_ID bsmId, BOOL enab, UD32 chan, EI e);

EXTC SD32 BsmCca(BSM_ID bsmId, BOOL average, BSM_CCA_DATA *data, UD32 dataSize, EI e);
    
/* kept for backwards compatibility - use BsmGetStatChanges() instead */
EXTC RSTAT BsmGetLinkStat(BSM_ID bsmId, MICS_LINK_STAT *ls, UD32 lsSize, EI e);

/* kept for backwards compatibility - use BsmGetStatChanges() instead */
EXTC RSTAT BsmGetLinkQual(BSM_ID bsmId, MICS_LINK_QUAL *lq, UD32 lqSize, EI e);

/* kept for backwards compatibility - use BsmGetStatChanges() instead */
EXTC RSTAT BsmGetLinkIntegrity(BSM_ID bsmId, MICS_LINK_QUAL *lq, UD32 lqSize, EI e);

EXTC RSTAT BsmStartBerRx(BSM_ID bsmId, BOOL start, UD32 chan, UD32 rxMod, EI e);

EXTC RSTAT BsmGetTraceMsg(BSM_ID bsmId, char *buf, UD32 bufSize, EI e);

EXTC RSTAT BsmCopyMicsRegs(BSM_ID bsmId, BOOL remote, EI e);

EXTC RSTAT BsmGetStatChanges(BSM_ID bsmId, BOOL32 force, BSM_STAT_CHANGES *sc,
    UD32 scSize, EI e);
    
EXTC RSTAT BsmStartDataTest(BSM_ID bsmId, const MICS_DT_SPECS *specs,
    UD32 specsSize, EI e);
    
EXTC RSTAT BsmResetMics(BSM_ID bsmId, EI e);

EXTC RSTAT BsmCalcMicsCrc(BSM_ID bsmId, BOOL remote, EI e);
    
EXTC RSTAT BsmClose(BSM_ID bsmId, EI e);

EXTC SD32 BsmReadCcReg(BSM_ID bsmId, UD32 regAddr, EI e);

EXTC RSTAT BsmWriteCcReg(BSM_ID bsmId, UD32 regAddr, UD32 regVal, EI e);

EXTC RSTAT BsmWakeup(BSM_ID bsmId, EI e);

EXTC RSTAT BsmSleep(BSM_ID bsmId, EI e);

EXTC BSM_NVM_RSTAT BsmCheckNvm(void *nvm, EI e);

EXTC RSTAT BsmTransferNvm(void *prevNvm, BSM_NVM *nvm, UD32 nvmSize, EI e);

EXTC RSTAT BsmGetMicsFactNvm(BSM_ID bsmId, BSM_MICS_FACT_NVM *fn, UD32 fnSize, EI e);

#endif /* ensure this file is only included once */
