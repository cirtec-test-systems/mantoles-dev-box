/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adk_AnyMezz_MicsLib_h
#define Adk_AnyMezz_MicsLib_h

#include "Adp/General.h"     /* UINT, RSTAT, BOOL, ... */
#include "Adp/Build/Build.h" /* UD8, UD16, ... */

/**************************************************************************
 * Defines and Macros
 */
 
/* Defines for the "options" argument passed to MicsAbort():
 * 
 * MICS_FLUSH_TX:
 *     Flush the TX buffer on the local MICS chip after the abort.
 * MICS_FLUSH_RX:
 *     Flush the RX buffer on the local MICS chip after the abort.
 * MICS_FLUSH:
 *     Both MICS_FLUSH_TX and MICS_FLUSH_RX (for convenience).
 * MICS_SET_MIN_RESEND:
 *     This tells MicsAbort() to set the minimum resend time when executing
 *     the abort command. For more information, see the prolog for MicsAbort().
 */
#define MICS_FLUSH_TX        (1 << 0)
#define MICS_FLUSH_RX        (1 << 1)
#define MICS_SET_MIN_RESEND  (1 << 2)
/* convenient combinations */
#define MICS_FLUSH  (MICS_FLUSH_TX | MICS_FLUSH_RX)

/* Error group and error codes for the MICS library software on a base station
 * or implant. Note for the error group, the firmware should always reference
 * the global variable MicsLibErr instead of MICS_LIB_ERR, so all of the
 * references will share the same constant global string.
 * 
 * The error ID strings (EID) include a detailed message for each error. These
 * are provided for use on the host (PC). For the format and use of error ID
 * strings, see "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c". The firmware doesn't use
 * the error ID strings because they would require too much memory (the
 * firmware only uses error groups and error codes).
 */
extern const char *MicsLibErr;
#define MICS_LIB_ERR "MicsLibErr"
/**/
#define MICS_LIB_ERR_REMOTE_HK_TIMEOUT                                       1
#define MICS_LIB_EID_REMOTE_HK_TIMEOUT \
    "MicsLibErr.1: Timed out waiting for a housekeeping reply when " \
    "accessing a register on the remote ZL7010X."
#define MICS_LIB_ERR_REMOTE_HK_WRITES_DISABLED                               2
#define MICS_LIB_EID_REMOTE_HK_WRITES_DISABLED \
    "MicsLibErr.1: Housekeeping write access is disabled on the remote " \
    "ZL7010X."
#define MICS_LIB_ERR_REMOTE_ADC_TIMEOUT                                      3
#define MICS_LIB_EID_REMOTE_ADC_TIMEOUT \
    "MicsLibErr.3: Timed out waiting for the ADC on the remote ZL7010X " \
    "to finish a conversion."
#define MICS_LIB_ERR_4                                                       4
#define MICS_LIB_EID_4 \
    "MicsLibErr.4: This error is no longer defined (shouldn't happen)."
#define MICS_LIB_ERR_5                                                       5
#define MICS_LIB_EID_5 \
    "MicsLibErr.5: This error is no longer defined (shouldn't happen)."
#define MICS_LIB_ERR_6                                                       6
#define MICS_LIB_EID_6 \
    "MicsLibErr.6: This error is no longer defined (shouldn't happen)."
#define MICS_LIB_ERR_ABORT_LINK_TIMEOUT                                      7
#define MICS_LIB_EID_ABORT_LINK_TIMEOUT \
    "MicsLibErr.7: Timed out waiting for the ZL7010X to execute an " \
    "abort command."
#define MICS_LIB_ERR_CALC_CRC_TIMEOUT                                        8
#define MICS_LIB_EID_CALC_CRC_TIMEOUT \
    "MicsLibErr.8: Timed out waiting for the ZL7010X to recalculate " \
    "the CRC."
#define MICS_LIB_ERR_MAC_CMD_TIMEOUT                                         9 
#define MICS_LIB_EID_MAC_CMD_TIMEOUT \
    "MicsLibErr.9: Timed out waiting for the ZL7010X to execute " \
    "command {0} (in reg_mac_ctrl)."
#define MICS_LIB_ERR_ADC_TIMEOUT                                             10
#define MICS_LIB_EID_ADC_TIMEOUT \
    "MicsLibErr.10: Timed out waiting for the ADC on the ZL7010X to" \
    "finish a conversion."
#define MICS_LIB_ERR_WAKEUP_TIMEOUT                                          11
#define MICS_LIB_EID_MICS_WAKEUP_TIMEOUT \
    "MicsLibErr.11: Timed out waiting for the ZL7010X to wake up."
#define MICS_LIB_ERR_12                                                      12
#define MICS_LIB_EID_12 \
    "MicsLibErr.12: This error is no longer defined (shouldn't happen)."
#define MICS_LIB_ERR_ATTEMPT_TO_ACCESS_REMOTE_PAGE_2_VIA_HK                  13
#define MICS_LIB_EID_ATTEMPT_TO_ACCESS_REMOTE_PAGE_2_VIA_HK \
    "MicsLibErr.13: An attempt was made to access a page 2 register on " \
    "a remote ZL7010X via housekeeping. Either the remote ZL7010X is " \
    "a ZL70101, which does not support this, or an implant attempted to " \
    "access page 2 in the base, but the software does not support it."
#define MICS_LIB_ERR_14                                                      14 
#define MICS_LIB_EID_14 \
    "MicsLibErr.14: This error is no longer defined (shouldn't happen)."
#define MICS_LIB_ERR_TIMED_OUT_WAITING_FOR_TX_MODE                           15
#define MICS_LIB_EID_TIMED_OUT_WAITING_FOR_TX_MODE \
    "MicsLibErr.15: Timed out waiting for the ZL7010X to enter TX mode."
#define MICS_LIB_ERR_ZL7010X_DID_NOT_SET_SYNTH_CT_READY                      16
#define MICS_LIB_EID_ZL7010X_DID_NOT_SET_SYNTH_CT_READY \
    "MicsLibErr.16: The ZL7010X did not set the synthesizer coarse tune " \
    "ready flag (reg_rf_synth_ct[4])."
#define MICS_LIB_ERR_SYNTH_DID_NOT_LOCK_FOR_SOME_CHAN                        17
#define MICS_LIB_EID_SYNTH_DID_NOT_LOCK_FOR_SOME_CHAN \
    "MicsLibErr.17: The ZL7010X synthesizer did not lock for some channels."
#define MICS_LIB_ERR_ZL7010X_DID_NOT_SET_SYNTH_CTRIM_READY                   18
#define MICS_LIB_EID_ZL7010X_DID_NOT_SET_SYNTH_CTRIM_READY \
    "MicsLibErr.18: The ZL7010X did not set the synthesizer ctrim ready " \
    "flag (reg_rf_synth_ctrim[6])."
#define MICS_LIB_ERR_SYNTH_CTRIM_READY_NOT_SET                               19
#define MICS_LIB_EID_SYNTH_CTRIM_READY_NOT_SET \
    "MicsLibErr.19: MicsMultiChanSynthCoarseTune() was called without " \
    "first setting the synthesizer ctrim ready flag (reg_rf_synth_ctrim[6])."
#define MICS_LIB_ERR_NVM_POINTER_NOT_INIT                                    20
#define MICS_LIB_EID_NVM_POINTER_NOT_INIT \
    "MicsLibErr.20: A pointer to the ZL7010X radio settings in the " \
    "nonvolatile memory is not initialized." 

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* structure of public data for MICS library */
typedef struct {
    
    /* IRQ status latches. These must be updated by the interrupt service
     * routine when a MICS interrupt occurs (i.e. if bits are set in the IRQ
     * statuses read from the MICS chip, the corresponding bits must set in
     * these latches). For bit definitions, see IRQ_HKREMOTEDONE, etc. in
     * "ZL7010XAdk\Sw\Includes\Adk\AnyMezz\MicsHw.h".
     * 
     * These latches allow the software to poll micro-controller memory to
     * detect when a MICS interrupt occurs instead of having to poll the MICS
     * chip via SPI. To wait for a MICS interrupt, the software must clear the
     * desired bit in a latch, start the desired operation, then poll the latch
     * to detect when the interrupt occurs. Note the desired interrupt must
     * also be enabled for this to work.
     * 
     * Note MicsWakeup() references irqStat2Latch.IRQ_RADIOREADY to detect
     * when the MICS chip is awake. MicsSleep() clears this bit so the next
     * MicsWakeup() can poll it to detect when the chip finishes waking up.
     * MicsAbort() and MicsReset() also clear this bit, but only temporarily
     * (the bit is set again before they return). If anything else clears this
     * bit while the MICS chip is awake, it should set the bit again when done.
     * 
     * These are declared volatile to prevent the compiler from assuming they
     * won't change during a section of code, because if a MICS interrupt
     * occurs, it will change them.
     */
    volatile UD8 irqStat1Latch;
    volatile UD8 irqStat2Latch;
    volatile UD8 irqAuxStatLatch;
    
} MICS_PUB;

/**************************************************************************
 * Global Declarations
 */
 
/* public data for MICS library */ 
extern MICS_PUB MicsPub;

/**************************************************************************
 * External Function Prototypes
 */
 
/* public functions supported on base station and implant */
extern RSTAT MicsInit(void);
extern void MicsWakeup(void);
extern void MicsCopyRegs(void);
extern void MicsCalcCrc(void);
extern UINT MicsRead(UINT regAddr);
extern void MicsWrite(UINT regAddr, UINT val);
extern void MicsWriteBit(UINT regAddr, UINT mask, UINT val);
extern void MicsWriteBits(UINT regAddr, UINT mask, UINT val);
extern int MicsReadR(UINT regAddr, BOOL remote, UD16 timeoutMs);
extern RSTAT MicsWriteR(UINT regAddr, UINT val, BOOL remote, UD16 timeoutMs);
extern RSTAT MicsWriteBitR(UINT regAddr, UINT mask, UINT val, BOOL remote,
    UD16 timeoutMs);
extern RSTAT MicsWriteBitsR(UINT regAddr, UINT mask, UINT val, BOOL remote,
    UD16 timeoutMs);
extern void MicsReadRxBuf(void *buf, UINT len);
extern void MicsWriteTxBuf(const void *data, UINT len);
extern BOOL MicsEnabAdc(BOOL enab);
extern UINT MicsAdc(UINT adcInput);
extern void MicsCal(UINT cals);
extern BOOL MicsEnabExtRssi(BOOL enab);
extern void MicsAbort(UINT options);
extern void MicsReset(void);
extern void MicsSleep(void);
extern void MicsMultiChanSynthCoarseTune(void);
extern UINT MicsSynthCtrim(void);
#if MICS_REV == ZL70101
  /* workaround for ZL70101 issue */
  extern RSTAT MicsWriteHkTxAddr(UINT regAddr, UD16 timeoutMs);
#endif

/* public functions supported on base station only */
#ifdef BASE_STATION
  extern int MicsAdcR(UINT adcInput, BOOL remote);
  extern RSTAT MicsCalR(UINT cals, BOOL remote);
  extern RSTAT MicsCopyRegsR(BOOL remote);
  extern RSTAT MicsCalcCrcR(BOOL remote);
  extern void MicsRevOnImplant(UINT micsRevOnImplant);
#endif

#endif /* ensure this file is only included once */
