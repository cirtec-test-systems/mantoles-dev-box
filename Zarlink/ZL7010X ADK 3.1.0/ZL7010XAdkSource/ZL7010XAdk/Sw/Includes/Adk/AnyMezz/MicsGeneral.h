/**************************************************************************
 * This contains various defines and structures for status and control of the
 * MICS boards (ADK mezzanine boards). These defines and structures are common
 * to both the implant and base station boards. For defines and structures
 * specific to each type of board, see "ZL7010XAdk\Sw\Includes\ImMezz\
 * ImGeneral.h" and "ZL7010XAdk\Sw\Includes\BsmMezz BsmGeneral.h".
 *
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
/* ensure this file is only included once */
#ifndef Adk_AnyMezz_MicsGeneral_h
#define Adk_AnyMezz_MicsGeneral_h

#include "Adp/Build/Build.h"    /* UD8, UD16, ... */
#include "Adk/AnyMezz/MicsHw.h" /* MAX_MICS_CHAN */

/**************************************************************************
 * Defines and Macros
 */

/**************************************************************************
 * Data Structures and Typedefs
 */

/* Structure for IMD transceiver ID (implant ID).
 */
typedef struct {
    
    /* The 3 byte IMD transceiver ID (implant ID). Note these are ordered from
     * MSB (b3) to LSB (b1) so if an ID is read directly from the ZL7010X's RX
     * buffer into a MICS_IMD_TID structure, each byte will appear in its
     * proper location in the structure.
     */
    UD8 b3;  /* MSB of ID */
    UD8 b2;
    UD8 b1;  /* LSB of ID */
    
} MICS_IMD_TID;
 
/* Link configuration (400 MHz).
 */
typedef struct {
        
    /* MICS channel. Note for a base station, if BSM_AUTO_CCA is set in
     * BSM_MICS_CONFIG.flags, some operations (e.g. "start session" and
     * "search for implant") will ignore this channel and perform a clear
     * channel assessment instead.
     */
    UD8 chan;
        
    /* RF modulation for TX & RX (see MAC_MODUSER). For a base station, this
     * also contains the user wakeup data.
     */
    UD8 modUser;
        
    /* TX block size (usually same as RX block size) */
    UD8 txBlockSize;
        
    /* RX block size (usually same as TX block size) */
    UD8 rxBlockSize;
        
    /* max number of blocks to transmit in a packet */
    UD8 maxBlocksPerTxPack;
        
} MICS_LINK_CONFIG;
        
/* Link status.
 */
typedef struct {
    
    /* current IMD transceiver ID (implant ID; 3 bytes) */
    MICS_IMD_TID imdTid;
        
    /* current company ID */
    UD8 companyId;
    
    /* MICS channel */
    UD8 chan;
        
    /* RF modulation for TX & RX (see MAC_MODUSER). For a base station, this
     * also contains the user wakeup data.
     */
    UD8 modUser;
        
    /* TX block size (usually same as RX block size) */
    UD8 txBlockSize;
        
    /* RX block size (usually same as TX block size) */
    UD8 rxBlockSize;
        
    /* max number of blocks to transmit in a packet */
    UD8 maxBlocksPerTxPack;
        
} MICS_LINK_STAT;

/* Link quality. Note each board will clear its link quality each time the PC
 * gets the status (via ImGetStatChanges() for implant, or BsmGetStatChanges()
 * for base). Typically, the PC will get the status once a second.
 */
typedef struct {
        
    /* true if IRQ_MAXBERR occurred since the PC last got MICS_LINK_QUAL */
    BOOL16 maxBErrInts;
        
    /* For a base station, this is true if IRQ_MAXRETRIES occurred since the
     * PC last got MICS_LINK_QUAL. For an implant, this is always false (it has
     * no meaning on an implant, but has been kept for backwards compatibility).
     */
    BOOL16 maxRetriesInts;
        
    /* error corrected blocks since the PC last got MICS_LINK_QUAL */
    UD16 errCorBlocks;
        
    /* CRC errors since the PC last got MICS_LINK_QUAL */
    UD16 crcErrs;
        
} MICS_LINK_QUAL;

/* Data test specifications (passed to BsmStartDataTest() and ImStartDataTest()).
 */
typedef struct {
    
    /* flags for various data test options (see MICS_DT_TX, etc. below) */
    UD8 options;
    
    /* If the MICS_DT_TX option is specified, this specifies the data value to
     * transmit. Not if the MICS_DT_INC_TX option is also specified, the data
     * will start with the specified value, but will be incremented for each
     * subsequent byte.
     */
    UD8 txData;
    
    /* If the MICS_DT_TX option is specified, this specifies the number of data
     * blocks to transmit (0 to keep transmitting until the test is stopped).
     */
    UD16 txBlockCount;
    
    /* If the MICS_DT_RX and MICS_DT_VALIDATE options are specified, this is
     * the data value expected to be received. Note if the MICS_DT_INC_RX
     * option is also specified, the data will start with the specified value,
     * but will be incremented for each subsequent byte.
     */
    UD8 rxData;
    
} MICS_DT_SPECS;
/*
 * Defines for MICS_DT_SPECS.options:
 * 
 * MICS_DT_TX:
 *     If this option is specified, the test will transmit data (otherwise,
 *     it won't transmit any data). Note data will be transmitted as fast as
 *     possible (i.e. whenever there is space available in the ZL7010X's TX
 *     buffer, the test will write data to it).
 * MICS_DT_RX: 
 *     If this option is specified, the test will receive data (i.e. as
 *     the ZL7010X receives data in its RX buffer, the test will read it).
 *     Otherwise, the test will leave the data in the ZL7010X's RX buffer
 *     (for the application to read if desired).
 * MICS_DT_VALIDATE:
 *     If this option is specified together with MICS_DT_RX, the test will
 *     validate the received data (i.e. it will compare each byte to its
 *     expected value).
 * MICS_DT_INC_TX:
 *     If this option is specified together with MICS_DT_TX, the test will
 *     increment each byte of transmitted data.
 * MICS_DT_INC_RX:
 *     If this option is specified together with MICS_DT_RX and
 *     MICS_DT_VALIDATE, the test will increment the data expected
 *     for each received byte.
 */
#define MICS_DT_TX        (1 << 0)
#define MICS_DT_RX        (1 << 1) 
#define MICS_DT_VALIDATE  (1 << 2)
#define MICS_DT_INC_TX    (1 << 3)
#define MICS_DT_INC_RX    (1 << 4)

/* Data status. Note each board will clear its data status each time the PC
 * gets the status (via ImGetStatChanges() for implant, or BsmGetStatChanges()
 * for base). Typically, the PC will get the status once a second. The PC
 * should get the status often enough to ensure these counters won't wrap
 * (~54 seconds for 2FSK_FB, and ~13 seconds for 4FSK). If they wrap, the next
 * status won't be accurate. To get the total counts, the PC must maintain its
 * own running sums, adding the counters in each status to the sums.
 */
typedef struct {
    
    /* Number of data blocks transmitted since the PC last got MICS_DATA_STAT.
     */
    UD16 txBlockCount;
    
    /* Number of data blocks received since the PC last got MICS_DATA_STAT.
     */
    UD16 rxBlockCount;
    
    /* Number of received bytes that didn't match the expected value (since
     * the PC last got MICS_DATA_STAT). Note this is only meaningful when the
     * data test is running with the MICS_DT_RX and MICS_DT_VALIDATE options
     * (see the MICS_DT_SPECS structure).
     */
    UD16 dataErrCount;
    
} MICS_DATA_STAT;

/* Structure for the section of the nonvolatile memory that contains the
 * factory settings for the ZL7010X radio on a base station. Note that this
 * structure should contain only fields that are applicable to all applications
 * for all boards that use the ZL7010X radio. It should not contain any fields
 * that are applicable only to specific applications.
 */
typedef struct
{
    /* This is the result of the RSSI-to-dBm calibration that is performed
     * manually at the factory, which is the RSSI measured for a -96.0 dBm
     * input signal for each channel. This is used to convert RSSI measurements
     * to approximate signal levels in dBm, and is also used as the RSSI for the
     * LBT threshold power level. The LBT threshold is defined in section 10.1
     * of the MICS specification (ETSI EN 301 839-1 V1.3.1), and is calculated
     * according to the equation described in section 10.1.4. For the ZL7010X
     * ADK the LBT threshold was calculated to be -96 dBm, which is why the
     * RSSI-to-dBm calibration uses a -96 dBm input signal. That way, the result
     * of the RSSI-to-dBm calibration can also be used as the LBT threshold.
     */
    UD16_BYTES rssiToDbmCal[MAX_MICS_CHAN + 1];

    /* This is the result of the RX ADC trim that is performed manually at the
     * factory. If this contains a valid value (0x1F or less), the base station
     * writes this to RXIFADCDECLEV (reg_rf_rxifadcdeclev). Otherwise, the base
     * station just uses the RXIFADCDECLEV value determined by the automated RX
     * ADC trim that the ZL7010X performs each time it wakes up. Note that the
     * manual factory calibration is only required for 4FSK, but can also be
     * used for 2FSK and 2FSK-FB if desired. For more information, see the
     * ZL7010X Design Manual.
     */
    UD8 rxIfAdcDecLev;

} BSM_MICS_FACT_NVM;

/* Structure for the section of the nonvolatile memory that contains the
 * factory settings for the ZL7010X radio on an implant. Note that this
 * structure should contain only fields that are applicable to all applications
 * for all boards that use the ZL7010X radio. It should not contain any fields
 * that are applicable only to specific applications.
 */
typedef struct
{
    /* This is the result of the RSSI-to-dBm calibration that is performed
     * manually at the factory, which is the RSSI measured for a -96.0 dBm
     * input signal. This is used to convert RSSI measurements to approximate
     * signal levels in dBm.
     */
    UD8 rssiToDbmCal;

    /* This is the result of the RX ADC trim that is performed manually at the
     * factory. If this contains a valid value (0x1F or less), the implant writes
     * this to RXIFADCDECLEV (reg_rf_rxifadcdeclev). Otherwise, the implant just
     * uses the RXIFADCDECLEV value determined by the automated RX ADC trim that
     * the ZL7010X performs each time it wakes up. Note that the manual factory
     * calibration is only required for 4FSK, but can also be used for 2FSK and
     * 2FSK-FB if desired. For more information, see the ZL7010X Design Manual.
     */
    UD8 rxIfAdcDecLev;

    /* This is the result of the 24-MHz crystal oscillator tuning that is
     * performed manually at the factory. The implant writes this to XO_TRIM
     * (reg_rf_xo_trim). For more information, see the ZL7010X Design Manual.
     */
    UD8 xoTrim;

    /* This is the result of the RSSI offset trim performed manually at the
     * factory. The implant writes this to RSSITRIM (reg_rf_rssitrim). For
     * more information, see reg_rf_rssitrim in the ZL7010X Design Manual.
     */
    UD8 rssiOffsetTrim;

} IM_MICS_FACT_NVM;

/**************************************************************************
 * Global Declarations
 */

/* Pointer to the factory settings for the ZL7010X (MICS) in nonvolatile
 * memory (NVM). Note that the application must initialize this pointer
 * before calling anything that references it.
 */
#ifdef BASE_STATION
extern const BSM_MICS_FACT_NVM *MicsFactNvm;
#else
extern const IM_MICS_FACT_NVM *MicsFactNvm;
#endif

/**************************************************************************
 * External Function Prototypes
 */
 
#endif /* ensure this file is only included once */
