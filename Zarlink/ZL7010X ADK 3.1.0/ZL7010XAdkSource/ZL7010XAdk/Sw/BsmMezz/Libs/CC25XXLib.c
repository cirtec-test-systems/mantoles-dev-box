/**************************************************************************
 * This file contains the CC25XX library, which provides functions to
 * interface to the CC25XX chip (CC2500 or CC2550).
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Adp/General.h"              /* UINT, RSTAT, WR_BITS(), ... */
#include "Adp/Build/Build.h"          /* UD8, _disable_interrupts(), BSM_MODEL, ... */
#include "Adp/AnyBoard/LocalSpiHw.h"  /* LSPI_SOMI, ... */
#include "Adp/AnyBoard/LocalSpiLib.h" /* LSpiRead(), ... */
#include "Adk/BsmMezz/CC25XXHw.h"     /* CC_FREQ0, ... */
#include "Adk/BsmMezz/CC25XXLib.h"    /* public include for CC25XX lib */
#include "Adk/BsmMezz/BsmGeneral.h"   /* BSM200, ... */

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/**************************************************************************
 * Function Prototypes
 */
 
/**************************************************************************
 * Initialize CC25XX interface.
 */
RSTAT
CcInit(void)
{
    return(0);
}

UINT
CcRead(UINT regAddr)
{
    UINT gie;
    UINT regVal;
    
    /* Save current GIE setting (general interrupt enable), then disable all
     * interrupts so no ISR will try to use the SPI bus.
     */
    gie = _get_SR_register() & GIE;
    _disable_interrupts();
    
    /* select chip on SPI bus, then wait for chip ready (SOMI low) */
    CC_WR_SPI_CS(CC_SPI_ON);
    while(LSPI_SOMI);
    
    /* transmit register address and receive register value */
    regVal = LSpiRead(CC_READ | regAddr);
        
    /* de-select chip on SPI bus */
    CC_WR_SPI_CS(CC_SPI_OFF);
    
    /* if interrupts were enabled, re-enable them */
    _bis_SR_register(gie);
    
    /* return register value received from chip */
    return(regVal);
}

void
CcReadBurst(UINT startRegAddr, void *buf, UINT len)
{
    UINT gie;
    
    /* Save current GIE setting (general interrupt enable), then disable all
     * interrupts so no ISR will try to use the SPI bus.
     */
    gie = _get_SR_register() & GIE;
    _disable_interrupts();
    
    /* select chip on SPI bus, then wait for chip ready (SOMI low) */
    CC_WR_SPI_CS(CC_SPI_ON);
    while(LSPI_SOMI);
    
    /* transmit starting address and receive specified number of bytes */
    LSpiReadMulti(CC_READ | CC_BURST | startRegAddr, buf, len);
    
    /* de-select chip on SPI bus */
    CC_WR_SPI_CS(CC_SPI_OFF);
    
    /* if interrupts were enabled, re-enable them */
    _bis_SR_register(gie);
}

void
CcWrite(UINT regAddr, UINT regVal)
{
    UINT gie;
    
    /* Save current GIE setting (general interrupt enable), then disable all
     * interrupts so no ISR will try to use the SPI bus.
     */
    gie = _get_SR_register() & GIE;
    _disable_interrupts();
    
    /* select chip on SPI bus, then wait for chip ready (SOMI low) */
    CC_WR_SPI_CS(CC_SPI_ON);
    while(LSPI_SOMI);
    
    /* transmit register address and value */
    LSpiWrite(regAddr, regVal);
        
    /* de-select chip on SPI bus */
    CC_WR_SPI_CS(CC_SPI_OFF);
    
    /* if interrupts were enabled, re-enable them */
    _bis_SR_register(gie);
}

void
CcWriteBurst(UINT startRegAddr, const void *data, UINT len)
{
    UINT gie;
    
    /* Save current GIE setting (general interrupt enable), then disable all
     * interrupts so no ISR will try to use the SPI bus.
     */
    gie = _get_SR_register() & GIE;
    _disable_interrupts();
    
    /* select chip on SPI bus */
    CC_WR_SPI_CS(CC_SPI_ON);
    while(LSPI_SOMI);
    
    /* transmit MICS RX buf address followed by specified data bytes */
    LSpiWriteMulti(CC_BURST | startRegAddr, data, len);
    
    /* de-select chip on SPI bus */
    CC_WR_SPI_CS(CC_SPI_OFF);
    
    /* if interrupts were enabled, re-enable them */
    _bis_SR_register(gie);
}

void
CcCommand(UINT command)
{
    UINT gie;
    
    /* Save current GIE setting (general interrupt enable), then disable all
     * interrupts so no ISR will try to use the SPI bus.
     */
    gie = _get_SR_register() & GIE;
    _disable_interrupts();
    
    /* select chip on SPI bus, then wait for chip ready (SOMI low) */
    CC_WR_SPI_CS(CC_SPI_ON);
    while(LSPI_SOMI);
        
    /* Write command to SPI TX buf, then wait for SPI interface to finish
     * transmitting it to the chip.
     */
    LSPI_TX_BUF = (UD8)command;
    while(!LSPI_TX_DONE);
    
    /* de-select chip on SPI bus */
    CC_WR_SPI_CS(CC_SPI_OFF);
    
    /* if interrupts were enabled, re-enable them */
    _bis_SR_register(gie);
}

UINT
CcChipStat(void)
{
    UINT gie;
    UINT chipStat;
    
    /* Save current GIE setting (general interrupt enable), then disable all
     * interrupts so no ISR will try to use the SPI bus.
     */
    gie = _get_SR_register() & GIE;
    _disable_interrupts();
    
    /* select chip on SPI bus, then wait for chip ready (SOMI low) */
    CC_WR_SPI_CS(CC_SPI_ON);
    while(LSPI_SOMI);
        
    /* Write no-op command to SPI TX buf, then wait for SPI interface to finish
     * transmitting it to the chip.
     */
    LSPI_TX_BUF = CC_SNOP;
    while(!LSPI_TX_DONE);
    
    /* read chip status received from chip */
    chipStat = LSPI_RX_BUF;
    
    /* de-select chip on SPI bus */
    CC_WR_SPI_CS(CC_SPI_OFF);
    
    /* if interrupts were enabled, re-enable them */
    _bis_SR_register(gie);
    
    /* return chip status received from chip */
    return(chipStat);
}
