/**************************************************************************
 * This file contains functions to manage the LED's on the base station
 * mezzanine board.
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

#include "Adp/General.h"         /* UINT, RSTAT, BOOL, WR_BITS(), ... */
#include "Adp/Build/Build.h"     /* UD8, TX_400_LED, BANK_A_LED_P(), ... */
#include "Adk/AnyMezz/MicsHw.h"  /* TXBUFF_USED, RXBUFF_USED */
#include "Adk/AnyMezz/MicsLib.h" /* MicsRead() */

/* local include shared by all source files for base station application */
#include "BsmApp.h"

/**************************************************************************
 * Defines and Macros
 */
 
/* Milliseconds between updates of the LED's, and the corresponding number of
 * clocks. For best accuracy, this should be a multiple of BSM_MS_PER_CLOCK.
 */
#define BSM_MS_PER_LED_UPDATE  200
#define BSM_CLOCKS_PER_LED_UPDATE \
    (BSM_MS_PER_LED_UPDATE / BSM_MS_PER_CLOCK)
    
/* Milliseconds between heartbeats (i.e. flash the heartbeat LED), and the
 * corresponding number of LED updates. For best accuracy, this should be a
 * multiple of BSM_MS_PER_LED_UPDATE.
 */
#define BSM_MS_PER_HEARTBEAT  3000
#define BSM_LED_UPDATES_PER_HEARTBEAT \
    (BSM_MS_PER_HEARTBEAT / BSM_MS_PER_LED_UPDATE)
    
/* mask for all LED's in bank A and bank B */
#define BSM_BANK_A_LEDS  (TX_400_LED | RX_400_LED | TX_245_LED | \
    SESSION_LED | HEARTBEAT_LED)
#define BSM_BANK_B_LEDS  (LINK_QUAL_LED_0 | LINK_QUAL_LED_1 | \
    LINK_QUAL_LED_2 | LINK_QUAL_LED_3)
    
/* mask for link quality LED's only */
#define BSM_LINK_QUAL_LEDS  (LINK_QUAL_LED_0 | LINK_QUAL_LED_1 | \
    LINK_QUAL_LED_2 | LINK_QUAL_LED_3)
    
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* structure of private data for LED interface on base station */
typedef struct {
    
    /* Various flags for the LED's. For bit definitions, see
     * BSM_TX_400_LED_AUTO_OFF, etc. This is declared volatile to prevent
     * the compiler from assuming it won't change during a section of code,
     * in case it's changed by interrupts.
     */
    volatile UD8 flags;
    
    /* Number of clocks left until BsmPeriodicLedTasks() will update the LED's
     * (BsmProc() periodically calls BsmPeriodicLedTasks()).
     */
    UD8 clocksToLedUpdate;
    
    /* Number of LED updates left until BsmPeriodicLedTasks() will flash the
     * heartbeat LED (BsmProc() periodically calls BsmPeriodicLedTasks() to
     * update the LED's).
     */
    UD8 ledUpdatesToHeartbeat;
    
    /* These are used to manage the on/off state of the LED's in bank A:
     * 
     * ledsOffA: The desired state of the LED's in bank A. If a bit is
     * set, it indicates the application wants the corresponding LED off.
     * BsmPeriodicLedTasks() will turn the LED off after it's been on for >=
     * one update period. BsmProc() periodically calls BsmPeriodicLedTasks()
     * to update the LED's.
     * 
     * ledsOffForNextUpdateA: The LED's in bank A that BsmPeriodicLedTasks()
     * will turn off during the next update (BsmProc() periodically calls
     * BsmPeriodicLedTasks() to update the LED's). This is used to wait one
     * update period before turning off the LED's, to ensure they'll remain
     * on long enough to be seen.
     */
    UD8 ledsOffA;
    UD8 ledsOffForNextUpdateA;
    
    /* override mode: BSM_NO_LED_OVERRIDE (0), BSM_ALL_LEDS_OFF, ... */
    UD8 overrideMode;
    
} BSM_LED_PRIV;
/*
 * Defines for bits in BSM_LED_PRIV.flags:
 * 
 * BSM_TX_400_LED_AUTO_OFF: True if we want BsmPeriodicLedTasks() to
 * automatically turn off the 400 MHz TX LED when the TX buffer on the
 * ZL7010X is empty. BsmProc() periodically calls BsmPeriodicLedTasks()
 * to update the LED's.
 * 
 * BSM_RX_400_LED_AUTO_OFF: True if we want BsmPeriodicLedTasks() to
 * automatically turn off the the 400 MHz RX LED when the RX buffer on the
 * ZL7010X is empty. BsmProc() periodically calls BsmPeriodicLedTasks() to
 * update the LED's.
 */
#define BSM_TX_400_LED_AUTO_OFF  (1 << 0)
#define BSM_RX_400_LED_AUTO_OFF  (1 << 1)
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/* private data for LED interface on base station */
static BSM_LED_PRIV BsmLedPriv;
 
/**************************************************************************
 * Function Prototypes
 */
 
/**************************************************************************
 * Initialize the LED interface on the base station.
 */
void
BsmLedInit(void)
{
    BSM_LED_PRIV *bl = &BsmLedPriv;
    
    /* clear data and init any non-zero defaults */
    (void)memset(bl, 0, sizeof(*bl));
    bl->clocksToLedUpdate = BSM_CLOCKS_PER_LED_UPDATE;
    bl->ledUpdatesToHeartbeat = BSM_LED_UPDATES_PER_HEARTBEAT;
    bl->ledsOffA = BSM_BANK_A_LEDS;
    
    /* make sure all LED's are initially off */
    BANK_A_LED_P(OUT) |= BSM_BANK_A_LEDS;
    BANK_B_LED_P(OUT) |= BSM_BANK_B_LEDS;
}

/**************************************************************************
 * Functions to turn LED's on and off. Note these functions perform additional
 * tasks such as checking for LED override, and managing LED status & control
 * flags. These functions work together with BsmPeriodicLedTasks() to ensure
 * the LED's stay on long enough to be seen (BsmProc() periodically calls
 * BsmPeriodicLedTasks() to update the LED's).
 */
void
BsmTx400LedOn(BOOL autoOff)
{
    /* if LED override is off, turn LED on */
    if (!BsmLedPriv.overrideMode) BANK_A_LED_P(OUT) &= ~TX_400_LED;
    
    /* indicate LED is on */
    BsmLedPriv.ledsOffA &= ~TX_400_LED;
    
    /* make sure BsmPeriodicLedTasks() won't turn LED off during next update */
    BsmLedPriv.ledsOffForNextUpdateA &= ~TX_400_LED;
    
    /* tell BsmPeriodicLedTasks() to turn LED off when MICS TX buf is empty */
    if (autoOff) BsmLedPriv.flags |= BSM_TX_400_LED_AUTO_OFF;
    
    /* set status flag so it will be reported to PC */
    BsmPub.statFlags |= BSM_400_MHZ_TX_ACTIVE;
}

void
BsmTx400LedOff(void)
{
    /* indicate caller wants LED off (BsmPeriodicLedTasks() will turn it off) */
    BsmLedPriv.ledsOffA |= TX_400_LED;
    
    /* ensure the LED's "auto off" flag is clear since it's no longer needed */
    BsmLedPriv.flags &= ~BSM_TX_400_LED_AUTO_OFF;
}

BOOL
BsmTx400LedIsOff(void)
{
    return(BsmLedPriv.ledsOffA & TX_400_LED);
}

void
BsmRx400LedOn(BOOL autoOff)
{
    /* if LED override is off, turn LED on */
    if (!BsmLedPriv.overrideMode) BANK_A_LED_P(OUT) &= ~RX_400_LED;
    
    /* indicate LED is on */
    BsmLedPriv.ledsOffA &= ~RX_400_LED;
    
    /* make sure BsmPeriodicLedTasks() won't turn LED off during next update */
    BsmLedPriv.ledsOffForNextUpdateA &= ~RX_400_LED;
    
    /* tell BsmPeriodicLedTasks() to turn LED off when MICS RX buf is empty */
    if (autoOff) BsmLedPriv.flags |= BSM_RX_400_LED_AUTO_OFF;
    
    /* set status flag so it will be reported to PC */
    BsmPub.statFlags |= BSM_400_MHZ_RX_ACTIVE;
}

void
BsmRx400LedOff(void)
{
    /* indicate caller wants LED off (BsmPeriodicLedTasks() will turn it off) */
    BsmLedPriv.ledsOffA |= RX_400_LED;
    
    /* ensure the LED's "auto off" flag is clear since it's no longer needed */
    BsmLedPriv.flags &= ~BSM_RX_400_LED_AUTO_OFF;
}

BOOL
BsmRx400LedIsOff(void)
{
    return(BsmLedPriv.ledsOffA & RX_400_LED);
}

void
BsmTx245LedOn(void)
{
    /* if LED override is off, turn LED on */
    if (!BsmLedPriv.overrideMode) BANK_A_LED_P(OUT) &= ~TX_245_LED;
    
    /* indicate LED is on */
    BsmLedPriv.ledsOffA &= ~TX_245_LED;
    
    /* make sure BsmPeriodicLedTasks() won't turn LED off during next update */
    BsmLedPriv.ledsOffForNextUpdateA &= ~TX_245_LED;
    
    /* set status flag so it will be reported to PC */
    BsmPub.statFlags |= BSM_245_GHZ_TX_ACTIVE;
}

void
BsmTx245LedOff(void)
{
    /* indicate caller wants LED off (BsmPeriodicLedTasks() will turn it off) */
    BsmLedPriv.ledsOffA |= TX_245_LED;
}

BOOL
BsmTx245LedIsOff(void)
{
    return(BsmLedPriv.ledsOffA & TX_245_LED);
}

void
BsmSessionLedOn(void)
{
    /* if LED override is off, turn LED on */
    if (!BsmLedPriv.overrideMode) BANK_A_LED_P(OUT) &= ~SESSION_LED;
    
    /* indicate LED is on */
    BsmLedPriv.ledsOffA &= ~SESSION_LED;
    
    /* make sure BsmPeriodicLedTasks() won't turn LED off during next update */
    BsmLedPriv.ledsOffForNextUpdateA &= ~SESSION_LED;
}
void
BsmSessionLedOff(void)
{
    /* indicate caller wants LED off (BsmPeriodicLedTasks() will turn it off) */
    BsmLedPriv.ledsOffA |= SESSION_LED;
}

/* Adjust link quality LED's to indicate the specified link quality (see
 * BSM_LINK_QUAL_POOR, etc. in "BsmApp.h").
 */
void 
BsmSetLinkQualLeds(UINT linkQual)
{
    UINT ledsOffB;
    
    /* if LED override is on, don't change link quality LED's */
    if (BsmLedPriv.overrideMode) return;
    
    /* get link quality LED's to turn off for the specified link quality */
    switch(linkQual) {
    case BSM_LINK_QUAL_NONE:
        ledsOffB = LINK_QUAL_LED_0 | LINK_QUAL_LED_1 |
            LINK_QUAL_LED_2 | LINK_QUAL_LED_3;
        break;
    case BSM_LINK_QUAL_POOR:
        ledsOffB = LINK_QUAL_LED_1 | LINK_QUAL_LED_2 | LINK_QUAL_LED_3;
        break;
    case BSM_LINK_QUAL_FAIR:
        ledsOffB = LINK_QUAL_LED_2 | LINK_QUAL_LED_3;
        break;
    case BSM_LINK_QUAL_GOOD:
        ledsOffB = LINK_QUAL_LED_3;
        break;
    case BSM_LINK_QUAL_EXCELLENT:
    default:
        ledsOffB = 0;
        break;
    }
    
    /* turn each link quality LED on or off as desired */
    WR_BITS(BANK_B_LED_P(OUT), BSM_LINK_QUAL_LEDS, ledsOffB);
}

/* Set LED override mode. The following modes can be specified:
 * 
 * BSM_NO_LED_OVERRIDE:
 *     LED override is shut off and each LED is restored to its desired state.
 * BSM_ALL_LEDS_OFF:
 *     All LED's are turned off and will remain off.
 * BSM_ALL_LEDS_ON:
 *     All LED's are turned on and will remain on.
 * BSM_FLASH_ALL_LEDS:
 *     All LED's are flashed.
 */
void
BsmOverrideLeds(UINT mode)
{
    switch(mode) {
    case BSM_NO_LED_OVERRIDE:
    
        /* restore each bank A LED to desired state, & bank B LED's off */
        WR_BITS(BANK_A_LED_P(OUT), BSM_BANK_A_LEDS, BsmLedPriv.ledsOffA);
        BANK_B_LED_P(OUT) |= BSM_BANK_B_LEDS;
        break;
    
    case BSM_ALL_LEDS_OFF:

        /* turn off all LED's */
        BANK_A_LED_P(OUT) |= BSM_BANK_A_LEDS; 
        BANK_B_LED_P(OUT) |= BSM_BANK_B_LEDS;
        break;
        
    case BSM_ALL_LEDS_ON:
    case BSM_FLASH_ALL_LEDS:

        /* turn on all LED's */
        BANK_A_LED_P(OUT) &= ~BSM_BANK_A_LEDS; 
        BANK_B_LED_P(OUT) &= ~BSM_BANK_B_LEDS;
        break;
        
    default:
        /* specified invalid mode, so do nothing (shouldn't happen) */
        break;
    }
    
    /* save new LED override mode */
    BsmLedPriv.overrideMode = mode;
}

/* BsmProc() calls this each clock tick (see BSM_MS_PER_CLOCK) to do any
 * periodic tasks required by the LED's on the base station.
 */
void
BsmPeriodicLedTasks(BOOL clockOverdue)
{
    BSM_LED_PRIV *bl = &BsmLedPriv;
    
    /* If the clock is overdue, don't update the LED's. That way, we'll never
     * update the LED's multiple times in rapid succession (it's ok if the
     * timing for LED updates sometimes shifts).
     */
    if (clockOverdue) return;
    
    /* if it's time to update the LED's, do so */
    if (--bl->clocksToLedUpdate == 0) {
        
        /* restart number of clocks to next LED update */
        bl->clocksToLedUpdate = BSM_CLOCKS_PER_LED_UPDATE;
        
        /* if it's time to flash the heartbeat LED */
        if (--bl->ledUpdatesToHeartbeat == 0) {
            
            /* restart number of LED updates to next heartbeat */
            bl->ledUpdatesToHeartbeat = BSM_LED_UPDATES_PER_HEARTBEAT;
            
            /* turn heartbeat LED on */
            if (!BsmLedPriv.overrideMode) BANK_A_LED_P(OUT) &= ~HEARTBEAT_LED;
    
            /* make BsmPeriodicLedTasks() leave LED on for one update period */
            BsmLedPriv.ledsOffForNextUpdateA &= ~HEARTBEAT_LED;
        }
        
        /* If desired, check if the TX buffer on the MICS chip is empty, and
         * if so, automatically turn off the 400 MHz TX LED.
         */
        if (bl->flags & BSM_TX_400_LED_AUTO_OFF) {
            if (MicsRead(TXBUFF_USED) == 0) BsmTx400LedOff();
        }
        /* If desired, check if the RX buffer on the MICS chip is empty, and
         * if so, automatically turn off the 400 MHz RX LED.
         */
        if (bl->flags & BSM_RX_400_LED_AUTO_OFF) {
            if (MicsRead(RXBUFF_USED) == 0) BsmRx400LedOff();
        }
        
        /* If LED override is off, set the output pin for each LED we want to
         * turn off during this update, then save the LED's we want the next
         * update to turn off. This is done to wait one update period before
         * turning off the LED's, to ensure they'll remain on long enough to
         * be seen.
         */
        if (!bl->overrideMode) BANK_A_LED_P(OUT) |= bl->ledsOffForNextUpdateA;
        bl->ledsOffForNextUpdateA = bl->ledsOffA;
        
        /* if override mode is set to flash all LED's, do so */
        if (bl->overrideMode == BSM_FLASH_ALL_LEDS) {
            INVERT_BITS(BANK_A_LED_P(OUT), BSM_BANK_A_LEDS);
            INVERT_BITS(BANK_B_LED_P(OUT), BSM_BANK_B_LEDS);
        }
    }
}
