/**************************************************************************
 * This include file contains things that are shared by all of the source files
 * for the base station application. Note this is private to the base station
 * application, and should not be included by anything outside of the base
 * station application.
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
/* ensure this file is only included once */
#ifndef BsmApp_h
#define BsmApp_h

#include "Standard/String.h"         /* NULL (BsmErr() args) */
#include "Standard/StdArg.h"         /* va_list (BsmErr() args) */
#include "Adp/General.h"             /* UINT, RSTAT, BOOL, TRUE/FALSE, ... */
#include "Adp/Build/Build.h"         /* UD16, ADC12MCTL0, ... */
#include "Adp/AnyBoard/ErrLib.h"     /* ERR_INFO */
#include "Adp/AnyBoard/TraceLib.h"   /* Trace0(), ... (for test/debug use) */
#include "Adp/AnyBoard/LocalSpiHw.h" /* LSPI_SPEC_..., LSPI_CLK_FREQ, ... */
#include "Adk/AnyMezz/MicsGeneral.h" /* MICS_LINK_QUAL, MICS_DATA_STAT, ... */
#include "Adk/BsmMezz/BsmGeneral.h"  /* BSM_MICS_CONFIG, ... */
#include "Adk/BsmMezz/BsmAppErrs.h"  /* BsmAppErr, BSM_APP_ERR_... */

/**************************************************************************
 * Defines and Macros
 */
 
/* Milliseconds per clock. This determines how often BsmProc() checks if
 * any periodic tasks need to be performed, and the minimum time period and
 * resolution for all periodic tasks, so it should be set accordingly.
 */
#define BSM_MS_PER_CLOCK  50

/* macro to get length of a constant string (excluding the '\0' terminator) */
#define BSM_LEN(constStr)  (sizeof(constStr) - 1)

/* defines for "autoOff" argument passed to some "LED on" functions */
#define BSM_NO_AUTO_OFF  FALSE
#define BSM_AUTO_OFF     TRUE

/* defines for override mode passed to BsmOverrideLeds() */
#define BSM_NO_LED_OVERRIDE  0
#define BSM_ALL_LEDS_OFF     1
#define BSM_ALL_LEDS_ON      2
#define BSM_FLASH_ALL_LEDS   3

/* defines for link quality argument passed to BsmSetLinkQualLeds() */
#define BSM_LINK_QUAL_NONE       0
#define BSM_LINK_QUAL_POOR       1
#define BSM_LINK_QUAL_FAIR       2
#define BSM_LINK_QUAL_GOOD       3
#define BSM_LINK_QUAL_EXCELLENT  4

/* defines for mode argument passed to BsmSetTx245Mode() */
#define BSM_TX_245_OFF      0  /* no 2.45 GHz TX */
#define BSM_TX_245_WAKE     1  /* transmit 2.45 GHz wakeup signal */
#define BSM_TX_245_CARRIER  2  /* transmit 2.45 GHz continuous carrier */

/* Clock frequency, dividers, and specs for local SPI bus. Note if using a
 * ZL70101, its SPI interface resets to 2 MHz max whenever it's awakened or
 * reset. Thus, if the local SPI clock is normally 4 MHz, the base must
 * temporarily change it to 2 MHz, configure the ZL70101 for 4 MHz, then 
 * restore the SPI clock to 4 MHz again. This is handled in the ZL70101
 * interrupt service routine (see BsmMicsIsr() and BsmInitInterfaceMode()).
 */
#define BSM_LSPI_CLK_FREQ  4000000UL
#define BSM_LSPI_CLK_DIV   (UD8)(LSPI_CLK_FREQ / BSM_LSPI_CLK_FREQ)
/* clock divider for 1 & 2 MHz */
#define BSM_LSPI_CLK_DIV_FOR_1_MHZ  (UD8)(LSPI_CLK_FREQ / (UD32)1000000UL)
#define BSM_LSPI_CLK_DIV_FOR_2_MHZ  (UD8)(LSPI_CLK_FREQ / (UD32)2000000UL)
/* specs (for specs argument passed LSpiInit() and LSpiConfig()) */
#define BSM_LSPI_SPECS  (LSPI_SPEC_INACTIVE_LOW_WITH_SAMP_ON_RISING_EDGE | \
    LSPI_SPEC_MASTER | LSPI_SPEC_8_BIT | LSPI_SPEC_MSB_FIRST)
 
/**************************************************************************
 * Data Structures and Typedefs
 */

/* structure for CC25XX configuration */
typedef struct {

    /* 2.45 GHz TX power (for first entry in CC_PATABLE in CC25XX) */
    UD8 ccPa;

    /* 2.45 GHz TX frequency control (for CC_FREQ0/1/2 in CC25XX) */
    UD8 ccFreq0;
    UD8 ccFreq1;
    UD8 ccFreq2;

} BSM_CC_CONFIG;
 
/* structure for data shared by all source files for base station application */
typedef struct {
    
    /* Flags used to indicate tasks are pending & require processing. This is
     * declared volatile to prevent the compiler from assuming it won't change
     * during a section of code, since it may be changed by interrupts. For
     * bit definitions, see BSM_ADP_SPI_RX_PEND, etc.
     */
    volatile UD8 pend;
    
    /* Various flags shared by all of the source files for the base station
     * application. For bit definitions, see BSM_INIT_ERR, etc. This is declared
     * volatile to prevent the compiler from assuming it won't change during a
     * section of code, in case it is changed by interrupts.
     */
    volatile UD8 flags;
    
    /* Status flags for BSM_STAT.flags (see BSM_STAT, BSM_ERR_OCCURRED, etc.
     * in "ZL7010XAdk\Sw\Includes\Adk\BsmMezz\BsmGeneral.h"). This is declared
     * volatile to prevent the compiler from assuming it won't change during a
     * section of code, since it may be changed by interrupts.
     */
    volatile UD8 statFlags;
    
    /* Current MICS operational state (see BSM_IDLE, BSM_STARTING_SESSION, etc.
     * in "ZL7010XAdk\Sw\Includes\Adk\BsmMezz\BsmGeneral.h"). Note this is
     * declared "const" because it's managed by BsmAppMics.c, which is the only
     * thing that should modify it. It is also declared volatile to prevent the
     * compiler from assuming it won't change during a section of code, since
     * the MICS interrupts change it.
     */
    volatile const UD8 micsState;
    
    /* current configuration for MICS chip and CC25XX (see BsmAppMics.c) */
    BSM_MICS_CONFIG micsConfig;
    BSM_CC_CONFIG ccConfig;

    /* The current block size settings in TXBUFF_BSIZE and RXBUFF_BSIZE.
     * To set TXBUFF_BSIZE and RXBUFF_BSIZE, the software should always call
     * BsmSetTxBlockSize() and BsmSetRxBlockSize() so these block sizes are
     * also updated. That way, the software can reference the current block
     * sizes without having to read TXBUFF_BSIZE and RXBUFF_BSIZE (see
     * BsmTransmitMics() and BsmReceiveMics()).
     */
    const UD8 txBlockSize;
    const UD8 rxBlockSize;
    
    /* link quality statistics (see BsmAppMics.c) */
    MICS_LINK_QUAL linkQual;
    
    /* data status (see BsmAppMics.c) */
    MICS_DATA_STAT dataStat;
    
} BSM_PUB;
/*
 * Defines for bits in BSM_PUB.pend (task pending flags for BsmProc()).
 * 
 * BSM_ADP_SPI_RX_PEND:
 *     BsmAdpSpiRxIsr() sets this to tell BsmProc() to call BsmProcAdpSpiRx()
 *     (receive and process a packet via the ADP SPI interface).
 * BSM_MICS_SESSION_PEND:
 *     BsmLinkReadyIsr() sets this to tell BsmProc() to call
 *     BsmProcMicsSession() (MICS session started).
 * BSM_MICS_RX_PEND:
 *     BsmMicsRxNotEmptyIsr() sets this to tell BsmProc() to call
 *     BsmProcMicsRx() (data received via MICS). 
 * BSM_MICS_ABORT_PEND:
 *     BsmConLostIsr() sets this to tell BsmProc() to call BsmAbortMics()
 *     (aborts MICS operation after ZL7010X asserts IRQ_WDOG).
 * BSM_WAKE_400_PEND:
 *     When using 400 MHz wakeup, BsmStartSession() and BsmSearchForImplant()
 *     set this flag to tell BsmProc() to call BsmProcWake400() as often as
 *     possible (to check if it's time to listen for responses, etc.). This
 *     flag is cleared when the session starts, or the operation is aborted
 *     (see BsmAbortMics()).
 * BSM_DT_TX_PEND:
 *     BsmStartDt() sets this to tell BsmProc() to call BsmProcDtTx() as
 *     often as possible (to transmit test data). This flag is cleared when
 *     BsmAbortMics() is called to stop the data test.
 */
#define BSM_ADP_SPI_RX_PEND    (1 << 0)
#define BSM_MICS_SESSION_PEND  (1 << 1)
#define BSM_MICS_RX_PEND       (1 << 2)
#define BSM_MICS_ABORT_PEND    (1 << 3)
#define BSM_WAKE_400_PEND      (1 << 4) 
#define BSM_DT_TX_PEND         (1 << 5)
/*
 * Defines for bits in BSM_PUB.flags:
 * 
 * BSM_INIT_ERR:
 *     If an error is encountered during initialization, BsmInit() sets this
 *     to tell BsmReceiveAndProcCmd() to report the error to the PC (instead
 *     of trying to process the command from the PC).
 * BSM_MICS_INIT:
 *     BsmInit() sets this to tell BsmProc() the MICS (ZL7010X) interface
 *     has been initialized, so BsmProc() will call BsmPeriodicMicsTasks()
 *     to perform any periodic tasks required by the MICS interface.
 */
#define BSM_INIT_ERR   (1 << 0)
#define BSM_MICS_INIT  (1 << 1)
 
/**************************************************************************
 * Global Declarations
 */
 
/* data shared by all source files for the base station application */
extern BSM_PUB  BsmPub;

/**************************************************************************
 * External Function Prototypes
 */
 
/* shared functions defined in BsmAppMain.c */
extern void BsmConfigSpi(UINT clockDiv);
extern void BsmErr(UD16 bsmAppErrCode, const char *argFormats, ...);

/* shared functions defined in BsmAppCom.c */
extern RSTAT BsmComInit(void); 
extern void BsmProcAdpSpiRx(void);
extern void BsmReceiveAndDiscard(UINT len);
extern RSTAT BsmReceive(void *buf, UINT len);
extern RSTAT BsmSendReply(const void *rep, UINT repLen);
extern RSTAT BsmSendReplyHdr(UD8 repType, UINT repLen);
extern RSTAT BsmSendReplyData(const void *data, UINT len);
extern RSTAT BsmSendErrReply(UD8 repType, const ERR_INFO *ei);

/* shared functions defined in BsmAppCmd.c */
extern void BsmCmdInit(void);
extern RSTAT BsmReceiveAndProcCmd(UINT cmdType, UINT packLen);

/* shared functions defined in BsmAppMics.c */
extern RSTAT BsmMicsInit(void); 
extern void BsmResetMics(BOOL initMicsConfig);
extern void BsmMicsWakeup(void);
extern void BsmMicsSleep(void);
extern void BsmConfigMics(const BSM_MICS_CONFIG *mc);
extern RSTAT BsmStartSession(BOOL enabWatchdog, UINT timeoutMs);
extern void BsmSearchForImplant(BOOL enabWatchdog, BOOL anyCompany,
    BOOL anyImplant);
extern void BsmListenForEmergency(BOOL startSession);
extern void BsmStartDt(const MICS_DT_SPECS *specs);
extern void BsmAbortMics(void);
extern void BsmResetCc(BOOL initCcConfig);
extern void BsmSetTx245Freq(UD8 ccFreq2, UD8 ccFreq1, UD8 ccFreq0);
extern void BsmSetTx245Power(UD8 ccPa);
extern void BsmSetTx245Mode(UINT mode);
extern void BsmStartTx400Carrier(BOOL start, UINT chan);
extern UINT BsmIntRssi(void);
extern UINT BsmExtRssi(BOOL average);
extern UINT BsmCca(BOOL average, BSM_CCA_DATA *retData);
extern void BsmProcMicsSession(void);
extern void BsmProcMicsRx(void);
extern void BsmProcWake400(void);
extern void BsmProcDtTx(void);
extern UINT BsmTransmitMics(const void *data, UINT nBytes, UINT timeoutMs);
extern UINT BsmReceiveMics(void *buf, UINT nBytes, UINT timeoutMs);
extern void BsmPeriodicMicsTasks(BOOL clockOverdue);
extern void BsmUpdateLinkQual(void);
extern void BsmMicsIsr(void);
extern void BsmWriteCcReg(UD8 reg, UD8 value);

/* shared functions defined in BsmAppAdc.c */
extern RSTAT BsmAdcInit(void);
extern UINT BsmAdc(UINT adcInput);

/* shared functions defined in BsmAppAdc.c */
extern RSTAT BsmDacInit(void);
extern __inline void BsmDac(UINT);

/* shared functions defined in BsmAppLed.c */
extern void BsmLedInit(void);
extern void BsmTx400LedOn(BOOL autoOff);
extern void BsmTx400LedOff(void);
extern BOOL BsmTx400LedIsOff(void);
extern void BsmRx400LedOn(BOOL autoOff);
extern void BsmRx400LedOff(void);
extern BOOL BsmRx400LedIsOff(void);
extern void BsmTx245LedOn(void);
extern void BsmTx245LedOff(void);
extern BOOL BsmTx245LedIsOff(void);
extern void BsmSessionLedOn(void);
extern void BsmSessionLedOff(void);
extern void BsmSetLinkQualLeds(UINT linkQual);
extern void BsmOverrideLeds(UINT mode);
extern void BsmPeriodicLedTasks(BOOL clockOverdue);

/**************************************************************************
 * Inline functions for the ADC in the MSP430.
 * 
 * BsmStartAdc() starts samples for the specified ADC input. Call BsmGetAdc()
 * to get each sample and start the next, and BsmStopAdc() to stop sampling.
 */
__inline void
BsmStartAdc(UINT adcInput)
{
    /* select ADC input for ADC12MEM0 (ADC output location 0) */
    ADC12MCTL0 = SREF_1 | adcInput;
    
    /* start first ADC conversion */
    ADC12CTL0 |= (ENC | ADC12SC);
}

/* Get the previous ADC sample and start the next. Note BsmStartAdc() must be
 * called before BsmGetAdc() (otherwise, BsmGetAdc() will never return).
 */
__inline UINT
BsmGetAdc(void)
{
    UINT prevAdc;
    
    /* wait for previous conversion to finish */
    while((ADC12IFG & BIT0) == 0);
    
    /* get previous conversion (note this is required to clear ADC12SC) */
    prevAdc = ADC12MEM0;
    
    /* start next conversion */
    ADC12CTL0 |= (ENC | ADC12SC);
    
    /* return previous conversion */
    return(prevAdc);
}

/* Start automatic sampling for the specified ADC input. To get each sample,
 * call BsmGetAutoAdc(). To stop sampling, call BsmStopAdc(). Note the ADC will
 * overwrite the previous sample every 3.6875 us, so if you need every sample,
 * you should disable interrupts while sampling, and get each sample before
 * it's overwritten.
 */
__inline void
BsmStartAutoAdc(UINT adcInput)
{
    /* select ADC input for ADC12MEM0 (ADC output location 0) */
    ADC12MCTL0 = SREF_1 | adcInput;
    
    /* config ADC to repeat single channel until ADC12CTL0.ENC is cleared */
    ADC12CTL1 |= CONSEQ_2;
    
    /* enable conversions & start automatic conversions */
    ADC12CTL0 |= (ENC | ADC12SC);
}

/* Get the next automatic ADC sample (for when the ADC is configured to
 * repeat samples automatically). Note this should only be called after
 * BsmStartAutoAdc() is called to start automatic ADC samples (otherwise, it
 * will never return). Note the ADC will overwrite the previous sample every
 * 3.6875 us, so if you need every sample, you should disable interrupts while
 * sampling, and get each sample before it's overwritten.
 */
__inline UINT
BsmGetAutoAdc(void)
{
    /* wait for next conversion to finish */
    while((ADC12IFG & BIT0) == 0);
    
    /* read result (also clears ADC12IFG.BIT0) */
    return(ADC12MEM0);
}

/* Stop ADC samples and disable the ADC. Note any sample read from ADC12MEM0
 * after this is called might be invalid.
 */
__inline
BsmStopAdc(void)
{
    /* clear ADC12CTL1.CONSEQ, then ADC12CTL0.ENC (stop immediately) */
    ADC12CTL1 &= ~CONSEQ_3;
    ADC12CTL0 &= ~ENC;
    
    /* ensure interrupt flag from any conversion is clear */
    ADC12IFG = 0;
}

#endif /* ensure this file is only included once */
