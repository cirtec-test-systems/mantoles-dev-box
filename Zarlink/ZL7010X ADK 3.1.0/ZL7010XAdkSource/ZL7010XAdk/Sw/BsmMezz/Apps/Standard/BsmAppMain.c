/**************************************************************************
 * This file contains the main processing functions for the application on
 * the base station mezzanine board (main entry point, initialization, and
 * processing loop).
 *
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Standard/String.h"          /* memset() */
#include "Adp/General.h"              /* RSTAT, BOOL, TRUE/FALSE, ... */
#include "Adp/Build/Build.h"          /* UD16, WDTCTL, P1OUT_INIT, ... */
#include "Adp/AnyBoard/ErrLib.h"      /* ErrInit(), ... */
#include "Adp/AnyBoard/SysTimerLib.h" /* StInit(), ... */
#include "Adp/AnyBoard/LocalSpiLib.h" /* LSpiInit(), LSpiConfig() */
#include "Adk/BsmMezz/CC25XXLib.h"    /* CcInit() */
#include "Adp/AnyBoard/NvmLib.h"      /* NvmInit(), ... */

/* local include shared by all source files for base station application */
#include "BsmApp.h"

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* structure of private data for main processing */
typedef struct {
    
    /* The time at which the last clock occurred. BsmProc() uses this to
     * determine if it's time for the next clock. At each clock, BsmProc()
     * checks if any periodic tasks need to be performed and performs them
     * as needed.
     */
    UD16 clockTimeMs;
    
} BSM_MAIN_PRIV;
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/* public data for base station application */
BSM_PUB  BsmPub;

/* private data for main processing */
static BSM_MAIN_PRIV  BsmMainPriv;

/* name of error group for base station application */
const char *BsmAppErr = BSM_APP_ERR;

/* Pointer to factory settings for ZL7010X radio in nonvolatile memory (NVM)
 * on both a bsm and an im. Note that the application must initialize this
 * before calling anything that references it.
 */
const BSM_MICS_FACT_NVM *MicsFactNvm;

/**************************************************************************
 * Function Prototypes
 */
static void BsmInit(void);
static RSTAT BsmNvmInit(void);
__inline void BsmInitMcIo(void);
__inline void BsmInitMcClocks(void);
__inline void BsmProc(void);
/* __inline void BsmSleep(void); */

/**************************************************************************
 * Main entry point for application on base station mezzanine board.
 */
int
main(void)
{
    /* initialization */
    BsmInit();
    
    /* main processing loop */
    while(1) { BsmProc(); }

    #pragma diag_suppress 112
    return 0;
    #pragma diag_default 112
}

static void
BsmInit(void)
{
    BOOL comInit = FALSE;
    
    /* stop watchdog timer */
    WDTCTL = WDTPW + WDTHOLD;
    
    /* init micro-controller I/O ports */
    BsmInitMcIo();
    
    /* init micro-controller clocks (must be called before StInit()) */
    BsmInitMcClocks();
    
    /* Clear public & private data and init any non-zero defaults. Note
     * this initializes BsmPub.micsState to 0 (BSM_SLEEPING).
     */
    (void)memset(&BsmPub, 0, sizeof(BsmPub));
    (void)memset(&BsmMainPriv, 0, sizeof(BsmMainPriv));
    
    /* init LED interface (turns all LED's off) */
    BsmLedInit();
    
    /* init error lib */
    if (ErrInit()) goto error;
    
    /* init system timer and enable interrupts so it will start counting */
    if (StInit()) goto error;
    _enable_interrupts();
    
    /* init communication before remaining init so PC can query error, ... */
    if (BsmComInit()) goto error;
    BsmCmdInit();
    comInit = TRUE;
    
    /* Init local SPI bus interface to talk to MICS chip, CC25XX chip, etc.
     * (for more information, see BSM_LSPI_CLK_FREQ in "BsmApp.h").
     */
    if (LSpiInit(BSM_LSPI_SPECS, BSM_LSPI_CLK_DIV)) goto error;
    
    /* init ADC interface (internal ADC in MSP430) */
    if (BsmAdcInit()) goto error;

    /* init CC25XX interface (chip used to transmit 2.45 GHz for wakeup) */
    if (CcInit()) goto error;
    
    /* init interface for nonvolatile memory */
    if(BsmNvmInit()) goto error;

    /* Init interface for MICS (ZL7010X). Note that this must be done after
     * BsmAdcInit(), CcInit(), and BsmNvmInit().
     */
    if (BsmMicsInit()) goto error;
    BsmPub.flags |= BSM_MICS_INIT;
    
    /* done (init successful) */
    return;
    
error:    
    /* If the communication interface wasn't initialized, there's no way
     * to report the initialization error to the PC, so just loop forever
     * (shouldn't happen). Otherwise, tell the LED interface to flash all
     * LED's, and set BSM_INIT_ERR to tell BsmReceiveAndProcCmd() to report
     * the error to the PC.
     */
    if (comInit == FALSE) {
        while(1) {/* BsmSleep(); */};
    }
    BsmOverrideLeds(BSM_FLASH_ALL_LEDS);
    BsmPub.flags |= BSM_INIT_ERR;
}

__inline void
BsmInitMcIo(void)
{
    /* Init output ports. Note unused ports should be initialized as outputs
     * to prevent floating inputs and reduce power (recommended in MSP430
     * manual). It's also best to drive them low in case they are accidently
     * shorted to ground during testing. This also applies to test points.
     */
    P1OUT = P1OUT_INIT;
    P1DIR = P1DIR_INIT;
    /**/
    P2OUT = P2OUT_INIT;
    P2DIR = P2DIR_INIT;
    /**/
    P3OUT = P3OUT_INIT;
    P3DIR = P3DIR_INIT;
    /**/
    P4OUT = P4OUT_INIT;
    P4DIR = P4DIR_INIT;
    /**/
    P5OUT = P5OUT_INIT;
    P5DIR = P5DIR_INIT;
    /**/
    P6OUT = P6OUT_INIT;
    P6DIR = P6DIR_INIT;
}

/* Initialize the micro-controller clocks. Note this configures the system
 * timer in a special mode, so it must be called before StInit() (otherwise,
 * it would clobber the normal system timer settings).
 */
__inline void
BsmInitMcClocks(void)
{
    /* Init BCSCTL1 (LFXT1 = 8 MHz, no XT2, and keep default resistor setting
     * so DCO frequency = ~800 KHz). Note after this, ACLK will source LFXT1
     * (8MHz), but MCLK and SMCLK will still source the DCO (~800 KHz).
     */
    BCSCTL1 = XTS | XT2OFF | RSEL2;
    
    /* Temporarily configure the system timer so we can use it to wait ~100 us
     * in the loop that follows. Note the MSP430 manual says if a timer clock
     * is asynchronous to MCLK, and the CPU tries to read the timer while it's
     * running, it might not read the correct value. Thus, this configures the
     * system timer to use the DCO (via SMCLK) so it's synchronous to MCLK.
     * 
     * ST_CC0_COUNT:
     *     The count at which the timer will set the CCIFG interrupt flag.
     *     This is calculated for ~100 us (100 * CountPerSec/UsPerSec =
     *     100 * SMCLK_FREQ/1000000).
     * ST_CONTROL:
     *   - Clock source = SMCLK (~800 KHz from DCO after power-on/reset).
     *   - Clock input divider = 1 (ST_DIV_1).
     *   - Mode control = MC_1 (count to ST_CC0_COUNT then restart from 0).
     */
    ST_CC0_COUNT = (UD16)((100 * SMCLK_FREQ) / (UD32)1000000UL);
    ST_CONTROL = ST_SMCLK | ST_DIV_1 | MC_1;
    /*
     * Switch MCLK source to LFXT1 divided by 2 (4 MHz). This procedure is
     * described in the MPS430 manual.
     */
    while(1) {
        
        IFG1 &= ~OFIFG;
        
        /* wait ~100 us (the MSP430 manual says to wait at least 50 us) */
        ST_COUNTER = 0;
        ST_CC0_CONTROL &= ~CCIFG;
        while(!(ST_CC0_CONTROL & CCIFG));
        
        if ((IFG1 & OFIFG) == 0) break;
    }
    BCSCTL2 = SELM_3 | DIVM_1;  /* set DIVM_1 so MCLK = (8 MHz / 2) */
    
    /* stop system timer */
    ST_CONTROL = 0;
}

__inline void
BsmProc(void)
{
    BSM_MAIN_PRIV *bm = &BsmMainPriv;
    BOOL clockOverdue;
    
    /* if any tasks are pending */
    if (BsmPub.pend) {
        
        /* ADP SPI RX (set by BsmAdpSpiRxIsr()) */
        if (BsmPub.pend & BSM_ADP_SPI_RX_PEND) BsmProcAdpSpiRx();
        
        /* MICS session established (set by BsmLinkReadyIsr()) */
        if (BsmPub.pend & BSM_MICS_SESSION_PEND) BsmProcMicsSession();
        
        /* MICS data RX (set by BsmMicsRxNotEmptyIsr()) */
        if (BsmPub.pend & BSM_MICS_RX_PEND) BsmProcMicsRx();
        
        /* MICS abort (set by BsmConLostIsr()) */
        if (BsmPub.pend & BSM_MICS_ABORT_PEND) BsmAbortMics();
        
        /* MICS is starting a session using 400 MHz wakeup */
        if (BsmPub.pend & BSM_WAKE_400_PEND) BsmProcWake400();
        
        /* MICS is transmitting test data (set by BsmStartDt()) */
        if (BsmPub.pend & BSM_DT_TX_PEND) BsmProcDtTx();
        
    } else {
        /* BsmSleep(); */
    }
        
    /* if a clock period elapsed, do any periodic tasks that are pending */
    if (StElapsedMs(bm->clockTimeMs, BSM_MS_PER_CLOCK)) {
        
        /* update time at which the last clock occurred */
        bm->clockTimeMs += BSM_MS_PER_CLOCK;
        
        /* If the clock is overdue, set "clockOverdue" to tell the periodic
         * tasks. That way, if the micro-controller was busy doing something
         * for longer than BSM_MS_PER_CLOCK, the periodic tasks can avoid
         * performing tasks multiple times in rapid succession (if needed).
         */
        if (StElapsedMs(bm->clockTimeMs, BSM_MS_PER_CLOCK)) {
            clockOverdue = TRUE;
        } else {
            clockOverdue = FALSE;
        }
        
        /* perform any periodic tasks required by MICS interface */
        if (BsmPub.flags & BSM_MICS_INIT) BsmPeriodicMicsTasks(clockOverdue);
        
        /* perform any periodic tasks required by LED interface */ 
        BsmPeriodicLedTasks(clockOverdue);
    }
}

#ifdef TODO
__inline void
BsmSleep(void)
{
    /* disable interrupts so they won't set any pending flags */
    _disable_interrupts();
            
    /* If an interrupt occurred and set a pending flag before we disabled
     * interrupts, re-enable interrupts and continue processing loop.
     * Otherwise, switch the micro-controller to low power mode (stops
     * execution and enters low power mode until next interrupt).
     */
    if (BsmPub.pend) {
         _enable_interrupts();
    } else {
        _bis_SR_register(LPM4_bits | GIE);
    }
}
#endif

void
BsmConfigSpi(UINT clockDiv)
{
    UINT gie;

    /* Save current GIE setting (general interrupt enable), then disable all
     * interrupts so no ISR will try to use the local SPI bus.
     */
    gie = _get_SR_register() & GIE;
    _disable_interrupts();

    LSpiConfig(BSM_LSPI_SPECS, clockDiv);

    /* if interrupts were enabled, re-enable them */
    _bis_SR_register(gie);
}

void
BsmErr(UD16 bsmAppErrCode, const char *argFormats, ...)
{
    va_list va;

    /* init variable arguments list */
    va_start(va, argFormats);

    ErrSetVa(BsmAppErr, bsmAppErrCode, argFormats, va);

    /* clean up variable args list */
    va_end(va);
}

/* This initializes the interface for nonvolatile memory.
 */
static RSTAT
BsmNvmInit(void)
{
    const BSM_NVM_HEAD *h;

    /* get pointer to NVM header */
    h = (BSM_NVM_HEAD *)NVM_ADDR;

    /* init library used to manage nonvolatile memory */
    if (NvmInit(&h->nvmInfo, sizeof(BSM_NVM),NVM_TYPE_10X_ADK_BSM)) {
        return(-1);
    }

    /* init global pointers to factory settings for ZL7010X radio */
    MicsFactNvm = (BSM_MICS_FACT_NVM *)((UD8 *)h + h->micsFactNvmOffset);

    return(0);
}
