/**************************************************************************
 * This file contains functions to interface to the MICS chip on the base
 * station mezzanine board.
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Standard/String.h"          /* memset() */
#include "Adp/General.h"              /* UINT, RSTAT, BOOL, TRUE/FALSE, ... */
#include "Adp/Build/Build.h"          /* UD8, UD16, EXT_RSSI_ADC_INPUT, ... */
#include "Adp/AnyBoard/SysTimerLib.h" /* StDelayMs() */
#include "Adp/AnyBoard/LocalSpiLib.h" /* LSpiConfig() */
#include "Adk/AnyMezz/MicsHw.h"       /* IRQ_RADIOREADY, ZL70102, ... */
#include "Adk/AnyMezz/MicsLib.h"      /* MicsInit(), MICS_IMD_TID, ...*/
#include "Adk/AnyMezz/MicsGeneral.h"  /* MICS_LINK_CONFIG, ... */
#include "Adk/BsmMezz/BsmGeneral.h"   /* BSM_IDLE, BSM_MICS_CONFIG, ... */
#include "Adk/BsmMezz/CC25XXHw.h"     /* CC_STX, CC_PATABLE, ... */
#include "Adk/BsmMezz/CC25XXLib.h"    /* CcCommand(), CcWrite(), ... */
#include "Adk/ImMezz/ImGeneral.h"     /* IM_SEC_PER_WAKE_400_SEARCH, ... */

/* local include shared by all source files for base station application */
#include "BsmApp.h"

/**************************************************************************
 * Defines and Macros
 */
 
/* Private macro for this file to use to set the MICS state. This is needed
 * because BsmPub.micsState is declared "const" so others can't modify it.
 */
#define BSM_SET_MICS_STATE(newState) (*((UD8 *)&BsmPub.micsState) = newState)
 
/* Milliseconds between updates of the link quality statistics, and the 
 * corresponding number of clocks. For best accuracy, this should be a
 * multiple of BSM_MS_PER_CLOCK.
 */
#define BSM_MS_PER_LINK_QUAL_UPDATE  250
#define BSM_CLOCKS_PER_LINK_QUAL_UPDATE \
    (BSM_MS_PER_LINK_QUAL_UPDATE / BSM_MS_PER_CLOCK)

/* Defines for the listening windows during 400 MHz wakeup. When the base
 * is starting a session or searching for implants using 400 MHz wakeup, it
 * transmits 400 MHz headers using the minimum resend time (930 us headers with
 * a 55 us gap between them). Every BSM_MS_PER_WAKE_400_LISTEN milliseconds
 * (a little over 1 second), the base opens a window to listen for responses.
 * 
 * To listen for responses when starting a session, the base changes the
 * resend time so the ZL7010X will listen for ~1.28 ms between TX headers,
 * and then waits 6 ms to allow the ZL7010X to receive any responses (see
 * BsmProcWake400()). If a response is received, the ZL7010X asserts
 * IRQ_LINKREADY. Otherwise, the base restores the minimum resend time
 * and continues transmitting.
 * 
 * To listen for responses when searching for implants, the action the base
 * takes depends on whether the implant has a ZL70103 or ZL70101/102 (see
 * BsmProcWake400()). If the implant has a ZL70103, then while the base is
 * searching for implants, the base's ZL7010X is already configured to receive
 * responses and place their ID's in the its RX buffer, so the base just changes
 * the resend time so its ZL7010X will listen for ~9.83 ms between TX headers.
 * If the implant has a ZL70101/102, then the base stops the TX, reconfigures
 * its ZL7010X to receive responses and place their ID's in its RX buffer, and
 * changes the resend time so the ZL7010X will listen for ~9.83 ms between TX
 * headers. Next, for all implant types (ZL70101/102/103), the base waits ~25
 * ms (BSM_US_TO_LISTEN_FOR_WAKE_400_SEARCH_RESPONSES) to allow its ZL7010X
 * to receive any responses and place their ID's in its RX buffer (note that
 * during this time, the ZL7010X also transmits 400 MHz headers with PID = 1
 * every ~9.83 ms to reserve the channel). The ~25 ms allows for the following:
 * 19.658 ms because implants transmit wakeup responses at random intervals
 * between ~9.829 and ~19.658 ms; 1 ms to cover the length of a response; 4.35
 * ms (MICS_LONG_SYNTH_LOCK_TIME_US) to ensure the synthesizer locks (note that
 * this isn't required if the implant has a ZL70103, but it doesn't hurt, so
 * it's included anyway). After the ~25 ms, the base restores its ZL7010X to
 * the state it had prior to the listening window in order to continue
 * searching for implants.
 * 
 * Every 4th listening window (approximately once every 4 seconds; defined by
 * BSM_WAKE_400_LISTEN_PER_RSSI), the base also disables the resend timer (so
 * the ZL7010X will stop transmitting) and takes RSSI samples for 10 ms to
 * check if the channel is still clear (see BsmCheckWake400Rssi()). The total
 * time for every 4th listening window is therefore 16 ms (6 + 10) when
 * starting a session, and BSM_US_TO_LISTEN_FOR_WAKE_400_SEARCH_RESPONSES +
 * 10 ms when searching for implants (~35 ms). If the current channel is still
 * clear, the base continues using it. Otherwise, the base performs a CCA
 * (see BsmCca()) and resumes 400 MHz wakeup on a different channel.
 * For more information, see BsmCheckWake400Rssi().
 * 
 * While the base is doing all of the above, the implant samples (RSSI)
 * for a wakeup signal every N seconds, where N can be any integer >= 1
 * (see IM_SEC_PER_WAKE_400_SEARCH in "ZL7010XAdk\Sw\Includes\Adk\ImMezz\
 * ImGeneral.h"). To ensure the implant won't sample during the base's
 * listening window twice in a row, BSM_MS_PER_WAKE_400_SEARCH is calculated
 * so the base's listening window will shift ~50 ms between each implant sample
 * (i.e. if one sample occurs during the base's listening window, the next will
 * occur ~50 ms before the listening window). This reduces the probability the
 * sampling will miss a wakeup signal twice in a row. Note the implant still
 * might sample the 55 us gap between headers, but this will only happen ~5.6%
 * of the time (55 / (930 + 55)), and isn't likely to happen two or three times
 * in a row. To prevent this, each sample on the implant could take two RSSI's
 * separated by 55 us, but that would consume power for minimal improvement.
 */    
#define BSM_MS_PER_WAKE_400_LISTEN \
    (1000 + (((50 - 1) / IM_SEC_PER_WAKE_400_SEARCH) + 1))
#define BSM_US_TO_LISTEN_FOR_WAKE_400_SEARCH_RESPONSES \
    (MICS_LONG_SYNTH_LOCK_TIME_US + 19658 + 1000)
#define BSM_WAKE_400_LISTEN_PER_RSSI  4
 
/* normal resend time for session, 2.45 GHz wakeup, etc. */
#define BSM_RESEND_TIME  RESENDTIME_RESET

/* normal LNA gain trim for session, etc. */
#if BSM_HW == BSM100_REV_E
    #define BSM_RXRFLNAGAINTRIM  0xFF
#elif BSM_HW == BSM300
    #define BSM_RXRFLNAGAINTRIM  0xFF
#else /* for all other BSM100, BSM200  revisions */
    #define BSM_RXRFLNAGAINTRIM  0x3F
#endif

/* The setting to use for the ZL7010X's INTERFACE_MODE. This sets the ZL7010X's
 * maximum SPI clock to the smallest value that will work with the local SPI.
 */
#if BSM_LSPI_CLK_FREQ <= 1000000UL
  #define BSM_INTERFACE_MODE  MAX_SPI_CLOCK_1_MHZ
#elif BSM_LSPI_CLK_FREQ <= 2000000UL
  #define BSM_INTERFACE_MODE  MAX_SPI_CLOCK_2_MHZ
#else  
  #define BSM_INTERFACE_MODE  MAX_SPI_CLOCK_4_MHZ
#endif

/* The number of ADC samples to average together for each external RSSI in
 * order to reduce the impact of noise some, improving the accuracy of each
 * RSSI. This also checks to make sure that the specified value is small enough
 * to ensure that the sum of the samples always fits in a 16-bit number, because
 * BsmExtRssi() uses a 16-bit variable to hold the sum. For the external RSSI,
 * each ADC sample is 12 bits, so the maximum number of samples is 0xFFFF/0xFFF.
 *
 * This number was determined empirically. It is the maximum value for which
 * each RSSI (sample set) performed during the 10 ms period takes <= 50 us,
 * including the overhead. This ensures that the CCA will detect a signal
 * consisting of a 100 pulse every 10 ms (one of the requirements in the MICS
 * standard). Keeping each sample set <= 50 us ensures that the 100 us pulse
 * will be present during at least one full sample set, so the average
 * calculated for that sample set will reflect the full signal strength. With
 * BSM_NUM_SAMPS_PER_EXIT_RSSI set to 12, each sample set took 47.17 us
 * including the overhead.
 */
#define BSM_NUM_SAMPS_PER_EXT_RSSI  12
#if (BSM_NUM_SAMPS_PER_EXT_RSSI > (0xFFFF / 0xFFF))
    #error "Invalid BSM_NUM_SAMPS_PER_EXT_RSSI."
#endif

/**************************************************************************
 * Data Structures and Typedefs
 */

/* Structure for the private data for the MICS interface (used to manage the
 * ZL7010X and CC25XX).
 */
typedef struct {
    
    /* Various flags for the MICS interface. For bit definitions, see
     * BSM_START_EMERGENCY_SESSION, etc. This is declared volatile to prevent
     * the compiler from assuming it won't change during a section of code,
     * in case it is changed by interrupts.
     */
    volatile UD8 flags;
    
    /* number of clocks left until link quality statistics are updated */
    UD8 clocksToLinkQualUpdate;
    
    /* During 400 MHz wakeup, BsmProcWake400() uses this to determine when it's
     * time to take RSSI samples to check if the channel is still clear (see
     * BSM_WAKE_400_LISTEN_PER_RSSI).
     */
    UD8 rssiCountdown;
    
    /* The time at which we last listened for a 400 MHz wakeup resonse. During
     * 400 MHz wakeup, BsmProcWake400() uses this to determine when it's time
     * to listen for a response (see BSM_MS_PER_WAKE_400_LISTEN).
     */
    UD16 listenTimeMs;
    
    /* The space we know is available in the ZL7010X TX buf (see
     * BsmTransmitMics()), and the amount of data we know is available
     * in the ZL7010X MICS RX buf (see BsmReceiveMics()).
     */
    UD16 txBytesAvail;
    UD16 rxBytesAvail;
    
    /* specifications for data test (see BsmStartDt()) */
    MICS_DT_SPECS dt;

} BSM_MICS_PRIV;
/*
 * Defines for bits in BSM_MICS_PRIV.flags:
 * 
 * BSM_START_EMERGENCY_SESSION:
 *     If BsmListenForEmergency() is called with "startSession" true,
 *     it will set this flag, so if and when emergency transmissions are
 *     received, BsmProcMicsRx() will read them and start an emergency
 *     session if any match the implant ID specified in the MICS config
 *     (BsmPub.micsConfig.imdTid). If an emergency session is established,
 *     BsmProcMicsSession() will clear this flag. BsmAbortMics() will also
 *     clear this flag in case the operation is aborted before an emergency
 *     session is established.
 */
#define BSM_START_EMERGENCY_SESSION  (1 << 0)

/**************************************************************************
 * Global and Static Definitions
 */
 
/* private data for the MICS interface (used to manage ZL7010X & CC25XX) */
static BSM_MICS_PRIV BsmMicsPriv;

/**************************************************************************
 * Function Prototypes
 */
static void BsmInitMicsOnWakeup(void);
static void BsmConfigLink(const MICS_LINK_CONFIG *lc);
static void BsmSetSyncPattern(UINT chan);
static void BsmPrepWake245(void);
static void BsmPrepWake400(void);
static void BsmStartEmergencySession(void);
static BOOL BsmCheckWake400Rssi(void);
static void BsmChangeWake400Chan(void);
static void BsmProcDtRx(void);
/**/
__inline void BsmSetTxBlockSize(UINT txBlockSize);
__inline void BsmSetRxBlockSize(UINT rxBlockSize);
__inline UINT BsmRadioFailIsr(UINT irqAuxStat);
__inline void BsmRadioReadyIsr(void);
__inline void BsmLinkReadyIsr(void);
__inline void BsmLinkQualIsr(void);
__inline void BsmMicsRxNotEmptyIsr(void);
__inline void BsmHkMessRegIsr(void);
__inline void BsmHkUserDataIsr(void);
__inline void BsmHkUserStatusIsr(void);
__inline void BsmHkRemoteDoneIsr(void);
__inline void BsmCommandDoneIsr(void);
__inline UINT BsmConLostIsr(UINT irqAuxStat);
#if MICS_REV == ZL70101
__inline void BsmInitInterfaceMode(void);
#endif

/**************************************************************************
 * Initialize the ZL7010X and its interface on the base station.
 */
RSTAT
BsmMicsInit(void)
{
    UINT micsRev;
    UD16 startMs;

    /* init MICS library */
    if (MicsInit()) return(-1);

    /* Set IBS pin to wake the ZL7010X, then wait for it to assert its
     * interrupt to ensure it finished waking up (IRQ_RADIOREADY). Note if
     * the IBS pin is high after power-on/reset, the ZL7010X will already be
     * waking up at this point, or finished. If it's finished, its interrupt
     * will already be asserted.
     *
     * Note if the MCU is power-cycled or reset (via a debugger, etc.),
     * but the ZL7010X remains awake and powered, the ZL7010X won't assert
     * IRQ_RADIOREADY, in which case this will just time out and continue.
     * Also, if the ZL7010X remains asleep and powered, then when this wakes
     * it, it's possible it will assert IRQ_CRCERR instead of IRQ_RADIOREADY.
     * If either of these cases occurs, it won't be an issue, because when
     * BsmResetMics() is called later on, it will still reset the ZL7010X.
     */
    MICS_WR_IBS(TRUE);
    for(startMs = StMs();;) {
        if (MICS_INT_ASSERTED || StElapsedMs(startMs, 250)) break;
    }
    /* Read the ZL7010X revision. Note if the SPI clock is > 1 MHz, this
     * temporarily reduces it in case the SPI clock is normally 4 MHz and the
     * chip is a ZL70101 (max SPI 2 MHz after sleep or POR), or the ZL7010X's
     * INTERFACE_MODE was somehow corrupted and only the MCU was power-cycled
     * or reset (via a debugger, etc.).
     */
    if (BSM_LSPI_CLK_FREQ > 1000000UL) {
        BsmConfigSpi(BSM_LSPI_CLK_DIV_FOR_1_MHZ);
        micsRev = MicsRead(MICS_REVISION);
        BsmConfigSpi(BSM_LSPI_CLK_DIV);
    } else {
        micsRev = MicsRead(MICS_REVISION);
    }

    /* If the ZL7010X revision doesn't match the revision this software is
     * intended for (MICS_REV), fail.
     */
    if (micsRev != MICS_REV) {
        BsmErr(BSM_APP_ERR_WRONG_MICS_REV_ON_BASE, "%u", micsRev);
        return(-1);
    }

    /* Reset and initialize the ZL7010X and CC25XX in case they remained
     * powered while the MCU was power-cycled or reset (via a debugger, etc.).
     * Also put the CC25XX to sleep afterwards. Note that if the ZL7010X and
     * CC25XX were also power-cycled, it's redundant to reset them again, but
     * do so anyway to be consistent, and because it's simpler.
     */
    BsmResetMics(TRUE);
    BsmResetCc(TRUE);
    CcCommand(CC_SPWD);

    return(0);
}

/* This resets and initializes the ZL7010X. If "initMicsConfig" is true, this
 * initializes the MICS configuration (BsmPub.micsConfig) with default values
 * and configures the ZL7010X with those default values. If "initMicsConfig" is
 * false, this keeps the current MICS configuration as is and configures the
 * ZL7010X with the current values.
 *
 * Note that when this is called, the ZL7010X must be awake, and when this
 * returns, the ZL7010X is initialized and awake.
 *
 * Note that this should never be called in an ISR (to avoid race conditions).
 */
void
BsmResetMics(BOOL initMicsConfig)
{
    BSM_MICS_PRIV *bm = &BsmMicsPriv;
    BSM_MICS_CONFIG *mc = &BsmPub.micsConfig;

    /* If the SPI clock is > 1MHz, temporarily reduce it to help ensure we can
     * write to the ZL7010X to reset it (in case the SPI clock is normally 4 MHz
     * and the ZL7010X was somehow awakened without adjusting INTERFACE_MODE,
     * or INTERFACE_MODE was somehow corrupted). Note that the SPI clock is
     * restored after MicsReset() is called below.
     */
    if (BSM_LSPI_CLK_FREQ > 1000000UL) BsmConfigSpi(BSM_LSPI_CLK_DIV_FOR_1_MHZ);
    /*
     * Ensure the ZL7010X watchdog is disabled, and then disable all interrupts
     * in the ZL7010X. This ensures the ZL7010X won't go to sleep or assert
     * another interrupt while we're resetting the affected status/control data
     * and the ZL7010X. The ZL7010X won't assert another interrupt until after
     * it is reset later in this function, at which point it will re-wake and
     * assert IRQ_RADIOREADY.
     */
    MicsWrite(WDOG_CONTROL, WDOG_CLEAR_AND_DISAB);
    MicsWrite(IRQ_ENABLECLEAR1, 0xFF);
    MicsWrite(IRQ_ENABLECLEAR2, 0xFF);
    /*
     * Configure the MICS interrupt to latch on rising edge, initialize
     * the MICS interrupt flag, then enable the interrupt.
     */
    MICS_WR_INT_EDGE(INT_ON_RISING_EDGE);
    MICS_WR_INT_FLAG(FALSE);
    MICS_WR_INT_ENAB(TRUE);

    /* Ensure the session LED, TX 400 LED, and RX 400 LED are off. Note that we
     * can't just wait for the next BsmPeriodicLedTasks() to turn off the TX/RX
     * 400 LEDs, because if the TX 400 LED is constantly on (e.g. for the 400
     * MHz TX carrier, or because an HK TX operation never received a reply
     * from the implant), and/or the RX 400 LED is constantly on (e.g. for the
     * external RSSI), BsmPeriodicLedTasks() won't turn them off, so they must
     * be turned off here.
     */
    BsmSessionLedOff();
    BsmTx400LedOff();
    BsmRx400LedOff();

    /* if 2.45 GHz TX is on, turn it off (also turns off TX 245 LED) */
    if (MICS_TX_245_ENAB) BsmSetTx245Mode(BSM_TX_245_OFF);

    /* clear BsmMicsPriv and init any non-zero defaults */
    (void)memset(bm, 0, sizeof(*bm));
    /*
     * If desired, initialize the MICS configuration (BsmPub.micsConfig) with
     * default values.
     */
    if (initMicsConfig) {

        /* Clear BsmPub.micsConfig and init any non-zero defaults.
         */
        (void)memset(mc, 0, sizeof(*mc));
        /**/
        mc->flags = BSM_AUTO_CCA | BSM_RESET_MICS_ON_WAKEUP;
        mc->companyId = 1;
        mc->imdTid.b1 = 1;
        mc->micsRevOnImplant = MICS_REV;
        /**/
        mc->normalLinkConfig.txBlockSize = 14;
        mc->normalLinkConfig.rxBlockSize = 14;
        mc->normalLinkConfig.maxBlocksPerTxPack = 31;
        mc->emergencyLinkConfig = mc->normalLinkConfig;
    }

    /* clear any pending flags affected by ZL7010X reset */
    BsmPub.pend &= ~(BSM_MICS_ABORT_PEND | BSM_MICS_SESSION_PEND |
        BSM_DT_TX_PEND | BSM_WAKE_400_PEND | BSM_MICS_RX_PEND);

    /* Reset ZL7010X. Note that this sets BsmPub.micsState to BSM_SLEEPING
     * before calling MicsReset() so when the ZL7010X re-wakes and asserts
     * IRQ_RADIOREADY after the reset, BsmMicsIsr() will handle it correctly.
     */
    BSM_SET_MICS_STATE(BSM_SLEEPING);
    MicsReset();

    /* if needed, restore SPI clock to its normal rate */
    if (BSM_LSPI_CLK_FREQ > 1000000UL) BsmConfigSpi(BSM_LSPI_CLK_DIV);

    /* Clear reg_wakeup_ctrl[0] to disable the internal wakeup strobe oscillator
     * because the base station doesn't use it for anything, so it just wastes
     * power if it's enabled while the ZL7010X is sleeping. Note that the Z7010X
     * preserves reg_wakeup_ctrl while it's sleeping, so reg_wakeup_ctrl[0]
     * doesn't need to be reinitialized each time the ZL7010X is awakened.
     */
    #if MICS_REV == ZL70101
        MicsWrite(WAKEUP_CTRL, WAKEUP_CTRL_RESET_101 & ~ENAB_INT_STROBE_OSC);
    #else
        MicsWrite(WAKEUP_CTRL, WAKEUP_CTRL_RESET_102 & ~ENAB_INT_STROBE_OSC);
    #endif
    /* recalc CRC for ZXL7010X (required because reg_wakeup_ctrl was changed */
    MicsCalcCrc();

    /* do ZL7010X init that must be done each time it wakes up */
    BsmInitMicsOnWakeup();
}

/* If the ZL7010X is sleeping, this wakes it and initializes it as needed after
 * it wakes up. If the ZL7010X is already awake, this just returns without doing
 * anything. The ZL7010X remains awake until BsmMicsSleep() is called to put it
 * to sleep.
 *
 * Note that if the BSM_RESET_MICS_ON_WAKEUP flag is set in the MICS
 * configuration (BsmPub.micsConfig), this function wakes the ZL7010X, resets
 * it, and reinitializes it from its reset state. If BSM_RESET_MICS_ON_WAKEUP
 * is clear, this function just wakes the ZL7010X and initializes the ZL7010X
 * settings that must be initialized each time it wakes up (that is, settings
 * that it resets when it sleeps). Either way, this function does not change
 * the current MICS configuration on the base station. Rather, after this
 * function initializes the ZL7010X, it also restores the ZL7010X settings
 * specified in the current MICS configuration.
 *
 * Note that this should never be called in an ISR (to avoid race conditions).
 */
void
BsmMicsWakeup(void)
{
    BSM_MICS_CONFIG *mc = &BsmPub.micsConfig;

    /* if ZL7010X is already awake, return (nothing to do) */
    if (BsmPub.micsState != BSM_SLEEPING) return;

    /* wake ZL7010X */
    MicsWakeup();

    /* If desired, reset and reinitialize the ZL7010X. Otherwise, just do the
     * initialization that must be done each time it wakes up.
     */
    if (mc->flags & BSM_RESET_MICS_ON_WAKEUP) {
        BsmResetMics(FALSE);
    } else {
        BsmInitMicsOnWakeup();
    }
}

/* This initializes the ZL7010X settings that must be initialized each time it
 * wakes up (that is, settings that it resets when it sleeps).
 *
 * Note that this should never be called in an ISR (to avoid race conditions).
 */
static void
BsmInitMicsOnWakeup(void)
{
    BSM_MICS_CONFIG *mc = &BsmPub.micsConfig;
    UINT synthCtrim;

    /* If using a ZL70102/103, set INTERFACE_MODE so if the actual SPI clock is
     * <= 2 MHz, the ZL70102/103's maximum SPI clock rate will be reduced from
     * 4 MHz (the reset value) to either 2 MHz or 1 MHz as appropriate. This is
     * done to reduce the power consumed by the ZL70102/103's SPI interface a
     * little, even though the power saved is probably insignificant relative
     * to the overall power consumed by the ZL70102/103.
     *
     * Note that each time the ZL70102/103 wakes up, it restores INTERFACE_MODE
     * to the value saved in its wakeup stack, and on the ADK base station, the
     * value saved in the wakeup stack is always the reset value because the
     * ADK base station never calls MicsCopyRegs() to copy registers to the
     * ZL7010X's wakeup stack.
     *
     * Note that for a ZL70101, INTERFACE_MODE isn't initialized here because
     * when the ZL70101 wakes up and asserts IRQ_RADIOREADY, BsmMicsIsr() calls
     * BsmInitInterfaceMode() to initialize INTERFACE_MODE as needed (see
     * BsmInitInterfaceMode() for details).
     */
    if (MICS_REV >= ZL70102) MicsWrite(INTERFACE_MODE, BSM_INTERFACE_MODE);

    /* Enable MICS interrupts needed by this application and the MICS library
     * functions. Note the ZL7010X enables the following interrupts by default:
     * IRQ_RADIOFAIL, IRQ_RADIOREADY, IRQ_LINKREADY, IRQ_CLOST, IRQ_HKUSERDATA,
     * IRQ_HKUSERSTATUS, and IRQ_HKREMOTEDONE. These don't have to be re-enabled
     * here, but they are for the sake of clarity. If any of the default
     * interrupts aren't needed, they should be disabled here.
     */
    MicsWrite(IRQ_ENABLESET1, IRQ_HKUSERDATA | IRQ_HKUSERSTATUS |
        IRQ_HKREMOTEDONE | IRQ_CLOST);
    MicsWrite(IRQ_ENABLESET2, IRQ_RADIOREADY | IRQ_RADIOFAIL |
        IRQ_LINKREADY | IRQ_LINKQUALITY | IRQ_HKMESSREG | IRQ_COMMANDDONE);

    /* Disable ZL7010X watchdog timer so it won't sleep cycle the ZL7010X
     * every 5 seconds when idle. Note this should normally be re-enabled
     * whenever the ZL7010X is transmitting, so it won't occupy a MICS channel
     * more than 5 seconds if there's no session (MICS spec).
     */
    MicsWrite(WDOG_CONTROL, WDOG_CLEAR_AND_DISAB);

    /* Configure the ZL7010X's programmable outputs:
     *
     * PO0: Configure PO0 to output TX245 (on/off keying for the 2.45 GHz
     * TX wakeup signal). Note that BsmSetTx245Mode() also changes PO0 each
     * time it is called to start the 2.45 GHz TX; it changes PO0 as needed in
     * order to transmit either a 2.45 GHz wake-up signal or a 2.45 GHz carrier.
     *
     * PO1: For the BSM200 and BSM100, output the TX/RX mode (required by the
     * external LNA for 400 MHz TX/RX on the BSM200 and BSM100).
     *
     * PO2: Output RX_MODE. This is connected to a micro-controller input
     * pin (MICS_RX_MODE) so the software can determine when the ZL7010X
     * is in receive mode without having to access the ZL7010X via SPI.
     */
    MicsWrite(PO0, PO0_TX245);
    #if (BSM_MODEL == BSM200)
        MicsWrite(PO1, PO1_RX_MODE_B_102);
    #elif (BSM_MODEL == BSM100)
        MicsWrite(PO1, PO1_TX_MODE_B);
    #endif
    MicsWrite(PO2, PO2_RX_MODE);

    /* Override various ZL7010X power-on/reset defaults (to optimize, etc.).
     */
    if (BSM_RESEND_TIME != RESENDTIME_RESET) {
        MicsWrite(RESENDTIME, BSM_RESEND_TIME);
    }
    if (BSM_RXRFLNAGAINTRIM != RXRFLNAGAINTRIM_RESET) {
        MicsWrite(RXRFLNAGAINTRIM, BSM_RXRFLNAGAINTRIM);
    }
    MicsWrite(TXRFPWRDEFAULTSET, 0x0C);
    /**/
    #if ((BSM_MODEL == BSM200))

        #if MICS_REV >= ZL70102
            MicsWrite(TXRFANTTUNE_TX_MODE_102, 0x09);
            MicsWrite(RXRFANTTUNE_RX_MODE_102, 0x1B);
        #endif

    #elif (BSM_MODEL == BSM300)

        /* Values provided by P. Watson */
        MicsWrite(TXRFANTTUNE_TX_MODE_102, 0x0);
        MicsWrite(RXRFANTTUNE_RX_MODE_102, 0x0);

     #elif BSM_HW == BSM100_REV_E

        MicsWrite(XO_TRIM, 0x26);

        #if MICS_REV >= ZL70102
            MicsWrite(TXRFANTTUNE_TX_MODE_102, 0x20);
            MicsWrite(RXRFANTTUNE_RX_MODE_102, 0x06);
        #else /* ZL70101 */
            MicsWrite(TXRFANTTUNE_101, 0x20);
            MicsWrite(RXRFANTTUNE_101, 0x06);
        #endif

    #else /* all other BSM100 revisions */

        /* note XO_TRIM has no affect on BSM100 Rev B, but it's set anyway */
        MicsWrite(XO_TRIM, 0x39);

        #if MICS_REV >= ZL70102
            MicsWrite(TXRFANTTUNE_TX_MODE_102, 0x20);
            MicsWrite(RXRFANTTUNE_RX_MODE_102, 0x28);
        #else /* ZL70101 */
            MicsWrite(TXRFANTTUNE_101, 0x20);
            MicsWrite(RXRFANTTUNE_101, 0x28);
        #endif
    #endif
    /**/
    #if MICS_REV >= ZL70102

        MicsWrite(TXRF_SEL_CTRL_102, 0x1F);

    #else /* ZL70101 */

        /* Set the "long off time" for the 2.45 GHz wakeup signal to ~45 us.
         * The ZL70101 manual recommends 45 us instead of the power-on/reset
         * default of ~38.125 us. This reduces the duty cycle of the wakeup
         * signal to < 10%, which makes it compliant with FCC regulations.
         * For the "short off time" and "on time", the power-on/reset defaults
         * are used ("short off time" = ~13.125 us, and "on time" = ~2.917 us).
         */
        MicsWrite(WUTIMECOARSE_101, 0xC4);
    #endif
    /*
     * Call MicsSynthCtrim() to trim SYNTH_CTRIM and set SYNTH_CTRIM_READY = 1
     * so the ZL7010X won't repeat this trim each time the synthesizer is
     * enabled in the future. If using a ZL70102/103, also copy the resulting
     * SYNTH_CTRIM[5:3] to RAMP_CTRIM_102[5:3] for the 400 MHz TX power
     * ramping.
     */
    synthCtrim = MicsSynthCtrim();
    if (MICS_REV >= ZL70102) MicsWrite(RAMP_CTRIM_102, synthCtrim);

    /* If the NVM contains a valid value for RXIFADCDECLEV (determined by a
     * manual factory calibration procedure for the RX ADC trim), write it to
     * RXIFADCDECLEV. Otherwise, just use the existing value for RXIFADCDECLEV
     * (determined by the automated RX ADC trim that the ZL7010X performs each
     * time it wakes up). Note that the manual factory calibration is only
     * required for 4FSK, but can also be used for 2FSK and 2FSK-FB.
     */
    if (MicsFactNvm->rxIfAdcDecLev <= 0x1F) {
        MicsWrite(RXIFADCDECLEV, MicsFactNvm->rxIfAdcDecLev);
    }

    /* Set CLEAR_RETRY_COUNTER in MAXRETRIES to disable the retry counter
     * so the ZL7010X won't assert IRQ_MAXRETRIES while the base station is
     * trying to start a session or searching for implants (note that when a
     * session is established, BsmLinkReadyIsr() re-enables the retry counter
     * for that session).
     */
    MicsWrite(MAXRETRIES, MicsRead(MAXRETRIES) | CLEAR_RETRY_COUNTER);

    /* config ZL7010X (to override ZL7010X defaults affected by MICS config) */
    BsmConfigMics(mc);
}

/* This aborts any ZL7010X operation and puts the ZL7010X to sleep. The ZL7010X
 * remains asleep until BsmMicsWakeup() is called to wake it up.
 *
 * Note that this should never be called in an ISR (to avoid race conditions).
 */
void
BsmMicsSleep(void)
{
    /* if MICS chip is active, abort operation */
    if (BsmPub.micsState > BSM_IDLE) BsmAbortMics();

    /* put ZL7010X to sleep */
    MicsSleep();

    /* update state to indicate ZL7010X is sleeping */
    BSM_SET_MICS_STATE(BSM_SLEEPING);
}

/* Configure the MICS interface and MICS chip. Note if the MICS chip is
 * active when this is called, it will abort the current operation.
 */
void
BsmConfigMics(const BSM_MICS_CONFIG *newMc)
{
    BSM_MICS_CONFIG *mc = &BsmPub.micsConfig;
    BSM_MICS_PRIV *bm = &BsmMicsPriv;
    
    /* if MICS chip is active, abort operation */
    if (BsmPub.micsState > BSM_IDLE) BsmAbortMics();
    
    /* if specified different MICS config, save new MICS config */
    if (newMc != mc) *mc = *newMc;

    /* init BsmMicsPriv as appropriate when config is changed */
    bm->clocksToLinkQualUpdate = BSM_CLOCKS_PER_LINK_QUAL_UPDATE;
    
    /* tell MICS library the revision of the MICS chip on the remote implant */
    MicsRevOnImplant(mc->micsRevOnImplant);
    
    /* set company ID and implant ID */
    MicsWrite(MAC_COMPANYID, mc->companyId);
    MicsWrite(MAC_IMDTRANSID1, mc->imdTid.b1);
    MicsWrite(MAC_IMDTRANSID2, mc->imdTid.b2);
    MicsWrite(MAC_IMDTRANSID3, mc->imdTid.b3);
    
    /* configure link for normal operation by default (not emergency) */
    BsmConfigLink(&mc->normalLinkConfig);
}

/* This is called to configure the MICS link. To configure the link for
 * normal operation, pass a pointer to BsmPub.micsConfig.normalLinkConfig,
 * and to configure the link for emergency operation, pass a pointer to
 * BsmPub.micsConfig.emergencyLinkConfig. Note this should only be called
 * when the ZL7010X is idle.
 */
static void
BsmConfigLink(const MICS_LINK_CONFIG *lc)
{
    UINT prevModUser;
    
    /* set various link config registers in ZL7010X */
    MicsWrite(MAC_CHANNEL, lc->chan);
    MicsWrite(TXBUFF_MAXPACKSIZE, lc->maxBlocksPerTxPack);
    BsmSetTxBlockSize(lc->txBlockSize);
    BsmSetRxBlockSize(lc->rxBlockSize);
    
    /* Set MAC_MODUSER (for TX/RX modulation, etc.), and if the new TX
     * modulation != its previous setting, recalibrate the TX IF oscillator
     * (in case it's being changed from 2FSK to 4FSK, or vice-versa).
     */
    prevModUser = MicsRead(MAC_MODUSER);
    MicsWrite(MAC_MODUSER, lc->modUser);
    if ((lc->modUser & TX_MOD_MASK) != (prevModUser & TX_MOD_MASK)) {
        MicsCal(CAL_TX_IF_OSC);
    }
    
    /* If using a ZL70101, set reg_mac_trainword to the optimal value for the
     * specified TX modulation.
     */
    #if MICS_REV == ZL70101
        if ((lc->modUser & TX_MOD_MASK) == TX_MOD_4FSK) {
            MicsWrite(TRAINWORD_101, 0xD8);
        } else {
            MicsWrite(TRAINWORD_101, 0xAA);
        }
    #endif
    
    /* set flag to report change in link status to PC (see BSM_STAT) */
    BsmPub.statFlags |= BSM_LINK_STAT_CHANGED;
}

/* This function starts a session with an implant. It configures the ZL7010X
 * with the normal link configuration (BsmPub.micsConfig.normalLinkConfig),
 * then tells it to transmit the wakeup signal and start a session if a
 * response is received from the implant.
 * 
 * Note if using 400 MHz wakeup and either the base or the implant has a
 * ZL70101, neither side will check the company ID in the 400 MHz packets.
 * This applies regardless of which side has the ZL70101, or if they both
 * have a ZL70101. As a result, if multiple implants from different companies
 * are in range, and they use the same implant ID, they will all respond to
 * the 400 MHz wakeup and try to start a session. In this case, the implants
 * might interfere with one another, making it difficult to start a session.
 * Also, after the session stars, the base and implant should negotiate to
 * confirm the company ID.
 *
 * PARAMETERS:
 *     enabWatchdog:
 *         If true, this enables the ZL7010X watchdog timer so it will assert
 *         IRQ_WDOG if the session doesn't start within ~4.37 seconds, so the
 *         "start session" will be aborted (see BsmConLostIsr()).
 *
 *         When using 2.45 GHz wakeup, "enabWatchdog" should normally be true
 *         to ensure the wakeup won't occupy the MICS channel for more than 5
 *         seconds. In contrast, when using 400 MHz wakeup, it should normally
 *         be false because it may take longer than 4.37 seconds for the
 *         implant to wake up (BsmProcWake400() will periodically check if the
 *         channel is still clear and automatically switch if needed).
 *
 *         Note if "timeoutMs" > 0, the watchdog isn't really needed, because
 *         you can just use the timeout to control when the "start session" is
 *         aborted. If both are specified, the first to occur will abort the
 *         "start session".
 *
 *         Note if the session starts (IRQ_LINKREADY), BsmLinkReadyIsr() will
 *         enable the ZL7010X watchdog, even if it was disabled while starting
 *         the session. That way, if the link is lost for more than ~4.37
 *         seconds, the session will be aborted (see IQ_WDOG and
 *         BsmConLostIsr()).
 *     timeoutMs:
 *         If this is 0, the function will return right away without waiting
 *         for the session to start. In that case, BsmPub.micsState can be
 *         monitored to detect when the session starts or is aborted. If
 *         "timeoutMs" > 0, the function will wait for the session to start
 *         (up to the specified timeout, or until the ZL7010X watchdog asserts
 *         IRQ_WDOG, whichever occurs first).
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it sets the global
 *     error information and returns -1 (to get the error, see ErrGet(), etc.
 *     in "AppDevPlat\Sw\AnyBoard\Libs\ErrLib.c"). If a non-zero timeout is
 *     specified, a timeout is treated as an error.
 */
RSTAT
BsmStartSession(BOOL enabWatchdog, UINT timeoutMs)
{
    BSM_MICS_CONFIG *mc = &BsmPub.micsConfig;
    UINT initCom;
    UD16 startMs;
    
    /* if ZL7010X is active, abort operation */
    if (BsmPub.micsState > BSM_IDLE) BsmAbortMics();
    
    /* if configured to use 400 MHZ wakeup */
    if (mc->flags & BSM_400_MHZ_WAKEUP) {
        
        /* prepare for 400 MHz wakeup */
        BsmPrepWake400();
        
        /* Prepare initCom to tell the ZL7010X to transmit 400 MHz packets with
         * PID = 0 (to reserve the channel, wake the implant, and invite it to
         * start a session), and to start a session if the implant responds.
         */
        initCom = IBS_FLAG | START_SESSION;
        
    } else { /* else, use 2.45 GHz wakeup */
        
        /* prepare for 2.45 GHz wakeup */
        BsmPrepWake245();
        
        /* Prepare initCom to tell the ZL7010X to transmit 2.45 GHz wakeup
         * (on/off keying for 2.45 GHz TX carrier), send 400 MHz packets with
         * PID = 0 (to reserve the channel and invite the implant to start a
         * session), and start a session if the implant responds.
         */
        initCom = IBS_FLAG | START_SESSION | TX_245_GHZ_WAKEUP;

        /* if implant has a ZL70100, adjust initCom as needed */
        if (mc->micsRevOnImplant == ZL70100) {
            initCom |= (INV_TX245_ON_OFF_KEYING | ENAB_RESPONSES_WITH_WRONG_ADDR);
        }
    }
    
    /* if specified, enable ZL7010X watchdog timer (see function prolog) */
    if (enabWatchdog) MicsWrite(WDOG_CONTROL, WDOG_ENAB);
    /*
     * Update state, then tell the ZL7010X to transmit wakeup and start a
     * session if the implant responds. Note the state is updated first to
     * ensure it happens before IRQ_LINKREADY (otherwise, it would override
     * the BSM_IN_SESSION state set by BsmLinkReadyIsr()).
     */
    BSM_SET_MICS_STATE(BSM_STARTING_SESSION);
    MicsWrite(INITCOM, initCom);
    
    /* if caller wants to wait for session to start, do so */
    if (timeoutMs != 0) {
        
        for(startMs = StMs();;) {
            
            /* call BsmProcWake400() to periodically listen for response, etc */
            if (BsmPub.pend & BSM_WAKE_400_PEND) BsmProcWake400();
            
            /* if session started (ZL7010X asserted IRQ_LINKREADY), stop */
            if (BsmPub.micsState == BSM_IN_SESSION) break;
            
            /* if it's been too long, or the ZL7010X asserted IRQ_CLOST, fail */
            if (StElapsedMs(startMs, timeoutMs) ||
                (MicsPub.irqStat1Latch & IRQ_CLOST)) {
                    
                BsmErr(BSM_APP_ERR_START_SESSION_TIMEOUT, NULL);
                BsmAbortMics();
                return(-1);
            }
        }
    }
    
    /* return success */
    return(0);
}

/* This function starts searching for implants. It configures the ZL7010X
 * with the normal link configuration (BsmPub.micsConfig.normalLinkConfig),
 * then tells it to transmit the wakeup signal, receive wakeup responses, and
 * place their implant ID's in its RX buffer. Note each ID is 3 bytes, so the
 * RX block size is set to 3 during the search. Also, since each implant
 * repeatedly transmits its ID, each ID may be received numerous times.
 * When the search is stopped, the base station will restore the RX block
 * size specified in the normal link configuration.
 * 
 * Note if using 400 MHz wakeup and either the base or the implant has a
 * ZL70101, neither side will check the company ID in the 400 MHz packets.
 * This applies regardless of which side has the ZL70101, or if they both
 * have a ZL70101. As a result, if multiple implants from different companies
 * are in range, and they use the same implant ID, they will all respond to
 * the 400 MHz wakeup. In this case, if a session is subsequently started,
 * the base and implant should negotiate to confirm the company ID.
 *
 * PARAMETERS:
 *     enabWatchdog:
 *         If true, this enables the ZL7010X's watchdog timer so it will
 *         assert IRQ_WDOG after ~4.37 seconds, so the search will be aborted
 *         (see BsmConLostIsr()).
 *
 *         When using 2.45 GHz wakeup, "enabWatchdog" should normally be
 *         true to ensure the search won't occupy the MICS channel for more
 *         than 5 seconds. In contrast, when using 400 MHz wakeup, it should
 *         normally be false because it may take longer than 4.37 seconds for
 *         the implant to wake up. During 400 MHz wakeup, BsmProcWake400()
 *         will periodically check if the channel is still clear and switch
 *         if needed, so the watchdog isn't necessary.
 *     anyCompany:
 *         If using 2.45 GHz wakeup, and this is true, the company ID is set
 *         to 0 (wildcard) during the search so the search will wake up
 *         implants for any company (provided the implant ID also matches).
 *
 *         If using 400 MHz wakeup, and both the base and the implant have a
 *         ZL70102 or later, searching with company ID 0 (wildcard) is not
 *         supported, so this option should be false.
 *
 *         If using 400 MHz wakeup, and either the base or the implant has a
 *         ZL70101, the company ID will not be screened, so this option will
 *         always effectively be true (since the ZL70101 doesn't include or
 *         check the company ID in 400 MHz packets, there's no way to screen
 *         the company ID).
 *     anyImplant:
 *         If this is true, the implant ID is set to 0 (wildcard) during the
 *         search, so it will wake up any implant (provided the company ID
 *         also matches).
 *
 *         Note if using 400 MHz wakeup and the implant has a ZL70101 or
 *         ZL70102, implant ID 0 (wildcard) must always be used to search
 *         for implants, so this option must be true. This is necessary
 *         because the way 400 MHz search is implemented on a ZL70101/102
 *         implant, the implant would start a session if the base station
 *         transmitted the implant's own unique ID.
 *
 * RETURN VALUES:
 *     None.
 */
void
BsmSearchForImplant(BOOL enabWatchdog, BOOL anyCompany, BOOL anyImplant)
{
    BSM_MICS_CONFIG *mc = &BsmPub.micsConfig;
    UINT initCom;
    
    /* if ZL7010X is active, abort operation */
    if (BsmPub.micsState > BSM_IDLE) BsmAbortMics();
    
    /* if configured to use 400 MHZ wakeup */
    if (mc->flags & BSM_400_MHZ_WAKEUP) {
        
        /* Since 400 MHz searches don't support the "anyCompany" option,
         * override the "anyCompany" argument to false so this function won't
         * write MAC_COMPANYID = 0 later on. Note, however, if either the base
         * or the implant have a ZL70101, "anyCompany' will still effectively
         * be true, because the ZL70101 doesn't include or check the company ID
         * in 400 MHz packets, so the company ID won't be screened.
         */
        anyCompany = FALSE;

        /* prepare for 400 MHz wakeup */
        BsmPrepWake400();

        /* if implant has a ZL70103 or later */
        if (mc->micsRevOnImplant >= ZL70103) {

            /* Prepare initCom to tell the ZL7010X to transmit 400 MHz headers
             * with PID = 1 (to reserve the channel and wake the implant),
             * receive wakeup responses, and place their ID's in its RX buf.
             */
            initCom = IBS_FLAG | RX_WAKEUP_RESPONSES;

        } else { /* implant has a ZL70101/102 */

            /* Prepare initCom to tell the ZL7010X to transmit 400 MHz headers
             * with PID = 0 (to reserve the channel and wake the implant). Note
             * the wakeup packets must contain PID = 0, because the way 400 MHz
             * wakeup is implemented on a ZL70101/102 implant, the implant
             * won't receive 400 MHz headers with PID = 1.
             *
             * Also, override the "anyImplant" argument so it's always true.
             * This ensures the 400 MHz headers will always contain implant
             * ID 0 (wildcard), because if they contained an implant's unique
             * ID, the implant would start a session instead of just responding
             * to the search.
             */
            initCom = IBS_FLAG | START_SESSION;
            anyImplant = TRUE;
        }

    } else { /* else, use 2.45 GHz wakeup */
        
        /* prepare for 2.45 GHz wakeup */
        BsmPrepWake245();
        
        /* Prepare initCom to tell the ZL7010X to transmit 2.45 GHz wakeup
         * (on/off keying for 2.45 GHz TX carrier), receive wakeup responses,
         * and place their ID's in its RX buf. Note the ZL7010X will also
         * transmit 400 MHz packets with PID = 1 to reserve the channel.
         */
        initCom = IBS_FLAG | TX_245_GHZ_WAKEUP | RX_WAKEUP_RESPONSES;

        /* if implant has a ZL70100, adjust initCom as needed */
        if (mc->micsRevOnImplant == ZL70100) {
            initCom |= (INV_TX245_ON_OFF_KEYING | ENAB_RESPONSES_WITH_WRONG_ADDR);
        }
    }
    
    /* if specified any implant, use implant ID 0 (wildcard) */
    if (anyImplant) {

        MicsWrite(MAC_IMDTRANSID1, 0);
        MicsWrite(MAC_IMDTRANSID2, 0);
        MicsWrite(MAC_IMDTRANSID3, 0);
    }

    /* If specified "any company", use company ID 0 (wildcard). In this case,
     * if using a ZL70102/103, also set ENAB_RESPONSES_WITH_WRONG_ADDR in
     * INITCOM so the ZL70102/103 will accept responses with any company ID
     * (otherwise, it wouldn't place the responses in its RX buffer).
     */
    if (anyCompany) {

        MicsWrite(MAC_COMPANYID, 0);
        if (MICS_REV >= ZL70102) initCom |= ENAB_RESPONSES_WITH_WRONG_ADDR;
    }

    /* set RX block size to 3 (each ID received in RX buf is 3 bytes) */
    BsmSetRxBlockSize(3);
    
    /* if specified, enable ZL7010X watchdog timer (see function prolog) */
    if (enabWatchdog) MicsWrite(WDOG_CONTROL, WDOG_ENAB);
    
    /* Tell the ZL7010X to transmit the wakeup signal. If using 2.45 GHz, the
     * chip will also receive wakeup responses and place their ID's in its RX
     * buffer. If using 400 MHz wakeup, BsmProcWake400() will periodically
     * tell the ZL7010X to stop transmitting, listen for wakeup responses
     * for a short time, then resume transmitting again.
     */
    BSM_SET_MICS_STATE(BSM_SEARCHING_FOR_IMPLANT);
    MicsWrite(INITCOM, initCom);
    
    /* enable IRQ_RXNOTEMPTY (for received wakeup responses) */
    MicsWrite(IRQ_ENABLESET1, IRQ_RXNOTEMPTY);
}

/* When using 400 MHz wakeup, BsmStartSession() and BsmSearchForImplant() call
 * this function to prepare for 400 MHz wakeup.
 */
static void
BsmPrepWake400(void)
{
    BSM_MICS_CONFIG *mc = &BsmPub.micsConfig;
    BSM_MICS_PRIV *bm = &BsmMicsPriv;
    UINT chan;
    
    /* configure link for normal operation (will override if needed) */
    BsmConfigLink(&mc->normalLinkConfig);
    chan = mc->normalLinkConfig.chan;
    
    /* set flag to tell BsmProc() to keep calling BsmProcWake400() */
    BsmPub.pend |= BSM_WAKE_400_PEND;
        
    /* init listen time & RSSI countdown for BsmProcWake400() */
    bm->listenTimeMs = StMs();
    bm->rssiCountdown = BSM_WAKE_400_LISTEN_PER_RSSI;

    /* Enable the external RSSI so it's already enabled each time
     * BsmCheckWake400Rssi() calls BsmExtRssi() to do a threshold-based CCA
     * to confirm the channel is still clear. Note that this does not enable
     * the receiver in the ZL7010X, so BsmExtRssi() should only be called when
     * the receiver is also enabled (BsmCheckWake400Rssi() waits for the ZL7010X
     * to enable the receiver before calling BsmExtRssi(), and BsmCca() enables
     * the receiver before calling BsmExtRssi()).
     */
    (void)MicsEnabExtRssi(TRUE);
    /*
     * If configured to do automatic CCA, use channel returned by CCA.
     */
    if (mc->flags & BSM_AUTO_CCA) {
        chan = BsmCca(FALSE, NULL);
        MicsWrite(MAC_CHANNEL, chan);
    }
        
    /* If the base has a ZL70101, or is configured to communicate with
     * a ZL70101 implant, set the sync pattern for the channel (see
     * BsmSetSyncPattern() for more information).
     */
    if ((MICS_REV == ZL70101) || (mc->micsRevOnImplant == ZL70101)) {
        BsmSetSyncPattern(chan);
    }
    
    /* Set the minimum resend time so the ZL7010X will transmit 400 MHz packets
     * as often as possible during 400 MHz wakeup (930 us packets with a 55 us
     * gap between them).
     */
    MicsWrite(RESENDTIME, RESENDTIME_MIN);
}

/* When using 2.45 GHz wakeup, BsmStartSession() and BsmSearchForImplant() call
 * this function to prepare for 2.45 GHz wakeup.
 */
static void
BsmPrepWake245(void)
{
    BSM_MICS_CONFIG *mc = &BsmPub.micsConfig;
    UINT chan;
    
    /* configure link for normal operation (will override if needed) */
    BsmConfigLink(&mc->normalLinkConfig);
    
    /* if configured to do automatic CCA, use channel returned by CCA */
    if (mc->flags & BSM_AUTO_CCA) {
        chan = BsmCca(FALSE, NULL);
        MicsWrite(MAC_CHANNEL, chan);
    }
        
    /* prepare to transmit 2.45 GHz wakeup (also turns on 2.45 GHz TX LED) */
    BsmSetTx245Mode(BSM_TX_245_WAKE);
}

/* This function starts listening for emergency transmissions. It configures
 * the MICS chip with the emergency link configuration (BsmPub.micsConfig.
 * emergencyLinkConfig), then tells it to receive emergency transmissions and
 * place their implant ID's in its RX buffer. Note each ID is 3 bytes, so the
 * RX block size is set to 3 while listening.
 * 
 * If "startSession" is true, the base station will read any ID's that
 * are received (see BsmProcMicsRx()), and if one matches the implant ID
 * specified in the MICS configuration (BsmPub.micsConfig.imdTid), it will
 * start an emergency session with that implant.
 * 
 * Note this disables the MICS watchdog timer while listening, so the
 * base station will continue to listen until it either starts an emergency
 * session with the implant (see the "startSession" argument), or the listen
 * is stopped (see BsmAbortMics()). Listening indefinitely won't violate the
 * MICS specification because nothing is transmitted.
 * 
 * PARAMETERS:
 *     startSession: 
 *         If true, the base station will read any ID's that are received (see
 *         BsmProcMicsRx()), and if one matches the implant ID specified in the
 *         MICS configuration (BsmPub.micsConfig.imdTid), it will start an
 *         emergency session with that implant.
 *
 *         If false, the base station will leave any ID's that are received in
 *         the RX buffer on the MICS chip for the application to read.
 *
 * RETURN VALUES:
 *     None.
 */
void
BsmListenForEmergency(BOOL startSession)
{
    BSM_MICS_PRIV *bm = &BsmMicsPriv;
    BSM_MICS_CONFIG *mc = &BsmPub.micsConfig;
    UINT initCom;
    
    /* if MICS chip is active, abort operation */
    if (BsmPub.micsState > BSM_IDLE) BsmAbortMics();
    
    /* configure link for emergency operation (will override as needed) */
    BsmConfigLink(&mc->emergencyLinkConfig);
    
    /* disable MICS watchdog timer (see function prolog) */
    MicsWrite(WDOG_CONTROL, WDOG_CLEAR_AND_DISAB);
    
    /* if specified "start session", set flag for BsmProcMicsRx() */
    if (startSession) bm->flags |= BSM_START_EMERGENCY_SESSION;
    
    /* set RX block size to 3 (each ID received in RX buf is 3 bytes) */
    BsmSetRxBlockSize(3);
    
    /* Disable the resend timer so the ZL7010X won't try to transmit 400 MHz
     * packets (other than the first) while this function is listening for
     * emergency transmissions from the implant (later on). This is needed
     * because the ZL7010X shouldn't transmit anything while it's listening
     * for emergency transmissions. Also, if the ZL7010X tries to transmit
     * packets while this function is listening, it might miss some emergency
     * transmissions. Note the ZL7010X will still try to transmit one packet
     * when this function first starts listening. If using a ZL70102/103,
     * this is resolved by setting INITCOM.DISAB_400_MHZ_TX_102 when we start
     * listening, so the ZL70102/103 won't generate any RF signal. If using a
     * ZL70101, it will will actually transmit one packet when we first start
     * listening. If this is an issue, the base station can do an RSSI to check
     * if the emergency channel is already being used by something else before
     * it starts listening for emergencies (the ADK does not do this).
     */
    MicsWrite(RESENDTIME, RESENDTIME_DISAB);
    
    /* Determine INITCOM, then update the state and tell the ZL7010X to receive
     * emergency transmissions and place their implant ID's in its RX buffer.
     * Note the following about INITCOM:
     * 
     * If configured for ZL70100 implants, ENAB_RESPONSES_WITH_WRONG_ADDR is
     * set so the ZL7010X will receive emergency transmissions from ZL70100
     * implants.
     * 
     * For a ZL70102/103, DISAB_400_MHZ_TX_102 is set so the ZL70102/103 won't
     * enable its 400 MHz TX. For more information, see the comment earlier in
     * this function where RESENDTIME is set to disable the resend timer.
     */
    if (mc->micsRevOnImplant == ZL70100) {
        initCom = IBS_FLAG | RX_WAKEUP_RESPONSES | ENAB_RESPONSES_WITH_WRONG_ADDR;
    } else {
        initCom = IBS_FLAG | RX_WAKEUP_RESPONSES;
    }
    if (MICS_REV >= ZL70102) initCom |= DISAB_400_MHZ_TX_102;
    /**/
    BSM_SET_MICS_STATE(BSM_LISTENING_FOR_EMERGENCY);
    MicsWrite(INITCOM, initCom);
    
    /* enable IRQ_RXNOTEMPTY (for received emergency transmissions) */
    MicsWrite(IRQ_ENABLESET1, IRQ_RXNOTEMPTY);
}

/* This function starts a ZL7010X data test. If a session is already active,
 * the test will use the current session. Otherwise, it will start a new one.
 * 
 * PARAMETERS:
 *     specs:
 *         The data test specifications (for description, see MICS_DT_SPECS in
 *         "ZL7010XAdk\Sw\Includes\Adk\AnyMezz\MicsGeneral.h").
 * 
 * RETURN VALUES:
 *     None.
 */
void
BsmStartDt(const MICS_DT_SPECS *specs)
{
    BSM_MICS_PRIV *bm = &BsmMicsPriv;
    
    /* If not already in session, start one. Note BsmStartSession() is called
     * before setting up the test because it might call BsmAbortMics(), which
     * would clear the test setup.
     */
    if (BsmPub.micsState != BSM_IN_SESSION) BsmStartSession(FALSE, 0);
    
    /* save data test specs */
    bm->dt = *specs;
    
    /* If MICS_DT_TX is specified, set BSM_DT_TX_PEND to tell BsmProc() to
     * call BsmProcDtTx() as often as possible (to transmit data).
     */
    if (specs->options & MICS_DT_TX) BsmPub.pend |= BSM_DT_TX_PEND;
}

/* Abort any MICS operation. Note that this should never be called in an ISR
 * (to avoid race conditions).
 */
void
BsmAbortMics(void)
{
    BSM_MICS_PRIV *bm = &BsmMicsPriv;
    BSM_MICS_CONFIG *mc = &BsmPub.micsConfig;
    MICS_LINK_CONFIG *nlc = &BsmPub.micsConfig.normalLinkConfig;
    MICS_LINK_CONFIG *elc = &BsmPub.micsConfig.emergencyLinkConfig;
    
    /* If the ZL7010X is sleeping, just return since we can't read/write
     * the ZL7010X registers, and there's no operation to abort anyway.
     */
    if (BsmPub.micsState == BSM_SLEEPING) return;

    /* abort current ZL7010X operation (if any) */
    switch(BsmPub.micsState) {
    case BSM_SEARCHING_FOR_IMPLANT: {
        
        /* abort search and flush RX buf on local MICS chip */
        MicsAbort(MICS_FLUSH_RX);
        
        /* restore normal RX block size (this is set to 3 during the search) */
        BsmSetRxBlockSize(nlc->rxBlockSize);
        
        /* restore company & implant ID's (in case they were set to wildcard) */
        MicsWrite(MAC_COMPANYID, mc->companyId);
        MicsWrite(MAC_IMDTRANSID1, mc->imdTid.b1);
        MicsWrite(MAC_IMDTRANSID2, mc->imdTid.b2);
        MicsWrite(MAC_IMDTRANSID3, mc->imdTid.b3);
        
        break;
        
    } case BSM_STARTING_SESSION: {
        
        /* abort MICS operation and flush TX & RX bufs on local MICS chip */
        MicsAbort(MICS_FLUSH);
        break;
        
    } case BSM_LISTENING_FOR_EMERGENCY: {
        
        /* Abort "listen for emergency". Specify MICS_FLUSH_RX to flush any
         * ID's remaining in the ZL7010X RX buffer, and MICS_SET_MIN_RESEND
         * because the resend timer is disabled while listening for emergencies
         * and the ZL7010X won't execute abort with the resend timer disabled.
         */
        MicsAbort(MICS_FLUSH_RX | MICS_SET_MIN_RESEND);
        
        /* restore emergency RX block size (this is set to 3 while listening) */
        BsmSetRxBlockSize(elc->rxBlockSize);
        
        /* restore normal resend time (it's disabled while listening) */
        MicsWrite(RESENDTIME, BSM_RESEND_TIME);
        
        break;
        
    } case BSM_IN_SESSION: {
        
        /* Use housekeeping to set the ABORT_LINK bit in the MAC_CTRL register
         * on the remote implant. This will cause the MICS chip on the remote
         * implant to assert IRQ_HKABORTLINK, so it can go to sleep sooner if
         * desired (instead of waiting for the MICS watchdog timer). Note this
         * will only work if housekeeping writes are enabled on the implant,
         * which could be a security concern. It�s not a big issue if the abort
         * link doesn�t work, because the watchdog on the implant�s ZL7010X
         * will still time out ~4.37 seconds after the base station ends the
         * session - the abort link just tells the implant to go back to sleep
         * sooner. If desired, this can always be done at the application
         * level - the base station can just tell the implant to go back to
         * sleep before the session is ended. Also note the following:
         * 
         * - The implant doesn't send any reply for a housekeeping abort link,
         *   so there's no need to wait for one.
         * 
         * - The implant doesn't send a housekeeping abort link to the base
         *   station for the following reasons: the base station usually ends
         *   a session, not the implant; there's usually no need to tell the
         *   base station to abort the link sooner to save power; the base
         *   station isn't likely to have housekeeping write access enabled;
         *   waiting 12 ms on the implant would waste power.
         */ 
        MicsWrite(HK_TXDATA, ABORT_LINK);
        #if MICS_REV == ZL70101
            /* workaround for ZL70101 issue */
            (void)MicsWriteHkTxAddr(MAC_CTRL, 50);
        #else
            MicsWrite(HK_TXADDR, MAC_CTRL);
        #endif
        
        /* wait to ensure the HK abort link is transmitted at least once */
        StDelayMs(12);
        
        /* abort session and flush TX & RX bufs on local MICS chip */
        MicsAbort(MICS_FLUSH);
        
        /* Turn off the session LED, and ensure the TX/RX 400 LEDs are off.
         * Note that we can't just wait for the next BsmPeriodicLedTasks()
         * to turn off the TX 400 LED, because if it's constantly on (e.g.
         * because an HK TX operation never received a reply from the implant),
         * BsmPeriodicLedTasks() won't turn it off. For the RX 400 LED,
         * BsmPeriodicLedTasks() would turn it off, but we turn it off now
         * so it always goes off at the same time as the TX 400 LED.
         */
        BsmSessionLedOff();
        BsmTx400LedOff();
        BsmRx400LedOff();

        break;
        
    } case BSM_IDLE: default: {
        
        /* don't call MicsAbort() (it times out if ZL7010X is idle) */
        break;
    }}
    
    /* if configured to use 400 MHZ wakeup (instead of 2.45 GHz) */
    if (mc->flags & BSM_400_MHZ_WAKEUP) {
        
        /* If 400 MHz wakeup was active, clear BSM_WAKE_400_PEND flag (so
         * BsmProc() will stop calling BsmProcWake400()), disable external
         * RSSI, and restore normal resend time.
         */
        if (BsmPub.pend & BSM_WAKE_400_PEND) {
            
            BsmPub.pend &= ~BSM_WAKE_400_PEND;
            (void)MicsEnabExtRssi(FALSE);
            MicsWrite(RESENDTIME, BSM_RESEND_TIME);
        }
        
        /* If the base has a ZL70101, or is configured to communicate with
         * a ZL70101 implant, restore the default sync pattern in case it was
         * changed for an odd channel (see BsmSetSyncPattern() for more).
         */
        if ((MICS_REV == ZL70101) || (mc->micsRevOnImplant == ZL70101)) {
            BsmSetSyncPattern(0);
        }
    }
    
    /* if 2.45 GHz TX is on, turn it off (also turns off TX 245 LED) */
    if (MICS_TX_245_ENAB) BsmSetTx245Mode(BSM_TX_245_OFF);
    
    /* clear other flags affected by abort (in case they're set) */
    bm->flags &= ~BSM_START_EMERGENCY_SESSION;
    
    /* clear data test option flags (in case data test was active) */
    bm->dt.options = 0;
    
    /* If the ZL7010X was active, call BsmUpdateLinkQual() to update the
     * link quality statistics one last time for the operation that was
     * active, and to clear the ZL7010X's error counters, IRQ_MAXRETRIES,
     * etc. so they'll be clear when the next operation is started.
     *
     * Also, set CLEAR_RETRY_COUNTER in MAXRETRIES to disable the retry
     * counter so the ZL7010X won't assert IRQ_MAXRETRIES the next time the
     * base station is starting a session or searching for implants (note
     * that the next time a session is established, BsmLinkReadyIsr() will
     * re-enable the retry counter for that session).
     */
    if (BsmPub.micsState > BSM_IDLE) {
        BsmUpdateLinkQual();
        MicsWrite(MAXRETRIES, MicsRead(MAXRETRIES) | CLEAR_RETRY_COUNTER);
    }
    
    /* Make sure the MICS watchdog timer is disabled and the
     * BSM_MICS_ABORT_PEND flag is clear (so BsmProc() won't keep calling
     * BsmAbortMics(). Note the watchdog is disabled first to ensure IRQ_WDOG
     * won't set BSM_MICS_ABORT_PEND again after it's cleared (see
     * BsmConLostIsr()). Also clear any other pending flags that should never
     * be set when the chip is idle (to be safe).
     */
    MicsWrite(WDOG_CONTROL, WDOG_CLEAR_AND_DISAB);
    BsmPub.pend &= ~(BSM_MICS_ABORT_PEND | BSM_MICS_SESSION_PEND |
        BSM_DT_TX_PEND | BSM_WAKE_400_PEND);
    
    /* set flag to report any change in link status to PC (see BSM_STAT) */
    BsmPub.statFlags |= BSM_LINK_STAT_CHANGED;

    /* update state to indicate ZL7010X is idle */
    BSM_SET_MICS_STATE(BSM_IDLE);
}

/* This resets and initializes the CC25XX. If "initCcConfig" is true, this
 * initializes the CC25XX configuration (BsmPub.ccConfig) with default values
 * and configures the CC25XX with those default values. If "initCcConfig" is
 * false, this keeps the current CC25XX configuration as is and configures the
 * CC25XX with the current values.
 *
 * Note that the CC25XX can either be awake or sleeping when this is called,
 * and when this returns, the CC25XX is initialized and awake.
 *
 * Note that this should never be called in an ISR (to avoid race conditions).
 */
void
BsmResetCc(BOOL initCcConfig)
{
    BSM_CC_CONFIG *cc = &BsmPub.ccConfig;

    /* If desired, initialize the CC25XX configuration (BsmPub.ccConfig) with
     * default values.
     */
    if (initCcConfig) {

        /* Clear BsmPub.ccConfig and init any non-zero defaults.
         */
        (void)memset(cc, 0, sizeof(*cc));
        /*
         * Initialize the 2.45 GHz TX frequency to 2450 MHz. Note that the
         * BSM100 values are different because it uses a 26 MHz crystal
         * frequency for the CC25XX, whereas the BSM200 and later use a
         * 24 MHz crystal frequency for the CC25XX.
         */
        #if (BSM_MODEL >= BSM200)
            cc->ccFreq0 = 0x55;
            cc->ccFreq1 = 0x15;
            cc->ccFreq2 = 0x66;
        #else /* BSM100 */
            cc->ccFreq0 = 0x13;
            cc->ccFreq1 = 0x3B;
            cc->ccFreq2 = 0x5E;
        #endif
        /*
         * Initialize the 2.45 GHz TX power to the default value. For the BSM300,
         * the default gives ~20 dBm output at the SMA connector. For the BSM200
         * and BSM100, the default gives an effective radiated power (ERP) of
         * ~20 dBm at the base station's antenna.
         */
        #if (BSM_MODEL >= BSM300)
            cc->ccPa = 0xBF;
        #else /* BSM200 & BSM100 */
            cc->ccPa = 0x7F;
        #endif
    }

    /* reset CC25XX (also wakes CC25XX if it's sleeping) */
    CcCommand(CC_SRES);

    /* Initialize CC25XX. Note that the POR default values are used for the
     * registers that are not initialized here, including the following:
     *
     * CC_FREND0 = 0x10: Configures CC25XX to use index 0 in PATABLE to transmit
     * both a 0 and a 1. The CC25XX doesn't need to alternate anything for the
     * on/off keying (OOK) because the ZL7010X drives the on/off keying via its
     * PO0 output.
     *
     * CC_CHANNR = 0: Configures CC25XX to use base frequency (channel 0).
     */

    /* OOK modulation; disable manchester; no preamble/sync word */
    CcWrite(CC_MDMCFG2, 0x30);

    /* modulator deviation = 0 (no modulation) */
    CcWrite(CC_DEVIATN, 0x00);

    /* Set FS_AUTOCAL to 1 so the CC25XX auto-calibrates when going from idle
     * to TX, and set PO_TIMEOUT to 2 to ensure that each time the CC25XX is
     * awakened, the digital supply voltage has time to stabilize before the
     * CC25XX drives CHIP_RDYn low (on SO pin).
     */
    CcWrite(CC_MCSM0, 0x18);

    /* whitening off; async serial mode; no CRC; infinite packet length */
    CcWrite(CC_PKTCTRL0, 0x32);

    /* Configure CC25XX to drive its GDO0 output high. By default, GDO0 outputs
     * a clock, so it's set high instead to help prevent digital noise.
     */
    CcWrite(CC_IOCFG0, 0x6F);

    /* set 2.45 GHz TX frequency */
    CcWrite(CC_FREQ0, cc->ccFreq0);
    CcWrite(CC_FREQ1, cc->ccFreq1);
    CcWrite(CC_FREQ2, cc->ccFreq2);

    /* set 2.45 GHz TX power level */
    CcWrite(CC_PATABLE, cc->ccPa);
}

/* This sets the frequency for the 2.45 GHz TX (for 2.45 GHz wakeup). The
 * "ccFreq2", "ccFreq1", and "ccFreq0" arguments are the values to write to the
 * CC25XX's frequency control registers. Note that if the 2.45 GHz TX is enabled
 * when this is called, this stops the CC25XX TX before changing the frequency,
 * and then restarts it afterwards.
 */
void
BsmSetTx245Freq(UD8 ccFreq2, UD8 ccFreq1, UD8 ccFreq0)
{
    BSM_CC_CONFIG *cc = &BsmPub.ccConfig;

    /* Make sure the CC25XX is idle before changing the frequency (also wakes
     * the CC25XX if it's sleeping).
     */
    CcCommand(CC_SIDLE);
    
    /* set the CC25XX frequency registers */
    CcWrite(CC_FREQ0, ccFreq0);
    CcWrite(CC_FREQ1, ccFreq1);
    CcWrite(CC_FREQ2, ccFreq2);
    
    /* save the new frequency settings for BsmResetCc() to reference */
    cc->ccFreq0 = ccFreq0;
    cc->ccFreq1 = ccFreq1;
    cc->ccFreq2 = ccFreq2;

    /* if 2.45 GHz TX was on, restart TX; else put CC25XX back to sleep */
    if (MICS_TX_245_ENAB) {
        CcCommand(CC_STX);
    } else {
        CcCommand(CC_SPWD);
    }
}

/* This sets the power level for the 2.45 GHz TX (for 2.45 GHz wakeup). The
 * "ccPa" argument is the value to write to the CC25XX's PA table.
 */
void
BsmSetTx245Power(UD8 ccPa)
{
    BSM_CC_CONFIG *cc = &BsmPub.ccConfig;

    /* set 1st entry in PA table (also wakes CC25XX if it's sleeping) */
    CcWrite(CC_PATABLE, ccPa);

    /* save the new power setting for BsmResetCc() to reference */
    cc->ccPa = ccPa;

    /* if 2.45 GHz TX is off, put CC25XX back to sleep */
    if (!MICS_TX_245_ENAB) CcCommand(CC_SPWD);
}

/* Set the 2.45 GHz TX mode (see BSM_TX_245_WAKE, etc.). Note if the mode is
 * BSM_TX_245_WAKE, the transmission won't actually start until the ZL7010X is
 * configured to transmit 2.45 GHz wakeup packets (reg_mac_initcom[0] = 1) by
 * BsmStartSession() or BsmSearchForImplant().
 */
void
BsmSetTx245Mode(UINT mode)
{
    BSM_MICS_CONFIG *mc  = &BsmPub.micsConfig;
    
    /* if desired, enable 2.45 GHz TX */
    if (mode) {

        /* If BSM_RESET_MICS_ON_WAKEUP is specified in the MICS configuration,
         * or the base station is a BSM200 or BSM100, reset and reinitialize
         * the CC25XX (also wakes CC25XX). This is always done for the BSM200
         * and BSM100 because they have a CC2550, and the CC2550 does not
         * preserve its register settings while sleeping, so it must be
         * reinitialized each time it is awakened.
         */
        if ((mc->flags & BSM_RESET_MICS_ON_WAKEUP) || (BSM_MODEL <= BSM200)) {
            BsmResetCc(FALSE);
        }
        /* If the base station is a BSM200 or BSM100 (CC2550), initialize TEST1
         * and TEST2 with the values provided by TI's SmartRF tool. This is done
         * because for the CC2550, the SmartRF tool recommends TEST1 and TEST2
         * values that are different than their reset values, and the CC2550
         * does not preserve the TEST registers while sleeping, so they need to
         * be set each time the CC2550 is awakened. Note that TI doesn't provide
         * any explanation for the recommended TEST1 and TEST2 values, so it's
         * not known if they really need to be set, but do so anyway to be safe.
         *
         * For the BSM300 (CC2500), the CC2500 also does not preserve the TEST
         * registers while sleeping, but for the CC2500, the SmartRF tool just
         * recommends the reset values for all TEST registers, so there's no
         * need to set them each time the CC2500 is awakened.
         */
        #if (BSM_MODEL <= BSM200)
            CcWrite(CC_TEST1, 0x21);
            CcWrite(CC_TEST2, 0x00);
        #endif

        /* start 2.45 GHz TX carrier (also wakes CC25XX if it's sleeping) */
        CcCommand(CC_STX);

        /* enable 2.45 GHz TX */
        MICS_WR_TX_245_ENAB(TRUE);

        /* turn on 2.45 GHz TX LED */
        BsmTx245LedOn();

        /* if desired, prepare to transmit 2.45 GHz wakeup signal */
        if (mode == BSM_TX_245_WAKE) {

            /* ensure PO0 outputs the on/off keying for 2.45 GHz TX wake-up */
            MicsWrite(PO0, PO0_TX245);
            
        } else { /* mode must be BSM_TX_245_CARRIER (TX continuous carrier) */

            /* Set GPO_0 to 1 and configure PO0 to output it. PO0 is used for
             * the 2.45 GHz TX on/off keying, so outputting 1 keeps the 2.45
             * GHz TX carrier on.
             */
            MicsWriteBit(GPO, GPO_0, 1);
            MicsWrite(PO0, PO0_GPO_0);
        }
            
    } else { /* else, ensure 2.45 GHz TX is off */
        
        /* disable 2.45 GHz TX */
	    MICS_WR_TX_245_ENAB(FALSE);
            
        /* turn off 2.45 GHz TX carrier, and then put CC25XX to sleep */
        CcCommand(CC_SIDLE);
        CcCommand(CC_SPWD);
            
        /* turn off 2.45 GHz TX LED */
        BsmTx245LedOff();

        /* Ensure PO0 is configured to output TX245 (in case it was configured
         * to output 1 to transmit a 2.45 GHz carrier). This ensures PO0
         * remains low while the 2.45 GHz TX is off.
         */
        MicsWrite(PO0, PO0_TX245);
    }
}

/* If "start" is true, this starts transmitting the 400 MHz TX carrier
 * (continuous wave). Otherwise, it stops the carrier. Note this is a test
 * mode, not a normal operational mode. The carrier should only be started
 * when the MICS chip is idle, and should be stopped before starting any
 * other MICS operation (so it won't affect the operation, or vice-versa).
 */
void
BsmStartTx400Carrier(BOOL start, UINT chan)
{
    /* if MICS chip is active, abort operation */
    if (BsmPub.micsState > BSM_IDLE) BsmAbortMics();
    
    /* if specified, start transmitting 400 MHz TX carrier (continuous wave) */
    if (start) {
        
        /* set channel & flag to report change to PC (see BSM_STAT) */
        MicsWrite(MAC_CHANNEL, chan);
        BsmPub.statFlags |= BSM_LINK_STAT_CHANGED;
        
        /* if using ZL70102/103 */
        if (MicsRead(MICS_REVISION) >= ZL70102) {
        
            /* start 400 MHz TX carrier (continuous wave) */
            if ((MicsRead(MAC_MODUSER) & TX_MOD_MASK) == TX_MOD_4FSK) {
                MicsWrite(TXIFFREQ_102, TX_IF_FREQ_OUT_SEL_102 | 0x08);
            } else {
                MicsWrite(TXIFFREQ_102, TX_IF_FREQ_OUT_SEL_102 | 0x0C);
            }
            MicsWrite(RF_GENENABLES, TX_IF | TX_RF | SYNTH);
            
        } else { /* ZL70101 */
        
            /* start 400 MHz TX carrier (continuous wave) */
            MicsWrite(TXIFCTRL, TX_IF_FREQ_OUT_SEL_101 | TX_IF_BLOCK_ENAB);
            /**/
            if ((MicsRead(MAC_MODUSER) & TX_MOD_MASK) == TX_MOD_4FSK) {
                MicsWrite(TXIFFREQ_101, 0x08);
            } else {
                MicsWrite(TXIFFREQ_101, 0x0C);
            }
            MicsWrite(RF_GENENABLES, TX_IF | TX_RF | SYNTH);
        }
        
        /* turn on 400 MHz TX LED & tell BsmPeriodicLedTasks() to leave it on */
        BsmTx400LedOn(BSM_NO_AUTO_OFF);
        
    } else {
        
        /* if using ZL70102/103 */
        if (MicsRead(MICS_REVISION) >= ZL70102) {
        
            /* stop 400 MHz TX carrier */
            MicsWrite(RF_GENENABLES, 0);
            MicsWrite(TXIFFREQ_102, 0);
            
        } else { /* ZL70101 */
            
            /* stop 400 MHz TX carrier */
            MicsWrite(TXIFCTRL, 0);
            MicsWrite(RF_GENENABLES, 0);
        }
        
        /* turn off 400 MHz TX LED */
        BsmTx400LedOff();
    }
}

/* This performs an external RSSI. This samples the RX signal level for 10 ms
 * because the MICS standard requires any device using a channel to transmit
 * every 10 ms. Note that if needed, this automatically enables the external
 * RSSI and the ZL7010X's receiver, and restores their previous settings
 * when done.
 *
 * PARAMETERS:
 *     average:
 *         If true, get the average RSSI over a 10 ms period. Otherwise, get the
 *         maximum RSSI detected over a 10 ms period.
 *
 * RETURN VALUES:
 *     If "average" is true, this function returns the average RSSI over 10 ms.
 *     Otherwise, it returns the maximum RSSI detected over 10 ms. The returned
 *     value is always >= 0.
 */
UINT
BsmExtRssi(BOOL average)
{
    UINT n, rssi, adcCount;
    BOOL prevExtRssiEnab, enabledRx;
    UD16 adcSum16, maxAdcSum16, startCount;
    UD32 adcSum32, remCount;

    /* init variables referenced during cleanup */
    enabledRx = FALSE;

    /* ensure external RSSI is enabled */
    prevExtRssiEnab = MicsEnabExtRssi(TRUE);

    /* If the ZL7010X's receiver is off, enable synth & RX, wait to give synth
     * time to coarse tune and lock, and set flag for cleanup logic later in
     * this function.
     */
    if (!MICS_RX_MODE) {

        MicsWrite(RF_GENENABLES, RX_RF | RX_IF | SYNTH);
        StDelayUs(MICS_LONG_SYNTH_LOCK_TIME_US);
        enabledRx = TRUE;
    }

    /* if caller wants the average of all samples taken */
    if (average) {

        /* take samples for 10 ms and sum them all */
        adcSum32 = adcCount = 0;
        remCount = StUsToCount(10000);
        BsmStartAdc(EXT_RSSI_ADC_INPUT);
        for(startCount = StCount();;) {

            adcSum32 += BsmGetAdc();
            ++adcCount;

            /* if it's been long enough, stop */
            if (StElapsedCount(&startCount, &remCount)) break;
        }

        /* calc average */
        rssi = adcSum32 / adcCount;

    } else {

        /* take sets of samples for 10 ms */
        adcSum16 = maxAdcSum16 = 0;
        n = BSM_NUM_SAMPS_PER_EXT_RSSI;
        remCount = StUsToCount(10000);
        _disable_interrupts();
        BsmStartAutoAdc(EXT_RSSI_ADC_INPUT);
        for(startCount = StCount();;) {

            /* take a set of samples and sum them */
            while(n) {adcSum16 += BsmGetAutoAdc(); --n;}

            /* if the sum is the max so far, save the new max */
            if (adcSum16 > maxAdcSum16) maxAdcSum16 = adcSum16;

            /* prepare for next set of samples */
            adcSum16 = 0;
            n = BSM_NUM_SAMPS_PER_EXT_RSSI;

            /* if it's been long enough, stop */
            if (StElapsedCount(&startCount, &remCount)) break;
        }
        _enable_interrupts();

        /* calc average for the maximum sum that was detected */
        rssi = maxAdcSum16 / BSM_NUM_SAMPS_PER_EXT_RSSI;
    }
    BsmStopAdc();

    /* if we enabled the ZL7010X's receiver, disable it */
    if (enabledRx) MicsWrite(RF_GENENABLES, 0);

    /* restore the previous enable setting for the external RSSI */
    (void)MicsEnabExtRssi(prevExtRssiEnab);

    /* return RSSI */
    return(rssi);
}

/* This performs an internal RSSI. This samples the RX signal level for 10 ms
 * and returns the average RSSI. Note that if needed, this automatically enables
 * the ZL7010X's internal RSSI, receiver, and internal ADC, and restores their
 * previous settings when done.
 *
 * RETURN VALUES:
 *     Returns the average RSSI over 10 ms. The returned value is always >= 0.
 */
UINT
BsmIntRssi(void)
{
    UINT adcSum, adcCount, aveRssi, prevRxGenCtrl;
    BOOL prevAdcEnab, enabledRx;
    UD16 startMs;

    /* init variables referenced during cleanup */
    enabledRx = FALSE;

    /* ensure the ZL7010X's internal RSSI is enabled */
    prevRxGenCtrl = MicsRead(RXGENCTRL);
    MicsWrite(RXGENCTRL, prevRxGenCtrl | RX_INT_RSSI);

    /* If the ZL7010X's receiver is off, enable synth & RX, wait to give synth
     * time to coarse tune and lock, and set flag for cleanup logic later in
     * this function.
     */
    if (!MICS_RX_MODE) {

        MicsWrite(RF_GENENABLES, RX_RF | RX_IF | SYNTH);
        StDelayUs(MICS_LONG_SYNTH_LOCK_TIME_US);
        enabledRx = TRUE;
    }

    /* ensure the ZL7010X's internal ADC is enabled */
    prevAdcEnab = MicsEnabAdc(TRUE);

    /* take samples for 10 ms and sum them all */
    adcSum = adcCount = 0;
    for(startMs = StMs();;) {

        adcSum += MicsAdc(ADC_INPUT_400_MHZ_RX_RSSI);
        ++adcCount;

        /* if it's been long enough, stop */
        if (StElapsedMs(startMs, 10)) break;
    }

    /* calc average */
    aveRssi = adcSum / adcCount;

    /* restore the previous enable setting for the ZL7010X's internal ADC */
    (void)MicsEnabAdc(prevAdcEnab);

    /* if we enabled the ZL7010X's receiver, disable it */
    if (enabledRx) MicsWrite(RF_GENENABLES, 0);

    /* restore the previous enable setting for the ZL7010X's internal RSSI */
    MicsWrite(RXGENCTRL, prevRxGenCtrl);

    /* return average RSSI */
    return(aveRssi);
}

/* Do a clear channel assessment (CCA). Note that this will abort any MICS
 * operation that is active so it can change the channel (MAC_CHANNEL), and
 * it doesn't restore the previous channel when it's done. Thus, after this
 * returns, the caller should set the channel as needed (to change it or
 * restore it to its previous value).
 *
 * PARAMETERS:
 *     average:
 *         If true, the CCA uses the average RSSI over a 10 ms period.
 *         Otherwise, it uses the maximum RSSI detected over a 10 ms period.
 *     data:
 *         Buffer in which to return the CCA data (NULL for none). BSM_CCA_DATA
 *         is defined in "ZL7010XAdk\Sw\Includes\Adk\BsmMezz\BsmGeneral.h".
 *
 * RETURN VALUES:
 *     This function returns the clearest channel (0 to 9).
 */
UINT
BsmCca(BOOL average, BSM_CCA_DATA *retData)
{
    BOOL prevExtRssiEnab;
    UINT minRssi = 0xFFFF;
    UINT clearChan = 0;
    UINT chan, rssi;

    /* if MICS chip is active, abort operation */
    if (BsmPub.micsState > BSM_IDLE) BsmAbortMics();

    /* Ensure the external RSSI is enabled. This is enabled beforehand so
     * BsmExtRssi() doesn't have to enable it for each RSSI.
     */
    prevExtRssiEnab = MicsEnabExtRssi(TRUE);

    /* for each MICS channel */
    for(chan = 0; chan <= MAX_MICS_CHAN; ++chan) {

        /* Disable synthesizer & RX, set channel, re-enable synthesizer & RX,
         * then wait to give the synthesizer time to restart. This ensures the
         * synthesizer will coarse tune and lock for every channel (so the
         * RSSI's will be accurate for every channel).
         */
        MicsWrite(RF_GENENABLES, 0);
        MicsWrite(MAC_CHANNEL, chan);
        MicsWrite(RF_GENENABLES, RX_RF | RX_IF | SYNTH);
        StDelayUs(MICS_LONG_SYNTH_LOCK_TIME_US);

        /* get max RSSI detected over 10 ms, or average RSSI over 10 ms */
        rssi = BsmExtRssi(average);

        /* if desired, save the RSSI in the return data specified by caller */
        if (retData != NULL) retData->rssi[chan] = rssi;

        /* if the RSSI for this channel is the lowest so far, note it */
        if (rssi < minRssi) {
            minRssi = rssi;
            clearChan = chan;
        }
    }

    /* disable synthesizer & RX */
    MicsWrite(RF_GENENABLES, 0);

    /* restore the previous enable setting for the external RSSI */
    (void)MicsEnabExtRssi(prevExtRssiEnab);

    /* return the clearest channel that was found */
    return(clearChan);
}

/* Process the start of a session. When a session starts (IRQ_LINKREADY),
 * BsmLinkReadyIsr() sets the BSM_MICS_SESSION_PEND flag to tell BsmProc() to
 * call this function. By default, this function doesn't do anything when a
 * session starts. Rather, the base station just reports the state change to
 * the application on the PC (see BSM_STAT). If desired, this function can be
 * customized to communicate with the implant. Note if the session was started
 * in response to an emergency transmission, BSM_START_EMERGENCY_SESSION will
 * be set in BsmMicsPriv.flags (see BsmListenForEmergency()). If desired, this
 * flag can be referenced to determine if it's an emergency session.
 */
void
BsmProcMicsSession(void)
{
    BSM_MICS_PRIV *bm = &BsmMicsPriv;
    BSM_MICS_CONFIG *mc = &BsmPub.micsConfig;
    int retVal;
    
    /* clear flag so BsmProc() won't keep calling this function */
    BsmPub.pend &= ~BSM_MICS_SESSION_PEND;
    
    /* if revision of ZL7010X on implant is wrong, set error */
    retVal = MicsReadR(MICS_REVISION, TRUE, 250);
    if ((retVal >= 0) && (retVal != mc->micsRevOnImplant)) {

        BsmErr(BSM_APP_ERR_WRONG_MICS_REV_ON_IMPLANT, "%u\f%u",
            retVal, mc->micsRevOnImplant);
    }
    
    /* Clear this flag in case ImListenForEmergency() set it. Note before
     * clearing this flag, it can be referenced if desired to determine if
     * the session was started in response to an emergency transmission.
     */
    bm->flags &= ~BSM_START_EMERGENCY_SESSION;
}

/* Process received data. When the MICS chip receives data (IRQ_RXNOTEMPTY),
 * BsmMicsRxNotEmptyIsr() sets the BSM_MICS_RX_PEND flag to tell BsmProc() to
 * call this function.
 * 
 * If BsmListenForEmergency() is called with "startSession" true, it
 * sets BSM_START_EMERGENCY_SESSION in BsmMicsPriv.flags. In that case,
 * if emergency transmissions are received, this function will read the
 * implant ID's from the MICS RX buffer and start an emergency session if
 * any match the expected ID (BsmPub.micsConfig.imdTid). Otherwise, this
 * function just leaves the data in the MICS RX buffer for the PC to read.
 * If desired, this function can be customized to read and process the data.
 * It can reference BsmPub.micsState to determine if the data consists of
 * wakeup responses (BSM_SEARCHING_FOR_IMPLANT), emergency transmissions
 * (BSM_LISTENING_FOR_EMERGENCY), or session data (BSM_IN_SESSION). Note
 * if this function reads the data from the MICS RX buffer, it should
 * re-enable IRQ_RXNOTEMPTY before returning, so if more data is received,
 * IRQ_RXNOTEMPTY will recur and set BSM_MICS_RX_PEND again.
 */
void
BsmProcMicsRx(void)
{
    BSM_MICS_CONFIG *mc = &BsmPub.micsConfig;
    BSM_MICS_PRIV *bm = &BsmMicsPriv;
    BOOL enabRxInt = FALSE;
    MICS_IMD_TID imdTid;
    
    /* if data test RX is active */
    if (bm->dt.options & MICS_DT_RX) {
        
        /* indicate we want to re-enable IRQ_RXNOTEMPTY when done */
        enabRxInt = TRUE;
        
        /* process received data */ 
        BsmProcDtRx();
    }
    
    /* If BsmListenForEmergency() was called with "startSession" true, read
     * the implant ID's from the RX buffer on the MICS chip, and if any match
     * the expected ID (BsmPub.micsConfig.imdTid), start an emergency session
     * with the implant.
     */
    if (bm->flags & BSM_START_EMERGENCY_SESSION) {
        
        /* indicate we want to re-enable IRQ_RXNOTEMPTY when done */
        enabRxInt = TRUE;
        
        /* read & process each ID in the MICS RX buf */
        while(BsmReceiveMics(&imdTid, sizeof(imdTid), 0) != 0) {
            
            /* if ID matches expected ID, start emergency session & stop */
            if ((imdTid.b1 == mc->imdTid.b1) &&
                (imdTid.b2 == mc->imdTid.b2) &&
                (imdTid.b3 == mc->imdTid.b3)) {
                    
                BsmStartEmergencySession();
                break;
            }
        }
    }
    
    /* Clear BSM_MICS_RX_PEND so BsmProc() won't keep calling this function,
     * then re-enable IRQ_RXNOTEMPTY if needed. Note if IRQ_RXNOTEMPTY was
     * already re-enabled (by BsmReceiveMics()), and it occurs before we clear
     * BSM_MICS_RX_PEND here, it will recur when we re-enable it here, so it
     * will still set BSM_MICS_RX_PEND again.
     */
    BsmPub.pend &= ~BSM_MICS_RX_PEND;
    if (enabRxInt) {
        /* clear IRQ_RXNOTEMPTY so it won't recur if RX buf is now empty */
        MicsWrite(IRQ_RAWSTATUS1, ~IRQ_RXNOTEMPTY);
        MicsWrite(IRQ_ENABLESET1, IRQ_RXNOTEMPTY);
    }
}

/* When the data test RX is active, BsmProcMicsRx() calls this to process the
 * received data. This reads the data contained in the ZL7010X's RX buffer,
 * and validates it if desired.
 */
static void
BsmProcDtRx(void)
{
    BSM_MICS_PRIV *bm = &BsmMicsPriv;
    UINT rxBlockSize = BsmPub.rxBlockSize;
    UD8 buf[RXBUFF_BSIZE_MAX];
    UINT i, n, nMax, dataInc;
    UD8 data;
    
    /* get data we expect the next block to start with */
    data = bm->dt.rxData;
    
    /* get value to increment the expected data by (0 or 1) */
    dataInc = (bm->dt.options & MICS_DT_INC_RX) ? 1 : 0;
    
    /* Init number of blocks received this time (n) and the max (nMax). The max
     * ensures we won't remain in BsmProcDtRx() too long if it can't keep up
     * with the RX data, so the implant can still report status to the PC, etc.
     */
    n = 0; nMax = RXBUFF_USED_MAX;
    
    while(1) {
        
        /* if no data block is available (ZL7010X RX buf empty), stop */
        if (BsmReceiveMics(buf, rxBlockSize, 0) == 0) break;
        
        /* if we want to validate the received data, do so */ 
        if (bm->dt.options & MICS_DT_VALIDATE) {

            /* if RX block size = max, check bit 0 of first byte */
            if (rxBlockSize == RXBUFF_BSIZE_MAX) {
                if ((buf[0] ^ data) & 1) ++BsmPub.dataStat.dataErrCount;
                data += dataInc;
                i = 1;
            } else {
                i = 0;
            }
            for(; i < rxBlockSize; ++i) {
                if (buf[i] != data) ++BsmPub.dataStat.dataErrCount;
                data += dataInc;
            }
        }
        
        /* if we've received the max number of blocks, stop */
        if (++n >= nMax) break;
    }
    
    /* save the data we expect the next block to start with */
    bm->dt.rxData = data;
    
    /* update data status and set flag to report change to PC (see BSM_STAT) */
    BsmPub.dataStat.rxBlockCount += n;
    BsmPub.statFlags |= BSM_DATA_STAT_CHANGED;
}

/* When the data test TX is active, BsmProc() calls this as often as possible
 * to transmit data. This checks if there is any space available in the
 * ZL7010X's TX buffer, and if so, writes data to it.
 */
void
BsmProcDtTx(void)
{
    BSM_MICS_PRIV *bm = &BsmMicsPriv;
    UINT txBlockSize = BsmPub.txBlockSize;
    UD8 buf[TXBUFF_BSIZE_MAX];
    UINT i, n, nMax;
    UD8 data;
    
    /* init data to transmit in next block */
    data = bm->dt.txData;
    if (bm->dt.options & MICS_DT_INC_TX) {
        for(i = 0; i < txBlockSize; ++i) buf[i] = data++;
    } else {
        for(i = 0; i < txBlockSize; ++i) buf[i] = data;
    }
    
    /* Init number of blocks transmitted this time (n) and the max (nMax). If
     * bm->dt.txBlockCount is 0 (transmit until stopped) or > TXBUFF_USED_MAX,
     * set nMax = TXBUFF_USED_MAX. This ensures we won't remain in BsmProcDtTx()
     * too long if it can't keep up with the ZL7010X TX, so the implant can
     * still report status to the PC, etc.
     */
    n = 0; 
    nMax = bm->dt.txBlockCount;
    if ((nMax == 0) || (nMax > TXBUFF_USED_MAX)) nMax = TXBUFF_USED_MAX;
    
    while(1) {
        
        /* if ZL7010X TX buf is full, save data to start next block & stop */
        if (BsmTransmitMics(buf, txBlockSize, 0) == 0) {
            bm->dt.txData = buf[0];
            break;
        }
        
        /* if max transmitted, save data to start next block & stop */
        if (++n >= nMax) {
            bm->dt.txData = data;
            break;
        }
        
        /* if incrementing data, update data for next block */
        if (bm->dt.options & MICS_DT_INC_TX) {
            for(i = 0; i < txBlockSize; ++i) buf[i] = data++;
        }
    }
    
    /* if no more data was written to TX buf, skip remaining logic */
    if (n == 0) return;
    
    /* if we're not transmitting forever, update blocks left to transmit */
    if (bm->dt.txBlockCount) {
        
        /* if all blocks have been transmitted, stop data test TX */
        if ((bm->dt.txBlockCount -= n) == 0) BsmPub.pend &= ~BSM_DT_TX_PEND;
    }
    
    /* update data status and set flag to report change to PC (see BSM_STAT) */
    BsmPub.dataStat.txBlockCount += n;
    BsmPub.statFlags |= BSM_DATA_STAT_CHANGED;
}

/* This function writes data to the ZL7010X's TX buffer, which will transmit
 * the data as soon as possible. If there's not enough space in the TX buffer,
 * this function will loop writing the data as space becomes available. If the
 * specified timeout expires or the ZL7010X asserts IRQ_CLOST before all of the
 * data has been written, it stops and returns the number of bytes written.
 * 
 * Note the specified number of bytes must be a multiple of the TX block
 * size (TXBUFF_BSIZE setting). Otherwise, it has not been determined how the
 * ZL7010X will behave.
 * 
 * Note for this function to work properly, MicsWriteTxBuf() must not be
 * called directly (only through this function), and BsmSetTxBlockSize() must
 * be called to change TXBUFF_BSIZE (instead of calling MicsWrite() directly).
 * Otherwise, this function won't track the space available in the TX buffer
 * correctly, and won't use the correct TX block size.
 * 
 * PARAMETERS:
 *     data:
 *         The data to transmit.
 *     nBytes:
 *         The number of data bytes. Note this must be a multiple of the TX
 *         block size (TXBUFF_BSIZE setting). Otherwise, it has not been
 *         determined how the ZL7010X will behave.
 *     timeoutMs:
 *         The maximum time to wait for space in the ZL7010X's TX buffer. If the
 *         specified timeout expires or IRQ_WDOG occurs before all of the data
 *         has been written to the TX buffer, this function will stop and
 *         return the number of bytes that were written. If the timeout is 0,
 *         it will write as much data as the TX buffer currently has space for
 *         and return immediately.
 *
 * RETURN VALUES:
 *     This function returns the number of bytes written to the ZL7010X TX buf.
 */
UINT
BsmTransmitMics(const void *data, UINT nBytes, UINT timeoutMs)
{
    BSM_MICS_PRIV *bm = &BsmMicsPriv;
    UINT nLeft, nAvail;
    UD16 startMs;
    
    /* init bytes left to write, bytes available in TX buf, & start time */
    if ((nLeft = nBytes) == 0) return(0);
    nAvail = bm->txBytesAvail;
    startMs = StMs();
    
    /* Turn on the 400 MHz TX LED & tell BsmPeriodicLedTasks() to turn it off
     * automatically when the ZL7010X's TX buffer becomes empty. BsmProc()
     * periodically calls BsmPeriodicLedTasks() to update the LED's.
     */
    BsmTx400LedOn(BSM_AUTO_OFF);
    
    /* until the specified number of bytes have been written to TX buf */
    while(1) {
        
        /* if we've used all the bytes we know were available in the TX buf */
        if (nAvail == 0) {
                
            /* wait for more space in the TX buf */
            while((nAvail = (TXBUFF_USED_MAX - MicsRead(TXBUFF_USED))) == 0) {
                
                /* If it's been too long, or the ZL7010X asserted IRQ_CLOST,
                 * return the number of bytes written to the TX buf.
                 */
                if ((timeoutMs == 0) || StElapsedMs(startMs, timeoutMs) ||
                    (MicsPub.irqStat1Latch & IRQ_CLOST)) {
                    
                    bm->txBytesAvail = 0;
                    return(nBytes - nLeft);
                }
                
                /* Wait ~1 ms to allow the ZL7010X to transmit some blocks from
                 * its TX buf. The TX buf holds 64 blocks, and the ZL7010X will
                 * take at least ~194 us to transmit each block (155 bits /
                 * 800000 bps). Thus, there's no need to recheck TXBUFF_USED
                 * more than once a millisecond, and the extra SPI bus activity
                 * would just waste power.
                 */
                StDelayMs(1);
            }
            /* convert blocks available to bytes available */
            nAvail *= BsmPub.txBlockSize;
        }
        
        /* if bytes left <= bytes available, write bytes left and return */
        if (nLeft <= nAvail) {
            
            MicsWriteTxBuf(data, nLeft);
            bm->txBytesAvail = nAvail - nLeft;
            return(nBytes);
            
        } else {
        
            /* write to available space, then update data pointer & counts */
            MicsWriteTxBuf(data, nAvail);
            data = (UD8 *)data + nAvail;
            nLeft -= nAvail;
            nAvail = 0;
        }
    }
}

/* This function reads data from the ZL7010X's RX buffer. If there's not enough
 * data available, this function will loop reading data as it becomes available.
 * If the specified timeout expires or the ZL7010X asserts IRQ_CLOST before all
 * of the requested data has been read, it stops and returns the number of bytes
 * read. In that case, it also clears IRQ_RXNOTEMPTY in MicsPub.irqStat1Latch
 * and re-enables the interrupt.
 * 
 * Note the specified number of bytes must be a multiple of the RX block
 * size (RXBUFF_BSIZE setting). Otherwise, it has not been determined how the
 * ZL7010X will behave.
 * 
 * Note for this function to work properly, MicsReadRxBuf() must not be
 * called directly (only through this function), and BsmSetRxBlockSize() must
 * be called to change RXBUFF_BSIZE (instead of calling MicsWrite() directly).
 * Otherwise, this function won't track the number of bytes available in the
 * RX buffer correctly, and won't use the correct RX block size.
 * 
 * PARAMETERS:
 *     buf:
 *         The buffer to put the received data in.
 *     nBytes:
 *         The number of bytes to receive. Note this must be a multiple of the
 *         RX block size (RXBUFF_BSIZE setting). Otherwise, it has not been
 *         determined how the ZL7010X will behave. It should also be <= the
 *         size of the specified buffer.
 *     timeoutMs:
 *         The maximum time to wait for data in the ZL7010X's RX buffer. If
 *         the specified timeout expires or IRQ_WDOG occurs before all of the
 *         requested data has been read, this function will stop and return the
 *         number of bytes read (if the timeout is 0, it will read any data in
 *         the RX buffer and return immediately). In that case, it also clears
 *         IRQ_RXNOTEMPTY in MicsPub.irqStat1Latch and re-enables the interrupt.
 *
 * RETURN VALUES:
 *     This function returns the number of bytes read from the ZL7010X RX buf.
 */
UINT
BsmReceiveMics(void *buf, UINT nBytes, UINT timeoutMs)
{
    BSM_MICS_PRIV *bm = &BsmMicsPriv;
    UINT nLeft, nAvail;
    UD16 startMs;
    
    /* init bytes left to read, bytes available in RX buf, & start time */
    if ((nLeft = nBytes) == 0) return(0);
    nAvail = bm->rxBytesAvail;
    startMs = StMs();
    
    /* until the specified number of bytes have been read from the RX buf */
    while(1) {
        
        /* if we've read all the data we know was available in the RX buf */
        if (nAvail == 0) {
            
            /* wait for more data in the RX buf */
            while((nAvail = MicsRead(RXBUFF_USED)) == 0) {
                
                /* Clear IRQ_RXNOTEMPTY in irqStat1Latch, re-enable it,
                 * then wait for it to occur. Note if IRQ_RXNOTEMPTY is
                 * already enabled at this point, and it occurs before we
                 * clear irqStat1Latch.IRQ_RXNOTEMPTY here, it will recur
                 * when we re-enable it here, so it will still set
                 * irqStat1Latch.IRQ_RXNOTEMPTY again.
                 */
                MicsPub.irqStat1Latch &= ~IRQ_RXNOTEMPTY;
                MicsWrite(IRQ_ENABLESET1, IRQ_RXNOTEMPTY);
                /**/
                while((MicsPub.irqStat1Latch & IRQ_RXNOTEMPTY) == 0) {
            
                    /* If it's been too long, or the ZL7010X asserted IRQ_CLOST,
                     * return the number of bytes read from the RX buf.
                     */
                    if ((timeoutMs == 0) || StElapsedMs(startMs, timeoutMs) ||
                        (MicsPub.irqStat1Latch & IRQ_CLOST)) {
                    
                        bm->rxBytesAvail = 0;
                        return(nBytes - nLeft);
                    }
                }
            }
            /* convert blocks available to bytes available */
            nAvail *= BsmPub.rxBlockSize;
        }
        
        /* if bytes left <= bytes available, read bytes left and return */
        if (nLeft <= nAvail) {
            
            MicsReadRxBuf(buf, nLeft);
            bm->rxBytesAvail = nAvail - nLeft;
            return(nBytes);
            
        } else {
        
            /* read available bytes, then update buf pointer & counts */
            MicsReadRxBuf(buf, nAvail);
            buf = (UD8 *)buf + nAvail;
            nLeft -= nAvail;
            nAvail = 0;
        }
    }
}

/* If the expected implant ID is received while listening for emergency
 * transmissions, BsmProcMicsRx() calls this to abort the "listen for
 * emergency" and start an emergency session with the implant.
 */
static void
BsmStartEmergencySession(void)
{
    MICS_LINK_CONFIG *elc = &BsmPub.micsConfig.emergencyLinkConfig;
    BSM_MICS_CONFIG *mc = &BsmPub.micsConfig;
    UINT initCom;
    
    /* Abort "listen for emergency". Specify MICS_FLUSH_RX to flush any
     * ID's remaining in the ZL7010X RX buffer, and MICS_SET_MIN_RESEND
     * because the resend timer is disabled while listening for emergencies
     * and the ZL7010X won't execute abort with the resend timer disabled.
     */
    MicsAbort(MICS_FLUSH_RX | MICS_SET_MIN_RESEND);
    
    /* restore RX block size and resend time for emergency session */
    BsmSetRxBlockSize(elc->rxBlockSize);
    MicsWrite(RESENDTIME, BSM_RESEND_TIME);
                
    /* Enable the ZL7010X watchdog timer so the ZL7010X will assert IRQ_WDOG if
     * the session doesn't start within ~4.37 seconds (see BsmConLostIsr()).
     * This ensures the chip won't occupy the channel more than 5 seconds if no
     * session is established (MICS specification).
     */
    MicsWrite(WDOG_CONTROL, WDOG_ENAB);
                
    /* Update state, then tell the ZL7010X to transmit 400 MHz packets with
     * PID = 0 (to reserve the channel and invite the implant to start a
     * session), and to start session if implant responds. Note the state is
     * updated first to ensure it happens before IRQ_LINKREADY (otherwise, it
     * would override the BSM_IN_SESSION state set by BsmLinkReadyIsr()).
     */
    if (mc->micsRevOnImplant == ZL70100) {
        initCom = IBS_FLAG | START_SESSION | ENAB_RESPONSES_WITH_WRONG_ADDR;
    } else {
        initCom = IBS_FLAG | START_SESSION;
    }
    BSM_SET_MICS_STATE(BSM_STARTING_SESSION);
    MicsWrite(INITCOM, initCom);
    
    /* set flag to report new RX block size to PC (see BSM_STAT) */
    BsmPub.statFlags |= BSM_LINK_STAT_CHANGED;
}

/* While using 400 MHz wakeup to start a session or search for implants, the
 * base calls this function as often as possible to check if it's time to
 * listen for responses, etc. (BsmStartSession() and BsmSearchForImplant()
 * set BSM_WAKE_400_PEND in "BsmPub.pend" so BsmProc() will keep calling this
 * function as often as possible).
 */
void
BsmProcWake400(void)
{
    BSM_MICS_CONFIG *mc = &BsmPub.micsConfig;
    BSM_MICS_PRIV *bm = &BsmMicsPriv;
    UINT prevRxUsed;
    UD16 startCount;
    UD32 remCount;
    
    /* if it's not time to listen for a 400 MHz wakeup response, return */
    if (!StElapsedMs(bm->listenTimeMs, BSM_MS_PER_WAKE_400_LISTEN)) return;
    bm->listenTimeMs += BSM_MS_PER_WAKE_400_LISTEN;
    
    /* Listen for 400 MHz wakeup responses (the logic to do so depends on
     * whether we're starting a session or searching for implants).
     */
    if (BsmPub.micsState == BSM_STARTING_SESSION) {

        /* Set the resend time to ~1.28 ms, wait for the ZL7010X to finish
         * transmitting any current packet, then wait 6 ms for a response. The
         * ZL7010X will continue transmitting packets (930 us each), but will
         * wait ~1.28 ms between them to receive any response. Waiting 6 ms
         * ensures the base station will transmit 3 packets and listen for
         * ~1.28 ms after each. For more information on the timing between
         * the base and implant during 400 MHz wakeup, see
         * BSM_MS_PER_WAKE_400_LISTEN earlier in this file.
         */
        MicsWrite(RESENDTIME, RESENDTIME_US(1000));
        while(!MICS_RX_MODE);
        for(startCount = StCount(), remCount = StUsToCount(6000);;) {
            
            /* if session started (received response), stop waiting */
            if (BsmPub.micsState == BSM_IN_SESSION) break;
            
            /* if it's been long enough, stop to continue 400 MHz wakeup */
            if (StElapsedCount(&startCount, &remCount)) break;
        }
        
        /* If configured to do auto-CCA's, and it's time to do RSSI's to check
         * if the channel is still clear, do so.
         */
        if ((mc->flags & BSM_AUTO_CCA) && (--bm->rssiCountdown == 0)) {
            
            /* restart countdown to next RSSI check */
            bm->rssiCountdown = BSM_WAKE_400_LISTEN_PER_RSSI;
                
            /* If the channel is no longer clear, and the session didn't start
             * while listening for responses or doing RSSI's (no response
             * received), abort and change the channel, then resume the 400 MHz
             * TX. For more information, see BsmCheckWake400Rssi(). Note that
             * BsmCheckWake400Rssi() disables the resend timer, but the
             * subsequent logic always re-enables it.
             */
            if (BsmCheckWake400Rssi() && (BsmPub.micsState != BSM_IN_SESSION)) {
                
                /* Specify MICS_SET_MIN_RESEND because BsmCheckWake400Rssi()
                 * disables the resend timer, and the ZL7010X won't execute
                 * abort with the resend timer disabled.
                 */
                MicsAbort(MICS_SET_MIN_RESEND);
                BsmChangeWake400Chan();
                MicsWrite(INITCOM, IBS_FLAG | START_SESSION);
            }
        }
        
        /* If session started (received response & asserted IRQ_LINKREADY),
         * end 400 MHz wakeup and return. This clears the BSM_WAKE_400_PEND
         * flag (so BsmProc() will stop calling BsmProcWake400()), restores
         * the normal resend time, and disables the external RSSI.
         */
        if (BsmPub.micsState == BSM_IN_SESSION) {
            
            BsmPub.pend &= ~BSM_WAKE_400_PEND;
            MicsWrite(RESENDTIME, BSM_RESEND_TIME);
            (void)MicsEnabExtRssi(FALSE);
            return;
        }
        
        /* restore minimum resend time to resume 400 MHz TX */
        MicsWrite(RESENDTIME, RESENDTIME_MIN);
        
    /* else, if searching for ZL70103 (or later) implant(s) */
    } else if (mc->micsRevOnImplant >= ZL70103) {

        /* get the previous number of implant ID's in the ZL7010X's RX buf */
        prevRxUsed = MicsRead(RXBUFF_USED);

        /* Set the resend time to its normal value, then wait ~25 ms
         * to allow the ZL7010X to listen for wakeup responses (see
         * BSM_US_TO_LISTEN_FOR_WAKE_400_SEARCH_RESPONSES). For more
         * information on the timing between the base and implant during 400
         * MHz wakeup, see BSM_MS_PER_WAKE_400_LISTEN earlier in this file.
         */
        MicsWrite(RESENDTIME, BSM_RESEND_TIME);
        StDelayUs(BSM_US_TO_LISTEN_FOR_WAKE_400_SEARCH_RESPONSES);

        /* If configured to do auto-CCA's, and it's time to do RSSI's to check
         * if the channel is still clear, do so.
         */
        if ((mc->flags & BSM_AUTO_CCA) && (--bm->rssiCountdown == 0)) {

            /* restart countdown to next RSSI check */
            bm->rssiCountdown = BSM_WAKE_400_LISTEN_PER_RSSI;

            /* If the channel is no longer clear, and no more responses were
             * received while listening for responses or doing RSSI's, abort
             * and change the channel, then resume the 400 MHz TX. For
             * more information, see BsmCheckWake400Rssi(). Note that
             * BsmCheckWake400Rssi() disables the resend timer, but
             * the subsequent logic always re-enables it.
             */
            if (BsmCheckWake400Rssi() && (MicsRead(RXBUFF_USED) == prevRxUsed)) {

                /* Specify MICS_SET_MIN_RESEND because BsmCheckWake400Rssi()
                 * disables the resend timer, and the ZL7010X won't execute
                 * the abort if the resend timer is disabled.
                 */
                MicsAbort(MICS_SET_MIN_RESEND);
                BsmChangeWake400Chan();
                MicsWrite(INITCOM, IBS_FLAG | RX_WAKEUP_RESPONSES);
            }
        }

        /* restore minimum resend time to resume 400 MHz TX */
        MicsWrite(RESENDTIME, RESENDTIME_MIN);

    } else { /* else, must be searching for ZL70101 or ZL70102 implant(s) */

        /* get the previous number of implant ID's in the ZL7010X's RX buf */
        prevRxUsed = MicsRead(RXBUFF_USED);
        
        /* Stop transmitting 400 MHz wakeup, set the resend time to its normal
         * value, then tell the ZL7010X to listen for wakeup responses and place
         * their ID's in its RX buf (note the ZL7010X will also transmit 400
         * MHz packets with PID = 1 to reserve the channel). Continue listening
         * for ~25 ms (see BSM_US_TO_LISTEN_FOR_WAKE_400_SEARCH_RESPONSES).
         * For more information on the timing between the base and implant
         * during 400 MHz wakeup, see BSM_MS_PER_WAKE_400_LISTEN earlier in
         * this file.
         * 
         * Note when this logic is entered, the resend time is already set to
         * the minimum (to transmit wakeup packets). Thus, MicsAbort() will be
         * as fast as possible. 
         */
        MicsAbort(0);
        MicsWrite(RESENDTIME, BSM_RESEND_TIME);
        MicsWrite(INITCOM, IBS_FLAG | RX_WAKEUP_RESPONSES);
        StDelayUs(BSM_US_TO_LISTEN_FOR_WAKE_400_SEARCH_RESPONSES);
        
        /* If configured to do auto-CCA's, and it's time to do RSSI's to check
         * if the channel is still clear, do so.
         */
        if ((mc->flags & BSM_AUTO_CCA) && (--bm->rssiCountdown == 0)) {
            
            /* restart countdown to next RSSI check */
            bm->rssiCountdown = BSM_WAKE_400_LISTEN_PER_RSSI;
                
            /* If the channel is no longer clear, and no more responses
             * were received while listening for responses or doing RSSI's,
             * abort and change the channel. For more information, see
             * BsmCheckWake400Rssi(). Note that BsmCheckWake400Rssi() is
             * called before we stop listening so we don't have to restart the
             * synthesizer and wait for it again. Also, BsmCheckWake400Rssi()
             * disables the resend timer, but the subsequent logic always
             * re-enables it.
             */
            if (BsmCheckWake400Rssi() && (MicsRead(RXBUFF_USED) == prevRxUsed)) {
                
                /* Specify MICS_SET_MIN_RESEND because BsmCheckWake400Rssi()
                 * disables the resend timer, and the ZL7010X won't execute
                 * the abort if the resend timer is disabled.
                 */
                MicsAbort(MICS_SET_MIN_RESEND);
                BsmChangeWake400Chan();
                
            } else {
                /* stop listening for responses */
                MicsAbort(MICS_SET_MIN_RESEND);
            }
        } else {
            /* Stop listening for responses. Specify MICS_SET_MIN_RESEND to
             * restore the minimum resend time so it's ready to resume 400 MHz
             * wakeup. Also, if using a ZL70101, MICS_SET_MIN_RESEND will make
             * the ZL70101 execute the abort sooner, because the ZL70101 waits
             * for the resend timer to expire before it executes an abort.
             */
            MicsAbort(MICS_SET_MIN_RESEND);
        }
        
        /* resume 400 MHz TX (note MicsAbort() set min resend time) */
        MicsWrite(INITCOM, IBS_FLAG | START_SESSION);
    }

    /* Update "listenTimeMs" to next multiple of BSM_MS_PER_WAKE_400_LISTEN
     * that is greater than the current time. That way, if the micro-controller
     * was busy doing something for longer than BSM_MS_PER_WAKE_400_LISTEN,
     * BsmProcWake400() won't listen for a 400 MHz wakeup response multiple
     * times in rapid succession. Using a multiple of BSM_MS_PER_WAKE_400_LISTEN
     * ensures the timing won't shift between the base station and the implant,
     * so the implant will never sample the base station's RX window two times
     * in a row.
     */
    while(StElapsedMs(bm->listenTimeMs, BSM_MS_PER_WAKE_400_LISTEN)) {
        bm->listenTimeMs += BSM_MS_PER_WAKE_400_LISTEN;
    }
}

/* When the base is transmitting a 400 MHz wakeup signal on a channel, it calls
 * this function every 4 seconds to stop transmitting and do a threshold-based
 * CCA for 10 ms to confirm the channel is still clear (see BsmProcWake400()).
 * If the channel is still clear, this function returns FALSE, and otherwise,
 * it returns TRUE. Note that this function disables the resend timer, so it
 * must be re-enabled afterwards if needed.
 *
 * Note that in practice, since the base is already using the channel to
 * transmit the 400 MHz wakeup signal, the channel should appear busy to anyone
 * else who performs a CCA, so no one else should try to use the channel for
 * anything else. Thus, each time the base calls this function, it should find
 * that the channel is still clear, and therefore, the base should never have
 * to switch to a different channel. The only reason the base does this every
 * 4 seconds is to comply with the MICS standard, which says if you don't
 * establish communication within 5 seconds, you must stop using the channel
 * and do a CCA. Since this function stops using the channel for 10 ms and does
 * a threshold-based CCA during that time, it complies with the MICS standard.
 */
static BOOL
BsmCheckWake400Rssi(void)
{
    UD16 rssiForLbtThresh;
    UINT wake400Chan;

    /* Disable the resend timer so the ZL7010X will stop transmitting packets
     * while we're taking RSSI samples (see BsmExtRssi()). Note that during this
     * time, the base station won't transmit anything for a little more than
     * 10 ms, so afterwards, the channel will no longer be reserved. This isn't
     * an issue because if the RSSI shows the channel is still clear, the base
     * station can continue using it.
     */
    MicsWrite(RESENDTIME, RESENDTIME_DISAB);

    /* get the channel we're using for 400 MHz wakeup */
    wake400Chan = MicsRead(MAC_CHANNEL);
        
    /* wait for ZL7010X to finish transmitting any current packet */
    while(!MICS_RX_MODE);
        
    /* Perform an RSSI, and if it exceeds the RSSI for the LBT threshold power
     * level, return TRUE (channel no longer clear). For more information,
     * see BSM_MICS_FACT_NVM.rssiToDbmCal in "[SourceTree]\ZL7010XAdk\Sw\
     * Includes\Adk\AnyMezz\MicsGeneral.h".
     */
    rssiForLbtThresh = GET_UD16(&MicsFactNvm->rssiToDbmCal[wake400Chan]);
    if (BsmExtRssi(FALSE) > rssiForLbtThresh) return(TRUE);
    
    /* return FALSE (channel is still clear) */
    return(FALSE);
}

/* If BsmProcWake400() detects the channel being used for 400 MHz wakeup is
 * no longer clear, it calls this function to perform another CCA and change
 * the channel. Note this should only be called when the MICS chip is idle.
 */
static void
BsmChangeWake400Chan(void)
{
    BSM_MICS_CONFIG *mc = &BsmPub.micsConfig;
    UINT chan;
    
    /* do CCA and set channel */
    chan = BsmCca(FALSE, NULL);
    MicsWrite(MAC_CHANNEL, chan);
    
    /* If the base has a ZL70101, or is configured to communicate with
     * a ZL70101 implant, set the sync pattern for the channel (see
     * BsmSetSyncPattern() for more information).
     */
    if ((MICS_REV == ZL70101) || (mc->micsRevOnImplant == ZL70101)) {
        BsmSetSyncPattern(chan);
    }
    
    /* set flag to report new channel to PC (see BSM_STAT) */
    BsmPub.statFlags |= BSM_LINK_STAT_CHANGED;
}

/* If using 400 MHz wakeup and the base has a ZL70101, or is configured to
 * communicate with a ZL70101 implant, this function is called to set the sync
 * pattern as required for the specified channel. The odd channels use a
 * different sync pattern than the even channels to ensure the implant won't
 * receive the wakeup packets on an adjacent channel. This is needed because
 * the ZL70101 doesn't include the channel in 400 MHz packets, or check it in
 * received 400 MHz packets, so if either the base or the implant has a ZL70101,
 * the implant might receive the 400 MHz wakeup packets on an adjacent channel.
 *
 * Note if the base has a ZL70102/103, and is configured to communicate with
 * a ZL70102/103 implant, the base will include the channel (and company ID)
 * in the 400 MHz packets, and the implant will check it. Thus, the same sync
 * pattern can be used for all channels, and this function is never called.
 *
 * Note if you want to use a ZL70101 base with a ZL70102/103 implant, you'll
 * need to change the ZL70102/103 implant software so it also alternates the
 * sync pattern. Otherwise, 400 MHz wake-up won't work on the odd channels.
 */
static void
BsmSetSyncPattern(UINT chan)
{
    /* If channel is odd, set sync pattern for odd channel. Otherwise, set
     * sync pattern for even channel (the power-on/reset default). 
     */
    if (chan % 2) {
        MicsWrite(SYNC1, 0x6A);
        MicsWrite(SYNC2, 0xE3);
        MicsWrite(SYNC3, 0x48);
        MicsWrite(SYNC4, 0xC5);
        MicsWrite(SYNC5, 0xE6);
    } else {
        MicsWrite(SYNC1, SYNC1_RESET);
        MicsWrite(SYNC2, SYNC2_RESET);
        MicsWrite(SYNC3, SYNC3_RESET);
        MicsWrite(SYNC4, SYNC4_RESET);
        MicsWrite(SYNC5, SYNC5_RESET);
    }
}

/* BsmProc() calls this each clock tick (see BSM_MS_PER_CLOCK) to perform
 * periodic MICS tasks, such as updating the link quality statistics,
 * automatically start listening for emergency transmissions if the ZL7010X
 * is idle and auto-listen is selected, etc.
 */
void
BsmPeriodicMicsTasks(BOOL clockOverdue)
{
    BSM_MICS_CONFIG *mc = &BsmPub.micsConfig;
    BSM_MICS_PRIV *bm = &BsmMicsPriv;
    
    /* If the clock is overdue, skip the remaining tasks so we'll never do
     * them multiple times in rapid succession (it's ok if the timing for
     * the remaining tasks sometimes shifts).
     */
    if (clockOverdue) return;
    
    /* if ZL7010X is idle */
    if (BsmPub.micsState == BSM_IDLE) {
        
        /* if auto-listen is on, start listening for emergency transmissions */
        if (mc->flags & BSM_AUTO_LISTEN) BsmListenForEmergency(TRUE);
        
    /* else if ZL7010X is active */
    } else if (BsmPub.micsState > BSM_IDLE) {
        
        /* if it's time to update the link quality statistics, do so */
        if (--bm->clocksToLinkQualUpdate == 0) {
            
            /* restart number of clocks to next link quality update */
            bm->clocksToLinkQualUpdate = BSM_CLOCKS_PER_LINK_QUAL_UPDATE;
            BsmUpdateLinkQual();
        }
    }

    /* If ZL7010X is awake, ensure MAC_COMPANYID on the ADK base station is
     * quickly restored to 1 if it ever changes for any reason.
     */
    if (BsmPub.micsState != BSM_SLEEPING) MicsWrite(MAC_COMPANYID, 1);
}

/* When the ZL7010X is active (not idle), BsmPeriodicMicsTasks() periodically
 * calls this function to update the link quality statistics (so the counters
 * in the ZL7010X won't overflow). This is also called when the PC queries the
 * base station status so the PC will get the latest link quality statistics
 * (see BsmCmdGetStatChanges()).
 */
void
BsmUpdateLinkQual(void)
{
    UINT crcErrs, errCorBlocks, irqAuxStat, maxRetries;
            
    /* if using a ZL70102/103 */
    if (MICS_REV >= ZL70102) {
        
        /* stop error counters, read them, then clear and restart them */
        MicsWrite(ERRCLR_102, STOP_ECCORR | STOP_CRCERR);
        crcErrs = MicsRead(CRCERR);
        errCorBlocks = MicsRead(ECCORR);
        MicsWrite(ERRCLR_102, 0);
        
        /* if there were any link quality errors */
        if (crcErrs || errCorBlocks) {
                        
            /* update link quality & set flag to report change to PC */
            BsmPub.linkQual.crcErrs += crcErrs;
            BsmPub.linkQual.errCorBlocks += errCorBlocks;
            BsmPub.statFlags |= BSM_LINK_QUAL_CHANGED;
        }
        
    } else { /* MICS_REV == ZL70101 */
    
        /* if there were any link quality errors */
        crcErrs = MicsRead(CRCERR);
        errCorBlocks = MicsRead(ECCORR);
        if (crcErrs || errCorBlocks) {
                        
            /* clear error counters in ZL70101 */
            MicsWrite(ERRCLR_101, CLEAR_ECCORR | CLEAR_CRCERR);
            MicsWrite(ERRCLR_101, 0);
            
            /* update link quality & set flag to report change to PC */
            BsmPub.linkQual.crcErrs += crcErrs;
            BsmPub.linkQual.errCorBlocks += errCorBlocks;
            BsmPub.statFlags |= BSM_LINK_QUAL_CHANGED;
        }
    }
    
    /* if the ZL7010X asserted IRQ_LINKQUALITY */
    if (MicsPub.irqStat2Latch & IRQ_LINKQUALITY) {
        
        /* get IRQ_AUXSTATUS */
        irqAuxStat = MicsRead(IRQ_AUXSTATUS);
    
        /* If IRQ_MAXBERR: set BsmPub.linkQual.maxBErrInts; set flag to report
         * change in link quality to PC; clear IRQ_MAXBERR in IRQ_AUXSTATUS.
         */
        if (irqAuxStat & IRQ_MAXBERR) {
            
            BsmPub.linkQual.maxBErrInts = TRUE;
            BsmPub.statFlags |= BSM_LINK_QUAL_CHANGED;
            MicsWrite(IRQ_AUXSTATUS, ~IRQ_MAXBERR);
        }
        
        /* If IRQ_MAXRETRIES: set BsmPub.linkQual.maxRetriesInts; set flag to
         * report change in link quality to PC; restart the retry counter;
         * clear IRQ_MAXRETRIES in IRQ_AUXSTATUS.
         */
        if (irqAuxStat & IRQ_MAXRETRIES) {
            
            BsmPub.linkQual.maxRetriesInts = TRUE;
            BsmPub.statFlags |= BSM_LINK_QUAL_CHANGED;
            
            maxRetries = MicsRead(MAXRETRIES);
            MicsWrite(MAXRETRIES, CLEAR_RETRY_COUNTER | maxRetries);
            MicsWrite(MAXRETRIES, maxRetries);
            
            MicsWrite(IRQ_AUXSTATUS, ~IRQ_MAXRETRIES);
        }
        
        /* clear IRQ_LINKQUALITY in irqStat2Latch, then re-enable it */
        MicsPub.irqStat2Latch &= ~IRQ_LINKQUALITY;
        MicsWrite(IRQ_ENABLESET2, IRQ_LINKQUALITY);
    }
}

/* This is called to set the TX block size (TXBUFF_BSIZE) instead of calling
 * MicsWrite() directly. This sets BsmPub.txBlockSize so BsmTransmitMics() and
 * other functions don't have to read TXBUFF_BSIZE.
 */
__inline void
BsmSetTxBlockSize(UINT txBlockSize)
{
    MicsWrite(TXBUFF_BSIZE, txBlockSize);
    *((UD8 *)&BsmPub.txBlockSize) = (UD8)txBlockSize;
}

/* This is called to set the RX block size (RXBUFF_BSIZE) instead of calling
 * MicsWrite() directly. This sets BsmPub.rxBlockSize so BsmReceiveMics() and
 * other functions don't have to read RXBUFF_BSIZE.
 */
__inline void
BsmSetRxBlockSize(UINT rxBlockSize)
{
    #if MICS_REV == ZL70101
        MicsWrite(RXBUFF_BSIZE, rxBlockSize);
    #else
        MicsWrite(RXBUFF_BSIZE, rxBlockSize | IMPROVE_ERR_COR_102);
    #endif
    *((UD8 *)&BsmPub.rxBlockSize) = (UD8)rxBlockSize;
}

/* This is called to turn on the TX 400 LED when starting an HK TX. MicsReadR()
 * and MicsWriteR() call this via MICS_APP_HOOK_FOR_START_HK_TX().
 */
void BsmAppHookForStartHkTx(void)
{
    /* Turn on the 400 MHz TX LED to indicate an HK TX is active, and
     * specify BSM_NO_AUTO_OFF so BsmPeriodicLedTasks() won't turn it off
     * automatically. That way, it will remain on until the remote ZXL7010X
     * (on the implant) responds to the HK TX. At that point, the base's ZL7010X
     * will assert IRQ_HKREMOTEDONE and BsmHkRemoteDoneIsr() will switch the
     * LED from BSM_NO_AUTO_OFF to BSM_AUTO_OFF so BsmPeriodicLedTasks() will
     * turn it off automatically if there's no data in the ZL7010X's TX buffer.
     * BsmProc() periodically calls BsmPeriodicLedTasks() to update the LED's.
     */
    BsmTx400LedOn(BSM_NO_AUTO_OFF);
}

/* This is the MICS (ZL7010X) interrupt service routine.
 */
void
BsmMicsIsr(void)
{
    UINT irqStat1, irqStat2, irqAuxStat;
    
    /* If using a ZL70101 and it was sleeping, call BsmInitInterfaceMode()
     * to initialize INTERFACE_MODE as needed. This initializes the ZL70101's
     * maximum SPI clock rate, which must be done before reading the ZL70101's
     * interrupt statuses to ensure the SPI works properly (see
     * BsmInitInterfaceMode() for details).
     */
    #if MICS_REV == ZL70101
        if (BsmPub.micsState == BSM_SLEEPING) BsmInitInterfaceMode();
    #endif
    
    /* Process MICS interrupts until the chip de-asserts the interrupt line.
     * That way, if an interrupt condition occurs after we read the statuses,
     * it will be detected and processed the next time through the loop. This
     * also ensures there will always be a rising edge if an interrupt occurs
     * after we exit the ISR, so the MSP430 will always detect it and set
     * MICS_INT_FLAG again (the interrupt is edge triggered).
     * 
     * Note the interrupt flag is cleared first outside of the loop in case
     * MICS_INT_ASSERTED is false (should never happen, but it's best to
     * ensure the flag is cleared so the interrupt won't keep recurring).
     */
    MICS_WR_INT_FLAG(FALSE);
    while(MICS_INT_ASSERTED) {
        
        /* Get status for interrupt groups 1 & 2 from MICS chip, and init
         * auxiliary status to 0 (if an interrupt flagged in group 1 or 2 has
         * an auxiliary status, it will be read from the MICS chip later).
         */
        irqStat1 = MicsRead(IRQ_STATUS1); 
        irqStat2 = MicsRead(IRQ_STATUS2);
        irqAuxStat = 0;
    
        /* if no interrupts are flagged, set error (shouldn't happen) */
        if ((irqStat1 | irqStat2) == 0) {
            BsmErr(BSM_APP_ERR_MICS_INT_WITH_NO_STAT, NULL);
        }
    
        /* if a group 1 interrupt is flagged */
        if (irqStat1) {
            
            /* RX buf not empty */
            if (irqStat1 & IRQ_RXNOTEMPTY) BsmMicsRxNotEmptyIsr();
            
            /* remote side accessed HK_USERDATA */
            if (irqStat1 & IRQ_HKUSERDATA) BsmHkUserDataIsr();
            
            /* remote side accessed HK_USERSTATUS */
            if (irqStat1 & IRQ_HKUSERSTATUS) BsmHkUserStatusIsr();
            
            /* remote side responded to HK access */
            if (irqStat1 & IRQ_HKREMOTEDONE) BsmHkRemoteDoneIsr();
            
            /* connection lost (IRQ_WDOG or IRQ_HKABORTLINK) */
            if (irqStat1 & IRQ_CLOST) irqAuxStat = BsmConLostIsr(irqAuxStat);
            
            /* set bits in the IRQ latch for the MICS library functions */
            MicsPub.irqStat1Latch |= irqStat1;
                
            /* clear any group 1 interrupts that were set in the status */
            MicsWrite(IRQ_RAWSTATUS1, ~irqStat1);
        }
        
        /* if a group 2 interrupt is flagged */
        if (irqStat2) {
            
            /* radio fail (IRQ_CRCERR or IRQ_SYNTHLOCKFAIL) */
            if (irqStat2 & IRQ_RADIOFAIL) irqAuxStat = BsmRadioFailIsr(irqAuxStat);
            
            /* radio ready (at end of wakeup, or after abort link) */
            if (irqStat2 & IRQ_RADIOREADY) BsmRadioReadyIsr();
            
            /* link ready (session established) */
            if (irqStat2 & IRQ_LINKREADY) BsmLinkReadyIsr();
            
            /* link quality (IRQ_MAXRETRIES or IRQ_MAXBERR) */
            if (irqStat2 & IRQ_LINKQUALITY) BsmLinkQualIsr();
            
            /* remote side accessed local register via housekeeping */
            if (irqStat2 & IRQ_HKMESSREG) BsmHkMessRegIsr();
            
            /* MAC_CTRL command done */
            if (irqStat2 & IRQ_COMMANDDONE) BsmCommandDoneIsr();
            
            /* set bits in the IRQ latch for the MICS library functions */
            MicsPub.irqStat2Latch |= irqStat2;
            
            /* clear any group 2 interrupts that were set in the status */
            MicsWrite(IRQ_RAWSTATUS2, ~irqStat2);
        }
        
        /* set bits in the IRQ latch for the MICS library functions */
        MicsPub.irqAuxStatLatch |= irqAuxStat;
        
        /* ensure interrupt flag is clear so it won't recur when ISR exits */
        MICS_WR_INT_FLAG(FALSE);
    }
}

/* If using a ZL70101, then each time the ZL70101 asserts IRQ_RADIOREADY when
 * it wakes up, BsmMicsIsr() calls this function to initialize INTERFACE_MODE
 * as needed. This is required because each time the ZL70101 is sleep cycled,
 * it resets INTERFACE_MODE, which resets the ZL70101's maximum SPI clock
 * rate to 2 MHz. If the actual SPI clock is > 2 MHz, this function adjusts
 * INTERFACE_MODE to increase the ZL70101's maximum SPI clock rate to 4 MHz,
 * which is required in order for the SPI to work. If the actual SPI clock
 * is <= 1 MHz, this function adjusts INTERFACE_MODE to decrease the ZL70101's
 * maximum SPI clock rate to 1 MHz in order to reduce the power consumed by the
 * ZL70101's SPI interface a little, even though the power saved is probably
 * insignificant relative to the overall power consumed by the ZL70101.
 *
 * Note that for a ZL70102/103, BsmMicsIsr() does not need to call this
 * function to initialize INTERFACE_MODE. Instead, each time the ZL70102/103
 * is awakened, BsmInitMicsOnWakeup() is called, which initializes
 * INTERFACE_MODE (see BsmInitMicsOnWakeup() for details).
 */
#if MICS_REV == ZL70101
__inline void
BsmInitInterfaceMode(void)
{
    /* if the desired INTERFACE_MODE value != reset value, set it */
    if (BSM_INTERFACE_MODE != INTERFACE_MODE_RESET_101) {

        /* If the local SPI bus clock is normally > 2 MHz, reduce it to 2 MHz
         * so we can talk to the ZL70101 in its default state (2 MHz max),
         * then restore it afterwards.
         */
        if (BSM_LSPI_CLK_FREQ > (UD32)2000000UL) {

            LSpiConfig(BSM_LSPI_SPECS, BSM_LSPI_CLK_DIV_FOR_2_MHZ);
            MicsWrite(INTERFACE_MODE, BSM_INTERFACE_MODE);
            LSpiConfig(BSM_LSPI_SPECS, BSM_LSPI_CLK_DIV);

        } else {
            MicsWrite(INTERFACE_MODE, BSM_INTERFACE_MODE);
        }
    }
}
#endif

__inline UINT
BsmRadioFailIsr(UINT irqAuxStat)
{
    /* if auxiliary status hasn't been read yet, read it */     
    if (irqAuxStat == 0) irqAuxStat = MicsRead(IRQ_AUXSTATUS);
    
    /* If CRC error interrupt, handle it. If desired, the base station
     * application can call BsmResetMics(FALSE) to reset and reinitialize the
     * ZL7010X to clear the CRC error. For now, just set an error to report to
     * the host (PC), tell the ZL7010X to skip the CRC, and clear IRQ_CRCERR
     * in IRQ_AUXSTATUS.
     */
    if (irqAuxStat & IRQ_CRCERR) {
        
        BsmErr(BSM_APP_ERR_MICS_CRC_ERR, NULL);
        MicsWrite(MAC_CTRL, SKIP_CRC);
        MicsWrite(IRQ_AUXSTATUS, ~IRQ_CRCERR);
    }
    
    /* If synth lock fail, set error to report to the host (PC) and clear
     * IRQ_SYNCLOCKFAIL in IRQ_AUXSTATUS.
     */
    if (irqAuxStat & IRQ_SYNTHLOCKFAIL) {
        
        BsmErr(BSM_APP_ERR_MICS_SYNTH_FAILED_TO_LOCK, NULL);
        MicsWrite(IRQ_AUXSTATUS, ~IRQ_SYNTHLOCKFAIL);
    }
    
    /* if using ZL70102/103 */
    #if MICS_REV >= ZL70102
    
        /* if IRQ_VREGFAIL_102, re-write VDDA & VDDD trims to clear it */
        if (irqAuxStat & IRQ_VREGFAIL_102) {
            
            MicsWrite(TESTCTRL1_102, ENAB_VDD_WRITE);
            MicsWrite(VDDATRIM_102, VDDATRIM_RESET_102);
            MicsWrite(VDDDTRIM_102, VDDDTRIM_RESET_102);
            MicsWrite(TESTCTRL1_102, 0);
        }
    #endif
    
    /* return auxiliary status read from MICS chip */
    return(irqAuxStat);
}

__inline void
BsmRadioReadyIsr(void)
{
    /* If ZL7010X was sleeping (IRQ_RADIOREADY is for wakeup, not an abort),
     * update state to indicate ZL7010X is awake & idle.
     */
    if (BsmPub.micsState == BSM_SLEEPING) BSM_SET_MICS_STATE(BSM_IDLE);
}

__inline void
BsmLinkReadyIsr(void)
{
    /* if 2.45 GHz TX is on, turn it off (also turns off TX 245 LED) */
    if (MICS_TX_245_ENAB) BsmSetTx245Mode(BSM_TX_245_OFF);
    
    /* Clear the following bits in INITCOM in case they were set:
     * 
     * TX_245_GHZ_WAKEUP:
     *     Clear to stop 2.45 GHz wakeup transmission.
     * RX_WAKEUP_RESPONSES:
     *     This shouldn't normally be set when a session is started, but clear
     *     it just in case (stop placing received wakeup responses in RX buf).
     * START_SESSION:
     *     Also cleared (doesn't matter since session has already started).
     */
    MicsWrite(INITCOM, IBS_FLAG);
    
    /* turn session LED on */
    BsmSessionLedOn();
    
    /* Enable MICS watchdog timer in case it wasn't enabled while starting
     * session or searching for implant(s) (see BsmStartSession() and 
     * BsmSearchForImplant(). This is needed so if the session is lost, the
     * MICS watchdog will time out and assert IRQ_WDOG (see BsmConLostIsr()),
     * to ensure the base station won't occupy the MICS channel more than
     * 5 seconds (MICS specification).
     */
    MicsWrite(WDOG_CONTROL, WDOG_ENAB);
    
    /* allow retry counter to start counting (for IRQ_MAXRETRIES) */
    MicsWrite(MAXRETRIES, MicsRead(MAXRETRIES) & ~CLEAR_RETRY_COUNTER);
    
    /* enable IRQ_RXNOTEMPTY (for received session data) */
    MicsWrite(IRQ_ENABLESET1, IRQ_RXNOTEMPTY);
    
    /* update state to indicate session started */
    BSM_SET_MICS_STATE(BSM_IN_SESSION);
    
    /* set flag to tell BsmProc() to call BsmProcMicsSession() */
    BsmPub.pend |= BSM_MICS_SESSION_PEND;
}

__inline void
BsmLinkQualIsr(void)
{
    /* Disable IRQ_LINKQUALITY until the next time BsmUpdateLinkQual() is
     * called. BsmUpdateLinkQual() is called periodically to update the link
     * quality (see IM_MS_PER_LINK_QUAL_UPDATE). When it's called, it will
     * detect IRQ_LINKQUALITY in MicsPub.irqStat2Latch, update the link
     * quality as needed, then re-enable IRQ_LINKQUALITY. IRQ_LINKQUALITY
     * is handled this way for the following reason: if the ZL7010X asserts
     * IRQ_LINKQUALITY for IRQ_MAXBERR, then never receives another packet
     * afterwards (as can happen if the RF link is poor), it will continue
     * to assert IRQ_LINKQUALITY, even if IRQ_MAXBERR is cleared in
     * IRQ_AUXSTATUS. To ensure this won't monopolize the micro-controller
     * and cause a lock-up condition, IRQ_LINKQUALITY is disabled when
     * it occurs, and the next BsmUpdateLinkQual() handles it.
     */
    MicsWrite(IRQ_ENABLECLEAR2, IRQ_LINKQUALITY);
}

__inline void
BsmMicsRxNotEmptyIsr(void)
{
    /* disable MICS RX interrupt until data in RX buf is read */
    MicsWrite(IRQ_ENABLECLEAR1, IRQ_RXNOTEMPTY);
    
    /* Turn on the 400 MHz RX LED & tell BsmPeriodicLedTasks() turn it off
     * automatically when the RX buffer on the MICS chip becomes empty.
     * BsmProc() periodically calls BsmPeriodicLedTasks() to update the LED's.
     */
    BsmRx400LedOn(BSM_AUTO_OFF);
    
    /* set flag to tell BsmProc() to call BsmProcMicsRx() */
    BsmPub.pend |= BSM_MICS_RX_PEND;
}

__inline void
BsmHkMessRegIsr(void)
{
    /* Turn on the 400 MHz RX & TX LED's to indicate a remote HK access was
     * received and replied to, & let BsmPeriodicLedTasks() turn them off
     * automatically. BsmProc() periodically calls BsmPeriodicLedTasks() to
     * update the LED's.
     */
    BsmRx400LedOn(BSM_AUTO_OFF);
    BsmTx400LedOn(BSM_AUTO_OFF);
}

__inline void
BsmHkUserDataIsr(void)
{
    /* Turn on the 400 MHz RX & TX LED's to indicate a remote HK access was
     * received and replied to, & let BsmPeriodicLedTasks() turn them off
     * automatically. BsmProc() periodically calls BsmPeriodicLedTasks() to
     * update the LED's.
     */
    BsmRx400LedOn(BSM_AUTO_OFF);
    BsmTx400LedOn(BSM_AUTO_OFF);
}

__inline void
BsmHkUserStatusIsr(void)
{
    /* Turn on the 400 MHz RX & TX LED's to indicate a remote HK access was
     * received and replied to, & let BsmPeriodicLedTasks() turn them off
     * automatically. BsmProc() periodically calls BsmPeriodicLedTasks() to
     * update the LED's.
     */
    BsmRx400LedOn(BSM_AUTO_OFF);
    BsmTx400LedOn(BSM_AUTO_OFF);
}

__inline void
BsmHkRemoteDoneIsr(void)
{
    /* Turn on the 400 MHz RX LED to indicate a reply was received for a remote
     * HK access, and let BsmPeriodicLedTasks() turn it off automatically. Also,
     * the 400 MHz TX LED is already on for the HK TX, so keep it on, but switch
     * it to BSM_AUTO_OFF so BsmPeriodicLedTasks() will turn it off if there's
     * no data in the ZL7010X's TX buffer. BsmProc() periodically calls
     * BsmPeriodicLedTasks() to update the LED's.
     */
    BsmRx400LedOn(BSM_AUTO_OFF);
    BsmTx400LedOn(BSM_AUTO_OFF);
}

__inline void
BsmCommandDoneIsr(void)
{
    BSM_MICS_PRIV *bm = &BsmMicsPriv;
    
    /* Clear txBytesAvail in case the command flushed the TX buffer in the
     * MICS chip. That way, the next time BsmTransmitMics() is called, it will
     * re-read TXBUFF_USED and recalculate txBytesAvail.
     * 
     * Clear rxBytesAvail in case the command flushed the RX buffer in the
     * MICS chip. That way, the next time BsmReceiveMics() is called, it will
     * re-read RXBUFF_BUSED and detect if the RX buffer is still empty.
     */
    bm->txBytesAvail = 0;
    bm->rxBytesAvail = 0;
}

__inline UINT
BsmConLostIsr(UINT irqAuxStat)
{
    /* if auxiliary status hasn't been read yet, read it */     
    if (irqAuxStat == 0) irqAuxStat = MicsRead(IRQ_AUXSTATUS);
    
    /* If abort link received via housekeeping, clear bit in auxiliary
     * status. Normally, this shouldn't occur on a base station, but since
     * IRQ_HKABORTLINK and IRQ_WDOG both use IRQ_CLOST, there's no way to
     * disable them separately, so clear IRQ_HKABORTLINK just in case.
     */
    if (irqAuxStat & IRQ_HKABORTLINK) {
        MicsWrite(IRQ_AUXSTATUS, ~IRQ_HKABORTLINK);
    }
    
    /* If MICS watchdog timer, clear it (clears interrupt source), then clear
     * the bit in the auxiliary status. Clearing the watchdog also ensures it
     * won't finish counting to ~5 seconds and force the MICS chip to sleep
     * (if it did, the software would still think the chip was awake, which
     * would cause problems). Note the watchdog is also disabled here because
     * on a base station, there's no reason to keep it enabled after it occurs.
     */
    if (irqAuxStat & IRQ_WDOG) {
        
        MicsWrite(WDOG_CONTROL, WDOG_CLEAR_AND_DISAB);
        MicsWrite(IRQ_AUXSTATUS, ~IRQ_WDOG);
    }
    
    /* Set BSM_MICS_ABORT_PEND flag to tell BsmProc() to abort the MICS link.
     * BsmProc() will detect this flag and call BsmAbortMics(), which in turn
     * will call MicsAbort() to abort the link. BsmAbortMics() isn't called
     * here in the ISR because that could cause race conditions if the software
     * is doing something with the MICS chip when IRQ_CLOST occurs. Also,
     * MicsAbort() might take up to 24 ms (the MICS chip only checks its
     * ABORT_LINK bit between transmit and receive, and the maximum possible
     * packet time is 24 ms), so it's not something you'd generally want to do
     * in an ISR.
     * 
     * Note the MICS watchdog asserts IRQ_WDOG ~4.37 seconds after the link is
     * lost, and the MICS standard says the channel should be released if the
     * link isn't active for 5 seconds, so BsmAbortMics() needs to be called
     * within ~0.6 seconds to ensure the channel is released in time. Normally,
     * BsmProc() will detect BSM_MICS_ABORT_PEND and call BsmAbortMics() right
     * away. However, this could be an issue if BsmProc() is busy doing
     * something else that takes longer than 0.6 seconds to finish. On the
     * ADK base station, when the MICS chip is in session, there's no operation
     * that would take BsmProc() more than 0.6 seconds to finish, so this isn't
     * an issue (a clear channel assessment might take more than 0.6 seconds,
     * but that's never performed during a session).
     */
    BsmPub.pend |= BSM_MICS_ABORT_PEND;
    
    /* return auxiliary status read from MICS chip */
    return(irqAuxStat);
}
