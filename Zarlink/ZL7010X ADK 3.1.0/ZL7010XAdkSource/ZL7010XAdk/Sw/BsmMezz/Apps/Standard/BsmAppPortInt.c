/**************************************************************************
 * This file contains functions for the I/O port interrupts on the base station
 * mezzanine board.
 *
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Adp/Build/Build.h"    /* PORT2_VECTOR */
#include "Adk/AnyMezz/MicsHw.h" /* MICS_INT_FLAG, ... */

/* local include shared by all source files for base station application */
#include "BsmApp.h"

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/**************************************************************************
 * Function Prototypes
 */
 
__interrupt void BsmPort2Isr(void);

/**************************************************************************
 * Interrupt service routine for port 2 interrupts on the base station.
 */
#pragma vector=PORT2_VECTOR
__interrupt void
BsmPort2Isr(void)
{
    /* if interrupt from MICS chip, process it */
    if (MICS_INT_FLAG) BsmMicsIsr();
        
#ifdef TODO
    /* If ISR set a pending flag for BsmProc(), adjust MCU's status register
     * on stack so we won't re-enter low power mode when ISR returns.
     */
    if (BsmPub.pend) LPM4_EXIT;
#endif    
}
