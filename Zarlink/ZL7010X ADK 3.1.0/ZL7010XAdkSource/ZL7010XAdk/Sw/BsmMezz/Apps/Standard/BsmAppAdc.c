/**************************************************************************
 * This file contains functions to interface to the A/D converter in the
 * MSP430 on the base station mezzanine board.
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Adp/General.h"              /* UINT, RSTAT, ... */
#include "Adp/Build/Build.h"          /* ADC12CTL0, EXT_RSSI_ADC_INPUT, ... */
#include "Adp/AnyBoard/SysTimerLib.h" /* StDelayMs(), ...  */

/* local include shared by all source files for base station application */
#include "BsmApp.h"

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/**************************************************************************
 * Function Prototypes
 */
 
/**************************************************************************
 * Initialize the A/D converter interface on the base station. This uses the
 * ADC in the MSP430.
 */
RSTAT
BsmAdcInit(void)
{
    /* Select ADC functionality for the I/O pins used for ADC inputs. This
     * prevents parasitic current flow from VCC to GND when the input voltage
     * is near the transition level of the gate (recommended in MSP430 manual).
     */
    ADC_INPUT_P(SEL) |= (EXT_RSSI_ADC_BIT | PWR_DET_ADC_BIT);
    
    /* Turn ADC on and set it up as follows:
     * 
     * ADC12CTL0:
     *   ADC12ON: Turn ADC on.
     *   SHT0_2: Sample period = 16 ADC12CLK cycles.
     *   REFON: Enable internal reference voltage generator.
     *   REF2_5V: Internal reference voltage = 2.5v
     *   MSC: auto-start next sample (only applies to repeat & sequence modes)
     * 
     * ADC12CTL1:
     *   SHP: SAMPCON signal is sourced from the sampling timer (automatic).
     *   ADC12SSEL_1: Clock source (ADC12CLK) = ACLK (8 MHz).
     * 
     * The clock source (ACLK = 8 MHz) and sample period (16 ACLK cycles) were
     * determined based on "Sample Timing Considerations" in the MSP430 manual,
     * which gives the following equation for the minimum sample time (where Rs
     * is the external source resistance on the ADC input):
     * 
     *   Minimum tSamp = (Rs + 2kOhm) * 9.011 * 40pF + 800ns
     * 
     * For the external RSSI input on a base station, this yields:
     * 
     *   Minimum tSamp = (0 + 2kOhm) * 9.011 x 40pF + 800ns = 1.521 us
     * 
     * Setting the sample period to 16 ACLK cycles yields the closest actual
     * tSamp. Note this allows ample padding in case Rs for the external RSSI
     * input isn't exactly 0:
     * 
     *   Actual tSamp = 16 * (ACLK cycle) = 16 * 0.125 = 2us
     * 
     * From the MSP430 manual, tSync and tConvert are as follows:
     * 
     *   tSync = (ACLK cycle) / 2 = 0.125 / 2 = 0.0625us
     *   tConvert = 13 * (ACLK cycle) = 13 * (1 / 8MHz) = 1.625us 
     * 
     * Thus, the total time required to do each ADC sample is:
     *
     *   tTotal = tSync + tSamp + tConvert = 0.0625 + 2 + 1.625 = 3.6875 us.
     */
    ADC12CTL0 = ADC12ON | SHT0_2 | REFON | REF2_5V | MSC;
    ADC12CTL1 = SHP | ADC12SSEL_1;
    
    /* SREFx = SREF_1: VR+ = VREF+ and VR? = AVSS */
    ADC12MCTL0 = SREF_1;
    
    /* wait 17 ms (to give VRef time to bias the storage capacitors) */
    StDelayMs(17);
    
    return(0);
}

/* Take a single sample for the specified ADC input.
 */
UINT
BsmAdc(UINT adcInput)
{
    /* select ADC input for ADC12MEM0 (ADC output location 0) */
    ADC12MCTL0 = SREF_1 | adcInput;
    
    /* enable conversions & start conversion */
    ADC12CTL0 |= (ENC | ADC12SC);
    
    /* wait for conversion to finish */
    while((ADC12IFG & BIT0) == 0);
    
    /* disable conversions (so input can be changed for future samples) */
    ADC12CTL0 &= ~ENC;
    
    /* read result (also clears ADC12IFG.BIT0) */
    return(ADC12MEM0);
}
