/**************************************************************************
 * This file contains functions to receive and process command packets on the
 * base station mezzanine board.
 *
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Standard/String.h"          /* memset() */
#include "Adp/General.h"              /* UINT, RSTAT, BOOL, TRUE/FALSE, ... */
#include "Adp/Build/Build.h"          /* UD8, UD16, ... */
#include "Adp/AnyBoard/Com.h"         /* COM_MAX_PACK_LEN */
#include "Adp/AnyBoard/ErrLib.h"      /* ErrGetFirst() */
#include "Adp/AnyBoard/TraceLib.h"    /* TRACE_ENAB, TraceGetNext(), ... */
#include "Adp/AnyBoard/SysTimerLib.h" /* StElapsedMs(), ... */ 
#include "Adk/AdkVer.h"               /* ADK_VER */
#include "Adk/AnyMezz/MicsHw.h"       /* MAC_CHANNEL, ... */
#include "Adk/AnyMezz/MicsLib.h"      /* MicsRead(), ... */
#include "Adk/AnyMezz/MicsGeneral.h"  /* MICS_LINK_STAT, MICS_DATA_STAT, ...*/
#include "Adk/BsmMezz/BsmCom.h"       /* BSM_START_SESSION_CMD, ... */
#include "Adk/BsmMezz/BsmGeneral.h"   /* BSM_IDLE, BSM_MODEL, BSM100, ...*/
#include "Adk/BsmMezz/CC25XXHw.h"     /* CC_SPWD, ... */
#include "Adk/BsmMezz/CC25XXLib.h"    /* CcCommand(), CcWrite(), ... */

/* local include shared by all source files for base station application */
#include "BsmApp.h"
 
/**************************************************************************
 * Defines and Macros
 */

/* model name for base station mezzanine board */
#if BSM_MODEL == BSM100
    #define BSM_MODEL_NAME  BSM100_NAME
#elif BSM_MODEL == BSM200
    #define BSM_MODEL_NAME  BSM200_NAME
#elif BSM_MODEL == BSM300
    #define BSM_MODEL_NAME  BSM300_NAME
#else
    #error "Invalid BSM_MODEL (make sure the build include defines BSM_HW)."
#endif
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* structure of private data for command processing */
typedef struct {
    
    /* Buffer used to hold a received command while the command is being
     * processed. This is a union because it is only used to hold one command
     * at a time.
     */
    union {
        BSM_START_SESSION_CMD startSessionCmd;
        BSM_SEARCH_FOR_IMPLANT_CMD searchForImplantCmd;
        BSM_LISTEN_FOR_EMERGENCY_CMD listenForEmergencyCmd;
        BSM_READ_MICS_REG_CMD readMicsRegCmd;
        BSM_RECEIVE_MICS_CMD receiveMicsCmd;
        BSM_WRITE_MICS_REG_CMD writeMicsRegCmd;
        BSM_MICS_CAL_CMD micsCalCmd;
        BSM_MICS_ADC_CMD micsAdcCmd;
        BSM_SET_TX_245_POWER_CMD setTx245PowerCmd;
        BSM_START_TX_245_CARRIER_CMD startTx245CarrierCmd;
        BSM_START_TX_400_CARRIER_CMD startTx400CarrierCmd;
        BSM_RSSI_CMD rssiCmd;
        BSM_ENAB_EXT_RSSI_CMD enabExtRssiCmd;
        BSM_CCA_CMD ccaCmd;
        BSM_COPY_MICS_REGS_CMD copyMicsRegsCmd;
        BSM_GET_STAT_CHANGES_CMD getStatChangesCmd;
        BSM_START_DATA_TEST_CMD startDataTestCmd;
        BSM_SET_TX_245_FREQ_CMD setTx245FreqCmd;
        BSM_CALC_MICS_CRC_CMD calcMicsCrcCmd;
        BSM_WRITE_CC_REG_CMD writeCcRegCmd;
        BSM_READ_CC_REG_CMD readCcRegCmd;
    } cmdBuf;
    
    /* Temporary buffer used to hold data for a reply, and to hold data for the
     * MICS chip while receiving a command or sending a reply. This is a union
     * because it is only used for one thing at a time.
     */
    union {
        BSM_STAT genStat;
        MICS_LINK_STAT linkStat;
        BSM_READ_MICS_REG_REPLY readMicsRegReply;
        BSM_MICS_ADC_REPLY micsAdcReply;
        BSM_RSSI_REPLY rssiReply;
        BSM_CCA_REPLY ccaReply;
        BSM_READ_CC_REG_REPLY readCcRegReply;
        BSM_GET_MICS_FACT_NVM_REPLY readFactNvmReply;
        UD8 data[TXBUFF_BSIZE_MAX];
    } tempBuf;
    
} BSM_CMD_PRIV;
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/* private data for command processing */
static BSM_CMD_PRIV BsmCmdPriv;
 
/**************************************************************************
 * Function Prototypes
 */
static void BsmGetGenStat(BSM_STAT *gs);
static void BsmGetLinkStat(MICS_LINK_STAT *ls);
static RSTAT BsmSendDataWithLen(const void *data, UD8 len);
static void BsmChangeChan(UINT chan);
static RSTAT BsmCmdLenErr(void);
/**/
static RSTAT BsmCmdGetDevType(UINT cmdLen);
static RSTAT BsmCmdGetDevName(UINT cmdLen);
static RSTAT BsmCmdGetVer(UINT cmdLen);
static RSTAT BsmCmdGetErr(UINT cmdLen);
static RSTAT BsmCmdGetStat(UINT cmdLen);
static RSTAT BsmCmdGetMicsConfig(UINT cmdLen);
static RSTAT BsmCmdSetMicsConfig(UINT cmdLen);
static RSTAT BsmCmdStartSession(UINT cmdLen);
static RSTAT BsmCmdSearchForImplant(UINT cmdLen);
static RSTAT BsmCmdListenForEmergency(UINT cmdLen);
static RSTAT BsmCmdAbortMics(UINT cmdLen);
static RSTAT BsmCmdReadMicsReg(UINT cmdLen);
static RSTAT BsmCmdWriteMicsReg(UINT cmdLen);
static RSTAT BsmCmdReceiveMics(UINT cmdLen);
static RSTAT BsmCmdTransmitMics(UINT cmdLen);
static RSTAT BsmCmdMicsCal(UINT cmdLen);
static RSTAT BsmCmdMicsAdc(UINT cmdLen);
static RSTAT BsmCmdSetTx245Power(UINT cmdLen);
static RSTAT BsmCmdStartTx245Carrier(UINT cmdLen);
static RSTAT BsmCmdStartTx400Carrier(UINT cmdLen);
static RSTAT BsmCmdRssi(UINT cmdLen);
static RSTAT BsmCmdEnabExtRssi(UINT cmdLen);
static RSTAT BsmCmdCca(UINT cmdLen);
static RSTAT BsmCmdGetLinkStat(UINT cmdLen);
static RSTAT BsmCmdGetLinkQual(UINT cmdLen);
static RSTAT BsmCmdGetTraceMsg(UINT cmdLen);
static RSTAT BsmCmdCopyMicsRegs(UINT cmdLen);
static RSTAT BsmCmdGetStatChanges(UINT cmdLen);
static RSTAT BsmCmdStartDataTest(UINT cmdLen);
static RSTAT BsmCmdSetTx245Freq(UINT cmdLen);
static RSTAT BsmCmdResetMics(UINT cmdLen);
static RSTAT BsmCmdCalcMicsCrc(UINT cmdLen);
static RSTAT BsmCmdWriteCcReg(UINT cmdLen);
static RSTAT BsmCmdReadCcReg(UINT cmdLen);
static RSTAT BsmCmdWakeup(UINT cmdLen);
static RSTAT BsmCmdSleep(UINT cmdLen);
static RSTAT BsmCmdGetFactNvm(UINT cmdLen);

/**************************************************************************
 * Initialize the command interface on the base station.
 */
void
BsmCmdInit(void)
{
    BSM_CMD_PRIV *bc = &BsmCmdPriv;
    
    /* clear data and init any non-zero defaults */
    (void)memset(bc, 0, sizeof(*bc));
}

/* Receive and process the remainder of the command packet for the specified
 * command type, and send a reply.
 */
RSTAT
BsmReceiveAndProcCmd(UINT cmdType, UINT packLen)
{
    RSTAT rStat;
    
    /* If BsmInit() encountered an initialization error, report the error
     * instead of trying to process the command from the PC (that might not
     * be safe since the initialization failed). Note some basic commands are
     * still allowed so the PC can find the base station (to open a connection).
     */
    if (BsmPub.flags & BSM_INIT_ERR) {
        
        switch(cmdType) {
        case COM_GET_DEV_TYPE_CMD_TYPE:
        case COM_GET_DEV_NAME_CMD_TYPE:
            break;
        default:
            BsmReceiveAndDiscard(packLen);
            (void)BsmSendErrReply(COM_ERR_REPLY_TYPE, ErrGetFirst());
            return(-1);
        }
    }
    
    /* receive and process remainder of command packet, & send reply */
    switch(cmdType) {
    case COM_GET_DEV_TYPE_CMD_TYPE:
        rStat = BsmCmdGetDevType(packLen); break;
    case COM_GET_DEV_NAME_CMD_TYPE:
        rStat = BsmCmdGetDevName(packLen); break;
    case COM_GET_DEV_VER_CMD_TYPE:
        rStat = BsmCmdGetVer(packLen); break;
    case COM_GET_DEV_ERR_CMD_TYPE:
        rStat = BsmCmdGetErr(packLen); break;
    case BSM_GET_STAT_CMD_TYPE:
        rStat = BsmCmdGetStat(packLen); break;
    case BSM_GET_MICS_CONFIG_CMD_TYPE:
        rStat = BsmCmdGetMicsConfig(packLen); break;
    case BSM_SET_MICS_CONFIG_CMD_TYPE:
        rStat = BsmCmdSetMicsConfig(packLen); break;
    case BSM_START_SESSION_CMD_TYPE:
        rStat = BsmCmdStartSession(packLen); break;
    case BSM_SEARCH_FOR_IMPLANT_CMD_TYPE:
        rStat = BsmCmdSearchForImplant(packLen); break;
    case BSM_LISTEN_FOR_EMERGENCY_CMD_TYPE:
        rStat = BsmCmdListenForEmergency(packLen); break;
    case BSM_ABORT_MICS_CMD_TYPE:
        rStat = BsmCmdAbortMics(packLen); break;
    case BSM_READ_MICS_REG_CMD_TYPE:
        rStat = BsmCmdReadMicsReg(packLen); break;
    case BSM_RECEIVE_MICS_CMD_TYPE:
        rStat = BsmCmdReceiveMics(packLen); break;
    case BSM_WRITE_MICS_REG_CMD_TYPE:
        rStat = BsmCmdWriteMicsReg(packLen); break;
    case BSM_TRANSMIT_MICS_CMD_TYPE:
        rStat = BsmCmdTransmitMics(packLen); break;
    case BSM_MICS_CAL_CMD_TYPE:
        rStat = BsmCmdMicsCal(packLen); break;
    case BSM_MICS_ADC_CMD_TYPE:
        rStat = BsmCmdMicsAdc(packLen); break;
    case BSM_SET_TX_245_POWER_CMD_TYPE:
        rStat = BsmCmdSetTx245Power(packLen); break;
    case BSM_START_TX_245_CARRIER_CMD_TYPE:
        rStat = BsmCmdStartTx245Carrier(packLen); break;
    case BSM_START_TX_400_CARRIER_CMD_TYPE:
        rStat = BsmCmdStartTx400Carrier(packLen); break;
    case BSM_RSSI_CMD_TYPE:
        rStat = BsmCmdRssi(packLen); break;
    case BSM_ENAB_EXT_RSSI_CMD_TYPE:
        rStat = BsmCmdEnabExtRssi(packLen); break;
    case BSM_CCA_CMD_TYPE:
        rStat = BsmCmdCca(packLen); break;
    case BSM_GET_LINK_STAT_CMD_TYPE:
        rStat = BsmCmdGetLinkStat(packLen); break;
    case BSM_GET_LINK_QUAL_CMD_TYPE:
        rStat = BsmCmdGetLinkQual(packLen); break;
    case BSM_GET_TRACE_MSG_CMD_TYPE:
        rStat = BsmCmdGetTraceMsg(packLen); break;
    case BSM_COPY_MICS_REGS_CMD_TYPE:
        rStat = BsmCmdCopyMicsRegs(packLen); break;
    case BSM_GET_STAT_CHANGES_CMD_TYPE:
        rStat = BsmCmdGetStatChanges(packLen); break;
    case BSM_START_DATA_TEST_CMD_TYPE:
        rStat = BsmCmdStartDataTest(packLen); break;
    case BSM_SET_TX_245_FREQ_CMD_TYPE:
        rStat = BsmCmdSetTx245Freq(packLen); break;
    case BSM_RESET_MICS_CMD_TYPE:
        rStat = BsmCmdResetMics(packLen); break;
    case BSM_CALC_MICS_CRC_CMD_TYPE:
        rStat = BsmCmdCalcMicsCrc(packLen); break;
    case BSM_WRITE_CC_REG_CMD_TYPE:
        rStat = BsmCmdWriteCcReg(packLen); break;
    case BSM_READ_CC_REG_CMD_TYPE:
        rStat = BsmCmdReadCcReg(packLen); break;
    case BSM_SLEEP_CMD_TYPE:
        rStat = BsmCmdSleep(packLen); break;
    case BSM_WAKEUP_CMD_TYPE:
        rStat = BsmCmdWakeup(packLen); break;
    case BSM_GET_MICS_FACT_NVM_CMD_TYPE:
        rStat = BsmCmdGetFactNvm(packLen); break;
    default:
        /* Since command was invalid, communication might not be synchronized on
         * packet boundaries, so receive and discard bytes until it times out.
         */
        BsmErr(BSM_APP_ERR_RECEIVED_CMD_WITH_INVALID_CMD_TYPE, "%u", cmdType);
        BsmReceiveAndDiscard(COM_MAX_PACK_LEN);
        return(-1);
    }
    return(rStat);
}

static RSTAT
BsmCmdGetDevType(UINT cmdLen)
{
    /* check command packet length */
    if (cmdLen != 0) return(BsmCmdLenErr());
    
    /* send reply for successful command (device type string including '\0') */
    return(BsmSendReply(BSM_DEV_TYPE, sizeof(BSM_DEV_TYPE)));
}

static RSTAT
BsmCmdGetDevName(UINT cmdLen)
{
    /* check command packet length */
    if (cmdLen != 0) return(BsmCmdLenErr());
    
    /* Send reply for successful command (device name including '\0'). The
     * base station doesn't currently provide an interface to set its device
     * name, so it just returns an empty string.
     */
    return(BsmSendReply("", sizeof("")));
}

static RSTAT
BsmCmdGetVer(UINT cmdLen)
{
    UINT packLen;
    
    /* check command packet length */
    if (cmdLen != 0) return(BsmCmdLenErr());
    
    /* Calculate the length of the reply packet for the base station's version
     * string, then send the packet header followed by version string.
     */
    packLen = BSM_LEN(BSM_MODEL_NAME) + BSM_LEN("; ") + BSM_LEN(ADK_VER) +
        BSM_LEN("\0");
    /**/    
    if (BsmSendReplyHdr(COM_OK_REPLY_TYPE, packLen)) return(-1);
    if (BsmSendReplyData(BSM_MODEL_NAME, BSM_LEN(BSM_MODEL_NAME))) return(-1);
    if (BsmSendReplyData("; ", BSM_LEN("; "))) return(-1);
    if (BsmSendReplyData(ADK_VER, BSM_LEN(ADK_VER))) return(-1);
    if (BsmSendReplyData("\0", BSM_LEN("\0"))) return(-1);
    
    return(0);
}

static RSTAT
BsmCmdGetErr(UINT cmdLen)
{
    /* check command packet length */
    if (cmdLen != 0) return(BsmCmdLenErr());
    
    /* Send reply containing information for latched error (if any). Note if
     * no error is latched, the error code will be 0, the error group will be
     * a default string, and the error arguments will an empty string.
     */
    if (BsmSendErrReply(COM_OK_REPLY_TYPE, ErrGetFirst())) return(-1);
    
    /* clear any latched error information */
    ErrClearFirst();
    
    return(0);
}

/* Kept for backwards compatibility (now queried via BsmCmdGetStatChanges()).
 */
static RSTAT
BsmCmdGetStat(UINT cmdLen)
{
    BsmGetGenStat(&BsmCmdPriv.tempBuf.genStat);
    return(BsmSendReply(&BsmCmdPriv.tempBuf.genStat, sizeof(BSM_STAT)));
}

static RSTAT
BsmCmdGetMicsConfig(UINT cmdLen)
{
    /* check command packet length */
    if (cmdLen != 0) return(BsmCmdLenErr());
    
    /* send reply for successful command (MICS config) */
    return(BsmSendReply(&BsmPub.micsConfig, sizeof(BsmPub.micsConfig)));
}

static RSTAT
BsmCmdSetMicsConfig(UINT cmdLen)
{
    /* Check command packet length. Note the command length can be less than
     * the current size of BsmPub.micsConfig (BSM_MICS_CONFIG). That way, if
     * new fields are added to the end of BSM_MICS_CONFIG, this interface will
     * remain compatible with older software on the PC (the fields added to the
     * end of the config will not be affected by the command, and will simply
     * be left as is).
     */
    if (cmdLen > sizeof(BsmPub.micsConfig)) return(BsmCmdLenErr());
    
    /* If MICS chip is active, abort before receiving the new MICS config (to
     * ensure the MICS chip is idle while BsmPub.micsConfig is overwritten).
     */
    if (BsmPub.micsState > BSM_IDLE) BsmAbortMics();

    /* receive new MICS config (up to command length) */
    if (BsmReceive(&BsmPub.micsConfig, cmdLen)) return(-1);
    
    #ifndef BSM_ALLOW_COMPANY_ID
        if (BsmPub.micsConfig.companyId != 1) {
            BsmErr(BSM_APP_ERR_NOT_ALLOWED_TO_CHANGE_COMPANY_ID, NULL);
            BsmPub.micsConfig.companyId = 1;
        }
    #endif

    /* ensure ZL7010X is awake (does nothing if already awake) */
    BsmMicsWakeup();

    /* update MICS config */
    BsmConfigMics(&BsmPub.micsConfig);
    
    /* send reply for successful command */
    return(BsmSendReply(NULL, 0));
}

static RSTAT
BsmCmdStartSession(UINT cmdLen)
{
    BSM_START_SESSION_CMD *cmd;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(BsmCmdLenErr());
    
    /* set pointer to command buf and receive rest of command packet */
    cmd = &BsmCmdPriv.cmdBuf.startSessionCmd;
    if (BsmReceive(cmd, sizeof(*cmd))) return(-1);
    
    /* ensure ZL7010X is awake (does nothing if already awake) */
    BsmMicsWakeup();

    /* start session (transmit wakeup & start session when response received) */
    if (BsmStartSession(cmd->enabWatchdog, cmd->timeoutSec * 1000)) return(-1);
        
    /* send reply for successful command */
    return(BsmSendReply(NULL, 0));
}

static RSTAT
BsmCmdSearchForImplant(UINT cmdLen)
{
    BSM_SEARCH_FOR_IMPLANT_CMD *cmd;
    BSM_MICS_CONFIG *mc = &BsmPub.micsConfig;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(BsmCmdLenErr());
    
    /* set pointer to command buf and receive rest of command packet */
    cmd = &BsmCmdPriv.cmdBuf.searchForImplantCmd;
    if (BsmReceive(cmd, sizeof(*cmd))) return(-1);
    
    #ifndef BSM_ALLOW_COMPANY_ID
        /* If "anyCompany" is true and the base station is using 2.45 GHz
         * wakeup, fail to prevent BsmSearchForImplant() from using company
         * ID 0 (wildcard) to search for implants (the ADK only allows company
         * ID = 1). Note if the base station is using 400 MHz wakeup, this is
         * not an issue because BsmSearchForImplant() won't use company ID 0
         * to search for implants (not supported by 400 MHz wakeup) .
         */
        if (cmd->anyCompany && !(mc->flags & BSM_400_MHZ_WAKEUP)) {

            BsmErr(BSM_APP_ERR_NOT_ALLOWED_TO_CHANGE_COMPANY_ID, NULL);
            return(-1);
        }
    #endif

    /* ensure ZL7010X is awake (does nothing if already awake) */
    BsmMicsWakeup();

    /* start search for implant (TX wakeup & place responses in MICS RX buf) */
    BsmSearchForImplant(cmd->enabWatchdog, cmd->anyCompany, cmd->anyImplant);
    
    /* send reply for successful command */
    return(BsmSendReply(NULL, 0));
}

static RSTAT
BsmCmdListenForEmergency(UINT cmdLen)
{
    BSM_LISTEN_FOR_EMERGENCY_CMD *cmd;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(BsmCmdLenErr());
    
    /* set pointer to command buf and receive rest of command packet */
    cmd = &BsmCmdPriv.cmdBuf.listenForEmergencyCmd;
    if (BsmReceive(cmd, sizeof(*cmd))) return(-1);
    
    /* ensure ZL7010X is awake (does nothing if already awake) */
    BsmMicsWakeup();

    /* start listening for emergency transmissions */
    BsmListenForEmergency(cmd->startSession);
    
    /* send reply for successful command */
    return(BsmSendReply(NULL, 0));
}

static RSTAT
BsmCmdAbortMics(UINT cmdLen)
{
    /* check command packet length */
    if (cmdLen != 0) return(BsmCmdLenErr());
    
    /* if ZL7010X is awake, abort any ZL7010X operation */
    if (BsmPub.micsState != BSM_SLEEPING) BsmAbortMics();
    
    /* send reply for successful command */
    return(BsmSendReply(NULL, 0));
}

static RSTAT
BsmCmdReadMicsReg(UINT cmdLen)
{
    BSM_READ_MICS_REG_CMD *cmd;
    BSM_READ_MICS_REG_REPLY *rep;
    int retVal;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(BsmCmdLenErr());
    
    /* set pointer to command buf & receive rest of command */
    cmd = &BsmCmdPriv.cmdBuf.readMicsRegCmd;
    if (BsmReceive(cmd, cmdLen)) return(-1);
    
    /* set pointer to reply buf */
    rep = &BsmCmdPriv.tempBuf.readMicsRegReply;
    
    /* ensure ZL7010X is awake (does nothing if already awake) */
    BsmMicsWakeup();

    /* if reading register in remote MICS chip (on implant) */
    if (cmd->remote) {
        
        /* read register in remote MICS chip (on implant) */
        if ((retVal = MicsReadR(cmd->regAddr, TRUE, 250)) < 0) return(-1);
        rep->regVal = (UD8)retVal;
        
    } else { /* else, read register in local MICS chip (on base station) */
        
        rep->regVal = (UD8)MicsRead(cmd->regAddr);
    }
    
    /* send reply for successful command */
    return(BsmSendReply(rep, sizeof(*rep)));
}

static RSTAT
BsmCmdWriteMicsReg(UINT cmdLen)
{
    BSM_WRITE_MICS_REG_CMD *cmd;
    UINT regAddr;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(BsmCmdLenErr());
    
    /* set pointer to command buf & receive rest of command */
    cmd = &BsmCmdPriv.cmdBuf.writeMicsRegCmd;
    if (BsmReceive(cmd, cmdLen)) return(-1);
    
    #ifndef BSM_ALLOW_COMPANY_ID
        if ((cmd->regAddr == MAC_COMPANYID) && (cmd->regVal != 1)) {
            BsmErr(BSM_APP_ERR_NOT_ALLOWED_TO_CHANGE_COMPANY_ID, NULL);
            return(-1);
        }
    #endif

    /* ensure ZL7010X is awake (does nothing if already awake) */
    BsmMicsWakeup();

    /* if writing to register in remote MICS chip (on implant) */
    if (cmd->remote) {
        
        /* write to register in ZL7010X on remote implant */
        if (MicsWriteR(cmd->regAddr, cmd->regVal, TRUE, 250)) return(-1);
        
    } else { /* else, writing to register in local MICS chip (on base station) */
        
        /* If changing register that might affect the link status, set status
         * flag so change will be reported to application on PC (see BSM_STAT).
         */
        regAddr = cmd->regAddr;
        if (((regAddr >= RXBUFF_BSIZE) && (regAddr <= TXBUFF_MAXPACKSIZE)) ||
            ((regAddr >= MAC_IMDTRANSID1) && (regAddr <= MAC_CHANNEL))) {
            
            BsmPub.statFlags |= BSM_LINK_STAT_CHANGED;
        }
        
        /* write to register in local MICS chip (on base station) */
        MicsWrite(cmd->regAddr, cmd->regVal);
    }
        
    /* send reply for successful command */
    return(BsmSendReply(NULL, 0));
}

static RSTAT
BsmCmdTransmitMics(UINT cmdLen)
{
    UINT blockSize, bytesLeft;
    
    /* ensure ZL7010X is awake (does nothing if already awake) */
    BsmMicsWakeup();

    /* init number of bytes to transmit */
    bytesLeft = cmdLen;

    /* get current TX block size (set via BsmSetTxBlockSize()) */
    blockSize = BsmPub.txBlockSize;
    
    /* receive data from command interface and write it to MICS TX buf */
    while(bytesLeft >= blockSize) {
        
        /* receive a block from command interface & update bytes left to do */
        if (BsmReceive(BsmCmdPriv.tempBuf.data, blockSize)) return(-1);
        bytesLeft -= blockSize;
            
        /* Write block to MICS TX buf and fail if it times out (also receive
         * and discard remainder of packet to keep communication synchronized).
         */
        if (BsmTransmitMics(BsmCmdPriv.tempBuf.data, blockSize, 4000) !=
            blockSize) {
            
            BsmErr(BSM_APP_ERR_MICS_DATA_TX_TIMEOUT, NULL);
            BsmReceiveAndDiscard(bytesLeft);
            return(-1);
        }
        /* update data status and set flag to report it to PC (see BSM_STAT) */
        ++BsmPub.dataStat.txBlockCount;
        BsmPub.statFlags |= BSM_DATA_STAT_CHANGED;
    }
    
    /* If length wasn't a multiple of TX block size, fail (also receive and
     * discard remainder of packet to keep communication synchronized).
     */
    if (bytesLeft != 0) {
        BsmErr(BSM_APP_ERR_INVALID_TX_LEN, "%d\f%d", cmdLen, blockSize);
        BsmReceiveAndDiscard(bytesLeft);
        return(-1);
    }
    
    /* send reply for successful command */
    return(BsmSendReply(NULL, 0));
}

static RSTAT
BsmCmdReceiveMics(UINT cmdLen)
{
    BSM_RECEIVE_MICS_CMD *cmd;
    UINT blockSize, bytesAvail, bytesLeft;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(BsmCmdLenErr());
    
    /* set pointer to command buf and receive rest of command packet */
    cmd = &BsmCmdPriv.cmdBuf.receiveMicsCmd;
    if (BsmReceive(cmd, sizeof(*cmd))) return(-1);

    /* ensure ZL7010X is awake (does nothing if already awake) */
    BsmMicsWakeup();

    /* get current RX block size (set via BsmSetRxBlockSize()) */
    blockSize = BsmPub.rxBlockSize;
    
    /* get number of bytes available in MICS RX buf */
    bytesAvail = MicsRead(RXBUFF_USED) * blockSize;
    
    /* If specified maximum length is < bytes available, set bytesLeft to the
     * next smaller multiple of the block size (note this usually shouldn't
     * happen since the maximum length will usually be > bytes available).
     * Otherwise, set bytesLeft = bytesAvail to read all available data.
     */
    if (cmd->maxLen < bytesAvail) {
        bytesLeft = (cmd->maxLen / blockSize) * blockSize;
    } else {
        bytesLeft = bytesAvail;
    }
    
    /* send reply packet header for successful command (replyLen = bytesLeft) */
    if (BsmSendReplyHdr(COM_OK_REPLY_TYPE, bytesLeft)) return(-1);
    
    /* read data from MICS chip and send it to reply interface */
    while(bytesLeft >= blockSize) {
        
        /* Read a block from from MICS chip & update bytes left to do (no need
         * to check the return value since we know the bytes are available).
         */
        (void)BsmReceiveMics(BsmCmdPriv.tempBuf.data, blockSize, 0);
        bytesLeft -= blockSize;
        
        /* send block to reply interface (continuation of reply data) */
        if (BsmSendReplyData(BsmCmdPriv.tempBuf.data, blockSize)) return(-1);
        
        /* update data status and set flag to report it to PC (see BSM_STAT) */
        ++BsmPub.dataStat.rxBlockCount;
        BsmPub.statFlags |= BSM_DATA_STAT_CHANGED;
    }
    
    /* If all of the available data was read, re-enable IRQ_RXNOTEMPTY. That
     * way, if BsmPeriodicLedTasks() automatically turns off the 400 MHz RX
     * LED the next time it's called, and more data is received after that,
     * IRQ_RXNOTEMPTY will recur and turn the LED back on again (see
     * BsmMicsRxNotEmptyIsr()). Note if the ZL7010X has already received more
     * data, it will immediately reassert IRQ_RXNOTEMPTY, which is ok.
     */
    if (cmd->maxLen >= bytesAvail) {
        /* clear IRQ_RXNOTEMPTY so it won't recur if RX buf is now empty */
        MicsWrite(IRQ_RAWSTATUS1, ~IRQ_RXNOTEMPTY);
        MicsWrite(IRQ_ENABLESET1, IRQ_RXNOTEMPTY);
    }
    
    return(0);
}

static RSTAT
BsmCmdMicsCal(UINT cmdLen)
{
    BSM_MICS_CAL_CMD *cmd = &BsmCmdPriv.cmdBuf.micsCalCmd;
    UINT cals;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) { 
        
        /* if only missing calSelect2, use 0 (for backwards compatibility) */
        if (cmdLen == (sizeof(*cmd) - 1)) {
            cmd->calSelect2 = 0;
        } else {
            return(BsmCmdLenErr());
        }
    }
        
    /* receive rest of command packet */
    if (BsmReceive(cmd, cmdLen)) return(-1);
    
    /* form calibration selection */
    cals = (cmd->calSelect2 << 8) | cmd->calSelect1;

    /* ensure ZL7010X is awake (does nothing if already awake) */
    BsmMicsWakeup();
    
    /* if doing calibration on remote MICS chip (implant) */
    if (cmd->remote) {
        
        /* perform specified calibrations on remote MICS chip (implant) */
        if (MicsCalR(cals, TRUE)) return(-1);
        
    } else { /* else, doing calibration on local MICS chip (base station) */
        
        /* if doing CAL_400_MHZ_ANT and a channel is specified */
        if ((cals & CAL_400_MHZ_ANT) && (cmd->chan >= 0)) {
            
            /* if specified new channel, abort any MICS operation & set chan */
            if (cmd->chan != MicsRead(MAC_CHANNEL)) BsmChangeChan(cmd->chan);
        }
        
        /* perform specified calibrations on local MICS chip (base station) */
        MicsCal(cals);
    }
    
    /* send reply for successful command */
    return(BsmSendReply(NULL, 0));
}

static RSTAT
BsmCmdMicsAdc(UINT cmdLen)
{
    BSM_MICS_ADC_CMD *cmd;
    BSM_MICS_ADC_REPLY *rep;
    int retVal;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(BsmCmdLenErr());
    
    /* set pointer to command buf and receive rest of command packet */
    cmd = &BsmCmdPriv.cmdBuf.micsAdcCmd;
    if (BsmReceive(cmd, sizeof(*cmd))) return(-1);
    
    /* set pointer to reply buf */
    rep = &BsmCmdPriv.tempBuf.micsAdcReply;
    
    /* ensure ZL7010X is awake (does nothing if already awake) */
    BsmMicsWakeup();

    /* if doing A/D conversion on remote MICS chip (implant) */
    if (cmd->remote) {
        
        /* do A/D conversion on remote MICS chip (implant) */
        if ((retVal = MicsAdcR(cmd->adcInput, TRUE)) < 0) return(-1);
        rep->adcResult = (UD8)retVal;
        
    } else { /* else, do A/D on local MICS chip (base station) */
        
        rep->adcResult = (UD8)MicsAdc(cmd->adcInput);
    }
    
    /* send reply for successful command */
    return(BsmSendReply(rep, sizeof(*rep)));
}

static RSTAT
BsmCmdSetTx245Power(UINT cmdLen)
{
    BSM_SET_TX_245_POWER_CMD *cmd;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(BsmCmdLenErr());
    
    /* set pointer to command buf and receive rest of command packet */
    cmd = &BsmCmdPriv.cmdBuf.setTx245PowerCmd;
    if (BsmReceive(cmd, sizeof(*cmd))) return(-1);
    
    /* set power level for 2.45 GHz TX */
    BsmSetTx245Power(cmd->ccPa);
    
    /* send reply for successful command */
    return(BsmSendReply(NULL, 0));
}

static RSTAT
BsmCmdStartTx245Carrier(UINT cmdLen)
{
    BSM_START_TX_245_CARRIER_CMD *cmd;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(BsmCmdLenErr());
    
    /* set pointer to command buf and receive rest of command packet */
    cmd = &BsmCmdPriv.cmdBuf.startTx245CarrierCmd;
    if (BsmReceive(cmd, sizeof(*cmd))) return(-1);
    
    /* ensure ZL7010X is awake (does nothing if already awake) */
    BsmMicsWakeup();

    /* start or stop transmitting 2.45 GHz carrier */
    BsmSetTx245Mode(cmd->start ? BSM_TX_245_CARRIER : BSM_TX_245_OFF);
    
    /* send reply for successful command */
    return(BsmSendReply(NULL, 0));
}

static RSTAT
BsmCmdStartTx400Carrier(UINT cmdLen)
{
    BSM_START_TX_400_CARRIER_CMD *cmd;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(BsmCmdLenErr());
    
    /* set pointer to command buf and receive rest of command packet */
    cmd = &BsmCmdPriv.cmdBuf.startTx400CarrierCmd;
    if (BsmReceive(cmd, sizeof(*cmd))) return(-1);
    
    /* ensure ZL7010X is awake (does nothing if already awake) */
    BsmMicsWakeup();

    /* start or stop transmitting 400 MHz TX carrier (continuous wave) */
    BsmStartTx400Carrier(cmd->start, cmd->chan);
    
    /* send reply for successful command */
    return(BsmSendReply(NULL, 0));
}

static RSTAT
BsmCmdRssi(UINT cmdLen)
{
    BSM_RSSI_CMD *cmd;
    BSM_RSSI_REPLY *rep;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(BsmCmdLenErr());
    
    /* set pointer to command buf and receive rest of command packet */
    cmd = &BsmCmdPriv.cmdBuf.rssiCmd;
    if (BsmReceive(cmd, sizeof(*cmd))) return(-1);
    
    /* set pointer to reply buf */
    rep = &BsmCmdPriv.tempBuf.rssiReply;
    
    /* ensure ZL7010X is awake (does nothing if already awake) */
    BsmMicsWakeup();

    /* if specified new channel, abort any MICS op & change it */
    if (cmd->chan != MicsRead(MAC_CHANNEL)) BsmChangeChan(cmd->chan);
    
    /* If "use internal RSSI" is specified (factory test/debug use only),
     * perform an internal RSSI (note that the "average" option is ignored in
     * this case because the internal RSSI always averages over 10 ms).
     * Otherwise, perform an external RSSI (either the max RSSI detected over
     * 10 ms, or the average RSSI over 10 ms).
     */
    if (cmd->useIntRssi) {
        rep->rssi = BsmIntRssi();
    } else {
        rep->rssi = BsmExtRssi(cmd->average);
    }
    
    /* send reply for successful command */
    return(BsmSendReply(rep, sizeof(*rep)));
}

static RSTAT
BsmCmdEnabExtRssi(UINT cmdLen)
{
    BSM_ENAB_EXT_RSSI_CMD *cmd;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(BsmCmdLenErr());
    
    /* set pointer to command buf and receive rest of command packet */
    cmd = &BsmCmdPriv.cmdBuf.enabExtRssiCmd;
    if (BsmReceive(cmd, sizeof(*cmd))) return(-1);
    
    /* ensure ZL7010X is awake (does nothing if already awake) */
    BsmMicsWakeup();

    /* if enabling external RSSI */
    if (cmd->enab) {
        
        /* turn on 400 MHz RX LED & tell BsmPeriodicLedTasks() to leave it on */
        BsmRx400LedOn(BSM_NO_AUTO_OFF);
        
        /* if specified new channel, abort any MICS op & change it */
        if (cmd->chan != MicsRead(MAC_CHANNEL)) BsmChangeChan(cmd->chan);
        
        /* if ZL7010X idle, enable synth & RX, then give synth time to lock */
        if (BsmPub.micsState == BSM_IDLE) {
            MicsWrite(RF_GENENABLES, RX_RF | RX_IF | SYNTH);
            StDelayUs(MICS_LONG_SYNTH_LOCK_TIME_US);
        }
        
        /* enable external RSSI */
        (void)MicsEnabExtRssi(TRUE);
        
    } else {
        
        /* turn off 400 MHz RX LED */
        BsmRx400LedOff();
        
        /* if MICS chip is idle, disable RX */
        if (BsmPub.micsState == BSM_IDLE) MicsWrite(RF_GENENABLES, 0);
        
        /* disable external RSSI */
        (void)MicsEnabExtRssi(FALSE);
    }
    
    /* send reply for successful command */
    return(BsmSendReply(NULL, 0));
}

static RSTAT
BsmCmdCca(UINT cmdLen)
{
    BSM_CCA_CMD *cmd;
    BSM_CCA_REPLY *rep;
    UINT prevChan;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(BsmCmdLenErr());
    
    /* set pointer to command buf and receive rest of command packet */
    cmd = &BsmCmdPriv.cmdBuf.ccaCmd;
    if (BsmReceive(cmd, sizeof(*cmd))) return(-1);
    
    /* set pointer to reply buf */
    rep = &BsmCmdPriv.tempBuf.ccaReply;
    
    /* ensure ZL7010X is awake (does nothing if already awake) */
    BsmMicsWakeup();

    /* save previous channel, do CCA, then restore previous channel */
    prevChan = MicsRead(MAC_CHANNEL);
    rep->clearChan = (UD8)BsmCca(cmd->average, &rep->data);
    MicsWrite(MAC_CHANNEL, prevChan);
    
    /* send reply for successful command */
    return(BsmSendReply(rep, sizeof(*rep)));
}

/* Kept for backwards compatibility (now queried via BsmCmdGetStatChanges()).
 */
static RSTAT
BsmCmdGetLinkStat(UINT cmdLen)
{
    /* ensure ZL7010X is awake (does nothing if already awake) */
    BsmMicsWakeup();

    BsmPub.statFlags &= ~BSM_LINK_STAT_CHANGED;
    BsmGetLinkStat(&BsmCmdPriv.tempBuf.linkStat);
    return(BsmSendReply(&BsmCmdPriv.tempBuf.linkStat, sizeof(MICS_LINK_STAT)));
}

/* Kept for backwards compatibility (now queried via BsmCmdGetStatChanges()).
 */
static RSTAT
BsmCmdGetLinkQual(UINT cmdLen)
{
    if (BsmPub.micsState > BSM_IDLE) BsmUpdateLinkQual();
    BsmPub.statFlags &= ~BSM_LINK_QUAL_CHANGED;
    if (BsmSendReply(&BsmPub.linkQual, sizeof(BsmPub.linkQual))) return(-1);
    BsmPub.linkQual.crcErrs = 0;
    BsmPub.linkQual.errCorBlocks = 0;
    BsmPub.linkQual.maxBErrInts = FALSE;
    BsmPub.linkQual.maxRetriesInts = FALSE;
    return(0);
}

static RSTAT
BsmCmdGetTraceMsg(UINT cmdLen)
{
    const char *msg;
    UINT msgSize;
    
    /* check command packet length */
    if (cmdLen != 0) return(BsmCmdLenErr());
    
    /* if tracing is enabled, get next trace message */
    if (TRACE_ENAB) {
        if ((msg = TraceGetNext()) == NULL) msg = "";
        msgSize = strlen(msg) + 1;
    } else {
        msg = "";
        msgSize = sizeof("");
    }
    /* send reply for successful command (message string including '\0') */
    return(BsmSendReply(msg, msgSize));
}

static RSTAT
BsmCmdCopyMicsRegs(UINT cmdLen)
{
    BSM_COPY_MICS_REGS_CMD *cmd;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(BsmCmdLenErr());
    
    /* set pointer to command buf & receive rest of command */
    cmd = &BsmCmdPriv.cmdBuf.copyMicsRegsCmd;
    if (BsmReceive(cmd, cmdLen)) return(-1);
    
    /* ensure ZL7010X is awake (does nothing if already awake) */
    BsmMicsWakeup();

    /* if executing ZL7010X "copy registers" on remote implant */
    if (cmd->remote) {
        
        /* execute ZL7010X "copy registers" on remote implant */
        if (MicsCopyRegsR(TRUE)) return(-1);
        
    } else { /* else, execute ZL7010X "copy registers" on base */
        
        MicsCopyRegs();
    }
    
    /* send reply for successful command */
    return(BsmSendReply(NULL, 0));
}

static RSTAT
BsmCmdGetStatChanges(UINT cmdLen)
{
    BSM_CMD_PRIV *bc = &BsmCmdPriv;
    BSM_GET_STAT_CHANGES_CMD *cmd;
    UINT packLen, statFlags;
    
    /* set pointer to command buf */
    cmd = &BsmCmdPriv.cmdBuf.getStatChangesCmd;
    
    /* If command packet includes any argument(s), check command length and
     * receive rest of command packet. Otherwise, use defaults.
     */
    if (cmdLen) {
        if (cmdLen != sizeof(*cmd)) return(BsmCmdLenErr());
        if (BsmReceive(cmd, sizeof(*cmd))) return(-1);
    } else {
        cmd->force = FALSE;
    }
    
    /* if ZL7010X is active, update link quality so we'll report the latest */
    if (BsmPub.micsState > BSM_IDLE) BsmUpdateLinkQual();
    
    /* If specified "force", force status flags so we'll also report sections
     * of the status that haven't changed. Note that when BsmGetGenStat() is
     * called below, it clears BSM_LINK_STAT_CHANGED if the ZL7010X is sleeping
     * since the link status isn't available (so we won't report an invalid
     * link status to the PC).
     */
    if (cmd->force) {
        BsmPub.statFlags |= (BSM_LINK_STAT_CHANGED | BSM_LINK_QUAL_CHANGED |
            BSM_DATA_STAT_CHANGED);
    }
    /* Get general status in temp buf, then make copy of status flags (so it
     * will remain valid after temp buf is reused later in this function).
     */
    BsmGetGenStat(&bc->tempBuf.genStat);
    statFlags = bc->tempBuf.genStat.flags;
    
    /* calc reply packet length and send reply packet header */
    packLen = (1 + sizeof(BSM_STAT));
    if (statFlags & BSM_LINK_STAT_CHANGED) {
        packLen += (1 + sizeof(MICS_LINK_STAT));
    }
    if (statFlags & BSM_LINK_QUAL_CHANGED) {
        packLen += (1 + sizeof(MICS_LINK_QUAL));
    }
    if (statFlags & BSM_DATA_STAT_CHANGED) {
        packLen += (1 + sizeof(MICS_DATA_STAT));
    }
    if (BsmSendReplyHdr(COM_OK_REPLY_TYPE, packLen)) return(-1);
    
    /* send general status */
    if (BsmSendDataWithLen(&bc->tempBuf.genStat, sizeof(BSM_STAT))) return(-1);
    
    /* if needed, send link status */
    if (statFlags & BSM_LINK_STAT_CHANGED) {
        
        /* Clear BSM_LINK_STAT_CHANGED to indicate the link status hasn't
         * changed since the last time it was reported.
         */
        BsmPub.statFlags &= ~BSM_LINK_STAT_CHANGED;
        
        /* send link status */
        BsmGetLinkStat(&bc->tempBuf.linkStat);
        if (BsmSendDataWithLen(&bc->tempBuf.linkStat, sizeof(MICS_LINK_STAT))) {
            return(-1);
        }
    }
    
    /* if needed, send link quality */
    if (statFlags & BSM_LINK_QUAL_CHANGED) {
        
        /* Clear BSM_LINK_QUAL_CHANGED to indicate the link quality hasn't
         * changed since the last time it was reported.
         */
        BsmPub.statFlags &= ~BSM_LINK_QUAL_CHANGED;
        
        /* send link quality, then clear it */
        if (BsmSendDataWithLen(&BsmPub.linkQual, sizeof(MICS_LINK_QUAL))) return(-1);
        BsmPub.linkQual.crcErrs = 0;
        BsmPub.linkQual.errCorBlocks = 0;
        BsmPub.linkQual.maxBErrInts = FALSE;
        BsmPub.linkQual.maxRetriesInts = FALSE;
    }
    
    /* if needed, send data status */
    if (statFlags & BSM_DATA_STAT_CHANGED) {
        
        /* Clear BSM_DATA_STAT_CHANGED to indicate the data status hasn't
         * changed since the last time it was reported.
         */
        BsmPub.statFlags &= ~BSM_DATA_STAT_CHANGED;
        
        /* send data status, then clear it */
        if (BsmSendDataWithLen(&BsmPub.dataStat, sizeof(MICS_DATA_STAT))) return(-1);
        BsmPub.dataStat.txBlockCount = 0;
        BsmPub.dataStat.rxBlockCount = 0;
        BsmPub.dataStat.dataErrCount = 0;
    }
    
    return(0);
}

static RSTAT
BsmCmdStartDataTest(UINT cmdLen)
{
    BSM_START_DATA_TEST_CMD *cmd;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(BsmCmdLenErr());
    
    /* set pointer to command buf and receive rest of command packet */
    cmd = &BsmCmdPriv.cmdBuf.startDataTestCmd;
    if (BsmReceive(cmd, sizeof(*cmd))) return(-1);
    
    /* ensure ZL7010X is awake (does nothing if already awake) */
    BsmMicsWakeup();

    /* start data test */
    BsmStartDt(cmd);
        
    /* send reply for successful command */
    return(BsmSendReply(NULL, 0));
}

static RSTAT
BsmCmdSetTx245Freq(UINT cmdLen)
{
    BSM_SET_TX_245_FREQ_CMD *cmd;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(BsmCmdLenErr());
    
    /* set pointer to command buf and receive rest of command packet */
    cmd = &BsmCmdPriv.cmdBuf.setTx245FreqCmd;
    if (BsmReceive(cmd, sizeof(*cmd))) return(-1);
    
    /* set CC25XX frequency control (for 2.45 GHz wakeup) */
    BsmSetTx245Freq(cmd->ccFreq2, cmd->ccFreq1, cmd->ccFreq0);
        
    /* send reply for successful command */
    return(BsmSendReply(NULL, 0));
}

static RSTAT
BsmCmdResetMics(UINT cmdLen)
{
    /* check command packet length */
    if (cmdLen != 0) return(BsmCmdLenErr());

    /* If the ZL7010X is sleeping, wake it up. Since we reset the ZL7010X right
     * after this, just call MicsWakeup() instead of BsmMicsWakeup().
     */
    if (BsmPub.micsState == BSM_SLEEPING) MicsWakeup();

    /* reset & re-initialize ZL7010X & CC25XX, & put CC25XX back to sleep */
    BsmResetMics(TRUE);
    BsmResetCc(TRUE);
    CcCommand(CC_SPWD);

    /* send reply for successful command */
    return(BsmSendReply(NULL, 0));
}

static RSTAT
BsmCmdCalcMicsCrc(UINT cmdLen)
{
    BSM_CALC_MICS_CRC_CMD *cmd;

    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(BsmCmdLenErr());

    /* set pointer to command buf & receive rest of command */
    cmd = &BsmCmdPriv.cmdBuf.calcMicsCrcCmd;
    if (BsmReceive(cmd, cmdLen)) return(-1);

    /* ensure ZL7010X is awake (does nothing if already awake) */
    BsmMicsWakeup();

    /* if calculating CRC for ZL7010X on remote implant */
    if (cmd->remote) {

        /* calculate CRC for ZL7010X on remote implant */
        if (MicsCalcCrcR(TRUE)) return(-1);

    } else { /* else, calculate CRC for ZL7010X on base */

        MicsCalcCrc();
    }

    /* send reply for successful command */
    return(BsmSendReply(NULL, 0));
}

static RSTAT
BsmCmdWriteCcReg(UINT cmdLen)
{
    BSM_WRITE_CC_REG_CMD *cmd;

    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(BsmCmdLenErr());

    /* set pointer to command buf and receive rest of command packet */
    cmd = &BsmCmdPriv.cmdBuf.writeCcRegCmd;
    if (BsmReceive(cmd, sizeof(*cmd))) return(-1);

    /* Write to CC25XX register. Registers from address 0x30 and 0x3d are
     * command strobes which don't require a register value.
     */
    if ((cmd->regAddr >= CC_SRES) && (cmd->regAddr <= CC_SNOP)) {
        CcCommand(cmd->regAddr);
    } else {
        CcWrite(cmd->regAddr, cmd->regVal);
    }

    /* send reply for successful command */
    return(BsmSendReply(NULL, 0));
}

static RSTAT
BsmCmdReadCcReg(UINT cmdLen)
{
    BSM_READ_CC_REG_CMD *cmd;
    BSM_READ_CC_REG_REPLY *rep;

    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(BsmCmdLenErr());

    /* set pointer to command buf & receive rest of command */
    cmd = &BsmCmdPriv.cmdBuf.readCcRegCmd;
    if (BsmReceive(cmd, cmdLen)) return(-1);

    /* set pointer to reply buf */
    rep = &BsmCmdPriv.tempBuf.readCcRegReply;

    /* Read CC25XX register. Registers above address 0x2F (e.g. PATABLE) have
     * to be read in burst mode even though only one byte is read.
     */
    if (cmd->regAddr > 0x2f) {
        CcReadBurst(cmd->regAddr, &rep->regVal, 1);
    } else {
        rep->regVal = (UD8)CcRead(cmd->regAddr);
    }

    /* send reply for successful command */
    return(BsmSendReply(rep, sizeof(*rep)));
}

static RSTAT
BsmCmdWakeup(UINT cmdLen)
{
    /* check command packet length */
    if (cmdLen != 0) return(BsmCmdLenErr());

    /* if the ZL7010X is sleeping, wake it up
     * BsmMicsWakeup() checks the current state of the MICS
     */
    BsmMicsWakeup();

    /* send reply for successful command */
    return(BsmSendReply(NULL, 0));
}

static RSTAT
BsmCmdSleep(UINT cmdLen)
{
    /* check command packet length */
    if (cmdLen != 0) return(BsmCmdLenErr());

    /* if the ZL7010X is sleeping, wake it up
     * BsmMicsSleep() checks the current state of the MICS
     */
    BsmMicsSleep();

    /* send reply for successful command */
    return(BsmSendReply(NULL, 0));
}

static void
BsmGetGenStat(BSM_STAT *gs)
{
    /* get current state of ZL7010X */
    gs->micsState = BsmPub.micsState;
    
    /* If the ZL7010X is sleeping, the link status (MICS_LINK_STAT) isn't
     * available, so make sure BSM_LINK_STAT_CHANGED is clear so we won't
     * report an invalid link status to the PC.
     */
    if (gs->micsState == BSM_SLEEPING) BsmPub.statFlags &= ~BSM_LINK_STAT_CHANGED;

    /* Get the status flags to report to the PC, then clear any status flags
     * corresponding to LED's that have been turned off. The "LED on" functions
     * set status flags when the LED's are turned on (see BsmAppLed.c), and the
     * flags remain set until they're reported to the PC (to ensure the GUI
     * will turn on its LED's). If the LED's have subsequently been turned off,
     * the status flags are cleared here so they'll be clear the next time the
     * PC gets the status (so the GUI will turn its LED's off).
     */
    gs->flags = BsmPub.statFlags;
    /**/
    if (BsmTx400LedIsOff()) BsmPub.statFlags &= ~BSM_400_MHZ_TX_ACTIVE;
    if (BsmRx400LedIsOff()) BsmPub.statFlags &= ~BSM_400_MHZ_RX_ACTIVE;
    if (BsmTx245LedIsOff()) BsmPub.statFlags &= ~BSM_245_GHZ_TX_ACTIVE;
    /* 
     * If an error is lacthed, set error flag.
     */
    if ((ErrGetFirst())->errCode) gs->flags |= BSM_ERR_OCCURRED;
}

static void
BsmGetLinkStat(MICS_LINK_STAT *ls)
{
    /* get link status */
    ls->companyId = MicsRead(MAC_COMPANYID);
    ls->imdTid.b1 = MicsRead(MAC_IMDTRANSID1);
    ls->imdTid.b2 = MicsRead(MAC_IMDTRANSID2);
    ls->imdTid.b3 = MicsRead(MAC_IMDTRANSID3);
    ls->chan = MicsRead(MAC_CHANNEL);
    ls->modUser = MicsRead(MAC_MODUSER); /* RF modulation (RX & TX), etc */
    ls->txBlockSize = MicsRead(TXBUFF_BSIZE);
    ls->rxBlockSize = MicsRead(RXBUFF_BSIZE) & RXBUFF_BSIZE_MASK;
    ls->maxBlocksPerTxPack = MicsRead(TXBUFF_MAXPACKSIZE);
}

static RSTAT
BsmSendDataWithLen(const void *data, UD8 len)
{
    /* send length of data first */
    if (BsmSendReplyData(&len, 1)) return(-1);
    
    /* send data */ 
    return(BsmSendReplyData(data, len));
}

static void
BsmChangeChan(UINT chan)
{
    /* if MICS chip is active, abort operation */
    if (BsmPub.micsState > BSM_IDLE) BsmAbortMics();
    
    /* set channel and status flag so it will be reported to the PC */
    MicsWrite(MAC_CHANNEL, chan);
    BsmPub.statFlags |= BSM_LINK_STAT_CHANGED;
}

static RSTAT
BsmCmdGetFactNvm(UINT cmdLen)
{
    /* check command packet length */
    if (cmdLen != 0) return(BsmCmdLenErr());

    /* reply with contents of factory NVM */
    return(BsmSendReply(MicsFactNvm, sizeof(BSM_MICS_FACT_NVM)));
}

static RSTAT
BsmCmdLenErr(void)
{
    /* Since the received command packet length didn't match the expected
     * length, the communication may not be synchronized on packet boundaries.
     * To get it synchronized again, receive and discard bytes until it
     * times out. Note the timeout will also disable the error reply (see
     * BSM_ENAB_ERR_REPLY in BsmAppCom.c), so BsmReceiveAndProcCmdPack() won't
     * send an error reply for the command (it's best not to send an
     * error reply if the communication is out of sync).
     */
    BsmErr(BSM_APP_ERR_RECEIVED_CMD_WITH_INVALID_LEN, NULL);
    BsmReceiveAndDiscard(COM_MAX_PACK_LEN);
    return(-1);
}
