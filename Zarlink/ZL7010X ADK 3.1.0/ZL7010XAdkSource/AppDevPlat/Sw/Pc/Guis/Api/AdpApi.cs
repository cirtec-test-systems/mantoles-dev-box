//
//      Includes software used to interface to the C API (DLL).
//      This file contains the public class AdpApi.
// 
// CLASS: AdpApi
//      The provides the interface to the ADP board for the Power Monitoring
//      functions of the ADP.  There is some duplication with the functions
//      in CommonApi.cs to allow an ADP-only connection to be established
//      for the situation where there is no mezzanine present (as in the
//      calibration of the power monitoring circuit).
// 
// NOTES:
//      Some types (enum, struct) in this class are passed to functions in the
//      C API (DLL). These must match the corresponding types in the API, and
//      must not be altered except as needed to reflect changes in the API.
//      For descriptions of these types, see the corresponding types in the
//      API. Note the names used for these types are generally the same as
//      the names used in the API. 
// 
// Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2012. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.
 
using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;   // For DLL
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Globalization;             // For NumberStyles

namespace Zarlink.Adp.Api
{
    // unsafe public abstract class AdpApi
    unsafe public class AdpApi
    {
        // name of DLL that contains the C API functions
        private const string C_API_DLL = Build.BuildSettings.C_API_DLL;
        //        
        // Declarations for the unmanaged API/DLL functions. Each declaration
        // corresponds to a function in the API/DLL.
        //
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern void EiInitSpecs(out EI_SPECS specs, int specsSize);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern void* EiOpen(ref EI_SPECS specs, int specsSize);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern sbyte* EiGetMsg(void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int EiClose(void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern void AdpInitSpecs(out ADP_SPECS specs, int specsSize);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern void* AdpOpen(ref ADP_SPECS specs, int specsSize, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int AdpClose(void* adpId, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int AdpGetBoardVer(void* adpId, sbyte[] ver, int size, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int AdpStartPowMon(void* adpId, ref ADP_POW_MON_OPTIONS pmo, uint pmoSize, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int AdpStopPowMon(void* adpId, void* e);
        [DllImport(C_API_DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int AdpGetPow(void* adpId, out UNSAFE_ADP_POW_BUF pb, uint pbSize, void* e);

        // these control the way the .init() functions respond
        // when no mezz is found (because it is not attached or
        // it is not powered as for current measurment cal)
        public const bool FAIL_INIT_IF_NO_MEZZ = true;
        public const bool OK_INIT_IF_NO_MEZZ = false;

        // TODO this should be calculated from the message length???
        // This has to match the embedded
        public const int NR_SAMPLES_IN_CURRENT_MSG = 1000;
        
        // The maximum number of samples that will ever be returned in a power monitor
        // buffer (ADP_POW_BUF). Note each sample is 16 bits, so the number of bytes is
        // twice the number of samples.
        //
        private const int ADP_MAX_SAMPS_IN_POW_BUF = 2000;


        // clone in c# of ADP_POW_BUF, must match all fields
        public unsafe struct UNSAFE_ADP_POW_BUF { 
            
            // serial number of buffer being sent or -1 if no data available
            public Int16 dmaSnAvailable;

            // number of samples in ADP_POW_BUF
            public UInt16 nSamps;

            // various power info flags (ADP_MORE_POW_AVAIL, ...)
            public Byte flags;

            // padding to make size of ADP_POW_BUF_INFO a multiple of 2
            public Byte reserved1;

            public fixed Int16 dma_buf_samples[NR_SAMPLES_IN_CURRENT_MSG]; 

    
        }

        // the struct below exists as the clone of the above but
        // without the fixed array so it can live in the unmanaged
        // universe
        // Note some data types change (samples become double)

        // this is the "clone" that works in the managed world
        // and is the unit we write to disk
        [Serializable()]
        public class DLL_ADP_CURRENT_AND_DMA_MANAGED : ISerializable
        {
            // this is the only field that is not in the structure returned by
            // the USB interface for Current DMA.  It is intended to facilitate
            // playback at different rates than recording

            public long qpc_us_after_dma_blk_rcvd;
            public UInt16 current_reading_ch1;
            public UInt16 current_reading_ch2;

            public Int16 dmaSnAvailable;
            public UInt16 dmaTriggeredBlock;
            public Int16 dmaReplyFlags;
            public double[] dma_buf_samples = new double[NR_SAMPLES_IN_CURRENT_MSG];

            // apparently when I define the serialization constructors,
            // I no longer get a default constructor free and have to 
            // define one
            public DLL_ADP_CURRENT_AND_DMA_MANAGED()
            {
                current_reading_ch1 = 0;
                current_reading_ch2 = 0;

                dmaSnAvailable = -1;    // -1 indicates no new data available
                dmaTriggeredBlock = 0;
                dmaReplyFlags = 0;
            }


            //Deserialization constructor.

            public DLL_ADP_CURRENT_AND_DMA_MANAGED(SerializationInfo info, StreamingContext ctxt)
            {
                //Get the values from info and assign them to the appropriate properties

                qpc_us_after_dma_blk_rcvd = (long)info.GetValue("qpc_us_after_dma_blk_rcvd", typeof(long));
                current_reading_ch1 = (UInt16)info.GetValue("current_reading_ch1", typeof(UInt16));
                current_reading_ch2 = (UInt16)info.GetValue("current_reading_ch2", typeof(UInt16));
                dmaSnAvailable = (Int16)info.GetValue("dma_buf_sn", typeof(Int16));
                dmaTriggeredBlock = (UInt16)info.GetValue("dma_reply_status_1", typeof(UInt16));
                dmaReplyFlags = (Int16)info.GetValue("dma_reply_status_2", typeof(Int16));
                dma_buf_samples = (double[])info.GetValue("dma_buf_samples", typeof(double[]));

            }

            //Serialization function.

            public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
            {
                //You can use any custom name for your name-value pair. But make sure you

                // read the values with the same name. For ex:- If you write EmpId as "EmployeeId"

                // then you should read the same with "EmployeeId"

                info.AddValue("qpc_us_after_dma_blk_rcvd", qpc_us_after_dma_blk_rcvd);
                info.AddValue("current_reading_ch1", current_reading_ch1);
                info.AddValue("current_reading_ch2", current_reading_ch2);
                info.AddValue("dma_buf_sn", dmaSnAvailable);
                info.AddValue("dma_reply_status_1", dmaTriggeredBlock);
                info.AddValue("dma_reply_status_2", dmaReplyFlags);
                info.AddValue("dma_buf_samples", dma_buf_samples);


                //info.AddValue("EmployeeId", EmpId);
                //info.AddValue("EmployeeName", EmpName);
            }

        }   // end     public class DLL_ADP_CURRENT_AND_DMA_MANAGED : ISerializable

        private struct ADP_SPECS
        {
            uint magic;
            public bool remote;
            public byte* localMezzDevType;
            public byte* localMezzDevName;
            public uint defTimeoutMs;
            public void* localAdpId;
        }



        // Defines for power monitoring input (ADP_POW_MON_OPTIONS.input):
        // 
        // ADP_POW_NONE:
        //     Setup to monitor power, but don't actually monitor it (for test/debug).
        // ADP_POW_1:
        //     Monitor power for VSUP1.
        // ADP_POW_2:
        //     Monitor power for VSUP2.
        // ADP_POW_1_AND_2:
        //     Monitor power for VSUP1 and VSUP2.
        //
        public const byte ADP_POW_NONE = 0;
        public const byte ADP_POW_1 = 1;
        public const byte ADP_POW_2 = 2;
        public const byte ADP_POW_1_AND_2 = 3;

        // The .init method to needs to know whether to connect
        // or only to init the error objects
        //
        public const byte INIT_ERR_AND_CONNECT = 1;
        public const byte INIT_ERR_DONT_CONNECT = 2;


        // Defines for various power monitoring options (ADP_POW_MON_OPTIONS.options);
        // 
        // ADP_HIGH_RANGE_POW_1:
        //     Set for high range VSUP1 power (clear for low range).
        // ADP_HIGH_RANGE_POW_2:
        //     Set for high range VSUP2 power (clear for low range).
        // ADP_CAL_POW_1:
        //     Set when calibrating VSUP1 power monitor.
        // ADP_CAL_POW_2:
        //     Set when calibrating VSUP2 power monitor.
        //

        private const byte ADP_HIGH_RANGE_POW_1 = (1 << 0);
        private const byte ADP_HIGH_RANGE_POW_2 = (1 << 1);
        private const byte ADP_CAL_POW_1 = (1 << 2);
        private const byte ADP_CAL_POW_2 = (1 << 3);


        private struct ADP_POW_MON_OPTIONS
        {
            // power supply to monitor (ADP_POW_1, ...)
            public byte input;

            // various options (ADP_HIGH_RANGE_POW_1, ...)
            public byte options;

            // number of microsececonds between samples
            public UInt16 usPerSamp;
        }

        private struct EI_SPECS
        {
            public uint magic;
        }

        // set to true if this interface is successfully initialized
        public bool initialized = false;

        // Error information objects for the API/DLL. These are passed to
        // the API functions, which use them to return error information
        // when an error occurs. Note the polling uses its own separate
        // error information object (eiIdPoll) in case it runs in a separate
        // asynchronous thread. That way, if an error happens to occur in both
        // the main processing thread and the status polling thread at the same
        // time, the error information won't conflict.
        //
        protected void* eiId = null;

        protected void* eiIdPoll = null;

        // connection to ADP board (returned by AdpOpen())
        // and then passed from caller (AdpPowMonForm.cs) when it
        // creates the instance of this object
        //protected void* adpId = null;
        public void* adpId = null;

        // we may be operating with only an ADP and no Mezz
        public bool bHaveAdpOnlyConnection = false;

        public AdpApi()
        {
        }
        
        public int AdpQueryAndGetPwrCktData(System.Windows.Forms.TextBox tbForErrors)
        {
            int result;

            // Reply for this command is found in 
            // public DLL_ADP_CURRENT_AND_DMA unsafe_returned_current_and_dma;
            //
            result = sendGetPowAndManageReply(tbForErrors);

            // current_meas_in_form_1 = current_reading_ch1;
            return (result);    // was (1)

        }

        // This is called to perform initialization common to the base station
        // and imlant module interfaces (BsmApi and ImApi). Returns 0 for
        // success, -1 for error.
        //
        public virtual int init(byte whatToInit, ref string err)
        {
            EI_SPECS eiSpecs;
            ADP_SPECS adpSpecs;

            // Create error information objects for the API/DLL. For more 
            // information, see the comments for eiId and eiIdStat.
            //
            EiInitSpecs(out eiSpecs, sizeof(EI_SPECS));
            if ((eiId = EiOpen(ref eiSpecs, sizeof(EI_SPECS))) == null)
            {
                err = errReport("Failed EiOpen().", null);
                goto error;
            }
            if ((eiIdPoll = EiOpen(ref eiSpecs, sizeof(EI_SPECS))) == null)
            {
                err = errReport("Failed EiOpen().", null);
                goto error;
            }

            if (whatToInit == INIT_ERR_DONT_CONNECT)
            {
                //the connection already exists, so we are done
                return (0);
            }

            // init specs to pass to AdpOpen() with default values
            AdpInitSpecs(out adpSpecs, sizeof(ADP_SPECS));

            // set to null so it will connect to the first ADP board it finds
            adpSpecs.localMezzDevType = null;

            adpId = AdpOpen(ref adpSpecs, sizeof(ADP_SPECS), eiId);
            if (adpId == null)
            {
                err = errReport("Failed AdpOpen(): ", eiId);
                goto error;
            }
            bHaveAdpOnlyConnection = true;


            return (0);
        error:
            // Close any API/DLL interfaces that were opened. This frees any
            // resources allocated by the API functions, and closes any board
            // connections so they're available to be re-opened later if desired.
            //
            string closeErr = "";
            close(ref closeErr);
            bHaveAdpOnlyConnection = true;
            return (-1);
        }

        // This is called to close any API/DLL interfaces that were opened by
        // the common API (AdpApi). This frees any resources allocated by the
        // API functions, and closes any board connections so they're available
        // to be re-opened later if desired. Returns 0 for success, -1 for error.
        //
        public virtual int close(ref string err)
        {
            int retVal = 0;

            if (adpId != null)
            {
                if (AdpClose(adpId, eiId) < 0)
                {
                    err = errReport("Failed AdpClose(): ", eiId);
                    retVal = -1;
                }
                adpId = null;
            }

            if (eiId != null)
            {
                if (EiClose(eiId) < 0)
                {
                    err = errReport("Failed EiClose().", null);
                    retVal = -1;
                }
                eiId = null;
            }

            if (eiIdPoll != null)
            {
                if (EiClose(eiIdPoll) < 0)
                {
                    err = errReport("Failed EiClose().", null);
                    retVal = -1;
                }
                eiIdPoll = null;
            }

            initialized = false;

            return (retVal);
        }

        // Here is an instance of the struct that requires unsafe
        // pointers to access in csharp (aka "unmanaged" memory)

        public UNSAFE_ADP_POW_BUF unsafeAdpPowBuf;

        // and here is the "clone" that works in the managed world
        // and is the unit we write to disk

        public DLL_ADP_CURRENT_AND_DMA_MANAGED managedReturnedSamplesFromDma = new DLL_ADP_CURRENT_AND_DMA_MANAGED();

        // I believe it is appropriate to keep all transfers that have to be 
        // done with unsafe pointers inside this file (AdpApi.cs) 
        // so that the callers do not have to be bothered with them.

        // We convert the un-managed reply for the current commands
        // to managed in the code below, so the caller will look in
        // managed_returned_current_and_dma for all responses

        public int sendGetPowAndManageReply(System.Windows.Forms.TextBox tbForErrors)
        {

            string err = "";
            // since power measurement commands are only to the adp, we
            // should NOT require the adkBrdInitialized to be true
            //if (adkBrdInitialized = true && (configEnabled))
            {
                int rslt = 0;

                // to build the embedded code for the new interfaces
                // I will usurp only an exact command for now and let others pass 
                // to the old code




                // call the new style interface in c dll
                // rslt = AdpGetPow(adpId, ADP_POW_BUF *pb, UD32 pbSize, EI e);
                // private static extern int AdpGetPow(void* adpId, ref ADP_POW_BUF pb, uint pbSize, EI e);
                uint pbSize = (uint)sizeof(UNSAFE_ADP_POW_BUF);

                // this was 6 which is correct pbSize = (uint)sizeof(ADP_POW_BUF_INFO);

                rslt = AdpApiGetPow(ref unsafeAdpPowBuf, pbSize);

                if (rslt == 0)
                {

                    // Here we copy to the clone class in the managed world
                    fixed (Zarlink.Adp.Api.AdpApi.UNSAFE_ADP_POW_BUF* pUnsafeStruct = &unsafeAdpPowBuf)
                    {
                        int i;


                        //managedReturnedCurrentAndDma.current_reading_ch1 = pUnsafeStruct->current_reading_ch1;
                        //managedReturnedCurrentAndDma.current_reading_ch2 = pUnsafeStruct->current_reading_ch2;

                        managedReturnedSamplesFromDma.dmaSnAvailable = pUnsafeStruct->dmaSnAvailable;

                        //managedReturnedCurrentAndDma.dmaTriggeredBlock = pUnsafeStruct->dma_reply_status_1;
                        //managedReturnedCurrentAndDma.dmaReplyFlags = pUnsafeStruct->dmaReplyFlags;

                        // the DMA data block physically resides here relative to the other members
                        // (just keeping the order consistent with the struct)
                        for (i = 0; i < NR_SAMPLES_IN_CURRENT_MSG; i++)
                        {
                            managedReturnedSamplesFromDma.dma_buf_samples[i] = pUnsafeStruct->dma_buf_samples[i];    // converts to double
                            pUnsafeStruct->dma_buf_samples[i] = 55;    // erase buffer to a known pattern
                        }


                        //managedReturnedCurrentAndDma.dmaSnAfterUsbLBT = pUnsafeStruct->dmaSnAfterUsbLBT;

                    }   // end of fixed clause
                }


                if (rslt != 0)
                {
                    string errorString = " Failed AdpApiGetPow(), rslt=" + rslt.ToString()+" ";

                    // make a string from the eiId
                    // TODO: Master this ei item 
                    err = errReport(errorString, eiIdPoll);

                    // we need to show this message because errorReport() 
                    // does not show anything!
                    //MessageBox.Show(err, "AdpApi.cs sendGetPowAndManageReply()");

                    //AdpPowMonForm.tbCurrentToolDataDisplay.

                    tbForErrors.AppendText(err + " AdpApi.cs sendGetPowAndManageReply()\r\n");
                    //tbForErrors.AppendText(errorString + " AdpApi.cs sendGetPowAndManageReply()\r\n");
                    return (-1);
                }

            }   // if (adkBrdInitialized = true && (configEnabled))

            return (0); // everything went OK
        }

        protected string errReport(string area, void* eiId)
        {
            string err;

            if (eiId != null)
            {
                // err = area + new string(EiGetMsg(eiId)); // for debugging
                err = new string(EiGetMsg(eiId));
            }
            else
            {
                err = area;
            }
            return (err);
        }

        public virtual void closeAdpOnlyConnection()
        {
            // need this to close a connection to an ADP when
            // there is no Mezz alive
            if (adpId != null) AdpClose(adpId, eiId);
        }

        // Only sets the bits in the ports
        public int
        AdpApiSetBitsOnly(uint cal4chan1, uint cal4chan2, uint range4chan1, uint range4chan2)
        {
            int rslt;

            ADP_POW_MON_OPTIONS passedOptions;
            UInt32 pmoSize = (UInt32)sizeof(ADP_POW_MON_OPTIONS);

            // Fill the structure with arguments

            // power supply to monitor (ADP_POW_1, ...)
            // but we are only setting the bits here, not starting to monitor
            passedOptions.input = ADP_POW_NONE;

            // in this message design we must set all 4 bits
            // (range 0 & 1, cal load 0 & 1) at the same time
            passedOptions.options = 0;
            
            if (range4chan1 == 1) {
                passedOptions.options |= ADP_HIGH_RANGE_POW_1;
            } else {
                // leave as 0 for low range
            }

            if (range4chan2 == 1)
            {
                passedOptions.options |= ADP_HIGH_RANGE_POW_2;
            }
            else
            {
                // leave as 0 for low range
            }

            if (cal4chan1 == 1) {
                passedOptions.options |= ADP_CAL_POW_1;
            } else {
                // leave as 0 for cal off
            }

            if (cal4chan2 == 1) {
                passedOptions.options |= ADP_CAL_POW_2;
            } else {
                // leave as 0 for cal off
            }

            
            // number of microsececonds between samples
            passedOptions.usPerSamp = 0;
 
            rslt = AdpStartPowMon(this.adpId, ref passedOptions, pmoSize, (void*)eiId);

            return (rslt);
        }


        // sets the bits in the ports *and* starts DMA
        public int
        AdpApiStartPowMon(uint cal4chan1, uint cal4chan2, uint range4chan1, 
            uint range4chan2, byte oneOrTwoOrBoth, UInt16 sampleRateInUsec)
        {
            int rslt;

            ADP_POW_MON_OPTIONS passedOptions;
            UInt32 pmoSize = (UInt32)sizeof(ADP_POW_MON_OPTIONS);

            // Fill the structure with arguments

            // power supply to monitor (ADP_POW_1, ...)
            passedOptions.input = oneOrTwoOrBoth;

            // in this design we must set all 4 bits at the same time
            passedOptions.options = 0;
            
            if (range4chan1 == 1) {
                passedOptions.options |= ADP_HIGH_RANGE_POW_1;
            } else {
                // leave as 0 for low range
            }

            if (range4chan2 == 1){
                passedOptions.options |= ADP_HIGH_RANGE_POW_2;
            }
            else
            {
                // leave as 0 for low range
            }

            if (cal4chan1==1) {
                passedOptions.options |= ADP_CAL_POW_1;
            } else {
                // leave as 0 for cal off
            }
            if (cal4chan2==1) {
                passedOptions.options |= ADP_CAL_POW_2;
            } else {
                // leave as 0 for cal off
            }

            
            // number of microsececonds between samples
            passedOptions.usPerSamp = sampleRateInUsec;
 
            // passedOptions
            //AdpStartPowMon(this.adpId, const ADP_POW_MON_OPTIONS *pmo, UD32 pmoSize, EI e)

            //rslt = AdpStartPowMon((void*)this.adpId, ref passedOptions, pmoSize, (void*)eiId);
            rslt = AdpStartPowMon(this.adpId, ref passedOptions, pmoSize, (void*)eiId);

            return (rslt);
        }


        // stops the DMA
        public int
        AdpApiStopPowMon()
        {
            int rslt;

            // no arguments for this command
            rslt = AdpStopPowMon(this.adpId, (void*)eiId);

            return (rslt);
        }

        // checks for data available and returns the data if so
        public int
        AdpApiGetPow(ref UNSAFE_ADP_POW_BUF csharpAdpPowBuf, UInt32 pbSize)
        {
            int rslt;
            // This will always be called from the polling thread, so
            // be sure we pass the eiIdPoll error object
            rslt = AdpGetPow(adpId, out csharpAdpPowBuf, pbSize, (void*)eiIdPoll);

            return (rslt);
        }

        // this was added so we can "ping" an adpId to see if
        // it has been killed or is still alive.
        // this code was taken from within init() and will put
        // the result in the same string which is why it is
        // named refresh

        string adpVer = "";

        public int checkConnection(ref string err)
        {
            // clear so nothing is there
            adpVer = "";

            // get ADP board version
            sbyte[] adpVerBuf = new sbyte[125];
            if ((AdpGetBoardVer(adpId, adpVerBuf, 120, eiId)) < 0)
            {
                err = errReport("Failed AdpGetBoardVer(): ", eiId);
                goto error;
            }
            for (int i = 0; i < 125; i++)
            {
                if ((char)adpVerBuf[i] == '\0') break;
                adpVer += (char)adpVerBuf[i];
            }
            return (0);

        error:
            // Close any API/DLL interfaces that were opened. This frees any
            // resources allocated by the API functions, and closes any board
            // connections so they're available to be re-opened later if desired.
            //
            string closeErr = "";
            close(ref closeErr);
            return (-1);

        }

    }       // end of unsafe public class AdpApi
}

