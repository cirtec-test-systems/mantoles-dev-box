//
// This file contains structures, etc. that are common to the nonvolatile
// memory (NVM) for all boards.
//
// Copyright Microsemi Semiconductor (U.S.) Inc. 2017. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2017. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.

using System.Runtime.InteropServices;

namespace Zarlink.Adp.Api
{
    // Class for the information at the beginning of nonvolatile memory (NVM).
    // For field descriptions, see NVM_INFO in "[SourceTree]\AppDevPlat\Sw\
    // Includes\Adp\Anyboard\NvmLib.h".
    //
    [StructLayout(LayoutKind.Sequential)]
    public class NvmInfo
    {
        private readonly Ud32Bytes nvmInitKey;
        private readonly Ud16Bytes nvmType;
        private readonly Ud16Bytes nvmSize;

        // NVM init key (for NvmInfo.nvmInitKey). Note that this must match
        // the corresponding define in "[SourceTree]\AppDevPlat\Sw\
        // Includes\Adp\Anyboard\NvmLib.h".
        //
        public const uint NVM_INIT_KEY = 0x4E564D20;

        // NVM types (for NvmInfo.nvmType). Note that these must match the
        // corresponding defines in "[SourceTree]\AppDevPlat\Sw\
        // Includes\Adp\Anyboard\NvmLib.h".
        //
        public const ushort NVM_TYPE_550_ADK_BAR  = 1;
        public const ushort NVM_TYPE_550_WSN_HUB  = 2;
        public const ushort NVM_TYPE_550_WSN_NODE = 3;
        public const ushort NVM_TYPE_10X_ADK_BSM  = 4;
        public const ushort NVM_TYPE_10X_ADK_IM   = 5;

        public NvmInfo(ushort nvmType, ushort nvmSize)
        {
            this.nvmInitKey = new Ud32Bytes(NVM_INIT_KEY);
            this.nvmType = new Ud16Bytes(nvmType);
            this.nvmSize = new Ud16Bytes(nvmSize);
        }
    }
}
