//
// This file contains various structures and utilties for all of the software
// Note that this does not contain anything that is application-specific, which
// must be defined in separate application-specific files.
//
// Copyright Microsemi Semiconductor (U.S.) Inc. 2016. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2016. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.

using System;
using System.Runtime.InteropServices;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Zarlink.Adp.General;

namespace Zarlink.Adp.Api
{
    // Class for an unsigned 8-bit value. This can be used in classes that are
    // converted to structures (via marshalling) that are then shared with other
    // platforms (e.g. embedded firmware). Note that this is essentialy the same
    // as a "byte", but it's provided anyway so classes that use the other classes
    // in this file (Ud8Hex, Ud16Bytes, Ud16BytesHex, ...) can also use Ud8 for
    // unsigned 8-bit values (for consistency).
    //
    [StructLayout(LayoutKind.Sequential)]
    public class Ud8: IXmlSerializable
    {
        public byte Val
        {
            get { return(val); }
            set { val = value; }
        }
        private byte val;
        
        public Ud8() {}
        public Ud8(byte val) { Val = val; }
        
        public virtual void WriteXml(XmlWriter writer)
        {
            writer.WriteString(Convert.ToString(Val, 10));
        }
        public virtual void ReadXml(XmlReader reader)
        {
            Val = (byte)Util.strToUInt(reader.ReadElementContentAsString());
        }
        public XmlSchema GetSchema() { return(null); }
    }
    
    // This is the same as Ud8, except when this is serialized to an XML file,
    // it writes a binary value to the XML file instead of a decimal value.
    //
    [StructLayout(LayoutKind.Sequential)]
    public class Ud8Bin: Ud8, IXmlSerializable
    {
        public Ud8Bin() {}
        public Ud8Bin(byte val): base(val) {}
        
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteString("b'" + Convert.ToString(Val, 2));
        }
        public override void ReadXml(XmlReader reader)
        {
            Val = (byte)Util.strToUInt(reader.ReadElementContentAsString());
        }
    }
    
    // This is the same as Ud8, except when this is serialized to an XML file,
    // it writes a hex value to the XML file instead of a decimal value.
    //
    [StructLayout(LayoutKind.Sequential)]
    public class Ud8Hex: Ud8, IXmlSerializable
    {
        public Ud8Hex() {}
        public Ud8Hex(byte val): base(val) {}
        
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteString("h'" + Convert.ToString(Val, 16));
        }
        public override void ReadXml(XmlReader reader)
        {
            Val = (byte)Util.strToUInt(reader.ReadElementContentAsString());
        }
    }

    // Class for an unsigned 16-bit value stored in little-endian byte order.
    // This can be used in classes that are converted to structures (via
    // marshalling) that are then shared with other platforms (e.g. embedded
    // firmware) to ensure that all platforms see the same value, even if the
    // platforms' native byte orders are different.
    //
    [StructLayout(LayoutKind.Sequential)]
    public class Ud16Bytes: IXmlSerializable
    {
        public ushort Val
        {
            get { return((ushort)((b1 << 8) | b0)); }
            set { b0 = (byte)(value); b1 = (byte)(value >> 8); }
        }
        private byte b0;
        private byte b1;
            
        public Ud16Bytes() {}
        public Ud16Bytes(ushort val) { Val = val; }
        
        public virtual void WriteXml(XmlWriter writer)
        {
            writer.WriteString(Convert.ToString(Val, 10));
        }
        public virtual void ReadXml(XmlReader reader)
        {
            Val = (ushort)Util.strToUInt(reader.ReadElementContentAsString());
        }
        public XmlSchema GetSchema() { return(null); }
    }
    
    // This is the same as Ud16Bytes, except when this is serialized to an XML
    // file, it writes a hex value to the XML file instead of a decimal value.
    //
    [StructLayout(LayoutKind.Sequential)]
    public class Ud16BytesHex: Ud16Bytes, IXmlSerializable
    {
        public Ud16BytesHex() {}
        public Ud16BytesHex(ushort val): base(val) {}
        
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteString("h'" + Convert.ToString(Val, 16));
        }
        public override void ReadXml(XmlReader reader)
        {
            Val = (ushort)Util.strToUInt(reader.ReadElementContentAsString());
        }
    }
    
    // Class for a signed 16-bit value stored in little-endian byte order.
    // This can be used in classes that are converted to structures (via
    // marshalling) that are then shared with other platforms (e.g. embedded
    // firmware) to ensure that all platforms see the same value, even if the
    // platforms' native byte orders are different.
    //
    [StructLayout(LayoutKind.Sequential)]
    public class Sd16Bytes
    {
        public short Val
        {
            get { return((short)((b1 << 8) | b0)); }
            set { b0 = (byte)(value); b1 = (byte)(value >> 8); }
        }
        private byte b0;
        private byte b1;
            
        public Sd16Bytes() {}
        public Sd16Bytes(short val) { Val = val; }
    }
    
    // Class for an unsigned 32-bit value stored in little-endian byte order.
    // This can be used in classes that are converted to structures (via
    // marshalling) that are then shared with other platforms (e.g. embedded
    // firmware) to ensure that all platforms see the same value, even if the
    // platforms' native byte orders are different.
    //
    [StructLayout(LayoutKind.Sequential)]
    public class Ud32Bytes: IXmlSerializable
    {
        public uint Val
        {
            get { return((uint)((b3 << 24) | (b2 << 16) | (b1 << 8) | b0)); }
            set { 
                b0 = (byte)(value);
                b1 = (byte)(value >> 8);
                b2 = (byte)(value >> 16);
                b3 = (byte)(value >> 24);
            }
        }
        private byte b0;
        private byte b1;
        private byte b2;
        private byte b3;
            
        public Ud32Bytes() {}
        public Ud32Bytes(uint val) { Val = val; }
        
        public virtual void WriteXml(XmlWriter writer)
        {
            writer.WriteString(Convert.ToString(Val, 10));
        }
        public virtual void ReadXml(XmlReader reader)
        {
            Val = Util.strToUInt(reader.ReadElementContentAsString());
        }
        public XmlSchema GetSchema() { return(null); }
    }
    
    // This is the same as Ud32Bytes, except when this is serialized to an XML
    // file, it writes a hex value to the XML file instead of a decimal value.
    //
    [StructLayout(LayoutKind.Sequential)]
    public class Ud32BytesHex: Ud32Bytes, IXmlSerializable
    {
        public Ud32BytesHex() {}
        public Ud32BytesHex(uint val): base(val) {}
        
        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteString("h'" + Convert.ToString(Val, 16));
        }
        public override void ReadXml(XmlReader reader)
        {
            Val = Util.strToUInt(reader.ReadElementContentAsString());
        }
    }
        
    // Class for a signed 32-bit value stored in little-endian byte order.
    // This can be used in classes that are converted to structures (via
    // marshalling) that are then shared with other platforms (e.g. embedded
    // firmware) to ensure that all platforms see the same value, even if the
    // platforms' native byte orders are different.
    //
    [StructLayout(LayoutKind.Sequential)]
    public class Sd32Bytes
    {
        public int Val
        {
            get { return((int)((b3 << 24) | (b2 << 16) | (b1 << 8) | b0)); }
            set { 
                b0 = (byte)(value);
                b1 = (byte)(value >> 8);
                b2 = (byte)(value >> 16);
                b3 = (byte)(value >> 24);
            }
        }
        private byte b0;
        private byte b1;
        private byte b2;
        private byte b3;
            
        public Sd32Bytes() {}
        public Sd32Bytes(int val) { Val = val; }
    }
}
