//
// This file contains miscellaneous utilities.
//
// Copyright Microsemi Semiconductor (U.S.) Inc. 2017. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2017. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.

using System;
using System.Media;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.Runtime.InteropServices;

namespace Zarlink.Adp.General
{
    public static class Util
    {
        public static string eMsg(Exception e)
        {
            string eMsg = e.Message; 
            
            while((e = e.InnerException) != null) {
                eMsg += " Underlying error: " + e.Message;
            }
            return(eMsg);
        }
        
        // This displays the specified error message in the specified text box
        // and issues an audio beep. If no text box is specified, it displays
        // the error message in a message box.
        //
        public static void displayError(TextBox textBox, string err)
        {
            if (textBox != null) {

                textBox.AppendText(Environment.NewLine + err);

                if (textBox.TextLength > 5000) {
                
                    string t = textBox.Text;
                    textBox.Text = t.Substring(2000, t.Length - 2000);
                }

                SystemSounds.Beep.Play();
                
            } else {
                MessageBox.Show(err);
            }
        }
        
        public static uint strToUInt(string s)
        {
            if (s.StartsWith("b'")) {
                return(Convert.ToUInt32(s.Substring(2), 2));
            } else if ((s.StartsWith("h'")) || (s.StartsWith("0x"))) {
                return(Convert.ToUInt32(s.Substring(2), 16));
            } else {
                return(Convert.ToUInt32(s, 10));
            }
        }
        
        // This tests the specified key event ("e") to determine if the pressed key
        // corresponds to a valid hex digit. The "permitNewline" parameter indicates
        // if a newline key is permitted. This returns false if the key is a valid
        // hex digit or a newline (if permitted); otherwise, it returns true to
        // indicate the key is invalid.
        //
        public static bool isInvalidHexKey(KeyEventArgs e, bool permitNewline)
        {
            bool shiftPressed;

            if ((Control.ModifierKeys & Keys.Shift) == Keys.Shift) {
                shiftPressed = true;
            } else {
                shiftPressed = false;
            }

            // determine if the key is a number from the top of the keyboard
            if (e.KeyCode < Keys.D0 || e.KeyCode > Keys.D9) {
            
                // determine if the key is a number from the keypad
                if (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9) {
                
                    // determine if the key is a backspace
                    if (e.KeyCode != Keys.Back) {
                    
                        // determine if the key is a hex alpha character
                        switch(e.KeyCode) {
                        case Keys.A:
                        case Keys.B:
                        case Keys.C:
                        case Keys.D:
                        case Keys.E:
                        case Keys.F:
                            break;
                        default: // key is not a hex alpha character
                                                
                            // If the key is a newline and a newline is permitted,
                            // return false to indicate the key is valid.
                            //
                            if ((e.KeyCode == Keys.Enter) && permitNewline) return(false);
                            
                            // return true to indicate the key is invalid
                            SystemSounds.Beep.Play();
                            return(true);
                        }
                    }
                }
                
            } else if (shiftPressed) {
            
                // return true to indicate the key is invalid
                return(true);
            }
            
            // return false to indicate the key is valid
            return(false);
        }

        // This tests the specified key event ("e") to determine if the pressed key
        // corresponds to a valid decimal digit. The "permitNewline" parameter indicates
        // if a newline key is permitted. This returns false if the key is a valid
        // decimal digit or a newline (if permitted); otherwise, it returns true to
        // indicate the key is invalid.
        //
        public static bool isInvalidIntegerKey(KeyEventArgs e, bool permitNewline)
        {
            // determine if the key is a number from the top of the keyboard
            if (e.KeyCode < Keys.D0 || e.KeyCode > Keys.D9) {
            
                // determine if the key is a number from the keypad
                if (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9) {
                
                    // determine whether the key is a backspace
                    if (e.KeyCode != Keys.Back) {
                    
                        // If the key is a newline and a newline is permitted,
                        // return false to indicate the key is valid.
                        //
                        if ((e.KeyCode == Keys.Enter) && permitNewline) return(false);
                            
                        // return true to indicate the key is invalid
                        SystemSounds.Beep.Play();
                        return(true);
                    }
                }
            }
            
            // return false to indicate the key is valid
            return(false);
        }
        
        // This converts the specified string into an array of ASCII bytes, where
        // each ASCII byte in the resulting array represents the corresponding
        // character in the string.
        //
        public static byte[] convertStrToAsciiBytes(string s)
        {
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            
            return(encoding.GetBytes(s));
        }
        
        // This converts the specified array of ASCII bytes (asciiBytes) into a string,
        // where each character in the resulting string represents the corresponding
        // ASCII byte in the array.
        //
        public static string convertAsciiBytesToStr(byte[] asciiBytes)
        {
            string s;
            
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            
            s = encoding.GetString(asciiBytes);
            
            return(s);
        }
        
        // This converts the specified segment of an array of ASCII bytes (asciiBytes)
        // into a string, where each character in the resulting string represents
        // the corresponding ASCII byte in the array. The beginning of the segment
        // to convert is specified by "startIndex", and the number of ASCII bytes to
        // convert is specified by "nAsciiBytesToConvert" (starting with the ASCII
        // byte at the specified starting index).
        //
        public static string convertAsciiBytesToStr(byte[] asciiBytes, int startIndex,
            int nAsciiBytesToConvert)
        {
            string s;
            
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            
            s = encoding.GetString(asciiBytes, startIndex, nAsciiBytesToConvert);
            
            return(s);
        }
    }
}
