//
// Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2012. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Zarlink.Adp.Forms
{
    public partial class AdpNonModalMessageBox : Form
    {

        public DialogResult dResult = DialogResult.None;


        public AdpNonModalMessageBox()
        {
            InitializeComponent();
        }


        private void btnContinue_Click(object sender, EventArgs e)
        {
            dResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            dResult = DialogResult.Cancel;
        }

    }
}