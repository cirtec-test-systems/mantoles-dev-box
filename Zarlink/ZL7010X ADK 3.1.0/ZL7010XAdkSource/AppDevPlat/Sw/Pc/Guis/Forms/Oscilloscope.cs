using System;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;

/*
 * C# Wrapper around the Oscilloscope DLL
 * 
 * (C)2006 Dustin Spicuzza
 *
* This library interface is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; only
* version 2.1 of the License.
* 
* Some comments/function declarations taken from the original "oscilloscope-lib" documentation
*
 */

namespace Signal_Project {
	sealed class Oscilloscope : IDisposable {
        // "A sealed class cannot be inherited."

        // interface System.IDisposable
        // defines a method to release allocated resources

        //The primary use of this interface is to release unmanaged resources.
        //The garbage collector automatically releases the memory allocated
        //to a managed object when that object is no longer used. However, 
        //it is not possible to predict when garbage collection will occur.
        //Furthermore, the garbage collector has no knowledge of unmanaged
        //resources such as window handles, or open files and streams.

        //Use the Dispose method of this interface to explicitly release
        //unmanaged resources in conjunction with the garbage collector.
        //The consumer of an object can call this method when the object
        //is no longer needed.

        // #region lets you specify a block of code that you can
        // expand or collapse when using the outlining feature 
        // of the Visual Studio Code Editor.
        #region External declarations

/*
 *	C-style function declarations
 * 
int (__cdecl * AtOpenLib) (int Prm);
int (__cdecl * ScopeCreate) (int Prm ,  char  * P_IniName,  char * P_IniSuffix);
int (__cdecl * ScopeDestroy) (int ScopeHandle);
int (__cdecl * ScopeShow) (int ScopeHandle);
int (__cdecl * ScopeHide) (int ScopeHandle);
int (__cdecl * ScopeCleanBuffers) (int ScopeHandle);
int (__cdecl * ShowNext) (int ScopeHandle, double * PArrDbl);
int (__cdecl * ExternalNext) (int ScopeHandle, double * PDbl);
int (__cdecl * QuickUpDate) (int ScopeHandle);
int (__cdecl * ScopeSetCaption) (int ScopeHandle, char * P_ZeroTermStr);
 */
		[DllImport("Osc_DLL.dll",CallingConvention=CallingConvention.Cdecl)]
		private static extern int AtOpenLib
			(int Prm);

		[DllImport("Osc_DLL.dll",CallingConvention=CallingConvention.Cdecl)]
		private static extern int ScopeCreate (
			int Prm ,  
			[MarshalAs(UnmanagedType.LPStr)] StringBuilder P_IniName, 
			[MarshalAs(UnmanagedType.LPStr)] StringBuilder P_IniSuffix);
		
		[DllImport("Osc_DLL.dll",CallingConvention=CallingConvention.Cdecl)]
		private static extern int ScopeDestroy
			(int ScopeHandle);

		[DllImport("Osc_DLL.dll",CallingConvention=CallingConvention.Cdecl)]
		private static extern int ScopeShow 
			(int ScopeHandle);

		[DllImport("Osc_DLL.dll",CallingConvention=CallingConvention.Cdecl)]
		private static extern int ScopeHide 
			(int ScopeHandle);

		[DllImport("Osc_DLL.dll",CallingConvention=CallingConvention.Cdecl)]
		private static extern int ScopeCleanBuffers
			(int ScopeHandle);

		[DllImport("Osc_DLL.dll",CallingConvention=CallingConvention.Cdecl)]
		private static extern int ShowNext 
			(int ScopeHandle, 
			double [] PArrDbl);
		
		[DllImport("Osc_DLL.dll",CallingConvention=CallingConvention.Cdecl)]
		private static extern int ExternalNext 
			(int ScopeHandle, 
			ref double PDbl);

		[DllImport("Osc_DLL.dll",CallingConvention=CallingConvention.Cdecl)]
		private static extern int QuickUpDate 
			(int ScopeHandle);

        [DllImport("Osc_DLL.dll",CallingConvention=CallingConvention.Cdecl)]
        private static extern int ScopeSetCaption
            (int ScopeHandle,
            [MarshalAs(UnmanagedType.LPStr)] StringBuilder P_Caption);

        //ScopeGetFormLeft,
        //ScopeGetFormTop,
        //ScopeGetFormWidth,
        //ScopeGetFormHeight,

        [DllImport("Osc_DLL.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int ScopeGetFormLeft
            (int ScopeHandle);
        [DllImport("Osc_DLL.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int ScopeGetFormTop
            (int ScopeHandle);
        [DllImport("Osc_DLL.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int ScopeGetFormWidth
            (int ScopeHandle);
        [DllImport("Osc_DLL.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int ScopeGetFormHeight
            (int ScopeHandle);

#endregion

		#region Static members
        static bool libInitialized = false;
        // static is appropriate for above since this is used to 
        // "open" the DLL which MUST only be done once for
        // all oscilloscope objects

        // a static member belongs to the type itself rather than to a specific object
        // (that is, there is only ONE instance of these members even if there are mulitple
        // instances of the class)

        //Static classes and class members are used to create data 
        //and functions that can be accessed without creating an 
        //instance of the class. 
        
        //Static class members can be used 
        //to separate data and behavior that is independent of any 
        //object identity: the data and functions do not change 
        //regardless of what happens to the object. 
        
        //Static classes
        //can be used when there is no data or behavior in the class
        //that depends on object identity.


        // this will be set when we call AtOpenLib()
        static int scopeInstancesAllowed = 0;

        // for debug: static is appropriate here
        static int lastHandleReturned = -1;

        // it is useful to be able to know the current position so we can
        // re-paint without moving the form if we want to
        // 0 will be used to mean size has not been set yet
        public static System.Drawing.Point oscFormLocation = new System.Drawing.Point(0,0);
        public static System.Drawing.Size oscFormSize = new System.Drawing.Size(0,0);


		/// <summary>
		/// Creates a new Oscilloscope. Returns the object if successful,
		/// otherwise returns null. Generally, if it returns null then
		/// it cannot find the correct DLL to load
        /// Static is appropriate here since these functions are meant
        /// to be accessed without creating an instance of the class. 
		/// </summary>
		/// <returns>Oscilloscope instance</returns>
		public static Oscilloscope Create() {
			return Create(null, null);
		}

		/// <summary>
		/// Creates a new Oscilloscope. Returns the object if successful,
		/// otherwise returns null. Generally, if it returns null then
		/// it cannot find the correct DLL to load
        /// Static is appropriate here since these functions are meant
        /// to be accessed without creating an instance of the class. 
        /// </summary>
		/// <param name="IniName">Name of INI file with scope settings</param>
		/// <param name="IniSuffix">Section name suffix (see manual)</param>
		/// <returns>Oscilloscope instance</returns>
		public static Oscilloscope Create(string IniName,string IniSuffix){



			try {
				if ( !libInitialized ) {
					// initialize               
            		// This can be called since it is a function in the DLL and
            		// not a member function of the class
                    scopeInstancesAllowed = AtOpenLib(0);

                    //MessageBox.Show("AtOpenLib(0) scopeInstancesAllowed is " + scopeInstancesAllowed.ToString(), "", MessageBoxButtons.OK);

                    if (scopeInstancesAllowed == -1)
						return null;
					// set to true
					libInitialized = true;
				}

			} catch {
				// return 
				return null;
			}

			// create the scope
            // This can be called since it is a function in the DLL and
            // not a member function of the class
			lastHandleReturned = ScopeCreate(0, new StringBuilder(IniName), new StringBuilder( IniSuffix));


            if (lastHandleReturned != 0)
            {
				// presumably the ScopeCreate() has created whatever structures
				// (it needs in some "unmanaged" space) for an instance of the scope.
				// Now C# can create a new Oscilloscope object that
				// we can use to access the instance created by the DLL using
				// the scopeHandle that is particular to the new instance
                // (the c'tor call below will assign lastHandleReturned to scopeHandle
                // member variable of the new object making that object only
                // able to access that handle)

                return new Oscilloscope(lastHandleReturned);
            }

            // getting here means something went wrong with ScopeCreate()
            MessageBox.Show("ScopeCreate() returned error! " + lastHandleReturned.ToString(), "", MessageBoxButtons.OK);

			return null;
		}

		#endregion  // static members

        // these variables are needed in each instance of the object
		public int scopeHandle = -1;
		bool _disposed = false;
        int lastResult = -1;


        private Oscilloscope()
        {
            MessageBox.Show("Oscilloscope(): c'tor called with no Handle arg! Should Never Happen! \r",
               "", MessageBoxButtons.OK);

		}

		private Oscilloscope(int handle) {
            //MessageBox.Show("Oscilloscope(int handle): \r"
            //    + "scopeHandle = handle (" + handle.ToString() + ")", "", MessageBoxButtons.OK);
			scopeHandle = handle;
            _disposed = false;

		}

        // finalizer
		~Oscilloscope() {
            //!! I saw that this was called N times when the main
            // (app launcher) form exits!
			Dispose();

		}

		/// <summary>
		/// Shows the scope
		/// </summary>
		public void Show(){
            if (_disposed) throw new ObjectDisposedException(this.GetType().FullName + " [ in this.Show()]"); 

            lastResult = ScopeShow(scopeHandle);
            if (lastResult != scopeHandle) MessageBox.Show("ScopeShow() returned error! " + lastResult.ToString(), "", MessageBoxButtons.OK);

		}
		
		/// <summary>
		/// Hides the scope from view
		/// </summary>
		public void Hide() {
            if (_disposed) throw new ObjectDisposedException(this.GetType().FullName + " [ in this.Hide()]"); 

            lastResult = ScopeHide(scopeHandle);
            if (lastResult != scopeHandle) MessageBox.Show("ScopeHide() returned error! " + lastResult.ToString(), "", MessageBoxButtons.OK);

		}

		/// <summary>
		/// Clears the buffer of the scope
		/// </summary>
		public void Clear() {

            if (_disposed) throw new ObjectDisposedException(this.GetType().FullName + " [ in this.Clear()]"); 
            
            lastResult = ScopeCleanBuffers(scopeHandle);
            if (lastResult != scopeHandle) MessageBox.Show("ScopeCleanBuffers() returned error! " + lastResult.ToString(), "", MessageBoxButtons.OK);
		}

		/// <summary>
		/// Add data to the scope
		/// </summary>
		/// <param name="beam1">Data for first beam</param>
		/// <param name="beam2">Data for second beam</param>
		/// <param name="beam3">Data for third beam</param>
		public void AddData(double beam1, double beam2, double beam3) {


            if (_disposed) throw new ObjectDisposedException(this.GetType().FullName + " [ in this.AddData()]"); 


            double[] PArrDbl = new double[3];
            PArrDbl[0] = beam1;
            PArrDbl[1] = beam2;
            PArrDbl[2] = beam3;

            lastResult = ShowNext(scopeHandle, PArrDbl);
            if (lastResult != scopeHandle) MessageBox.Show("ShowNext() returned error! " + lastResult.ToString(), "", MessageBoxButtons.OK);

		}

		/// <summary>
		/// Add data to the 'external' trigger function signal
		/// </summary>
		/// <param name="data">The data</param>
		public void AddExternalData(double data) {
            if (_disposed) throw new ObjectDisposedException(this.GetType().FullName + " [ in this.AddExternalData()]"); 

            lastResult = ExternalNext(scopeHandle, ref data);
            if (lastResult != scopeHandle) MessageBox.Show("ExternalNext() returned error! " + lastResult.ToString(), "", MessageBoxButtons.OK);

		}

		/// <summary>
		/// Quickly refreshes screen of oscilloscope. Calling this function is
		/// not usually required. Recommended for using in situations when 
		/// intensive data stream is going into oscilloscope
		/// </summary>
		public void Update() {
            if (_disposed) throw new ObjectDisposedException(this.GetType().FullName + " [ in this.Update()]"); 
            lastResult = QuickUpDate(scopeHandle);
            if (lastResult != scopeHandle) MessageBox.Show("QuickUpDate() returned error! " + lastResult.ToString(), "", MessageBoxButtons.OK);

        }
        /// <summary>
        /// Sets the caption of the scope form
        /// 
        /// </summary>
        /// <param name="CaptionString"></param>

        /// <returns>void</returns>
        public void Caption(string CaptionString)
        {

            if (_disposed) throw new ObjectDisposedException(this.GetType().FullName + " [ in this.Caption()]");
            // change the caption
            lastResult = ScopeSetCaption(scopeHandle, new StringBuilder(CaptionString));
            if (lastResult != scopeHandle) MessageBox.Show("ScopeSetCaption() returned error! " + lastResult.ToString(), "", MessageBoxButtons.OK);

        }

        /// <summary>
        /// Gets the current location and size of the scope form and puts it into
        /// this object's variables
        /// 
        /// </summary>
        /// <param name="none"></param>

        /// <returns>void</returns>
        public void GetScopeLocAndSize()
        {
            // 
            if (_disposed) throw new ObjectDisposedException(this.GetType().FullName + " [ in this.GetScopeLocAndSize()]");

            // it is useful to be able to know the current position so we can
            // re-paint without moving the form if we want to
            // 0 will be used to mean size has not been set yet

            oscFormLocation.X = ScopeGetFormLeft(scopeHandle);
            oscFormLocation.Y = ScopeGetFormTop(scopeHandle);
            oscFormSize.Width = ScopeGetFormWidth(scopeHandle);
            oscFormSize.Height = ScopeGetFormHeight(scopeHandle);

        }

        #region IDisposable Members

		/// <summary>
		/// Dispose the object
		/// </summary>
		public void Dispose() {
            //I believe we must allow Dispose() to be called
            // multiple times on an already disposed object
            if (!_disposed) {
                lastResult = ScopeDestroy(scopeHandle);
                if (lastResult != scopeHandle) MessageBox.Show("ScopeDestroy() returned error! " + lastResult.ToString(), "", MessageBoxButtons.OK);
                _disposed = true;
            }			
		}

		/// <summary>
		/// True if object is already disposed
		/// </summary>
		public bool IsDisposed {
			get {
				return _disposed;
			}
		}

		#endregion
	}
}
