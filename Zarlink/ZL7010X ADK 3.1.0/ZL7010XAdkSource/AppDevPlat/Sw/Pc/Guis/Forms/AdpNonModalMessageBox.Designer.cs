//
// Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2012. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.

namespace Zarlink.Adp.Forms
{
    partial class AdpNonModalMessageBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdpNonModalMessageBox));
            this.lblStopIfNoTrigWait = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblStopIfNoTrigWait
            // 
            this.lblStopIfNoTrigWait.Location = new System.Drawing.Point(12, 22);
            this.lblStopIfNoTrigWait.Name = "lblStopIfNoTrigWait";
            this.lblStopIfNoTrigWait.Size = new System.Drawing.Size(354, 53);
            this.lblStopIfNoTrigWait.TabIndex = 61;
            this.lblStopIfNoTrigWait.Text = "generic non-modal form will be customized by run-time code";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(61, 88);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(77, 36);
            this.button1.TabIndex = 62;
            this.button1.Text = "Continue";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btnContinue_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(210, 88);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(84, 36);
            this.button2.TabIndex = 63;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // AdpNonModalMessageBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(378, 358);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblStopIfNoTrigWait);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AdpNonModalMessageBox";
            this.Text = "Cannot calibrate with mezzanine attached!";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblStopIfNoTrigWait;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;

    }
}