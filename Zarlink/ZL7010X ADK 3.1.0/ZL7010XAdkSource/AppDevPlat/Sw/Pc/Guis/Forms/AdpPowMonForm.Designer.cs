//
// Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2012. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.

namespace Zarlink.Adp.Forms
{
    partial class AdpPowMonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdpPowMonForm));
            this.timerPowerPolling = new System.Windows.Forms.Timer(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lblLinesNowInTraceTextbox = new System.Windows.Forms.Label();
            this.lblStateOfChan = new System.Windows.Forms.Label();
            this.lblStateOfRange = new System.Windows.Forms.Label();
            this.lblStateOfCal = new System.Windows.Forms.Label();
            this.lblStopIfNoTrigWait = new System.Windows.Forms.Label();
            this.timerCheckCalAfterFormAppears = new System.Windows.Forms.Timer(this.components);
            this.tabControlCustomerEtc = new System.Windows.Forms.TabControl();
            this.tabPageCustomer = new System.Windows.Forms.TabPage();
            this.groupCalControl = new System.Windows.Forms.GroupBox();
            this.cmbSn = new System.Windows.Forms.ComboBox();
            this.btnAutoPowerCal = new System.Windows.Forms.Button();
            this.btnFactCalSettings = new System.Windows.Forms.Button();
            this.labAdpSerialNumber = new System.Windows.Forms.Label();
            this.groupMeasSetupAndControl = new System.Windows.Forms.GroupBox();
            this.groupVsup2 = new System.Windows.Forms.GroupBox();
            this.labIsup2Maximum = new System.Windows.Forms.Label();
            this.rbVsup2MeasRange1 = new System.Windows.Forms.RadioButton();
            this.rbVsup2MeasRange0 = new System.Windows.Forms.RadioButton();
            this.cbVsup2Monitor = new System.Windows.Forms.CheckBox();
            this.groupVsup1 = new System.Windows.Forms.GroupBox();
            this.lbIsup1Maximum = new System.Windows.Forms.Label();
            this.rbVsup1MeasRange1 = new System.Windows.Forms.RadioButton();
            this.rbVsup1MeasRange0 = new System.Windows.Forms.RadioButton();
            this.cbVsup1Monitor = new System.Windows.Forms.CheckBox();
            this.tabPageFactory = new System.Windows.Forms.TabPage();
            this.groupFactoryStatCtrl = new System.Windows.Forms.GroupBox();
            this.groupDbgMsgsCtrl = new System.Windows.Forms.GroupBox();
            this.cbShowDebug1 = new System.Windows.Forms.CheckBox();
            this.cbShowDebug2 = new System.Windows.Forms.CheckBox();
            this.btnInfoMsgFormToggleVisible = new System.Windows.Forms.Button();
            this.groupCalStats = new System.Windows.Forms.GroupBox();
            this.txtTimesToRepeatCal = new System.Windows.Forms.TextBox();
            this.btnRunRepeatedCal = new System.Windows.Forms.Button();
            this.labCalibrationCount = new System.Windows.Forms.Label();
            this.groupAdpCtrl = new System.Windows.Forms.GroupBox();
            this.cbVsup1Load = new System.Windows.Forms.CheckBox();
            this.cbVsup2Load = new System.Windows.Forms.CheckBox();
            this.groupAcqStatusCtrl = new System.Windows.Forms.GroupBox();
            this.labCurTimerInterval = new System.Windows.Forms.Label();
            this.cmbPowerPollFreq = new System.Windows.Forms.ComboBox();
            this.btnShowIsrStats = new System.Windows.Forms.Button();
            this.labAcqPollRate = new System.Windows.Forms.Label();
            this.tabPageOther = new System.Windows.Forms.TabPage();
            this.cbTextFileFormatting = new System.Windows.Forms.CheckBox();
            this.cmbMaxPowMonFormTextBoxLines = new System.Windows.Forms.ComboBox();
            this.btnClearTraceTextBox = new System.Windows.Forms.Button();
            this.btnDestroyScope = new System.Windows.Forms.Button();
            this.btnCounts = new System.Windows.Forms.Button();
            this.btnShowCalDiffs = new System.Windows.Forms.Button();
            this.groupPowMonStatAndCtrl = new System.Windows.Forms.GroupBox();
            this.groupRecording = new System.Windows.Forms.GroupBox();
            this.lblLedPlayback = new System.Windows.Forms.Label();
            this.btnStatPlaying = new System.Windows.Forms.Button();
            this.lblLedRecord = new System.Windows.Forms.Label();
            this.btnStatRecording = new System.Windows.Forms.Button();
            this.labRecPlayFileName = new System.Windows.Forms.Label();
            this.cbRecordToFile = new System.Windows.Forms.CheckBox();
            this.txtLogFilePathPlusName = new System.Windows.Forms.TextBox();
            this.btnFileDialog = new System.Windows.Forms.Button();
            this.btnPlayBack = new System.Windows.Forms.Button();
            this.groupAcqControl = new System.Windows.Forms.GroupBox();
            this.btnOscilloscope = new System.Windows.Forms.Button();
            this.cmbUnitsOfTime = new System.Windows.Forms.ComboBox();
            this.cmbTimeToRecord = new System.Windows.Forms.ComboBox();
            this.cmbDMASampleRate = new System.Windows.Forms.ComboBox();
            this.cbContinuousAcquisition = new System.Windows.Forms.CheckBox();
            this.btnStartAcquisition = new System.Windows.Forms.Button();
            this.labAcquisitionTime = new System.Windows.Forms.Label();
            this.labSampleRate = new System.Windows.Forms.Label();
            this.groupSystemMessages = new System.Windows.Forms.GroupBox();
            this.txtSysMsgs = new System.Windows.Forms.TextBox();
            this.btnClrSysMsg = new System.Windows.Forms.Button();
            this.tabControlCustomerEtc.SuspendLayout();
            this.tabPageCustomer.SuspendLayout();
            this.groupCalControl.SuspendLayout();
            this.groupMeasSetupAndControl.SuspendLayout();
            this.groupVsup2.SuspendLayout();
            this.groupVsup1.SuspendLayout();
            this.tabPageFactory.SuspendLayout();
            this.groupFactoryStatCtrl.SuspendLayout();
            this.groupDbgMsgsCtrl.SuspendLayout();
            this.groupCalStats.SuspendLayout();
            this.groupAdpCtrl.SuspendLayout();
            this.groupAcqStatusCtrl.SuspendLayout();
            this.tabPageOther.SuspendLayout();
            this.groupPowMonStatAndCtrl.SuspendLayout();
            this.groupRecording.SuspendLayout();
            this.groupAcqControl.SuspendLayout();
            this.groupSystemMessages.SuspendLayout();
            this.SuspendLayout();
            // 
            // timerPowerPolling
            // 
            this.timerPowerPolling.Tick += new System.EventHandler(this.timerPowerPolling_Tick);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.CheckFileExists = false;
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // lblLinesNowInTraceTextbox
            // 
            this.lblLinesNowInTraceTextbox.AutoSize = true;
            this.lblLinesNowInTraceTextbox.Location = new System.Drawing.Point(272, 192);
            this.lblLinesNowInTraceTextbox.Name = "lblLinesNowInTraceTextbox";
            this.lblLinesNowInTraceTextbox.Size = new System.Drawing.Size(115, 13);
            this.lblLinesNowInTraceTextbox.TabIndex = 29;
            this.lblLinesNowInTraceTextbox.Text = "[Lines In TextBox now]";
            // 
            // lblStateOfChan
            // 
            this.lblStateOfChan.Location = new System.Drawing.Point(0, 0);
            this.lblStateOfChan.Name = "lblStateOfChan";
            this.lblStateOfChan.Size = new System.Drawing.Size(100, 23);
            this.lblStateOfChan.TabIndex = 93;
            // 
            // lblStateOfRange
            // 
            this.lblStateOfRange.Location = new System.Drawing.Point(0, 0);
            this.lblStateOfRange.Name = "lblStateOfRange";
            this.lblStateOfRange.Size = new System.Drawing.Size(100, 23);
            this.lblStateOfRange.TabIndex = 94;
            // 
            // lblStateOfCal
            // 
            this.lblStateOfCal.Location = new System.Drawing.Point(0, 0);
            this.lblStateOfCal.Name = "lblStateOfCal";
            this.lblStateOfCal.Size = new System.Drawing.Size(100, 23);
            this.lblStateOfCal.TabIndex = 95;
            // 
            // lblStopIfNoTrigWait
            // 
            this.lblStopIfNoTrigWait.Location = new System.Drawing.Point(0, 0);
            this.lblStopIfNoTrigWait.Name = "lblStopIfNoTrigWait";
            this.lblStopIfNoTrigWait.Size = new System.Drawing.Size(100, 23);
            this.lblStopIfNoTrigWait.TabIndex = 92;
            // 
            // timerCheckCalAfterFormAppears
            // 
            this.timerCheckCalAfterFormAppears.Tick += new System.EventHandler(this.timerCheckCalAfterFormAppears_Tick);
            // 
            // tabControlCustomerEtc
            // 
            this.tabControlCustomerEtc.Controls.Add(this.tabPageCustomer);
            this.tabControlCustomerEtc.Controls.Add(this.tabPageFactory);
            this.tabControlCustomerEtc.Controls.Add(this.tabPageOther);
            this.tabControlCustomerEtc.Location = new System.Drawing.Point(12, 4);
            this.tabControlCustomerEtc.Name = "tabControlCustomerEtc";
            this.tabControlCustomerEtc.SelectedIndex = 0;
            this.tabControlCustomerEtc.Size = new System.Drawing.Size(936, 219);
            this.tabControlCustomerEtc.TabIndex = 81;
            // 
            // tabPageCustomer
            // 
            this.tabPageCustomer.Controls.Add(this.groupCalControl);
            this.tabPageCustomer.Controls.Add(this.groupMeasSetupAndControl);
            this.tabPageCustomer.Location = new System.Drawing.Point(4, 22);
            this.tabPageCustomer.Name = "tabPageCustomer";
            this.tabPageCustomer.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCustomer.Size = new System.Drawing.Size(928, 193);
            this.tabPageCustomer.TabIndex = 0;
            this.tabPageCustomer.Text = "Customer";
            this.tabPageCustomer.UseVisualStyleBackColor = true;
            // 
            // groupCalControl
            // 
            this.groupCalControl.Controls.Add(this.cmbSn);
            this.groupCalControl.Controls.Add(this.btnAutoPowerCal);
            this.groupCalControl.Controls.Add(this.btnFactCalSettings);
            this.groupCalControl.Controls.Add(this.labAdpSerialNumber);
            this.groupCalControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupCalControl.Location = new System.Drawing.Point(232, 11);
            this.groupCalControl.Name = "groupCalControl";
            this.groupCalControl.Size = new System.Drawing.Size(226, 171);
            this.groupCalControl.TabIndex = 86;
            this.groupCalControl.TabStop = false;
            this.groupCalControl.Text = "Calibration Control";
            // 
            // cmbSn
            // 
            this.cmbSn.BackColor = System.Drawing.Color.Red;
            this.cmbSn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSn.IntegralHeight = false;
            this.cmbSn.Items.AddRange(new object[] {
            "(none)"});
            this.cmbSn.Location = new System.Drawing.Point(119, 95);
            this.cmbSn.Name = "cmbSn";
            this.cmbSn.Size = new System.Drawing.Size(88, 21);
            this.cmbSn.TabIndex = 83;
            this.cmbSn.SelectedIndexChanged += new System.EventHandler(this.cmbSn_SelectedIndexChanged);
            this.cmbSn.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cmbSn_KeyUp);
            this.cmbSn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbSn_KeyDown);
            // 
            // btnAutoPowerCal
            // 
            this.btnAutoPowerCal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAutoPowerCal.Location = new System.Drawing.Point(18, 32);
            this.btnAutoPowerCal.Name = "btnAutoPowerCal";
            this.btnAutoPowerCal.Size = new System.Drawing.Size(87, 42);
            this.btnAutoPowerCal.TabIndex = 82;
            this.btnAutoPowerCal.Text = "Start Calibration";
            this.btnAutoPowerCal.UseVisualStyleBackColor = true;
            this.btnAutoPowerCal.Click += new System.EventHandler(this.btnAutoPowerCal_Click);
            // 
            // btnFactCalSettings
            // 
            this.btnFactCalSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFactCalSettings.Location = new System.Drawing.Point(119, 32);
            this.btnFactCalSettings.Name = "btnFactCalSettings";
            this.btnFactCalSettings.Size = new System.Drawing.Size(88, 42);
            this.btnFactCalSettings.TabIndex = 81;
            this.btnFactCalSettings.Text = "Load\r\nCalibration";
            this.btnFactCalSettings.UseVisualStyleBackColor = true;
            this.btnFactCalSettings.Click += new System.EventHandler(this.btnLoadCalSettings_Click);
            // 
            // labAdpSerialNumber
            // 
            this.labAdpSerialNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labAdpSerialNumber.Location = new System.Drawing.Point(14, 91);
            this.labAdpSerialNumber.Name = "labAdpSerialNumber";
            this.labAdpSerialNumber.Size = new System.Drawing.Size(91, 26);
            this.labAdpSerialNumber.TabIndex = 78;
            this.labAdpSerialNumber.Text = "ADP100 Serial #";
            this.labAdpSerialNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupMeasSetupAndControl
            // 
            this.groupMeasSetupAndControl.Controls.Add(this.groupVsup2);
            this.groupMeasSetupAndControl.Controls.Add(this.groupVsup1);
            this.groupMeasSetupAndControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupMeasSetupAndControl.Location = new System.Drawing.Point(3, 11);
            this.groupMeasSetupAndControl.Name = "groupMeasSetupAndControl";
            this.groupMeasSetupAndControl.Size = new System.Drawing.Size(223, 171);
            this.groupMeasSetupAndControl.TabIndex = 85;
            this.groupMeasSetupAndControl.TabStop = false;
            this.groupMeasSetupAndControl.Text = "Measurement Setup and Control";
            // 
            // groupVsup2
            // 
            this.groupVsup2.Controls.Add(this.labIsup2Maximum);
            this.groupVsup2.Controls.Add(this.rbVsup2MeasRange1);
            this.groupVsup2.Controls.Add(this.rbVsup2MeasRange0);
            this.groupVsup2.Controls.Add(this.cbVsup2Monitor);
            this.groupVsup2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupVsup2.Location = new System.Drawing.Point(14, 96);
            this.groupVsup2.Name = "groupVsup2";
            this.groupVsup2.Size = new System.Drawing.Size(206, 67);
            this.groupVsup2.TabIndex = 85;
            this.groupVsup2.TabStop = false;
            this.groupVsup2.Text = "Vsup2 Configuration";
            // 
            // labIsup2Maximum
            // 
            this.labIsup2Maximum.AutoSize = true;
            this.labIsup2Maximum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labIsup2Maximum.Location = new System.Drawing.Point(85, 16);
            this.labIsup2Maximum.Name = "labIsup2Maximum";
            this.labIsup2Maximum.Size = new System.Drawing.Size(83, 13);
            this.labIsup2Maximum.TabIndex = 3;
            this.labIsup2Maximum.Text = "Isup 2 Maximum";
            // 
            // rbVsup2MeasRange1
            // 
            this.rbVsup2MeasRange1.AutoSize = true;
            this.rbVsup2MeasRange1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbVsup2MeasRange1.Location = new System.Drawing.Point(139, 29);
            this.rbVsup2MeasRange1.Name = "rbVsup2MeasRange1";
            this.rbVsup2MeasRange1.Size = new System.Drawing.Size(54, 17);
            this.rbVsup2MeasRange1.TabIndex = 2;
            this.rbVsup2MeasRange1.Text = "40 ma";
            this.rbVsup2MeasRange1.UseVisualStyleBackColor = true;
            this.rbVsup2MeasRange1.CheckedChanged += new System.EventHandler(this.rbVsup2MeasRange1_CheckedChanged);
            // 
            // rbVsup2MeasRange0
            // 
            this.rbVsup2MeasRange0.AutoSize = true;
            this.rbVsup2MeasRange0.Checked = true;
            this.rbVsup2MeasRange0.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbVsup2MeasRange0.Location = new System.Drawing.Point(85, 29);
            this.rbVsup2MeasRange0.Name = "rbVsup2MeasRange0";
            this.rbVsup2MeasRange0.Size = new System.Drawing.Size(48, 17);
            this.rbVsup2MeasRange0.TabIndex = 1;
            this.rbVsup2MeasRange0.TabStop = true;
            this.rbVsup2MeasRange0.Text = "6 ma";
            this.rbVsup2MeasRange0.UseVisualStyleBackColor = true;
            this.rbVsup2MeasRange0.CheckedChanged += new System.EventHandler(this.rbVsup2MeasRange0_CheckedChanged);
            // 
            // cbVsup2Monitor
            // 
            this.cbVsup2Monitor.AutoSize = true;
            this.cbVsup2Monitor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbVsup2Monitor.Location = new System.Drawing.Point(6, 30);
            this.cbVsup2Monitor.Name = "cbVsup2Monitor";
            this.cbVsup2Monitor.Size = new System.Drawing.Size(59, 17);
            this.cbVsup2Monitor.TabIndex = 0;
            this.cbVsup2Monitor.Text = "Enable";
            this.cbVsup2Monitor.UseVisualStyleBackColor = true;
            this.cbVsup2Monitor.CheckedChanged += new System.EventHandler(this.cbVsup2Monitor_CheckedChanged);
            // 
            // groupVsup1
            // 
            this.groupVsup1.Controls.Add(this.lbIsup1Maximum);
            this.groupVsup1.Controls.Add(this.rbVsup1MeasRange1);
            this.groupVsup1.Controls.Add(this.rbVsup1MeasRange0);
            this.groupVsup1.Controls.Add(this.cbVsup1Monitor);
            this.groupVsup1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupVsup1.Location = new System.Drawing.Point(14, 23);
            this.groupVsup1.Name = "groupVsup1";
            this.groupVsup1.Size = new System.Drawing.Size(206, 67);
            this.groupVsup1.TabIndex = 84;
            this.groupVsup1.TabStop = false;
            this.groupVsup1.Text = "Vsup1 Configuration";
            // 
            // lbIsup1Maximum
            // 
            this.lbIsup1Maximum.AutoSize = true;
            this.lbIsup1Maximum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbIsup1Maximum.Location = new System.Drawing.Point(85, 16);
            this.lbIsup1Maximum.Name = "lbIsup1Maximum";
            this.lbIsup1Maximum.Size = new System.Drawing.Size(83, 13);
            this.lbIsup1Maximum.TabIndex = 3;
            this.lbIsup1Maximum.Text = "Isup 1 Maximum";
            // 
            // rbVsup1MeasRange1
            // 
            this.rbVsup1MeasRange1.AutoSize = true;
            this.rbVsup1MeasRange1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbVsup1MeasRange1.Location = new System.Drawing.Point(139, 29);
            this.rbVsup1MeasRange1.Name = "rbVsup1MeasRange1";
            this.rbVsup1MeasRange1.Size = new System.Drawing.Size(54, 17);
            this.rbVsup1MeasRange1.TabIndex = 2;
            this.rbVsup1MeasRange1.Text = "40 ma";
            this.rbVsup1MeasRange1.UseVisualStyleBackColor = true;
            this.rbVsup1MeasRange1.CheckedChanged += new System.EventHandler(this.rbVsup1MeasRange1_CheckedChanged);
            // 
            // rbVsup1MeasRange0
            // 
            this.rbVsup1MeasRange0.AutoSize = true;
            this.rbVsup1MeasRange0.Checked = true;
            this.rbVsup1MeasRange0.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbVsup1MeasRange0.Location = new System.Drawing.Point(85, 29);
            this.rbVsup1MeasRange0.Name = "rbVsup1MeasRange0";
            this.rbVsup1MeasRange0.Size = new System.Drawing.Size(48, 17);
            this.rbVsup1MeasRange0.TabIndex = 1;
            this.rbVsup1MeasRange0.TabStop = true;
            this.rbVsup1MeasRange0.Text = "6 ma";
            this.rbVsup1MeasRange0.UseVisualStyleBackColor = true;
            this.rbVsup1MeasRange0.CheckedChanged += new System.EventHandler(this.rbVsup1MeasRange0_CheckedChanged);
            // 
            // cbVsup1Monitor
            // 
            this.cbVsup1Monitor.AutoSize = true;
            this.cbVsup1Monitor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbVsup1Monitor.Location = new System.Drawing.Point(6, 30);
            this.cbVsup1Monitor.Name = "cbVsup1Monitor";
            this.cbVsup1Monitor.Size = new System.Drawing.Size(59, 17);
            this.cbVsup1Monitor.TabIndex = 0;
            this.cbVsup1Monitor.Text = "Enable";
            this.cbVsup1Monitor.UseVisualStyleBackColor = true;
            this.cbVsup1Monitor.CheckedChanged += new System.EventHandler(this.cbVsup1Monitor_CheckedChanged);
            // 
            // tabPageFactory
            // 
            this.tabPageFactory.Controls.Add(this.groupFactoryStatCtrl);
            this.tabPageFactory.Controls.Add(this.lblLinesNowInTraceTextbox);
            this.tabPageFactory.Controls.Add(this.lblStopIfNoTrigWait);
            this.tabPageFactory.Controls.Add(this.lblStateOfChan);
            this.tabPageFactory.Controls.Add(this.lblStateOfRange);
            this.tabPageFactory.Controls.Add(this.lblStateOfCal);
            this.tabPageFactory.Location = new System.Drawing.Point(4, 22);
            this.tabPageFactory.Name = "tabPageFactory";
            this.tabPageFactory.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageFactory.Size = new System.Drawing.Size(928, 193);
            this.tabPageFactory.TabIndex = 1;
            this.tabPageFactory.Text = "Factory";
            this.tabPageFactory.UseVisualStyleBackColor = true;
            // 
            // groupFactoryStatCtrl
            // 
            this.groupFactoryStatCtrl.Controls.Add(this.groupDbgMsgsCtrl);
            this.groupFactoryStatCtrl.Controls.Add(this.groupCalStats);
            this.groupFactoryStatCtrl.Controls.Add(this.groupAdpCtrl);
            this.groupFactoryStatCtrl.Controls.Add(this.groupAcqStatusCtrl);
            this.groupFactoryStatCtrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupFactoryStatCtrl.Location = new System.Drawing.Point(3, 8);
            this.groupFactoryStatCtrl.Name = "groupFactoryStatCtrl";
            this.groupFactoryStatCtrl.Size = new System.Drawing.Size(670, 179);
            this.groupFactoryStatCtrl.TabIndex = 91;
            this.groupFactoryStatCtrl.TabStop = false;
            this.groupFactoryStatCtrl.Text = "Debug Status and Control";
            // 
            // groupDbgMsgsCtrl
            // 
            this.groupDbgMsgsCtrl.Controls.Add(this.cbShowDebug1);
            this.groupDbgMsgsCtrl.Controls.Add(this.cbShowDebug2);
            this.groupDbgMsgsCtrl.Controls.Add(this.btnInfoMsgFormToggleVisible);
            this.groupDbgMsgsCtrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupDbgMsgsCtrl.Location = new System.Drawing.Point(9, 19);
            this.groupDbgMsgsCtrl.Name = "groupDbgMsgsCtrl";
            this.groupDbgMsgsCtrl.Size = new System.Drawing.Size(200, 72);
            this.groupDbgMsgsCtrl.TabIndex = 89;
            this.groupDbgMsgsCtrl.TabStop = false;
            this.groupDbgMsgsCtrl.Text = "Debug Messages and Control";
            // 
            // cbShowDebug1
            // 
            this.cbShowDebug1.AutoSize = true;
            this.cbShowDebug1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbShowDebug1.Location = new System.Drawing.Point(8, 19);
            this.cbShowDebug1.Name = "cbShowDebug1";
            this.cbShowDebug1.Size = new System.Drawing.Size(62, 17);
            this.cbShowDebug1.TabIndex = 88;
            this.cbShowDebug1.Text = "debug1";
            this.cbShowDebug1.UseVisualStyleBackColor = true;
            // 
            // cbShowDebug2
            // 
            this.cbShowDebug2.AutoSize = true;
            this.cbShowDebug2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbShowDebug2.Location = new System.Drawing.Point(8, 39);
            this.cbShowDebug2.Name = "cbShowDebug2";
            this.cbShowDebug2.Size = new System.Drawing.Size(62, 17);
            this.cbShowDebug2.TabIndex = 87;
            this.cbShowDebug2.Text = "debug2";
            this.cbShowDebug2.UseVisualStyleBackColor = true;
            // 
            // btnInfoMsgFormToggleVisible
            // 
            this.btnInfoMsgFormToggleVisible.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInfoMsgFormToggleVisible.Location = new System.Drawing.Point(82, 19);
            this.btnInfoMsgFormToggleVisible.Name = "btnInfoMsgFormToggleVisible";
            this.btnInfoMsgFormToggleVisible.Size = new System.Drawing.Size(86, 42);
            this.btnInfoMsgFormToggleVisible.TabIndex = 86;
            this.btnInfoMsgFormToggleVisible.Text = "Information Messages";
            this.btnInfoMsgFormToggleVisible.UseVisualStyleBackColor = true;
            this.btnInfoMsgFormToggleVisible.Click += new System.EventHandler(this.btnInfoMsgFormToggleVisible_Click);
            // 
            // groupCalStats
            // 
            this.groupCalStats.Controls.Add(this.txtTimesToRepeatCal);
            this.groupCalStats.Controls.Add(this.btnRunRepeatedCal);
            this.groupCalStats.Controls.Add(this.labCalibrationCount);
            this.groupCalStats.Location = new System.Drawing.Point(481, 19);
            this.groupCalStats.Name = "groupCalStats";
            this.groupCalStats.Size = new System.Drawing.Size(218, 154);
            this.groupCalStats.TabIndex = 87;
            this.groupCalStats.TabStop = false;
            this.groupCalStats.Text = "Calibration Statistics";
            // 
            // txtTimesToRepeatCal
            // 
            this.txtTimesToRepeatCal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTimesToRepeatCal.Location = new System.Drawing.Point(8, 104);
            this.txtTimesToRepeatCal.Name = "txtTimesToRepeatCal";
            this.txtTimesToRepeatCal.Size = new System.Drawing.Size(86, 20);
            this.txtTimesToRepeatCal.TabIndex = 84;
            // 
            // btnRunRepeatedCal
            // 
            this.btnRunRepeatedCal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRunRepeatedCal.Location = new System.Drawing.Point(8, 19);
            this.btnRunRepeatedCal.Name = "btnRunRepeatedCal";
            this.btnRunRepeatedCal.Size = new System.Drawing.Size(86, 42);
            this.btnRunRepeatedCal.TabIndex = 48;
            this.btnRunRepeatedCal.Text = "Run Calibration";
            this.btnRunRepeatedCal.UseVisualStyleBackColor = true;
            this.btnRunRepeatedCal.Click += new System.EventHandler(this.btnRunRepeatedCal_Click);
            // 
            // labCalibrationCount
            // 
            this.labCalibrationCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labCalibrationCount.Location = new System.Drawing.Point(100, 106);
            this.labCalibrationCount.Name = "labCalibrationCount";
            this.labCalibrationCount.Size = new System.Drawing.Size(87, 17);
            this.labCalibrationCount.TabIndex = 85;
            this.labCalibrationCount.Text = "Calibration Count";
            // 
            // groupAdpCtrl
            // 
            this.groupAdpCtrl.Controls.Add(this.cbVsup1Load);
            this.groupAdpCtrl.Controls.Add(this.cbVsup2Load);
            this.groupAdpCtrl.Location = new System.Drawing.Point(9, 97);
            this.groupAdpCtrl.Name = "groupAdpCtrl";
            this.groupAdpCtrl.Size = new System.Drawing.Size(200, 76);
            this.groupAdpCtrl.TabIndex = 89;
            this.groupAdpCtrl.TabStop = false;
            this.groupAdpCtrl.Text = "ADP Control";
            // 
            // cbVsup1Load
            // 
            this.cbVsup1Load.AutoSize = true;
            this.cbVsup1Load.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbVsup1Load.Location = new System.Drawing.Point(8, 19);
            this.cbVsup1Load.Name = "cbVsup1Load";
            this.cbVsup1Load.Size = new System.Drawing.Size(105, 17);
            this.cbVsup1Load.TabIndex = 82;
            this.cbVsup1Load.Text = "Vsup1 Load (1K)";
            this.cbVsup1Load.UseVisualStyleBackColor = true;
            this.cbVsup1Load.CheckedChanged += new System.EventHandler(this.cbVsup1Load_CheckedChanged);
            // 
            // cbVsup2Load
            // 
            this.cbVsup2Load.AutoSize = true;
            this.cbVsup2Load.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbVsup2Load.Location = new System.Drawing.Point(8, 42);
            this.cbVsup2Load.Name = "cbVsup2Load";
            this.cbVsup2Load.Size = new System.Drawing.Size(105, 17);
            this.cbVsup2Load.TabIndex = 83;
            this.cbVsup2Load.Text = "Vsup2 Load (1K)";
            this.cbVsup2Load.UseVisualStyleBackColor = true;
            this.cbVsup2Load.CheckedChanged += new System.EventHandler(this.cbVsup2Load_CheckedChanged);
            // 
            // groupAcqStatusCtrl
            // 
            this.groupAcqStatusCtrl.Controls.Add(this.labCurTimerInterval);
            this.groupAcqStatusCtrl.Controls.Add(this.cmbPowerPollFreq);
            this.groupAcqStatusCtrl.Controls.Add(this.btnShowIsrStats);
            this.groupAcqStatusCtrl.Controls.Add(this.labAcqPollRate);
            this.groupAcqStatusCtrl.Location = new System.Drawing.Point(216, 19);
            this.groupAcqStatusCtrl.Name = "groupAcqStatusCtrl";
            this.groupAcqStatusCtrl.Size = new System.Drawing.Size(258, 154);
            this.groupAcqStatusCtrl.TabIndex = 88;
            this.groupAcqStatusCtrl.TabStop = false;
            this.groupAcqStatusCtrl.Text = "Acquisition Status and Control";
            // 
            // labCurTimerInterval
            // 
            this.labCurTimerInterval.AutoSize = true;
            this.labCurTimerInterval.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labCurTimerInterval.Location = new System.Drawing.Point(10, 127);
            this.labCurTimerInterval.Name = "labCurTimerInterval";
            this.labCurTimerInterval.Size = new System.Drawing.Size(97, 13);
            this.labCurTimerInterval.TabIndex = 86;
            this.labCurTimerInterval.Text = "Becomes Poll Rate";
            // 
            // cmbPowerPollFreq
            // 
            this.cmbPowerPollFreq.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPowerPollFreq.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPowerPollFreq.FormattingEnabled = true;
            this.cmbPowerPollFreq.Items.AddRange(new object[] {
            "AUTO",
            "1",
            "2",
            "5",
            "10",
            "20",
            "50",
            "100",
            "200",
            "500",
            "1000",
            "2000"});
            this.cmbPowerPollFreq.Location = new System.Drawing.Point(9, 103);
            this.cmbPowerPollFreq.Name = "cmbPowerPollFreq";
            this.cmbPowerPollFreq.Size = new System.Drawing.Size(88, 21);
            this.cmbPowerPollFreq.TabIndex = 85;
            // 
            // btnShowIsrStats
            // 
            this.btnShowIsrStats.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnShowIsrStats.Location = new System.Drawing.Point(9, 19);
            this.btnShowIsrStats.Name = "btnShowIsrStats";
            this.btnShowIsrStats.Size = new System.Drawing.Size(88, 42);
            this.btnShowIsrStats.TabIndex = 73;
            this.btnShowIsrStats.Text = "Polling Statistics";
            this.btnShowIsrStats.UseVisualStyleBackColor = true;
            this.btnShowIsrStats.Click += new System.EventHandler(this.btnShowIsrStats_Click);
            // 
            // labAcqPollRate
            // 
            this.labAcqPollRate.AutoSize = true;
            this.labAcqPollRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labAcqPollRate.Location = new System.Drawing.Point(21, 70);
            this.labAcqPollRate.Name = "labAcqPollRate";
            this.labAcqPollRate.Size = new System.Drawing.Size(72, 26);
            this.labAcqPollRate.TabIndex = 72;
            this.labAcqPollRate.Text = " Acquisition\r\nPoll Rate (ms)";
            // 
            // tabPageOther
            // 
            this.tabPageOther.Controls.Add(this.cbTextFileFormatting);
            this.tabPageOther.Controls.Add(this.cmbMaxPowMonFormTextBoxLines);
            this.tabPageOther.Controls.Add(this.btnClearTraceTextBox);
            this.tabPageOther.Controls.Add(this.btnDestroyScope);
            this.tabPageOther.Controls.Add(this.btnCounts);
            this.tabPageOther.Controls.Add(this.btnShowCalDiffs);
            this.tabPageOther.Location = new System.Drawing.Point(4, 22);
            this.tabPageOther.Name = "tabPageOther";
            this.tabPageOther.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOther.Size = new System.Drawing.Size(928, 193);
            this.tabPageOther.TabIndex = 2;
            this.tabPageOther.Text = "Other";
            this.tabPageOther.UseVisualStyleBackColor = true;
            // 
            // cbTextFileFormatting
            // 
            this.cbTextFileFormatting.AutoSize = true;
            this.cbTextFileFormatting.Checked = true;
            this.cbTextFileFormatting.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbTextFileFormatting.Location = new System.Drawing.Point(19, 130);
            this.cbTextFileFormatting.Name = "cbTextFileFormatting";
            this.cbTextFileFormatting.Size = new System.Drawing.Size(81, 30);
            this.cbTextFileFormatting.TabIndex = 96;
            this.cbTextFileFormatting.Text = "Format files\r\nas CSV text\r\n";
            this.cbTextFileFormatting.UseVisualStyleBackColor = true;
            this.cbTextFileFormatting.CheckedChanged += new System.EventHandler(this.cbTextFileFormatting_CheckedChanged);
            // 
            // cmbMaxPowMonFormTextBoxLines
            // 
            this.cmbMaxPowMonFormTextBoxLines.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMaxPowMonFormTextBoxLines.FormattingEnabled = true;
            this.cmbMaxPowMonFormTextBoxLines.Items.AddRange(new object[] {
            "100",
            "200",
            "500",
            "1000",
            "2000",
            "5000"});
            this.cmbMaxPowMonFormTextBoxLines.Location = new System.Drawing.Point(487, 89);
            this.cmbMaxPowMonFormTextBoxLines.Name = "cmbMaxPowMonFormTextBoxLines";
            this.cmbMaxPowMonFormTextBoxLines.Size = new System.Drawing.Size(115, 21);
            this.cmbMaxPowMonFormTextBoxLines.TabIndex = 94;
            // 
            // btnClearTraceTextBox
            // 
            this.btnClearTraceTextBox.Location = new System.Drawing.Point(384, 34);
            this.btnClearTraceTextBox.Name = "btnClearTraceTextBox";
            this.btnClearTraceTextBox.Size = new System.Drawing.Size(79, 58);
            this.btnClearTraceTextBox.TabIndex = 93;
            this.btnClearTraceTextBox.Text = "Clear Terminal Window";
            this.btnClearTraceTextBox.UseVisualStyleBackColor = true;
            // 
            // btnDestroyScope
            // 
            this.btnDestroyScope.Location = new System.Drawing.Point(400, 113);
            this.btnDestroyScope.Name = "btnDestroyScope";
            this.btnDestroyScope.Size = new System.Drawing.Size(63, 44);
            this.btnDestroyScope.TabIndex = 92;
            this.btnDestroyScope.Text = "Destroy Scope";
            this.btnDestroyScope.UseVisualStyleBackColor = true;
            // 
            // btnCounts
            // 
            this.btnCounts.Location = new System.Drawing.Point(134, 123);
            this.btnCounts.Name = "btnCounts";
            this.btnCounts.Size = new System.Drawing.Size(93, 24);
            this.btnCounts.TabIndex = 89;
            this.btnCounts.Text = "Scale to Counts";
            this.btnCounts.UseVisualStyleBackColor = true;
            this.btnCounts.Click += new System.EventHandler(this.btnCounts_Click);
            // 
            // btnShowCalDiffs
            // 
            this.btnShowCalDiffs.Location = new System.Drawing.Point(132, 50);
            this.btnShowCalDiffs.Name = "btnShowCalDiffs";
            this.btnShowCalDiffs.Size = new System.Drawing.Size(61, 27);
            this.btnShowCalDiffs.TabIndex = 88;
            this.btnShowCalDiffs.Text = "diff";
            this.btnShowCalDiffs.UseVisualStyleBackColor = true;
            // 
            // groupPowMonStatAndCtrl
            // 
            this.groupPowMonStatAndCtrl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.groupPowMonStatAndCtrl.Controls.Add(this.groupRecording);
            this.groupPowMonStatAndCtrl.Controls.Add(this.groupAcqControl);
            this.groupPowMonStatAndCtrl.Controls.Add(this.groupSystemMessages);
            this.groupPowMonStatAndCtrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupPowMonStatAndCtrl.Location = new System.Drawing.Point(12, 227);
            this.groupPowMonStatAndCtrl.Name = "groupPowMonStatAndCtrl";
            this.groupPowMonStatAndCtrl.Size = new System.Drawing.Size(713, 252);
            this.groupPowMonStatAndCtrl.TabIndex = 89;
            this.groupPowMonStatAndCtrl.TabStop = false;
            this.groupPowMonStatAndCtrl.Text = "Power Monitor Status and Control";
            // 
            // groupRecording
            // 
            this.groupRecording.Controls.Add(this.lblLedPlayback);
            this.groupRecording.Controls.Add(this.btnStatPlaying);
            this.groupRecording.Controls.Add(this.lblLedRecord);
            this.groupRecording.Controls.Add(this.btnStatRecording);
            this.groupRecording.Controls.Add(this.labRecPlayFileName);
            this.groupRecording.Controls.Add(this.cbRecordToFile);
            this.groupRecording.Controls.Add(this.txtLogFilePathPlusName);
            this.groupRecording.Controls.Add(this.btnFileDialog);
            this.groupRecording.Controls.Add(this.btnPlayBack);
            this.groupRecording.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupRecording.Location = new System.Drawing.Point(359, 19);
            this.groupRecording.Name = "groupRecording";
            this.groupRecording.Size = new System.Drawing.Size(348, 125);
            this.groupRecording.TabIndex = 89;
            this.groupRecording.TabStop = false;
            this.groupRecording.Text = "Record and Playback Control";
            // 
            // lblLedPlayback
            // 
            this.lblLedPlayback.AutoSize = true;
            this.lblLedPlayback.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLedPlayback.Location = new System.Drawing.Point(196, 49);
            this.lblLedPlayback.Name = "lblLedPlayback";
            this.lblLedPlayback.Size = new System.Drawing.Size(51, 13);
            this.lblLedPlayback.TabIndex = 94;
            this.lblLedPlayback.Text = "Playback";
            // 
            // btnStatPlaying
            // 
            this.btnStatPlaying.BackColor = System.Drawing.Color.White;
            this.btnStatPlaying.Location = new System.Drawing.Point(176, 46);
            this.btnStatPlaying.Name = "btnStatPlaying";
            this.btnStatPlaying.Size = new System.Drawing.Size(18, 18);
            this.btnStatPlaying.TabIndex = 93;
            this.btnStatPlaying.UseVisualStyleBackColor = false;
            // 
            // lblLedRecord
            // 
            this.lblLedRecord.AutoSize = true;
            this.lblLedRecord.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLedRecord.Location = new System.Drawing.Point(196, 24);
            this.lblLedRecord.Name = "lblLedRecord";
            this.lblLedRecord.Size = new System.Drawing.Size(42, 13);
            this.lblLedRecord.TabIndex = 92;
            this.lblLedRecord.Text = "Record";
            // 
            // btnStatRecording
            // 
            this.btnStatRecording.BackColor = System.Drawing.Color.White;
            this.btnStatRecording.Location = new System.Drawing.Point(176, 21);
            this.btnStatRecording.Name = "btnStatRecording";
            this.btnStatRecording.Size = new System.Drawing.Size(18, 18);
            this.btnStatRecording.TabIndex = 91;
            this.btnStatRecording.UseVisualStyleBackColor = false;
            // 
            // labRecPlayFileName
            // 
            this.labRecPlayFileName.AutoSize = true;
            this.labRecPlayFileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labRecPlayFileName.Location = new System.Drawing.Point(22, 67);
            this.labRecPlayFileName.Name = "labRecPlayFileName";
            this.labRecPlayFileName.Size = new System.Drawing.Size(110, 13);
            this.labRecPlayFileName.TabIndex = 90;
            this.labRecPlayFileName.Text = "Record/Playback File";
            // 
            // cbRecordToFile
            // 
            this.cbRecordToFile.AutoSize = true;
            this.cbRecordToFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbRecordToFile.Location = new System.Drawing.Point(101, 25);
            this.cbRecordToFile.Name = "cbRecordToFile";
            this.cbRecordToFile.Size = new System.Drawing.Size(75, 30);
            this.cbRecordToFile.TabIndex = 88;
            this.cbRecordToFile.Text = "Enable\r\nRecording";
            this.cbRecordToFile.UseVisualStyleBackColor = true;
            // 
            // txtLogFilePathPlusName
            // 
            this.txtLogFilePathPlusName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLogFilePathPlusName.Location = new System.Drawing.Point(6, 85);
            this.txtLogFilePathPlusName.Name = "txtLogFilePathPlusName";
            this.txtLogFilePathPlusName.Size = new System.Drawing.Size(142, 20);
            this.txtLogFilePathPlusName.TabIndex = 87;
            // 
            // btnFileDialog
            // 
            this.btnFileDialog.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFileDialog.Location = new System.Drawing.Point(161, 81);
            this.btnFileDialog.Name = "btnFileDialog";
            this.btnFileDialog.Size = new System.Drawing.Size(79, 26);
            this.btnFileDialog.TabIndex = 77;
            this.btnFileDialog.Text = "Browse";
            this.btnFileDialog.UseVisualStyleBackColor = true;
            this.btnFileDialog.Click += new System.EventHandler(this.btnFileDialog_Click);
            // 
            // btnPlayBack
            // 
            this.btnPlayBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlayBack.Location = new System.Drawing.Point(6, 19);
            this.btnPlayBack.Name = "btnPlayBack";
            this.btnPlayBack.Size = new System.Drawing.Size(88, 40);
            this.btnPlayBack.TabIndex = 70;
            this.btnPlayBack.Text = "Start Playback";
            this.btnPlayBack.UseVisualStyleBackColor = true;
            this.btnPlayBack.Click += new System.EventHandler(this.btnPlayBack_Click);
            // 
            // groupAcqControl
            // 
            this.groupAcqControl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.groupAcqControl.Controls.Add(this.btnOscilloscope);
            this.groupAcqControl.Controls.Add(this.cmbUnitsOfTime);
            this.groupAcqControl.Controls.Add(this.cmbTimeToRecord);
            this.groupAcqControl.Controls.Add(this.cmbDMASampleRate);
            this.groupAcqControl.Controls.Add(this.cbContinuousAcquisition);
            this.groupAcqControl.Controls.Add(this.btnStartAcquisition);
            this.groupAcqControl.Controls.Add(this.labAcquisitionTime);
            this.groupAcqControl.Controls.Add(this.labSampleRate);
            this.groupAcqControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupAcqControl.Location = new System.Drawing.Point(9, 19);
            this.groupAcqControl.Name = "groupAcqControl";
            this.groupAcqControl.Size = new System.Drawing.Size(344, 125);
            this.groupAcqControl.TabIndex = 88;
            this.groupAcqControl.TabStop = false;
            this.groupAcqControl.Text = "Acquision Control";
            // 
            // btnOscilloscope
            // 
            this.btnOscilloscope.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOscilloscope.Location = new System.Drawing.Point(6, 73);
            this.btnOscilloscope.Name = "btnOscilloscope";
            this.btnOscilloscope.Size = new System.Drawing.Size(88, 43);
            this.btnOscilloscope.TabIndex = 94;
            this.btnOscilloscope.Text = "Display Oscilloscope";
            this.btnOscilloscope.UseVisualStyleBackColor = true;
            this.btnOscilloscope.Click += new System.EventHandler(this.btnOscilloscope_Click);
            // 
            // cmbUnitsOfTime
            // 
            this.cmbUnitsOfTime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUnitsOfTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUnitsOfTime.FormattingEnabled = true;
            this.cmbUnitsOfTime.Items.AddRange(new object[] {
            "seconds",
            "minutes",
            "hours"});
            this.cmbUnitsOfTime.Location = new System.Drawing.Point(271, 59);
            this.cmbUnitsOfTime.Name = "cmbUnitsOfTime";
            this.cmbUnitsOfTime.Size = new System.Drawing.Size(67, 21);
            this.cmbUnitsOfTime.TabIndex = 93;
            this.cmbUnitsOfTime.SelectedIndexChanged += new System.EventHandler(this.cmbUnitsOfTime_SelectedIndexChanged);
            // 
            // cmbTimeToRecord
            // 
            this.cmbTimeToRecord.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmbTimeToRecord.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTimeToRecord.FormattingEnabled = true;
            this.cmbTimeToRecord.Items.AddRange(new object[] {
            "1",
            "2",
            "5",
            "10",
            "20",
            "50",
            "100",
            "200",
            "500"});
            this.cmbTimeToRecord.Location = new System.Drawing.Point(193, 59);
            this.cmbTimeToRecord.Name = "cmbTimeToRecord";
            this.cmbTimeToRecord.Size = new System.Drawing.Size(73, 21);
            this.cmbTimeToRecord.TabIndex = 92;
            this.cmbTimeToRecord.SelectedIndexChanged += new System.EventHandler(this.cmbTimeToRecord_SelectedIndexChanged);
            this.cmbTimeToRecord.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbTimeToRecord_KeyUp);
            // 
            // cmbDMASampleRate
            // 
            this.cmbDMASampleRate.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cmbDMASampleRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDMASampleRate.FormattingEnabled = true;
            this.cmbDMASampleRate.Items.AddRange(new object[] {
            "16000",
            "14000",
            "12000",
            "10000",
            "8000",
            "4000",
            "2000",
            "1000",
            "500",
            "200",
            "100"});
            this.cmbDMASampleRate.Location = new System.Drawing.Point(193, 90);
            this.cmbDMASampleRate.Name = "cmbDMASampleRate";
            this.cmbDMASampleRate.Size = new System.Drawing.Size(73, 21);
            this.cmbDMASampleRate.TabIndex = 91;
            this.cmbDMASampleRate.SelectedIndexChanged += new System.EventHandler(this.cmbDMASampleRate_SelectedIndexChanged);
            this.cmbDMASampleRate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbDMASampleRate_KeyUp);
            // 
            // cbContinuousAcquisition
            // 
            this.cbContinuousAcquisition.AutoSize = true;
            this.cbContinuousAcquisition.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbContinuousAcquisition.Location = new System.Drawing.Point(105, 32);
            this.cbContinuousAcquisition.Name = "cbContinuousAcquisition";
            this.cbContinuousAcquisition.Size = new System.Drawing.Size(133, 17);
            this.cbContinuousAcquisition.TabIndex = 90;
            this.cbContinuousAcquisition.Text = "Continuous Acquisition";
            this.cbContinuousAcquisition.UseVisualStyleBackColor = true;
            this.cbContinuousAcquisition.CheckedChanged += new System.EventHandler(this.cbContinuousAcquisition_CheckedChanged);
            // 
            // btnStartAcquisition
            // 
            this.btnStartAcquisition.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartAcquisition.Location = new System.Drawing.Point(6, 20);
            this.btnStartAcquisition.Name = "btnStartAcquisition";
            this.btnStartAcquisition.Size = new System.Drawing.Size(88, 40);
            this.btnStartAcquisition.TabIndex = 84;
            this.btnStartAcquisition.Text = "Start\r\nAcquisition";
            this.btnStartAcquisition.UseVisualStyleBackColor = true;
            this.btnStartAcquisition.Click += new System.EventHandler(this.btnRecord_Click);
            // 
            // labAcquisitionTime
            // 
            this.labAcquisitionTime.AutoSize = true;
            this.labAcquisitionTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labAcquisitionTime.Location = new System.Drawing.Point(102, 66);
            this.labAcquisitionTime.Name = "labAcquisitionTime";
            this.labAcquisitionTime.Size = new System.Drawing.Size(84, 13);
            this.labAcquisitionTime.TabIndex = 55;
            this.labAcquisitionTime.Text = "Acquisition Time";
            // 
            // labSampleRate
            // 
            this.labSampleRate.AutoSize = true;
            this.labSampleRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labSampleRate.Location = new System.Drawing.Point(102, 93);
            this.labSampleRate.Name = "labSampleRate";
            this.labSampleRate.Size = new System.Drawing.Size(68, 13);
            this.labSampleRate.TabIndex = 54;
            this.labSampleRate.Text = "Sample Rate";
            // 
            // groupSystemMessages
            // 
            this.groupSystemMessages.Controls.Add(this.txtSysMsgs);
            this.groupSystemMessages.Controls.Add(this.btnClrSysMsg);
            this.groupSystemMessages.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupSystemMessages.Location = new System.Drawing.Point(6, 149);
            this.groupSystemMessages.Name = "groupSystemMessages";
            this.groupSystemMessages.Size = new System.Drawing.Size(699, 103);
            this.groupSystemMessages.TabIndex = 84;
            this.groupSystemMessages.TabStop = false;
            this.groupSystemMessages.Text = "System Messages";
            // 
            // txtSysMsgs
            // 
            this.txtSysMsgs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtSysMsgs.Location = new System.Drawing.Point(4, 18);
            this.txtSysMsgs.MaxLength = 500;
            this.txtSysMsgs.Multiline = true;
            this.txtSysMsgs.Name = "txtSysMsgs";
            this.txtSysMsgs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtSysMsgs.Size = new System.Drawing.Size(638, 85);
            this.txtSysMsgs.TabIndex = 49;
            this.txtSysMsgs.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSysMsgs_KeyDown);
            // 
            // btnClrSysMsg
            // 
            this.btnClrSysMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClrSysMsg.Location = new System.Drawing.Point(645, 18);
            this.btnClrSysMsg.Name = "btnClrSysMsg";
            this.btnClrSysMsg.Size = new System.Drawing.Size(49, 41);
            this.btnClrSysMsg.TabIndex = 47;
            this.btnClrSysMsg.Text = "Clear";
            this.btnClrSysMsg.UseVisualStyleBackColor = true;
            this.btnClrSysMsg.Click += new System.EventHandler(this.btnClrSysMsg_Click);
            // 
            // AdpPowMonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(732, 482);
            this.Controls.Add(this.groupPowMonStatAndCtrl);
            this.Controls.Add(this.tabControlCustomerEtc);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AdpPowMonForm";
            this.Text = "ADP100 Power Monitor Default Caption";
            this.Load += new System.EventHandler(this.AdpPowMonForm_Load);
            this.Shown += new System.EventHandler(this.AdpPowMonForm_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdpPowMonForm_FormClosing);
            this.Resize += new System.EventHandler(this.AdpPowMonForm_Resize);
            this.tabControlCustomerEtc.ResumeLayout(false);
            this.tabPageCustomer.ResumeLayout(false);
            this.groupCalControl.ResumeLayout(false);
            this.groupMeasSetupAndControl.ResumeLayout(false);
            this.groupVsup2.ResumeLayout(false);
            this.groupVsup2.PerformLayout();
            this.groupVsup1.ResumeLayout(false);
            this.groupVsup1.PerformLayout();
            this.tabPageFactory.ResumeLayout(false);
            this.tabPageFactory.PerformLayout();
            this.groupFactoryStatCtrl.ResumeLayout(false);
            this.groupDbgMsgsCtrl.ResumeLayout(false);
            this.groupDbgMsgsCtrl.PerformLayout();
            this.groupCalStats.ResumeLayout(false);
            this.groupCalStats.PerformLayout();
            this.groupAdpCtrl.ResumeLayout(false);
            this.groupAdpCtrl.PerformLayout();
            this.groupAcqStatusCtrl.ResumeLayout(false);
            this.groupAcqStatusCtrl.PerformLayout();
            this.tabPageOther.ResumeLayout(false);
            this.tabPageOther.PerformLayout();
            this.groupPowMonStatAndCtrl.ResumeLayout(false);
            this.groupRecording.ResumeLayout(false);
            this.groupRecording.PerformLayout();
            this.groupAcqControl.ResumeLayout(false);
            this.groupAcqControl.PerformLayout();
            this.groupSystemMessages.ResumeLayout(false);
            this.groupSystemMessages.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timerPowerPolling;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label lblLinesNowInTraceTextbox;
        private System.Windows.Forms.Label lblStateOfChan;
        private System.Windows.Forms.Label lblStateOfRange;
        private System.Windows.Forms.Label lblStateOfCal;
        private System.Windows.Forms.Label lblStopIfNoTrigWait;
        private System.Windows.Forms.Timer timerCheckCalAfterFormAppears;
        private System.Windows.Forms.TabControl tabControlCustomerEtc;
        private System.Windows.Forms.TabPage tabPageCustomer;
        private System.Windows.Forms.TabPage tabPageFactory;
        private System.Windows.Forms.GroupBox groupMeasSetupAndControl;
        private System.Windows.Forms.GroupBox groupVsup2;
        private System.Windows.Forms.Label labIsup2Maximum;
        private System.Windows.Forms.RadioButton rbVsup2MeasRange1;
        private System.Windows.Forms.RadioButton rbVsup2MeasRange0;
        private System.Windows.Forms.CheckBox cbVsup2Monitor;
        private System.Windows.Forms.GroupBox groupVsup1;
        private System.Windows.Forms.Label lbIsup1Maximum;
        private System.Windows.Forms.RadioButton rbVsup1MeasRange1;
        private System.Windows.Forms.RadioButton rbVsup1MeasRange0;
        private System.Windows.Forms.CheckBox cbVsup1Monitor;
        private System.Windows.Forms.GroupBox groupCalControl;
        private System.Windows.Forms.Button btnAutoPowerCal;
        private System.Windows.Forms.Button btnFactCalSettings;
        private System.Windows.Forms.Label labAdpSerialNumber;
        private System.Windows.Forms.ComboBox cmbSn;
        private System.Windows.Forms.GroupBox groupPowMonStatAndCtrl;
        private System.Windows.Forms.GroupBox groupSystemMessages;
        private System.Windows.Forms.Button btnClrSysMsg;
        private System.Windows.Forms.GroupBox groupRecording;
        private System.Windows.Forms.Label lblLedPlayback;
        private System.Windows.Forms.Button btnStatPlaying;
        private System.Windows.Forms.Label lblLedRecord;
        private System.Windows.Forms.Button btnStatRecording;
        private System.Windows.Forms.Label labRecPlayFileName;
        private System.Windows.Forms.CheckBox cbRecordToFile;
        private System.Windows.Forms.TextBox txtLogFilePathPlusName;
        private System.Windows.Forms.Button btnFileDialog;
        private System.Windows.Forms.Button btnPlayBack;
        private System.Windows.Forms.GroupBox groupAcqControl;
        private System.Windows.Forms.Button btnOscilloscope;
        private System.Windows.Forms.ComboBox cmbUnitsOfTime;
        private System.Windows.Forms.ComboBox cmbTimeToRecord;
        private System.Windows.Forms.ComboBox cmbDMASampleRate;
        private System.Windows.Forms.CheckBox cbContinuousAcquisition;
        private System.Windows.Forms.Button btnStartAcquisition;
        private System.Windows.Forms.Label labAcquisitionTime;
        private System.Windows.Forms.Label labSampleRate;
        public System.Windows.Forms.TextBox txtSysMsgs;
        private System.Windows.Forms.TabPage tabPageOther;
        private System.Windows.Forms.Button btnClearTraceTextBox;
        private System.Windows.Forms.Button btnDestroyScope;
        private System.Windows.Forms.Button btnCounts;
        private System.Windows.Forms.Button btnShowCalDiffs;
        private System.Windows.Forms.GroupBox groupFactoryStatCtrl;
        private System.Windows.Forms.GroupBox groupDbgMsgsCtrl;
        private System.Windows.Forms.Button btnInfoMsgFormToggleVisible;
        private System.Windows.Forms.GroupBox groupCalStats;
        private System.Windows.Forms.TextBox txtTimesToRepeatCal;
        private System.Windows.Forms.Button btnRunRepeatedCal;
        private System.Windows.Forms.Label labCalibrationCount;
        private System.Windows.Forms.GroupBox groupAdpCtrl;
        private System.Windows.Forms.CheckBox cbVsup1Load;
        private System.Windows.Forms.CheckBox cbVsup2Load;
        private System.Windows.Forms.GroupBox groupAcqStatusCtrl;
        private System.Windows.Forms.Label labCurTimerInterval;
        private System.Windows.Forms.ComboBox cmbPowerPollFreq;
        private System.Windows.Forms.Button btnShowIsrStats;
        private System.Windows.Forms.Label labAcqPollRate;
        private System.Windows.Forms.CheckBox cbShowDebug1;
        private System.Windows.Forms.CheckBox cbShowDebug2;

        private System.Windows.Forms.ComboBox cmbMaxPowMonFormTextBoxLines;
        private System.Windows.Forms.CheckBox cbTextFileFormatting;
    }
}