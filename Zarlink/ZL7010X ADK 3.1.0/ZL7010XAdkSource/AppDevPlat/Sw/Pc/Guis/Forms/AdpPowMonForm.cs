//
// Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
// This copyrighted work constitutes an unpublished work created in 2012. The
// use of the copyright notice is intended to provide notice that Microsemi
// Semiconductor Corp owns a copyright in this unpublished work; the copyright
// notice is not an admission that publication has occurred. This work contains
// confidential, proprietary information and trade secrets of Microsemi
// Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
// or in part, in any form or by any means without the prior written permission
// of Microsemi Semiconductor Corp. This work is provided on a right to use
// basis subject to additional restrictions set out in the applicable license
// or other agreement.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using Zarlink.Adp.Api;
using Signal_Project;
using System.Threading;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Diagnostics;   // Process.Start()

namespace Zarlink.Adp.Forms
{
    public partial class AdpPowMonForm : Form
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool QueryPerformanceCounter(out long lpPerformanceCount);
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool QueryPerformanceFrequency(out long lpFrequency);
        [DllImport("user32.dll")]
        public static extern Int32 LockWindowUpdate(Int32 hwndLock);
        [DllImport("shlwapi.dll", CharSet = CharSet.Auto)]
        public static extern bool PathRemoveFileSpec([In, Out] StringBuilder pszPath);
        [DllImport("shlwapi.dll", CharSet = CharSet.Auto)]
        public static extern bool PathStripPath([In, Out] StringBuilder pszPath);

        private AdpApi adpApi = new AdpApi();

        // this does not include the name of the .exe
        private string pathOnlyToExeDir = "";

        private string titleOfMezz = "";

        int maxLinesInFormTextbox = 1200;

        // We will stop recording time etc. for stats display after
        // this many blocks (the graphs become unreadable for more)
        const int MAX_DMA_BLOCKS_FOR_STATS = 20;

        // ?? is this available to this GUI via some defined symbol??
        const double ACLK_RATE = 8000000.0;

        // if we are in Cal loop (rather than single cal)
        // we will supress certain messages
        private bool bCalLoopRrunning = false;

        // also we want to be able to supress messages as we go through
        // the 8 cal steps
        private bool bWithinCalProcess = false;

        // so scope can be rescaled only when needed
        private bool bScopeShouldBeReScaled = false;

        // these provide a way to end the Acquire or Playback loop early
        // if the user clicks a button (the button must set this)
        private bool bUser_Requests_Abort = false;
        private bool bAcquisitionInProgress = false;
        private bool bPlaybackInProgress = false;

        // evoloution of split mode required a separate backing store for these:
        public ushort rangeSettingIsup1 = 0;
        public ushort rangeSettingIsup2 = 0;

        // evoloution of a rational methodology required a separate backing store for these:
        public ushort calSettingIsup1 = 0;
        public ushort calSettingIsup2 = 0;

        public ushort adcChanToDmaInForm = 0;

        // due to granularity of using microseconds in the protocol
        // we may not always get exactly the sample rate we requested 
        public double targetUsecBetweenSamples;
        public ushort usecBetweenSamples;

        // an engineering mode user can override this, which is
        // calculated to be 2.5 times the block rate
        public int suggestedPollRate = 0;

        // we look at the data as we pass it to the scope so
        // we can develop these statistics
        public double highest_val_in_oscope;
        public double lowest_val_in_oscope;
        public double range_from_highest_to_lowest_in_oscope;
        public double tot_of_all_beam0_samples;
        public double tot_of_all_beam1_samples;
        public int nrSamplesInOscopeForAvg;

        // These arrays are sized to be able to hold the averages for 
        // all 4 combinations of range/cal= xx (2 binary bits)
        // The channel bit chooses which arrays to use (isup1_* or isup2_*)
        public double[] isup1Avgs = new double[4] { 0.0, 0.0, 0.0, 0.0 };

        public double[] isup2Avgs = new double[4] { 0.0, 0.0, 0.0, 0.0 };

        // These are the scale factors we calculate for the current
        // measuring ADC channels
        public double m_isup1_range0 = 0;  // m = slope
        public double b_isup1_range0 = 0;  // b = intercept
        public double m_isup1_range1 = 0;  // m = slope
        public double b_isup1_range1 = 0;  // b = intercept

        public double m_isup2_range0 = 0;  // m = slope
        public double b_isup2_range0 = 0;  // b = intercept
        public double m_isup2_range1 = 0;  // m = slope
        public double b_isup2_range1 = 0;  // b = intercept

        // these can be calculated after cal factors are
        // put into effect
        double max_isup1_range0 = 0.0;
        double max_isup1_range1 = 0.0;
        double max_isup2_range0 = 0.0;
        double max_isup2_range1 = 0.0;

        // factors below are from SN 0390 when detached from any mezzanine
        // 2/23/2010 1:16:18 PM
        public double fact_m_isup1_range0 =  -570.6894; 
        public double fact_b_isup1_range0 = -3751.0100; 
        public double fact_m_isup1_range1 =   -96.4809; 
        public double fact_b_isup1_range1 = -3776.1530; 
        public double fact_m_isup2_range0 =  -568.4130; 
        public double fact_b_isup2_range0 = -2271.6858; 
        public double fact_m_isup2_range1 =   -96.1758;
        public double fact_b_isup2_range1 = -3522.9403;

        // to prevent re-entering autocal code
        public int withinAutoCalDueToActivate = 0;


        public unsafe AdpPowMonForm(string titleOfMezz, void* adpIdFromCaller)
        {

            string err = "";

            // This name is provided by the launching form even if there 
            // is no connection established to a mezzanine: it then simply
            // reflects which button was pushed on the first launcher.
            // It is used as part of the file name for remembered SN of the ADP
            this.titleOfMezz = titleOfMezz;


            InitializeComponent();

            // set form caption text (must come after InitializeComponent();)
            this.Text = "ADP100 Power Monitor with Application Board "
                + titleOfMezz;

            unsafe
            {
                // copy certain fields since these objects are different
                adpApi.adpId = adpIdFromCaller;
                //adpApi.initialized = commonApiObj.initialized;

            }

            // init file name to path to .exe
            initPowMonLogFileName();

            // The caller passes an adpId.  If it is null, we know it is closed.
            // However, it may also be not null but still closed.  To check for
            // this case, we "ping" the ADP and see what we get.
            if (adpIdFromCaller != null)
            {
                // the caller has passed us a connection, but the connection
                // might be from a time when the mezzanine was alive, and
                // now it is no longer alive.  To figure this out, we will
                // do get ADP board version to see if there is an answer
                int verRslt = adpApi.checkConnection(ref err);

                if (verRslt != 0)
                {
                    // a bad adpId: we will set it to null
                    adpIdFromCaller = null;
                    powMonInfoMsg("Detected non-working adpIdFromCaller! \r\n");

                    goto needToMakeAdpConnection;
                }


                // assume the passed connection is 
                // to an ADP *and* a Mezzanine
                adpApi.bHaveAdpOnlyConnection = false;

                // in this case adpApi.init() has not been called, so do it now
                // to get the error objects initialized
                int rslt = adpApi.init(AdpApi.INIT_ERR_DONT_CONNECT, ref err);
                if (rslt != 0)
                {
                    // MessageBox is appropriate here since the form may not have
                    // appeared yet so we cannot use it's textbox
                    MessageBox.Show("error (" + rslt.ToString() + 
                        ") trying to only init error objects",
                        " adpApi.init() returned error in AdpPowMonForm() call");
                }
            }


needToMakeAdpConnection:

            if (adpIdFromCaller == null)
            {
                DialogResult result= System.Windows.Forms.DialogResult.OK;

                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    // try to open a private connection to the ADP only
                    int rslt = attemptAdpOnlyConnection(ref err);
                    if (rslt != 0)
                    {
                        // prevent the AdpPowMonForm window from opening
                        // throw new ArgumentException("The attempt to make
                        // a connection to only an ADP failed.");
                        powMonInfoMsg("The attempt to make a connection to only an ADP failed.\r\n");
                    }

                }
                else
                {
                    // prevent the AdpPowMonForm window from opening
                    //throw new ArgumentException("Declined offer to attempt an ADP only connection ");
                    powMonInfoMsg("Declined offer to attempt an ADP only connection\r\n");
                }

            }

            return;            
        }

        private int attemptAdpOnlyConnection(ref string err)
        {
            // scope can obscure the message box so hide scope
            if (instOsc != null) instOsc.Hide();

            if (adpApi.init(AdpApi.INIT_ERR_AND_CONNECT, ref err) == 0)
            {
                // success: put a caption on the top of the form
                this.Text = "ADP100 Power Monitor ";
                    //[no Application Board attached] launched as " 
                    //+ titleOfMezz;
                adpApi.bHaveAdpOnlyConnection = true;

                if (instOsc != null) instOsc.Show();

                return (0); // no error
            }
            else
            {
                // cleanup seems to mean close a connection if it is opened
                // (perhaps means close an APD connection if the Mezz could
                // not be found on that ADP)
                //cleanup(ref err);

                string failMsg = "Failure: Unable to connect to the ADP100 board. "
                + "Make sure the ADP100 board to be calibrated does NOT have "
                + "an application board attached.  Also, be sure that no other "
                + "ADP100 boards are connected to a USB port on the PC performing "
                + "the calibration.";

                powMonSysMsg("\r\n");   // code meaning clear SysMsg box
                powMonSysMsg(failMsg);
                powMonInfoMsg(failMsg + "\r\n");

                if (instOsc != null) instOsc.Show();

                return (-1);    // error
            }
        }


        private void initCmbDMASampleRate()
        {
            int j = cmbDMASampleRate.FindStringExact("2000");

            cmbDMASampleRate.SelectedIndex = j;
        }


        private void initCmbPowerPollFreq()
        {
            // set initial index to display in combo boxes
            cmbPowerPollFreq.SelectedIndex = 0; // 1->AUTO
        }


        private void initCmbTimeToRecord()
        {
            // 0 gives 1, so 1 gives 2, 2:5 etc.
            cmbTimeToRecord.SelectedIndex = 1;
        }


        private void initCmbUnitsOfTime()
        {
            // 0=seconds (vs 1=min, 2=hour)
            cmbUnitsOfTime.SelectedIndex = 0;
        }


        private void initCbEnableChansToAcquire()
        {
            // doing this assignment will cause the _CheckedChanged
            // function to be called, which will then do the calculations
            // for the acquisition that are needed and reported in the
            // info message text box

            // default to only chan 1 enabled
            cbVsup1Monitor.Checked = true;
            cbVsup2Monitor.Checked = false;
        }


        private void initAndCalAcqSettings()
        {
            initCmbPowerPollFreq();
            // the calls below will cause the corresponding
            // *_SelectedIndexChanged() event to fire in the
            // order they are called.  We need to prevent 
            // calcAcqParmsFromSettings() from trying to do
            // its calculations before these are all initialized,
            // and therefore within calcAcqParmsFromSettings() we
            // check that the last of the items below has become 
            // valid before doing the calculations
            initCmbDMASampleRate();
            initCmbTimeToRecord();
            initCmbUnitsOfTime();
            // last of the items that contribute to the values
            // calculated by calcAcqParmsFromSettings()
            initCbEnableChansToAcquire();   
        }


        private void AdpPowMonForm_Load(object sender, EventArgs e)
        {
            // get the caption before we change it
            btnStartAcquisition_TextToRestore = btnStartAcquisition.Text;
            btnStartAcquisition_ColorToRestore = btnStartAcquisition.BackColor;
            btnStatRecording_ColorToRestore = btnStatRecording.BackColor;

            // the thread priority cannot be raised higher than the
            // process priority, so make this as high as possible
            //Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.RealTime;

            //Process thisProc = Process.GetCurrentProcess();
            //thisProc.PriorityClass = ProcessPriorityClass.High;
            //Console.WriteLine("    new priority class: {0}", thisProc.PriorityClass);

            if (Thread.CurrentThread.Name == null)
            {
                Thread.CurrentThread.Name = "ThreadOfCurrentTool";
            }
            Thread.CurrentThread.Priority = ThreadPriority.Highest;

            // Determine if Performance Counter is available
            bQpcAvailable = QueryPerformanceFrequency(out qpf_counts_per_sec);

            if (bQpcAvailable)
            {
                // make this conversion factor we can use whenever we want
                // to display useconds
                dQpcCountsPerUs = qpf_counts_per_sec / 1000000.0;
            }

            // Acquisition rate,time, 1 or 2 channel defaults
            initAndCalAcqSettings();

            cmbMaxPowMonFormTextBoxLines.SelectedIndex = 3;  // corresponds to 1000 lines

            // default this
            txtTimesToRepeatCal.Text = "10";

            // we snapshot these so we can resize later since they contain
            // the txtSysMsgs box which is what we want to resize
            originalGroupSystemMessagesSize = groupSystemMessages.Size;
            originalGroupPowMonStatAndCtrlSize = groupPowMonStatAndCtrl.Size;

            // TODO: debug, remove
            //cbShowDebug1.Checked = true;
        }

        // The Other tab can be hidden from a user
        private void removeOtherTab()
        {
            if (otherTabAvailable == true)
            {
                // it is up, so take it away
                //tabPageFactory.* cannot .Remove(): must be done 
                // via tabControlCustomerEtc.TabPages.Remove()
                tabControlCustomerEtc.TabPages.Remove(tabPageOther);
                // mark it as now removed
                otherTabAvailable = false;
            }
        }

        private void restoreOtherTab()
        {
            if (otherTabAvailable == false)
            {
                // it is not up, so add it
                tabControlCustomerEtc.TabPages.Add(tabPageOther);
                // make it as now available
                otherTabAvailable = true;
            }
        }


        // The Factory tab can be hidden from a user
        private void removeFactoryTab()
        {
            if (factoryTabAvailable == true)
            {
                // it is up, so take it away
                //tabPageFactory.* cannot .Remove(): must be done 
                // via tabControlCustomerEtc.TabPages.Remove()
                tabControlCustomerEtc.TabPages.Remove(tabPageFactory);
                // make it as now removed
                factoryTabAvailable = false;
            }
        }


        private void restoreFactoryTab()
        {
            if (factoryTabAvailable == false)
            {
                // it is not up, so add it
                tabControlCustomerEtc.TabPages.Add(tabPageFactory);
                // make it as now available
                factoryTabAvailable = true;
            }
        }


        // accumulate the last 3 keystrokes for a simple password 
        // to hide and unhide the Factory and Other tabs
        private string last3keys = "";

        // this is initialized to true since at the start of the
        // program the Factory tab *is* available until we remove it.
        private bool factoryTabAvailable = true;
        // this is initialized to true since at the start of the
        // program the Other tab *is* available until we remove it.
        private bool otherTabAvailable = true;


        // accept keystrokes and look for the secret password
        private void txtSysMsgs_KeyDown(object sender, KeyEventArgs e)
        {
            // If typing goes on in the this message box, it will be examined
            // for the passwords that turn tabs on or off
            txtSysMsgs.AppendText("<");
            // if the first arg is byte or int, then I can specify the radix to print
            txtSysMsgs.AppendText(System.Convert.ToString((int)e.KeyCode, 10));    // type is Keys
            txtSysMsgs.AppendText("{" + System.Convert.ToString((int)e.KeyCode, 16) + "} ");    // type is Keys
            txtSysMsgs.AppendText(":");
            // type is Keys; prints a single ascii character but always upper
            // case: check the state of the shift if case is needed
            txtSysMsgs.AppendText(System.Convert.ToString(e.KeyData));    
            txtSysMsgs.AppendText(":");
            // type is int; prints a number in hex (radix 16)
            txtSysMsgs.AppendText(System.Convert.ToString(e.KeyValue, 16));    
            txtSysMsgs.AppendText(">\n");

            while (last3keys.Length > 2)
            {
                // take characters out until only 2 are left
                last3keys = last3keys.Remove(0, 1);
            }

            // this may append more than one character, for example
            // the space bar will add the word "Space", apparently
            // the normal behavior of the ToString().
            last3keys += System.Convert.ToString(e.KeyData);

            if (last3keys == "RAZ")
            {
                // secret string will make factory tab visible
                removeFactoryTab();
                removeOtherTab();
            }

            if (last3keys == "ZAR")
            {
                restoreFactoryTab();
                restoreOtherTab();
            }


        }


        // Blocks of sample data from the ADP will each contain a 
        // serial number that increments.
        static int last_sn = -1;

        // how many blocks were lost
        static int skip_count = 0;

        private int checkForAndGetDataIfAvailable()  
        {
            string text_to_add = "";
            int rslt;

            // check for data available and put it into managed memory
            // after transfering it
            rslt = queryPwrCktAndProcessRetData();

            if (rslt != 0)
            {
                // problem, do not try to process data
                return (rslt);
            }

            // build a string that we may display later if we want
            // details about the returned data block
            text_to_add += Convert.ToString(adpApi.managedReturnedSamplesFromDma.dmaSnAvailable);

            if (adpApi.managedReturnedSamplesFromDma.dmaSnAvailable != -1)
            {
                // A data block was returned with the poll response!

                // check for skipped data blocks
                last_sn += 1;

                // Block Serial Numbers should increment by 1 every time, otherwise
                // some data has been lost.             
                if (adpApi.managedReturnedSamplesFromDma.dmaSnAvailable != last_sn)
                {
                    skip_count += adpApi.managedReturnedSamplesFromDma.dmaSnAvailable - last_sn;
                    last_sn = adpApi.managedReturnedSamplesFromDma.dmaSnAvailable;

                    text_to_add += "Lost Data Block Detected " + Convert.ToString(skip_count);

                    // give text to the box
                    powMonSysMsg(text_to_add + "\r\n");
                    powMonInfoMsg(text_to_add + "\r\n");
                    text_to_add = "";   // clear to prevent printing duplicates

                    return (-1);    // report error to caller
                }

                text_to_add += adpApi.managedReturnedSamplesFromDma.dmaSnAvailable.ToString();
                text_to_add += " ";            
            }
            else
            {
                // the poll did not return a data block
            }

            // report the contents of this string which is debug info
            // items added by the code above
            if (cbShowDebug1.Checked)
            {
                // give text to the box
                if (text_to_add.Length !=0)
                powMonInfoMsg(text_to_add + "\r\n");
            }

            return (0); // no error

        }   // end checkForAndGetDataIfAvailable()  


        private void limitLinesInTxtSysMsgs()
        {
            // we want to know how long this scroll operation takes
            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();

            int lines_in_array_now = txtSysMsgs.Lines.Length;

            if (lines_in_array_now > maxLinesInFormTextbox)
            {
                int lines_to_remove = lines_in_array_now - maxLinesInFormTextbox;

                // create a new empty array
                string[] newlines = new string[txtSysMsgs.Lines.Length - lines_to_remove];

                int counter = 0;

                foreach (string line in txtSysMsgs.Lines)
                {
                    if (counter >= lines_to_remove)
                    {
                        newlines[counter - lines_to_remove] = line;
                    }
                    counter++;
                }

                //API Declaration
                //[System.Runtime.InteropServices.DllImport("user32.dll")]
                //static extern Int32 LockWindowUpdate(Int32hwndLock);
                //

                // I noticed a visual FLASHING which could be observed in the
                // following situation: the first 500 lines in the textbox are
                // long lines, the next lines are short lines.  When the total
                // gets to > 1000 I could clearly see the longer lines briefly
                // flashing on in the text box and then quickly they would
                // be replaced with the more recent short lines.  I deduced that
                // this was caused by the screen being updated after the
                // assignment to the new (shortened) buffer (which still has the
                // long lines at the front) and before the scroll to the end
                // where the shorter lines were. 
                // I dug out this as a potential solution and it seemed to work.

                // there is a bizzare behavior when this window is minimized
                // and I try to use LockWindowUpdate():  the icons on the desktop
                // flash every time the polling happened (but only when the  
                // form is minimized).  Here I will test for minimized, and
                // only call LockWindowUpdate if the form is *not* minimized

                // avoid the possibility of the Minimized state changing
                // while we are executing this code
                bool bCalled_LockWindowUpdate = false;

                if (this.WindowState != FormWindowState.Minimized)
                {
                    LockWindowUpdate(this.Handle.ToInt32());
                    bCalled_LockWindowUpdate = true;    // we know we have to call unlock
                }
                
                txtSysMsgs.Lines = newlines;
                //put the cursor at the end of the textbox
                txtSysMsgs.SelectionStart = txtSysMsgs.Text.Length;
                txtSysMsgs.ScrollToCaret();

                // only call unlock if we actually called lock
                if (bCalled_LockWindowUpdate)
                {
                    LockWindowUpdate(0);
                }

                sw.Stop();
            }
        }


        // When a time to acquire is specified it will be converted
        // into a number of blocks based on the sample rate and
        // 1 or 2 channels
        int nrDmaBlocksRequested;

        // count when playing back
        int nrDmaBlocksPlayed = 0;

        // Create arrays to hold "performance verification" data.
        // The purpose of these is to give the user some clue how close to 
        // the limits of the PC to get real-time data from the USB
        // interface when acquiring power logging data.  We use the PC
        // "Performance Counter" to get microsecond time for the start
        // and finish of the command that transfers the data block.
        // If we interpret this carefully via a graph, we can get a 
        // feel for how close we are to the limit of acquiring data
        // without losing any

        long[] usQpcBeforeDmaCmdSent = new long[MAX_DMA_BLOCKS_FOR_STATS];
        long[] usQpcAfterDmaBlkRcvd = new long[MAX_DMA_BLOCKS_FOR_STATS];
        int[] nrPollTicksWhileBusy = new int[MAX_DMA_BLOCKS_FOR_STATS];
        // used for computation and to put the latest block time 
        // into the managed data structure when needed
        long usQpcAfterLatestDmaBlkRcvd = 0;

        // A simpler measure of the margin for acquiring the power logging
        // data is to keep track of how many Query commands we have to send
        // before we get a Data Available.  If this ever falls below 2, we
        // are near the danger zone and may see data loss if the PC is doing
        // anything else.  So we should try to eliminate all other programs
        // running on the PC.

        // If we actually *do* lose data, we are *always* informed of this
        // by the code, but we should make some effort before acquiring
        // to ensure we will be sucessful.

        long[] countQueriesBeforeDataAvailable = new long[MAX_DMA_BLOCKS_FOR_STATS];

        private int nrDmaBlocksReceived = 0;

        private int consecutivePollsWithNoDataAvailable;

        // here we count any these are timer ticks that that cannot be used 
        // because we are currently within the poll function and do not
        // want a recursion
        private int countPollTicksWhileBusy;

        // Some general conventions are required to avoid gross confusion
        // in time results:
        // "qpc" prefix alone is QueryPerformanceCounter() raw counts
        //
        // "usQpc" prefix is a QueryPerformanceCounter() value that has been
        //         converted into useconds
        //
        // "dQpc" is a conversion factor held as a double
        long qpcTemp;

        // initialize to avoid an error report: use of unassigned local variable
        long usQpcBaseForRecording = 1;

        // create arrays to hold time performance data
        //long[] usQpc_before_dma_cmd_sent = new long[MAX_DMA_BLOCKS_ALLOWED];
        //long[] usQpc_after_dma_blk_rcvd = new long[MAX_DMA_BLOCKS_ALLOWED];

        // report what timing resolution is available from this
        // performance counter
        long qpf_counts_per_sec;
        // initialize to avoid an error report: use of unassigned local variable
        double dQpcCountsPerUs = 1;

        // will return false if Performance counter is not available
        bool bQpcAvailable = false;

        // declare these here so they are available anywhere 

        Stream powMonDataLogStream = null;
        bool bPowMonDataLogStreamOpen = false;

        BinaryFormatter bFormatter = new BinaryFormatter();

        // there is no such thing as a text formatter, but this behaves
        // similarly when we open it using a Stream
        StreamWriter tOutFormatter = null;


        private int dmaBuffSingleOrSplit()
        {
            if (cbVsup1Monitor.Checked && cbVsup2Monitor.Checked)
            {
                return DMA_BUF_IS_SPLIT_TWO_ADC12_CHANS;
            }
            else
            {
                return DMA_BUF_IS_SINGLE_ADC12_CHAN;
            }
        }


        private int sendRecordTaskStart()   // old args: object sender, EventArgs e)
        {
            int rslt = -1;

            if (cbRecordToFile.Checked)
            {
                // we want to record to a file so we prepare the file

                // get the path+name from this text box
                string CurrentDataFileName = txtLogFilePathPlusName.Text;

                // ignore any errors here: ok if file does not exist
                try
                {
                    File.Delete(CurrentDataFileName);
                }
                catch (Exception)
                {
                    // do nothing, but catch this exception so there will
                    // not be a run time error
                }

                try
                {
                    // this open is common to both Binary and Text mode recording
                    powMonDataLogStream = File.Open(CurrentDataFileName, FileMode.Create);
                    bPowMonDataLogStreamOpen = true;

                    // here we will add a "formatter" based on the recording choice
                    // of either Text or not text (binary)
                    if (cbTextFileFormatting.Checked)
                    {
                        tOutFormatter = new StreamWriter(powMonDataLogStream);                 
                    }
                    else
                    {
                        // no formatter needed here for binary
                    }
                }
                catch (Exception e)
                {
                    powMonSysMsg("\r\n");   // code meaning clear SysMsg box
                    powMonSysMsg("Error opening file: " + e.ToString() + "\r\n");
                    // turn off the Record check box
                    cbRecordToFile.Checked = false;
                }

            }

            last_sn = -1;   // sn starts here when we start acquiring a group

            if (bQpcAvailable)
            {

                if (cbShowDebug1.Checked)
                //if (1==0)   // printing this is supressed for now
                {
                    powMonInfoMsg(
                    "f=" + qpf_counts_per_sec.ToString() 
                    + " QueryPerformanceFrequency (counts per second); "
                    + "\r\n");
                }

                // record this so we can report time as 0=starting DMA loop
                QueryPerformanceCounter(out qpcTemp);

                // cvt to us always
                usQpcBaseForRecording = (long)(((double)qpcTemp) / dQpcCountsPerUs);


                if (cbShowDebug1.Checked)
                {
                    powMonInfoMsg(
                    " (counts per microsecond)= " + dQpcCountsPerUs.ToString() + "\r\n");
                }

            }

            // Send the START command for DMA acquisition of Power Data

            byte oneOrTwoOrBoth = AdpApi.ADP_POW_1;

            // Test if buffer is one or two channels of data and
            // set a local var for convenience
            if (dmaBuffSingleOrSplit() == DMA_BUF_IS_SINGLE_ADC12_CHAN)
            {
                if (cbVsup1Monitor.Checked) {
                    oneOrTwoOrBoth = AdpApi.ADP_POW_1;
                } else
                if (cbVsup2Monitor.Checked) {
                    oneOrTwoOrBoth = AdpApi.ADP_POW_2;
                }
            }
            else
            {
                oneOrTwoOrBoth = AdpApi.ADP_POW_1_AND_2;
            }

            // Include the channel info in the text format file if we are
            // recording in text (so we can adapt to 1 or 2 channels on playback)
            // Also include settings the acquisition was made at so they can 
            // be restored, and include the block time so playback can be
            // throttled to the same speed we recorded at
            if (cbRecordToFile.Checked)
            {
                if (cbTextFileFormatting.Checked)
                {
                    //File looks like this:
                    //1    Channel acquired (3=both)
                    //2    
                    //seconds    
                    //2000    samples/second
                    //FALSE    continuous
                    //500000    us/block rate
                    //645593    
                    //4.16E-02    
                    //4.16E-02    
                    //4.16E-02    
                    //4.16E-02    
                    //etc.  All the following data is samples


                    tOutFormatter.WriteLine(oneOrTwoOrBoth.ToString() + "\t" + "Channel acquired (3=both)");

                    tOutFormatter.WriteLine(cmbTimeToRecord.Text);
                    tOutFormatter.WriteLine(cmbUnitsOfTime.Text);

                    tOutFormatter.WriteLine(cmbDMASampleRate.Text+ "\t"+"samples/second");
                    tOutFormatter.WriteLine(cbContinuousAcquisition.Checked.ToString() + "\t" + "continuous");
                    tOutFormatter.WriteLine(((Int32)(oneDmaBlockAcqTimeSeconds * 1000000.0)).ToString() + "\t" + "us/block rate");

                }
            }

            rslt =
             adpApi.AdpApiStartPowMon(calSettingIsup1, calSettingIsup2, rangeSettingIsup1, rangeSettingIsup2,
              oneOrTwoOrBoth, usecBetweenSamples);     

            if (rslt != 0)
            {
                //powMonSysMsg("\r\n");   // code meaning clear SysMsg box
                powMonSysMsg("sendRecordTaskStart(): Failed to start DMA (" + rslt.ToString() + ")\r\n");
                return (rslt);
            }

            // This is also tested for 0 to decide if "performance verification"
            // statistical data is available. Resetting it causes the Statistics
            // button to report no data yet
            nrDmaBlocksReceived = 0;

            // Start the timer that will trigger the polling routine
            startPowerPolling();

            return 0;
        }


        private void startPowerPolling()
        {

            // Note that the cmbPowerPollFreq.SelectedIndex *CHANGES*
            // just by mousing over the dropped list

            // AVOID changing things if the user is still making
            // a choice (the dropped list is down).  Otherwise, we can
            // watch the poll rate CHANGE as we mouse over different
            // items in the dropped list!
            if (cmbPowerPollFreq.DroppedDown) return;

            if (cmbPowerPollFreq.SelectedIndex == 0)
            {
                // this is the AUTO setting so use the suggested poll rate
                timerPowerPolling.Interval = suggestedPollRate;
            }
            else
            {
                // We will set the timer based on the  list
                // timer wants ms for interval
                timerPowerPolling.Interval = Convert.ToInt32(cmbPowerPollFreq.SelectedItem);
            }

            // turn on timer events
            timerPowerPolling.Enabled = true;

            // report the interval on the screen for information
            labCurTimerInterval.Text = Convert.ToString(timerPowerPolling.Interval) + " ms";

        }


        private void stopPowerPolling()
        {
            // allow this to be called even when polling is off
            // by checking first
            if (timerPowerPolling.Enabled == true)
            {
                // no more timer events
                timerPowerPolling.Enabled = false;

                // report the interval on the screen for information
                labCurTimerInterval.Text += "Polling Stopped";
            }
        }


        private int pollForDmaData()   
        {
            int rslt = 0;

            // allow a user to stop this loop early
            if (bUser_Requests_Abort == true)
            {
                powMonInfoMsg("USER BREAK REQUESTED AT BLOCK " + nrDmaBlocksReceived.ToString() + "\r\n");
                bUser_Requests_Abort = false;

                // enable those controls we had disabled
                enableAfterRecordOrCal(FROM_RECORD_BTN);

            } else
            if (consecutivePollsWithNoDataAvailable > 500)
            {
                // also abort if too many polls without a data available
                powMonInfoMsg("TOO MANY POLLS WITH NO DMA DATA AT BLOCK " + nrDmaBlocksReceived.ToString() + "\r\n");

            }
            else      // not abort so send next poll command
            {

                consecutivePollsWithNoDataAvailable++;

                // ask if power meas data is available
                rslt = checkForAndGetDataIfAvailable();

                if (rslt != 0)
                {
                    // error so we should abort
                    powMonInfoMsg("ERROR CAUSED BREAK REQUESTED AT BLOCK " + nrDmaBlocksReceived.ToString() + "\r\n");
                    bUser_Requests_Abort = false;

                    // enable those controls we had disabled
                    enableAfterRecordOrCal(FROM_RECORD_BTN);

                    goto error_aborting;

                } else // reply will be in fields of the AdpApi object
                if (adpApi.managedReturnedSamplesFromDma.dmaSnAvailable == -1)
                {
                    // No new data is available, wait for next timer tick
                    // to poll again
                    return (rslt);  // will always be 0 here
                }

                // record how many queries we sent before data available
                // so we can see how close we are to the PC processing limit
                if (nrDmaBlocksReceived < MAX_DMA_BLOCKS_FOR_STATS)
                {
                    countQueriesBeforeDataAvailable[nrDmaBlocksReceived] = consecutivePollsWithNoDataAvailable;

                    nrPollTicksWhileBusy[nrDmaBlocksReceived] = countPollTicksWhileBusy;

                    // we will have the PC compute estimates for sample rates
                    // even though these will be imperfect due to USB message
                    // variations
                    QueryPerformanceCounter(out qpcTemp);

                    usQpcBeforeDmaCmdSent[nrDmaBlocksReceived] = (long)(((double)qpcTemp) / dQpcCountsPerUs);   // cvt to us always
                    // adjust so 0=start of DMA loop
                    usQpcBeforeDmaCmdSent[nrDmaBlocksReceived] = usQpcBeforeDmaCmdSent[nrDmaBlocksReceived] - usQpcBaseForRecording;
                }

                consecutivePollsWithNoDataAvailable = 0;
                countPollTicksWhileBusy = 0;

                showUnsafeReturnedData();              

                // read the time as soon as we can after the block is received
                QueryPerformanceCounter(out usQpcAfterLatestDmaBlkRcvd);

                // cvt to us always
                usQpcAfterLatestDmaBlkRcvd = (long)(((double)usQpcAfterLatestDmaBlkRcvd) / dQpcCountsPerUs);

                // adjust so time0 is start of recording
                usQpcAfterLatestDmaBlkRcvd = usQpcAfterLatestDmaBlkRcvd - usQpcBaseForRecording;

                if (nrDmaBlocksReceived < MAX_DMA_BLOCKS_FOR_STATS)
                {
                    //// read the time as soon as we can after the block is received
                    //QueryPerformanceCounter(out usQpcAfterDmaBlkRcvd[nrDmaBlocksReceived]);

                    //// cvt to us always
                    //usQpcAfterDmaBlkRcvd[nrDmaBlocksReceived] = (long)(((double)usQpcAfterDmaBlkRcvd[nrDmaBlocksReceived]) / dQpcCountsPerUs);

                    // record in stat array
                    usQpcAfterDmaBlkRcvd[nrDmaBlocksReceived] = usQpcAfterLatestDmaBlkRcvd;
                }

                // we may log the data now if requested
                // The first Radio button is for "no" meaning do not record to file
                if (cbRecordToFile.Checked)
                {
                    // one of the other radio buttons must be checked
                    // so write data to the file

                    // put the QPC time into the structure we record
                    adpApi.managedReturnedSamplesFromDma.qpc_us_after_dma_blk_rcvd = usQpcAfterLatestDmaBlkRcvd;
                    if (!bPowMonDataLogStreamOpen)
                    {
                        // stream not opened so nothing to do
                    }
                    else
                    {
                        if (cbTextFileFormatting.Checked) {

                            // we must loop to write this text out
                            int i;
                            for (i = 0; i < AdpApi.NR_SAMPLES_IN_CURRENT_MSG; i++)
                            {
                                tOutFormatter.WriteLine(adpApi.managedReturnedSamplesFromDma.dma_buf_samples[i].ToString("E"));
                            }

                        } else {

                            bFormatter.Serialize(powMonDataLogStream, adpApi.managedReturnedSamplesFromDma);
                        }
                    }
                }

                nrDmaBlocksReceived++;

                // Check for done if applicable
                if (!cbContinuousAcquisition.Checked)
                {
                    // timed record is done when the number of blocks we
                    // calculated as equivalent to the recording time have 
                    // arrived

                    if (nrDmaBlocksReceived < nrDmaBlocksRequested)
                    {
                        // wait until next poll
                        return (0);
                    }
                }
                else
                {
                    // recording only ends by operator button click
                    // wait until next poll
                    return (0);
                }

            }   // end not aborting

error_aborting:
      
            // FINISHED TRANSFERING ALL DATA (or aborting early)
            stopPowerPolling();

            // call the new style interface
            rslt = adpApi.AdpApiStopPowMon();

            if (cbShowDebug2.Checked)
            {
                // after the Stop DMA command report the stop index
                powMonInfoMsg("DMA stop loop index was " + nrDmaBlocksReceived.ToString() + "\r\n");
                powMonInfoMsg("rslt was " + rslt.ToString() + "\r\n");
            }

            // return button to normal color and name
            bAcquisitionInProgress = false;

            // enable those controls we had disabled
            enableAfterRecordOrCal(FROM_RECORD_BTN);
            
            // If we were recording, close the file
            if (cbRecordToFile.Checked)
            {
                if (cbTextFileFormatting.Checked)
                {
                    if (tOutFormatter != null) tOutFormatter.Close();
                }
                else
                {
                    powMonDataLogStreamCloseIfOpen();
                }
            }

            // We cannot compute averages if this is 0
            if (nrSamplesInOscopeForAvg == 0)
            {
                //append message
                powMonInfoMsg("In pollForDmaData, nrSamplesInOscopeForAvg == 0 " + "\r\n");
                return (0);
            }

            // For Calibration, we need to compute averages of acquired
            // data as we cycle through the different settings of Range
            // and Cal bit ON/OFF.  

            // It may be useful to skip this if we are not doing a
            // calibration and then we might have better performance
            // at the higher sampling rates

            // Here is where we save the averages by creating a
            // binary index into a table
            ushort idxAvgsRangeCal;

            // ccrc = chan range cal bit positions
            idxAvgsRangeCal = 0;

            // these two arrays are 4 locations each now
            //idxAvgsRangeCal |= (ushort)(range_var_in_form << 1);     //      x
            //idxAvgsRangeCal |= (ushort)(cal_var_in_form << 0);       //       x

            // choose array for isup1Avgs or isup2Avgs
            // 1 ISUP1_ADC_INPUT
            // 2 ISUP2_ADC_INPUT

            if (adcChanToDmaInForm == 1)
            {
                idxAvgsRangeCal |= (ushort)(rangeSettingIsup1 << 1);
                idxAvgsRangeCal |= (ushort)(calSettingIsup1 << 0);
                               
                // compute new average
                isup1Avgs[idxAvgsRangeCal] = tot_of_all_beam0_samples / nrSamplesInOscopeForAvg;
                // Test if buffer is one or two channels of data
                if (dmaBuffSingleOrSplit() == DMA_BUF_IS_SPLIT_TWO_ADC12_CHANS)
                {
                    isup2Avgs[idxAvgsRangeCal] = tot_of_all_beam1_samples / nrSamplesInOscopeForAvg;
                }

                if (cbShowDebug1.Checked)
                    powMonInfoMsg("DMA chan 1, idxAvgsRangeCal=" + idxAvgsRangeCal.ToString() + "\r\n");
            }


            if (adcChanToDmaInForm == 2)
            {
                idxAvgsRangeCal |= (ushort)(rangeSettingIsup2 << 1);
                idxAvgsRangeCal |= (ushort)(calSettingIsup2 << 0);

                // compute new average/
                isup2Avgs[idxAvgsRangeCal] = tot_of_all_beam0_samples / nrSamplesInOscopeForAvg;

                if (cbShowDebug1.Checked)
                    powMonInfoMsg("DMA chan 2, idxAvgsRangeCal=" + idxAvgsRangeCal.ToString() + "\r\n");
            }

            return (0);

        }

        //
        // Regarding the family of routines with the suffix "Batch"
        // 
        // These have evolved so each one has to process either a
        // single buffer of channel data, or a split buffer that
        // has data from one ADC12 channel in the first half and
        // data from the next ADC12 channel in the second half.
        // 
        // The source data is always the same:
        // adpApi.managed_returned_current_and_dma.dma_buf_samples[i]
        // which is the data from the ADP now in a managed block so we do
        // not have to used fixed/unsafe pointers.
        // 
        // The setting of the cmbDMAOneOrTwo.SelectedIndex tells if the
        // buffer is single ADC12 channel or split between 2 ADC12 channels
        // (0=single, 1=split)
        // 
        //

        const int DMA_BUF_IS_SINGLE_ADC12_CHAN = 0;
        const int DMA_BUF_IS_SPLIT_TWO_ADC12_CHANS = 1;

        // scale a batch of samples using the factors passed in the arguments
        private int scaleBatch(bool bSplit, double m_passed, double b_passed ,double isup2_m_passed, double isup2_b_passed)
        {
            int i,j;
            double beam0_point;
            double beam1_point;

            // Test if buffer is one or two channels of data
            if (!bSplit)
            {
                for (i = 0; i < AdpApi.NR_SAMPLES_IN_CURRENT_MSG; i++)
                {
                    // add data to oscope 1 point at a time
                    // beam0 is data, beam2 and beam3 are 0

                    // get raw value    
                    beam0_point = adpApi.managedReturnedSamplesFromDma.dma_buf_samples[i];    // converts to double

                    // apply scaling
                    beam0_point = m_passed * (beam0_point + b_passed);

                    // put result back in array
                    adpApi.managedReturnedSamplesFromDma.dma_buf_samples[i] = beam0_point;
                }
            }
            else
            {
                // Buffer is split and can only have ISUP1/ISUP2 data in first/second
                // half (split mode is not applicable if any other data source)

                for (i = 0, j = (AdpApi.NR_SAMPLES_IN_CURRENT_MSG / 2); i < (AdpApi.NR_SAMPLES_IN_CURRENT_MSG / 2); i++, j++)
                {
                    // get raw value    
                    beam0_point = adpApi.managedReturnedSamplesFromDma.dma_buf_samples[i];    // converts to double

                    // apply scaling
                    beam0_point = m_passed * (beam0_point + b_passed);

                    // put result back in array
                    adpApi.managedReturnedSamplesFromDma.dma_buf_samples[i] = beam0_point;

                    // get raw value    
                    beam1_point = adpApi.managedReturnedSamplesFromDma.dma_buf_samples[j];    // converts to double

                    // apply scaling
                    beam1_point = isup2_m_passed * (beam1_point + isup2_b_passed);

                    // put result back in array
                    adpApi.managedReturnedSamplesFromDma.dma_buf_samples[j] = beam1_point;
                }

            }
            return (1);

        }


        // this could be used if we the samples are from VBAT_SENSE_ADC_INPUT
        // (which is not currently possible due to the protocol limitations)
        private int scaleBatchVsup(bool bSplit)
        {
            int i,j;
            double beam0_point;
            double beam1_point;

            // Test if buffer is one or two channels of data
            if (!bSplit)
            {
                for (i = 0; i < AdpApi.NR_SAMPLES_IN_CURRENT_MSG; i++)
                {
                    // add data to oscope 1 point at a time
                    // beam1 is data, beam3 and beam3 are 0

                    // get raw value and convert to double
                    beam0_point = adpApi.managedReturnedSamplesFromDma.dma_buf_samples[i];    

                    // apply scaling
                    beam0_point *=  ((2.5 * 1.43 )/ 0xfff);
                    //beam0_point = m_passed * (beam0_point + b_passed);

                    // put result back in array
                    adpApi.managedReturnedSamplesFromDma.dma_buf_samples[i] = beam0_point;
                }
            }
            else
            {
                // Buffer is split 

                for (i = 0, j = (AdpApi.NR_SAMPLES_IN_CURRENT_MSG / 2); i < (AdpApi.NR_SAMPLES_IN_CURRENT_MSG / 2); i++, j++)
                {
                    // get raw value    
                    beam0_point = adpApi.managedReturnedSamplesFromDma.dma_buf_samples[i];    // converts to double

                    // apply scaling
                    beam0_point *=  ((2.5 * 1.43 )/ 0xfff);
                    //beam0_point = m_passed * (beam0_point + b_passed);

                    // put result back in array
                    adpApi.managedReturnedSamplesFromDma.dma_buf_samples[i] = beam0_point;


                    // get raw value    
                    beam1_point = adpApi.managedReturnedSamplesFromDma.dma_buf_samples[j];    // converts to double

                    // apply scaling
                    beam1_point *=  ((2.5 * 1.43 )/ 0xfff);
                    //beam1_point = isup2_m_passed * (beam1_point + isup2_b_passed);

                    // put result back in array
                    adpApi.managedReturnedSamplesFromDma.dma_buf_samples[j] = beam1_point;
                }

            }
            return (1);
        }


        // this could be used if we the samples are from VSUP1_ADC_INPUT or VSUP2_ADC_INPUT
        // (which is not currently possible due to the protocol limitations)
        private int scaleVbatBatch(bool bSplit)
        {
            int i;
            double beam0_point;

            // Test if buffer is one or two channels of data
            if (!bSplit)
            {
                for (i = 0; i < AdpApi.NR_SAMPLES_IN_CURRENT_MSG; i++)
                {
                    // add data to oscope 1 point at a time
                    // beam1 is data, beam3 and beam3 are 0

                    // get raw value    
                    beam0_point = adpApi.managedReturnedSamplesFromDma.dma_buf_samples[i];    // converts to double

                    // apply scaling
                    // counts * (2.5 * 2.0 * 1000)) / 0x0FFF)
                    beam0_point *= ((2.5 * 2.0) / 0xfff);
                    //beam0_point *= ((2.5 * 1.43) / 0xfff);
                    //beam0_point = m_passed * (beam0_point + b_passed);

                    // put result back in array
                    adpApi.managedReturnedSamplesFromDma.dma_buf_samples[i] = beam0_point;


                }
            }
            else
            {
                // Buffer is split and can only have ISUP1/ISUP2 data in first/second
                // half (split mode is not applicable if any other data source)

                powMonInfoMsg("ERROR: cannot be in split mode for scaleVbatBatch()" + "\r\n");
            }
            return (1);

        }


        // computes a total (or 2 totals) for the samples in the batch
        private int totSamplesBatch(bool bSplit)
        {
            int i,j;

            // note we set tot_of_all_beam0_samples to 0 
            // elsewhere since we will make this total over multiple
            // buffer groups

            // Test if buffer is one or two channels of data
            if (!bSplit)
            {
                for (i = 0; i < AdpApi.NR_SAMPLES_IN_CURRENT_MSG; i++)
                {
                    // before any scaling of raw ADC result
                    tot_of_all_beam0_samples += adpApi.managedReturnedSamplesFromDma.dma_buf_samples[i];
                }
            }
            else
            {   
                // Buffer is split and can only have ISUP1/ISUP2 data in first/second
                // half (split mode is not applicable if any other data source)

                for (i = 0, j = (AdpApi.NR_SAMPLES_IN_CURRENT_MSG / 2); i < (AdpApi.NR_SAMPLES_IN_CURRENT_MSG / 2); i++, j++)
                {
                    tot_of_all_beam0_samples += adpApi.managedReturnedSamplesFromDma.dma_buf_samples[i];
                    tot_of_all_beam1_samples += adpApi.managedReturnedSamplesFromDma.dma_buf_samples[j];
                }
            }

            return (1);
        }


        // go through the samples to learn the high and low values
        // (or 2 highs/lows)
        private int learnHighLowBatch(bool bSplit)
        {
            int i,j;
            double beam0_point;

            // Since the High and Low are only reported in the caption,
            // we will only compute this for the ISUP1 samples if we
            // are in the split mode

            // Test if buffer is one or two channels of data
            if (!bSplit)
            {

                for (i = 0; i < AdpApi.NR_SAMPLES_IN_CURRENT_MSG; i++)
                {
                    // before any scaling of raw ADC result
                    beam0_point = adpApi.managedReturnedSamplesFromDma.dma_buf_samples[i]; // raw samples are not really "in" the oscope

                    // learn the highest and lowest values as we pass them in
                    if (beam0_point > highest_val_in_oscope)
                    {
                        highest_val_in_oscope = beam0_point;
                    }
                    if (beam0_point < lowest_val_in_oscope)
                    {
                        lowest_val_in_oscope = beam0_point;
                    }
                }
            }
            else
            {   
                // Buffer is split and can only have ISUP1/ISUP2 data in first/second
                // half (split mode is not applicable if any other data source)

                for (i = 0, j = (AdpApi.NR_SAMPLES_IN_CURRENT_MSG / 2); i < (AdpApi.NR_SAMPLES_IN_CURRENT_MSG / 2); i++, j++)
                {
                    // before any scaling of raw ADC result
                    beam0_point = adpApi.managedReturnedSamplesFromDma.dma_buf_samples[i]; // raw samples are not really "in" the oscope

                    // Note we are ignoring all j index data in this case, see
                    // comment above split test

                    // learn the highest and lowest values as we pass them in
                    if (beam0_point > highest_val_in_oscope)
                    {
                        highest_val_in_oscope = beam0_point;
                    }
                    if (beam0_point < lowest_val_in_oscope)
                    {
                        lowest_val_in_oscope = beam0_point;
                    }
                }
            }

            return (1);
        }

        
        // moves the current batch of data into the scope (either 1 or 2 beams)
        private int putInScopeBatch(bool bSplit)
        {
            int i,j;
            double beam0_point = 0.0;
            double beam1_point = 0.0;
            double beam2_point = 0.0;

            // Test if buffer is one or two channels of data
            if (!bSplit)
            {
                for (i = 0; i < AdpApi.NR_SAMPLES_IN_CURRENT_MSG; i++)
                {
                    beam0_point = adpApi.managedReturnedSamplesFromDma.dma_buf_samples[i]; // raw samples are not really "in" the oscope

                    //experiment beam2_point = beam0_point * 3.3;

                    // put the data point in the scope
                    if (instOsc != null) instOsc.AddData(beam0_point, 0, beam2_point);

                    //cumulative_power += beam0_point * 3.3;
                }
            }
            else
            {   
                // Buffer is split and can only have ISUP1/ISUP2 data in first/second
                // half (split mode is not applicable if any other data source)

                for (i = 0, j = (AdpApi.NR_SAMPLES_IN_CURRENT_MSG / 2); i < (AdpApi.NR_SAMPLES_IN_CURRENT_MSG / 2); i++, j++)
                {
                    beam0_point = adpApi.managedReturnedSamplesFromDma.dma_buf_samples[i]; // raw samples are not really "in" the oscope
                    beam1_point = adpApi.managedReturnedSamplesFromDma.dma_buf_samples[j]; // raw samples are not really "in" the oscope
                    // put the data points in the scope
                    if (instOsc != null) instOsc.AddData(beam0_point, beam1_point, 0);

                    //cumulative_power += beam0_point * 3.3;
                }
            }
            return (1);
        }


        // 
        unsafe private int queryPwrCktAndProcessRetData()
        {

            // This is where to keep the Record button alive
            // incase we want to STOP an acquisition early
            Application.DoEvents();

            // below causes the write-then read transaction
            // Current measurement results will be put in
            // the form variables current_meas_in_form_1 and 
            // current_meas_in_form_2
            int errint = adpApi.AdpQueryAndGetPwrCktData(txtSysMsgs);

            // Ensure there is some report if this call fails
            if (errint != 0)
            {
                powMonInfoMsg(
                "adpApi.AdpQueryAndGetPwrCktData() failed in queryPwrCktAndProcessRetData() "
                + "\r\n");
                powMonInfoMsg(
                "errint = " + errint.ToString()
                + "\r\n");
            }

            return (errint);
        }



        unsafe private void showUnsafeReturnedData()
        {
            bool bSplit;

            // Test if buffer is one or two channels of data and
            // set a local var for convenience
            if (dmaBuffSingleOrSplit() == DMA_BUF_IS_SINGLE_ADC12_CHAN)
            {
                bSplit = false;
            }
            else
            {
                bSplit = true;
            }


            // Here are all the possible ADC12 inputs, but now the protocol
            // only allows ISUP1_ADC_INPUT and/or ISUP2ADC_INPUT

            //0 VBAT_SENSE_ADC_INPUT
            //1 ISUP1_ADC_INPUT
            //2 ISUP2_ADC_INPUT
            //2 BAT_CHARGE_DIFF_AMP_ADC_INPUT
            //4 VSUP1_ADC_INPUT
            //5 VSUP2_ADC_INPUT
            //6 A6
            //7 A7
            //8 VeREF+
            //9 VREF?/VeREF?
            //A Temperature sensor
            //B (AVCC � AVSS) / 2
            //C (AVCC � AVSS) / 2
            //D (AVCC � AVSS) / 2
            //E (AVCC � AVSS) / 2
            //F (AVCC � AVSS) / 2

            // scale the received channels depending on what channel it is
            switch(adcChanToDmaInForm) {


                case 0: //0 VBAT_SENSE_ADC_INPUT
                    scaleVbatBatch(bSplit);
                    totSamplesBatch(bSplit);
                    learnHighLowBatch(bSplit);
                    putInScopeBatch(bSplit);
                    break;


                case 1: //1 ISUP1_ADC_INPUT

                    // Calling this *before* scaling will always give
                    // us raw counts: but do not call it after if what
                    // we want is raw counts!
                    //totSamplesBatch(bSplit);


                    if (rangeSettingIsup1 == 0)
                    {
                        // Range 0
                        // assume this slope (m_...) will only be *exactly* 0
                        // when the scaling values have not been calculated yet
                        if (m_isup1_range0 != 0.0)
                        {
                            // we have the cal factors we need to SCALE to ma

                            // There exists the possibility that, in split mode, the
                            // isup2  input is currently set to the other range!
                            if (rangeSettingIsup2 == 0)
                            {
                                scaleBatch(bSplit, m_isup1_range0, b_isup1_range0, m_isup2_range0, b_isup2_range0);
                            }
                            else
                            {
                                scaleBatch(bSplit, m_isup1_range0, b_isup1_range0, m_isup2_range1, b_isup2_range1);
                            }
                        }
                    }
                    else
                    {
                        // Range 1
                        if (m_isup1_range1 != 0.0)
                        {

                            // we have the cal factors we need to SCALE to ma

                            // There exists the possibility that, in split mode, the
                            // isup2  input is currently set to the other range!
                            if (rangeSettingIsup2 == 0)
                            {
                                scaleBatch(bSplit, m_isup1_range1, b_isup1_range1, m_isup2_range0, b_isup2_range0);
                            }
                            else
                            {
                                scaleBatch(bSplit, m_isup1_range1, b_isup1_range1, m_isup2_range1, b_isup2_range1);
                            }

                        }
                    }

                    totSamplesBatch(bSplit);
                    learnHighLowBatch(bSplit);
                    putInScopeBatch(bSplit);
                    break;


                case 2: //2 ISUP2_ADC_INPUT

                    // Calling this *before* scaling will always give
                    // us raw counts: but do not call it after if what
                    // we want is raw counts!
                    //totSamplesBatch(bSplit);

                    if (rangeSettingIsup2 == 0)
                    {
                        // Range 0
                        // assume this slope (m_...) will only be *exactly* 0
                        // when the scaling values have not been calculated yet
                        if (m_isup2_range0 != 0.0)
                        {

                            // we have the cal factors we need to SCALE to ma

                            // second set of scaling values is not needed here since
                            // we can not be in split mode here
                            scaleBatch(bSplit, m_isup2_range0, b_isup2_range0,0,0);

                        }
                    }
                    else
                    {
                        // Range 1
                        if (m_isup2_range1 != 0.0)
                        {
                            // we have the cal factors we need to SCALE to ma

                            // second set of scaling values is not needed here since
                            // we can not be in split mode here
                            scaleBatch(bSplit,m_isup2_range1, b_isup2_range1,0,0);

                        }
                    }

                    totSamplesBatch(bSplit);
                    learnHighLowBatch(bSplit);
                    putInScopeBatch(bSplit);
                    break;

                // no case for this; result is raw counts
                //3 BAT_CHARGE_DIFF_AMP_ADC_INPUT

                case 4: //4 VSUP1_ADC_INPUT

                    //beam0_point *=  ((2.5 * 1.43 )/ 0xfff);
                    scaleBatchVsup(bSplit);
                    totSamplesBatch(bSplit);
                    learnHighLowBatch(bSplit);
                    putInScopeBatch(bSplit);
                    break;

                case 5: //5 VSUP2_ADC_INPUT

                    //beam0_point *=  ((2.5 * 1.43 )/ 0xfff);
                    scaleBatchVsup(bSplit);
                    totSamplesBatch(bSplit);
                    learnHighLowBatch(bSplit);
                    putInScopeBatch(bSplit);
                    break;

                // no case for these; result is raw counts
                //6 A6
                //7 A7
                //8 VeREF+
                //9 VREF?/VeREF?

                case 0xA:   //A Temperature sensor

                //The typical temperature sensor transfer function is shown in Figure 17?10.
                //When using the temperature sensor, the sample period must be greater than
                //30 ?s. The temperature sensor offset error can be large, and may need to be
                //calibrated for most applications. See device-specific datasheet for
                //parameters.  VTEMP=0.00355(TEMPC)+0.986
                // TEMPC = (VTEMP-0.986)/0.00355

                    //beam0_point *= ((2.5) / 0xfff); // convert to V
                    //beam0_point = (beam0_point - 0.986) / 0.00355;

                    totSamplesBatch(bSplit);
                    learnHighLowBatch(bSplit);
                    putInScopeBatch(bSplit);
                    break;


                // no case for these; result is raw counts
                //B (AVCC � AVSS) / 2
                //C (AVCC � AVSS) / 2
                //D (AVCC � AVSS) / 2
                //E (AVCC � AVSS) / 2
                //F (AVCC � AVSS) / 2
                default:
                    totSamplesBatch(bSplit);
                    learnHighLowBatch(bSplit);
                    putInScopeBatch(bSplit);
                    break;
            }   // end of switch on ADC channel for scaling


            // now reset the range since we may have learned a new value
            range_from_highest_to_lowest_in_oscope = highest_val_in_oscope - lowest_val_in_oscope;

            // check if 1 or 2 channel DMA
            if (dmaBuffSingleOrSplit() == DMA_BUF_IS_SINGLE_ADC12_CHAN)
            {
                nrSamplesInOscopeForAvg += AdpApi.NR_SAMPLES_IN_CURRENT_MSG;
            }
            else
            {
                nrSamplesInOscopeForAvg += (AdpApi.NR_SAMPLES_IN_CURRENT_MSG / 2);
            }

            // Now that we are done moving the data, 
            // we can report things about it

            StringBuilder str = new StringBuilder( "    H=" 
                    + highest_val_in_oscope.ToString("#####.###")  +
                    " L=" + lowest_val_in_oscope.ToString("#####.###") + 
                    " D=" + range_from_highest_to_lowest_in_oscope.ToString("#####.###") 
                + " AV=" + (tot_of_all_beam0_samples / nrSamplesInOscopeForAvg).ToString("#####.000")
                + " n=" + nrSamplesInOscopeForAvg.ToString("#####.")
            );

            // this takes too much time and causes DMA skips
            //AdkInfoMessage( str + "\r\n");

        }

        
        // used to detect unwanted recursion
        bool bWithinPowerPollTick = false;

        // event fired by the timer used to poll for power monitor data
        private void timerPowerPolling_Tick(object sender, EventArgs e)
        {            
            // polling timer has elapsed

            // it can happen that the last tick is still being
            // procesed when the next tick comes along, so try
            // to prevent this with a flag
            // 
            if (bWithinPowerPollTick){

                countPollTicksWhileBusy++;
                return;
            }
            bWithinPowerPollTick = true;

            pollForDmaData();

            bWithinPowerPollTick = false;

        }


        // we need to snapshot these so we can resize later
        private System.Drawing.Size originalGroupSystemMessagesSize;
        private System.Drawing.Size originalGroupPowMonStatAndCtrlSize;

        // only the txtSysMsg box (and the groups that contain it)
        // will change in this resize
        private void AdpPowMonForm_Resize(object sender, EventArgs e)
        {
            System.Drawing.Size tsize;

            // get the graphic object from the handle
            Graphics g = Graphics.FromHwnd(this.Handle);
            // get the bounds of your box
            RectangleF formRect = g.VisibleClipBounds;

            // note this gutter (between the left edge of the TextBox and
            // the left edge of the form) and make the right and bottom 
            // gutters the same
            int currentLeftGutter = txtSysMsgs.Location.X;


            // adjust the groupPowMonStatAndCtrl size
            tsize = originalGroupPowMonStatAndCtrlSize;

            tsize.Height = (int)formRect.Height - groupPowMonStatAndCtrl.Location.Y - currentLeftGutter;

            groupPowMonStatAndCtrl.Size = tsize;

            // adjust the groupSystemMessages size next
            tsize = originalGroupSystemMessagesSize;

            tsize.Height = (int)formRect.Height 
                - (groupSystemMessages.Location.Y + groupPowMonStatAndCtrl.Location.Y)
                - (3*currentLeftGutter);

            groupSystemMessages.Size = tsize;

            // finally adjust the txtSysMsgs size
            tsize = txtSysMsgs.Size;

            // Try to make the gutter on the right side of this textbox 
            // the same as the gutter on the left...we need the *2 since we
            // are going from a *Location* to a *width* (since there is a
            // gutter on both sides)
            // do not affect the width for now
            //tsize.Width = (int)formRect.Width - (2 * currentLeftGutter);

            // We cannot go too far wrong by making the *bottom* gutter
            // the same as the left and right gutters
            tsize.Height = (int)formRect.Height 
                - (txtSysMsgs.Location.Y + groupSystemMessages.Location.Y + groupPowMonStatAndCtrl.Location.Y)
                - (4*currentLeftGutter);

            // this becomes the new TextBox size
            txtSysMsgs.Size = tsize;

        }


        private void closeConnectionIfAdpOnly()
        {
            if (adpApi.bHaveAdpOnlyConnection)
            {
                adpApi.closeAdpOnlyConnection();
                adpApi.bHaveAdpOnlyConnection = false;
            }
        }


        private void AdpPowMonForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (instOsc != null) instOsc.Dispose();
            if (instOsc != null) instOsc = null;

            // Close any API/DLL interfaces that were opened by the ADK module
            // interface. This frees any resources allocated by the API
            // functions, and closes any board connections so they're
            // available to be re-opened later if desired.
            //
            closeConnectionIfAdpOnly();
        }

  
        private void waitForRecordToFinish()
        {
            while (bAcquisitionInProgress)
                Application.DoEvents();

            return;
        }


        private void disableGroupRecording()
        {
            // Now disable all in this group but the btnStatRecording LED(button)
            // and it's separate caption
            foreach (Control c in groupRecording.Controls)
            {

                if ((c == btnStatRecording) || (c.Text == "Record"))
                {
                    c.Enabled = true;
                }
                else
                {
                    c.Enabled = false;
                }
            }
        }


        private void enableGroupRecording()
        {
            // Now enable all in this group 
            foreach (Control c in groupRecording.Controls)
            {
                c.Enabled = true;
            }
        }


        // We need to remember what color this button is so
        // we can turn it RED and then turn it back to normal
        Color btnStartAcquisition_ColorToRestore;
        Color btnStatRecording_ColorToRestore;


        // also the button text caption
        string btnStartAcquisition_TextToRestore = "";

        // Cal can be initiated.....
        // These behave slightly different depending on where they are called from:
        // When normal recording we want to restore the Start Acquisition.
        // When doing a CAL, 
        const int FROM_RECORD_BTN = 1;
        const int FROM_CAL_BTN = 2;

        private void disableBeforeRecordOrCal(int calledFrom)
        {
            // turn button red
            btnStartAcquisition.BackColor = Color.LimeGreen;

            // turn Recording LED(button) red only if we are
            // recording to file
            if (cbRecordToFile.Checked)
            {
                btnStatRecording.BackColor = Color.Red;
            }

            // If the cal button is running, let that code control
            // the disabling of controls.  Otherwise, disable here.
            if ((calState != CAL_STATE_0_NOT_STARTED) && (calledFrom == FROM_RECORD_BTN))
            {
                // cal in progress, don't disable buttons below yet
                return;
            } 

            // Not a cal so this is an ordinary operator clicked to
            // record. Disable certain groups while recording.
            disableGroupRecording();

            // change the Start Acquisition caption
            btnStartAcquisition.Text = "Stop";

            tabControlCustomerEtc.Enabled = false;

            // Now hide all but Record and Display Oscilloscope this group
            foreach (Control c in groupAcqControl.Controls)
            {
                //btnStartAcquisition
                //btnOscilloscope
                if (c.Text == btnStartAcquisition.Text)
                {
                    if (calledFrom == FROM_CAL_BTN)
                    {   // disable
                        c.Enabled = false;
                    }
                    else
                    {
                        // leave this enabled
                    }
                } else
                if (c.Text == btnOscilloscope.Text)
                {
                    if (calledFrom == FROM_CAL_BTN)
                    {   // disable
                        c.Enabled = false;
                    }
                    else
                    {
                        // leave this enabled
                    }
                }
                else {
                    c.Enabled = false;
                }
            }

            return;
        }


        private void enableAfterRecordOrCal(int calledFrom)
        {
            // restore this button caption 
            btnStartAcquisition.Text = btnStartAcquisition_TextToRestore;
            // restore colors
            btnStartAcquisition.BackColor = btnStartAcquisition_ColorToRestore;

            btnStatRecording.BackColor = btnStatRecording_ColorToRestore;

            // If the cal button is running, let that code control
            // the enabling of controls.  Otherwise, enable here
            if ((calState != CAL_STATE_0_NOT_STARTED) && (calledFrom == FROM_RECORD_BTN ))
            {
                // cal in progress, don't enable buttons below yet
                return;
            } 

            // enable certain groups while recording
            enableGroupRecording();
            // disable all of these tabs while recording
            tabControlCustomerEtc.Enabled = true;


            // When enabling these we do not have to exclude any
            foreach (Control c in groupAcqControl.Controls)
            {
                c.Enabled = true;
            }

            return;
        }


        // This will start the process for getting power data from the embedded
        private void btnRecord_Click(object sender, EventArgs e)
        {

            // also start stat chart rotation over next time
            whichStatChartShowing = 0;

            // this allows us to process a second click as an abort
            if (bAcquisitionInProgress == true)
            {
                // set this flag so the inner loop can notice it
                // and exit this click
                bUser_Requests_Abort = true;
                return;
            };

            // disable some controls, but not all, so we can stop/abort
            // and remember state info
            // To test for correct processing of inappropriate command while DMA
            // in progress, we need to comment the line below and then we can click
            // on the Vsup1 configuration buttons to get send an inappropriate
            // command
            disableBeforeRecordOrCal(FROM_RECORD_BTN);

            // this allows us to process a second click as an abort
            bAcquisitionInProgress = true;

            // clear the oscope trace before doing the DMA acquisition
            if(instOsc != null) instOsc.Clear();

            // this will re-write the scope .ini file incase the number
            // of beams (channels) has changed since it was painted last
            if (bScopeShouldBeReScaled)
            {
                rePaintScopeInSameLocation();
                bScopeShouldBeReScaled = false;
            }

            string newcaption = "";

            // choose our channel to DMA
            if (cbVsup1Monitor.Checked && cbVsup2Monitor.Checked) {
                // for both channels ask for 1
                adcChanToDmaInForm = 1;
                newcaption = "1 and 2";
            }
            else
            if (cbVsup1Monitor.Checked) {           
                adcChanToDmaInForm = 1;
                newcaption = "1";
            }
            else
            if (cbVsup2Monitor.Checked)
            {
                adcChanToDmaInForm = 2;
                newcaption = "2";
            }

            // re-display the data
            if (instOsc != null) instOsc.Caption("Data from Mezzanine ADC12 Channel " + newcaption);

            // reset what we know about the contents of the oscope
            // here (at the click) since we may do multiple loops
            // of the DMA message to fill the oscope
            range_from_highest_to_lowest_in_oscope=0;
            highest_val_in_oscope = double.MinValue;
            lowest_val_in_oscope = double.MaxValue;

            // below are used to produce the averages
            tot_of_all_beam0_samples = 0.0;
            tot_of_all_beam1_samples = 0.0;
            nrSamplesInOscopeForAvg = 0;

            sendRecordTaskStart();
            // Record is not finished at this point, but is
            // running based timer ticks

            return;
        }

        void paintOrRePaintScope()
        {            
            // this sequence was found to be required to re-write the
            // scope.  otherwise the new values would not appear on the
            // scope until we manually performed a disconnect-connect
            // sequence using the scope buttons
            Oscilloscope tempOsc = null;

            if (instOsc != null)
            {
                // copy a ref to the existing scope
                tempOsc = instOsc;
            }

            // the following does a new in Create()
            instOsc = Oscilloscope.Create();

            if (instOsc == null)
            {
                powMonInfoMsg("paintOrRePaintScope() Unable to create Oscilloscope, possibly DLL Osc_DLL.dll is missing.\r\n");
                if (tempOsc != null)
                {
                    // restore
                    instOsc = tempOsc;
                    tempOsc = null;
                }
            }
            else
            {
                if(cbShowDebug1.Checked)
                    powMonInfoMsg("paintOrRePaintScope() New Oscilloscope, scopeHandle = " + instOsc.scopeHandle.ToString() + "\r\n");

                if (tempOsc != null)
                {
                    // dispose old one now
                    tempOsc.Dispose();
                    tempOsc = null;
                }
            }

            if (instOsc != null) instOsc.Show();
        }


        // this is useful when the scale needs to be changed
        private void rePaintScopeInSameLocation()
        {    
            //System.Drawing.Point oscFormLocation, System.Drawing.Size oscFormSize)
            if (instOsc != null)
            {
                // learn the current location so we can re-paint without moving it
                if (instOsc != null) instOsc.GetScopeLocAndSize();

                if (cbShowDebug1.Checked)
                    powMonInfoMsg("rePaintScopeInSameLocation() learned current scope location\r\n");
                if (cbShowDebug1.Checked)
                    powMonInfoMsg("  Oscilloscope.oscFormLocation(X,Y)= " + Oscilloscope.oscFormLocation.X.ToString() + "," + Oscilloscope.oscFormLocation.Y.ToString() + " ");
                if (cbShowDebug1.Checked)
                    powMonInfoMsg("  Oscilloscope.oscFormSize(H,W)= " + Oscilloscope.oscFormSize.Height.ToString() + "," + Oscilloscope.oscFormSize.Width.ToString() + "\r\n");

                reWriteScopeIniFileOnly(Oscilloscope.oscFormLocation, Oscilloscope.oscFormSize);
                paintOrRePaintScope();

            }
        }


        // this is useful when the scope needs to be moved
        private void rePaintScopeInNewLocation(System.Drawing.Point oscFormLocation, System.Drawing.Size oscFormSize)
        {
            //System.Drawing.Point oscFormLocation, System.Drawing.Size oscFormSize)
            if (instOsc != null)
            {
                reWriteScopeIniFileOnly(oscFormLocation, oscFormSize);
                paintOrRePaintScope();

            }
        }


        // this may be called before an instance of the scope has been
        // created so do not make any assignments to the scope instance.
        // the *Only means it does not do any scope operations other
        // than writing the .ini file, eg no .Hide, .Show, etc.)
        private void reWriteScopeIniFileOnly(System.Drawing.Point oscFormLocation ,System.Drawing.Size oscFormSize )
        {
            string caption = "";

            // We will re-create the whole ini file so we can affect
            // settings that are not settable via the oscope API calls

            // delete if there
            File.Delete(pathOnlyToExeDir + @"\Scope_Desk.ini");

            StreamWriter sw;
            try
            {
                sw = new StreamWriter(pathOnlyToExeDir + @"\Scope_Desk.ini");

                sw.WriteLine("[Oscilloscope]");
                sw.WriteLine("OscVrulFontName=Arial");
                sw.WriteLine("OscVrulFontSize=8");
                sw.WriteLine("OscVrulFontStyleBold=1");
                sw.WriteLine("OscVrulFontStyleItalic=0");
                sw.WriteLine("OscBackColor=00000000");
                sw.WriteLine("OscGridColor=00404040");
                sw.WriteLine("OscHorScaleColor=00FFFFFF");
                sw.WriteLine("OscBeam0Color=0000FF00");
                sw.WriteLine("OscBeam1Color=00FF00FF");
                sw.WriteLine("OscBeam2Color=0000FFFF");
                sw.WriteLine("OscVrulGenFontColor=000000FF");
                sw.WriteLine("OscZeroLineColor=00808080");
                // order in ini file can be anything so this was
                // moved until after the scaling caption is generated
                // sw.WriteLine("OscOscCaption=Oscilloscope");


                if (dmaBuffSingleOrSplit() == DMA_BUF_IS_SINGLE_ADC12_CHAN)
                //if (!bSplit)
                {
                    sw.WriteLine("ShowSecondBeam=0");
                } else
                {
                    sw.WriteLine("ShowSecondBeam=1");
                }

                sw.WriteLine("ShowThirdBeam=0");
                sw.WriteLine("ShowTestButton=1");
                sw.WriteLine("BfrSmplSize=100000");
                sw.WriteLine("BuffersRecordType=1");
                sw.WriteLine("CreateSecondBeam=1");
                sw.WriteLine("CreateThirdBeam=1");
                sw.WriteLine("SpreadViewTiming=1");
                sw.WriteLine("EventsProcDivider=1000");
                sw.WriteLine("GridRefreshDivider=41");
                sw.WriteLine("GridCellPixelsSize=40");
                sw.WriteLine("TriggerEnable=0");
                sw.WriteLine("TriggerSource=0");
                sw.WriteLine("SecondTriggerLevel=0.0000000000");
                sw.WriteLine("ThirdTriggerLevel=0.0000000000");
                sw.WriteLine("ExtrnTriggerLevel=0.0000000000");
                sw.WriteLine("FirstTriggerLevel=0.0000000000");
                sw.WriteLine("TimeSamplesPerCell=2000.0000000000");

                // Here we assume that if this max has been computed (and
                // so it is non-zero) then we should scale for ma.
                // This could test for any max_* for our purposes:
                // earlier in the evolution of this program it was possible
                // to have only some CAL factors in effect, but now, if
                // any is in effect we assump they will ALL be in effect.
                if (m_isup1_range0 == 0.0)
                {
                    caption += "All beams: ADC counts";
                    // scale for ADC counts
                    sw.WriteLine("Beam0VertScale=500");
                    sw.WriteLine("Beam1VertScale=500");
                    sw.WriteLine("Beam2VertScale=500");

                    sw.WriteLine("Beam0VertOffset=-2000");
                    sw.WriteLine("Beam1VertOffset=-2000");
                    sw.WriteLine("Beam2VertOffset=-2000");
                }
                else
                {
                    // scale based on requested settings
                    if (dmaBuffSingleOrSplit() == DMA_BUF_IS_SINGLE_ADC12_CHAN)
                    {
                        caption += " 1 Chan ";
                    }
                    else
                    {
                        caption += " 2 Chan ";
                    }

                    // scale for ma based on the range that is in effect
                    // Note that isup2 may be displayed in Beam0 for
                    // single channel, but in two channel it will ALWAYS
                    // be in Beam1

                    if (cbVsup1Monitor.Checked) // 1=ISUP1_ADC_INPUT
                    {
                        caption += "VSUP1 on ";
                        if (rangeSettingIsup1 == 0)
                        {
                            //sw.WriteLine("Beam0VertScale={0}", max_isup1_range0 / 8);
                            //sw.WriteLine("Beam0VertOffset={0}", -max_isup1_range0 / 2);
                            sw.WriteLine("Beam0VertScale={0}", (8) / 8);
                            sw.WriteLine("Beam0VertOffset={0}", (-8) / 2);
                            caption += "Beam0 is ma range0 (6ma)";

                        }
                        if (rangeSettingIsup1 == 1)
                        {
                            //sw.WriteLine("Beam0VertScale={0}", max_isup1_range1 / 8);
                            //sw.WriteLine("Beam0VertOffset={0}", -max_isup1_range1 / 2);
                            sw.WriteLine("Beam0VertScale={0}", (42) / 8);
                            sw.WriteLine("Beam0VertOffset={0}", (-42) / 2);
                            caption += "Beam0 is ma range1 (40ma)";
                        }
                    }
                    
                    if (cbVsup2Monitor.Checked) // 2=ISUP2_ADC_INPUT
                    {
                        caption += "VSUP2 on ";

                        if (rangeSettingIsup2 == 0)
                        {
                            //sw.WriteLine("Beam0VertScale={0}", max_isup2_range0 / 8);
                            //sw.WriteLine("Beam0VertOffset={0}", -max_isup2_range0 / 2);
                            sw.WriteLine("Beam0VertScale={0}", (8) / 8);
                            sw.WriteLine("Beam0VertOffset={0}", (-8) / 2);
                            caption += "Beam0 is ma range0 ";
                        }
                        if (rangeSettingIsup2 == 1)
                        {
                            //sw.WriteLine("Beam0VertScale={0}", max_isup2_range1 / 8);
                            //sw.WriteLine("Beam0VertOffset={0}", -max_isup2_range0 / 2);
                            sw.WriteLine("Beam0VertScale={0}", (42) / 8);
                            sw.WriteLine("Beam0VertOffset={0}", (-42) / 2);
                            caption += "Beam0 is ma range1 ";
                        }
                    }

                    // The scale and offset have been written for Beam0.
                    // So, here for Beam1 we can always assume it will be
                    // Isup2 (although Beam1 might not be enabled)
                    if (rangeSettingIsup2 == 0)
                    {
                        //sw.WriteLine("Beam1VertScale={0}", max_isup2_range0 / 8);
                        //sw.WriteLine("Beam1VertOffset={0}", -max_isup2_range0 / 2);
                        sw.WriteLine("Beam1VertScale={0}", 8 / 8);
                        sw.WriteLine("Beam1VertOffset={0}", (-8) / 2);
                        caption += "Beam1 is ma range0 ";
                    }
                    if (rangeSettingIsup2 == 1)
                    {
                        //sw.WriteLine("Beam1VertScale={0}", max_isup2_range1 / 8);
                        //sw.WriteLine("Beam1VertOffset={0}", -max_isup2_range1 / 2);
                        sw.WriteLine("Beam1VertScale={0}", 42 / 8);
                        sw.WriteLine("Beam1VertOffset={0}", (-42) / 2);
                        caption += "Beam1 is ma range1 ";
                    }

                    // in dual chan mode Beam1 is always isup2
                    // in single chan mode Beam1 scaling does not matter
                    // so we only need to test for it for the caption
                    if (dmaBuffSingleOrSplit() == DMA_BUF_IS_SPLIT_TWO_ADC12_CHANS )
                    {
                        // must be both channels 
                        caption += "VSUP2 on ";
                    }
                    else
                    {
                        caption += " Beam1 off ";
                    }

                    // the meaning of this beam is TBD
                    sw.WriteLine("Beam2VertScale={0}", max_isup2_range0/8);
                    // the meaning of this beam is TBD
                    sw.WriteLine("Beam2VertOffset={0}", -max_isup2_range0/2);

                }
                sw.WriteLine("OscOscCaption=" + caption);

                sw.WriteLine("ScopeFormTop={0}", oscFormLocation.Y);
                sw.WriteLine("ScopeFormLeft={0}", oscFormLocation.X);
                sw.WriteLine("ScopeFormHeight={0}", oscFormSize.Height);
                sw.WriteLine("ScopeFormWidth={0}", oscFormSize.Width);

                sw.WriteLine("ScopeStayOnTop=0");
                sw.WriteLine("ScopeShowHints=1");
                sw.WriteLine("ShowTriggerControls=0");
                sw.WriteLine("ShowOscJaggies=0");
                sw.WriteLine("ShowOscHexVal=0");
                sw.WriteLine("ScopeAutoSweep=0");
                sw.WriteLine("ScopeAutoSweepDivider=4");
                sw.WriteLine("ScopeShowAutoSweep=0");
                sw.WriteLine("ShowHexCheckBox=1");
                sw.WriteLine("ScopeInputDumpSize=0");
                sw.WriteLine("Producer_Vendor_Name=+972 507669424 Michael Bernstein, E_Mail: brnstin@cheerful.com");
                sw.WriteLine("ScopePolylineOptimize=0");
                sw.WriteLine("");
                sw.WriteLine("[Miscellaneous]");
                sw.WriteLine("ReadDelayPerSample=0");
                sw.WriteLine("");
                sw.WriteLine("");
                sw.WriteLine("");
                sw.WriteLine("");

           }

            catch (Exception ex)
            {
                //Console.WriteLine(
                powMonSysMsg("Exception caught. " + ex.ToString());
                return;
            }

            sw.Close();

            // remember the Location/Size in variables of the scope class
            // which can be done even if no instances have been declared
            Oscilloscope.oscFormLocation = oscFormLocation;

            Oscilloscope.oscFormSize = oscFormSize;

            if (cbShowDebug1.Checked)
                powMonInfoMsg("reWriteScopeIniFileOnly() setting scope instance variables to passed-in loc and size\r\n");
            if (cbShowDebug1.Checked)
                powMonInfoMsg("  Oscilloscope.oscFormLocation(X,Y)= " + Oscilloscope.oscFormLocation.X.ToString() + "," + Oscilloscope.oscFormLocation.Y.ToString() + " ");
            if (cbShowDebug1.Checked)
                powMonInfoMsg("  Oscilloscope.oscFormSize(H,W)= " + Oscilloscope.oscFormSize.Height.ToString() + "," + Oscilloscope.oscFormSize.Width.ToString() + "\r\n");


        }


        
        // we may create one instance of the oscilloscope

        // this does not call either constructor until the 
        // button is pressed.... instOsc is null, but static member
        // functions can be called on this object, and static member
        // variables exist on this object even though it is null
        // (because static means only one instance for the class
        // no matter how many instances of the object)
        // Oscilloscope instOsc;
        Oscilloscope instOsc = null;


        private void btnOscilloscope_Click(object sender, EventArgs e)
        {
            // adjust the position of the form window when the oscope is shown

            // We may want to use the total width of the display which
            // we can get from the Bounds member of the screen object.
            // there may be more than one screen! TODO
            Screen[] allScreens = Screen.AllScreens;


            // this gets the position of the AdpPowMonForm on the screen
            System.Drawing.Point tLocation = this.Location;

            //// move it to the extreme left side
            //tLocation.X = 0;
            //// move it to the extreme top edge
            //tLocation.Y = 0;
            //// this sets the position of the form
            //this.Location = tLocation;

            // get the size of the AdpPowMonForm on the screen
            System.Drawing.Size tSize = this.Size;

            // variables to pass to the scope resize function
            System.Drawing.Point oscFormLocation = new System.Drawing.Point();
            System.Drawing.Size oscFormSize = new System.Drawing.Size();
        
            // make the scope position immediately to the right of
            // the AdpPowMonForm (as if docked)
            oscFormLocation.X = tLocation.X + tSize.Width;
            oscFormLocation.Y = tLocation.Y;


            // make the scope height the same as the AdpPowMon form
            oscFormSize.Height = this.Height;
            // make the scope width fill what is left of the screen
            oscFormSize.Width = allScreens[0].Bounds.Width - tSize.Width;

            if (cbShowDebug1.Checked)
                powMonInfoMsg("btnOscilloscope_Click() computed docked location:\r\n");
            if (cbShowDebug1.Checked)
                powMonInfoMsg("  temp oscFormLocation(X,Y)= " + oscFormLocation.X.ToString() + "," + oscFormLocation.Y.ToString() + " ");
            if (cbShowDebug1.Checked)
                powMonInfoMsg("  temp oscFormSize(H,W)= " + oscFormSize.Height.ToString() + "," + oscFormSize.Width.ToString() + "\r\n");

            // this is needed to create the first instance so the
            // scope can appear
            paintOrRePaintScope();

            // that dispose problem reared it's head again (the new scope
            // appears but does not accept new data). This helped!?!?
            delayWithDoEvents(1000);

            rePaintScopeInNewLocation(oscFormLocation, oscFormSize);

            // since we jiggered forms around we ought to jigger this one also
            popUpFrmAdpInfoMsgs_RightSize();
        }


        // wait for some time while keeping buttons alive
        private void delayWithDoEvents(int DelayInMilliSeconds)
        {
            TimeSpan ts;
            DateTime dt = DateTime.Now.AddMilliseconds(DelayInMilliSeconds);
            do
            {
                ts = dt.Subtract(DateTime.Now);
                Application.DoEvents(); // keep app responsive
                System.Threading.Thread.Sleep(1); // Reduce CPU usage (keep interval SMALL)
            }
            while (ts.TotalMilliseconds > 0);
        }


        private void zeroCurrentCalFactors()
        {

            m_isup1_range0 = 0;  // m = slope
            b_isup1_range0 = 0;  // b = intercept
            m_isup1_range1 = 0;  // m = slope
            b_isup1_range1 = 0;  // b = intercept

            m_isup2_range0 = 0;  // m = slope
            b_isup2_range0 = 0;  // b = intercept
            m_isup2_range1 = 0;  // m = slope
            b_isup2_range1 = 0;  // b = intercept

            // these may be used to scale the oscilloscope, so be
            // sure they are zeroed also
            max_isup1_range0 = 0.0;
            max_isup1_range1 = 0.0;
            max_isup2_range0 = 0.0;
            max_isup2_range1 = 0.0;

        }


        private int calState = 0;
        private const int CAL_STATE_0_NOT_STARTED = 0;
        private const int CAL_STATE_1 = 1;
        private const int CAL_STATE_2 = 2;
        private const int CAL_STATE_3 = 3;
        private const int CAL_STATE_4 = 4;

        // performs the series of steps needed to compute the calibration factors
        private void btnAutoPowerCal_Click(object sender, EventArgs e)
        {
            AdpNonModalMessageBox frmAdpMsg;
            frmAdpMsg = new AdpNonModalMessageBox();

            DialogResult result;

            if (calState == CAL_STATE_0_NOT_STARTED)
            {
                // prepare for next state: notice in the cal loop mode we fall
                // into the CAL_STATE_1 code below rather than returning
                calState = CAL_STATE_1;
                
                // do not print this info if we are in the cal loop
                if (!bCalLoopRrunning)
                {
                    // change caption of Cal button
                    btnAutoPowerCal.Text = "Continue";

                    //write message to window
                    txtAdpInfoMsgs.WordWrap = true;
                    clearTxtPopUp();

                    powMonSysMsg("\r\n");   // code meaning clear SysMsg box
                    powMonSysMsg("Be sure that all ADK boards, including " +
                    "the ADP100 to be calibrated, are turned off. Make sure " +
                    "the application board is removed from the ADP100 to be " +
                    "calibrated. Turn on the power to the ADP100 board to be " +
                    "calibrated.  Enter the serial number for the ADP100 in " +
                    "the test box labeled �ADP100 Serial #�. Press the �Continue� " +
                    "button to start the automatic calibration.\r\n");

                    // at this point, disconnect if we have an ADP only connection
                    // so we can re-connect below (does nothing if not an ADP-only connection)
                    closeConnectionIfAdpOnly();

                    // TODO: confirm with Bill W.
                    // We do not close the Mezzanine connection: what should happen
                    // is when the operator powers-off, the parent (application) form
                    // (Implant, Base Station, etc.) should detect no polling responses
                    // and disconnect that automatically

                    return; // so operator can press the continue button next
                }
                else
                {
                    // at this point, disconnect if we have an ADP only connection
                    // so we can re-connect below (does nothing if not an ADP-only connection)
                    closeConnectionIfAdpOnly();

                    // .. and fall into next state without waiting for a Continue click

                }

            }   // end CAL_STATE_0

            if (calState == CAL_STATE_1)
            {
                // operator clicked continue
                // It would be nice if there we could check the connection
                // status to see that there is NO connection at this time 
                // (neither the private connection nor the mezzanine connection).
                
                // I was not able to figure a way to do that, though. 
                // And it turns out that:
                
                // If there is NO connection and we try to and successfully establish
                // a private connection, then we are fine.
                
                // If we still have the mezzanine connection (because the operator
                // failed to follow our instructions) and we try to establish a
                // private connection, we will fail to make the private connection
                // and the operator will be notified.
                

                // make the necessary connection (private ADP only connection)

                string err = "";

                int rslt = attemptAdpOnlyConnection(ref err);

                if (rslt != 0)
                {
                    // we cannot cal since we do not have a private connection

                    // reset state 
                    calState = CAL_STATE_0_NOT_STARTED;
                    // restore button caption
                    btnAutoPowerCal.Text = "Start Calibration";

                    // if we were doing cal loops, stop that process by forcing
                    // the countdown to 0
                    if (bCalLoopRrunning) calLoopsLeft = 0;

                    return;
                }


                // do not print this info if we are in the cal loop
                // (which can repeat the whole series of cal events multiple times)
                if (!bCalLoopRrunning)
                {
                    powMonSysMsg("\r\n");   // code meaning clear SysMsg box
                    powMonSysMsg("Automatic calibration is being performed. " +
                    "This will take approximately 37 seconds to complete.\r\n");
                }

                makeSn4Digits();

                resetToUncaledState();

                // do this to make counts scaling visible
                rePaintScopeInSameLocation();

                // We can only cal if we have an ADP-only connection
                // so warn and exit if not
                if (!adpApi.bHaveAdpOnlyConnection)
                {
                    frmAdpMsg.dResult = DialogResult.None;

                    // scope can obscure the message box so hide scope
                    if (instOsc != null) instOsc.Hide();

                    // tell operator this is illegal because there is
                    // a mezzanine attached

                    result =
                    MessageBox.Show(
                    "A mezzanine board is attached to the ADP.\r\r"
                    + "Please exit this GUI, power-off all ADKs, and remove the "
                    + "mezzanine from the ADP you want to calibrate.\r"
                    , "Calibration cannot be performed with a mezzanine attached.",
                    MessageBoxButtons.OK);


                    if (instOsc != null) instOsc.Show();

                    // no matter what the dialog result, we return here
                    // and do not continue the cal
                    return;

                }   // end of not an ADP-only connection


                // we are going to cal now

                // disable many buttons
                disableBeforeRecordOrCal(FROM_CAL_BTN);

                if (!bCalLoopRrunning)
                {
                    // Always Cal at the same 
                    // Acquisition rate,time, 1 or 2 channel defaults.
                    // But, this is inside the if() test above so we can run 
                    // the "N-times" cal loop at different settings if we like
                    initAndCalAcqSettings();
                }

                // set this to allow messages to be supressed as we change 
                // settings in the 8 cal steps.  But be sure we set this 
                // flag *after* the call to initAndCalAcqSettings() above.
                bWithinCalProcess = true;

                // go through the 8 combinations we need to get the current
                // measuring circuits calibrated

                if (cbShowDebug1.Checked)
                    powMonInfoMsg(" ZEROING cal factors so they will be re-computed \r\n");

                resetToUncaledState();

                //0
                // first group is for isup1
                cbVsup1Monitor.Checked = true; cbVsup2Monitor.Checked = false;

                rbVsup1MeasRange0.Checked = true;
                //rbVsup1Cal0.Checked = true;
                cbVsup1Load.Checked = false;    // load off = 0

                // I noticed that if I did this whole sequence, and then
                // did it again, I could see the value in the first acquisition
                // clearly drooping towards the end of the trace.
                // Droop in the counts means an INCREASE in the current: I
                // am speculating that when I switch a load resistor OFF
                // after it has been ON for a while, with NO JUMPERS, it
                // takes a while for the voltage on the cap to drain
                // ?? but is this consistent with an INCREASE in the current??
                // need to think about this more but for now I will test
                // a delay here
                // Delta in counts for the first acquisition was 7 from
                // coldstart but was 17 for the first acquisition when
                // repeating
                //
                delayWithDoEvents(1000);
                btnRecord_Click(sender, e); waitForRecordToFinish();
                // want to be able to see scope acquisition data
                if (instOsc != null) instOsc.Update(); delayWithDoEvents(3000);


                //1
                rbVsup1MeasRange0.Checked = true;
                //rbVsup1Cal1.Checked = true;
                cbVsup1Load.Checked = true;     // load ON     = 1

                delayWithDoEvents(1000);
                btnRecord_Click(sender, e); waitForRecordToFinish();
                // want to be able to see scope acquisition data
                if (instOsc != null) instOsc.Update(); delayWithDoEvents(3000);


                //2
                // now Range1
                rbVsup1MeasRange1.Checked = true;
                //rbVsup1Cal0.Checked = true;
                cbVsup1Load.Checked = false;    // load off = 0

                delayWithDoEvents(1000);
                btnRecord_Click(sender, e); waitForRecordToFinish();
                // want to be able to see scope acquisition data
                if (instOsc != null) instOsc.Update(); delayWithDoEvents(3000);



                //3
                rbVsup1MeasRange1.Checked = true;
                //rbVsup1Cal1.Checked = true;
                cbVsup1Load.Checked = true;     // load ON     = 1

                delayWithDoEvents(1000);
                btnRecord_Click(sender, e); waitForRecordToFinish();
                // want to be able to see scope acquisition data
                if (instOsc != null) instOsc.Update(); delayWithDoEvents(3000);



                //4
                // next group is for isup2
                cbVsup1Monitor.Checked = false; cbVsup2Monitor.Checked = true;
                rbVsup2MeasRange0.Checked = true;
                //rbVsup2Cal0.Checked = true;
                cbVsup2Load.Checked = false;    // load off = 0

                delayWithDoEvents(1000);
                btnRecord_Click(sender, e); waitForRecordToFinish();
                // want to be able to see scope acquisition data
                if (instOsc != null) instOsc.Update(); delayWithDoEvents(3000);


                //5
                rbVsup2MeasRange0.Checked = true;
                //rbVsup2Cal1.Checked = true;
                cbVsup2Load.Checked = true;     // load ON     = 1

                delayWithDoEvents(1000);
                btnRecord_Click(sender, e); waitForRecordToFinish();
                // want to be able to see scope acquisition data
                if (instOsc != null) instOsc.Update(); delayWithDoEvents(3000);


                //6
                // now Range1
                rbVsup2MeasRange1.Checked = true;
                //rbVsup2Cal0.Checked = true;
                cbVsup2Load.Checked = false;    // load off = 0

                delayWithDoEvents(1000);
                btnRecord_Click(sender, e); waitForRecordToFinish();
                // want to be able to see scope acquisition data
                if (instOsc != null) instOsc.Update(); delayWithDoEvents(3000);


                //7
                rbVsup2MeasRange1.Checked = true;
                //rbVsup2Cal1.Checked = true;
                cbVsup2Load.Checked = true;     // load ON     = 1

                delayWithDoEvents(1000);
                btnRecord_Click(sender, e); waitForRecordToFinish();
                // want to be able to see scope acquisition data
                if (instOsc != null) instOsc.Update(); delayWithDoEvents(3000);


                // turn cal resistor off at the end, but Chan 2 first
                // so at the end of cal we will always have Chan 1 selected
                // (remember btnChanXXX_Click() will change chan acquired)


                rbVsup2MeasRange0.Checked = true;
                //rbVsup2Cal0.Checked = true;
                cbVsup2Load.Checked = false;    // load off = 0

                rbVsup1MeasRange0.Checked = true;
                //rbVsup1Cal0.Checked = true;
                cbVsup1Load.Checked = false;    // load off = 0

                // finally do the calculations
                btnCalcCurrentFactors_Click(sender, e);


                // prompt operator to shut down only if we are not in the 
                // N cals loop
                if (!bCalLoopRrunning)
                {
                    // this does the file write
                    storeCurrentFactorsToFileBySn();

                    powMonSysMsg("\r\n");   // code meaning clear SysMsg box
                    powMonSysMsg("Calibration successfully completed!  A calibration " +
                               "file with the name �ADP100CalibrationForSN" + cmbSn.Text + ".txt� has " +
                               "been created and stored in a local directory.  Turn the " +
                               "power to the ADP100 off and re-assemble the application " +
                               "board to the ADP100. Once assembled, turn the ADP100 board back on. " +
                               "Each time the �ADP100 Power Monitor� " +
                               "GUI is launched from the application GUI, it will automatically " +
                               "load the calibration coefficients for that ADP100. Now you must " +
                               "close the ADP100 Power Monitor GUI and the " + titleOfMezz + " GUI and re-launch " +
                               "the " + titleOfMezz + " Application Implant and ADP100 Power Monitor GUI.\r\n");


                    //// scope can obscure the message box so hide scope
                    //if (instOsc != null) instOsc.Hide();

                    //// stop polling in parent form:
                    //// operator must do this step manually so we do not have
                    //// to create a shared variable to get this effect
                    //result =
                    //MessageBox.Show(" Please exit this GUI, power-off\r"
                    //+ "all ADKs, attach a mezzanine, then re-start.\r"
                    //, "Calibration completed.",
                    //              MessageBoxButtons.OK);

                    //if (instOsc != null) instOsc.Show();


                    // bring back buttons that were disabled
                    enableAfterRecordOrCal(FROM_CAL_BTN);


                    // reset state 
                    calState = CAL_STATE_0_NOT_STARTED;
                    // restore button caption
                    btnAutoPowerCal.Text = "Start Calibration";

                    return;
                }
                else   // we are cal looping
                {
                    // to be able to do the next cal loop we need to be disconnected
                    closeConnectionIfAdpOnly();
                    // reset state 
                    calState = CAL_STATE_0_NOT_STARTED;
                    // restore button caption
                    btnAutoPowerCal.Text = "Start Calibration";

                    return;

                }

            }   // end if (calState == CAL_STATE_1)



            //if (!bCal_Loop_running)
            //{

            //    if (instOsc != null) instOsc.Show();

            //}   // end if not cal loop running

        }


        // Compute the actual highest current that can be
        // measured by the ADC12 
        // We need to do this every time the cal factors change
        // since these will be used to scale the Oscilloscope
        private void calcMaxIsupsRanges()
        {


            // as always, we assume that if the slope (m_*) value
            // is equal to 0 then cal factors are not in effect

            if (m_isup1_range0 != 0.0)
            max_isup1_range0 = m_isup1_range0 * (0.0 + b_isup1_range0);

            if (m_isup1_range1 != 0.0)
            max_isup1_range1 = m_isup1_range1 * (0.0 + b_isup1_range1);

            if (m_isup2_range0 != 0.0)
            max_isup2_range0 = m_isup2_range0 * (0.0 + b_isup2_range0);

            if (m_isup2_range1 != 0.0)
            max_isup2_range1 = m_isup2_range1 * (0.0 + b_isup2_range1);


        }



        private void btnCalcCurrentFactors_Click(object sender, EventArgs e)
        {
            // Assuming 3.3 v, the extra resistor of 1000 ohms
            // should draw 3.3/1000 = 3.3 ma more when switched in

            // 3.3 ma through 8.33 ohms -> 0.027489 volts drop, *10 gain=0.274 v
            // 3.3 ma through 49.9 ohms -> 0.164670 volts drop, *10 gain=1.6467 v


            // vvv-----binary index into the table of learned averages
            //


            //  isup1
            //  |range0 (R=49.9)
            //  ||cal load (on/off)
            //  |||
            //  000 counts without cal load
            //  001 counts with cal load

            //  isup1
            //  |range1 (R=8.33)
            //  ||cal load (on/off)
            //  |||
            //  010 counts without cal load
            //  011 counts with cal load

            //  isup2
            //  |range0 (R=49.9)
            //  ||cal load (on/off)
            //  |||
            //  100 counts without cal load
            //  101 counts with cal load

            //  isup2
            //  |range1 (R=8.33)
            //  ||cal load (on/off)
            //  |||
            //  110 counts without cal load
            //  111 counts with cal load

            if (isup1Avgs[0] != 0.0)
            {
                if (cbShowDebug1.Checked)
                    powMonInfoMsg(" Have isup1:Range0:counts without cal load \r\n");

                if (isup1Avgs[1] != 0.0)
                {
                    if (cbShowDebug1.Checked)
                        powMonInfoMsg(" Have isup1:Range0:counts WITH    cal load \r\n");
                    if (cbShowDebug1.Checked)
                        powMonInfoMsg("     Performing Cal Factors calculation for isup1_range0 \r\n");

                    // The big assumption is that this CAL will be done while
                    // the mezz is drawing 0 current


                    // Let us call Z the unknown current being drawn before
                    // the cal load resistor is switched in.  We have the
                    // measured ADC counts with and without the load (example
                    // numbers below were from isup1_range0:

                    // vvv-----binary index into the table of learned averages
                    //
                    // isup1
                    // 000   Z ma          = 3763 counts
                    // 001   Z ma + 3.3 ma = 1880 counts
                    //      ------------------    (subtract equations)
                    //       0    - 3.3 ma = 1883 counts  -> -570 counts/ma
                    //                   1 = -570 (counts/ma)
                    //                   1 = -0.001754 (ma/counts)


                    // I will assume cal was done in SLEEPING mode, so Z=0
                    // (this assumption is that NO detectable current is
                    // drawn in sleep mode)



                    // for now ASSUME 3.3v supply and so 3.3 ma extra i with 1000 ohms
                    // index 0 is counts of isup1:Range0 no cal load
                    // index 1 is counts of isup1:Range0 *with* cal load
                    m_isup1_range0 = isup1Avgs[0] - isup1Avgs[1];
                    m_isup1_range0 = m_isup1_range0 / (-3.3);
                    if (cbShowDebug2.Checked)
                        powMonInfoMsg(" m_isup1_range0 = " + m_isup1_range0.ToString(".0000 ") + "; \r\n");
                    // we want an m we can multiply by so invert this
                    m_isup1_range0 = 1.0 / m_isup1_range0;

                    // here is where we use the assumption the current without
                    // the cal load resistor is 0.0
                    b_isup1_range0 = isup1Avgs[0] * -1.0;

                    if (cbShowDebug2.Checked)
                        powMonInfoMsg(" b_isup1_range0 = " + b_isup1_range0.ToString(".0000 ") + "; \r\n");

                    if (cbShowDebug1.Checked)
                        powMonInfoMsg(" From now on Oscilloscope will display the scaled value for this channel \r\n");

                }   //if (isup1_beam0_avgs[1] != 0.0)

            }       //if (isup1_beam0_avgs[0] != 0.0)



            if (isup1Avgs[2] != 0.0)
            {
                if (cbShowDebug1.Checked)
                    powMonInfoMsg(" Have isup1:Range1:counts without cal load \r\n");

                if (isup1Avgs[3] != 0.0)
                {
                    if (cbShowDebug1.Checked)
                        powMonInfoMsg(" Have isup1:Range1:counts WITH    cal load \r\n");
                    if (cbShowDebug1.Checked)
                        powMonInfoMsg("     Performing Cal Factors calculation for isup1_range1 \r\n");


                    // for now ASSUME 3.3v supply and so 3.3 ma extra i with 1000 ohms
                    // index 0 is counts of isup1:Range1 no cal load
                    // index 1 is counts of isup1:Range1 *with* cal load
                    m_isup1_range1 = isup1Avgs[2] - isup1Avgs[3];
                    m_isup1_range1 = m_isup1_range1 / (-3.3);
                    if (cbShowDebug2.Checked)
                        powMonInfoMsg(" m_isup1_range1 = " + m_isup1_range1.ToString(".0000 ") + "; \r\n");
                    // we want an m we can multiply by so invert this
                    m_isup1_range1 = 1.0 / m_isup1_range1;

                    // here is where we use the assumption the current without
                    // the cal load resistor is 0.0
                    b_isup1_range1 = isup1Avgs[2] * -1.0;

                    if (cbShowDebug2.Checked)
                        powMonInfoMsg(" b_isup1_range1 = " + b_isup1_range1.ToString(".0000 ") + "; \r\n");

                    if (cbShowDebug1.Checked)
                        powMonInfoMsg(" From now on Oscilloscope will display the scaled value for this channel \r\n");

                }   //if (isup1_beam0_avgs[1] != 0.0)

            }       //if (isup1_beam0_avgs[0] != 0.0)

            //---------------------------------------------
            if (isup2Avgs[0] != 0.0)
            {
                if (cbShowDebug1.Checked)
                    powMonInfoMsg(" Have isup2:Range0:counts without cal load \r\n");

                if (isup2Avgs[1] != 0.0)
                {
                    if (cbShowDebug1.Checked)
                        powMonInfoMsg(" Have isup2:Range0:counts WITH    cal load \r\n");
                    if (cbShowDebug1.Checked)
                        powMonInfoMsg("     Performing Cal Factors calculation for isup2_range0 \r\n");

                    // The big assumption is that this CAL will be done while
                    // the mezz is drawing 0 current


                    // Let us call Z the unknown current being drawn before
                    // the cal load resistor is switched in.  We have the
                    // measured ADC counts with and without the load (example
                    // numbers below were from isup2_range0:

                    // vvv-----binary index into the table of learned averages
                    //
                    // isup2
                    // 000   Z ma          = 3763 counts
                    // 001   Z ma + 3.3 ma = 1880 counts
                    //      ------------------    (subtract equations)
                    //       0    - 3.3 ma = 1883 counts  -> -570 counts/ma
                    //                   1 = -570 (counts/ma)
                    //                   1 = -0.001754 (ma/counts)


                    // I will assume cal was done in SLEEPING mode, so Z=0
                    // (this assumption is that NO detectable current is
                    // drawn in sleep mode)



                    // for now ASSUME 3.3v supply and so 3.3 ma extra i with 1000 ohms
                    // index 0 is counts of isup2:Range0 no cal load
                    // index 1 is counts of isup2:Range0 *with* cal load
                    m_isup2_range0 = isup2Avgs[0] - isup2Avgs[1];
                    m_isup2_range0 = m_isup2_range0 / (-3.3);
                    if (cbShowDebug2.Checked)
                        powMonInfoMsg(" m_isup2_range0 = " + m_isup2_range0.ToString(".0000 ") + "; \r\n");
                    // we want an m we can multiply by so invert this
                    m_isup2_range0 = 1.0 / m_isup2_range0;

                    // here is where we use the assumption the current without
                    // the cal load resistor is 0.0
                    b_isup2_range0 = isup2Avgs[0] * -1.0;

                    if (cbShowDebug2.Checked)
                        powMonInfoMsg(" b_isup2_range0 = " + b_isup2_range0.ToString(".0000 ") + "; \r\n");

                    if (cbShowDebug1.Checked)
                        powMonInfoMsg(" From now on Oscilloscope will display the scaled value for this channel \r\n");

                }   //if (isup2_beam0_avgs[1] != 0.0)

            }       //if (isup2_beam0_avgs[0] != 0.0)



            if (isup2Avgs[2] != 0.0)
            {
                if (cbShowDebug1.Checked)
                    powMonInfoMsg(" Have isup2:Range1:counts without cal load \r\n");

                if (isup2Avgs[3] != 0.0)
                {
                    if (cbShowDebug1.Checked)
                        powMonInfoMsg(" Have isup2:Range1:counts WITH    cal load \r\n");
                    if (cbShowDebug1.Checked)
                        powMonInfoMsg("     Performing Cal Factors calculation for isup2_range1 \r\n");


                    // for now ASSUME 3.3v supply and so 3.3 ma extra i with 1000 ohms
                    // index 2 is counts of isup2:Range1 no cal load
                    // index 3 is counts of isup2:Range1 *with* cal load
                    m_isup2_range1 = isup2Avgs[2] - isup2Avgs[3];
                    m_isup2_range1 = m_isup2_range1 / (-3.3);
                    if (cbShowDebug2.Checked)
                        powMonInfoMsg(" m_isup2_range1 = " + m_isup2_range1.ToString(".0000 ") + "; \r\n");
                    // we want an m we can multiply by so invert this
                    m_isup2_range1 = 1.0 / m_isup2_range1;

                    // here is where we use the assumption the current without
                    // the cal load resistor is 0.0
                    b_isup2_range1 = isup2Avgs[2] * -1.0;

                    if (cbShowDebug2.Checked)
                        powMonInfoMsg(" b_isup2_range1 = " + b_isup2_range1.ToString(".0000 ") + "; \r\n");

                    if (cbShowDebug1.Checked)
                        powMonInfoMsg(" From now on Oscilloscope will display the scaled value for this channel \r\n");

                }   //if (isup2_beam0_avgs[1] != 0.0)

            }       //if (isup2_beam0_avgs[0] != 0.0)


            // For stability test print cal values on one line
            // so I can import them into excel

            powMonInfoMsg(
                (1 / m_isup1_range0).ToString(".0000 ") +
                b_isup1_range0.ToString(".0000 ") +
                (1 / m_isup1_range1).ToString(".0000 ") +
                b_isup1_range1.ToString(".0000 ") +
                (1 / m_isup2_range0).ToString(".0000 ") +
                b_isup2_range0.ToString(".0000 ") +
                (1 / m_isup2_range1).ToString(".0000 ") +
                b_isup2_range1.ToString(".0000 ") +
                "; \r\n");


            // Compute the actual highest current that can be
            // measured by the ADC12 
            // We need to do this every time the cal factors change
            // since these will be used to scale the Oscilloscope
            calcMaxIsupsRanges();

        }


        private void storeCurrentFactorsToFileBySn()
        {


            //try
            //{
            //    fileInfo = File.ReadAllText(filename);
            //}
            //catch (FileNotFoundException)
            //{
            //    MessageBox.Show("The register description file (" + filename + ") was not found.");
            //    return;
            //}

            //try
            //{
            //    string s = null;
            //    ProcessString(s);
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine("{0} Exception caught.", e);
            //}

            string snDigits;

            // get the SN currently in the entry box
            snDigits = cmbSn.Text;

            // make it 4 digits
            while (snDigits.Length < 4) snDigits = "0" + snDigits;


            string pathPlusFileName = pathOnlyToExeDir + "\\AdpPowMonCal" + snDigits + ".txt";



            // do not prompt for file store if we are in the 
            // Cal NNNN times loop
            if (!bCalLoopRrunning)
            {
                // the oscilloscope was obscuring the message box
                // until I added this
                this.TopMost = true;
                this.TopMost = false;

                {

                    // To use Visual Basic InputBox:
                    // set a reference to the "Microsoft.VisualBasic" 
                    // dll (from the .NET tab of the "Add Reference" dialog - via right-click on project references).

                    // Then, just call it directly:
                    // e.g.,
                    // Microsoft.VisualBasic.Interaction.InputBox("test", "test", "", 100, 100);

                    // -1 for x and y coordinates will ask for centering of the box
                    //snDigits = Microsoft.VisualBasic.Interaction.InputBox("Remove Jumpers JP1 and JP2 from ADP board now\n\nPlease enter ADP board SN (digits only)", "Field Calibration", "", -1, -1);




                    //// scope can obscure the message box so hide scope
                    //if (instOsc != null) instOsc.Hide();

                    //DialogResult result = 
                    //MessageBox.Show("Calibration completed.  Store to file:\r "
                    //            + pathPlusFileName + " ?" , "Complete",
                    //        MessageBoxButtons.OKCancel);



                    //if (instOsc != null) instOsc.Show();



                    //if (result != DialogResult.OK)
                    //{
                    //    if (cbShowDebug1.Checked)
                    //        powMonInfoMsg("CANCELing without storing cal factors to disk\r\n");
                    //    return;
                    //}

                }
//                this.TopMost = false;


            }   // end if (!bCal_Loop_running)

            if (cbShowDebug2.Checked)
            powMonInfoMsg("snDigits == " + snDigits + "\r\n");



            StreamWriter sw;
            try
            {

                sw = new StreamWriter(pathPlusFileName);
                
                // record the date and time
                sw.WriteLine(DateTime.Now);


                // record the Cal conditions
                string calConditions = cmbDMASampleRate.Text;
                calConditions += " " + cmbTimeToRecord.Text;
                calConditions += " " + cmbUnitsOfTime.Text;
                sw.WriteLine(calConditions);

                // record the cal values
                sw.WriteLine((1/m_isup1_range0).ToString(".0000 "));
                sw.WriteLine(b_isup1_range0.ToString(".0000 "));
                sw.WriteLine((1/m_isup1_range1).ToString(".0000 "));
                sw.WriteLine(b_isup1_range1.ToString(".0000 "));
                sw.WriteLine((1/m_isup2_range0).ToString(".0000 "));
                sw.WriteLine(b_isup2_range0.ToString(".0000 "));
                sw.WriteLine((1/m_isup2_range1).ToString(".0000 "));
                sw.WriteLine(b_isup2_range1.ToString(".0000 "));
                sw.Close();

           }

            catch (Exception ex)
            {
                //Console.WriteLine(
                powMonSysMsg("Exception caught." + ex.ToString());
                return;
            }

            // since the writing of the new calibration data to a file
            // worked, we will rewrite the SN file too
            saveSnToFile();
            // and turn the SN field green
            cmbSn.BackColor = Color.LimeGreen;

        }

 
 
        private void btnShowCalDiffs_Click(object sender, EventArgs e)
        {

            if (m_isup1_range0 == 0.0)
            {
                powMonInfoMsg("!! Avoiding divide by 0 since cal factors have not been set yet!\r\n");
                return;
            }


            // In code above this slope is inverted after printing.
            // This was so that printed form is not a fraction.
            // So, I have to maintain that convention here
            m_isup1_range0 = 1.0 / m_isup1_range0;
            m_isup1_range1 = 1.0 / m_isup1_range1;
            m_isup2_range0 = 1.0 / m_isup2_range0;
            m_isup2_range1 = 1.0 / m_isup2_range1;


            // show percent difference from factory values
            // see definitions for fact_* above
            powMonInfoMsg("diff m_isup1_range0 = " + String.Format("{0,15:0.000000}",(100.0 * ((m_isup1_range0 - fact_m_isup1_range0) / fact_m_isup1_range0))) + "% \r\n");
            powMonInfoMsg("diff b_isup1_range0 = " + String.Format("{0,15:0.000000}",(100.0 * ((b_isup1_range0 - fact_b_isup1_range0) / fact_b_isup1_range0))) + "% \r\n");
            powMonInfoMsg("diff m_isup1_range1 = " + String.Format("{0,15:0.000000}",(100.0 * ((m_isup1_range1 - fact_m_isup1_range1) / fact_m_isup1_range1))) + "% \r\n");
            powMonInfoMsg("diff b_isup1_range1 = " + String.Format("{0,15:0.000000}",(100.0 * ((b_isup1_range1 - fact_b_isup1_range1) / fact_b_isup1_range1))) + "% \r\n");

            powMonInfoMsg("diff m_isup2_range0 = " + String.Format("{0,15:0.000000}",(100.0 * ((m_isup2_range0 - fact_m_isup2_range0) / fact_m_isup2_range0))) + "% \r\n");
            powMonInfoMsg("diff b_isup2_range0 = " + String.Format("{0,15:0.000000}",(100.0 * ((b_isup2_range0 - fact_b_isup2_range0) / fact_b_isup2_range0))) + "% \r\n");
            powMonInfoMsg("diff m_isup2_range1 = " + String.Format("{0,15:0.000000}",(100.0 * ((m_isup2_range1 - fact_m_isup2_range1) / fact_m_isup2_range1))) + "% \r\n");
            powMonInfoMsg("diff b_isup2_range1 = " + String.Format("{0,15:0.000000}",(100.0 * ((b_isup2_range1 - fact_b_isup2_range1) / fact_b_isup2_range1))) + "% \r\n");


            // invert slope only
            m_isup1_range0 = 1.0 / m_isup1_range0;
            m_isup1_range1 = 1.0 / m_isup1_range1;
            m_isup2_range0 = 1.0 / m_isup2_range0;
            m_isup2_range1 = 1.0 / m_isup2_range1;



        }

        private void makeString4Digits(ref string passedString)
        {
            //Make sure the number is 4 digits, add leading 0's 
            while (passedString.Length < 4)
            {
                passedString = "0" + passedString;
            }

        }

        private void makeSn4Digits()
        {
            string tempStr = this.cmbSn.Text;

            makeString4Digits(ref tempStr);

            this.cmbSn.Text = tempStr;

        }


        private void btnLoadCalSettings_Click(object sender, EventArgs e)
        {
            // make sure we always use the .exe directory for this
            // even if the current dir has changed

            makeSn4Digits();

            string calFactFileNameWithSn = "AdpPowMonCal" + cmbSn.Text + ".txt";

            try
            {
                // Create an instance of StreamReader to read from a file.
                // The using statement also closes the StreamReader.
                using (StreamReader sr = new StreamReader(pathOnlyToExeDir + @"\" + calFactFileNameWithSn))
                {
                    String line;


                    powMonInfoMsg("Recalling Cal factors from: " + calFactFileNameWithSn + "\r\n");
                    line = sr.ReadLine();
                    powMonInfoMsg("Cal factors date: " + line + "\r\n");
                    line = sr.ReadLine();
                    powMonInfoMsg("Cal sample rate and time: " + line + "\r\n");

                    line = sr.ReadLine();
                    m_isup1_range0 = Convert.ToDouble(line);
                    line = sr.ReadLine();
                    b_isup1_range0 = Convert.ToDouble(line);
                    line = sr.ReadLine();
                    m_isup1_range1 = Convert.ToDouble(line);
                    line = sr.ReadLine();
                    b_isup1_range1 = Convert.ToDouble(line);

                    line = sr.ReadLine();
                    m_isup2_range0 = Convert.ToDouble(line);
                    line = sr.ReadLine();
                    b_isup2_range0 = Convert.ToDouble(line);
                    line = sr.ReadLine();
                    m_isup2_range1 = Convert.ToDouble(line);
                    line = sr.ReadLine();
                    b_isup2_range1 = Convert.ToDouble(line);


                }
                // we successfully loaded calibration data so update the SN file
                saveSnToFile();
                // and turn the SN field green
                cmbSn.BackColor = Color.LimeGreen;

            }

            catch (Exception)
            {

                resetToUncaledState();
                if (instOsc != null) rePaintScopeInSameLocation();

                powMonSysMsg("\r\n");   // code meaning clear SysMsg box
                powMonSysMsg("Warning: A calibration file was not found for this board�s serial number. Check to ensure the correct serial number is entered. Either create a new calibration file by pressing �Start Calibration� and follow the on-screen instructions, or proceed without calibrating.  Be aware that the oscilloscope vertical axis will be displayed in counts from 0 � 4095 representing the actual ADC output and the polarity of the signal is inverted.");

                //DialogResult result =
                //    MessageBox.Show("No calibration file was found for SN " + cmbSn.Text + "\r\r"
                //    + "Values will now be displayed in raw ADC12 counts (0-4095).\r"

                //  , "Calibration values are NOT in effect.", MessageBoxButtons.OK);


                // no user options other than OK at this time: operator
                // must push the Calibrate button himself
                //

                // re-paint the scope to show the new scale for values
                if (instOsc != null)
                {
                    btnOscilloscope_Click(sender, e);
                }

                return;
            
            }   // end exception caught for no cal file exists

            // So that the printed and saved form are NOT a small fraction
            // we adopted the convention that they should be stored as
            // the inverse. Now change them back to a fraction here
            m_isup1_range0 = 1.0 / m_isup1_range0;
            m_isup1_range1 = 1.0 / m_isup1_range1;
            m_isup2_range0 = 1.0 / m_isup2_range0;
            m_isup2_range1 = 1.0 / m_isup2_range1;

            // Compute the actual highest current that can be
            // measured by the ADC12 
            // We need to do this every time the cal factors change
            // since these will be used to scale the Oscilloscope
            calcMaxIsupsRanges();


            // report the actual range with the cal factors just loaded 
            // from the file
            powMonInfoMsg("Isup1 Range 0 Full Scale = " +
                    String.Format("{0,5:0.00}", max_isup1_range0) +
                          " ma.\r\n"
                          );

            // report the actual range with these cal factors
            powMonInfoMsg("Isup1 Range 1 Full Scale = " +
                    String.Format("{0,5:0.00}", max_isup1_range1) +
                          " ma.\r\n"
                          );

            // compute the actual highest current that can be
            // measured by the ADC12 
            calcMaxIsupsRanges();
            

            // report the actual range with these cal factors
            powMonInfoMsg("Isup2 Range 0 Full Scale = " +
                    String.Format("{0,5:0.00}", max_isup2_range0) +
                          " ma.\r\n"
                          );

            // report the actual range with these cal factors
            powMonInfoMsg("Isup2 Range 1 Full Scale = " +
                    String.Format("{0,5:0.00}", max_isup2_range1) +
                          " ma.\r\n"
                          );

            // re-paint the scope to show the new scale for values
            if (instOsc != null)
            {
                btnOscilloscope_Click(sender, e);
            }

        }


        // this can be useful to abort a cal loop early by setting it to 0
        int calLoopsLeft = 0;


        private void btnRunRepeatedCal_Click(object sender, EventArgs e)
        {

            calLoopsLeft = Convert.ToInt32(txtTimesToRepeatCal.Text);
           
            powMonInfoMsg("Running repeated cal loop " + calLoopsLeft.ToString() + " times\r\n");

            bCalLoopRrunning = true;

            while (calLoopsLeft > 0)
            {
                calLoopsLeft--; 
                btnAutoPowerCal_Click(sender, e);

                if (cbShowDebug2.Checked)
                {
                    powMonInfoMsg("calLoopsLeft " + calLoopsLeft.ToString() + "\r\n");
                }
            }

            // restore tabs (and buttons on the tabs) that were turned off
            tabControlCustomerEtc.Enabled = true;
            // enable those controls we had disabled
            enableAfterRecordOrCal(FROM_RECORD_BTN);

            bCalLoopRrunning = false;
            powMonInfoMsg("Finished repeated cal loop.\r\n");
        }

        private void cmbDMASampleRate_SelectedIndexChanged(object sender, EventArgs e)
        {

            calcAcqParmsFromSettings( sender,  e);
        }


        // member variable so it can be shared
        double oneDmaBlockAcqTimeSeconds;

        // this one is read back from the file header
        Int32 fileDmaBlockAcqTimeUs = 0;


        private void calcAcqParmsFromSettings(object sender, EventArgs e)
        {
            // do not make these computations until all the items that
            // contribute to this calculation are valid.  The order in
            // which they become valid is defined in 
            // initAndCalAcqSettings(), so we know the following item
            // is the last of the participating items  to become valid.
            // So, after it is initialized we are safely able
            // to do this calculation

            // cannot do this calc until at least one of these is checked
            if ((!cbVsup1Monitor.Checked) && (!cbVsup2Monitor.Checked))
                return;

            // we will skip this calculation and reporting if we are in the 
            // cal loop mode since there is no need to do this every time
            if (bCalLoopRrunning) return;
            // same reason here
            // note we are sure this has been done at least once since
            // we set this flag *after* the cal
            if (bWithinCalProcess) return;            

            // Compute and report useful info for operator

            // note this so scope can be rescaled
            bScopeShouldBeReScaled = true;

            double requestedSamplesPerSecond;

            try
            {
                // it is important to convert the .Text member 
                // (not SelectedItem) so items can be ADDED
                requestedSamplesPerSecond = Convert.ToDouble(cmbDMASampleRate.Text);
            }

            catch (Exception ex)
            {
                powMonInfoMsg(
                "Converting cmbDMASampleRate.Text to a double failed: " + ex.ToString() + "\r\n"
                + " Restored to default \r\n" );

                MessageBox.Show("Converting cmbDMASampleRate.Text to a double failed: " + ex.ToString() + "\r\n"
                + " Restored to default. \r\n", "Combo Box Entry Error",
                            MessageBoxButtons.OK);

                // this may cause an event to fire
                initCmbDMASampleRate();
                // this should never throw an exception since we restored the default
                requestedSamplesPerSecond = Convert.ToDouble(cmbDMASampleRate.Text);

            }


            // Below we go to some length to report that the actual sample rate
            // may differ slightly from the requested rate due to integer
            // division of the clock AND integer representation of microseconds
            // in the argument passed to the embedded
            powMonInfoMsg(
                    String.Format("{0,8}", requestedSamplesPerSecond) +
                          " channel samples/second requested (current setting)\r\n"
                          );

            targetUsecBetweenSamples = (1 / Convert.ToDouble(cmbDMASampleRate.Text)) * 1000000.0;

            powMonInfoMsg(
                    String.Format("{0,11:0.00}", targetUsecBetweenSamples) +
                          " target usecs between samples (from above)\r\n"
                          );

            // convert to integer usec 
            usecBetweenSamples = Convert.ToUInt16(targetUsecBetweenSamples);

            powMonInfoMsg(
                    String.Format("{0,11:0.00}", usecBetweenSamples) +
                          " integer usecs between samples (truncation of above)\r\n"
                          );

            // In the embedded, the conversion of integer usecs to a divisor
            // is only a multiply by 8, and so there can be no loss due to that!
            // Even in the case we have to double the rate (so we halve the divisor)
            // there is still no loss because 8 is  4*2
            ushort divisor = (ushort)(usecBetweenSamples * 8);

            // There is an opportunity for confusion in these calculations
            // since there are 2 modes: 
            //  1) One channel mode: channel sampled every ADC12 trigger
            //
            //  2) Two channels mode: still one channel sampled each ADC12
            //     trigger, but we alternate between 2 channels.  So, we
            //     need to generate the triggers at twice the one channel
            //     rate so *both* channels get sampled at the chosen
            //     sample rate

            // For this reason, we write the descriptions below in terms
            // of ADC12 triggers since the timerB device on the micro needs
            // to be programmed to generate the correct number of ADC12
            // triggers-per-second.

            if (dmaBuffSingleOrSplit() == DMA_BUF_IS_SINGLE_ADC12_CHAN)
            {

                // The Timer B block of the MSP430 generates the trigger for the
                // ADC12 to sample (sequencing is not used)
                powMonInfoMsg("ONE channel only enabled to acquire (current setting)\r\n");
                powMonInfoMsg(" --> ADC12 triggers/second = requested samples/second\r\n");

                // do not cut divisor in half 

                powMonInfoMsg(
                        String.Format("{0,8}", divisor) +
                              " divisor assuming one channel acquisition\r\n"
                              );
            }
            else // must be DMA_BUF_IS_SPLIT_TWO_ADC12_CHANS
            {
                // divisor will be halved in both channels mode

                // We need to double the ADC12 trigger rate when we are using
                // the split mode to get the requested sample rate.  This is
                // because we are using a sequence of 2 ADC12 conversions and
                // we are NOT using Multiple Sample and Convert mode
                // so each ADC12 trigger causes only ONE conversion
                // of the sequence of length 2.  
                // The reason for this choice is to space the sampling for the
                // alternating channels as far apart as possible to minimize any
                // interaction due to the analog muxing of the inputs.

                powMonInfoMsg("BOTH channels acquired simultaneously (current setting)\r\n");
                powMonInfoMsg(" --> ADC12 triggers/second = 2 * (requested samples/second)\r\n");

                divisor = (ushort)(divisor/2);  // cut in half
                powMonInfoMsg(
                        String.Format("{0,8}", divisor) +
                              " divisor assuming TWO channel acquisition\r\n"
                              );
            }


            // assume we know the rate of the crystal in the embedded
            double aclocksPerSecond = ACLK_RATE;

            powMonInfoMsg(
                    String.Format("{0,8}", aclocksPerSecond) +
                          " ACLKs/second assumed (embedded crystal)\r\n"
                          );


            // Compute results based on this ACLK rate and the divisor


            // if we think of the divider as a down counter then
            // divisor units are "(aclocks/underflow)"
            // so aclocksPerSecond/divisor = (aclocks/Second)/(aclocks/underflow)
            // = (underflows/second)
            // and of course each underflow will trigger the ADC12

            // now we can compute the actual trigger rate we will get
            double adc_triggers_per_second =(aclocksPerSecond / divisor );

            powMonInfoMsg(
                    String.Format("{0,11:0.00}", adc_triggers_per_second) +
                          " actual ADC12 triggers/second (after us truncation to integer)\r\n"
                          );


            // compute actual sample rate
            double actualSampleRate;

            if (dmaBuffSingleOrSplit() == DMA_BUF_IS_SINGLE_ADC12_CHAN)
            {
                actualSampleRate = adc_triggers_per_second;
                powMonInfoMsg("ONE channel so trigger rate was not doubled\r\n");
            }
            else
            {
                actualSampleRate = adc_triggers_per_second/2;
                powMonInfoMsg("BOTH channels so sample rate is half of trigger rate\r\n");
            }

            powMonInfoMsg(
                    String.Format("{0,11:0.00}", actualSampleRate) +
                          " actual ADC12 samples/second\r\n"
                          );

            double totalSamplesPerDmaBlock;

            double sameChannelSamplesPerDmaBlock;

            // here time interval is 1/(requested sample rate)
            double nrChannelsSampledEachTimeInterval;
            string channelsSampledInSameDmaBlock;


            if (dmaBuffSingleOrSplit() == DMA_BUF_IS_SINGLE_ADC12_CHAN)
            {
                nrChannelsSampledEachTimeInterval = 1;
                channelsSampledInSameDmaBlock = "";
            } else
            {
                // This mode puts samples from 2 different ADC12 channels
                // in the same DMA block
                nrChannelsSampledEachTimeInterval = 2;
                channelsSampledInSameDmaBlock = "(2 channels in one block)";
            }

            totalSamplesPerDmaBlock = AdpApi.NR_SAMPLES_IN_CURRENT_MSG;

            sameChannelSamplesPerDmaBlock = AdpApi.NR_SAMPLES_IN_CURRENT_MSG / nrChannelsSampledEachTimeInterval;


            // this is defined as an object member variable so it can be
            // shared with other methods
            oneDmaBlockAcqTimeSeconds = (totalSamplesPerDmaBlock / adc_triggers_per_second);


            // Compute the number of blocks from the TIME

            // old double nr_dma_blocks_per_acquisition = Convert.ToInt32(cmbNrDmaBlocks.SelectedItem);

            double blocks_per_acq_temp;
            try {
                // it is important to convert the .Text member 
                // (not SelectedItem) so items can be ADDED
                blocks_per_acq_temp = Convert.ToDouble(cmbTimeToRecord.Text);
            }

            catch (Exception ex)
            {
                powMonInfoMsg(
                "Converting cmbTimeToRecord.Text to a double failed: " + ex.ToString() + "\r\n"
                + " Restored to default \r\n" );

                MessageBox.Show("Converting cmbTimeToRecord.Text to a double failed: " + ex.ToString() + "\r\n"
                + " Restored to default. \r\n", "Combo Box Entry Error",
                            MessageBoxButtons.OK);

                // this may cause an event to fire
                initCmbTimeToRecord();
                // this should never throw an exception since we restored the default
                blocks_per_acq_temp = Convert.ToDouble(cmbTimeToRecord.Text);

            }


            if (cmbUnitsOfTime.SelectedIndex == 2)  // hours
            {
                blocks_per_acq_temp *= (60 * 60);

            } else
            if (cmbUnitsOfTime.SelectedIndex == 1)  // minutes
            {
                blocks_per_acq_temp *= 60;
            }
            else      // must be seconds, so no need to multiply
            {

            }

            nrDmaBlocksRequested = Convert.ToInt32(blocks_per_acq_temp / oneDmaBlockAcqTimeSeconds);



            double total_samples = nrDmaBlocksRequested * totalSamplesPerDmaBlock;

            
            double total_acq_time = total_samples / adc_triggers_per_second;



            powMonInfoMsg(
                    String.Format("{0,8}", totalSamplesPerDmaBlock) +
                          " total samples/DMA block.\r\n"
                          );
          

            powMonInfoMsg(
                    String.Format("{0,8}", sameChannelSamplesPerDmaBlock) +
                          " same channel samples/DMA block " +
                          channelsSampledInSameDmaBlock +
                          ".\r\n"
                          );

            powMonInfoMsg(
                    String.Format("{0,8}",nrDmaBlocksRequested) +
                          " DMA blocks/acquisition.\r\n"
                          );

            powMonInfoMsg(
                    String.Format("{0,15:0.000000}", oneDmaBlockAcqTimeSeconds) + 
                          " s single DMA block acquisition time" +
                          ".\r\n"
                          );

            // compute a polling rate that is more often than block time
            suggestedPollRate = Convert.ToInt32((oneDmaBlockAcqTimeSeconds / 2.5) * 1000.0);

            powMonInfoMsg(
                    String.Format("{0,15:0.000000}", suggestedPollRate) +
                          " ms suggested polling rate" +
                          ".\r\n"
                          );


            powMonInfoMsg(
                    String.Format("{0,15:0.000000}", total_acq_time) + 
                          " s total acquisition time" +
                          ".\r\n"
                          );

            powMonInfoMsg(
                    String.Format("{0,8}",total_samples) + 
                          " total samples all channels" + 
                          ".\r\n"
                          );
        }


        private void cmbDMASampleRateAddIfNeeded()
        {
            if (cmbDMASampleRate.Text != "")
            {

                if (cmbDMASampleRate.SelectedItem == null)
                {
                    // the value in the textbox is not in the list!
                    // we will add it, but this does not make it the
                    // .SelectedItem!  So, be sure to convert the
                    // .Text member when we want to use the value
                    // of this combo box
                    cmbDMASampleRate.Items.Add(cmbDMASampleRate.Text);
                }
            }

        }


        private void cmbTimeToRecordAddIfNeeded()
        {
            if (cmbTimeToRecord.Text != "")
            {

                if (cmbTimeToRecord.SelectedItem == null)
                {
                    // the value in the textbox is not in the list!
                    // we will add it, but this does not make it the
                    // .SelectedItem!  So, be sure to convert the
                    // .Text member when we want to use the value
                    // of this combo box
                    cmbTimeToRecord.Items.Add(cmbTimeToRecord.Text);
                }
            }
        }



        private void cmbDMASampleRate_KeyUp(object sender, KeyEventArgs e)
        {
            // The Enter key is filtered out of many events that you would
            // think it should trigger (like *_KeyPress, *_Leave, etc.)
            // But this seems to work.
            if (e.KeyCode == Keys.Return)
            {
                cmbDMASampleRateAddIfNeeded();
                calcAcqParmsFromSettings(sender, e);
            }
        }



        // capture the color of the Playback button
        // before it is turned red
        Color playbackBtnRestoreColor;

        // remember the button caption so we can restore it
        string playbackBtnCaptionToRestore;

        // restore thjese items if playback had to be aborted
        private void playBackDoneOrAbort()
        {
            // return button to normal color and name
            bPlaybackInProgress = false;
            btnPlayBack.Text = playbackBtnCaptionToRestore;
            btnPlayBack.BackColor = playbackBtnRestoreColor;
            btnStatPlaying.BackColor = Color.White;

            powMonDataLogStreamCloseIfOpen();
        }



        private void powMonDataLogStreamCloseIfOpen()
        {
            if (bPowMonDataLogStreamOpen)
            {
                powMonInfoMsg("closing \r\n");
                powMonDataLogStream.Close();
                bPowMonDataLogStreamOpen = false;
                powMonInfoMsg(".........closed. \r\n");

            }
        }


        private void btnPlayBack_Click(object sender, EventArgs e)
        {
            int i, j;
            double beam0_point = 0.0;
            double beam1_point = 0.0;
            double beam2_point = 0.0;

            bool bSplit = false;


            // general message incase of problem
            string summary = "Problem in btnPlayBack_Click(): aborting.\r\n";

            // This is also tested for 0 to decide if "performance verification"
            // statistical data is available. Resetting it causes the Statistics
            // button to report no data yet
            nrDmaBlocksReceived = 0;

            // this allows us to process a second click as an abort
            if (bPlaybackInProgress == true)
            {
                // set this flag so the inner loop can notice it
                // and exit this click
                bUser_Requests_Abort = true;
                return;
            };

            // capture the color of the Playback button
            // before it is turned red
            playbackBtnRestoreColor = btnPlayBack.BackColor;

            // remember the button caption so we can restore it
            playbackBtnCaptionToRestore = btnPlayBack.Text;

            // no further need to test oscope for null after this
            if (instOsc == null)
            {
                powMonSysMsg("\r\n");   // code meaning clear SysMsg box
                powMonSysMsg("The Oscilloscope must be displayed" + "\r\n");
                playBackDoneOrAbort();
                return;
            }

            // declare these here so they are available anywhere 
            // in this function
            BinaryFormatter bFormatter = new BinaryFormatter();

            string logFilePathPlusName = txtLogFilePathPlusName.Text;

            StreamReader tInFormatter = null;

            int lineIndex = 0;  // meaning no lines read yet

            try
            {
                powMonDataLogStream = File.Open(logFilePathPlusName, FileMode.Open);
                bPowMonDataLogStreamOpen = true;

                if (cbTextFileFormatting.Checked)
                {

                    tInFormatter = new StreamReader(powMonDataLogStream);
                }
                else
                {
                    // no formatter needed for binary
                }

            }
            catch (Exception ex)
            {
                powMonSysMsg("\r\n");   // code meaning clear SysMsg box
                powMonSysMsg(summary);
                powMonInfoMsg(summary +"[In btnPlayBack_Click() the File.Open failed]\r\n"+ ex.ToString() + "\r\n");
                playBackDoneOrAbort();

                return;
            }

            // part of allowing us to process a second click as an abort
            bPlaybackInProgress = true;
            btnPlayBack.Text = "Stop";
            btnPlayBack.BackColor = Color.LimeGreen;
            btnStatPlaying.BackColor = Color.LimeGreen;

            powMonInfoMsg("Data from File: " + logFilePathPlusName + ".\r\n");

            // for reading in data line
            string txtFileLine = "(none read yet)";



            try {
                // read from the file a number that tells us if the file contains
                // channel 1 or 2 or both data.  To avoid flashing the scope every
                // time, we will detect if the beams need to change and only re-paint
                // the scope if they do 
                byte readOneOrTwoOrBoth;
                bool beamsNeedChange = false;

                if (cbTextFileFormatting.Checked)
                {
                    // From the text file, we will read in several lines that occur only
                    // once in the file before the data starts

                    //File looks like this:
                    //1    Channel acquired (3=both)
                    //2    
                    //seconds    
                    //2000    samples/second
                    //FALSE    continuous
                    //500000    us/block rate
                    //645593    
                    //4.16E-02    
                    //4.16E-02    
                    //4.16E-02    
                    //4.16E-02    
                    //etc.  All the following data is samples

//                    if ((txtFileLine = tInFormatter.ReadLine()) != null)
                    try
                    {
                        txtFileLine = tInFormatter.ReadLine();
                        lineIndex++;

                        // there is extra discriptive text on this line, so ignore that
                        readOneOrTwoOrBoth = System.Convert.ToByte(txtFileLine[0]);

                        // we can adapt the Scope to the number of beams needed: 1 or 2
                        if (readOneOrTwoOrBoth == AdpApi.ADP_POW_1)
                        {
                            bSplit = false;
                            if (cbVsup1Monitor.Checked != true) beamsNeedChange = true;
                            if (cbVsup2Monitor.Checked != false) beamsNeedChange = true;
                        }
                        else
                            if (readOneOrTwoOrBoth == AdpApi.ADP_POW_2)
                            {
                                bSplit = false;
                                if (cbVsup1Monitor.Checked != false) beamsNeedChange = true;
                                if (cbVsup2Monitor.Checked != true) beamsNeedChange = true;
                            }
                            else
                                if (readOneOrTwoOrBoth == AdpApi.ADP_POW_1_AND_2)
                                {
                                    bSplit = true;
                                    if (cbVsup1Monitor.Checked != true) beamsNeedChange = true;
                                    if (cbVsup2Monitor.Checked != true) beamsNeedChange = true;
                                }

                        if (beamsNeedChange)
                        {
                            if (readOneOrTwoOrBoth == AdpApi.ADP_POW_1)
                            {
                                cbVsup1Monitor.Checked = true;
                                cbVsup2Monitor.Checked = false;
                            }
                            else
                            if (readOneOrTwoOrBoth == AdpApi.ADP_POW_2)
                            {
                                cbVsup1Monitor.Checked = false;
                                cbVsup2Monitor.Checked = true;
                            }
                            else
                            if (readOneOrTwoOrBoth == AdpApi.ADP_POW_1_AND_2)
                            {
                                cbVsup1Monitor.Checked = true;
                                cbVsup2Monitor.Checked = true;
                            }
                            // so scope has correct number of beams
                            rePaintScopeInSameLocation();
                        }

                        // read time-to-acquire header line
                        txtFileLine = tInFormatter.ReadLine();
                        lineIndex++;

                        //tOutFormatter.WriteLine(cmbTimeToRecord.Text);
                        cmbTimeToRecord.Text = txtFileLine;

                        // read units of time header line
                        txtFileLine = tInFormatter.ReadLine();
                        lineIndex++;

                        //tOutFormatter.WriteLine(cmbUnitsOfTime.Text);
                        cmbUnitsOfTime.Text = txtFileLine;

                        // read sample rate header line
                        txtFileLine = tInFormatter.ReadLine();
                        lineIndex++;

                        //tOutFormatter.WriteLine(cmbDMASampleRate.Text + "\t" + "samples/second");
                        // eliminate the tab and everything that follows
                        int idxOfTab = txtFileLine.IndexOf("\t");
                        txtFileLine = txtFileLine.Substring(0, idxOfTab);
                        cmbDMASampleRate.Text = txtFileLine;

                        // read continuous checked header line
                        txtFileLine = tInFormatter.ReadLine();
                        lineIndex++;
                        //txtFileLine.
                        //tOutFormatter.WriteLine(cbContinuousAcquisition.Checked.ToString() + "\t" + "continuous");
                        if (txtFileLine.Contains("True"))
                        {
                            cbContinuousAcquisition.Checked = true;
                        }
                        else
                        {
                            cbContinuousAcquisition.Checked = false;
                        }

                        // read us for one block header line
                        txtFileLine = tInFormatter.ReadLine();
                        lineIndex++;

                        //tOutFormatter.WriteLine(((Int32)(oneDmaBlockAcqTimeSeconds * 1000000.0)).ToString() + "\t" + "us/block rate");
                        // eliminate the tab and everything that follows
                        idxOfTab = txtFileLine.IndexOf("\t");
                        txtFileLine = txtFileLine.Substring(0, idxOfTab);
                        

                        fileDmaBlockAcqTimeUs = System.Convert.ToInt32(txtFileLine);

                        // if the recorded file used non-standard rates, we
                        // need to call these so the rates are added to the list
                        cmbTimeToRecordAddIfNeeded();
                        cmbDMASampleRateAddIfNeeded();
                        calcAcqParmsFromSettings(sender, e);

                        // rest of the file should be all data

                    }   // end of the try that is intended to cover all the header lines

                    
                    
                    catch (Exception ex)
                    {   
                        powMonSysMsg("\r\n");   // code meaning clear SysMsg box
                        powMonSysMsg(summary);
                        powMonInfoMsg(summary +"[problem reading header lines]\r\n"+ ex.ToString() + "\r\n");
                        powMonInfoMsg("Line# " +lineIndex.ToString() + " = [\""+ txtFileLine + "\"]\r\n");

                        playBackDoneOrAbort();

                        return;
                    }


                    // Next read settings from the text file


                }   // text file format box is checked 


                // clear the oscope trace starting playback
                instOsc.Clear();

                long usQpcPlaybackBase;
                long usQpcPlaybackDelta;

                // get a base to use for throttling (playing back at the same
                // speed we recorded approximately)
                QueryPerformanceCounter(out usQpcPlaybackBase);
                usQpcPlaybackBase = (long)(((double)usQpcPlaybackBase) / dQpcCountsPerUs);   // cvt to us always

                // count as we read blocks from the file
                nrDmaBlocksPlayed = 0;

                // time is not stored in the text file, so we estimate
                // it based on the time we know it takes to acquire
                // one block of samples
                long qpcUsComputed = 0;

                while (powMonDataLogStream.Position < powMonDataLogStream.Length)
                {
                    // Get the data from the file, and put it in the same place
                    // it was when we originally read it as a reply from the USB
                    if (cbTextFileFormatting.Checked)
                    {
                        // put a computed time based on the blocks displayed so far
                        qpcUsComputed += fileDmaBlockAcqTimeUs;

                        adpApi.managedReturnedSamplesFromDma.qpc_us_after_dma_blk_rcvd = qpcUsComputed;


                        while ((txtFileLine = tInFormatter.ReadLine()) != null)
                        {

                            //get data
                            adpApi.managedReturnedSamplesFromDma.dma_buf_samples[lineIndex] = System.Convert.ToDouble(txtFileLine);

                            lineIndex++;

                            // reset after each batch                        
                            if (lineIndex == AdpApi.NR_SAMPLES_IN_CURRENT_MSG)
                            {
                                lineIndex = 0;
                                break;  // out of while loop to process data
                            }

                        }
                    }
                    else
                    {
                        // read block via bFormatter
                        adpApi.managedReturnedSamplesFromDma = (AdpApi.DLL_ADP_CURRENT_AND_DMA_MANAGED)bFormatter.Deserialize(powMonDataLogStream);
                        //adpApi.managedReturnedSamplesFromDma
                    }


                    // advance this as we read blocks from the file
                    nrDmaBlocksPlayed++;

                    if (!cbTextFileFormatting.Checked)
                    {
                        // set this flag so we know if the block has one channel of
                        // data or two channels of data
                        bSplit = ((adpApi.managedReturnedSamplesFromDma.dmaReplyFlags & 1) == 1);

                    }

                    if (cbShowDebug1.Checked)
                    {
                        // report the PC block us timestamp
                        powMonInfoMsg(
                        String.Format("{0,8}", adpApi.managedReturnedSamplesFromDma.qpc_us_after_dma_blk_rcvd) +
                              " us" +
                              ".\r\n"
                              );
                    }

                    // Simple throttling of playback: we kill time until
                    // the playback delta is greater than the recorded time,
                    // then display the block

                    while (true)
                    {
                        // time now
                        QueryPerformanceCounter(out usQpcPlaybackDelta);

                        usQpcPlaybackDelta = (long)(((double)usQpcPlaybackDelta) / dQpcCountsPerUs);   // cvt to us always

                        usQpcPlaybackDelta = usQpcPlaybackDelta - usQpcPlaybackBase;

                        if (cbShowDebug1.Checked)
                        {
                            powMonInfoMsg(
                            String.Format("{0,8}", usQpcPlaybackDelta) +
                                  " pbD" +
                                  ".\r\n");
                        }

                        if (usQpcPlaybackDelta >
                            adpApi.managedReturnedSamplesFromDma.qpc_us_after_dma_blk_rcvd)
                        {

                            break;  // out of while loop
                        }
                        delayWithDoEvents(2);
                    }

                    // Test if buffer is one or two channels of data
                    if (!bSplit)
                    {
                        for (i = 0; i < AdpApi.NR_SAMPLES_IN_CURRENT_MSG; i++)
                        {
                            // add data to oscope 1 point at a time
                            // beam0 is data, beam2 and beam3 are 0

                            // get raw value    
                            beam0_point = adpApi.managedReturnedSamplesFromDma.dma_buf_samples[i];    // converts to double

                            // put the data point in the scope
                            instOsc.AddData(beam0_point, beam1_point, beam2_point);
                        }
                    }
                    else
                    {
                        // Buffer is split and can only have ISUP1/ISUP2 data in first/second
                        // half (split mode is not applicable if any other data source)
                        for (i = 0, j = (AdpApi.NR_SAMPLES_IN_CURRENT_MSG / 2); i < (AdpApi.NR_SAMPLES_IN_CURRENT_MSG / 2); i++, j++)
                        {
                            // get raw value    
                            beam0_point = adpApi.managedReturnedSamplesFromDma.dma_buf_samples[i];    // converts to double

                            // get raw value    
                            beam1_point = adpApi.managedReturnedSamplesFromDma.dma_buf_samples[j];    // converts to double

                            // put the data point in the scope
                            instOsc.AddData(beam0_point, beam1_point, beam2_point);

                        }
                    }

                    if (bUser_Requests_Abort) break;    // out of while loop

                }   // end of while more data in the file

            }   // end of try

            catch (Exception ex)
            {   
                powMonSysMsg("\r\n");   // code meaning clear SysMsg box
                powMonSysMsg(summary);
                powMonInfoMsg(summary + ex.ToString() + "\r\n");
                powMonInfoMsg("Line# "+ lineIndex.ToString() + "["+txtFileLine+"]" + "\r\n");

            }

            bUser_Requests_Abort = false;

            playBackDoneOrAbort();            
        }



        // this is designed to be called on a separate
        // thread ONLY (never directly)
        private void displayUserAbortButton()
        {

            DialogResult result;

            // the oscilloscope was obscuring the message box
            // unitl I added this
            //this.TopMost = true;
            {

                result = MessageBox.Show("Click Cancel to abort acq early", "Reminder",
                    MessageBoxButtons.OKCancel);
            }
            //this.TopMost = false;

            if (result == System.Windows.Forms.DialogResult.Cancel)
            {
                bUser_Requests_Abort = true;
                return;
            }

        }


        private void rbRecordFile1_CheckedChanged(object sender, EventArgs e)
        {

            // "Record to file"  radio buttons A,B,C,D, all use this
            // same _CheckedChanged() function (the No radio button
            // does not use this _CheckedChanged) 

            // if this button is going to false, do nothing
            if (((RadioButton)sender).Checked == false) return;

            // Not going false means it must be going to True


            // the oscilloscope may obscure the message box
            // uless this is added 
            this.TopMost = true;
            {
                string CurrentDataFileName = txtLogFilePathPlusName.Text;

                MessageBox.Show("The file " + CurrentDataFileName 
                    + " will be overwritten without prompting each "
                    + "time Record is clicked.", "Reminder",
                    MessageBoxButtons.OK);
            }
            this.TopMost = false;

        }


        // multiple clicks will rotate through the charts we can display
        private int whichStatChartShowing = 0;

        private void btnShowIsrStats_Click(object sender, EventArgs e)
        {
            // loop below does a post-acquisition dump of the timing data
            // acquired in the real DMA loop above

            int i;

            if (nrDmaBlocksReceived == 0)
            {
                powMonInfoMsg("No statistical data acquired yet." + "\r\n");
                return;
            }



            // Compute approximate times and rates from the PC 
            // performance counter

            // The this code on the PC collects two times:
            //
            // A. The time the "get DMA data" command starts
            // B. The time the command completes

            // Ideally, we would see the spacing for these events
            // would be exactly uniform: 
            // A[n] to A[n+1] = A[n+1] to A[n+2] 
            // exactly for all n.  But we know there will be variation,
            // and the code here is designed to measure that variation.

            // The A and B events are handled separately, but the
            // process is the same.  Both are recorded as microseconds
            // elapsed relative to a "zero time" established just
            // before entering the Record loop.

            // If we divide the elapsed time from start by the
            // loop index + 1, we generate a number I call the "long
            // term average" which gets smoother the more loops we
            // go through.  

            // Using that average, we can compare a new A or B event
            // to the time we expect it to happen, and get a measure
            // of how early or late it is relative to our expectation.

            // we may have saved less stat data than the total number of blocks
            int lesserOfLoopsOrMax = nrDmaBlocksReceived;

            if (lesserOfLoopsOrMax > MAX_DMA_BLOCKS_FOR_STATS)
            {
                lesserOfLoopsOrMax = MAX_DMA_BLOCKS_FOR_STATS;
            }

            object[] processedForDisplay = new Object[lesserOfLoopsOrMax];

            for (i = 0; i < lesserOfLoopsOrMax; i++)
            {

                // Computation of the A-to-B difference which is the time
                // spent in the block transfer
                double timeInTransferFunc = 0;
                timeInTransferFunc = usQpcAfterDmaBlkRcvd[i] - usQpcBeforeDmaCmdSent[i];


                // Computations for Event "A" uniformity
                // ev = event
                double evAexpectedLongTermAvg = 0;
                double evAdiffFromPrev = 0;
                double evAdeltaFromAverage = 0;

                if (i>2)
                {
                    // Compute a new Long Term Average So Far
                    evAexpectedLongTermAvg = usQpcBeforeDmaCmdSent[i - 1] / (i + 1);
                    // Compute a difference from this event to the previous event
                    evAdiffFromPrev = usQpcBeforeDmaCmdSent[i] - usQpcBeforeDmaCmdSent[i - 1];
                    // Now how far from the expected difference is this time?
                    evAdeltaFromAverage = evAdiffFromPrev - evAexpectedLongTermAvg;
                }

                // Computations for Event "B" uniformity
                double evBexpectedLongTermAvg = 0;
                double evBdiffFromPrev = 0;
                double evBdeltaFromAverage = 0;

                if (i > 2)
                {
                    // Compute a new Long Term Average So Far
                    evBexpectedLongTermAvg = usQpcAfterDmaBlkRcvd[i - 1] / (i + 1);
                    // Compute a difference from this event to the previous event
                    evBdiffFromPrev = usQpcAfterDmaBlkRcvd[i] - usQpcAfterDmaBlkRcvd[i - 1];
                    // Now how far from the expected difference is this time?
                    evBdeltaFromAverage = evBdiffFromPrev - evBexpectedLongTermAvg;
                }

                if (cbShowDebug1.Checked)
                {
                    powMonInfoMsg(
                    i.ToString() +
                    " qpcB4= " + (usQpcBeforeDmaCmdSent[i]).ToString()
                    + " " +
                    " elta= " + (evAexpectedLongTermAvg).ToString()
                    + " " +
                    " difPrev= " + (evAdiffFromPrev).ToString()
                    + " " +
                    " deltaPrev= " + (evAdeltaFromAverage).ToString()
                    + " " +

                    " qpcAft= " + (usQpcAfterDmaBlkRcvd[i]).ToString()
                    + " " +
                    " elta= " + (evBexpectedLongTermAvg).ToString()
                    + " " +
                    " difPrev= " + (evBdiffFromPrev).ToString()
                    + " " +
                    " deltaPrev= " + (evBdeltaFromAverage).ToString()
                    + " " +

                     "\r\n");
                }

                // put in array for graph display
                switch (whichStatChartShowing)
                {
                    case 0:
                        //processedForDisplay[i] = nrPollTicksWhileBusy[i];
                        processedForDisplay[i] = countQueriesBeforeDataAvailable[i];
                        break;

                    case 1:
                        processedForDisplay[i] = timeInTransferFunc;

                        break;

                    case 2:
                        processedForDisplay[i] = evAdeltaFromAverage;
                        break;

                    case 3:
                        processedForDisplay[i] = evBdeltaFromAverage;
                        break;

                    case 4:
                        processedForDisplay[i] = usQpcBeforeDmaCmdSent[i];
                        break;

                    case 5:
                        processedForDisplay[i] = usQpcAfterDmaBlkRcvd[i];
                        break;

                }   // end of switch


            };  // end of for nr DMA blocks





            // throw away lines after a batch has been transfered
            limitLinesInTxtSysMsgs();


            Object[,] objArray = new Object[2, lesserOfLoopsOrMax];

            for (int k = 0; k < lesserOfLoopsOrMax; k++)
            {
                objArray[0, k] = processedForDisplay[k];
                objArray[1, k] = nrPollTicksWhileBusy[k];
                powMonInfoMsg("objArray[0, k]= " + Convert.ToString(objArray[0, k]) + "\r\n");
                powMonInfoMsg("objArray[1, k]= " + Convert.ToString(objArray[1, k]) + "\r\n");
            }

            powMonInfoMsg("----------------- end of btnShowIsrStats_Click() " + "\r\n");
            whichStatChartShowing++;

            if (whichStatChartShowing > 5) whichStatChartShowing = 0;

        }// end of btnShowIsrStats_Click()



        private void cmbPowerPollFreq_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbTimeToRecord_SelectedIndexChanged(object sender, EventArgs e)
        {
            calcAcqParmsFromSettings(sender, e);
        }

        private void cmbUnitsOfTime_SelectedIndexChanged(object sender, EventArgs e)
        {
            calcAcqParmsFromSettings(sender, e);
        }

        private void cmbTimeToRecord_KeyUp(object sender, KeyEventArgs e)
        {

            // The Enter key is filtered out of many events that you would
            // think it should trigger (like *_KeyPress, *_Leave, etc.)
            // But this seems to work.
            if (e.KeyCode == Keys.Return)
            {
                cmbTimeToRecordAddIfNeeded();
                calcAcqParmsFromSettings(sender, e);
            }

        }


        private void cmbSn_KeyUp(object sender, KeyEventArgs e)
        {

            if (cbShowDebug1.Checked) 
            powMonInfoMsg("cmbSn_KeyUp()= " + Convert.ToString(e.KeyCode) + "\r\n");

            // The Enter key is filtered out of many events that you would
            // think it should trigger (like *_KeyPress, *_Leave, etc.)
            // But this seems to work.
            if (e.KeyCode == Keys.Return)
            {
                // treat ENTER same as Load Cal button
                btnLoadCalSettings_Click(sender, e);
            }
            else
            {
                // Any other key should turn this red to warn not calibrated
                resetToUncaledState();
            }

        }


        private void resetToUncaledState()
        {
            // any change to the SN will cause us to zero all
            // the cal factors (and so all DMA results will
            // revert to ADC12 Counts, and mark the SN field
            // background RED to indicate that no calibration
            // factors are in effect
            if (cbShowDebug2.Checked) powMonInfoMsg("Resetting to un-cal'ed state\r\n");

            zeroCurrentCalFactors();
            cmbSn.BackColor = Color.Red;
        }


        private void cmbSn_TextUpdate(object sender, EventArgs e)
        {

            if (cbShowDebug1.Checked) 
            powMonInfoMsg("cmbSn_TextUpdate()= " + cmbSn.Text + "\r\n");

            resetToUncaledState();

            // 1. When a calibration completes
            // 2. When a load-calibration-from-file button happens


        }

        private void saveSnToFile()
        {
            string pathPlusFileName = pathOnlyToExeDir + "\\AdpSerialNumFor" + titleOfMezz + ".txt";

            // attempt to delete: not checked for failure since it is
            // ok if this fails since the file may not exist yet
            File.Delete(pathPlusFileName);
            
            StreamWriter sw;
            try
            {
                sw = new StreamWriter(pathPlusFileName);



                // if an item is currently selected, then we want to write
                // that item first, then skip it when it comes up in the loop.
                // In this way we will re-order the list so the current item
                // is always at the top
                if (((string)this.cmbSn.SelectedItem) == this.cmbSn.Text)
                {
                    // write one SN to file 
                    sw.WriteLine(this.cmbSn.Text);
                }
                else
                {
                    // ADD it so it will be in the list next time
                    cmbSn.Items.Add(this.cmbSn.Text);
                    //
                    sw.WriteLine(this.cmbSn.Text);
                }


                // write all items in combo box (except the selected item) 
                // to the remembered SN file
                foreach (string item in this.cmbSn.Items)
                {
                    //if (item ==  ((string)this.cmbSn.SelectedItem)) {
                    if (item == this.cmbSn.Text)
                    {
                        // do not write the text item

                    } else
                    if (item == "****") {
                        // do not write the **** string

                    } else 
                    {
                        // write one SN to file 
                        sw.WriteLine(item);
                    }
                }
                sw.Close();
            }

            catch (Exception ex)
            {
                //Console.WriteLine(
                powMonInfoMsg("In saveSnToFile() Exception caught." + ex.ToString());
                return;
            }


        }


        private int tryLoadingSnAndCalAtFormStart(object sender, EventArgs e)
        {
            // to be invoked by a timer 2 seconds after the form is activated

            int rslt = 0;

            // is this a place we can call load cal?
            if (withinAutoCalDueToActivate == 0)
            {
                // avoid re-entering this due to multiple _Activate() calls
                withinAutoCalDueToActivate++;
                this.btnFactCalSettings.PerformClick();
                //this.btnAutoPowerCal.PerformClick();
                withinAutoCalDueToActivate--;


                //Win32API.SendMessage(hWndButton,Win32API.WM_LBUTTONDOWN,......); 

                //                this->SetActiveWindow();
                //this->GetDlgItem(nButtonID)->PostMessage(BM_CLICK);

                //or

                //this->PostMessage(WM_COMMAND,
                //                  MAKEWPARAM(nButtonID, BN_CLICKED),
                //                  (LPARAM)(this->GetDlgItem(nButtonID)->GetSafeHwnd()));

                //                btnFactCalSettings_Click(sender, e);

                //You do it by sending the button a BM_CLICK message.

                //Something like this:

                //CButton *b = (CButton *)GetDlgItem(IDOK);
                //::PostMessage(b->m_hWnd, BM_CLICK, 0, 0);

                //this->GetDlgItem(

                //    btnFactCalSettings)->PostMessage(BM_CLICK);


            }
            else
            {
            };

            return rslt;

        }



        private int checkForSnFile(object sender, EventArgs e)
        {

            int rslt = 0;
            // zero this incase the SN file read fails
            cmbSn.Text = "0000";

            string pathPlusFileName = pathOnlyToExeDir + "\\AdpSerialNumFor"+titleOfMezz+".txt";

            try
            {
                // Create an instance of StreamReader to read from a file.
                // The using statement also closes the StreamReader.
                using (StreamReader sr = new StreamReader(pathPlusFileName))
                {
                    String line;

                    while (!sr.EndOfStream)
                    {
                        line = sr.ReadLine();
                        
                        // We have set the DropDownStyle property to DropDown, so we 
                        // you can type any value in the editable area of the ComboBox.

                        // put first line into the .text field!!
                        if (cmbSn.Text == "0000")
                        {
                            cmbSn.Items.Clear();
                            cmbSn.Text = line;
                        }
                        // add each SN we find to the droplist
                        //cmbSn. = txtFileLine;
                        cmbSn.Items.Add(line);
                        if (cbShowDebug1.Checked) powMonInfoMsg("checkForSnFile() Adding" + line + "\r\n");

                        //cmbSn.Text = txtFileLine;
                    }
                    sr.Close();


                }

            }
            catch (Exception)
            {
                powMonSysMsg("\r\n");   // code meaning clear SysMsg box
                powMonSysMsg("Warning: The serial number for the ADP100 board is unknown.  Please enter the serial number for the ADP100 attached to the "
                    + titleOfMezz + ".");

                // indicate an error to caller
                rslt = -1;
            }

            return rslt;
        }


        

        // TODO: what is this for
        void popUpFrmAdpfrmAdpMsChart_FormClosed(object sender, EventArgs e)
        {
            throw new Exception("The method or operation is not implemented.");
        }





        // this textbox will pop up and hold all messages that are 
        // NOT PowMon "System Messages"
        public AdpNonModalMessageBox frmAdpInfoMsgs = null;
        public TextBox txtAdpInfoMsgs = null;
        public Button btnAdpInfoMsgsClear = null;
        public System.Drawing.Size btnAdpInfoMsgsClearSize = new System.Drawing.Size(0, 0);


        private void popUpFrmAdpInfoMsgs()
        {
            // Code: Adding Controls at Run Time (Visual C#)
            frmAdpInfoMsgs = new AdpNonModalMessageBox();

            frmAdpInfoMsgs.Text = "Informational messages";
            frmAdpInfoMsgs.Resize += new System.EventHandler(frmAdpInfoMsgs_Resize);
            frmAdpInfoMsgs.dResult = DialogResult.None;


            btnAdpInfoMsgsClear = new Button();
            btnAdpInfoMsgsClear.Click += new System.EventHandler(btnClearTxtPopUp_Click);
            btnAdpInfoMsgsClear.Text = "Clear text";

            // the button above will appear at the top, so start the top of the
            // textbox down from the top by the height of the button
            btnAdpInfoMsgsClearSize = btnAdpInfoMsgsClear.Size;
            btnAdpInfoMsgsClearSize.Width = 0;


            txtAdpInfoMsgs = new TextBox();


            txtAdpInfoMsgs.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            //dynamictextbox.Location = new System.Drawing.Point(12, 331);
            txtAdpInfoMsgs.MaxLength = 500;
            txtAdpInfoMsgs.Multiline = true;
            //dynamictextbox.Name = "tbCurrentToolDataDisplay";
            txtAdpInfoMsgs.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            txtAdpInfoMsgs.TabIndex = 2;
            txtAdpInfoMsgs.WordWrap = false;


            // docking to bottom will cause auto re-size if the form is resized
            // but this is not useful since the 0,0 point for location is
            // bottom left of the form
            txtAdpInfoMsgs.Dock = System.Windows.Forms.DockStyle.Bottom;

            // make all existing controls invisible
            makeExistingControlsInvisible(frmAdpInfoMsgs);

            // add text box to the form now
            frmAdpInfoMsgs.Controls.Add(txtAdpInfoMsgs);

            // add the control we want
            frmAdpInfoMsgs.Controls.Add(btnAdpInfoMsgsClear);

            //frmAdpInfoMsgs.Show(this);

            // At this point the .Size of the pop up is from the form we cloned.
            // We have no use for this size, though so ignore the following
            // System.Drawing.Size tempSize = frmAdpInfoMsgs.Size; 
            popUpFrmAdpInfoMsgs_RightSize();

            frmAdpInfoMsgs.Show(this);

            // default this form to not vsible
            frmAdpInfoMsgs.Visible = false;

        }

        private void popUpFrmAdpInfoMsgs_RightSize()
        {

            // we may want to use the total width of the display which
            // we can get from the Bounds member of the screen object.
            // there may be more than one screen! TODO
            Screen[] allScreens = Screen.AllScreens;


            // We want the size to match the AdpPowMonForm roughly
            // but to look "docked" to the bottom of that form.

            // this gets the position of the AdpPowMonForm on the screen
            System.Drawing.Point tLocation = this.Location;


            // variables to pass to the scope resize function
            System.Drawing.Point msgFormLocation = new System.Drawing.Point();
            System.Drawing.Size msgFormSize = new System.Drawing.Size();

            // make the message form position immediately below
            // the AdpPowMonForm (looking as if docked)
            msgFormLocation.X = tLocation.X ;
            msgFormLocation.Y = tLocation.Y + this.Size.Height;


            // make the message form width the same as the AdpPowMonForm
            msgFormSize.Width = this.Width;

            // make the message form height the same as the the AdpPowMonForm
            msgFormSize.Height = this.Size.Height;

            // check if the message form goes off the screen below
            if ( (msgFormLocation.Y + msgFormSize.Height) >  allScreens[0].Bounds.Height)
            {
                // make the message form height fill what is left of the screen below
                // the AdpPowMonForm
                msgFormSize.Height = allScreens[0].Bounds.Height - (tLocation.Y + this.Size.Height);
            }

            // make the message form position immediately below
            // the AdpPowMonForm (as if docked)
            msgFormLocation.X = tLocation.X;
            msgFormLocation.Y = tLocation.Y + this.Size.Height;

            frmAdpInfoMsgs.Size = msgFormSize;
            frmAdpInfoMsgs.Location = msgFormLocation;   



            // a small increase in the height will force _Resize() to be called
            // so the TextBox can be made the correct size
            //tempSize.Height++;

            // will force a call to _Resize()
            //frmAdpInfoMsgs.Size = tempSize;              

        }

        private void clearTxtPopUp()
        {
            if (!frmAdpInfoMsgs.IsDisposed)
            {
                // form is open so OK to add text

                // this will erase all text in the box
                //txtAdpInfoMsgs.Text = "";
                // so will this???
                txtAdpInfoMsgs.Clear();
            }
        }

        private void btnClearTxtPopUp_Click(object sender, EventArgs e)
        {
            // this will erase all text in the box
            clearTxtPopUp();
        }

        // when the info messages (pop-up) form is resized, adjust the text box too
        private void frmAdpInfoMsgs_Resize(object sender, EventArgs e)
        {

            System.Drawing.Point tbLocation = new Point();
            System.Drawing.Size tbSize = new Size();

            // Always located at 0,0, and the Size is what will adjust
            tbLocation.X = 0;
            tbLocation.Y = 0;
            txtAdpInfoMsgs.Location = tbLocation;

            tbSize.Height = frmAdpInfoMsgs.ClientRectangle.Height - (btnAdpInfoMsgsClearSize.Height);
            tbSize.Width = frmAdpInfoMsgs.ClientRectangle.Width;

            // TODO: consider getting the size from AdpPowMonForm, then
            // reduce is this would extend past the bottom of the screen

            txtAdpInfoMsgs.Size = tbSize;
            
        }



        private void powMonInfoMsg(string argText)
        {
            // check if this has been created yet
            if (frmAdpInfoMsgs == null)
            {
                popUpFrmAdpInfoMsgs();
            }


            if (!frmAdpInfoMsgs.IsDisposed)
            {
                // form is open so OK to add text
                txtAdpInfoMsgs.AppendText(argText);
            }

        }

        private void powMonSysMsg(string argText)
        {
            if (argText == "\r\n")
            {
                // this code alone in a call means clear 
                txtSysMsgs.Clear();
            }
            else
            {
                txtSysMsgs.AppendText(argText);
            }
        }

        private void makeExistingControlsInvisible(AdpNonModalMessageBox frmPassed)
        {

            // Now hide all the controls in the form we are cloning
            foreach (Control c in frmPassed.Controls)
            {
                // this is a control we want to get rid of
                c.Visible = false;
                // a problem using foreach is that as we *remove* items
                // the iterator will not iterate through all items
                // and we wind up not removing them all.  For this
                // reason we will just hide them and not remove.
                //frmAdpMsg.Controls.Remove(c);
            }
        }


        private void timerCheckCalAfterFormAppears_Tick(object sender, EventArgs e)
        {
            // disable this timer now that we have triggered
            timerCheckCalAfterFormAppears.Enabled = false;

            // the Factory and Other tab should not be available to customers
            removeFactoryTab();
            removeOtherTab();

            // ensure the CAL and RANGE bits are set to something
            // (the control properties setting in this case)
            anyCalSettingChanged();

            if (checkForSnFile(sender, e) == 0)
            {
                // we found the recent SN file
                tryLoadingSnAndCalAtFormStart(sender, e);

            }
            else
            {
                // already reported??

            }

        }

        private void btnDestroyScope_Click(object sender, EventArgs e)
        {
            // for test I want to see what this does
            if (instOsc != null) instOsc.Dispose();
            if (instOsc != null) instOsc = null;

            //instOsc = null;
        }

        private void AdpPowMonForm_Shown(object sender, EventArgs e)
        {


            // We use a timer to delay checking for the existence
            // of the Cal factors file until after the form has appeared
            // timer wants ms for interval
            timerCheckCalAfterFormAppears.Interval = 1;
            // turn on timer events
            timerCheckCalAfterFormAppears.Enabled = true;

        }


        private void anyCalSettingChanged()
        {
            // note this so scope can be rescaled
            bScopeShouldBeReScaled = true;

            int rslt =
                adpApi.AdpApiSetBitsOnly(calSettingIsup1, calSettingIsup2, rangeSettingIsup1, rangeSettingIsup2);

            string ccrr =
                  "c1=" + calSettingIsup1.ToString()
                + " c2=" + calSettingIsup2.ToString()
                + " r1=" + rangeSettingIsup1.ToString()
                + " r2=" + rangeSettingIsup2.ToString();

            if(cbShowDebug1.Checked)
                powMonInfoMsg("[" + ccrr + "]\r\n");
        }

        private void rbVsup1Cal0_CheckedChanged(object sender, EventArgs e)
        {
            //rbVsup1Cal0.Checked reflects the state before the 
            //click has changed it, so we have to look at the 
            //sender object to test for checked after this click

            if (((RadioButton)sender).Checked)
            {
                calSettingIsup1 = 0;
                // only send packet if this radio button is turning
                // ON since the .CheckedChanged() for the button that
                // is turning off will be called also
                anyCalSettingChanged();
            }
            else
            {
                calSettingIsup1 = 1;
            }



        }

        private void rbVsup1Cal1_CheckedChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                calSettingIsup1 = 1;
                // only send packet if this radio button is turning
                // ON since the .CheckedChanged() for the button that
                // is turning off will be called also
                anyCalSettingChanged();
            }
            else
            {
                calSettingIsup1 = 0;
            }
  
        }

        private void rbVsup2Cal0_CheckedChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                calSettingIsup2 = 0;
                // only send packet if this radio button is turning
                // ON since the .CheckedChanged() for the button that
                // is turning off will be called also
                anyCalSettingChanged();
            }
            else
            {
                calSettingIsup2 = 1;
            }
            
        }

        private void rbVsup2Cal1_CheckedChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                calSettingIsup2 = 1;
                // only send packet if this radio button is turning
                // ON since the .CheckedChanged() for the button that
                // is turning off will be called also
                anyCalSettingChanged();
            }
            else
            {
                calSettingIsup2 = 0;
            }

        }

        private void rbVsup1MeasRange0_CheckedChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                rangeSettingIsup1 = 0;
                // only send packet if this radio button is turning
                // ON since the .CheckedChanged() for the button that
                // is turning off will be called also
                anyCalSettingChanged();
            }
            else
            {
                rangeSettingIsup1 = 1;
            }

        }

        private void rbVsup1MeasRange1_CheckedChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                rangeSettingIsup1 = 1;
                // only send packet if this radio button is turning
                // ON since the .CheckedChanged() for the button that
                // is turning off will be called also
                anyCalSettingChanged();
            }
            else
            {
                rangeSettingIsup1 = 0;
            }

        }

        private void rbVsup2MeasRange0_CheckedChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                rangeSettingIsup2 = 0;
                // only send packet if this radio button is turning
                // ON since the .CheckedChanged() for the button that
                // is turning off will be called also
                anyCalSettingChanged();
            }
            else
            {
                rangeSettingIsup2 = 1;
            }

        }

        private void rbVsup2MeasRange1_CheckedChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                rangeSettingIsup2 = 1;
                // only send packet if this radio button is turning
                // ON since the .CheckedChanged() for the button that
                // is turning off will be called also
                anyCalSettingChanged();
            }
            else
            {
                rangeSettingIsup2 = 0;
            }

        }

        private void btnCounts_Click(object sender, EventArgs e)
        {

            m_isup1_range0 = 0;  // m = slope
            b_isup1_range0 = 0;  // b = intercept
            m_isup1_range1 = 0;  // m = slope
            b_isup1_range1 = 0;  // b = intercept

            m_isup2_range0 = 0;  // m = slope
            b_isup2_range0 = 0;  // b = intercept
            m_isup2_range1 = 0;  // m = slope
            b_isup2_range1 = 0;  // b = intercept

            // make oscope repaint and rescale
            btnOscilloscope_Click(sender, e);


        }

        private void cbVsup1Monitor_CheckedChanged(object sender, EventArgs e)
        {
            calcAcqParmsFromSettings(sender, e);
        }

        private void cbVsup2Monitor_CheckedChanged(object sender, EventArgs e)
        {
            calcAcqParmsFromSettings(sender, e);
        }

        // this prepares the file name and path for read/write
        private void initPowMonLogFileName()
        {

            if (pathOnlyToExeDir == "")
            {
                // We need a path to the place we want to create the PowMon log file
                // which for now is the dir that the .exe is in
                StringBuilder pathPlusFname = new StringBuilder(Application.ExecutablePath);

                // take off the name of the .exe file
                bool bRet = PathRemoveFileSpec(pathPlusFname);

                // save this for other functions that will need it
                pathOnlyToExeDir = pathPlusFname.ToString();
            }

            if (txtLogFilePathPlusName.Text != "")
            {
                // this box has some text already so leave it alone
                // except to change the extension if it is wrong for
                // the recording mode
                if (cbTextFileFormatting.Checked)
                {
                    if (txtLogFilePathPlusName.Text.EndsWith(".bin"))
                    {
                        // change .bin to .txt
                        int txtLen = txtLogFilePathPlusName.Text.Length - 4;
                        txtLogFilePathPlusName.Text = txtLogFilePathPlusName.Text.Substring(0, txtLen);
                        txtLogFilePathPlusName.Text += ".txt";
                    }
                }
                else
                {
                    if (txtLogFilePathPlusName.Text.EndsWith(".txt"))
                    {
                        // change .bin to .txt
                        int txtLen = txtLogFilePathPlusName.Text.Length - 4;
                        txtLogFilePathPlusName.Text = txtLogFilePathPlusName.Text.Substring(0, txtLen);
                        txtLogFilePathPlusName.Text += ".bin";
                    }

                }
                // put cursor at end of the path+fn string
                txtLogFilePathPlusName.SelectionStart = txtLogFilePathPlusName.Text.Length;

                return;
            }

            // first make this the path without the file name
            txtLogFilePathPlusName.Text = pathOnlyToExeDir;

            // then ADD the name of the logfile directory
            txtLogFilePathPlusName.AppendText(@"\LogFiles");

            // Make sure the directory exists
            if (Directory.Exists(txtLogFilePathPlusName.Text))
            {
                // no need to create
            }
            else
            {
                // Need to create dir
                try 
                {
                    Directory.CreateDirectory(txtLogFilePathPlusName.Text);
                }
                catch (Exception e)
                {
                    powMonSysMsg("\r\n");   // code meaning clear SysMsg box
                    powMonSysMsg("Error creating directory: " + e.ToString() + "\r\n");
                    // disable the Record check box
                    cbRecordToFile.Checked = false;
                    cbRecordToFile.Enabled = false;

                }
            }


            // finally ADD the name of the logfile directory
            if (cbTextFileFormatting.Checked)
            {
                txtLogFilePathPlusName.AppendText(@"\PowMonLog.txt");
            } else {
                txtLogFilePathPlusName.AppendText(@"\PowMonLog.bin");
            }

            // put cursor at end of the path+fn string
            txtLogFilePathPlusName.SelectionStart = txtLogFilePathPlusName.Text.Length;


        }

        private void whatColorNow(string whereFrom, Color passedColor)
        {

            //TODO: remove

            powMonSysMsg(
                passedColor.A.ToString() + " "
            + passedColor.B.ToString() + " "
            + passedColor.G.ToString() + " "
            + passedColor.R.ToString() + " "

            + passedColor.IsEmpty.ToString() + " "
            + passedColor.IsKnownColor.ToString() + " "
            + passedColor.IsNamedColor.ToString() + " "
            + passedColor.IsSystemColor.ToString() + " "

            + whereFrom 
            + "\r\n");

        }

        private void btnFileDialog_Click(object sender, EventArgs e)
        {


            // TODO: show this to Bill to see if he has any ideas
            // about why the button color changes
            //btnOscilloscope.BackColor = btnOscilloscope.BackColor;



            // An OpenFileDialog object was created by the form.
            // Initialize the OpenFileDialog to look for the appropriate
            // type (txt or bin files).
            if (cbTextFileFormatting.Checked)
            {
                openFileDialog1.Filter = "Text Files|*.txt";
            }
            else
            {
                openFileDialog1.Filter = "Binary Files|*.bin";
            }


            if (openFileDialog1.FileName == "")
            {
                // nothing in the filename text box yet, so suggest the .exe dir
                initPowMonLogFileName();
                // suggest this name in the dialog
                openFileDialog1.FileName = "PowMonLog.txt";
            }
            else
            {
                // use what is currently in the filename text box
                //openFileDialog1.InitialDirectory = tbCurrentToolFileName.Text;
                StringBuilder pathPlusFname = new StringBuilder(txtLogFilePathPlusName.Text);
                // take off the name of the file
                bool bRet = PathRemoveFileSpec(pathPlusFname);

                StringBuilder fnameOnly = new StringBuilder(txtLogFilePathPlusName.Text);
                // take off the path part leaving only the name
                bRet = PathStripPath(fnameOnly);



                openFileDialog1.InitialDirectory = pathPlusFname.ToString();

                // suggest the name that was already on the path
                // for the dialog filename box
                openFileDialog1.FileName = fnameOnly.ToString();                                
            }

            // allow typing a name that does not exist yet
            openFileDialog1.CheckFileExists = false;


            openFileDialog1.Title = "Choose a file to Record To or to Playback From";
 
            // Check if the user selected a file from the OpenFileDialog.

            System.Windows.Forms.DialogResult rslt;

            // note this chooses a file but does not open it
            if ((rslt = openFileDialog1.ShowDialog()) == System.Windows.Forms.DialogResult.OK)
            {

                // put new path+fn into the filename text box
                txtLogFilePathPlusName.Text = openFileDialog1.FileName;
                // put cursor at end of the path+fn string
                txtLogFilePathPlusName.SelectionStart = txtLogFilePathPlusName.Text.Length;


                // When I mouse over the rslt.ToString() above I get 
                // "this method overload is obsolete; use System.Enum.ToString()"
                // However as far as I can tell that warning only applies to the 
                // ToString() overload that passes an IFormatProvider, which is *not* 
                // the overload I am using!  See 
                // http://msdn.microsoft.com/en-us/library/system.enum.tostring(VS.85).aspx
                // if it is still there!

            }
            else // file dialog rslt not OK 
            {
                // report what dialog returned
                powMonInfoMsg(rslt.ToString()+"\r\n");   
            }

        }

        private void cbVsup1Load_CheckedChanged(object sender, EventArgs e)
        {

            if (cbVsup1Load.Checked)
            {
                // we are becoming CHECKED
                calSettingIsup1 = 1;
            }
            else
            {
                calSettingIsup1 = 0;
            }

            // send the command that changes the embedded
            anyCalSettingChanged();

        }

        private void cbVsup2Load_CheckedChanged(object sender, EventArgs e)
        {

            if (cbVsup2Load.Checked)
            {
                // we are becoming CHECKED
                calSettingIsup2 = 1;
            }
            else
            {
                calSettingIsup2 = 0;
            }

            // send the command that changes the embedded
            anyCalSettingChanged();

        }

        private void cmbSn_SelectedIndexChanged(object sender, EventArgs e)
        {
            // whenever a different SN is selected, assume we are now
            // un-caled until Load Cal is pressed
            resetToUncaledState();

        }

        private void cbContinuousAcquisition_CheckedChanged(object sender, EventArgs e)
        {
            if (cbContinuousAcquisition.Checked)
            {
                // Continuous becoming CHECKED so disable the time entry boxes
                cmbTimeToRecord.Enabled = false;
                cmbUnitsOfTime.Enabled = false;
            }
            else
            {
                // Continuous becoming UN-CHECKED: enable the time entry boxes
                cmbTimeToRecord.Enabled = true;
                cmbUnitsOfTime.Enabled = true;                
            }


        }

        private void btnClrSysMsg_Click(object sender, EventArgs e)
        {
            // clear this textbox
            txtSysMsgs.Text = "";
        }

        private void cbTextFileFormatting_CheckedChanged(object sender, EventArgs e)
        {
            // this will change the file extension in the log file path/name
            // text box if needed
            initPowMonLogFileName();
        }

        private void cmbSn_KeyDown(object sender, KeyEventArgs e)
        {
            // The code inside the combo box can think the "ENTER" 
            // is an error and so causes a beep unless we do this
            // (this: e.Handled = true; does not work)
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
            }
        }

        private void btnInfoMsgFormToggleVisible_Click(object sender, EventArgs e)
        {
            if (frmAdpInfoMsgs != null)
            {
                // if this form has been disposed with the X in the corner, then
                // we cannot get it back
                if (!frmAdpInfoMsgs.IsDisposed)
                {
                frmAdpInfoMsgs.Visible = !frmAdpInfoMsgs.Visible;
                }
            }
    
        }




    }   // class

}       // namespace
