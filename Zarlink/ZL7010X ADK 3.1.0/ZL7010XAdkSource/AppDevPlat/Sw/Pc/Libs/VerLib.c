/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

#include "Standard/String.h" /* NULL, ... */
#include "Standard/StdLib.h" /* atoi(), ... */
#include "Standard/CType.h"  /* isdigit(), isspace(), ... */
#include "Adp/General.h"     /* TRUE/FALSE, ... */
#include "Adp/Build/Build.h" /* UD32, BOOL32, ... */
#include "Adp/Pc/VerLib.h"   /* VerLib public include */

/**************************************************************************
 * Defines and Macros
 */

/**************************************************************************
 * Data Structures and Typedefs
 */

/**************************************************************************
 *
 * Global and Static Definitions
 */

/**************************************************************************
 * Function Prototypes
 */

/**************************************************************************
 * Check if an actual version is compatible with a required version. Returns
 * true if the actual version is compatible with the required version, and
 * false if not.
 *
 * The passed versions are version strings which may contain one or more
 * components (separated by semicolons). To be compatible, the actual version
 * string must contain a compatible component for each component specified in
 * the required version string.
 *
 * Each component consists of a name, followed by a version number (optional),
 * followed by a tag (optional). The version number has the form
 * <Major>.<Minor>.<Patch> (the patch number is optional). Note that a tag
 * can only be specified if a version number is specified.
 *
 * For two components to be considered compatible, their names and major
 * versions must match, and the required minor version must be less than the
 * actual minor version (in newer minor versions, all interfaces should remain
 * backwards compatible with previous minor versions). Any patch number and/or
 * tag that are specified don't affect compatibility.
 */
BOOL32
VerIsCompat(const char *actualVer, const char *requiredVer)
{
    UD32 acNameLen, acMajor, acMinor, acPatch, acTagLen;
    UD32 rcNameLen, rcMajor, rcMinor, rcPatch, rcTagLen;
    const char *ac, *nextAc, *acTag;
    const char *rc, *nextRc, *rcTag;

    /* For each required component found in the required version string, search
     * for a compatible component in the actual version string.
     */
    for(rc = requiredVer; rc != NULL; rc = nextRc) {

        /* parse this required component & get the next */
        nextRc = VerParse(rc, &rcNameLen, &rcMajor, &rcMinor, &rcPatch,
            &rcTag, &rcTagLen);

        /* search for a compatible component in the actual version string */
        for(ac = actualVer; ac != NULL; ac = nextAc) {

            /* parse actual component & get next */
            nextAc = VerParse(ac, &acNameLen, &acMajor, &acMinor, &acPatch,
                &acTag, &acTagLen);

            /* If the actual component isn't compatible with the required
             * component, continue searching. For a description of how
             * compatibility is determined, see the function prolog.
             */
            if (acNameLen != rcNameLen) continue;
            if (strncmp(ac, rc, acNameLen) != 0) continue;
            if (acMajor != rcMajor) continue;
            if (acMinor < rcMinor) continue;

            /* stop searching (found compatible component) */
            break;
        }

        /* If we couldn't find a component in the actual version string that is
         * compatible with this component in the required version string, assume
         * the actual version isn't compatible with the required version.
         */
        if (ac == NULL) return(FALSE);
    }

    /* If we get here, we found a compatible component in the actual version
     * string for every component in the required version string, so assume the
     * actual version is compatible with the required version.
     */
    return(TRUE);
}

const char *
VerParse(const char *ver, UD32 *nameLen, UD32 *major, UD32 *minor, UD32 *patch,
    const char **tag, UD32 *tagLen)
{
    const char *p, *pTemp;

    /* set default return values */
    *nameLen = 0;
    *major = 0;
    *minor = 0;
    *patch = 0;
    *tag = ver;
    *tagLen = 0;

    /* if version is NULL or empty, return NULL (no components in string) */
    if ((ver == NULL) || (*ver == '\0')) return(NULL);

    /* search the first component in the string for "<major>.<minor>" */
    for(p = &ver[1];; ++p) {

        /* If we reached the end of the component, it must not contain any
         * "<major>.<minor>". Thus, set the component name and tag lengths,
         * skip any spaces, then return a pointer to next component in the
         * version string.
         */
        if (*p == ';') {
            *nameLen = (UD32)(p - ver);
            *tagLen = 0;
            *tag = p;
            while(isspace(*++p));
            return(p);
        }
        /* If we reached the end of the version string, it must contain only
         * one component, and that component must not contain "<major>.<minor>".
         * Thus, set the component name and tag lengths, then return NULL to
         * indicate there are no more components in the version string.
         */
        if (*p == '\0') {
            *nameLen = (UD32)(p - ver);
            *tagLen = 0;
            *tag = p;
            return(NULL);
        }

        /* if char is '.' surrounded by digits, found "<major>.<minor>" */
        if ((*p == '.') && isdigit(p[-1]) && isdigit(p[1])) break;
    }
    *minor = atoi(&p[1]);

    /* search backwards for beginning of major version */
    for(pTemp = p;;) {
        if (--pTemp == ver) break;
        if (!isdigit(pTemp[-1])) break;
    }
    *major = atoi(pTemp);

    /* calc length of name in component (all chars before major version) */
    *nameLen = (UD32)(pTemp - ver);

    /* skip over minor version to beginning of patch version (if any) */
    #pragma warning(disable:4127) /* don't warn about while(1) */
    while(1) {
        ++p;
        if (*p == '.') {
            *patch = atoi(++p);
            break;
        }
        if (!isdigit(*p)) break;
    }
    #pragma warning(default:4127)

    /* skip over patch version (if any) and any spaces that follow it */
    while(isdigit(*p)) ++p;
    while(isspace(*p)) ++p;

    /* Save pointer to tag, search for end of tag (i.e. end of component or
     * string), then set tag length (will be 0 if there's no tag).
     */
    *tag = p;
    while((*p != ';') && (*p != '\0')) ++p;
    *tagLen = (UD32)(p - *tag);

    /* if there's no more components in version string, return NULL */
    if (*p == '\0') return(NULL);

    /* skip any spaces, then return pointer to next component in version string */
    while(isspace(*++p));
    return(p);
}
