/**************************************************************************
 * FILE: ErrInfoLib.c
 *
 * This file contains functions for the "error information library", which are
 * used to pass error information up function call chains.
 *
 * NOTES:
 *     None.
 *
 * VISIBILITY: Customer
 *
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

#include "Standard/String.h"   /* memset(), NULL, ... */
#include "Standard/StdLib.h"   /* calloc(), ... */
#include "Standard/Malloc.h"   /* calloc(), ... */
#include "Standard/StdIo.h"    /* vsnprintf(), ... */
#include "Standard/StdArg.h"   /* va_list, ... */
#include "Adp/General.h"       /* RSTAT, BOOL, TRUE/FALSE, ... */
#include "Adp/Build/Build.h"   /* UD32, BOOL32, ... */
#include "Adp/Pc/ErrInfoLib.h" /* ErrInfoLib public include */

/**************************************************************************
 * Defines and Macros
 */

/* magic number used to validate error information (ascii "EI  ") */
#define EI_MAGIC  0x45492020

/* strings returned by EiGetId() & EiGetMsg() when no error is available */
#define EI_NOT_INIT_MSG "[No error information is available (not initialized).]"
#define EI_NOT_SET_MSG "[No error information is available (not set).]"

/**************************************************************************
 * Data Structures and Typedefs
 */

/* Structure of private data for error information library.
 */
typedef struct {

    /* magic number used to validate data structure */
    volatile UD32 magic;

    /* error ID string (format "Group.Key:DefaultMessage") */
    char eid[512];

    /* argument(s) to insert into message (separated by '\f' if more than 1) */
    char args[512];

    /* length of group in error ID string (0 if none) */
    UINT groupLen;

    /* length of group and key specified in error ID string (0 if none) */
    UINT groupAndKeyLen;

    /* pointer to beginning of default message in error ID string */
    char *defMsg;

    /* completed error message (with arguments inserted) */
    char msg[1024];

} EI_PRIV;

/**************************************************************************
 *
 * Global and Static Definitions
 */

/**************************************************************************
 * Function Prototypes
 */

static void EiInsertArgs(const char *msg, const char *msgArgs,
    char *buf, UD32 bufSize);

/**************************************************************************
 * FUNCTION: EiInitSpecs
 *
 * This function initializes a EI_SPECS structure with default values. The
 * structure may then be modified as desired and passed to EiOpen().
 *
 * PARAMETERS:
 *     specs:
 *         The EI_SPECS structure to initialize. EI_SPECS is defined in
 *         "AppDevPlat\Sw\Includes\Adp\Pc\ErrInfoLib.h".
 *     specsSize:
 *         The size of the specified EI_SPECS structure (bytes). Note
 *         EiInitSpecs() won't write to anything in the structure beyond the
 *         specified size. That way, if new fields are added to the end of
 *         EI_SPECS, this function will remain compatible with older
 *         applications that don't use the new fields.
 *
 * RETURN VALUES:
 *     None.
 *
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate EI_SPECS structure.
 *
 * VISIBILITY: Customer
 */
void
EiInitSpecs(EI_SPECS *specs, UD32 specsSize)
{
    EI_SPECS defSpecs;

    /* init default specs with default values for everything */
    (void)memset(&defSpecs, 0, sizeof(defSpecs));
    defSpecs.magic = EI_MAGIC;

    /* copy default specs into caller's specs (up to specified size) */
    if (specsSize <= sizeof(defSpecs)) {
        (void)memcpy(specs, &defSpecs, specsSize);
    } else {
        (void)memset(specs, 0, specsSize);
        (void)memcpy(specs, &defSpecs, sizeof(defSpecs));
    }
}

/**************************************************************************
 * FUNCTION: EiOpen
 *
 * This function opens an error information object (EI) that can be used to
 * pass error information back from failed functions.
 *
 * Note for multi-threaded applications, each thread should use its own
 * separate error object to pass error information, so if more than one thread
 * detects an error at the same time, they won't conflict (i.e. the error for
 * one thread won't overwrite the error for another thread).
 *
 * PARAMETERS:
 *     specs:
 *         Specifications for the error object (see EI_SPECS in "AppDevPlat\
 *         Sw\Includes\Adp\Pc\ErrInfoLib.h"), or NULL to use defaults. Note
 *         before passing an EI_SPECS structure to EiOpen(), it must be
 *         initialized using EiInitSpecs() and modified as desired.
 *     specsSize:
 *         The size of the specified EI_SPECS structure (bytes). Note EiOpen()
 *         won't reference anything in the structure beyond the specified size.
 *         That way, if new fields are added to the end of EI_SPECS, this
 *         function will remain compatible with older applications that don't
 *         use the new fields.
 *
 * RETURN VALUES:
 *     If successful, this function returns a non-NULL ID for the error object.
 *     Otherwise, it returns NULL (unable to allocate memory).
 *
 * THREAD SAFE:
 *     Multiple threads may call this function. Note each thread should use
 *     its own separate error object to pass error information, so if more than
 *     one thread detects an error at the same time, they won't conflict (i.e.
 *     the error for one thread won't overwrite the error for another thread).
 *
 * VISIBILITY: Customer
 */
EI
EiOpen(const EI_SPECS *specs, UD32 specsSize)
{
    EI_SPECS specsBuf;
    EI_PRIV *ei;

    /* init resources referenced in error cleanup */
    ei = NULL;

    /* Init local specs buf with default values, then overwrite the defaults
     * with the passed specs (up to the specified size). That way, if fields
     * are added to the specs in the future, and the calling application isn't
     * updated, the new fields will still have valid default values.
     */
    EiInitSpecs(&specsBuf, sizeof(specsBuf));
    if (specs != NULL) {

        /* if passed specs not initialized by EiInitSpecs() or too big, fail */
        if ((specs->magic != EI_MAGIC) || (specsSize > sizeof(specsBuf))) {
            goto error;
        }
        (void)memcpy(&specsBuf, specs, specsSize);
    }
    specs = &specsBuf;

    /* Allocate memory for private data and clear all resources in it so
     * the free function can check if each resource was allocated before trying
     * to free it. This way the free function can be called to clean up if
     * an error is detected during the remainder of the creation.
     */
    if ((ei = calloc(1, sizeof(*ei))) == NULL) goto error;
    ei->magic = EI_MAGIC;
    ei->defMsg = ei->eid;  /* ensure default message pointer is never NULL */

    return((EI)ei);

error:
    /* free any resources allocated up to where the error occurred */
    if (ei != NULL) (void)EiClose((EI)ei);

    return(NULL);
}

/**************************************************************************
 * FUNCTION: EiClose
 *
 * This function closes an error information object that was opened by EiOpen(),
 * freeing any memory and other resources allocated for it.
 *
 * PARAMETERS:
 *     e:
 *         The error object to close (an ID returned by EiOpen()).
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it returns -1
 *     (invalid error object, or error object already closed).
 *
 * THREAD SAFE:
 *     Multiple threads may call this function. Note each error object should
 *     only be closed once by a single thread, and after it's closed, it should
 *     no longer be used (if it is, the called function will detect it and do
 *     nothing).
 *
 * VISIBILITY: Customer
 */
RSTAT
EiClose(EI e)
{
    EI_PRIV *ei = (EI_PRIV *)e;
    RSTAT rStat = 0;

    /* if specified error info is invalid, don't try to close it */
    if ((ei == NULL) || (ei->magic != EI_MAGIC)) return(-1);

    /* Invalidate the magic number so we'll detect any attempt to use the ID
     * after it has been closed.
     */
    ei->magic = 0;
    free(ei);
    return(rStat);
}

/**************************************************************************
 * FUNCTION: EiSet
 *
 * This function sets the error information in the specified error object.
 *
 * PARAMETERS:
 *     e:
 *         An error object returned by EiOpen().
 *     eid:
 *         An error ID string (format "Group.Key:DefaultMessage").
 *     argFormats:
 *         A string containing a conversion specification for each of the
 *         arguments to be inserted into the error message (NULL for none).
 *         This string should not contain any text except the conversion
 *         specifications, with each conversion specification separated from
 *         the others by a single '\f' character (form feed). For example, for
 *         a string argument and an integer argument, argFormats could be
 *         "%s\f%d". Internally, argFormats and the remaining arguments are
 *         passed to vsnprintf() to build a string containing the formatted
 *         arguments (the "arguments string"). The length of the resulting
 *         string and terminating '\0' should not exceed 512 bytes. If it does,
 *         it won't corrupt memory or cause catastrophic problems, but the
 *         resulting arguments string will be truncated.
 *     ...:
 *         The arguments to insert into the error message. Each argument is
 *         converted according to the conversion specification specified for
 *         the argument in "argFormats". Note that the correct type of argument
 *         must be passed for each conversion specification in "argFormats"
 *         (otherwise, the behavior of vsnprintf() is undefined). If and when
 *         EiGetMsg() is called to get the default message for the error, it
 *         will insert the first argument where {0} appears in the message,
 *         the second where {1} appears, and so on. If you need to include a
 *         curly brace in a message without inserting an argument, you must
 *         specify two consecutive curly braces ("{{" or "}}"), which will
 *         be converted to a single curly brace in the resulting message.
 *
 * RETURN VALUES:
 *     None.
 *
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object, so if more than one thread detects an
 *     error at the same time, they won't conflict (i.e. the error for one
 *     thread won't overwrite the error for another thread).
 *
 * VISIBILITY: Customer
 */
void
EiSet(EI e, const char *eid, const char *argFormats, ...)
{
    va_list va;

    /* init variable arguments list */
    va_start(va, argFormats);

    EiSetVa(e, eid, argFormats, va);

    /* clean up variable args list */
    va_end(va);
}

/**************************************************************************
 * FUNCTION: EiSetVa
 *
 * This function sets the error information in the specified error object.
 * This is the same as EiSet(), except it takes a va_list argument instead
 * of variable arguments.
 *
 * PARAMETERS:
 *     e:
 *         An error object returned by EiOpen().
 *     eid:
 *         An error ID string (format "Group.Key:DefaultMessage").
 *     argFormats:
 *         A string containing a conversion specification for each of the
 *         arguments to be inserted into the error message (NULL for none).
 *         See EiSet() for details.
 *     va:
 *         The va_list for the arguments to insert into the error message. See
 *         EiSet() for how the arguments are processed and inserted into the
 *         error message.
 *
 * RETURN VALUES:
 *     None.
 *
 * THREAD SAFE:
 *     Same as EiSet().
 *
 * VISIBILITY: Customer
 */
void
EiSetVa(EI e, const char *eid, const char *argFormats, va_list va)
{
    EI_PRIV *ei = (EI_PRIV *)e;
    char *p;

    /* if specified error info is invalid, do nothing */
    if ((ei == NULL) || (ei->magic != EI_MAGIC)) return;

    /* save error ID string and make sure it's terminated */
    (void)strncpy(ei->eid, eid, sizeof(ei->eid));
    ei->eid[sizeof(ei->eid) - 1] = '\0';

    /* get pointer to default message in error ID string */
    if ((p = strchr(ei->eid, ':')) != NULL) {

        /* save pointer to default message (ignore any leading spaces) */
        for(ei->defMsg = p + 1; ei->defMsg[0] == ' '; ++ei->defMsg);

        /* save length of group and key */
        ei->groupAndKeyLen = (UINT)(p - ei->eid);

        /* save length of group only */
        while((*p != '.') && (p != ei->eid)) --p;
        ei->groupLen = (UINT)(p - ei->eid);

    } else {
        ei->defMsg = ei->eid;
        ei->groupAndKeyLen = 0;
        ei->groupLen = 0;
    }

    /* if there are arguments to insert into the error message */
    if (argFormats != NULL) {

        /* Build arguments string and make sure it's terminated (just in case
         * vsnprintf() doesn't terminate it). Note the buffer size minus 1 is
         * passed to vsnprintf() instead of the full buffer size to ensure
         * it doesn't write beyond the end of the buffer.
         */
        (void)vsnprintf(ei->args, sizeof(ei->args) - 1, argFormats, va);
        ei->args[sizeof(ei->args) - 1] = '\0';

    } else { /* else there are no arguments to insert into the error message */

        /* clear arguments string */
        ei->args[0] = '\0';
    }
}

/**************************************************************************
 * FUNCTION: EiErrIs
 *
 * This function checks if the specified error object (e) contains the
 * specified error (eid). Note if the specified error object (e) contains
 * an error group and key, this will only compare the error group and key
 * (not the default message). Otherwise, it assumes the error ID strings
 * don't contain a group and key, so it compares the whole passed string.
 *
 * PARAMETERS:
 *     e:
 *         An error object returned by EiOpen().
 *     eid:
 *         An error ID string (format "Group.Key:DefaultMessage").
 *
 * RETURN VALUES:
 *     If the specified error object (e) contains the specified error (eid),
 *     this function returns TRUE. Otherwise, it returns FALSE.
 *
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 *
 * VISIBILITY: Customer
 */
BOOL32
EiErrIs(EI e, const char *eid)
{
    EI_PRIV *ei = (EI_PRIV *)e;
    size_t compareLen;

    /* if specified error info is invalid, just return false */
    if ((ei == NULL) || (ei->magic != EI_MAGIC)) return(FALSE);

    /* If ei->eid contains an error group and key, set length to compare only
     * the error group and key. Otherwise, set length to compare the whole
     * passed string.
     */
    if (ei->groupAndKeyLen) {
        compareLen = ei->groupAndKeyLen;
    } else {
        compareLen = strlen(eid);
    }

    /* if the errors match, return true */
    if (strncmp(eid, ei->eid, compareLen) == 0) return(TRUE);

    /* return false (errors didn't match) */
    return(FALSE);
}

/**************************************************************************
 * FUNCTION: EiGroupIs
 *
 * This function checks if the specified error object (e) contains an error in
 * the specified error group (group). Note if the specified error object (e)
 * contains an error group, this will only compare the error group (not the
 * key or the default message). Otherwise, it assumes the error ID strings
 * don't contain a group, so it compares the whole passed string.
 *
 * PARAMETERS:
 *     e:
 *         An error object returned by EiOpen().
 *     group:
 *         The name of the error group. If desired, this can also be a full
 *         error ID string (format "Group.Key:DefaultMessage"), but only the
 *         group will be compared.
 *
 * RETURN VALUES:
 *     If the specified error object contains an error in the specified group,
 *     this function returns TRUE. Otherwise, it returns FALSE.
 *
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 *
 * VISIBILITY: Customer
 */
BOOL32
EiGroupIs(EI e, const char *group)
{
    EI_PRIV *ei = (EI_PRIV *)e;
    size_t compareLen;

    /* if specified error info is invalid, just return false */
    if ((ei == NULL) || (ei->magic != EI_MAGIC)) return(FALSE);

    /* If ei->eid contains an error group, set length to compare only the error
     * group. Otherwise, set length to compare the whole passed string.
     */
    if (ei->groupLen) {
        compareLen = ei->groupLen;
    } else {
        compareLen = strlen(group);
    }

    /* if the error groups match, return true */
    if (strncmp(group, ei->eid, compareLen) == 0) return(TRUE);

    /* return false (error groups didn't match) */
    return(FALSE);
}

/**************************************************************************
 * FUNCTION: EiGetId
 *
 * This function gets the error ID string contained in the specified error
 * object. The format of the string is "Group.Key:DefaultMessage".
 *
 * PARAMETERS:
 *     e:
 *         An error object returned by EiOpen().
 *
 * RETURN VALUES:
 *     This function returns the error ID string contained in the specified
 *     error object. If the specified error object is NULL or invalid, or does
 *     not yet contain an error, this function will return a string stating no
 *     error information is available (it never returns NULL).
 *
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 *
 * VISIBILITY: Customer
 */
const char *
EiGetId(EI e)
{
    EI_PRIV *ei = (EI_PRIV *)e;

    /* if specified error info is invalid, return default message */
    if ((ei == NULL) || (ei->magic != EI_MAGIC)) return(EI_NOT_INIT_MSG);

    /* if error info isn't set, return default message */
    if (ei->eid[0] == '\0') return(EI_NOT_SET_MSG);

    return(ei->eid);
}

/**************************************************************************
 * FUNCTION: EiGetArgs
 *
 * This function gets the arguments string contained in the specified error
 * object (a string containing the arguments to insert into the error message).
 * If there are multiple arguments in the string, they are separated by a '\f'
 * (form feed). If there are no arguments to insert, the arguments string is
 * an empty string.
 *
 * PARAMETERS:
 *     e:
 *         An error object returned by EiOpen().
 *
 * RETURN VALUES:
 *     This function returns the arguments string contained in the specified
 *     error object. If the specified error object is NULL or invalid, or
 *     there are no arguments to insert, this function will return an empty
 *     string (it never returns NULL).
 *
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 *
 * VISIBILITY: Customer
 */
const char *
EiGetArgs(EI e)
{
    EI_PRIV *ei = (EI_PRIV *)e;

    /* if specified error info is invalid, return empty arguments string */
    if ((ei == NULL) || (ei->magic != EI_MAGIC)) return("");

    return(ei->args);
}

/**************************************************************************
 * FUNCTION: EiGetMsg
 *
 * This function gets the default message for the error contained in the
 * specified error object. If the error object contains arguments to insert
 * into the message, this inserts them. The first argument is inserted where
 * {0} appears in the message, the second where {1} appears, and so on. If the
 * message expects an argument that's not available, a string stating this is
 * inserted instead.
 *
 * PARAMETERS:
 *     e:
 *         An error object returned by EiOpen().
 *
 * RETURN VALUES:
 *     This function returns the default error message for the error contained
 *     in the specified error object. If the specified error object is NULL or
 *     invalid, or does not yet contain an error, this function will return a
 *     string stating no error information is available (it never returns NULL).
 *
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 *
 * VISIBILITY: Customer
 */
const char *
EiGetMsg(EI e)
{
    EI_PRIV *ei = (EI_PRIV *)e;

    /* if specified error info is invalid, return default message */
    if ((ei == NULL) || (ei->magic != EI_MAGIC)) return(EI_NOT_INIT_MSG);

    /* if error info isn't set, return default message */
    if (ei->eid[0] == '\0') return(EI_NOT_SET_MSG);

    /* build message (i.e. insert any args into message) */
    EiInsertArgs(ei->defMsg, ei->args, ei->msg, sizeof(ei->msg));

    /* return error message */
    return(ei->msg);
}

/* This inserts any arguments contained in the specified arguments string
 * (msgArgs) into the specified message (msg), and returns the resulting
 * message in the specified buffer (buf). If there are multiple arguments in
 * the arguments string, they must be separated by a '\f' (form feed)). If
 * there are no arguments, the arguments string can be NULL or an empty string.
 * The first argument is inserted where {0} appears in the message, the second
 * where {1} appears, and so on. If the message expects an argument that's not
 * specified in the arguments string, a string stating this will be inserted
 * instead. If you need to include a curly brace in a message without inserting
 * an argument, you must specify two consecutive curly braces ("{{" or "}}"),
 * which will be converted to a single curly brace in the resulting message.
 */
static void
EiInsertArgs(const char *msg, const char *msgArgs, char *buf, UD32 bufSize)
{
    char *end = &buf[bufSize - 1];
    int numNestedBraces = 0;
    const char *in = msg;
    char *out = buf;
    const char *arg;
    int i, c, argNum;

    /* for each char in string */
    for(;;) {

        /* get char */
        if ((c = *in++) == '\0') break;

        /* if it's a double curly brace ("{{" or "}}"), output one */
        if (((c == '{') && (*in == '{')) || ((c == '}') && (*in == '}'))) {

            if (out == end) break;
            *out++ = (char)c;
            ++in;

        /* if it's an opening brace, assume it begins an argument insertion */
        } else if (c == '{') {

            /* get argument number (the n in the {n,...} sequence) */
            argNum = atoi(in);

            /* ignore anything else in the {n,...} sequence */
            for(;;) {

                if ((c = *in++) == '\0') break;

                if (c == '{') {

                    ++numNestedBraces;

                } else if (c == '}') {

                    if (numNestedBraces == 0) break;
                    --numNestedBraces;
                }
            }
            if (c == '\0') break;

            /* if the specified argument string contains any arguments */
            if ((arg = msgArgs) != NULL) {

                /* find argument to insert at this point in output string */
                for(i = 0; i != argNum; arg++) {

                    c = *arg;

                    if (c == '\0') break;

                    /* if form feed char, it's the start of the next arg */
                    if (c == '\f') ++i;
                }

                /* if argument wasn't found, set to NULL */
                if (*arg == '\0') arg = NULL;
            }

            /* if argument isn't available, use default */
            if (arg == NULL) arg = "[NoArgumentWasAvailableToInsert]";

            /* insert argument into output string */
            for(;;) {

                c = *arg++;
                if ((c == '\0') || (c == '\f')) break;
                if (out == end) break;
                *out++ = (char)c;
            }

        } else {  /* else, copy char to output string */

            if (out == end) break;
            *out++ = (char)c;
        }
    }

    /* terminate output string */
    *out = '\0';
}
