/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

#include <Windows.h>                 /* CreateMutex(), also for FTD2XX.h */
#pragma warning(disable:4214)
#include "Ftdi/FTD2XX.h"             /* FT_Write(), FT_Read(), ... */
#pragma warning(default:4214)
#include "Standard/String.h"         /* memset(), NULL, ... */
#include "Standard/StdLib.h"         /* calloc(), ... */
#include "Standard/Malloc.h"         /* calloc(), ... */
#include "Standard/StdIo.h"          /* _snprintf() */
#include "Adp/General.h"             /* RSTAT, BOOL, ... */
#include "Adp/Build/Build.h"         /* UD32, ... */
#include "Adp/AnyBoard/Com.h"        /* COM_PACK_HDR, ... */
#include "Adp/AdpBoard/AdpGeneral.h" /* ADP_DEV_USB_ID, ADP_DEV_TYPE */
#include "Adp/Pc/ErrInfoLib.h"       /* EI, EiSet() */
#include "Adp/Pc/ComLib.h"           /* ComLib public include */
 
/**************************************************************************
 * Defines and Macros
 */
 
/* magic number used to validate data structures (ascii "COM ") */
#define COM_MAGIC  0x434F4D20

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* Structure of data private to each instance of the communication library.
 */
typedef struct {
    
    /* magic number used to validate data structure */
    volatile UD32 magic;
    
    /* connection to the ADP board for this communication interface */
    FT_HANDLE ftId;
    
    /* Semaphore used to ensure only one thread at a time uses this
     * communication interface to send a command to a device and receive the
     * reply from the device (via the ADP board). Otherwise, the commands and
     * replies for different threads could conflict.
     */
    HANDLE sem;
    
    /* default timeout (millisecs) for sending commands & receiving replies */
    UD32 defTimeoutMs;
    
} COM_PRIV;

/**************************************************************************
 * 
 * Global and Static Definitions
 */
 
/**************************************************************************
 * Function Prototypes
 */
 
static RSTAT ComSendCmd(COM_ID comId, UD32 devAddr, UD32 cmdType,
    const void *cmd, UD32 cmdLen, EI e);
    
static SD32 ComReceiveReply(COM_ID comId, void *repBuf, UD32 repBufSize,
    EI e);
    
static void ComReceiveAndDiscard(COM_ID comId, UD32 len, EI e);
    
static RSTAT ComSend(COM_ID comId, const void *data, UD32 len, EI e);
    
static RSTAT ComReceive(COM_ID comId, void *buf, UD32 len, EI e);

static RSTAT ComWait(COM_ID comId, UD32 timeoutMs, EI e);
static RSTAT ComRelease(COM_ID comId, EI e);

static HANDLE ComCreateSem(const char *semName, EI e);
static RSTAT ComWaitSem(HANDLE sem, UD32 timeoutMs, EI e);
static RSTAT ComReleaseSem(HANDLE sem, EI e);

static void ComErrReplyToErrInfo(const char *errRep, UD32 errRepLen, EI e);

static RSTAT ComSizeErr(EI e);
static RSTAT ComIdErr(EI e);
 
/**************************************************************************
 * Initialize a COM_SPECS structure. The structure may then be modified as
 * required and passed to ComOpen().
 */
void
ComInitSpecs(COM_SPECS *specs, UD32 specsSize)
{
    COM_SPECS defSpecs;
    
    /* init default specs with default values for everything */
    (void)memset(&defSpecs, 0, sizeof(defSpecs));
    defSpecs.magic = COM_MAGIC;
    defSpecs.defTimeoutMs = COM_DEF_TIMEOUT_MS;
    
    /* copy default specs into caller's specs (up to specified size) */
    if (specsSize <= sizeof(defSpecs)) {
        (void)memcpy(specs, &defSpecs, specsSize);
    } else {
        (void)memset(specs, 0, specsSize);
        (void)memcpy(specs, &defSpecs, sizeof(defSpecs));
    }
}
 
COM_ID
ComOpen(const COM_SPECS *specs, UD32 specsSize, EI e)
{
    FT_DEVICE_LIST_INFO_NODE *ftDevList, *ftDev;
    char devTypeBuf[COM_DEV_TYPE_BUF_SIZE];
    char devNameBuf[COM_DEV_NAME_BUF_SIZE];
    UD32 i, size, nFtDevs;
    BOOL releaseFtDevListSem;
    HANDLE ftDevListSem;
    COM_SPECS specsBuf;
    FT_STATUS ftStat;
    COM_PRIV *com;
    COM_ID comId;
    
    /* init variables referenced in goto for error/cleanup */
    com = NULL;
    comId = NULL;
    ftDevList = NULL;
    ftDevListSem = NULL;
    releaseFtDevListSem = FALSE;
    
    /* Init local specs buf with default values, then overwrite the defaults
     * with the passed specs (up to the specified size). That way, if fields
     * are added to the specs in the future, and the calling application isn't
     * updated, the new fields will still have valid default values.
     */
    ComInitSpecs(&specsBuf, sizeof(specsBuf));
    if (specs != NULL) {
        
        /* if passed specs not initialized by ComInitSpecs() or too big, fail */
        if ((specs->magic != COM_MAGIC) || (specsSize > sizeof(specsBuf))) {
            EiSet(e, COM_LIB_EID_INVALID_SPECS, NULL);
            goto error;
        }
        (void)memcpy(&specsBuf, specs, specsSize);
    }
    specs = &specsBuf;
    
    /* Allocate memory for private data and clear all resources in it so 
     * ComClose() can check if each resource was allocated before trying
     * to free it (so it can be called to clean up if an error occurs).
     */
    if ((com = calloc(1, sizeof(*com))) == NULL) {
        EiSet(e, COM_LIB_EID_FAILED_TO_ALLOCATE_MEM, "%d", sizeof(*com));
        goto error;
    }
    com->magic = COM_MAGIC;
    comId = (COM_ID)com;
    
    /* create semaphore for this communication interface (see COM_PRIV.sem) */
    if ((com->sem = ComCreateSem(NULL, e)) == NULL) goto error;
    
    /* Open handle for FTDI device list semaphore (creates semaphore if it
     * doesn't exist), then wait for exclusive access to the list. This ensures
     * only one program at a time will open the FTDI device list and search it
     * for ADP boards, querying the mezzanine board attached to each ADP board
     * to see if it's the desired one. This is needed because while one program
     * has an ADP board open, that ADP board won't appear in the device list
     * for any other programs, so if two programs are searching at the same
     * time, one program could hide an ADP board from the other program, even
     * though the first program may not keep that ADP board open (wrong
     * mezzanine board). This probably wouldn't happen often since different
     * programs won't usually search at the same time, but this semaphore
     * ensures it will never happen.
     * 
     * This semaphore also ensures the FTDI functions are only called by one
     * program or thread at a time, in case they aren't thread-safe.
     */
    if ((ftDevListSem = ComCreateSem("ZarlinkComLibFtDevListSem", e)) == NULL){
        goto error;
    }
    if (ComWaitSem(ftDevListSem, 10000, e)) goto error;
    releaseFtDevListSem = TRUE;
    
    /* create FTDI device list get number of devices in it */
    if ((ftStat = FT_CreateDeviceInfoList((LPDWORD)&nFtDevs)) != FT_OK) {
        EiSet(e, COM_LIB_EID_FAILED_FT_CREATE_DEV_INFO_LIST, "%d", ftStat);
        goto error;
    }
    /* allocate memory to hold FTDI device list */
    size = nFtDevs * sizeof(*ftDevList);
    if ((ftDevList = malloc(size)) == NULL) {
        EiSet(e, COM_LIB_EID_FAILED_TO_ALLOCATE_MEM, "%d", size);
        goto error;
    }
    /* get FTDI device list */
    if ((ftStat = FT_GetDeviceInfoList(ftDevList, (LPDWORD)&nFtDevs)) != FT_OK) {
        EiSet(e, COM_LIB_EID_FAILED_FT_GET_DEV_INFO_LIST, "%d", ftStat);
        goto error;
    }
    /* search FTDI device list for the desired ADP board */
    for(i = 0;; ++i) {
        
        /* if opened connection to ADP board, close it (wrong ADP board) */
        if (com->ftId != NULL) (void)FT_Close(com->ftId);
        com->ftId = NULL;
        
        /* if no more FTDI devices, stop (couldn't find desired ADP board) */
        if (i >= nFtDevs) break;
        
        /* get pointer to next device in FTDI device list */
        ftDev = &ftDevList[i];
        
        /* If the device's USB ID or description (type) != ADP board, skip it.
         * Note this also checks for type "Zarlink ADP Board" so it will work
         * with older ADP boards (before Zarlink changed to Microsemi).
         */
        if (ftDev->ID != ADP_DEV_USB_ID) continue;
        if ((strcmp(ftDev->Description, ADP_DEV_TYPE) != 0) &&
            (strcmp(ftDev->Description, "Zarlink ADP Board") != 0)) {
            continue;
        }

        /* If the device is open, something must already have a communication
         * interface open for this ADP board, so skip it (only one communcation
         * interface can be open for each ADP board).
         */
        if (ftDev->ftHandle != NULL) continue;
            
        /* open connection to this ADP board (sets com->ftId) */
        if (FT_Open(i, &com->ftId) != FT_OK) continue;
        
        /* if no device type specified, stop search (found 1st ADP board) */
        if (specs->localMezzDevType == NULL) break;

        /* set default timeout to 2 sec for now (to query device type & name) */
        if (ComSetDefTimeout(comId, 2000, e) != FT_OK) continue;

        /* get device type from mezzanine board & if it doesn't match, skip it */
        if (ComGetDevType(comId, COM_LOCAL_MEZZ_ADDR, devTypeBuf,
            sizeof(devTypeBuf), e)) {
            continue;
        }
        if (strcmp(devTypeBuf, specs->localMezzDevType) != 0) continue;
                
        /* if no device name specified, stop search (found desired ADP board) */
        if (specs->localMezzDevName == NULL) break;
        
        /* get device name from mezzanine board & if it doesn't match, skip it */
        if (ComGetDevName(comId, COM_LOCAL_MEZZ_ADDR, devNameBuf,
            sizeof(devNameBuf), e)) {
            continue;
        }
        if (strcmp(devNameBuf, specs->localMezzDevName) != 0) continue;
        
        /* stop searching (found desired ADP board) */
        break;
    }
    
    /* if desired ADP board not found, set error info and fail */
    if (com->ftId == NULL) {
        
        if ((specs->localMezzDevType != NULL) && (specs->localMezzDevName != NULL)) {

            EiSet(e, COM_LIB_EID_COULD_NOT_FIND_NAMED_DEV, "%s\f%s",
                specs->localMezzDevType, specs->localMezzDevName);

        } else if (specs->localMezzDevType != NULL) {

            EiSet(e, COM_LIB_EID_COULD_NOT_FIND_DEV, "%s", specs->localMezzDevType);

        } else {

            EiSet(e, COM_LIB_EID_COULD_NOT_FIND_DEV, "%s", ADP_DEV_TYPE);
        }
        goto error;
    }
    
    /* set default timeout to specified value (milliseconds) */
    if (ComSetDefTimeout(comId, specs->defTimeoutMs, e)) goto error;
    
    /* Set the FTDI USB latency timer to 2 ms. This determines the amount of
     * time the USB chip on the ADP board will keep a reply in its TX buffer
     * before it's transmitted to the PC, which affects the maximum
     * command/reply rate. The default is 16 ms, which limits the command/reply
     * rate to ~62 per second. For 2 ms, the maximum command/reply rate is ~500
     * per second (for small command and reply packets).
     */
    if ((ftStat = FT_SetLatencyTimer(com->ftId, 2)) != FT_OK) {
        EiSet(e, COM_LIB_EID_FAILED_FT_SET_LATENCY_TIMER, "%d", ftStat);
        goto error;
    }
    
    /* Set the FTDI USB flow control. This is a work-around from FTDI for a
     * FT245R issue. This ensures the TXE# pin will always remain high when the
     * FT245R's TX buffer is full. We haven't encountered this, possibly
     * because we use a command/reply protocol with relatively small packets.
     * Nevertheless, do this anyway to ensure it never happens.
     */
    if ((ftStat = FT_SetFlowControl(com->ftId, FT_FLOW_RTS_CTS, 0, 0)) != FT_OK) {
        EiSet(e, COM_LIB_EID_FAILED_FT_SET_FLOW_CONTROL, "%d", ftStat);
        goto error;
    }
    
    /* go cleanup and return id */
    goto cleanup;
    
error:    
    /* free any resources allocated up to where the error occurred */
    if (comId != NULL) (void)ComClose(comId, e);
    comId = NULL;
    goto cleanup;
    
cleanup:    
    /* if memory allocated for FTDI device list, free it */
    if (ftDevList != NULL) free(ftDevList);
    
    /* If handle opened for FTDI device list semaphore, release semaphore if
     * it was taken, then close the handle.
     */
    if (ftDevListSem != NULL) {
        if (releaseFtDevListSem) (void)ComReleaseSem(ftDevListSem, NULL);
        (void)CloseHandle(ftDevListSem);
    }
    
    return(comId);
}

RSTAT
ComClose(COM_ID comId, EI e)
{
    COM_PRIV *com = (COM_PRIV *)comId;
    RSTAT rStat = 0;
    
    /* if specified id is invalid, don't try to close it */
    if ((com == NULL) || (com->magic != COM_MAGIC)) return(ComIdErr(e));
    
    /* If semaphore created, wait a short time for it. If ComWait() fails,
     * either the specified ID was invalid (already closed), or something else
     * must still be using the communication interface, so don't continue with
     * the close (could cause undefined behavior).
     */
    if (com->sem != NULL) {
        if (ComWait(comId, 5000, e)) return(-1);
    }
    
    /* if connection to ADP board is open, close it */
    if (com->ftId != NULL) (void)FT_Close(com->ftId);
    
    /* Invalidate the magic number (so ComWait() will detect any attempt to
     * use the communication interface after it has been closed), then release
     * the semaphore and free the memory. Note the semaphore is never closed
     * because another thread may try to wait for it, and the behavior would
     * be undefined if it was closed. It shouldn't hurt to leave the semaphore
     * open because programs will usually just open a communication interface
     * once, then close it before they exit, and Windows will close the
     * semaphore when the program exits.
     */
    com->magic = 0;
    rStat = ComRelease(comId, e);
    free(com);
    return(rStat);
}

RSTAT
ComGetDevType(COM_ID comId, UD32 devAddr, char *buf, UD32 bufSize, EI e)
{
    UD32 cmdType = COM_GET_DEV_TYPE_CMD_TYPE;
    SD32 repLen;
    
    /* send command and receive variable length reply (device type string) */
    if ((repLen = ComCmdAndVarReply(comId, devAddr, cmdType, NULL, 0,
        buf, bufSize, e)) < 0) {
        return(-1);
    }
    
    /* if reply length = 0, fail (should be at least a terminating '\0') */
    if (repLen == 0) {
        EiSet(e, COM_LIB_EID_REPLY_TOO_SHORT, "%d\f%d", devAddr, cmdType);
        return(-1);
    }
    /* make sure string is terminated (just to be safe) */
    buf[repLen - 1] = '\0';
    
    return(0);
}

RSTAT
ComGetDevName(COM_ID comId, UD32 devAddr, char *buf, UD32 bufSize, EI e)
{
    UD32 cmdType = COM_GET_DEV_NAME_CMD_TYPE;
    SD32 repLen;
    
    /* send command and receive variable length reply (device name string) */
    if ((repLen = ComCmdAndVarReply(comId, devAddr, cmdType, NULL, 0,
        buf, bufSize, e)) < 0) {
        return(-1);
    }
    
    /* if reply length = 0, fail (should be at least a terminating '\0') */
    if (repLen == 0) {
        EiSet(e, COM_LIB_EID_REPLY_TOO_SHORT, "%d\f%d", devAddr, cmdType);
        return(-1);
    }
    /* make sure string is terminated (just to be safe) */
    buf[repLen - 1] = '\0';
    
    return(0);
}

RSTAT
ComGetDevVer(COM_ID comId, UD32 devAddr, char *buf, UD32 bufSize, EI e)
{
    UD32 cmdType = COM_GET_DEV_VER_CMD_TYPE;
    SD32 repLen;
    
    /* send command and receive variable length reply (version string) */
    if ((repLen = ComCmdAndVarReply(comId, devAddr, cmdType, NULL, 0,
        buf, bufSize, e)) < 0) {
        return(-1);
    }
    
    /* if reply length = 0, fail (should be at least a terminating '\0') */
    if (repLen == 0) {
        EiSet(e, COM_LIB_EID_REPLY_TOO_SHORT, "%d\f%d", devAddr, cmdType);
        return(-1);
    }
    /* make sure string is terminated (just to be safe) */
    buf[repLen - 1] = '\0';
    
    return(0);
}

RSTAT
ComGetDevErr(COM_ID comId, UD32 devAddr, EI e)
{
    char errRep[COM_ERR_REPLY_BUF_SIZE];
    SD32 errRepLen;
    
    if ((errRepLen = ComCmdAndVarReply(comId, devAddr,
        COM_GET_DEV_ERR_CMD_TYPE, NULL, 0, errRep, sizeof(errRep), e)) < 0) {
        return(-1);    
    }
    
    /* set error info for caller to reference */
    ComErrReplyToErrInfo(errRep, errRepLen, e);
    
    return(0);
}

static void
ComErrReplyToErrInfo(const char *errRep, UD32 errRepLen, EI e)
{
    const char *eid, *errArgs;
    
    /* get pointer to error ID string */
    eid = errRep; 
    
    /* get pointer to error arguments string (may be an empty string) */
    errArgs = &errRep[strlen(eid)] + 1;
    if (errArgs >= &errRep[errRepLen]) errArgs = "";
    
    /* Set error information for caller to reference. Note the error arguments
     * string (errArgs) is passed to EiSet() as a single string via "%s". This
     * will not change the arguments string, so the resulting error information
     * will contain the same arguments string.
     */
    EiSet(e, eid, "%s", errArgs);
}

RSTAT
ComCmdAndReply(COM_ID comId, UD32 devAddr, UD32 cmdType, const void *cmd,
    UD32 cmdLen, void *repBuf, UD32 repBufSize, EI e)
{
    COM_PRIV *com = (COM_PRIV *)comId;
    
    return(ComCmdAndReplyT(comId, devAddr, cmdType, cmd, cmdLen,
        repBuf, repBufSize, com->defTimeoutMs, e));
}

RSTAT
ComCmdAndReplyT(COM_ID comId, UD32 devAddr, UD32 cmdType, const void *cmd,
    UD32 cmdLen, void *repBuf, UD32 repBufSize, UD32 timeoutMs, EI e)
{
    SD32 repLen;
    
    if ((repLen = ComCmdAndVarReplyT(comId, devAddr, cmdType, cmd, cmdLen,
        repBuf, repBufSize, timeoutMs, e)) < 0) {
        return(-1);
    }
    if ((UD32)repLen != repBufSize) {
        EiSet(e, COM_LIB_EID_REPLY_TOO_SHORT, "%d\f%d", devAddr, cmdType);
        return(-1);
    }
    return(0);
}

SD32
ComCmdAndVarReply(COM_ID comId, UD32 devAddr, UD32 cmdType, const void *cmd,
    UD32 cmdLen, void *repBuf, UD32 repBufSize, EI e)
{
    COM_PRIV *com = (COM_PRIV *)comId;
    
    return(ComCmdAndVarReplyT(comId, devAddr, cmdType, cmd, cmdLen,
        repBuf, repBufSize, com->defTimeoutMs, e));
}

SD32
ComCmdAndVarReplyT(COM_ID comId, UD32 devAddr, UD32 cmdType, const void *cmd,
    UD32 cmdLen, void *repBuf, UD32 repBufSize, UD32 timeoutMs, EI e)
{
    COM_PRIV *com = (COM_PRIV *)comId;
    FT_STATUS ftStat;
    BOOL releaseCom;
    SD32 repLen;
    
    /* init variables referenced in goto for error/cleanup */
    releaseCom = FALSE;
    
    /* Wait for exclusive use of this communication interface so while the
     * calling thread is sending the specified command and receiving the reply,
     * any other threads that try to send a command will have to wait for the
     * first thread to finish. Otherwise, they will conflict. This also
     * validates the comId.
     */
    if (ComWait(comId, timeoutMs, e)) goto error;
    releaseCom = TRUE;
    
    /* if specified timeout != default, temporarily override default */
    if (timeoutMs != com->defTimeoutMs) {
        
        if ((ftStat = FT_SetTimeouts(com->ftId,timeoutMs,timeoutMs)) != FT_OK) {
            EiSet(e, COM_LIB_EID_FAILED_FT_SET_TIMEOUTS, "%d", ftStat);
            goto error;
        }
    }
    
    /* send command packet */
    if (ComSendCmd(comId, devAddr, cmdType, cmd, cmdLen, e)) goto error;
    
    /* receive reply packet */
    if ((repLen = ComReceiveReply(comId,repBuf,repBufSize,e)) < 0) goto error;
    
    /* go cleanup & return reply length */
    goto cleanup;
    
error:
    repLen = -1;
    goto cleanup;
    
cleanup:
    /* if specified timeout != default, restore default timeout */
    if (timeoutMs != com->defTimeoutMs) {
        (void)FT_SetTimeouts(com->ftId, com->defTimeoutMs, com->defTimeoutMs);
    }
    
    /* allow other threads to use this communication interface */
    if (releaseCom) (void)ComRelease(comId, NULL);
    
    return(repLen);
}

static RSTAT
ComSendCmd(COM_ID comId, UD32 devAddr, UD32 cmdType, const void *cmd,
    UD32 cmdLen, EI e)
{
    COM_PACK_HDR packHdr;
    
    /* check device address, command type, and command length */
    if (devAddr > COM_MAX_ADDR) {
        EiSet(e, COM_LIB_EID_DEV_ADDR_EXCEEDED_MAX, "%d", devAddr);
        return(-1);
    }
    if (cmdType > COM_MAX_CMD_TYPE) {
        EiSet(e, COM_LIB_EID_CMD_TYPE_EXCEEDED_MAX, "%d", cmdType);
        return(-1);
    }
    if (cmdLen > COM_MAX_PACK_LEN) {
        EiSet(e, COM_LIB_EID_CMD_LEN_EXCEEDED_MAX, "%d", cmdLen);
        return(-1);
    }
    
    /* build command packet header */
    packHdr.srcAndDest = (UD8)((COM_PC_ADDR << 4) | devAddr);
    packHdr.type = (UD8)cmdType;
    packHdr.lenLsb = (UD8)(cmdLen);
    packHdr.lenMsb = (UD8)(cmdLen >> 8);
    
    /* send command packet header */
    if (ComSend(comId, &packHdr, sizeof(packHdr), e)) return(-1);
    
    /* send any data for command */
    if (cmdLen != 0) {
        if (ComSend(comId, cmd, cmdLen, e)) return(-1);
    }
    
    return(0);
}

static SD32
ComReceiveReply(COM_ID comId, void *repBuf, UD32 repBufSize, EI e)
{
    char errRep[COM_ERR_REPLY_BUF_SIZE];
    COM_PACK_HDR packHdr;
    UD32 repLen;
    
    /* receive reply packet header */
    if (ComReceive(comId, &packHdr, sizeof(packHdr), e)) return(-1);
    
    /* If reply type indicates it's an error reply (command failed), prepare to
     * receive reply data (error info) into the error reply buffer instead of
     * the specified buffer.
     */
    if (packHdr.type != COM_OK_REPLY_TYPE) {
        repBuf = errRep;
        repBufSize = sizeof(errRep);
    }
    
    /* receive any reply data */
    repLen = ((UD32)packHdr.lenMsb << 8) | packHdr.lenLsb;
    if (repLen != 0) {
        
        /* If reply too big for buf, receive & discard rest of reply (to keep
         * communication synchronized on packet boundaries).
         */
        if (repLen > repBufSize) {
        
            ComReceiveAndDiscard(comId, repLen, NULL);
            
            if (repBufSize == 0) {
                EiSet(e, COM_LIB_EID_NO_BUF_FOR_REPLY, NULL);
            } else {
                EiSet(e, COM_LIB_EID_REPLY_TOO_BIG_FOR_BUF, NULL);
            }
            return(-1);
        }
        
        /* receive reply data */
        if (ComReceive(comId, repBuf, repLen, e)) return(-1);
    }
    
    /* if error reply (command failed), set error info & return error */
    if (repBuf == errRep) {
        ComErrReplyToErrInfo(errRep, repLen, e);
        return(-1);
    }
    
    /* return reply length (note this may be 0) */
    return(repLen);
}

static void
ComReceiveAndDiscard(COM_ID comId, UD32 len, EI e)
{
    char buf[32];
    UD32 n;
    
    while(len) {
        
        n = (len < sizeof(buf)) ? len : sizeof(buf);
        if (ComReceive(comId, buf, n, e)) break;
        len = len - n;
    }
}

static RSTAT
ComSend(COM_ID comId, void *data, UD32 len, EI e)
{
    COM_PRIV *com = (COM_PRIV *)comId;
    FT_STATUS ftStat;
    DWORD nWr;
    
    if ((ftStat = FT_Write(com->ftId, data, len, &nWr)) != FT_OK) {
        EiSet(e, COM_LIB_EID_FAILED_FT_WRITE, "%d", ftStat);
        return(-1);    
    }
    /* if not all of the data was written, assume FT_Write() timed out */
    if (nWr != len) {
        EiSet(e, COM_LIB_EID_SEND_TIMEOUT, NULL);
        return(-1);
    }
        
    return(0);
}

static RSTAT
ComReceive(COM_ID comId, void *buf, UD32 len, EI e)
{
    COM_PRIV *com = (COM_PRIV *)comId;
    FT_STATUS ftStat;
    DWORD nRd;
    
    /* receive data (FT_Read() will wait for data up to the current timeout) */
    if ((ftStat = FT_Read(com->ftId, buf, len, &nRd)) != FT_OK) {
        EiSet(e, COM_LIB_EID_FAILED_FT_READ, "%d", ftStat);
        return(-1);    
    }
    /* if not all of the requested data was received, FT_Read() timed out */
    if (nRd != len) {
        EiSet(e, COM_LIB_EID_RECEIVE_TIMEOUT, NULL);
        return(-1);
    }
    
    return(0);
}

RSTAT
ComSetDefTimeout(COM_ID comId, UD32 defTimeoutMs, EI e)
{
    COM_PRIV *com = (COM_PRIV *)comId;
    FT_STATUS ftStat;
    BOOL releaseCom;
    RSTAT rStat;
    
    /* init variables referenced in goto for error/cleanup */
    releaseCom = FALSE;
    
    /* Wait for exclusive use of this communication interface to ensure we'll
     * never change the timeout while another thread is sending a command and
     * receiving a reply to the command. This also validates the comId.
     */
    if (ComWait(comId, defTimeoutMs, e)) goto error;
    releaseCom = TRUE;
    
    /* set timeout for FTDI interface */
    ftStat = FT_SetTimeouts(com->ftId, defTimeoutMs, defTimeoutMs);
    if (ftStat != FT_OK) {
        EiSet(e, COM_LIB_EID_FAILED_FT_SET_TIMEOUTS, "%d", ftStat);
        goto error;
    }
    com->defTimeoutMs = defTimeoutMs;
    
    /* go cleanup & return 0 (success) */
    rStat = 0;
    goto cleanup;
    
error:
    rStat = -1;
    goto cleanup;
    
cleanup:
    /* allow other threads to use this communication interface */
    if (releaseCom) (void)ComRelease(comId, NULL);
    
    return(rStat);
}

static RSTAT
ComWait(COM_ID comId, UD32 timeoutMs, EI e)
{
    COM_PRIV *com = (COM_PRIV *)comId;
    
    /* if specified ID is invalid, fail */
    if ((com == NULL) || (com->magic != COM_MAGIC)) return(ComIdErr(e));
    
    /* wait for exclusive use of this communication interface */
    if (ComWaitSem(com->sem, timeoutMs, e)) return(-1);
    
    /* Check the ID again in case another thread closed the communication
     * interface while we were waiting. Note a communication interface should
     * not be closed while there are any threads using it, but check the ID
     * again just to be safe (to avoid undefined behavior).
     */
    if (com->magic != COM_MAGIC) {
        (void)ComReleaseSem(com->sem, NULL);
        return(ComIdErr(e));
    }
    
    return(0);
}

static RSTAT
ComRelease(COM_ID comId, EI e)
{
    COM_PRIV *com = (COM_PRIV *)comId;
    
    return(ComReleaseSem(com->sem, e));
}

static HANDLE
ComCreateSem(const char *semName, EI e)
{
    HANDLE sem;
    
    if ((sem = CreateMutex(NULL, FALSE, semName)) == NULL) {
        EiSet(e, COM_LIB_EID_FAILED_TO_CREATE_SEM, NULL);
        return(NULL);
    }
    return(sem);
}

static RSTAT
ComWaitSem(HANDLE sem, UD32 timeoutMs, EI e)
{
    if (WaitForSingleObject(sem, (DWORD)timeoutMs) != 0) {
        EiSet(e, COM_LIB_EID_FAILED_TO_WAIT_FOR_SEM, NULL);
        return(-1);
    }
    return(0);
}

static RSTAT
ComReleaseSem(HANDLE sem, EI e)
{
    if (ReleaseMutex(sem) == 0) {
        EiSet(e, COM_LIB_EID_FAILED_TO_RELEASE_SEM, NULL);
        return(-1);
    }
    return(0);
}

static RSTAT
ComSizeErr(EI e)
{
    EiSet(e, COM_LIB_EID_INVALID_STRUCT_SIZE, NULL);
    return(-1);
}

static RSTAT
ComIdErr(EI e)
{
    EiSet(e, COM_LIB_EID_INVALID_COM_ID, NULL);
    return(-1);
}
