/**************************************************************************
 * FILE: AdpLib.c
 * 
 * This file contains functions to communicate with an ADP board (Application
 * Development Platform).
 *
 * NOTES:
 *     None.
 * 
 * VISIBILITY: Customer
 *
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Standard/String.h"            /* memset(), strstr(), NULL */
#include "Standard/StdLib.h"            /* calloc() */
#include "Standard/Malloc.h"            /* calloc() */
#include "Adp/General.h"                /* RSTAT, FALSE, ... */
#include "Adp/Build/Build.h"            /* UD32, ... */
#include "Adp/AppDevPlatVer.h"          /* APP_DEV_PLAT_VER */
#include "Adp/AdpBoard/AdpCom.h"        /* ADP_LOCAL_ADDR, ... */
#include "Adp/AdpBoard/AdpGeneral.h"    /* ADP_CONFIG, ADP_STAT, ... */
#include "Adp/AdpBoard/AdpAppErrs.h"    /* ADP_APP_ERR, ... */
#include "Adp/Pc/ErrInfoLib.h"          /* EI, EiSet() */
#include "Adp/Pc/ComLib.h"              /* ComCmdAndReply(), ... */ 
#include "Adp/Pc/VerLib.h"              /* VerIsCompat() */
#include "Adp/Pc/AdpLib.h"              /* AdpLib public include */

/**************************************************************************
 * Defines and Macros
 */
 
/* magic number used to validate data structures (ascii "ADP ") */
#define ADP_MAGIC  0x41445020

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* Structure of private data for ADP library.
 */
typedef struct {
    
    /* magic number used to validate data structure */
    volatile UD32 magic;
    
    /* Communication interface to communicate with local ADP board (ADP board
     * connected to the PC via USB). If ADP_PRIV.addr is ADP_LOCAL_ADDR, the
     * local ADP board processes the commands. If ADP_PRIV.addr is
     * ADP_REMOTE_ADDR, the local ADP board forwards commands to the remote
     * ADP board (via the mezzanine boards and the wireless link), and the
     * remote ADP board processes the commands.
     */
    COM_ID comId;
    
    /* Address of ADP board to communicate with (ADP_LOCAL_ADDR for local
     * ADP board, or ADP_REMOTE_ADDR for remote ADP board). For more
     * information see ADP_PRIV.comId.
     */
    UD8 addr;
    
    /* true if multi-byte fields sent to & received from ADP must be swapped */
    BOOL swap;

} ADP_PRIV;
 
/**************************************************************************
 * 
 * Global and Static Definitions
 */
 
/**************************************************************************
 * Function Prototypes
 */
 
static RSTAT AdpIdErr(EI e);
static RSTAT AdpSizeErr(EI e);
static RSTAT AdpConvertErr(EI e);

/**************************************************************************
 * FUNCTION: AdpGetApiVer
 * 
 * This function gets the version string for this library, DLL, or API (the
 * Application Programming Interface for the Application Development Platform).
 * The caller may pass this to VerIsCompat() to check if it's compatible with
 * the API version required by the caller. For the format and content of
 * version strings, see VerIsCompat() in "AppDevPlat\Sw\Pc\Libs\VerLib.c".
 *
 * PARAMETERS:
 *     None.
 *
 * RETURN VALUES:
 *     This function returns the version string for this API (never NULL).
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function.
 *
 * VISIBILITY: Customer
 */
const char *
AdpGetApiVer(void)
{
    return(APP_DEV_PLAT_VER);
}

/**************************************************************************
 * FUNCTION: AdpInitSpecs
 * 
 * This function initializes an ADP_SPECS structure with default values. The
 * structure may then be modified as desired and passed to AdpOpen().
 *
 * PARAMETERS:
 *     specs:
 *         The ADP_SPECS structure to initialize. ADP_SPECS is defined in
 *         "AppDevPlat\Sw\Includes\Adp\Pc\AdpLib.h".
 *     specsSize:
 *         The size of the specified ADP_SPECS structure (bytes). Note
 *         AdpInitSpecs() won't write to anything in the structure beyond the
 *         specified size. That way, if new fields are added to the end of
 *         ADP_SPECS, this function will remain compatible with older
 *         applications that don't use the new fields.
 *
 * RETURN VALUES:
 *     None.
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate ADP_SPECS structure.
 *
 * VISIBILITY: Customer
 */
void
AdpInitSpecs(ADP_SPECS *specs, UD32 specsSize)
{
    ADP_SPECS defSpecs;
    
    /* init default specs with default values for everything */
    (void)memset(&defSpecs, 0, sizeof(defSpecs));
    defSpecs.magic = ADP_MAGIC;
    defSpecs.defTimeoutMs = COM_DEF_TIMEOUT_MS;
    
    /* copy default specs into caller's specs (up to specified size) */
    if (specsSize <= sizeof(defSpecs)) {
        (void)memcpy(specs, &defSpecs, specsSize);
    } else {
        (void)memset(specs, 0, specsSize);
        (void)memcpy(specs, &defSpecs, sizeof(defSpecs));
    }
}
 
/**************************************************************************
 * FUNCTION: AdpOpen
 * 
 * This function opens a connection to an ADP board (Application Development
 * Platform). It returns an ADP_ID that may be passed to the other functions
 * in this library to communicate with the ADP board.
 * 
 * Note only one connection should be opened for each ADP board, but it can
 * be shared by multiple threads if desired (semaphores are used to coordinate
 * communication so the threads won't conflict).
 * 
 * PARAMETERS:
 *     specs:
 *         Specifications for the connection (see ADP_SPECS in "AppDevPlat\
 *         Sw\Includes\Adp\Pc\AdpLib.h"), or NULL to use defaults. Note before
 *         passing an ADP_SPECS structure to AdpOpen(), it must be initialized
 *         using AdpInitSpecs() and modified as desired.
 *     specsSize:
 *         The size of the specified ADP_SPECS structure (bytes). Note AdpOpen()
 *         won't reference anything in the structure beyond the specified size.
 *         That way, if new fields are added to the end of ADP_SPECS, this
 *         function will remain compatible with older applications that don't
 *         use the new fields.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns a non-NULL ID for the connection.
 *     Otherwise, it fills the specified error object and returns NULL (to get
 *     the error information, see EiGetMsg(), etc. in "AppDevPlat\Sw\Pc\Libs\
 *     ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but only one connection should
 *     be opened for each ADP board (the connection may be shared by multiple
 *     threads if desired). Each thread should also specify its own separate
 *     error object.
 *
 * VISIBILITY: Customer
 */
ADP_ID
AdpOpen(const ADP_SPECS *specs, UD32 specsSize, EI e)
{
    char adpBoardVer[ADP_BOARD_VER_BUF_SIZE];
    COM_SPECS comSpecs;
    ADP_SPECS specsBuf;
    ADP_PRIV *adp;
    ADP_ID adpId;
    
    /* init resources referenced in error cleanup */
    adp = NULL;
    adpId = NULL;
    
    /* Init local specs buf with default values, then overwrite the defaults
     * with the passed specs (up to the specified size). That way, if fields
     * are added to the specs in the future, and the calling application isn't
     * updated, the new fields will still have valid default values.
     */
    AdpInitSpecs(&specsBuf, sizeof(specsBuf));
    if (specs != NULL) {
        
        /* if passed specs not initialized by AdpInitSpecs() or too big, fail */
        if ((specs->magic != ADP_MAGIC) || (specsSize > sizeof(specsBuf))) {
            EiSet(e, ADP_LIB_EID_INVALID_SPECS, NULL);
            goto error;
        }
        (void)memcpy(&specsBuf, specs, specsSize);
    }
    specs = &specsBuf;
    
    /* Allocate memory for private data and clear all resources in it so
     * the free function can check if each resource was allocated before trying
     * to free it. This way the free function can be called to clean up if
     * an error is detected during the remainder of the creation.
     */
    if ((adp = calloc(1, sizeof(*adp))) == NULL) {
        EiSet(e, ADP_LIB_EID_FAILED_TO_ALLOCATE_MEM, "%d", sizeof(*adp));
        goto error;
    }
    adp->magic = ADP_MAGIC;
    adpId = (ADP_ID)adp;
    
    /* if want to communicate with a remote ADP board */
    if (specs->remote) {
    
        /* If no local ADP board connection is specified, fail. This is
         * required because the local ADP board forwards command and reply
         * packets to and from the remote ADP board.
         */
        if (specs->localAdpId == NULL) {
            EiSet(e, ADP_LIB_EID_NO_LOCAL_ADP_ID, NULL);
            goto error;
        }
        /* get communication interface for local ADP board */
        if ((adp->comId = AdpGetComId(specs->localAdpId, e)) == NULL) {
            goto error;
        }
        
        /* set address for remote ADP board */
        adp->addr = ADP_REMOTE_ADDR;
        
    } else {
        
        /* open communication interface for local ADP board */
        ComInitSpecs(&comSpecs, sizeof(comSpecs));
        comSpecs.localMezzDevType = specs->localMezzDevType;
        comSpecs.localMezzDevName = specs->localMezzDevName;
        comSpecs.defTimeoutMs = specs->defTimeoutMs;
        if ((adp->comId = ComOpen(&comSpecs, sizeof(comSpecs), e)) == NULL) {
            goto error;
        }
        
        /* set address for local ADP board */
        adp->addr = ADP_LOCAL_ADDR;
    }
    
    /* get version string from ADP board */
    if (AdpGetBoardVer(adpId, adpBoardVer, sizeof(adpBoardVer), e)) goto error;
    /*
     * Check if the version string contains a board model supported by this
     * library. This also verifies we're talking to an ADP board.
     */
    if (strstr(adpBoardVer, ADP100_NAME) == NULL) {
        EiSet(e, ADP_LIB_EID_INCOMPATIBLE_BOARD_MODEL, "%s", adpBoardVer);
        goto error;
    }
    /* Check if the ADP board version is compatible with the version required
     * by this library (APP_DEV_PLAT_VER, defined in "Adp/AppDevPlatVer.h").
     */
    if (VerIsCompat(adpBoardVer, APP_DEV_PLAT_VER) == FALSE) {
        EiSet(e, ADP_LIB_EID_INCOMPATIBLE_ADP_BOARD_VER, "%s\f%s",
            adpBoardVer, APP_DEV_PLAT_VER);
        goto error;
    }
    
    return(adpId);
    
error:
    /* free any resources allocated up to where the error occurred */
    if (adp != NULL) (void)AdpClose((ADP_ID)adp, NULL);
    
    return(NULL);
}

/**************************************************************************
 * FUNCTION: AdpClose
 * 
 * This function closes a connection that was opened by AdpOpen(), freeing any
 * memory and other resources allocated for it. Note this won't close the local
 * ADP board connection that was passed to AdpOpen() (see ADP_SPECS.localAdpId
 * in "AppDevPlat\Sw\Includes\Adp\Pc\AdpLib.h"). Thus, that local ADP board
 * connection must be closed separately if desired.
 *
 * PARAMETERS:
 *     adpId:
 *         The ID of the connection to close (an ID returned by AdpOpen()).
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     A connection should only be closed once by a single thread, and after
 *     it's closed, the connection ID should no longer be used by any thread
 *     (if it is, the called function will detect it and return an error).
 *     Each thread should also specify its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
AdpClose(ADP_ID adpId, EI e)
{
    ADP_PRIV *adp = (ADP_PRIV *)adpId;
    RSTAT rStat = 0;
    
    /* if specified id is invalid, don't try to close it */
    if ((adp == NULL) || (adp->magic != ADP_MAGIC)) return(AdpIdErr(e));
    
    /* If talking to local ADP board, and a communication interface is open,
     * AdpOpen() must have opened the communication interface, so close it.
     */
    if ((adp->addr == ADP_LOCAL_ADDR) && (adp->comId != NULL)) {
        if (ComClose(adp->comId, e)) rStat = -1;
    }
    
    /* Invalidate the magic number so we'll detect any attempt to use the ID
     * after it has been closed.
     */
    adp->magic = 0;
    free(adp);
    return(rStat);
}

/**************************************************************************
 * FUNCTION: AdpGetBoardVer
 * 
 * This function gets the version string for the ADP board.
 *
 * PARAMETERS:
 *     adpId:
 *         A connection ID returned by AdpOpen().
 *     buf:
 *         Buffer in which to return the version string.
 *     bufSize:
 *         The size of the specified buffer (bytes). To ensure the version
 *         string will fit in the buffer, the buffer should be at least
 *         ADP_BOARD_VER_BUF_SIZE bytes (defined in "AppDevPlat\Sw\Includes\
 *         Adp\Pc\AdpLib.h"). If the version string won't fit in the buffer,
 *         it will be truncated (note it will still be NULL-terminated).
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function puts the NULL-terminated version string
 *     in the specified buffer and returns 0. If not successful, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate version string buffer and error object.
 *
 * VISIBILITY: Customer
 */
RSTAT
AdpGetBoardVer(ADP_ID adpId, char *buf, UD32 bufSize, EI e)
{
    ADP_PRIV *adp = (ADP_PRIV *)adpId;
    
    /* if specified id is invalid, fail */
    if ((adp == NULL) || (adp->magic != ADP_MAGIC)) return(AdpIdErr(e));
    
    if (ComGetDevVer(adp->comId, adp->addr, buf, bufSize, e)) {
        return(AdpConvertErr(e));
    }
    return(0);
}

/**************************************************************************
 * FUNCTION: AdpGetStat
 * 
 * This function gets the current status from the ADP board. Typically, an
 * application will call this periodically to check the board's status, such
 * as once a second.
 *
 * PARAMETERS:
 *     adpId:
 *         A connection ID returned by AdpOpen().
 *     as:
 *         Buffer in which to return the board's status. ADP_STAT is defined
 *         in "AppDevPlat\Sw\Includes\Adp\AdpBoard\AdpGeneral.h".
 *     asSize:
 *         The size of the specified buffer (bytes). Note AdpGetStat() won't
 *         write beyond the specified size. That way, if new fields are added
 *         to the end of ADP_STAT, this function will remain compatible with
 *         older applications that don't use the new fields.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If this function successfully gets the board's status, it puts the status
 *     in the specified buffer and returns 0. Note if the ADP_ERR_OCCURRED flag
 *     is set in the status, this function will also put the board's error
 *     information in the specified error object, but in this case, the
 *     function will still return 0.
 *
 *     If the function fails to get the board's status, it will fill the
 *     specified error object and return -1.
 *
 *     To get the error information (when ADP_ERR_OCCURRED is set, or the
 *     function returns -1), see EiGetMsg(), etc. in "AppDevPlat\Sw\Pc\Libs\
 *     ErrInfoLib.c".
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate ADP_STAT buffer and error object.
 *
 * VISIBILITY: Customer
 */
RSTAT
AdpGetStat(ADP_ID adpId, ADP_STAT *as, UD32 asSize, EI e)
{
    ADP_PRIV *adp = (ADP_PRIV *)adpId;
    UD8 asBuf[256];
    SD32 asLen;
    
    /* if specified id is invalid, fail */
    if ((adp == NULL) || (adp->magic != ADP_MAGIC)) return(AdpIdErr(e));
    
    /* Get status from ADP board. Note asBuf should always be larger than the
     * ADP_STAT structure, so this will always read the full status available
     * from the board, whatever the board's version. If ADP_STAT ever outgrows
     * asBuf, the worst that will happen is ComCmdAndVarReply() will fail
     * during development and asBuf will have to be increased.
     */
    if ((asLen = ComCmdAndVarReply(adp->comId, adp->addr,
        ADP_GET_STAT_CMD_TYPE, NULL, 0, asBuf, sizeof(asBuf), e)) < 0) {
        return(AdpConvertErr(e));
    }
    /* If the caller expects a larger status than is available, fail. It's ok
     * if the caller expects a smaller status (fewer fields). That way, if new
     * fields are added to the end of ADP_STAT, this interface will remain
     * compatible with older software on the PC - since the caller doesn't
     * expect the new fields that were added to the end of ADP_STAT, those
     * fields simply won't be returned to the caller.
     */
    if (asSize > (UD32)asLen) return(AdpSizeErr(e));
    (void)memcpy(as, asBuf, asSize);
    
    /* if error occurred on board, get error & set error info for caller */
    if (as->flags & ADP_ERR_OCCURRED) {
        if (ComGetDevErr(adp->comId, adp->addr, e)) return(-1);
        (void)AdpConvertErr(e);
    }
    
    return(0);
}

/**************************************************************************
 * FUNCTION: AdpGetConfig
 * 
 * This function gets the current configuration from the ADP board.
 *
 * PARAMETERS:
 *     adpId:
 *         A connection ID returned by AdpOpen().
 *     ac:
 *         Buffer in which to return the board's configuration. ADP_CONFIG is
 *         defined in "AppDevPlat\Sw\Includes\Adp\AdpBoard\AdpGeneral.h".
 *     acSize:
 *         The size of the specified buffer (bytes). Note AdpGetConfig() won't
 *         write beyond the specified size. That way, if new fields are added
 *         to the end of ADP_CONFIG, this function will remain compatible with
 *         older applications that don't use the new fields.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function puts the board's configuration in the
 *     specified buffer and returns 0. If not successful, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate ADP_CONFIG buffer and error object.
 * 
 * VISIBILITY: Customer
 */

RSTAT
AdpGetConfig(ADP_ID adpId, ADP_CONFIG *ac, UD32 acSize, EI e)
{
    ADP_PRIV *adp = (ADP_PRIV *)adpId;
    SD32 acLen;
    union {
        ADP_CONFIG ac;
        UD8 buf[256];
    } acBuf;
    
    /* if specified id is invalid, fail */
    if ((adp == NULL) || (adp->magic != ADP_MAGIC)) return(AdpIdErr(e));
    
    /* Get config from ADP board. Note acBuf should always be larger than the
     * ADP_CONFIG structure, so this will always read the full config available
     * from the board, whatever the board's version. If ADP_CONFIG ever
     * outgrows acBuf, the worst that will happen is ComCmdAndVarReply() will
     * fail during development and acBuf will have to be increased.
     */
    if ((acLen = ComCmdAndVarReply(adp->comId, adp->addr,
        ADP_GET_CONFIG_CMD_TYPE, NULL, 0, &acBuf, sizeof(acBuf), e)) < 0) {
        return(AdpConvertErr(e));
    }
    /* if needed, swap multi-byte fields */
    if (adp->swap) {
        acBuf.ac.pow.vsup1Mv = SWAP16(acBuf.ac.pow.vsup1Mv);
        acBuf.ac.pow.vsup2Mv = SWAP16(acBuf.ac.pow.vsup2Mv);
    }
    /* If the caller expects a larger config than is available, fail. It's ok
     * if the caller expects a smaller config (fewer fields). That way, if new
     * fields are added to the end of ADP_CONFIG, this interface will remain
     * compatible with older software on the PC - since the caller doesn't
     * expect the new fields that were added to the end of ADp_CONFIG, those
     * fields simply won't be returned to the caller.
     */
    if (acSize > (UD32)acLen) return(AdpSizeErr(e));
    (void)memcpy(ac, &acBuf, acSize);
    
    return(0);
}

/**************************************************************************
 * FUNCTION: AdpSetConfig
 * 
 * This function sets the configuration on the ADP board.
 * 
 * Note every field must be specified in the configuration. Usually,
 * an application will call AdpGetConfig() to get the current configuration,
 * modify it as desired, then pass it to AdpSetConfig().
 * 
 * Note when the configuration is changed, the ADP board will
 * automatically stop any operation that is active (note this won't affect
 * any operation on the mezzanine board attached to the ADP board).
 *
 * PARAMETERS:
 *     adpId:
 *         A connection ID returned by AdpOpen().
 *     ac:
 *         The new configuration. ADP_CONFIG is defined in "AppDevPlat\Sw\
 *         Includes\Adp\AdpBoard\AdpGeneral.h".
 *     acSize:
 *         The size of the specified ADP_CONFIG structure (bytes). Note
 *         AdpSetConfig() won't reference anything in the structure beyond
 *         the specified size. That way, if new fields are added to the end of
 *         ADP_CONFIG, this function will remain compatible with older
 *         applications that don't use the new fields.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
AdpSetConfig(ADP_ID adpId, const ADP_CONFIG *ac, UD32 acSize, EI e)
{
    ADP_PRIV *adp = (ADP_PRIV *)adpId;
    ADP_CONFIG acBuf;
    
    /* if specified id is invalid, fail */
    if ((adp == NULL) || (adp->magic != ADP_MAGIC)) return(AdpIdErr(e));
    
    /* Check specified config size. Note the specified size can be less than
     * the current size of ADP_CONFIG. That way, if new fields are added to the
     * end of ADP_CONFIG, this interface will remain compatible with older
     * software on the PC (the fields added to the end of the config will not
     * be affected by the command, and will simply be left as is).
     */
    if (acSize > sizeof(ADP_CONFIG)) return(AdpSizeErr(e));
    
    /* if we need to swap multi-byte fields, copy config & swap the fields */
    if (adp->swap) {
        (void)memcpy(&acBuf, ac, acSize);
        acBuf.pow.vsup1Mv = SWAP16(acBuf.pow.vsup1Mv);
        acBuf.pow.vsup2Mv = SWAP16(acBuf.pow.vsup2Mv);
        ac = &acBuf;
    }
    
    /* send command and receive reply */
    if (ComCmdAndReply(adp->comId, adp->addr,
        ADP_SET_CONFIG_CMD_TYPE, ac, acSize, NULL, 0, e)) {
        return(AdpConvertErr(e));
    }
    
    return(0);
}

/**************************************************************************
 * FUNCTION: AdpSetVsup
 * 
 * This function sets the desired VSUP1 and VSUP2 (voltage supply levels) on
 * the ADP board. If desired, this can be used to change VSUP1 and VSUP2 on
 * the fly without reconfiguring the whole ADP board (see AdpSetConfig()).
 *
 * PARAMETERS:
 *     adpId:
 *         A connection ID returned by AdpOpen().
 *     vsup1Mv:
 *         The new value for VSUP1 (millivolts; 0 = off).
 *     vsup2Mv:
 *         The new value for VSUP2 (millivolts; 0 = off).
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns 0. Otherwise, it fills the
 *     specified error object and returns -1 (to get the error, see EiGetMsg(),
 *     etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
RSTAT
AdpSetVsup(ADP_ID adpId, UD32 vsup1Mv, UD32 vsup2Mv, EI e)
{ 
    ADP_PRIV *adp = (ADP_PRIV *)adpId;
    ADP_SET_VSUP_CMD cmd;
    
    /* if specified id is invalid, fail */
    if ((adp == NULL) || (adp->magic != ADP_MAGIC)) return(AdpIdErr(e));
    
    /* build command */
    cmd.vsup1Mv = (UD16)vsup1Mv;
    cmd.vsup2Mv = (UD16)vsup2Mv;
    
    /* if needed, swap multi-byte fields */
    if (adp->swap) {
        cmd.vsup1Mv = SWAP16(cmd.vsup1Mv);
        cmd.vsup2Mv = SWAP16(cmd.vsup2Mv);
    }
    
    /* send command and receive reply */
    if (ComCmdAndReply(adp->comId, adp->addr, ADP_SET_VSUP_CMD_TYPE,
        &cmd, sizeof(cmd), NULL, 0, e)) {
        return(AdpConvertErr(e));
    }
    return(0);
}

/**************************************************************************
 * FUNCTION: AdpGetTraceMsg
 * 
 * This function gets the the next message in the trace buffer on the ADP
 * board. Note trace messages are generally intended for test/debug, so they're
 * normally disabled, but they can be enabled if desired. For more information
 * on tracing, see "AppDevPlat\Sw\Includes\Adp\AnyBoard\TraceLib.h".
 *
 * PARAMETERS:
 *     adpId:
 *         A connection ID returned by AdpOpen().
 *     buf:
 *         The buffer in which to return the next trace message (note if
 *         no more trace messages are available, the message will be a
 *         NULL-terminated empty string).
 *     bufSize:
 *         The size of the specified buffer (bytes). If the trace message
 *         won't fit in the buffer, it will be truncated (note it will still
 *         be NULL-terminated). The maximum size of a trace message depends
 *         on the TRACE_MSG_BUF_SIZE defined in "AppDevPlat\Sw\Includes\Adp\
 *         AnyBoard\TraceLib.h". It's recommended the specified buffer is >=
 *         256 bytes to ensure this will get the whole message.
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function puts the NULL-terminated trace message in
 *     the specified buffer and returns 0 (note if no more trace messages are
 *     available, the message will be a NULL-terminated empty string). If not
 *     successful, it fills the specified error object and returns -1 (to get
 *     the error, see EiGetMsg(), etc. in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate buffer and error object.
 *
 * VISIBILITY: Customer
 */
RSTAT
AdpGetTraceMsg(ADP_ID adpId, char *buf, UD32 bufSize, EI e)
{
    ADP_PRIV *adp = (ADP_PRIV *)adpId;
    SD32 repLen;
    
    /* if specified id is invalid, fail */
    if ((adp == NULL) || (adp->magic != ADP_MAGIC)) return(AdpIdErr(e));
    
    /* send command and receive variable length reply (trace message string) */
    if ((repLen = ComCmdAndVarReply(adp->comId, adp->addr,
        ADP_GET_TRACE_MSG_CMD_TYPE, NULL, 0, buf, bufSize, e)) < 0) {
        return(AdpConvertErr(e));
    }
    
    /* make sure string is terminated (just to be safe) */
    if (repLen) {
        buf[repLen - 1] = '\0';
    } else {
        buf[0] = '\0';
    }
    
    return(0);
}

/**************************************************************************
 * FUNCTION: AdpGetComId
 * 
 * This function gets the underlying communication interface (COM_ID) for the
 * specified ADP board connection, which is used to communicate with the local
 * ADP board. Note this is intended for internal use, so the application should
 * only use it if there's a special need. The COM_ID can be passed to the
 * communication library functions to communicate with the local ADP board
 * (see "AppDevPlat\Sw\Pc\Libs\ComLib.c").
 * 
 * Note if the specified ADP board connection is for a remote ADP board,
 * it still uses a COM_ID to communicate with a local ADP board (the local ADP
 * board forwards commands to the remote ADP board via the mezzanine boards and
 * their wireless link).
 * 
 * PARAMETERS:
 *     adpId:
 *         A connection ID returned by AdpOpen().
 *     e:
 *         Error object to use to return error information if the function
 *         fails (see EiOpen() in "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c").
 *
 * RETURN VALUES:
 *     If successful, this function returns the non-NULL COM_ID. Otherwise,
 *     it fills the specified error object and returns NULL (to get the
 *     error information, see EiGetMsg(), etc. in "AppDevPlat\Sw\Pc\Libs\
 *     ErrInfoLib.c").
 * 
 * THREAD SAFE:
 *     Multiple threads may call this function, but each thread should specify
 *     its own separate error object.
 * 
 * VISIBILITY: Customer
 */
COM_ID
AdpGetComId(ADP_ID adpId, EI e)
{
    ADP_PRIV *adp = (ADP_PRIV *)adpId;
    
    /* if specified id is invalid, fail */
    if ((adp == NULL) || (adp->magic != ADP_MAGIC)) {
        (void)AdpIdErr(e);
        return(NULL);
    }
    
    return(adp->comId);
}

RSTAT
AdpStartPowMon(ADP_ID adpId, const ADP_POW_MON_OPTIONS *pmo, UD32 pmoSize, EI e)
{
    ADP_PRIV *adp = (ADP_PRIV *)adpId;
    ADP_START_POW_MON_CMD cmd;
    UINT cmdLen;

    /* if specified id is invalid, fail */
    if ((adp == NULL) || (adp->magic != ADP_MAGIC)) return(AdpIdErr(e));

    /* Check size of specified options. Note the specified size can be less
     * than the current size of ADP_POW_MON_OPTIONS. That way, if new fields
     * are added to the end of ADP_POW_MON_OPTIONS, this interface will remain
     * compatible with older software on the PC.
     */
    if (pmoSize > sizeof(cmd)) return(AdpSizeErr(e));

    /* build command and swap multi-byte fields (if needed) */
    memcpy(&cmd, pmo, pmoSize);
    cmdLen = pmoSize;
    if (adp->swap) {
        cmd.usPerSamp = SWAP16(cmd.usPerSamp);
    }

    /* send command and receive reply */
    if (ComCmdAndReply(adp->comId, adp->addr, ADP_START_POW_MON_CMD_TYPE,
        &cmd, cmdLen, NULL, 0, e)) {
        return(AdpConvertErr(e));
    }
    return(0);
}

RSTAT
AdpStopPowMon(ADP_ID adpId, EI e)
{
    ADP_PRIV *adp = (ADP_PRIV *)adpId;

    /* if specified id is invalid, fail */
    if ((adp == NULL) || (adp->magic != ADP_MAGIC)) return(AdpIdErr(e));

    /* send command and receive reply */
    if (ComCmdAndReply(adp->comId, adp->addr, ADP_STOP_POW_MON_CMD_TYPE,
        NULL, 0, NULL, 0, e)) {
        return(AdpConvertErr(e));
    }
    return(0);
}

RSTAT
AdpGetPow(ADP_ID adpId, ADP_POW_BUF *pb, UD32 pbSize, EI e)
{
    ADP_PRIV *adp = (ADP_PRIV *)adpId;
    SD16 *samp;

    /* if specified id is invalid, fail */
    if ((adp == NULL) || (adp->magic != ADP_MAGIC))
        return(AdpIdErr(e));

    /* check size of specified buffer */
    if (pbSize != sizeof(ADP_POW_BUF)) return(AdpSizeErr(e));

    /* send command and receive reply */
    if (ComCmdAndVarReply(adp->comId, adp->addr, ADP_GET_POW_CMD_TYPE,
        NULL, 0, pb, pbSize, e) < 0) {
        return(AdpConvertErr(e));
    }

    /* if needed, swap multi-byte fields */
    if (adp->swap) {

        if ((pb->info.nSamps = SWAP16(pb->info.nSamps)) > ADP_MAX_SAMPS_IN_POW_BUF) {
            return(AdpSizeErr(e));
        }
        for(samp = pb->samps; samp < &pb->samps[pb->info.nSamps]; ++samp) {
            *samp = SWAP16(*samp);
        }
    }
    return(0);
}

static RSTAT
AdpSizeErr(EI e)
{
    EiSet(e, ADP_LIB_EID_INVALID_STRUCT_SIZE, NULL);
    return(-1);
}

static RSTAT
AdpIdErr(EI e)
{
    EiSet(e, ADP_LIB_EID_INVALID_ADP_ID, NULL);
    return(-1);
}

/* If the specified error is from the ADP board, this will convert the error
 * so it contains a specific error message (errors from the ADP board only
 * contain a generic message). Note this will only change the error's message,
 * not the error group or code. This always returns -1 so it can be passed to
 * a return statement, if desired.
 */
static RSTAT
AdpConvertErr(EI e)
{
    const char *eid;
    
    /* get original error ID string */
    eid = EiGetId(e);
    
    /* If it's an error from the ADP board, convert it to a new error ID string
     * that contains a specific error message.
     */
    if (EiGroupIs(e, ADP_APP_ERR)) {
        
        /* extract error code from original error ID string and convert it */
        switch(atoi(&eid[sizeof(ADP_APP_ERR)])) {
        case ADP_APP_ERR_I2C_NOT_YET_SUPPORTED:
            eid = ADP_APP_EID_I2C_NOT_YET_SUPPORTED; break;
        case ADP_APP_ERR_RECEIVED_CMD_WITH_INVALID_CMD_TYPE:
            eid = ADP_APP_EID_RECEIVED_CMD_WITH_INVALID_CMD_TYPE; break;
        case ADP_APP_ERR_RECEIVED_CMD_WITH_INVALID_LEN:
            eid = ADP_APP_EID_RECEIVED_CMD_WITH_INVALID_LEN; break;
        case ADP_APP_ERR_USB_RX_TIMEOUT:
            eid = ADP_APP_EID_USB_RX_TIMEOUT; break;
        case ADP_APP_ERR_USB_TX_TIMEOUT:
            eid = ADP_APP_EID_USB_TX_TIMEOUT; break;
        case ADP_APP_ERR_ADP_SPI_RX_TIMEOUT:
            eid = ADP_APP_EID_ADP_SPI_RX_TIMEOUT; break;
        case ADP_APP_ERR_ADP_SPI_TX_TIMEOUT:
            eid = ADP_APP_EID_ADP_SPI_TX_TIMEOUT; break;
        case ADP_APP_ERR_RECEIVED_PACK_WITH_DEST_0:
            eid = ADP_APP_EID_RECEIVED_PACK_WITH_DEST_0; break;
        case ADP_APP_ERR_FAILED_TO_SET_MAX_BAT_CHARGE:
            eid = ADP_APP_EID_FAILED_TO_SET_MAX_BAT_CHARGE; break;
        case ADP_APP_ERR_FAILED_TO_SET_VSUP1:
            eid = ADP_APP_EID_FAILED_TO_SET_VSUP1; break;
        case ADP_APP_ERR_FAILED_TO_SET_VSUP2:
            eid = ADP_APP_EID_FAILED_TO_SET_VSUP2; break;
        case ADP_APP_ERR_ILLEGAL_CMD_WHILE_DMA_RUNNING:
            eid = ADP_APP_EID_ILLEGAL_CMD_WHILE_DMA_RUNNING; break;

        default:
            eid = NULL; break; /* don't convert error */
        }
        
    } else { /* unknown error group */
        
        eid = NULL; /* don't convert error */
    }
    
    /* If there's a new error ID string for the error, call EiSet() to update
     * the error information. Note the error arguments string (returned by
     * EiGetArgs()) is passed as a single string via "%s". This won't change
     * the arguments string, so the new error information will still contain
     * the same arguments string.
     */
    if (eid != NULL) EiSet(e, eid, "%s", EiGetArgs(e));
    
    return(-1);
}
