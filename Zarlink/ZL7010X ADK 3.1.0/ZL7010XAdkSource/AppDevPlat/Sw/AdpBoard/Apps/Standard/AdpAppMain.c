/**************************************************************************
 * This file contains the main processing functions on the ADP board (main
 * entry point, initialization, and processing loop).
 *
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Standard/String.h"          /* memset() */
#include "Adp/General.h"              /* RSTAT, UINT, ... */
#include "Adp/Build/Build.h"          /* UD16, WDTCTL, RESET_BIT, ... */
#include "Adp/AnyBoard/ErrLib.h"      /* ErrInit(), ... */
#include "Adp/AnyBoard/SysTimerLib.h" /* StInit() */

/* local include shared by all source files for ADP board application */
#include "AdpApp.h"

/**************************************************************************
 * Defines and Macros
 */
 
/* This determines how often the example periodic task is performed (note the
 * ADP board currently doesn't do any periodic tasks, but this example is
 * provided for future reference if needed). For best accuracy, this should
 * be a multiple of ADP_MS_PER_CLOCK.
 */
#define ADP_MS_PER_EXAMPLE_TASK  1000
#define ADP_CLOCKS_PER_EXAMPLE_TASK  (ADP_MS_PER_EXAMPLE_TASK/ ADP_MS_PER_CLOCK)
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* structure of private data for main processing */
typedef struct {
    
    /* The time at which the last clock occurred. AdpProc() uses this to
     * determine if it's time for the next clock. At each clock, AdpProc()
     * checks if any periodic tasks need to be performed and performs them
     * as needed.
     */
    UD16 clockTimeMs;
    
    /* number of clocks left until example periodic task is performed */
    UD8 clocksToExampleTask;
    
} ADP_MAIN_PRIV;
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/* private data for main processing */
static ADP_MAIN_PRIV AdpMainPriv;

/* data shared by all source files for ADP board application */
ADP_PUB  AdpPub;

/* constant global string for ADP application error group */
const char *AdpAppErr = ADP_APP_ERR;

/**************************************************************************
 * Function Prototypes
 */
static RSTAT AdpInit(void);
__inline void AdpInitMcIo(void);
__inline void AdpInitMcClocks(void);
__inline void AdpProc(void);
/* __inline void AdpSleep(void); */

/**************************************************************************
 * Main entry point for application running on ADP board.
 */
int
main(void)
{
    /* if initialization fails, loop forever */
    if (AdpInit()) {
        for(;;) { /* AdpSleep(); */ }
    }
    
    /* main processing loop */
    while(1) { AdpProc(); }

    #pragma diag_suppress 112
    return 0;
    #pragma diag_default 112
}

static RSTAT
AdpInit(void)
{
    /* stop watchdog timer */
    WDTCTL = WDTPW + WDTHOLD;
    
    /* init micro-controller I/O ports */
    AdpInitMcIo();
    
#ifndef DEBUG
    /* If the RESET_STAT_BIT input is low (cold power-on reset), set strobe to
     * latch it high (so we won't repeat this during a warm reset), then clear
     * the RESET_BIT output to initiate a warm reset. This is done to clear a
     * cold power-on problem with the ADP SPI interface (after cold power-on,
     * the ADP SPI data is sometimes skewed by 1 bit).
     * 
     * Note if you are using the CCE debugger to run this software for the
     * first time on an ADP board, RESET_STAT_BIT will be 0, so this logic
     * will reset the micro-controller, causing the debugger to fail. To fix
     * this, restart the debugger (on restart, RESET_STAT_BIT will be 1, so
     * this logic won't reset the micro-controller again). To avoid this
     * issue altogether, run the debug configuration of the sofware (with
     * DEBUG defined) so this logic will be excluded.
     */
    if ((RESET_STAT_P(IN) & RESET_STAT_BIT) == 0) {
        
        RESET_STAT_STROBE_P(OUT) |= RESET_STAT_STROBE_BIT;
        RESET_P(OUT) &= ~RESET_BIT;
    }
#endif

    /* init micro-controller clocks (must be called before StInit()) */
    AdpInitMcClocks();
    
    /* clear public & private data and init any non-zero defaults */
    (void)memset(&AdpPub, 0, sizeof(AdpPub));
    (void)memset(&AdpMainPriv, 0, sizeof(AdpMainPriv));
    AdpMainPriv.clocksToExampleTask = ADP_CLOCKS_PER_EXAMPLE_TASK;

    /* init error lib */
    if (ErrInit()) return(-1);
    
    /* init system timer and enable interrupts so it will start counting */
    if (StInit()) return(-1);
    _enable_interrupts();
    
    /* init command processing interface */
    if (AdpCmdInit()) return(-1);
    
    /* Init ADC interface (internal ADC in MSP430), then initialize the power
     * interface (VSUP1, VSUP2, etc.). AdpAdcInit() enables the internal VRef
     * for the MSP430's ADC's and DAC's (REFON & REF2_5V in ADC12CTL0), then
     * waits 17 ms to give VRef time to bias the storage capacitors. Note
     * AdpAdcInit() must be called before AdpPowInit() so VRef is ready before
     * the DAC's are initialized (for VSUP1 & VSUP2).
     */
    if (AdpAdcInit()) return(-1);
    if (AdpPowInit()) return(-1);
    
    /* Wait 2 sec to ensure the USB interface is ready before initializing it,
     * and to ensure the micro-controller on the mezzanine board finishes its
     * power-on initialization before strobing MEZZ_RESET_BIT to reset it again
     * (see MEZZ_RESET_BIT below).
     */
    StDelayMs(2000);
    /*
     * Init communication interface (ADP SPI & USB).
     */
    if (AdpComInit()) return(-1);
    /* 
     * Reset the mezzanine board for the following reasons:
     * 
     * - Ensures the mezzanine board is reinitialized after VSUP2 is up, so
     *   the I/O signals to the mezzanine board are stable (VSUP2 powers the
     *   mezzanine side of the level shifters on the ADP board).
     * 
     * - Clears the cold power-on problem with the ADP SPI interface on the
     *   mezzanine board (after cold power-on, the ADP SPI data is sometimes
     *   shifted by 1 bit).
     */
    MEZZ_RESET_P(OUT) &= ~MEZZ_RESET_BIT;
    StDelayMs(1);  /* ensure it's low long enough to initiate reset */
    MEZZ_RESET_P(OUT) |= MEZZ_RESET_BIT;
    
    return(0);
}

__inline void
AdpInitMcIo(void)
{
    /* Init output ports. Note unused ports should be initialized as outputs
     * to prevent floating inputs and reduce power (recommended in MSP430
     * manual). It's also best to drive them low in case they are accidently
     * shorted to ground during testing. This also applies to test points.
     */
    P1OUT = P1OUT_INIT;
    P1DIR = P1DIR_INIT;
    /**/
    P2OUT = P2OUT_INIT;
    P2DIR = P2DIR_INIT;
    /**/
    P3OUT = P3OUT_INIT;
    P3DIR = P3DIR_INIT;
    /**/
    P4OUT = P4OUT_INIT;
    P4DIR = P4DIR_INIT;
    /**/
    P5OUT = P5OUT_INIT;
    P5DIR = P5DIR_INIT;
    /**/
    P6OUT = P6OUT_INIT;
    P6DIR = P6DIR_INIT;
}

/* Initialize the micro-controller clocks. Note this configures the system
 * timer in a special mode, so it must be called before StInit() (otherwise,
 * it would clobber the normal system timer settings).
 */
__inline void
AdpInitMcClocks(void)
{
    /* Init BCSCTL1 (LFXT1 = 8 MHz, no XT2, and keep default resistor setting
     * so DCO frequency = ~800 KHz). Note after this, ACLK will source LFXT1
     * (8MHz), but MCLK and SMCLK will still source the DCO (~800 KHz).
     */
    BCSCTL1 = XTS | XT2OFF | RSEL2;
    
    /* Temporarily configure the system timer so we can use it to wait ~100 us
     * in the loop that follows. Note the MSP430 manual says if a timer clock
     * is asynchronous to MCLK, and the CPU tries to read the timer while it's
     * running, it might not read the correct value. Thus, this configures the
     * system timer to use the DCO (via SMCLK) so it's synchronous to MCLK.
     * 
     * ST_CC0_COUNT:
     *     The count at which the timer will set the CCIFG interrupt flag.
     *     This is calculated for ~100 us (100 * CountPerSec/UsPerSec =
     *     100 * SMCLK_FREQ/1000000).
     * ST_CONTROL:
     *   - Clock source = SMCLK (~800 KHz from DCO after power-on/reset).
     *   - Clock input divider = 1 (ST_DIV_1).
     *   - Mode control = MC_1 (count to ST_CC0_COUNT then restart from 0).
     */
    ST_CC0_COUNT = (UD16)((100 * SMCLK_FREQ) / (UD32)1000000UL);
    ST_CONTROL = ST_SMCLK | ST_DIV_1 | MC_1;
    /*
     * Switch MCLK source to LFXT1 divided by 2 (4 MHz). This procedure is
     * described in the MPS430 manual.
     */
    while(1) {
        
        IFG1 &= ~OFIFG;
        
        /* wait ~100 us (the MSP430 manual says to wait at least 50 us) */
        ST_COUNTER = 0;
        ST_CC0_CONTROL &= ~CCIFG;
        while(!(ST_CC0_CONTROL & CCIFG));
        
        if ((IFG1 & OFIFG) == 0) break;
    }
    BCSCTL2 = SELM_3 | DIVM_1;  /* set DIVM_1 so MCLK = (8 MHz / 2) */
    
    /* stop system timer */
    ST_CONTROL = 0;
}

__inline void
AdpProc(void)
{
    ADP_MAIN_PRIV *am = &AdpMainPriv;
    BOOL clockOverdue;
    
    /* if any tasks are pending */
    if (AdpPub.pend) {
        
        /* USB RX (set by AdpUsbRxIsr()) */
        if (AdpPub.pend & ADP_USB_RX_PEND) AdpProcUsbRx();
        
        /* ADP SPI RX (set by AdpSpiRxIsr()) */
        if (AdpPub.pend & ADP_SPI_RX_PEND) AdpProcSpiRx();
        
    } else {
        /* AdpSleep(); */
    }
    
    /* if a clock period elapsed, do any periodic tasks that are pending */
    if (StElapsedMs(am->clockTimeMs, ADP_MS_PER_CLOCK)) {
        
        /* update time at which the last clock occurred */
        am->clockTimeMs += ADP_MS_PER_CLOCK;
        
        /* If the clock is overdue, set "clockOverdue" to tell the periodic
         * tasks. That way, if the micro-controller was busy doing something
         * for longer than ADP_MS_PER_CLOCK, the periodic tasks can avoid
         * performing tasks multiple times in rapid succession (if needed).
         */
        if (StElapsedMs(am->clockTimeMs, ADP_MS_PER_CLOCK)) {
            clockOverdue = TRUE;
        } else {
            clockOverdue = FALSE;
        }
            
        /* If the clock is on time (not overdue), and it's time to perform
         * the example periodic task, do so (note the ADP board currently
         * doesn't do any periodic tasks, but this example is provided for
         * future reference if needed). If the clock is overdue, skip the
         * task so we'll never do it multiple times in rapid succession.
         */
        if (!clockOverdue && (--am->clocksToExampleTask == 0)) {
                
            /* restart number of clocks to next example task */
            am->clocksToExampleTask = ADP_CLOCKS_PER_EXAMPLE_TASK;
            
            /* perform the task here (typically a function call) */
        }
    }
}

#ifdef TODO
__inline void
AdpSleep(void)
{
    /* disable interrupts so they won't set any pending flags */
    _disable_interrupts();
            
    /* If an interrupt occurred and set a pending flag before we disabled
     * interrupts, re-enable interrupts and continue processing loop.
     * Otherwise, switch the micro-controller to low power mode (stops
     * execution and enters low power mode until next interrupt).
     */
    if (AdpPub.pend) {
         _enable_interrupts();
    } else {
        _bis_SR_register(LPM4_bits | GIE);
    }
}
#endif

void
AdpErr(UD16 adpAppErrCode, const char *argFormats, ...)
{
    va_list va;

    /* init variable arguments list */
    va_start(va, argFormats);

    ErrSetVa(AdpAppErr, adpAppErrCode, argFormats, va);

    /* clean up variable args list */
    va_end(va);
}
