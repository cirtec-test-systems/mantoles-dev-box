/**************************************************************************
 * This file contains functions to interface to the A/D converter in the
 * MSP430 on the ADP board.
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Adp/General.h"              /* RSTAT, UINT, ... */
#include "Adp/Build/Build.h"          /* ADC12CTL0, VSUP1_ADC_INPUT, ... */
#include "Adp/AnyBoard/SysTimerLib.h" /* StDelayMs(), ...  */

/* local include shared by all source files for ADP board application */
#include "AdpApp.h"

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/**************************************************************************
 * Function Prototypes
 */
 
__inline UINT AdpAdc(UINT adcInput);
__inline UINT AdpAdcAve(UINT adcInput, UINT nSamp);
 
/**************************************************************************
 * Initialize the A/D converter interface on the ADP board. This uses the ADC
 * in the MSP430.
 */
RSTAT
AdpAdcInit(void)
{
    /* Select ADC functionality for the I/O pins used for ADC inputs. This
     * prevents parasitic current flow from VCC to GND when the input voltage
     * is near the transition level of the gate (recommended in MSP430 manual).
     */
    ADC_INPUT_P(SEL) |= (VBAT_SENSE_ADC_BIT | ISUP1_ADC_BIT | ISUP2_ADC_BIT | \
        BAT_CHARGE_DIFF_AMP_ADC_BIT | VSUP1_ADC_BIT | VSUP2_ADC_BIT);
    
    /* Turn ADC on and set it up as follows:
     * 
     * ADC12CTL0:
     *   ADC12ON: Turn ADC on.
     *   SHT0_3: Sample period = 32 ADC12CLK cycles.
     *   REFON: Enable internal reference voltage generator.
     *   REF2_5V: Internal reference voltage = 2.5v
     *   MSC: auto-start next sample (only applies to repeat & sequence modes)
     * 
     * ADC12CTL1:
     *   SHP: SAMPCON signal is sourced from the sampling timer (automatic).
     *   ADC12SSEL_1: Clock source (ADC12CLK) = ACLK (8 MHz).
     * 
     * The clock source (ACLK = 8 MHz) and sample period (32 ACLK cycles) were
     * determined based on "Sample Timing Considerations" in the MSP430 manual,
     * which gives the following equation for the minimum sample time (where Rs
     * is the external source resistance on the ADC input):
     * 
     *   Minimum tSamp = (Rs + 2kOhm) * 9.011 * 40pF + 800ns
     * 
     * The maximum external source resistance for any of the ADC inputs is
     * ~3kOhm (for the VSUP1, VSUP2, ISUP1, and ISUP2 ADC inputs), so the
     * ADC is configured as necessary for those inputs (for convenience, the
     * same settings are used for all ADC inputs). For 3kOhm, the equation
     * above yields the following:
     * 
     *   Minimum tSamp = (3kOhm + 2kOhm) * 9.011 * 40pF + 800ns = ~2.602 us
     * 
     * Setting the sample period to 32 ACLK cycles yields the closest actual
     * sample time. Note this allows ample padding in case the external source
     * resistance isn't exactly 3kOhm:
     * 
     *   Actual tSamp = 32 * (ACLK cycle) = 32 * 0.125 = 4us
     * 
     * From the MSP430 manual, tSync and tConvert are as follows:
     * 
     *   tSync = (ACLK cycle) / 2 = 0.125 / 2 = 0.0625us
     *   tConvert = 13 * (ACLK cycle) = 13 * (1 / 8MHz) = 1.625us 
     * 
     * Thus, the total time required to do each ADC sample is:
     *
     *   tTotal = tSync + tSamp + tConvert = 0.0625 + 4 + 1.625 = 5.6875 us.
     *   (for a theoretical maximum sample rate of 175824 samples/second
     *   which is nearly the data sheet maximum of 200K samples/second)
     */
    ADC12CTL0 = ADC12ON | SHT0_3 | REFON | REF2_5V | MSC;
    ADC12CTL1 = SHP | ADC12SSEL_1;
    
    /* SREFx = SREF_1: VR+ = VREF+ and VR? = AVSS */
    ADC12MCTL0 = SREF_1;
    
    /* wait 17 ms (to give VRef time to bias the storage capacitors) */
    StDelayMs(17);
    
    return(0);
}

__inline UINT
AdpAdc(UINT adcInput)
{
    /* select input */
    ADC12MCTL0 = SREF_1 | adcInput;
    
    /* enable conversions & start conversion */
    ADC12CTL0 |= (ENC | ADC12SC);
    
    /* wait for conversion to finish */
    while((ADC12IFG & BIT0) == 0);
    
    /* disable conversions (so input can be changed for future samples) */
    ADC12CTL0 &= ~ENC;
    
    /* read result (also clears interrupt flag) */
    return(ADC12MEM0);
}

__inline UINT
AdpAdcAve(UINT adcInput, UINT nSamp)
{
    UINT n, adc;
    
    /* take specified number of samples and add to sum */
    adc = 0;
    n = nSamp;
    AdpStartAutoAdc(adcInput);
    while(n) {adc += AdpGetAutoAdc(); --n;}
    AdpStopAdc();
        
    /* return the average */    
    return(adc/nSamp);
}

UINT
AdpVsup1Adc(UINT nSamp)
{
    /* if caller wants average, take multiple samples and return average */
    if (nSamp > 1) {
        return(AdpAdcAve(VSUP1_ADC_INPUT, nSamp));
    } else {
        return(AdpAdc(VSUP1_ADC_INPUT));
    }
}

UINT
AdpVsup2Adc(UINT nSamp)
{
    /* if caller wants average, take multiple samples and return average */
    if (nSamp > 1) {
        return(AdpAdcAve(VSUP2_ADC_INPUT, nSamp));
    } else {
        return(AdpAdc(VSUP2_ADC_INPUT));
    }
}
UINT
AdpIsup1Adc(UINT nSamp)
{
    /* if caller wants average, take multiple samples and return average */
    if (nSamp > 1) {
        return(AdpAdcAve(ISUP1_ADC_INPUT, nSamp));
    } else {
        return(AdpAdc(ISUP1_ADC_INPUT));
    }
}

UINT
AdpIsup2Adc(UINT nSamp)
{
    /* if caller wants average, take multiple samples and return average */
    if (nSamp > 1) {
        return(AdpAdcAve(ISUP2_ADC_INPUT, nSamp));
    } else {
        return(AdpAdc(ISUP2_ADC_INPUT));
    }
}
