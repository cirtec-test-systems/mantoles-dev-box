/**************************************************************************
 * This include file contains things that are shared by all of the source files
 * for the ADP board application. This is private to the ADP board application,
 * and should not be included by anything outside of the ADP board application.
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef AdpApp_h
#define AdpApp_h

#include "Standard/String.h"         /* memset(), NULL (for AdpErr() args) */
#include "Standard/StdArg.h"         /* va_list (for AdpErr() args) */
#include "Adp/General.h"             /* UINT, RSTAT, ... */
#include "Adp/Build/Build.h"         /* UD8, UD16, ... */   
#include "Adp/AnyBoard/ErrLib.h"     /* ERR_INFO */
#include "Adp/AnyBoard/TraceLib.h"   /* Trace0(), ... (for test/debug use) */
#include "Adp/AdpBoard/AdpGeneral.h" /* ADP_CONFIG, ... */
#include "Adp/AdpBoard/AdpAppErrs.h" /* AdpAppErr, ... */

/**************************************************************************
 * Defines and Macros
 */
 
/* Milliseconds per clock. This determines how often AdpProc() checks if
 * any periodic tasks need to be performed, and the minimum time period and
 * resolution for all periodic tasks, so it should be set accordingly.
 */
#define ADP_MS_PER_CLOCK  50

/* macro to get length of a constant string (excluding the '\0' terminator) */
#define ADP_LEN(constStr)  (sizeof(constStr) - 1)

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* structure of data shared by all source files for ADP board application */
typedef struct {
    
    /* Flags used to indicate tasks are pending & require processing. This is
     * declared volatile to prevent the compiler from assuming it won't change
     * during a section of code, since it may be changed by interrupts. For
     * bit definitions, see ADP_SPI_RX_PEND, etc.
     */
    volatile UD16 pend;
    
    /* Status flags for ADP_STAT.flags (see ADP_STAT, ADP_ERR_OCCURRED, etc.
     * in "AppDevPlat\Sw\Includes\Adp\AdpBoard\AdpGeneral.h"). This is declared
     * volatile to prevent the compiler from assuming it won't change during a
     * section of code, since it may be changed by interrupts.
     */
    volatile UD8 statFlags;
    

    /* from AdpAppPower.c to prevent ADC12 use while DMA is running */
    volatile BOOL8 dmaRunning;
    
    /* current ADP board config */
    ADP_CONFIG adpConfig;
    
} ADP_PUB;
/*
 * Defines for bits in ADP_PUB.pend (task pending flags).
 */
#define ADP_SPI_RX_PEND  (1 << 0)
#define ADP_USB_RX_PEND  (1 << 1) 
 
/**************************************************************************
 * Global Declarations
 */
 
/* data shared by all source files for ADP board application */
extern ADP_PUB  AdpPub;

/**************************************************************************
 * External Function Prototypes
 */
 
/* shared functions defined in AdpAppMain.c */
extern void AdpErr(UD16 adpAppErrCode, const char *argFormats, ...);
 
/* shared functions defined in AdpAppCom.c */
extern RSTAT AdpComInit(void); 
extern void AdpProcUsbRx(void);
extern void AdpProcSpiRx(void);
extern void AdpReceiveAndDiscard(UINT len);
extern RSTAT AdpReceive(void *buf, UINT len);
extern RSTAT AdpSendReply(const void *rep, UINT repLen);
extern RSTAT AdpSendReplyHdr(UD8 repType, UINT repLen);
extern RSTAT AdpSendReplyData(const void *data, UINT len);
extern RSTAT AdpSendErrInfoReply(UD8 repType, const ERR_INFO *ei);

/* shared functions defined in AdpAppCmd.c */
extern RSTAT AdpCmdInit(void);
extern RSTAT AdpCmdGetDevType(UINT cmdLen);
extern RSTAT AdpCmdGetDevName(UINT cmdLen);
extern RSTAT AdpCmdGetVer(UINT cmdLen);
extern RSTAT AdpCmdGetErr(UINT cmdLen);
extern RSTAT AdpCmdGetStat(UINT cmdLen);
extern RSTAT AdpCmdGetTraceMsg(UINT cmdLen);
extern RSTAT AdpCmdGetConfig(UINT cmdLen);
extern RSTAT AdpCmdSetConfig(UINT cmdLen);
extern RSTAT AdpCmdSetVsup(UINT cmdLen);
extern RSTAT AdpCmdStartPowMon(UINT cmdLen);
extern RSTAT AdpCmdStopPowMon(UINT cmdLen);
extern RSTAT AdpCmdGetPow(UINT cmdLen);

/* shared functions defined in AdpAppAdc.c */
extern RSTAT AdpAdcInit(void);
extern UINT AdpVsup1Adc(UINT nSamp);
extern UINT AdpVsup2Adc(UINT nSamp);
extern UINT AdpIsup1Adc(UINT nSamp);
extern UINT AdpIsup2Adc(UINT nSamp);

/* shared functions defined in AdpAppPower.c */
extern RSTAT AdpPowInit(void);
extern void AdpConfigPow(const ADP_POW_CONFIG *pc);
extern void AdpSetVsup(UINT vsup1Mv, UINT vsup2Mv);
extern void AdpStartPowMon(ADP_POW_MON_OPTIONS *pmo);
extern void AdpStopPowMon(void);
extern RSTAT AdpGetPowMonData(UINT *pDataLen, UINT *pInfoLen,
    char **pRepStruct, char **pRepData);

/**************************************************************************
 * Inline functions for the ADC in the MSP430.
 * 
 * AdpStartAdc() starts samples for the specified ADC input. Call AdpGetAdc()
 * to get each sample and start the next, and AdpStopAdc() to stop sampling.
 */
__inline void
AdpStartAdc(UINT adcInput)
{
    /* select ADC input for ADC12MEM0 (ADC output location 0) */
    ADC12MCTL0 = SREF_1 | adcInput;
    
    /* start first ADC conversion */
    ADC12CTL0 |= (ENC | ADC12SC);
}

/* Get the previous ADC sample and start the next. Note AdpStartAdc() must be
 * called before AdpGetAdc() (otherwise, AdpGetAdc() will never return).
 */
__inline UINT
AdpGetAdc(void)
{
    UINT prevAdc;
    
    /* wait for previous conversion to finish */
    while((ADC12IFG & BIT0) == 0);
    
    /* get previous conversion (note this is required to clear ADC12SC) */
    prevAdc = ADC12MEM0;
    
    /* start next conversion */
    ADC12CTL0 |= (ENC | ADC12SC);
    
    /* return previous conversion */
    return(prevAdc);
}

/* Start automatic sampling for the specified ADC input. To get each sample,
 * call AdpGetAutoAdc(). To stop sampling, call AdpStopAdc(). Note the ADC will
 * overwrite the previous sample every 5.6875 us, so if you need every sample,
 * you should disable interrupts while sampling, and get each sample before
 * it's overwritten.
 */
__inline void
AdpStartAutoAdc(UINT adcInput)
{
    /* select ADC input for ADC12MEM0 (ADC output location 0) */
    ADC12MCTL0 = SREF_1 | adcInput;
    
    /* config ADC to repeat single channel until ADC12CTL0.ENC is cleared */
    ADC12CTL1 |= CONSEQ_2;
    
    /* enable conversions & start automatic conversions */
    ADC12CTL0 |= (ENC | ADC12SC);
}

/* Get the next automatic ADC sample (for when the ADC is configured to
 * repeat samples automatically). Note this should only be called after
 * AdpStartAutoAdc() is called to start automatic ADC samples (otherwise, it
 * will never return). Note the ADC will overwrite the previous sample every
 * 5.6875 us, so if you need every sample, you should disable interrupts while
 * sampling, and get each sample before it's overwritten.
 */
__inline UINT
AdpGetAutoAdc(void)
{
    /* wait for next conversion to finish */
    while((ADC12IFG & BIT0) == 0);
    
    /* read result (also clears ADC12IFG.BIT0) */
    return(ADC12MEM0);
}

/* Stop ADC samples and disable the ADC. Note any sample read from ADC12MEM0
 * after this is called might be invalid.
 */
__inline
AdpStopAdc(void)
{
    /* clear ADC12CTL1.CONSEQ, then ADC12CTL0.ENC (stop immediately) */
    ADC12CTL1 &= ~CONSEQ_3;
    ADC12CTL0 &= ~ENC;
    
    /* ensure interrupt flag from any conversion is clear */
    ADC12IFG = 0;
}

#endif /* ensure this file is only included once */
