/**************************************************************************
 * This file contains functions to manage the power on the ADP board
 * (VSUP1, VSUP2, etc.).
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Standard/String.h"          /* memset() */
#include "Adp/General.h"              /* RSTAT, ... */
#include "Standard/StdLib.h"          /* abs() */
#include "Adp/Build/Build.h"          /* DAC12_0CTL, CAL1_BIT, CAL1_P(), ... */
#include "Adp/AnyBoard/SysTimerLib.h" /* StDelayUs(), ... */
#include "Adp/AdpBoard/AdpGeneral.h"  /* ADP_POW_CONFIG, ADP_VSUP_MV_TO_ADC, ... */

/* local include shared by all source files for ADP board application */
#include "AdpApp.h"

/**************************************************************************
 * Defines and Macros
 */
 
/* Macro to calculate the approximate DAC setting for a VSUP1 or VSUP2 voltage
 * level. The relationship is linear. The equation was derived experimentally
 * by identifying two known points on the line.
 */
#define ADP_VSUP_MV_TO_DAC(vsupMv)  \
    (UD16)((((UD32)3718 - (vsupMv)) * 1840) / 2000)
    
/* Defines for the VSUP1/2 initialization loop in AdpSetVsup().
 * ADP_DAC_DELTA_FOR_VSUP_INIT determines the amount the initialization loop
 * will increase the voltage on each iteration (~15 mv), which determines how
 * fast the voltage will rise. ADP_ADC_UNDERSHOOT_FOR_VSUP_INIT determines the
 * amount by which the initialization loop will undershoot the target
 * voltage (~30 mv). Note the undershoot (~30 mv) should be larger than the
 * delta (~15 mv) to ensure the initialization loop won't overshoot the
 * target voltage. Also, the delta must be small enough to ensure the output
 * voltage can keep up (i.e. the DAC must not be increased faster than the
 * output voltage can change).
 */
#define ADP_DAC_DELTA_FOR_VSUP_INIT \
    (ADP_VSUP_MV_TO_DAC(0) - ADP_VSUP_MV_TO_DAC(15))
/**/
#define ADP_ADC_UNDERSHOOT_FOR_VSUP_INIT  ADP_VSUP_MV_TO_ADC(30)
    
/* Note on naming of some #defined symbols:
 * Consider the example "ADC12DIV_1".  The "_1" is the pattern
 * that contributes to the total, and in this case
 * the "1" means "divide by 2."  The 3 bits (001) are 
 * shifted left in the #define that defines ADC12DIV_1
 *    
 *    
 * TI does not use the above convention for fields
 * that are only 1 bit.  This makes it difficult to audit code
 * to check that all the fields of a register have been set 
 * with intention.  Therefore defines are provided here
 * to facilitate auditing.
 */
      
/* ADC12 Invert Sample Hold Signal */
#define ISSH_0                 (0*ISSH)
#define ISSH_1                 (1*ISSH)

/* ADC12 Sample/Hold Pulse Mode */
#define SHP_0                  (0*SHP)
#define SHP_1                  (1*SHP)

/* ADC12 Busy (is read-only) */
#define ADC12BUSY_0            (0*ADC12BUSY)

/* ADC12 End of Sequence */
#define EOS_0   (0*EOS)
#define EOS_1   (1*EOS)

    
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* structure of private data for power setting and monitoring */
typedef struct {
    
    /* The poll-for-new-data command from the PC will update
     * this if new data is available.  Then the get data 
     * function will use it.
     */
    SD16 dmaSnAvailable;
     
    /* When acquiring ADC12 data by DMA, the DMA ISR will run each
     * time a DMA buffer block is filled until it is stopped by the code.
     * The background code will examine the Serial Number variable to 
     * learn when a DMA block has been filled and so a block of data
     * is ready for sending to the PC.  Since the serial number can 
     * change in the ISR it is declared volatile.
     * sn = Serial Number
     */
    volatile SD16 isrDmaBufferSn;
    
    /* The count setting for the timer is calculated by the PC host
     * and sent as part of the command that starts the DMA
     */
    UD16 dmaTimerBword;
    
    /* These are global versions of the arguments passed
     * in a command
     */
    int adcChanToDma;   
    
    BOOL bTwoChannelMode;
    
    SD16 * pDmaBufferReadyToSend;
    
    /* Currently includes only Split buffer mode flag.  */
    UD16 dmaOtherSettings;    
    
    /* ADC12 samples are 12 bits unsigned, but
     * to do math on them that can produce negative results,
     * like for trigger compare for rising and falling edges,
     * it easier to make these signed.
     */ 
    SD16 dmaEvenBlk[NR_SAMPS_IN_DMA_BUFFER];
    SD16 dmaOddBlk[NR_SAMPS_IN_DMA_BUFFER];
                        
} ADP_POW_MON_PRIV;

/**************************************************************************
 * Global and Static Definitions
 */
 
/* instantiate only one of the above structures */
static ADP_POW_MON_PRIV AdpPowMonPriv;
  
/**************************************************************************
 * Function Prototypes
 */
__interrupt void AdpPowDmaIsr(void);
 
/**************************************************************************
 * Initialize the power interface on the ADP board (VSUP1, VSUP2, etc.). Note
 * AdpAdcInit() must be called before AdpPowInit() so the internal VRef is
 * ready for the DAC's.
 */
RSTAT
AdpPowInit(void)
{
    ADP_POW_CONFIG *pc = &AdpPub.adpConfig.pow;
    
    /* Init the DAC's for VSUP1 & VSUP2 (voltage supplies to mezzanine board)
     * and calibrate them. Note AdpAdcInit() must be called before AdpPowInit()
     * so the internal VRef is ready for the DAC's.
     */
    DAC12_0CTL = DAC12IR | DAC12AMP_2;
    DAC12_1CTL = DAC12IR | DAC12AMP_2;
    DAC12_0CTL |= DAC12CALON;
    while(DAC12_0CTL & DAC12CALON); /* up to 100 ms for DAC12AMP_2 setting */
    DAC12_1CTL |= DAC12CALON;
    while(DAC12_1CTL & DAC12CALON); /* up to 100 ms for DAC12AMP_2 setting */
    
    /* Init any non-zero defaults we want in the power config (AdpInit() clears
     * all other fields before calling AdpPowInit()), then call AdpConfigPow().
     * 
     * Note ADP_VSUP1_MV and ADP_VSUP2_MV depend on the needs of the mezzanine
     * board attached to the ADP board, and therefore, they are defined in a
     * mezzanine-specific build include for the ADP board (for a description
     * of build includes and how they are used, see "Adp/Build/Build.h").
     */
    pc->vsup1Mv = ADP_VSUP1_MV;
    pc->vsup2Mv = ADP_VSUP2_MV;
    /**/
    AdpConfigPow(pc);
    
    /* initialize private variables used in this source file */
    AdpPowMonPriv.dmaSnAvailable = -1;
     
    AdpPowMonPriv.isrDmaBufferSn = -1;
        
    AdpPowMonPriv.dmaTimerBword = 0;
    
    /* this 0 default would convert XXXX and not 
     * the current measurement channels 
     */
    AdpPowMonPriv.adcChanToDma = 0;   
    
    AdpPowMonPriv.bTwoChannelMode = FALSE;;
        
    /* Currently includes only Split buffer mode flag.  */
    AdpPowMonPriv.dmaOtherSettings  = 0;

    return(0);
}

void
AdpConfigPow(const ADP_POW_CONFIG *pc)
{
    /* if specified different power config, save new power config */
    if (pc != &AdpPub.adpConfig.pow) AdpPub.adpConfig.pow = *pc;
    
    /* set VSUP1 and VSUP2 */
    AdpSetVsup(pc->vsup1Mv, pc->vsup2Mv);
}

/* Set VSUP1 and VSUP2 (voltage supplies to mezzanine board). If 0 is
 * specified, the corresponding voltage supply is turned off.
 */
void
AdpSetVsup(UINT vsup1Mv, UINT vsup2Mv)
{
    ADP_POW_CONFIG *pc = &AdpPub.adpConfig.pow;
    UINT adc1, adc1Targ, adc1Init;
    UINT adc2, adc2Targ, adc2Init;
    int delta1, delta2;
    UINT nSamp;
    BOOL done;
    
    /* Init default values for local variables:
     * 
     * adc1Init, adc2Init:
     *     These indicate if VSUP1/2 need to be initialized, and the ADC level
     *     to initialize them to (0 = don't need to initialize VSUP1/2).
     * adc1Targ, adc2Targ:
     *     These indicate if VSUP1/2 need to be adjusted, and the new target
     *     ADC level (0 = don't need to adjust VSUP1/2).
     */
    adc1Init = adc2Init = 0;
    adc1Targ = adc2Targ = 0;
    
    /* if changing VSUP1, or it's currently off (to force power-on init) */
    if ((vsup1Mv != pc->vsup1Mv) || (VSUP1_OFF_P(IN) & VSUP1_OFF_BIT)) {
        
        /* update VSUP1 in config */
        pc->vsup1Mv = vsup1Mv;
        
        /* enable CAL1 resistor so VSUP1 will change faster */
        CAL1_P(OUT) |= CAL1_BIT;
        
        /* if specified VSUP1 on */
        if (vsup1Mv) {
            
            /* calc target ADC level for VSUP1 */
            adc1Targ = ADP_VSUP_MV_TO_ADC(vsup1Mv);
            
            /* if VSUP1 is off, calc ADC level for init and turn it on */
            if (VSUP1_OFF_P(IN) & VSUP1_OFF_BIT) {
            
                adc1Init = adc1Targ - ADP_ADC_UNDERSHOOT_FOR_VSUP_INIT;
                DAC12_0DAT = ADP_VSUP_MV_TO_DAC(0);
                VSUP1_OFF_P(OUT) &= ~VSUP1_OFF_BIT;
            }
            
        } else { /* else, turn off VSUP1 */
            
            VSUP1_OFF_P(OUT) |= VSUP1_OFF_BIT;
        }
    }
    
    /* if changing VSUP2, or it's currently off (to force power-on init) */
    if ((vsup2Mv != pc->vsup2Mv) || (VSUP2_OFF_P(IN) & VSUP2_OFF_BIT)) {
        
        /* update VSUP2 in config */
        pc->vsup2Mv = vsup2Mv;
        
        /* enable CAL2 resistor so VSUP2 will change faster */
        CAL2_P(OUT) |= CAL2_BIT;
        
        /* if specified VSUP2 on */
        if (vsup2Mv) {
            
            /* calc target ADC level for VSUP2 */
            adc2Targ = ADP_VSUP_MV_TO_ADC(vsup2Mv);
            
            /* if VSUP2 is off, calc ADC level for init and turn it on */
            if (VSUP2_OFF_P(IN) & VSUP2_OFF_BIT) {
            
                adc2Init = adc2Targ - ADP_ADC_UNDERSHOOT_FOR_VSUP_INIT;
                DAC12_1DAT = ADP_VSUP_MV_TO_DAC(0);
                VSUP2_OFF_P(OUT) &= ~VSUP2_OFF_BIT;
            }
            
        } else { /* else, turn off VSUP2 */
            
            VSUP2_OFF_P(OUT) |= VSUP2_OFF_BIT;
        }
    }
    
    /* If VSUP1 or VSUP2 need to be initialized (turned on), increase them
     * rapidly until they've reached their initial levels.
     */
    if (adc1Init | adc2Init) {
        
        done = FALSE;
        while(!done) {
        
            done = TRUE;
            
            /* if VSUP1 is too low, increase it */
            if ((adc1 = AdpVsup1Adc(1)) < adc1Init) {
                
                /* if new DAC setting < 50, fail (shouldn't happen) */
                if ((DAC12_0DAT -= ADP_DAC_DELTA_FOR_VSUP_INIT) < 50) {
                    AdpErr(ADP_APP_ERR_FAILED_TO_SET_VSUP1, NULL);
                    goto error;
                }
                done = FALSE;
            }
            
            /* if VSUP2 is too low, increase it */
            if ((adc2 = AdpVsup2Adc(1)) < adc2Init) {
            
                /* if new DAC setting < 50, fail (shouldn't happen) */
                if ((DAC12_1DAT -= ADP_DAC_DELTA_FOR_VSUP_INIT) < 50) {
                    AdpErr(ADP_APP_ERR_FAILED_TO_SET_VSUP2, NULL);
                    goto error;
                }
                done = FALSE;
            }
        }    
    }
    
    /* Adjust VSUP1 and VSUP2 until they reach their target levels. First
     * adjust using single ADC samples to quickly get close to the specified
     * voltages, then repeat using averaging to achieve the best accuracy.
     */
    for(nSamp = 1;; nSamp = 15) {
        
        /* get first ADC sample and determine DAC delta for each VSUP */
        if (adc1Targ) {
            adc1 = AdpVsup1Adc(nSamp);
            delta1 = (adc1 < adc1Targ) ? -1 : 1;
        }
        if (adc2Targ) {
            adc2 = AdpVsup2Adc(nSamp);
            delta2 = (adc2 < adc2Targ) ? -1 : 1;
        }
        
        /* adjust VSUP1 & VSUP2 until they reach the target levels */
        done = FALSE; 
        while(!done) {
            
            done = TRUE;
            
            /* if adjusting VSUP1 */
            if (adc1Targ) {
                
                /* if VSUP1 hasn't reached its target level, adjust it */
                if (((delta1 > 0) && (adc1 > adc1Targ)) ||
                    ((delta1 < 0) && (adc1 < adc1Targ))) {
            
                    /* if new DAC setting = 0, fail (shouldn't happen) */
                    if ((DAC12_0DAT += delta1) == 0) {
                        AdpErr(ADP_APP_ERR_FAILED_TO_SET_VSUP1, NULL);
                        goto error;
                    }
                    done = FALSE;
                }
                adc1 = AdpVsup1Adc(nSamp);
            }
            
            /* if adjusting VSUP2 */
            if (adc2Targ) {
                
                /* if VSUP2 hasn't reached its target level, adjust it */
                if (((delta2 > 0) && (adc2 > adc2Targ)) ||
                    ((delta2 < 0) && (adc2 < adc2Targ))) {
            
                    /* if new DAC setting = 0, fail (shouldn't happen) */
                    if ((DAC12_1DAT += delta2) == 0) {
                        AdpErr(ADP_APP_ERR_FAILED_TO_SET_VSUP2, NULL);
                        goto error;
                    }
                    done = FALSE;
                }
                adc2 = AdpVsup2Adc(nSamp);
            }
        }
        
        /* if we checked the average ADC levels too, stop (done) */
        if (nSamp > 1) break;
    }
        
    /* if VSUP1/2 is on, disable its calibration resistor */
    if (vsup1Mv) CAL1_P(OUT) &= ~CAL1_BIT;
    if (vsup2Mv) CAL2_P(OUT) &= ~CAL2_BIT;
    
    /* return (done) */
    return;
    
error:
    /* disable VSUP1 and VSUP2 to be safe (must be some problem) */
    CAL1_P(OUT) |= CAL1_BIT;
    CAL2_P(OUT) |= CAL2_BIT;
    VSUP1_OFF_P(OUT) |= VSUP1_OFF_BIT;
    VSUP2_OFF_P(OUT) |= VSUP2_OFF_BIT;
}


/* Start the DMA of ADC12 data for monitoring the current
 * measuring circuits.  Since the ADC12 is re-configured for
 * DMA use it cannot be used by the functions that read
 * the ADC12 directly while this DMA is running.   
 */
void 
AdpStartDMAforPower()
{
    volatile unsigned int i;
  
    /* make sure timer is in STOP mode */
    TBCTL = MC_0;
    
    /* make sure DMAs are not running */       
    DMA0CTL = 0;   /* Config */
    DMA1CTL = 0;   /* Config */
  
  
    /* clear DMA buffer and variables */
    AdpPowMonPriv.isrDmaBufferSn = -1;
    AdpPowMonPriv.dmaSnAvailable = -1;
    
    AdpPub.dmaRunning = FALSE;
      
    /*
     * Conversion Memory Control: this is the first location of
     * a table that defines the sequence of conversions
     * EOS|SREFx|INCHx (EOS,Select Reference,Input channel)
     * e   SSS   IIII
     * 0              -> not end of sequence
     *     001        -> VR+=VREF+, VR-=AVSS (SREF_1)
     *           1010 -> temperature diode
     * 0   001   1010 = 0x01A
     * ADC12MCTL0 = 0x01A;  
    */

    /* 
     * For this DMA mode, the ADC12 will perform either 1 or 2
     * ADC conversions, then start over.  
     * We can always set both locations of the 
     * ADC12 control memory, and if the mode is not set
     * for sequence, it will just use the first location.
     * If the mode *is* set for sequence, it will use both,
     * then start over.
     */
      
    /* Arg is passed in message with the channel byte */
    ADC12MCTL0 = EOS_0 + SREF_1 + AdpPowMonPriv.adcChanToDma;  
    /* Arg is passed in message with the channel byte */
    ADC12MCTL1 = EOS_1 + SREF_1 + (AdpPowMonPriv.adcChanToDma+1);


    /* 
     * ADC12IFGx Bits 15-0:
     * ADC12MEMx Interrupt flag. These bits are set when corresponding
     * ADC12MEMx is loaded with a conversion result. The ADC12IFGx bits are
     * reset if the corresponding ADC12MEMx is accessed, or may be reset with
     * software.
     * 0 No interrupt pending
     * 1 Interrupt pending
     */
      
    /* jcb clears all flags and so indicates NO */
    /* memories have valid sample values yet */
    ADC12IFG = 0x00;
        
    /*  
     * ADC12CTL1, ADC12 Control Register 1
     *     
     * xxxx CSTART
     *     xx SHSx
     *       x SHP
     *        x ISSH
     *         xxx ADC12DIVx
     *         |  xx ADC12SSELx
     *         |  | xx CONSEQx
     *         |  |   x ADC12BUSY
     *         |  |
     * 0000    |  |     first conversion goes to ADC15MEMx where x=this 4 bits
     *     11  |  |     sam/hold src sel=TIMERB.OUT1
     *       0 |  |     SAMPCON signal is sourced from the sample-input signal.
     *        0|  |     The sample-input signal is not inverted.
     *         000|     ADC12 clock divider = /1
     *            00    ADC12 clock source select=00 ADC12OSC
     *              10  Conversion sequence mode select=Repeat-single-channel
     *                0 ADC12 busy.
     */

    /*     
     * Here we use the ACLK because this is what
     * is used in other ADP code for the ADC12.  The TI
     * example was using the ADC12OSC which is described as
     * "about 5 MHz."  Since ACLK is 4 MHz, the divider
     * should be 1 to give close to the same conversion time.  
     * Divider of 1 is selected by "000" = 0 in that field.
     * 
     * Here is how AdpAdcInit() sets this:
     * ADC12CTL1 = SHP | ADC12SSEL_1;
     * 
     * Here is how the TI example code in fet140_dma_09.c sets it:    
     * ADC12CTL1 = SHS_3 + CONSEQ_2;   // S&H TB.OUT1, rep. single chan
     * 
     * SHP sets the source of the sampling signal.  
     * 
     * Microsemi used SHP_1 = SAMPCON signal is sourced from the
     * sampling timer.
     * 
     * TI used SHP_0 = SAMPCON signal is sourced from the sample
     * input signal (which is TimerB TB.OUT1) and arranged for
     * TB.OUT1 to have a particular duty cycle to perform the
     * Sample/Hold function.
     * 
     * There is an ADC12 bug reported in SLAZ018B (bug name is ADC18).
     * It is only associated with SHP=0, so I concluded it would be
     * prudent to use SHP=1 mode to avoid it.
     * 
     * ----------------
     *     
     * ADC18 - Bug description:
     * 
     * Module: ADC12, incorrect conversion result in extended sample mode
     * The ADC12 conversion result can be incorrect in the case where the
     * extended sample mode is selected (SHP = 0), the conversion clock
     * is not the internal ADC12 oscillator (ADC12SSEL > 0), and one of
     * the following two conditions is true:
     * 
     * 1.) The extended sample input signal SHI is asynchronous to the
     * clock source used for ADC12CLK and the undivided ADC12 input 
     * clock frequency exceeds 3.15 MHz
     * Or
     * 2.) The extended sample input signal SHI is synchronous to the 
     * clock source used for ADC12CLK and the undivided ADC12 input 
     * clock frequency exceeds 6.3 MHz.
     * 
     * Workaround:
     * 1.) Use the pulse sample mode (SHP = 1)
     * Or
     * 2.) Use the ADC12 internal oscillator as the ADC12 clock source
     * Or
     * 3.) Limit the undivided ADC12 input clock frequency to 3.15 MHz
     * Or
     * 4.) Use the same clock source (such as ACLK or SMCLK) to derive
     * both SHI and ADC12CLK in order to achieve synchronous operation,
     * and also limit the undivided ADC12 input clock frequency
     * to 6.3 MHz
     * ----------------
     * 
     */

    /*
     * Conversion Mode Summary
     * CONSEQx Mode Operation
     * CONSEQ_0 00 Single channel single-conversion A single channel is converted 
     *             once.
     * CONSEQ_1 01 Sequence-of channels A sequence of chans is converted once.
     * CONSEQ_2 10 Repeat-singlechannel A single channel is converted repeatedly.
     * CONSEQ_3 11 Repeat-sequence of channels A sequence of channels is converted       
     */

    if (AdpPowMonPriv.bTwoChannelMode == FALSE) {
      ADC12CTL1 = CSTARTADD_0 + SHS_3 + SHP_1 + ISSH_0 + ADC12DIV_0 
        + ADC12SSEL_1 + CONSEQ_2 + ADC12BUSY_0; 
    }
    
    /*
     * For acquiring two ADC channels of data at the same time, 
     * we need to use a sequence in the ADC12 block.  The 
     * only difference is CONSEQ_3 since now 2 ADC12 conversions
     * will happen for every timer trigger (the sequence calls for
     * *2* ADC conversions to get to the End-of-Sequence).
     */ 
    if (AdpPowMonPriv.bTwoChannelMode == TRUE) {
        ADC12CTL1 = CSTARTADD_0 + SHS_3 + SHP_1 + ISSH_0 + ADC12DIV_0 
            + ADC12SSEL_0 + CONSEQ_3 + ADC12BUSY_0;
    }


    
    /*   
     * ADC12CTL0, ADC12 Control Register 0
     * 
     *  xxxx SHT1x
     *  |   xxxx SHT0x
     *  |   |   x MSC
     *  |   |   |x REF2_5V
     *  |   |   ||x REFON
     *  |   |   || x ADC12ON
     *  |   |   ||  x ADC12OVIE
     *  |   |   ||  |x ADC12TOVIE
     *  |   |   ||  | x ENC
     *  |   |   ||  |  x ADC12SC
     *  |   |   ||  |  |
     *  0011|   ||  |  | Sample-and-hold time 0=32 ADC12CLK cycles (ADC12MEM8-15)
     *      0011||  |  | Sample-and-hold time 0=32 ADC12CLK cycles (ADC12MEM0-7)                                              
     *          0|  |  | The sampling timer requires a rising 
     *           |  |  | edge of the SHI signal to trigger
     *           |  |  | each sample-and-conversion.
     *           1  |  | Reference generator voltage 1=2.5 V
     *            1 |  | 1=Reference on
     *             1|  | 1=ADC12 on
     *              0  | 0=Overflow interrupt disabled
     *               0 | 0=Conversion time overflow interrupt disabled
     *                1| 1=ADC12 enabled
     *                 0 0= No sample-and-conversion-start                  
     *  ____----____----
     */             

    /* VRef ADC12 on, enabled */
    ADC12CTL0 = SHT0_3 + SHT1_3 + REF2_5V + REFON + ADC12ON + ENC;     \
  
    for (i = 0xFFF; i > 0; i--);              /* Time VRef to settle */

    /* Timer_B capture/compare 0
     *  
     * The up mode is used if the timer period must be different
     * from TBR(max) counts. The timer repeatedly counts up to 
     * the value of compare latch TBCL0, which defines the period,
     * as shown in Figure 12-2. The number of timer counts in
     * the period is TBCL0+1. When the timer value equals TBCL0
     * the timer restarts counting from zero. If up mode is 
     * selected when the timer value is greater than TBCL0, the
     * timer immediately restarts counting from zero.
     * 
     * We need to double the ADC12 trigger rate when we are using
     * the split mode to get the requested sample rate.  This is
     * because we are using a sequence of 2 ADC12 conversions and
     * we are NOT using Multiple Sample and Convert mode
     * so each ADC12 trigger causes only ONE conversion
     * of the sequence of length 2.  
     * The reason for this choice is to space the sampling for the
     * alternating channels as far apart as possible to minimize any
     * interaction due to the analog muxing of the inputs.
     */
    TBCCR0 = AdpPowMonPriv.dmaTimerBword; /* Init TBCCR0 w/sample prd */
  
    /* Timer_B capture/compare 1 
     *   
     * In SHP_1 mode the duty cycle of this signal does not
     * matter since it is NOT being used as the Sample/Hold
     * source for the ADC12
     */
    TBCCR1 = AdpPowMonPriv.dmaTimerBword - 1;   /*Trigger for ADC12 SC */
    
    /* TBCCTLx, Capture/Compare Control Register
     * ||||||||||
     * xx CMx
     *   xx CCISx
     *     x SCS
     *      xx CLLDx
     *        x CAP
     *         xxx OUTMODx
     *            x CCIE
     *             x CCI
     *              x OUT
     *               x COV
     *                x CCIFG
     * 
     * 00               Capture mode 00=No capture
     *   00             Capture/compare input select 00=CCIxA
     *     0            Synchronize capture source 0=Asynchronous capture 
     *      00          Compare latch load 00=TBCLx loads on write to TBCCRx
     *        0         Capture mode 0=Compare mode
     *         111      Output mode 111=Reset/set
     *            0     Capture/compare interrupt enable 0=Interrupt disabled
     *             0    Capture/compare input (read in this bit) 
     *              0   Output 0=Output low
     *               0  Capture overflow 0=No capture overflow occurred
     *                0 Capture/compare interrupt flag 0=No interrupt pending
     * ____----____---- 
     * 
     */

    
    /*Reset OUT1 on EQU1, set on EQU0 */
    TBCCTL1 = OUTMOD_7;   

    /*  DMA channel 0 source address
     *  xxxx xxxx xxxx xxxx
     */    
    DMA0SA = (unsigned int)&ADC12MEM0;    /*Src address = ADC12 module */
    DMA1SA = (unsigned int)&ADC12MEM1;    /*Src address = ADC12 module */
  
    /* DMA channel 0 destination address
     * xxxx xxxx xxxx xxxx 
     */
    DMA0DA = (unsigned short) &AdpPowMonPriv.dmaEvenBlk[0];    /* */      
    DMA1DA = (unsigned short) &AdpPowMonPriv.dmaEvenBlk[NR_SAMPS_IN_DMA_BUFFER/2];    /* */      
  
    /* DMA channel 0 transfer size
     * xxxx xxxx xxxx xxxx
     *   
     * For two channels of ADC, we have to split this buffer 
     * into two buffers 
     */  
    if (AdpPowMonPriv.bTwoChannelMode == TRUE) {
      DMA0SZ = NR_SAMPS_IN_DMA_BUFFER/2;  /* Size in words */
      DMA1SZ = NR_SAMPS_IN_DMA_BUFFER/2;  /* Size in words */
    }
    else {
      DMA0SZ = NR_SAMPS_IN_DMA_BUFFER;  /* Size in words */
    }
    
    /*  DMA control 0
     *  
     *  There is only ONE instance of this register, and
     *  it is shared by all 3 DMA controllers
     *  
     *  From TI slau049f.pdf (User's Guide):
     *  
     *  0110:
     * A transfer is triggered by an ADC12IFGx flag. When 
     * single-channel conversions are performed, the 
     * corresponding ADC12IFGx is the trigger. When 
     * sequences are used, the ADC12IFGx for the last 
     * conversion in the sequence is the trigger. A 
     * transfer is triggered when the conversion is 
     * completed and the ADC12IFGx is set. Setting the
     * ADC12IFGx with software will not trigger a transfer.
     * All ADC12IFGx flags are automatically reset when 
     * the associated ADC12MEMx register is accessed by 
     * the DMA controller. 
     *
     * xxxx Reserved
     *     xxxx DMA2TSELx
     *         xxxx DMA1TSELx
     *             xxxx DMA0TSELx
     * 
     * 0000             Reserved
     *     0000         DMA2 trigger select 0000=DMAREQ bit (software trigger)
     *         0000     DMA1 trigger select 0110=ADC12 ADC12IFGx bit
     *             0110 DMA0 trigger select 0110=ADC12 ADC12IFGx bit
     * ____----____----
     * 
     */
    
    /* both DMAs trigger from the same signal */   
    DMACTL0 = DMA1TSEL_6+ DMA0TSEL_6; 
  
    /* Do this as close as possible to the enabling of the DMA:
     * clears all flags and so indicates NO     
     * memories have valid sample values yet
     */
    ADC12IFG = 0x00;    
    
    /*  
     * DMA channel 0 control
     * 
     * x Reserved
     *  xxx DMADTx
     *     xx DMADSTINCRx
     *       xx DMASRCINCRx
     *         x DSTBYTE
     *          x SRCBYTE
     *           x DMALEVEL
     *            x DMAEN
     *            |x DMAIFG
     *            | x DMAIE
     *            | |x ABORT
     *            | | x DMAREQ   
     * 0          | |   Reserved 
     *  000       | |   DMA Transfer mode 000=Single transfer
     *     11     | |   11=Destination address is incremented
     *       00   | |   00=Source address is unchanged
     *         0  | |   0=Word
     *          0 | |   0=Word
     *           0| |   0=Edge sensitive (rising edge)
     *            1 |   1=Enabled
     *             0|   0=No interrupt pending
     *              1   1=Enabled
     *               0  0=DMA transfer not interrupted(by NMI)
     *                0 0=No DMA start
     * ____----____----
     * 
     */ 
    DMA0CTL = DMADSTINCR_3 + DMAIE + DMAEN;  
  
    if (AdpPowMonPriv.bTwoChannelMode == TRUE) {
       DMA1CTL = DMADSTINCR_3 + DMAIE + DMAEN; 
    }


    /*
     * x Unused
     *  xx TBCLGRPx
     *    xx CNTLx
     *      x Unused
     *       xx TBSSELx
     *       | xx IDx
     *       |   xx MCx
     *       |   | x Unused
     *       |   |  x TBCLR
     *       |   |  |x TBIE
     *       |   |  | x TBIFG
     * 0     |   |  |
     *  00   |   |  |   Each TBCLx latch loads independently
     *    00 |   |  |   16-bit, TBR(max) = 0FFFFh
     *      0|   |  |
     *       10  |  |   SMCLK (see SMCLK_FREQ = 800000UL)
     *         00|  |   /1
     *           01 |   Up mode: the timer counts up to TBCL0
     *             0|
     *              1   Timer_B clear. Setting this bit resets TBR, 
     *                  the clock divider, and the count direction. 
     *                  The TBCLR bit is automatically reset and is 
     *                  always read as zero.
     *               0  Interrupt disabled  
     *                0 No interrupt pending
     * ____----____----
     *  
     */

    /*/1, ACLK, up mode, clear TBR */
    TBCTL = ID_0+ TBSSEL_1+ MC_1 + TBCLR;  

    AdpPub.dmaRunning = TRUE;
}

void
AdpStartPowMon(ADP_POW_MON_OPTIONS *pmo)
{
    /* Check for error in commands: if we get any start
     * command (either to set bits only or to start)
     * while the DMA is running this is an error
     */
    if (AdpPub.dmaRunning) {
        /* report error */
        AdpErr(ADP_APP_ERR_ILLEGAL_CMD_WHILE_DMA_RUNNING, NULL);
        /* goto error; this was used only to turn off
         *  CAL bits and disable Vsups so is not needed here 
         */
        AdpStopPowMon();   
        /* ??how to ask caller to return not OK? */    
        /* perhaps when the next poll happens the poll code
         * will notice that DMA is not running and return an error?
         */  
        return; /* but no error was flagged here for caller?? */ 
    }      
         
        
    /* check for setting the bits */
    if (pmo->input == ADP_POW_NONE) {
                 
        /* SET the physical port bits as requested in the command */
        if ((pmo->options & ADP_HIGH_RANGE_POW_1) != 0) {
            RANGE1_P(OUT) |= RANGE1_BIT;    /* sets to 1 */
        } else {
            RANGE1_P(OUT) &= ~RANGE1_BIT;   /* sets to 0 */
        }
        
        if ((pmo->options & ADP_CAL_POW_1) != 0) {
            /* now turn ON extra current analog switch */
            CAL1_P(OUT) |= CAL1_BIT;                        
        } else         {
            /* turn extra CAL current analog switch OFF */                        
            CAL1_P(OUT) &= ~CAL1_BIT;
        }

        if ((pmo->options & ADP_HIGH_RANGE_POW_2) != 0) {
            RANGE2_P(OUT) |= RANGE2_BIT;    /* sets to 1 */
        } else {
            RANGE2_P(OUT) &= ~RANGE2_BIT;   /* sets to 0 */
        }
        
        if ((pmo->options & ADP_CAL_POW_2) != 0) {
            /* now turn ON extra current analog switch */
            CAL2_P(OUT) |= CAL2_BIT;                        
        } else         {
            /* turn extra CAL current analog switch OFF */                        
            CAL2_P(OUT) &= ~CAL2_BIT;
        }
    } else {

        /* convert the usec sample rate from the PC into the
         * timerB cycle count as follows:
         *         
         * (usec/sample)/(1000000 usec/second) = (seconds/sample)
         *
         * (ACLK clocks/second)*(seconds/sample) = ACLK clocks/sample
         *
         * (pmo->usPerSamp/1000000)* 8000000 = ACLK clocks/sample
         *
         * or (pmo->usPerSamp) * 8 = ACLK clocks/sample
         */

        AdpPowMonPriv.dmaTimerBword   = pmo->usPerSamp * 8;
           
        if(!AdpPub.dmaRunning) {
        
            /* assume this will be false */
            AdpPowMonPriv.bTwoChannelMode = FALSE;
                    
            if (pmo->input == ADP_POW_1) {
                AdpPowMonPriv.adcChanToDma = 1;
            } else
            if (pmo->input == ADP_POW_2) {
                AdpPowMonPriv.adcChanToDma = 2;
            } else
            if (pmo->input == ADP_POW_1_AND_2) {
                /* in 2 chan mode this is the channel to start with
                 * and the next channel will be converted  also
                 */    
                AdpPowMonPriv.adcChanToDma = 1;

                AdpPowMonPriv.bTwoChannelMode = TRUE;
                
                /* divide by 2 to double this rate */
                AdpPowMonPriv.dmaTimerBword = AdpPowMonPriv.dmaTimerBword >> 1;
            }                                 
        
            /* start power monitoring by calling the func that does the starting */
            AdpStartDMAforPower();
                            
        }  /* end if(!dmaRunning) */
                
    }   /* end of else not set bits only */                 
}

/**************************************************************************
 * Interrupt service routine for DACDMA on the ADP board.
 * Note the DAC and the DMA share an interrupt.
 * All DMAIFG flags source only one DMA controller interrupt vector and
 * the interrupt vector is shared with the DAC12 module. Software must
 * check the DMAIFG and DAC12IFG flags to determine the source of the
 * interrupt. The DMAIFG flags are not reset automatically and must 
 * be reset by software.
 */
#pragma vector=DACDMA_VECTOR
__interrupt void 
AdpPowDmaIsr(void)
{
        /* pulse for logic analyzer */
#ifdef NEVER_DEFINED              
        RANGE2_P(OUT) |= RANGE2_BIT;
        _nop();
        RANGE2_P(OUT) &= ~RANGE2_BIT; 
        _nop();
#endif    
            
    /* test the DMAIFG bit in each DMA controller register */
    if (RD_BIT(DMA0CTL, DMAIFG)!=0) {
                    
        /* We have acquired a buffer full of samples.
         * Now, we have to switch to the opposite buffer.
         * The LSB of the SN tells us which buffer we just
         * filled, and then after it is incremented, which
         * buffer to use next.
         * 
         * It is important that we test isrDmaBufferSn
         * *before* we increment it
         */
        if (AdpPowMonPriv.isrDmaBufferSn == -1) {
            
            /* This is the FIRST time we have filled a buffer,
             * so we know this is the even buffer
             */
             
            /* Now switch DMA destination to the opposite buffer */
            DMA0DA = (unsigned short) &AdpPowMonPriv.dmaOddBlk[0];      
        } else 
            /* 
             * Not the first buffer, so the LSB of the
             * SN tells us if we have just filled the
             * even or the odd buffer
             */
        if ((AdpPowMonPriv.isrDmaBufferSn & 0x01) == 0) {
            /*
             *  If the last filled buffer is 0, then we must
             * have just finished filling 1, so now we want
             * to go back to filling 0
             */
                         
            /* Now switch DMA destination to the opposite buffer */
            DMA0DA = (unsigned short) &AdpPowMonPriv.dmaEvenBlk[0];      
            
        } else {
            /*
             *  If the last filled buffer is 1, then we must
             * have just finished filling 0, so now we want
             * to go back to filling 1
             */
                        
            /* Now switch DMA destination to the opposite buffer */
            DMA0DA = (unsigned short) &AdpPowMonPriv.dmaOddBlk[0];      
        }
        
        /* increase buffer serial number */        
        AdpPowMonPriv.isrDmaBufferSn++;
                        
        /* Clear DMA0 interrupt flag */
        DMA0CTL &= ~DMAIFG;                       
        
        /*
         * DMA channel 0 control
         * |||||||||||
         * x Reserved
         *  xxx DMADTx
         *     xx DMADSTINCRx
         *     | xx DMASRCINCRx
         *     |   x DSTBYTE
         *     |    x SRCBYTE
         *     |     x DMALEVEL
         *     |      x DMAEN
         *     |      |x DMAIFG
         *     |      | x DMAIE
         *     |      | |x ABORT
         *     |      | | x DMAREQ
         *     |      | |
         * 0   |      | |   Reserved
         *  000|      | |   DMA Transfer mode 000=Single transfer (jcb each xfer
         *     |             requires a trigger)
         *     11     | |   11=Destination address is incremented
         *       00   | |   00=Source address is unchanged
         *         0  | |   0=Word
         *          0 | |   0=Word
         *           0| |   0=Edge sensitive (rising edge)
         *            1 |   1=Enabled
         *             0|   0=No interrupt pending
         *              1   1=Enabled (jcb since DMAENwas automatically cleared
         *                             after DMAxSZ transfers)
         *               0  0=DMA transfer not interrupted(by NMI)
         *                0 0=No DMA start
         * ----____----____
         * 
         */
          DMA0CTL = DMADSTINCR_3 + DMAEN + DMAIE;
        
    }   /* end DMA0 IFG set */

    if (RD_BIT(DMA1CTL, DMAIFG)!=0) {
       
        /* If the second DMA channel is interrupting, it will
         * be because we are acquiring 2 channels at the same time.
         * 
         * We can ABSOLUTELY COUNT ON the fact that the
         * DMA0 ISR has just run, and therefore we are testing
         * the isrDmaBufferSn LSB after it has been incremented,
         * and so we have to REVERSE our interpretation of it. 
         */

        if ( (AdpPowMonPriv.isrDmaBufferSn & 0x01) == 0) {
                         
            /* Now switch DMA destination to the opposite buffer */
            DMA1DA = (unsigned short) &AdpPowMonPriv.dmaOddBlk[NR_SAMPS_IN_DMA_BUFFER/2];      
            
        } else {
            
            /* Now switch DMA destination to the opposite buffer */
            DMA1DA = (unsigned short) &AdpPowMonPriv.dmaEvenBlk[NR_SAMPS_IN_DMA_BUFFER/2];      
        }
        
        /* Clear DMA1 interrupt flag */
        DMA1CTL &= ~DMAIFG;                       
        
        /* see comment in DMA0 code above */
        DMA1CTL = DMADSTINCR_3 + DMAEN + DMAIE ;        
        
    }   /* end DMA1 IFG set */
}

RSTAT
AdpGetPowMonData(UINT *pDataLen, UINT *pInfoLen, char **pRepStruct, char **pRepData)
{
    SD16 sampledIsrDmaBufferSn;

    /* only one instance of this for the whole system */
    static ADP_POW_BUF_INFO repStruct = { -1,NR_SAMPS_IN_DMA_BUFFER,0,0 };
    
    ADP_POW_BUF_INFO *rep;
     
    rep = &repStruct;
       
    /* passing out to caller */    
    *pRepStruct = (char*)&repStruct;
    
    /* The reply will have only the info part if new data
     * is not available, or both the info part and the data
     *  part if new data is available.
     * Calculate the byte length of each part.
     * In the future, this may be programmable to make best
     * use of the RAM memory available
     */         
    *pInfoLen = sizeof(ADP_POW_BUF_INFO);     

    if (!AdpPub.dmaRunning) {
        /* error: we should not get this command if DMA not running */
        
        /* report error */
        AdpErr(ADP_APP_ERR_ILLEGAL_CMD_WHILE_DMA_RUNNING, NULL);
        /* goto error; this was used only to turn off
         *  CAL bits and disable Vsups so is not needed here 
         */
        /*AdpStopPowMon();*/   
        /* ??how to ask caller to return not OK? */    
        /* perhaps when the next poll happens the poll code
         * will notice that DMA is not running and return an error?
         */  
        return (-1); /* flag error for caller */         
    }
   
    /* Avoid a very unlikely race condition (of the ISR SN changing while
     * we are processing a buffer) by being sure we only read the ISR SN once
     */ 
    sampledIsrDmaBufferSn = AdpPowMonPriv.isrDmaBufferSn;     
        
    /* Check if the Serial Number for the DMA buffer has
     * has changed from the last time we polled
     */
    if (sampledIsrDmaBufferSn == AdpPowMonPriv.dmaSnAvailable) {
        /* SN is still the same, Report no new data yet */         
        rep->dmaSnAvailable = -1;

        /* send only the ADP_POW_BUF_INFO reply */
        *pDataLen = 0;   
        
        return (0); /* no error */                  
    } else {
        /* SN has been changed by the ISR so new data is available */
        
        /* Update dmaSnAvailable and report to host 
         * that new data is ready 
         */
        AdpPowMonPriv.dmaSnAvailable = sampledIsrDmaBufferSn;
         
        /* indicate data is available with whole (positive) SN */         
        rep->dmaSnAvailable = AdpPowMonPriv.dmaSnAvailable;
                  
        /* send the ADP_POW_BUF_INFO reply AND THE DATA TOO 
         * which will come directly from the DMA buffer 
         */

        /* The PC host is requesting a DMA data block transfer,
         * The LSB of this SN tells us to transfer the EVEN or ODD buffer
         */
        if ( (AdpPowMonPriv.dmaSnAvailable & 0x01) == 0x00) {                             
            AdpPowMonPriv.pDmaBufferReadyToSend = &AdpPowMonPriv.dmaEvenBlk[0];                                          
        } else {                
            AdpPowMonPriv.pDmaBufferReadyToSend = &AdpPowMonPriv.dmaOddBlk[0];                    
        }
        
        /* put SN into reply structure */
        *pRepData = (char*)AdpPowMonPriv.pDmaBufferReadyToSend;
        
        /* how much data to send with reply: bytes = 2*(16 bit words) */
        *pDataLen = (NR_SAMPS_IN_DMA_BUFFER*2);        
        
        return (0); /* no error */        
    }
}

void
AdpStopPowMon(void)
{   
    /* Here we stop the DMA-based ADC12 acquisition
     * and restore the idle loop settings for ADC12     * 
     */
    if (AdpPub.dmaRunning) {
        /* STOP the DMA and restore the ADC so polling can resume */
        
        /* DMA0CTL = DMADSTINCR_3 + DMAIE + DMAEN;    Config */
        DMA0CTL = 0;   /* Config */
        /* DMA1CTL = 0; */   /* Config */
        
        AdpPub.dmaRunning = FALSE;
         
        /* power-down MSP430 modules before re-init of the 
         * ADC12 for command-driven operation (vs DMA driven)
         */
        ADC12CTL1 &= ~CONSEQ_2;        /*  Stop conversion immediately */
        ADC12CTL0 &= ~ENC;             /*  Disable ADC12 conversion */
        ADC12CTL0 = 0;                 /*  Switch off ADC12 & ref voltage */
        TBCTL = 0;                     /*  Disable Timer_B */
                    
        /* call init again to return to command driven ADC12 operation */
        /* RSTAT */
        AdpAdcInit();
        
    }
}
    

