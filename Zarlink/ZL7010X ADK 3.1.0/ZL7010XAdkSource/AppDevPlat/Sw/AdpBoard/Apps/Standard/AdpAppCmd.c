/**************************************************************************
 * This file contains functions to receive and process command packets on the
 * ADP board.
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Standard/String.h"            /* memset() */
#include "Adp/General.h"                /* UINT, RSTAT, ... */
#include "Adp/AppDevPlatVer.h"          /* APP_DEV_PLAT_VER */
#include "Adp/AnyBoard/Com.h"           /* COM_MAX_PACK_LEN */
#include "Adp/AnyBoard/ErrLib.h"        /* ErrGetFirst() */
#include "Adp/AnyBoard/TraceLib.h"      /* TRACE_ENAB, TraceGetNext(), ... */
#include "Adp/AdpBoard/AdpGeneral.h"    /* ADP_DEV_TYPE, ADP100_NAME, ... */
#include "Adp/AdpBoard/AdpCom.h"        /* ADP_GET_STAT_REPLY */

/* local include shared by all source files for ADP board application */
#include "AdpApp.h"

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* structure of private data for command processing */
typedef struct {
    
    /* Buffer used to hold a received command while the command is being
     * processed. This is a union because it is only used to hold one command
     * at a time.
     */
    union {
        ADP_SET_VSUP_CMD setVsupCmd;
        ADP_START_POW_MON_CMD startPowMonCmd;
    } cmdBuf;
    
    /* Buffer used to build a reply. This is a union because only one reply
     * is built and transmitting at a time.
     */
    union {
        ADP_GET_STAT_REPLY getStatReply;
    } replyBuf;
    
} ADP_CMD_PRIV;
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/* private data for command processing */
static ADP_CMD_PRIV AdpCmdPriv;
 
/**************************************************************************
 * Function Prototypes
 */
 
static RSTAT AdpCmdLenErr(void);
 
/**************************************************************************
 * Initialize the command interface on the ADP board.
 */
RSTAT
AdpCmdInit(void)
{
    ADP_CMD_PRIV *ac = &AdpCmdPriv;
    
    /* clear data and init any non-zero defaults */
    (void)memset(ac, 0, sizeof(*ac));
    
    return(0);
}

RSTAT
AdpCmdGetDevType(UINT cmdLen)
{
    /* check command packet length */
    if (cmdLen != 0) return(AdpCmdLenErr());
    
    /* send reply for successful command (device type string including '\0') */
    return(AdpSendReply(ADP_DEV_TYPE, sizeof(ADP_DEV_TYPE)));
}

RSTAT
AdpCmdGetDevName(UINT cmdLen)
{
    /* check command packet length */
    if (cmdLen != 0) return(AdpCmdLenErr());
    
    /* Send reply for successful command (device name including '\0'). The ADP
     * board doesn't currently provide an interface to set its device name, so
     * it just returns an empty string.
     */
    return(AdpSendReply("", sizeof("")));
}

RSTAT
AdpCmdGetVer(UINT cmdLen)
{
    UINT packLen;
    
    /* check command packet length */
    if (cmdLen != 0) return(AdpCmdLenErr());
    
    /* Calculate the length of the reply packet for the ADP board's version
     * string, and then send the packet header followed by version string.
     */
    packLen = ADP_LEN(ADP100_NAME) + ADP_LEN("; ") +
        ADP_LEN(APP_DEV_PLAT_VER) + ADP_LEN("\0");
    /**/
    if (AdpSendReplyHdr(COM_OK_REPLY_TYPE, packLen)) return(-1);
    if (AdpSendReplyData(ADP100_NAME, ADP_LEN(ADP100_NAME))) return(-1);
    if (AdpSendReplyData("; ", ADP_LEN("; "))) return(-1);
    if (AdpSendReplyData(APP_DEV_PLAT_VER, ADP_LEN(APP_DEV_PLAT_VER))) return(-1);
    if (AdpSendReplyData("\0", ADP_LEN("\0"))) return(-1);
    
    return(0);
}

RSTAT
AdpCmdGetErr(UINT cmdLen)
{
    /* check command packet length */
    if (cmdLen != 0) return(AdpCmdLenErr());
    
    /* Send reply containing information for latched error (if any). Note if
     * no error is latched, the error code will be 0, the error group will be
     * a default string, and the error arguments will an empty string.
     */
    if (AdpSendErrInfoReply(COM_OK_REPLY_TYPE, ErrGetFirst())) return(-1);
    
    /* clear any latched error information */
    ErrClearFirst();
    
    return(0);
}

RSTAT
AdpCmdGetStat(UINT cmdLen)
{
    ADP_GET_STAT_REPLY *rep;
    
    /* check command packet length */
    if (cmdLen != 0) return(AdpCmdLenErr());
    
    /* set pointer to reply buf */
    rep = &AdpCmdPriv.replyBuf.getStatReply;
    
    /* get current status flags to report to PC */
    rep->flags = AdpPub.statFlags;
    
    if (AdpPub.dmaRunning) 
    {
        rep->vsup1Adc = 0;
        rep->vsup2Adc = 0;
    } else 
    {
    /* get current VSUP1 & VSUP2 ADC levels (converted to mv on PC) */
    rep->vsup1Adc = AdpVsup1Adc(1);
    rep->vsup2Adc = AdpVsup2Adc(1);
    }
    
    /* if an error is latched, set error flag */
    if ((ErrGetFirst())->errCode) rep->flags |= ADP_ERR_OCCURRED;
    
    /* send reply for successful command */
    return(AdpSendReply(rep, sizeof(*rep)));
}

RSTAT
AdpCmdGetTraceMsg(UINT cmdLen)
{
    const char *msg;
    UINT msgSize;
    
    /* check command packet length */
    if (cmdLen != 0) return(AdpCmdLenErr());
    
    /* if tracing is enabled, get next trace message */
    if (TRACE_ENAB) {
        if ((msg = TraceGetNext()) == NULL) msg = "";
        msgSize = strlen(msg) + 1;
    } else {
        msg = "";
        msgSize = sizeof("");
    }
    /* send reply for successful command (message string including '\0') */
    return(AdpSendReply(msg, msgSize));
}

RSTAT
AdpCmdGetConfig(UINT cmdLen)
{
    /* check command packet length */
    if (cmdLen != 0) return(AdpCmdLenErr());
    
    /* send reply for successful command (ADP config) */
    return(AdpSendReply(&AdpPub.adpConfig, sizeof(AdpPub.adpConfig)));
}

RSTAT
AdpCmdSetConfig(UINT cmdLen)
{
    /* Check command packet length. Note the command length can be less than
     * the current size of AdpPub.adpConfig (ADP_CONFIG). That way, if new
     * fields are added to the end of ADP_CONFIG, this interface will remain
     * compatible with older software on the PC (the fields added to the end
     * of the config will not be affected by the command, and will simply be
     * left as is).
     */
    if (cmdLen > sizeof(AdpPub.adpConfig)) return(AdpCmdLenErr());
    
    /* receive new config (up to command length) */
    if (AdpReceive(&AdpPub.adpConfig, cmdLen)) return(-1);
    
    /* update power config */
    AdpConfigPow(&AdpPub.adpConfig.pow);
    
    /* send reply for successful command */
    return(AdpSendReply(NULL, 0));
}

RSTAT
AdpCmdSetVsup(UINT cmdLen)
{
    ADP_SET_VSUP_CMD *cmd;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(AdpCmdLenErr());
    
    /* set pointer to command buf and receive rest of command packet */
    cmd = &AdpCmdPriv.cmdBuf.setVsupCmd;
    if (AdpReceive(cmd, sizeof(*cmd))) return(-1);
    
    AdpSetVsup(cmd->vsup1Mv, cmd->vsup2Mv);
    
    /* send reply for successful command */
    return(AdpSendReply(NULL, 0));
}

RSTAT
AdpCmdStartPowMon(UINT cmdLen)
{
    ADP_START_POW_MON_CMD *cmd;
    
    /* check command packet length */
    if (cmdLen != sizeof(*cmd)) return(AdpCmdLenErr());
    
    /* set pointer to command buf and receive rest of command packet */
    cmd = &AdpCmdPriv.cmdBuf.startPowMonCmd;
    if (AdpReceive(cmd, sizeof(*cmd))) return(-1);
    
    /* pass pointer to received command packet */
    AdpStartPowMon(cmd);
    
    /* send reply for successful command */
    return(AdpSendReply(NULL, 0));
}

RSTAT
AdpCmdStopPowMon(UINT cmdLen)
{
    /* check command packet length */
    if (cmdLen != 0) return(AdpCmdLenErr());
    
    /* stop power monitoring */
    AdpStopPowMon();    
    
    
    /* send reply for successful command */
    return(AdpSendReply(NULL, 0));
}

RSTAT
AdpCmdGetPow(UINT cmdLen)
{
    UINT dataLen;
    UINT infoLen;
    char* repInfo;
    char* repData;
    RSTAT rstatFromGet;

    /* check command packet length */
    if (cmdLen != 0) return(AdpCmdLenErr());

    rstatFromGet = AdpGetPowMonData( &dataLen, &infoLen, &repInfo, &repData );

    if (rstatFromGet != 0) {
        
        return (-1);
    };

    /* send packet header followed by various reply components */
    
    /* WHOLE length (infoLen + dataLen) must go in the header 
     * so the receiver knows how many bytes to expect
     */
    if (AdpSendReplyHdr(COM_OK_REPLY_TYPE, infoLen + dataLen)) {return(-1);};
    
    /* now send only the info part */
    if (AdpSendReplyData(repInfo, infoLen)) {return(-1);};
    
    /* now (if needed) send the data part */
    if (dataLen != 0) {
        if (AdpSendReplyData(repData, dataLen)) {return(-1);};
    }
         
    return (0);
}

static RSTAT
AdpCmdLenErr(void)
{
    /* Since the received command packet length didn't match the expected
     * length, the communication may not be synchronized on packet boundaries.
     * To get it synchronized again, receive and discard bytes until it
     * times out. Note the timeout will also disable the error reply (see
     * ADP_ENAB_ERR_REPLY in AdpAppCom.c), so AdpReceiveAndProcCmdPack() won't
     * send an error reply for the command (it's best not to send an
     * error reply if the communication is out of sync).
     */
    AdpErr(ADP_APP_ERR_RECEIVED_CMD_WITH_INVALID_LEN, NULL);
    AdpReceiveAndDiscard(COM_MAX_PACK_LEN);
    return(-1);
}
