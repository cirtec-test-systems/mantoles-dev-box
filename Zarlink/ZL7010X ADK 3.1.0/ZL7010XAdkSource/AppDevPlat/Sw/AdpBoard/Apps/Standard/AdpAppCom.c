/**************************************************************************
 * This file contains functions for the communication interface on the
 * ADP board.
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Standard/String.h"          /* memset() */
#include "Standard/StdIo.h"           /* sprintf() */
#include "Adp/General.h"              /* UINT, RSTAT, ... */
#include "Adp/Build/Build.h"          /* UD8, UD16, ... */  
#include "Adp/AnyBoard/Com.h"         /* COM_PACK_HDR, ... */
#include "Adp/AnyBoard/AdpSpiHw.h"    /* ASPI_TX_BUF, ... */
#include "Adp/AnyBoard/AdpSpiLib.h"   /* ASpiInit(), ... */
#include "Adp/AnyBoard/SysTimerLib.h" /* StMs(), ... */
#include "Adp/AdpBoard/UsbHw.h"       /* USB_TX_FULL, USB_TX_DATA, ... */
#include "Adp/AdpBoard/UsbLib.h"      /* UsbInit() */
#include "Adp/AdpBoard/AdpCom.h"      /* ADP_LOCAL_ADDR, ... */

/* local include shared by all source files for ADP board application */
#include "AdpApp.h"

/**************************************************************************
 * Defines and Macros
 */
 
/* Communication interface identifiers:
 * 
 * ADP_SPI_INTERFACE: SPI interface between ADP board and mezzanine board.
 * ADP_USB_INTERFACE: USB interface between ADP board and PC.
 * ADP_I2C_INTERFACE: I2C interface between ADP board and mezzanine board.
 */
#define ADP_SPI_INTERFACE  0
#define ADP_USB_INTERFACE  1
#define ADP_I2C_INTERFACE  2

/* Set communications timeout (milliseconds). If an expected byte isn't
 * received or a byte can't be transmitted within this timeout, the
 * communication interface will abort the packet being received or
 * transmitted and report the error.
 */
#define ADP_COM_TIMEOUT_MS  2000

/* Default error message for errors that occur on a ADP board. Note
 * this does not end with a period because AdpSendErrInfoReply() appends
 * ": <ErrGroup>.<ErrCode>" to it.
 */
#define ADP_ERR_MSG  "Error on ADP board"

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* structure of private data for communication interface */
typedef struct {
    
    /* Various flags for the communication interface. For bit definitions,
     * see ADP_ENAB_ERR_REPLY, etc. This is declared volatile to prevent the
     * compiler from assuming it won't change during a section of code, in
     * case it is changed by interrupts.
     */
    volatile UD8 flags;
    
    UD8 rxInterface;
    
    UD8 mezzInterface;
    
    UD8 repSrcAndDest;
    
} ADP_COM_PRIV;
/*
 * Defines for bits in ADP_COM_PRIV.flags:
 * 
 * ADP_ENAB_ERR_REPLY: This is set before receiving and processing each
 * command packet, and cleared if needed to prevent AdpReceiveAndProcCmdPack()
 * from sending an error reply if the command fails.
 * 
 * ADP_FORCE_SPI_RX_FULL: If ASPI_RX_FULL is clear when AdpProcSpiRx() is
 * called, it sets this flag to indicate the SPI RX buffer contains a byte even
 * though ASPI_RX_FULL is clear. This is needed because a SPI RX interrupt
 * occurs for the first byte of each packet (see AdpSpiRxIsr()), which
 * automatically clears ASPI_RX_FULL on some MSP430 variants.
 */
#define ADP_ENAB_ERR_REPLY     (1 << 0)
#define ADP_FORCE_SPI_RX_FULL  (1 << 1)
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/* private data for communication interface */
static ADP_COM_PRIV AdpComPriv;

/**************************************************************************
 * Function Prototypes
 */
 
static RSTAT AdpSendUsb(const void *data, UINT len);
static RSTAT AdpSendSpi(const void *data, UINT len);
static RSTAT AdpReceiveAndProcPack(int thisBoardAddr);
__inline RSTAT AdpReceiveAndProcCmdPack(const COM_PACK_HDR *packHdr,
    UINT packLen);
__inline RSTAT AdpReceiveAndForwardPack(const COM_PACK_HDR *packHdr,
    UINT packLen);
__inline RSTAT AdpReceiveUsb(void *buf, UINT len);
__inline RSTAT AdpReceiveSpi(void *buf, UINT len);
__inline RSTAT AdpReceiveUsbAndForwardToSpi(UINT len);
__inline RSTAT AdpReceiveSpiAndForwardToUsb(UINT len);
__interrupt void AdpSpiRxIsr(void);

/**************************************************************************
 * Initialize the communication interface on the ADP board.
 */
RSTAT
AdpComInit(void)
{
    ADP_COM_PRIV *ac = &AdpComPriv;
    
    /* clear data and init any non-zero defaults */
    (void)memset(ac, 0, sizeof(*ac));
    ac->mezzInterface = ADP_SPI_INTERFACE;
    
    /* init ADP SPI interface (configures SPI for slave mode) */
    if (ASpiInit()) return(-1);
    
    /* init USB interface */
    if (UsbInit()) return(-1);
    
    return(0);
}

void
AdpProcUsbRx(void)
{
    ADP_COM_PRIV *ac = &AdpComPriv;
    
    /* clear ADP_USB_RX_PEND flag for AdpProc() */
    AdpPub.pend &= ~ADP_USB_RX_PEND;
    
    /* set interface we're receiving packet from */
    ac->rxInterface = ADP_USB_INTERFACE;
        
    /* set USB data direction to receive bytes */
    USB_DATA_DIR = USB_DATA_DIR_RX;
    
    /* Receive packet via USB and process it. Note for a packet received via
     * USB, this board is the local ADP board, so use ADP_LOCAL_ADDR for this
     * board's address.
     */
    (void)AdpReceiveAndProcPack(ADP_LOCAL_ADDR);
    
    /* Clear USB RX interrupt flag, then re-enable interrupt (if there's
     * already another byte available in the RX buf, set interrupt flag so the
     * interrupt will occur again).
     */
    USB_WR_RX_INT_FLAG(FALSE);
    if (!USB_RX_EMPTY) USB_WR_RX_INT_FLAG(TRUE);
    USB_WR_RX_INT_ENAB(TRUE);
}

void
AdpProcSpiRx(void)
{
    ADP_COM_PRIV *ac = &AdpComPriv;
    
    /* clear ADP_SPI_RX_PEND flag for AdpProc() */
    AdpPub.pend &= ~ADP_SPI_RX_PEND;
    
    /* set interface we're receiving packet from */
    ac->rxInterface = ADP_SPI_INTERFACE;
    
    /* If ASPI_RX_FULL is clear, set flag to indicate the SPI RX buffer
     * contains a byte even though ASPI_RX_FULL is clear. This is needed
     * because a SPI RX interrupt occurs for the first byte of each packet
     * (see AdpSpiRxIsr()), which automatically clears ASPI_RX_FULL on some
     * MSP430 variants.
     */
    if (!ASPI_RX_FULL) ac->flags |= ADP_FORCE_SPI_RX_FULL;
    
    /* Receive packet via SPI and process it. Note for a packet received via
     * SPI, this board is the remote ADP board, so use ADP_REMOTE_ADDR for this
     * board's address.
     */
    (void)AdpReceiveAndProcPack(ADP_REMOTE_ADDR);
    
    /* re-enable ADP SPI RX interrupt for first byte of next packet */
    ASPI_WR_RX_INT_ENAB(TRUE);
}

static RSTAT
AdpReceiveAndProcPack(int thisBoardAddr)
{
    COM_PACK_HDR packHdr;
    UINT destAddr;
    UD16 packLen;
    
    /* receive packet header */
    if (AdpReceive(&packHdr, sizeof(packHdr))) goto error;
    
    /* get packet length from header (note it may be 0) */
    packLen = ((UD16)packHdr.lenMsb << 8) | packHdr.lenLsb;
    
    /* If the destination address is 0, communication may not be synchronized
     * on packet boundaries, so receive & discard bytes until it times out.
     */
    if ((destAddr = (packHdr.srcAndDest & 0x0F)) == 0) {
        AdpErr(ADP_APP_ERR_RECEIVED_PACK_WITH_DEST_0, NULL);
        AdpReceiveAndDiscard(COM_MAX_PACK_LEN);
        goto error;
    }
    
    /* if packet is for us, receive and process command; else forward packet */
    if (destAddr == thisBoardAddr) {
        if (AdpReceiveAndProcCmdPack(&packHdr, packLen)) goto error;
    } else {
        if (AdpReceiveAndForwardPack(&packHdr, packLen)) goto error;
    }
    
    /* If the SPI interface was switched to master mode (to forward a packet
     * to SPI, or to reply to a command), configure it for slave mode again.
     */
    if (ASPI_MASTER) ASpiSetSlave();
    return(0);
    
error:
    /* reset SPI interface in case it's out of sync (shouldn't happen) */
    ASpiSetSlave();
    
    return(-1);   
}

__inline RSTAT
AdpReceiveAndProcCmdPack(const COM_PACK_HDR *packHdr, UINT packLen)
{
    ADP_COM_PRIV *ac = &AdpComPriv;
    const ERR_INFO *ei;
    RSTAT cmdStat;
    
    /* set reply source and destination (swap command source and destination) */
    ac->repSrcAndDest = (packHdr->srcAndDest << 4) | (packHdr->srcAndDest >> 4);
    
    /* enable error reply if command fails (cleared later if needed) */
    ac->flags |= ADP_ENAB_ERR_REPLY;
    
    /* receive and process remainder of command */
    switch(packHdr->type) {
    case COM_GET_DEV_TYPE_CMD_TYPE:
        cmdStat = AdpCmdGetDevType(packLen); break;
    case COM_GET_DEV_NAME_CMD_TYPE:
        cmdStat = AdpCmdGetDevName(packLen); break;
    case COM_GET_DEV_VER_CMD_TYPE:
        cmdStat = AdpCmdGetVer(packLen); break;
    case COM_GET_DEV_ERR_CMD_TYPE:
        cmdStat = AdpCmdGetErr(packLen); break;
    case ADP_GET_STAT_CMD_TYPE:
        cmdStat = AdpCmdGetStat(packLen); break;
    case ADP_GET_TRACE_MSG_CMD_TYPE:
        cmdStat = AdpCmdGetTraceMsg(packLen); break;
    case ADP_GET_CONFIG_CMD_TYPE:
        cmdStat = AdpCmdGetConfig(packLen); break;
    case ADP_SET_CONFIG_CMD_TYPE:
        cmdStat = AdpCmdSetConfig(packLen); break;
    case ADP_SET_VSUP_CMD_TYPE:
        cmdStat = AdpCmdSetVsup(packLen); break;
    case ADP_START_POW_MON_CMD_TYPE:
        cmdStat = AdpCmdStartPowMon(packLen); break;     
    case ADP_STOP_POW_MON_CMD_TYPE:
        cmdStat = AdpCmdStopPowMon(packLen); break;
    case ADP_GET_POW_CMD_TYPE:
        cmdStat = AdpCmdGetPow(packLen); break;
        
    default:
        /* Since command was invalid, communication may not be synchronized on
         * packet boundaries, so receive and discard bytes until it times out.
         */
        AdpErr(ADP_APP_ERR_RECEIVED_CMD_WITH_INVALID_CMD_TYPE,
            "%d", packHdr->type);
        AdpReceiveAndDiscard(COM_MAX_PACK_LEN);
        return(-1);
    }
    
    /* If the command failed & error reply is still enabled, send error reply
     * (packet header with reply type COM_ERR_REPLY_TYPE followed by error ID
     * string). Note if the error reply is sent successfully, and this is the
     * first error that occurred since ErrClearFirst() was called, clear first
     * error so it won't be reported in the board status.
     */
    if (cmdStat && (ac->flags & ADP_ENAB_ERR_REPLY)) {
        
        ei = ErrGet();
        if (AdpSendErrInfoReply(COM_ERR_REPLY_TYPE, ei) == 0) {
            if (ErrIsFirst(ei->errGroup, ei->errCode)) ErrClearFirst();
        }
    }
    return(cmdStat);
}

__inline RSTAT
AdpReceiveAndForwardPack(const COM_PACK_HDR *packHdr, UINT packLen)
{
    ADP_COM_PRIV *ac = &AdpComPriv;
    
    /* if receiving packet via USB, forward packet from USB to mezzanine */
    if (ac->rxInterface == ADP_USB_INTERFACE) {
        
        /* if interface to mezzanine board is SPI, forward packet to SPI */
        if (ac->mezzInterface == ADP_SPI_INTERFACE) {
            
            /* configure SPI interface for master mode */
            ASpiSetMaster();
            
            /* Forward packet header to SPI, and if it fails, receive & discard
             * remainder of packet (to keep communication synchronized).
             */
            if (AdpSendSpi(packHdr, sizeof(*packHdr))) {
                AdpReceiveAndDiscard(packLen);
                return(-1);
            }
                
            /* forward remainder of packet from USB to SPI */
            if (packLen) return(AdpReceiveUsbAndForwardToSpi(packLen));
            
        } else { /* else forward packet from USB to I2C */
            
            /* Receive and discard remainder of packet (to keep communication
             * synchronized on packet boundaries).
             */
            AdpErr(ADP_APP_ERR_I2C_NOT_YET_SUPPORTED, NULL);
            AdpReceiveAndDiscard(packLen);
            return(-1);
        }
        
    } else { /* else forward packet from mezzanine to USB */
    
        /* set USB data direction to transmit bytes */
        USB_DATA_DIR = USB_DATA_DIR_TX;
        
        /* Forward packet header to USB, and if it fails, receive and discard
         * remainder of packet (to keep communication synchronized).
         */
        if (AdpSendUsb(packHdr, sizeof(*packHdr))) {
            AdpReceiveAndDiscard(packLen);
            return(-1);
        }
        
        /* if packet contains any data */
        if (packLen) {
            
            /* if receiving packet from SPI, forward SPI to USB */
            if (ac->rxInterface == ADP_SPI_INTERFACE) {
                
                return(AdpReceiveSpiAndForwardToUsb(packLen));
                
            } else { /* else forward packet from I2C to USB */
                
                /* set error info and return error */
                AdpErr(ADP_APP_ERR_I2C_NOT_YET_SUPPORTED, NULL);
                return(-1);
            }
        }
    }
    return(0);
}

RSTAT
AdpReceive(void *buf, UINT len)
{
    ADP_COM_PRIV *ac = &AdpComPriv;
    RSTAT rStat;
    
    /* receive bytes from interface we are receiving a packet from */
    switch(ac->rxInterface) {
    case ADP_USB_INTERFACE:
    
        rStat = AdpReceiveUsb(buf, len);
        break;
        
    case ADP_SPI_INTERFACE:
        rStat = AdpReceiveSpi(buf, len);
        break;
        
    default: /* assume it's I2C */
    
        AdpErr(ADP_APP_ERR_I2C_NOT_YET_SUPPORTED, NULL);
        rStat = -1;
        break;
    }
    /* If a receive error occurred, disable error reply so if we were receiving
     * a command packet, AdpReceiveAndProcCmdPack() won't try to send an error
     * reply for the command (it's best not to send a reply if receive fails).
     */
    if (rStat) ac->flags &= ~ADP_ENAB_ERR_REPLY;
    
    return(rStat);
}

void
AdpReceiveAndDiscard(UINT len)
{
    UINT n;
    UD8 buf[8];
    
    ErrLock(TRUE);
    while(len != 0) {
        
        /* receive more bytes and update length remaining */
        n = (len < sizeof(buf)) ? len : sizeof(buf);
        if (AdpReceive(buf, n)) break;
        len -= n;
    }
    ErrLock(FALSE);
}

__inline RSTAT
AdpReceiveUsb(void *buf, UINT len)
{
    UD16 startMs;
    UD8 *next = buf;
    
    while(len != 0) {
        
        /* if USB RX buf is empty, wait for it */
        if (USB_RX_EMPTY) {
            
            startMs = StMs();
            while(USB_RX_EMPTY) {
                
                if (StElapsedMs(startMs, ADP_COM_TIMEOUT_MS)) {
                    AdpErr(ADP_APP_ERR_USB_RX_TIMEOUT, NULL);
                    return(-1);
                }
            }
        }
        
        /* read byte from USB RX buf and save it in specified buf */
        USB_WR_RX_STROBE(0);
        *next++ = USB_RX_DATA;
        USB_WR_RX_STROBE(1);
        
        /* decrement length remaining */
        --len;
    }
    
    return(0);
}

__inline RSTAT
AdpReceiveSpi(void *buf, UINT len)
{
    ADP_COM_PRIV *ac = &AdpComPriv;
    UD16 startMs;
    UD8 *next = buf;
    
    while(len != 0) {
        
        /* while SPI RX buf is empty, wait for it */
        startMs = StMs();
        while(!ASPI_RX_FULL) {
                
            /* If we know the first byte of a packet is ready, don't wait for
             * ASPI_RX_FULL. This is done because a SPI RX interrupt occurs for
             * the first byte, which automatically clears ASPI_RX_FULL on some
             * MSP430 variants.
             */
            if (ac->flags & ADP_FORCE_SPI_RX_FULL) {
                ac->flags &= ~ADP_FORCE_SPI_RX_FULL;
                break;
            }
            if (StElapsedMs(startMs, ADP_COM_TIMEOUT_MS)) {
                AdpErr(ADP_APP_ERR_ADP_SPI_RX_TIMEOUT, NULL);
                return(-1);
            }
        }
                    
        /* Read byte from SPI RX buf and save it in specified buf (also clears
         * ASPI_RX_FULL), then invoke ASPI_STROBE_SLAVE_READY() to tell the
         * transmitting side (master) the byte was received.
         */
        *next++ = ASPI_RX_BUF;
        ASPI_STROBE_SLAVE_READY();
        
        /* decrement length remaining */
        --len;
    }
    
    return(0);
}

__inline RSTAT
AdpReceiveUsbAndForwardToSpi(UINT len)
{
    UD16 startMs;
    
    while(len != 0) {
        
        /* if USB RX buf is empty, wait for it */
        if (USB_RX_EMPTY) {
            
            startMs = StMs();
            while(USB_RX_EMPTY) {
                
                if (StElapsedMs(startMs, ADP_COM_TIMEOUT_MS)) {
                    AdpErr(ADP_APP_ERR_USB_RX_TIMEOUT, NULL);
                    return(-1);
                }
            }
        }
        
        /* if slave hasn't received previous byte, wait for it */
        if (!ASPI_SLAVE_READY) {
            
            startMs = StMs();
            while(!ASPI_SLAVE_READY) {
            
                /* If SPI TX timeout expired, fail (also receive & discard
                 * remainder of packet to keep communication synchronized).
                 */
                if (StElapsedMs(startMs, ADP_COM_TIMEOUT_MS)) {
                    AdpErr(ADP_APP_ERR_ADP_SPI_TX_TIMEOUT, NULL);
                    AdpReceiveAndDiscard(len);
                    return(-1);
                }
            }
        }
        /* clear latched ASPI_SLAVE_READY before sending the next byte */
        ASPI_WR_SLAVE_READY(0);
        
        /* read byte from USB RX buf and write it to SPI TX buf */
        USB_WR_RX_STROBE(0);
        ASPI_TX_BUF = USB_RX_DATA;
        USB_WR_RX_STROBE(1);
        
        /* decrement length remaining */
        --len;
    }
    
    return(0);
}

__inline RSTAT
AdpReceiveSpiAndForwardToUsb(UINT len)
{
    UD16 startMs;
    
    while(len != 0) {
        
        /* if USB tx buf is full, wait for it */
        if (USB_TX_FULL) {
        
            startMs = StMs();
            while(USB_TX_FULL) {
                    
                /* If USB TX timeout expired, fail (also receive & discard
                 * remainder of packet to keep communication synchronized).
                 */
                if (StElapsedMs(startMs, ADP_COM_TIMEOUT_MS)) {
                    AdpErr(ADP_APP_ERR_USB_TX_TIMEOUT, NULL);
                    AdpReceiveAndDiscard(len);
                    return(-1);
                }
            }
        }
            
        /* if SPI RX buf is empty, wait for it */
        if (!ASPI_RX_FULL) {
            
            startMs = StMs();
            while(!ASPI_RX_FULL) {
                
                /* Note we don't need to check ADP_FORCE_SPI_RX_FULL here
                 * because AdpReceiveSpi() is always called first to receive
                 * the packet header, so this function will never be called to
                 * receive the first byte of a packet.
                 */
                
                if (StElapsedMs(startMs, ADP_COM_TIMEOUT_MS)) {
                    AdpErr(ADP_APP_ERR_ADP_SPI_RX_TIMEOUT, NULL);
                    return(-1);
                }
            }
        }
            
        /* Read byte from SPI RX buf and latch it in the USB_TX_DATA port,
         * invoke ASPI_STROBE_SLAVE_READY() to tell the transmitting side
         * (master) the byte was received, then strobe the byte into the USB
         * TX buf. Note ASPI_STROBE_SLAVE_READY() is invoked before the USB
         * strobe because we want to tell the SPI master the byte was received
         * as soon as possible, so it will send the next byte.
         */
        USB_TX_DATA = ASPI_RX_BUF;
        ASPI_STROBE_SLAVE_READY();
        USB_WR_TX_STROBE(0);
        USB_WR_TX_STROBE(1);
        
        /* decrement length remaining */
        --len;
    }
        
    return(0);
}

RSTAT
AdpSendReply(const void *rep, UINT repLen)
{
    /* send reply packet header for successful command */
    if (AdpSendReplyHdr(COM_OK_REPLY_TYPE, repLen)) return(-1);
    
    /* send reply data (note structure of reply depends on command) */
    if (repLen != 0) {
        return(AdpSendReplyData(rep, repLen));
    }
    return(0);
}

RSTAT
AdpSendErrInfoReply(UD8 repType, const ERR_INFO *ei)
{
    UINT packLen, sErrCodeLen, errGroupLen, errArgsLen;
    char sErrCode[6];
    
    /* Build error code string. Max length = 5 (max string length for 16 bit
     * error code in decimal) + '\0' = 6.
     */
    (void)sprintf(sErrCode, "%d", ei->errCode);
    
    /* get string lengths (excluding terminating '\0') */
    errGroupLen = strlen(ei->errGroup);
    sErrCodeLen = strlen(sErrCode);
    errArgsLen = strlen(ei->errArgs);
    
    /* Calc length of error reply packet (error ID string + '\0' + error
     * arguments string + '\0') and send reply packet header.
     */
    packLen = errGroupLen + ADP_LEN(".") + sErrCodeLen + ADP_LEN(": ") +
        ADP_LEN(ADP_ERR_MSG) + ADP_LEN(": ") + errGroupLen + ADP_LEN(".") +
        sErrCodeLen + ADP_LEN("\0") + errArgsLen + ADP_LEN("\0");
        
    if (AdpSendReplyHdr(repType, packLen)) return(-1);
    
    /* Send error ID string followed by '\0'. The string is:
     * 
     * <ErrGroup>.<ErrCode>: <ADP_ERR_MSG>: <ErrGroup>.<ErrCode>
     */
    if (AdpSendReplyData(ei->errGroup, errGroupLen)) return(-1);
    if (AdpSendReplyData(".", ADP_LEN("."))) return(-1);
    if (AdpSendReplyData(sErrCode, sErrCodeLen)) return(-1);
    if (AdpSendReplyData(": ", ADP_LEN(": "))) return(-1);
    if (AdpSendReplyData(ADP_ERR_MSG, ADP_LEN(ADP_ERR_MSG))) return(-1);
    if (AdpSendReplyData(": ", ADP_LEN(": "))) return(-1);
    if (AdpSendReplyData(ei->errGroup, errGroupLen)) return(-1);
    if (AdpSendReplyData(".", ADP_LEN("."))) return(-1);
    if (AdpSendReplyData(sErrCode, sErrCodeLen)) return(-1);
    if (AdpSendReplyData("\0", ADP_LEN("\0"))) return(-1);
    
    /* send error arguments string followed by '\0' */
    if (AdpSendReplyData(ei->errArgs, errArgsLen + 1)) return(-1);
    
    return(0);
}

RSTAT
AdpSendReplyHdr(UD8 repType, UINT repLen)
{
    COM_PACK_HDR packHdr;
    ADP_COM_PRIV *ac = &AdpComPriv;
    
    /* Disable error reply so if an error is encountered after the reply is
     * started, AdpReceiveAndProcCmdPack() won't try to send an error reply.
     */
    ac->flags &= ~ADP_ENAB_ERR_REPLY;
    
    /* Build reply packet header. Note the reply type must be COM_OK_REPLY_TYPE
     * or COM_ERR_REPLY_TYPE. If any other value is specified for the reply
     * type, COM_ERR_REPLY_TYPE will be used instead.
     */
    packHdr.srcAndDest = ac->repSrcAndDest;
    packHdr.lenLsb = (UD8)(repLen);
    packHdr.lenMsb = (UD8)(repLen >> 8);
    if (repType == COM_OK_REPLY_TYPE) {
        packHdr.type = COM_OK_REPLY_TYPE;
    } else {    
        packHdr.type = COM_ERR_REPLY_TYPE;
    }
    
    /* send reply packet header to interface we received command from */
    switch(ac->rxInterface) {
    case ADP_USB_INTERFACE:
        
        /* set USB data direction to transmit bytes */
        USB_DATA_DIR = USB_DATA_DIR_TX;
        
        return(AdpSendUsb(&packHdr, sizeof(packHdr)));
        
    case ADP_SPI_INTERFACE:
    
        /* configure ADP SPI interface for master mode */
        ASpiSetMaster();
        
        return(AdpSendSpi(&packHdr, sizeof(packHdr)));
        
    default: /* assume command received from I2C */
        
        AdpErr(ADP_APP_ERR_I2C_NOT_YET_SUPPORTED, NULL);
        return(-1);
    }
}

RSTAT
AdpSendReplyData(const void *data, UINT len)
{
    ADP_COM_PRIV *ac = &AdpComPriv;
    
    /* send reply data to interface we received command from */
    switch(ac->rxInterface) {
    case ADP_USB_INTERFACE:
        
        return(AdpSendUsb(data, len));
        
    case ADP_SPI_INTERFACE:
        
        return(AdpSendSpi(data, len));
    
    default: /* assume command received from I2C */
        
        AdpErr(ADP_APP_ERR_I2C_NOT_YET_SUPPORTED, NULL);
        return(-1);
    }
}

static RSTAT
AdpSendUsb(const void *data, UINT len)
{
    UD16 startMs;
    const UD8 *next = data;
    
    while(len != 0) {
        
        /* if USB tx buf is full, wait for it */
        if (USB_TX_FULL) {
            
            startMs = StMs();
            while(USB_TX_FULL) {
                    
                if (StElapsedMs(startMs, ADP_COM_TIMEOUT_MS)) {
                    AdpErr(ADP_APP_ERR_USB_TX_TIMEOUT, NULL);
                    return(-1);
                }
            }
        }
            
        /* write byte to USB TX data port and strobe it into USB TX buf */
        USB_TX_DATA = *next++;
        USB_WR_TX_STROBE(0);
        USB_WR_TX_STROBE(1);
        
        /* decrement length remaining */
        --len;
    }
    
    return(0);
}

static RSTAT
AdpSendSpi(const void *data, UINT len)
{
    UD16 startMs;
    const UD8 *next = data;
    
    while(len != 0) {
        
        /* while slave hasn't received the previous byte, wait for it */
        startMs = StMs();
        while(!ASPI_SLAVE_READY) {
            
            if (StElapsedMs(startMs, ADP_COM_TIMEOUT_MS)) {
                AdpErr(ADP_APP_ERR_ADP_SPI_TX_TIMEOUT, NULL);
                return(-1);
            }
        }
        /* clear latched ASPI_SLAVE_READY before sending the next byte */
        ASPI_WR_SLAVE_READY(0);
        
        /* write next byte to SPI TX buf */
        ASPI_TX_BUF = *next++;
        
        --len;
    }
    
    return(0);
}

#pragma vector=ASPI_RX_VECTOR
__interrupt void
AdpSpiRxIsr(void)
{
    /* disable SPI RX interrupt */
    ASPI_WR_RX_INT_ENAB(FALSE);
        
    /* set ADP_SPI_RX_PEND flag for AdpProc() */
    AdpPub.pend |= ADP_SPI_RX_PEND;
    
#ifdef TODO
    /* Since we set a pending flag for AdpProc(), adjust MCU's status register
     * on stack so we won't re-enter low power mode when ISR returns.
     */
    LPM4_EXIT;
#endif    
}
