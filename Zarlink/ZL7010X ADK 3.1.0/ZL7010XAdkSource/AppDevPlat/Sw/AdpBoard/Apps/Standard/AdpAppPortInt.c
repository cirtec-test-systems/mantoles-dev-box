/**************************************************************************
 * This file contains functions for the I/O port interrupts on the ADP board.
 *
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Adp/Build/Build.h"     /* PORT2_VECTOR */
#include "Adp/AdpBoard/UsbHw.h"  /* USB_RX_INT_FLAG, ... */
#include "Adp/AdpBoard/UsbLib.h" /* UsbPowerIsr() */
#include "Adp/AdpBoard/BatLib.h" /* BatSetMaxUsbMa() */

/* local include shared by all source files for ADP board application */
#include "AdpApp.h"

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/**************************************************************************
 * Function Prototypes
 */
 
__interrupt void AdpPort2Isr(void);
__inline void AdpUsbRxIsr(void);
__inline void AdpUsbPowerIsr(void);

/**************************************************************************
 * Interrupt service routine for port 2 interrupts on the ADP board.
 */
#pragma vector=PORT2_VECTOR
__interrupt void
AdpPort2Isr(void)
{
    /* USB power status interrupt (power on/off) */
    if (USB_POWER_INT_FLAG) AdpUsbPowerIsr();
    
    /* USB RX interrupt */
    if (USB_RX_INT_FLAG) AdpUsbRxIsr();
    
#ifdef TODO
    /* If ISR set a pending flag for AdpProc(), adjust MCU's status register
     * on stack so we won't re-enter low power mode when ISR returns.
     */
    if (AdpPub.pend) LPM4_EXIT;
#endif    
}

/* This interrupt occurs when USB is connected and initialized, and when it
 * is disconnected.
 */
__inline void
AdpUsbPowerIsr(void)
{
    /* Perform interrupt service required by USB lib. This will enable the USB
     * RX interrupt when USB is connected and initialized, and disable it when
     * USB is disconnected.
     */
    UsbPowerIsr();
    
    /* If USB is disconnected, set the maximum current to draw from USB to
     * charge the battery to 100 mA, so if and when USB is reconnected, it
     * won't draw more than 100 mA until it is initialized. Otherwise, USB
     * must be connected and initialized, so set it to 300 mA (the maximum
     * the battery can handle).
     */
    if (USB_POWER_OFF) {
        BatSetMaxUsbMa(100);
    } else {
        BatSetMaxUsbMa(300);
        
        /* flush anything in USB RX fifo after it's first plugged in */
        while(!USB_RX_EMPTY) {
            USB_WR_RX_STROBE(0);
            USB_WR_RX_STROBE(1);
        }
    }
}

__inline void
AdpUsbRxIsr(void)
{
    /* Set ADP_USB_RX_PEND for AdpProc() and disable the USB RX interrupt
     * until AdpProc() processes it. Note while the interrupt is disabled,
     * the CPU may still set USB_RX_INT_FLAG.
     */
    AdpPub.pend |= ADP_USB_RX_PEND;
    USB_WR_RX_INT_ENAB(FALSE);
}
