/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Adp/General.h"         /* UINT, ... */
#include "Adp/Build/Build.h"     /* I2C_SDA_BIT, I2C_SDA_P(), ... */
#include "Adp/AdpBoard/BatHw.h"  /* BAT_MAX_USB_CURRENT, ... */
#include "Adp/AdpBoard/BatLib.h" /* public include for BatLib */

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/**************************************************************************
 * Function Prototypes
 */
 
/**************************************************************************
 * Set the maximum current the LP3947 battery charger may draw from USB.
 */
void
BatSetMaxUsbMa(UINT maxUsbMa)
{
    UINT maxUsbCurrentRegVal;
    
    /* if specified maxUsbMa <= 100, return (already cleared BAT_CHARGE_HIGH) */
    if (maxUsbMa <= 100) return;
    
    /* calc value of BAT_MAX_USB_CURRENT register for specified mA */
    maxUsbCurrentRegVal = BAT_MAX_USB_CURRENT_MA(maxUsbMa);
    
    /* Select I2C functionality for the I/O pins used by the I2C interface.
     * (SDA and SCL). Note when the I2C functionality is selected for an I/O
     * pin, the MSP430 automatically controls the pin's direction, so there's
     * no need to set the direction explicitly (see the I/O pin schematics in
     * the data sheet for the MSP430 being used).
     */
    I2C_SDA_P(SEL) |= I2C_SDA_BIT;
    I2C_SCL_P(SEL) |= I2C_SCL_BIT;
    
    /* Init I2C & write maximum USB current setting to BAT_MAX_USB_CURRENT
     * register in LP3947. Note I2CEN is the same bit as SWRST (different
     * meaning for I2C & other USART modes), which is 1 by default after reset.
     */
    U0CTL = I2C | SYNC | I2CEN; /* procedure specified in MSP430 manual */
    U0CTL &= ~I2CEN; /* disable I2C */
    I2CTCTL = I2CSSEL_1; /* ACLK (8 MHz) */
    I2CSCLL = 9; /* SCL low for 1.375 us = (9 + 2) x (0.125 us) */
    I2CSCLH = 9; /* SCL high for 1.375 us = (9 + 2) x (0.125 us) */
    I2CSA = BAT_CHARGER_I2C_ADDR; /* LP3947's I2C address */
    U0CTL |= I2CEN; /* enable I2C */
    
    I2CNDAT = 2; /* set transfer byte count (transmit reg addr & value) */
    U0CTL |= MST; /* I2C master (auto-cleared on stop or lost arbitration) */
    I2CTCTL |= (I2CSTT | I2CSTP | I2CTRX); /* start write transfer (autostop) */
    while((I2CIFG & TXRDYIFG) == 0);
    I2CDRB = BAT_MAX_USB_CURRENT; /* transmit address of LP3947 register */
    while((I2CIFG & TXRDYIFG) == 0);
    I2CDRB = maxUsbCurrentRegVal; /* transmit value to write to reg */
    while((I2CIFG & ARDYIFG) == 0); /* wait for write transfer to finish */
    
    /* ensure I2C is done (not busy), then disable it */
    while(I2CBB & I2CDCTL);
    U0CTL &= ~I2CEN;
    
    /* set BAT_CHARGE_HIGH to put charger in high power mode */
    BAT_WR_CHARGE_HIGH(TRUE);
}
