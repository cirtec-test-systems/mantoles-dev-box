/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Adp/General.h"         /* RSTAT, TRUE/FALSE, ... */
#include "Adp/Build/Build.h"     /* INT_ON_RISING_EDGE, ... */
#include "Adp/AdpBoard/UsbHw.h"  /* USB_POWER_OFF, ... */
#include "Adp/AdpBoard/UsbLib.h" /* public include for UsbLib */

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/**************************************************************************
 * Function Prototypes
 */
 
/**************************************************************************
 * Initialize the USB interface.
 */
RSTAT
UsbInit(void)
{
    /* Configure USB power interrupt to occur when USB_POWER_OFF goes low (USB
     * connected and initialized). If we already missed the falling edge, set
     * the interrupt flag so the interrupt will occur.
     */
    USB_WR_POWER_INT_EDGE(INT_ON_FALLING_EDGE);
    USB_WR_POWER_INT_FLAG(FALSE);
    if (!USB_POWER_OFF) USB_WR_POWER_INT_FLAG(TRUE);
    USB_WR_POWER_INT_ENAB(TRUE);
    
    /* Configure USB RX interrupt to occur when USB_RX_EMPTY goes low (byte
     * available in RX buf). Note the interrupt is not enabled until USB is
     * connected and initialized (see USB_POWER_INT_FLAG and UsbPowerIsr()).
     */
    USB_WR_RX_INT_EDGE(INT_ON_FALLING_EDGE);
    
    return(0);
}

void
UsbPowerIsr(void)
{
    /* if USB is disconnected */
    if (USB_POWER_OFF) {
        
        /* Clear the USB power interrupt, then configure it to occur again when
         * USB_POWER_OFF goes low (if we already missed the falling edge, set
         * the interrupt flag, so as soon as the ISR returns, it will recur).
         */
        USB_WR_POWER_INT_EDGE(INT_ON_FALLING_EDGE);
        USB_WR_POWER_INT_FLAG(FALSE);
        if (!USB_POWER_OFF) USB_WR_POWER_INT_FLAG(TRUE);
        
        /* disable USB RX interrupt */
        USB_WR_RX_INT_ENAB(FALSE);
        
    } else { /* else, USB is connected and initialized */
        
        /* Clear the USB power interrupt, then configure it to occur again when
         * USB_POWER_OFF goes high (if we already missed the rising edge, set
         * the interrupt flag, so as soon as the ISR returns, it will recur).
         */
        USB_WR_POWER_INT_EDGE(INT_ON_RISING_EDGE);
        USB_WR_POWER_INT_FLAG(FALSE);
        if (USB_POWER_OFF) USB_WR_POWER_INT_FLAG(TRUE);
        
        /* Clear any old USB RX interrupt flag, then enable interrupt (if we
         * already missed the falling edge, set flag so interrupt will occur).
         */
        USB_WR_RX_INT_FLAG(FALSE);
        if (!USB_RX_EMPTY) USB_WR_RX_INT_FLAG(TRUE);
        USB_WR_RX_INT_ENAB(TRUE);
    }
}
