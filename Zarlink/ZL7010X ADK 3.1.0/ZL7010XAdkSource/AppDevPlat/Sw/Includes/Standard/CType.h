/**************************************************************************
 * This just includes the corresponding standard include file that comes
 * with the compiler. Source files should include this instead of directly
 * including the standard file so it can easily be customized if needed for 
 * different compilers, etc.
 *
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
/* ensure this file is only included once */
#ifndef Standard_CType_h
#define Standard_CType_h

/* include the corresponding standard include file */
#include <ctype.h>

#endif /* ensure this file is only included once */
