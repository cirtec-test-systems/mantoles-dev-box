/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adp_AdpBoard_AdpAppErrs_h
#define Adp_AdpBoard_AdpAppErrs_h

/**************************************************************************
 * Defines and Macros
 */
 
/* Error group and error codes for the application software on the ADP board.
 * Note for the error group, the firmware should always reference the global
 * variable AdpAppErr instead of ADP_APP_ERR, so all of the references will
 * share the same constant global string.
 * 
 * The error ID strings (EID) include a detailed message for each error. These
 * are provided for use on the host (PC). For the format and use of error ID
 * strings, see "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c". The firmware doesn't use
 * the error ID strings because they would require too much memory (the
 * firmware only uses error groups and error codes).
 */
extern const char *AdpAppErr;
#define ADP_APP_ERR "AdpAppErr"
/**/
#define ADP_APP_ERR_I2C_NOT_YET_SUPPORTED                                    1
#define ADP_APP_EID_I2C_NOT_YET_SUPPORTED \
    "AdpAppErr.1: The ADP board does not yet support command/reply " \
    "packet communication via the I2C interface."
#define ADP_APP_ERR_RECEIVED_CMD_WITH_INVALID_CMD_TYPE                       2
#define ADP_APP_EID_RECEIVED_CMD_WITH_INVALID_CMD_TYPE \
    "AdpAppErr.2: The ADP board received a command packet with an " \
    "invalid command type ({0})."
#define ADP_APP_ERR_RECEIVED_CMD_WITH_INVALID_LEN                            3
#define ADP_APP_EID_RECEIVED_CMD_WITH_INVALID_LEN \
    "AdpAppErr.3: The ADP board received a command packet with an " \
    "invalid length."
#define ADP_APP_ERR_USB_RX_TIMEOUT                                           4
#define ADP_APP_EID_USB_RX_TIMEOUT \
    "AdpAppErr.4: The ADP board timed out receiving bytes from the " \
    "USB interface."
#define ADP_APP_ERR_USB_TX_TIMEOUT                                           5
#define ADP_APP_EID_USB_TX_TIMEOUT \
    "AdpAppErr.5: The ADP board timed out transmitting bytes to the " \
    "USB interface."
#define ADP_APP_ERR_ADP_SPI_RX_TIMEOUT                                       6
#define ADP_APP_EID_ADP_SPI_RX_TIMEOUT \
    "AdpAppErr.6: The ADP board timed out receiving bytes from the " \
    "mezzanine board via the ADP SPI interface."
#define ADP_APP_ERR_ADP_SPI_TX_TIMEOUT                                       7
#define ADP_APP_EID_ADP_SPI_TX_TIMEOUT \
    "AdpAppErr.7: The ADP board timed out transmitting bytes to the " \
    "mezzanine board via the ADP SPI interface."
#define ADP_APP_ERR_RECEIVED_PACK_WITH_DEST_0                                8
#define ADP_APP_EID_RECEIVED_PACK_WITH_DEST_0 \
    "AdpAppErr.8: The ADP board received a command or reply packet " \
    "with destination address 0."
#define ADP_APP_ERR_FAILED_TO_SET_MAX_BAT_CHARGE                             9
#define ADP_APP_EID_FAILED_TO_SET_MAX_BAT_CHARGE \
    "AdpAppErr.9: The ADP board failed to set the maximum current to " \
    "draw from USB to charge the battery."
#define ADP_APP_ERR_FAILED_TO_SET_VSUP1                                      10
#define ADP_APP_EID_FAILED_TO_SET_VSUP1 \
    "AdpAppErr.10: The ADP board failed to set VSUP1."
#define ADP_APP_ERR_FAILED_TO_SET_VSUP2                                      11
#define ADP_APP_EID_FAILED_TO_SET_VSUP2 \
    "AdpAppErr.11: The ADP board failed to set VSUP2."
#define ADP_APP_ERR_ILLEGAL_CMD_WHILE_DMA_RUNNING                            12
#define ADP_APP_EID_ILLEGAL_CMD_WHILE_DMA_RUNNING \
    "AdpAppErr.12: The ADP board received an illegal command while DMA was running."

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global Declarations
 */

/**************************************************************************
 * External Function Prototypes
 */
 
#endif /* ensure this file is only included once */
