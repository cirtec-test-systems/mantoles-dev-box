/**************************************************************************
 * This include file contains defines and structures used to communicate
 * with the ADP board.
 *
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
/* ensure this file is only included once */
#ifndef Adp_AdpBoard_AdpCom_h
#define Adp_AdpBoard_AdpCom_h

#include "Adp/AnyBoard/Com.h"        /* COM_LOCAL_ADP_ADDR, ... */
#include "Adp/AdpBoard/AdpGeneral.h" /* ADP_CONFIG, ADP_STAT, ... */

/**************************************************************************
 * Defines and Macros
 */
 
/* Command types for ADP board. The command and reply structures for each of 
 * these are defined later on. Note the ADP board also supports the commands
 * that are common to all devices (see COM_GET_DEV_TYPE_CMD_TYPE, etc. in
 * "AppDevPlat\Sw\Includes\Adp\AnyBoard\Com.h").
 */
#define ADP_GET_STAT_CMD_TYPE       1  /* 0x01 */
#define ADP_RESERVED_CMD_TYPE_2     2  /* 0x02 (obsolete) */
#define ADP_GET_TRACE_MSG_CMD_TYPE  3  /* 0x03 */
#define ADP_GET_CONFIG_CMD_TYPE     4  /* 0x04 */
#define ADP_SET_CONFIG_CMD_TYPE     5  /* 0x05 */
#define ADP_SET_VSUP_CMD_TYPE       6  /* 0x06 */
#define ADP_START_POW_MON_CMD_TYPE  7  /* 0x07 */
#define ADP_STOP_POW_MON_CMD_TYPE   8  /* 0x08 */
#define ADP_GET_POW_CMD_TYPE        9  /* 0x09 */

/* Addresses for the local and remote ADP boards. The local board is the board
 * on the local side of the wireless link, which is connected to the PC via USB.
 * The remote board is the board on the remote side of the wireless link. These
 * addresses are used for the source & destination in communication packet
 * headers (see COM_PACK_HDR.srcAndDest in "Adp/AnyBoard/Com.h"). Note these
 * addresses are reserved for the ADP board and must not be used for the
 * mezzanine boards or any other devices.
 */
#define ADP_LOCAL_ADDR   COM_LOCAL_ADP_ADDR
#define ADP_REMOTE_ADDR  COM_REMOTE_ADP_ADDR

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/******************************************************************
 * Structures for ADP_GET_STAT_CMD_TYPE:
 */
typedef ADP_STAT  ADP_GET_STAT_REPLY;

/******************************************************************
 * Structures for ADP_GET_CONFIG_CMD_TYPE:
 */
typedef ADP_CONFIG  ADP_GET_CONFIG_REPLY;

/******************************************************************
 * Structures for ADP_SET_CONFIG_CMD_TYPE:
 */
typedef ADP_CONFIG  ADP_SET_CONFIG_REPLY;

/******************************************************************
 * Structures for ADP_SET_VSUP_CMD_TYPE:
 */
typedef struct {
    
    /* VSUP1 & VSUP2 voltage levels (millivolts, 0 = disable) */
    UD16 vsup1Mv;
    UD16 vsup2Mv;
    
} ADP_SET_VSUP_CMD;

/******************************************************************
 * Structures for ADP_START_POW_MON_CMD_TYPE:
 */

typedef ADP_POW_MON_OPTIONS  ADP_START_POW_MON_CMD;



        
/**************************************************************************
 * Global Declarations
 */

/**************************************************************************
 * External Function Prototypes
 */
 
#endif /* ensure this file is only included once */
