/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adp_AdpBoard_BatHw_h
#define Adp_AdpBoard_BatHw_h

#include "Adp/General.h"     /* RD_BIT(), WR_BIT(), ... */
#include "Adp/Build/Build.h" /* BAT_CHARGE_OFF_BIT, BAT_CHARGE_OFF_P(), ... */

/**************************************************************************
 * Defines and Macros
 */
 
/* Defines for battery charger status and control (LP3947).
 */
#define BAT_CHARGE_OFF          RD_BIT(BAT_CHARGE_OFF_P(IN), BAT_CHARGE_OFF_BIT)
#define BAT_WR_CHARGE_OFF(val)  WR_BIT(BAT_CHARGE_OFF_P(OUT), BAT_CHARGE_OFF_BIT, val)
#define BAT_CHARGE_HIGH         RD_BIT(BAT_CHARGE_HIGH_P(IN), BAT_CHARGE_HIGH_BIT)
#define BAT_WR_CHARGE_HIGH(val) WR_BIT(BAT_CHARGE_HIGH_P(OUT), BAT_CHARGE_HIGH_BIT, val)

/**************************************************************************
 * Defines for registers in battery charger (LP3947 register addresses, etc.).
 */
 
/* Address of battery charger (LP3947) on I2C bus.
 */
#define BAT_CHARGER_I2C_ADDR  0x47
 
/* Register to select the maximum current to draw from USB to charge the
 * battery when in high power mode (see BAT_CHARGE_HIGH).
 */
#define BAT_MAX_USB_CURRENT        0x02  /* register 2 (R/W) */
#define BAT_MAX_USB_CURRENT_MASK   0x0F
#define BAT_MAX_USB_CURRENT_RESET  0x08  /* 500 mA */
#define BAT_MAX_USB_CURRENT_MA(maxUsbMa) \
    ((maxUsbMa < 100) ? 0: \
    ((maxUsbMa > 750) ? 0x0D : \
    ((maxUsbMa - 100) / 50)));
    
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global Declarations
 */

/**************************************************************************
 * External Function Prototypes
 */
 
#endif /* ensure this file is only included once */
