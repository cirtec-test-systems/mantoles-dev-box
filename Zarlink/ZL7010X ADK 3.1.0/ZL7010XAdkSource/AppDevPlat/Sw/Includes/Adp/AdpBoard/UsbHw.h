/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adp_AdpBoard_UsbHw_h
#define Adp_AdpBoard_UsbHw_h

#include "Adp/General.h"     /* RD_BIT(), WR_BIT(), ... */
#include "Adp/Build/Build.h" /* USB_TX_FULL_BIT, USB_DATA_P(), ... */

/**************************************************************************
 * Defines and Macros
 */
 
/* Defines for USB power status (PWREN# from FT245RQ chip). The FT245RQ sets
 * USB_POWER_OFF when the USB is detached or suspended, and clears it when USB
 * is connected and initialized.
 */
#define USB_POWER_OFF               RD_BIT(USB_POWER_OFF_P(IN), USB_POWER_OFF_BIT)
#define USB_POWER_INT_FLAG          RD_BIT(USB_POWER_OFF_P(IFG), USB_POWER_OFF_BIT)
#define USB_POWER_INT_ENAB          RD_BIT(USB_POWER_OFF_P(IE), USB_POWER_OFF_BIT)
#define USB_WR_POWER_INT_FLAG(val)  WR_BIT(USB_POWER_OFF_P(IFG), USB_POWER_OFF_BIT, val)
#define USB_WR_POWER_INT_ENAB(val)  WR_BIT(USB_POWER_OFF_P(IE), USB_POWER_OFF_BIT, val)
#define USB_WR_POWER_INT_EDGE(val)  WR_BIT(USB_POWER_OFF_P(IES), USB_POWER_OFF_BIT, val)

/* Defines for TX status and control (interrupt is not used).
 */
#define USB_TX_FULL            RD_BIT(USB_TX_FULL_P(IN), USB_TX_FULL_BIT)
#define USB_WR_TX_STROBE(val)  WR_BIT(USB_TX_STROBE_P(OUT), USB_TX_STROBE_BIT, val)

/* Defines for RX status, control, and interrupt.
 */
#define USB_RX_EMPTY             RD_BIT(USB_RX_EMPTY_P(IN), USB_RX_EMPTY_BIT)
#define USB_RX_INT_FLAG          RD_BIT(USB_RX_EMPTY_P(IFG), USB_RX_EMPTY_BIT)
#define USB_RX_INT_ENAB          RD_BIT(USB_RX_EMPTY_P(IE), USB_RX_EMPTY_BIT)
#define USB_WR_RX_INT_FLAG(val)  WR_BIT(USB_RX_EMPTY_P(IFG), USB_RX_EMPTY_BIT, val)
#define USB_WR_RX_INT_ENAB(val)  WR_BIT(USB_RX_EMPTY_P(IE), USB_RX_EMPTY_BIT, val)
#define USB_WR_RX_INT_EDGE(val)  WR_BIT(USB_RX_EMPTY_P(IES), USB_RX_EMPTY_BIT, val)
#define USB_WR_RX_STROBE(val)    WR_BIT(USB_RX_STROBE_P(OUT), USB_RX_STROBE_BIT, val)

/* Defines for TX & RX data.
 */
#define USB_TX_DATA      USB_DATA_P(OUT)
#define USB_RX_DATA      USB_DATA_P(IN)
#define USB_DATA_DIR     USB_DATA_P(DIR)
#define USB_DATA_DIR_RX  0x00
#define USB_DATA_DIR_TX  0xFF

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global Declarations
 */

/**************************************************************************
 * External Function Prototypes
 */
 
#endif /* ensure this file is only included once */
