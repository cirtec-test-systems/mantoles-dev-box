/**************************************************************************
 * This include file contains various defines and structures for status and
 * control of the ADP board.
 *
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
/* ensure this file is only included once */
#ifndef Adp_AdpBoard_AdpGeneral_h
#define Adp_AdpBoard_AdpGeneral_h

#include "Adp/Build/Build.h"     /* UD8, UD16, ... */

/**************************************************************************
 * Defines and Macros
 */

/* the USB ID and device type used to communicate with an ADP board */
#define ADP_DEV_USB_ID  0x15A01000  /* vendor ID 0x15A0, product ID 0x1000 */
#define ADP_DEV_TYPE    "ADP Board"

/* The board model name for the ADP100. Note that AdpGetBoardVer() returns the
 * board's model name in its version string, along with its firmware version
 * (see APP_DEV_PLAT_VER in "Adp/AppDevPlatVer.h").
 */
#define ADP100_NAME  "ADP100"
 
/* Macros to convert an ADC value for VSUP1 or VSUP2 to the corresponding
 * voltage (millivolts), and vice-versa. These macros use 32 bit integer math
 * at run-time because it's faster than floating point in the embedded software
 * (the floating point constants in the equation are processed at compile-time).
 * In the equations, 2.5 is the internal ADC reference voltage, 1.43 is the
 * external voltage divider used to scale VSUP1&2 down to 2.5 volts, and 0x0FFF
 * is the ADC value at the maximum input voltage (12 bit ADC).
 */
#define ADP_VSUP_ADC_TO_MV(vsupAdc) \
    (UD16)(((UD32)(vsupAdc) * (UD32)(2.5 * 1.43 * 1000)) / 0x0FFF)
#define ADP_VSUP_MV_TO_ADC(vsupMv) \
    (UD16)(((UD32)(vsupMv) * 0x0FFF) / (UD32)(2.5 * 1.43 * 1000))

/* Defines for power monitoring input (ADP_POW_MON_OPTIONS.input):
 * 
 * ADP_POW_NONE:
 *     Setup to monitor power, but don't actually monitor it (for test/debug).
 * ADP_POW_1:
 *     Monitor power for VSUP1.
 * ADP_POW_2:
 *     Monitor power for VSUP2.
 * ADP_POW_1_AND_2:
 *     Monitor power for VSUP1 and VSUP2.
 */
#define ADP_POW_NONE     0
#define ADP_POW_1        1
#define ADP_POW_2        2
#define ADP_POW_1_AND_2  3

/* Defines for various power monitoring options (ADP_POW_MON_OPTIONS.options);
 * 
 * ADP_HIGH_RANGE_POW_1:
 *     Set for high range VSUP1 power (clear for low range).
 * ADP_HIGH_RANGE_POW_2:
 *     Set for high range VSUP2 power (clear for low range).
 * ADP_CAL_POW_1:
 *     Set when calibrating VSUP1 power monitor.
 * ADP_CAL_POW_2:
 *     Set when calibrating VSUP2 power monitor.
 */
#define ADP_HIGH_RANGE_POW_1  (1 << 0)
#define ADP_HIGH_RANGE_POW_2  (1 << 1)
#define ADP_CAL_POW_1         (1 << 2)
#define ADP_CAL_POW_2         (1 << 3)

/* The maximum number of samples that will ever be returned in a power monitor
 * buffer (ADP_POW_BUF). Note each sample is 16 bits, so the number of bytes is
 * twice the number of samples.
 */
#define ADP_MAX_SAMPS_IN_POW_BUF  1000

#define NR_SAMPS_IN_DMA_BUFFER (1000)

/**************************************************************************
 * Data Structures and Typedefs
 */

/* ADP board configuration. Note this structure is passed between the PC and
 * the firmware on the ADP board, so if you modify this structure, please add
 * padding as needed to ensure all 32-bit fields begin at an offset that's
 * divisible by 4, and all 16-bit fields begin at an offset that's divisible
 * by 2. This ensures the different compilers for the PC and firmware will
 * never add hidden padding, so when this structure is passed between the PC
 * and the firmware, it will always match.
 */
typedef struct {

    /* ADP board configuration flags (reserved for future use) */
    UD8 flags;

    /* ensure next field (UD16) is aligned (see comment for structure) */
    UD8 unused1;

    /* power config (VSUP1, VSUP2, etc.) */
    struct AdpPowConfig {

        /* desired values for VSUP1 & VSUP2 (millivolts; 0 = off) */
        UD16 vsup1Mv;
        UD16 vsup2Mv;

    } pow;

} ADP_CONFIG;
/*
 * Typedefs for structures nested in ADP_CONFIG (for convenience).
 */
typedef struct AdpPowConfig  ADP_POW_CONFIG;

/* ADP board status. Note this structure is passed between the PC and the
 * firmware on the ADP board, so if you modify this structure, please add
 * padding as needed to ensure all 32-bit fields begin at an offset that's
 * divisible by 4, and all 16-bit fields begin at an offset that's divisible
 * by 2. This ensures the different compilers for the PC and firmware will
 * never add hidden padding, so when this structure is passed between the PC
 * and the firmware, it will always match.
 */
typedef struct {

    /* flags (see ADP_ERR_OCCURRED, ...) */
    UD8 flags;

    /* ensure next field (UD16) is aligned (see comment for structure) */
    UD8 unused1;

    /* Current ADC levels for VSUP1 and VSUP2. To convert this to millivolts,
     * see ADP_VSUP_ADC_TO_MV() (best to do on PC because of 32 bit math).
     */
    UD16 vsup1Adc;
    UD16 vsup2Adc;

} ADP_STAT;
/*
 * Defines for ADP_STAT.flags:
 *
 * ADP_ERR_OCCURRED:
 *     This is set when the ADP board detects an error, and cleared when the PC
 *     application retrieves the error information. Note when AdpGetStat() is
 *     called to get the board's status, it will retrieve the error information
 *     and return it to the caller (see AdpGetStat() in "AppDevPlat\Sw\Pc\
 *     Libs\AdpLib.c").
 */
#define ADP_ERR_OCCURRED  (1 << 0)

/* Power monitoring options. Note this structure is passed between the PC and
 * the firmware on the ADP board, so if you modify this structure, please add
 * padding as needed to ensure all 32-bit fields begin at an offset that's
 * divisible by 4, and all 16-bit fields begin at an offset that's divisible
 * by 2. This ensures the different compilers for the PC and firmware will
 * never add hidden padding, so when this structure is passed between the PC
 * and the firmware, it will always match.
 */
typedef struct {

    /* power supply to monitor (ADP_POW_1, ...) */
    UD8 input;

    /* various options (ADP_HIGH_RANGE_POW_1, ...) */
    UD8 options;

    /* number of microseconds between samples */
    UD16 usPerSamp;

} ADP_POW_MON_OPTIONS;

/* Information included with a buffer of power data (see ADP_POW_BUF).
 *
 * Note this structure is passed between the PC and the firmware on the ADP
 * board, so if you modify this structure, please add padding as needed to
 * ensure all 32-bit fields begin at an offset that's divisible by 4, and all
 * 16-bit fields begin at an offset that's divisible by 2. This ensures the
 * different compilers for the PC and firmware will never add hidden padding,
 * so when this structure is passed between the PC and the firmware, it will
 * always match.
 *
 * Also, please add padding as needed to ensure the size of this structure
 * is a multiple of 2. This ensures the different compilers for the PC and
 * firmware will never add hidden padding between ADP_POW_BUF.info and
 * ADP_POW_BUF.samps, so when the ADP_POW_BUF structure is passed between
 * the PC and the firmware, it will always match.
 */
typedef struct {

    /* serial number of buffer being sent or -1 if no data available */
    SD16 dmaSnAvailable;

    /* number of samples in ADP_POW_BUF */
    UD16 nSamps;

    /* various power info flags (ADP_MORE_POW_AVAIL, ...) */
    UD8 flags;

    /* ensure structure size is a multiple of 2 (see comment for structure) */
    UD8 unused1;

} ADP_POW_BUF_INFO;
/*
 * Defines for ADP_POW_BUF_INFO.flags:
 *
 * ADP_MORE_POW_AVAIL:
 *     True if more power data is available (call AdpGetPow() again to get it).
 */
#define ADP_MORE_POW_AVAIL  (1 << 0)

/* Buffer to hold power monitoring information and data. Note this is large
 * enough to hold the maximum number of samples that will ever be returned by
 * the power monitoring.
 *
 * Note this structure is passed between the PC and the firmware on the ADP
 * board, so if you modify this structure, please add padding as needed to
 * ensure all 32-bit fields begin at an offset that's divisible by 4, and all
 * 16-bit fields begin at an offset that's divisible by 2. This ensures the
 * different compilers for the PC and firmware will never add hidden padding,
 * so when this structure is passed between the PC and the firmware, it will
 * always match.
 */
typedef struct {

    /* buffer info (number of samples, etc.) */
    ADP_POW_BUF_INFO info;

    /* power monitoring samples */
    SD16 samps[ADP_MAX_SAMPS_IN_POW_BUF];

} ADP_POW_BUF;
 
/**************************************************************************
 * Global Declarations
 */

/**************************************************************************
 * External Function Prototypes
 */

#endif /* ensure this file is only included once */
