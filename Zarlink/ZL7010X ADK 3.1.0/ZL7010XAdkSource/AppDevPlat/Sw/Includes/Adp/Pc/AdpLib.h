/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
/* ensure this file is only included once */
#ifndef Adp_Pc_AdpLib_h
#define Adp_Pc_AdpLib_h

#include "Adp/General.h"             /* RSTAT, ... */
#include "Adp/Build/Build.h"         /* UD32, EXTC, ... */
#include "Adp/Pc/ErrInfoLib.h"       /* EI, EiSet() */
#include "Adp/Pc/ComLib.h"           /* COM_ID */  
#include "Adp/AnyBoard/Com.h"        /* COM_DEV_VER_BUF_SIZE, ... */
#include "Adp/AdpBoard/AdpGeneral.h" /* ADP_CONFIG, ADP_STAT, ... */

/**************************************************************************
 * Defines and Macros
 */
 
/* Error ID strings for the ADP board library. For the format and use of
 * error ID strings, see "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c".
 */
#define ADP_LIB_EID_INVALID_SPECS \
    "AdpLibErr.InvalidSpecs: Invalid ADP_SPECS structure (it appears it " \
    "was not initialized by calling AdpInitSpecs(), or is too big)."
#define ADP_LIB_EID_FAILED_TO_ALLOCATE_MEM \
    "AdpLibErr.FailedToAllocateMem: Failed to allocate {0} bytes of memory."
#define ADP_LIB_EID_NO_LOCAL_ADP_ID \
    "AdpLibErr.NoLocalAdpId: When \"remote\" is specified in the ADP_SPECS " \
    "passed to AdpOpen(), \"localAdpId\" must also be specified."
#define ADP_LIB_EID_INCOMPATIBLE_BOARD_MODEL \
    "AdpLibErr.IncompatibleBoardModel: The board version ({0}) does not " \
    "contain an ADP board model that is supported by the ADP library."
#define ADP_LIB_EID_INCOMPATIBLE_ADP_BOARD_VER \
    "AdpLibErr.IncompatibleAdpBoardVer: The ADP board version ({0}) is not " \
    "compatible with the version required by the ADP library ({1})."
#define ADP_LIB_EID_INVALID_STRUCT_SIZE \
    "AdpLibErr.InvalidStructSize: Specified structure was the wrong size."
#define ADP_LIB_EID_INVALID_ADP_ID \
    "AdpLibErr.InvalidAdpId: Specified ADP_ID was invalid (it may have been " \
    "closed - make sure nothing tries to use an ADP_ID after it is closed)."
    
/* Size of buffer required to hold a maximum length version string for
 * ADP board (and its terminating '\0'). The buffer passed to AdpGetBoardVer()
 * should be this size or larger to ensure it can hold the full version string.
 */
#define ADP_BOARD_VER_BUF_SIZE  COM_DEV_VER_BUF_SIZE
#define ADP_BOARD_VER_MAX_LEN   COM_DEV_VER_MAX_LEN
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* type for ID returned by AdpOpen() */
typedef const void * ADP_ID;
 
/* Structure for specifications passed to AdpOpen(). Note before passing
 * this to AdpOpen(), you must call AdpInitSpecs() to initialize it with
 * default values, then override any settings for which you do not want
 * the default value.
 */
typedef struct {

    /* Internal magic number set by AdpInitSpecs() (used to verify the specs
     * were initialized by calling AdpInitSpecs()).
     */
    UD32 magic;
    
    /* If "remote" is false, AdpOpen() will connect to a local ADP board (that
     * is, an ADP board connected directly to the PC via USB). In that case,
     * ADP_SPECS.localMezzDevType and ADP_SPECS.localMezzDevName can be
     * specified to identify which local ADP board, if desired.
     * 
     * If "remote" is true, it's ignored (not yet supported). For future
     * reference, if "remote" is true, AdpOpen() will connect to a remote
     * ADP board via a wireless link. In that case, ADP_SPECS.localAdpId must
     * specify an existing connection to a local ADP board, which will then be
     * used to communicate with the remote ADP board (the PC will send commands
     * to the local ADP board via USB, which will forward them to its mezzanine
     * board via SPI, which will forward them to a remote mezzanine board via
     * the wireless link, which will forward them to the remote ADP board via
     * SPI. Note before attempting to communicate with a remote ADP board, you
     * must establish the wireless link (otherwise, it will just time out).
     * 
     * AdpInitSpecs() sets this to false by default.
     */
    BOOL remote;
    
    /* If ADP_SPECS.remote is false, these specify the type and name of the
     * mezzanine board attached to the local ADP board (see ADP_SPECS.remote
     * for more information). If no type is specified (NULL), both the type
     * and name will be ignored, and AdpOpen() will connect to the first local
     * ADP board it finds. If no name is specified (NULL), it will be ignored.
     * 
     * AdpInitSpecs() sets these to NULL by default.
     */
    const char *localMezzDevType;
    const char *localMezzDevName;
    
    /* This is not yet supported. For future reference, if ADP_SPECS.remote
     * is true, this must specify an existing connection to a local ADP board,
     * which will then be used to communicate with the remote ADP board (see
     * ADP_SPECS.remote for more information).
     * 
     * Note AdpInitSpecs() sets this to NULL by default, so if ADP_SPECS.remote
     * is true, the calling application must override this (otherwise, AdpOpen()
     * will fail).
     */
    ADP_ID localAdpId;
    
    /* The default timeout for sending commands and receiving replies.
     * AdpInitSpecs() sets this to COM_DEF_TIMEOUT_MS by default (see
     * "AppDevPlat\Sw\Includes\Adp\Pc\ComLib.h").
     */
    UD32 defTimeoutMs;
    
} ADP_SPECS;

/**************************************************************************
 * Global Declarations
 */
 
/**************************************************************************
 * External Function Prototypes
 */
 
EXTC const char *AdpGetApiVer(void);
 
EXTC void AdpInitSpecs(ADP_SPECS *specs, UD32 specsSize);
 
EXTC ADP_ID AdpOpen(const ADP_SPECS *specs, UD32 specsSize, EI e);

EXTC RSTAT AdpGetBoardVer(ADP_ID adpId, char *buf, UD32 bufSize, EI e);

EXTC RSTAT AdpGetStat(ADP_ID adpId, ADP_STAT *as, UD32 asSize, EI e);

EXTC COM_ID AdpGetComId(ADP_ID adpId, EI e);

EXTC RSTAT AdpGetTraceMsg(ADP_ID adpId, char *buf, UD32 bufSize, EI e);

EXTC RSTAT AdpGetConfig(ADP_ID adpId, ADP_CONFIG *ac, UD32 acSize, EI e);

EXTC RSTAT AdpSetConfig(ADP_ID adpId, const ADP_CONFIG *ac, UD32 acSize, EI e);

EXTC RSTAT AdpSetVsup(ADP_ID adpId, UD32 vsup1Mv, UD32 vsup2Mv, EI e);

EXTC RSTAT AdpSetConfig(ADP_ID adpId, const ADP_CONFIG *ac, UD32 acSize, EI e);

EXTC RSTAT AdpSetConfig(ADP_ID adpId, const ADP_CONFIG *ac, UD32 acSize, EI e);

EXTC RSTAT AdpStartPowMon(ADP_ID adpId, const ADP_POW_MON_OPTIONS *pmo, UD32 pmoSize, EI e);

EXTC RSTAT AdpStopPowMon(ADP_ID adpId, EI e);

EXTC RSTAT AdpGetPow(ADP_ID adpId, ADP_POW_BUF *pb, UD32 pbSize, EI e);

EXTC RSTAT AdpClose(ADP_ID adpId, EI e);

#endif /* ensure this file is only included once */
