/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adp_Pc_ErrInfoLib_h
#define Adp_Pc_ErrInfoLib_h

#include "Standard/StdArg.h" /* va_list, ... */
#include "Adp/General.h"     /* RSTAT, ... */
#include "Adp/Build/Build.h" /* UD32, BOOL32, EXTC, ... */

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* type for ID returned by EiOpen() */
typedef const void * EI;

/* Structure and defines for specifications passed to EiOpen(). Note you
 * must call EiInitSpecs() to initialize the specifications with default
 * values before passing it to EiOpen().
 */
typedef struct {

    /* Internal magic number set by EiInitSpecs() (used to verify the specs
     * were initialized by calling EiInitSpecs()).
     */
    UD32 magic;
    
} EI_SPECS;
 
/**************************************************************************
 * Global Declarations
 */
 
/**************************************************************************
 * External Function Prototypes
 */
 
EXTC void EiInitSpecs(EI_SPECS *specs, UD32 specsSize);
 
EXTC EI EiOpen(const EI_SPECS *specs, UD32 specsSize);

EXTC void EiSet(EI e, const char *eid, const char *argFormats, ...);
    
EXTC void EiSetVa(EI e, const char *eid, const char *argFormats, va_list va);

EXTC BOOL32 EiErrIs(EI e, const char *eid);

EXTC BOOL32 EiGroupIs(EI e, const char *group);

EXTC const char *EiGetId(EI e);

EXTC const char *EiGetArgs(EI e);

EXTC const char *EiGetMsg(EI e);
    
EXTC RSTAT EiClose(EI e);
 
#endif /* ensure this file is only included once */
