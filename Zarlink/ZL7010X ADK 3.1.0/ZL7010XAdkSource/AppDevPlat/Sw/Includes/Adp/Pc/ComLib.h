/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adp_Pc_ComLib_h
#define Adp_Pc_ComLib_h

#include "Adp/General.h"       /* RSTAT, ... */
#include "Adp/Build/Build.h"   /* UD32, EXTC, ... */
#include "Adp/Pc/ErrInfoLib.h" /* EI, ... */

/**************************************************************************
 * Defines and Macros
 */
 
/* default timeout (milliseconds) */
#define COM_DEF_TIMEOUT_MS  5000

/* Error ID strings for the communications interface library. For the format
 * and use of error ID strings, see "AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c".
 */
#define COM_LIB_EID_INVALID_SPECS \
    "ComLibErr.InvalidSpecs: Invalid COM_SPECS structure (it appears it " \
    "was not initialized by calling ComInitSpecs(), or is too big)."
#define COM_LIB_EID_FAILED_TO_ALLOCATE_MEM \
    "ComLibErr.FailedToAllocateMem: Failed to allocate {0} bytes of memory."
#define COM_LIB_EID_COULD_NOT_FIND_NAMED_DEV \
    "ComLibErr.CouldNotFindNamedDev: Could not find device type \"{0}\" " \
    "named \"{1}\"."
#define COM_LIB_EID_COULD_NOT_FIND_DEV \
    "ComLibErr.CouldNotFindDev: Could not find device type \"{0}\"."
#define COM_LIB_EID_DEV_ADDR_EXCEEDED_MAX \
    "ComLibErr.DevAddrExceededMax: Specified device address {0} > maximum."
#define COM_LIB_EID_CMD_TYPE_EXCEEDED_MAX \
    "ComLibErr.CmdTypeExceededMax: Specified command type {0} > maximum."
#define COM_LIB_EID_CMD_LEN_EXCEEDED_MAX \
    "ComLibErr.CmdLenExceededMax: Specified command length {0} > maximum."
#define COM_LIB_EID_REPLY_TOO_SHORT \
    "ComLibErr.ReplyTooShort: The reply from the device at address {0} for " \
    "command {1} was too short."
#define COM_LIB_EID_NO_BUF_FOR_REPLY \
    "ComLibErr.NoBufForReply: No buffer was specified for a reply, but the " \
    "reply required one."
#define COM_LIB_EID_REPLY_TOO_BIG_FOR_BUF \
    "ComLibErr.ReplyTooBigForBuf: A reply was too big for the specified buffer."
#define COM_LIB_EID_SEND_TIMEOUT \
    "ComLibErr.SendTimeout: Timed out sending data to device."
#define COM_LIB_EID_RECEIVE_TIMEOUT \
    "ComLibErr.ReceiveTimeout: Timed out waiting to receive data from device."
#define COM_LIB_EID_FAILED_TO_CREATE_SEM \
    "ComLibErr.FailedToCreateSem: Failed to create a semaphore."
#define COM_LIB_EID_FAILED_TO_WAIT_FOR_SEM \
    "ComLibErr.FailedToWaitForSem: Failed to wait for a semaphore."
#define COM_LIB_EID_FAILED_TO_RELEASE_SEM \
    "ComLibErr.FailedToReleaseSem: Failed to release a semaphore."
#define COM_LIB_EID_INVALID_STRUCT_SIZE \
    "ComLibErr.InvalidStructSize: Specified structure was the wrong size."
#define COM_LIB_EID_INVALID_COM_ID \
    "ComLibErr.InvalidComId: Specified COM_ID was invalid (it may have been " \
    "closed - make sure nothing tries to use a COM_ID after it is closed)."
#define COM_LIB_EID_FAILED_FT_CREATE_DEV_INFO_LIST \
    "ComLibErr.FailedFtCreateDevInfoList: FT_CreateDeviceInfoList() returned " \
    "error status {0}."
#define COM_LIB_EID_FAILED_FT_GET_DEV_INFO_LIST \
    "ComLibErr.FailedFtGetDevInfoList: FT_GetDeviceInfoList() returned " \
    "error status {0}."
#define COM_LIB_EID_FAILED_FT_READ \
    "ComLibErr.FailedFtRead: FT_Read() returned error status {0}."
#define COM_LIB_EID_FAILED_FT_WRITE \
    "ComLibErr.FailedFtWrite: FT_Write() returned error status {0}."
#define COM_LIB_EID_FAILED_FT_SET_TIMEOUTS \
    "ComLibErr.FailedFtSetTimeouts: FT_SetTimeouts() returned " \
    "error status {0}."
#define COM_LIB_EID_FAILED_FT_SET_LATENCY_TIMER \
    "ComLibErr.FailedFtSetLatencyTimer: FT_SetLatencyTimer() returned " \
    "error status {0}."
#define COM_LIB_EID_FAILED_FT_SET_FLOW_CONTROL \
    "ComLibErr.FailedFtSetFlowControl: FT_SetFlowControl() returned " \
    "error status {0}."
    
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* type for ID returned by ComOpen() */
typedef const void * COM_ID;
 
/* Structure for specifications passed to ComOpen(). Note before passing
 * this to ComOpen(), you must call ComInitSpecs() to initialize it with
 * default values, then override any settings for which you do not want
 * the default value.
 */
typedef struct {

    /* Internal magic number set by ComInitSpecs() (used to verify the specs
     * were initialized by calling ComInitSpecs()).
     */
    UD32 magic;
    
    /* Type and name of the mezzanine board attached to the desired ADP board.
     * If a type is specified, ComOpen() will search for a local ADP board that
     * is attached to the specified type of mezzanine board, then connect to
     * that ADP board. If a name is also specified, ComOpen() with search for
     * the specified name as well as the type.
     * 
     * If no type is specified (NULL), the name will be ignored, and ComOpen()
     * will connect to the first ADP board it finds.
     * 
     * ComInitSpecs() sets these to NULL by default.
     */
    const char *localMezzDevType;
    const char *localMezzDevName;
    
    /* Default timeout for sending commands and receiving replies.
     * ComInitSpecs() sets this to COM_DEF_TIMEOUT_MS by default.
     */
    UD32 defTimeoutMs;
    
} COM_SPECS;
 
/**************************************************************************
 * Global Declarations
 */
 
/**************************************************************************
 * External Function Prototypes
 */
 
EXTC void ComInitSpecs(COM_SPECS *specsBuf, UD32 specsBufSize);
 
EXTC COM_ID ComOpen(const COM_SPECS *specs, UD32 specsSize, EI e);

EXTC RSTAT ComGetDevType(COM_ID comId, UD32 devAddr, char *buf, UD32 bufSize,
    EI e);
    
EXTC RSTAT ComGetDevName(COM_ID comId, UD32 devAddr, char *buf, UD32 bufSize,
    EI e);
    
EXTC RSTAT ComGetDevVer(COM_ID comId, UD32 devAddr, char *buf, UD32 bufSize,
    EI e);
    
EXTC RSTAT ComGetDevErr(COM_ID comId, UD32 devAddr, EI e);

EXTC RSTAT ComCmdAndReply(COM_ID comId, UD32 devAddr, UD32 cmdType,
    const void *cmd, UD32 cmdLen, void *repBuf, UD32 repBufSize, EI e);
    
EXTC RSTAT ComCmdAndReplyT(COM_ID comId, UD32 devAddr, UD32 cmdType,
    const void *cmd, UD32 cmdLen, void *repBuf, UD32 repBufSize,
    UD32 timeoutMs, EI e);
 
EXTC SD32 ComCmdAndVarReply(COM_ID comId, UD32 devAddr, UD32 cmdType,
    const void *cmd, UD32 cmdLen, void *repBuf, UD32 repBufSize, EI e);
    
EXTC SD32 ComCmdAndVarReplyT(COM_ID comId, UD32 devAddr, UD32 cmdType,
    const void *cmd, UD32 cmdLen, void *repBuf, UD32 repBufSize,
    UD32 timeoutMs, EI e);
    
EXTC RSTAT ComSetDefTimeout(COM_ID comId, UD32 defTimeoutMs, EI e);

EXTC RSTAT ComClose(COM_ID comId, EI e);
 
#endif /* ensure this file is only included once */
