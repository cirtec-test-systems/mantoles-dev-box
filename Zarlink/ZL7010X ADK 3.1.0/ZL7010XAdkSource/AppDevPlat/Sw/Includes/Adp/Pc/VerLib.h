/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adp_Pc_VerLib_h
#define Adp_Pc_VerLib_h

#include "Adp/Build/Build.h" /* UD32, BOOL32, EXTC, ... */

/**************************************************************************
 * Defines and Macros
 */

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global Declarations
 */
 
/**************************************************************************
 * External Function Prototypes
 */
 
EXTC BOOL32 VerIsCompat(const char *actualVer, const char *requiredVer);
 
EXTC const char * VerParse(const char *ver, UD32 *nameLen, UD32 *major,
    UD32 *minor, UD32 *patch, const char **tag, UD32 *tagLen);
 
#endif /* ensure this file is only included once */
