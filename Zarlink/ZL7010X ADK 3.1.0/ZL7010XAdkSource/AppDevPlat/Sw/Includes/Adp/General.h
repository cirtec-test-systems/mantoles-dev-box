/**************************************************************************
 * This file contains general defines and macros that are commonly used in
 * source files (UINT, TRUE/FALSE, RSTAT, BOOL, RD_BIT(), WR_BIT(), etc.).
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
/* ensure this file is only included once */
#ifndef Adp_General_h
#define Adp_General_h

/**************************************************************************
 * Defines and Macros
 */
 
/* Defines for the unsigned types supported by "C" compilers. These may be
 * used as an alternative to spelling out "unsigned ..." (for example, in type
 * casting arguments to printf()).
 */
#ifndef UCHAR
  #define UCHAR   unsigned char
  #define UINT    unsigned int
  #define USHORT  unsigned short
  #define ULONG   unsigned long
#endif
 
/* Fixed size data types. These can be used where data fields must be a
 * predetermined size that is guaranteed to be the same regardless of the
 * platform on which the software is being compiled and run. For example,
 * these data types might be used in data structures that are shared by
 * software running on different platforms, such as software running under
 * Windows on a PC, and software running on a peripheral device.
 *
 * Here, the data types are defined to get the correct size when the software
 * is compiled using IAR's compiler for Atmel XMEGA microcontrollers.
 */
#ifndef UD8
    #define UD8   unsigned char   /* unsigned 8 bit data */
    #define SD8   signed char     /* signed 8 bit data */
    #define UD16  unsigned short  /* unsigned 16 bit data */
    #define SD16  signed short    /* signed 16 bit data */
    #define UD32  unsigned long   /* unsigned 32 bit data */
    #define SD32  signed long     /* signed 32 bit data */
    #define FD32  float           /* 32 bit floating point data */
#endif

/* Defines for boolean values. This is an integer because compilers should
 * use the most efficient data type (number of bytes) for an integer (as
 * appropriate for the target CPU or micro-controller). If you need a
 * fixed-size boolean type (for communication structures, etc.), use BOOL8,
 * BOOL16, etc. (defined in "Adp/Build/Build.h").
 */
#ifndef BOOL
  #define BOOL  int
#endif
/**/
#ifndef TRUE
  #define TRUE   1
  #define FALSE  0
#endif

/* Type for return status. For functions that return RSTAT, a return value of
 * 0 always indicates the function was successful, and a non-zero return value
 * always indicates the function encountered an error. That way, callers can
 * just check for a non-zero return value to determine if the function failed:
 * 
 *     if (func()) {
 *        <function failed>
 *     }
 * 
 * This is more efficient and requires fewer characters because it doesn't
 * require "< 0" or "!= 0" to check if the return value indicates error.
 * Typically, functions will return -1 for an error, which is why RSTAT is
 * signed, but any non-zero return value could also be used indicate an error.
 * If desired, a function could return different non-zero values to indicate
 * different error conditions.
 * 
 * Note RSTAT is defined to be an integer because compilers should use the most
 * efficient data type (number of bytes) for an integer (as appropriate for
 * the target CPU or micro-controller).
 */
#ifndef RSTAT
  #define RSTAT  int
#endif

/* Macros to read and write bits and bit fields. The "dest" argument is
 * the destination for write operations, and "src" is the source for read
 * operations. The macros are as follows:
 * 
 * WR_BIT():
 *     If the "val" argument is 1 or any other non-zero value, this will set
 *     the bit selected by "mask". If "val" is 0, it will clear the bit.
 * WR_BITS():
 *     Each bit selected by "mask" will be set to the value specified for
 *     each bit in "val" (i.e. if a bit is 1 in "val", it will be set to 1,
 *     and if a bit is 0 in "val", it will be cleared).
 * INVERT_BIT:
 *     The bit selected by "mask" will be inverted (i.e. if the bit is currently
 *     0, it will be set to 1, and if it's currently 1, it will be cleared).
 * INVERT_BITS:
 *     Each bit selected by "mask" will be inverted (i.e. if a bit is currently
 *     0, it will be set to 1, and if it's currently 1, it will be cleared).
 *     Note this actually does the same thing as INVERT_BIT(), but is named
 *     INVERT_BITS() to make it obvious multiple bits are being inverted.
 * RD_BIT:
 *     If the bit selected by "mask" is 1, this will return a non-zero value,
 *     and if the bit is 0, this will return 0.
 * RD_BITS:
 *     If any bit selected by "mask" is 1, this will return a non-zero value,
 *     and if all bits selected by "mask" are 0, this will return 0. Note this
 *     actually does the same thing as RD_BIT, bit is named RD_BITS() to make
 *     it obvious multiple bits are being read (not just a single bit).
 */
#ifndef WR_BIT
  #define WR_BIT(dest, mask, val) \
      (dest) = (val) ? ((dest) | (mask)) : ((dest) & ~(mask))
  #define WR_BITS(dest, mask, val) \
      (dest) = ((dest) & ~(mask)) | (val)
  #define INVERT_BIT(dest, mask) \
      (dest) ^= (mask)
  #define INVERT_BITS(dest, mask) \
      (dest) ^= (mask)
  #define RD_BIT(src, mask) \
      ((src) & (mask))
  #define RD_BITS(src, mask) \
      ((src) & (mask))
#endif
    
/**************************************************************************
 * Data Structures and Typedefs
 */

/* Structures for signed and unsigned 16-bit values stored in little-endian byte
 * order, and macros to set and get the values contained in the structures (i.e.
 * convert them from the native byte order to little-endian, and vice-versa).
 * These can be used in structures that are shared between multiple platforms
 * (e.g. PC and embedded firmware) to ensure that all platforms see the same
 * value, even if the platforms' native byte orders are different.
 */
typedef struct
{
    UD8 b0;
    UD8 b1;
} UD16_BYTES;
/**/
typedef struct
{
    UD8 b0;
    UD8 b1;
} SD16_BYTES;
/**/
#define SET_UD16(p, ud16Val) \
{ \
    (p)->b0 = (UD8)(ud16Val); \
    (p)->b1 = (UD8)(ud16Val >> 8); \
}
#define SET_SD16(p, sd16) \
{ \
    (p)->b0 = (UD8)(sd16); \
    (p)->b1 = (UD8)(sd16 >> 8); \
}
#define GET_UD16(p) ((UD16)(((UD16)((p)->b1) << 8) | ((p)->b0)))
#define GET_SD16(p) ((SD16)(((UD16)((p)->b1) << 8) | ((p)->b0)))

/* Structures for signed and unsigned 32-bit values stored in little-endian byte
 * order, and macros to set and get the values contained in the structures (i.e.
 * convert them from the native byte order to little-endian, and vice-versa).
 * These can be used in structures that are shared between multiple platforms
 * (e.g. PC and embedded firmware) to ensure that all platforms see the same
 * value, even if the platforms' native byte orders are different.
 */
typedef struct
{
    UD8 b0;
    UD8 b1;
    UD8 b2;
    UD8 b3;
} UD32_BYTES;
/**/
typedef struct
{
    UD8 b0;
    UD8 b1;
    UD8 b2;
    UD8 b3;
} SD32_BYTES;
/**/
#define SET_UD32(p, ud32) \
{ \
    (p)->b0 = (UD8)(ud32); \
    (p)->b1 = (UD8)(ud32 >> 8); \
    (p)->b2 = (UD8)(ud32 >> 16); \
    (p)->b3 = (UD8)(ud32 >> 24); \
}
#define SET_SD32(p, sd32) \
{ \
    (p)->b0 = (UD8)(sd32); \
    (p)->b1 = (UD8)(sd32 >> 8); \
    (p)->b2 = (UD8)(sd32 >> 16); \
    (p)->b3 = (UD8)(sd32 >> 24); \
}
#define GET_UD32(p) ((UD32)(((UD32)((p)->b3) << 24) | \
    ((UD32)((p)->b2) << 16) | ((UD16)((p)->b1) << 8) | ((p)->b0)))
#define GET_SD32(p) ((SD32)(((UD32)((p)->b3) << 24) | \
    ((UD32)((p)->b2) << 16) | ((UD16)((p)->b1) << 8) | ((p)->b0)))
 
/**************************************************************************
 * Global Declarations
 */

/**************************************************************************
 * External Function Prototypes
 */

#endif /* ensure this file is only included once */
