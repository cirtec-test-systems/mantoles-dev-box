/**************************************************************************
 * This file contains defines and macros for 32-bit Windows platforms. This
 * should be included by build includes for 32-bit Windows platforms. For a
 * description of build includes and how they're used, see "Adp/Build/Build.h".
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adp_Build_BuildWin32_h
#define Adp_Build_BuildWin32_h

/**************************************************************************
 * Defines and Macros
 */

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* Fixed size data types. These can be used where data fields must be a
 * predetermined size that is guaranteed to be the same regardless of the
 * platform on which the software is being compiled and run. For example,
 * these data types might be used in data structures that are shared by
 * software running on different platforms, such as software running under
 * Windows on a PC, and software running in a device controlled by the PC.
 *
 * Here, the data types are defined to get the correct size when the software
 * is compiled for 32-bit Windows on a PC.
 */
#ifndef UD8
  #define UD8   unsigned char   /* unsigned 8 bit data */
  #define SD8   signed char     /* signed 8 bit data */
  #define UD16  unsigned short  /* unsigned 16 bit data */
  #define SD16  signed short    /* signed 16 bit data */
  #define UD32  unsigned int    /* unsigned 32 bit data */
  #define SD32  signed int      /* signed 32 bit data */
  #define FD32  float           /* 32 bit floating point data */
#endif
 
/**************************************************************************
 * Global Declarations
 */

/**************************************************************************
 * External Function Prototypes
 */
 
#endif /* ensure this file is only included once */
