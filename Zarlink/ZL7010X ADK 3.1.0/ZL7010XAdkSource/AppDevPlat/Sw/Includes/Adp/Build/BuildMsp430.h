/**************************************************************************
 * This file contains defines and macros for MSP430 micro-controllers. This
 * should be included by build includes for MSP430 platforms. For a description
 * of build includes and how they're used, see "Adp/Build/Build.h".
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adp_Build_BuildMsp430_h
#define Adp_Build_BuildMsp430_h

/**************************************************************************
 * Defines and Macros
 */
 
/* Fixed size data types. These can be used where data fields must be a
 * predetermined size that is guaranteed to be the same regardless of the
 * platform on which the software is being compiled and run. For example,
 * these data types might be used in data structures that are shared by
 * software running on different platforms, such as software running under
 * Windows on a PC, and software running in a device controlled by the PC.
 *
 * Here, the data types are defined to get the correct size when the software
 * is compiled using TI's MSP430 compiler.
 */
#ifndef UD8
  #define UD8   unsigned char   /* unsigned 8 bit data */
  #define SD8   signed char     /* signed 8 bit data */
  #define UD16  unsigned short  /* unsigned 16 bit data */
  #define SD16  signed short    /* signed 16 bit data */
  #define UD32  unsigned long   /* unsigned 32 bit data */
  #define SD32  signed long     /* signed 32 bit data */
  #define FD32  float           /* 32 bit floating point data */
#endif

/* Macros to swap the byte order of data. For MSP430, the _swap_bytes()
 * intrinsic is used to take advantage of the SWPB instruction.
 */
#define SWAP16(d16)  (_swap_bytes((unsigned short)(d16)))
#define SWAP32(d32)  ((((unsigned long)SWAP16(d32)) << 16) | \
    ((unsigned long)SWAP16((d32) >> 16)))
    
/* Defines for interrupt edge selection. If a bit in P1IES or P2IES is 0, the
 * interrupt flag for the corresponding I/O port bit will be latched on the
 * rising edge of the input signal (low-to-high transition). Otherwise, it will
 * be latched on the falling edge of the input signal (high-to-low transition).
 */
#define INT_ON_RISING_EDGE   0
#define INT_ON_FALLING_EDGE  1
    
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global Declarations
 */

/**************************************************************************
 * External Function Prototypes
 */
 
#endif /* ensure this file is only included once */
