/**************************************************************************
 * This is the default build include for the ADP board, which contains various
 * platform-dependent defines and macros as required for the ADP board. For a
 * description of build includes and how they're used, see "Adp/Build/Build.h".
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adp_Build_BuildAdpBoard_h
#define Adp_Build_BuildAdpBoard_h

/* If debug, include "Standard/StdIo.h" so printf() can easily be invoked in
 * any file that includes this file (via "Adp/Build/Build.h").
 */
#ifdef DEBUG
#include "Standard/StdIo.h"
#endif

/* include file for micro-controller on ADP board (provided with compiler) */
#include "msp430f1611.h"  /* P1IN, P1OUT, BIT0, ... */

/* include the default build file for MSP430 platforms */
#include "Adp/Build/BuildMsp430.h" /* UD32, P1IN, BIT0, ... */

/**************************************************************************
 * Defines and Macros
 */
 
/* Define clock frequencies for the ADP board. Note these must match the actual
 * clock frequencies on the ADP board, which depend on the hardware (i.e. the
 * crystal oscillator, etc.) and the way the firmware configures the
 * micro-controller.
 */
#define MCLK_FREQ   (UD32)4000000UL
#define ACLK_FREQ   (UD32)8000000UL
#define SMCLK_FREQ  (UD32)800000UL  /* approximate (DCOCLK with defaults) */

/* I/O port 1:
 * 
 * ADP_IO_0_BIT, ... (schematic MSP_IO<0>, ...):
 *     General purpose I/O bits connected to the mezzanine board attached to
 *     the ADP board. When these aren't driven, they're pulled up by the level
 *     shifters on the ADP board.
 * ASPI_SLAVE_READY_BIT (schematic MSP_SPI_RDY_B):
 *     ADP SPI slave ready. When this isn't driven, it's pulled up by the level
 *     shifters on the ADP board. It must be initialized as an input, but the
 *     corresponding bit in P1OUT must also be initialized to 0 so it will be
 *     driven low when it's switched to an output. For more information, see
 *     ASPI_SLAVE_READY in "Adp/AnyBoard/AdpSpiHw.h".
 * P1DIR_INIT, ...:
 *     Initial I/O settings.
 * ..._P():
 *     Macros to form names of I/O port registers for each bit.
 */
#define ADP_IO_0_BIT          BIT0  /* MSP_IO<0> */
#define ADP_IO_1_BIT          BIT1  /* MSP_IO<1> */
#define ADP_IO_2_BIT          BIT2  /* MSP_IO<2> */
#define ADP_IO_3_BIT          BIT3  /* MSP_IO<3> */
#define ADP_IO_4_BIT          BIT4  /* MSP_IO<4> */
#define ADP_IO_5_BIT          BIT5  /* MSP_IO<5> */
#define ADP_IO_6_BIT          BIT6  /* MSP_IO<6> */
#define ASPI_SLAVE_READY_BIT  BIT7  /* MSP_SPI_RDY_B */
/**/
#define P1DIR_INIT  (0)
#define P1OUT_INIT  (0)
/**/
#define ADP_IO_P(suffix)            P1##suffix
#define ASPI_SLAVE_READY_P(suffix)  P1##suffix

/* I/O port 2:
 * 
 * USB_POWER_OFF_BIT, ... (schematic USB_PWREN_B, ...):
 *     Status and control signals to/from USB chip (FT245RQ).
 * RESET_BIT (schematic ADP_SW_RST_B):
 *     If this output is driven low, it will reset the ADP board and the
 *     mezzanine board attached to it.
 * RESET_STAT_STROBE_BIT & RESET_STAT_BIT (schematic RST_STAT_STB & RST_STAT): 
 *     A rising edge on the RESET_STAT_STROBE_BIT output will latch the
 *     RESET_STAT_BIT input high, which will remain high as long as the ADP
 *     board is powered (i.e. it will remain high even if the reset button is
 *     pressed, or the software initiates a reset). The software uses this to
 *     differentiate between a warm reset and a cold power-on reset.
 * P2DIR_INIT, ...:
 *     Initial I/O settings.
 * ..._P():
 *     Macros to form names of I/O port registers for each bit.
 */
#define USB_POWER_OFF_BIT      BIT0  /* USB_PWREN_B */
#define USB_TX_FULL_BIT        BIT1  /* USB_TXE_B */
#define USB_RX_EMPTY_BIT       BIT2  /* USB_RXF_B */
#define USB_TX_STROBE_BIT      BIT3  /* USB_WR */
#define USB_RX_STROBE_BIT      BIT4  /* USB_RD_B */
#define RESET_BIT              BIT5  /* ADP_SW_RST_B */
#define RESET_STAT_STROBE_BIT  BIT6  /* RST_STAT_STB */
#define RESET_STAT_BIT         BIT7  /* RST_STAT */
/**/
#define P2DIR_INIT  (BIT3 | BIT4 | BIT5 | BIT6)
#define P2OUT_INIT  (BIT3 | BIT4 | BIT5)
/**/
#define USB_POWER_OFF_P(suffix)      P2##suffix
#define USB_TX_FULL_P(suffix)        P2##suffix
#define USB_RX_EMPTY_P(suffix)       P2##suffix
#define USB_TX_STROBE_P(suffix)      P2##suffix
#define USB_RX_STROBE_P(suffix)      P2##suffix
#define RESET_P(suffix)              P2##suffix
#define RESET_STAT_STROBE_P(suffix)  P2##suffix
#define RESET_STAT_P(suffix)         P2##suffix

/* I/O port 3:
 * 
 * MEZZ_RESET_BIT (schematic MZ_SW_RST_B):
 *     If this output is driven low, it will reset the mezzanine board
 *     (the board attached to the ADP board). Note it should be driven low
 *     for about 1 ms to ensure it initiates a reset.
 * I2C_SDA_BIT, I2C_SCL_BIT (schematic MSP_I2C_SDA & MSP_I2C_SCL):
 *     I2C interface used for status and control of the batter charger chip
 *     (LP3947). Note the USART function is selected for these pins, so the
 *     USART controls their direction automatically.
 * UNUSED_P3_BIT2:
 *     Unused bit (init as output to prevent floating & reduce power).
 * BAT_CHARGE_OFF_BIT (schematic CHG_EN_B; inverted LP3947 EN input):
 *     Set to turn off the battery charger (disable), clear to turn it on.
 * BAT_CHARGE_HIGH_BIT (CHG_ISEL_B on schematic, inverted LP3947 ISEL input):
 *     Set to put the battery charger in high power mode, so the current drawn
 *     from USB is configured via I2C. Otherwise, the maximum current drawn
 *     from USB is 100 mA.
 * VSUP1_OFF_BIT, VSUP2_OFF_BIT (schematic VSUP2_EN_B & VSUP1_EN_B):
 *     Set to turn off VSUP1 and VSUP2 (disable), clear to turn them on.
 *     These are the power supplies to the mezzanine board (the board attached
 *     to the ADP board).
 * P3DIR_INIT, ...:
 *     Initial I/O settings.
 * ..._P():
 *     Macros to form names of I/O port registers for each bit.
 */
#define MEZZ_RESET_BIT       BIT0  /* MZ_SW_RST_B */
#define I2C_SDA_BIT          BIT1  /* MSP_I2C_SDA */
#define UNUSED_P3_BIT2       BIT2  /* unused */
#define I2C_SCL_BIT          BIT3  /* MSP_I2C_SCL */
#define BAT_CHARGE_OFF_BIT   BIT4  /* CHG_EN_B */
#define VSUP2_OFF_BIT        BIT5  /* VSUP2_EN_B */
#define VSUP1_OFF_BIT        BIT6  /* VSUP1_EN_B */
#define BAT_CHARGE_HIGH_BIT  BIT7  /* CHG_ISEL_B */
/**/
#define P3DIR_INIT  (BIT0 | BIT2 | BIT4 | BIT5 | BIT6 | BIT7)
#define P3OUT_INIT  (BIT0 | BIT5 | BIT6)
/**/
#define MEZZ_RESET_P(suffix)       P3##suffix
#define I2C_SDA_P(suffix)          P3##suffix
#define I2C_SCL_P(suffix)          P3##suffix
#define BAT_CHARGE_OFF_P(suffix)   P3##suffix
#define BAT_CHARGE_HIGH_P(suffix)  P3##suffix
#define VSUP1_OFF_P(suffix)        P3##suffix
#define VSUP2_OFF_P(suffix)        P3##suffix

/* I/O port 4:
 * 
 * USB_DATA_0, ... (schematic USB_D<0>, ...):
 *     Pins used to transfer data bytes to/from USB chip (FT245RQ).
 * P4DIR_INIT, ...:
 *     Initial I/O settings.
 * ..._P():
 *     Macros to form names of I/O port registers for each bit.
 */
#define USB_DATA_0  BIT0  /* USB_D<0> */
#define USB_DATA_1  BIT1  /* USB_D<1> */
#define USB_DATA_2  BIT2  /* USB_D<2> */
#define USB_DATA_3  BIT3  /* USB_D<3> */
#define USB_DATA_4  BIT4  /* USB_D<4> */
#define USB_DATA_5  BIT5  /* USB_D<5> */
#define USB_DATA_6  BIT6  /* USB_D<6> */
#define USB_DATA_7  BIT7  /* USB_D<7> */
/**/
#define P4DIR_INIT  (0)
#define P4OUT_INIT  (0)
/**/
#define USB_DATA_P(suffix)  P4##suffix

/* I/O port 5:
 * 
 * ASPI_STE_BIT, ... (schematic MSP_SPI_STE1, ...):
 *     ADP SPI interface signals. Note as of this writing, the ADP SPI
 *     interface only uses ASPI_SIMO_BIT and ASPI_CLK_BIT, so the USART function
 *     is only selected for those pins, and the MSP430 controls their direction
 *     automatically. The remaining pins are initialized as inputs and are
 *     pulled up by the level shifters on the ADP board. For more information,
 *     see "AppDevPlat/Sw/AnyBoard/Libs/AdpSpiLib.c".
 * RANGE1_BIT, RANGE2_BIT (schematic RANGE1 & RANGE2):
 *     Selects the range for monitoring the current drawn from VSUP1 and VSUP2
 *     by the mezzanine board (the board attached to the ADP board). Set to 1
 *     to select the high current range, 0 to select the low current range.
 *     The current is monitored using the ISUP1_ADC and ISUP2_ADC inputs.
 * CAL1_BIT, CAL2_BIT (schematic CAL1 & CAL2):
 *     Set during VSUP1 and VSUP2 calibration. These are initially set high
 *     to make sure the associated capacitors are drained, then cleared after
 *     the DAC's are configured for VSUP1 and VSUP2.
 * P5DIR_INIT, ...:
 *     Initial I/O settings.
 * ..._P():
 *     Macros to form names of I/O port registers for each bit.
 */
#define ASPI_STE_BIT    BIT0  /* MSP_SPI_STE1 */
#define ASPI_SIMO_BIT   BIT1  /* MSP_SPI_SIMO1 */
#define ASPI_SOMI_BIT   BIT2  /* MSP_SPI_SOMI1 */
#define ASPI_CLK_BIT    BIT3  /* MSP_SPI_CLK */
#define RANGE1_BIT      BIT4  /* RANGE1 */
#define CAL1_BIT        BIT5  /* CAL1 */
#define CAL2_BIT        BIT6  /* CAL2 */
#define RANGE2_BIT      BIT7  /* RANGE2 */
/**/
#define P5DIR_INIT  (BIT4 | BIT5 | BIT6 | BIT7)
#define P5OUT_INIT  (BIT4 | BIT5 | BIT6 | BIT7)
/**/
#define ASPI_SIMO_P(suffix)  P5##suffix
#define ASPI_CLK_P(suffix)   P5##suffix
#define RANGE1_P(suffix)     P5##suffix
#define RANGE2_P(suffix)     P5##suffix
#define CAL1_P(suffix)       P5##suffix
#define CAL2_P(suffix)       P5##suffix

/* I/O port 6:
 * 
 * VBAT_SENSE_ADC_BIT, ... (schematic VBAT_SENSE, ...):
 *     ADC input pins.
 * VSUP1_VSET_BIT, VSUP2_VSET_BIT (schematic VSUP1_VSET & VSUP2_VSET):
 *     DAC outputs to control the level of VSUP1 and VSUP2. Note when the
 *     DAC's are configured, they automatically select the DAC function for
 *     these pins, regardless of settings for these bits in P6DIR and P6SEL.
 * P6DIR_INIT, ...:
 *     Initial I/O settings.
 * ..._P():
 *     Macros to form names of I/O port registers for each bit.
 * ..._ADC_INPUT:
 *     ADC input channels.
 */
#define VBAT_SENSE_ADC_BIT           BIT0  /* VBAT_SENSE (ADC input) */
#define ISUP1_ADC_BIT                BIT1  /* ISUP1_ADC (ADC input) */
#define ISUP2_ADC_BIT                BIT2  /* ISUP1_ADC (ADC input) */
#define BAT_CHARGE_DIFF_AMP_ADC_BIT  BIT3  /* CHG_DIFF_AMP (ADC input) */
#define VSUP1_ADC_BIT                BIT4  /* VSUP1_ADC (ADC input) */
#define VSUP2_ADC_BIT                BIT5  /* VSUP2_ADC (ADC input) */
#define VSUP1_VSET_BIT               BIT6  /* VSUP1_VSET (DAC output) */
#define VSUP2_VSET_BIT               BIT7  /* VSUP2_VSET (DAC output) */
/**/
#define P6DIR_INIT  (0)
#define P6OUT_INIT  (0)
/**/
#define ADC_INPUT_P(suffix)  P6##suffix
/**/
#define VBAT_SENSE_ADC_INPUT           INCH_0
#define ISUP1_ADC_INPUT                INCH_1
#define ISUP2_ADC_INPUT                INCH_2
#define BAT_CHARGE_DIFF_AMP_ADC_INPUT  INCH_3
#define VSUP1_ADC_INPUT                INCH_4
#define VSUP2_ADC_INPUT                INCH_5

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global Declarations
 */

/**************************************************************************
 * External Function Prototypes
 */
 
#endif /* ensure this file is only included once */
