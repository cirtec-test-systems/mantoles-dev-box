/**************************************************************************
 * This is the default build include for Windows DLL's. It defines various
 * platform-dependent defines and macros required to build Windows DLL's.
 * For a description of build includes and how they're used, see
 * "Adp/Build/Build.h".
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adp_Build_BuildWinDll_h
#define Adp_Build_BuildWinDll_h

/* Tell the compiler to export functions declared with EXTC (see EXTC in
 * "Adp/Build/BuildWin.h").
 */
#define DLL_EXPORTS

/* include the default build file for Windows platforms */
#include "Adp/Build/BuildWin.h"
 
#endif /* ensure this file is only included once */
