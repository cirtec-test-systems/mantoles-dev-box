/**************************************************************************
 * This file can be included for defines and macros that depend on what
 * is being built and the platform it's being built for (UD8, UD16, I/O
 * ports for micro-controllers, etc.). This file doesn't contain all of the
 * defines and macros itself, but rather, it includes the file defined by
 * BUILD_INCLUDE, which must be passed to the compiler when it's invoked. For
 * example, you might specify "-DBUILD_INCLUDE=<Adp/Build/BuildWinDll.h>" on
 * the compiler command line to build a DLL for Windows, or "-DBUILD_INCLUDE=
 * <Adk/Build/BuildAdp.h>" to build firmware for the ADP board. The project
 * can specify a different build include for each of its build targets, and/or
 * the build includes can check pre-defines for different compilers, etc.
 * Source files should include this file (Adp/Build/Build.h) instead of
 * including a specific build include directly, so they can be compiled for
 * different targets without having to change the build include referenced in
 * every file.
 *
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
/* ensure this file is only included once */
#ifndef Adp_Build_Build_h
#define Adp_Build_Build_h
 
/* Include the file defined by BUILD_INCLUDE, which must be passed to the
 * compiler when it is invoked. The BUILD_INCLUDE file contains defines and
 * macros that depend on what is being built and the platform it's being built
 * for (UD8, UD16, I/O port definitions for micro-controllers, etc.). For more
 * information, see the prolog at the top of this file.
 */
#include BUILD_INCLUDE  /* UD8, UD16, I/O ports for micro-controllers, ... */

/**************************************************************************
 * Defines and Macros
 */

/* Fixed-size boolean types (for TRUE or FALSE). These can be used where a
 * fixed-size boolean value is needed, such as in communication structures. If
 * you don't need a fixed size, use BOOL instead (defined in "Adp/General.h").
 */
#ifndef BOOL8
  #define BOOL8   UD8
  #define BOOL16  UD16
  #define BOOL32  UD32
#endif

/* Default macros to swap the byte order of data.
 */
#ifndef SWAP16
  #define SWAP16(d16) (((UD16)(d16) << 8) | ((UD16)(d16) >> 8))
  #define SWAP32(d32) (((UD32)(d32) >> 24) | \
      (((UD32)(d32) >> 8) & 0x0000FF00) | \
      ((UD32)(d32) << 24) | (((UD32)(d32) << 8) & 0x00FF0000))
#endif

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global Declarations
 */

/**************************************************************************
 * External Function Prototypes
 */

#endif /* ensure this file is only included once */
