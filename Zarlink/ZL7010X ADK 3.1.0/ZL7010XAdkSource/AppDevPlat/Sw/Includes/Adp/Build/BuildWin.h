/**************************************************************************
 * This is the default build include for Windows platforms. It defines various
 * platform-dependent defines and macros as required for Windows platforms.
 * For a description of build includes and how they're used, see
 * "Adp/Build/Build.h".
 * 
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adp_Build_BuildWin_h
#define Adp_Build_BuildWin_h

/* if debug, include StdIo.h so printf() can easily be invoked anywhere */
#ifdef DEBUG
#include "Standard/StdIo.h"
#endif

/* include build file for Windows platform (32-bit or 64-bit) */
#if defined _WIN32
#    include "Adp/Build/BuildWin32.h"
#elif defined _WIN64
#    include "Adp/Build/BuildWin64.h"
#endif

/**************************************************************************
 * Defines and Macros
 */

/* EXTC should be specified at the beginning of every function declared in a
 * library include file. If DLL_EXPORTS is defined, EXTC will tell the compiler
 * to export the function. If the compiler is C++, EXTC will tell the compiler
 * to preserve the C function name. Note you only need to specify EXTC in the
 * library include files, not where the functions are implemented (it wouldn't
 * hurt anything to do so, but it's not necessary).
 */
#ifdef DLL_EXPORTS
#    define IMPORT_EXPORT  __declspec(dllexport)
#else
#    define IMPORT_EXPORT  __declspec(dllimport)
#endif
#ifdef __cplusplus 
#    define EXTC  extern "C" IMPORT_EXPORT
#else
#    define EXTC  extern IMPORT_EXPORT
#endif

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global Declarations
 */

/**************************************************************************
 * External Function Prototypes
 */
 
#endif /* ensure this file is only included once */
