/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
/* ensure this file is only included once */
#ifndef Adp_AppDevPlatVer_h
#define Adp_AppDevPlatVer_h

/* Include the build include so a project can override the standard
 * APP_DEV_PLAT_VER to create a custom, project-specific version of the
 * Application Development Platform, if desired. To do so, the project should
 * define APP_DEV_PLAT_VER in its build include files (for a description of
 * build includes and how they're used, see "Adp/Build/Build.h").
 */
#include "Adp/Build/Build.h"

/**************************************************************************
 * Defines and Macros
 */
 
/* Standard Application Development Platform versions.
 */
#define APP_DEV_PLAT_VER_1_0_0  "Application Development Platform 1.0.0"
#define APP_DEV_PLAT_VER_1_0_1  "Application Development Platform 1.0.1"
#define APP_DEV_PLAT_VER_1_1_0  "Application Development Platform 1.1.0"
#define APP_DEV_PLAT_VER_1_1_1  "Application Development Platform 1.1.1"
#define APP_DEV_PLAT_VER_1_1_2  "Application Development Platform 1.1.2"
#define APP_DEV_PLAT_VER_1_1_3  "Application Development Platform 1.1.3"
#define APP_DEV_PLAT_VER_2_0_0  "AppDevPlat 2.0.0"
#define APP_DEV_PLAT_VER_2_0_1  "AppDevPlat 2.0.1"
#define APP_DEV_PLAT_VER_2_1_0  "AppDevPlat 2.1.0"
#define APP_DEV_PLAT_VER_2_1_1  "AppDevPlat 2.1.1"
#define APP_DEV_PLAT_VER_2_1_2  "AppDevPlat 2.1.2"
#define APP_DEV_PLAT_VER_2_1_3  "AppDevPlat 2.1.3"
#define APP_DEV_PLAT_VER_2_1_4  "AppDevPlat 2.1.4"
#define APP_DEV_PLAT_VER_2_1_5  "AppDevPlat 2.1.5"
#define APP_DEV_PLAT_VER_2_1_6  "AppDevPlat 2.1.6"
#define APP_DEV_PLAT_VER_2_1_7  "AppDevPlat 2.1.7"
/* 
 * Current Application Development Platform version. The ADP board and the
 * API for the Application Development Platform return this in their version
 * string (see AdpGetBoardVer() and AdpGetApiVer()). Note AdpGetBoardVer()
 * also returns the board's hardware model in the version string.
 * 
 * By default, the Application Development Platform maintains its own standard
 * version independent of any project that uses the Platform. That way,
 * different versions of a project can all use the same standard version of
 * the Platform, including the ADP board, so the ADP board won't have to be
 * reprogrammed for each release of the project. If needed, a project can
 * override the standard APP_DEV_PLAT_VER to create a custom, project-specific
 * version of the Application Development Platform. To do so, the project
 * should define APP_DEV_PLAT_VER in its build include files (for a description
 * of build includes and how they're used, see "Adp/Build/Build.h").
 */
#ifndef APP_DEV_PLAT_VER
#define APP_DEV_PLAT_VER  APP_DEV_PLAT_VER_2_1_7
#endif

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global Declarations
 */
 
/**************************************************************************
 * External Function Prototypes
 */
 
#endif /* ensure this file is only included once */
