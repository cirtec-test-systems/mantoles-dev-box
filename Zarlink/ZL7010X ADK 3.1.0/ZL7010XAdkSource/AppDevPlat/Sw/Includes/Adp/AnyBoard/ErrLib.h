/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adp_AnyBoard_ErrLib_h
#define Adp_AnyBoard_ErrLib_h

#include "Standard/String.h" /* NULL (ErrSet() args) */
#include "Standard/StdArg.h" /* va_list (ErrSet() args) */
#include "Adp/General.h"     /* RSTAT, BOOL, ... */
#include "Adp/Build/Build.h" /* UD16, ... */

/**************************************************************************
 * Defines and Macros
 */
 
/* Size of buffer required to hold maximum length error arguments string
 * and terminating '\0' (see ERR_INFO.errArgs and ErrSet() for more
 * information). 24 was chosen because it's not too big, but can still hold
 * multiple arguments of some length, as follows:
 * 
 *   1 argument up to 23 characters long (23 + 1 terminator)
 *   2 arguments up to 11 characters each (2*11 + 1 separator + 1 terminator)
 *   3 arguments up to 7 characters each (3*7 + 2 separators + 1 terminator)
 *   4 arguments up to 5 characters each (4*5 + 3 separators + 1 terminator)
 *   and so on ...
 * 
 * Note if one argument is shorter, the others can be longer.
 * 
 * To build the arguments string, ErrSet() passes its argFormats parameter
 * and remaining arguments to vsnprintf(). The length of the resulting string
 * and terminating '\0' should not exceed ERR_ARGS_BUF_SIZE. If it does,
 * it won't corrupt memory or cause catastrophic problems, but the resulting
 * arguments string will be truncated.
 * 
 * The separator between arguments is '\f' (form feed character). This was
 * chosen because there's no reason '\f' would ever appear in an argument, and
 * because vsnprintf() passes '\f' through as is.
 * 
 * This buffer size can be reduced if the memory for it just can't be afforded,
 * but this would reduce the number of arguments it can handle and how long
 * they can be.
 */
#define ERR_ARGS_BUF_SIZE  24
#define ERR_ARGS_MAX_LEN   (ERR_ARGS_BUF_SIZE - 1)
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* structure for error information */
typedef struct {
    
    /* The error group passed to ErrSet(). This must point to a constant global
     * string. Each error group must have one constant global string associated
     * with it that all of the micro-controller software references to identify
     * that group.
     */
    const char *errGroup;
    
    /* The error code passed to ErrSet(). Note each error group defines
     * its own set of error codes starting at 1. Thus, the error code alone
     * is not sufficient to uniquely identify an error. Rather, both the error
     * group and error code are required.
     */
    UD16 errCode;
    
    /* String containing arguments to insert into the error message for the
     * error. If there are multiple arguments, they are separated by '\f'
     * (form feed character). When this string is parsed, the parser will
     * search for '\f' to find each argument. '\f' was chosen because there's
     * no reason '\f' would ever appear in an argument itself, and because
     * vsnprintf() passes '\f' through as is. For more information, see
     * ERR_ARGS_BUF_SIZE and ErrSet().
     */
    char errArgs[ERR_ARGS_BUF_SIZE];
    
} ERR_INFO;
 
/**************************************************************************
 * Global Declarations
 */
 
/**************************************************************************
 * External Function Prototypes
 */
 
extern RSTAT ErrInit(void);

extern void ErrSet(const char *errGroup, UD16 errCode,
    const char *argFormats, ...);
    
extern void ErrSetVa(const char *errGroup, UD16 errCode,
    const char *argFormats, va_list va);
    
extern const ERR_INFO *ErrGet(void);
extern const ERR_INFO *ErrGetFirst(void);

extern BOOL ErrIs(const char *errGroup, UD16 errCode);
extern BOOL ErrIsFirst(const char *errGroup, UD16 errCode);

extern void ErrClearFirst(void);

extern void ErrLock(BOOL lock);

#endif /* ensure this file is only included once */
