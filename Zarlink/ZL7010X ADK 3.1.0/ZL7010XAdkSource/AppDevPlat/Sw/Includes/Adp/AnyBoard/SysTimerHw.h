/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
/* ensure this file is only included once */
#ifndef Adp_AnyBoard_SysTimerHw_h
#define Adp_AnyBoard_SysTimerHw_h

#include "Adp/Build/Build.h" /* UD16, TAR, ACLK_FREQ, ST_MS_PER_INT, ... */

/**************************************************************************
 * Defines and Macros
 */
 
/* system timer registers */
#define ST_CONTROL      TACTL    /* system timer control */     
#define ST_COUNTER      TAR      /* system timer counter */
#define ST_CC0_CONTROL  TACCTL0  /* capture/compare control */
#define ST_CC0_COUNT    TACCR0   /* capture/compare count for interrupt */

/* macro to declare interrupt service routine for timer capture/compare */
#define ST_CC0_VECTOR  TIMERA0_VECTOR

/* clock source settings for timer control register (ST_CONTROL) */
#define ST_TACLK  TASSEL_0  /* TACLK (external clock) */
#define ST_ACLK   TASSEL_1  /* ACLK */
#define ST_SMCLK  TASSEL_2  /* SMCLK */
#define ST_INCLK  TASSEL_3  /* INCLK (external clock) */

/* clock divider settings for timer control register (ST_CONTROL) */
#define ST_DIV_1  ID_0  /* divide by 1 (input divider code 0) */
#define ST_DIV_2  ID_1  /* divide by 2 (input divider code 1) */
#define ST_DIV_4  ID_2  /* divide by 4 (input divider code 2) */
#define ST_DIV_8  ID_3  /* divide by 8 (input divider code 3) */

/* If ST_CLK_SRC isn't defined by the build include (see "Adp/Build/Build.h"),
 * use the default clock source, frequency, and divider, then calculate the
 * corresponding number of timer counts per second (ST_COUNT_PER_SEC).
 * 
 * Note if the resulting ST_COUNT_PER_SEC is less than 1000000, the timer won't
 * have microsecond resolution. For example, if ST_COUNT_PER_SEC is 32768, the
 * resolution will be ~30 microseconds (1000000 / 32768).
 * 
 * Also, if the resulting ST_COUNT_PER_SEC isn't a multiple of 1000, time
 * periods detected using the millisecond time (StPub.msTime) may be off from
 * the actual time. For details, see ST_MS_PER_INT later in this file.
 */
#ifndef ST_CLK_SRC 
  #define ST_CLK_SRC   ST_ACLK
  #define ST_CLK_FREQ  ACLK_FREQ
  #define ST_CLK_DIV   ST_DIV_8
#endif
/**/ 
#if ST_CLK_DIV == ST_DIV_1
  #define ST_COUNT_PER_SEC  (ST_CLK_FREQ / 1)
#elif ST_CLK_DIV == ST_DIV_2
  #define ST_COUNT_PER_SEC  (ST_CLK_FREQ / 2)
#elif ST_CLK_DIV == ST_DIV_4
  #define ST_COUNT_PER_SEC  (ST_CLK_FREQ / 4)
#elif ST_CLK_DIV == ST_DIV_8
  #define ST_COUNT_PER_SEC  (ST_CLK_FREQ / 8)
#endif

/* The number of milliseconds per system timer interrupt (ST_MS_PER_INT) and
 * the corresponding number of timer counts per interrupt (ST_COUNT_PER_INT).
 * This determines how often the capture/compare interrupt occurs (StCc0Isr())
 * to update the millisecond time (StPub.msTime), which determines the
 * resolution of the millisecond time.
 * 
 * If this isn't defined by the build include (see "Adp/Build/Build.h"), it's
 * set to 1 ms by default. To reduce the interrupt overhead, the build include
 * can define a larger ST_MS_PER_INT so the interrupt will occur less often,
 * but that will reduce the millisecond time resolution. For the number of
 * clock cycles it takes to service this interrupt each time it occurs,
 * see the comment for the system timer interrupt ISR (StCc0Isr()) in
 * "AppDevPlat/Sw/AnyBoard/SysTimerLib.c".
 * 
 * Note if ST_COUNT_PER_SEC (defined earlier in this file) isn't a multiple of
 * 1000, time periods detected using the millisecond time (StPub.msTime) may
 * be off from the actual time. For example, if ST_COUNT_PER_SEC = 32768,
 * and ST_MS_PER_INT = 1, then ST_COUNT_PER_INT = 32768*1/1000 = 32.768 =
 * integer 32, so the system timer interrupt will occur every ~0.97656 ms
 * (32 * 1/32768). Thus, the millisecond time will be off by ~0.02344 ms per
 * millisecond, and any period detected using the millisecond time will be
 * short by ~2.344%. Note if ST_MS_PER_INT = 2, this will improve to ~0.818%,
 * but the millisecond time will only have a resolution of ~2 ms.
 *
 * Note the resulting ST_COUNT_PER_INT must be less than 0xFFFF (16 bits), so
 * if ST_COUNT_PER_SEC is too big, it may require a smaller ST_MS_PER_INT.
 */
#ifndef ST_MS_PER_INT
  #define ST_MS_PER_INT  1
#endif
#define ST_COUNT_PER_INT  ((UD16)((ST_COUNT_PER_SEC * ST_MS_PER_INT) / 1000))
    
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global Declarations
 */

/**************************************************************************
 * External Function Prototypes
 */
 
#endif /* ensure this file is only included once */
