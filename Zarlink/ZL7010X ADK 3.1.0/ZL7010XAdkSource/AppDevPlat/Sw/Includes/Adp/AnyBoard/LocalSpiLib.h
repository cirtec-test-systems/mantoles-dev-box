/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adp_AnyBoard_LocalSpiLib_h
#define Adp_AnyBoard_LocalSpiLib_h

#include "Adp/General.h"             /* UINT, RSTAT, ... */
#include "Adp/Build/Build.h"         /* UD8, ... */   
#include "Adp/AnyBoard/LocalSpiHw.h" /* LSPI_TX_EMPTY, LSPI_TX_BUF, ... */

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global Declarations
 */
 
/**************************************************************************
 * External Function Prototypes
 */
 
extern RSTAT LSpiInit(UINT specs, UINT clockDiv);
extern void LSpiConfig(UINT specs, UINT clockDiv);

/**************************************************************************
 * Read a byte from a device on the local SPI bus. Note the device must
 * already be selected on the local SPI bus when this is called.
 */
__inline UINT
LSpiRead(UINT addr)
{
    /* Write address byte to SPI TX buf (with read flag), then wait for the
     * SPI interface to shift it out of the SPI TX buf.
     */
    LSPI_TX_BUF = (UD8)addr;
    while(!LSPI_TX_EMPTY);
    /*
     * Write dummy byte to SPI TX buf, then wait for SPI interface to finish
     * transmitting both the address and the dummy byte to the device. Note
     * when the SPI interface transmits the dummy byte, it will also receive
     * the data value from the device.
     */
    LSPI_TX_BUF = 0;
    while(!LSPI_TX_DONE);
    
    /* return value received from device */
    return(LSPI_RX_BUF);
}

__inline void
LSpiReadMulti(UINT startAddr, void *buf, UINT len)
{
    UD8 *buf8 = buf;
    
    /* Write starting address to SPI TX buf, then wait for SPI interface to
     * shift it out of the SPI TX buf. When the device receives this byte, it
     * will start the multi-byte read.
     */
    LSPI_TX_BUF = (UD8)startAddr;
    while(!LSPI_TX_EMPTY);
    
    /* Write first dummy byte to SPI TX buf. Note when the SPI interface
     * transmits this byte, it will also receive the first byte from the device.
     */
    LSPI_TX_BUF = 0;
    
    /* until all but the last byte has been read */
    while(--len != 0) {
        
        /* Wait until the dummy byte has been transmitted (i.e. received next
         * byte), then read the received byte, and start the next dummy byte.
         */
        while(!LSPI_TX_DONE);
        *buf8++ = LSPI_RX_BUF;
        LSPI_TX_BUF = 0;
    }
    /* Wait for the SPI interface to finish transmitting the last dummy byte,
     * then read the last byte received from the device.
     */
    while(!LSPI_TX_DONE);
    *buf8 = LSPI_RX_BUF;
}

__inline void
LSpiWrite(UINT addr, UINT val)
{
    /* Write address byte to SPI TX buf, then wait for the SPI interface to
     * shift it out of the SPI TX buf.
     */
    LSPI_TX_BUF = (UD8)addr;
    while(!LSPI_TX_EMPTY);
    /*
     * Write value to SPI TX buf, then wait for SPI interface to finish
     * transmitting both the address and the value to the device.
     */
    LSPI_TX_BUF = (UD8)val;
    while(!LSPI_TX_DONE);
}

__inline void
LSpiWriteMulti(UINT startAddr, const void *data, UINT len)
{
    const UD8 *data8 = data;
    
    /* Write starting address to SPI TX buf. When the device receives this
     * byte, it will start the multi-byte write.
     */
    LSPI_TX_BUF = (UD8)startAddr;
    
    while(len != 0) {
        
        /* wait for SPI interface to shift previous byte out of SPI TX buf */
        while(!LSPI_TX_EMPTY);
        
        /* write next data byte to SPI TX buf */
        LSPI_TX_BUF = *data8++;
        
        /* decrement length remaining */
        --len;
    }
    
    /* wait for the SPI interface to finish transmitting the last byte */
    while(!LSPI_TX_DONE);
}

#endif /* ensure this file is only included once */
