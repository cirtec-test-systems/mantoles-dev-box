/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
/* ensure this file is only included once */
#ifndef Adp_AnyBoard_AdpSpiHw_h
#define Adp_AnyBoard_AdpSpiHw_h

#include "Adp/General.h"     /* RD_BIT(), WR_BIT(), ... */
#include "Adp/Build/Build.h" /* __msp430x16x, UD32, U0TXBUF, ACLK_FREQ, ... */

/**************************************************************************
 * Defines and Macros
 */
 
/* Defines which vary depending on the micro-controller and serial interface.
 */
#ifdef __msp430x16x

  /* settings for ASPI_USART (serial interface for ADP SPI) & the default */
  #define ASPI_USART_0  0
  #define ASPI_USART_1  1
  /**/
  #ifndef ASPI_USART
    #define ASPI_USART  ASPI_USART_1
  #endif

  /* set defines for specified USART */
  #if ASPI_USART == ASPI_USART_0
  
    #define ASPI_CTL        U0CTL
    #define ASPI_TCTL       U0TCTL
    #define ASPI_BR0        U0BR0
    #define ASPI_BR1        U0BR1
    #define ASPI_MCTL       U0MCTL
    #define ASPI_RX_BUF     U0RXBUF
    #define ASPI_TX_BUF     U0TXBUF
    /* module enable register & bits */
    #define ASPI_ME         ME1
    #define ASPI_SPIE       USPIE0
    /* interrupt enable register & bits */
    #define ASPI_IE         U0IE   
    #define ASPI_RX_IE      URXIE0
    /* interrupt flag register & bits */
    #define ASPI_IFG        U0IFG
    #define ASPI_TX_IFG     UTXIFG0
    #define ASPI_RX_IFG     URXIFG0
    /*
     * Macro to declare ISR for RX interrupt.
     */
    #define ASPI_RX_VECTOR  USART0RX_VECTOR
    /*
     * Default I/O port pins & macros to form names of port registers.
     */
    #ifndef ASPI_SIMO_BIT
      #define ASPI_SIMO_BIT       BIT1
      #define ASPI_SIMO_P(suffix) P3##suffix
      #define ASPI_CLK_BIT        BIT3
      #define ASPI_CLK_P(suffix)  P3##suffix
    #endif
    
  #elif ASPI_USART == ASPI_USART_1
  
    #define ASPI_CTL        U1CTL
    #define ASPI_TCTL       U1TCTL
    #define ASPI_BR0        U1BR0
    #define ASPI_BR1        U1BR1
    #define ASPI_MCTL       U1MCTL
    #define ASPI_RX_BUF     U1RXBUF
    #define ASPI_TX_BUF     U1TXBUF
    /* module enable register & bits */
    #define ASPI_ME         ME2
    #define ASPI_SPIE       USPIE1
    /* interrupt enable register & bits */
    #define ASPI_IE         U1IE   
    #define ASPI_RX_IE      URXIE1
    /* interrupt flag register & bits */
    #define ASPI_IFG        U1IFG
    #define ASPI_TX_IFG     UTXIFG1
    #define ASPI_RX_IFG     URXIFG1
    /* 
     * Macro to declare ISR for RX interrupt.
     */
    #define ASPI_RX_VECTOR  USART1RX_VECTOR
    /*
     * Default I/O port pins & macros to form names of port registers.
     */
    #ifndef ASPI_SIMO_BIT
      #define ASPI_SIMO_BIT       BIT1
      #define ASPI_SIMO_P(suffix) P5##suffix
      #define ASPI_CLK_BIT        BIT3
      #define ASPI_CLK_P(suffix)  P5##suffix
    #endif
    
  #endif
  
  /* settings for clock source (ASPI_TCTL.SSELx bits) */
  #define ASPI_ACLK   SSEL0
  #define ASPI_SMCLK  SSEL1
  /* non-zero if ADP SPI interface is in master mode */
  #define ASPI_MASTER  RD_BIT(ASPI_CTL, MM)
  /* non-zero when TX buf is empty */
  #define ASPI_TX_EMPTY  RD_BIT(ASPI_IFG, ASPI_TX_IFG)
  /* non-zero when TX buf and shift register are empty */
  #define ASPI_TX_DONE  RD_BIT(ASPI_TCTL, TXEPT)
  /* non-zero when RX buf is full (received byte) */
  #define ASPI_RX_FULL  RD_BIT(ASPI_IFG, ASPI_RX_IFG)
  /* macro to change RX interrupt enable */
  #define ASPI_WR_RX_INT_ENAB(val)  WR_BIT(ASPI_IE, ASPI_RX_IE, val)
  
#endif /* #ifdef __msp430x16x */
/*
 * Defines for MSP430x26x micro-controllers.
 */
#ifdef __msp430x26x

  /* settings for ASPI_USCI (serial interface for ADP SPI) & the default */
  #define ASPI_USCI_A0  0
  #define ASPI_USCI_A1  1
  #define ASPI_USCI_B0  2
  #define ASPI_USCI_B1  3
  /**/
  #ifndef ASPI_USCI
    #define ASPI_USCI  ASPI_USCI_B1
  #endif

  /* set defines for specified USCI */
  #if ASPI_USCI == ASPI_USCI_A0
  
    #define ASPI_CTL0       UCA0CTL0
    #define ASPI_CTL1       UCA0CTL1
    #define ASPI_BR0        UCA0BR0
    #define ASPI_BR1        UCA0BR1
    #define ASPI_STAT       UCA0STAT
    #define ASPI_RX_BUF     UCA0RXBUF
    #define ASPI_TX_BUF     UCA0TXBUF
    /* interrupt enable register & bits */
    #define ASPI_IE         UC0IE
    #define ASPI_RX_IE      UCA0RXIE
    /* interrupt flag register & bits */
    #define ASPI_IFG        UC0IFG
    #define ASPI_RX_IFG     UCA0RXIFG
    #define ASPI_TX_IFG     UCA0TXIFG
    /* 
     * Macro to clear modulation control (SPI doesn't use modulation control,
     * but MSP430 manuals says it should be set to 0).
     */
    #define ASPI_CLEAR_MCTL()  {UCA0MCTL = 0;}
    /*
     * Macro to declare ISR for RX interrupt. Note USCI A0 and B0 share the
     * same RX interrupt vector, so if they both use the RX interrupt, the ISR
     * must determine which USCI asserted the interrupt.
     */
    #define ASPI_RX_VECTOR  USCIAB0RX_VECTOR
    /*
     * Default I/O port pins & macros to form names of port registers.
     */
    #ifndef ASPI_SIMO_BIT
      #define ASPI_SIMO_BIT       BIT4
      #define ASPI_SIMO_P(suffix) P3##suffix
      #define ASPI_CLK_BIT        BIT0
      #define ASPI_CLK_P(suffix)  P3##suffix
    #endif
    
  #elif ASPI_USCI == ASPI_USCI_A1
  
    #define ASPI_CTL0       UCA1CTL0
    #define ASPI_CTL1       UCA1CTL1
    #define ASPI_BR0        UCA1BR0
    #define ASPI_BR1        UCA1BR1
    #define ASPI_STAT       UCA1STAT
    #define ASPI_RX_BUF     UCA1RXBUF
    #define ASPI_TX_BUF     UCA1TXBUF
    /* interrupt enable register & bits */
    #define ASPI_IE         UC1IE
    #define ASPI_RX_IE      UCA1RXIE
    /* interrupt flag register & bits */
    #define ASPI_IFG        UC1IFG
    #define ASPI_RX_IFG     UCA1RXIFG
    #define ASPI_TX_IFG     UCA1TXIFG
    /* 
     * Macro to clear modulation control (SPI doesn't use modulation control,
     * but MSP430 manuals says it should be set to 0).
     */
    #define ASPI_CLEAR_MCTL()  {UCA1MCTL = 0;}
    /*
     * Macro to declare ISR for RX interrupt. Note USCI A1 and B1 share the
     * same RX interrupt vector, so if they both use the RX interrupt, the ISR
     * must determine which USCI asserted the interrupt.
     */
    #define ASPI_RX_VECTOR  USCIAB1RX_VECTOR
    /*
     * Default I/O port pins & macros to form names of port registers.
     */
    #ifndef ASPI_SIMO_BIT
      #define ASPI_SIMO_BIT       BIT6
      #define ASPI_SIMO_P(suffix) P3##suffix
      #define ASPI_CLK_BIT        BIT0
      #define ASPI_CLK_P(suffix)  P5##suffix
    #endif
    
  #elif ASPI_USCI == ASPI_USCI_B0
  
    #define ASPI_CTL0       UCB0CTL0
    #define ASPI_CTL1       UCB0CTL1
    #define ASPI_BR0        UCB0BR0
    #define ASPI_BR1        UCB0BR1
    #define ASPI_STAT       UCB0STAT
    #define ASPI_RX_BUF     UCB0RXBUF
    #define ASPI_TX_BUF     UCB0TXBUF
    /* interrupt enable register & bits */
    #define ASPI_IE         UC0IE
    #define ASPI_RX_IE      UCB0RXIE
    /* interrupt flag register & bits */
    #define ASPI_IFG        UC0IFG
    #define ASPI_RX_IFG     UCB0RXIFG
    #define ASPI_TX_IFG     UCB0TXIFG
    /*
     * Macro to clear modulation control register (does nothing since USCI
     * B0 & B1 don't have a modulation control register).
     */
    #define ASPI_CLEAR_MCTL()
    /*
     * Macro to declare ISR for RX interrupt. Note USCI A0 and B0 share the
     * same RX interrupt vector, so if they both use the RX interrupt, the ISR
     * must determine which USCI asserted the interrupt.
     */
    #define ASPI_RX_VECTOR  USCIAB0RX_VECTOR
    /*
     * Default I/O port pins & macros to form names of port registers.
     */
    #ifndef ASPI_SIMO_BIT
      #define ASPI_SIMO_BIT       BIT1
      #define ASPI_SIMO_P(suffix) P3##suffix
      #define ASPI_CLK_BIT        BIT3
      #define ASPI_CLK_P(suffix)  P3##suffix
    #endif
    
  #elif ASPI_USCI == ASPI_USCI_B1
  
    #define ASPI_CTL0       UCB1CTL0
    #define ASPI_CTL1       UCB1CTL1
    #define ASPI_BR0        UCB1BR0
    #define ASPI_BR1        UCB1BR1
    #define ASPI_STAT       UCB1STAT
    #define ASPI_RX_BUF     UCB1RXBUF
    #define ASPI_TX_BUF     UCB1TXBUF
    /* interrupt enable register & bits */
    #define ASPI_IE         UC1IE
    #define ASPI_RX_IE      UCB1RXIE
    /* interrupt flag register & bits */
    #define ASPI_IFG        UC1IFG
    #define ASPI_RX_IFG     UCB1RXIFG
    #define ASPI_TX_IFG     UCB1TXIFG
    /*
     * Macro to clear modulation control register (does nothing since USCI
     * B0 & B1 don't have a modulation control register).
     */
    #define ASPI_CLEAR_MCTL()
    /*
     * Macro to declare ISR for RX interrupt. Note USCI A1 and B1 share the
     * same RX interrupt vector, so if they both use the RX interrupt, the ISR
     * must determine which USCI asserted the interrupt.
     */
    #define ASPI_RX_VECTOR  USCIAB1RX_VECTOR
    /*
     * Default I/O port pins & macros to form names of port registers.
     */
    #ifndef ASPI_SIMO_BIT
      #define ASPI_SIMO_BIT       BIT1
      #define ASPI_SIMO_P(suffix) P5##suffix
      #define ASPI_CLK_BIT        BIT3
      #define ASPI_CLK_P(suffix)  P5##suffix
    #endif
    
  #endif
  
  /* settings for clock source (ASPI_CTL1.UCSELx bits) */
  #define ASPI_ACLK   UCSSEL_1
  #define ASPI_SMCLK  UCSSEL_2
  /* non-zero if ADP SPI interface is in master mode */
  #define ASPI_MASTER  RD_BIT(ASPI_CTL0, UCMST)
  /* non-zero when TX buf is empty */
  #define ASPI_TX_EMPTY  RD_BIT(ASPI_IFG, ASPI_TX_IFG)
  /* non-zero when RX buf is full (received byte) */
  #define ASPI_RX_FULL  RD_BIT(ASPI_IFG, ASPI_RX_IFG)
  /* macro to change RX interrupt enable */
  #define ASPI_WR_RX_INT_ENAB(val)  WR_BIT(ASPI_IE, ASPI_RX_IE, val)
  /* non-zero when serial interface is busy (transmitting or receiving) */
  #define ASPI_BUSY  RD_BIT(ASPI_STAT, UCBUSY)
  
#endif /* #ifdef __msp430x26x */

/* If ASPI_CLK_SRC or ADP_CLK_FREQ aren't defined through "Adp/Build/Build.h", use defaults.
 * Note the clock source only affects master mode (in slave mode, the USCI
 * always uses the external UCLK from the master).
  */
#ifndef ASPI_CLK_SRC
  #define ASPI_CLK_SRC  ASPI_ACLK
#endif
/**/
#ifndef ASPI_CLK_FREQ
  #define ASPI_CLK_FREQ  (UD32)4000000
#endif
/*
 * Clock divider to get 4 MHz SPI clock. Note for MSP430x16x micro-controllers,
 * the manual says UxBR (ASPI_BR0 and ASPI_BR1) must be >= 2 to ensure proper
 * SPI operation, so ASPI_CLK_DIV must be >= 2. For MSP430x26x
 * micro-controllers, the manual says ASPI_CLK_DIV can be 1.
 */
#if ASPI_CLK_SRC == ASPI_ACLK
  #define ASPI_CLK_DIV  (ACLK_FREQ / ASPI_CLK_FREQ)
#elif ASPI_CLK_SRC == ASPI_SMCLK
  #define ASPI_CLK_DIV  (SMCLK_FREQ / ASPI_CLK_FREQ)
#endif

/* Defines for the ASPI_SLAVE_READY_BIT I/O pin. Note ASPI_SLAVE_READY_BIT and
 * ASPI_SLAVE_READY_P() must be defined in the build include (for a description
 * of build includes and how they are used, see "Adp/Build/Build.h").
 * 
 * When the ADP SPI interface is idle, both sides are in slave mode, and both
 * sides configure this pin to be an input, so it is 1 by default (it's pulled
 * up by level shifters on the ADP board when it's not driven). When one side
 * has a packet to transmit, it switches to master mode and transmits the first
 * byte of the packet (without waiting). When the slave receives the byte, it
 * changes the pin to an output and strobes it low to tell the master it
 * received the byte. The strobe is latched by the master (see
 * ASPI_SLAVE_READY), and the master waits for the strobe before transmitting
 * the next byte. Thereafter, as the slave receives each byte, it continues to
 * strobe the pin, and the master waits for each strobe before transmitting the
 * next byte. The defines are as follows:
 * 
 * ASPI_SLAVE_READY: The master checks this to see if the slave has received
 * the previous byte and is ready for the next. It is set to 1 (latched) when
 * the slave strobes ASPI_SLAVE_READY_BIT low. It's latched on the falling edge
 * of the strobe so the master will detect it as soon as possible, so the
 * master will transmit the next byte as soon as possible. 
 * 
 * ASPI_WR_SLAVE_READY(val): the master uses this to clear ASPI_SLAVE_READY
 * before transmitting the next byte.
 *
 * ASPI_STROBE_SLAVE_READY(): This temporarily changes ADP_SPI_READY_BIT to
 * be an output, then an input again, which strobes the external signal low
 * (because the corresponding bit in the output port is 0, and the external
 * signal is pulled high when it's not driven). The slave invokes this after
 * receiving each byte to tell the master it received the byte.
 */
#define ASPI_SLAVE_READY \
    RD_BIT(ASPI_SLAVE_READY_P(IFG), ASPI_SLAVE_READY_BIT)
/**/    
#define ASPI_WR_SLAVE_READY(val) \
    WR_BIT(ASPI_SLAVE_READY_P(IFG), ASPI_SLAVE_READY_BIT, val)
/**/    
#define ASPI_STROBE_SLAVE_READY() \
{ \
    ASPI_SLAVE_READY_P(DIR) |=  ASPI_SLAVE_READY_BIT; \
    ASPI_SLAVE_READY_P(DIR) &= ~ASPI_SLAVE_READY_BIT; \
}

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global Declarations
 */

/**************************************************************************
 * External Function Prototypes
 */
 
#endif /* ensure this file is only included once */
