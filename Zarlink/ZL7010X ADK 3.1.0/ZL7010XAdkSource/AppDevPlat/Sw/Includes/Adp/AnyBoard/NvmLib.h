/**************************************************************************
 * This file contains public structures and defines for the nonvolatile-memory
 * library in the firmware. This library is used to manage the nonvolatile
 * memory (NVM).
 *
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2016. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2016. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adp_AnyBoard_NvmLib_h
#define Adp_AnyBoard_NvmLib_h

/**************************************************************************
 * Defines and Macros
 */

/* key used to indicate the NVM is initialized (ascii "NVM ") */
#define NVM_INIT_KEY  0x4E564D20UL

/* NVM types */
#define NVM_TYPE_550_ADK_BAR   1
#define NVM_TYPE_550_WSN_HUB   2
#define NVM_TYPE_550_WSN_NODE  3
#define NVM_TYPE_10X_ADK_BSM   4
#define NVM_TYPE_10X_ADK_IM    5

/* Error group and error codes for the NVM library. Note that for the error
 * group, embedded firmware should always reference the global variable
 * NvmLibErr instead of NVM_LIB_ERR so all of the references will share the
 * same constant global string.
 *
 * The error ID strings (EID) include a detailed message for each error. These
 * are provided for use on the host (PC). For the format and use of error ID
 * strings, see "[SourceTree]\AppDevPlat\Sw\Pc\Libs\ErrInfoLib.c". Embedded
 * firmware doesn't use the error ID strings because they would require too
 * much memory (the firmware only uses error groups and error codes).
 */
#define NVM_LIB_ERR "NvmLibErr"
extern const char *NvmLibErr;
/**/
#define NVM_LIB_ERR_NVM_IS_NOT_INIT                                          1
#define NVM_LIB_EID_NVM_IS_NOT_INIT \
    "NvmLibErr.1: The nonvolatile memory is not initialized."
#define NVM_LIB_ERR_NVM_IS_WRONG_TYPE                                        2
#define NVM_LIB_EID_NVM_IS_WRONG_TYPE \
    "NvmLibErr.2: The nonvolatile memory is the wrong type."
#define NVM_LIB_ERR_NVM_IS_OLDER_VER                                         3
#define NVM_LIB_EID_NVM_IS_OLDER_VER \
    "NvmLibErr.3: The nonvolatile memory appears to be an " \
    "older (smaller) version that the firmware does not support."

/**************************************************************************
 * Data Structures and Typedefs
 */

/* Structure for the information at the beginning of nonvolatile memory (NVM).
 */
typedef struct
{
    /* key used to indicate the NVM is initialized (see NVM_INIT_KEY) */
    const UD32_BYTES nvmInitKey;

    /* NVM type (see NVM_TYPE_10X_ADK_BSM, ...) */
    const UD16_BYTES nvmType;

    /* the NVM size (bytes) including the NVM_INFO at the beginning */
    const UD16_BYTES nvmSize;

} NVM_INFO;

/**************************************************************************
 * Global Declarations
 */

/**************************************************************************
 * External Function Prototypes
 */
extern RSTAT NvmInit(const NVM_INFO *ni, UD16 minNvmSize, UD16 reqNvmType);

#endif /* ensure this file is only included once */
