/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adp_AnyBoard_SysTimerLib_h
#define Adp_AnyBoard_SysTimerLib_h

#include "Adp/General.h"             /* RSTAT, BOOL, TRUE/FALSE, ... */
#include "Adp/Build/Build.h"         /* UD16, UD32, ... */
#include "Adp/AnyBoard/SysTimerHw.h" /* ST_COUNTER, ... */

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* structure of public data for system timer library */
typedef struct {
    
    /* Millisecond time. Note this should only be accessed by calling StMs().
     * The capture/compare interrupt (StCc0Isr()) occurs every ST_MS_PER_INT
     * milliseconds and adds ST_MS_PER_INT to this time. Thus, if ST_MS_PER_INT
     * is 1, the interrupt will occur every millisecond and add 1, so the
     * resolution will be 1ms. In contrast, if ST_MS_PER_INT is is 2 (for
     * example), the interrupt will occur every 2 ms and add 2, so the
     * resolution will be 2 ms.
     * 
     * Note this will complete once full cycle every 65,536 milliseconds (about
     * 65 seconds), so it can't be used to detect time periods longer than ~60
     * seconds. When it reaches its maximum value (65535), it wraps back to 0
     * and continues counting from there.
     * 
     * Note this is declared "const" because it's managed by the system
     * timer library, which is the only thing that should modify it. It's
     * also declared volatile to prevent the compiler from assuming it won't
     * change during a section of code, since it is updated by an interrupt.
     */
    volatile const UD16 msTime;
    
} ST_PUB;
 
/**************************************************************************
 * Global Declarations
 */
 
/* public data for system timer */
extern ST_PUB  StPub;
 
/**************************************************************************
 * External Function Prototypes
 */
 
extern RSTAT StInit(void);
extern UD32 StCountToUs(UD16 count);

/**************************************************************************
 * Get the current value of the system timer counter. Note the rate of the
 * system timer counter is determined by ST_COUNT_PER_SEC, which is defined
 * in "Adp/AnyBoard/SysTimerHw.h".
 */
__inline UD16
StCount(void)
{
    return(ST_COUNTER);
}

/**************************************************************************
 * Get the number of system timer counts corresponding to the specified 
 * number of microseconds. The returned value is 32 bits to ensure it can hold
 * the count corresponding to the maximum number of microseconds (in case the
 * count is > the number of microseconds).
 * 
 * Note the passed argument should be a constant if possible, so the compiler
 * will do the calculations at compile-time, which won't generate any in-line
 * code. If the passed argument is a variable, this will do calculations at
 * run-time, which might generate a lot of in-line code. In that case, it's
 * best to create a real function that invokes this in-line function.
 */
__inline UD32
StUsToCount(UD16 us)
{
    UD32 count;
    
    /* Calculate the number of system timer counts corresponding to the
     * specified number of microseconds. This references ST_COUNT_PER_SEC at
     * compile-time to decide which calculation to use, and uses a simpler
     * calculation if possible. This won't make any difference if the passed
     * argument is a constant, because the compiler will do the calculation at
     * compile-time. However, if the passed argument is variable, the simpler
     * logic will be smaller and faster.
     * 
     * If ST_COUNT_PER_SEC is a multiple of 1000000, this simply divides
     * ST_COUNT_PER_SEC by 1000000 to get the exact count per microsecond,
     * then multiplies that by the specified number of microseconds.
     * 
     * If ST_COUNT_PER_SEC <= 0xFFFFFFFF/0xFFFF (as is the case for a 32768
     * Hz clock), we know the product of ST_COUNT_PER_SEC and the us argument
     * will always fit in 32 bits. Thus, this just multiplies them and divides
     * the product by 1000000. The - 1 and + 1 in the calculation ensures the 
     * count will always represent a time >= the specified time.
     * 
     * For all other values of ST_COUNT_PER_SEC, this checks the us argument
     * at run-time to decide which calculation to use. For larger values, it
     * gives up some accuracy to ensure the math will fit in 32 bits. Note
     * ST_COUNT_PER_SEC must be < 65,538,000 to ensure the math will fit in 32
     * bits for the maximum us argument. The - 1 and + 1 in the calculations
     * ensures the count will always represent a time >= the specified time.
     */
    if ((ST_COUNT_PER_SEC % 1000000UL) == 0) {
        
        count = us * (ST_COUNT_PER_SEC / 1000000UL);
        
    } else if (ST_COUNT_PER_SEC <= (0xFFFFFFFFUL / 0xFFFF)) {
        
        count = (((us * ST_COUNT_PER_SEC) - 1) / 1000000UL) + 1;
        
    } else {
            
        if (us <= (0xFFFFFFFFUL / ST_COUNT_PER_SEC)) {
            count = ((us * ST_COUNT_PER_SEC) - 1) / 1000000UL;
        } else if (us <= (0xFFFFFFFFUL / (ST_COUNT_PER_SEC / 10))) {
            count = ((us * (ST_COUNT_PER_SEC / 10)) - 1) / 100000UL;
        } else if (us <= (0xFFFFFFFFUL / (ST_COUNT_PER_SEC / 100))) {
            count = ((us * (ST_COUNT_PER_SEC / 100)) - 1) / 10000;
        } else {
            count = ((us * (ST_COUNT_PER_SEC / 1000)) - 1) / 1000;
        }
        count += 1; 
    }
    return(count);
}

/**************************************************************************
 * This checks if the specified number of system timer counts (remCount) have
 * elapsed since the specified start count (startCount). It returns true if the
 * specified number of counts have elapsed, and false otherwise. Note this can
 * be used to detect time periods accurate to within a handful of microseconds.
 * If you only need accuracy to within a few milliseconds, it's better to use
 * StElapsedMs() because it's more efficient.
 * 
 * StElapsedCount() is typically called inside a loop. The start count and
 * remaining count are initialized before the loop, then each iteration of the
 * loop calls this function to check if the specified number of counts have
 * elapsed since the start count. Note each time this is called, it also
 * updates the start count and remaining count. That way, it can be used to
 * detect time periods >= one system timer cycle (65536 counts). Note for this
 * to work accurately, each iteration of the loop must take less than one
 * system timer cycle. For example, if ST_COUNT_PER_SEC is 1000000, the system
 * timer will cycle every ~64 ms, so each iteration of the loop must take < ~64
 * ms. Otherwise, this function will miss system timer cycles, and will
 * therefore take longer than intended. Here's a usage example:
 * 
 * void
 * ExampleLoop(void)
 * {
 *     UD16 startCount; 
 *     UD32 remCount;
 * 
 *     for(startCount = StCount(), remCount = StUsToCount(10000);) {
 * 
 *         if (StElapsedCount(&startCount, &remCount) break;
 *         ...
 *     }
 * }
 */
__inline BOOL
StElapsedCount(UD16 *startCount, UD32 *remCount)
{
    UD16 n;
    
    /* The actual number of count periods that have elapsed since the previous
     * call is (StCount() - *startCount +- 1). For example, when StCount() -
     * *startCount = 10, the actual number of count periods could be anywhere
     * from 9 to 11. Thus, this function will only return TRUE if StCount() -
     * *startCount is > *remCount, ensuring the actual number of count periods
     * that have elapsed is at least as large as *remCount. For example, if
     * *remCount is 10, this function will only return TRUE if StCount() -
     * *startCount is > 10, ensuring at least 10 full count periods have
     * actually elapsed.
     */
    n = StCount() - *startCount;
    if ((n > (UD16)*remCount) && ((UD16)(*remCount >> 16) == 0)) return(TRUE);
    *startCount += n;
    *remCount -= n;
    return(FALSE);
}

/**************************************************************************
 * Wait until the specified number of microseconds have elapsed since the
 * specified start time (a count returned by a previous call to StCount()).
 * This function won't return until at least the specified number of
 * microseconds have elapsed since StCount() was called to get the start count.
 * 
 * Note if the system timer resolution is > 1us, this function will wait until
 * the next multiple of the resolution greater than the specified number of
 * microseconds. For example, if the resolution is 5us, and the specified
 * microseconds is 23, this function will wait until at least 25 us have
 * elapsed since the start count. If the specified microseconds have already
 * elapsed by the time this function is called, it will return immediately.
 * 
 * Note the passed argument (number of microseconds) should be a constant if
 * possible, so the compiler will do calculations beforehand. If the passed
 * argument is a variable and ST_COUNT_PER_SEC is > 1000000, the calculations
 * performed at run-time will generate a lot of inline code. In that case, you
 * might want to create a real function that invokes this inline function,
 * especially if you need to call it in more than one place.
 *
 * Note this does not sleep while it is waiting. Rather, it polls the counter
 * for the system timer, which monopolizes the CPU (except for interrupts).
 * Thus, you should avoid calling this function on a regular basis. Also, if
 * you are calling this function to wait for a long period, make sure it won't
 * cause any problems.
 */
__inline void
StWaitUs(UD16 startCount, UD16 us)
{
    UD16 count16, n;
    UD32 count32;
    
    /* calc sys timer count to ensure at least the specified usecs */
    count32 = StUsToCount(us);
    
    /* If the count fits in 16 bits, use 16 bit math. Note if the passed 'us'
     * is a constant, the resulting count will also be a constant, so the
     * compiler will handle the 'if' statement. However, if the passed 'us' is
     * a variable, the resulting count will be a variable, so it will have to
     * be checked at run-time. To help prevent this, ST_COUNT_PER_SEC is also
     * checked, so if it's <= 1000000, the compiler will still handle the 'if'
     * statement. Note 1000000 is used because if ST_COUNT_PER_SEC is <=
     * 1000000, we know the count must be <= the passed 'us', and since 'us'
     * is 16 bits, the count must therefore also fit in 16 bits.
     */
    if ((ST_COUNT_PER_SEC <= 1000000UL) || ((UD16)(count32 >> 16) == 0)) {
        
        count16 = (UD16)count32;
        
        /* If the count to wait is < 1/2 of a full counter cycle, just wait
         * until the counter delta exceeds the required count. Otherwise, update
         * the start count and remaining count each time through the loop.
         * 
         * Explanation: while in a wait loop, an interrupt may occur. If the
         * interrupt takes too long, the timer may complete one full cycle and
         * lap the start count before the wait loop gets to run again. If that
         * happens, the loop will miss its stop count, so it won't stop until
         * the next go around. To help prevent this, if the count to wait is >=
         * 1/2 of a full cycle, the start count is updated each time through
         * the loop. As a result, in that case, an interrupt would have to take
         * more than one full cycle to cause a miss. In contrast, if the count
         * to wait is < 1/2 of a full cycle, the start count isn't updated each
         * time through the loop (because that generates less inline code). In
         * that case, in the worst scenario, an interrupt would have to take at
         * least 1/2 of a full cycle to cause a miss. Note in either case, if
         * an interrupt does cause a miss, it will just extend the wait time
         * (the loop will just stop the next go around).
         */
        if (count16 < (0xFFFF / 2)) {
            while((StCount() - startCount) <= count16);
        } else {
            while((n = (StCount() - startCount)) <= count16) {
                startCount += n;
                count16 -= n;
            }
        }
    } else {
        
        /* Since the maximum StCount() - startCount is 16 bits (0xFFFF), it
         * can't cover a 32 bit count, so we have to update the start count
         * and remaining count each time through the loop.
         */
        while((n = (StCount() - startCount)) <= count32) {
            startCount += n;
            count32 -= n;
        }
    }
}

/**************************************************************************
 * Delay the specified number of microseconds.
 * 
 * Note the passed argument (number of microseconds) should be a constant if
 * possible, so the compiler will do calculations beforehand. If the passed
 * argument is a variable and ST_COUNT_PER_SEC is > 1000000, the calculations
 * performed at run-time will generate a lot of inline code. In that case, you
 * might want to create a real function that invokes this inline function,
 * especially if you need to call it in more than one place.
 * 
 * Note this does not sleep while it is waiting. Rather, it polls the system
 * timer count, which monopolizes the CPU (except for interrupts). Thus, you
 * should avoid calling this function on a regular basis. Also, if you are
 * calling this function to wait for a long period, make sure it won't cause
 * any problems. If you need to wait for a period of milliseconds, and you do
 * not require microsecond resolution, it's better to use StDelayMs().
 */
__inline void
StDelayUs(UD16 us)
{
    UD16 startCount = StCount();
    StWaitUs(startCount, us);
}

/**************************************************************************
 * Get the current time in milliseconds. Note this will complete one full
 * cycle every 65,536 milliseconds (about 65 seconds). When it reaches its
 * maximum value (65535), it wraps back to 0 and continues counting.
 */
__inline UD16
StMs(void)
{
    return(StPub.msTime);
}

/**************************************************************************
 * This checks if the specified number of milliseconds (ms) have elapsed since
 * the specified start time (startMs). It returns true if the specified number
 * of milliseconds have elapsed, and false otherwise. Note the millisecond time
 * maintained by the system timer completes one full cycle every 65,536 ms
 * (about 65 seconds). Thus, this function can't be used to detect time periods
 * longer than ~60 seconds. Also, this can only detect time periods accurate to
 * within a few milliseconds. If you need better accuracy than that, use
 * StElapsedCount() instead.
 * 
 * StElapsedMs() is typically called inside a loop. The start time (startMs)
 * is initialized before the loop, then each iteration of the loop calls this
 * function to check if the specified number of milliseconds have elapsed
 * since the start time. Here's a usage example:
 * 
 * void
 * ExampleLoop(void)
 * {
 *     UD16 startMs;
 * 
 *     for(startMs = StMs();;) {
 * 
 *         if (StElapsedMs(startMs, 50) break;
 *         ...
 *     }
 * }
 */
__inline BOOL
StElapsedMs(UD16 startMs, UD16 ms)
{    
    /* The actual of number of milliseconds that have elapsed since
     * startMs is (StMs() - startMs +- ST_MS_PER_INT). For example, if
     * StMs() - startMs = 10, and ST_MS_PER_INT = 1, the actual elapsed could
     * be anywhere from 9 to 11 ms, and if ST_MS_PER_INT = 2, the actual
     * elapsed could be anywhere from 8 to 12 ms. Thus, this function only
     * returns TRUE when StMs() - startMs is >= ms + ST_MS_PER_INT, ensuring
     * the actual elapsed is at least as large as the specified ms. For
     * example, if ms is 10, and ST_MS_PER_INT = 1, it will only return TRUE
     * when StMs() - startMs is >= 11, ensuring at least 10 full milliseconds
     * have elapsed (11 - ST_MS_PER_INT), and if ST_MS_PER_INT = 2, it will
     * only TRUE when StMs() - startMs is >= 12, ensuring at least 10 full
     * milliseconds have elapsed (12 - ST_MS_PER_INT).
     * 
     * If ST_MS_PER_INT = 1, use > for the compare (instead of using >= and
     * adding ST_MS_PER_INT to the specified ms). If the specified ms is a
     * constant, this won't make any difference at run-time, because the
     * compiler will calculate ms + ST_MS_PER_INT beforehand. However, if the
     * specified ms is a variable, and ST_MS_PER_INT = 1, this will save an
     * instruction at run-time.
     */
    if (ST_MS_PER_INT == 1) {
        return((StMs() - startMs) > ms);
    } else {
        return((StMs() - startMs) >= (ms + ST_MS_PER_INT));
    }
}

/**************************************************************************
 * Delay the specified number of milliseconds.
 * 
 * Note this does not sleep while it is waiting. Rather, it polls the
 * millisecond time, which monopolizes the CPU (except for interrupts). Thus,
 * you should avoid calling this function on a regular basis. Also, if you are
 * calling this function to wait for a long period, make sure it won't cause
 * any problems.
 */
__inline void
StDelayMs(UD16 ms)
{
    UD16 startMs = StMs();
    while(StElapsedMs(startMs, ms) == 0);
}

#endif /* ensure this file is only included once */
