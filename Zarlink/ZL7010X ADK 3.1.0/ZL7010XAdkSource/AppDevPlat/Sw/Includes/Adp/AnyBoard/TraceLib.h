/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adp_AnyBoard_TraceLib_h
#define Adp_AnyBoard_TraceLib_h

#include "Adp/General.h"              /* TRUE/FALSE, ... */
#include "Adp/Build/Build.h"          /* GIE, _disable_interrupts(), ... */
#include "Adp/AnyBoard/SysTimerLib.h" /* StCount(), StMs(), ... */ 

/**************************************************************************
 * Defines and Macros
 */
 
/* Defines to configure tracing.
 * 
 * TRACE_ENAB: If false, the trace functions (Trace0(), Trace1(), etc.) will
 * be defined to be nothing, so they won't do anything if they're invoked.
 * 
 * TRACE_PRINTF: If true, the trace functions will also call printf(). Note for
 * this to work, TRACE_ENAB must also be true, and DEBUG must be defined.
 * 
 * TRACE_TIME: Set to TRACE_US to record microsecond time, TRACE_MS to record
 * millisecond time, or FALSE for no time. Note the resolution will be as high
 * as possible using the system timer (depending on the system timer settings,
 * it may be as good as 1 us for TRACE_US, or 1 ms for TRACE_MS).
 * 
 * TRACE_INTERRUPT_SAFE: If true, the trace functions will include logic to
 * coordinate with interrupts so they can safely be called in interrupt service
 * routines. If you need to minimize the overhead in the trace functions, and
 * you don't need to call them in interrupt service routines, make this false.
 * 
 * TRACE_SIZE: Size of trace buffer (1 is added because last entry is ignored).
 * 
 * TRACE_MSG_BUF_SIZE: Size of buffer TraceGetNext() uses to build the message
 * (add time & insert args). 1 is added to account for the terminating '\0'
 * (the maximum length of the resulting message is buffer size - 1).
 */
#define TRACE_ENAB            FALSE
#define TRACE_PRINTF          FALSE  /* if true, DEBUG must also be defined */
#define TRACE_TIME            TRACE_MS
#define TRACE_INTERRUPT_SAFE  TRUE
#define TRACE_SIZE            (100 + 1)
#define TRACE_MSG_BUF_SIZE    (80 + 1)

/* if DEBUG defined, include StdIo.h for printf(); else ensure no printf() */
#ifdef DEBUG
#  include "Standard/StdIo.h"
#else
#  undef TRACE_PRINTF
#  define TRACE_PRINTF  FALSE
#endif

/* settings for TRACE_TIME (TRACE_TIME may also be set to FALSE) */
#define TRACE_US  1
#define TRACE_MS  2

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* Structure of entry in trace buffer. Note though this data is global, it is
 * private to the trace library, and should only be accessed via the trace
 * library functions (i.e. it should not be accessed directly).
 */
typedef struct {
    
        /* pointer to message and any arguments to insert into it */
        const char *msg;
        unsigned int arg1;
        unsigned int arg2;
        
#if TRACE_TIME
        /* time of trace entry (either system timer count or ms time) */
        unsigned int stTime;    
#endif

} TRACE_ENTRY;
 
/* Structure of private data for trace library. Note though this data is
 * global, it is private to the trace library, and should only be accessed
 * via the trace library functions (i.e. it should not be accessed directly).
 */
typedef struct {
    
    /* next entry in trace buffer (for TraceGetNext()) */
    TRACE_ENTRY *next;
    
    /* next unused entry in trace buffer (for TraceGetUnused()) */
    TRACE_ENTRY *unused;
    
    /* trace buffer */
    TRACE_ENTRY entry[TRACE_SIZE];
    
    /* buffer TraceGetNext() uses to build message (add time & insert args) */
    char msgBuf[TRACE_MSG_BUF_SIZE];
    
} TRACE_PRIV;
 
/**************************************************************************
 * Global Declarations
 */
 
/* Private data for the trace library. Note though this data is global, it is
 * private to the trace library, and should only be accessed via the trace
 * library functions (i.e. it should not be accessed directly).
 */
extern TRACE_PRIV  TracePriv;
 
/**************************************************************************
 * External Function Prototypes
 */
 
/* public trace library functions */ 
extern const char * TraceGetNext(void);

/* if tracing is enabled, define inline trace functions */
#if TRACE_ENAB

/* declare function that is private to trace library */
__inline TRACE_ENTRY * TraceGetUnused(void);
 
/**************************************************************************
 * Log a trace message with no arguments.
 */
__inline void
Trace0(const char *msg)
{
    TRACE_ENTRY *te = TraceGetUnused();
    te->msg = msg;
    
    #if TRACE_PRINTF
        printf(msg);
    #endif
}

/**************************************************************************
 * Log a trace message with 1 argument to insert.
 */
__inline void
Trace1(const char *msg, unsigned int arg1)
{
    TRACE_ENTRY *te = TraceGetUnused();
    te->msg = msg;
    te->arg1 = arg1;
    
    #if TRACE_PRINTF
        printf(msg, arg1);
    #endif
}

/**************************************************************************
 * Log a trace message with 2 arguments to insert.
 */
__inline void
Trace2(const char *msg, unsigned int arg1, unsigned int arg2)
{
    TRACE_ENTRY *te = TraceGetUnused();
    te->msg = msg;
    te->arg1 = arg1;
    te->arg2 = arg2;
    
    #if TRACE_PRINTF
        printf(msg, arg1, arg2);
    #endif
}

/**************************************************************************
 * Get next unused entry in trace buffer (also sets time in entry). If the
 * trace buffer is full, this will return the last entry in the buffer, which
 * is ok to keep overwriting because it's ignored (i.e. it's not included in
 * the trace).
 */
__inline TRACE_ENTRY *
TraceGetUnused(void)
{
    TRACE_ENTRY *te;
    unsigned int prevGie;
    
    /* If we want to be interrupt safe, save GIE setting (interrupt enable),
     * then disable all interrupts so no ISR will call a trace function.
     */
    if (TRACE_INTERRUPT_SAFE) {
        prevGie = _get_SR_register() & GIE;
        _disable_interrupts();
    }        
    
    /* Get pointer to the next unused entry, then if it's not the last entry
     * in the buffer (dummy entry that is ignored), increment it.
     */
    te = TracePriv.unused;
    if (TracePriv.unused < &TracePriv.entry[TRACE_SIZE - 1]) ++TracePriv.unused;
    
#if TRACE_TIME    
    /* save time at which entry was made */
    if (TRACE_TIME == TRACE_US) {
        te->stTime = StCount();
    } else {
        te->stTime = StMs();
    }
#endif    

    /* if interrupt safe, restore previous GIE (interrupt enable) */
    if (TRACE_INTERRUPT_SAFE) _bis_SR_register(prevGie);
    
    /* return pointer to unused entry */
    return(te);
}

#else /* else, TRACE_ENAB = FALSE, so define trace functions to do nothing */
#  define Trace0(msg)
#  define Trace1(msg, arg1)
#  define Trace2(msg, arg1, arg2)
#endif

#endif /* ensure this file is only included once */
