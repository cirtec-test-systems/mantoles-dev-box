/**************************************************************************
 * This file contains structures and defines used to communicate with
 * micro-controller devices (boards). These are common to all devices; those
 * specific to each device reside in a separate include file for each device.
 *
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

/* ensure this file is only included once */
#ifndef Adp_AnyBoard_Com_h
#define Adp_AnyBoard_Com_h

#include "Adp/Build/Build.h" /* UD8, ... */

/**************************************************************************
 * Defines and Macros
 */
 
/* Device addresses that are common to all applications. A source and
 * destination address are specified in each packet header to indicate who
 * the packet is from, and who it is for (see COM_PACK_HDR.srcAndDest). The
 * addresses defined below are common to all applications and should not be
 * used for any other purpose. Note address 0 is invalid:
 * 
 * COM_PC_ADDR:
 *     Address for the PC. As of this writing, all command packets originate
 *     from the PC, so the PC is the source address for all command packets,
 *     and the destination address for all reply packets.
 * COM_LOCAL_ADP_ADDR:
 *     Address for a local ADP board (an ADP board on the local side of a
 *     wireless link, connected to the PC via USB).
 * COM_LOCAL_MEZZ_ADDR:
 *     Address for a mezzanine board attached to a local ADP board.
 * COM_REMOTE_MEZZ_ADDR:
 *     Address for a mezzanine board attached to a remote ADP board.
 * COM_REMOTE_ADP_ADDR:
 *     Address for a remote ADP board (an ADP board on the remote side of a
 *     wireless link).
 * COM_RESERVED_ADDR_6, COM_RESERVED_ADDR_7:
 *     Reserved for possible future use.
 */
#define COM_PC_ADDR           1
#define COM_LOCAL_ADP_ADDR    2
#define COM_LOCAL_MEZZ_ADDR   3
#define COM_REMOTE_MEZZ_ADDR  4
#define COM_REMOTE_ADP_ADDR   5
#define COM_RESERVED_ADDR_6   6
#define COM_RESERVED_ADDR_7   7
/*
 * Start of addresses that can be defined by the application if desired.
 */
#define COM_APP_START_ADDR    8
/*
 * Maximum valid address (must fit in 4 bits).
 */
#define COM_MAX_ADDR          15 /* 0xF */


/* Packet types that are common to all devices. The packet type is specified 
 * in each communication packet header (see COM_PACK_HDR.type).
 * 
 * Command packet types that are common to all devices:
 * 
 *   COM_GET_DEV_TYPE_CMD_TYPE:
 *       Command to get device type string. For this command, the reply
 *       contains the device type (terminated by '\0'). The buffer size
 *       required to hold a maximum length reply is COM_DEV_TYPE_BUF_SIZE.
 *   COM_GET_DEV_NAME_CMD_TYPE:
 *       Command to get device name string. For this command, the reply
 *       contains the device name (terminated by '\0'). The buffer size
 *       required to hold a maximum length reply is COM_DEV_NAME_BUF_SIZE.
 *   COM_GET_DEV_VER_CMD_TYPE:
 *       Command to get device version string. For this command, the reply
 *       contains the device version (terminated by '\0'). The buffer size
 *       required to hold a maximum length reply is COM_DEV_VER_BUF_SIZE.
 *   COM_GET_DEV_ERR_CMD_TYPE:
 *       Command to get device error information. For this command, the reply
 *       packet is the same as for an error reply (see COM_ERR_REPLY_TYPE), but
 *       the reply type in the reply packet header will be COM_OK_REPLY_TYPE,
 *       not COM_ERR_REPLY_TYPE. The buffer size required to hold a maximum
 *       length reply is COM_ERR_REPLY_BUF_SIZE.
 *   COM_MAX_CMD_TYPE:
 *       Maximum valid command type.
 * 
 * Reserved packet types:
 * 
 *   COM_RESERVED_TYPE_125:
 *       Reserved for possible future use.
 *   COM_RESERVED_TYPE_126:
 *       Reserved for possible future use.
 * 
 * Reply packet types (note these are the only valid reply types):
 * 
 *   COM_OK_REPLY_TYPE:
 *       Reply for a command that was successful. The content of the reply
 *       packet depends on the command the reply is for.
 *   COM_ERR_REPLY_TYPE:
 *       Reply for a command that failed. The reply packet contains an error ID
 *       string (terminated with a '\0'), followed by an error arguments string
 *       (terminated with a '\0'). The format of the error ID string is
 *       "<ErrorGroup>.<ErrorCode>: <DefaultErrorMessage>". The arguments
 *       string contains arguments to insert into the error message. If there
 *       are multiple arguments, they must be separated by '\f' (form feed) in
 *       the arguments string. If there are no arguments, the arguments string
 *       should be empty (just the terminating '\0'). The host (PC) will insert
 *       the first argument where {0} appears in the message, the second where
 *       {1} appears, and so on. For more information about error ID strings,
 *       argument strings, and how they're used, see "AppDevPlat/Sw/Pc/Libs/
 *       ErrInfoLib.c". Note the buffer size required to hold a maximum length
 *       error reply is COM_ERR_REPLY_BUF_SIZE.
 */
#define COM_GET_DEV_TYPE_CMD_TYPE    121  /* 0x79 */
#define COM_GET_DEV_NAME_CMD_TYPE    122  /* 0x7A */
#define COM_GET_DEV_VER_CMD_TYPE     123  /* 0x7B */
#define COM_GET_DEV_ERR_CMD_TYPE     124  /* 0x7B */
#define COM_MAX_CMD_TYPE             124
/* reserved types */
#define COM_RESERVED_TYPE_125        125  /* 0x7D */
#define COM_RESERVED_TYPE_126        126  /* 0x7E */
/* reply types */
#define COM_OK_REPLY_TYPE              0  /* 0x00 */ 
#define COM_ERR_REPLY_TYPE           127  /* 0x7F */

/* maximum packet length (see COM_PACK_HDR.lenLsb & lenMsb) */
#define COM_MAX_PACK_LEN  0xFFFF

/* Size of buffer required to hold a maximum length device type string and
 * its terminating '\0'.
 */
#define COM_DEV_TYPE_BUF_SIZE  128
#define COM_DEV_TYPE_MAX_LEN   (COM_DEV_TYPE_BUF_SIZE - 1)
/*
 * Size of buffer required to hold a maximum length device name and
 * its terminating '\0'.
 */
#define COM_DEV_NAME_BUF_SIZE  128
#define COM_DEV_NAME_MAX_LEN   (COM_DEV_NAME_BUF_SIZE - 1)
/*
 * Size of buffer required to hold a maximum length version string and its
 * terminating '\0'.
 */
#define COM_DEV_VER_BUF_SIZE  256
#define COM_DEV_VER_MAX_LEN   (COM_DEV_VER_BUF_SIZE - 1)
/*
 * Size of buffer required to hold a maximum length error reply. For the
 * content of an error reply packet, see COM_ERR_REPLY_TYPE.
 */
#define COM_ERR_REPLY_BUF_SIZE  256

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* Structure for packet header. Every communication packet must begin with
 * this header, including all command and reply packets.
 */
typedef struct {
    
    /* The packet source and destination addresses (source in upper 4 bits, 
     * destination in lower 4 bits). These indicate who the packet is from,
     * and who it is for. For addresses that are common to all applications,
     * see COM_PC_ADDR, COM_LOCAL_ADP_ADDR, etc. Note address 0 is invalid.
     * 
     * Note when a reply packet is sent in response to a command packet, the
     * source and destination in the command packet are swapped in the reply
     * packet. This ensures the reply will reach whoever sent the command,
     * and helps confirm the reply is from whomever the command was sent to.
     * As of this writing, all command packets originate from the PC, so
     * COM_PC_ADDR is the source address for all command packets, and the
     * destination address for all reply packets.
     */
    UD8 srcAndDest;
    
    /* Packet type. The most significant bit is reserved for possible future
     * use, and should always be 0 for now. The remaining bits are the type.
     * 
     * Types 1 through 120 are device-specific commands. The meaning of each
     * command and the content of the packet depend on the device the command
     * is for (the destination address).
     * 
     * Types 121 through 126 include commands that are common to all devices
     * (see COM_GET_DEV_TYPE_CMD_TYPE, etc.), and reserved types that should
     * not be used for now.
     * 
     * Types 0 and 127 (0x7F) are reply types. If the command the reply is for
     * was successful, the reply type is 0 (COM_OK_REPLY_TYPE), and the content
     * of the reply packet depends on the command the reply is for. If the
     * command failed, the reply type is 0x7F (COM_ERR_REPLY_TYPE), and the
     * reply packet contains the error information (for the format of the error
     * reply packet, see COM_ERR_REPLY_TYPE).
     */
     UD8 type;
     
    /* Packet length (LSB & MSB). This is the length of the packet excluding
     * packet header. The maximum packet length is 0xFFFF (COM_MAX_PACK_LEN).
     */
    UD8 lenLsb;
    UD8 lenMsb;
    
} COM_PACK_HDR;

/**************************************************************************
 * Global Declarations
 */

/**************************************************************************
 * External Function Prototypes
 */
 
#endif /* ensure this file is only included once */
