/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
/* ensure this file is only included once */
#ifndef Adp_AnyBoard_LocalSpiHw_h
#define Adp_AnyBoard_LocalSpiHw_h

#include "Adp/General.h"     /* RD_BIT(), WR_BIT(), ... */
#include "Adp/Build/Build.h" /* __msp430x16x, U0TXBUF, ACLK_FREQ, ... */

/**************************************************************************
 * Defines and Macros
 */

/* Defines which vary depending on the micro-controller and serial interface.
 */
#ifdef __msp430x16x

  /* settings for LSPI_USART (serial interface for local SPI) & the default */
  #define LSPI_USART_0  0
  #define LSPI_USART_1  1
  /**/
  #ifndef LSPI_USART
    #define LSPI_USART  LSPI_USART_0
  #endif
 
  /* set defines for specified USART */
  #if LSPI_USART == LSPI_USART_0
  
    #define LSPI_CTL        U0CTL
    #define LSPI_TCTL       U0TCTL
    #define LSPI_BR0        U0BR0
    #define LSPI_BR1        U0BR1
    #define LSPI_MCTL       U0MCTL
    #define LSPI_TX_BUF     U0TXBUF
    #define LSPI_RX_BUF     U0RXBUF
    /* module enable register & bits */
    #define LSPI_ME         ME1
    #define LSPI_SPIE       USPIE0
    /* interrupt enable register & bits */
    #define LSPI_IE         U0IE   
    #define LSPI_TX_IE      UTXIE0
    #define LSPI_RX_IE      URXIE0
    /* interrupt flag register & bits */
    #define LSPI_IFG        U0IFG
    #define LSPI_TX_IFG     UTXIFG0
    #define LSPI_RX_IFG     URXIFG0
    /*
     * Macros to declare ISR for TX & RX interrupts.
     */
    #define LSPI_TX_VECTOR  USART0TX_VECTOR
    #define LSPI_RX_VECTOR  USART0RX_VECTOR
    /*
     * Default I/O port pins & macros to form names of port registers.
     */
    #ifndef LSPI_SIMO_BIT
      #define LSPI_SIMO_BIT       BIT1
      #define LSPI_SIMO_P(suffix) P3##suffix
      #define LSPI_SOMI_BIT       BIT2
      #define LSPI_SOMI_P(suffix) P3##suffix
      #define LSPI_CLK_BIT        BIT3
      #define LSPI_CLK_P(suffix)  P3##suffix
    #endif
    
  #elif LSPI_USART == LSPI_USART_1
  
    #define LSPI_CTL        U1CTL
    #define LSPI_TCTL       U1TCTL
    #define LSPI_BR0        U1BR0
    #define LSPI_BR1        U1BR1
    #define LSPI_MCTL       U1MCTL
    #define LSPI_TX_BUF     U1TXBUF
    #define LSPI_RX_BUF     U1RXBUF
    /* module enable register & bits */
    #define LSPI_ME         ME2
    #define LSPI_SPIE       USPIE1
    /* interrupt enable register & bits */
    #define LSPI_IE         U1IE   
    #define LSPI_TX_IE      UTXIE1
    #define LSPI_RX_IE      URXIE1
    /* interrupt flag register & bits */
    #define LSPI_IFG        U1IFG
    #define LSPI_TX_IFG     UTXIFG1
    #define LSPI_RX_IFG     URXIFG1
    /*
     * Macros to declare ISR for TX & RX interrupts.
     */
    #define LSPI_TX_VECTOR  USART1TX_VECTOR
    #define LSPI_RX_VECTOR  USART1RX_VECTOR
    /*
     * Default I/O port pins & macros to form names of port registers.
     */
    #ifndef LSPI_SIMO_BIT
      #define LSPI_SIMO_BIT       BIT1
      #define LSPI_SIMO_P(suffix) P5##suffix
      #define LSPI_SOMI_BIT       BIT2
      #define LSPI_SOMI_P(suffix) P5##suffix
      #define LSPI_CLK_BIT        BIT3
      #define LSPI_CLK_P(suffix)  P5##suffix
    #endif
  
  #endif
  
  /* settings for LSPI_CLK_SRC (clock source = LSPI_TCTL.SSELx bits) */
  #define LSPI_ACLK   SSEL0
  #define LSPI_SMCLK  SSEL1
  
  /* non-zero if local SPI interface is in master mode */
  #define LSPI_MASTER  RD_BIT(LSPI_CTL, MM)
  /* non-zero when TX buf is empty */
  #define LSPI_TX_EMPTY  RD_BIT(LSPI_IFG, LSPI_TX_IFG)
  /* non-zero when TX buf and shift register are empty (done transmitting) */
  #define LSPI_TX_DONE  RD_BIT(LSPI_TCTL, TXEPT)
  /* non-zero when RX buf is full (received byte) */
  #define LSPI_RX_FULL  RD_BIT(LSPI_IFG, LSPI_RX_IFG)
  
  /* macros to change TX & RX interrupt enables */
  #define LSPI_WR_TX_INT_ENAB(val)  WR_BIT(LSPI_IE, LSPI_TX_IE, val)
  #define LSPI_WR_RX_INT_ENAB(val)  WR_BIT(LSPI_IE, LSPI_RX_IE, val)
  
  /* Defines to OR together to form the "specs" argument for LSpiInit()
   * and LSpiConfig(). Note the clock phase and polarity are applied to
   * LSPI_TCTL.CKPH & CKPL, whereas the other specs are applied to LSPI_CTL.
   */
  #define LSPI_SPEC_INACTIVE_LOW_WITH_SAMP_ON_FALLING_EDGE   (0)
  #define LSPI_SPEC_INACTIVE_LOW_WITH_SAMP_ON_RISING_EDGE    (CKPH)
  #define LSPI_SPEC_INACTIVE_HIGH_WITH_SAMP_ON_FALLING_EDGE  (CKPL | CKPH)
  #define LSPI_SPEC_INACTIVE_HIGH_WITH_SAMP_ON_RISING_EDGE   (CKPL)
  /* master or slave mode */
  #define LSPI_SPEC_MASTER  MM
  #define LSPI_SPEC_SLAVE   0
  /* 7 bit or 8 bit (character length) */
  #define LSPI_SPEC_7_BIT  0
  #define LSPI_SPEC_8_BIT  CHAR
  /* MSB first (the MSP430x16x doesn't support LSB first) */
  #define LSPI_SPEC_MSB_FIRST  0
  
#endif /* #ifdef __msp430x16x */
/*
 * Defines for MSP430x26x micro-controllers.
 */
#ifdef __msp430x26x

  /* settings for LSPI_USCI (serial interface for local SPI) & the default */
  #define LSPI_USCI_A0  0
  #define LSPI_USCI_A1  1
  #define LSPI_USCI_B0  2
  #define LSPI_USCI_B1  3
  /**/
  #ifndef LSPI_USCI
    #define LSPI_USCI  LSPI_USCI_A0
  #endif
  
  /* set defines for specified USCI */
  #if LSPI_USCI == LSPI_USCI_A0
  
    #define LSPI_CTL0       UCA0CTL0
    #define LSPI_CTL1       UCA0CTL1
    #define LSPI_BR0        UCA0BR0
    #define LSPI_BR1        UCA0BR1
    #define LSPI_STAT       UCA0STAT
    #define LSPI_TX_BUF     UCA0TXBUF
    #define LSPI_RX_BUF     UCA0RXBUF
    /* interrupt enable register & bits */
    #define LSPI_IE         UC0IE
    #define LSPI_TX_IE      UCA0TXIE
    #define LSPI_RX_IE      UCA0RXIE
    /* interrupt flag register & bits */
    #define LSPI_IFG        UC0IFG
    #define LSPI_TX_IFG     UCA0TXIFG
    #define LSPI_RX_IFG     UCA0RXIFG
    /* 
     * Macro to clear modulation control (SPI doesn't use modulation control,
     * but MSP430 manuals says it should be set to 0).
     */
    #define LSPI_CLEAR_MCTL()  {UCA0MCTL = 0;}
    /*
     * Macros to declare ISR's for interrupts. Note if USCI A0 and B0 are both
     * being used and they both use an interrupt, the ISR must determine which
     * USCI asserted the interrupt.
     */
    #define LSPI_TX_VECTOR  USCIAB0TX_VECTOR
    #define LSPI_RX_VECTOR  USCIAB0RX_VECTOR
    /*
     * Default I/O port pins & macros to form names of port registers.
     */
    #ifndef LSPI_SIMO_BIT
      #define LSPI_SIMO_BIT       BIT4
      #define LSPI_SIMO_P(suffix) P3##suffix
      #define LSPI_SOMI_BIT       BIT5
      #define LSPI_SOMI_P(suffix) P3##suffix
      #define LSPI_CLK_BIT        BIT0
      #define LSPI_CLK_P(suffix)  P3##suffix
    #endif

  #elif LSPI_USCI == LSPI_USCI_A1
  
    #define LSPI_CTL0       UCA1CTL0
    #define LSPI_CTL1       UCA1CTL1
    #define LSPI_BR0        UCA1BR0
    #define LSPI_BR1        UCA1BR1
    #define LSPI_STAT       UCA1STAT
    #define LSPI_TX_BUF     UCA1TXBUF
    #define LSPI_RX_BUF     UCA1RXBUF
    /* interrupt enable register & bits */
    #define LSPI_IE         UC1IE
    #define LSPI_TX_IE      UCA1TXIE
    #define LSPI_RX_IE      UCA1RXIE
    /* interrupt flag register & bits */
    #define LSPI_IFG        UC1IFG
    #define LSPI_TX_IFG     UCA1TXIFG
    #define LSPI_RX_IFG     UCA1RXIFG
    /* 
     * Macro to clear modulation control (SPI doesn't use modulation control,
     * but MSP430 manuals says it should be set to 0).
     */
    #define LSPI_CLEAR_MCTL()  {UCA1MCTL = 0;}
    /*
     * Macros to declare ISR's for interrupts. Note if USCI A1 and B1 are both
     * being used and they both use an interrupt, the ISR must determine which
     * USCI asserted the interrupt.
     */
    #define LSPI_TX_VECTOR  USCIAB1TX_VECTOR
    #define LSPI_RX_VECTOR  USCIAB1RX_VECTOR
    /*
     * Default I/O port pins & macros to form names of port registers.
     */
    #ifndef LSPI_SIMO_BIT
      #define LSPI_SIMO_BIT       BIT6
      #define LSPI_SIMO_P(suffix) P3##suffix
      #define LSPI_SOMI_BIT       BIT7
      #define LSPI_SOMI_P(suffix) P3##suffix
      #define LSPI_CLK_BIT        BIT0
      #define LSPI_CLK_P(suffix)  P5##suffix
    #endif
    
  #elif LSPI_USCI == LSPI_USCI_B0
  
    #define LSPI_CTL0       UCB0CTL0
    #define LSPI_CTL1       UCB0CTL1
    #define LSPI_BR0        UCB0BR0
    #define LSPI_BR1        UCB0BR1
    #define LSPI_STAT       UCB0STAT
    #define LSPI_TX_BUF     UCB0TXBUF
    #define LSPI_RX_BUF     UCB0RXBUF
    /* interrupt enable register & bits */
    #define LSPI_IE         UC0IE
    #define LSPI_TX_IE      UCB0TXIE
    #define LSPI_RX_IE      UCB0RXIE
    /* interrupt flag register & bits */
    #define LSPI_IFG        UC0IFG
    #define LSPI_TX_IFG     UCB0TXIFG
    #define LSPI_RX_IFG     UCB0RXIFG
    /*
     * Macro to clear modulation control register (does nothing since USCI
     * B0 doesn't have a modulation control register).
     */
    #define LSPI_CLEAR_MCTL()
    /*
     * Macros to declare ISR's for interrupts. Note if USCI A0 and B0 are both
     * being used and they both use an interrupt, the ISR must determine which
     * USCI asserted the interrupt.
     */
    #define LSPI_TX_VECTOR  USCIAB0TX_VECTOR
    #define LSPI_RX_VECTOR  USCIAB0RX_VECTOR
    /*
     * Default I/O port pins & macros to form names of port registers.
     */
    #ifndef LSPI_SIMO_BIT
      #define LSPI_SIMO_BIT       BIT1
      #define LSPI_SIMO_P(suffix) P3##suffix
      #define LSPI_SOMI_BIT       BIT2
      #define LSPI_SOMI_P(suffix) P3##suffix
      #define LSPI_CLK_BIT        BIT3
      #define LSPI_CLK_P(suffix)  P3##suffix
    #endif
    
  #elif LSPI_USCI == LSPI_USCI_B1
  
    #define LSPI_CTL0       UCB1CTL0
    #define LSPI_CTL1       UCB1CTL1
    #define LSPI_BR0        UCB1BR0
    #define LSPI_BR1        UCB1BR1
    #define LSPI_STAT       UCB1STAT
    #define LSPI_TX_BUF     UCB1TXBUF
    #define LSPI_RX_BUF     UCB1RXBUF
    /* interrupt enable register & bits */
    #define LSPI_IE         UC1IE
    #define LSPI_TX_IE      UCB1TXIE
    #define LSPI_RX_IE      UCB1RXIE
    /* interrupt flag register & bits */
    #define LSPI_IFG        UC1IFG
    #define LSPI_TX_IFG     UCB1TXIFG
    #define LSPI_RX_IFG     UCB1RXIFG
    /*
     * Macro to clear modulation control register (does nothing since USCI
     * B1 doesn't have a modulation control register).
     */
    #define LSPI_CLEAR_MCTL()
    /*
     * Macros to declare ISR's for interrupts. Note if USCI A1 and B1 are both
     * being used and they both use an interrupt, the ISR must determine which
     * USCI asserted the interrupt.
     */
    #define LSPI_TX_VECTOR  USCIAB1TX_VECTOR
    #define LSPI_RX_VECTOR  USCIAB1RX_VECTOR
    /*
     * Default I/O port pins & macros to form names of port registers.
     */
    #ifndef LSPI_SIMO_BIT
      #define LSPI_SIMO_BIT       BIT1
      #define LSPI_SIMO_P(suffix) P5##suffix
      #define LSPI_SOMI_BIT       BIT2
      #define LSPI_SOMI_P(suffix) P5##suffix
      #define LSPI_CLK_BIT        BIT3
      #define LSPI_CLK_P(suffix)  P5##suffix
    #endif
    
  #endif
  
  /* settings for LSPI_CLK_SRC (clock source = LSPI_CTL1.UCSELx bits) */
  #define LSPI_ACLK   UCSSEL_1
  #define LSPI_SMCLK  UCSSEL_2
  
  /* non-zero if local SPI interface is in master mode */
  #define LSPI_MASTER  RD_BIT(LSPI_CTL0, UCMST)
  /* non-zero when serial interface is busy (transmitting or receiving) */
  #define LSPI_BUSY  RD_BIT(LSPI_STAT, UCBUSY)
  /* non-zero when TX buf is empty */
  #define LSPI_TX_EMPTY  RD_BIT(LSPI_IFG, LSPI_TX_IFG)
  /* non-zero when TX buf and shift register are empty (done transmitting) */
  #define LSPI_TX_DONE  LSPI_BUSY
  /* non-zero when RX buf is full (received byte) */
  #define LSPI_RX_FULL  RD_BIT(LSPI_IFG, LSPI_RX_IFG)
  
  /* macros to change TX & RX interrupt enables */
  #define LSPI_WR_TX_INT_ENAB(val)  WR_BIT(LSPI_IE, LSPI_TX_IE, val)
  #define LSPI_WR_RX_INT_ENAB(val)  WR_BIT(LSPI_IE, LSPI_RX_IE, val)
  
  /* Defines to OR together to form the "specs" argument for LSpiInit()
   * and LSpiConfig(). Note the specs are applied to LSPI_CTL0.
   */
  #define LSPI_SPEC_INACTIVE_LOW_WITH_SAMP_ON_FALLING_EDGE   (0)
  #define LSPI_SPEC_INACTIVE_LOW_WITH_SAMP_ON_RISING_EDGE    (UCCKPH)
  #define LSPI_SPEC_INACTIVE_HIGH_WITH_SAMP_ON_FALLING_EDGE  (UCCKPL | UCCKPH)
  #define LSPI_SPEC_INACTIVE_HIGH_WITH_SAMP_ON_RISING_EDGE   (UCCKPL)
  /* master or slave mode */
  #define LSPI_SPEC_MASTER  UCMST
  #define LSPI_SPEC_SLAVE   0
  /* 7 bit or 8 bit (character length) */
  #define LSPI_SPEC_7_BIT  UC7BIT
  #define LSPI_SPEC_8_BIT  0
  /* MSB or LSB first */
  #define LSPI_SPEC_MSB_FIRST  UCMSB
  #define LSPI_SPEC_LSB_FIRST  0
  
#endif /* #ifdef __msp430x26x */

/* If LSPI_CLK_SRC isn't defined through "Adp/Build/Build.h", use default.
 * Note the clock source only affects master mode (in slave mode, it always
 * uses the external UCLK from the master).
  */
#ifndef LSPI_CLK_SRC
  #define LSPI_CLK_SRC  LSPI_ACLK
#endif
/*
 * Frequency of the specified clock source.
 */
#if LSPI_CLK_SRC == LSPI_ACLK
  #define LSPI_CLK_FREQ  ACLK_FREQ
#elif LSPI_CLK_SRC == LSPI_SMCLK
  #define LSPI_CLK_FREQ  SMCLK_FREQ
#endif

/* non-zero if SIMO is high (slave in, master out pin) */
#define LSPI_SIMO  RD_BIT(LSPI_SIMO_P(IN), LSPI_SIMO_BIT)

/* non-zero if SOMI is high (slave out, master in pin) */
#define LSPI_SOMI  RD_BIT(LSPI_SOMI_P(IN), LSPI_SOMI_BIT)

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global Declarations
 */

/**************************************************************************
 * External Function Prototypes
 */
 
#endif /* ensure this file is only included once */
