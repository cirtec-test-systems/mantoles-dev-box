/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Standard/StdIo.h"          /* snprintf(), ... */
#include "Standard/String.h"         /* strlen(), ... */
#include "Adp/General.h"             /* UINT, ... */
#include "Adp/Build/Build.h"         /* UD16, ... */
#include "Adp/AnyBoard/SysTimerHw.h" /* ST_COUNT_PER_SEC, ... */
#include "Adp/AnyBoard/TraceLib.h"   /* public include for trace lib */

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/* private data for trace library */
#if TRACE_ENAB
TRACE_PRIV TracePriv = {&TracePriv.entry[0], &TracePriv.entry[0]};
#endif
 
/**************************************************************************
 * Function Prototypes
 */
 
const char *
TraceGetNext(void)
{
#if !TRACE_ENAB
    return(NULL);
#else
    UINT n, bufSizeMinus1;
    TRACE_ENTRY *te;
    char *retVal;
    UINT prevGie;
    
    /* Save previous GIE setting (interrupt enable), then disable all
     * interrupts so no ISR will call a trace function.
     */
    prevGie = _get_SR_register() & GIE;
    _disable_interrupts();
    /*
     * If there are no more entries in trace buffer.
     */
    if (TracePriv.next == TracePriv.unused) {
        
        /* If we're at the end of the trace buffer (dummy entry that isn't
         * included in the trace), trace messages may have been lost because
         * the buffer was full, so set return message to tell user.
         */
        if (TracePriv.next == &TracePriv.entry[TRACE_SIZE - 1]) {
            retVal = "\r\nTrace buffer may have overflowed.\r\n";
        } else {
            retVal = NULL;
        }
        
        /* reset trace buffer pointers */
        TracePriv.next = TracePriv.unused = &TracePriv.entry[0];
        
        /* restore previous GIE setting (interrupt enable) */
        _bis_SR_register(prevGie);
    
        /* return either the overflow message or NULL (see above) */
        return(retVal);
    }
    /* restore previous GIE setting (interrupt enable) */
    _bis_SR_register(prevGie);
    
    /* get pointer to next entry, then increment it */
    te = TracePriv.next++;
    
#if TRACE_TIME    
    /* if using microsecond time */
    if (TRACE_TIME == TRACE_US) {
        
        /* Convert system timer count to microseconds and add it to the
         * trace message. Note the most significant 16 bits of the resulting
         * miroseconds is dropped, so the microseconds shown in the message
         * will always wrap every 65535 microseconds.
         */
        (void)sprintf(TracePriv.msgBuf, "Us %05u: ",
            (UD16)StCountToUs(te->stTime));
            
    } else {
        
        /* add millisecond time to the trace message */
        (void)sprintf(TracePriv.msgBuf, "Ms %05u: ", te->stTime);
    }
#else    
    TracePriv.msgBuf[0] = '\0';
#endif

    /* Build the rest of the trace message (insert any arguments) and make sure
     * it's terminated. Note the buffer size minus 1 is passed to snprintf()
     * instead of the full buffer size, because if the resulting string exceeds
     * the passed size, snprintf() will write a terminating '\0' at that index
     * in the buffer. For example, if the buffer size is 16, and 16 is passed,
     * snprintf() would write '\0' at buf[16], which would be beyond the end
     * of the buffer, so it would corrupt memory.
     */
    n = strlen(TracePriv.msgBuf);
    bufSizeMinus1 = sizeof(TracePriv.msgBuf) - 1;
    (void)snprintf(&TracePriv.msgBuf[n], bufSizeMinus1 - n,
        te->msg, te->arg1, te->arg2);
    TracePriv.msgBuf[bufSizeMinus1] = '\0';
    
    return(TracePriv.msgBuf);
    
#endif
}
