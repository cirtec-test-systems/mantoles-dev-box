/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Adp/General.h"            /* RSTAT, TRUE, ... */
#include "Adp/Build/Build.h"        /* __msp430x16x, UD8, ASPI_SIMO_BIT, ... */
#include "Adp/AnyBoard/AdpSpiHw.h"  /* ASPI_CTL, ASPI_MASTER, ... */
#include "Adp/AnyBoard/AdpSpiLib.h" /* ASpiInit(), ... */

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/**************************************************************************
 * Function Prototypes
 */
 
__inline void ASpiSetMasterSi(void);
__inline void ASpiSetSlaveSi(void);
 
/**************************************************************************
 * Initialize the ADP SPI interface.
 */
RSTAT
ASpiInit(void)
{
    /* Configure the ASPI_SLAVE_READY_BIT I/O pin so its interrupt flag will be
     * set (latched) on the falling edge of the input. For more information,
     * see ASPI_SLAVE_READY in "Adp/AnyBoard/AdpSpiHw.h".
     */
    ASPI_SLAVE_READY_P(IES) |= ASPI_SLAVE_READY_BIT;
    
    /* config ADP SPI interface for slave mode */
    ASpiSetSlave();
    
    /* Select SPI functionality for the I/O pins used by the ADP SPI interface
     * (SIMO and UCLK). Note we don't select SOMI because it isn't used, and if
     * we selected it, then when the ADP board and mezzanine board are both in
     * slave mode, they would both try to drive it, causing contention. Note
     * when the SPI functionality is selected for an I/O pin, the MSP430
     * automatically controls the pin's direction, so there's no need to set
     * the direction explicitly (see the I/O pin schematics in the data sheet
     * for the MSP430 being used).
     */
    ASPI_SIMO_P(SEL) |= ASPI_SIMO_BIT;
    ASPI_CLK_P(SEL) |= ASPI_CLK_BIT;
    
    return(0);
}

void
ASpiSetSlave(void)
{
    /* config serial interface in micro-controller for SPI slave mode */
    ASpiSetSlaveSi();
    
    /* enable SPI RX interrupt */
    ASPI_WR_RX_INT_ENAB(TRUE);
}

void
ASpiSetMaster(void)
{
    /* config serial interface in micro-controller for SPI master mode */
    ASpiSetMasterSi();
    
    /* Set ASPI_SLAVE_READY to 1 so the first byte of the packet will be
     * transmitted without waiting. For more information, see ASPI_SLAVE_READY
     * in "Adp/AnyBoard/AdpSpiHw.h".
     */
    ASPI_WR_SLAVE_READY(1);
}

/* Functions for MSP430x16x micro-controllers.
 */
#ifdef __msp430x16x
__inline void
ASpiSetMasterSi(void)
{
    /* Set ASPI_CTL.SWRST to hold the USART in reset (also disables USART RX &
     * TX interrupts). The MSP430 manual says to ensure proper operation, this
     * must be done before initializing or re-configuring the USART for SPI
     * mode, including ASPI_CTL.
     */
    ASPI_CTL |= SWRST;
    
    /* Set USART control:
     * 
     * - Character length = 8 bits (CHAR=1).
     * - SPI mode (SYNC=1).
     * - Master mode (MM=1).
     * - Listen/loopback disabled (LISTEN=0).
     * - Hold USART in reset (SWRST=1). Also disables USART RX & TX interrupts.
     */
    ASPI_CTL = CHAR | SYNC | MM | SWRST;
    
    /* Set USART TX control. The UCLK is configured to be inactive high with
     * sample on rising edge. UCLK is inactive high (CKPL=1) because the level
     * shifters on the ADP board have internal pull-ups, so UCLK will be high
     * by default (before the SPI interface is initialized). It's best to match
     * the default UCLK, and driving UCLK low when inactive would waste power.
     * 
     * - UCLK phase = normal (CKPH=0).
     * - UCLK polarity = inactive high (CKPL=1).
     * - Disable STE (STC=1).
     * - Clock source for BRCLK = ASPI_CLK_SRC.
     */
    ASPI_TCTL = STC | CKPL | ASPI_CLK_SRC;
    
    /* set USART baud rate control (UCLK = BRCLK / ASPI_CLK_DIV) */
    ASPI_BR0 = (UD8)(ASPI_CLK_DIV);
    ASPI_BR1 = (UD8)(ASPI_CLK_DIV >> 8);
    
    /* clear modulation control (not used for SPI, but should be set to 0) */
    ASPI_MCTL = 0;

    /* enable SPI mode for USART */
    ASPI_ME |= ASPI_SPIE;
    
    /* release USART from reset */
    ASPI_CTL &= ~SWRST;
}

__inline void
ASpiSetSlaveSi(void)
{
    /* If transmission is active and we're in master mode, wait for
     * transmission to finish before switching modes. Note we check for master
     * mode in case ASPI_TX_DONE (UxTCTL.TXEPT) is always 0 in slave mode,
     * since the MSP430 manual isn't clear about this. Note when the MSP430
     * is reset, ASpiInit() calls this function to initialize the SPI interface.
     * This is ok because when the MSP430 is reset, it sets ASPI_TX_DONE and
     * clears ASPI_MASTER (UxCTL.MM).
     */
    while(!ASPI_TX_DONE && ASPI_MASTER);
    
    /* Set ASPI_CTL.SWRST to hold the USART in reset (also disables USART RX &
     * TX interrupts). The MSP430 manual says to ensure proper operation, this
     * must be done before initializing or re-configuring the USART for SPI
     * mode, including ASPI_CTL.
     */
    ASPI_CTL |= SWRST;
    
    /* Set USART control:
     * 
     * - Character length = 8 bits (CHAR=1).
     * - SPI mode (SYNC=1).
     * - Slave mode (MM=0).
     * - Listen/loopback disabled (LISTEN=0).
     * - Hold USART in reset (SWRST=1). Also disables USART RX & TX interrupts.
     */
    ASPI_CTL = CHAR | SYNC | SWRST;
    
    /* Set USART TX control. The UCLK is configured to be inactive high with
     * sample on rising edge. UCLK is inactive high (CKPL=1) because the level
     * shifters on the ADP board have internal pull-ups, so UCLK will be high
     * by default (before the SPI interface is initialized). It's best to match
     * the default UCLK, and driving UCLK low when inactive would waste power.
     * 
     * - UCLK phase = normal (CKPH=0).
     * - UCLK polarity = inactive high (CKPL=1).
     * - Disable STE (STC=1).
     * - SSEL1 & SSEL0 are ignored (UCLK is always external in slave mode).
     */
    ASPI_TCTL = STC | CKPL;
    
    /* clear modulation control (not used for SPI, but should be set to 0) */
    ASPI_MCTL = 0;
    
    /* enable SPI mode for USART */
    ASPI_ME |= ASPI_SPIE;
    
    /* release USART from reset */
    ASPI_CTL &= ~SWRST;
}
#endif /* #ifdef __msp430x16x */

/* Functions for MSP430x26x micro-controllers.
 */
#ifdef __msp430x26x
__inline void
ASpiSetMasterSi(void)
{
    /* Set ASPI_CTL1.UCSWRST to hold the USCI in reset (also disables USCI RX &
     * TX interrupts). The MSP430 manual says to ensure proper operation, this
     * must be done before initializing or re-configuring the USCI for SPI
     * mode, including ASPI_CTL1.
     */
    ASPI_CTL1 |= UCSWRST;
    
    /* Set USCI control 0. The SPI clock (UCLK) is configured to be inactive
     * high with sample on rising edge. The clock is inactive high (UCCKPL=1)
     * because the level shifters on the ADP board have internal pull-ups, so
     * the clock will be high by default (before the SPI interface is
     * initialized). It's best to match the default clock, and driving UCLK
     * low when inactive would waste power.
     * 
     * - Clock phase = normal (UCCKPH=0).
     * - Clock polarity = inactive high (UCCKPL=1).
     * - Bit order = MSB first (UCMSB=1).
     * - Character length = 8 (UC7BIT=0).
     * - SPI master mode (UCMST=1).
     * - 3-pin SPI mode (UCMODEx=0).
     * - Synchronous mode (UCSYNC=1).
     */
    ASPI_CTL0 = UCCKPL | UCMSB | UCMST | UCSYNC;
    
    /* Set USCI control 1:
     * 
     * - Clock source for BRCLK = ASPI_CLK_SRC.
     * - Hold USCI in reset (UCSWRST=1). Also disables USCI RX & TX interrupts.
     */
    ASPI_CTL1 = ASPI_CLK_SRC | UCSWRST;
    
    /* set USCI bit rate control (UCLK = BRCLK / ASPI_CLK_DIV) */
    ASPI_BR0 = (UD8)(ASPI_CLK_DIV);
    ASPI_BR1 = (UD8)(ASPI_CLK_DIV >> 8);
    
    /* clear modulation control (not used for SPI, but should be set to 0) */
    ASPI_CLEAR_MCTL();
    
    /* release USCI from reset */
    ASPI_CTL1 &= ~UCSWRST;
}

__inline void
ASpiSetSlaveSi(void)
{
    /* Wait for the USCI to finish any current operation before switching to
     * slave mode (to ensure any current transmission is completed).
     */
    while(ASPI_BUSY);
    
    /* Set ASPI_CTL1.UCSWRST to hold the USCI in reset (also disables USCI RX &
     * TX interrupts). The MSP430 manual says to ensure proper operation, this
     * must be done before initializing or re-configuring the USCI for SPI
     * mode, including ASPI_CTL1.
     */
    ASPI_CTL1 |= UCSWRST;
    
    /* Set USCI control 0. The SPI clock (UCLK) is configured to be inactive
     * high with sample on rising edge. The clock is inactive high (UCCKPL=1)
     * because the level shifters on the ADP board have internal pull-ups, so
     * the clock will be high by default (before the SPI interface is
     * initialized). It's best to match the default clock, and driving UCLK
     * low when inactive would waste power.
     * 
     * - Clock phase = normal (UCCKPH=0).
     * - Clock polarity = inactive high (UCCKPL=1).
     * - Bit order = MSB first (UCMSB=1).
     * - Character length = 8 (UC7BIT=0).
     * - SPI slave mode (UCMST=0).
     * - 3-pin SPI mode (UCMODEx=0).
     * - Synchronous mode (UCSYNC=1).
     */
    ASPI_CTL0 = UCCKPL | UCMSB | UCSYNC;
    
    /* clear modulation control (not used for SPI, but should be set to 0) */
    ASPI_CLEAR_MCTL();
    
    /* release USCI from reset */
    ASPI_CTL1 &= ~UCSWRST;
}
#endif /* #ifdef __msp430x26x */
