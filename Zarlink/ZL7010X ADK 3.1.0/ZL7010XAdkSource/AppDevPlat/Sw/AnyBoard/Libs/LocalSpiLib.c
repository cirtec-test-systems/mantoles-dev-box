/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Adp/General.h"              /* UINT, RSTAT */
#include "Adp/Build/Build.h"          /* __msp430x16x, UD8, LSPI_SIMO_BIT, ... */
#include "Adp/AnyBoard/LocalSpiHw.h"  /* LSPI_CTL, ... */
#include "Adp/AnyBoard/LocalSpiLib.h" /* LSpiInit(), ... */

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/**************************************************************************
 * Function Prototypes
 */
 
__inline void LSpiConfigSi(UINT specs, UINT clockDiv);
 
/**************************************************************************
 * Initialize the local SPI interface.
 */
RSTAT
LSpiInit(UINT specs, UINT clockDiv)
{
    /* config SPI interface */
    LSpiConfig(specs, clockDiv);
    
    /* Select SPI functionality for the I/O pins used by the local SPI
     * interface (SIMO, SOMI, and UCLK). Note when the SPI functionality is
     * selected for an I/O pin, the MSP430 automatically controls the pin's
     * direction, so there's no need to set the direction explicitly (see the
     * I/O pin schematics in the data sheet for the MSP430 being used).
     */
    LSPI_SIMO_P(SEL) |= LSPI_SIMO_BIT;
    LSPI_SOMI_P(SEL) |= LSPI_SOMI_BIT;
    LSPI_CLK_P(SEL)  |= LSPI_CLK_BIT;
    
    return(0);
}

void
LSpiConfig(UINT specs, UINT clockDiv)
{
    /* config serial interface in micro-controller for SPI master mode */
    LSpiConfigSi(specs, clockDiv);
}

/* Functions for MSP430x16x micro-controllers.
 */
#ifdef __msp430x16x
__inline void
LSpiConfigSi(UINT specs, UINT clockDiv)
{
    /* Set LSPI_CTL.SWRST to hold the USART in reset (also disables USART RX &
     * TX interrupts). The MSP430 manual says to ensure proper operation, SWRST
     * must be set before initializing or re-configuring the USART for SPI mode
     * (including setting LSPI_CTL), and if reconfiguring from I2C mode,
     * LSPI_CTL must first be cleared (in case USART is used in both modes
     * for some reason).
     */
    LSPI_CTL = 0;
    LSPI_CTL = SWRST;
    
    /* Set USART control. Note the specs.CKPH and CKPL settings (clock phase
     * and polarity) don't apply to LSPI_CTL, but the corresponding bits in
     * LSPI_CTL aren't used, so they won't affect anything. Also, the MSP430x16x
     * only supports MSB first (LSPI_SPEC_MSB_FIRST), so LSPI_SPEC_LSB_FIRST
     * isn't defined and will cause a compile error if specified.
     * 
     * - Master mode = specs (LSPI_SPEC_MASTER, ...).
     * - Character length = specs (LSPI_SPEC_8_BIT, ...).
     * - SPI mode (SYNC=1).
     * - Listen/loopback disabled (LISTEN=0).
     * - Hold in reset (SWRST=1). Also disables USART RX & TX interrupts.
     */
    LSPI_CTL = SYNC | SWRST | specs;
    
    /* Set USART TX control:
     * 
     * - UCLK phase = specs (LSPI_INACTIVE_LOW_WITH_SAMP_ON_RISING_EDGE, ...).
     * - UCLK polarity = specs (LSPI_INACTIVE_LOW_WITH_SAMP_ON_RISING_EDGE, ...).
     * - Clock source for BRCLK = LSPI_CLK_SRC (only applies in master mode).
     * - Disable STE (STC=1).
     */
    LSPI_TCTL = STC | LSPI_CLK_SRC | (specs & (CKPH | CKPL));
    
    /* set baud rate (UCLK = BRCLK / clockDiv; only applies in master mode) */
    LSPI_BR0 = (UD8)clockDiv;
    LSPI_BR1 = 0;
    
    /* clear modulation control (not used for SPI, but should be set to 0) */
    LSPI_MCTL = 0;

    /* enable SPI mode for USART */
    LSPI_ME |= LSPI_SPIE;
    
    /* release USART from reset */
    LSPI_CTL &= ~SWRST;
}
#endif /* #ifdef __msp430x16x */

/* Functions for MSP430x26x micro-controllers.
 */
#ifdef __msp430x26x
__inline void
LSpiConfigSi(UINT specs, UINT clockDiv)
{
    /* Set LSPI_CTL1.UCSWRST to hold the USCI in reset (also disables USCI RX &
     * TX interrupts). The MSP430 manual says to ensure proper operation, this
     * must be done before initializing or re-configuring the USCI for SPI
     * mode, including LSPI_CTL1.
     */
    LSPI_CTL1 |= UCSWRST;
    
    /* Set USCI control 0:
     * 
     * - Master/slave mode = specs (LSPI_SPEC_MASTER, ...).
     * - Character length = specs (LSPI_SPEC_8_BIT, ...).
     * - Bit order = specs (LSPI_SPEC_MSB_FIRST, ...).
     * - UCLK phase = specs (LSPI_INACTIVE_LOW_WITH_SAMP_ON_RISING_EDGE, ...).
     * - UCLK polarity = specs (LSPI_INACTIVE_LOW_WITH_SAMP_ON_RISING_EDGE, ...).
     * - Synchronous mode (UCSYNC=1).
     * - 3 pin SPI mode (UCMODEx=0).
     */
    LSPI_CTL0 = UCSYNC | specs;
    
    /* Set USCI control 1:
     * 
     * - Clock source for BRCLK = LSPI_CLK_SRC (only applies in master mode).
     * - Hold USCI in reset (UCSWRST=1). Also disables USCI RX & TX interrupts.
     */
    LSPI_CTL1 = LSPI_CLK_SRC | UCSWRST;
    
    /* set bit rate (UCLK = BRCLK / clockDiv; only applies in master mode) */
    LSPI_BR0 = (UD8)clockDiv;
    LSPI_BR1 = 0;
    
    /* clear modulation control (not used for SPI, but should be set to 0) */
    LSPI_CLEAR_MCTL();
    
    /* release USCI from reset */
    LSPI_CTL1 &= ~UCSWRST;
}
#endif /* #ifdef __msp430x26x */
