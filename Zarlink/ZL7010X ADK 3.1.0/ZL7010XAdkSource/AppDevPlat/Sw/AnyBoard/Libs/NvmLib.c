/**************************************************************************
 * This file contains functions for the nonvolatile-memory library in the
 * firmware. This library is used to manage the nonvolatile memory (NVM).
 *
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2016. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2016. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */

#include "Adp/General.h"              /* RSTAT, TRUE/FALSE, ... */
#include "Adp/Build/Build.h"          /* UD16, ... */
#include "Adp/AnyBoard/ErrLib.h"
#include "Adp/AnyBoard/NvmLib.h"

/**************************************************************************
 * Defines and Macros
 */

/**************************************************************************
 * Data Structures and Typedefs
 */

/**************************************************************************
 * Global and Static Definitions
 */

/* error group for the NVM library */
const char *NvmLibErr = NVM_LIB_ERR;

/**************************************************************************
 * Function Prototypes
 */
static void NvmErr(UD16 nvmLibErrCode, const char *argFormats, ...);

/**************************************************************************
 * Functions
 */

/* This initializes the NVM library.
 */
RSTAT
NvmInit(const NVM_INFO *ni, UD16 minNvmSize, UD16 reqNvmType)
{

    /* if NVM isn't initialized, fail */
    if (GET_UD32(&ni->nvmInitKey) != NVM_INIT_KEY) {

        NvmErr(NVM_LIB_ERR_NVM_IS_NOT_INIT, NULL);
        return(-1);
    }

    /* if NVM isn't the required type, fail */
    if (GET_UD16(&ni->nvmType) != reqNvmType) {

        NvmErr(NVM_LIB_ERR_NVM_IS_WRONG_TYPE, NULL);
        return(-1);
    }

    /* if NVM is an older (smaller) version, fail */
    if (GET_UD16(&ni->nvmSize) < minNvmSize) {

        NvmErr(NVM_LIB_ERR_NVM_IS_OLDER_VER, NULL);
        return(-1);
    }

    return(0);
}

static void
NvmErr(UD16 nvmLibErrCode, const char *argFormats, ...)
{
    va_list va;

    /* init variable arguments list */
    va_start(va, argFormats);

    ErrSetVa(NvmLibErr, nvmLibErrCode, argFormats, va);

    /* clean up variable args list */
    va_end(va);
}
