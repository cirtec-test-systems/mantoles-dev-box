/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Standard/String.h"         /* memset(), NULL, ... */
#include "Standard/StdArg.h"         /* va_list (ErrSet() args) */
#include "Standard/StdIo.h"          /* vsnprintf(), ... */
#include "Adp/General.h"             /* RSTAT, TRUE/FALSE, BOOL, ... */
#include "Adp/Build/Build.h"         /* UD8, UD16, ... */
#include "Adp/AnyBoard/ErrLib.h"     /* public include for ErrLib */

/**************************************************************************
 * Defines and Macros
 */

/**************************************************************************
 * Data Structures and Typedefs
 */
 
/* structure of private data for error library */
typedef struct {
    
    /* Various flags for error library. For bit definitions, see ERR_LOCK, etc.
     * This is declared volatile to prevent the compiler from assuming it won't
     * change during a section of code, in case it is changed by interrupts.
     */
    volatile UD8 flags;
    
    /* first error that occurred since ErrClearFirst() was called */
    ERR_INFO firstErr;
    
    /* last error that occurred (latest error) */
    ERR_INFO lastErr;
    
} ERR_PRIV;
/*
 * Defines for bits in ERR_PRIV.flags:
 * 
 * ERR_LOCK: when true, all error information is locked & won't change
 * if ErrSet() is called (see ErrLock()).
 */
#define ERR_LOCK  (1 << 0)
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/* private data for global error library */
static ERR_PRIV ErrPriv;

/* default error group used if no error group is specified */
static const char *ErrUnspecifiedErrGroup = "UnspecifiedErrGroup";

/**************************************************************************
 * Function Prototypes
 */
 
/**************************************************************************
 * Initialize the global error information library.
 */
RSTAT
ErrInit(void)
{
    ERR_PRIV *e = &ErrPriv;
    
    /* clear data and init any non-zero defaults */
    (void)memset(e, 0, sizeof(*e));
    e->firstErr.errGroup = ErrUnspecifiedErrGroup;
    e->lastErr.errGroup = ErrUnspecifiedErrGroup;
    
    return(0);
}

/**************************************************************************
 * Set global error information. The arguments are as follows:
 * 
 * errGroup:
 *     The error group (should never be NULL). This must point to a constant
 *     global string. Each error group must have one constnt global string
 *     associated with it that all of the micro-controller software references
 *     to identify that group.
 * errCode:
 *     The error code within the specified error group. Note each error
 *     group defines its own set of error codes starting at 1. Thus, the
 *     error code alone is not sufficient to uniquely identify an error.
 *     Rather, both the error group and error code are required.
 * argFormats:
 *     A string containing a conversion specification for each of the
 *     arguments to be inserted into the error message for the error, or
 *     NULL for none. This string should not contain any text except the
 *     conversion specifications, with each conversion specification
 *     separated from the others by a single '\f' character (form feed).
 *     For example, for a string argument and an integer argument,
 *     argFormats could be "%s\f%d". Internally, argFormats and the
 *     remaining arguments are passed to vsnprintf() to build a string
 *     containing the formatted arguments. The length of the resulting
 *     string and terminating '\0' should not exceed ERR_ARGS_BUF_SIZE.
 *     If it does, it won't corrupt memory or cause catastrophic problems,
 *     but the resulting arguments string will be truncated. For guidance
 *     regarding the number of arguments and how long they can be, see
 *     the define for ERR_ARGS_BUF_SIZE.
 * ...:
 *     The arguments to be inserted into the error message. Each
 *     argument is converted according to the conversion specification
 *     specified for the argument in "argFormats". Note that the correct
 *     type of argument must be passed for each conversion
 *     specification. Otherwise, the behavior is undefined.
 */
void
ErrSet(const char *errGroup, UD16 errCode, const char *argFormats, ...)
{
    va_list va;

    /* init variable arguments list */
    va_start(va, argFormats);

    ErrSetVa(errGroup, errCode, argFormats, va);

    /* clean up variable args list */
    va_end(va);
}

/**************************************************************************
 * Set global error information. This is the same as ErrSet(), except it 
 * takes a va_list argument instead of a variable argument list. See ErrSet()
 * for details.
 */
void
ErrSetVa(const char *errGroup, UD16 errCode,
    const char *argFormats, va_list va)
{
    ERR_INFO *fe = &ErrPriv.firstErr;
    ERR_INFO *le = &ErrPriv.lastErr;
    int bufSizeMinus1;
    
    /* if error info is locked, return (nothing to do) */
    if (ErrPriv.flags & ERR_LOCK) return;
    
    /* if specified NULL error group, use default (shouldn't happen) */
    if (errGroup == NULL) errGroup = ErrUnspecifiedErrGroup;
    
    /* Save info for last error that occurred. If there are arguments to insert
     * into the error message, build the arguments string and make sure it's
     * terminated. Note the buffer size minus 1 is passed to vsnprintf()
     * instead of the full buffer size, because if the resulting string exceeds
     * the passed size, vsnprintf() will write a terminating '\0' at that index
     * in the buffer. For example, if the buffer size is 16, and 16 is passed,
     * vsnprintf() would write '\0' at buf[16], which would be beyond the end
     * of the buffer, so it would corrupt memory.
     */
    le->errCode = errCode;
    le->errGroup = errGroup;
    le->errArgs[0] = '\0';
    if (argFormats != NULL) {
        bufSizeMinus1 = sizeof(le->errArgs) - 1;
        (void)vsnprintf(le->errArgs, bufSizeMinus1, argFormats, va);
        le->errArgs[bufSizeMinus1] = '\0';
    }
    
    /* if first error since ErrClearFirst() was called, save first error */
    if (fe->errCode == 0) {
        fe->errCode = errCode;
        fe->errGroup = errGroup;
        (void)strcpy(fe->errArgs, le->errArgs);
    }
}

const ERR_INFO *
ErrGet(void)
{
    return(&ErrPriv.lastErr);
}

const ERR_INFO *
ErrGetFirst(void)
{
    return(&ErrPriv.firstErr);
}

BOOL
ErrIs(const char *errGroup, UD16 errCode)
{
    ERR_INFO *le = &ErrPriv.lastErr;
    
    /* if error code doesn't match, return false */
    if (errCode != le->errCode) return(FALSE);
    
    /* if error group doesn't match, return false */
    if (errGroup != le->errGroup) return(FALSE);

    return(TRUE);
}

BOOL
ErrIsFirst(const char *errGroup, UD16 errCode)
{
    ERR_INFO *fe = &ErrPriv.firstErr;
    
    /* if error code doesn't match, return false */
    if (errCode != fe->errCode) return(FALSE);
    
    /* if error group doesn't match, return false */
    if (errGroup != fe->errGroup) return(FALSE);

    return(TRUE);
}

void
ErrClearFirst(void)
{
    ERR_INFO *fe = &ErrPriv.firstErr;
    
    fe->errCode = 0; 
    fe->errGroup = ErrUnspecifiedErrGroup;
    fe->errArgs[0] = '\0';
}

void
ErrLock(BOOL lock)
{
    if (lock) {
        ErrPriv.flags |= ERR_LOCK;
    } else {
        ErrPriv.flags &= ~ERR_LOCK;
    }
}
