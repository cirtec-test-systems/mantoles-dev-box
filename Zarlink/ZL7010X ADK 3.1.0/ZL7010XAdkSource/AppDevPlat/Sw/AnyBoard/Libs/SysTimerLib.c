/**************************************************************************
 * Copyright Microsemi Semiconductor (U.S.) Inc. 2012. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2012. The
 * use of the copyright notice is intended to provide notice that Microsemi
 * Semiconductor Corp owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Microsemi
 * Semiconductor Corp; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Microsemi Semiconductor Corp. This work is provided on a right to use
 * basis subject to additional restrictions set out in the applicable license
 * or other agreement.
 */
 
#include "Standard/String.h"          /* memset() */
#include "Adp/General.h"              /* RSTAT, TRUE/FALSE, ... */
#include "Adp/Build/Build.h"          /* UD16, ... */
#include "Adp/AnyBoard/SysTimerHw.h"  /* ST_CONTROL, ST_MS_PER_INT, ... */
#include "Adp/AnyBoard/SysTimerLib.h" /* ST_PUB, ... */

/**************************************************************************
 * Defines and Macros
 */
 
/**************************************************************************
 * Data Structures and Typedefs
 */
 
/**************************************************************************
 * Global and Static Definitions
 */
 
/* public data for system timer library */
ST_PUB  StPub;
 
/**************************************************************************
 * Function Prototypes
 */
__interrupt void StCc0Isr(void);
 
/**************************************************************************
 * Initialize the system timer.
 */
RSTAT
StInit(void)
{
    /* clear public data and init any non-zero defaults */
    (void)memset(&StPub, 0, sizeof(StPub));
    
    /* ST_CC0_CONTROL:
     *
     * - Capture mode = no capture (CM_0).
     * - Capture/compare input = CCIS_0 (not used).
     * - Capture/compare mode = compare (no CAP).
     * - Enable capture/compare 0 interrupt (CCIE).
     */
    ST_CC0_CONTROL = CM_0 | CCIS_0 | CCIE;
    
    /* ST_CC0_COUNT:
     *     The timer count at which the next interrupt will occur to update
     *     the millisecond time (StPub.msTime). See StCc0Isr().
     * ST_CONTROL:
     *   - Clock source = ST_CLK_SRC (defined in SysTimerHw.h).
     *   - Clock input divider = ST_CLK_DIV (defined in SysTimerHw.h).
     *   - Mode control = MC_2 (count to 0xFFFF then cycle back to 0).
     */
    ST_CC0_COUNT = ST_COUNT_PER_INT;
    ST_CONTROL = ST_CLK_SRC | ST_CLK_DIV | MC_2;
        
    return(0);
}

/* This gets the number of microseconds corresponding to the specified number
 * of system timer counts. The returned value is 32 bits to ensure it can hold
 * the number of microseconds corresponding to the maximum count (in case the
 * number of microseconds is > the count).
 */
UD32
StCountToUs(UD16 count)
{
    UD32 us;
    
    /* Calculate the number of microseconds corresponding to the specified
     * number of system timer counts. If ST_COUNT_PER_SEC is a multiple of
     * 1000000, use 16 bit math for efficiency. Otherwise, use 32 bit math
     * for better accuracy. 
     */
    if ((ST_COUNT_PER_SEC % 1000000UL) == 0) {
        
        /* check ST_COUNT_PER_SEC >= 1000000 to prevent divide by 0 warning */
        us = count / ((ST_COUNT_PER_SEC >= 1000000UL) ?
            (UD16)(ST_COUNT_PER_SEC / 1000000UL) : 1);
        
    } else {
        
        /* divide by 16 to ensure resulting numerator will fit in 32 bits */
        us = (count * ((UD32)1000000UL/16)) / (ST_COUNT_PER_SEC/16);
    }
    return(us);
}

/* This is the ISR for the system timer interrupt. This ISR normally
 * takes 38 MCLK periods, which will take 9.5 us if MCLK is 4MHz. Thus, if
 * ST_MS_PER_INT = 1 (one interrupt every millisecond), StCc0Isr() will use
 * ~0.95 % of the CPU's time (9.5 us every 1000 us).
 */
#pragma vector=ST_CC0_VECTOR
__interrupt void
StCc0Isr(void)
{
    UD16 diff;
    
    /* update the millisecond time */
    *((UD16 *)&StPub.msTime) += ST_MS_PER_INT;
    
    /* Update the timer count at which the next timer interrupt should occur.
     * If the timer already passed that count, set the interrupt flag so the
     * interrupt will occur again right away. This will keep happening until
     * ST_CC0_COUNT catches up with the timer. That way, StPub.msTime will
     * catch up if the system timer interrupt is ever delayed too long (for
     * example, if interrupts are disabled longer than ST_MS_PER_INT). Note if
     * the system timer interrupt is ever delayed longer than one full timer
     * cycle (~0xFFFF / ST_COUNT_PER_SEC, or ~64 ms if ST_COUNT_PER_SEC = 1MHz),
     * this logic won't detect it, so StPub.msTime will miss that many timer
     * cycle's worth of time.
     */
    ST_CC0_COUNT += ST_COUNT_PER_INT;
    /**/
    diff = ST_CC0_COUNT - ST_COUNTER;
    /**/
    if ((diff == 0) || (diff > ST_COUNT_PER_INT)) {
        ST_CC0_CONTROL |= CCIFG;
    }
}
