BSM200_REV_B_SN_109 would not work (all LEDs blinked. nvm init error)
with ADK software version 3.1 -- this is how i got it to work:

1- used MSP fet 430 tool to read entire flash memory off of BSM300 s/n 66 (All Memory option)
2- programmed this flash file onto BSM200 s/n 109 (All Memory option)
3- programmed Bsm200.txt which should be the latest FW onto BSM200 (Main Memory Only option)

it works


Thoughts:  It looks like the new devices/firmware looks for some non-volatile storage.  If its not
set, it errors out.  This process copies the NV storage from a newer board version to an older one.
It is possible that at some point through development the old board had NV memory correctly programmed
at one point, but it was not being checked and got corrupted at some point.  before upgrading the other
'old' boards, i would try to simply update them first to test your luck.