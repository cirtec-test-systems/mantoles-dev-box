while True:
    loop_type = input("Enter the loop type (for/while) or exit (e): ")
    if loop_type == "for":
        # For loop examples
        print("For loop examples:")

        # Looping through a list
        print("Looping through a list:")
        my_list = [1, 2, 3, 4, 5]
        for item in my_list:
            print(item)

        # Looping through a string
        print("Looping through a string:")
        my_string = "Hello"
        for char in my_string:
            print(char)

        # Using the range function
        print("Using the range function:")
        for i in range(5):
            print(i)
    elif loop_type == "while":
        # While loop examples
        print("While loop examples:")

        # Basic while loop
        print("Basic while loop:")
        i = 0
        while i < 5:
            print(i)
            i += 1

        # Using a break statement
        print("Using a break statement:")
        i = 0
        while True:
            if i == 5:
                break
            print(i)
            i += 1

        # Using a continue statement
        print("Using a continue statement:")
        i = 0
        while i < 10:
            i += 1
            if i % 2 == 0:
                continue
            print(i)
    elif loop_type == "e":
        print("Exiting program")
        break
    else:
        print("Invalid loop type")
