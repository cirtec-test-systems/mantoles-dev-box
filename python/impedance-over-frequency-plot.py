import numpy as np
import matplotlib.pyplot as plt

# Define frequency range (1 Hz to 1 MHz)
frequency = np.logspace(0, 6, num=1000)

# Define inductance (in Henrys)
L = 1e-3  # 1 mH

# Calculate impedance of the inductor
impedance = 2 * np.pi * frequency * L

# Plot the graph
plt.figure(figsize=(10, 6))
plt.plot(frequency, impedance)
plt.xscale('log')
plt.yscale('log')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Impedance (Ohms)')
plt.title('Impedance over Frequency for an Inductor')
plt.grid(True, which='both', linestyle='--', linewidth=0.5)
plt.show()
