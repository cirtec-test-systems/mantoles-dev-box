from abc import ABC, abstractmethod

class Shape(ABC):
    @abstractmethod
    def area(self):
        pass

    @abstractmethod
    def perimeter(self):
        pass

class Rectangle(Shape):
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def area(self):
        return self.width * self.height

    def perimeter(self):
        return 2 * (self.width + self.height)

class Triangle(Shape):
    def __init__(self, base, height, sideA, sideB, sideC):
         self.base = base
         self.height = height
         self.sideA = sideA
         self.sideB = sideB
         self.sideC = sideC 
         
    def area(self):
         return (self.base * self.height / 2.0)  
    def perimeter(self):
        return (self.sideA + self.sideB + self.sideC)     

class Circle(Shape):
    def __init__(self, radius):
        self.radius = radius

    def area(self):
        return 3.14 * self.radius ** 2

    def perimeter(self):
        return 2 * 3.14 * self.radius

rect = Rectangle(10, 20)
print(f"Area of rectangle: {rect.area()}")
print(f"Perimeter of rectangle: {rect.perimeter()}")

circ = Circle(5)
print(f"Area of circle: {circ.area()}")
print(f"Perimeter of circle: {circ.perimeter()}")

tria = Triangle(10,20,12,20,13)
print(f"Area of triangle: {tria.area()}")
print(f"Perimeter of triangle: {tria.perimeter()}")
