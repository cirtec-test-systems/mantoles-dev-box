# -*- coding: utf-8 -*-
"""
Created on Fri May 17 08:39:12 2024

@author: jaems.flowers
"""

import os
import xml.etree.ElementTree as ET
import tkinter.filedialog
import tkinter as tk

#directory_path = r'C:\data\validation-data\IPG-PCBA'
#directory_path = r'C:\data\Station-5-PCBA-Integer'
directory_path = r'C:\data\Station1-PCBA-Cirtec'

def get_files():
  """Gets all files from the 'files' folder."""
  files = []
  if os.path.exists(directory_path):
    for filename in os.listdir(directory_path):
      if filename.endswith(".xml"):
        files.append(filename)
  return files

def update_list(listbox, files):
  """Updates the listbox with the provided files."""
  listbox.delete(0, tk.END)
  for filename in files:
    listbox.insert(tk.END, filename)
    


def select_all(listbox):
  """Selects all items in the listbox."""
  listbox.select_set(0, tk.END)

def convert_files(result_entry):
  """Converts selected XML files and prints serial numbers."""
  tag = "QiG Serial Number"
  result_entry.delete(0, tk.END)
  selected_files = listbox.curselection()
  if not selected_files:
    return
  for index in selected_files:
    filename = listbox.get(index)
    with open(f"{directory_path}/{filename}", "r") as f:
      tree = ET.parse(f)  
      root = tree.getroot()
      serial = ''
      for child in root:
          for resultset in child:
              for testgroup in resultset.findall("{*}TestGroup//{*}TestResult"):
                  if testgroup.attrib.get('name') == tag:
                      #print(testgroup.tag, testgroup.attrib)
                      result = testgroup.find("{*}TestData//{*}Datum//{*}Value")
                      serial = result.text
      if serial:
        print(f"{filename}: {serial}")
        text = filename + ' : ' + serial
        result_entry.insert(tk.END, text)

def to_text_file():
    file_path = tk.filedialog.asksaveasfile(defaultextension=".txt")
    if file_path:
        text = result_entry.get(0, tk.END)
        for row in text:
            row += '\n'
            file_path.write(row)

root = tk.Tk()
root.geometry("800x600")
root.title("XML Serial Number Extractor")

# Listbox frame
listbox_frame = tk.Frame(root)
listbox_frame.pack(fill="both", expand=True)

# Files listbox
files = get_files()
listbox = tk.Listbox(listbox_frame, selectmode="multiple")
update_list(listbox, files)
listbox.pack(side="left", fill="both", expand=True)

# Scrollbar
list_scrollbar = tk.Scrollbar(listbox_frame, orient="vertical", command=listbox.yview)
list_scrollbar.pack(side="right", fill="y")
listbox.config(yscrollcommand=list_scrollbar.set)

# Select all button
select_all_button = tk.Button(listbox_frame, text="Select All", command=lambda: select_all(listbox))
select_all_button.pack(pady=5)

# Listbox frame
result_frame = tk.Frame(root)
result_frame.pack(fill="both", expand=True)

# Result textbox
result_entry = tk.Listbox(result_frame, selectmode="multiple")
result_entry.pack(side="left", fill="both", expand=True)

# Convert button
convert_button = tk.Button(root, text="Convert", command=lambda: convert_files(result_entry))
convert_button.pack(pady=10)

# Convert button
convert_button = tk.Button(root, text="Export", command=to_text_file)
convert_button.pack(pady=10)

# Scrollbar
result_scrollbar = tk.Scrollbar(result_frame, orient="vertical", command=listbox.yview)
result_scrollbar.pack(side="right", fill="y")
result_entry.config(yscrollcommand=result_scrollbar.set)

root.mainloop()