import os
import xml.etree.ElementTree as ET

# Define the directory path
directory_path = r'C:\data\validation-data\IPG-PCBA'

# Initialize an empty list to store results
results = []

# Walk through the directory
for root, dirs, files in os.walk(directory_path):
    for filename in files:
        if filename.lower().endswith('.xml'):
            xml_file_path = os.path.join(root, filename)
            try:
                # Parse the XML file
                tree = ET.parse(xml_file_path)
                root = tree.getroot()

                # Find the "QiG Serial Number" element
                for elem in root.iter():
                    if elem.tag.endswith('}Value') and 'QiG Serial Number' in elem.text:
                        serial_number = elem.text.strip()
                        results.append((filename, serial_number))
                        break
            except ET.ParseError:
                print(f"Error parsing {xml_file_path}")

# Write results to results.txt
with open(os.path.join(directory_path, 'results.txt'), 'w') as result_file:
    for filename, serial_number in results:
        result_file.write(f"{filename}: {serial_number}\n")

print("Results written to results.txt")
