import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_csv('data.csv', delimiter=',')
x = data['x']
y = data['y']

plt.plot(x, y)
plt.xlabel('x-axis')
plt.ylabel('y-axis')
plt.show()
