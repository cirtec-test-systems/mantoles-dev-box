import math

# Constants
mu_0 = 4 * math.pi * 10**-7  # Permeability of free space (H/m)
copper_resistivity = 1.68 * 10**-8  # Resistivity of copper (Ohm meter)
silver_resistivity = 1.59 * 10**-8  # Resistivity of silver (Ohm meter)

# Coil parameters
wire_length_inches = 52.9
wire_thickness_inches = 0.003
number_of_turns = 36
silver_content_percentage = 47 / 100

# Convert inches to meters
wire_length_meters = wire_length_inches * 0.0254
wire_thickness_meters = wire_thickness_inches * 0.0254

# Cross-sectional area of the wire (circular cross-section)
wire_area = math.pi * (wire_thickness_meters / 2)**2

# Calculate the effective resistivity of the wire with 47% silver content
effective_resistivity = (silver_content_percentage * silver_resistivity) + ((1 - silver_content_percentage) * copper_resistivity)

# Calculate the resistance of the wire
resistance = effective_resistivity * wire_length_meters / wire_area

# Calculate the inductance of the coil
# Assuming a simple solenoid with length equal to wire length and cross-sectional area equal to wire area
inductance = (number_of_turns**2) * mu_0 * wire_area / wire_length_meters

# Function to calculate impedance at a given frequency
def calculate_impedance(frequency):
    omega = 2 * math.pi * frequency
    impedance_real = resistance
    impedance_imaginary = omega * inductance
    return complex(impedance_real, impedance_imaginary)

# Frequencies to calculate impedance for
frequencies = [100000, 250000]  # in Hz

# Calculate and print impedance for each frequency
for freq in frequencies:
    impedance = calculate_impedance(freq)
    print(f"Impedance at {freq} Hz: {impedance} Ohms")
