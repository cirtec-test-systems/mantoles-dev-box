import os
import csv
import datetime

def parse_files(directory):
    for filename in os.listdir(directory):
        if filename.endswith(".csv"):
            with open(os.path.join(directory, filename), 'r') as f:
                reader = csv.reader(f)
                for row in reader:
                    if "Fail" in row:
                        yield row

def main():
    directory = input("Enter the path to the folder: ")
    timestamp = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
    outputDirectory = directory + "results"
    output_filename = os.path.join(outputDirectory, f"parsed-data-{timestamp}.csv")

    with open(output_filename, 'w') as outfile:
        for line in parse_files(directory):
            outfile.write(','.join(line) + '\n')

    print(f"Output written to {output_filename}")

if __name__ == "__main__":
    main()