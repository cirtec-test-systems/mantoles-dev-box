Circleclass Shape:
    def __init__(self, name):
        self.name = name

    def area(self):
        pass

    def perimeter(self):
        pass

class Rectangle(Shape):
    def __init__(self, width, height):
        super().__init__("Rectangle")
        self.width = width
        self.height = height

    def area(self):
        return self.width * self.height

    def perimeter(self):
        return 2 * (self.width + self.height)

class Circle(Shape):
    def __init__(self, radius):
        super().__init__("Circle")
        self.radius = radius

    def area(self):
        return 3.14 * self.radius ** 2

    def perimeter(self):
        return 2 * 3.14 * self.radius

shapes = []

while True:
    shape_type = input("Enter the shape type (rectangle/circle) or exit (e): ")
    if shape_type == "rectangle":
        width = float(input("Enter the width: "))
        height = float(input("Enter the height: "))
        rect = Rectangle(width, height)
        shapes.append(rect)
        print(f"Shape: {rect.name}")
        print(f"Area: {rect.area()}")
        print(f"Perimeter: {rect.perimeter()}")
    elif shape_type == "circle":
        radius = float(input("Enter the radius: "))
        circ = Circle(radius)
        shapes.append(circ)
        print(f"Shape: {circ.name}")
        print(f"Area: {circ.area()}")
        print(f"Perimeter: {circ.perimeter()}")
    elif shape_type == "e":
        print("Exiting program")
        break
    else:
        print("Invalid shape type")

