import queue
import threading
import time

# Create a shared queue object
shared_queue = queue.Queue()

# Define a producer function
def producer():
    while True:
        shared_queue.put("data")
        time.sleep(1)

# Define a consumer function
def consumer():
    while True:
        data = shared_queue.get()
        print("Consumed:", data)
        shared_queue.task_done()

# Create and start the producer and consumer threads
producer_thread = threading.Thread(target=producer)
consumer_thread = threading.Thread(target=consumer)
producer_thread.start()
consumer_thread.start()

# Wait for the threads to finish (which they never will in this example)
producer_thread.join()
consumer_thread.join()
