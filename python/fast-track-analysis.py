import csv

# Initialize counters
c229_hours = 0.0
other_hours = 0.0

# Open the file
with open('C:\\debug\\Sophronis-fastrack.csv', 'r') as file:
    reader = csv.reader(file, delimiter='\t')
    for row in reader:
        # Check if the row contains the project name
        if "C229 SIMPLIFI IPG GEN 1" in row:
            # The date and hours are in the next field
            date_hours = row[row.index("C229 SIMPLIFI IPG GEN 1") + 1]
            # Split the field by comma and get the hours
            hours = float(date_hours.split(',')[1])
            # Add the hours to the project counter
            c229_hours += hours
        else:
            # If the row doesn't contain the project name, add the hours to the other counter
            other_hours += float(row[-1])

total_hours = c229_hours + other_hours

print(f"Out of {total_hours} total hours, {c229_hours} hours were for the C229 project.")
print(f"The rest of the hours ({other_hours}) were for other projects.")
