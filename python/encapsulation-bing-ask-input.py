class BankAccount:
    def __init__(self, balance):
        self.__balance = balance

    def deposit(self, amount):
        self.__balance += amount

    def withdraw(self, amount):
        if amount > self.__balance:
            print("Insufficient funds")
            return
        self.__balance -= amount

    def get_balance(self):
        return self.__balance

account = BankAccount(1000)

while True:
    action = input("Select one the Following: Do you want to deposit, withdraw, get balance on your account or exit program? (d/w/b/e): ")
    if action == "d":
        amount = float(input("Enter the amount to deposit: "))
        account.deposit(amount)
    elif action == "w":
        amount = float(input("Enter the amount to withdraw: "))
        account.withdraw(amount)
    elif action == "b":
        print(f"Current balance: {account.get_balance()}")
    elif action == "e":
        print("Exiting program")
        break
    else:
        print("Invalid action")

