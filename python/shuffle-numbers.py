import random

# Generate a shuffled list of numbers from 1 to 10
shuffled_numbers = random.sample(range(1, 11), 10)

# Join the numbers with newline characters
shuffled_numbers_newline = "\n".join(map(str, shuffled_numbers))

print(shuffled_numbers_newline)
