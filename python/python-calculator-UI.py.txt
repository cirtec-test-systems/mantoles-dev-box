red
from collections import deque
from threading import Thread, Condition
import time

queue = deque()
MAX_ITEMS = 5
condition = Condition()

def apple_routine():
    print("Executing apple routine")

def orange_routine():
    print("Executing orange routine")

def producer():
    items = ['apple', 'orange', 'apple', 'orange', 'apple']
    for item in items:
        with condition:
            while len(queue) == MAX_ITEMS:
                print("Queue is full. Producer is waiting.")
                condition.wait()
            queue.append(item)
            print(f"Producer added {item} to queue")
            condition.notify_all()
        time.sleep(1)

def consumer():
    while True:
        with condition:
            while not queue:
                print("Queue is empty. Consumer is waiting.")
                condition.wait()
            item = queue.popleft()
            if item == 'apple':
                apple_routine()
            elif item == 'orange':
                orange_routine()
            print(f"Consumer removed {item} from queue")
            condition.notify_all()
        time.sleep(1)

producer_thread = Thread(target=producer)
consumer_thread = Thread(target=consumer)

producer_thread.start()
consumer_thread.start()

producer_thread.join()
consumer_thread.join()
