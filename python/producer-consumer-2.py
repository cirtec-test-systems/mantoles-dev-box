import threading
import queue

# A shared queue between the producer and consumer
shared_queue = queue.Queue()

# A dictionary of jobs to execute
jobs = {
    1: lambda: print("Job 1 executed"),
    2: lambda: print("Job 2 executed"),
    3: lambda: print("Job 3 executed")
}

# A function that consumes items from the shared queue
def consumer():
    while True:
        # Prompt the user for a job number
        job_num = int(input("Enter a job number (1-3) to execute, or 0 to quit: "))
        if job_num == 0:
            # Exit the loop if the user enters 0
            break
        elif job_num not in jobs:
            # Prompt the user to enter a valid job number
            print("Invalid job number")
        else:
            # Execute the selected job
            jobs[job_num]()
            shared_queue.task_done()

# Create the consumer thread
consumer_thread = threading.Thread(target=consumer)

# Start the consumer thread
consumer_thread.start()

# Wait for the user to enter 0 to quit
consumer_thread.join()

# Signal the consumer thread to exit by putting a None value into the queue
shared_queue.put(None)
