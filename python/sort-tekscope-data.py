""""
import os
def get_files_by_timestamp(directory):
    files = []
    for filename in os.listdir(directory):
        if os.path.isfile(os.path.join(directory, filename)):
            timestamp = filename[-18:-4]
            files.append((filename, timestamp))
    return sorted(files, key=lambda x: x[1])

directory = 'Z:/TekSCope-Data/03-oct-2023/'
files = get_files_by_timestamp(directory)
print(files)

version 2 works 

import os
import re

def get_files_by_timestamp(directory):
    files = []
    for filename in os.listdir(directory):
        if os.path.isfile(os.path.join(directory, filename)):
            timestamp = re.search(r'\d+', filename).group()
            files.append((filename, timestamp))
    return sorted(files, key=lambda x: x[1])

def sort_files_by_extension(directory):
    files = []
    for filename in os.listdir(directory):
        if os.path.isfile(os.path.join(directory, filename)):
            files.append(filename)
    files.sort(key=lambda f: os.path.splitext(f)[1])
    return files

directory = 'Z:/TekSCope-Data/03-oct-2023/'
files = get_files_by_timestamp(directory)
with open('C:/data/timestamp.txt', 'w') as f:
    for file in files:
        f.write(file[0] + '\n')

import os
import re
from datetime import datetime
def extract_timestamp(file_path):
    Extracts timestamp from file name of type .txt in Z:/TekSCope-Data/03-oct-2023/ directory and writes it to a file on C:\data\ directory.
    # Extract timestamp from file name
    match = re.search(r'\d{17}', os.path.basename(file_path))
    timestamp = match.group(0)

    # Write timestamp to file on C:\data\ directory
    with open(r'C:\data\timestamp.txt', 'w') as f:
        f.write(timestamp)
    # Return only numbers in the timestamp
    return int(timestamp)

the following program works    

import os
import re

def extract_numbers(file_list):
    num_list = []
    for file in file_list:
        num = re.findall(r'\d+', file)
        if num:
            num_list.append(int(''.join(num)))
    return num_list

def main():
    dir_path = "Z:\\TekSCope-Data\\03-oct-2023\\"
    files = os.listdir(dir_path)
    numbers = extract_numbers(files)
    print(numbers)

if __name__ == "__main__":
    main()
"""

import os
import re

def extract_numbers(file_list):
    num_list = []
    for file in file_list:
        num = re.findall(r'\d+', file)
        if num:
            num_list.append(int(''.join(num)))
    return num_list

def main():
    #dir_path = "Z:\\TekSCope-Data\\03-oct-2023\\"
    dir_path = "C:\\Data\\03-oct-2023\\"
    output_file_path = "c:\\data\\data-file.txt"
    files = os.listdir(dir_path)
    numbers = extract_numbers(files)
    
    with open(output_file_path, 'w') as f:
        for number in numbers:
            f.write(str(number) + '\n')

if __name__ == "__main__":
    main()
