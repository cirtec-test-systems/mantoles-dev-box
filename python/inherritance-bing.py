class Animal:
    def __init__(self, name):
        self.name = name

    def speak(self):
        print(f"My name is {self.name} and I am an animal.")

class Dog(Animal):
    def __init__(self, name, breed):
        super().__init__(name)
        self.breed = breed

    def speak(self):
        print(f"My name is {self.name}, I am a {self.breed} and I bark.")

class Cat(Animal):
    def __init__(self, name, color):
        super().__init__(name)
        self.color = color

    def speak(self):
        print(f"My name is {self.name}, I am {self.color} and I meow.")
        
class Mouse(Animal):
    def __init__(self, name, sex ):
        super().__init__(name)
        self.sex = sex

    def speak(self):
        print(f"My name is {self.name}, I am {self.sex} and I squeek.")       
        

dog = Dog("Rufus", "Golden Retriever")
dog.speak()

cat = Cat("Whiskers", "black")
cat.speak()

mouse = Mouse("Mickey","male")
mouse.speak()
