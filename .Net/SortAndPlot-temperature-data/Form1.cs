﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace SortAndPlot
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            char[] separator = { ' ', '\t', ',' };
            string filePath = "";
            // Create an OpenFileDialog object
            OpenFileDialog openFileDialog = new OpenFileDialog();

            // Set the filter to only show CSV files
            openFileDialog.Filter = "CSV files (*.csv)|*.csv";

            // Show the dialog and check if the user selected a file
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                // Get the selected file path
                filePath = openFileDialog.FileName;

                // Read the contents of the file
                //string fileContents = File.ReadAllText(filePath);

            }

            // Read data from CSV file
            var data = File.ReadAllLines(filePath)
                .Select(line => line.Split(separator))
                .Select(parts => new { Channel = int.Parse(parts[0]), Temp = double.Parse(parts[1]) })
                .GroupBy(x => x.Channel)
                .ToDictionary(g => g.Key, g => g.Select(x => x.Temp).ToList());

            // Create chart
            var chart = new Chart();
            chart.Dock = DockStyle.Fill;
            this.Controls.Add(chart);

            // Add chart area
            var chartArea = new ChartArea();
            chart.ChartAreas.Add(chartArea);

            // Add series for each channel
            for (int channel = 101; channel <= 109; channel++)
            {
                if (channel == 106)
                {
                    //do nothing
                }
                else if (data.ContainsKey(channel))
                {
                    var series = new Series($"Channel {channel}");
                    series.ChartType = SeriesChartType.Line;
                    series.Points.DataBindY(data[channel]);
                    chart.Series.Add(series);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
