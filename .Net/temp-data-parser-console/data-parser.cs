using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace CSVParser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Read data from CSV file
            var data = File.ReadAllLines("data.csv")
                .Select(line => line.Split(','))
                .Select(parts => new { Channel = int.Parse(parts[0]), Temp = double.Parse(parts[1]) })
                .GroupBy(x => x.Channel)
                .ToDictionary(g => g.Key, g => g.Select(x => x.Temp).ToList());

            // Create chart
            var chart = new Chart();
            chart.Dock = DockStyle.Fill;
            this.Controls.Add(chart);

            // Add chart area
            var chartArea = new ChartArea();
            chart.ChartAreas.Add(chartArea);

            // Add series for each channel
            for (int channel = 101; channel <= 110; channel++)
            {
                if (data.ContainsKey(channel))
                {
                    var series = new Series($"Channel {channel}");
                    series.ChartType = SeriesChartType.Line;
                    series.Points.DataBindY(data[channel]);
                    chart.Series.Add(series);
                }
            }
        }
    }
}
