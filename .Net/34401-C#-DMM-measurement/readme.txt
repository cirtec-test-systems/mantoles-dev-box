34970A VISA and C# Measurement Example Program
This program sets the 34970A for a simple scan that makes both DCV and type J 
thermocouple temperature measurements. The program also checks the instrument and 
module identification to make sure there is communication between the 34970A and 
the computer, and the correct module is installed. The returned data is entered 
into a list box.

The program requires that VISA is installed in your computer. VISA comes with the
Agilent I/O Library.

Include the visa32.cs file. This file comes with VISA version M.01.01.041 or above. 


The program was developed in Microsoft� C# (C Sharp) .NET


Date: August 27, 2004
Version: 1.0.0.0
Language: Microsoft� C# (C Sharp)
Operating System: Windows� NT 4.0, 2000 Professional, XP
VISA Version: Use Agilent I/O Library version M.01.01.041 or above


Microsoft, Windows, NT, XP are registered trademarks of Microsoft Corporation


Copyright � 2004 Agilent Technologies Inc. All rights reserved.

You have a royalty-free right to use, modify, reproduce and distribute these example 
files (and/or any modified version) in any way you find useful, provided that you 
agree that Agilent has no warranty, obligations or liability for any Sample 
Application Files.

Agilent Technologies will not modify the program to provide added
functionality or construct procedures to meet your specific needs.

