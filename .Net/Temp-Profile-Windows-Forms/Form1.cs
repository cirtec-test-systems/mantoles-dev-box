﻿/*using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Temp_Profile_Windows_Forms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
*/

using System;
using System.Drawing;
using System.Windows.Forms;
using LiveCharts;
using LiveCharts.Wpf;

namespace TestProgram
{
    public partial class Form1 : Form
    {
        private Timer timer;
        private int testLength;
        private int currentIteration;
        private int totalIterations;
        private Random random;
        private ChartValues<int> chartValues;
        private object resultsListBox;

        public Form1()
        {
            InitializeComponent();
            timer = new Timer();
            timer.Tick += Timer_Tick;
            currentIteration = 0;
            random = new Random();
            chartValues = new ChartValues<int>();
            /*cartesianChart1.Series.Add(new LineSeries
            {
                Values = chartValues,
                PointGeometrySize = 15
            }); */
          

            intervalComboBox.Items.Add("2 hours");
            intervalComboBox.Items.Add("4 hours");
            intervalComboBox.Items.Add("6 hours");
            intervalComboBox.SelectedIndex = 0;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            currentIteration++;
            int progressValue = (int)(((double)currentIteration / totalIterations) * 100);
            progressBar1.Value = progressValue;
            if (progressValue < 33)
            {
                progressBar1.ForeColor = Color.Red;
            }
            else if (progressValue < 66)
            {
                progressBar1.ForeColor = Color.Yellow;
            }
            else
            {
                progressBar1.ForeColor = Color.Green;
            }
            int randomNumber = random.Next(30, 66);
            resultsListBox1.Items.Add(randomNumber);
            //resultsListBox.Items.Add(randomNumber);
            chartValues.Add(randomNumber);
            if (currentIteration >= totalIterations)
            {
                timer.Stop();
                MessageBox.Show("Test completed!");
            }
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            if (int.TryParse(textLengthTextBox.Text, out testLength))
            {
                if (testLength < 10)
                {
                    MessageBox.Show("Test length must be at least 10 seconds!");
                }
                else
                {
                    totalIterations = testLength / 10;
                    string selectedInterval = intervalComboBox.SelectedItem.ToString();
                    int intervalHours = int.Parse(selectedInterval.Split(' ')[0]);
                    timer.Interval = intervalHours * 60 * 60 * 1000; // convert hours to milliseconds
                    timer.Start();
                }
            }
            else
            {
                MessageBox.Show("Invalid test length!");
            }
        }

        private void abortButton_Click(object sender, EventArgs e)
        {
            timer.Stop();
            MessageBox.Show("Test aborted!");
        }

    }
}

