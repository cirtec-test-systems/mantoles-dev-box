﻿
namespace TestProgram
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.button1 = new System.Windows.Forms.Button();
            this.intervalComboBox = new System.Windows.Forms.ComboBox();
            this.resultsListBox1 = new System.Windows.Forms.ListBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.cartesianChart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.textLengthTextBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.cartesianChart1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 322);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(126, 68);
            this.button1.TabIndex = 0;
            this.button1.Text = "Start";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // intervalComboBox
            // 
            this.intervalComboBox.FormattingEnabled = true;
            this.intervalComboBox.Location = new System.Drawing.Point(12, 50);
            this.intervalComboBox.Name = "intervalComboBox";
            this.intervalComboBox.Size = new System.Drawing.Size(260, 24);
            this.intervalComboBox.TabIndex = 1;
            // 
            // resultsListBox1
            // 
            this.resultsListBox1.FormattingEnabled = true;
            this.resultsListBox1.ItemHeight = 16;
            this.resultsListBox1.Location = new System.Drawing.Point(287, 50);
            this.resultsListBox1.Name = "resultsListBox1";
            this.resultsListBox1.Size = new System.Drawing.Size(261, 164);
            this.resultsListBox1.TabIndex = 2;
            // 
            // cartesianChart1
            // 
            chartArea2.Name = "ChartArea1";
            this.cartesianChart1.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.cartesianChart1.Legends.Add(legend2);
            this.cartesianChart1.Location = new System.Drawing.Point(563, 50);
            this.cartesianChart1.Name = "cartesianChart1";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.cartesianChart1.Series.Add(series2);
            this.cartesianChart1.Size = new System.Drawing.Size(506, 322);
            this.cartesianChart1.TabIndex = 3;
            this.cartesianChart1.Text = "cartesianChart1";
            // 
            // textLengthTextBox
            // 
            this.textLengthTextBox.Location = new System.Drawing.Point(12, 102);
            this.textLengthTextBox.Name = "textLengthTextBox";
            this.textLengthTextBox.Size = new System.Drawing.Size(100, 22);
            this.textLengthTextBox.TabIndex = 5;
            this.textLengthTextBox.Text = "100";
             // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(1323, 602);
            this.Controls.Add(this.textLengthTextBox);
            this.Controls.Add(this.cartesianChart1);
            this.Controls.Add(this.resultsListBox1);
            this.Controls.Add(this.intervalComboBox);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.cartesianChart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox intervalComboBox;
        private System.Windows.Forms.ListBox resultsListBox1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.DataVisualization.Charting.Chart cartesianChart1;
        private System.Windows.Forms.TextBox textLengthTextBox;
    }
}

