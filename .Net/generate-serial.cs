using System;
using System.IO;

namespace GenerateSerialNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            // Get the current serial number from the file.
            string serialNumber = GetSerialNumber("serial.txt");

            // Check if the serial number file is in sync with the backup file.
            bool isInSync = CheckFileSync("serial.txt", "serial-backup.txt");

            if (isInSync)
            {
                // Generate the next serial number.
                int nextSerialNumber = int.Parse(serialNumber) + 1;

                // Write the new serial number to the file.
                WriteSerialNumber("serial.txt", nextSerialNumber.ToString());

                // Display the new serial number.
                Console.WriteLine("The next available serial number is: " + nextSerialNumber);
            }
            else
            {
                // Notify the user that the files are not in sync.
                Console.WriteLine("The serial number files are not in sync. Please fix the issue before generating a new serial number.");
            }
        }

        private static string GetSerialNumber(string filename)
        {
            // Open the file in read-only mode.
            StreamReader reader = new StreamReader(filename);

            // Read the first line of the file.
            string serialNumber = reader.ReadLine();

            // Close the file.
            reader.Close();

            return serialNumber;
        }

        private static bool CheckFileSync(string filename1, string filename2)
        {
            // Open the two files in read-only mode.
            StreamReader reader1 = new StreamReader(filename1);
            StreamReader reader2 = new StreamReader(filename2);

            // Read the first line of each file.
            string line1 = reader1.ReadLine();
            string line2 = reader2.ReadLine();

            // Close the two files.
            reader1.Close();
            reader2.Close();

            // Compare the two lines.
            return line1 == line2;
        }

        private static void WriteSerialNumber(string filename, string serialNumber)
        {
            // Open the file in write mode.
            StreamWriter writer = new StreamWriter(filename);

            // Write the serial number to the file.
            writer.WriteLine(serialNumber);

            // Close the file.
            writer.Close();
        }
    }
}
