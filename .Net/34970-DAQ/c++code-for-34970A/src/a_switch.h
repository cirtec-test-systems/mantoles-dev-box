// File: a_switch.h

#ifndef __A_SWITCH_H__
#define __A_SWITCH_H__

/******************************************************************************
 * Constant definitions
 */


#define HWHIDMM_ID					34970     


#define HP34901_TIMETORESET			100000 // 100 ms
#define HP34902_TIMETORESET			100000 
#define HP34903_TIMETORESET			100000
#define HP34904_TIMETORESET			100000
#define HP34905_TIMETORESET			200000 // 200 ms
#define HP34906_TIMETORESET			200000
#define HP34907_TIMETORESET			100000
#define HP34908_TIMETORESET			100000 

#define HP34901_TIMETOSETPOSITION	10000 // 10 ms  (speed in datasheet 120/s)
#define HP34902_TIMETOSETPOSITION	10000 // (120/s)
#define HP34903_TIMETOSETPOSITION	10000 // (120/s)
#define HP34904_TIMETOSETPOSITION	10000 // (120/s)
#define HP34905_TIMETOSETPOSITION	16700 // (60/s)
#define HP34906_TIMETOSETPOSITION	16700 // (60/s)
#define HP34907_TIMETOSETPOSITION	10000 // (x/s)
#define HP34908_TIMETOSETPOSITION	10000 // (70/s)

#define HP34901_COLUMNS				1	// 1x20
#define HP34902_COLUMNS				1	// 1x16	
#define HP34903_COLUMNS				1	// 1x20
#define HP34904_COLUMNS				8	// 8x4
#define HP34905_COLUMNS				2	// 2x4
#define HP34906_COLUMNS				2	// 2x4
#define HP34907_COLUMNS				2	// means Ports of Digital I/O
#define HP34908_COLUMNS				1	// 1x40

#define HP34901_ROWS				20 // �without the current channels 21&22
#define HP34902_ROWS				16
#define HP34903_ROWS				20	
#define HP34904_ROWS				4
#define HP34905_ROWS				4
#define HP34906_ROWS				4
#define HP34907_ROWS				8
#define HP34908_ROWS				40



#define DEMOSECONDS					0

#define TYPE_MATRIX					1	// Will be used to create Nodes (Aliases)
#define TYPE_MUX					2
#define TYPE_SWITCH					3
#define TYPE_OTHER					0


#define SWITCH_NUM_ELEMENTS			400  /* Number of elements for all cards */
#define ITSDONE						1     /* 1us */

#define DEFAULT_ADDRESS				9
#define DEFAULT_NAME				"HP34970A"
#define DEFAULT_MODEL				0
#define DEFAULT_TYPE				2
#define DEFAULT_COLUMNS				4
#define DEFAULT_ROWS				16
#define DEFAULT_BASE				1

#define OPEN						0
#define CLOSE						1


/******************************************************************************
 * Type declarations
 *
 * Here we define a data structure to hold information unique to every
 * instance of the Switch Handler.
 */
typedef struct t_InstParmStruct
{
	ViSession		vi;                 // Visa Session number
	ULONG			ulHandlerID;        // Unique ID for the handler that opened us
	void			*pInstState;
    
	ViSession		defaultRM;
	UTAINT32		nInterface;			// 0 = HPIB, 1-N = Com1-ComN
	UTAINT32		nBoardNumber;			// GPIB Card number 0.. 
	UTAINT32		nAddress;			// GPIB Address 
	UTAINT32		nBaudrate;			// Baudrate for Serial 
	UTAINT32		nAutoConf;			// 0 or 1 to indicate Auto configuration 
	UTAINT32		nDmmUsage;			// 0 or 1 to indicate using of internal DMM
	UTAINT32		nModel1;			// Number of card installed in slot 1
	UTAINT32		nModel2;			// Number of card installed in slot 2
	UTAINT32		nModel3;			// Number of card installed in slot 3
	UTAINT32		nColumns[4];		// Index 1 to 3 for Columns in each Slot
	UTAINT32		nRows[4];			// Index 1 to 3 for Rows in each Slot
	UTAINT32		nTimeReset[4];		// Value for each card in Slot	
	UTAINT32		nTimeSetPos[4];		// Value for each card in Slot	
	UTAINT32		nDemoTic;			// Tic counter for Demo mode
	char			nName[80];			// string for name
	unsigned char	nSlope[4][4];		// 	
    unsigned char	nElementState[400];	// Contains position (state) for each element	

	BOOL			bSetPosQuery;		// globals to get call SetPosition without IO
	UTAINT32		nSetPosChannel;		// Channel number for idmm and mfunc actions
} InstParmStruct;




extern "C"
{
	int  HP34970_AutoConfig    (InstParmStruct* pParmStruct, HUTAHWMOD hModule, HUTAPB hPB);
	void HP34970_SetParameters (InstParmStruct *pParmStruct);
	void HP34970_CreateNodes   (InstParmStruct *pParmStruct, HUTAHWMOD hModule, int iSlot);
	int	 DemoTimeElapsed	   (DWORD TicEnd);

	// in File io_util.cpp
	char *VisaErrorMessage (ViStatus VisaError);
}


#endif // __A_SWITCH_H__
