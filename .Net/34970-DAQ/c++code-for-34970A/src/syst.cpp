// File: syst.cpp

// This functions are system level to HP34970A


#include "stdlib.h"
#include "uta.h"
#include "utaapi.h"
#include "pubapi.h"
#include "visa.h"

#include "a_switch.h"
#include "syst.h"


///////////////////////////////////////////////////////////////////////
// File globals:

extern "C" long GetChannelFromPB (HUTAPB hPB, char *ParName);

extern ViSession		vSession;
extern ViStatus			vStatus;
extern InstParmStruct	*userHandle;        
extern int				iDmmUsage;


////////////////////////////////////////////////////////////////////////
// HUTAINST validation

static BOOL hUtaInstIsValid (HUTAINST inst)
{
    // Immediately return FALSE if IO Bypass is enabled.
//    if (LinkVGetIOBypass()) return FALSE;  
    
    // Make sure the userHandle_1 in the "UtaInst" data type is valid, 
    // and that its associated VISA session id exists.
    userHandle = (InstParmStruct*) UtaInstGetUserHandle (inst);
    if ((!userHandle) || ((vSession = userHandle->vi) == NULL))
    {
        UtaExcRaiseUserError("Invalid Instrument Handle passed to hwh34970a.", 9 );
        return FALSE;
    }

    if ((vSession = userHandle->vi) == NULL)
    {
        UtaExcRaiseUserError ("No VISA session passed to HP34970A.", 9);
        return FALSE;
    }

	if (DemoTimeElapsed (userHandle->nDemoTic)) return FALSE;


/*
    // Make sure the userHandle_1 was created for a Multifunc card 
    if (userHandle->ulHandlerID != HWHMULTIFUNC_ID)
    {
        UtaExcRaiseUserError("Invalid Instrument ID passed to hwh34970a.", 9 );
        return FALSE;
    }
*/

    // Get the Slot Number
    UtaInstHPIBGetValues (inst,
                          NULL,
                          NULL,
                          0,
                          0,
                          0,
                          NULL,
                          NULL,
                          &iDmmUsage,
                          NULL,
                          NULL,
                          NULL);

	return TRUE;
}


////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
//  Syst actions
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////


void CheckInstError (ViSession vSession)
{
	char  szError[80];
	char  *pCmd = "SYST:ERR?\n";

	UtaTrace (pCmd);
	viPrintf (vSession, pCmd);
	viScanf (vSession, "%t", szError);
	UtaTrace (szError);
	if (atoi (szError)) 
	{
		VSendReportMsg ("Intrument error: ");
		VSendReportMsg (szError);
	}
}


////////////////////////////////////////////////////////////////////////
// Action sysSetData

void UTADLL sysSetDate (HUTAPB hPB)
{
	long iYear, iMonth, iDate;
	char szCmd[80];
    
	IUtaInst    hInst    (hPB, "sys");
    if (! hUtaInstIsValid(hInst))  return;

	UtaPbGetInt32 (hPB, "year", &iYear);
	UtaPbGetInt32 (hPB, "month", &iMonth);
	UtaPbGetInt32 (hPB, "day", &iDate);

	if (iYear < 1996 || iMonth < 1 || iDate < 1)
	{
			UtaExcRaiseUserError ("Incorrect parameter value for date", 9);
	}

	sprintf (szCmd, "SYST:DATE %4.4d,%2.2d,%2.2d\n", iYear, iMonth, iDate);
	UtaTrace (szCmd);
	vStatus = viPrintf (vSession, szCmd);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }
}


////////////////////////////////////////////////////////////////////////
// Action sysGetDate

void UTADLL sysGetDate (HUTAPB hPB)
{
	int  iDate[3];

    
	IUtaInst    hInst    (hPB, "sys");
    if (! hUtaInstIsValid(hInst))  return;

    HUTAR64ARR hDate = UtaPbGetReal64Array (hPB, "date");

	vStatus = viPrintf (vSession, "SYST:DATE?\n");
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }

	vStatus = viScanf (vSession, "%,3ld%*t", iDate);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }

	for (int i = 0; i < 3; i++)
	{
		UtaR64ArrSetAt1 (hDate, i, iDate[i]);
	}
}


////////////////////////////////////////////////////////////////////////
// Action sysSetTime

void UTADLL sysSetTime (HUTAPB hPB)
{
	long  iHour, iMinute, iSecond;
	char  szCmd[80];

	IUtaInst    hInst    (hPB, "sys");
    if (! hUtaInstIsValid(hInst))  return;

	UtaPbGetInt32 (hPB, "hour", &iHour);
	UtaPbGetInt32 (hPB, "minute", &iMinute);
	UtaPbGetInt32 (hPB, "second", &iSecond);

	if (iHour > 23 || iMinute >  59 || iSecond > 59)
	{
			UtaExcRaiseUserError ("Incorrect parameter value for time", 9);
			return;
	}

	sprintf (szCmd, "SYST:TIME %2.2d,%2.2d,%2.2d\n", iHour, iMinute, iSecond);
	UtaTrace (szCmd);
	vStatus = viPrintf (vSession, szCmd);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }
}


////////////////////////////////////////////////////////////////////////
// Action sysGetTime

void UTADLL sysGetTime (HUTAPB hPB)
{
	int  iTime[3];


	IUtaInst    hInst    (hPB, "sys");
    if (! hUtaInstIsValid(hInst))  return;

    HUTAR64ARR hTime = UtaPbGetReal64Array (hPB, "time");

	vStatus = viPrintf (vSession, "SYST:TIME?\n");
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }
	vStatus = viScanf (vSession, "%,3ld%*t", iTime);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }

	for (int i = 0; i < 3; i++)
	{
		UtaR64ArrSetAt1 (hTime, i, iTime[i]);
	}
}

////////////////////////////////////////////////////////////////////////
// Action sysLocal

void UTADLL sysLocal (HUTAPB hPB)
{
	char *pCmd = "SYST:LOCAL\n";

	IUtaInst    hInst    (hPB, "sys");
    if (! hUtaInstIsValid(hInst))  return;

	if (userHandle->nBaudrate > 0)
	{
		UtaTrace (pCmd);
		vStatus = viPrintf (vSession, pCmd);
	}
	else
	{
		VSendReportMsg ("No command available setting instrument to local state\n"); 
	}

    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage(vStatus), 9);
        return;
    }
}

////////////////////////////////////////////////////////////////////////

void UTADLL sysTest (HUTAPB hPB)
{

	int iTst;
	int i;
	char szCmd[80];

	IUtaInst    hInst    (hPB, "sys");
    if (! hUtaInstIsValid(hInst))  return;

	sprintf (szCmd, "*TST?\n");
	UtaTrace (szCmd);
	vStatus = viPrintf (vSession, szCmd);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }

	VSendReportMsg ("The selftest takes 10 seconds. Please wait ...\n"); 
	for (i = 0; i < 10; i++)
		Sleep (1000);

	vStatus = viScanf (vSession, "%t", szCmd);
	UtaTrace (szCmd);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }

	sscanf (szCmd, "%d", &iTst);
	UtaPbSetInt32 (hPB, "Result", iTst);
}


////////////////////////////////////////////////////////////////////////
// Action sysDisplay

void UTADLL sysDisplay (HUTAPB hPB)
{
	char szText[80];
	char szCmd[128];

	IUtaInst    hInst    (hPB, "sys");
    if (! hUtaInstIsValid(hInst))  return;

	IUtaInt32  iControl (hPB, "Control");
	UtaPbGetString (hPB, "Text", szText, 79);

	if (iControl == 0)
	{
		sprintf (szCmd, "DISP OFF\n");
	}
	else
	{
		if (strlen (szText))
			sprintf (szCmd, "DISP ON;DISP:TEXT \"%s\"\n", szText);
		else
			sprintf (szCmd, "DISP ON\n");
	}
	UtaTrace (szCmd);
	vStatus = viPrintf (vSession, szCmd);

    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }
}


////////////////////////////////////////////////////////////////////////
// Actions sysTring

void UTADLL sysTrig (HUTAPB hPB)
{
	char *pCmd = "*TRG\n";

	IUtaInst    hInst    (hPB, "sys");
    if (! hUtaInstIsValid(hInst))  return;

	UtaTrace (pCmd);
	vStatus = viPrintf (vSession, pCmd);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }
}


////////////////////////////////////////////////////////////////////////
// Action sysError

void UTADLL sysError (HUTAPB hPB)
{
	char  *pCmd = "SYST:ERR?\n";
	char  szError[80];
    int   iError;

    IUtaInst    hInst    (hPB, "sys");
    if (! hUtaInstIsValid(hInst))  return;

	UtaTrace (pCmd);
   	vStatus = viPrintf (vSession, pCmd);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }
	// Read the answer (e.g. +0,"No Error"\n) to get Error and Message without ""
	vStatus = viScanf (vSession, "%t", szError);
	UtaTrace (szError);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }

	sscanf (szError, "%d", &iError);

	// Mask out CR+LF and the ending "
	szError[strlen (szError) - 2] = 0;
	UtaPbSetInt32 (hPB, "Error", iError);
	UtaPbSetString (hPB, "ErrorMessage", szError);
}


////////////////////////////////////////////////////////////////////////
// Action sysRead

void UTADLL sysRead (HUTAPB hPB)
{
	char  szRead[128];
	int	  iPos;


    IUtaInst    hInst    (hPB, "sys");
    if (! hUtaInstIsValid(hInst))  return;


	UtaPbGetString (hPB, "Read", szRead, 128);
   	vStatus = viScanf (vSession, "%t", szRead);
	UtaTrace (szRead);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
	}

	iPos = strlen (szRead) - 1;
	// remove the CR and LF characters
	if (szRead[iPos] == '\n') szRead[iPos] = 0;
	iPos--;
	if (szRead[iPos] == '\r') szRead[iPos] = 0;

	UtaPbSetString (hPB, "Read", szRead);
}


////////////////////////////////////////////////////////////////////////
// Action sysSend

void UTADLL sysSend (HUTAPB hPB)
{
	char  szSend[128];

    IUtaInst    hInst    (hPB, "sys");
    if (! hUtaInstIsValid(hInst))  return;

	UtaPbGetString (hPB, "Send", szSend, 79);

   	sprintf (szSend, "%s\n", szSend);
	UtaTrace (szSend);
   	vStatus = viPrintf (vSession, szSend);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
	}
}


////////////////////////////////////////////////////////////////////////
// Action diaGetCycles

void UTADLL diagGetCycles (HUTAPB hPB)
{

	long   lChannel;
	long   lCycles;
	char   szCmd[80];

    IUtaInst    hInst    (hPB, "sys");
	if (! hUtaInstIsValid(hInst))  return;

    lChannel = GetChannelFromPB (hPB, "Channel");
    if (lChannel <= 0) return;

    // Channel = 0: returns 3 values !!! vStatus = viPrintf (vSession, "Diag:DMM:Cycles?\n");
	sprintf (szCmd, "Diag:Rel:Cycles? (@%ld)\n", lChannel);
	UtaTrace (szCmd);
   	vStatus = viPrintf (vSession, szCmd);
	if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
	}

    vStatus = viScanf (vSession, "%t", szCmd);
	UtaTrace (szCmd);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }

	sscanf (szCmd, "%d", &lCycles);
	UtaPbSetInt32 (hPB, "Cycles", lCycles);
}


////////////////////////////////////////////////////////////////////////
// Action sysReset

void UTADLL sysReset (HUTAPB hPB)
{
	char szCmd[80];

    // Parameters from the Action Parameter Block.
    IUtaInst    hInst    (hPB, "sys");
    if (! hUtaInstIsValid(hInst))  return;
    
	sprintf (szCmd, "*RST\n");
	UtaTrace (szCmd);
	vStatus = viPrintf (vSession, szCmd); // resets the complete HP34970A
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }

	sprintf (szCmd, "*CLS\n");
	UtaTrace (szCmd);
	vStatus = viPrintf (vSession, szCmd); // resets the status
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }

	sprintf (szCmd, "*OPC?\n");
	UtaTrace (szCmd);
	vStatus = viPrintf (vSession, szCmd); // wait for operation complete
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }

	vStatus = viScanf (vSession, "%t", szCmd);  // ignore answer
	UtaTrace (szCmd);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }

    /* Track the reset */
	UtaTrace ("Reset the switch states\n");
    for (int i = 0; i < SWITCH_NUM_ELEMENTS; i++)
		userHandle->nElementState[i] = OPEN;

    CheckInstError (vSession);
}


////////////////////////////////////////////////////////////////////////
// Action sysSetStatus

void UTADLL sysSetStatus (HUTAPB hPB)
{
	long  lType;
	long  lValue;
	char  szCmd[80];


	IUtaInst    hInst    (hPB, "sys");
    if (! hUtaInstIsValid(hInst))  return;

	UtaPbGetInt32 (hPB, "type", &lType);
	UtaPbGetInt32 (hPB, "value", &lValue);

	switch (lType)
	{
	case 0: sprintf (szCmd, "STAT:ALARM:ENABLE %d\n", lValue); break;
	case 1: sprintf (szCmd, "*CLS\n"); break; 
	case 2: sprintf (szCmd, "DATA:POINTS:EVENT:THRESHOLD %d\n", lValue); break;
	case 3: sprintf (szCmd, "*ESE %d\n", lValue); break;
	case 4: sprintf (szCmd, "*OPC\n"); break;
	case 5: sprintf (szCmd, "STAT:OPER:ENABLE %d\n", lValue); break;
	case 6: sprintf (szCmd, "STAT:PRESET\n"); break;
	case 7: sprintf (szCmd, "*PSC %d\n", lValue); break;
	case 8: sprintf (szCmd, "STAT:QUES:ENABLE %d\n", lValue); break;
	case 9: sprintf (szCmd, "*SRE %d\n", lValue); break;// SRE
	}

	UtaTrace (szCmd);
	vStatus = viPrintf (vSession, szCmd);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }
}


////////////////////////////////////////////////////////////////////////
// Action sysGetStatus

void UTADLL sysGetStatus (HUTAPB hPB)
{
	long  lType;
	long  lValue;
	char  szCmd[80];


	IUtaInst    hInst    (hPB, "sys");
    if (! hUtaInstIsValid(hInst))  return;

	UtaPbGetInt32 (hPB, "type", &lType);
	UtaPbGetInt32 (hPB, "value", &lValue);

	switch (lType)
	{
	case 0: sprintf (szCmd, "STAT:ALARM:COND?\n"); break;
	case 1: sprintf (szCmd, "STAT:ALARM:EVENT?\n"); break;
	case 2: sprintf (szCmd, "STAT:ALARM:ENAB?\n"); break;
	case 3: sprintf (szCmd, "DATA:POINTS:EVENT:THRESHOLD?\n"); break;
	case 4: sprintf (szCmd, "*ESE?\n"); break; 
	case 5: sprintf (szCmd, "*ESR?\n"); break;
	case 6: sprintf (szCmd, "*OPC?\n"); break;
	case 7: sprintf (szCmd, "STAT:OPER:COND?\n"); break;
	case 8: sprintf (szCmd, "STAT:OPER:EVENT?\n"); break;
	case 9: sprintf (szCmd, "STAT:OPER:ENABLE?\n"); break;
	case 10: sprintf (szCmd, "*PSC?\n"); break;
	case 11: sprintf (szCmd, "STAT:QUES:COND?\n"); break;
	case 12: sprintf (szCmd, "STAT:QUES:EVENT?\n"); break;
	case 13: sprintf (szCmd, "STAT:QUES:ENABLE?\n"); break;
	case 14: sprintf (szCmd, "*SRE?\n"); break;// SRE
	}

	UtaTrace (szCmd);
	vStatus = viPrintf (vSession, szCmd);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }

    vStatus = viScanf (vSession, "%t", szCmd);
	UtaTrace (szCmd);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }

	sscanf (szCmd, "%d", &lValue);
	UtaPbSetInt32 (hPB, "value", lValue);
}



