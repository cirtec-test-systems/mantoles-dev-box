///////////////////////////////////////////////////////////////////////
// File: multifunc.cpp

// All functions that belong to the multifunction module of HP34970A


#include "stdlib.h"
#include "uta.h"
#include "utaapi.h"
#include "pubapi.h"
#include "visa.h"

#include "a_switch.h"
#include "multifunc.h"




///////////////////////////////////////////////////////////////////////
// File globals:

extern "C" long GetChannelFromPB (HUTAPB hPB, char *ParName);
extern "C" void CheckInstError (ViSession vSession);

extern ViSession		vSession;
extern ViStatus			vStatus;
extern InstParmStruct	*userHandle;        
extern int				iDmmUsage;


////////////////////////////////////////////////////////////////////////
// HUTAINST validation

static BOOL hUtaInstIsValid (HUTAINST inst)
{
    // Make sure the userHandle_1 in the "UtaInst" data type is valid, 
    // and that its associated VISA session id exists.
    userHandle = (InstParmStruct*) UtaInstGetUserHandle (inst);
    if ((!userHandle) || ((vSession = userHandle->vi) == NULL))
    {
        UtaExcRaiseUserError( "Invalid Instrument Handle passed to hwh34970a.", 9 );
        return FALSE;
    }
    if ((vSession = userHandle->vi) == NULL)
	{
        UtaExcRaiseUserError ("No VISA session avaliable for hwH34970a.", 9);
        return FALSE;
    }

	if (DemoTimeElapsed (userHandle->nDemoTic)) return FALSE;


    // Get the Slot Number
    UtaInstHPIBGetValues (inst,
                          NULL,
                          NULL,
                          0,
                          0,
                          0,
                          NULL,
                          NULL,
                          &iDmmUsage,
                          NULL,
                          NULL,
                          NULL);

	return TRUE;
}


////////////////////////////////////////////////////////////////////////
// hwh4970a IsDigChannel

int IsDigChannel (long lChannel)
{
	int iSlot;
	int iPort;

	iSlot = lChannel / 100;
	iPort = lChannel % 10;
	if (iPort < 1 || iPort > 2)
	{
        UtaExcRaiseUserError ("Incorrect Digital Channel", 9);
		return 0;
	}
	return 1;
}


int IsTotChannel (long lChannel)
{
	int iSlot;
	int iPort;

	iSlot = lChannel / 100;
	iPort = lChannel % 10;
	if (iPort != 3)
	{
        UtaExcRaiseUserError ("Incorrect Totalizer Channel", 9);
		return 0;
	}
	return 1;
}


int IsDacChannel (long lChannel)
{
	int iSlot;
	int iPort;

	iSlot = lChannel / 100;
	iPort = lChannel % 10;
	if (iPort < 4 || iPort > 5)
	{
        UtaExcRaiseUserError ("Incorrect Analog Output Channel", 9);
		return 0;
	}
	return 1;
}


////////////////////////////////////////////////////////////////////////
// hwH34970_multifunc Actions
////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////
// Action mfuncReset

void UTADLL mfuncReset (HUTAPB hPB)
{
	long lChannel;
	char szCmd[80];

    IUtaInst   hInst (hPB, "mfunc");
    lChannel = GetChannelFromPB (hPB, "Channel");
    if (lChannel <= 0) return;
    if (! hUtaInstIsValid(hInst))  return;
    
	sprintf (szCmd, "Syst:CPon %ld\n", (lChannel / 100) * 100);
	UtaTrace (szCmd);
	vStatus = viPrintf (vSession, szCmd);
	if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
	}

	// Set the global variable that defines the Digital Out
	// for each possible Port to default
	for (int iSlot = 0; iSlot <= 3; iSlot++)
		for (int iPort = 0; iPort <=2; iPort++)
			userHandle->nSlope[iPort][iSlot] = 0;
}


///////////////////////////////////////////////////////////////////////
// Action mfuncSetDigOut

void UTADLL mfuncSetDigOut (HUTAPB hPB)
{

	long lChannel;
	long lMode;
	long lBit;
	long lValue;
    long lActual;
	long lState;
	char szCmd[80];

    IUtaInst hInst (hPB, "mfunc");
    UtaPbGetInt32  (hPB, "Mode",  &lMode);
    UtaPbGetInt32  (hPB, "Bit",   &lBit);
    UtaPbGetInt32  (hPB, "Value", &lValue);
		
    lChannel = GetChannelFromPB (hPB, "Channel");
    if (lChannel <= 0) return;
    if (! hUtaInstIsValid(hInst))  return;
	if (! IsDigChannel (lChannel)) return;


    switch (lMode)
	{
    case MULTIFUNC_DIG_MODE_BIT:
			sprintf (szCmd, "Sour:Dig:State? (@%d)\n", lChannel);
			UtaTrace (szCmd);
			vStatus = viPrintf (vSession, szCmd);
			if (vStatus != VI_SUCCESS)
			{
				UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
				return;
			}
			vStatus = viScanf (vSession, "%t", szCmd);
			UtaTrace (szCmd);
			sscanf (szCmd, "%d", &lState);
			if (vStatus != VI_SUCCESS)
			{
				UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
				return;
			}
			if (lState != 0)
			{
				sprintf (szCmd, "Source:Dig:Data:Byte? (@%ld)\n", lChannel);
				UtaTrace (szCmd);
				vStatus = viPrintf (vSession, szCmd);
				if (vStatus != VI_SUCCESS)
				{
					UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
					return;
				}
				vStatus = viScanf (vSession, "%t", szCmd);
				UtaTrace (szCmd);
				sscanf (szCmd, "%d", &lActual);
				if (vStatus != VI_SUCCESS)
				{
					UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
					return;
				}
			}
			else
			{
				lActual = 0;
			}
			if (lValue)	 lActual |= (1 << lBit);
			else         lActual &= ~(1 << lBit);

			sprintf (szCmd, "Source:Dig:Data:Byte %ld,(@%ld)\n", lActual, lChannel);
			UtaTrace (szCmd);
			vStatus = viPrintf (vSession, szCmd);
			break;

    case MULTIFUNC_DIG_MODE_BYTE:
            sprintf (szCmd, "Source:Dig:Data:Byte %ld,(@%ld)\n", lValue, lChannel);
			UtaTrace (szCmd);
            vStatus = viPrintf (vSession, szCmd);
		    break;

	case MULTIFUNC_DIG_MODE_WORD:
            sprintf (szCmd, "Source:Dig:Data:Word %ld,(@%ld)\n", lValue, lChannel);
			UtaTrace (szCmd);
            vStatus = viPrintf (vSession, szCmd);
	        break;

	default: return;
	}

	if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
	}
}


///////////////////////////////////////////////////////////////////////
// Action mfuncGetDigIn

void UTADLL mfuncGetDigIn (HUTAPB hPB)
{
	long   lChannel;
	long   lMode;
	long   lBit;
	char   szCmd[80];
	double dData;

    IUtaInst    hInst    (hPB, "mfunc");
    UtaPbGetInt32  (hPB, "Mode", &lMode);
    UtaPbGetInt32  (hPB, "Bit", &lBit);

    lChannel = GetChannelFromPB (hPB, "Channel");
    if (lChannel <= 0) return;
	if (! IsDigChannel (lChannel)) return;
    if (! hUtaInstIsValid(hInst))  return;
    

    long lData = 0;

    switch (lMode)
	{
    case MULTIFUNC_DIG_MODE_BIT:
		    sprintf (szCmd, "Meas:Dig:Byte? (@%ld)\n", lChannel);
			UtaTrace (szCmd);
		    vStatus = viPrintf (vSession, szCmd);
			if (vStatus != VI_SUCCESS)
			{
				UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
				return;
			}
			vStatus = viScanf (vSession, "%t", szCmd);
			UtaTrace (szCmd);
			sscanf (szCmd, "%lf", &dData);
			lData = (long) dData;
            if (lData & (1 << lBit)) dData = 1;
			else                     dData = 0;
	        break;

    case MULTIFUNC_DIG_MODE_BYTE:
		    sprintf (szCmd, "Meas:Dig:Byte? (@%ld)\n", lChannel);
			UtaTrace (szCmd);
		    vStatus = viPrintf (vSession, szCmd);
			if (vStatus != VI_SUCCESS)
			{
				UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
				return;
			}
			vStatus = viScanf (vSession, "%t", szCmd);
			UtaTrace (szCmd);
			sscanf (szCmd, "%lf", &dData);
	        break;

	case MULTIFUNC_DIG_MODE_WORD:
		    sprintf (szCmd, "Dig:Data:Word? (@%ld)\n", lChannel);
			UtaTrace (szCmd);
		    vStatus = viPrintf (vSession, szCmd);
			if (vStatus != VI_SUCCESS)
			{
				UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
				return;
			}
			vStatus = viScanf (vSession, "%t", szCmd);
			UtaTrace (szCmd);
			sscanf (szCmd, "%lf", &dData);
			break;

	default: return;
	}
 
	if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
	}

	UtaPbSetInt32 (hPB, "Result", (long) dData);
}


///////////////////////////////////////////////////////////////////////
// Action mfuncSetDigOutArray

void UTADLL mfuncSetDigOutArray (HUTAPB hPB)
{
	long lChannel;
	long lMode;
	long lBit;
	long lCount;
	long lData;
    long lActual;
	int  i;
	char szCmd[80];

    IUtaInst		hInst (hPB, "mfunc");
    UtaPbGetInt32   (hPB, "Mode", &lMode);    // Bit=0, LB=01, HB=2, Word=3
    UtaPbGetInt32   (hPB, "Bit", &lBit);     // 0..15
    UtaPbGetInt32   (hPB, "Count", &lCount);  
    HUTAI32ARR hValues = UtaPbGetInt32Array (hPB, "Values");

    lChannel = GetChannelFromPB (hPB, "Channel");
    if (lChannel <= 0) return;
	if (! IsDigChannel (lChannel)) return;
    if (! hUtaInstIsValid(hInst))  return;
    

    switch (lMode)
	{
    case MULTIFUNC_DIG_MODE_BIT:
			// read the current value. port has byte|word access only
			sprintf (szCmd, "Source:Dig:Data:Byte? (@%ld)\n", lChannel);
			UtaTrace (szCmd);
		    vStatus = viPrintf (vSession, szCmd);
			if (vStatus != VI_SUCCESS)
			{
				UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
				return;
			}
			vStatus = viScanf (vSession, "%ld", &lActual);
			if (vStatus != VI_SUCCESS)
			{
				UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
				return;
			}
			for (i = 0; i < lCount; i++)
			{
				lData = UtaI32ArrGetAt1 (hValues, i);
				if (lData)	lActual |= (1 << lBit);
				else        lActual &= ~(1 << lBit);

				sprintf (szCmd, "Source:Dig:Data:Byte %ld,(@%ld)\n", lActual, lChannel);
				UtaTrace (szCmd);
				vStatus = viPrintf (vSession, szCmd);
				if (vStatus != VI_SUCCESS)
				{
					UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
					return;
				}
			}
			break;

    case MULTIFUNC_DIG_MODE_BYTE:
			for (i = 0; i < lCount; i++)
			{
				lData = UtaI32ArrGetAt1 (hValues, i);
				sprintf (szCmd, "Source:Dig:Data:Byte %ld,(@%ld)\n", lData, lChannel);
				UtaTrace (szCmd);
				vStatus = viPrintf (vSession, szCmd);
				if (vStatus != VI_SUCCESS)
				{
					UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
					return;
				}
			}
	        break;

	case MULTIFUNC_DIG_MODE_WORD:
			for (i = 0; i < lCount; i++)
			{
				lData = UtaI32ArrGetAt1 (hValues, i);
				sprintf (szCmd, "Source:Dig:Data:Word %ld,(@%ld)\n", lData, lChannel);
				UtaTrace (szCmd);
				vStatus = viPrintf (vSession, szCmd);
				if (vStatus != VI_SUCCESS)
				{
					UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
					return;
				}
			}
	        break;

	default: return;
	}

}


///////////////////////////////////////////////////////////////////////
// Action mfuncGetDigInArray

void UTADLL mfuncGetDigInArray (HUTAPB hPB)
{
	long   lChannel;
	long   lMode;
	long   lBit;
	long   lCount;
	long   lData;
	int    i;
	char   szCmd[80];
	char   szResponse[80];
	double dData;

    IUtaInst		hInst    (hPB, "mfunc");
    UtaPbGetInt32   (hPB, "Mode", &lMode);    // Bit=0, LB=01, HB=2, Word=3
    UtaPbGetInt32   (hPB, "Bit", &lBit);     // 0..15
    UtaPbGetInt32   (hPB, "Count", &lCount);  
    HUTAI32ARR hReadings = UtaPbGetInt32Array (hPB, "Readings");

    lChannel = GetChannelFromPB (hPB, "Channel");
    if (lChannel <= 0) return;
	if (! IsDigChannel (lChannel)) return;
    if (! hUtaInstIsValid(hInst))  return;
    

    switch (lMode)
	{
    case MULTIFUNC_DIG_MODE_BIT:
			sprintf (szCmd, "Meas:Dig:Byte? (@%ld)\n", lChannel);
			for (i = 0; i < lCount; i++)
			{
				UtaTrace (szCmd);
				vStatus = viPrintf (vSession, szCmd);
				if (vStatus != VI_SUCCESS)
				{
					UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
					return;
				}
				vStatus = viScanf (vSession, "%t", szResponse);
				UtaTrace (szResponse);
				sscanf (szResponse , "%lf", &dData);
				lData = (long) dData;
				if (vStatus != VI_SUCCESS)
				{
					UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
					return;
				}
			    if (lBit > 7) lData <<= 8;
				if (lData & (1 << lBit)) dData = 1;
				else                     dData = 0;
				UtaI32ArrSetAt1 (hReadings, i, (long) dData);
			}
	        break;

    case MULTIFUNC_DIG_MODE_BYTE:
			sprintf (szCmd, "Meas:Dig:Byte? (@%ld)\n", lChannel);
			for (i = 0; i < lCount; i++)
			{
				UtaTrace (szCmd);
				vStatus = viPrintf (vSession, szCmd);
				if (vStatus != VI_SUCCESS)
				{
					UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
					return;
				}
				vStatus = viScanf (vSession, "%t", szResponse);
				UtaTrace (szResponse);
				sscanf (szResponse, "%lf", &dData);
				if (vStatus != VI_SUCCESS)
				{
					UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
					return;
				}
				UtaI32ArrSetAt1 (hReadings, i, (long) dData);
			}
	        break;

	case MULTIFUNC_DIG_MODE_WORD:
			sprintf (szCmd, "Meas:Dig:Word? (@%ld)\n", lChannel);
			for (i = 0; i < lCount; i++)
			{
				UtaTrace (szCmd);
				vStatus = viPrintf (vSession, szCmd);
				if (vStatus != VI_SUCCESS)
				{
					UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
					return;
				}
				vStatus = viScanf (vSession, "%t", &dData);
				UtaTrace (szResponse);
				sscanf (szResponse, "%lf", &dData);
				if (vStatus != VI_SUCCESS)
				{
					UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
					return;
				}
				UtaI32ArrSetAt1 (hReadings, i, (long) dData);
			}
	        break;
	default: return;
	}

}


///////////////////////////////////////////////////////////////////////
// Action mfuncTotConf

void UTADLL mfuncTotConf (HUTAPB hPB)
{
	long lChannel;
	long lType;
	long lSlope;
	char szCmd[80];

    IUtaInst    hInst    (hPB, "mfunc");
    UtaPbGetInt32  (hPB, "Type", &lType);
    UtaPbGetInt32  (hPB, "Slope", &lSlope);

    lChannel = GetChannelFromPB (hPB, "Channel");
    if (lChannel <= 0) return;
	if (! IsTotChannel (lChannel)) return;
    if (! hUtaInstIsValid(hInst))  return;

    
    if (lType)  // Read only
      sprintf (szCmd, "Sense:Tot:Type Read,(@%ld)\n", lChannel);
	else        // Read & Reset
      sprintf (szCmd, "Sense:Tot:Type RReset,(@%ld)\n", lChannel);

	UtaTrace (szCmd);
    vStatus = viPrintf (vSession, szCmd);

	if (lSlope)
      sprintf (szCmd, "Sense:Tot:Slope Pos,(@%ld)\n", lChannel);
	else
      sprintf (szCmd, "Sense:Tot:Slope Neg,(@%ld)\n", lChannel);

	UtaTrace (szCmd);
    vStatus = viPrintf (vSession, szCmd);
	if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
	}
}


///////////////////////////////////////////////////////////////////////
// Action mfuncTotInitiate

void UTADLL mfuncTotInitiate (HUTAPB hPB)
{
	long lChannel;
	char szCmd[80];

    IUtaInst    hInst (hPB, "mfunc");

    lChannel = GetChannelFromPB (hPB, "Channel");
    if (lChannel <= 0) return;
	if (! IsTotChannel (lChannel)) return;
    if (! hUtaInstIsValid(hInst))  return;
    
    sprintf (szCmd, "Sense:Tot:Clear:Imm (@%ld)\n", lChannel);
	UtaTrace (szCmd);
    vStatus = viPrintf (vSession, szCmd);
	if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
	}
}


///////////////////////////////////////////////////////////////////////
// Action mfuncTotGetResult

void UTADLL mfuncTotGetResult (HUTAPB hPB)
{
	long   lChannel;
	long   lResult;
	char   szCmd[80];
	double dData;

    IUtaInst    hInst    (hPB, "mfunc");
	UtaPbGetInt32  (hPB, "Result", &lResult);

    lChannel = GetChannelFromPB (hPB, "Channel");
    if (lChannel <= 0) return;
	if (! IsTotChannel (lChannel)) return;
    if (! hUtaInstIsValid(hInst))  return;
    
    sprintf (szCmd, "Sense:Tot:Data? (@%ld)\n", lChannel);
	UtaTrace (szCmd);
    vStatus = viPrintf (vSession, szCmd);
	if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
	}

    vStatus = viScanf (vSession, "%t", szCmd);
	UtaTrace (szCmd);
	sscanf (szCmd, "%lf", &dData);
	if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
	}

	UtaPbSetInt32 (hPB, "Result", (long) dData);
}


///////////////////////////////////////////////////////////////////////
// Action mfuncTotReset

void UTADLL mfuncTotReset (HUTAPB hPB)
{
    long lChannel;
	char szCmd[80];

    IUtaInst    hInst    (hPB, "mfunc");

    lChannel = GetChannelFromPB (hPB, "Channel");
    if (lChannel <= 0) return;
	if (! IsTotChannel (lChannel)) return;
    if (! hUtaInstIsValid(hInst))  return;
    
    sprintf (szCmd, "Tot:Clear:Imm (@%ld)\n", lChannel);
	UtaTrace (szCmd);
    vStatus = viPrintf (vSession, szCmd);
	if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
	}
}

///////////////////////////////////////////////////////////////////////
// Action mfuncSetAnalogOut

void UTADLL mfuncSetAnalogOut (HUTAPB hPB)
{
	// Inst, OutputValue
	long   lChannel;
	double dValue;
	char   szCmd[80];

    IUtaInst    hInst    (hPB, "mfunc");
    UtaPbGetReal64 (hPB, "Value", &dValue);

    lChannel = GetChannelFromPB (hPB, "Channel");
    if (lChannel <= 0) return;
//	if (! IsDacChannel (lChannel)) return;
	if (! hUtaInstIsValid(hInst))  return;

    sprintf (szCmd, "Source:Volt %lf,(@%ld)\n", (double) dValue, lChannel);
	UtaTrace (szCmd);
    vStatus = viPrintf (vSession, szCmd);
	if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
	}
}


///////////////////////////////////////////////////////////////////////
// Action mfuncSet

void UTADLL mfuncSet (HUTAPB hPB)
{ 
    IUtaInst    hInst    (hPB, "mfunc");
	char szCmd[80];

    if (! hUtaInstIsValid(hInst))  return;
    
    sprintf (szCmd, "*OPC?\n");
	UtaTrace (szCmd);
    vStatus = viPrintf (vSession, szCmd);
    vStatus = viScanf (vSession, "%*t");
	if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
	}
}


///////////////////////////////////////////////////////////////////////
// Action mfuncIsSet

void UTADLL mfuncIsSet (HUTAPB hPB)
{
    IUtaInst    hInst    (hPB, "mfunc");
	char  szCmd[80];

    if (! hUtaInstIsValid(hInst))  return;
    
    sprintf (szCmd, "*OPC?\n");
	UtaTrace (szCmd);
    vStatus = viPrintf (vSession, szCmd);
	if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
	}
    vStatus = viScanf (vSession, "%t", szCmd);
	UtaTrace (szCmd);
	if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
	}
}


///////////////////////////////////////////////////////////////////////
// Action mfuncDefineDigIO

void UTADLL mfuncDefineDigIO (HUTAPB hPB)
{
	long	lChannel;
	long	lSlope;
	int		iSlot;
	int		iPort;
	char	szCmd[80];

	IUtaInst    hInst    (hPB, "mfunc");
	UtaPbGetInt32  (hPB, "Slope", &lSlope);

    lChannel = GetChannelFromPB (hPB, "Channel");
    if (lChannel <= 0) return;
	if (! IsDigChannel (lChannel)) return;
    if (! hUtaInstIsValid (hInst))  return;

	// Set the global variable 
	iSlot = lChannel / 100;
	iPort = lChannel % 10;
	userHandle->nSlope[iPort][iSlot] = (unsigned char) lSlope;
	if (lSlope)
		sprintf (szCmd, "Sour:Dig:Data:Byte 0,(@%d)\n", lChannel);
	else
		sprintf (szCmd, "Sour:Dig:Data:Byte 255,(@%d)\n", lChannel);

	UtaTrace (szCmd);
	vStatus = viPrintf (vSession, szCmd);
	if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
	}

/*
	// For Debugging
	{
		char txt[256];
		
		wsprintf (txt, "");
		for (iSlot = 0; iSlot <= 3; iSlot++)
			for (iPort = 0; iPort <=2; iPort++)
				wsprintf (&txt[strlen (txt)], "Slot %ld, Port %ld: %ld\n", iSlot, iPort, userHandle->nSlope[iPort][iSlot]);
		MessageBox (txt);
	}
*/
}
