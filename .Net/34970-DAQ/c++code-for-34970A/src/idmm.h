////////////////////////////////////////////////////////////////////////
//
// File: idmm.h
//
// Description: Define idmm specific parameters and limits, and 
// the structure used to track the state of an instance of the Handler. 
//
////////////////////////////////////////////////////////////////////////
#ifndef __IDMM_H__
#define __IDMM_H__

#define HWHIDMM_ID		34970     

#define	IDMM_TIMEOUT	5000


// IDMM functions
#define IDMM_DCV         0
#define IDMM_ACV         1
#define IDMM_DCC         2
#define IDMM_ACC         3
#define IDMM_RES         4
#define IDMM_FRES        5
#define IDMM_FREQ        6
#define IDMM_PER         7

#define IDMM_TC_J       10
#define IDMM_TC_K       11
#define IDMM_TC_N       12
#define IDMM_TC_R       13
#define IDMM_TC_S       14
#define IDMM_TC_T       15





// IDMM trigger sources
#define IDMM_TRIG_BUS         0
#define IDMM_TRIG_IMM         1
#define IDMM_TRIG_EXT         2
#define	IDMM_TRIG_TIM         3
#define IDMM_TRIG_ALARM1      4
#define IDMM_TRIG_ALARM2      5
#define IDMM_TRIG_ALARM3      6
#define IDMM_TRIG_ALARM4      7



// Error codes returned by lower-level functions.
#define     IDMM_SUCCESS              0
#define     IDMM_ERR_BAD_PARM         -1
#define     IDMM_ERR_IO               -2
#define		IDMM_ERR_NO_TRIGGER		  -3
#define     IDMM_ERR_BIT_TIMEOUT      -4



// hwh34970_idmm Actions
extern "C" {

	void UTADLL idmmAbort				(HUTAPB hPB);
	void UTADLL idmmConfigure			(HUTAPB hPB);
	void UTADLL idmmConfigureScanList	(HUTAPB hPB);
	void UTADLL idmmError				(HUTAPB hPB);
	void UTADLL idmmDataPoints			(HUTAPB hPB);
	void UTADLL idmmGetReading			(HUTAPB hPB);
	void UTADLL idmmGetReadings			(HUTAPB hPB);
	void UTADLL idmmInitiate			(HUTAPB hPB);
	void UTADLL idmmIsSet				(HUTAPB hPB); 
	void UTADLL idmmMeasure				(HUTAPB hPB);
	void UTADLL idmmMeasureScanList		(HUTAPB hPB);
	void UTADLL idmmTrigger				(HUTAPB hPB);
	void UTADLL	idmmGetStatistic		(HUTAPB hPB);

	void UTADLL Win32Sleep (HUTAPB hPB);


	// in File io_util.cpp
	char *VisaErrorMessage (ViStatus VisaError);
}

#endif  // ifndef __IDMM_H__
