// File: idmm.cpp
//
// All actions that belong to internal DMM of HP34970A


#include "stdlib.h"
#include "string.h"
#include "uta.h"
#include "utaapi.h"
#include "pubapi.h"
#include "visa.h"
#include "windows.h"

#include "idmm.h"
#include "a_switch.h"


///////////////////////////////////////////////////////////////////////
// File globals:
ViSession       vSession;
ViStatus        vStatus;
InstParmStruct  *userHandle = NULL;        
int             iDmmUsage;



////////////////////////////////////////////////////////////////////////
// HUTAINST validation
static BOOL hUtaInstIsValid (HUTAINST inst)
{
    // Make sure the userHandle in the "UtaInst" data type is valid, 
    // and that its associated VISA session id exists.
	// userHandle = (INSTSTATE*) UtaInstGetUserHandle (inst);

  
	userHandle = (InstParmStruct*) UtaInstGetUserHandle (inst);
    if ((userHandle) == NULL)
	{
        UtaExcRaiseUserError (
	      "Invalid Instrument Handle passed to HP34970A.", 9);
        return FALSE;
    }
    if ((vSession = userHandle->vi) == NULL)
	{
        UtaExcRaiseUserError ("No VISA session passed to HP34970A.", 9);
        return FALSE;
    }

	if (DemoTimeElapsed (userHandle->nDemoTic)) return FALSE;
 
/*
    // Make sure the userHandle was created for a dmm
    if (userHandle->ulHandlerID != HWHIDMM_ID)
	{
        UtaExcRaiseUserError ("Invalid Instrument ID passed to HP34970A.", 9);
        return FALSE;
    }
*/

    // Get the Slot Number
    UtaInstHPIBGetValues (inst,			// HUTAINST hInst,
						  NULL,			// LPCSTR *instName UTACPP_DEFAULT(NULL),
						  NULL,			// LPCSTR *modelName UTACPP_DEFAULT(NULL),
						  0,			// int    *selectCode UTACPP_DEFAULT(NULL),
						  0,			// int    *primAddr UTACPP_DEFAULT(NULL),
						  0,			// int    *secAddr UTACPP_DEFAULT(NULL),
						  NULL,			// LPCSTR *idQueryStr UTACPP_DEFAULT(NULL),
						  NULL,			// LPCSTR *returnStr UTACPP_DEFAULT(NULL),
						  &iDmmUsage,	// int    *subInstNum UTACPP_DEFAULT(NULL),
						  NULL,			// void   **userHandle UTACPP_DEFAULT(NULL),
						  NULL,			// LPCSTR *rootHandlerName UTACPP_DEFAULT(NULL),
						  NULL);		// LPCSTR *handlerDLLName UTACPP_DEFAULT(NULL));
/*
	if (strstr (pModel, "34970") == NULL)
	{
		UtaExcRaiseUserError ("The selected instrument is not HP34970A", 9);
		return FALSE;
	}
*/
	if (iDmmUsage)
	{
		return TRUE;
	}
	else
	{
		UtaExcRaiseUserError ("The internal DMM is not configured.", 9);
		return FALSE;
	}
}


extern "C" int CheckInstError (ViSession vSession)
{
	char  szError[80];
    int   iError;

	UtaTrace ("SYST:ERR?\n");
   	vStatus = viPrintf (vSession, "SYST:ERR?\n");
	vStatus = viScanf (vSession, "%t", szError);
	UtaTrace (szError);
    iError = atoi (szError);
	if (iError != 0)
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		//VisaErrorMessage (vStatus);
	return iError;
}


////////////////////////////////////////////////////////////////////////
/*
    long GetChannelFromPB (HUTAPB hPB)
    - The parameter string in the block is "Path" and is defined in the umd files.
    - Return value for an invalid channel is <= 0 
 */
extern "C" long GetChannelFromPB (HUTAPB hPB, char *ParName)
{
	char		szNode[80];
	HUTAPATH	hPath, hLocalPath;
	HUTANODE	hNode;


	// Get Handle for local struct userHandle
    if ((userHandle) == NULL)
	{
		HUTAINST hInst;
	    
		// Call from mfunc
		hInst = UtaPbGetInst (hPB, "mfunc");
		// Call from idmm
		if (hInst == NULL)
			hInst = UtaPbGetInst (hPB, "idmm");
		// Call from sys
		if (hInst == NULL)
			hInst = UtaPbGetInst (hPB, "sys");

		userHandle = (InstParmStruct*) UtaInstGetUserHandle (hInst);
	}


	// New Method: Get the channel number using UtaPathConnect, which
	//				calls the function SetPosition in a_switch.

	hPath = UtaPbGetPath (hPB, ParName);
	if (hPath == NULL) return 0; // MessageBox ("No Path found!");
	strcpy (szNode, UtaPathGetNodeName (hPath, 1));

	hNode	   = UtaNodeCreate (szNode);
	hLocalPath = UtaPathCreate ();
	UtaPathAddNode (hLocalPath, szNode); 
    if (UtaNodeGetAdjacentNodeCount (hNode))
	{
		strcpy (szNode, UtaNodeGetAdjacentNodeName (hNode, 1));
		UtaPathAddNode (hLocalPath, szNode);    
	}

	// Set the flag bSetPosQuery for not doing any output to instrument
	// Call UtaPathConnect. 
	// This function calls the SetPosition in a_switch
	// and writes the the channel number to the global variable nSetPosChannel;  
	userHandle->nSetPosChannel = 0;
	userHandle->bSetPosQuery = TRUE;
	UtaPathConnect (hLocalPath, TRUE);
	return (long) userHandle->nSetPosChannel;


#if GET_CHANNEL_FROM_PATHSTRING
	//////////////////////////////////////////////////////////////////////
	// Old Method: extract the channel number from path string
	//
    char	szPath[80], *pPath;
	long	lChannel = 0;
	int		iModel, iSlot;

	// Get Handle for local struct userHandle
    if ((userHandle) == NULL)
	{
		HUTAINST hInst;
	    
		// Call from mfunc
		hInst = UtaPbGetInst (hPB, "mfunc");
		// Call from idmm
		if (hInst == NULL)
			hInst = UtaPbGetInst (hPB, "idmm");
		// Call from sys
		if (hInst == NULL)
			hInst = UtaPbGetInst (hPB, "sys");

		userHandle = (InstParmStruct*) UtaInstGetUserHandle (hInst);
	}


	HUTASTRING hData;
	hData = (HUTASTRING) UtaPbFindData (hPB, ParName);
	if (hData != NULL)
	{
		strcpy (szPath, UtaStringGetValue (hData));
    }
	else
	{
	    UtaExcRaiseUserError ("Path missing", 9);
		return 0;
	}

	// Get substring which contains the channel number, e.g. "[Box:Ch101]" or "Box:Ch304.D/A"
    pPath = strstr (szPath, ":Ch");
    if (pPath == NULL)
	{
	    UtaExcRaiseUserError ("Path missing or incorrect (Node should be \":ChXXX\")", 9);
		return 0;
	}

    //::MessageBox (NULL, pPath, "GetChannelFromPB", MB_OK);
	lChannel = atol (pPath + 3);


	// Get the card model of the channel
	iSlot = lChannel / 100;
	switch (iSlot)
	{
	case 1: iModel = userHandle->nModel1; break;
	case 2: iModel = userHandle->nModel2; break;
	case 3: iModel = userHandle->nModel3; break;
	default:	
		    UtaExcRaiseUserError ("Path incorrect (wrong channel number)", 9);
			return 0;
	}

	
	// Only the multiplexer 34901, 34902 and 34908 can be used with internal DMM
	// and the digital-I/O channels of 34907 are used as parameter for switching
	switch (iModel)
	{
	case 34901: // 20 Ch Mux
	case 34902:	// 16 Ch Mux
	case 34908:	// 40 Ch Mux
	case 34907:	// MFunc
				return lChannel;
	default:  
				UtaExcRaiseUserError ("Path incorrect for card", 9);
				return 0;
	}
#endif

}


////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
// hwh4970a idmm Actions
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////
// hwh4970a idmmError

void UTADLL idmmError (HUTAPB hPB)
{
	char  szCmd[80];
    int   iError;

    IUtaInst    hInst    (hPB, "idmm");
    if (! hUtaInstIsValid(hInst))  return;

   	sprintf (szCmd, "SYST:ERR?\n");
	UtaTrace (szCmd);
   	vStatus = viPrintf (vSession, szCmd);
	if (vStatus != VI_SUCCESS)
	{
	    UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
	}

	vStatus = viScanf (vSession, "%t", szCmd);
	UtaTrace (szCmd);
	sscanf (szCmd, "%ld", &iError);
	if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
	}
	// Mask out the last " and CR+LF
	UtaPbSetInt32 (hPB, "Error", iError);
	UtaPbSetString (hPB, "ErrorMessage", szCmd);
}


////////////////////////////////////////////////////////////////////////
// hwh4970a idmmAbort

void UTADLL idmmAbort (HUTAPB hPB)
{
	char szCmd[80];

    IUtaInst    hInst    (hPB, "idmm");
    if (! hUtaInstIsValid(hInst))  return;

	if (userHandle->nBaudrate > 0)
	 	sprintf (szCmd, "\003\n"); // Ctrl-C
	else
   		sprintf (szCmd, "ABORT\n");

	UtaTrace (szCmd);
	vStatus = viPrintf (vSession, szCmd);
	if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
	}
	CheckInstError (vSession);
}


/* Format settings for reading only the result values 
   = default after *RST
   
   FORMAT:READING:ALARM OFF
   FORMAT:READING:CHANNEL OFF
   FORMAT:READING:TIME OFF
   FORMAT:READING:UNIT OFF
*/

////////////////////////////////////////////////////////////////////////
// hwh4970a idmmIsSet

void UTADLL idmmIsSet (HUTAPB hPB)
{

	char szCmd[80];

	IUtaInst    hInst    (hPB, "idmm");
    if (! hUtaInstIsValid(hInst))  return;
    
	sprintf (szCmd, "*OPC?\n");
	UtaTrace (szCmd);
    vStatus = viPrintf (vSession, szCmd);
	if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
	}

    vStatus = viScanf (vSession, "%t", szCmd);
	UtaTrace (szCmd);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }
}


////////////////////////////////////////////////////////////////////////
// hwh4970a idmmConfigure

void UTADLL idmmConfigure (HUTAPB hPB)
{
	IUtaInst    hInst    (hPB, "idmm");
    if (! hUtaInstIsValid(hInst))  return;

	long   lFunction    = 0;
	long   lChannel     = 0;
	double dRange       = 0;
	double dResolution  = 0;
    char   szCmd[80], *pCmd;


	UtaPbGetInt32  (hPB, "Function", &lFunction); 
	UtaPbGetReal64 (hPB, "Range", &dRange);
	UtaPbGetReal64 (hPB, "Resolution", &dResolution);

    lChannel = GetChannelFromPB (hPB, "Channel");
    if (lChannel <= 0) return;

/****

	// Check for correct channel

	if (lModule == 34901)
		if (lFunction == IDMM_DCC || lFunction == IDMM_ACC)
			if ((lChannel % 100 != 21) || (lChannel % 100 != 22))
			{
				UtaExcRaiseUserError ("Path incorrect for this function", 9);
				return;
			}
		else
			if ((lChannel % 100 == 21) || (lChannel % 100 == 22))
			{
				UtaExcRaiseUserError ("Path incorrect for this function", 9);
				return;
			}
*****/

	switch (lFunction)
	{
	case IDMM_DCV:  pCmd = "CONF:VOLT:DC"; break;
	case IDMM_ACV:  pCmd = "CONF:VOLT:AC"; break;
    case IDMM_DCC:  pCmd = "CONF:CURR:DC"; break;
    case IDMM_ACC:  pCmd = "CONF:CURR:AC"; break;
    case IDMM_RES:  pCmd = "CONF:RES"; break;
    case IDMM_FRES: pCmd = "CONF:FRES"; break;
    case IDMM_FREQ: pCmd = "CONF:FREQ"; break;
    case IDMM_PER:  pCmd = "CONF:PER"; break;
    case IDMM_TC_J: pCmd = "CONF:TEMP TC,J,"; break;
    case IDMM_TC_K: pCmd = "CONF:TEMP TC,K,"; break;
    case IDMM_TC_N: pCmd = "CONF:TEMP TC,N,"; break;
    case IDMM_TC_R: pCmd = "CONF:TEMP TC,R,"; break;
    case IDMM_TC_S: pCmd = "CONF:TEMP TC,S,"; break;
    case IDMM_TC_T: pCmd = "CONF:TEMP TC,T,"; break;
	default:   
		       return;
	}


	// Construct the command string to HP34790A
    sprintf (szCmd, "%s ", pCmd);
	// add the range parameter for func TC
	if (lFunction >= IDMM_TC_J)
  	   sprintf (&szCmd[strlen (szCmd)], "1,");  // 1V Ranges
	// add the range parameter for other funcs
	else if (dRange == 0.0)  
  	   sprintf (&szCmd[strlen (szCmd)], "AUTO,");
	else  
  	   sprintf (&szCmd[strlen (szCmd)], "%lf,", dRange);
    // add the resolution parameter
	if (dResolution == 0.0)
  	   sprintf (&szCmd[strlen (szCmd)], "DEF,");
	else
  	   sprintf (&szCmd[strlen (szCmd)], "%lf,", dResolution);
    // add the chennel list and the \n
	if (lChannel > 0)
	   sprintf (&szCmd[strlen(szCmd)], "(@%ld)\n", lChannel);


	// Send the command
	UtaTrace (szCmd);
	vStatus = viPrintf (vSession, szCmd);
    if (vStatus != VI_SUCCESS)
	{
        UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
    }
	CheckInstError (vSession);
}


////////////////////////////////////////////////////////////////////////
// hwh4970a idmmConfigureScanList

void UTADLL idmmConfigureScanList (HUTAPB hPB)
{
	IUtaInst    hInst    (hPB, "idmm");
    if (! hUtaInstIsValid(hInst))  return;

	long   lFunction    = 0;
	long   lChan_first  = 0;
	long   lChan_last   = 0;
	double dRange       = 0;
	double dResolution  = 0;
    char   szCmd[80], *pCmd;


	UtaPbGetInt32  (hPB, "Function", &lFunction); 
	UtaPbGetReal64 (hPB, "Range", &dRange);
	UtaPbGetReal64 (hPB, "Resolution", &dResolution);

    lChan_first = GetChannelFromPB (hPB, "Chan_first");
    lChan_last = GetChannelFromPB (hPB, "Chan_last");

    if (lChan_first <= 0 || lChan_last <= 0)
	{
        UtaExcRaiseUserError ("Incorrect channel in parameter(s) (Chan_first, Chan_last)", 9);
		return;
	}
    if (lChan_last < lChan_first)
	{
        UtaExcRaiseUserError ("Incorrect Channels (Chan_first > Chan_last)", 9);
		return;
	}
	if (lChan_first / 100 != lChan_last / 100)
	{
        UtaExcRaiseUserError ("The scan list can only used for one mux", 9);
		return;
	}

	switch (lFunction)
	{
	case IDMM_DCV:  pCmd = "CONF:VOLT:DC"; break;
	case IDMM_ACV:  pCmd = "CONF:VOLT:AC"; break;
    case IDMM_DCC:  pCmd = "CONF:CURR:DC"; break;
    case IDMM_ACC:  pCmd = "CONF:CURR:AC"; break;
    case IDMM_RES:  pCmd = "CONF:RES"; break;
    case IDMM_FRES: pCmd = "CONF:FRES"; break;
    case IDMM_FREQ: pCmd = "CONF:FREQ"; break;
    case IDMM_PER:  pCmd = "CONF:PER"; break;
    case IDMM_TC_J: pCmd = "CONF:TEMP TC,J,"; break;
    case IDMM_TC_K: pCmd = "CONF:TEMP TC,K,"; break;
    case IDMM_TC_N: pCmd = "CONF:TEMP TC,N,"; break;
    case IDMM_TC_R: pCmd = "CONF:TEMP TC,R,"; break;
    case IDMM_TC_S: pCmd = "CONF:TEMP TC,S,"; break;
    case IDMM_TC_T: pCmd = "CONF:TEMP TC,T,"; break;
	default:   
		       return;
	}


	// Construct the command string to HP34790A
    sprintf (szCmd, "%s ", pCmd);
	// add the range parameter for func TC
	if (lFunction >= IDMM_TC_J)
  	   sprintf (&szCmd[strlen (szCmd)], "1,");  // 1V Ranges
	// add the range parameter for other funcs
	else if (dRange == 0.0)  
  	   sprintf (&szCmd[strlen (szCmd)], "AUTO,");
	else  
  	   sprintf (&szCmd[strlen (szCmd)], "%lf,", dRange);
    // add the resolution parameter
	if (dResolution == 0.0)
  	   sprintf (&szCmd[strlen (szCmd)], "DEF,");
	else
  	   sprintf (&szCmd[strlen (szCmd)], "%lf,", dResolution);
    // add the chennel list and the \n
	if ((lChan_first > 0) && (lChan_last > 0))
	   sprintf (&szCmd[strlen(szCmd)], "(@%ld:%ld)\n", lChan_first, lChan_last);


	// Send the command
	UtaTrace (szCmd);
	vStatus = viPrintf (vSession, szCmd);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }
	CheckInstError (vSession);
}


////////////////////////////////////////////////////////////////////////
// hwh4970a idmmMeasure

void UTADLL idmmMeasure (HUTAPB hPB)
{
	IUtaInst    hInst    (hPB, "idmm");
    if (! hUtaInstIsValid(hInst))  return;

	long   lFunction    = 0;
	long   lChannel     = 0;
	double dRange       = 0;
	double dResolution  = 0;
	double dResult;
    char   szCmd[80], *pCmd;


	UtaPbGetInt32  (hPB, "Function", &lFunction); 
	UtaPbGetReal64 (hPB, "Range", &dRange);
	UtaPbGetReal64 (hPB, "Resolution", &dResolution);


    lChannel = GetChannelFromPB (hPB, "Channel");
    if (lChannel <= 0) return;

	switch (lFunction)
	{
	case IDMM_DCV:  pCmd = "MEAS:VOLT:DC?"; break;
	case IDMM_ACV:  pCmd = "MEAS:VOLT:AC?"; break;
    case IDMM_DCC:  pCmd = "MEAS:CURR:DC?"; break;
    case IDMM_ACC:  pCmd = "MEAS:CURR:AC?"; break;
    case IDMM_RES:  pCmd = "MEAS:RES?"; break;
    case IDMM_FRES: pCmd = "MEAS:FRES?"; break;
    case IDMM_FREQ: pCmd = "MEAS:FREQ?"; break;
    case IDMM_PER:  pCmd = "MEAS:PER?"; break;
    case IDMM_TC_J: pCmd = "MEAS:TEMP? TC,J,"; break;
    case IDMM_TC_K: pCmd = "MEAS:TEMP? TC,K,"; break;
    case IDMM_TC_N: pCmd = "MEAS:TEMP? TC,N,"; break;
    case IDMM_TC_R: pCmd = "MEAS:TEMP? TC,R,"; break;
    case IDMM_TC_S: pCmd = "MEAS:TEMP? TC,S,"; break;
    case IDMM_TC_T: pCmd = "MEAS:TEMP? TC,T,"; break;
	default:        return;
	}


	if (lFunction == IDMM_ACV)
	{
		UtaTrace ("Set Visa Timeout to 10 sec\n");
		vStatus = viSetAttribute (vSession, VI_ATTR_TMO_VALUE, 10000); //
		if (vStatus != VI_SUCCESS)
		{	 
			UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
			return;
		}
	}

	// Construct the command string to HP34790A
    sprintf (szCmd, "%s ", pCmd);
	// add the range parameter for func TC
	if (lFunction >= IDMM_TC_J)
  	   sprintf (&szCmd[strlen (szCmd)], "1,");  // 1V Ranges
	// add the range parameter for other funcs
	else if (dRange == 0.0)  
  	   sprintf (&szCmd[strlen (szCmd)], "AUTO,");
	else  
  	   sprintf (&szCmd[strlen (szCmd)], "%lf,", dRange);
    // add the resolution parameter
	if (dResolution == 0.0)
  	   sprintf (&szCmd[strlen (szCmd)], "DEF,");
	else
  	   sprintf (&szCmd[strlen (szCmd)], "%lf,", dResolution);
    // add the chennel list and the \n
	if (lChannel > 0)
	   sprintf (&szCmd[strlen(szCmd)], "(@%ld)\n", lChannel);

	// Send the command
    UtaTrace (szCmd);
	vStatus = viPrintf (vSession, szCmd);
    if (vStatus != VI_SUCCESS)
	{
        UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }
	// read the result
	dResult = 0;
	vStatus = viScanf (vSession, "%t", szCmd);
	UtaTrace (szCmd);
	sscanf (szCmd, "%lf", &dResult);
    if (vStatus != VI_SUCCESS)
	{
	    UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
	    return;
    }

	// Set the result in the parameter block
	UtaPbSetReal64 (hPB, "Result", dResult);

	if (lFunction == IDMM_ACV)
	{
		UtaTrace ("Set Visa Timeout to default value\n");
		vStatus = viSetAttribute (vSession, VI_ATTR_TMO_VALUE, 5000); //
		if (vStatus != VI_SUCCESS)
		{	 
			UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
			return;
		}
	}

	CheckInstError (vSession);
}



////////////////////////////////////////////////////////////////////////
// hwh4970a idmmMeasureScanList

void UTADLL idmmMeasureScanList (HUTAPB hPB)
{
	IUtaInst    hInst    (hPB, "idmm");
    if (! hUtaInstIsValid(hInst))  return;

	long   lFunction    = 0;
	long   lChan_first  = 0;
	long   lChan_last   = 0;
	double dRange       = 0;
	double dResolution  = 0;
    char   szCmd[80], *pCmd;


	UtaPbGetInt32  (hPB, "Function", &lFunction); 
	UtaPbGetReal64 (hPB, "Range", &dRange);
	UtaPbGetReal64 (hPB, "Resolution", &dResolution);
    HUTAR64ARR hReadings = UtaPbGetReal64Array (hPB, "Readings");


    lChan_first = GetChannelFromPB (hPB, "Chan_first");
    lChan_last  = GetChannelFromPB (hPB, "Chan_last");

    if (lChan_first <= 0 || lChan_last <= 0)
	{
        UtaExcRaiseUserError ("Incorrect channel in parameter(s) (Chan_first, Chan_last)", 9);
		return;
	}
    if (lChan_last < lChan_first)
	{
        UtaExcRaiseUserError ("Incorrect Channels (Chan_first > Chan_last)", 9);
		return;
	}

	if (lChan_first / 100 != lChan_last / 100)
	{
        UtaExcRaiseUserError ("The scan list can only be used for one mux", 9);
		return;
	}

	switch (lFunction)
	{
	case IDMM_DCV:  pCmd = "MEAS:VOLT:DC?"; break;
	case IDMM_ACV:  pCmd = "MEAS:VOLT:AC?"; break;
    case IDMM_DCC:  pCmd = "MEAS:CURR:DC?"; break;
    case IDMM_ACC:  pCmd = "MEAS:CURR:AC?"; break;
    case IDMM_RES:  pCmd = "MEAS:RES?"; break;
    case IDMM_FRES: pCmd = "MEAS:FRES?"; break;
    case IDMM_FREQ: pCmd = "MEAS:FREQ?"; break;
    case IDMM_PER:  pCmd = "MEAS:PER?"; break;
    case IDMM_TC_J: pCmd = "MEAS:TEMP? TC,J,"; break;
    case IDMM_TC_K: pCmd = "MEAS:TEMP? TC,K,"; break;
    case IDMM_TC_N: pCmd = "MEAS:TEMP? TC,N,"; break;
    case IDMM_TC_R: pCmd = "MEAS:TEMP? TC,R,"; break;
    case IDMM_TC_S: pCmd = "MEAS:TEMP? TC,S,"; break;
    case IDMM_TC_T: pCmd = "MEAS:TEMP? TC,T,"; break;
	default:        return;
	}


	// Construct the command string to HP34790A
    sprintf (szCmd, "%s ", pCmd);
	// add the range parameter for func TC
	if (lFunction >= IDMM_TC_J)
  	   sprintf (&szCmd[strlen (szCmd)], "1,");  // 1V Ranges
	// add the range parameter for other funcs
	else if (dRange == 0.0)  
  	   sprintf (&szCmd[strlen (szCmd)], "AUTO,");
	else  
  	   sprintf (&szCmd[strlen (szCmd)], "%lf,", dRange);
    // add the resolution parameter
	if (dResolution == 0.0)
  	   sprintf (&szCmd[strlen (szCmd)], "DEF,");
	else
  	   sprintf (&szCmd[strlen (szCmd)], "%lf,", dResolution);
    // add the chennel list and the \n
	if ((lChan_first > 0) && (lChan_last > 0))
 	   sprintf (&szCmd[strlen(szCmd)], "(@%ld:%ld)\n", lChan_first, lChan_last);

	// Send the command
    UtaTrace (szCmd);
	vStatus = viPrintf (vSession, szCmd);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }


	// Read the measurements
	{
		double	*dReadings = new double[lChan_last - lChan_first + 1];
		int		i;
		char	szFmt[32];

		sprintf (szFmt, "%%,%dlf%*t", lChan_last - lChan_first + 1);
		vStatus = viScanf (vSession, szFmt, dReadings);
		if (vStatus != VI_SUCCESS)
		{
			delete dReadings;
			UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
			return;
		}

		for (i = 0; i < (lChan_last - lChan_first + 1); i++)
		{
			UtaR64ArrSetAt1 (hReadings, i, dReadings[i]);
		}
		delete dReadings;
	}
}



////////////////////////////////////////////////////////////////////////
// hwh4970a idmmTrigger

void UTADLL idmmTrigger (HUTAPB hPB)
{
    IUtaInst    hInst    (hPB, "idmm");
    if (! hUtaInstIsValid(hInst))  return;

    long    lTrigSource = 1;
	long    lTrigCount  = 1;
    double  dTrigTimer;
	char    *pCmd;
	char	szCmd[80];


	UtaPbGetInt32  (hPB, "Source", &lTrigSource); 
	UtaPbGetReal64 (hPB, "Timer", &dTrigTimer); 
	UtaPbGetInt32  (hPB, "Count", &lTrigCount); 

	switch (lTrigSource)
	{
	case IDMM_TRIG_BUS:    pCmd = "BUS";    break;
	case IDMM_TRIG_IMM:    pCmd = "IMM";    break;
	case IDMM_TRIG_EXT:    pCmd = "EXT";    break;
	case IDMM_TRIG_TIM:    pCmd = "TIM";    break;
	case IDMM_TRIG_ALARM1: pCmd = "ALARM1"; break;
	case IDMM_TRIG_ALARM2: pCmd = "ALARM2"; break;
	case IDMM_TRIG_ALARM3: pCmd = "ALARM3"; break;
	case IDMM_TRIG_ALARM4: pCmd = "ALARM4"; break;
	default: 
				return;
	}

	// Send the command
	sprintf (szCmd, "TRIG:SOURCE %s\n", pCmd);
	UtaTrace (szCmd);
	vStatus = viPrintf (vSession, szCmd);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }

	sprintf (szCmd, "TRIG:COUNT %ld\n", lTrigCount);
	UtaTrace (szCmd);
	vStatus = viPrintf (vSession, szCmd);

	if ((vStatus == VI_SUCCESS) && (lTrigSource == IDMM_TRIG_TIM) && (dTrigTimer > 0.0))
	{
		sprintf (szCmd, "TRIG:TIMER %lf\n", dTrigTimer);
		UtaTrace (szCmd);
  		vStatus = viPrintf (vSession, szCmd);
	}

    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }
	CheckInstError (vSession);
}


////////////////////////////////////////////////////////////////////////
// hwh4970a idmmInitiate

void UTADLL idmmInitiate (HUTAPB hPB)
{
    IUtaInst    hInst    (hPB, "idmm");
    if (! hUtaInstIsValid(hInst))  return;

	char *pCmd = "INIT\n";
 
	UtaTrace (pCmd);
	vStatus = viPrintf (vSession, pCmd);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
    }
}


////////////////////////////////////////////////////////////////////////
// hwh4970a idmmGetReading

void UTADLL idmmGetReading (HUTAPB hPB)
{
    double dReading;
	char   szCmd[80];

    IUtaInst    hInst    (hPB, "idmm");
    if (! hUtaInstIsValid(hInst))  return;

	sprintf (szCmd, "FETCH?\n");
	UtaTrace (szCmd);
	vStatus = viPrintf (vSession, szCmd); 
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
	}

	vStatus = viScanf (vSession, "%t", szCmd);
	UtaTrace (szCmd);
	sscanf (szCmd, "%d", &dReading);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
	}

	UtaPbSetReal64 (hPB, "Reading", dReading);
}


 
////////////////////////////////////////////////////////////////////////
// hwh4970a idmmGetReadings

void UTADLL idmmGetReadings(HUTAPB hPB)
{
	int  points;
    char szFmt[64];
	char szCmd[80];

    IUtaInst    hInst    (hPB, "idmm");
    if (! hUtaInstIsValid(hInst))  return;

    HUTAR64ARR hReadings = UtaPbGetReal64Array (hPB, "Readings");
 
	sprintf (szCmd, "DATA:POINTS?\n");
	UtaTrace (szCmd);
	vStatus = viPrintf (vSession, szCmd);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
    }

    vStatus = viScanf (vSession, "%t", szCmd);
	UtaTrace (szCmd);
	sscanf (szCmd, "%ld", &points);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
		return;
    }

	if (points < 1)
	{
		UtaExcRaiseUserError ("No data in instrument available (Points=0)", 9 );
	}
	else
	{
	    double *dReadings = new double[points];
		int    i;

		sprintf (szCmd, "DATA:REMOVE? %ld\n", points); // Reads and clears the readings
		UtaTrace (szCmd);
		vStatus = viPrintf (vSession, szCmd);
        if (vStatus != VI_SUCCESS)
		{
			delete dReadings;
			UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
			return;
		}

		sprintf (szFmt, "%%,%dlf%*t", points);
		vStatus = viScanf (vSession, szFmt, dReadings);
        if (vStatus != VI_SUCCESS)
		{
			delete dReadings;
			UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
			return;
		}

		for (i = 0; i < points; i++)
		{
			UtaR64ArrSetAt1 (hReadings, i, dReadings[i]);
		}
		delete dReadings;
	}
}


////////////////////////////////////////////////////////////////////////
// hwh4970a idmmDataPoints

void UTADLL idmmDataPoints (HUTAPB hPB)
{

	long  lPoints;
	char  szCmd[80];

	UtaPbGetInt32 (hPB, "Points", &lPoints);
    IUtaInst    hInst    (hPB, "idmm");
	if (! hUtaInstIsValid(hInst))  return;


	sprintf (szCmd, "DATA:POINTS?\n");
	UtaTrace (szCmd);
	vStatus = viPrintf (vSession, szCmd);
    if (vStatus != VI_SUCCESS)
	{
	    UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }

    vStatus = viScanf (vSession, "%t", szCmd);
	UtaTrace (szCmd);
	sscanf (szCmd, "%ld", &lPoints);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }
	UtaPbSetInt32 (hPB, "Points", lPoints);
}


////////////////////////////////////////////////////////////////////////
// hwh4970a idmmGetStatistic

void UTADLL idmmGetStatistic (HUTAPB hPB)
{
	long   lType;
	long   lChannel;
	double dValue;
	char   szCmd[80];


	IUtaInst    hInst    (hPB, "idmm");
    if (! hUtaInstIsValid(hInst))  return;

	UtaPbGetInt32 (hPB, "type", &lType);
    lChannel = GetChannelFromPB (hPB, "Channel");

    if (lChannel <= 0)
	{
        UtaExcRaiseUserError ("Incorrect channel in parameter Channel", 9);
		return;
	}

	switch (lType)
	{
	case 0: sprintf (szCmd, "CALC:AVER:CLEAR (@%d);*OPC?\n", lChannel); break;
	case 1: sprintf (szCmd, "CALC:AVER:AVER? (@%d)\n", lChannel); break;
	case 2: sprintf (szCmd, "CALC:AVER:COUNT? (@%d)\n", lChannel); break;
	case 3: sprintf (szCmd, "CALC:AVER:MAX? (@%d)\n", lChannel); break;
	case 4: sprintf (szCmd, "CALC:AVER:MAX:TIME? (@%d)\n", lChannel); break;
	case 5: sprintf (szCmd, "CALC:AVER:MIN? (@%d)\n", lChannel); break;
	case 6: sprintf (szCmd, "CALC:AVER:MIN:TIME? (@%d)\n", lChannel); break;
	case 7: sprintf (szCmd, "CALC:AVER:PTPEAK? (@%d)\n", lChannel); break;
	}

	UtaTrace (szCmd);
	vStatus = viPrintf (vSession, szCmd);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }

    vStatus = viScanf (vSession, "%t", szCmd);
	UtaTrace (szCmd);
    if (vStatus != VI_SUCCESS)
	{
		UtaExcRaiseUserError (VisaErrorMessage (vStatus), 9);
        return;
    }

	// Create a double from date and time values
	// The format is "YYYYMMDD.hhmmssccc"
	if (lType == 4 || lType == 6)
	{
		int		iYear, iMonth, iDay, iHour, iMin, iSec;

		sscanf (szCmd, "%d%*c%d%*c%d%*c%d%*c%d%*c%d",
			&iYear, &iMonth, &iDay, &iHour, &iMin, &iSec);

		dValue = 0.0;
		dValue += (double) iYear * 10000.0;
		dValue += (double) iMonth * 100.0;
		dValue += (double) iDay;
		dValue += (double) iHour / 100.0;
		dValue += (double) iMin / 10000.0;
		dValue += (double) iSec / 1000000.0;
	}
	else
	{
		sscanf (szCmd, "%lf", &dValue);
	}

	UtaPbSetReal64 (hPB, "value", dValue);
}


////////////////////////////////////////////////////////////////////////
// hwh4970a Win32Sleep

void UTADLL Win32Sleep (HUTAPB hPB)
{
	long msec;

	UtaPbGetInt32 (hPB, "msec", &msec);
	Sleep (msec);
}


