////////////////////////////////////////////////////////////////////////
//
// File: syst.h
//
// Description: Define syst specific parameters and limits, and 
// the structure used to track the state of an instance of the Handler. 
//
////////////////////////////////////////////////////////////////////////

#ifndef __SYST_H__
#define __SYST_H__

extern "C" {
	void UTADLL sysSetDate			(HUTAPB hPB);
	void UTADLL sysGetDate			(HUTAPB hPB);
	void UTADLL sysSetTime			(HUTAPB hPB);
	void UTADLL sysGetTime			(HUTAPB hPB);
	void UTADLL sysSetStatus		(HUTAPB hPB);
	void UTADLL sysGetStatus		(HUTAPB hPB);
	void UTADLL sysLocal			(HUTAPB hPB);
	void UTADLL sysTest				(HUTAPB hPB);
	void UTADLL sysDisplay			(HUTAPB hPB);
	void UTADLL sysTrig				(HUTAPB hPB);
	void UTADLL sysError			(HUTAPB hPB);
	void UTADLL sysRead				(HUTAPB hPB);
	void UTADLL sysSend				(HUTAPB hPB);
	void UTADLL diagGetCycles		(HUTAPB hPB);
	void UTADLL sysReset			(HUTAPB hPB);

	// in File io_util.cpp
	char *VisaErrorMessage (ViStatus VisaError);

}

#endif