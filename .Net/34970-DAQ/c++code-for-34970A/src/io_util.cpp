// File: io_util.cpp

// Utility functions for the handler


#include "visa.h"
#include "uta.h"
#include "pubapi.h"


#define TESTEXECSL


// extern "C" void __declspec (dllexport) CheckVisaError (char *Label)
#if defined __cplusplus
	extern "C" {
#endif


static char ErrorText[256];


//////////////////////////////////////////////////////////////////////////
// Wrapper for trace routine

BOOL	UTADLL	MsgInst_IsTracing(HUTAHWMOD hModule)
{
    return UtaHwModIsTracing(hModule);
}

void	UTADLL	MsgInst_Trace(HUTAHWMOD hModule, LPCSTR lpszMsg)
{
	if (MsgInst_IsTracing(hModule)) UtaHwModTrace(hModule, lpszMsg);
}



/*
	if (MsgInst_IsTracing (UtaInstGetModule (hInst)))
	{
		CString szMsg = "Sent: ";
		szMsg = szMsg + lpszCmd;
		szMsg = szMsg + '\n';
		MsgInst_Trace(UtaInstGetModule(hInst), szMsg);
	};
*/


//////////////////////////////////////////////////////////////////////////
// Create Visa Error Message from Error Number

char *VisaErrorMessage (ViStatus VisaError)
{
	char *ErrorMessage;

	switch (VisaError)
	{
	case VI_SUCCESS:					ErrorMessage = ""; break;
	case VI_SUCCESS_EVENT_EN:         	ErrorMessage = ""; break; //"VI_SUCCESS_EVENT_EN"; break; // 1073676290		! (0x3FFF0002L)
	case VI_SUCCESS_EVENT_DIS:        	ErrorMessage = ""; break; //"VI_SUCCESS_EVENT_DIS"; break; //1073676291		! (0x3FFF0003L)	
	case VI_SUCCESS_QUEUE_EMPTY:      	ErrorMessage = ""; break; //"VI_SUCCESS_QUEUE_EMPTY"; break; //1073676292		! (0x3FFF0004L)
	case VI_SUCCESS_TERM_CHAR:        	ErrorMessage = ""; break; //"VI_SUCCESS_TERM_CHAR"; break; //1073676293		! (0x3FFF0005L)
	case VI_SUCCESS_MAX_CNT:          	ErrorMessage = ""; break; //"VI_SUCCESS_MAX_CNT"; break; //1073676294		! (0x3FFF0006L)
	case VI_SUCCESS_DEV_NPRESENT:     	ErrorMessage = ""; break; //"VI_SUCCESS_DEV_NPRESENT"; break; //1073676413		! (0x3FFF007DL)
	case VI_SUCCESS_QUEUE_NEMPTY:     	ErrorMessage = ""; break; //"VI_SUCCESS_QUEUE_NEMPTY"; break; //1073676416		! (0x3FFF0080L)
	case VI_SUCCESS_NESTED_SHARED:    	ErrorMessage = ""; break; //"VI_SUCCESS_NESTED_SHARED"; break; //1073676441		! (0x3FFF0099L)
	case VI_SUCCESS_NESTED_EXCLUSIVE: 	ErrorMessage = ""; break; //"VI_SUCCESS_NESTED_EXCLUSIVE"; break; //1073676442		! (0x3FFF009AL)
	case VI_SUCCESS_SYNC:             	ErrorMessage = ""; break; //"VI_SUCCESS_SYNC"; break; //1073676443		! (0x3FFF009BL)
	case VI_WARN_CONFIG_NLOADED:      	ErrorMessage = "VI_WARN_CONFIG_NLOADED"; break; //1073676407		! (0x3FFF0077L)
	case VI_WARN_NULL_OBJECT:         	ErrorMessage = "VI_WARN_NULL_OBJECT"; break; //1073676418		! (0x3FFF0082L)
	case VI_WARN_NSUP_ATTR_STATE:     	ErrorMessage = "VI_WARN_NSUP_ATTR_STATE"; break; //1073676420		! (0x3FFF0084L)
	case VI_WARN_UNKNOWN_STATUS:      	ErrorMessage = "VI_WARN_UNKNOWN_STATUS"; break; //1073676421		! (0x3FFF0085L)
	case VI_WARN_NSUP_BUF:				ErrorMessage = "VI_WARN_NSUP_BUF"; break; //1073676424		! (0x3FFF0088L)
	
	case VI_ERROR_SYSTEM_ERROR:			ErrorMessage = "VI_ERROR_SYSTEM_ERROR"; break; //=	-1073807360
	case VI_ERROR_INV_OBJECT:			ErrorMessage = "VI_ERROR_INV_OBJECT"; break; //=	-1073807346
	case VI_ERROR_RSRC_LOCKED:			ErrorMessage = "VI_ERROR_RSRC_LOCKED"; break; //=	-1073807345
	case VI_ERROR_INV_EXPR:				ErrorMessage = "VI_ERROR_INV_EXPR"; break; //=	-1073807344
	case VI_ERROR_RSRC_NFOUND:			ErrorMessage = "VI_ERROR_RSRC_NFOUND"; break; //=	-1073807343
	case VI_ERROR_INV_RSRC_NAME:		ErrorMessage = "VI_ERROR_INV_RSRC_NAME"; break; //=	-1073807342
	case VI_ERROR_INV_ACC_MODE:			ErrorMessage = "VI_ERROR_INV_ACC_MODE"; break; //=	-1073807341
	case VI_ERROR_TMO:					ErrorMessage = "VI_ERROR_TMO"; break; //=	-1073807339
	case VI_ERROR_CLOSING_FAILED:		ErrorMessage = "VI_ERROR_CLOSING_FAILED"; break; //=	-1073807338
	case VI_ERROR_INV_DEGREE:			ErrorMessage = "VI_ERROR_INV_DEGREE"; break; //=	-1073807333
	case VI_ERROR_INV_JOB_ID:			ErrorMessage = "VI_ERROR_INV_JOB_ID"; break; //=	-1073807332
	case VI_ERROR_NSUP_ATTR:			ErrorMessage = "VI_ERROR_NSUP_ATTR"; break; //=	-1073807331
	case VI_ERROR_NSUP_ATTR_STATE:		ErrorMessage = "VI_ERROR_NSUP_ATTR_STATE"; break; //=	-1073807330
	case VI_ERROR_ATTR_READONLY:		ErrorMessage = "VI_ERROR_ATTR_READONLY"; break; //=	-1073807329
	case VI_ERROR_INV_LOCK_TYPE:		ErrorMessage = "VI_ERROR_INV_LOCK_TYPE"; break; //=	-1073807328
	case VI_ERROR_INV_ACCESS_KEY:		ErrorMessage = "VI_ERROR_INV_ACCESS_KEY"; break; //=	-1073807327
	case VI_ERROR_INV_EVENT:			ErrorMessage = "VI_ERROR_INV_EVENT"; break; //=	-1073807322
	case VI_ERROR_INV_MECH:				ErrorMessage = "VI_ERROR_INV_MECH"; break; //=	-1073807321
	case VI_ERROR_HNDLR_NINSTALLED:		ErrorMessage = "VI_ERROR_HNDLR_NINSTALLED"; break; //=	-1073807320
	case VI_ERROR_INV_HNDLR_REF:		ErrorMessage = "VI_ERROR_INV_HNDLR_REF"; break; //=	-1073807319
	case VI_ERROR_INV_CONTEXT:			ErrorMessage = "VI_ERROR_INV_CONTEXT"; break; //=	-1073807318
	case VI_ERROR_ABORT:				ErrorMessage = "VI_ERROR_ABORT"; break; //=	-1073807312
	case VI_ERROR_RAW_WR_PROT_VIOL:		ErrorMessage = "VI_ERROR_RAW_WR_PROT_VIOL"; break; //=	-1073807308
	case VI_ERROR_RAW_RD_PROT_VIOL:		ErrorMessage = "VI_ERROR_RAW_RD_PROT_VIOL"; break; //=	-1073807307
	case VI_ERROR_OUTP_PROT_VIOL:		ErrorMessage = "VI_ERROR_OUTP_PROT_VIOL"; break; //=	-1073807306
	case VI_ERROR_INP_PROT_VIOL:		ErrorMessage = "VI_ERROR_INP_PROT_VIOL"; break; //=	-1073807305
	case VI_ERROR_BERR:					ErrorMessage = "VI_ERROR_BERR"; break; //=	-1073807304
	case VI_ERROR_INV_SETUP:			ErrorMessage = "VI_ERROR_INV_SETUP"; break; //=	-1073807302
	case VI_ERROR_QUEUE_ERROR:			ErrorMessage = "VI_ERROR_QUEUE_ERROR"; break; //=	-1073807301
	case VI_ERROR_ALLOC:				ErrorMessage = "VI_ERROR_ALLOC"; break; //=	-1073807300
	case VI_ERROR_INV_MASK:				ErrorMessage = "VI_ERROR_INV_MASK"; break; //=	-1073807299
	case VI_ERROR_IO:					ErrorMessage = "VI_ERROR_IO"; break; //=	-1073807298	
	case VI_ERROR_INV_FMT:				ErrorMessage = "VI_ERROR_INV_FMT"; break; //=	-1073807297
	case VI_ERROR_NSUP_FMT:				ErrorMessage = "VI_ERROR_NSUP_FMT"; break; //=	-1073807295
	case VI_ERROR_LINE_IN_USE:			ErrorMessage = "VI_ERROR_LINE_IN_USE"; break; //=	-1073807294
	case VI_ERROR_SRQ_NOCCURRED:		ErrorMessage = "VI_ERROR_SRQ_NOCCURRED"; break; //=	-1073807286
	case VI_ERROR_INV_SPACE:			ErrorMessage = "VI_ERROR_INV_SPACE"; break; //=	-1073807282
	case VI_ERROR_INV_OFFSET:			ErrorMessage = "VI_ERROR_INV_OFFSET"; break; //=	-1073807279
	case VI_ERROR_INV_WIDTH:			ErrorMessage = "VI_ERROR_INV_WIDTH"; break; //=	-1073807278
	case VI_ERROR_NSUP_OFFSET:			ErrorMessage = "VI_ERROR_NSUP_OFFSET"; break; //=	-1073807276
	case VI_ERROR_NSUP_VAR_WIDTH:		ErrorMessage = "VI_ERROR_NSUP_VAR_WIDTH"; break; //=	-1073807275
	case VI_ERROR_WINDOW_NMAPPED:		ErrorMessage = "VI_ERROR_WINDOW_NMAPPED"; break; //=	-1073807273
	case VI_ERROR_NLISTENERS:			ErrorMessage = "VI_ERROR_NLISTENERS"; break; //=	-1073807265
	case VI_ERROR_NCIC:					ErrorMessage = "VI_ERROR_NCIC"; break; //=	-1073807264
	case VI_ERROR_NSUP_OPER:			ErrorMessage = "VI_ERROR_NSUP_OPER"; break; //=	-1073807257
	case VI_ERROR_ASRL_PARITY:			ErrorMessage = "VI_ERROR_ASRL_PARITY"; break; //=	-1073807254
	case VI_ERROR_ASRL_FRAMING:			ErrorMessage = "VI_ERROR_ASRL_FRAMING"; break; //=	-1073807253
	case VI_ERROR_ASRL_OVERRUN:			ErrorMessage = "VI_ERROR_ASRL_OVERRUN"; break; //=	-1073807252
	case VI_ERROR_NSUP_ALIGN_OFFSET:	ErrorMessage = "VI_ERROR_NSUP_ALIGN_OFFSET"; break; //=	-1073807248
	case VI_ERROR_USER_BUF:				ErrorMessage = "VI_ERROR_USER_BUF"; break; //=	-1073807247
	case VI_ERROR_RSRC_BUSY:			ErrorMessage = "VI_ERROR_RSRC_BUSY"; break; //=	-1073807246
	case VI_ERROR_NSUP_WIDTH:			ErrorMessage = "VI_ERROR_NSUP_WIDTH"; break; //=	-1073807242
	case VI_ERROR_INV_PARAMETER:		ErrorMessage = "VI_ERROR_INV_PARAMETER"; break; //=	-1073807240
	case VI_ERROR_INV_PROT:				ErrorMessage = "VI_ERROR_INV_PROT"; break; //=	-1073807239
	case VI_ERROR_INV_SIZE:				ErrorMessage = "VI_ERROR_INV_SIZE"; break; //=	-1073807237
	case VI_ERROR_WINDOW_MAPPED:		ErrorMessage = "VI_ERROR_WINDOW_MAPPED"; break; //=	-1073807232
	case VI_ERROR_NIMPL_OPER:			ErrorMessage = "VI_ERROR_NIMPL_OPER"; break; //=	-1073807231
	case VI_ERROR_INV_LENGTH:			ErrorMessage = "VI_ERROR_INV_LENGTH"; break; //=	-1073807229
	case VI_ERROR_SESN_NLOCKED:			ErrorMessage = "VI_ERROR_SESN_NLOCKED"; break; //=	-1073807204
	case VI_ERROR_MEM_NSHARED:			ErrorMessage = "VI_ERROR_MEM_NSHARED"; break; //=	-1073807203

	default: ErrorMessage = "undefined Error";
	}


	if ((int) strlen (ErrorMessage) > 1)
	{
		//char string[256];
		//wsprintf (string, "VISA Error \"%s\"\n", ErrorMessage);
		//VSendReportMsg (string);
		wsprintf (ErrorText, "VISA Error \"%s\"\n", ErrorMessage);
		// UtaExcRaiseUserError (string, 9);
	}
	return ErrorText;
}

#if defined __cplusplus
	}
#endif
