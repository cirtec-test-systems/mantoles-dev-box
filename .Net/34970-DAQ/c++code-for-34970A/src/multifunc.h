////////////////////////////////////////////////////////////////////////
//
// File: multifunc.h
//
// Description: Define Multifunc-specific parameters and limits, and 
// the structure used to track the state of an instance of the Handler. 
//
////////////////////////////////////////////////////////////////////////

#ifndef __MultiFunc_H__
#define __MultiFunc_H__


#define HWHMULTIFUNC_ID					34907     

#define	MULTIFUNC_TIMEOUT				3000


// value for Digital modes
#define MULTIFUNC_DIG_MODE_BIT          0
#define MULTIFUNC_DIG_MODE_BYTE		    1
#define MULTIFUNC_DIG_MODE_WORD         2

// Min/Max values for the number of counts
#define MULTIFUNC_MAX_TOT_COUNT			0xFFFFF  // 20 Bits
#define MULTIFUNC_MIN_TOT_COUNT			0


// Min/Max values for the DAC output voltage.
#define MULTIFUNC_MIN_DAC_VOLTAGE		-10.0
#define MULTIFUNC_MAX_DAC_VOLTAGE		 +10.0

// Min/Max values for voltage-level triggering.
#define MULTIFUNC_POS_SLOPE				0
#define MULTIFUNC_NEG_SLOPE				1

// Values used to determine if a Bit is enabled.
#define MULTIFUNC_OPEN					0
#define MULTIFUNC_CLOSED				1


// Trigger sources
#define MULTIFUNC_TRIGGER_TTL0			0
#define MULTIFUNC_TRIGGER_TTL1			1
#define MULTIFUNC_TRIGGER_TTL2			2
#define MULTIFUNC_TRIGGER_TTL3			3
#define MULTIFUNC_TRIGGER_TTL4			4
#define MULTIFUNC_TRIGGER_TTL5			5
#define MULTIFUNC_TRIGGER_TTL6			6
#define MULTIFUNC_TRIGGER_TTL7			7
#define MULTIFUNC_TRIGGER_IMM			8
#define MULTIFUNC_TRIGGER_EXT			9
  
// Basic parameters 
#define MULTIFUNC_NUM_BITS				16
#define MULTIFUNC_NUM_PORTS				2
#define MULTIFUNC_NUM_DACS				2




// Error codes returned by lower-level functions.
#define     MULTIFUNC_SUCCESS              0
#define     MULTIFUNC_ERR_BAD_PARM         -1
#define     MULTIFUNC_ERR_IO               -2
#define		MULTIFUNC_ERR_NO_TRIGGER		-3
#define     MULTIFUNC_ERR_BIT_TIMEOUT      -4
#define     MULTIFUNC_ERR_BIT_FAILURE      -5

// Working functions which could be called from other Actions 
// developed for the HP34970A Multifunc. 
// The functions will need to know he VISA Session ID for the instrument.  (In theory...)


// hwH34970_multifunc Actions
extern "C" {

	void UTADLL mfuncReset           (HUTAPB hPB); // Inst
	void UTADLL mfuncSetDigOut       (HUTAPB hPB); // Inst, Mode (Word|Byte|Bit), Data
	void UTADLL mfuncGetDigIn        (HUTAPB hPB); // Inst, Mode (Word|Byte|Bit), Result
	void UTADLL mfuncSetDigOutArray  (HUTAPB hPB); // Inst, Mode (Word|Byte|Bit), Array
	void UTADLL mfuncGetDigInArray   (HUTAPB hPB); // Inst, Mode (Word|Byte|Bit), Array
	void UTADLL mfuncTotConf         (HUTAPB hPB); // Inst, ...
	void UTADLL mfuncTotInitiate     (HUTAPB hPB); // Inst
	void UTADLL mfuncTotGetResult    (HUTAPB hPB); // Inst, Result
	void UTADLL mfuncTotReset		 (HUTAPB hPB); // Inst, Result
	void UTADLL mfuncSetAnalogOut    (HUTAPB hPB); // Inst, OutputValue
	void UTADLL mfuncSet             (HUTAPB hPB); // Inst
	void UTADLL mfuncIsSet           (HUTAPB hPB); // Inst
	void UTADLL mfuncDefineDigIO	 (HUTAPB hPB); // define Slope Switching mode
	
	// in File io_util.cpp
	char *VisaErrorMessage (ViStatus VisaError);
}

#endif  // ifndef __MultiFunc_H__
