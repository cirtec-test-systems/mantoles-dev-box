/*****************************************************************************
 *****************************************************************************
 *
 * File:    a_switch.cpp
 *
 * Purpose: Switch handler for all HP34970A cards
 *
 * Author:  Rudolf Glas - HP Solution Consultant - HP SSV
 * Version: A.01.00
 *
 * Date created:  January 2000
 * Last Modified:
 *
 * Revision History:
 *****************************************************************************
 *****************************************************************************/


/*
	AutoConfig Mode. 
	- This creates all nodes and adjecencies of the found cards in each slot.
	- If a Multifunction Module is found in AutoConfig Mode, the Digital Ports
	  are used as Digital Output Only(!) and can be set using the Switching Concept
	  inside a testplan. Currently the Digital Outputs are set to 0 during Init.
	  To avoid this the set AutoConfig Mode to 0 and do manual configuration.
	- If an internal DMM is found the nodes of installed Multiplexers (34901, 34902
	  and 34908) will be created.
	  This allows switching of mux channels to internal DMM. In case of 4wire
	  measurements only the first half of channel numbers (e.g. s01-s10) can be used.
 */

#ifdef _WINDOWS
#include <windows.h>            
#endif

#include <stdio.h>              /* for definition of sprintf() and printf() */
#include "uta.h"                /* TestCore interface declarations          */
#include "utacore.h"
#include "switch_hndl.h"        /* UTA Switch Handler standard declarations */
#include "utaapi.h"
#include "pubapi.h"


#include "visa.h"
#include "a_switch.h"



#if defined __cplusplus
	extern "C" {
#endif 

/******************************************************************************
 * Private function declarations
 *
 * The following are declarations for utility functions in the Switch
 * Handler.  TestExecSL will never call these entry points directly.
 * (see below)
 */
static void ResetState(InstParmStruct*);
static void UpdateState(InstParmStruct*, int, int);


/*****************************************************************************
 ********************************* FUNCTIONS *********************************
 *****************************************************************************/

/*****************************************************************************
 * void UTADLL DeclareParms (HUTAHWMOD hModule, 
 *                           HUTAPBDEF hParameterBlockDefinition)
 *
 * DeclareParms is the first (1 of 7) function required by every
 * TestExecSL Switch Handler.  This function defines the parameters
 * that TestExecSL must pass into the Switch Handler.
 *
 * This function is called only once the first time you hit the
 * "Update" button in the TestExecSL Topology Editor after specifying
 * the DLL (this DLL).
 */


void UTADLL DeclareParms (HUTAHWMOD hModule, HUTAPBDEF hpbDef)
{
    /*
     * For this switch handler, the parameter will be a unique handle to
     * the instrument doing the switch operations -- TestExecSL will
     * assign this value and properly set it when calling other functions
     * in the Switch Handler.  Other parameters typically declared here
     * include address parameters, a selector for controlling multiple
     * modes of operation (or personalities), and anything that might be
     * needed based on the physical location of the switch module.
     */

	HUTAINT32	hData   = NULL;
	HUTASTRING	hString = NULL;
    char        szDesc[128];
	int			iStat;


	iStat = MessageBox (NULL, "To use AutoConfig mode set the parameters\n\n\tAddress > 0 (Default=9) and\n\n\tAutoConfig = 1\n\nand press the Add/Update button", "Configure Parameters", MB_OK);

	// Board number    
	wsprintf (szDesc, "GPIB Board Number");
	hData = (HUTAINT32) UtaHwModDeclareParm (hModule, hpbDef, "BoardNumber", "CUtaInt32", szDesc);
	UtaInt32SetValue (hData, 0); 

	// Address    
	wsprintf (szDesc, "HP-IB Primary address (1-31) or COM port (1-n). Use Address 0 for Non-Live mode");
	hData = (HUTAINT32) UtaHwModDeclareParm (hModule, hpbDef, "Address", "CUtaInt32", szDesc);
	UtaInt32SetValue (hData, DEFAULT_ADDRESS); 

	// Baudrate    
	wsprintf (szDesc, "Baudrate. Any value uses the COM port, 0 works with HP-IB interface");
	hData = (HUTAINT32) UtaHwModDeclareParm (hModule, hpbDef, "Baudrate", "CUtaInt32", szDesc);
	UtaInt32SetValue (hData, 0); 

	// AutoConf
	wsprintf (szDesc, "Automatic Configuration of Models and Nodes (0=None or 1=Automatic)");
    hData = (HUTAINT32) UtaHwModDeclareParm (hModule, hpbDef, "AutoConf", "CUtaInt32", szDesc);
	UtaInt32SetValue (hData, 1); 

	// Model Slot1
	wsprintf (szDesc, "Card Model in Slot 1 (34901-34908, 0=No card installed");
    hData = (HUTAINT32) UtaHwModDeclareParm (hModule, hpbDef, "Model1", "CUtaInt32", szDesc);
	UtaInt32SetValue (hData, DEFAULT_MODEL); 

	// Model Slot2
	wsprintf (szDesc, "Card Model in Slot 2 (34901-34908, 0=No card installed");
    hData = (HUTAINT32) UtaHwModDeclareParm (hModule, hpbDef, "Model2", "CUtaInt32", szDesc);
	UtaInt32SetValue (hData, DEFAULT_MODEL); 

	// Model Slot3
	wsprintf (szDesc, "Card Model in Slot 3 (34901-34908, 0=No card installed");
    hData = (HUTAINT32) UtaHwModDeclareParm (hModule, hpbDef, "Model3", "CUtaInt32", szDesc);
	UtaInt32SetValue (hData, DEFAULT_MODEL); 

	// Internal DMM usage 
	wsprintf (szDesc, "Internal DMM usage if installed  in HP34970A (0=off, 1=on)");
    hData = (HUTAINT32) UtaHwModDeclareParm (hModule, hpbDef, "iDMM", "CUtaInt32", szDesc);
	UtaInt32SetValue (hData, 1); 

	// Name
	wsprintf (szDesc, "Name of the instrument");
    hString = (HUTASTRING) UtaHwModDeclareParm (hModule, hpbDef, "Name", "CUtaString", szDesc);
	UtaStringSetValue (hString, DEFAULT_NAME); 


	// Other declarations
	UtaHwModDeclareHasTrace (hModule);
    UtaHwModDeclareAsInst (hModule, TRUE);
    return;
}


/*****************************************************************************
 * void UTADLL DeclareNodes (HUTAHWMOD hModule, HUTAPB hPB)
 *
 * The second (2 of 7) function required by every TestExecSL Switch
 * Handler.  This function declares all the nodes and their
 * adjacencies on the switch module.  It is called every time the
 * "Update" button in the TestExecSL Topology Editor is selected.
 * (Note: if a parameter changes the available nodes or their
 * adjacencies, the "Update" button needs to be hit whenever that
 * parameter is changed!)
 */
void UTADLL DeclareNodes (HUTAHWMOD hModule, HUTAPB hPB)
{
	int  iSlot;


    /* Declare and create a parameter structure for this switch module. */
    InstParmStruct *pParmStruct;
    pParmStruct = (InstParmStruct*) malloc(sizeof(InstParmStruct));


	// Retrieve the parameters and store it in the InstParmStruct.
	UtaPbGetInt32 (hPB, "Address",	&pParmStruct->nAddress);
	UtaPbGetInt32 (hPB, "Baudrate",	&pParmStruct->nBaudrate);
	UtaPbGetInt32 (hPB, "AutoConf",	&pParmStruct->nAutoConf);
	UtaPbGetInt32 (hPB, "iDMM",		&pParmStruct->nDmmUsage);
	UtaPbGetInt32 (hPB, "Model1",	&pParmStruct->nModel1);
	UtaPbGetInt32 (hPB, "Model2",	&pParmStruct->nModel2);
	UtaPbGetInt32 (hPB, "Model3",	&pParmStruct->nModel3);


	// Automatic Configuration of Models and Nodes
	if (HP34970_AutoConfig (pParmStruct, hModule, hPB) != 0) return;

	// Set the Models, Columns, Rows and Timing values
	HP34970_SetParameters (pParmStruct);

	// Create Node(s) for Internal DMM, Rows and Columns for all 3 Slots
	for (iSlot = 0; iSlot <= 3; iSlot++) // Slot 0 is used for Internal DMM
		HP34970_CreateNodes (pParmStruct, hModule, iSlot);

   	UtaHwModDeclareAsInst (hModule, TRUE); 
}


/////////////////////////////////////////////////////////////////////////////
// Activate:
//    This function is called when the instrument is placed in the hardware
//    configuration table.  (When a Testplan referencing a Topology layer
//    containing the module is run for the first time.)
//
/////////////////////////////////////////////////////////////////////////////
void UTADLL Activate(HUTAHWMOD hModule, HUTAPB hParmBlock, HUTAINST hInst, HUTAPBDEF hPrivBlock)
{
   HUTAPARM    hInstParm = NULL;
   HUTAPBDEF   hInstrumentTable;


   IUtaInt32   nDmmUsage (hParmBlock, "iDMM");
   IUtaInt32   nBoardNumber (hParmBlock, "BoardNumber");
   IUtaInt32   nAddr (hParmBlock, "Address");
   IUtaInt32   nBaudrate (hParmBlock, "Baudrate");
   const char* szInstname = UtaHwModGetName (hModule);
   IUtaString  szModel (hParmBlock, "Name"); // Model


   // Create the "hwconfig" symbol table if it doesn't exist.
    hInstrumentTable = UtaTableRegFindTable ("hwconfig", TRUE );
    if( hInstrumentTable == NULL )
	{
        hInstrumentTable=UtaPbDefCreate();
        UtaTableSetName( hInstrumentTable, "hwconfig");
        UtaTableRegister( hInstrumentTable, TRUE );
    }


    // Load up the GPIB instrument information.
    UtaInstHPIBSetValues (hInst, 
	                  "HP34970A",      // Modelname
			  nBoardNumber,     // selectCode is GPIBn
			  nAddr,           // primAddr
			  0,               // secAddr
			  "*IDN?",         // ID query string
			  "",              // ID return SubString
			  nDmmUsage,       // subInstNum
			  NULL,            // *userHandle     
			  "HP34970A",      // rootHandlerName
			  "hwh34970a.dll"); // handlerDLLName
    if (hInstParm = UtaParmCreate (szInstname, "CUtaInst"))
	{
        UtaParmSetData (hInstParm, (HUTADATA) hInst);
        UtaPbDefAddParm (hInstrumentTable, hInstParm);
    }
}  

/////////////////////////////////////////////////////////////////////////////
void UTADLL Deactivate (HUTAHWMOD hModule,   HUTAPB    hParmBlock,
                        HUTAINST  hInstParm, HUTAPBDEF hPrivBlock)
{
    HUTAPBDEF   hInstrumentTable = UtaTableRegFindTable ("hwconfig", TRUE);            
    HUTAPARM	hParm = NULL;
    const char* szInstname = UtaHwModGetName (hModule);

	if (hParm = UtaPbDefFindParm (hInstrumentTable, szInstname))
	{
	    UtaParmRemoveData (hParm);
	    UtaPbDefReleaseParm (hInstrumentTable, szInstname);
	}
}  

/*****************************************************************************
 * LPVOID UTADLL Init (HUTAHWMOD hModule, HUTAPB hParameterBlock)
 *
 * (This used to be called "BindParms" in previous releases)
 *
 * The third (3 of 7) function required by every TestExecSL Switch
 * Handler.  This function is called just prior to the first time a
 * Testplan containing paths which reference this Switch Handler is
 * run.  It is a good place to establish communication with the switch
 * module.  If multiple switch modules have been defined in the
 * topology, this will be called once for each one of them.
 */

static UtaInt32 iDummyViSession = 0;

LPVOID UTADLL Init (HUTAHWMOD hModule, HUTAPB hParameterBlock)
{
	/* Declare and create a parameter structure for this switch module. */
    InstParmStruct *pParmStruct;
    pParmStruct = (InstParmStruct*) malloc(sizeof(InstParmStruct));


	pParmStruct->nDemoTic = GetTickCount () + DEMOSECONDS * 60 * 1000; // sec * sec/min * ms/sec


    /*
     * Retrieve the parameters and store it in the InstParmStruct.
     */
    UtaPbGetInt32  (hParameterBlock, "BoardNumber",  &pParmStruct->nBoardNumber);
    UtaPbGetInt32  (hParameterBlock, "Address",  &pParmStruct->nAddress);
    UtaPbGetInt32  (hParameterBlock, "Baudrate", &pParmStruct->nBaudrate);
    UtaPbGetInt32  (hParameterBlock, "Model1",   &pParmStruct->nModel1);
    UtaPbGetInt32  (hParameterBlock, "Model2",   &pParmStruct->nModel2);
    UtaPbGetInt32  (hParameterBlock, "Model3",   &pParmStruct->nModel3);
    UtaPbGetString (hParameterBlock, "Name",  	 pParmStruct->nName, 80);

    /*
     * If this were real, we would open a SICL or VI session with our
     * I/O subsystem of choice, packaging up any parameters such as the
     * address retrieved above.  We will dummy it out just for fun.
     */
	if (pParmStruct->nAddress > 0)
	{
		ViSession	vi;
		ViStatus	viStat;
		char		szAddr[32];
		char		szResponse[80];
		char		szTxt[80];
		int			iSlot;


		viStat = viOpenDefaultRM (&pParmStruct->defaultRM);
		if (viStat != VI_SUCCESS)
		{
 			UtaExcRaiseUserError ("Error in call to viOpenDefaultRM", 9);
 			VisaErrorMessage (viStat);
			return NULL;
		}
	
		if (pParmStruct->nBaudrate > 0)
			wsprintf (szAddr, "ASRL%d::INSTR", pParmStruct->nAddress);
		else
			wsprintf (szAddr, "GPIB%d::%d::INSTR", pParmStruct->nBoardNumber,
				  pParmStruct->nAddress);

		viStat = viOpen (pParmStruct->defaultRM, szAddr, VI_NULL, VI_NULL, &vi);
		if (viStat != VI_SUCCESS)
		{
			char msg[1000];
			wsprintf(msg, "Error in call to viOpen %s", szAddr);
			UtaExcRaiseUserError (msg, 9);
 			VisaErrorMessage (viStat);
			return NULL;
		}
		pParmStruct->vi = vi;

		// Set the Baudrate
		if (pParmStruct->nBaudrate > 0)
		{
			viStat = viSetAttribute (vi, VI_ATTR_ASRL_BAUD, pParmStruct->nBaudrate);
			if (viStat != VI_SUCCESS)
			{	 
				UtaExcRaiseUserError ("Error setting baudrate", 9);
	 			VisaErrorMessage (viStat);
				return NULL;
			}
		}

		// Timeout default = 200ms
		viStat = viSetAttribute (vi, VI_ATTR_TMO_VALUE, 5000); //
		if (viStat != VI_SUCCESS)
		{	 
			UtaExcRaiseUserError ("Error setting timeout", 9);
 			VisaErrorMessage (viStat);
			return NULL;
		}

		// Check if the Instrument is HP34970A
		viStat = viPrintf (vi, "*IDN?\n");
		if (viStat != VI_SUCCESS)
		{
			UtaExcRaiseUserError ("Error accessing the instrument", 9);
 			VisaErrorMessage (viStat);
			return NULL;
		}
		viStat = viScanf (vi, "%t", szResponse);
		if (viStat != VI_SUCCESS)
		{
			UtaExcRaiseUserError ("Error reading from instrument", 9);
 			VisaErrorMessage (viStat);
			return NULL;
		}

		if (strstr (szResponse, "34970") == NULL)
		{
			wsprintf (szTxt, "Init HP34970A");
			MessageBox (NULL, "The instrument is no HP34970A", szTxt, MB_OK); 
			return NULL;
		}

		// Reset HP34970A
		viStat = viPrintf (vi, "*RST;*CLS\n");
		if (viStat != VI_SUCCESS)
		{
			UtaExcRaiseUserError ("Error resetting the instrument", 9);
 			VisaErrorMessage (viStat);
			return NULL;
		}

		// Read Card ID
		for (iSlot = 100; iSlot <= 300; iSlot += 100)
		{
			viStat = viPrintf (vi, "Syst:CType? %d\n", iSlot);
			if (viStat != VI_SUCCESS)
			{
				UtaExcRaiseUserError ("Error accessing the instrument", 9);
	 			VisaErrorMessage (viStat);
				return NULL;
			}

			viStat = viScanf (vi, "%t", szResponse);
			if (viStat != VI_SUCCESS)
			{
				UtaExcRaiseUserError ("Error reading from instrument", 9);
	 			VisaErrorMessage (viStat);
				return NULL;
			}

			switch (iSlot)
			{
			case 100:	wsprintf (szTxt, "%ld", pParmStruct->nModel1); break;
			case 200:	wsprintf (szTxt, "%ld", pParmStruct->nModel2); break;
			case 300:	wsprintf (szTxt, "%ld", pParmStruct->nModel3); break;
			default:	wsprintf (szTxt, "0");
			}

			if ((atoi (szTxt) > 0) && (strstr (szResponse, szTxt) == NULL))
			{
				wsprintf (szTxt, "Incorrect Card in Slot %d", iSlot / 100);
				MessageBox (NULL, szResponse, szTxt, MB_OK); 
			}

			// Set Digital Output Ports to 0
			if (atoi (szTxt) == 34907)
			{
			//	viPrintf (vi, "Sour:Dig:Data:Byte 0,(@%d)\n", iSlot + 1);
			//	viPrintf (vi, "Sour:Dig:Data:Byte 0,(@%d)\n", iSlot + 2);
			}

			// Check DMM and disable if 
			viStat = viPrintf (vi, "Inst:DMM:Installed?\n");
			if (viStat != VI_SUCCESS)
			{
				UtaExcRaiseUserError ("Error accessing the instrument", 9);
	 			VisaErrorMessage (viStat);
				return NULL;
			}

			viScanf (vi, "%t", szResponse);
			if (viStat != VI_SUCCESS)
			{
				UtaExcRaiseUserError ("Error reading from instrument", 9);
	 			VisaErrorMessage (viStat);
				return NULL;
			}

			if (atoi (szResponse) && pParmStruct->nDmmUsage)
			{
				viStat = viPrintf (vi, "Inst:DMM:Off\n");
				if (viStat != VI_SUCCESS)
				{
					UtaExcRaiseUserError ("Error accessing the instrument", 9);
		 			VisaErrorMessage (viStat);
					return NULL;
				}
			}
		}
	}
	else
		pParmStruct->vi = ++iDummyViSession;
	

	// Initialize the Slope parameters for DigitalIO mode
	for (int iSlot = 0; iSlot <= 3; iSlot++)
		for (int iPort = 0; iPort <=3; iPort++)
			pParmStruct->nSlope[iPort][iSlot] = 0;

    /* Set up the state tracking for this switch module. */
    ResetState (pParmStruct);

    /*
     * Return a pointer to the parameter structure.  TestExecSL will
     * associate this pointer with the "hModule" it has provided.
     * This means the data structure will be unique to each switch
     * module using this Switch Handler.
     */
    UtaHwModTraceEx (hModule, 
                     "Opened %s with address %d, Models_1to3 %d, %d, %d\n",
					 pParmStruct->nName,
                     pParmStruct->nAddress,
                     pParmStruct->nModel1,
                     pParmStruct->nModel2,
                     pParmStruct->nModel3
					 );

    return (LPVOID) pParmStruct;
}


/*****************************************************************************
 * void UTADLL Close (HUTAHWMOD hModule, HUTAPB hParameterBlock, LPVOID pInitData)
 *
 * (This used to be called "UnbindParms" in previous releases)
 *
 * The fourth (4 of 7) function required of a TestExecSL Switch
 * Handler.  This function is called by TestExecSL once whenever a
 * Testplan with a path which referenced this switch module is closed.
 * If multiple switch modules were created, this function will be
 * called once for each of them.  (I.e., once for each 'hModule').
 */
void UTADLL Close (HUTAHWMOD hModule, HUTAPB hParameterBlock, LPVOID pInitData)
{
    /*
     * Close the I/O session, and then free up the memory used by the
     * data structure.
     */

   	if (((InstParmStruct*) pInitData)->nAddress > 0)
	{
		viClose (((InstParmStruct*) pInitData)->vi);
	}
	
    UtaHwModTraceEx (hModule, 
                     "Closed %s with address %d\n",
					 ((InstParmStruct*) pInitData)->nName,
                     ((InstParmStruct*) pInitData)->nAddress);

    free (pInitData);
    return;
}


/*****************************************************************************
 * UTAUSECS UTADLL Reset (HUTAHWMOD hModule, HUTAPB hParameterBlock, LPVOID pInitData)
 *
 * The fifth (5 of 7) function which must be in every TestExecSL
 * Switch Handler.  This function is called by TestExecSL in order to
 * reset the switch module(s) controlled by this Switch Handler.
 *
 * The return value should be the number of microseconds this
 * operation will take.  This allows TestExecSL to speed things
 * up. If "*OPC?" is used, the return value can just be '1'.
 * Using '0' could cause a call to 'IsPositionSet', which is not
 * implemented in this Switch Handler.
 */
UTAUSECS UTADLL Reset (HUTAHWMOD hModule, HUTAPB hParameterBlock, LPVOID pInitData)
{

	int iSlot = 1;

	if (DemoTimeElapsed (((InstParmStruct*) pInitData)->nDemoTic)) return 0;
	
    /* Track the reset Parameter Block */
    ResetState ((InstParmStruct *) pInitData);

	if (((InstParmStruct *) pInitData)->nAddress > 0)
	{
		if (viPrintf (((InstParmStruct *) pInitData)->vi, "*RST;*CLS\n") != VI_SUCCESS)
		{
 			UtaExcRaiseUserError ("Error reset instrument", 9);
 			return 0;
		}
	}

	for (int i = 0; i < SWITCH_NUM_ELEMENTS; i++)
		((InstParmStruct *) pInitData)->nElementState[i] = OPEN;

    /* Print message to the screen. */
    UtaHwModTraceEx (hModule, "Opened all elements\n");

    return ((InstParmStruct *) pInitData)->nTimeReset[iSlot];
}


/*****************************************************************************
 * UTAUSECS UTADLL SetPosition(HUTAHWMOD hModule, 
 *                             HUTAPB hParameterBlock, 
 *                             LPVOID pInitData,
 *                             IDUTASWELM nElement, 
 *                             IDUTASWPOS nPosition)
 *
 * The sixth (6 of 7) function required in every TestExecSL Switch
 * Handler.  This function is the runtime heart of the Switch Handler.
 * It controls all the opening and closing of relays.  In effect,
 * whenever a 'Switching Action' in a Test is executed, the Switch
 * Manager analyzes the "Path(s)" and, based on its knowledge of the
 * topology of the entire system, makes the necessary calls to the
 * 'SetPosition' functions in each of the Switch Handlers referenced by
 * the path(s).
 *
 * The return value is the number of microseconds it will take for the
 * switch to complete the open or close operation.  If this function
 * returns 0, then the switch manager will use the switch handler
 * function, IsPositionSet, to actively determine whether or not the
 * operation is complete, rather than waiting.
 *
 * In addition to actually performing the opening and closing of the
 * switch, some static structure may be updated here as the switch
 * elements are opened and closed, if the Switch Handler implements
 * state tracking internally.
 */
UTAUSECS UTADLL SetPosition (HUTAHWMOD hModule, 
                             HUTAPB hParameterBlock, 
                             LPVOID pInitData,
                             IDUTASWELM nElement, 
                             IDUTASWPOS nPosition)
{
    /*
     * Program the switching using your I/O Scheme.  TestExecSL will pass
     * us the "nElement" that was defined in
     * "UtaSwModDeclareAdjacent", and the "nPosition" value.
     */

	ViSession	vi;
	ViStatus	viStat;
	int			iModel, iSlot;
	int			iPort, iBit, iByte;


	if (DemoTimeElapsed (((InstParmStruct*) pInitData)->nDemoTic)) return 0;

	iSlot = nElement / 100;

	switch (iSlot)
	{
	case 1: iModel = ((InstParmStruct*) pInitData)->nModel1; break;
	case 2: iModel = ((InstParmStruct*) pInitData)->nModel2; break;
	case 3: iModel = ((InstParmStruct*) pInitData)->nModel3; break;
	}

	// Method to get the channel number using the UtaPathConnect for path parameter
	if (((InstParmStruct*) pInitData)->bSetPosQuery == TRUE)
	{
		switch (iModel)
		{
		case 34901:
		case 34902:
		case 34903:
		case 34904:
		case 34905:
		case 34906:
		case 34908:	((InstParmStruct*) pInitData)->nSetPosChannel = nElement;
					break;
		case 34907:	// Calculate slot, port and element number from channel number
					iSlot = nElement / 100;    // slot may 0,1,2
					iPort = nElement % 100;    // 
					if (nElement % 100 >= 10)
						iPort = (nElement % 100) / 10;
					((InstParmStruct*) pInitData)->nSetPosChannel = iSlot * 100 + iPort;
					break;

		default:	((InstParmStruct*) pInitData)->nSetPosChannel = 0;
		}
		((InstParmStruct*) pInitData)->bSetPosQuery = FALSE;
		return 0;
	}
	// end of the method


	// If really switching is to do

	if (GetPosition (hModule, hParameterBlock, pInitData, nElement) == nPosition)
        return ITSDONE;

	vi = ((InstParmStruct *) pInitData)->vi;

	switch (iModel)
	{
	case 34901:
	case 34902:
	case 34903: 
	case 34904:
	case 34905:
	case 34906:
	case 34908:
				switch (nPosition)
				{
				case CLOSE:
							if (((InstParmStruct*) pInitData)->nAddress > 0)
								viStat = viPrintf (vi, "ROUTE:CLOSE (@%ld)\n", nElement);
							UtaHwModTraceEx (hModule, "Closed element %d\n", nElement);
							break;
		        case OPEN: 
							if (((InstParmStruct*) pInitData)->nAddress > 0)
								viStat = viPrintf (vi, "ROUTE:OPEN (@%ld)\n", nElement);
							UtaHwModTraceEx (hModule, "Opened element %d\n", nElement);
							break;
				}
				break;


	case 34907: // Here the "switching" of an digital bit is implemented
				iSlot = nElement / 100;
				iPort = (nElement % 100) / 10;
				iBit  = (nElement % 10);
				iBit  = 1 << iBit; // value of Bit
				iByte = 0;

				// Read back the current value from port
				if (((InstParmStruct*) pInitData)->nAddress > 0)
				{
					viStat = viPrintf (vi, "Sour:Dig:State? (@%d)\n", iSlot * 100 + iPort);
					viStat = viScanf (vi, "%d%*t", &iByte);
					if (iByte != 0)
					{
						viStat = viPrintf (vi, "Sour:Dig:Data:Byte? (@%d)\n", iSlot * 100 + iPort);
						viStat = viScanf (vi, "%d%*t", &iByte);

					}
				}


				switch (nPosition)
				{
				case CLOSE:
							if (((InstParmStruct*) pInitData)->nAddress > 0)
							{
								if (((InstParmStruct*) pInitData)->nSlope[iPort][iSlot])
									iByte |= iBit;  // positive logic: set one bit in the byte
								else
									iByte &= ~iBit; // negative logic: clear one bit in the byte

								iByte &= 0xff;
								viStat = viPrintf (vi, "Sour:Dig:Data:Byte %d,(@%d)\n", iByte, iSlot * 100 + iPort);
							}
							UtaHwModTraceEx (hModule, "Set Port%d, Byte = 0x%X\n", iPort, iByte); // nElement);
							break;
		        case OPEN:	
							if (((InstParmStruct*) pInitData)->nAddress > 0)
							{
								if (((InstParmStruct*) pInitData)->nSlope[iPort][iSlot])
									iByte &= ~iBit; // positive logic: clear one bit in byte
								else
									iByte |= iBit;  // negative logic: set one bit in byte

								iByte &= 0xff;
								viStat = viPrintf (vi, "Sour:Dig:Data:Byte %d,(@%d)\n", iByte, iSlot * 100 + iPort);
							}
							UtaHwModTraceEx (hModule, "Set Port%d, Byte = 0x%X\n", iPort, iByte); // nElement);
							break;
				}
				break;

	default: ;
	}

    /*
     * Update the stack tracking.
     */
    UpdateState ((InstParmStruct *) pInitData, nElement, nPosition);

    return ((InstParmStruct *) pInitData)->nTimeSetPos[iSlot];
}


/*****************************************************************************
 * IDUTASWPOS UTADLL GetPosition (HUTAHWMOD hModule,
 *                                HUTAPB hParameterBlock, 
 *                                LPVOID pInitData,
 *                                IDUTASWELM idElement)
 *
 * The seventh (7 of 7) function which must be in every TestExecSL
 * Switch Handler.  This function is called by the TestCore Switch
 * Manager in order to determine the state of the card (for example,
 * when a user has selected "Return to Previous State" for a switching
 * Action).
 */
IDUTASWPOS UTADLL GetPosition (HUTAHWMOD hModule,
                               HUTAPB hParameterBlock, 
                               LPVOID pInitData, 
                               IDUTASWELM idElement)
{
    /*
     * This assumes that all access to the switch module is through
     * this Switch Handler!  If your switch has readback circuitry you
     * may wish to access it here!
     */

	return ((InstParmStruct*) pInitData)->nElementState [idElement]; 
}

/*****************************************************************************
 * BOOL UTADLL IsPositionSet (HUTAHWMOD hModule, HUTAPB hParameterBlock, LPVOID pInitData, IDUTASWELM idElement)
 *
 * This function is an optional switch handler function.  It can be
 * used to decrease switching time.  If the 'SetPosition' function
 * returns '0' as the amount of time to wait, the TestCore
 * Switch Manager will poll this function to determine when the switch
 * has been safely set. 
 */
#if 0 /* We don't need this one... */
BOOL UTADLL IsPositionSet (HUTAHWMOD idModule, HUTAPB hParameterBlock, LPVOID pInitData, IDUTASWELM idElement)
{
    /* ...do what it takes to figure out if relay is really closed... */
    return TRUE;
}
#endif /* 0 */

/*****************************************************************************
 * void UTADLL DeclareStatus (HUTAHWMOD hModule, HUTAPB hPB)
 *
 * The first of two functions required by those TestExecSL Switch
 * Handlers supporting the watch window for debugging.  This function 
 * declares all the watchable fields for this switch card.
 */
void UTADLL DeclareStatus (HUTAHWMOD hModule, HUTAPB hPB)
{
	IDUTAHWSTAT idStatus;
	int         iR, iC, iSlot;
	long		iModel[4];
	char        szNode[32], szTxt[32];   


	
    UtaPbGetInt32 (hPB, "Model1",  &iModel[1]);
    UtaPbGetInt32 (hPB, "Model2",  &iModel[2]);
    UtaPbGetInt32 (hPB, "Model3",  &iModel[3]);


	/*  indent title */
	for (iSlot = 1; iSlot <= 3; iSlot++)
	{
		switch (iModel[iSlot])
		{
		case 34901:
					for (iR = 1; iR <= HP34901_ROWS; iR++)
					{
						wsprintf (szNode, "Chan %d", iSlot * 100 + iR);
						idStatus = UtaHwModDeclareStatus (hModule, 1, szNode);    // returns 1...
					}
					for (iC = 1; iC <= HP34901_COLUMNS; iC++)
					{
						wsprintf (szTxt, "Common %d.%d", iSlot * 100, iC);
						idStatus = UtaHwModDeclareStatus (hModule, 2, szTxt); 
					}
					wsprintf (szNode, "Chan %d", iSlot * 100 + 21);
					idStatus = UtaHwModDeclareStatus (hModule, 1, szNode);    // returns 1...
					wsprintf (szNode, "Chan %d", iSlot * 100 + 22);
					idStatus = UtaHwModDeclareStatus (hModule, 1, szNode);    // returns 1...
					wsprintf (szTxt, "Common 3(C)");
					idStatus = UtaHwModDeclareStatus (hModule, 2, szTxt); 
					break;
		case 34902:
					for (iR = 1; iR <= HP34902_ROWS; iR++)
					{
						wsprintf (szNode, "Chan %d", iSlot * 100 + iR);
						idStatus = UtaHwModDeclareStatus (hModule, 1, szNode);    // returns 1...
					}
					for (iC = 1; iC <= HP34902_COLUMNS; iC++)
					{
						wsprintf (szTxt, "Common %d.%d", iSlot * 100, iC);
						idStatus = UtaHwModDeclareStatus (hModule, 2, szTxt); 
					}
					break;
		case 34903:
					for (iR = 1; iR <= HP34903_ROWS; iR++)
					{
						wsprintf (szNode, "Chan %d", iSlot * 100 + iR);
						idStatus = UtaHwModDeclareStatus (hModule, 1, szNode);    // returns 1...
					}
					break;
		case 34905:
		case 34906:
					for (iR = 1; iR <= HP34908_ROWS; iR++)
					{
						wsprintf (szNode, "Chan %d", iSlot * 100 + iR + 10);
						idStatus = UtaHwModDeclareStatus (hModule, 1, szNode);    // returns 1...
					}
					for (iR = 1; iR <= HP34908_ROWS; iR++)
					{
						wsprintf (szNode, "Chan %d", iSlot * 100 + iR + 20);
						idStatus = UtaHwModDeclareStatus (hModule, 1, szNode);    // returns 1...
					}
					for (iC = 1; iC <= HP34908_COLUMNS; iC++)
					{
						wsprintf (szTxt, "Common %d.%d", iSlot * 100, iC);
						idStatus = UtaHwModDeclareStatus (hModule, 2, szTxt); 
					}
					break;

		case 34904: // Slot * 100 + Row * 10 + Column
					for (iR = 1; iR <= HP34908_ROWS; iR++)
					{
						wsprintf (szNode, "Row %d", iSlot * 100 + iR);
						idStatus = UtaHwModDeclareStatus (hModule, 1, szNode);    // returns 1...
					}
					for (iC = 1; iC <= HP34908_COLUMNS; iC++)
					{
						wsprintf (szTxt, "Column %d.%d", iSlot * 100, iC);
						idStatus = UtaHwModDeclareStatus (hModule, 2, szTxt); 
					}
					break;
		case 34908:
					for (iR = 1; iR <= HP34908_ROWS; iR++)
					{
						wsprintf (szNode, "Chan %d", iSlot * 100 + iR);
						idStatus = UtaHwModDeclareStatus (hModule, 1, szNode);    // returns 1...
					}
					for (iC = 1; iC <= HP34908_COLUMNS; iC++)
					{
						wsprintf (szTxt, "Common %d.%d", iSlot * 100, iC);
						idStatus = UtaHwModDeclareStatus (hModule, 2, szTxt); 
					}
					break;
		default:;
		}
	}

}



/*****************************************************************************
 * void UTADLL GetStatus (HUTAHWMOD hModule, 
 *                        HUTAPB hParameterBlock,
 *                        LPVOID pUserInitData,
 *                        IDUTAHWSTAT idStatus,
 *                        LPSTR lpszBuffer,
 *                        int nBufferSize);
 *
 * The second of two functions required by those TestExecSL Switch
 * Handlers supporting the watch window for debugging.  This function 
 * provides the text status for each of thw watchable fields.
 */
#define INSTRUMENT_STATUS_ID 0

#define GET_STATUS_CHAR(n)  (((InstParmStruct*) \
                             pUserInitData)->nElementState[n] ? 'X':'O')

#define GET_STATUS(n) (((InstParmStruct*) \
                       pUserInitData)->nElementState[n] ? "CLOSED" : "OPEN")

void UTADLL GetStatus (HUTAHWMOD hModule, 
                       HUTAPB hParameterBlock,
                       LPVOID pUserInitData,
                       IDUTAHWSTAT idStatus,
                       LPSTR lpszBuffer,
                       int nBufferSize)
{
	int  iR, iC, iSlot;
    char *lpszP;


	iSlot = 1;
	lpszP = lpszBuffer;
	lpszBuffer[0] = '\0';  

	if (idStatus == INSTRUMENT_STATUS_ID) /* Summary status of all of the relays */
    {
		if (nBufferSize > 8) 
			for (iSlot = 1; iSlot <= 3; iSlot++)
			{
				for (iC = 1; iC <= ((InstParmStruct*) pUserInitData)->nColumns[iSlot]; iC++)
				{
					for (iR = 0; iR <= ((InstParmStruct*) pUserInitData)->nRows[iSlot]; iR++)   
					{
						// #define GET_STATUS_CHAR(n)  (((InstParmStruct*) pUserInitData)->nElementState[n] ? 'X':'O')
						wsprintf (lpszP++, "%c", GET_STATUS_CHAR (iSlot * 100 + iC * 10 + iR));
					}
				}
				wsprintf (lpszP++, "\n");
			}
    }
}

/*****************************************************************************
 * void UTADLL AdviseTrace (HUTAHWMOD hModule, 
 *                          HUTAPB hParameterBlock,
 *                          LPVOID pUserInitData,
 *                          BOOL   bTraceEnabled);
 *
 * This function is truly optional.  If your instrument handler needs
 * to know when tracing is turned on or off for the particular 
 * instance (you may have two or more Muxes configured in the system)
 * of the instrument, use this routine.  It will be called whenever 
 * tracing is turned on or off.  
 */
#if 0 /* We don't need this one... */
void UTADLL AdviseTrace (HUTAHWMOD hModule, HUTAPB hParameterBlock, LPVOID pUserInitData, BOOL bTraceEnabled)
{
	int unused = 1;
}
#endif

/*****************************************************************************
 * void UTADLL AdviseMonitor (HUTAHWMOD hModule, 
 *                            HUTAPB hParameterBlock,
 *                            LPVOID pUserInitData);
 *
 * This function is truly optional.  If your hardware handler needs to 
 * periodically  monitor the hardware, you can use this routine.  An example
 * is to monitor a button, temperature sensor, etc.  We recommend that
 * you model the button only and pass in, as  parameters, the instrument
 * (i.e a digital I/O card), and channel that needs to be examined.
 * 
 */
#if 0 /* We don't need this one... */
static int nCount = 0;
static int nSkip = 0;
void UTADLL AdviseMonitor (HUTAHWMOD hModule, HUTAPB hParameterBlock, LPVOID pUserInitData)
{
  /*
   * Example:
   * Monitor the digital I/O channel for the button press. 
   * Upon detection, you might set operator abort or change
   * a value in the symbol table (operator yes/no buttons).
   *
   * As an alternative, you might set a flag in your 
   * "pUserInitData".  If you export a function that takes
   * a HUTAINST as the parameter, any action can poll for the
   * flag. The exported function would get the pUserInit data
   * with UtaInstGetUserData API which uses the HUTAINST parameter.  
   * The HUTAINST is the hardware module that this handler was 
   * written for.
   */ 

	nSkip++;
	if (nSkip > 15)
    {
      nCount++;
      UtaHwModTraceEx (hModule, "%d Count \n", nCount);
      nSkip = 0;
    }
}
#endif

/*****************************************************************************
 *****************************************************************************
 *
 * Utility functions unique to this Switch Handler.  TestExecSL will never
 * call these function directly.  They exist only to make life in the
 * required entry points a little easier.
 *
 ******************************************************************************
 *****************************************************************************/

static void UpdateState (InstParmStruct* pParmStruct, int nElement, int nPosition)
{
	int  iIndex;

	iIndex = nElement; 
	pParmStruct->nElementState[iIndex] = nPosition;
}

/*****************************************************************************/

static void ResetState (InstParmStruct* pParmStruct )
{
	int i;

    /*
     * Set all elements of the state-tracking array to 0.
     */
    for (i=0; i < SWITCH_NUM_ELEMENTS; i++)
		pParmStruct->nElementState[i] = OPEN;
}

/********************************* END-OF-FILE *******************************/



/*****************************************************************************/
// HP34970_AutoConfig
//
// This function does the auto configuration of HP34970A

int HP34970_AutoConfig (InstParmStruct* pParmStruct, HUTAHWMOD hModule, HUTAPB hPB)
{
	int  iModel, iSlot;
	char *Function = "hwh34970a - AutoConfig";


	if (pParmStruct->nAutoConf != 0)
	{
		char szAddr[32];
		char szNode[80];
		char szTxt[128];
		char szResponse[80];
		ViSession vi;


		if (pParmStruct->nAddress > 0)
		{
			if (viOpenDefaultRM (&pParmStruct->defaultRM) != VI_SUCCESS)
			{
 				MessageBox (NULL, "Error in call to viOpenDefaultRM", Function, MB_OK);
				return -1;
			}
		
			if (pParmStruct->nBaudrate > 0)
				wsprintf (szAddr, "ASRL%d::INSTR", pParmStruct->nAddress);
			else
				wsprintf (szAddr, "GPIB%d::%d::INSTR",
					  pParmStruct->nBoardNumber,
					  pParmStruct->nAddress);
			if (viOpen (pParmStruct->defaultRM, szAddr, VI_NULL, VI_NULL, &vi) != VI_SUCCESS)
			{
				strcpy (szTxt, "Error in call to viOpen\nAddress ");
				strcat (szTxt, szAddr);
				MessageBox (NULL, szTxt, Function, MB_OK);
				return -1;
			}

			// Set the Baudrate
			if (pParmStruct->nBaudrate > 0)
			{
				if (viSetAttribute (vi, VI_ATTR_ASRL_BAUD, pParmStruct->nBaudrate) != VI_SUCCESS)
				{	 
					MessageBox (NULL, "Error setting baudrate", Function, MB_OK);
					viClose (pParmStruct->defaultRM);
					return -1;
				}
			}

			// Is the Internal DMM installed?
			if (viPrintf (vi, "Inst:DMM:Installed?\n") != VI_SUCCESS)
			{
				MessageBox (NULL, "Error accessing the instrument (DMM?)", Function, MB_OK);
				viClose (pParmStruct->defaultRM);
				return -1;
			}
			if (viScanf (vi, "%d%*t", &iModel) != VI_SUCCESS)
			{
				MessageBox (NULL, "Error reading from instrument (DMM?)", Function, MB_OK);
				viClose (pParmStruct->defaultRM);
				return -1;
			}

			if (iModel)
			{
				wsprintf (szTxt, "Internal DMM found.\n\nAccept the DMM in the configuration?"); 
				if (MessageBox (NULL, szTxt, "HP34970A. AutoConfig DMM", MB_YESNO) == IDYES)
				{
					pParmStruct->nDmmUsage = iModel;
					UtaPbSetInt32 (hPB, "iDMM", iModel);
					wsprintf (szNode, "iDMM");
					wsprintf (szTxt, "Internal DMM Input (Sense & 4W Source)");
					UtaHwModDeclareNode (hModule, szNode, szTxt, NULL);
				}
				else
				{
					pParmStruct->nDmmUsage = 0;
					UtaPbSetInt32 (hPB, "iDMM", 0);
			
				}
			}

			// Read Card ID
			for (iSlot = 100; iSlot <= 300; iSlot += 100)
			{
				if (viPrintf (vi, "Syst:CType? %d\n", iSlot) != VI_SUCCESS)
				{
					MessageBox (NULL, "Error accessing the instrument (CardType?)", Function, MB_OK);
					viClose (pParmStruct->defaultRM);
					return -1;
				}
				if (viScanf (vi, "%t", szResponse) != VI_SUCCESS)
				{
					MessageBox (NULL, "Error reading from instrument (CardType)", Function, MB_OK);
					viClose (pParmStruct->defaultRM);
					return -1;
				}

				wsprintf (szTxt, "HP34970A. AutoConfig Slot %d", iSlot); 
				strcat (szResponse, "\n\nAccept the module in the configuration?");
				if (MessageBox (NULL, szResponse, szTxt, MB_YESNO) != IDYES)
				  wsprintf (szResponse, "");

					 if (strstr  (szResponse, "34901") != NULL) iModel = 34901;
				else if (strstr  (szResponse, "34902") != NULL) iModel = 34902;
				else if (strstr  (szResponse, "34903") != NULL) iModel = 34903;
				else if (strstr  (szResponse, "34904") != NULL) iModel = 34904;
				else if (strstr  (szResponse, "34905") != NULL) iModel = 34905;
				else if (strstr  (szResponse, "34906") != NULL) iModel = 34906;
				else if (strstr  (szResponse, "34907") != NULL) iModel = 34907;
				else if (strstr  (szResponse, "34908") != NULL) iModel = 34908;
				else /* No Switch Card */                       iModel = 0;

				switch (iSlot)
				{
				case 100:	pParmStruct->nModel1 = iModel; 
							UtaPbSetInt32 (hPB, "Model1", iModel);
							break;
				case 200:	pParmStruct->nModel2 = iModel;
							UtaPbSetInt32 (hPB, "Model2", iModel);
							break;
				case 300:	pParmStruct->nModel3 = iModel;
							UtaPbSetInt32 (hPB, "Model3", iModel);
							break;
				default: ;
				}
			}

			viClose (pParmStruct->defaultRM);
		} 

		else
		{
			MessageBox (NULL, "Instrument in non-live Mode (Address = 0)", "Automatic Configuration", MB_OK);
		}
	}
	return 0;
}

/*****************************************************************************/
// HP34970_SetParameters
//
// This function sets the parameters Columns, Rows, TimeToReset and TimeToSetPosition
// according to card found.
 

void HP34970_SetParameters (InstParmStruct *pParmStruct)
{
	int iSlot, iModel;

	for (iSlot = 1; iSlot <= 3; iSlot++)
	{
		switch (iSlot)
		{
		case 1: iModel = pParmStruct->nModel1; break;
		case 2: iModel = pParmStruct->nModel2; break;
		case 3: iModel = pParmStruct->nModel3; break;
		default:;
		}

		switch (iModel)
		{
		case 34901: pParmStruct->nColumns[iSlot]	= HP34901_COLUMNS;
					pParmStruct->nRows[iSlot]		= HP34901_ROWS;
					pParmStruct->nTimeReset[iSlot]	= HP34901_TIMETORESET;
					pParmStruct->nTimeSetPos[iSlot] = HP34901_TIMETOSETPOSITION;
					break;
		case 34902: pParmStruct->nColumns[iSlot]	= HP34902_COLUMNS;
					pParmStruct->nRows[iSlot]		= HP34902_ROWS;
					pParmStruct->nTimeReset[iSlot]	= HP34902_TIMETORESET;
					pParmStruct->nTimeSetPos[iSlot] = HP34902_TIMETOSETPOSITION;
					break;
		case 34903: pParmStruct->nColumns[iSlot]	= HP34903_COLUMNS;
					pParmStruct->nRows[iSlot]		= HP34903_ROWS;
					pParmStruct->nTimeReset[iSlot]	= HP34903_TIMETORESET;
					pParmStruct->nTimeSetPos[iSlot] = HP34903_TIMETOSETPOSITION;
					break;
		case 34904: pParmStruct->nColumns[iSlot]	= HP34904_COLUMNS;
					pParmStruct->nRows[iSlot]		= HP34904_ROWS;
					pParmStruct->nTimeReset[iSlot]	= HP34904_TIMETORESET;
					pParmStruct->nTimeSetPos[iSlot] = HP34904_TIMETOSETPOSITION;
					break;
		case 34905: pParmStruct->nColumns[iSlot]	= HP34905_COLUMNS;
					pParmStruct->nRows[iSlot]		= HP34905_ROWS;
					pParmStruct->nTimeReset[iSlot]	= HP34905_TIMETORESET;
					pParmStruct->nTimeSetPos[iSlot] = HP34905_TIMETOSETPOSITION;
					break;
		case 34906: pParmStruct->nColumns[iSlot]	= HP34906_COLUMNS;
					pParmStruct->nRows[iSlot]		= HP34906_ROWS;
					pParmStruct->nTimeReset[iSlot]	= HP34906_TIMETORESET;
					pParmStruct->nTimeSetPos[iSlot] = HP34906_TIMETOSETPOSITION;
					break;
		case 34907: pParmStruct->nColumns[iSlot]	= HP34907_COLUMNS;
					pParmStruct->nRows[iSlot]		= HP34907_ROWS;
					pParmStruct->nTimeReset[iSlot]	= HP34907_TIMETORESET;
					pParmStruct->nTimeSetPos[iSlot]	= HP34907_TIMETOSETPOSITION;
					break;
		case 34908: pParmStruct->nColumns[iSlot]	= HP34908_COLUMNS;
					pParmStruct->nRows[iSlot]		= HP34908_ROWS;
					pParmStruct->nTimeReset[iSlot]	= HP34906_TIMETORESET;
					pParmStruct->nTimeSetPos[iSlot] = HP34906_TIMETOSETPOSITION;
					break;
		default:;
		}
	}
}

/*****************************************************************************/
// HP34970_CreateNodes
// 
// This function creates the nodes (system.ust) for iDMM and the cards

void HP34970_CreateNodes (InstParmStruct *pParmStruct, HUTAHWMOD hModule, int iSlot)
{
	int   iModel, iC, iR;
	char  szNode[32];
	char  szTxt[128];


	// Create Nodes for Rows and Columns for all 3 Slots
	for (iSlot = 0; iSlot <= 3; iSlot++)
	{

		switch (iSlot)
		{
		case 0: iModel = pParmStruct->nDmmUsage; break; 
		case 1: iModel = pParmStruct->nModel1; break;
		case 2: iModel = pParmStruct->nModel2; break;
		case 3: iModel = pParmStruct->nModel3; break;
		default:;
		}

		// Create Nodes for the switching cards of HP34970A
		switch (iModel)
		{
		case 0:		// No card installed
					break;
		case 1:		// Create Notes for iDMM
					wsprintf (szNode, "iDMM");
					wsprintf (szTxt, "Internal DMM");
					UtaHwModDeclareNode (hModule, szNode, szTxt, NULL);
					break;
		case 34901:
		case 34902:
		case 34908:
					// Declare Nodes for Mux
					for (iC = 1; iC <= pParmStruct->nColumns[iSlot]; iC++)
					{
						wsprintf (szNode, "Com%3.3d.%d", iSlot * 100, iC);
						wsprintf (szTxt, "Common %d.%d", iSlot * 100, iC);
						UtaHwModDeclareNode (hModule, szNode, szTxt, NULL);

						// Create the channels for the current COM
						for (iR = 1; iR <= pParmStruct->nRows[iSlot]; iR++)
						{
							wsprintf (szNode, "Ch%3.3d", iSlot * 100 + iR);
							wsprintf (szTxt, "Channel %d", iSlot * 100 + iR);
							UtaHwModDeclareNode (hModule, szNode, szTxt, NULL);
						}
					}
					// Declare Adjacent Nodes for Mux
					for (iC = 1; iC <= pParmStruct->nColumns[iSlot]; iC++)
					{
						wsprintf (szNode, "Com%3.3d.%d", iSlot * 100, iC);
						for (iR = 1; iR <= pParmStruct->nRows[iSlot]; iR++)
						{
							wsprintf (szTxt, "Ch%3.3d", iSlot * 100 + iR);
							UtaHwModDeclareAdjacent (hModule, szNode, szTxt, 
								(short) (iSlot * 100 + iR), 1);
						}
					}
					// Declare Adjacent Nodes for Mux to Internal DMM
					if (pParmStruct->nDmmUsage)
					{
						iC = 1;
						wsprintf (szNode, "iDMM");
						for (iR = 1; iR <= pParmStruct->nRows[iSlot]; iR++)
						{
							wsprintf (szTxt, "Ch%3.3d", iSlot * 100 + iR);
							UtaHwModDeclareAdjacent (hModule, szNode, szTxt, 
								(short) (iSlot * 100 + iR), 1);
						}
					}

					if (iModel == 34901)
					{
						wsprintf (szNode, "Com%3.3d.C", iSlot * 100);
						wsprintf (szTxt, "Common %d Current", iSlot * 100);
						UtaHwModDeclareNode (hModule, szNode, szTxt, NULL);

						for (iR = 21; iR <= 22; iR++)
						{
							wsprintf (szNode, "Ch%3.3d", iSlot * 100 + iR);
							wsprintf (szTxt, "Channel %d", iSlot * 100 + iR);
							UtaHwModDeclareNode (hModule, szNode, szTxt, NULL);
						}

						wsprintf (szNode, "Com%3.3d.C", iSlot * 100);
						wsprintf (szTxt, "Ch%3.3d", iSlot * 100 + 21);
						UtaHwModDeclareAdjacent (hModule, szNode, szTxt, 
								(short) (iSlot * 100 + 21), 1);
						wsprintf (szNode, "Com%3.3d.C", iSlot * 100);
						wsprintf (szTxt, "Ch%3.3d", iSlot * 100 + 22);
						UtaHwModDeclareAdjacent (hModule, szNode, szTxt,
								(short)(iSlot * 100 + 22), 1);

						// Declare Adjacent Nodes for Mux to Internal DMM
						if (pParmStruct->nDmmUsage)
						{
							iC = 1;
							wsprintf (szNode, "iDMM");
							for (iR = 21; iR <= 22; iR++)
							{
								wsprintf (szTxt, "Ch%3.3d", iSlot * 100 + iR);
								UtaHwModDeclareAdjacent (hModule, szNode, szTxt, 
									(short) (iSlot * 100 + iR), 1);
							}
						}
					}
					break;

		case 34905:
		case 34906:
					// Create Nodes for RF-Mux
					for (iC = 1; iC <= pParmStruct->nColumns[iSlot]; iC++)
					{
						wsprintf (szNode, "Com%3.3d.%d", iSlot * 100, iC);
						wsprintf (szTxt, "Common %d.%d", iSlot * 100, iC);
						UtaHwModDeclareNode (hModule, szNode, szTxt, NULL);

						// Create the channels for the current COM
						for (iR = 1; iR <= pParmStruct->nRows[iSlot]; iR++)
						{
							wsprintf (szNode, "Ch%3.3d", iSlot * 100 + (iC * 10) + iR);
							wsprintf (szTxt, "Channel %d", iSlot * 100 + (iC * 10) + iR);
							UtaHwModDeclareNode (hModule, szNode, szTxt, NULL);
						}
					}
					// Declare Adjacent Nodes for Mux
					for (iC = 1; iC <= pParmStruct->nColumns[iSlot]; iC++)
					{
						wsprintf (szNode, "Com%3.3d.%d", iSlot * 100, iC);
						for (iR = 1; iR <= pParmStruct->nRows[iSlot]; iR++)
						{
							wsprintf (szTxt, "Ch%3.3d", iSlot * 100 + (iC * 10) + iR);
							UtaHwModDeclareAdjacent (hModule, szNode, szTxt, 
								(short)((iSlot * 100) + iR + iC * 10), 1);
						}
					}
					break;

		case 34903:	// Declare Nodes for Switch
					for (iR = 1; iR <= pParmStruct->nRows[iSlot]; iR++)
					{
						wsprintf (szNode, "NO%3.3d", iSlot * 100 + iR);
						wsprintf (szTxt, "Relay %d Normally Open", iSlot * 100 + iR);
						UtaHwModDeclareNode (hModule, szNode, szTxt, NULL);

						wsprintf (szNode, "NC%3.3d", iSlot * 100 + iR);
						wsprintf (szTxt, "Relay %d Normally Closed", iSlot * 100 + iR);
						UtaHwModDeclareNode (hModule, szNode, szTxt, NULL);

						wsprintf (szNode, "COM%3.3d", iSlot * 100 + iR);
						wsprintf (szTxt, "Relay %d Common", iSlot * 100 + iR);
						UtaHwModDeclareNode (hModule, szNode, szTxt, NULL);
					}
					// Declare Adjacenct Nodes for Switch
					for (iR = 1; iR <= pParmStruct->nRows[iSlot]; iR++)
					{
						wsprintf (szNode, "NO%3.3d", iSlot * 100 + iR);
						wsprintf (szTxt, "COM%3.3d", iSlot * 100 + iR);
						UtaHwModDeclareAdjacent (hModule, szNode, szTxt, 
							(short) ((iSlot * 100) + iR), 1);

						wsprintf (szNode, "NC%3.3d", iSlot * 100 + iR);
						wsprintf (szTxt, "COM%3.3d", iSlot * 100 + iR);
						UtaHwModDeclareAdjacent (hModule, szNode, szTxt, 
							(short) (iSlot * 100 + iR), 0);
					}
					break;

		case 34904:	// Declare Nodes for Matrix
					for (iR = 1; iR <= pParmStruct->nRows[iSlot]; iR++)
					{
						wsprintf (szNode, "Row%3.3d", iSlot * 100 + iR);
						wsprintf (szTxt, "Row %d", iSlot * 100 + iR);
						UtaHwModDeclareNode (hModule, szNode, szTxt, NULL);
					}
					for (iC = 1; iC <= pParmStruct->nColumns[iSlot]; iC++)
					{
						wsprintf (szNode, "Col%3.3d", iSlot * 100 + iC);
						wsprintf (szTxt, "Column %d", iSlot * 100 + iC);
						UtaHwModDeclareNode (hModule, szNode, szTxt, NULL);
					}
					// Declare Adjacent Nodes for Matrix
					for (iR = 1; iR <= pParmStruct->nRows[iSlot]; iR++)
					{
						wsprintf (szNode, "Row%3.3d", iSlot * 100 + iR);
						for (iC = 1; iC <= pParmStruct->nColumns[iSlot]; iC++)
						{
							wsprintf (szTxt, "Col%3.3d", iSlot * 100 + iC);
							UtaHwModDeclareAdjacent (hModule, szNode, szTxt,
								(short)(iSlot * 100 + iR * 10 + iC), 1);
						}
					}
					break;

		case 34907:	// Declare Nodes for Digital-I/O
					for (iC = 1; iC <= pParmStruct->nColumns[iSlot]; iC++)
					{
						wsprintf (szNode, "Ch%3.3d.P%d", iSlot * 100 + iC, iC);
						wsprintf (szTxt, "Digital I/O, Port %d, Channel %d", iC, iSlot * 100 + iC);
						UtaHwModDeclareNode (hModule, szNode, szTxt, NULL);

						for (iR = 1; iR <= pParmStruct->nRows[iSlot]; iR++)
						{
							wsprintf (szNode, "Ch%3.3d.Bit%d", iSlot * 100 + iC, (iR - 1));
							wsprintf (szTxt, "Digital I/O, Port %d, Bit%d", iC, (iR - 1));
							UtaHwModDeclareNode (hModule, szNode, szTxt, NULL);
						}
					}

					// Declare Adjacent Nodes for Digital-I/O
					for (iC = 1; iC <= pParmStruct->nColumns[iSlot]; iC++)
					{
						wsprintf (szNode, "Ch%3.3d.P%d", iSlot * 100 + iC, iC);
						UtaHwModDeclareAdjacent (hModule, szNode, szNode,
							(short) (iSlot * 100 + iC * 10), 1);

						for (iR = 1; iR <= pParmStruct->nRows[iSlot]; iR++)
						{
							wsprintf (szNode, "Ch%3.3d.Bit%d", iSlot * 100 + iC, (iR - 1));
							wsprintf (szTxt, "Ch%3.3d.Bit%d", iSlot * 100 + iC, (iR - 1));
							UtaHwModDeclareAdjacent (hModule, szNode, szTxt, 
								(short) (iSlot * 100 + iC * 10 + (iR - 1)), 1);
						}

					}


					// Create Node for Totalizer
					wsprintf (szNode, "Ch%3.3d.Tot", iSlot * 100 + 3);
					wsprintf (szTxt, "Totalizer Channel %d", iSlot * 100 + 3);
					UtaHwModDeclareNode (hModule, szNode, szTxt, NULL);
					UtaHwModDeclareAdjacent (hModule, szNode, szNode, (short) (iSlot * 100 + 3));
					
					// Create Node for AnalogOut
					wsprintf (szNode, "Ch%3.3d.D/A", iSlot * 100 + 4);
					wsprintf (szTxt, "Analog Output Channel %d", iSlot * 100 + 4);
					UtaHwModDeclareNode (hModule, szNode, szTxt, NULL);
					UtaHwModDeclareAdjacent (hModule, szNode, szNode, (short) (iSlot * 100 + 4));

					wsprintf (szNode, "Ch%3.3d.D/A", iSlot * 100 + 5);
					wsprintf (szTxt, "Analog Output Channel %d", iSlot * 100 + 5);
					UtaHwModDeclareNode (hModule, szNode, szTxt, NULL);
					UtaHwModDeclareAdjacent (hModule, szNode, szNode, (short) (iSlot * 100 + 5));

					break;

		default:	wsprintf (szTxt, "Incorrect number for Model%d.\nValid numbers are 34901-34908", iSlot);
					MessageBox (NULL, szTxt, "HP34970A Configuration", MB_OK);
		}

	}
}


///////////////////////////////////////////////////////////////////////////////////////////////
// DemoTimeElapsed 
//
// This function checks the ticks for the demo mode

int DemoTimeElapsed (DWORD TicEnd)
{
	if (DEMOSECONDS	> 0)
	{
		if (GetTickCount () < TicEnd)
			return 0;
		else
		{
			VSendReportMsg ("\n");
			VSendReportMsg ("\n");
			VSendReportMsg ("***********************************\n");
			VSendReportMsg ("* DEMO TIME FOR HWH34970A ELAPSED *\n");
			UtaExcRaiseUserError ("",9);
			return 1;
		}
	}
	return 0;
}


/*****************************************************************************/
/*****************************************************************************/

#if defined __cplusplus 
	}
#endif 


