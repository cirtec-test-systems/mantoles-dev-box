using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace VISAExample
//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
//34970A VISA and C# Measurement Example Program - This program sets the
// 34970A for 2 simple scans that make both DCV and type J thermocouple temperature
// measurements. The program also checks the instrument and module identification to
// make sure there is communication between the 34970A and the computer, and the
// correct module is installed. The returned data is entered into a list box.
//
// This program requires a 34901A module
//
// Included is an error checking routine to make sure the SCPI commands executed have
// the correct syntax.
//
// The program requires that VISA is installed in your computer. VISA comes with the
// Agilent I/O Library.
//
// Include the visa32.cs file. This file comes with VISA version M.01.01.041 or above. 
//
// The program was developed in Microsoft� C# (C Sharp) .NET
//
// The program can use either the user selectable GPIB or RS-232 interface. If
// selecting RS-232, the 34970A must be set to the following RS-232 parameters:
//   Baud Rate: 115200
//   Parity: None
//   Data bits: 8
//   Start bits: 1
//   Stop bits: 1
//   Flow control: XON/XOFF
//
//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
// Copyright � 2004 Agilent Technologies Inc. All rights reserved.
//
// You have a royalty-free right to use, modify, reproduce and distribute this
// example files (and/or any modified version) in any way you find useful, provided
// that you agree that Agilent has no warranty, obligations or liability for any
// Sample Application Files.
//
// Agilent Technologies will not modify the program to provide added
// functionality or construct procedures to meet your specific needs.
//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
//
{
	/// <summary>
	/// Summary description for VISAExample.
	/// </summary>
	public class VisaExample : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button GetReadings;
		private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.Button EndProg;
		private System.Windows.Forms.TextBox ioType;
		private System.Windows.Forms.Button SelectIO;
		private System.Windows.Forms.Label label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public VisaExample()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
						
			// Add initial values to list box
			listBox1.Items.Add("Enter/select instrument address, if needed,");
			listBox1.Items.Add("click on \"Select I/O\" to select the adress,");
			listBox1.Items.Add("and click on \"Get Readings\" to trigger instrument.");
			listBox1.Items.Add("Measurements will take some time.");
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.GetReadings = new System.Windows.Forms.Button();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.EndProg = new System.Windows.Forms.Button();
			this.ioType = new System.Windows.Forms.TextBox();
			this.SelectIO = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// GetReadings
			// 
			this.GetReadings.Location = new System.Drawing.Point(120, 96);
			this.GetReadings.Name = "GetReadings";
			this.GetReadings.Size = new System.Drawing.Size(96, 32);
			this.GetReadings.TabIndex = 0;
			this.GetReadings.Text = "Get Readings";
			this.GetReadings.Click += new System.EventHandler(this.GetReadings_Click);
			// 
			// listBox1
			// 
			this.listBox1.Location = new System.Drawing.Point(16, 136);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(304, 173);
			this.listBox1.TabIndex = 2;
			// 
			// EndProg
			// 
			this.EndProg.Location = new System.Drawing.Point(120, 320);
			this.EndProg.Name = "EndProg";
			this.EndProg.Size = new System.Drawing.Size(96, 32);
			this.EndProg.TabIndex = 1;
			this.EndProg.Text = "Exit";
			this.EndProg.Click += new System.EventHandler(this.EndProg_Click);
			// 
			// ioType
			// 
			this.ioType.Location = new System.Drawing.Point(80, 48);
			this.ioType.Name = "ioType";
			this.ioType.Size = new System.Drawing.Size(64, 20);
			this.ioType.TabIndex = 3;
			this.ioType.Text = "GPIB0::9";
			// 
			// SelectIO
			// 
			this.SelectIO.Location = new System.Drawing.Point(160, 40);
			this.SelectIO.Name = "SelectIO";
			this.SelectIO.Size = new System.Drawing.Size(96, 32);
			this.SelectIO.TabIndex = 4;
			this.SelectIO.Text = "Select I/O";
			this.SelectIO.Click += new System.EventHandler(this.SelectIO_Click);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(44, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(248, 40);
			this.label1.TabIndex = 5;
			this.label1.Text = "\"Enter/select Instrument Address (e.g., GPIB0::9, ASRL1, etc.), click on \"Select " +
				"I/O\", and then click on \"Get Readings\".";
			// 
			// VisaExample
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(336, 357);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.label1,
																		  this.SelectIO,
																		  this.ioType,
																		  this.EndProg,
																		  this.listBox1,
																		  this.GetReadings});
			this.Name = "VisaExample";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "VisaExample";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]

		static void Main() 
		{
			Application.Run(new VisaExample());
		}

		public class Globls
		//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		// Sets global variables to be used in different functions of the program.
		//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		{ 
			public static int videfaultRM = 0;	// Resource manager session returned by viOpenDefaultRM(videfaultRM)
			public static int vi = 0;			// Session identifier of devices
			public static int errorStatus;		// VISA function status return code
			public static string addrtype;		// Stores the address type, GPIB or RS232
			public static bool connected = false;  // Used to determine if there is connection with the instrument

			public static int NumRdgs;			// Used for the number of readings taken
			public static double TotTime;		// Used to calculate total measurement time
			public static float TrigCount;		// Used to determine number of scans
			public static short NumChan;		// Used to determine the number of channels scanned
		} 

		public void RunProg()
		//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		// This function executes the main program.
		//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		{
			// Call the function that opens communication with instrument
			if (!Globls.connected)
				if (!OpenPort())
					return;

			// Abort a scan, if one is in progress
			SendCmd("ABORt");
			
			// Call function to setup the 34970A
			Setup();
			
			listBox1.Items.Clear();
			listBox1.Items.Add("Scanning and making measurements; please wait.");
			listBox1.Items.Add("The instrument will scan through the channels once,");
			listBox1.Items.Add("then wait a pre-determined time, and scan again.");
			listBox1.Items.Add("Measurement time is about: " + Globls.TotTime.ToString() + " seconds.");
			listBox1.Refresh();

			//  Call function to trigger the 34970A, make measurements, and return the readings
			Readings();

			// Enable Exit button
			EndProg.Focus();
		}

		public double Setup()
		//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		// This function performs the instrument setup.
		//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		{
			double DelayVal, TrigTime;
			string rdg;

			// Check for exceptions			
			try
			{
				// Reset instrument to turn-on condition
				SendCmd("*RST");
				
				// Configure for temperature measurements
				//  Select channels 101 to 110
				//  Type J thermocouple measurement
				//  5.5 digit (selected by *RST)
				SendCmd("CONFigure:TEMPerature TCouple, K, (@101:110)");
				
				// Select the temperature unit (C = Celcius)
				SendCmd("UNIT:TEMPerature C, (@101:110)");
		
				// Set the reference temperature type (internal)
				SendCmd("SENSe:TEMPerature:TRANSducer:TCouple:RJUNction:TYPE INTernal, (@101:110)");
		
				// Configure for voltage readings:
				//   Select channels 111 to 120
				//   DC volts
				//   10 V range
				//   5.5 digit (selected by *RST)
				SendCmd("CONFigure:VOLTage:DC 10, (@111:120)");
				
				// Set the NPLC value for channels 111 to 120
				SendCmd("SENSe:VOLTage:NPLC 1,(@111:120)");
		
				// Select the scan list for channels 101 to 120 (all configured channels)
				SendCmd("ROUTe:SCAN (@101:120)");
		
				// Set the measurement delay between the channels
				SendCmd("ROUTe:CHANnel:DELay 0.25, (@101:120)");
		
				// Set number of sweeps to 2; use your own value
				SendCmd("TRIGger:COUNt 2");
		
				// Set the trigger mode to TIMER (timed trigger); use your own type
				SendCmd("TRIGger:SOURce TIMer");
		
				// Set the trigger time to 10 seconds (i.e., time between scans); use your own value
				SendCmd("TRIGger:TIMer 10");
		
				// Format the reading time to show the time value from the start of the scan
				SendCmd("FORMat:READing:TIME:TYPE RELative");
		
				// Add time stamp to reading using the selected time format
				SendCmd("FORMat:READing:TIME ON");
		
				// Add the channel number to reading
				SendCmd("FORMat:READing:CHANnel ON");
		
				// Wait for instrument to setup
				SendCmd("*OPC?");
				rdg = GetData();

				// Gets the number of channels to be scanned and is used to determine 
				// the number of readings; 34970A returns a short number in ASCII format.
				SendCmd("ROUTe:SCAN:SIZE?");
				if (Globls.addrtype == "ASRL")
					Globls.NumChan = Convert.ToInt16(GetData());
				else
					Globls.errorStatus = visa32.viScanf(Globls.vi, "%d", out Globls.NumChan);

				// Gets the number of triggers; 34970A returns a floating-point number
				// in ASCII format.
				SendCmd("TRIGger:COUNt?");
				if (Globls.addrtype == "ASRL")
					Globls.TrigCount = Convert.ToSingle(GetData());
				else
					Globls.errorStatus = visa32.viScanf(Globls.vi, "%f", out Globls.TrigCount);

				// Get the delay; for future use; 34970A returns a floating-point number
				// in ASCII format.
				SendCmd("ROUTe:CHANnel:DELay? (@101)");
				if (Globls.addrtype == "ASRL")
					DelayVal = Convert.ToDouble(GetData());
				else
					Globls.errorStatus = visa32.viScanf(Globls.vi, "%lf", out DelayVal);

				// Get the trigger time; 34970A returns a floating-point number
				// in ASCII format.
				SendCmd("TRIGger:TIMer?");
				if (Globls.addrtype == "ASRL")
					TrigTime = Convert.ToDouble(GetData());
				else
					Globls.errorStatus = visa32.viScanf(Globls.vi, "%lf", out TrigTime);

				// Calculate total number of readings
				Globls.NumRdgs = Globls.NumChan * Convert.ToInt16(Globls.TrigCount);

				// Calculate total time
				Globls.TotTime = (TrigTime * Globls.TrigCount) - TrigTime + (Globls.NumChan * DelayVal);

				//Check for errors
				Check_Error("Setup");

				return Globls.TotTime;
			}
			catch(Exception e) 
			{
				System.Windows.Forms.MessageBox.Show(e.Message + "\nin function: Setup", "VISAExample");
				End_Prog();
				return 0;
			}
		}

		public void Readings()
		//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		// This function triggers the instrument and takes readings.
		//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		{
			string	rdgs,		// Returns a reading from memory
				Dateval,	// Returns the date value
				Timeval,	// Returns the time value
				reading,	// Used to store reading
				timestamp,	// Used to store time stamp
				channelnum;	// Used to store channel number
			int		i;			// Used for a loop

			// Check for exceptions			
			try
			{
				// Trigger instrument
				SendCmd("INITiate");
				
				// Get the date at which the scan was started
				SendCmd("SYSTem:DATE?");
				Dateval = GetData();
				
				// Get the time at which the scan was started
				SendCmd("SYSTem:TIME?");
				Timeval = GetData();
				
				// Wait until instrument is finished taken readings. The instrument is queried 
				// until all channels are measured.
				do 
				{
					SendCmd("DATA:POINTS?");
					i = (Convert.ToInt16(GetData()));
				}
				while(i != Globls.NumRdgs);

				// Check for errors
				Check_Error("Readings");

				listBox1.Items.Clear();
				listBox1.Items.Add("Enter/select instrument address, if needed;");
				listBox1.Items.Add("Click on \"Get Readings\" to trigger instrument.");
				listBox1.Items.Add("Measurements will take some time.");
		
				listBox1.Items.Add("");
				listBox1.Items.Add("Start Date (yyyy,mm,dd): " + Dateval);
				listBox1.Items.Add("Start Time (hh,mm,ss): " + Timeval);
				listBox1.Items.Add("Rdng#\tChannel\tValue\tTime");
				listBox1.Refresh();

				// Take readings out of memory one reading at a time. The "FETCh?" can also be used.
				// It reads all readings in memory, but leaves the readings in memory. The
				// "DATA:REMove?" command removes and erases the readings in memory.
				for (i=0; i<Globls.NumRdgs; i++)
				{
					SendCmd("DATA:REMove? 1");
					rdgs = GetData();

					// Get readingg
					reading = rdgs.Substring(0,rdgs.IndexOf(","));

					// Get timestamp
					rdgs = rdgs.Substring(rdgs.IndexOf(",") + 1, (rdgs.Length - reading.Length - 1));
					timestamp = rdgs.Substring(0,rdgs.IndexOf(","));

					// Get channel number
					channelnum = rdgs.Substring(rdgs.IndexOf(",") + 1, (rdgs.Length - timestamp.Length - 1));

					listBox1.Items.Add((i+1).ToString() + "\t" + channelnum + "\t" + reading + "\t" + timestamp); 			
				}
			}
			catch(Exception e) 
			{
				System.Windows.Forms.MessageBox.Show(e.Message + "\nin function: Readings", "VISAExample");
				End_Prog();
			}
		}

		public bool OpenPort()
		//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		// This function opens a port (the communication between the instrument and
		// computer).
		//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		{
			string addr, rdg;
			// Check for exceptions			
			try
			{
			
				// Get address and type (GPIB or ASRL), and do an initial check
				addr = (ioType.Text).ToUpper();
				if (addr.Length < 5)
				{
					System.Windows.Forms.MessageBox.Show("Incorrect address; check address", "VISAExample");
					ioType.Text = "GPIB0::9";
					ioType.Refresh();
					Globls.connected = false;
					return false;
				}
				else
					Globls.addrtype = addr.Substring(0,4);

				// If port is open, close it
				if (Globls.connected)
					Globls.errorStatus = visa32.viClose(Globls.vi);

				// Open the Visa session
				Globls.errorStatus = visa32.viOpenDefaultRM(out Globls.videfaultRM);

				// Open communication to the instrument
				Globls.errorStatus = visa32.viOpen(Globls.videfaultRM, (ioType.Text).ToUpper() + "::INSTR", 0, 0, out Globls.vi);
				
				// If an error occurs, give a message
				if (Globls.errorStatus < visa32.VI_SUCCESS)
				{
					System.Windows.Forms.MessageBox.Show("Unable to Open port; check address", "VISAExample");
					ioType.Text = "GPIB0::9";
					ioType.Refresh();
					Globls.connected = false;
					return false;
				}
				
				// Set timeout in milliseconds; set the timeout for your requirements
				Globls.errorStatus = visa32.viSetAttribute(Globls.vi, visa32.VI_ATTR_TMO_VALUE, 2000);
				
				// Set RS-232 parameters, if address is for S-232 (ASRLx)
				if (Globls.addrtype.CompareTo("ASRL") == 0)
				{
					Globls.addrtype = "ASRL";
					
					// Set the RS-232 parameters; refer to the 34970A and VISA documentation
					// to change the settings. Make sure the instrument and the following
					// settings agree.
					Globls.errorStatus = visa32.viSetAttribute(Globls.vi, visa32.VI_ATTR_ASRL_BAUD, 115200);
					Globls.errorStatus = visa32.viSetAttribute(Globls.vi, visa32.VI_ATTR_ASRL_DATA_BITS, 8);
					Globls.errorStatus = visa32.viSetAttribute(Globls.vi, visa32.VI_ATTR_ASRL_PARITY, visa32.VI_ASRL_PAR_NONE);
					Globls.errorStatus = visa32.viSetAttribute(Globls.vi, visa32.VI_ATTR_ASRL_STOP_BITS, visa32.VI_ASRL_STOP_ONE);
					Globls.errorStatus = visa32.viSetAttribute(Globls.vi, visa32.VI_ATTR_ASRL_FLOW_CNTRL, visa32.VI_ASRL_FLOW_XON_XOFF);
					
					// Set the instrument to remote
					SendCmd("SYSTem:REMote");
				}
		
				// Check and make sure the correct instrument is addressed
				SendCmd("*IDN?");
				rdg = GetData();
				if (rdg.IndexOf("34970A") < 0)
				{
					System.Windows.Forms.MessageBox.Show("Incorrect instrument addressed; use the correct address.", "VISAExample");
					ioType.Text = "GPIB0::9";
					ioType.Refresh();
					Globls.connected = false;
					return false;
				}

				// Enter ID string into list box
				listBox1.Items.Clear();
				listBox1.Items.Add("Instrument ID is:");
				listBox1.Items.Add(rdg);

				// Check and make sure the 34901A Module is installed in slot 100;
				// Exit program if not correct
				SendCmd("SYSTem:CTYPe? 100");
				rdg = GetData();
				if (rdg.IndexOf("34901A") < 0)
				{
					System.Windows.Forms.MessageBox.Show("Incorrect Module Installed in slot 100!", "VISAExample");
					End_Prog();
				}

				// Check if the DMM is installed; convert returned ASCII string to number.
				// Exit program if not installed
				SendCmd("INSTrument:DMM:INSTalled?");
				if (Convert.ToInt16(GetData()) == 0)
				{
					System.Windows.Forms.MessageBox.Show("DMM not installed; unable to make measurements.", "VISAExample");
					End_Prog();
				}

				// Check if the DMM is enabled;; convert returned ASCII string to number.
				// Enable the DMM, if not enabled
				SendCmd("INSTrument:DMM?");
				if (Convert.ToInt16(GetData()) == 0)
					SendCmd("INSTrument:DMM ON");

				Globls.connected = true;
				return true;
			}
			catch(Exception e) 
			{
				System.Windows.Forms.MessageBox.Show(e.Message + "\nin function: OpenPort", "VISAExample");				
				End_Prog();
				return false;
			}
		}

		public void SendCmd(string SCPICmd)
		//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		// This routine will send a SCPI command string to the instrument. If the
		// command contains a question mark (i.e., is a query command), you must
		// read the response with the 'GetData' function.
		//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		{
			try
			{
				// Write the command to the instrument (terminated by a linefeed; vbLf is ASCII character 10)
				Globls.errorStatus = visa32.viPrintf(Globls.vi, SCPICmd + "\n");

				if (Globls.errorStatus < visa32.VI_SUCCESS)
				{
					System.Windows.Forms.MessageBox.Show("I/O Error!", "VISAExample");
					End_Prog();
				}
			}
			catch(Exception e) 
			{
				System.Windows.Forms.MessageBox.Show(e.Message + "\nin function: SendCmd", "VISAExample");				
				End_Prog();
			}
		}

		public string GetData()
		//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		// This function reads the string returned by the instrument
		//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		{
			System.Text.StringBuilder msg = new System.Text.StringBuilder(2048);
			try
			{
				// Return the reading
				Globls.errorStatus = visa32.viScanf(Globls.vi, "%2048t", msg);
			
				if (Globls.errorStatus < visa32.VI_SUCCESS)
				{
					System.Windows.Forms.MessageBox.Show("I/O Error!", "VISAExample");
					End_Prog();
					return null;
				}
				// Remove carriage return or line feed and return data
				if (Globls.addrtype == "ASRL")
					return (msg.ToString()).Remove((msg.ToString().Length - 2),2);
				else
					return (msg.ToString()).Remove((msg.ToString().Length - 1),1);
			}
			catch(Exception e) 
			{
				System.Windows.Forms.MessageBox.Show(e.Message + "\nin function: GetData", "VISAExample");				
				End_Prog();
				return null;
			}
		}

		public void Check_Error(string msg)
		//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		// Checks for syntax and other errors.
		//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		{
			int err_num;
			bool exit_code = false;
			string err_msg, show_msg;

			try
			{
				// Read error queue
				SendCmd("SYSTem:ERRor?");

				err_msg = GetData();
		
				// Get error number and message
				err_num = Convert.ToInt16(err_msg.Substring(0,err_msg.IndexOf(",")));
				err_msg = err_msg.Substring((err_msg.IndexOf(",") + 1), (err_msg.Length - (Convert.ToInt16(err_msg.IndexOf(","))) - 1) );

				// If error found, check for more errors and exit program
				while (err_num != 0)
				{
					exit_code = true;

					show_msg = "Error in: \"" + msg + "\" function\n";
					show_msg += "Error Number: " + err_num.ToString() + "," + err_msg;

					System.Windows.Forms.MessageBox.Show(show_msg , "VISAExample");

					// Read error queue
					SendCmd("SYSTem:ERRor?");
					err_msg = GetData();
				
					// Get error number and message
					err_num = Convert.ToInt16(err_msg.Substring(0,err_msg.IndexOf(",")));
					err_msg = err_msg.Substring((err_msg.IndexOf(",") + 1), (err_msg.Length - (Convert.ToInt16(err_msg.IndexOf(","))) - 1) );
				}

				if (exit_code)
				{
					SendCmd("*CLS");
					
					// Close instrument session
					Globls.errorStatus = visa32.viClose(Globls.vi);

					// Close the session
					Globls.errorStatus = visa32.viClose(Globls.videfaultRM);
					
					// end the program
					Environment.Exit(0);
				}			
			
			}
			catch(Exception e) 
			{
				System.Windows.Forms.MessageBox.Show(e.Message + "\nin function: CheckError", "VISAExample");
				End_Prog();
			}
		}

		public void End_Prog()
		//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		// Closes the program
		//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		{
			if (Globls.connected)
			{
				// Close the device session
				Globls.errorStatus = visa32.viClose(Globls.vi);

				// Close the session
				Globls.errorStatus = visa32.viClose(Globls.videfaultRM);
			}
			
			//System.Windows.Forms.Form.ActiveForm.Close();
			Environment.Exit(0);
		}
		
		private void GetReadings_Click(object sender, System.EventArgs e)
		//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		// Call function routine to trigger instrument and get readings.
        //"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		{
		    // Clear list box
			listBox1.Items.Clear();

	        // Call the Readings function
			RunProg();
		}

		private void SelectIO_Click(object sender, System.EventArgs e)
		//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		// Button that selects the I/O and creates an instrument session.
		//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		{
	        OpenPort();
		}

		private void EndProg_Click(object sender, System.EventArgs e)
		//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		// Calls function to close the session and end the program.
		//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
		{
			End_Prog();		
		}
	}
			
}
