﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NationalInstruments.NI4882;

namespace DMM_Logger
{
    public partial class dmmLoggerForm : Form
    {
        private Device dmm; // declare the device       
        private bool isRunning = false;
        private string filePath;

        public dmmLoggerForm()
        {
            InitializeComponent();
            InitializeGPIBResources();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (isRunning)
            {
                isRunning = false;
                btnStartStop.Text = "Start";
                StopLogging(); // stop logging data 
            }
            else
            {
                isRunning = true;
                btnStartStop.Text = "Stop";
                // create a data file 
                filePath = $@"C:\data\dmmData-{DateTime.Now:yyyyMMddHHmmss}.csv";
                StartLogging();
            }
        }

        private void InitializeGPIBResources()
        {
            // Populate the ComboBox with GPIB resources
            cbGPIBResources.Items.Add("GPIB0::08::INSTR"); // DMM
            cbGPIBResources.SelectedIndex = 0;
        }

        private void StartLogging()
        {
            string selectedResource = cbGPIBResources.SelectedItem.ToString();
            //dmm = new Device(0,);
            dmm = new Device(0,08);

            // Configure the DMM for 4-wire resistance measurement
            //dmm.Write("SENS:FUNC 'RES'");
            //dmm.Write("SENS:RES:MODE FRES");
            //dmm.Write("SENS:RES:RANG:AUTO ON");
            dmm.Write("CONF:FRES");

            //filePath = $@"C:\data\dmmLogger-{DateTime.Now:yyyyMMddHHmmss}.csv";
            
            using (StreamWriter writer = new StreamWriter(filePath))
            {
                double[] measurements = new double[10];
                resultsBox.AppendText(Environment.NewLine);
                writer.WriteLine("Resistance:Ohm");

                while (isRunning)
                 {
                    for (int i = 0; i < 10; i++)
                    {
                        dmm.Write("READ?");
                        measurements[i] = Convert.ToDouble(dmm.ReadString());
                    }
                    double sum = 0;
                    foreach (double measurement in measurements)
                      {
                        sum += measurement;
                      }
                    double average = sum / measurements.Length;
                    writer.WriteLine(average);
                    resultsBox.AppendText(average + Environment.NewLine);
                    Application.DoEvents(); // Allow UI to update
                }
             }
            
        }

        private void StopLogging()
        {
            // Close the DMM connection
            dmm.Dispose();
            resultsBox.Clear();
            Application.DoEvents();
        }

        private void revision_Click(object sender, EventArgs e)
        {

        }
    }

 }
