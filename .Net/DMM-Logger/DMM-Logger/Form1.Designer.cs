﻿namespace DMM_Logger
{
    partial class dmmLoggerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStartStop = new System.Windows.Forms.Button();
            this.cbGPIBResources = new System.Windows.Forms.ComboBox();
            this.resultsBox = new System.Windows.Forms.RichTextBox();
            this.revision = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnStartStop
            // 
            this.btnStartStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnStartStop.Location = new System.Drawing.Point(19, 286);
            this.btnStartStop.Name = "btnStartStop";
            this.btnStartStop.Size = new System.Drawing.Size(75, 36);
            this.btnStartStop.TabIndex = 0;
            this.btnStartStop.Text = "start";
            this.btnStartStop.UseVisualStyleBackColor = true;
            this.btnStartStop.Click += new System.EventHandler(this.button1_Click);
            // 
            // cbGPIBResources
            // 
            this.cbGPIBResources.FormattingEnabled = true;
            this.cbGPIBResources.Location = new System.Drawing.Point(19, 64);
            this.cbGPIBResources.Name = "cbGPIBResources";
            this.cbGPIBResources.Size = new System.Drawing.Size(121, 21);
            this.cbGPIBResources.TabIndex = 1;
            // 
            // resultsBox
            // 
            this.resultsBox.Location = new System.Drawing.Point(254, 64);
            this.resultsBox.Name = "resultsBox";
            this.resultsBox.Size = new System.Drawing.Size(312, 194);
            this.resultsBox.TabIndex = 2;
            this.resultsBox.Text = "results box";
            // 
            // revision
            // 
            this.revision.AutoSize = true;
            this.revision.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            this.revision.Location = new System.Drawing.Point(478, 21);
            this.revision.Name = "revision";
            this.revision.Size = new System.Drawing.Size(148, 16);
            this.revision.TabIndex = 3;
            this.revision.Text = "DMM-Logger-REV 1.0.2";
            this.revision.Click += new System.EventHandler(this.revision_Click);
            // 
            // dmmLoggerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 344);
            this.Controls.Add(this.revision);
            this.Controls.Add(this.resultsBox);
            this.Controls.Add(this.cbGPIBResources);
            this.Controls.Add(this.btnStartStop);
            this.Name = "dmmLoggerForm";
            this.Text = "dmm-logger";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStartStop;
        private System.Windows.Forms.ComboBox cbGPIBResources;
        private System.Windows.Forms.RichTextBox resultsBox;
        private System.Windows.Forms.Label revision;
    }
}

