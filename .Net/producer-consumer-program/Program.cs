﻿using System;
using System.Collections.Concurrent;
using System.Threading;

class Program
{
    static readonly BlockingCollection<string> jobQueue = new BlockingCollection<string>(new ConcurrentQueue<string>(), 10);

    static void Main()
    {
        Console.WriteLine("Producer-Consumer Example\n");

        // Start consumer thread
        var consumer = new Thread(ConsumeJobs);
        consumer.Start();

        // Start producing jobs
        while (true)
        {
            Console.Write("Enter a job (e.g. job001): ");
            string job = Console.ReadLine();

            if (job.Equals("exit", StringComparison.OrdinalIgnoreCase))
            {
                jobQueue.CompleteAdding();
                break;
            }

            if (!IsValidJob(job))
            {
                Console.WriteLine("Invalid job number. Please enter a job in the format 'jobxxx' where xxx is a number between 001 and 999.");
                continue;
            }

            jobQueue.Add(job);
            Console.WriteLine("job '{0}' added to queue.", job);
        }

        Console.WriteLine("\nPress any key to exit.");
        Console.ReadKey();
    }

    static bool IsValidJob(string job)
    {
        if (!job.StartsWith("job"))
        {
            return false;
        }

        if (job.Length != 6)
        {
            return false;
        }

        if (!int.TryParse(job.Substring(3), out int jobNumber))
        {
            return false;
        }

        if (jobNumber < 1 || jobNumber > 999)
        {
            return false;
        }

        return true;
    }

    static void ConsumeJobs()
    {
        while (!jobQueue.IsCompleted)
        {
            try
            {
                string job = jobQueue.Take();
                Console.WriteLine("\nProcessing job '{0}'...", job);
                Console.WriteLine("\n");
                Thread.Sleep(1000); // Simulate job processing time
                ProcessJob(job);
            }
            catch (InvalidOperationException) { }
        }

        Console.WriteLine("\njob queue has been emptied.");
        Console.WriteLine("\n");
    }

    static void ProcessJob(string job)
    {
        switch (job)
        {
            case "job001":
                job001();
                break;
            case "job002":
                job002();
                break;
            // Add more job processing functions here
            default:
                Console.WriteLine("No processing function defined for job '{0}'.", job);
                break;
        }

        // Remove job from queue
        jobQueue.TryTake(out _);
    }


    static void job001()
    {
        Console.WriteLine("job001 processing...");
        Console.WriteLine("\n");
        // execute this work and dequeue
        //jobQueue.Dispose(job);
    }

    static void job002()
    {
        Console.WriteLine("job002 processing...");
        Console.WriteLine("\n");
    }

    // Add more job processing functions here
}
