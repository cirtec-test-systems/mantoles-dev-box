using System;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter three values separated by spaces:");
            string input = Console.ReadLine();
            string[] values = input.Split(' ');

            double num1 = Convert.ToDouble(values[0]);
            double num2 = Convert.ToDouble(values[1]);
            double num3 = Convert.ToDouble(values[2]);

            Console.WriteLine("Enter an operator (+, -, *, /):");
            string op = Console.ReadLine();

            double result = 0;
            switch (op)
            {
                case "+":
                    result = num1 + num2 + num3;
                    break;
                case "-":
                    result = num1 - num2 - num3;
                    break;
                case "*":
                    result = num1 * num2 * num3;
                    break;
                case "/":
                    result = num1 / num2 / num3;
                    break;
                default:
                    Console.WriteLine("Invalid operator entered.");
                    break;
            }

            Console.WriteLine("Result: " + result);
            Console.ReadLine();
        }
    }
}
