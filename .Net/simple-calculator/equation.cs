using System;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Enter three numbers:");
        double num1 = double.Parse(Console.ReadLine());
        double num2 = double.Parse(Console.ReadLine());
        double num3 = double.Parse(Console.ReadLine());

        Console.WriteLine("Choose an operator (+, -, *, /):");
        string op = Console.ReadLine();

        double result = 0;

        switch (op)
        {
            case "+":
                result = num1 + num2 + num3;
                break;
            case "-":
                result = num1 - num2 - num3;
                break;
            case "*":
                result = num1 * num2 * num3;
                break;
            case "/":
                result = num1 / num2 / num3;
                break;
            default:
                Console.WriteLine("Invalid operator!");
                break;
        }

        Console.WriteLine("The result is: " + result);
    }
}
