﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace serial_number_generator_demo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

       

        private void btnStart_Click(object sender, EventArgs e)
        {
            
                string filename = "serial-backup.txt";
                int serialNumber;

                if (File.Exists(filename))
                {
                    serialNumber = int.Parse(File.ReadAllText(filename));
                }
                else
                {
                    serialNumber = 1;
                }

                File.WriteAllText(filename, serialNumber + 1.ToString());

                txtSerialNumber.Text = serialNumber.ToString();

        }
    }
}
