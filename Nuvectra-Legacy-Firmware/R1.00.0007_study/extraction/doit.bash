#!  /usr/bin/bash

set -o xtrace

 src_file=Algostim_Product_Firmware_R1.00.0007_UnifiedImage.s28

# # SDB is in INFO_C. Extract the SDB.
# srec_cat -Disable_Sequence_Warnings \
#     $src_file \
#     -crop 0x1040 0x1080 \
#     -Output sdb.dump -HEX_Dump
#
# gvim sdb.dump
#
# Result: (:r sdb.dump)
# Using Git.Nuvectra/xPG_Bootloader/software_description_block.
#
# 00001040:00010000 uint32_t HighMemStartAddress
#          0001B8EC uint32_t HighMemEndAddress
#              9000 uint16_t LowMemStartAddress
#              FE00 uint16_t LowMemEndAddress
#              FFDC uint16_t VectorStartAddress
#              FFFE uint16_t VectorEndAddress
# 00001050:    914C uint16_t ApplicationEntryPoint
#              F20E uint16_t ApplicationCRC
#                FE uint8_t  BootFlag
#                0A uint8_t  MICSListeningWindowDuration
#              C778 uint16_t SoftwareDescriptionBlockCRC

srec_cat -Disable_Sequence_Warnings \
    $src_file \
    -crop \
        0x09000 0x0FE00 \
        0x10000 0x1B8EC \
    -Execution_Start_Address 0x0914C \
    -Output app.s28
