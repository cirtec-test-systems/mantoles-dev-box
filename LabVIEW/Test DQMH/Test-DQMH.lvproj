﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="25008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Documentation" Type="Folder">
			<Item Name="lib" Type="Folder">
				<Item Name="Add New DQMH Module wizard.jpg" Type="Document" URL="../Documentation/lib/Add New DQMH Module wizard.jpg"/>
				<Item Name="Calling Start Module VI from TestStand.png" Type="Document" URL="../Documentation/lib/Calling Start Module VI from TestStand.png"/>
				<Item Name="Create New DQMH Event wizard.jpg" Type="Document" URL="../Documentation/lib/Create New DQMH Event wizard.jpg"/>
				<Item Name="Create Project Wizard.jpg" Type="Document" URL="../Documentation/lib/Create Project Wizard.jpg"/>
				<Item Name="DQMH Module Main VI Overview.jpg" Type="Document" URL="../Documentation/lib/DQMH Module Main VI Overview.jpg"/>
				<Item Name="DQMH Project Structure Overview.jpg" Type="Document" URL="../Documentation/lib/DQMH Project Structure Overview.jpg"/>
				<Item Name="DQMH Project Template from Create Project.jpg" Type="Document" URL="../Documentation/lib/DQMH Project Template from Create Project.jpg"/>
				<Item Name="Edit the icon for the DQMH renamed module.png" Type="Document" URL="../Documentation/lib/Edit the icon for the DQMH renamed module.png"/>
				<Item Name="EHL inside a DQMH Cloneable Module.jpg" Type="Document" URL="../Documentation/lib/EHL inside a DQMH Cloneable Module.jpg"/>
				<Item Name="Multiple Instances Folder.png" Type="Document" URL="../Documentation/lib/Multiple Instances Folder.png"/>
				<Item Name="My Singleton DQMH Module is locked.jpg" Type="Document" URL="../Documentation/lib/My Singleton DQMH Module is locked.jpg"/>
				<Item Name="Remove DQMH Event wizard.jpg" Type="Document" URL="../Documentation/lib/Remove DQMH Event wizard.jpg"/>
				<Item Name="Rename DQMH Event wizard.jpg" Type="Document" URL="../Documentation/lib/Rename DQMH Event wizard.jpg"/>
				<Item Name="Rename DQMH Module wizard.png" Type="Document" URL="../Documentation/lib/Rename DQMH Module wizard.png"/>
				<Item Name="Start Module returns Wait for Event Sync.png" Type="Document" URL="../Documentation/lib/Start Module returns Wait for Event Sync.png"/>
				<Item Name="Start Module starts a new Device Under Test.png" Type="Document" URL="../Documentation/lib/Start Module starts a new Device Under Test.png"/>
				<Item Name="Start Module VI and Synchronize Module Events.png" Type="Document" URL="../Documentation/lib/Start Module VI and Synchronize Module Events.png"/>
				<Item Name="Synchronize Module Events.png" Type="Document" URL="../Documentation/lib/Synchronize Module Events.png"/>
				<Item Name="Test Module API VI Overview.jpg" Type="Document" URL="../Documentation/lib/Test Module API VI Overview.jpg"/>
				<Item Name="TestStand Thermal Chamber Control example.png" Type="Document" URL="../Documentation/lib/TestStand Thermal Chamber Control example.png"/>
				<Item Name="Thermal Chamber Controller VI.jpg" Type="Document" URL="../Documentation/lib/Thermal Chamber Controller VI.jpg"/>
				<Item Name="Thermal Chamber Controller with DUT.jpg" Type="Document" URL="../Documentation/lib/Thermal Chamber Controller with DUT.jpg"/>
				<Item Name="Thermal Chamber Controller with Multiple DUTs.jpg" Type="Document" URL="../Documentation/lib/Thermal Chamber Controller with Multiple DUTs.jpg"/>
				<Item Name="Convert DQMH Event Wizard.png" Type="Document" URL="../Documentation/lib/Convert DQMH Event Wizard.png"/>
				<Item Name="Create Project Sample Projects Dialog.png" Type="Document" URL="../Documentation/lib/Create Project Sample Projects Dialog.png"/>
				<Item Name="Tools Delacor DQMH Menu.png" Type="Document" URL="../Documentation/lib/Tools Delacor DQMH Menu.png"/>
				<Item Name="Tools Delacor DQMH Real-Time Tools Menu.png" Type="Document" URL="../Documentation/lib/Tools Delacor DQMH Real-Time Tools Menu.png"/>
				<Item Name="Reply Payload Arguments Window.jpg" Type="Document" URL="../Documentation/lib/Reply Payload Arguments Window.jpg"/>
				<Item Name="DQMH Palette.png" Type="Document" URL="../Documentation/lib/DQMH Palette.png"/>
				<Item Name="DQMH-R-square.png" Type="Document" URL="../Documentation/lib/DQMH-R-square.png"/>
				<Item Name="Error Debug Property Node.png" Type="Document" URL="../Documentation/lib/Error Debug Property Node.png"/>
				<Item Name="Error Handler for Helper Loop.png" Type="Document" URL="../Documentation/lib/Error Handler for Helper Loop.png"/>
				<Item Name="Find DQMH Broadcast Event Frames Window.png" Type="Document" URL="../Documentation/lib/Find DQMH Broadcast Event Frames Window.png"/>
				<Item Name="Find DQMH Broadcast Event Frames.png" Type="Document" URL="../Documentation/lib/Find DQMH Broadcast Event Frames.png"/>
				<Item Name="Find the DQMH Broadcast Event Frame.png" Type="Document" URL="../Documentation/lib/Find the DQMH Broadcast Event Frame.png"/>
			</Item>
			<Item Name="DQMHDocumentation.html" Type="Document" URL="../Documentation/DQMHDocumentation.html"/>
		</Item>
		<Item Name="Testers" Type="Folder">
			<Item Name="Test My Singleton DQMH API.vi" Type="VI" URL="../Libraries/My Singleton DQMH/Test My Singleton DQMH API.vi"/>
			<Item Name="Test My CloneableDQMH API.vi" Type="VI" URL="../Libraries/My CloneableDQMH/Test My CloneableDQMH API.vi"/>
			<Item Name="Test Module 1 API.vi" Type="VI" URL="../Libraries/Module 1/Test Module 1 API.vi"/>
		</Item>
		<Item Name="Modules" Type="Folder">
			<Item Name="My Singleton DQMH.lvlib" Type="Library" URL="../Libraries/My Singleton DQMH/My Singleton DQMH.lvlib"/>
			<Item Name="My CloneableDQMH.lvlib" Type="Library" URL="../Libraries/My CloneableDQMH/My CloneableDQMH.lvlib"/>
			<Item Name="Module 1.lvlib" Type="Library" URL="../Libraries/Module 1/Module 1.lvlib"/>
		</Item>
		<Item Name="Application.lvlib" Type="Library" URL="../Libraries/Application/Application.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies"/>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Application" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{297F3F0F-3AFF-4BA9-B1B2-2438714F77E8}</Property>
				<Property Name="App_INI_GUID" Type="Str">{2DAC7B8A-DE9A-43E7-9C19-6238DAF455BE}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_serverType" Type="Int">1</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{F4AC0F2E-9817-491A-A1E8-2799068AD2F5}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Application</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/Application</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{A3D48F6E-64BC-45A6-ADEB-64083211F11E}</Property>
				<Property Name="Bld_version.build" Type="Int">2</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Application.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/Application/Application.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/Application/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{146DC3F2-CCE8-42E3-BD7C-88299F1DDE3E}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Application.lvlib/Main.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Application</Property>
				<Property Name="TgtF_internalName" Type="Str">Application</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2019</Property>
				<Property Name="TgtF_productName" Type="Str">Application</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{6113D017-51FE-4C02-9632-17FD25301CC4}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Application.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
