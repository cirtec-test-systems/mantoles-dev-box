/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: This is the header file that contains the common
 *	(or shared) state machine data structure definitions.
 *
 ***************************************************************************/

#ifndef COMMANDPROCESSOR_H_
#define COMMANDPROCESSOR_H_

#include <stdbool.h>
#include "system_events.h"
#include "system_events.h"
#include "error_code.h"

/// \defgroup commandProcessor	Command Processor
/// \ingroup deviceManagementLayer
/// @{

//Commands
#include "Common/Protocol/cmndTokens.h"
#include "Common/Protocol/cmndGetXpgStatus.h"
#include "Common/Protocol/cmndChrg.h"
#include "Common/Protocol/cmndChrgCntrl.h"
#include "Common/Protocol/cmndConfigDeviceParams.h"
#include "Common/Protocol/cmndStim.h"
#include "Common/Protocol/cmndClearLog.h"
#include "Common/Protocol/cmndReadLog.h"
#include "Common/Protocol/cmndAppendLog.h"
#include "Common/Protocol/cmndGetPpcConst.h"
#include "Common/Protocol/cmndGetPrgmNames.h"
#include "Common/Protocol/cmndGetPrgmConst.h"
#include "Common/Protocol/cmndChrgCntrl.h"
#include "Common/Protocol/cmndGetXpgIdent.h"
#include "Common/Protocol/cmndSetXpgIdent.h"
#include "Common/Protocol/cmndGetGeneralCals.h"
#include "Common/Protocol/cmndSetGeneralCals.h"
#include "Common/Protocol/cmndGetCounter.h"
#include "Common/Protocol/cmndSetCounter.h"
#include "Common/Protocol/cmndEcho.h"
#include "Common/Protocol/cmndGetSetChannelCals.h"
#include "Common/Protocol/cmndStimAsicHVCals.h"
#include "Common/Protocol/cmndGetTrimList.h"
#include "Common/Protocol/cmndSetTrimList.h"
#include "Common/Protocol/cmndGetTestStim.h"
#include "Common/Protocol/cmndSetTestStim.h"
#include "Common/Protocol/cmndDiagData.h"
#include "Common/Protocol/cmndSlctPrgm.h"
#include "Common/Protocol/cmndGetPrgmDef.h"
#include "Common/Protocol/cmndSetPrgmDef.h"
#include "Common/Protocol/cmndSetPrgmConst.h"
#include "Common/Protocol/cmndSetPulseConst.h"
#include "Common/Protocol/cmndGetPulseConst.h"
#include "Common/Protocol/cmndConfigDeviceParams.h"
#include "Common/Protocol/cmndIncPrgmAmpl.h"
#include "Common/Protocol/cmndDecPrgmAmpl.h"
#include "Common/Protocol/cmndDecPulseAmpl.h"
#include "Common/Protocol/cmndIncPulseAmpl.h"
#include "Common/Protocol/cmndSetPulseAmpl.h"
#include "Common/Protocol/cmndGetPulseWidths.h"
#include "Common/Protocol/cmndSetPulseWidth.h"
#include "Common/Protocol/cmndDecPulseWidth.h"
#include "Common/Protocol/cmndIncPulseWidth.h"
#include "Common/Protocol/cmndGetPrgmFreq.h"
#include "Common/Protocol/cmndSetPrgmFreq.h"
#include "Common/Protocol/cmndIncPrgmFreq.h"
#include "Common/Protocol/cmndDecPrgmFreq.h"
#include "Common/Protocol/cmndRstrPrgmDflt.h"
#include "Common/Protocol/cmndGetPopConst.h"
#include "Common/Protocol/cmndLeadLimits.h"
#include "Common/Protocol/cmndGetCPData.h"
#include "Common/Protocol/cmndSetCPData.h"
#include "Common/Protocol/cmndGetRampTime.h"
#include "Common/Protocol/cmndSetRampTime.h"
#include "Common/Protocol/cmndGetVBAT.h"
#include "Common/Protocol/cmndGetTitrStim.h"
#include "Common/Protocol/cmndSetTitrStim.h"
#include "Common/Protocol/cmndSetTestWaveform.h"
#include "Common/Protocol/cmndImpedance.h"
#include "Common/Protocol/cmndReadMemory.h"
#include "Common/Protocol/cmndWriteMemory.h"
#include "Common/Protocol/cmndBackgroundImpedance.h"
#include "Common/Protocol/cmndCalibrateChannel.h"
#include "Common/Protocol/cmndGetLogRange.h"
#include "Common/Protocol/cmndInjectEvent.h"
#include "dataStoreXpgIdentity.h"

//Responses
#include "Common/Protocol/response_code.h"

//For the command table
typedef void (*cmndHandler)(void);

typedef struct CMND_TABLE_ENTRY
{
	CMND_TOKEN cmdToken;
	uint16_t dataSize;
	cmndHandler handler;
} CMND_TABLE_ENTRY;

// Public functions for System Health Monitor
void commandProcessorProcessEvent(EVENT eventToProcess);

void sendCommandResponse(RESPONSE responseCode, CMND_TOKEN token, uint16_t size);
void sendCommandResponseBasic(RESPONSE responseCode);

void setArmForPOPPairing(bool armDecision);
bool getArmForPOPPairing(void);

//House the data received over MICS
typedef union MICS_RX_DATA
{
	CONFIG_DEVICE_PARAMETERS configDeviceParams;
	STIM_PARAMS stimParams;
	CLEAR_LOG_PARAMS clearLogParams;
	READ_LOG readLogData;
	APPEND_LOG appendLogData;
	XPG_IDENTITY xpgIdentity;
	SET_GENERAL_CALS_PARAMS genCals;
	GET_COUNTER_PARAMS counterParams;
	SET_COUNTER_PARAMS setCounterData;
	ECHO_SHORT_PARAMS shortParams;
	ECHO_LONG_PARAMS longParams;
	CHANNEL_CALS channelCalParams;
	HV_CAL_PARAMS hvCalParams;
	GET_TRIM_LIST_PARAMS trimListParams;
	SET_TRIM_LIST_PARAMS setTrimListParams;
	SET_TEST_STIM_PARAMS setTestStimParams;
	DIAG_DATA_PARAMS diagDataParams;
	SLCT_PRGM_PARAMS selectedProgramParams;
	GET_PRGM_DEF_PARAMS prgmDefinitionParams;
	SET_PRGM_DEF_PARAMS setPrgmDefinitionParams;
	SET_PRGM_CONST_PARAMS setPgrmConstParams;
	SET_PULSE_CONST_PARAMS setPulseConstParams;
	SET_PULSE_AMPL_PARAMS setPulseAmplParams;
	INC_PULSE_AMPL_PARAMS incrPulseAmplParams;
	DEC_PULSE_AMPL_PARAMS decrPulseAmplParams;
	SET_PULSE_WIDTH_PARAMS setPulseWidthParams;
	DEC_PULSE_WIDTH_PARAMS decrPulseWidthParams;
	INC_PULSE_WIDTH_PARAMS incrPulseWidthParams;
	SET_PRGM_FREQ_PARAMS programFreqParams;
	RSTR_PRGM_DFLT_PARAMS restorePrgmDefaultParams;
	LEAD_LIMIT_PARAMS leadLimitParams;
	GET_CP_DATA_PARAMS cpDataParams;
	SET_CP_DATA_PARAMS setCpDataParams;
	SET_RAMP_TIME_PARAMS rampTimeParams;
	SET_TITR_STIM_PARAMS titrStimParams;
	IMPEDANCE_PARAMS impedanceParams;
	READ_MEMORY_PARAMS readMemoryParams;
	WRITE_MEMORY_PARAMS writeMemoryParams;
	BACKGROUND_IMPEDANCE_PARAMS bgImpedanceParams;
	INJECT_EVENT_PARAMS injectEventParams;
	CAL_CHAN_PARAMS calChanParams;
	GET_LOG_RANGE_PARAMS getLogRangeParams;
	GET_CHANNEL_CALS_PARAMS getChannelParams;
	MICS_DIAGNOSTICS_PARAMS micsDiagnosticsParams;
} MICS_RX_DATA;

//House the data received over MICS
typedef union MICS_TX_DATA
{
	READ_LOG_RESPONSE_DATA readLogResponseData;
	APPEND_LOG_RESPONSE_DATA appendLogResponseData;
	GET_PPC_CONST_RESPONSE_DATA ppcConstResponseData;
	GET_PRGM_NAMES_RESPONSE_DATA pgrmNamesResponseData;
	GET_XPG_STATUS_RESPONSE_DATA xpgStatus;
	GET_PRGM_CONST_RESPONSE_DATA prgmConstResponseData;
	CHARGING_CONTROL_RESPONSE_DATA chargingControlResponseData;
	GET_XPG_IDENT_RESPONSE_DATA xpgIdentResponseData;
	GET_GENERAL_CALS_RESPONSE_DATA genCalsResponseData;
	GET_COUNTER_RESPONSE_DATA counterResponseData;
	ECHO_SHORT_PARAMS shortParams;
	ECHO_LONG_PARAMS longParams;
	CHANNEL_CALS channelCalParams;
	HV_CAL_PARAMS hvCalParams;
	GET_TRIM_LIST_RESPONSE_DATA trimListParams;
	GET_TEST_STIM_RESPONSE_DATA testStimData;
	DIAG_DATA_RESPONSE_DATA diagDataResponse;
	GET_PRGM_DEF_RESPONSE_DATA programDefinitionResponseData;
	GET_PULSE_CONST_RESPONSE_DATA pulseConstResponseData;
	CONFIG_DEVICE_PARAMETERS configParamResponseData;
	INC_PRGM_AMPL_RESPONSE_DATA incrPrgmAmplResponseData;
	DEC_PRGM_AMPL_RESPONSE_DATA decrPrgmAmplResponseData;
	SET_PULSE_AMPL_RESPONSE_DATA setPulseAmplResponseData;
	DEC_PULSE_AMPL_RESPONSE_DATA decrPulseAmplResponseData;
	INC_PULSE_AMPL_RESPONSE_DATA incrPulseAmplResponseData;
	GET_PULSE_WIDTHS_RESPONSE_DATA pulseWidthsResponseData;
	DEC_PULSE_WIDTH_RESPONSE_DATA decrPulseWidthResponseData;
	INC_PULSE_WIDTH_RESPONSE_DATA incrPulseWidthResponseData;
	GET_PRGM_FREQ_RESPONSE_DATA programFreqResponseData;
	INC_PRGM_FREQ_RESPONSE_DATA incrPrgmFreqResponseData;
	DEC_PRGM_FREQ_RESPONSE_DATA decrPrgmFreqResponseData;
	GET_POP_CONST_RESPONSE_DATA popConstResponseData;
	LEAD_LIMIT_PARAMS leadLimitResponseData;
	GET_CP_DATA_RESPONSE_DATA cpDataResponseData;
	GET_RAMP_TIME_RESPONSE_DATA rampTimeResponseData;
	GET_VBAT_RESPONSE_DATA vbatResponseData;
	GET_TITR_STIM_RESPONSE_DATA titrStimResponseData;
	IMPEDANCE_RESPONSE_DATA impedanceResponseData;
	READ_MEMORY_RESPONSE_DATA readMemoryResponseData;
	BACKGROUND_IMPEDANCE_PARAMS bgImpedanceResponseData;
	CAL_CHAN_RESPONSE_DATA calChanResponseData;
	GET_LOG_RANGE_RESPONSE_DATA getLogRangeResponseData;
} MICS_TX_DATA;


typedef struct MICS_RX_BUFFER
{
	CMND_TOKEN token;
	MICS_RX_DATA rxData;
} MICS_RX_BUFFER;

//House the data to be sent over MICS
typedef struct MICS_TX_BUFFER
{
	RESPONSE responseCode;
	MICS_TX_DATA txData;
} MICS_TX_BUFFER;

typedef struct INITIAL_MICS_RX_PACKET
{
	uint8_t commandTokenBlock;
	uint8_t externalId1stByte;
	uint8_t externalId2ndByte;
	uint8_t externalId3rdByte;
	uint16_t cmndToken;
	uint8_t dataBlock[8];
	uint8_t checkSum;
} INITIAL_MICS_RX_PACKET;

typedef struct MIDDLE_MICS_RX_PACKET
{
	uint8_t commandTokenBlock;
	uint8_t dataBlock[14];
} MIDDLE_MICS_RX_PACKET;

typedef struct FINAL_MICS_RX_PACKET
{
	uint8_t commandTokenBlock;
	uint8_t dataBlock[13];
	uint8_t checksum;
} FINAL_MICS_RX_PACKET;

/// @}

#endif /* COMMANDPROCESSOR_H_ */
