/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: Contains include information for MICS Packet
 *	Assembler / Disassembler (MicsPAD).
 *
 ***************************************************************************/

#ifndef MICSPAD_H_
#define MICSPAD_H_

/// \defgroup micspad	MicsPAD
/// \ingroup commandProcessor
///
/// MICS Packet Assembler and Disassembler
///
/// @{

#include "system_events.h"

//Define some constants
#define COMMAND_TOKEN_BLOCK 	0x01
#define INTERMEDIATE_BLOCK  	0x00
#define CTB_BLOCK_DATA_SIZE		8
#define INT_BLOCK_DATA_SIZE 	14

#define RESPONSE_CODE_BLOCK 	0x01
#define RC_INTERMEDIATE_BLOCK 	0x00
#define RCB_BLOCK_DATA_SIZE 	10
#define RCB_BLOCK_DATA_OFFSET	4
#define RC_INT_BLOCK_DATA_SIZE 	14


// Public functions for System Health Monitor
void micsPADProcessEvent(EVENT eventToProcess);
void sendQuickMicsResponse(RESPONSE response, CMND_TOKEN token);

/// @}

#endif /* MICSPAD_H_ */
