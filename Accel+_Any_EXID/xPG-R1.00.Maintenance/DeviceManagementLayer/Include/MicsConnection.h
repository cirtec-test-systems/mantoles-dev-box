
/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: MICS connection management state machine.
 *
 ***************************************************************************/

#ifndef MICS_CONNECTION_H_
#define MICS_CONNECTION_H_

#include "system_events.h"
#include "MicsConnectionCommand.h"


void micsConnectionProcessEvent(EVENT eventToProcess);

#endif
