/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: Header file for the software timer module.
 *
 ***************************************************************************/
#ifndef SWTIMER_H
#define SWTIMER_H

#include <stdint.h>
#include <stdbool.h>

/// \defgroup swTimer Multi Channel Timer
/// \ingroup deviceManagementLayer
/// @{

#include "swtmr_application_timers.h"
#include "system_events.h"
#include "ticks.h"

#define SWTMR_FLAGS_NOT_IN_USE 	0x00
#define SWTMR_FLAGS_ACTIVE 	    0x01


//timeout durations using the software timer
#define SWTMR_500_MS (MS(500))
#define SWTMR_01_SECOND_TIMEOUT TICKS_HZ
#define SWTMR_02_SECOND_TIMEOUT (2UL*TICKS_HZ)
#define SWTMR_03_SECOND_TIMEOUT (3UL*TICKS_HZ)
#define SWTMR_04_SECOND_TIMEOUT (4UL*TICKS_HZ)
#define SWTMR_05_SECOND_TIMEOUT (5UL*TICKS_HZ)
#define SWTMR_10_SECOND_TIMEOUT (10UL*TICKS_HZ)
#define SWTMR_30_SECOND_TIMEOUT (30UL*TICKS_HZ)
#define SWTMR_60_SECOND_TIMEOUT (60UL*TICKS_HZ)

#define SWTMR_01_MINUTE_TIMEOUT (SWTMR_60_SECOND_TIMEOUT)
#define SWTMR_05_MINUTE_TIMEOUT (5UL*SWTMR_60_SECOND_TIMEOUT)
#define SWTMR_10_MINUTE_TIMEOUT (10UL*SWTMR_60_SECOND_TIMEOUT)
#define SWTMR_15_MINUTE_TIMEOUT (15UL*SWTMR_60_SECOND_TIMEOUT)
#define SWTMR_01_HOUR_TIMEOUT (60UL*SWTMR_60_SECOND_TIMEOUT)
#define SWTMR_04_HOUR_TIMEOUT (4UL*SWTMR_01_HOUR_TIMEOUT)
#define SWTMR_08_HOUR_TIMEOUT (8UL*SWTMR_01_HOUR_TIMEOUT)
#define SWTMR_24_HOUR_TIMEOUT (24UL*SWTMR_01_HOUR_TIMEOUT)


/*
 * Public function declarations.
 */
void swTimerProcessEvent(EVENT event);
void swTimerInit(void);
void swTimerSet(SWTMR_ENUM timerQueueElement, uint32_t timeout, EVENT event, bool repeat);
uint32_t swTimerGetRemainingTime(SWTMR_ENUM timerQueueElement);
void swTimerCancel(SWTMR_ENUM timerQueueElement);
void swTimerOverflow(void);
void swTimerInterrupt();
void swTimerUpdateVloCalibration();
uint32_t swTimerGetCurrentTime(void);

#ifndef ACLK_USES_LFXT1CLK
unsigned int VLOCLK_measurement(void);
#endif


/// @}

#endif /* SWTIMER_H */
