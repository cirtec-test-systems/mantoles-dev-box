/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *	
 *	@file Description: Generic program declarations 
 *	
 ***************************************************************************/

#ifndef GPRGM_H_
#define GPRGM_H_

#include <stdint.h>
#include <stdbool.h>
#include "stim.h"

#include "Common/Protocol/cmndCalibrateChannel.h"

#define MAX_PERIOD_ERROR_US	1000	// allow the period to be off by 1 msec

#define RAMP_MAX		0xFF	///< The maximum ramp value, meaning "full output"

#define COMPLIANCE_MAX	0x3F	///< The maximum boost converter DAC value

enum WAVEFORM {
	WAVEFORM_RECT = 0,	//!< Select a standard rectangular waveform
	WAVEFORM_RAM0 = 1,	//!< Select waveform generation from RAM0
	WAVEFORM_RAM1 = 2 	//!< Select waveform generation from RAM1
};


void 	gprgmInit(void);
void 	gprgmClear(void);
int 	gprgmRun(bool rampDisable);
int 	gprgmRunTitration();
int 	gprgmUpdateAmplitude(uint8_t level);

int32_t gprgmCurrentBalanceCheck(int32_t *currentBalance);
int 	gprgmCalibrateChannel(CAL_CHAN_PARAMS const *p, CAL_CHAN_RESPONSE_DATA *r);
int 	gprgmCheckDensities(void);
int 	gprgmImpedanceAndOutputCapCheck(bool *result);
int 	gprgmEpgImpedanceAndOutputCapCheck(bool *result);
bool 	gprgmPulseGuardCheck(uint8_t src, uint8_t sink);

int 	gprgmAutoWaveformAdjustment(void);
int 	gprgmAutoUncontrolledSourceSink(void);
int 	gprgmAutoComplianceVoltage(void);
uint8_t gprgmComplianceVoltageIndex(uint32_t amplitude_total, int8_t *percent, uint32_t pulseWidth, uint8_t krec);
CHANNEL_MAX_AMP_INDEX gprgm_FindReferenceCurrentIndex();

int 	gprgmMeasureImpedance(uint16_t amplitude, uint8_t ch1, uint8_t ch2, bool oneShot, bool checkCap, uint32_t *impedancePtr, uint32_t *capVoltage);

/***
 *** Getters
 ***/

uint8_t 	gprgmGetFreqIdx(void);
uint8_t 	gprgmGetComplianceDACValue(void);
uint8_t 	gprgmGetNumPulses(void);
uint8_t 	gprgmGetRampLevel(void);
uint16_t 	gprgmGetAmplitude(int gpulse);
uint16_t 	gprgmGetPulseWidth(int gpulse);
int8_t const *gprgmGetAmpPercent(int gpulse);
uint8_t 	gprgmGetRecRatio(int gpulse);
uint16_t 	gprgmGetInterphaseDelay(int gpulse);
uint16_t 	gprgmGetPassiveWidth(int gpulse);
uint16_t 	gprgmGetCBCWidth(int gpulse);
bool 		gprgmGetStimUSource(int gpulse);
bool 		gprgmGetRecUSource(int gpulse);
bool 		gprgmGetCBCUSource(int gpulse);
bool const *gprgmGetUSSChannels(int gpulse);
enum WAVEFORM gprgmGetStimWaveform(int gpulse);
enum WAVEFORM gprgmGetRecWaveform(int gpulse);

/***
 *** Setters
 ***/

void gprgmSetFreqIdx(uint8_t freqIdx);
void gprgmSetComplianceDACValue(uint8_t n);
void gprgmSetNumPulses(uint8_t n);
void gprgmSetRampLevel(uint8_t rampLevel);
void gprgmSetAmplitude(int gpulse, uint16_t amp);
void gprgmSetPulseWidth(int gpulse, uint16_t pw);
void gprgmSetAmpPercent(int gpulse, int8_t const *ampPercent);
void gprgmSetRecRatio(int gpulse, uint8_t krec);
void gprgmSetInterphaseDelay(int gpulse, uint16_t ipd);
void gprgmSetPassiveWidth(int gpulse, uint16_t us);
void gprgmSetCBCWidth(int gpulse, uint16_t us);
void gprgmSetStimUSource(int gpulse, bool isSource);
void gprgmSetRecUSource(int gpulse, bool isSource);
void gprgmSetCBCUSource(int gpulse, bool isSource);
void gprgmSetUSSChannels(int gpulse, bool const *ussChannels);
void gprgmSetStimWaveform(int gpulse, enum WAVEFORM wf);
void gprgmSetRecWaveform(int gpulse, enum WAVEFORM wf);
void gprgmSetWaveformReferenceCurrent(CHANNEL_MAX_AMP_INDEX ref);

#endif
