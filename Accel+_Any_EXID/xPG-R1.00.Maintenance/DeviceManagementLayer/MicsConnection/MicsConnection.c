/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: MICS connection management state machine.
 *
 ***************************************************************************/

#include <string.h>
#include "state_machine_common.h"
#include "EventQueue.h"
#include "MicsConnection.h"
#include "mics.h"
#include "ChargeManager.h"
#include "MultiChannelTimer.h"
#include "xpginfo.h"
#include "delay.h"
#include "log.h"
#include "trim.h"
#include "system.h"
#include "common/Protocol/cmndMicsDiagnostics.h"

#define MICSCONN_400MHZ_SLEEP_TIME MS(500)
#define MICSCONN_400MHZ_LISTEN_TIME MS(40)
#define MICSCONN_400MHZ_VERIFY_TIME (2ul*TICKS_HZ)
#define MICSCONN_RESET_WAIT_TIME (5ul*TICKS_HZ)
#define MICSCONN_CHIP_OFF_TIME TICKS_HZ
#define MICSCONN_MAX_BLOCKS_TO_POST 31

//Define the states of the state machine
typedef enum MICSCONN_STATE
{
	MICSCONN_STATE_INITIAL,						//!< Before initialization
	MICSCONN_STATE_SLEEPING, 					//!< The chip is idle
	MICSCONN_STATE_IN_SESSION, 					//!< A session is in progress.
	MICSCONN_DROP_SESSION_DELAY,				//!< If the read buffer overflows, this state machine responds by waiting 5 sec before restarting
												//! 400MHz wakeup to prevent the dropped session from resuming.
	MICSCONN_STATE_CHIP_OFF,					//! The chip has no power. This is a wait state when resetting the MICS chip.

	//400MHz wakeup states
	MICSCONN_STATE_400MHZ_SLEEPING, 			//!< We are in a 400MHz wakeup mode, but the chip is sleeping (between tries).
	MICSCONN_STATE_400MHZ_WAKING_CHIP,			//!< Initializing the 400MHz wakeup attempt. Waiting for the radio to wake up.
	MICSCONN_STATE_400MHZ_LISTENING,			//!< Listening on a channel for a 400MHz wakeup signal.
	MICSCONN_STATE_400MHZ_WAITING_TO_ESTABLISH_LINK,	//!< Found a channel that is trying to talk to us. Resetting the chip to configure for establishing the connection.
	MICSCONN_STATE_400MHZ_ESTABLISHING_LINK,	//!< Found a channel that is trying to talk to us. Configured chip to establish link. Waiting for link_ready.

	//States that control diagnostics mode.
	MICSCONN_STATE_DIAG_DELAY,					//!< Waiting to perform a diagnostic function.
	MICSCONN_STATE_DIAG_STARTUP,				//!<
	MICSCONN_STATE_DIAG_RSSI,					//!< Performing RSSI measurements.
	MICSCONN_STATE_DIAG_TRANS_CW,				//!< Transmitting a CW signal.
	MICSCONN_STATE_DIAG_TRANS_MODULATED,		//!< Transmitting a modulated signal.
	MICSCONN_STATE_DIAG_400_RECEIVE,				//!< In a receiving mode, but not responding to connections.
	MICSCONN_STATE_DIAG_24_RECEIVE				//!< In a wakeup receiving mode, but not responding to wakeup packets.

} MICSCONN_STATE;


/*Event Handers*/
static void eventHandlerE_INITIALIZE(EVENT event);
static void eventHandler245Wakeup(EVENT event);
static void eventHandlerSessionLost(EVENT event);
static void eventHandlerReceiveBlock(EVENT event);
static void eventHandlerRxFailed(EVENT event);
static void eventHandlerRxFailedResetTimeout(EVENT event);

//400MHz wakeup event handlers
static void eventHandlerChargingEngaged(EVENT event);
static void eventHandler400mhzSleepingEnter(EVENT event);
static void eventHandler400mhzWakeChip(EVENT event);
static void eventHandler400mhzStartListening(EVENT event);
static void eventHandler400mhzCheckTid(EVENT event);
static void eventHandler400mhzNextChannel(EVENT event);
static void eventHandler400mhzStartConnection(EVENT event);
static void eventHandler400mhzConnected(EVENT event);
static void eventHandler400mhzConnectionTimeout(EVENT event);

//Reset event handlers
static void eventHandlerFullReset(EVENT event);
static void eventHandlerChipResetTimeout(EVENT event);
static void fullMicsChipReset();

//diagnostics event handlers
static void eventHandlerStartMicsDiagnosticsDelay(EVENT event);
static void eventHandlerStartMicsDiagnostics(EVENT event);
static void eventHandlerDiagChipIsReady(EVENT event);
static void eventHandlerDiagRssiSample(EVENT event);
static void eventHandlerDiagResetWatchdog(EVENT event);
static void eventHandlerDiagTransmitCwEnter(EVENT event);
static void eventHandlerDiagTransmitModulatedEnter(EVENT event);
static void eventHandlerDiag400ReceiveEnter(EVENT event);
static void eventHandlerDiag24ReceiveEnter(EVENT event);
static void eventHandlerDiagCleanup(EVENT event);
static void eventHandlerPollErrors(EVENT event);

//helpers
static void transitionToCorrectSleepState();
static void setTimer(MICSCONN_TIMEOUT_PHASE phase, uint32_t time);
static void newSessionOpened();

// Predefine the state machine table size
#define MICSCONN_EVENT_TABLE_LEN 49

// Define the State Machine Table
static STATE_MACHINE_TABLE_ENTRY const micsconnStateTable[MICSCONN_EVENT_TABLE_LEN] =
{
//   There is one table entry for every event processed in each state
//   CURRENT STATE					EVENT ID				EVENT HANDLER				    END STATE
//   --------------------------------------------------------------------------------------------------------------------
DISPATCH_GEN_START(micsConnectionProcessEvent)
{MICSCONN_STATE_INITIAL,			E_INITIALIZE,			eventHandlerE_INITIALIZE		/* MICSCONN_STATE_SLEEPING 			*/},
{MICSCONN_STATE_SLEEPING,			E_MICS_INT_LINK_READY,	eventHandler245Wakeup			/* MICSCONN_STATE_IN_SESSION		*/},
{MICSCONN_STATE_IN_SESSION,			E_MICS_INT_CLOST,		eventHandlerSessionLost			/* MICSCONN_STATE_SLEEPING			*/},
{MICSCONN_STATE_IN_SESSION,			E_MICS_INT_RX_NOT_EMPTY, eventHandlerReceiveBlock		/* No Transition					*/},
{MICSCONN_STATE_SLEEPING,			E_CHARGER_ENGAGED,		eventHandlerChargingEngaged		/* MICSCONN_STATE_400MHZ_SLEEPING	*/},
{MICSCONN_STATE_400MHZ_SLEEPING,	E_STATE_ENTER,			eventHandler400mhzSleepingEnter	/* Multiple							*/},
{MICSCONN_STATE_400MHZ_SLEEPING,	E_MICS_INT_LINK_READY,	eventHandler245Wakeup			/* MICSCONN_STATE_IN_SESSION		*/},
{MICSCONN_STATE_400MHZ_SLEEPING,	E_MICS_CONNECTION_TIMEOUT, eventHandler400mhzWakeChip	/* MICSCONN_STATE_400MHZ_WAKING_CHIP	*/},
{MICSCONN_STATE_400MHZ_WAKING_CHIP, E_MICS_INT_RADIO_READY,	eventHandler400mhzStartListening	/* MICSCONN_STATE_400MHZ_LISTENING	*/},
{MICSCONN_STATE_400MHZ_LISTENING,	E_MICS_INT_RX_NOT_EMPTY, eventHandler400mhzCheckTid		/* Multiple							*/},
{MICSCONN_STATE_400MHZ_LISTENING,	E_MICS_CONNECTION_TIMEOUT, eventHandler400mhzNextChannel	/* MICSCONN_STATE_WAKING_CHIP		*/},
{MICSCONN_STATE_400MHZ_WAITING_TO_ESTABLISH_LINK,	E_MICS_INT_RADIO_READY,	eventHandler400mhzStartConnection	/* MICSCONN_STATE_400MHZ_CONNECTING	*/},
{MICSCONN_STATE_400MHZ_ESTABLISHING_LINK,	E_MICS_INT_LINK_READY,	eventHandler400mhzConnected			/* MICSCONN_STATE_IN_SESSION	*/},
{MICSCONN_STATE_400MHZ_ESTABLISHING_LINK,	E_MICS_CONNECTION_TIMEOUT,	eventHandler400mhzConnectionTimeout	/* MICSCONN_STATE_400MHZ_SLEEPING	*/},

//Diagnostics
{MICSCONN_STATE_IN_SESSION,			E_MICS_PERFORM_DIAGNOSTICS, eventHandlerStartMicsDiagnosticsDelay /* MICSCONN_STATE_DIAG_DELAY */	},
{MICSCONN_STATE_IN_SESSION,         E_POLL_LINK_ERRORS,         eventHandlerPollErrors              /* Do error polling */              },
{MICSCONN_STATE_DIAG_DELAY,			E_MICS_CONNECTION_TIMEOUT,	eventHandlerStartMicsDiagnostics	/* DIAG_RSSI or DIAG_STARTUP */		},
{MICSCONN_STATE_DIAG_STARTUP,		E_MICS_INT_RADIO_READY,		eventHandlerDiagChipIsReady			/* One of the diag run states*/		},
{MICSCONN_STATE_DIAG_RSSI,			E_STATE_ENTER,				eventHandlerDiagRssiSample			/* no transition */					},
{MICSCONN_STATE_DIAG_RSSI,			E_MICS_PERFORM_DIAGNOSTICS_RUN,	eventHandlerDiagRssiSample			/* no transition */					},
{MICSCONN_STATE_DIAG_RSSI,			E_MICS_CONNECTION_TIMEOUT,			eventHandlerDiagCleanup		/* IN_SESSION or a sleep state */	},
{MICSCONN_STATE_DIAG_TRANS_CW,		E_STATE_ENTER,				eventHandlerDiagTransmitCwEnter		/* no transition */					},
{MICSCONN_STATE_DIAG_TRANS_CW,		E_MICS_PERFORM_DIAGNOSTICS_RUN, eventHandlerDiagResetWatchdog		/* no transition */					},
{MICSCONN_STATE_DIAG_TRANS_CW,		E_MICS_CONNECTION_TIMEOUT,	eventHandlerDiagCleanup				/* a sleep state */					},
{MICSCONN_STATE_DIAG_TRANS_MODULATED,	E_STATE_ENTER,			eventHandlerDiagTransmitModulatedEnter  /* no transition */				},
{MICSCONN_STATE_DIAG_TRANS_MODULATED,	E_MICS_PERFORM_DIAGNOSTICS_RUN, eventHandlerDiagResetWatchdog		/* no transition */				},
{MICSCONN_STATE_DIAG_TRANS_MODULATED,	E_MICS_CONNECTION_TIMEOUT,	eventHandlerDiagCleanup			/* a sleep state */					},
{MICSCONN_STATE_DIAG_400_RECEIVE,		E_STATE_ENTER,				eventHandlerDiag400ReceiveEnter		/* no transition */					},
{MICSCONN_STATE_DIAG_400_RECEIVE,		E_MICS_PERFORM_DIAGNOSTICS_RUN, eventHandlerDiagResetWatchdog		/* no transition */					},
{MICSCONN_STATE_DIAG_400_RECEIVE,		E_MICS_CONNECTION_TIMEOUT,	eventHandlerDiagCleanup				/* a sleep state */					},
{MICSCONN_STATE_DIAG_24_RECEIVE,		E_STATE_ENTER,				eventHandlerDiag24ReceiveEnter		/* no transition */					},
{MICSCONN_STATE_DIAG_24_RECEIVE,		E_MICS_PERFORM_DIAGNOSTICS_RUN, eventHandlerDiagResetWatchdog		/* no transition */					},
{MICSCONN_STATE_DIAG_24_RECEIVE,		E_MICS_CONNECTION_TIMEOUT,	eventHandlerDiagCleanup				/* a sleep state */					},


//from any non-sleep state handle rx failed.
{MICSCONN_STATE_IN_SESSION,			E_MICS_INT_RX_OVERFLOW,		eventHandlerRxFailed				/* MICSCONN_DROP_SESSION_DELAY				*/},
{MICSCONN_STATE_400MHZ_WAKING_CHIP,	E_MICS_INT_RX_OVERFLOW,		eventHandlerRxFailed				/* MICSCONN_DROP_SESSION_DELAY				*/},
{MICSCONN_STATE_400MHZ_LISTENING,	E_MICS_INT_RX_OVERFLOW,		eventHandlerRxFailed				/* MICSCONN_DROP_SESSION_DELAY				*/},
{MICSCONN_STATE_400MHZ_WAITING_TO_ESTABLISH_LINK,	E_MICS_INT_RX_OVERFLOW,	eventHandlerRxFailed	/* MICSCONN_DROP_SESSION_DELAY				*/},
{MICSCONN_STATE_400MHZ_ESTABLISHING_LINK,	E_MICS_INT_RX_OVERFLOW,	eventHandlerRxFailed			/* MICSCONN_DROP_SESSION_DELAY				*/},

{MICSCONN_DROP_SESSION_DELAY,		E_MICS_INT_LINK_READY,		eventHandler245Wakeup				/* MICSCONN_STATE_IN_SESSION				*/},
{MICSCONN_DROP_SESSION_DELAY,		E_MICS_CONNECTION_TIMEOUT,	eventHandlerRxFailedResetTimeout	/* A sleep state						*/},

//from any state handle rx failed.
{MICSCONN_STATE_INITIAL,			E_MICS_FULL_RESET,		eventHandlerFullReset				/* MICSCONN_STATE_CHIP_OFF					*/},
{MICSCONN_STATE_SLEEPING,			E_MICS_FULL_RESET,		eventHandlerFullReset				/* MICSCONN_STATE_CHIP_OFF					*/},
{MICSCONN_STATE_IN_SESSION,			E_MICS_FULL_RESET,		eventHandlerFullReset				/* MICSCONN_STATE_CHIP_OFF					*/},
{MICSCONN_DROP_SESSION_DELAY,		E_MICS_FULL_RESET,		eventHandlerFullReset				/* MICSCONN_STATE_CHIP_OFF					*/},
{MICSCONN_STATE_400MHZ_SLEEPING,	E_MICS_FULL_RESET,		eventHandlerFullReset				/* MICSCONN_STATE_CHIP_OFF					*/},
{MICSCONN_STATE_400MHZ_WAKING_CHIP,	E_MICS_FULL_RESET,		eventHandlerFullReset				/* MICSCONN_STATE_CHIP_OFF					*/},
{MICSCONN_STATE_400MHZ_LISTENING,	E_MICS_FULL_RESET,		eventHandlerFullReset				/* MICSCONN_STATE_CHIP_OFF					*/},
{MICSCONN_STATE_400MHZ_WAITING_TO_ESTABLISH_LINK,	E_MICS_FULL_RESET,	eventHandlerFullReset	/* MICSCONN_STATE_CHIP_OFF					*/},
{MICSCONN_STATE_400MHZ_ESTABLISHING_LINK,	E_MICS_FULL_RESET,	eventHandlerFullReset			/* MICSCONN_STATE_CHIP_OFF					*/},

{MICSCONN_STATE_CHIP_OFF,			E_MICS_CONNECTION_TIMEOUT, eventHandlerChipResetTimeout	/* MICSCONN_STATE_RESET_TEST					*/},

DISPATCH_GEN_END
};

static void micsconnTransition(MICSCONN_STATE aNewState);


static MICSCONN_STATE micsconnCurrentState = MICSCONN_STATE_INITIAL;
static XPG_MICS_ID micsconn400mhzMyMicsId;
static uint8_t micsconn400mhzCurrentChannel;

static MICS_DIAGNOSTICS_PARAMS micsconnDiagnosticsMode;

void micsConnectionProcessEvent(EVENT eventToProcess)
{
	int i = 0;
	EVENT_ID eventId = eventToProcess.eventID;

	// Iterate through the state machine table until find the current state.
	for(i=0; i < MICSCONN_EVENT_TABLE_LEN; i++)
	{
		if((micsconnStateTable[i].state == micsconnCurrentState)  && micsconnStateTable[i].eventId == eventId)
		{
			break;
		}
	}

	//Now call the appropriate event handler
	if(i != MICSCONN_EVENT_TABLE_LEN)
	{
		micsconnStateTable[i].eventHandler(eventToProcess);
	}

	return;
}


//State Machine Helper Functions
static void micsconnTransition(MICSCONN_STATE aNewState)
{
	int i = 0;
	int exitHandler = -1;
	int entryHandler = -1;
	EVENT tmpEvent;


	// Iterate through the state machine table until find the current state and transition.
	// Check both enters and exits so we don't have to repeat the loop.
	for(i=0; i < MICSCONN_EVENT_TABLE_LEN; i++)
	{
		if((micsconnStateTable[i].state == micsconnCurrentState)  && micsconnStateTable[i].eventId == E_STATE_EXIT)
		{
			exitHandler = i;
		}

		if((micsconnStateTable[i].state == aNewState) && micsconnStateTable[i].eventId == E_STATE_ENTER)
		{
			entryHandler = i;
		}

		if(exitHandler != -1 && entryHandler != -1)
		{
			break;
		}
	}


	//Exit the current function.
	if(exitHandler != -1)
	{
		tmpEvent.eventID = E_STATE_EXIT;
		micsconnStateTable[exitHandler].eventHandler(tmpEvent);
	}

	//Set the state.
	micsconnCurrentState = aNewState;

	//Enter the new function
	if(entryHandler != -1)
	{
		tmpEvent.eventID = E_STATE_ENTER;
		micsconnStateTable[entryHandler].eventHandler(tmpEvent);
	}

	return;

}

#pragma diag_suppress 880
//These handlers often don't reference the event, but must receive it to meet the interface.

//EVENT HANDLERS
static void eventHandlerE_INITIALIZE(EVENT event)
{
	micsconnTransition(MICSCONN_STATE_SLEEPING);
}

static void eventHandler245Wakeup(EVENT event)
{
	newSessionOpened();
	micsconnTransition(MICSCONN_STATE_IN_SESSION);
}


static void eventHandlerSessionLost(EVENT event)
{
	micsSleep();

	dispPutSimpleEvent(E_MICS_SESSION_CLOSED);

	transitionToCorrectSleepState();
}


static void eventHandlerReceiveBlock(EVENT event)
{
	EVENT outEvent;
	uint16_t bytesReady;

	bytesReady = micsReadBufferBytesReady();
	if((bytesReady / 15) > MICSCONN_MAX_BLOCKS_TO_POST)
	{
		logNormal(E_LOG_MICS_TOOMUCH, bytesReady);
		fullMicsChipReset();
		return;
	}


	while(micsReadBufferBytesReady() >= MICS_BLOCK_SIZE)
	{
		memset(&outEvent, 0, sizeof(event));
		outEvent.eventID = E_MICS_DATA_BLOCK;

		//read the data out of the zarlink chip:
		if(micsReadFromBuffer(outEvent.eventData.micsDataBlock, 0, MICS_BLOCK_SIZE) != MICS_BLOCK_SIZE)
		{
			// Somehow the number of available bytes was reduced between the while loop's condition
			// and the read. This should not be possible because we are single-threaded and the isr
			// should only increase the number of available bytes. If we ever get here, reset the IPG.
			resetSystem();
		}

		//publish an event that includes the data just read
		dispPutEvent(outEvent);
	}

	micsDoneReadingFromBuffer();
}


static void eventHandlerChargingEngaged(EVENT event)
{
	if(isIPG())
	{
		micsconnTransition(MICSCONN_STATE_400MHZ_SLEEPING);
	}
}

static void eventHandlerRxFailed(EVENT event)
{
	EVENT timeoutEvent;

	// The driver level aready handled the reset and put the
	// chip to sleep. We then wait 5s before listening on 400MHz
	// to force the other end to hang up.
	timeoutEvent.eventID = E_MICS_CONNECTION_TIMEOUT;
	timeoutEvent.eventData.micsConnectionTimeoutPhase = MICSCONN_TIMEOUT_PHASE_RESETTING;
	swTimerSet(SWTMR_MICS_CONNECTION, MICSCONN_RESET_WAIT_TIME, timeoutEvent, false);

	micsconnTransition(MICSCONN_DROP_SESSION_DELAY);
}

static void eventHandlerRxFailedResetTimeout(EVENT event)
{
	//Now that there's a timeout, go to the normal sleep state.
	transitionToCorrectSleepState();
}

static void eventHandler400mhzSleepingEnter(EVENT event)
{
	//set a timer to start the listening.
	EVENT timeoutEvent;
	timeoutEvent.eventID = E_MICS_CONNECTION_TIMEOUT;
	timeoutEvent.eventData.micsConnectionTimeoutPhase = MICSCONN_TIMEOUT_PHASE_SLEEPING;
	swTimerSet(SWTMR_MICS_CONNECTION, MICSCONN_400MHZ_SLEEP_TIME, timeoutEvent, false);
}

static void eventHandler400mhzWakeChip(EVENT event)
{
	EVENT timeoutEvent;

	if(event.eventID == E_MICS_CONNECTION_TIMEOUT && event.eventData.micsConnectionTimeoutPhase == MICSCONN_TIMEOUT_PHASE_SLEEPING)
	{
		if(!micsIsAwake())
		{
			//cache xPG identity so we don't have to keep re-reading flash in the upcoming states.
			micsconn400mhzMyMicsId = getXpgInfo()->xpgIdentity.xpgMicsId;

			//clear the current channel variable
			micsconn400mhzCurrentChannel = 0;

			// hold the WU_EN pin high and wait for radio_ready
			micsManualWakeup();

			micsconnTransition(MICSCONN_STATE_400MHZ_WAKING_CHIP);
		}
		else
		{
			//if we got here, we are probably opening the session already via 2.45GHz. Skip this attempt and try again.
			timeoutEvent.eventID = E_MICS_CONNECTION_TIMEOUT;
			timeoutEvent.eventData.micsConnectionTimeoutPhase = MICSCONN_TIMEOUT_PHASE_SLEEPING;
			swTimerSet(SWTMR_MICS_CONNECTION, MICSCONN_400MHZ_SLEEP_TIME, timeoutEvent, false);
		}

	}
}



static void eventHandler400mhzStartListening(EVENT event)
{
	EVENT timeoutEvent;

	// start the chip listening
	micsSetChannel(micsconn400mhzCurrentChannel);
	micsConfigureFor400mhzListen(&micsconn400mhzMyMicsId);

	// set the timer to switch channels
	timeoutEvent.eventID = E_MICS_CONNECTION_TIMEOUT;
	timeoutEvent.eventData.micsConnectionTimeoutPhase = MICSCONN_TIMEOUT_PHASE_LISTENING;
	swTimerSet(SWTMR_MICS_CONNECTION, MICSCONN_400MHZ_LISTEN_TIME, timeoutEvent, false);

	// transition to the listening state
	micsconnTransition(MICSCONN_STATE_400MHZ_LISTENING);
}

static void eventHandler400mhzCheckTid(EVENT event)
{
	int bytesInBuffer;
	int i;
	char tid[3];

	while((bytesInBuffer = micsReadBufferBytesReady()) >= 3)
	{
		for(i=bytesInBuffer/3; i>0; i--) //we expect the /3 to truncate the result. We only read full tids.
		{
			if(micsReadFromBuffer(tid, 0, 3) != 3)
			{
				// Somehow the number of available bytes was reduced between the while loop's condition
				// and the read. This should not be possible because we are single-threaded and the isr
				// should only increase the number of available bytes. If we ever get here, reset the IPG.
				resetSystem();
			}
			else
			{
				if((tid[0] == micsconn400mhzMyMicsId.micsIDmsb
							&& tid[1] == micsconn400mhzMyMicsId.micsIDmiddleByte
							&& tid[2] == micsconn400mhzMyMicsId.micsIDlsb)
						|| ( tid[0] == 0 && tid[1] == 0 && tid[2] == 0))
				{
					micsStop400mhzWakeupOperation();
					micsconnTransition(MICSCONN_STATE_400MHZ_WAITING_TO_ESTABLISH_LINK);
					return;
				}

			}
		}
	}

	micsDoneReadingFromBuffer();
}


static void eventHandler400mhzNextChannel(EVENT event)
{
	if(event.eventID == E_MICS_CONNECTION_TIMEOUT)
	{
		if((event.eventData.micsConnectionTimeoutPhase == MICSCONN_TIMEOUT_PHASE_LISTENING && micsconnCurrentState == MICSCONN_STATE_400MHZ_LISTENING))

		{
			++micsconn400mhzCurrentChannel;
			if(micsconn400mhzCurrentChannel >= 10)
			{
				//looked at all the channels. Reset and give up.
				micsSleep();
				transitionToCorrectSleepState();
			}
			else
			{
				//exit listening mode.
				micsStop400mhzWakeupOperation();

				//wait for radio_ready and retry
				micsconnTransition(MICSCONN_STATE_400MHZ_WAKING_CHIP);
			}
		}
	}
}


static void eventHandler400mhzStartConnection(EVENT event)
{
	EVENT timeoutEvent;

	//reset MICS identity in case we changed it to the wildcard for verification.
	micsSetMicsID(&micsconn400mhzMyMicsId);

	//Clear receive buffers since we won't be resetting the chip.
	micsFlushRxBuffer();

	//release the chip to connect.
	mics400mhzConnect();

	//set a timeout
	timeoutEvent.eventID = E_MICS_CONNECTION_TIMEOUT;
	timeoutEvent.eventData.micsConnectionTimeoutPhase = MICSCONN_TIMEOUT_PHASE_CONNECTING;
	swTimerSet(SWTMR_MICS_CONNECTION, MICSCONN_400MHZ_VERIFY_TIME, timeoutEvent, false);

	micsconnTransition(MICSCONN_STATE_400MHZ_ESTABLISHING_LINK);

}

static void eventHandler400mhzConnected(EVENT event)
{
	newSessionOpened();
	micsconnTransition(MICSCONN_STATE_IN_SESSION);
}

static void eventHandler400mhzConnectionTimeout(EVENT event)
{
	if(event.eventID == E_MICS_CONNECTION_TIMEOUT && event.eventData.micsConnectionTimeoutPhase == MICSCONN_TIMEOUT_PHASE_CONNECTING)
	{
		++micsconn400mhzCurrentChannel;
		if(micsconn400mhzCurrentChannel >= 10)
		{
			//looked at all the channels. Reset and give up.
			micsSleep();
			transitionToCorrectSleepState();
		}
		else
		{
			//exit listening mode.
			micsStop400mhzWakeupOperation();

			//wait for radio_ready and retry
			micsconnTransition(MICSCONN_STATE_400MHZ_WAKING_CHIP);
		}
	}
}

static void eventHandlerFullReset(EVENT event)
{
	fullMicsChipReset();
}

static void fullMicsChipReset(){

	EVENT timeoutEvent;

	dispPutSimpleEvent(E_MICS_SESSION_CLOSED);

	timeoutEvent.eventID = E_MICS_CONNECTION_TIMEOUT;
	timeoutEvent.eventData.micsConnectionTimeoutPhase = MICSCONN_TIMEOUT_PHASE_CHIP_OFF;
	swTimerSet(SWTMR_MICS_CONNECTION, MICSCONN_CHIP_OFF_TIME, timeoutEvent, false);

	micsconnTransition(MICSCONN_STATE_CHIP_OFF);
}

static void eventHandlerChipResetTimeout(EVENT event)
{
	if(event.eventData.micsConnectionTimeoutPhase == MICSCONN_TIMEOUT_PHASE_CHIP_OFF)
	{
		// set up the chip.
		if (micsInit(trimIsListValid(TRIM_ZL), trimGetList(TRIM_ZL), &getXpgInfo()->xpgIdentity.xpgMicsId) < 0)
		{
			resetSystem();
		}



		transitionToCorrectSleepState();
	}
}



//mics diagnostics event handlers
static void eventHandlerStartMicsDiagnosticsDelay(EVENT event)
{
	micsconnDiagnosticsMode = event.eventData.micsDiagnosticsParams;

	setTimer(MICSCONN_TIMEOUT_PHASE_DIAG_DELAY, MS(micsconnDiagnosticsMode.delay));

	micsconnTransition(MICSCONN_STATE_DIAG_DELAY);
}


static void eventHandlerStartMicsDiagnostics(EVENT event){
	if(event.eventID == E_MICS_CONNECTION_TIMEOUT && event.eventData.micsConnectionTimeoutPhase == MICSCONN_TIMEOUT_PHASE_DIAG_DELAY)
	{
		if(micsconnDiagnosticsMode.channel == MICS_DIAGNOSTICS_CURRENT_CHANNEL
				&& micsconnDiagnosticsMode.mode == MICS_DIAGNOSTICS_MODE_RSSI)
		{
			if(micsconnDiagnosticsMode.durration > 0)
			{
				setTimer(MICSCONN_TIMEOUT_PHASE_DIAG_RUN, MS(micsconnDiagnosticsMode.durration));
			}
			micsconnTransition(MICSCONN_STATE_DIAG_RSSI);
		}
		else
		{
			if(micsconnDiagnosticsMode.channel == MICS_DIAGNOSTICS_CURRENT_CHANNEL)
			{
				//retrieve and store the current channel
				micsconnDiagnosticsMode.channel = micsGetCurrentChannel();
			}

			micsSleep();
			micsManualWakeup();

			micsconnTransition(MICSCONN_STATE_DIAG_STARTUP);
		}
	}
}

static void eventHandlerDiagChipIsReady(EVENT event)
{
	//set the channel
	micsSetChannel(micsconnDiagnosticsMode.channel);

	if(micsconnDiagnosticsMode.durration > 0)
	{
		setTimer(MICSCONN_TIMEOUT_PHASE_DIAG_RUN, MS(micsconnDiagnosticsMode.durration));
	}

	//We are now ready to run the command. Go to the appropriate handler event.
	switch(micsconnDiagnosticsMode.mode)
	{
	case MICS_DIAGNOSTICS_MODE_RSSI:
		micsconnTransition(MICSCONN_STATE_DIAG_RSSI);
		break;
	case MICS_DIAGNOSTICS_MODE_TRANSMIT_CW:
		micsconnTransition(MICSCONN_STATE_DIAG_TRANS_CW);
		break;
	case MICS_DIAGNOSTICS_MODE_TRANSMIT_MODULATED:
		micsconnTransition(MICSCONN_STATE_DIAG_TRANS_MODULATED);
		break;
	case MICS_DIAGNOSTICS_MODE_400_RECEIVE:
		micsconnTransition(MICSCONN_STATE_DIAG_400_RECEIVE);
		break;
	case MICS_DIAGNOSTICS_MODE_24_RECEIVE:
			micsconnTransition(MICSCONN_STATE_DIAG_24_RECEIVE);
			break;
	default:
		//unknown mode somehow. Just go back to sleep.
		micsSleep();
		transitionToCorrectSleepState();
	}
}


static void eventHandlerDiagRssiSample(EVENT event)
{
	uint8_t rssi;

	rssi = micsMeasureRssi();
	logNormal(E_LOG_MICS_RSSI, rssi);

	micsResetWatchdog();

	//enqueue the event again. This will trigger another measurement without blocking the event queue.
	dispPutSimpleEvent(E_MICS_PERFORM_DIAGNOSTICS_RUN);
}

static void eventHandlerDiagResetWatchdog(EVENT event)
{
	micsResetWatchdog();

	// re-post the event to keep the watchdog cleared.
	dispPutSimpleEvent(E_MICS_PERFORM_DIAGNOSTICS_RUN);
}

static void eventHandlerDiagTransmitCwEnter(EVENT event)
{
	micsTransmitCw();

	//the event to keep the watchdog cleared.
	dispPutSimpleEvent(E_MICS_PERFORM_DIAGNOSTICS_RUN);
}

static void eventHandlerDiagTransmitModulatedEnter(EVENT event)
{
	micsTransmitModulated();

	//the event to keep the watchdog cleared.
	dispPutSimpleEvent(E_MICS_PERFORM_DIAGNOSTICS_RUN);
}

static void eventHandlerDiag400ReceiveEnter(EVENT event)
{
	micsDiagnostic400ReceiveMode();

	//the event to keep the watchdog cleared.
	dispPutSimpleEvent(E_MICS_PERFORM_DIAGNOSTICS_RUN);
}

static void eventHandlerDiag24ReceiveEnter(EVENT event)
{
	micsDiagnostic24ReceiveMode();

	//the event to keep the watchdog cleared.
	dispPutSimpleEvent(E_MICS_PERFORM_DIAGNOSTICS_RUN);
}

static void eventHandlerDiagCleanup(EVENT event)
{
	if(event.eventID == E_MICS_CONNECTION_TIMEOUT
			&& event.eventData.micsConnectionTimeoutPhase == MICSCONN_TIMEOUT_PHASE_DIAG_RUN)
	{
		if(micsconnDiagnosticsMode.channel == MICS_DIAGNOSTICS_CURRENT_CHANNEL)
		{
			micsconnTransition(MICSCONN_STATE_IN_SESSION);
		}
		else
		{
			micsSleep();
			transitionToCorrectSleepState();
		}
	}
}

static void eventHandlerPollErrors(EVENT event)
{
    if(event.eventID == E_POLL_LINK_ERRORS)

    {
        micsPollLinkStatus(1);
    }
}

#pragma diag_default 880


static void transitionToCorrectSleepState()
{
	//Check that we are still charging. If not, transition back to the normal sleeping state.
#ifndef EPG
	if(isIPG() && chrgMgrIsChargerEngaged())
	{
		micsconnTransition(MICSCONN_STATE_400MHZ_SLEEPING);
	}
	else
	{
		micsconnTransition(MICSCONN_STATE_SLEEPING);
	}
#else
	micsconnTransition(MICSCONN_STATE_SLEEPING);
#endif
}


static void setTimer(MICSCONN_TIMEOUT_PHASE phase, uint32_t time)
{
	EVENT timeoutEvent;
	timeoutEvent.eventID = E_MICS_CONNECTION_TIMEOUT;
	timeoutEvent.eventData.micsConnectionTimeoutPhase = phase;
	swTimerSet(SWTMR_MICS_CONNECTION, time, timeoutEvent, false);
}

static void newSessionOpened()
{
    /*
     * Set TXBUFF_MAXPACKSIZE, TXBUFF_BSIZE and RXBUFF_BSIZE for a
     * normal session.  This ensures they'll always be ready for a
     * normal session with the ZL7010x is awakened via 2.45 GHz (in
     * case their emergency settings where copied to the ZL7010x's
     * wakeup stack during an earlier emergency operation, and were
     * therefore restored when the ZL7010x woke up).
     */
    micsConfigBuffers(MICS_BLOCK_SIZE, MICS_MAXPACKSIZE_MAX);
	dispPutSimpleEvent(E_MICS_SESSION_OPEN);
}

