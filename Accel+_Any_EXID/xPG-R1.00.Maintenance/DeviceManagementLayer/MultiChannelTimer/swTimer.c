/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description:	Software Timer Module main file.
 *
 * The Software Timer Module provides the means to share a single hardware timer
 * and create any number of virtual timers.
 *
 * The application defines the virtual timers in an enumeration
 * stored in the file swtmr_application_timers.h
 ***************************************************************************/

#include <stdint.h>
#include <stdbool.h>
#include "timer.h"
#include "MultiChannelTimer.h"
#include "swtmr_application_timers.h"
#include "Interrupt.h"
#include "EventQueue.h"
#include "system_events.h"
#include "system.h"
#include "log.h"
#include "delay.h"
#include "clock.h"
#include "timeout.h"
#include "system.h"

#ifndef NULL
#define NULL 0
#endif

//define this to run a timer test with the oscilloscope
//#define TOGGLETIMER_TEST_DURRATION MS(250)

/*! This structure represents a software timer. */
typedef volatile struct SoftwareTimer_
{
#ifndef ACLK_USES_LFXT1CLK
	//! The time at which the timer will expire. Used when we do not have a consistent tick basis.
	uint64_t expiryTime;
#else
	//! The upper 32-bits of the virtual 48-bit clock.
	uint32_t expiryRollovers;

	//! The lower 16 bits of the virtual 48-bit clock.
	uint16_t expiryTicks;
#endif

	//! The duration of the timer. Used to re-arm recurring timers.
	uint32_t durration;

	//! The event to enqueue when the timer expires.
	EVENT event;

	//! When true, the timer is in the timer queue.
	bool isRunning;

	//! True if the event should recur automatically.
	bool isRecurring;

	//! The next timer in the queue.
	volatile struct SoftwareTimer_ *next;

	//! The previous timer in the queue.
	volatile struct SoftwareTimer_ *prev;
} SoftwareTimer;

static SoftwareTimer softwareTimers[(unsigned int)SWTMR_NUM_TIMERS];

#ifndef ACLK_USES_LFXT1CLK

#ifdef UNIT_TESTING
uint16_t lastVloCalibration;
#else
static uint16_t lastVloCalibration = 13000/8; //default calibratin so it doesn't run as 0 before calibraiton.
#endif
static uint16_t lastCalibrationTicks = 0;
static uint64_t elapsedMs;

// The "ul" on the constant below is important. We want to do all of the multiplications and divisions
// in this calculation in 32-bit variables. 64-bit takes many orders of magnitude more time.
#define CURMS(curTicks) (elapsedMs + (curTicks - lastCalibrationTicks) * 1000ul / lastVloCalibration)

#else

static volatile uint32_t rolloverCount;

#endif

static SoftwareTimer * volatile timerQueue;


// forward-declare static functions
static void setNextTimerInterrupt();
static void enqueueTimer(SoftwareTimer *tim);
static void dequeueTimers();
static void rearmTimer(SoftwareTimer *tim);
static void checkAllTimersForExpiration();


// helper macros
#define SWTIMER_PACKTIME(roll, tick) ((((uint64_t)roll) << 16) + tick)

#define SWTIMER_UNPACKTIME(time, roll, tick)\
	roll = (uint32_t)(time >> 16);\
	tick = (uint16_t)(time);

#define SWTIMER_ISVALIDTIME(time) ((time & 0xFFFF0000000000) == 0)

// See errata sheet for details on why this is set to 2.
#define TICKS_ENSURED_IN_THE_FUTURE 2

//-----------------------------------------------------------------------------------
/**
 * This process initializes the timer queue elements.
 *
 *
 * \returns void
 *
 */
//-----------------------------------------------------------------------------------
void swTimerInit(void)
{
#ifdef TOGGLETIMER_TEST_DURRATION
	EVENT e;
#endif
	/*SWTIMUTEST
	EVENT e;
	SoftwareTimer *testTimer;
    */

	int16_t i;
    uint32_t interruptSave;

    //disable interrupts so that a timeout does not happen
    //while an element is changing
    interruptSave = interruptDisable();
#ifndef ACLK_USES_LFXT1CLK
    elapsedMs = 0;
    lastVloCalibration = VLOCLK_measurement();
    lastCalibrationTicks = timerTicks();
#else
    rolloverCount = 0;
#endif

    timerQueue = NULL;

    for (i = 0; i < SWTMR_NUM_TIMERS; i++)
    {
        SoftwareTimer *tim = &softwareTimers[i];
        tim->next = NULL;
        tim->prev = NULL;
        tim->isRunning = false;
        tim->isRecurring = false;
    }

    //Initialize the hardware
    timerConfigureSWTimer();

    //re-enable interrupts
    interruptRestore(interruptSave);  // restore the interrupts



  //For unit testing:
#ifdef TOGGLETIMER_TEST_DURRATION
    e.eventID = E_SWTIMER_TOGGLETEST;
    swTimerSet(SWTMR_SWTIMER_TOGGLETEST, TOGGLETIMER_TEST_DURRATION, e, true);

#endif

    /*
    //SWTIMUTEST
    setTST_TRIG_HIGH();
    e.eventID = E_SWTIMER_TOGGLETEST;
    testTimer = &softwareTimers[(unsigned int)SWTMR_SWTIMER_TOGGLETEST];
    testTimer->durration = 0;
    testTimer->event = e;
    testTimer->expiryRollovers = 0;
    testTimer->isRunning = true;
    testTimer->expiryTicks = 0xFFFF;
    enqueueTimer(testTimer);
    */

}

//-----------------------------------------------------------------------------------
/**
 *	This process sets up a new timer queue element.
 *
 *	Add new Timer Queue Element with passed parameters
 *
 * \param  timerQueueElement - Which element to update
 * \param  timeout - Time in timer ticks until the routine should be called.
 * \param  event - Event to put into the event queue when the timer elapses.
 * \param repeat - A boolean flag that, when true, causes the timer to run until canceled.
 * Otherwise the timer will run only once.
 *
 *	\returns
 *		void
 *
 */
//-----------------------------------------------------------------------------------

void swTimerSet(SWTMR_ENUM timerQueueElement, uint32_t timeout, EVENT event, bool repeat)
{
	SoftwareTimer *tim;
	uint16_t curTicks;
	uint16_t interruptSave;
#ifdef ACLK_USES_LFXT1CLK
	uint64_t expiryTime;
#endif

	//Find the timer. If it is set, cancel it.
	tim = &softwareTimers[(unsigned int)timerQueueElement];
	if(tim->isRunning)
	{
		swTimerCancel(timerQueueElement);
	}

	tim->durration = timeout;
	tim->event = event;
	tim->isRecurring = repeat;
	tim->isRunning = true;


	// We have to disable the interrupts just after reading the ticks to prevent a
	// rollover from being processed while inserting the timer. Don't disable before
	// timerTicks() returns because it runs a loop which may have a rollover in it.
	// This would cause the timer to be off by 1 rollover.
	curTicks = timerTicks();
	interruptSave = interruptDisable();

#ifndef ACLK_USES_LFXT1CLK
	// calculate current ms
	tim->expiryTime = CURMS(curTicks);
	tim->expiryTime += timeout;

#else
	expiryTime = SWTIMER_PACKTIME(rolloverCount, curTicks);
	expiryTime += timeout;

	//if the time isn't valid, reset.
	if(!SWTIMER_ISVALIDTIME(expiryTime))
	{
		logNormal(E_LOG_SW_TIMER_RESET, 0);
		resetSystem();
	}

	SWTIMER_UNPACKTIME(expiryTime, tim->expiryRollovers, tim->expiryTicks);
#endif

	// Put the timer in the queue and setup the interrupt if needed.
	enqueueTimer(tim);

	interruptRestore(interruptSave);

}

//-----------------------------------------------------------------------------------
/**
 *	This process clears a new timer queue element.
 *
 *	Set the Timer Queue Element Flag to NOT_USED for the
 * 	element passed as a parameter.
 *
 *	\param timerQueueElement - Which element to update
 *
 *	\returns void
 *
 */
//-----------------------------------------------------------------------------------
void swTimerCancel(SWTMR_ENUM timerQueueElement)
{
	SoftwareTimer *tim;
	SoftwareTimer *prev;
	SoftwareTimer *next;
	uint16_t interruptSave;


	tim = &softwareTimers[(unsigned int)timerQueueElement];

	// The rearmTimer() function may re-arm a timer if the message to do so is
	// already in the queue. It checks to make sure that the timer is still a
	// recurring timer, so clear that here to cause it to stop.
	tim->isRecurring = false;

	if(tim->isRunning)
	{
		interruptSave = interruptDisable();
		prev = tim->prev;
		next = tim->next;

		if(prev)
		{
			prev->next = next;
		}
		else
		{
			//start of queue
			timerQueue = next;
		}

		if(next)
		{
			next->prev = prev;
		}
		//else nothing to update.

		// if we removed the first element, we don't want to
		// bother waking for it. Reset the next interrupt.
		if(!prev)
		{
			setNextTimerInterrupt();
		}

		tim->isRunning = false;
		interruptRestore(interruptSave);
	}


}

//-----------------------------------------------------------------------------------
/**
 * \brief
 *	This process returns the remaining time in the timer queue element specified.
 *
 *	\param timerQueueElement - Which element whose remaining time we are returning
 *
 *	\returns This function returns the remaining timer counts until the timer will elapse.
 *
 */
//-----------------------------------------------------------------------------------
uint32_t swTimerGetRemainingTime(SWTMR_ENUM timerQueueElement)
{
	uint16_t curTicks;
	uint64_t curTime;
	SoftwareTimer *tim;
#ifdef ACLK_USES_LFXT1CLK
	uint64_t expiryTime;
#endif

	tim = &softwareTimers[(unsigned int)timerQueueElement];

	if(tim->isRunning)
	{
#ifndef ACLK_USES_LFXT1CLK
		curTicks = timerTicks();
		curTime = CURMS(curTicks);
		if(tim->expiryTime > curTime)
		{
			return (uint32_t)(tim->expiryTime - curTime);
		}
#else
		expiryTime = SWTIMER_PACKTIME(tim->expiryRollovers, tim->expiryTicks);
		curTicks = timerTicks();
		curTime = SWTIMER_PACKTIME(rolloverCount, curTicks);

		if(expiryTime > curTime)
		{
			return (uint32_t)(expiryTime - curTime);
		}
#endif
	}

	// not running or expiry time in the past.
	return 0;
}


uint32_t swTimerGetCurrentTime(void)
{

#ifndef ACLK_USES_LFXT1CLK
	uint16_t curTicks = timerTicks();
	return (uint32_t)CURMS(curTicks);
#else
	uint16_t curTicks;
	uint64_t res;
	uint16_t gie;

	gie = interruptDisable();

	curTicks = timerTicks();
	res = (uint64_t)rolloverCount;

	interruptRestore(gie);

	res = res << 16;
	res += curTicks;
	res *= 1000;
	res /= TICKS_HZ;

	return res;
#endif
}



/**
 * This is the function that the timer module calls when an timer overflow interrupt
 * occurs on Timer A.
 */
void swTimerOverflow(void)
{
#ifndef ACLK_USES_LFXT1CLK
	elapsedMs += ((uint64_t)(0x010000ul - lastCalibrationTicks)) * 1000 / lastVloCalibration;

	//update the rest
	lastCalibrationTicks = 0;

	//queue a request to recalibrate
	dispPutSimpleEvent(E_CALIBRATE_VLO);

#else
	rolloverCount++;

	if(rolloverCount == 0)
	{
		// 2179 years have passed somehow. Reset.
		logNormal(E_LOG_SW_TIMER_RESET, 0);
		resetSystem();
	}
#endif

	dequeueTimers();

	//as a backup, make sure that all timers are either not running, or expire in the future.
	checkAllTimersForExpiration();
}

/*! This function is called by the timer whenever the sw timer CCR interrupt is raised.
 */
void swTimerInterrupt()
{
	dequeueTimers();
}


void swTimerProcessEvent(EVENT event)
{
DISPATCH_GEN_START(swTimerProcessEvent)

	SoftwareTimer *tim;

#ifndef ACLK_USES_LFXT1CLK
	if(event.eventID == E_CALIBRATE_VLO)
	{
		swTimerUpdateVloCalibration();
	}
#endif

	if(event.eventID == E_SWTIMER_REARM)
	{
		tim = (SoftwareTimer*)event.eventData.swTimerPointer;
		rearmTimer(tim);
	}


#ifdef TOGGLETIMER_TEST_DURRATION

    if (event.eventID == E_SWTIMER_TOGGLETEST)
    {
    	setTST_TRIG_TOGGLE();
    }


#endif

    /*SWTIMUTEST
    if (event.eventID == E_SWTIMER_TOGGLETEST)
	{
		setTST_TRIG_LOW();
	}
	*/

DISPATCH_GEN_END
}

#ifndef ACLK_USES_LFXT1CLK

void swTimerUpdateVloCalibration()
{
	int avgCalib;
	int newCalibration;
	uint16_t curTicks;
	int gie;

	newCalibration = VLOCLK_measurement();

	//Get the time
	curTicks = timerTicks();
	gie = interruptDisable();

	if(curTicks > lastCalibrationTicks) //here we assume that the clock hasn't rolled over because we handle that in the ISR.
	{
		//get the average between the last and the new calibrations
		avgCalib = (lastVloCalibration + newCalibration) / 2;

		//update the ms count.
		elapsedMs += ((uint64_t)(curTicks - lastCalibrationTicks)) * 1000 / avgCalib;

		//update the rest
		lastCalibrationTicks = curTicks;
		lastVloCalibration = newCalibration;

		dequeueTimers();

		interruptRestore(gie);
	}
	else
	{
		interruptRestore(gie);
	}
}

#endif


/*! Configure the hardware timer for the next interrupt.
 *
 * \warn Must be called with interrupts disabled.
 */
static void setNextTimerInterrupt(void)
{
	uint16_t curTicks;
	SoftwareTimer *tim;
#ifndef ACLK_USES_LFXT1CLK
	uint64_t expiryTicks;
#endif

	tim = timerQueue;

#ifndef ACLK_USES_LFXT1CLK
	if(tim && tim->expiryTime < elapsedMs)
#else
	if(tim && tim->expiryRollovers < rolloverCount)
#endif
	{
		//Something is wrong. Reset.
		logNormal(E_LOG_SW_TIMER_RESET, 0);
		resetSystem();
	}

#ifndef ACLK_USES_LFXT1CLK
	if(tim != NULL)
	{
		curTicks = timerTicks();
		// The cast to uint32_t below is important. We want to do all of the multiplications and divisions
		// in this calculation in 32-bit variables. 64-bit takes many orders of magnitude more time.
		expiryTicks = ((uint32_t)(tim->expiryTime - elapsedMs)) * lastVloCalibration / 1000ul + lastCalibrationTicks;
	}

	if(tim == NULL || expiryTicks >= 0x10000)
#else
		if(tim == NULL || tim->expiryRollovers > rolloverCount)
#endif
	{
		// disable CCR2
		timerDisableSWTimerInterrupt();
	}
	else
	{
#ifndef ACLK_USES_LFXT1CLK
		// set CCR2 to the timer's tick value.
		timerEnableSWTimerInterrupt();
		timerSetSWTimer((uint16_t)expiryTicks);

		// It is possible that tim->expiryTicks has already passed, or
		// is set so close in the future that the hardware will not
		// interrupt (see errata sheet). If so, reset the timer to a
		// very short time in the future.
		curTicks = timerTicks();
		if(curTicks + TICKS_ENSURED_IN_THE_FUTURE >= expiryTicks)
		{
			timerSetSWTimer(curTicks + TICKS_ENSURED_IN_THE_FUTURE + 1);
		}
#else
		// set CCR2 to the timer's tick value.
		timerEnableSWTimerInterrupt();
		timerSetSWTimer(tim->expiryTicks);

		// It is possible that tim->expiryTicks has already passed, or
		// is set so close in the future that the hardware will not
		// interrupt (see errata sheet). If so, reset the timer to a
		// very short time in the future.
		curTicks = timerTicks();
		if(curTicks + TICKS_ENSURED_IN_THE_FUTURE >= tim->expiryTicks)
		{
			timerSetSWTimer(curTicks + TICKS_ENSURED_IN_THE_FUTURE + 1);
		}
#endif
	}


}


/*! Enqueue a timer into the active timer queue.
 * \param tim A pointer to the timer object to enqueue.
 *
 * \warn The function must be called with interrupts disabled.
 */
static void enqueueTimer(SoftwareTimer *tim)
{
	SoftwareTimer *prev;
	SoftwareTimer *cur;

	prev = NULL;
	cur = timerQueue;

	//search for the insertion point.
#ifndef ACLK_USES_LFXT1CLK
	while(cur && cur->expiryTime < tim->expiryTime)
#else
	while(cur
			&& (cur->expiryRollovers < tim->expiryRollovers
				|| (cur->expiryRollovers == tim->expiryRollovers && cur->expiryTicks < tim->expiryTicks)))
#endif
	{
		prev = cur;
		cur = cur->next;
	}

	// Cur now points to the first element that should be after tim, and prev points the last element before tim.
	if(cur)
	{
		tim->next = cur;
		cur->prev = tim;
	}
	else
	{
		//end of queue
		tim->next = NULL;
	}

	if(prev)
	{
		prev->next = tim;
		tim->prev = prev;
	}
	else
	{
		// Beginning of the queue.
		tim->prev = NULL;
		timerQueue = tim;

		//The next timeout expected has changed. Update the interrupt.
		setNextTimerInterrupt();
	}
}

static void dequeueTimers()
{
	bool dequeued;
	SoftwareTimer *tim;
	EVENT rearmEvent;
	uint16_t curTicks;
#ifndef ACLK_USES_LFXT1CLK
	uint64_t curMs;
#endif

	rearmEvent.eventID = E_SWTIMER_REARM;

	// This loop keeps the current time up to date to account
	// for the time taken in processing the interrupt.
	do
	{
		dequeued = false;

#ifndef ACLK_USES_LFXT1CLK
		// Get the current time
		curTicks = timerTicks();
		curMs = CURMS(curTicks);

		// Process events that are in the past
		while(timerQueue && timerQueue->expiryTime < curMs)
#else
		// Get the current time
		curTicks = timerTicks();

		// Process events that are in the past
		while(timerQueue
				&& (timerQueue->expiryRollovers < rolloverCount
					|| (timerQueue->expiryRollovers == rolloverCount && timerQueue->expiryTicks <= curTicks)))
#endif
		{
			dequeued = true;

			tim = timerQueue;
			timerQueue = timerQueue->next;

			tim->isRunning = false;
			tim->next = NULL;
			tim->prev = NULL;
			dispPutEvent(tim->event);



			// Put a message on the queue to re-arm events in the future.
			if(tim->isRecurring)
			{
				// void* in the event to make sure SoftwareTimer is opaque.
				rearmEvent.eventData.swTimerPointer = (void*)tim;
				dispPutEvent(rearmEvent);
			}
		}
	}while(dequeued);

	// The first entry may have moved into that position. Clear the prev pointer.
	timerQueue->prev = NULL;

	// Set the next interrupt.
	setNextTimerInterrupt();

}

static void checkAllTimersForExpiration()
{
	int i;
	SoftwareTimer *tim;

	for(i=0; i<(uint16_t)SWTMR_NUM_TIMERS; i++)
	{
		tim = softwareTimers + i;
		if(tim->isRunning)
		{
#ifndef ACLK_USES_LFXT1CLK
			if(tim->expiryTime < elapsedMs)
#else
			if(tim->expiryRollovers < rolloverCount)
#endif
			{
				logNormal(E_LOG_SW_TIMER_POSTED_MISSED_EVENT, i);
				tim->isRunning = false;
				dispPutEvent(tim->event);
			}
		}
	}
}

static void rearmTimer(SoftwareTimer *tim)
{
	uint16_t curTicks;
	uint16_t interruptSave;
	uint64_t expiryTime;
	uint64_t curTime;
	EVENT rearmEvent;

	// When re-arming. if it is already running, do nothing. This indicates that
	// there was another call to swTimerSet(). If it is not set to be recurring
	// anymore, then it was canceled.
	if(tim->isRunning || !tim->isRecurring)
	{
		return;
	}

	// We want to calculate the new expiry time from the last expiry time to prevent
	// drift in the clock.

	// We have to disable the interrupts just after reading the ticks to prevent a
	// rollover from being processed while inserting the timer. Don't disable before
	// timerTicks() returns because it runs a loop which may have a rollover in it.
	// This would cause the timer to be off by 1 rollover.
	curTicks = timerTicks();
	interruptSave = interruptDisable();

#ifndef ACLK_USES_LFXT1CLK
	expiryTime = tim->expiryTime + tim->durration;
	curTime = CURMS(curTicks);
	tim->expiryTime = expiryTime;

#else
	expiryTime = SWTIMER_PACKTIME(tim->expiryRollovers, tim->expiryTicks);
	expiryTime += tim->durration;

	curTime = SWTIMER_PACKTIME(rolloverCount, curTicks);

	//if the time isn't valid, reset.
	if(!SWTIMER_ISVALIDTIME(expiryTime))
	{
		logNormal(E_LOG_SW_TIMER_RESET, 0);
		resetSystem();
	}

	//we must always unpack this time or once we get behind we will call the timer as fast as possible forever.
	SWTIMER_UNPACKTIME(expiryTime, tim->expiryRollovers, tim->expiryTicks);
#endif

	// if the new expiry time is in the past, raise it now.
	if(expiryTime < curTime)
	{
		//note: tim->isRunning = false;
		dispPutEvent(tim->event);

#ifndef ACLK_USES_LFXT1CLK
		tim->expiryTime = curTime;
#else
		tim->expiryRollovers = rolloverCount;
		tim->expiryTicks = curTicks;
#endif

		rearmEvent.eventID = E_SWTIMER_REARM;
		rearmEvent.eventData.swTimerPointer = (void*)tim;
		dispPutEvent(rearmEvent);
	}
	else
	{
		//otherwise, enqueue it as normal.
		tim->isRunning = true;

		// Put the timer in the queue and setup the interrupt if needed.
		enqueueTimer(tim);
	}

	interruptRestore(interruptSave);
}


TIMEOUT timeoutSet(int16_t ticks)
{
#ifndef ACLK_USES_LFXT1CLK
	//when using vlo, the "ticks" passed into this function are actually ms.
	return (int16_t)(timerTicks() + ((uint32_t)ticks) * lastVloCalibration / 1000ul);
#else
	return (int16_t)timerTicks() + ticks;
#endif
}

bool timeoutIsExpired(TIMEOUT t)
{
	return (int16_t)timerTicks() - t > 0;
}

#ifndef ACLK_USES_LFXT1CLK
unsigned int VLOCLK_measurement(void)
{
  uint16_t count1, count2;
  uint16_t freq;
  uint16_t gie;

  gie = interruptDisable();

  count1 = timerTicks();
  DELAY_MS(10);
  count2 = timerTicks();

  interruptRestore(gie);

  // rollover is not a problem here because the subtraction reverses it. If we had two
  // rollovers durring this measurement it would be a problem, but that should not be
  // possilbe in 10ms. There are 100 10ms intervals in a second.
  freq = (count2 - count1) * 100u;

  //If the result is out of range, pick a reasonable value. TIMERA is driven by ACLK/8.
  if(freq < 4000/8 || freq > 20000/8)
  {
	  freq = 13000/8;
  }

  return(freq);
}

#endif

#ifdef UNIT_TESTING
// ExtendedTimeValue() returns 32-bit extended timer.
// Returns the lower 32 bits of the 48-bit or 64-bit virtual timer.
uint32_t ExtendedTimeValue(void)
{
#ifndef ACLK_USES_LFXT1CLK
    uint64_t curTime;
    uint16_t curTicks;

    // Convert current timer ticks into mSec, and return low 32-bit mSec
    curTicks = timerTicks();
    curTime = CURMS(curTicks);
    return ((uint32_t)curTime);
#else
    uint16_t ticks;
    uint16_t extendedTime;

    // loop to handle 16 bit timer wrap
    do {
        ticks = timerTicks();           // TimerA2 provides low 16 bits of timer
        extendedTime = (uint16_t)rolloverCount; // upper 16 bits of timer
    } while (timerTicks() < ticks);
    return (ticks + ((uint32_t)extendedTime << 16));
#endif
}
#endif // UNIT_TESTING


