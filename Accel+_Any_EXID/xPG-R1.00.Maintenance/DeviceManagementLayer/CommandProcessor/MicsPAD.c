/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: MICS Packet Assembler / Disassembler - MicsPAD
 *	implementation.
 *
 ***************************************************************************/


//#define MICSPAD_PRINT_OUTPUT	// to turn on/off print debug output for Command Processor module
//#define MICSPAD_DEBUG_PKT		// Print out all send and receive packets
#ifdef  MICSPAD_PRINT_OUTPUT
#include <stdio.h>
#endif
#ifdef  MICSPAD_DEBUG_PKT
#include <stdio.h>
#endif

//#define MICSPAD_DEBUG

//Includes
#include <string.h>
#include "MicsPAD.h"
#include "state_machine_common.h"
#include "system_events.h"
#include "log_events.h"
#include "error_code.h"
#include "EventQueue.h"
#include "MultiChannelTimer.h"
#include "log.h"
#include "CommandProcessor.h"
#include "swtmr_application_timers.h"
#include "Common/Protocol/protocol.h"
#include "Common/Protocol/cmndStim.h"
#include "cdp.h"
#include "MICSCommandTable.h"
#include "mics.h"
#include "delay.h"
#include "DeviceDriverLayer/MICS/zl.h"
#include "ChargeManager.h"
#include "timer.h"
#include "log.h"
#include "system.h"


//Define the states of the state machine
typedef enum MICSPAD_STATE
{
	MICSPAD_STATE_INITIAL,		//Before initialization
	MICSPAD_STATE_OPEN,			//MICS Session is open
	MICSPAD_STATE_CLOSED,		//MICS Session is closed
	MICSPAD_STATE_RECEIVING,	//Waiting to receive another packet in a multi-packet message
	MICSPAD_STATE_PENDING_RESP	//Waiting for a response to a MICS Command
} MICSPAD_STATE;


/*Event Handers*/
static void eventHandlerE_INITIALIZE(EVENT event);
static void eventHandlerE_MICS_SESSION_OPEN(EVENT event);
static void eventHandlerE_MICS_SESSION_CLOSE(EVENT event);
static void eventHandlerE_MICS_DATA_BLOCK(EVENT event);
static void eventHandlerE_MICS_TIMEOUT(EVENT event);
static void eventHandlerE_MICS_SEND_FRAME(EVENT event);

/*Helper Functions*/
static void sendInitialBlock(EVENT event);
static void sendIntermediateBlock(void);
static void sendFinalEmptyBlock(void);
static bool isPairedExternal(uint8_t firstByte, uint8_t secondByte, uint8_t thirdByte, CMND_TOKEN token);
static uint8_t micsChecksum(const uint8_t *buf, size_t len);
static bool isUnpairedCommandAccepted(CMND_TOKEN aToken);

/*Debugging*/
#ifdef MICSPAD_DEBUG
void recvCommand(void);
void recvCommand2(void);
char debugPacket[15];
#include "version.h"
#endif

// Define the State Machine Table
static STATE_MACHINE_TABLE_ENTRY const micspadStateTable[] =
{
//   There is one table entry for every event processed in each state
//   CURRENT STATE					EVENT ID				EVENT HANDLER				    	END STATE
//   --------------------------------------------------------------------------------------------------------------------
DISPATCH_GEN_START(micsPADProcessEvent)
	{MICSPAD_STATE_INITIAL,			E_INITIALIZE,			eventHandlerE_INITIALIZE			/* MICSPAD_STATE_CLOSED	*/},
	{MICSPAD_STATE_CLOSED,			E_MICS_SESSION_OPEN,	eventHandlerE_MICS_SESSION_OPEN,	/* MICSPAD_STATE_OPEN	*/},
	{MICSPAD_STATE_OPEN,			E_MICS_SESSION_CLOSED,	eventHandlerE_MICS_SESSION_CLOSE,	/* MICSPAD_STATE_CLOSED */},
	{MICSPAD_STATE_OPEN,			E_MICS_DATA_BLOCK,		eventHandlerE_MICS_DATA_BLOCK,		/* MULTIPLE				*/},
	{MICSPAD_STATE_RECEIVING,		E_MICS_DATA_BLOCK,		eventHandlerE_MICS_DATA_BLOCK,		/* MULTIPLE				*/},
	{MICSPAD_STATE_RECEIVING,		E_MICS_TIMEOUT,			eventHandlerE_MICS_TIMEOUT,			/* MICSPAD_STATE_OPEN	*/},
	{MICSPAD_STATE_PENDING_RESP,	E_MICS_SEND_FRAME,		eventHandlerE_MICS_SEND_FRAME,		/* MICSPAD_STATE_OPEN   */},
DISPATCH_GEN_END
};

// Calculate the state machine table size
#define MICSPAD_EVENT_TABLE_LEN (sizeof(micspadStateTable)/sizeof(micspadStateTable[0]))

//Initialize the state machine
static MICSPAD_STATE micspadCurrentState = MICSPAD_STATE_INITIAL;

//More Private Variables
static uint16_t bytesRemaining = 0;
static uint16_t bytesSent = 0;
static uint16_t bytesReceived = 0;
static uint16_t bytesToReceive = 0;
static bool sendFinalBlock = false;
static uint8_t txCheckSum = 0;
static uint8_t rxCheckSum = 0;
static bool endSessionFlag = false;
static bool firstMessageInSession = true;

#ifdef CMND_PERF_MON
static uint16_t startTimeTotal = 0;
static uint16_t endTimeTotal = 0;
uint16_t startTimeCommand = 0;
uint16_t endTimeCommand = 0;
uint16_t startTimeStartResponse = 0;
static uint16_t endTimeStartResponse = 0;
#endif

#ifndef EPG
static CMND_TOKEN acceptedUnpairedCommands[] =
{
		TKN_GET_XPG_STATUS,
		TKN_GET_PPC_CONST,
		TKN_CHARGING_CONTROL,
		TKN_PAIR_PPC,
		TKN_PAIR_POP
};
static const int numAcceptedUnpairedCommands = sizeof(acceptedUnpairedCommands) / sizeof(CMND_TOKEN);
static bool unpairedCommandsAllowed;
#endif


static unsigned char packetBuffer[15];

//Main Event Handler that processes all subscribed events
void micsPADProcessEvent(EVENT eventToProcess)
{
	int i = 0;
	EVENT_ID eventId = eventToProcess.eventID;

	#ifdef MICSPAD_PRINT_OUTPUT
		printf("MicsPAD - ProcessEvent\n");
	#endif

	// Iterate through the state machine table until find the current state.
	for(i=0; i < MICSPAD_EVENT_TABLE_LEN; i++)
	{
		if((micspadStateTable[i].state == micspadCurrentState)  && micspadStateTable[i].eventId == eventId)
		{
			break;
		}
	}

	//Now call the appropriate event handler
	if(i != MICSPAD_EVENT_TABLE_LEN)
	{
		micspadStateTable[i].eventHandler(eventToProcess);
	}

	return;
}

/*
 *
 * EVENT HANDLER FUNCTIONS
 *
 */

/*Event Handers*/
//Transition function for E_INITIALIZE event
static void eventHandlerE_INITIALIZE(EVENT event)
{
	(void) event;	// Parameter not used

#ifdef MICSPAD_PRINT_OUTPUT
	printf("MicsPAD - eventHandlerE_INITIALIZE\n");
#endif

	micspadCurrentState = MICSPAD_STATE_CLOSED;

#ifdef MICSPAD_DEBUG
	recvCommand2();
#endif

	return;
}

//Transition function for E_MICS_SESSION_OPEN event
static void eventHandlerE_MICS_SESSION_OPEN(EVENT event)
{
#ifdef MICSPAD_PRINT_OUTPUT
	printf("MicsPAD - eventHandlerE_MICS_SESSION_OPEN\n");
#endif

	(void)event;

#ifndef EPG
	//disable unpaired commands. This is re-enabled when we first try to use an unpaired command if allowed.
	unpairedCommandsAllowed = false;
#endif

	//Reset some statistics.
	bytesRemaining = 0;
	bytesSent = 0;
	txCheckSum = 0;
	rxCheckSum = 0;
	firstMessageInSession = true;

	//Log the connection and associate channel
	logNormal(E_LOG_MICS_CONNECTION,zl_GetWakeupChannel());

	micspadCurrentState = MICSPAD_STATE_OPEN;

	return;
}

//Transition function for E_MICS_SESSION_CLOSE event
static void eventHandlerE_MICS_SESSION_CLOSE(EVENT event)
{
#ifdef MICSPAD_PRINT_OUTPUT
	printf("MicsPAD - eventHandlerE_MICS_SESSION_CLOSE\n");
#endif
	(void)event;

	//Reset some statistics.
	bytesRemaining = 0;
	bytesSent = 0;
	txCheckSum = 0;
	rxCheckSum = 0;

	micspadCurrentState = MICSPAD_STATE_CLOSED;

	return;
}

//Transition function for E_MICS_DATA_BLOCK event
static void eventHandlerE_MICS_DATA_BLOCK(EVENT event)
{

	#ifdef MICSPAD_PRINT_OUTPUT
		printf("MicsPAD - eventHandlerE_MICS_DATA_BLOCK.\n");
	#endif

	#ifdef MICSPAD_DEBUG_PKT
			int i;
			printf("mrecv<");
			for (i=0;i<15;i++) {
				printf(" %02x ",event.eventData.micsDataBlock[i] );
			}
			printf("\n");
	#endif


	//First ensure you have the correct Command Token Block
	//If this is the first block, bytesReceived=0, and the ctb=0x01
	//If this is a subsequent block, ctb=0x00.
	if (event.eventData.micsDataBlock[0] == COMMAND_TOKEN_BLOCK && (bytesReceived != 0))
	{
		//If we get in here, then we have received a new command, when expecting a data block...reset data
		#ifdef MICSPAD_PRINT_OUTPUT
			printf("MicsPAD - Received a new command when expecting data block.\n");
		#endif

		micspadCurrentState = MICSPAD_STATE_OPEN;
		bytesReceived = 0;
		bytesToReceive = 0;
		rxCheckSum = 0;

		#ifdef VERBOSE_MICS_LOGGING
			logNormal(E_LOG_MICS_PACKET_DUMPED, 1);
		#endif

	}
	else if (event.eventData.micsDataBlock[0] == INTERMEDIATE_BLOCK && (bytesReceived == 0))
	{
		//If we get in here, then we received a data block, when we were expecting a new command
		#ifdef MICSPAD_PRINT_OUTPUT
			printf("MicsPAD - Received a data block when expecting a new command.\n");
		#endif

		micspadCurrentState = MICSPAD_STATE_OPEN;
		bytesReceived = 0;
		bytesToReceive = 0;
		rxCheckSum = 0;

		#ifdef VERBOSE_MICS_LOGGING
			logNormal(E_LOG_MICS_PACKET_DUMPED, 2);
		#endif

		return;
	}

	//Now set up to read the block
	//Read the First Block - Command / Data
	if (event.eventData.micsDataBlock[0] == COMMAND_TOKEN_BLOCK)
	{
		//Cast a pointer to easily access the data in the packet
		INITIAL_MICS_RX_PACKET* initialPacket = (INITIAL_MICS_RX_PACKET*)(event.eventData.micsDataBlock);
		uint8_t ctbChecksum;

		#ifdef CMND_PERF_MON
			startTimeTotal = timerTicks();
			startTimeCommand = timerTicks();
		#endif

		// First check to see we have a valid external
		// NOTE: isPairdExternal also performs the re-pairing if it is armed.
		if (!isPairedExternal(initialPacket->externalId1stByte, initialPacket->externalId2ndByte, initialPacket->externalId3rdByte, (CMND_TOKEN)initialPacket->cmndToken)
				&& !isUnpairedCommandAccepted((CMND_TOKEN)initialPacket->cmndToken))
		{
			EVENT sendFrameEvent;

			//If we get in here, then we received a data block, when we were expecting a new command
			#ifdef MICSPAD_PRINT_OUTPUT
				printf("MicsPAD - Received a command from an invalid external.\n");
			#endif

			//Just reset and ignore commands from external we can't talk to
			micspadCurrentState = MICSPAD_STATE_PENDING_RESP; //so we don't drop the response.
			bytesReceived = 0;
			bytesToReceive = 0;

			sendFrameEvent.eventID = E_MICS_SEND_FRAME;
			sendFrameEvent.eventData.eCommandResponseData.commandSource = MICS;
			sendFrameEvent.eventData.eCommandResponseData.responseCode = CMND_RESP_INVALID_EXID;
			sendFrameEvent.eventData.eCommandResponseData.commandToken = (CMND_TOKEN)initialPacket->cmndToken;
			sendFrameEvent.eventData.eCommandResponseData.responseSize = 0;

			endSessionFlag = true;

			dispPutEvent(sendFrameEvent);

			#ifdef VERBOSE_MICS_LOGGING
				logNormal(E_LOG_MICS_INVALID_EXID, 0);
			#endif

			return;
		}
		else if (firstMessageInSession && initialPacket->externalId3rdByte == 0xff)
		{
			//Reset flag to stop logging connection
			firstMessageInSession = false;

			//Log the connection
			logNormal(E_LOG_CP_CONNECTED, BUILD_LOG_DATA(initialPacket->externalId1stByte, initialPacket->externalId2ndByte, initialPacket->externalId3rdByte,0));
		}

		//Check for a valid token...
		if (initialPacket->cmndToken >= numCmndTableEntries)
		{
			EVENT sendFrameEvent;

			//If we get in here, then we got a bad event.
			#ifdef MICSPAD_PRINT_OUTPUT
				printf("MicsPAD - Received an invalid event.\n");
			#endif


			micspadCurrentState = MICSPAD_STATE_PENDING_RESP; //so we don't drop the response.

			sendFrameEvent.eventID = E_MICS_SEND_FRAME;
			sendFrameEvent.eventData.eCommandResponseData.commandSource = MICS;
			sendFrameEvent.eventData.eCommandResponseData.responseCode = CMND_RESP_INVALID_CMND;
			sendFrameEvent.eventData.eCommandResponseData.commandToken = (CMND_TOKEN)initialPacket->cmndToken;
			sendFrameEvent.eventData.eCommandResponseData.responseSize = 0;

			dispPutEvent(sendFrameEvent);

			#ifdef VERBOSE_MICS_LOGGING
				logNormal(E_LOG_MICS_INVALID_TOKEN, 0);
			#endif

			return;
		}

		//Check for valid checksum in first packet
		ctbChecksum = micsChecksum((uint8_t*)initialPacket,14);

		if (ctbChecksum != initialPacket->checkSum)
		{
			#ifdef MICSPAD_PRINT_OUTPUT
				printf("MicsPAD - CTB checksum is bad - computed=%d, received=%d.\n", ctbChecksum, initialPacket->checkSum);
			#endif

			#ifdef VERBOSE_MICS_LOGGING
				logNormal(E_LOG_MICS_CHECKSUM_FAILED, 0);
			#endif

			return;
		}

		//Get the total amount data bytes to go get...
		bytesToReceive = cmndTable[initialPacket->cmndToken].dataSize;

		#ifdef MICSPAD_DEBUG_PKT
			printf("MicsPAD - Command Token = %02x, Data BytesToRecv = %d.\n", initialPacket->cmndToken, bytesToReceive);
		#endif

		//Clear the buffer before you start writing to it...
		memset(&micsRxBuffer, 0, sizeof(micsRxBuffer));

		//Now go copy the command token and databuffer => token = 2 bytes, data buffer = 8 bytes
		micsRxBuffer.token = (CMND_TOKEN)initialPacket->cmndToken;

		//Determine how many bytes to go get...
		if (bytesToReceive > CTB_BLOCK_DATA_SIZE)
		{
			bytesReceived = CTB_BLOCK_DATA_SIZE;
			bytesRemaining = bytesToReceive - bytesReceived;
			rxCheckSum = micsChecksum((uint8_t*)initialPacket,15);
		}
		else
		{
			bytesReceived = bytesToReceive;
			rxCheckSum = 0;
		}

		//Now copy it into the buffer;
		if (bytesReceived > 0)
		{
			memcpy(((char*)&micsRxBuffer.rxData), &(initialPacket->dataBlock[0]), bytesReceived);
		}


		//Now see how much is left to copy.  
		if (bytesReceived >= bytesToReceive)
		{
			// If the command is PAIR_PPC, then we must handle that here. This is a special case because
			// we do not have access to the EXID outside of MicsPad.
			if(micsRxBuffer.token == TKN_PAIR_PPC)
			{

				EVENT sendFrameEvent;
				EXID_BYTES exid;

				exid.lsb = initialPacket->externalId1stByte;
				exid.mid = initialPacket->externalId2ndByte;
				exid.msb = initialPacket->externalId3rdByte;

				#ifndef EPG
					if(!(isIPG() && chrgMgrIsVrectPresent()))
					{
						sendFrameEvent.eventData.eCommandResponseData.responseCode = RESP_FEATURE_NOT_ENABLED;
					}
					else if(initialPacket->externalId3rdByte == 0xFF) //it is a CP
					{
						sendFrameEvent.eventData.eCommandResponseData.responseCode = CMND_RESP_INVALID_PARAM;
					}
					else
					{
						cdp_SetExid(SOURCE_PPC, &exid, true);
						logNormal(E_LOG_PAIRING_CHANGED_PPC,0);
						sendFrameEvent.eventData.eCommandResponseData.responseCode = GEN_SUCCESS;
					}

					sendFrameEvent.eventID = E_MICS_SEND_FRAME;
					sendFrameEvent.eventData.eCommandResponseData.commandSource = MICS;
					sendFrameEvent.eventData.eCommandResponseData.commandToken = TKN_PAIR_PPC;
					sendFrameEvent.eventData.eCommandResponseData.responseSize = 0;

					dispPutEvent(sendFrameEvent);
				#else
					if(initialPacket->externalId3rdByte == 0xFF) //it is a CP
					{
						sendFrameEvent.eventData.eCommandResponseData.responseCode = CMND_RESP_INVALID_PARAM;
					}
					else
					{
						cdp_SetExid(SOURCE_PPC, &exid, true);
						logNormal(E_LOG_PAIRING_CHANGED_PPC,0);
						sendFrameEvent.eventData.eCommandResponseData.responseCode = GEN_SUCCESS;
					}

					sendFrameEvent.eventID = E_MICS_SEND_FRAME;
					sendFrameEvent.eventData.eCommandResponseData.commandSource = MICS;
					sendFrameEvent.eventData.eCommandResponseData.commandToken = TKN_PAIR_PPC;
					sendFrameEvent.eventData.eCommandResponseData.responseSize = 0;

					dispPutEvent(sendFrameEvent);
				#endif
			}
			else
			{
				//Send and event to say the message is ready
				EVENT sendEvent;
				sendEvent.eventID = E_MICS_CMD;
				dispPutEvent(sendEvent);

				#ifdef VERBOSE_MICS_LOGGING
					//logNormal(E_LOG_MICS_COMMAND_RECEIVED, 0);
				#endif
			}

			//Reset state and state variables
			micspadCurrentState = MICSPAD_STATE_PENDING_RESP;
			bytesReceived = 0;
			bytesToReceive = 0;
		}
		else
		{
			//Set the new state...
			micspadCurrentState = MICSPAD_STATE_RECEIVING;
		}
	} //End the conditional block if we have a first block
	else if (*event.eventData.micsDataBlock == INTERMEDIATE_BLOCK)
	{
		//Set up some pointers
		//Read the Middle Block - Command / Intermediate Data
		if (bytesRemaining >= INT_BLOCK_DATA_SIZE)
		{
			MIDDLE_MICS_RX_PACKET* middlePacket = (MIDDLE_MICS_RX_PACKET*)(event.eventData.micsDataBlock);

			//Now copy it into the buffer;
			memcpy(((char*)&micsRxBuffer.rxData) + bytesReceived, &(middlePacket->dataBlock[0]), INT_BLOCK_DATA_SIZE);

			//Set the statistics...
			bytesReceived = bytesReceived + INT_BLOCK_DATA_SIZE;
			bytesRemaining = bytesRemaining - INT_BLOCK_DATA_SIZE;

			//Keep gathering checksum
			rxCheckSum += micsChecksum((uint8_t*)middlePacket,15);
		}
		else
		{
			FINAL_MICS_RX_PACKET* finalPacket = (FINAL_MICS_RX_PACKET*)(event.eventData.micsDataBlock);
			EVENT sendEvent;

			//Now copy it into the buffer;
			if (bytesRemaining > 0)
			{
				memcpy(((char*)&micsRxBuffer.rxData) + bytesReceived, &(finalPacket->dataBlock[0]), bytesRemaining);
			}

			//Keep gathering checksum
			rxCheckSum += micsChecksum((uint8_t*)finalPacket,14);

			if (rxCheckSum == finalPacket->checksum)
			{
				//Send and event to say the message is ready
				sendEvent.eventID = E_MICS_CMD;
				dispPutEvent(sendEvent);
				micspadCurrentState = MICSPAD_STATE_PENDING_RESP;

			#ifdef VERBOSE_MICS_LOGGING
				//logNormal(E_LOG_MICS_COMMAND_RECEIVED, 0);
			#endif
			}
			else
			{
				micspadCurrentState = MICSPAD_STATE_OPEN;
				#ifdef MICSPAD_PRINT_OUTPUT
					printf("MicsPAD - tChkSum checksum is bad - calculated=%d, received=%d.\n", rxCheckSum, finalPacket->checksum);
				#endif
			}

			//Reset state and state variables
			bytesReceived = 0;
			bytesToReceive = 0;
			rxCheckSum = 0;
		}
	}

	return;
}

//Transition function for E_MICS_TIMEOUT event
static void eventHandlerE_MICS_TIMEOUT(EVENT event)
{
#ifdef MICSPAD_PRINT_OUTPUT
	printf("MicsPAD - eventHandlerE_MICS_TIMEOUT\n");
#endif
	(void)event;

	//Reset state and state variables
	micspadCurrentState = MICSPAD_STATE_OPEN;
	bytesReceived = 0;
	bytesToReceive = 0;
	rxCheckSum = 0;
	bytesSent = 0;
	bytesRemaining = 0;
	txCheckSum = 0;

	return;
}

//Transition function for E_MICS_SEND_FRAM
static void eventHandlerE_MICS_SEND_FRAME(EVENT event)
{
	#ifdef CMND_PERF_MON
		endTimeStartResponse = timerTicks();
	#endif

	//Cancel any wait timers...
	swTimerCancel(SWTMR_MICS_RXTX_TIMEOUT);

	//Reset how many sent
	bytesSent = 0;

	//Figure out how many total bytes to send
	bytesRemaining = event.eventData.eCommandResponseData.responseSize;

	//Send the initial block out
	sendInitialBlock(event);

	//Send any intermediate packets...
	while (bytesRemaining > 0)
	{
		sendIntermediateBlock();
	}

	//If there is a empty final block remaining send it out
	if (sendFinalBlock)
	{
		sendFinalEmptyBlock();
	}

	//Reset the state machine and variables...
	bytesSent = 0;
	bytesRemaining = 0;
	txCheckSum = 0;
	micspadCurrentState = MICSPAD_STATE_OPEN;

	#ifdef VERBOSE_MICS_LOGGING
		//logNormal(E_LOG_MICS_RESPONSE_SENT, 1);
	#endif

	if (endSessionFlag)
	{
		micsWaitForTxComplete();
		micsDisconnect();
		endSessionFlag = false;

		#ifdef VERBOSE_MICS_LOGGING
			logNormal(E_LOG_MICS_SESSION_DUMPED, 1);
		#endif
	}

	#ifdef CMND_PERF_MON
		endTimeTotal = timerTicks();

		if((endTimeTotal - startTimeTotal) > 0)
		{
		   DBGI(f_cmdproc_micspad, endTimeTotal - startTimeTotal);
		}

		if((endTimeStartResponse - startTimeStartResponse) > 0)
		{
		   DBGI(f_cmdproc_micspad, endTimeStartResponse - startTimeStartResponse);
		}
	#endif

	return;
}

/*
 * Helper Functions
 */

//Sends the initial response data
static void sendInitialBlock(EVENT event)
{
	uint8_t sendSize;

	//Clear the packet buffer of any stale data
	memset(&packetBuffer,0,sizeof(packetBuffer));

	#ifdef MICSPAD_PRINT_OUTPUT
		printf("MicsPAD - sendInitialBlock\n");
	#endif
		
	//Set first block header
	packetBuffer[0] = RESPONSE_CODE_BLOCK;	//Set Response Code Block
	packetBuffer[1] = event.eventData.eCommandResponseData.responseCode;

	#ifdef MICSPAD_DEBUG_PKT
		printf("MicsPAD - Response Code = %02x, Data BytesToSend = %d.\n", event.eventData.eCommandResponseData.responseCode,
																		   event.eventData.eCommandResponseData.responseSize);
	#endif

	packetBuffer[2] = (uint8_t)((event.eventData.eCommandResponseData.commandToken ) & 0xFF);
	packetBuffer[3] = (uint8_t)((event.eventData.eCommandResponseData.commandToken >>8 )  & 0xFF);


	//Determine how much data to put in
	if (bytesRemaining <= RCB_BLOCK_DATA_SIZE)
	{
		sendSize = bytesRemaining;
	}
	else
	{
		sendSize = RCB_BLOCK_DATA_SIZE;
	}

	//Copy the data in, skipping 4 bytes for the header
	memcpy(&packetBuffer[RCB_BLOCK_DATA_OFFSET], &micsTxBuffer.txData, sendSize);

	//Write out the checksum for the first block
	packetBuffer[14] = micsChecksum(&packetBuffer[0],14);

	#ifdef MICSPAD_DEBUG_PKT
		{
		int i = 0;
		printf("msend>");
		for (i=0;i<15;i++)
		{
			printf(" %02x ", packetBuffer[i]);
		}
		printf("\n");
		}
	#endif

	#ifdef MICSPAD_PRINT_OUTPUT
		printf("First packet checksum=%x.\n", micsChecksum(&packetBuffer[0],14));
	#endif

	//Keep a running total of the entire checksum
	txCheckSum = micsChecksum(&packetBuffer[0],15);

	//Make synchronous call to send buffer
	micsSendPacket(packetBuffer, 15);

	//Set the data to the amount remaining
	if (bytesRemaining <= RCB_BLOCK_DATA_SIZE)
	{
		bytesRemaining = 0;
	}
	else
	{
		bytesRemaining = bytesRemaining - RCB_BLOCK_DATA_SIZE;
	}

	bytesSent = RCB_BLOCK_DATA_SIZE;

	return;
}

//Sends intermediate packets
static void sendIntermediateBlock(void)
{
#ifdef MICSPAD_PRINT_OUTPUT
	printf("MicsPAD - sendIntermediateBlock\n");
#endif

	//Clear the packet buffer of any stale data
	memset(&packetBuffer,0,sizeof(packetBuffer));
	
	//Set the RCB to something other than first block
	packetBuffer[0] = RC_INTERMEDIATE_BLOCK;

	if (bytesRemaining < RC_INT_BLOCK_DATA_SIZE)
	{
		//Send out the remaining data
		memcpy(&packetBuffer[1], (char*)(&micsTxBuffer.txData) + bytesSent, bytesRemaining);

		//Compute the final checksum and write it into the packet
		txCheckSum += micsChecksum(&packetBuffer[0], 15);
		packetBuffer[14]=txCheckSum;

		micspadCurrentState = MICSPAD_STATE_OPEN;
		bytesSent = 0;
		bytesRemaining = 0;
		txCheckSum = 0;
	}
	else if (bytesRemaining >= RC_INT_BLOCK_DATA_SIZE)
	{
		//Send out a middle block full of data
		memcpy(&packetBuffer[1], (char*)(&micsTxBuffer.txData) + bytesSent, RC_INT_BLOCK_DATA_SIZE);

		//Figure out what's left and what's been sent
		bytesRemaining = bytesRemaining - RC_INT_BLOCK_DATA_SIZE;
		bytesSent = bytesSent + RC_INT_BLOCK_DATA_SIZE;
		txCheckSum += micsChecksum(&packetBuffer[0], 15);

		//Handles the case where we have to send out one more block for data crc
		if (bytesRemaining == 0)
		{
			sendFinalBlock = true;
		}
	}

	#ifdef MICSPAD_DEBUG_PKT
	{
		int i = 0;
		printf("msend>");
		for (i=0;i<15;i++)
		{
			printf(" %02x ", packetBuffer[i]);
		}
		printf("\n");
	}
	#endif

	//Make synchronous call to send buffer
	micsSendPacket(packetBuffer, 15);

	return;
}

//Sends intermediate packets
static void sendFinalEmptyBlock(void)
{
	#ifdef MICSPAD_PRINT_OUTPUT
		printf("MicsPAD - sendFinalEmptyBlock\n");
	#endif

	//Clear the buffer
	memset(packetBuffer, 0, RC_INT_BLOCK_DATA_SIZE);

	//Compute the final checksum and write it into the packet
	packetBuffer[14]=txCheckSum;

	#ifdef MICSPAD_DEBUG_PKT
	{
		int i = 0;
		printf("msend>");
		for (i=0;i<15;i++)
		{
			printf(" %02x ", packetBuffer[i]);
		}
		printf("\n");
	}
	#endif

	//Make synchronous call to send buffer
	micsSendPacket(packetBuffer, 15);

	sendFinalBlock = false;

	return;
}



//Determine if we are getting a command from a valid external
static bool isPairedExternal(uint8_t firstByte, uint8_t secondByte, uint8_t thirdByte, CMND_TOKEN token)
{
	bool returnStatus = false;
	EXID_BYTES exid;
	COMMAND_SOURCE cmndSrc;

	exid.lsb = firstByte;
	exid.mid = secondByte;
	exid.msb = thirdByte;

	cmndSrc = cdp_IdentifyExid(&exid);

	//Return true if anything except unknown
	if (cmndSrc != SOURCE_UNKNOWN)
	{
		returnStatus = true;
	}
	else if (getArmForPOPPairing())
	{
		if (token == TKN_GET_XPG_STATUS  || token == TKN_GET_POP_CONST)
		{
			if (getArmForPOPPairing())
			{
				// Set the EXID in the data store
				//
				// Shift the old POP1 pairing into POP2, so that this mechanism
				// can be used to add a pairing to an existing pairing or to
				// pair two POPs to a single xPG.
				cdp_SetExid(SOURCE_POP2, cdp_GetExid(SOURCE_POP1), cdp_IsExidEnabled(SOURCE_POP1));
				cdp_SetExid(SOURCE_POP1, &exid, true);

				//Log that the pairing changed
				logNormal(E_LOG_PAIRING_CHANGED_PPC,0);

				returnStatus = true;
			}
		}
		else
		{
			returnStatus = false;
		}
	}
	return returnStatus;
}

//Compute the checksum for a buffer
static uint8_t micsChecksum(const uint8_t *buf, size_t len)
{
	uint8_t sum;

	sum = 0;

	while (len-- > 0)
	{
		sum += *buf++;
	}

	return sum;
}

//Public API - Only to be used in special circumstances
void sendQuickMicsResponse(RESPONSE response, CMND_TOKEN token)
{
	memset(&packetBuffer[0],0,sizeof(packetBuffer));

	//Set first block header
	packetBuffer[0] = RESPONSE_CODE_BLOCK;	//Set Response Code Block
	packetBuffer[1] = response;

	#ifdef MICSPAD_DEBUG_PKT
		printf("MicsPAD - Response Code = %02x, Data BytesToSend = %d.\n", response, token);
	#endif

	packetBuffer[2] = (uint8_t)((token ) & 0xFF);
	packetBuffer[3] = (uint8_t)((token >>8 )  & 0xFF);

	//Keep a running total of the entire checksum
	packetBuffer[14] = micsChecksum(&packetBuffer[0],15);

	//Make synchronous call to send buffer
	micsSendPacket(packetBuffer, 15);

	//Allow the packet to be sent
	DELAY_MS(250);

	//Reset state
	micspadCurrentState = MICSPAD_STATE_OPEN;
}

static bool isUnpairedCommandAccepted(CMND_TOKEN aToken)
{
#ifndef EPG
	int cmdidx;

	if(!isIPG())
	{
		return false;
	}

	//Is this command allowed?
	for(cmdidx = 0;
			cmdidx < numAcceptedUnpairedCommands && acceptedUnpairedCommands[cmdidx] != aToken;
			cmdidx++){}

	if(cmdidx >= numAcceptedUnpairedCommands)
	{
		return false;
	}

	// Are the commands allowed at all?
	if(unpairedCommandsAllowed)
	{
		return true;
	}
	else if(chrgMgrIsChargerEngaged())
	{
		unpairedCommandsAllowed = true;
		return true;
	}
	else
	{
		return false;
	}
#else
	return false;
#endif
}
