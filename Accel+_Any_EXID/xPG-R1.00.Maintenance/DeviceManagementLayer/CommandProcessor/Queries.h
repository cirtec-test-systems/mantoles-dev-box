/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: Command Processor Unit.  Queries from external.
 *
 ***************************************************************************/

#ifndef QUERIES_H_
#define QUERIES_H_

/// \defgroup queries	Queries
/// \ingroup commandProcessor
/// @{

void cmndHndlrGetXpgStatus(void);
void cmndHndlrReadLog(void);
void cmndHndlrGetPpcConst(void);
void cmndHndlrGetPgrmNames(void);
void cmndHndlrGetPgrmConst(void);
void cmndHndlrGetxPgIdentity(void);
void cmndHndlrGetGenCal(void);
void cmndHndlrGetCounter(void);
void cmndHndlrEchoShort(void);
void cmndHndlrEchoLong(void);
void cmndHndlrGetChannelCals(void);
void cmndHndlrGetStimHvCals(void);
void cmndHndlrGetTrimList(void);
void cmndHndlrGetTestStim(void);
void cmndHndlrGetDiagData(void);
void cmndHndlrGetPrgmDef(void);
void cmndHndlrGetPulseConst(void);
void cmndHndlrGetCfgDevParams(void);
void cmndHndlrGetPulseWidths(void);
void cmndHndlrGetPrgmFreq(void);
void cmndHndlrGetPopConst(void);
void cmndHndlrGetLeadLimits(void);
void cmndHndlrGetCpData(void);
void cmndHndlrGetRampTime(void);
void cmndHndlrGetVBAT(void);
void cmndHndlrGetTitrStim(void);
void cmndHndlrReadMemory(void);
void cmndHndlrGetBgImpedance(void);
void cmndHndlrGetLogRange(void);

/// @}

#endif /* QUERIES_H_ */
