/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: Command Processor Unit.  Setters/Mutators from externals
 *
 ***************************************************************************/

#include "Mutators.h"

//#define CMDTBL_PRINT_OUTPUT	// to turn on/off print debug output for Command Processor module
#ifdef  CMDTBL_PRINT_OUTPUT
#include <stdio.h>
#endif

#include <string.h>

#include "activePgm.h"
#include "bgImp.h"
#include "cal.h"
#include "cdp.h"
#include "CommandProcessor.h"
#include "const.h"
#include "counter.h"
#include "cpdata.h"
#include "dataStoreXpgIdentity.h"
#include "GenericProgram.h"
#include "log.h"
#include "mics.h"
#include "MICSCommandTable.h"
#include "NV.h"
#include "pgm.h"
#include "PowerASIC.h"
#include "Stim.h"
#include "StimManager.h"
#include "trim.h"
#include "version.h"
#include "xpginfo.h"
#include "ChargeManager.h"
#include "flash.h"

#include <intrinsics.h>
#include "Common/Protocol/cmndMicsOptions.h"


//Handler for TKN_APPEND_LOG
void cmndHndlrAppendLog(void)
{
	#ifdef CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrAppendLog\n");
	#endif

	if (micsRxBuffer.rxData.appendLogData.logNum > 1)
	{
		sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_APPEND_LOG, 0);

		return;
	}

	if (micsRxBuffer.rxData.appendLogData.logNum == 0)
	{
		logError((LOG_EVENT_ID)micsRxBuffer.rxData.appendLogData.event, *(uint32_t*)micsRxBuffer.rxData.appendLogData.eventData);
	}
	else
	{
		logNormal((LOG_EVENT_ID)micsRxBuffer.rxData.appendLogData.event, *(uint32_t*)micsRxBuffer.rxData.appendLogData.eventData);
	}

	micsTxBuffer.txData.appendLogResponseData.firstSerial = logGetFirstSerialNum(micsRxBuffer.rxData.appendLogData.logNum);

	sendCommandResponse(GEN_SUCCESS, TKN_APPEND_LOG, sizeof(APPEND_LOG_RESPONSE_DATA));
}


//Command for TKN_SET_XPG_IDENTITY
void cmndHndlrSetXpgIdentity(void)
{
	DS_XPG_IDENTITY dsXpgIdentity;
	RESPONSE response;

#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrSetXpgIdentity.\n");
#endif

	//Check for Non-Zero Company ID
	if (micsRxBuffer.rxData.xpgIdentity.xpgMicsId.micsIDCompanyId == 0x0)
	{
		sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_SET_XPG_IDENT, 0);
		return;
	}

	//Check for Non-Zero MICS ID
	if (micsRxBuffer.rxData.xpgIdentity.xpgMicsId.micsIDlsb == 0 &&
		micsRxBuffer.rxData.xpgIdentity.xpgMicsId.micsIDmiddleByte == 0 &&
		micsRxBuffer.rxData.xpgIdentity.xpgMicsId.micsIDmsb == 0)
	{
		sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_SET_XPG_IDENT, 0);
		return;
	}

	//Ensure no garbage data in structure
	memset(&dsXpgIdentity, 0, sizeof(DS_XPG_IDENTITY));

	logNormal(E_LOG_SET_XPG_IDENTITY,0);

	//Set MICS ID
	dsXpgIdentity.xpgIdentity.xpgMicsId.micsIDCompanyId = micsRxBuffer.rxData.xpgIdentity.xpgMicsId.micsIDCompanyId;
	dsXpgIdentity.xpgIdentity.xpgMicsId.micsIDlsb = micsRxBuffer.rxData.xpgIdentity.xpgMicsId.micsIDlsb;
	dsXpgIdentity.xpgIdentity.xpgMicsId.micsIDmiddleByte = micsRxBuffer.rxData.xpgIdentity.xpgMicsId.micsIDmiddleByte;
	dsXpgIdentity.xpgIdentity.xpgMicsId.micsIDmsb = micsRxBuffer.rxData.xpgIdentity.xpgMicsId.micsIDmsb;

	//Set Model Number
	memcpy(dsXpgIdentity.xpgIdentity.xpgModelNumber, micsRxBuffer.rxData.xpgIdentity.xpgModelNumber, sizeof(dsXpgIdentity.xpgIdentity.xpgModelNumber));

	//Set Serial Number
	memcpy(dsXpgIdentity.xpgIdentity.xpgSerialNumber, micsRxBuffer.rxData.xpgIdentity.xpgSerialNumber, sizeof(dsXpgIdentity.xpgIdentity.xpgSerialNumber));

	//Write firmware version back in as well...
	memcpy(dsXpgIdentity.firmwareVersion, FIRMWARE_VERSION, sizeof(dsXpgIdentity.firmwareVersion));

	//Now commit it to memory
	response = setXpgInfo(&dsXpgIdentity);

	sendCommandResponse(response, TKN_SET_XPG_IDENT, 0);
}

//Handler for TKN_SET_GEN_CAL
void cmndHndlrSetGenCal(void)
{
	uint8_t i = 0;
	RESPONSE response;

#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrSetGenCal.\n");
#endif

	logNormal(E_LOG_SET_GENERAL_CALIBRATION,0);

	for (i = 0; i < 15; i++)
	{
		if (micsRxBuffer.rxData.genCals.tempStateCal[i] > 4095 || micsRxBuffer.rxData.genCals.powerAsicTempStateCal[i] > 4095)
		{
			sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_SET_GEN_CAL, 0);
			return;
		}
	}

	for (i = 0; i < 6; i++)
	{
		if (micsRxBuffer.rxData.genCals.batteryStateCalStimOff[i] > 4095 || micsRxBuffer.rxData.genCals.batteryStateCalStimOn[i] > 4095)
		{
			sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_SET_GEN_CAL, 0);
			return;
		}
	}

	if (micsRxBuffer.rxData.genCals.rTarget > 4095)
	{
		sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_SET_GEN_CAL, 0);
		return;
	}

	response =  calSetGenCals(&micsRxBuffer.rxData.genCals);

	sendCommandResponse(response, TKN_SET_GEN_CAL, 0);
}


//Hander for TKN_SET_COUNTER
void cmndHndlrSetCounter(void)
{
	RESPONSE retVal;

#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrSetCounter.\n");
#endif

	switch(micsRxBuffer.rxData.counterParams.eventCounterID)
	{
		case COUNTER_PRECHARGE_COMPLETE:
			logNormal(E_LOG_RESET_CURRENT_TO_VOLT_CNTR,0);
			break;
		case COUNTER_CHARGE_COMPLETE:
			logNormal(E_LOG_RESET_CHARGE_COMPLETE_CNTR,0);
			break;
		case COUNTER_STORAGE_MODE:
			logNormal(E_LOG_RESET_STORAGE_MODE_CNTR,0);
			break;
	}

	//Check to see if you have a valid counter id
	if (!counterRangeCheckSetCounter(&micsRxBuffer.rxData.setCounterData))
	{
		sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_SET_COUNTER, 0);
		return;
	}

	//Write the counter value
	retVal = counterCmdSetCounter(&micsRxBuffer.rxData.setCounterData);

	//Send Response
	sendCommandResponse(retVal, TKN_SET_COUNTER, 0);
}


//Handler for TKN_SET_CHANNEL_CALS
void cmndHndlrSetChannelCals(void)
{
	RESPONSE response;
	uint8_t i = 0;

	#ifdef  CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrSetChannelCals.\n");
	#endif

	logNormal(E_LOG_SET_CHANNEL_CALIBRATION,0);

	if (micsRxBuffer.rxData.channelCalParams.index >= NUM_CHANNEL_CAL_TABLES)
	{
		sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_SET_CHANNEL_CALS, 0);
		return;
	}

	for (i = 0; i < NUM_CHANNELS; i++)
	{
		if (micsRxBuffer.rxData.channelCalParams.channelCalSinking[i] > MAX_CHANNEL_CAL || micsRxBuffer.rxData.channelCalParams.channelCalSinking[i] == 0)
		{
			sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_SET_CHANNEL_CALS, 0);
			return;
		}

		if (micsRxBuffer.rxData.channelCalParams.channelCalSourcing[i] > MAX_CHANNEL_CAL || micsRxBuffer.rxData.channelCalParams.channelCalSourcing[i] == 0)
		{
			sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_SET_CHANNEL_CALS, 0);
			return;
		}
	}

	response = calSetChannelCals(&micsRxBuffer.rxData.channelCalParams);

	sendCommandResponse(response, TKN_SET_CHANNEL_CALS, 0);
}


//Handler for TKN_SET_STIM_ASIC_HV_CALS
void cmndHndlrSetStimHvCals(void)
{
	RESPONSE response;
	uint8_t i;
	uint16_t previousValue = 0;

	#ifdef  CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrSetStimHvCals.\n");
	#endif

	logNormal(E_LOG_SET_HV_CALIBRATION,0);

	//Ensure the values are in descending order and within range...
	for (i = 0; i < NUM_STIM_ASIC_HV_SETTINGS; i++)
	{
		if (micsRxBuffer.rxData.hvCalParams.stimAsicHVmVOutput[i] > MAX_STIM_ASIC_HV_CAL)
		{
			sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_SET_STIM_ASIC_HV_CALS, 0);
			return;
		}

		if (micsRxBuffer.rxData.hvCalParams.stimAsicHVmVOutput[i] < previousValue)
		{
			sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_SET_STIM_ASIC_HV_CALS, 0);
			return;
		}
		else
		{
			previousValue = micsRxBuffer.rxData.hvCalParams.stimAsicHVmVOutput[i];
		}
	}

	response = calSetHVCals(&micsRxBuffer.rxData.hvCalParams);

	sendCommandResponse(response, TKN_SET_STIM_ASIC_HV_CALS, 0);
}

//Handler for TKN_SET_TRIM_LIST
void cmndHndlrSetTrimList(void)
{
	RESPONSE response;
	TRIM_LIST tlist;

#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrSetTrimList.\n");
#endif

	switch(micsRxBuffer.rxData.setTrimListParams.listID)
	{
		case TRIM_PLUTO:
			logNormal(E_LOG_SET_POWER_TRIM_LIST,0);
			if (!trim_RangeCheckSetTrimList(&micsRxBuffer.rxData.setTrimListParams))
			{
				sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_SET_TRIM_LIST, 0);
				return;
			}

			memset(tlist.padding, 0, TRIM_LIST_PADDING_SIZE);
			memcpy(&tlist, &micsRxBuffer.rxData.setTrimListParams, sizeof(TRIM_LIST_CMD));

			response = trimSetList(&tlist);
			sendCommandResponse(response, TKN_SET_TRIM_LIST, 0);

			break;

		case TRIM_ZL:
			logNormal(E_LOG_SET_RADIO_TRIM_LIST,0);

			if (!trim_RangeCheckSetTrimList(&micsRxBuffer.rxData.setTrimListParams))
			{
				sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_SET_TRIM_LIST, 0);
				return;
			}

			memset(tlist.padding, 0, TRIM_LIST_PADDING_SIZE);
			memcpy(&tlist, &micsRxBuffer.rxData.setTrimListParams, sizeof(TRIM_LIST_CMD));

			response = trimSetPrimaryList(&tlist);
			sendCommandResponse(response, TKN_SET_TRIM_LIST, 0);

			break;

		case TRIM_SATURN:
			logNormal(E_LOG_SET_STIM_TRIM_LIST,0);

			if (!trim_RangeCheckSetSaturnTrimList(&micsRxBuffer.rxData.setTrimListParams))
			{
				sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_SET_TRIM_LIST, 0);
				return;
			}

			memset(tlist.padding, 0, TRIM_LIST_PADDING_SIZE);
			memcpy(&tlist, &micsRxBuffer.rxData.setTrimListParams, sizeof(TRIM_LIST_CMD));

			response = trimSetList(&tlist);

			sendCommandResponse(response, TKN_SET_TRIM_LIST, 0);

			break;

		case TRIM_ZL_FACTORY:
			logNormal(E_LOG_SET_RADIO_FACTORY_TRIM_LIST, 0);

			//override the trim ID here to match the normal ZL id.
			micsRxBuffer.rxData.setTrimListParams.listID = TRIM_ZL;

			if (!trim_RangeCheckSetTrimList(&micsRxBuffer.rxData.setTrimListParams))
			{
				sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_SET_TRIM_LIST, 0);
				return;
			}

			memset(tlist.padding, 0, TRIM_LIST_PADDING_SIZE);
			memcpy(&tlist, &micsRxBuffer.rxData.setTrimListParams, sizeof(TRIM_LIST_CMD));

			response = trimSetBackupList(&tlist);
			sendCommandResponse(response, TKN_SET_TRIM_LIST, 0);

			break;

		default:
			sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_SET_TRIM_LIST, 0);
			break;
	}

	return;
}


//Handler for TKN_SET_PRGM_DEF
void cmndHndlrSetPrgmDef(void)
{
	RESPONSE response;

#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrSetPrgmDef.\n");
#endif

	logNormal(E_LOG_SET_PROGRAM_DEFINITION,0);

	if (pgm_RangeCheckSetPrgmDef(&micsRxBuffer.rxData.setPrgmDefinitionParams))
	{
		response = pgm_SetPrgmDef(&micsRxBuffer.rxData.setPrgmDefinitionParams);
	}
	else
	{
		response = CMND_RESP_INVALID_PARAM;
	}

	sendCommandResponse(response, TKN_SET_PRGM_DEF, 0);
}


//Handler for TKN_SET_PRGM_CONST
void cmndHndlrSetPgrmConst(void)
{
	RESPONSE response;

#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrSetPgrmConst.\n");
#endif

	logNormal(E_LOG_SET_PROGRAM_CONSTANTS,0);

	if (constRangeCheckPrgmConst(&micsRxBuffer.rxData.setPgrmConstParams.prgmConst))
	{
		response = constSetPrgmConst(&micsRxBuffer.rxData.setPgrmConstParams.prgmConst);
	}
	else
	{
		response = CMND_RESP_INVALID_PARAM;
	}

	sendCommandResponse(response, TKN_SET_PRGM_CONST, 0);
}


//Handler for TKN_SET_PULSE_CONST
void cmndHndlrSetPulseConst(void)
{
	RESPONSE response;

#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrSetPulseConst.\n");
#endif

	logNormal(E_LOG_SET_PULSE_CONSTANTS,0);

	if (constRangeCheckPulseConst(&micsRxBuffer.rxData.setPulseConstParams.pulseConst))
	{
		response = constSetPulseConst(&micsRxBuffer.rxData.setPulseConstParams.pulseConst);
	}
	else
	{
		response = CMND_RESP_INVALID_PARAM;
	}

	sendCommandResponse(response, TKN_SET_PULSE_CONST, 0);
}

//Handler for TKN_SET_CONFIG_DEVICE_PARAMS
void cmndHndlrSetCfgDevParams(void)
{
	RESPONSE response;

#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrSetCfgDevParams.\n");
#endif
	
	logNormal(E_LOG_SET_CDP,0);

	if (cdp_RangeCheck(&micsRxBuffer.rxData.configDeviceParams))
	{
		bool changePairingFlag = false;
		CONFIG_DEVICE_PARAMETERS cdp;
		int i;

		//Find out if the pairing information is changing in order to log it
		if (cdp_Get(&cdp) < 0)
		{
			// The CDP is corrupted in NVRAM, so we aren't paired to anything.
			// The present Set CDP operation will establish new pairings, which counts as a change.
			changePairingFlag = true;
		}
		else
		{
			for (i = 0; i < NUM_PAIRINGS; i++)
			{
				if (micsRxBuffer.rxData.configDeviceParams.pairing[i].enabled != cdp.pairing[i].enabled)
				{
					// The pairing's changed from enabled to disabled, or vice versa.
					changePairingFlag = true;
				}
				else
				{
					// If the old and new EXIDs in an enabled pairing differ, the pairing changed.
					// Ignore disabled pairings, because they have no functional effect.
					if (micsRxBuffer.rxData.configDeviceParams.pairing[i].enabled
							&& memcmp(&micsRxBuffer.rxData.configDeviceParams.pairing[i].exid, &cdp.pairing[i].exid, sizeof(cdp.pairing[i].exid)) != 0)
					{
						changePairingFlag = true;
					}
				}
			}
		}

		response = cdp_SetConfigDeviceParams(&micsRxBuffer.rxData.configDeviceParams);
		sendCommandResponse(response, TKN_SET_CONFIG_DEVICE_PARAMS, 0);

		if (response != GEN_SUCCESS)
		{
			return;
		}
		else if (changePairingFlag)
		{
			logNormal(E_LOG_PAIRING_CHANGED_MICS,0);
		}
	}
	else
	{
		sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_SET_CONFIG_DEVICE_PARAMS, 0);
		return;
	}
}


//Handler for TKN_SET_LEAD_LIMITS
void cmndHndlrSetLeadLimits(void)
{
	RESPONSE retVal;

#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrGetLeadLimist.\n");
#endif

	logNormal(E_LOG_SET_LEAD_LIMITS,0);

	//Check to see if the params are within range
	if (!constRangeCheckLeadLimits(&micsRxBuffer.rxData.leadLimitParams))
	{
		sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_SET_LEAD_LIMITS, 0);
		return;
	}

	//Write the data to memory
	retVal = constSetLeadLimits(&micsRxBuffer.rxData.leadLimitParams);

	//Send the response
	sendCommandResponse(retVal, TKN_SET_LEAD_LIMITS, 0);
}


//Handler for TKN_SET_CP_DATA
void cmndHndlrSetCpData(void)
{
	int8_t retVal;

#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrSetCpData.\n");
#endif

	logNormal(E_LOG_SET_CP_DATA,0);

	//Read out the data
	if (micsRxBuffer.rxData.cpDataParams.block >= NUM_CP_DATA)
	{
		sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_SET_CP_DATA, 0);
		return;
	}

	retVal = cpDataWrite(micsRxBuffer.rxData.cpDataParams.block, micsRxBuffer.rxData.setCpDataParams.data);

	//Check if read failed
	if (retVal == ERROR_RETURN_VALUE)
	{
		sendCommandResponse(RESP_WRITE_FAILED, TKN_SET_CP_DATA, 0);
		return;
	}

	//Send response
	sendCommandResponse(GEN_SUCCESS, TKN_SET_CP_DATA, 0);
}


//Handler for TKN_SET_RAMP_TIME
void cmndHndlrSetRampTime(void)
{
	uint16_t rampTime = micsRxBuffer.rxData.rampTimeParams.rampTime;

#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrSetRampTime.\n");
#endif

	logNormal(E_LOG_SET_RAMP_TIME,0);

	// Range check
	if (!constRangeCheckRampTime(rampTime))
	{
		sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_SET_RAMP_TIME, 0);
		return;
	}

	// Save the value and send response
	sendCommandResponse(constSetRampTime(rampTime), TKN_SET_RAMP_TIME, 0);
}


//Handler for TKN_WRITE_MEMORY
void cmndHndlrWriteMemory(void)
{
	uint32_t boundaries[6] = {
			ADDRESS_MSP430_BYTE,
			ADDRESS_MSP430_WORD,
			ADDRESS_NVRAM,
			ADDRESS_POWER,
			ADDRESS_STIM,
			ADDRESS_MICS
	};
	int i = 0;
	uint8_t addressSpace;
	uint32_t physicalAddress;
	int retStatus = OK_RETURN_VALUE;

	#ifdef  CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrWriteMemory.\n");
	#endif

	logNormal(E_LOG_WRITE_MEMORY_REQUESTED,0);

	//Check for invalid address spaces...
	if (micsRxBuffer.rxData.writeMemoryParams.address < ADDRESS_MSP430_BYTE || micsRxBuffer.rxData.readMemoryParams.address > (ADDRESS_MICS | 0x00ffffffuL))
	{
		sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_WRITE_MEMORY, 0);
		return;
	}

	//Can't write more than MAX_MEMORY_WRITE_SIZE
	if (micsRxBuffer.rxData.writeMemoryParams.length > MAX_MEMORY_WRITE_SIZE)
	{
		sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_WRITE_MEMORY, 0);
		return;
	}

	//Check the range, to make sure it doesn't cross boundaries
	for (i = 0; i < 6; i++)
	{
		if (micsRxBuffer.rxData.writeMemoryParams.address < boundaries[i])
		{
			if (micsRxBuffer.rxData.writeMemoryParams.address + micsRxBuffer.rxData.writeMemoryParams.length > boundaries[i])
			{
				sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_WRITE_MEMORY, 0);
				return;
			}
		}
	}

	addressSpace = ADDRESS_SPACE(micsRxBuffer.rxData.writeMemoryParams.address);
	physicalAddress = ADDRESS_OFFSET(micsRxBuffer.rxData.writeMemoryParams.address);

	//Write it out based on device
	switch (addressSpace)
	{
		case ADDRESS_SPACE(ADDRESS_MSP430_BYTE)://MSP430 Byte Access
		{
			uint8_t const *p = micsRxBuffer.rxData.writeMemoryParams.data;
			if(ADDRESS_IS_FLASH(physicalAddress) != ADDRESS_IS_FLASH(physicalAddress + micsRxBuffer.rxData.writeMemoryParams.length))
			{
				retStatus = ERROR_RETURN_VALUE;
			}
			else if(ADDRESS_IS_FLASH(physicalAddress))
			{
				if(flashWriteWithinSegment(p, micsRxBuffer.rxData.writeMemoryParams.length, (void*)physicalAddress) != GEN_SUCCESS)
				{
					retStatus = ERROR_RETURN_VALUE;
				}
			}
			else
			{
				for (i = 0; i < micsRxBuffer.rxData.writeMemoryParams.length; i += sizeof(*p))
				{
					_data20_write_char(physicalAddress++, *p++);
				}
			}
			break;
		}
		case ADDRESS_SPACE(ADDRESS_MSP430_WORD)://MSP430 Word Access
		{
			uint16_t const *p = (void const *)micsRxBuffer.rxData.writeMemoryParams.data;
			if(ADDRESS_IS_FLASH(physicalAddress) != ADDRESS_IS_FLASH(physicalAddress + micsRxBuffer.rxData.writeMemoryParams.length))
			{
				retStatus = ERROR_RETURN_VALUE;
			}
			else if(ADDRESS_IS_FLASH(physicalAddress))
			{
				if(flashWriteWithinSegment(p, micsRxBuffer.rxData.writeMemoryParams.length, (void*)physicalAddress) != GEN_SUCCESS)
				{
					retStatus = ERROR_RETURN_VALUE;
				}
			}
			else
			{
				for (i = 0; i < micsRxBuffer.rxData.writeMemoryParams.length; i += sizeof(*p))
				{
					_data20_write_short(physicalAddress, *p++);
					physicalAddress += sizeof(*p);
				}
			}
			break;
		}
		case ADDRESS_SPACE(ADDRESS_NVRAM)://NVRAM
			retStatus = OK_RETURN_VALUE;

			if (nvStart() < 0)
			{
				retStatus = -1;
			}
			else if (nvWrite((NVADDR)physicalAddress, &micsRxBuffer.rxData.writeMemoryParams.data[0], micsRxBuffer.rxData.writeMemoryParams.length) < 0)
			{
				retStatus = -1;
			}

			if (nvStop() < 0)
			{
				retStatus = -1;
			}
			break;
		case ADDRESS_SPACE(ADDRESS_POWER)://Power ASIC
			for (i = 0; i < micsRxBuffer.rxData.readMemoryParams.length; i++)
			{
				if (pwrPoke((uint8_t)physicalAddress++, micsTxBuffer.txData.readMemoryResponseData.data[i]) < 0)
				{
					retStatus = ERROR_RETURN_VALUE;
				}
			}
			break;
		case ADDRESS_SPACE(ADDRESS_STIM)://Stim ASIC
			retStatus = stimWriteRegisters((uint16_t)physicalAddress, &micsRxBuffer.rxData.writeMemoryParams.data[0], micsRxBuffer.rxData.writeMemoryParams.length);
			break;
		case ADDRESS_SPACE(ADDRESS_MICS)://MICS (ZL7010x)
			for (i = 0; i < micsRxBuffer.rxData.readMemoryParams.length; i++)
			{
				micsPoke((uint8_t)physicalAddress++, micsTxBuffer.txData.readMemoryResponseData.data[i]);
			}
			break;
		default://Anything else
			sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_WRITE_MEMORY, 0);
			return;
	}

	if (retStatus == OK_RETURN_VALUE)
	{
		sendCommandResponse(GEN_SUCCESS, TKN_WRITE_MEMORY, 0);
	}
	else
	{
		sendCommandResponse(RESP_WRITE_FAILED, TKN_WRITE_MEMORY, 0);
	}
}


//Handler for TKN_SET_BG_IMPEDANCE
void cmndHndlrSetBgImpedance(void)
{
	RESPONSE response;

#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrSetBgImpedance.\n");
#endif

	logNormal(E_LOG_SET_BG_IMPEDANCE_PARAMS,0);

	if (bgImpRangeCheck(&micsRxBuffer.rxData.bgImpedanceParams))
	{
		response = bgImpSetBackgroundImpedanceParams(&micsRxBuffer.rxData.bgImpedanceParams);
		sendCommandResponse(response, TKN_SET_BG_IMPEDANCE, 0);
	}
	else
	{
		sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_SET_BG_IMPEDANCE, 0);
	}
}


/**
 * Command handler for the MICS Options (TKN_MICS_OPTIONS) command.
 */
void cmndHndlrMicsOptions(void)
{
	MICS_OPTIONS_PARAMS const * const p = (MICS_OPTIONS_PARAMS const *)&micsRxBuffer.rxData;

	if ((p->flags & ~MICS_LOW_LATENCY) != 0)
	{
		sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_MICS_OPTIONS, 0);
		return;
	}

	micsSetLowLatencyMode((p->flags & MICS_LOW_LATENCY) != 0);
	sendCommandResponse(GEN_SUCCESS, TKN_MICS_OPTIONS, 0);
}


/**
 * Arms the system to pair the next external to connect as a PoP
 */
void cmndHndlrPairPop(void)
{
#ifndef EPG
	if(chrgMgrIsChargerEngaged())
	{
		setArmForPOPPairing(true);
		sendCommandResponse(GEN_SUCCESS, TKN_PAIR_POP, 0);
	}
	else
	{
		sendCommandResponse(RESP_FEATURE_NOT_ENABLED, TKN_PAIR_POP, 0);
	}
#else
	sendCommandResponse(RESP_FEATURE_NOT_ENABLED, TKN_PAIR_POP, 0);
#endif
}
