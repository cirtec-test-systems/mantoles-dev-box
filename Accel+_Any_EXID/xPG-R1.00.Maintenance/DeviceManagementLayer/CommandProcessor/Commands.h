/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: Command Processor Unit.  Commands from external.
 *
 ***************************************************************************/

#ifndef COMMANDS_H_
#define COMMANDS_H_

/// \defgroup commands	Commands
/// \ingroup commandProcessor
/// @{

void cmndHndlrUnhandled(void);
void cmndHndlrStimCommand(void);
void cmndHndlrChargeControl(void);
void cmndHndlrStorageMode(void);
void cmndHndlrSelectProgram(void);
void cmndHndlrIncrPrgmAmpl(void);
void cmndHndlrDecrPrgmAmpl(void);
void cmndHndlrSetPulseAmpl(void);
void cmndHndlrIncrPulseAmpl(void);
void cmndHndlrDecrPulseAmpl(void);
void cmndHndlrIncrPulseWidth(void);
void cmndHndlrDecrPulseWidth(void);
void cmndHndlrIncrPrgmFreq(void);
void cmndHndlrDecrPrgmFreq(void);
void cmndHndlrRestorePrgmDef(void);
void cmndHndlrResetXpg(void);
void cmndHndlrImpedance(void);
void cmndHndlrSetPrgmFreq(void);
void cmndHndlrSetPulseWidth(void);
void cmndHndlrSetTitrStim(void);
void cmndHndlrSetTestStim(void);
void cmndHndlrSetTestWaveform(void);
void cmndHndlrCalibrateChannel(void);
void cmndHndlrInjectEvent(void);
void cmndHndlrTurnDebugOn(void);
void cmndHndlrTurnDebugOff(void);
void cmndHndlrClearLog(void);
void cmndHndlrStopBootloader(void);
void cmndHndlrMicsTune(void);
void cmndHndlrMicsDiagnostics(void);

/// @}

#endif /* COMMANDS_H_ */
