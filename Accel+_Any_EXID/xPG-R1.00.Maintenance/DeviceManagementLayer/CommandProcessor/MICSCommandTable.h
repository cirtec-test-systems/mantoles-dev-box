/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *	
 *	@file Description: Command Processor Unit. Command Table.
 *	
 ***************************************************************************/



#ifndef MICSCOMMANDTABLE_H_
#define MICSCOMMANDTABLE_H_

#include "CommandProcessor.h"

/// \defgroup micsCommandTable	MICS Command Table
/// \ingroup commandProcessor
/// @{

extern const CMND_TABLE_ENTRY cmndTable[];
extern unsigned const numCmndTableEntries;
extern MICS_RX_BUFFER micsRxBuffer;
extern MICS_TX_BUFFER micsTxBuffer;

/// @}

#endif /* MICSCOMMANDTABLE_H_ */
