/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: Command Processor Unit.  Mutators from external.
 *
 ***************************************************************************/

#ifndef MUTATORS_H_
#define MUTATORS_H_

/// \defgroup mutators	Mutators
/// \ingroup commandProcessor
/// @{

void cmndHndlrAppendLog(void);
void cmndHndlrSetXpgIdentity(void);
void cmndHndlrSetGenCal(void);
void cmndHndlrSetCounter(void);
void cmndHndlrSetChannelCals(void);
void cmndHndlrSetStimHvCals(void);
void cmndHndlrSetTrimList(void);
void cmndHndlrSetPrgmDef(void);
void cmndHndlrSetPgrmConst(void);
void cmndHndlrSetPulseConst(void);
void cmndHndlrSetCfgDevParams(void);
void cmndHndlrSetLeadLimits(void);
void cmndHndlrSetCpData(void);
void cmndHndlrSetRampTime(void);
void cmndHndlrWriteMemory(void);
void cmndHndlrSetBgImpedance(void);
void cmndHndlrMicsOptions(void);
void cmndHndlrPairPop(void);

/// @}

#endif /* MUTATORS_H_ */
