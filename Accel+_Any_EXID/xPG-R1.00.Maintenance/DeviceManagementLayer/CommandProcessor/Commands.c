/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: Command Processor Unit.  Command Table.
 *
 ***************************************************************************/
//#define CMDTBL_PRINT_OUTPUT	// to turn on/off print debug output for Command Processor module
#ifdef  CMDTBL_PRINT_OUTPUT
#include <stdio.h>
#endif

#include "CommandProcessor.h"
#include "StimManager.h"
#include "SystemHealthMonitor.h"
#include "EventQueue.h"
#include "Watchdog.h"
#include "pgm.h"
#include "activePgm.h"
#include "Commands.h"
#include "MICSCommandTable.h"
#include "MicsPAD.h"
#include "PowerASIC.h"
#include "delay.h"
#include "trim.h"

/*
 * All the handlers
 */
//This is for unhandled commands
void cmndHndlrUnhandled(void)
{
	#ifdef CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrUnhandled\n");
	#endif

	logNormal(E_LOG_COMMAND_UNHANDLED,0);

	sendCommandResponse(CMND_RESP_INVALID_CMND, micsRxBuffer.token, sizeof(RESPONSE));
}

//This is for stimulation commands
void cmndHndlrStimCommand(void)
{
	EVENT stimEvent;
	#ifdef CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrStimCommand\n");
		printf("Stim State is %d\n", micsRxBuffer.rxData.stimParams.stimState);
	#endif

	stimEvent.eventID = E_STIM_COMMAND;
	stimEvent.eventData.eStimMode = (enum STIM_STATE)micsRxBuffer.rxData.stimParams.stimState;
	dispPutEvent(stimEvent);
}


//Command for TKN_STORAGE
void cmndHndlrStorageMode(void)
{
	EVENT storageModeEvent;

	#ifdef CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrStorageMode\n");
	#endif

	logNormal(E_LOG_STORAGE_MODE,0);

	sendQuickMicsResponse(GEN_SUCCESS, TKN_STORAGE);

	storageModeEvent.eventID = E_ENTER_STORAGE_MODE;
	dispPutEvent(storageModeEvent);
}


//Command for TKN_SELECT_PROGRAM
void cmndHndlrSelectProgram(void)
{
	EVENT selectProgramEvent;

	#ifdef CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrSelectProgram\n");
		printf("Selected program is %d\n", micsRxBuffer.rxData.selectedProgramParams.program);
	#endif

	logNormal(E_LOG_SELECT_PROGRAM,0);

	selectProgramEvent.eventID = E_SELECT_PROGRAM;
	selectProgramEvent.eventData.selectedProgram = micsRxBuffer.rxData.selectedProgramParams.program;
	dispPutEvent(selectProgramEvent);
}


//Command for TKN_INC_PRGM_AMPL
void cmndHndlrIncrPrgmAmpl(void)
{
	EVENT incrementEvent;
	#ifdef CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrIncrPrgmAmpl\n");
	#endif

	logNormal(E_LOG_INCR_PRGM_AMPLITUDE,0);

	incrementEvent.eventID = E_STIM_INCREASE_PROGRAM_AMPLITUDE;
	dispPutEvent(incrementEvent);
}


//Command for TKN_DEC_PRGM_AMPL
void cmndHndlrDecrPrgmAmpl(void)
{
	EVENT decrementEvent;

	#ifdef CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrDecrPrgmAmpl\n");
	#endif

	logNormal(E_LOG_DECR_PRGM_AMPLITUDE,0);

	decrementEvent.eventID = E_STIM_DECREASE_PROGRAM_AMPLITUDE;
	dispPutEvent(decrementEvent);
}


//Command for TKN_SET_PULSE_AMPL
void cmndHndlrSetPulseAmpl(void)
{
	EVENT setPulseAmplEvent;

	#ifdef CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrSetPulseAmpl\n");
	#endif

	logNormal(E_LOG_SET_PULSE_AMPLITUDE,0);

	setPulseAmplEvent.eventID = E_STIM_SET_AMPLITUDE;
	setPulseAmplEvent.eventData.setPulseAmplParams.displayAmplitudeStepIndex = micsRxBuffer.rxData.setPulseAmplParams.displayAmplitudeStepIndex;
	setPulseAmplEvent.eventData.setPulseAmplParams.pulseIndex = micsRxBuffer.rxData.setPulseAmplParams.pulseIndex;
	dispPutEvent(setPulseAmplEvent);
}

//Handler for TKN_SET_PULSE_WIDTH
void cmndHndlrSetPulseWidth(void)
{
	EVENT setPulseWidthEvent;

#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrSetPulseWidth.\n");
#endif

	setPulseWidthEvent.eventID = E_SET_PULSE_WIDTH;
	setPulseWidthEvent.eventData.setPulseWidthParams.pulseWidth = micsRxBuffer.rxData.setPulseWidthParams.pulseWidth;
	setPulseWidthEvent.eventData.setPulseWidthParams.pulseIndex = micsRxBuffer.rxData.setPulseWidthParams.pulseIndex;
	setPulseWidthEvent.eventData.setPulseWidthParams.reserved   = micsRxBuffer.rxData.setPulseWidthParams.reserved;

	dispPutEvent(setPulseWidthEvent);
}


//Handler for TKN_SET_PRGM_FREQ
void cmndHndlrSetPrgmFreq(void)
{
	EVENT setProgramFreqEvent;

#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrSetPrgmFreq.\n");
#endif

	logNormal(E_LOG_SET_PRGM_FREQUENCY,0);

	setProgramFreqEvent.eventID = E_SET_FREQUENCY;
	setProgramFreqEvent.eventData.setProgramFreqParams.programFrequencyIndex = micsRxBuffer.rxData.programFreqParams.programFrequencyIndex;

	dispPutEvent(setProgramFreqEvent);
}

//Command for TKN_INC_PULSE_AMPL
void cmndHndlrIncrPulseAmpl(void)
{
	EVENT incrPulseAmplEvent;

	#ifdef CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrIncrPulseAmpl\n");
	#endif

	incrPulseAmplEvent.eventID = E_STIM_INCREASE_PULSE_AMPLITUDE;
	incrPulseAmplEvent.eventData.pulseIndex = micsRxBuffer.rxData.incrPulseAmplParams.pulseIndex;
	dispPutEvent(incrPulseAmplEvent);
}


//Command for TKN_DEC_PULSE_AMPL
void cmndHndlrDecrPulseAmpl(void)
{
	EVENT decrPulseAmplEvent;

	#ifdef CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrDecrPulseAmpl\n");
	#endif

	decrPulseAmplEvent.eventID = E_STIM_DECREASE_PULSE_AMPLITUDE;
	decrPulseAmplEvent.eventData.pulseIndex = micsRxBuffer.rxData.decrPulseAmplParams.pulseIndex;
	dispPutEvent(decrPulseAmplEvent);
}

//Command for TKN_INC_PULSE_WIDTH
void cmndHndlrIncrPulseWidth(void)
{
	EVENT incrPulseWidthEvent;

	#ifdef CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrIncrPulseWidth\n");
	#endif

	incrPulseWidthEvent.eventID = E_STIM_INCREASE_PULSE_WIDTH;
	incrPulseWidthEvent.eventData.pulseIndex = micsRxBuffer.rxData.incrPulseWidthParams.pulseIndex;
	dispPutEvent(incrPulseWidthEvent);
}


//Command for TKN_DEC_PULSE_WIDTH
void cmndHndlrDecrPulseWidth(void)
{
	EVENT decrPulseWidthEvent;

	#ifdef CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrDecrPulseWidth\n");
	#endif

	decrPulseWidthEvent.eventID = E_STIM_DECREASE_PULSE_WIDTH;
	decrPulseWidthEvent.eventData.pulseIndex = micsRxBuffer.rxData.decrPulseWidthParams.pulseIndex;
	dispPutEvent(decrPulseWidthEvent);
}


//Command for TKN_INC_PRGM_FREQ
void cmndHndlrIncrPrgmFreq(void)
{
	EVENT incrPrgmFreqEvent;

	#ifdef CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrIncrPrgmFreq\n");
	#endif

	incrPrgmFreqEvent.eventID = E_STIM_INCREASE_FREQUENCY;
	dispPutEvent(incrPrgmFreqEvent);
}


//Command for TKN_DEC_PRGM_FREQ
void cmndHndlrDecrPrgmFreq(void)
{
	EVENT decrPrgmFreqEvent;

	#ifdef CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrDecrPrgmFreq\n");
	#endif

	decrPrgmFreqEvent.eventID = E_STIM_DECREASE_FREQUENCY;
	dispPutEvent(decrPrgmFreqEvent);
}


//Command handler for TKN_RSTR_PRGM_DFLT
void cmndHndlrRestorePrgmDef(void)
{
	int i = 0;
	GET_PRGM_DEF_RESPONSE_DATA programDefinitionResponseData;
	GET_PRGM_DEF_PARAMS programParams;
	RESPONSE response;
	int retVal;

#ifdef CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrRestorePrgmDef\n");
#endif

	if (isStimOn())
	{
		sendCommandResponse(CMND_RESP_BUSY, TKN_RSTR_PRGM_DFLT, 0);
		return;
	}

	for (i = 1; i <= NUM_PRGM_DEFS; i++)
	{
		programParams.program = i;

		response = pgm_GetPrgmDef(&programParams, &programDefinitionResponseData);

		if (response == GEN_SUCCESS)
		{
			if ((micsRxBuffer.rxData.restorePrgmDefaultParams.programMap >> (i-1)) & 0x1)
			{
				retVal = actPgmSetInitialActiveSettings(i, &programDefinitionResponseData.programDef);

				if (retVal != OK_RETURN_VALUE )
				{
					sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_RSTR_PRGM_DFLT, 0);
					return;
				}
				else
				{
					logNormal(E_LOG_RESTORE_PROGRAM_DEFAULTS,0);
				}
			}
		}
		else
		{
			sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_RSTR_PRGM_DFLT, 0);
			return;
		}

	}

	sendCommandResponse(response, TKN_RSTR_PRGM_DFLT, 0);
}


//Command handler for TKN_RESET_XPG
void cmndHndlrResetXpg(void)
{
	EVENT resetEvent;
	bool charging = false;
	CHARGE_STATE chargeState;

	#ifdef CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrResetXpg\n");
	#endif

	//Get the current charge state
	chargeState = chargeGetChargeState();

	if (chargeState == CHARGE_STATE_CHARGE_PRE_CONDITION || chargeState == CHARGE_STATE_CHARGE_CONSTANT_CURRENT || chargeState == CHARGE_STATE_CHARGE_VOLTAGE)
	{
		charging = true;
	}

	//Do not allow soft reset if currently stimulating or charging.
	if (isStimOn() || charging)
	{
		sendCommandResponse(CMND_RESP_BUSY, TKN_RESET_XPG, 0);
	}
	else
	{
		sendQuickMicsResponse(GEN_SUCCESS, TKN_RESET_XPG);

		resetEvent.eventID = E_SOFTWARE_RESET;
		dispPutEvent(resetEvent);
	}
}

//Command handler for TKN_IMP_MEAS
void cmndHndlrImpedance(void)
{
	EVENT impedanceEvent;

	#ifdef CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrImpedance\n");
	#endif

	impedanceEvent.eventID = E_IMPEDANCE_COMMAND;
	impedanceEvent.eventData.impedanceParams = &micsRxBuffer.rxData.impedanceParams;
	dispPutEvent(impedanceEvent);
}


//Handler for TKN_CLEAR_LOG
void cmndHndlrClearLog(void)
{
#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrClearLog.\n");
#endif

	//Check for invalid parameter
	if (micsRxBuffer.rxData.clearLogParams.logNum > 1)
	{
		sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_CLEAR_LOG, 0);

		return;
	}

	//Log the action
	logNormal(E_LOG_CLEAR_LOG,0);

	//Clear the log
	logClear(micsRxBuffer.rxData.clearLogParams.logNum);

	//Send the response
	sendCommandResponse(GEN_SUCCESS, TKN_CLEAR_LOG, 0);
}


//Handler for TKN_SET_TITR_STIM
void cmndHndlrSetTitrStim(void)
{
	EVENT titrEvent;

	#ifdef  CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrSetTitrStim.\n");
	#endif

	//logNormal(E_LOG_TITRATION_PARAMS,0);

	titrEvent.eventID = E_SET_TITRATION_STIM;
	titrEvent.eventData.titrParams = &micsRxBuffer.rxData.titrStimParams;

	dispPutEvent(titrEvent);
}


//Handler for TKN_SET_TEST_STIM
void cmndHndlrSetTestStim(void)
{
	EVENT event;

#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrSetTestStim.\n");
#endif

	logNormal(E_LOG_SET_TEST_PARAMS,0);

	event.eventID = E_SET_TEST_STIM;
	event.eventData.testStim = &micsRxBuffer.rxData.setTestStimParams;

	dispPutEvent(event);
}

/**
 * Handler for TKN_SET_TEST_WAVEFORM
 */
void cmndHndlrSetTestWaveform(void)
{
	EVENT event;

#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrSetTestWaveform.\n");
#endif

	event.eventID = E_SET_TEST_WAVEFORM;
	event.eventData.waveformParams = (void *)&micsRxBuffer.rxData;

	dispPutEvent(event);
}


/**
 * Handler for TKN_CALIBRATE_CHANNEL
 */
void cmndHndlrCalibrateChannel(void)
{
	RESPONSE response = testStimCmdCalibrateChannel(&micsRxBuffer.rxData.calChanParams, &micsTxBuffer.txData.calChanResponseData);
	if (response == GEN_SUCCESS)
	{
		sendCommandResponse(GEN_SUCCESS, TKN_CALIBRATE_CHANNEL, sizeof(micsTxBuffer.txData.calChanResponseData));
	}
	else
	{
		sendCommandResponse(response, TKN_CALIBRATE_CHANNEL, 0);
	}
}

//Handler for TKN_INJECT_EVENT
void cmndHndlrInjectEvent(void)
{
	EVENT injectEvent;

#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrInjectEvent.\n");
#endif

	injectEvent.eventID = (EVENT_ID)micsRxBuffer.rxData.injectEventParams.eventId;

	if (micsRxBuffer.rxData.injectEventParams.eventId == E_ERR_ERROR_DETECTED)
	{
		injectEvent.eventData.eActiveErrorData = (ACTIVE_ERROR_CODE)micsRxBuffer.rxData.injectEventParams.eventData;
	}
	else if (micsRxBuffer.rxData.injectEventParams.eventId == E_ERR_CHARGER_ERROR)
	{
		injectEvent.eventData.eChargeErrorData = (CHARGE_ERROR_CODE)micsRxBuffer.rxData.injectEventParams.eventData;
	}

	dispPutEvent(injectEvent);

	sendCommandResponse(GEN_SUCCESS, TKN_INJECT_EVENT, 0);
}


void cmndHndlrStopBootloader(void)
{
	sendCommandResponse(GEN_SUCCESS, TKN_STOP_BOOTLOADER, 0);
}

void cmndHndlrMicsTune(void)
{
	const TRIM_LIST *flashtrims;
	TRIM_LIST trims;
	RESPONSE response;

	if(!trimIsListValid(TRIM_ZL))
	{
		//We do the best we can to tune MICS, but we don't fail.
		sendCommandResponse(RESP_DATA_CORRUPTED, TKN_MICS_TUNE, 0);
	}

	// Get a copy of the trim list.
	flashtrims = trimGetList(TRIM_ZL);
	memcpy(&trims, flashtrims, sizeof(TRIM_LIST));

	// Run the autotune.
	micsAutotune(&trims);

	// Store the new trim list
	response = trimSetPrimaryList(&trims);

	sendCommandResponse(response, TKN_MICS_TUNE, 0);
}

void cmndHndlrMicsDiagnostics(void)
{
	EVENT event;

	//validate the command
	if(micsRxBuffer.rxData.micsDiagnosticsParams.channel > 11
			&& micsRxBuffer.rxData.micsDiagnosticsParams.channel != 0xFF)
	{
		sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_MICS_DIAGNOSTICS, 0);
		return;
	}

	if(micsRxBuffer.rxData.micsDiagnosticsParams.mode > 4)
	{
		sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_MICS_DIAGNOSTICS, 0);
		return;
	}

	//Post it so that MicsConnection can perform the diagnostic
	event.eventID = E_MICS_PERFORM_DIAGNOSTICS;
	event.eventData.micsDiagnosticsParams = micsRxBuffer.rxData.micsDiagnosticsParams;
	dispPutEvent(event);

	sendCommandResponse(GEN_SUCCESS, TKN_MICS_DIAGNOSTICS, 0);
}
