/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: Command Processor Unit.  Command Table.
 *
 ***************************************************************************/

#define CMDTBL_PRINT_OUTPUT	// to turn on/off print debug output for Command Processor module
#ifdef  CMDTBL_PRINT_OUTPUT
#include <stdio.h>
#endif

#include "CommandProcessor.h"
#include "Queries.h"
#include "Commands.h"
#include "Mutators.h"
#include "MICSCommandTable.h"

#include "Common/Protocol/cmndMicsOptions.h"

//The following table must be in the order of the command tokens as this table is indexed by the command
//token.

//Define the table
CMND_TABLE_ENTRY const cmndTable[] =
{
		//Token							Data Size							Handler
		//-------------------------------------------------------------------------------------------------------
		{(CMND_TOKEN)0,					0,									cmndHndlrUnhandled		},
		{TKN_GET_XPG_STATUS,			0,									cmndHndlrGetXpgStatus	},
		{TKN_STIM,						sizeof(STIM_PARAMS),				cmndHndlrStimCommand	},
		{TKN_SLCT_PRGM,					sizeof(SLCT_PRGM_PARAMS),			cmndHndlrSelectProgram	},
		{TKN_INC_PRGM_AMPL,				0,									cmndHndlrIncrPrgmAmpl	},
		{TKN_DEC_PRGM_AMPL,				0,									cmndHndlrDecrPrgmAmpl	},
		{TKN_GET_POP_CONST,				0,									cmndHndlrGetPopConst	},
		{TKN_GET_PPC_CONST,				0,									cmndHndlrGetPpcConst	},
		{TKN_GET_PRGM_NAMES,			0,									cmndHndlrGetPgrmNames	},
		{TKN_SET_PULSE_AMPL,			sizeof(SET_PULSE_AMPL_PARAMS),		cmndHndlrSetPulseAmpl	},
		{TKN_INC_PULSE_AMPL,			sizeof(INC_PULSE_AMPL_PARAMS),		cmndHndlrIncrPulseAmpl	},
		{TKN_DEC_PULSE_AMPL,			sizeof(DEC_PULSE_AMPL_PARAMS),		cmndHndlrDecrPulseAmpl	},
		{TKN_GET_PULSE_WIDTHS,			0,									cmndHndlrGetPulseWidths	},
		{TKN_SET_PULSE_WIDTH,			sizeof(SET_PULSE_WIDTH_PARAMS),		cmndHndlrSetPulseWidth	},
		{TKN_INC_PULSE_WIDTH,			sizeof(INC_PULSE_WIDTH_PARAMS),		cmndHndlrIncrPulseWidth	},
		{TKN_DEC_PULSE_WIDTH,			sizeof(DEC_PULSE_WIDTH_PARAMS),		cmndHndlrDecrPulseWidth	},
		{TKN_GET_PRGM_FREQ,				0,									cmndHndlrGetPrgmFreq	},
		{TKN_SET_PRGM_FREQ,				sizeof(SET_PRGM_FREQ_PARAMS),		cmndHndlrSetPrgmFreq	},
		{TKN_INC_PRGM_FREQ,				0,									cmndHndlrIncrPrgmFreq	},
		{TKN_DEC_PRGM_FREQ,				0,									cmndHndlrDecrPrgmFreq	},
		{TKN_INJECT_EVENT,				sizeof(INJECT_EVENT_PARAMS),		cmndHndlrInjectEvent	},
		{TKN_CHARGING_CONTROL,			0,									cmndHndlrChargeControl	},
		{TKN_GET_XPG_IDENT,				0,									cmndHndlrGetxPgIdentity },
		{TKN_SET_XPG_IDENT,				sizeof(XPG_IDENTITY),				cmndHndlrSetXpgIdentity	},
		{TKN_RSTR_PRGM_DFLT,			sizeof(RSTR_PRGM_DFLT_PARAMS),		cmndHndlrRestorePrgmDef	},
		{TKN_GET_PRGM_DEF,				sizeof(GET_PRGM_DEF_PARAMS),		cmndHndlrGetPrgmDef		},
		{TKN_SET_PRGM_DEF,				sizeof(SET_PRGM_DEF_PARAMS),		cmndHndlrSetPrgmDef		},
		{TKN_GET_PRGM_CONST,			0,									cmndHndlrGetPgrmConst	},
		{TKN_SET_PRGM_CONST,			sizeof(SET_PRGM_CONST_PARAMS),		cmndHndlrSetPgrmConst	},
		{TKN_GET_CONFIG_DEVICE_PARAMS,	0,									cmndHndlrGetCfgDevParams},
		{TKN_SET_CONFIG_DEVICE_PARAMS,	sizeof(CONFIG_DEVICE_PARAMETERS),	cmndHndlrSetCfgDevParams},
		{TKN_GET_PULSE_CONST,			0,									cmndHndlrGetPulseConst	},
		{TKN_SET_PULSE_CONST,			sizeof(SET_PULSE_CONST_PARAMS),		cmndHndlrSetPulseConst	},
		{TKN_GET_LEAD_LIMITS,			0,									cmndHndlrGetLeadLimits	},
		{TKN_SET_LEAD_LIMITS,			sizeof(LEAD_LIMIT_PARAMS),			cmndHndlrSetLeadLimits	},
		{TKN_GET_CHANNEL_CALS,			sizeof(GET_CHANNEL_CALS_PARAMS),	cmndHndlrGetChannelCals	},
		{TKN_SET_CHANNEL_CALS,			sizeof(CHANNEL_CALS),				cmndHndlrSetChannelCals	},
		{TKN_UNUSED_37,					0,									cmndHndlrUnhandled		},
		{TKN_UNUSED_38,					0,									cmndHndlrUnhandled		},
		{TKN_UNUSED_39,					0,									cmndHndlrUnhandled		},
		{TKN_UNUSED_40,					0,									cmndHndlrUnhandled		},
		{TKN_UNUSED_41,					0,									cmndHndlrUnhandled		},
		{TKN_UNUSED_42,					0,									cmndHndlrUnhandled		},
		{TKN_GET_STIM_ASIC_HV_CALS,		0,									cmndHndlrGetStimHvCals	},
		{TKN_SET_STIM_ASIC_HV_CALS,		sizeof(HV_CAL_PARAMS),				cmndHndlrSetStimHvCals	},
		{TKN_UNUSED_45,					0,									cmndHndlrUnhandled		},
		{TKN_UNUSED_46,					0,									cmndHndlrUnhandled		},
		{TKN_GET_CP_DATA,				sizeof(GET_CP_DATA_PARAMS),			cmndHndlrGetCpData		},
		{TKN_SET_CP_DATA,				sizeof(SET_CP_DATA_PARAMS),			cmndHndlrSetCpData		},
		{TKN_CLEAR_LOG,					sizeof(CLEAR_LOG_PARAMS),			cmndHndlrClearLog		},
		{TKN_READ_LOG,					sizeof(READ_LOG),					cmndHndlrReadLog		},
		{TKN_APPEND_LOG,				sizeof(APPEND_LOG),					cmndHndlrAppendLog		},
		{TKN_UNUSED_52,					0,									cmndHndlrUnhandled		},
		{TKN_UNUSED_53,					0,									cmndHndlrUnhandled		},
		{TKN_IMP_MEAS,					sizeof(IMPEDANCE_PARAMS),			cmndHndlrImpedance		},
		{TKN_STORAGE,					0,									cmndHndlrStorageMode	},
		{TKN_ECHO_SHORT,				sizeof(ECHO_SHORT_PARAMS),			cmndHndlrEchoShort		},
		{TKN_ECHO_LONG,					sizeof(ECHO_LONG_PARAMS),			cmndHndlrEchoLong		},
		{TKN_GET_GEN_CAL,				0,									cmndHndlrGetGenCal		},
		{TKN_SET_GEN_CAL,				sizeof(SET_GENERAL_CALS_PARAMS),	cmndHndlrSetGenCal		},
		{TKN_GET_TRIM_LIST,				sizeof(GET_TRIM_LIST_PARAMS),		cmndHndlrGetTrimList	},
		{TKN_SET_TRIM_LIST,				sizeof(SET_TRIM_LIST_PARAMS),		cmndHndlrSetTrimList	},
		{TKN_GET_COUNTER,				sizeof(GET_COUNTER_PARAMS),			cmndHndlrGetCounter		},
		{TKN_SET_COUNTER,				sizeof(SET_COUNTER_PARAMS),			cmndHndlrSetCounter		},
		{TKN_GET_TEST_STIM,				0,									cmndHndlrGetTestStim	},
		{TKN_SET_TEST_STIM,				sizeof(SET_TEST_STIM_PARAMS),		cmndHndlrSetTestStim	},
		{TKN_GET_TITR_STIM,				0,									cmndHndlrGetTitrStim	},
		{TKN_SET_TITR_STIM,				sizeof(SET_TITR_STIM_PARAMS),		cmndHndlrSetTitrStim	},
		{TKN_GET_RAMP_TIME,				0,									cmndHndlrGetRampTime	},
		{TKN_SET_RAMP_TIME,				sizeof(SET_RAMP_TIME_PARAMS),		cmndHndlrSetRampTime	},
		{TKN_GET_VBAT,					0,									cmndHndlrGetVBAT		},
		{TKN_SET_SDB,					0,									cmndHndlrUnhandled		},
		{TKN_GET_SDB,					0,									cmndHndlrUnhandled		},
		{TKN_WRITE_MEMORY,				sizeof(WRITE_MEMORY_PARAMS),		cmndHndlrWriteMemory	},
		{TKN_READ_MEMORY,				sizeof(READ_MEMORY_PARAMS),			cmndHndlrReadMemory		},
		{TKN_RESET_XPG,					0,									cmndHndlrResetXpg		},
		{TKN_ERASE_FLASH_BLOCK,			0,									cmndHndlrUnhandled		},
		{TKN_WRITE_VECTORS,				0,									cmndHndlrUnhandled		},
		{TKN_READ_VECTORS,				0,									cmndHndlrUnhandled		},
		{TKN_DIAG_DATA,					sizeof(DIAG_DATA_PARAMS),			cmndHndlrGetDiagData	},
		{TKN_SET_BG_IMPEDANCE,			sizeof(BACKGROUND_IMPEDANCE_PARAMS),cmndHndlrSetBgImpedance },
		{TKN_GET_BG_IMPEDANCE,			0,									cmndHndlrGetBgImpedance },
		{TKN_SET_TEST_WAVEFORM,			sizeof(SET_TEST_WAVEFORM_PARAMS),	cmndHndlrSetTestWaveform},
		{TKN_CALIBRATE_CHANNEL,			sizeof(CAL_CHAN_PARAMS),			cmndHndlrCalibrateChannel},
		{TKN_GET_LOG_RANGE,				sizeof(GET_LOG_RANGE_PARAMS),		cmndHndlrGetLogRange	},
		{TKN_GET_ANT_TUNE,				0,									cmndHndlrUnhandled		},
		{TKN_SET_ANT_TUNE,				0,									cmndHndlrUnhandled		},
		{TKN_MICS_OPTIONS,				sizeof(MICS_OPTIONS_PARAMS),		cmndHndlrMicsOptions	},
		{TKN_PAIR_PPC,					0,									cmndHndlrUnhandled		}, //This is handled by MicsPAD directly.
		{TKN_PAIR_POP,					0,									cmndHndlrPairPop		},
		{TKN_STOP_BOOTLOADER,			0,									cmndHndlrStopBootloader	},
		{TKN_MICS_TUNE,					0,									cmndHndlrMicsTune		},
		{TKN_MICS_DIAGNOSTICS,			sizeof(MICS_DIAGNOSTICS_PARAMS),	cmndHndlrMicsDiagnostics},
};

unsigned const numCmndTableEntries = sizeof(cmndTable)/sizeof(cmndTable[0]);

//Allocate space for send/receive buffers
MICS_RX_BUFFER micsRxBuffer;
MICS_TX_BUFFER micsTxBuffer;
