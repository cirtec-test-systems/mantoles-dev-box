/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: Command Processor Module.
 *
 ***************************************************************************/

//#define CMDPR_PRINT_OUTPUT	// to turn on/off print debug output for Command Processor module
#ifdef  CMDPR_PRINT_OUTPUT
#include <stdio.h>
#endif

#include "state_machine_common.h"
#include "system_events.h"
#include "log_events.h"
#include "error_code.h"
#include "EventQueue.h"
#include "MultiChannelTimer.h"
#include "log.h"
#include "trim.h"
#include "CommandProcessor.h"
#include "swtmr_application_timers.h"
#include "xpginfo.h"
#include "Common/Protocol/trim_list.h"
#include "timer.h"
#include "cdp.h"
#include "MICSCommandTable.h"

//Define the states of the state machine
typedef enum CMDPR_STATE
{
	CMDPR_STATE_INITIAL 			=0,	//Before initialization
	CMDPR_STATE_READY				=1,	//Ready to receive command
	CMDPR_STATE_WAIT_FOR_MICS_RC   	=2,	//Waiting for a response to a MICS Command
	CMDPR_STATE_ALL					=3	//Represents all states - allows for handling an even in any state
} CMDPR_STATE;


/*Event Handers*/
static void eventHandlerE_INITIALIZE(EVENT event);
static void eventHandlerE_MICS_CMD(EVENT event);
static void eventHandlerE_CMD_RESPONSE(EVENT event);
static void eventHandlerE_MICS_TIMEOUT(EVENT event);

// Predefine the state machine table size
#define CMDPR_EVENT_TABLE_LEN 4

// Define the State Machine Table
static STATE_MACHINE_TABLE_ENTRY const cmdprStateTable[CMDPR_EVENT_TABLE_LEN] =
{
//   There is one table entry for every event processed in each state
//   CURRENT STATE					EVENT ID			EVENT HANDLER				    END STATE
//   --------------------------------------------------------------------------------------------------------------------
DISPATCH_GEN_START(commandProcessorProcessEvent)
	{CMDPR_STATE_INITIAL,			E_INITIALIZE,		eventHandlerE_INITIALIZE		/* CMDPR_STATE_READY 			*/},
	{CMDPR_STATE_READY,				E_MICS_CMD,			eventHandlerE_MICS_CMD			/* CMDPR_STATE_WAIT_FOR_MICS_RC	*/},
	{CMDPR_STATE_WAIT_FOR_MICS_RC,	E_CMD_RESPONSE,		eventHandlerE_CMD_RESPONSE		/* CMDPR_STATE_READY			*/},
	{CMDPR_STATE_WAIT_FOR_MICS_RC,	E_MICS_RC_TIMEOUT,	eventHandlerE_MICS_TIMEOUT		/* CMDPR_STATE_READY			*/},
DISPATCH_GEN_END
};

//Initialize the state machine and necessary
static CMDPR_STATE cmdprCurrentState = CMDPR_STATE_INITIAL;

//More Private Variables
static CMND_TOKEN micsCmndToken = TKN_X_UNDEFINED;
static bool armedForPOPPairing = false;

#ifdef CMND_PERF_MON
extern uint16_t startTimeCommand;
extern uint16_t endTimeCommand;
extern uint16_t startTimeStartResponse;
#endif

//Main Event Handler that processes all subscribed events
void commandProcessorProcessEvent(EVENT eventToProcess)
{
	int i = 0;
	EVENT_ID eventId = eventToProcess.eventID;

	#ifdef CMDPR_PRINT_OUTPUT
		printf("Command Processor - ProcessEvent - Event ID = %d.\n", eventId);
	#endif

	// Iterate through the state machine table until find the current state.
	for(i=0; i < CMDPR_EVENT_TABLE_LEN; i++)
	{
		if((cmdprStateTable[i].state == cmdprCurrentState || cmdprStateTable[i].state == CMDPR_STATE_ALL)  && cmdprStateTable[i].eventId == eventId)
		{
			break;
		}
	}

	//Now call the appropriate event handler
	if(i != CMDPR_EVENT_TABLE_LEN)
	{
		cmdprStateTable[i].eventHandler(eventToProcess);
	}

	return;
}

/*
 *
 * EVENT HANDLER FUNCTIONS
 *
 */

//Transition function for E_INITIALIZE event
static void eventHandlerE_INITIALIZE(EVENT event)
{
	CONFIG_DEVICE_PARAMETERS cdp;
	RESPONSE response;

	(void) event;	// This event carries no data.

	#ifdef CMDPR_PRINT_OUTPUT
		printf("Command Processor - eventHandlerE_INITIALIZE\n");
	#endif

	//Check for corruption in identity data
	if (!isXpgInfoValid())
	{
		logError(E_LOG_ERR_MICS_ID_CORRUPTED,0);
	}

	//Check for corruption in any of cdp parameters
	response = cdp_GetConfigDeviceParams(&cdp);
	if (response != GEN_SUCCESS)
	{
		logError(E_LOG_ERR_CONFIG_DEVICE_PARAMS_CORRUPTED,0);
	}

	cmdprCurrentState = CMDPR_STATE_READY;

	return;
}

//Transition function for E_MICS_CMD event
static void eventHandlerE_MICS_CMD(EVENT event)
{
	EVENT timeoutEvent;
	micsCmndToken = micsRxBuffer.token;
	(void)event;

	#ifdef CMDPR_PRINT_OUTPUT
		printf("Command Processor - eventHandlerE_MICS_CMD = token= %d / %x.\n", micsCmndToken, micsCmndToken);
	#endif


	#ifdef CMND_PERF_MON
		endTimeCommand = timerTicks();

		if((endTimeCommand - startTimeCommand) > 0)
		{
		   DBGI(f_cmdproc_commandprocessor, endTimeCommand - startTimeCommand);
		}
	#endif

	//Change the state to wait for a response...
	if (cmdprCurrentState == CMDPR_STATE_READY)
	{
		//Depending on what type of command, call the appropriate handler
		cmndTable[micsCmndToken].handler();

		cmdprCurrentState = CMDPR_STATE_WAIT_FOR_MICS_RC;

		//Set a timer to wait for a response
		timeoutEvent.eventID = E_MICS_RC_TIMEOUT;
		swTimerSet( SWTMR_MICS_RESPONSE_TIMEOUT, SWTMR_30_SECOND_TIMEOUT, timeoutEvent, false );
	}
	else
	{
		//Busy with another command...ignore the command
		cmdprCurrentState = CMDPR_STATE_READY;
	}

	return;
}


//Transition function for E_CMD_RESPONSE event
static void eventHandlerE_CMD_RESPONSE(EVENT event)
{
	EVENT sendFrameEvent;

	#ifdef CMDPR_PRINT_OUTPUT
		printf("Command Processor - eventHandlerE_CMD_RESPONSE\n");
	#endif

	if (event.eventData.eCommandResponseData.commandToken == micsCmndToken)
	{
		if (cmdprCurrentState == CMDPR_STATE_WAIT_FOR_MICS_RC)
		{
			//Cancel timeout for waiting for response
			swTimerCancel(SWTMR_MICS_RESPONSE_TIMEOUT);

			sendFrameEvent.eventID = E_MICS_SEND_FRAME;
			sendFrameEvent.eventData.eCommandResponseData.commandSource = MICS;
			sendFrameEvent.eventData.eCommandResponseData.responseCode = event.eventData.eCommandResponseData.responseCode;
			sendFrameEvent.eventData.eCommandResponseData.commandToken = event.eventData.eCommandResponseData.commandToken;
			sendFrameEvent.eventData.eCommandResponseData.responseSize = event.eventData.eCommandResponseData.responseSize;

			dispPutEvent(sendFrameEvent);

			if (cmdprCurrentState == CMDPR_STATE_WAIT_FOR_MICS_RC)
			{
				cmdprCurrentState = CMDPR_STATE_READY;
			}
		}
		else
		{
			//The only way we can get here is if we are waiting for a TETS response, but get MICS response
			#ifdef  CMDPR_PRINT_OUTPUT
				printf("Invalid Command Token in Response.\n");
			#endif
		}
	}
	else
	{
		#ifdef  CMDPR_PRINT_OUTPUT
			printf("BAD COMMAND TOKEN RESPONSE REC'D.\n");
		#endif
	}

	return;
}


//Transition function for E_MICS_TIMEOUT event
static void eventHandlerE_MICS_TIMEOUT(EVENT event)
{
	EVENT sendFrameEvent;
	(void)event;

	#ifdef CMDPR_PRINT_OUTPUT
		printf("Command Processor - eventHandlerE_MICS_TIMEOUT\n");
	#endif

	sendFrameEvent.eventID = E_MICS_SEND_FRAME;
	sendFrameEvent.eventData.eCommandResponseData.commandSource = MICS;
	sendFrameEvent.eventData.eCommandResponseData.responseCode = CMND_RESP_BUSY;
	sendFrameEvent.eventData.eCommandResponseData.commandToken = micsCmndToken;
	sendFrameEvent.eventData.eCommandResponseData.responseSize = 0;

	dispPutEvent(sendFrameEvent);

	//Reset the state
	if (cmdprCurrentState == CMDPR_STATE_WAIT_FOR_MICS_RC)
	{
		cmdprCurrentState = CMDPR_STATE_READY;
	}

	return;
}


/*
 *
 * PUBLIC API FUNCTIONS
 *
 */

//Central function to allow everybody to send a response the same way
void sendCommandResponse(RESPONSE responseCode, CMND_TOKEN token, uint16_t size)
{
	EVENT responseEvent;

	#ifdef CMDPR_PRINT_OUTPUT
		printf("Command Processor - sendCommandResponse\n");
	#endif

	responseEvent.eventID = E_CMD_RESPONSE;
	responseEvent.eventData.eCommandResponseData.commandSource = MICS;
	responseEvent.eventData.eCommandResponseData.responseCode = responseCode;
	responseEvent.eventData.eCommandResponseData.commandToken = token;
	responseEvent.eventData.eCommandResponseData.responseSize = size;

	dispPutEvent(responseEvent);

	return;
}

// Simplified routine for send just a command response when the size is 0
void sendCommandResponseBasic(RESPONSE responseCode)
{
	#ifdef CMND_PERF_MON
		startTimeStartResponse = timerTicks();
	#endif

	micsTxBuffer.responseCode = responseCode;
	sendCommandResponse(responseCode, micsRxBuffer.token, 0);
}

//Set arming for POP pairing
void setArmForPOPPairing(bool armDecision)
{
#ifdef CMDPR_PRINT_OUTPUT
	printf("Command Processor - setArmForPOPPairing\n");
#endif

	armedForPOPPairing = armDecision;

	if (armedForPOPPairing)
	{
		EVENT timeoutEvent;

		timeoutEvent.eventID = E_ARM_PAIR_TIMEOUT;
		swTimerSet( SWTMR_ARM_PAIR_TIMEOUT, SWTMR_60_SECOND_TIMEOUT, timeoutEvent, false );
	}

	return;
}


//Get current arming status for POP pairing
bool getArmForPOPPairing(void)
{
#ifdef CMDPR_PRINT_OUTPUT
	printf("Command Processor - getArmForPOPPairing\n");
#endif

	return armedForPOPPairing;
}
