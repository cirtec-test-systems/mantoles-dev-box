/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: Command Processor Unit. Queries from external.
 *
 ***************************************************************************/

#include "Queries.h"

//#define CMDTBL_PRINT_OUTPUT	// to turn on/off print debug output for Command Processor module
#ifdef  CMDTBL_PRINT_OUTPUT
#include <stdio.h>
#endif

#include <string.h>

#include "activePgm.h"
#include "BatteryMonitor.h"
#include "bgImp.h"
#include "cal.h"
#include "cdp.h"
#include "ChargeManager.h"
#include "CommandProcessor.h"
#include "const.h"
#include "counter.h"
#include "cpdata.h"
#include "dataStoreXpgIdentity.h"
#include "EventQueue.h"
#include "GenericProgram.h"
#include "lastresponse.h"
#include "log.h"
#include "mics.h"
#include "MICSCommandTable.h"
#include "NV.h"
#include "StimManager.h"
#include "pgm.h"
#include "PowerASIC.h"
#include "prgmDef.h"
#include "Stim.h"
#include "StimulationCommon.h"
#include "SystemHealthMonitor.h"
#include "trim.h"
#include "version.h"
#include "xpginfo.h"

#include "Common/Protocol/cmndDiagData.h"
#include "Common/Protocol/cmndStim.h"
#include "Common/Protocol/trim_list.h"
#include <intrinsics.h>
#include "DeviceManagementLayer/GenericProgram/gprgm_def.h"

#define CONVERSION_FACTOR_50MV 50u

extern GPRGM gp;

/*
 * All the query handlers
 */

//Response for TKN_GET_XPG_STATUS
void cmndHndlrGetXpgStatus(void)
{
	int i = 0;

	#ifdef CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrGetXpgStatus\n");
	#endif

    //Clear the buffer
    memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

	micsTxBuffer.txData.xpgStatus.activeError = getActiveError();
	micsTxBuffer.txData.xpgStatus.chargingError = getChargeError();

	//Clear any category 3 errors
	clearCategory3Error();

	if (isStimOn())
	{
		uint8_t status = GS_STIM_ACTIVE;

		if (isStimRamping())
		{
			status = status | GS_RAMPING;
		}

		if (isStimLockedout())
		{
			status = status | GS_INCR_LOCKOUT;
		}

		micsTxBuffer.txData.xpgStatus.generalStatus = status;
	}
	else
	{
		micsTxBuffer.txData.xpgStatus.generalStatus = 0x0;
	}

	micsTxBuffer.txData.xpgStatus.programSelectMap = pgm_GetProgramMap() & pgm_GetProgramEnabledMap();
	micsTxBuffer.txData.xpgStatus.selectedProgram = pgm_SelectedProgramNumber();
	micsTxBuffer.txData.xpgStatus.pulseSelectMap = pgm_GetPulseMap();

	if (micsTxBuffer.txData.xpgStatus.selectedProgram != 0)
	{
		for (i = 0; i < NUM_PULSES; i++)
		{
			if (pgm_IsPulseNumberValid(i))
			{
				micsTxBuffer.txData.xpgStatus.pulseVirtualAmplStepIndex[i] = actPgmGetActiveVirtualAmpStepIndex(i);
			}
			else
			{
				micsTxBuffer.txData.xpgStatus.pulseVirtualAmplStepIndex[i] = 0;
			}
		}
	}

	micsTxBuffer.txData.xpgStatus.xpgBatteryState = battMonGetXpgBattState();

	micsTxBuffer.txData.xpgStatus.stimState = (uint8_t) stimState();

	micsTxBuffer.txData.xpgStatus.reserved = 0;

	sendCommandResponse(GEN_SUCCESS, TKN_GET_XPG_STATUS, sizeof(micsTxBuffer.txData.xpgStatus));

	return;
}


//Response for TKN_READ_LOG
void cmndHndlrReadLog(void)
{
	uint8_t logNumber = micsRxBuffer.rxData.readLogData.logNum;
	uint32_t firstEntrySerialNumber = micsRxBuffer.rxData.readLogData.serial;
	uint16_t sizeOfBuffer = MAX_LOG_ENTRIES_RESPONSE;
	uint16_t remainingEntries;

#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrReadLog.\n");
#endif

	//Check if you have a valid log number...
	if ( micsRxBuffer.rxData.readLogData.logNum > 1)
	{
		sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_READ_LOG, 0);
		return;
	}

    //Clear the buffer
    memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

	micsTxBuffer.txData.readLogResponseData.logNum = logNumber;

	if (logNumber == 0)
	{
		logNormal(E_LOG_READ_ERROR_LOG,0);
	}
	else
	{
		logNormal(E_LOG_READ_EVENT_LOG,0);
	}

	logReadEventRecords(logNumber,
						&firstEntrySerialNumber,
						&micsTxBuffer.txData.readLogResponseData.log[0],
						&sizeOfBuffer, &remainingEntries);

	micsTxBuffer.txData.readLogResponseData.serial = firstEntrySerialNumber;
	micsTxBuffer.txData.readLogResponseData.count =  sizeOfBuffer;
	micsTxBuffer.txData.readLogResponseData.reserved = 0x00;

	sendCommandResponse(GEN_SUCCESS, TKN_READ_LOG, sizeof(READ_LOG_RESPONSE_DATA));
}


//Response for TKN_GET_PPC_CONST
void cmndHndlrGetPpcConst(void)
{
#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrGetPpcConst.\n");
#endif

	/// pointer to return data structure in response buffer
    CONFIG_DEVICE_PARAMETERS cdp;

    //Clear the buffer
    memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

    logNormal(E_LOG_QUERY_PPC_CONSTANTS,0);

    //if crcs are good, return the data
    if (!constIsRampTimeValid())
    {
        //data was corrupted - don't return data
    	sendCommandResponse(RESP_DATA_CORRUPTED, TKN_GET_PPC_CONST, 0);
    	dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
        return;
    }

    if (cdp_Get(&cdp) < 0)
    {
        //data was corrupted - don't return data
    	sendCommandResponse(RESP_DATA_CORRUPTED, TKN_GET_PPC_CONST, 0);
    	dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
    	return;
    }

    // Copy data into the response data buffer
    micsTxBuffer.txData.ppcConstResponseData.amplitudeSteps = cdp_GetAmplitudeSteps();;
    micsTxBuffer.txData.ppcConstResponseData.pulseWidthStep = cdp_GetPulseWidthStep();
    micsTxBuffer.txData.ppcConstResponseData.PPCOptions = cdp.ppcOptions;

    memcpy(micsTxBuffer.txData.ppcConstResponseData.ppcText, cdp.ppcText, PPC_TEXT_CHARS);

    //added ramp time and increment lockout time to data block
    micsTxBuffer.txData.ppcConstResponseData.rampTime = constRampTime();
    micsTxBuffer.txData.ppcConstResponseData.incrementLockoutMsecs = constLockoutTime();

	//transfer 64-bit integer that represents implant date
	memcpy(micsTxBuffer.txData.ppcConstResponseData.implantDateTime, cdp.implantDateTime, NUM_BYTES_IMPLANT_DATE);

	//and replacement interval months
	micsTxBuffer.txData.ppcConstResponseData.replacementIntervalMonths = cdp.replacementIntervalMonths;

	sendCommandResponse(GEN_SUCCESS, TKN_GET_PPC_CONST, sizeof(GET_PPC_CONST_RESPONSE_DATA));
}


//Response for TKN_GET_PRGM_NAMES
void cmndHndlrGetPgrmNames(void)
{
	RESPONSE resp;
#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrGetPgrmNames.\n");
#endif

    //Clear the buffer
    memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

    logNormal(E_LOG_QUERY_PROGRAM_NAMES,0);

    resp = pgm_GetPrgmNames(&micsTxBuffer.txData.pgrmNamesResponseData);
    micsTxBuffer.txData.pgrmNamesResponseData.selectableProgramMap = pgm_GetProgramMap() & pgm_GetProgramEnabledMap();

    sendCommandResponse(resp, TKN_GET_PRGM_NAMES, sizeof(GET_PRGM_NAMES_RESPONSE_DATA));
}

//Response for TKN_GET_PRGM_CONST
void cmndHndlrGetPgrmConst(void)
{
    GET_PRGM_CONST_RESPONSE_DATA* prgmData = &micsTxBuffer.txData.prgmConstResponseData;

	#ifdef  CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrGetPgrmConst.\n");
	#endif

    //Clear the buffer
    memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

    logNormal(E_LOG_QUERY_PROGRAM_CONSTANTS,0);

    //Check if we have valid data
    if (!constIsPrgmConstValid())
    {
    	sendCommandResponse(RESP_PROGRAM_CONSTANTS_CORRUPT, TKN_GET_PRGM_CONST, 0);
        dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
        return;
    }

    //Copy data into transmit buffer
    memcpy(prgmData, constGetPrgmConst(), sizeof(GET_PRGM_CONST_RESPONSE_DATA));

    //Send response
    sendCommandResponse(GEN_SUCCESS, TKN_GET_PRGM_CONST, sizeof(GET_PRGM_CONST_RESPONSE_DATA));
}

//Response for TKN_GET_XPG_IDENT
void cmndHndlrGetxPgIdentity(void)
{
	/// Cast a pointer to the current xPG Info Block
	const DS_XPG_IDENTITY *currentXpgInfo = getXpgInfo();

	#ifdef  CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrGetxPgIdentity.\n");
	#endif

    //Clear the buffer
    memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

    logNormal(E_LOG_QUERY_XPG_IDENTITY,0);

	//If CRC is good, return the data, otherwise return an error
	if (!isXpgInfoValid())
	{
		//data was corrupted - don't return data
		sendCommandResponse(RESP_DATA_CORRUPTED, TKN_GET_XPG_IDENT, 0);
		dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
		return;
	}

	//Transfer model number, serial number, and firmware version
	memcpy(micsTxBuffer.txData.xpgIdentResponseData.modelNumber, currentXpgInfo->xpgIdentity.xpgModelNumber, sizeof(micsTxBuffer.txData.xpgIdentResponseData.modelNumber));
	memcpy(micsTxBuffer.txData.xpgIdentResponseData.serialNumber, currentXpgInfo->xpgIdentity.xpgSerialNumber, sizeof(micsTxBuffer.txData.xpgIdentResponseData.serialNumber));
	memcpy(micsTxBuffer.txData.xpgIdentResponseData.firmwareVersion, FIRMWARE_VERSION, sizeof(micsTxBuffer.txData.xpgIdentResponseData.firmwareVersion));

	sendCommandResponse(GEN_SUCCESS, TKN_GET_XPG_IDENT, sizeof(GET_XPG_IDENT_RESPONSE_DATA));
}


//Command for TKN_CHARGING_CONTROL
void cmndHndlrChargeControl(void)
{
	uint8_t chrgStateAndTemperature;
	uint16_t rechargePower = 0;
	uint8_t returnValChrg;
	uint8_t returnValVolt;

	#ifdef CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrChargeControl\n");
	#endif

    //Clear the buffer
    memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

    //Get the data for the command
	#ifndef EPG
    	returnValChrg = chrgMgrGetChrgAndTempState(&chrgStateAndTemperature);
        returnValVolt = chargeGetRechargeVoltage(&rechargePower);
   	#else
    	returnValChrg = 0;
    	returnValVolt = 0;
	#endif

    //If data is good, return the data, otherwise return an error
    if ((returnValChrg == OK_RETURN_VALUE) && (returnValVolt == OK_RETURN_VALUE))
    {
    	micsTxBuffer.txData.chargingControlResponseData.ChargeStateTempStateBitMap = chrgStateAndTemperature;
    	micsTxBuffer.txData.chargingControlResponseData.voltageLevel = (uint8_t)(rechargePower/CONVERSION_FACTOR_50MV);

    	sendCommandResponse(GEN_SUCCESS, TKN_CHARGING_CONTROL, sizeof(CHARGING_CONTROL_RESPONSE_DATA));
    }
    else
    {
		sendCommandResponse(RESP_CHARGING_CONTROL_ERROR, TKN_CHARGING_CONTROL, 0);
    }
}


//Handler for TKN_GET_GEN_CAL
void cmndHndlrGetGenCal(void)
{
#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrGetGenCal.\n");
#endif

	GET_GENERAL_CALS_RESPONSE_DATA* calsData = &micsTxBuffer.txData.genCalsResponseData;

    //Clear the buffer
    memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

    logNormal(E_LOG_QUERY_GENERAL_CALIBRATION,0);

	if (!calIsGenCalsValid())
	{
		sendCommandResponse(RESP_DATA_CORRUPTED, TKN_GET_GEN_CAL, 0);
		dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
		return;
	}

	memcpy(calsData, calGetGenCals(), sizeof(GET_GENERAL_CALS_RESPONSE_DATA));
	sendCommandResponse(GEN_SUCCESS, TKN_GET_GEN_CAL, sizeof(GET_GENERAL_CALS_RESPONSE_DATA));
}

//Hander for TKN_GET_COUNTER
void cmndHndlrGetCounter(void)
{
	RESPONSE retVal;

	#ifdef  CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrGetCounter.\n");
	#endif

	// Range check
	if (!counterRangeCheckGetCounter(&micsRxBuffer.rxData.counterParams))
	{
		if (micsRxBuffer.rxData.counterParams.eventCounterID == 99)
		{
			micsTxBuffer.txData.counterResponseData.eventCounterID = 99;
			micsTxBuffer.txData.counterResponseData.eventCount = gp.complianceDACValue;
			sendCommandResponse(GEN_SUCCESS, TKN_GET_COUNTER, sizeof(GET_COUNTER_RESPONSE_DATA));
			return;
		}
		else if (micsRxBuffer.rxData.counterParams.eventCounterID == 98)
		{
			micsTxBuffer.txData.counterResponseData.eventCounterID = 98;
			micsTxBuffer.txData.counterResponseData.eventCount = gp.pulse[0].recRatio;
			sendCommandResponse(GEN_SUCCESS, TKN_GET_COUNTER, sizeof(GET_COUNTER_RESPONSE_DATA));
			return;
		}
		else
		{
			sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_GET_COUNTER, 0);
		}
		return;
	}

    //Clear the buffer
    memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

    switch(micsRxBuffer.rxData.counterParams.eventCounterID)
    {
		case COUNTER_PRECHARGE_COMPLETE:
			logNormal(E_LOG_QUERY_CURRENT_TO_VOLT_CNTR,0);
			break;
		case COUNTER_CHARGE_COMPLETE:
			logNormal(E_LOG_QUERY_CHARGE_COMPLETE_CNTR,0);
			break;
		case COUNTER_STORAGE_MODE:
			logNormal(E_LOG_QUERY_STORAGE_MODE_CNTR,0);
			break;
    }

    //Go read the counter value
    retVal = counterCmdGetCounter(&micsRxBuffer.rxData.counterParams, &micsTxBuffer.txData.counterResponseData);

    //Send the response back
	sendCommandResponse(retVal, TKN_GET_COUNTER, sizeof(GET_COUNTER_RESPONSE_DATA));
}

//Hander for TKN_ECHO_SHORT
void cmndHndlrEchoShort(void)
{
	#ifdef  CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrEchoShort.\n");
	#endif

    //Clear the buffer
    memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

    //Copy the data to transmit buffer
    memcpy(micsTxBuffer.txData.shortParams, micsRxBuffer.rxData.shortParams, sizeof(ECHO_SHORT_PARAMS));

    //Send it out
    sendCommandResponse(GEN_SUCCESS, TKN_ECHO_SHORT, sizeof(ECHO_SHORT_PARAMS));
}

//Handler for TKN_ECHO_LONG
void cmndHndlrEchoLong(void)
{
	#ifdef  CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrEchoLong.\n");
	#endif

    //Clear the buffer
    memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

    //Copy the data to transmit buffer
    memcpy(micsTxBuffer.txData.longParams, micsRxBuffer.rxData.longParams, sizeof(ECHO_LONG_PARAMS));

    //Send it out
    sendCommandResponse(GEN_SUCCESS, TKN_ECHO_LONG, sizeof(ECHO_LONG_PARAMS));
}

//Handler for TKN_GET_CHANNEL_CALS
void cmndHndlrGetChannelCals(void)
{
#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrGetChannelCals.\n");
#endif

	//Clear the buffer
	memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

	logNormal(E_LOG_QUERY_CHANNEL_CALIBRATION,0);

	if (!calIsChannelCalsValid())
	{
		sendCommandResponse(RESP_CHANNEL_CALS_CORRUPT, TKN_GET_CHANNEL_CALS, 0);
		dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
		return;
	}

	calGetChannelCals(&micsTxBuffer.txData.channelCalParams, micsRxBuffer.rxData.getChannelParams.index);
	sendCommandResponse(GEN_SUCCESS, TKN_GET_CHANNEL_CALS, sizeof(CHANNEL_CALS));
}

//Handler for TKN_GET_STIM_ASIC_HV_CALS
void cmndHndlrGetStimHvCals(void)
{
	#ifdef  CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrGetStimHvCals.\n");
	#endif

	//Clear the buffer
	memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

	logNormal(E_LOG_QUERY_HV_CALIBRATION,0);

	//Check if valid, return error if not
	if (!calIsHVCalsValid())
	{
		sendCommandResponse(RESP_STIM_ASIC_HV_CALS_CORRUPT, TKN_GET_STIM_ASIC_HV_CALS, 0);
		dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
		return;
	}

	//Copy out the HV Cals
	memcpy(&micsTxBuffer.txData.hvCalParams, calGetHVCals(), sizeof(HV_CAL_PARAMS));

	//Send out the response
	sendCommandResponse(GEN_SUCCESS, TKN_GET_STIM_ASIC_HV_CALS, sizeof(HV_CAL_PARAMS));
}


//Handler for TKN_GET_TRIM_LIST
void cmndHndlrGetTrimList(void)
{
#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrGetTrimList.\n");
#endif
	//Clear the buffer
	memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

	logNormal(E_LOG_QUERY_TRIM_LISTS,0);

	switch(micsRxBuffer.rxData.trimListParams.listID)
	{
		case TRIM_PLUTO:
		case TRIM_ZL:
			//Check for valid trim list ID
			if (!trim_RangeCheckGetTrimList(&micsRxBuffer.rxData.trimListParams))
			{
				sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_GET_TRIM_LIST, 0);
				return;
			}

			//Check for valid trim list data
			if (!trimIsListValid((enum TRIM_LIST_ID)micsRxBuffer.rxData.trimListParams.listID))
			{
				sendCommandResponse(RESP_DATA_CORRUPTED, TKN_GET_TRIM_LIST, 0);
				dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
				return;
			}

			memcpy(&micsTxBuffer.txData.trimListParams, trimGetList((enum TRIM_LIST_ID)micsRxBuffer.rxData.trimListParams.listID), sizeof(GET_TRIM_LIST_RESPONSE_DATA));
			sendCommandResponse(GEN_SUCCESS, TKN_GET_TRIM_LIST, sizeof(GET_TRIM_LIST_RESPONSE_DATA));

			break;

		case TRIM_SATURN:
			//Check for valid trim list ID
			if (!trim_RangeCheckGetSaturnTrimList(&micsRxBuffer.rxData.trimListParams))
			{
				sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_GET_TRIM_LIST, 0);
				return;
			}

			//Check for valid trim list data
			if (!trimIsListValid((enum TRIM_LIST_ID)micsRxBuffer.rxData.trimListParams.listID))
			{
				sendCommandResponse(RESP_DATA_CORRUPTED, TKN_GET_TRIM_LIST, 0);
				dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
				return;
			}

			memcpy(&micsTxBuffer.txData.trimListParams, trimGetList((enum TRIM_LIST_ID)micsRxBuffer.rxData.trimListParams.listID), sizeof(GET_TRIM_LIST_RESPONSE_DATA));
			sendCommandResponse(GEN_SUCCESS, TKN_GET_TRIM_LIST, sizeof(GET_TRIM_LIST_RESPONSE_DATA));

			break;

		case TRIM_ZL_FACTORY:
					//Check for valid trim list ID
					if (!trim_RangeCheckGetTrimList(&micsRxBuffer.rxData.trimListParams))
					{
						sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_GET_TRIM_LIST, 0);
						return;
					}

					//Check for valid trim list data. Override the id here to use the same ID as the normal trim list.
					if (!trimIsListBackupValid(TRIM_ZL))
					{
						sendCommandResponse(RESP_DATA_CORRUPTED, TKN_GET_TRIM_LIST, 0);
						dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
						return;
					}

					memcpy(&micsTxBuffer.txData.trimListParams, trimGetListBackup(TRIM_ZL), sizeof(GET_TRIM_LIST_RESPONSE_DATA));
					//override the trim id in the response to set it back to the requested id.
					micsTxBuffer.txData.trimListParams.listID = TRIM_ZL_FACTORY;

					sendCommandResponse(GEN_SUCCESS, TKN_GET_TRIM_LIST, sizeof(GET_TRIM_LIST_RESPONSE_DATA));

					break;

		default:
			sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_GET_TRIM_LIST, 0);
			break;
	}

	return;
}

//Handler for TKN_GET_TEST_STIM
void cmndHndlrGetTestStim(void)
{
	sendCommandResponse(testStimCmdGet(&micsTxBuffer.txData.testStimData), TKN_GET_TEST_STIM, sizeof(GET_TEST_STIM_RESPONSE_DATA));
}

//Handler for TKN_DIAG_DATA
void cmndHndlrGetDiagData(void)
{
	uint16_t temperature = 0;
	TEMPERATURE_DATA temperatureData;

	#ifdef  CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrGetDiagData.\n");
	#endif

	//Clear the buffer
	memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

	micsTxBuffer.txData.diagDataResponse.select = micsRxBuffer.rxData.diagDataParams.select;
	micsTxBuffer.txData.diagDataResponse.command = micsRxBuffer.rxData.diagDataParams.command;

	switch(micsRxBuffer.rxData.diagDataParams.select)
	{
		case DIAG_CAPACITOR:
			logNormal(E_LOG_QUERY_OUTPUT_CAP_DIAG,0);
			getOutputCapCheckData((bool)(micsTxBuffer.txData.diagDataResponse.command), (OUTPUT_CAPACITOR_CHECK_DATA *)(micsTxBuffer.txData.diagDataResponse.data));
			break;
		case DIAG_BG_IMPEDANCE:
			getBackgroundImpedanceData((bool)(micsTxBuffer.txData.diagDataResponse.command), (BACKGROUND_IMPEDANCE_CHECK_DATA *)(micsTxBuffer.txData.diagDataResponse.data));
			logNormal(E_LOG_QUERY_BG_IMPEDANCE_DIAG,0);
			break;
		case DIAG_VBAT:
			logNormal(E_LOG_QUERY_VBAT_DIAG,0);
			*(uint16_t*)micsTxBuffer.txData.diagDataResponse.data = battMonGetLastSampledBatteryVoltage();
			break;
		case DIAG_THERMISTOR:
			logNormal(E_LOG_QUERY_THERMISTOR_DIAG,0);
			chargeGetTemperature(&temperature, &temperatureData.thermOffset, &temperatureData.thermInput, &temperatureData.thermBias);
			memcpy(&micsTxBuffer.txData.diagDataResponse.data[0], &temperatureData, sizeof(TEMPERATURE_DATA));
			break;
		default:
			break;
	}

	if (micsRxBuffer.rxData.diagDataParams.command == DIAG_READ_CLEAR)
	{
		switch(micsRxBuffer.rxData.diagDataParams.select)
		{
			case DIAG_CAPACITOR:
			case DIAG_BG_IMPEDANCE:
			case DIAG_VBAT:
				battMonClearLastSampledBatteryVoltage();
				break;
			case DIAG_THERMISTOR:
				#ifndef EPG
					chrgMgrClearLastTemperatureDataReading();
				#endif
				break;
			default:
				break;
		}
	}

	sendCommandResponse(GEN_SUCCESS, TKN_DIAG_DATA, sizeof(DIAG_DATA_RESPONSE_DATA ));
}


//Command handler for TKN_GET_PRGM_DEF
void cmndHndlrGetPrgmDef(void)
{
	RESPONSE response;

	#ifdef  CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrGetPrgmDef.\n");
	#endif

	//Clear the buffer
	memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

	logNormal(E_LOG_QUERY_PROGRAM_DEF,0);

	if (!pgm_RangeCheckGetPrgmDef(&micsRxBuffer.rxData.prgmDefinitionParams))
	{
		sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_GET_PRGM_DEF, 0);
		return;
	}

	response = pgm_GetPrgmDef(&micsRxBuffer.rxData.prgmDefinitionParams, &micsTxBuffer.txData.programDefinitionResponseData);

	//Active Error Codes are internally stored in disabled byte.  However SWEX 0085 requires only 0 or 1 for disabled flag.
	if (micsTxBuffer.txData.programDefinitionResponseData.programDef.disabled > 0)
	{
		micsTxBuffer.txData.programDefinitionResponseData.programDef.disabled = 1;
	}

	if (response != GEN_SUCCESS)
	{
		sendCommandResponse(response, TKN_GET_PRGM_DEF, 0);
	}
	else
	{
		sendCommandResponse(response, TKN_GET_PRGM_DEF, sizeof(GET_PRGM_DEF_RESPONSE_DATA));
	}
}


//Handler for TKN_GET_PULSE_CONST
void cmndHndlrGetPulseConst(void)
{
	GET_PULSE_CONST_RESPONSE_DATA* pulseData = &micsTxBuffer.txData.pulseConstResponseData;

	#ifdef  CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrGetPgrmConst.\n");
	#endif

    //Clear the buffer
    memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

    logNormal(E_LOG_QUERY_PULSE_CONSTANTS,0);

    //Check if we have valid data
    if (!constIsPulseConstValid())
    {
    	sendCommandResponse(RESP_PULSE_CONSTANTS_CORRUPT, TKN_GET_PULSE_CONST, 0);
        dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
        return;
    }

    //Copy data into transmit buffer
    memcpy(pulseData, constGetPulseConst(), sizeof(GET_PULSE_CONST_RESPONSE_DATA));

    //Send response
    sendCommandResponse(GEN_SUCCESS, TKN_GET_PULSE_CONST, sizeof(GET_PULSE_CONST_RESPONSE_DATA));
}


//Handler for TKN_GET_CONFIG_DEVICE_PARAMS
void cmndHndlrGetCfgDevParams(void)
{
	int response;

	#ifdef  CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrGetCfgDevParams.\n");
	#endif

	//Clear the buffer
	memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

	logNormal(E_LOG_QUERY_CDP,0);

	response = cdp_Get(&micsTxBuffer.txData.configParamResponseData);

	if (response == OK_RETURN_VALUE)
	{
		sendCommandResponse(GEN_SUCCESS, TKN_GET_CONFIG_DEVICE_PARAMS, sizeof(CONFIG_DEVICE_PARAMETERS));
	}
	else
	{
		sendCommandResponse(RESP_DATA_CORRUPTED, TKN_GET_CONFIG_DEVICE_PARAMS, 0);
		dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
	}

	return;
}


//Handler for TKN_GET_PULSE_WIDTHS
void cmndHndlrGetPulseWidths(void)
{
	PRGM_DEF programDefinition;
	int i;
	RESPONSE response;
	int8_t retVal;

	#ifdef  CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrGetPulseWidths.\n");
	#endif

	//Clear the buffer
	memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

	logNormal(E_LOG_QUERY_PULSE_WIDTHS,0);

	//Get the current program definition...this call sets a response code
	retVal = pgm_GetCurInternalPrgmDef(&programDefinition);

	if (retVal == OK_RETURN_VALUE)
	{
		micsTxBuffer.txData.pulseWidthsResponseData.definedPulseMap = programDefinition.pulseValid;

		for (i = 0; i < NUM_PULSES; i++)
		{
			micsTxBuffer.txData.pulseWidthsResponseData.pulseWidth[i] = actPgmGetActivePulseWidth(i);
		}

		micsTxBuffer.txData.pulseWidthsResponseData.reserved = 0;

		sendCommandResponse(GEN_SUCCESS, TKN_GET_PULSE_WIDTHS, sizeof(GET_PULSE_WIDTHS_RESPONSE_DATA));
	}
	else
	{
		//Go get the response code
		response = getLastResponseCode();

		sendCommandResponse(response,TKN_GET_PULSE_WIDTHS,0);
		return;
	}
}


//Handler for TKN_GET_PRGM_FREQ
void cmndHndlrGetPrgmFreq(void)
{
	#ifdef  CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrGetPrgmFreq.\n");
	#endif

	uint8_t prgmNumber = pgm_SelectedProgramNumber();

	//Clear the buffer
	memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

	logNormal(E_LOG_QUERY_PRGM_FREQUENCY,0);

	if (prgmNumber != 0)
	{
		micsTxBuffer.txData.programFreqResponseData.programFrequencyIndex = actPgmGetActiveFreq();
		sendCommandResponse(GEN_SUCCESS, TKN_GET_PRGM_FREQ, sizeof(GET_PRGM_FREQ_RESPONSE_DATA));
	}
	else
	{
		sendCommandResponse(RESP_NO_PROGRAM_SELECTED, TKN_GET_PRGM_FREQ, 0);
	}
}


//Handler for TKN_GET_POP_CONST
void cmndHndlrGetPopConst(void)
{
	/// pointer to return data structure in response buffer
    CONFIG_DEVICE_PARAMETERS cdp;
    RESPONSE respCode;

#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrGetPopConst.\n");
#endif

    //Clear the buffer
    memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

    logNormal(E_LOG_QUERY_POP_CONSTANTS,0);

    //if crcs are good, return the data
    if (!constIsRampTimeValid())
    {
        //data was corrupted - don't return data
    	sendCommandResponse(RESP_DATA_CORRUPTED, TKN_GET_POP_CONST, 0);
    	dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
        return;
    }

    if (cdp_Get(&cdp) < 0)
    {
        //data was corrupted - don't return data
    	sendCommandResponse(RESP_DATA_CORRUPTED, TKN_GET_POP_CONST, 0);
    	dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
    	return;
    }

    // Copy data into the response data buffer
    micsTxBuffer.txData.popConstResponseData.amplitudeSteps = cdp.amplitudeSteps;
    micsTxBuffer.txData.popConstResponseData.incrementLockoutMsecs = constLockoutTime();
    micsTxBuffer.txData.popConstResponseData.reservedPoPConst = 0x00;
    respCode = constGetRampTime(&micsTxBuffer.txData.popConstResponseData.rampTime);

	if (respCode == GEN_SUCCESS)
	{
		sendCommandResponse(GEN_SUCCESS, TKN_GET_POP_CONST, sizeof(GET_POP_CONST_RESPONSE_DATA));
	}
	else
	{
		sendCommandResponse(respCode, TKN_GET_POP_CONST, 0);
	}
}


//Handler for TKN_GET_LEAD_LIMITS
void cmndHndlrGetLeadLimits(void)
{
	RESPONSE retVal;

#ifdef  CMDTBL_PRINT_OUTPUT
	printf("Command Processor - cmndHndlrGetLeadLimist.\n");
#endif

    //Clear the buffer
    memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

    logNormal(E_LOG_QUERY_LEAD_LIMITS,0);

    //Read the value...
	retVal = constGetLeadLimits(&micsTxBuffer.txData.leadLimitResponseData);

	//Send the response
	sendCommandResponse(retVal, TKN_GET_LEAD_LIMITS, sizeof(LEAD_LIMIT_PARAMS));
}



//Handler for TKN_GET_CP_DATA
void cmndHndlrGetCpData(void)
{
	int8_t retVal;

	#ifdef  CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrGetCpData.\n");
	#endif

	//Clear the buffer
    memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

	logNormal(E_LOG_QUERY_CP_DATA,0);

	if (micsRxBuffer.rxData.cpDataParams.block >= NUM_CP_DATA)
	{
		sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_GET_CP_DATA, 0);
		return;
	}

	retVal = cpDataRead(micsRxBuffer.rxData.cpDataParams.block, micsTxBuffer.txData.cpDataResponseData.data);

	//Check if read failed
	if (retVal == ERROR_RETURN_VALUE)
	{
		sendCommandResponse(RESP_READ_FAILED, TKN_GET_CP_DATA, 0);
		return;
	}

	//Fill in rest of data
	micsTxBuffer.txData.cpDataResponseData.block = micsRxBuffer.rxData.cpDataParams.block;
	micsTxBuffer.txData.cpDataResponseData.filler = 0;

	//Send response
	sendCommandResponse(GEN_SUCCESS, TKN_GET_CP_DATA, sizeof(GET_CP_DATA_RESPONSE_DATA));
}


//Handler for TKN_GET_RAMP_TIME
void cmndHndlrGetRampTime(void)
{
	#ifdef  CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrGetRampTime.\n");
	#endif

	//Clear the buffer
	memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

	logNormal(E_LOG_QUERY_RAMP_TIME,0);

	if (constIsRampTimeValid())
	{
		sendCommandResponse(constGetRampTime(&micsTxBuffer.txData.rampTimeResponseData.rampTime), TKN_GET_RAMP_TIME, sizeof(GET_RAMP_TIME_RESPONSE_DATA));
	}
	else
	{
		sendCommandResponse(RESP_RAMP_TIME_CORRUPT, TKN_GET_RAMP_TIME, sizeof(GET_RAMP_TIME_RESPONSE_DATA));
	}
}

//Handler for TKN_GET_VBAT
void cmndHndlrGetVBAT(void)
{
	#ifdef  CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrGetVBAT.\n");
	#endif

	//Clear the buffer
	memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

	micsTxBuffer.txData.vbatResponseData.vbat = battMonGetCurrentBatteryVoltage();

	sendCommandResponse(GEN_SUCCESS,TKN_GET_VBAT,sizeof(GET_VBAT_RESPONSE_DATA));
}


//Handler for TKN_GET_TITR_STIM
void cmndHndlrGetTitrStim(void)
{
	#ifdef  CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrSetTitrStim.\n");
	#endif

	//Clear the buffer
	memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

	logNormal(E_LOG_QUERY_TITRATION_STIM,0);

	sendCommandResponse(titrStimCmdGet(&micsTxBuffer.txData.titrStimResponseData), TKN_GET_TITR_STIM, sizeof(GET_TITR_STIM_RESPONSE_DATA));
}

//Handler for TKN_READ_MEMORY
void cmndHndlrReadMemory(void)
{
	uint32_t boundaries[6] = {0x01000000, 0x02000000, 0x10000000, 0x20000000, 0x30000000, 0x40000000};
	int i = 0;
	uint8_t addressSpace;
	uint32_t physicalAddress;
	int retStatus = OK_RETURN_VALUE;

	#ifdef  CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrReadMemory.\n");
	#endif

	//Clear the buffer
	memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

	logNormal(E_LOG_READ_MEMORY_REQUESTED,0);

	//Check for invalid address spaces...
	if (micsRxBuffer.rxData.readMemoryParams.address < 0x01000000 || micsRxBuffer.rxData.readMemoryParams.address > 0x50000000)
	{
		sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_READ_MEMORY, 0);
		return;
	}

	//Can't write more than MAX_MEMORY_WRITE_SIZE
	if (micsRxBuffer.rxData.readMemoryParams.length > MAX_MEMORY_WRITE_SIZE)
	{
		sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_READ_MEMORY, 0);
		return;
	}

	//Check the range, to make sure it doesn't cross boundaries
	for (i = 0; i < 6; i++)
	{
		if (micsRxBuffer.rxData.readMemoryParams.address < boundaries[i])
		{
			if (micsRxBuffer.rxData.readMemoryParams.address + micsRxBuffer.rxData.writeMemoryParams.length > boundaries[i])
			{
				sendCommandResponse(CMND_RESP_INVALID_PARAM, TKN_READ_MEMORY, 0);
				return;
			}
		}
	}

	addressSpace = micsRxBuffer.rxData.readMemoryParams.address >> 24 & 0xFF;
	physicalAddress = micsRxBuffer.rxData.readMemoryParams.address & 0x00FFFFFF;

	//Read data out based on device
	switch (addressSpace)
	{
		case 0x01://MSP430 Byte Access
		{
			uint8_t *p = micsTxBuffer.txData.readMemoryResponseData.data;
			for (i = 0; i < micsRxBuffer.rxData.readMemoryParams.length; i += sizeof(*p))
			{
				*p++ = _data20_read_char(physicalAddress);
				physicalAddress += sizeof(*p);
			}
			break;
		}
		case 0x02://MSP430 Word Access
		{
			uint16_t *p = (void *)micsTxBuffer.txData.readMemoryResponseData.data;
			for (i = 0; i < micsRxBuffer.rxData.readMemoryParams.length; i += sizeof(*p))
			{
				*p++ = _data20_read_short(physicalAddress);
				physicalAddress += sizeof(*p);
			}
			break;
		}
		case 0x10://NVRAM
			retStatus = OK_RETURN_VALUE;

			if (nvStart() < 0)
			{
				retStatus = ERROR_RETURN_VALUE;
			}
			else
			{
				nvRead((NVADDR)physicalAddress, &micsTxBuffer.txData.readMemoryResponseData.data[0], micsRxBuffer.rxData.readMemoryParams.length);
			}

			if (nvStop() < 0)
			{
				retStatus = ERROR_RETURN_VALUE;
			}
			break;
		case 0x20://Power ASIC
			for (i = 0; i < micsRxBuffer.rxData.readMemoryParams.length; i++)
			{
				micsTxBuffer.txData.readMemoryResponseData.data[i] = pwrPeek((uint8_t)physicalAddress++);
			}
			break;
		case 0x30://Stim ASIC
			stimReadRegisters((uint16_t)physicalAddress, &micsTxBuffer.txData.readMemoryResponseData.data[0], micsRxBuffer.rxData.readMemoryParams.length);
			break;
		case 0x40://MICS (ZL7010x)
			for (i = 0; i < micsRxBuffer.rxData.readMemoryParams.length; i++)
			{
				micsTxBuffer.txData.readMemoryResponseData.data[i] = micsPeek((uint8_t)physicalAddress++);
			}
			break;
		default://Anything else
			retStatus = ERROR_RETURN_VALUE;
			break;
	}


	if (retStatus == OK_RETURN_VALUE)
	{
		micsTxBuffer.txData.readMemoryResponseData.address = micsRxBuffer.rxData.readMemoryParams.address;
		micsTxBuffer.txData.readMemoryResponseData.length = micsRxBuffer.rxData.readMemoryParams.length;
		sendCommandResponse(GEN_SUCCESS, TKN_READ_MEMORY, sizeof(READ_MEMORY_RESPONSE_DATA));
	}
	else
	{
		sendCommandResponse(RESP_READ_FAILED, TKN_READ_MEMORY, 0);
	}
}


//Handler for TKN_GET_BG_IMPEDANCE
void cmndHndlrGetBgImpedance(void)
{
	RESPONSE response;

	#ifdef  CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrGetBgImpedance.\n");
	#endif

	//Clear the buffer
	memset(&micsTxBuffer, 0, sizeof(micsTxBuffer));

	logNormal(E_LOG_QUERY_BG_IMPEDANCE_PARAMS,0);

	response = bgImpGetBackgroundImpedanceParams(&micsTxBuffer.txData.bgImpedanceResponseData);

	if (response == GEN_SUCCESS)
	{
		sendCommandResponse(GEN_SUCCESS, TKN_GET_BG_IMPEDANCE, sizeof(BACKGROUND_IMPEDANCE_PARAMS));
	}
	else
	{
		sendCommandResponse(response, TKN_GET_BG_IMPEDANCE, 0);
	}
}

//Handler for TKN_GET_LOG_RANGE
void cmndHndlrGetLogRange(void)
{
	#ifdef  CMDTBL_PRINT_OUTPUT
		printf("Command Processor - cmndHndlrGetLogRange.\n");
	#endif

	micsTxBuffer.txData.getLogRangeResponseData.logNum = micsRxBuffer.rxData.getLogRangeParams.logNum;
	micsTxBuffer.txData.getLogRangeResponseData.maxSerialNum = logGetLastSerialNum(micsRxBuffer.rxData.getLogRangeParams.logNum);
	micsTxBuffer.txData.getLogRangeResponseData.minSerialNum = logGetFirstSerialNum(micsRxBuffer.rxData.getLogRangeParams.logNum);

	sendCommandResponse(GEN_SUCCESS, TKN_GET_LOG_RANGE, sizeof(GET_LOG_RANGE_RESPONSE_DATA));
}
