/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *	
 *	@file Description: Generic program implementation 
 *	
 ***************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <math.h>
#include <stdbool.h>

#include "DeviceDriverLayer/Common/msp430f2618.h"
#include "DeviceDriverLayer/Common/bits.h"

#include "system.h"
#include "log.h"
#include "Stim.h"
#include "ADC.h"
#include "const.h"
#include "cal.h"
#include "delay.h"

#include "GenericProgram.h"
#include "gprgm_def.h"
#include "PhaseCounter.h"

#include "lastresponse.h"

#define CBC_SINK_CURRENT	1000
#define PERCENT 			100

GPRGM 	gp;
bool 	singleStep = false;		// set to true to keep stim asic idle and examine registers

extern unsigned 	pgmNum ;

static CHAN calculateChannelCBCSink(int n, uint16_t amplitude)
{
	CHAN 		chan;
	uint16_t 	dacCal;
	uint32_t 	amp;
	uint16_t 	channelCalCurrent;

	dacCal = calLookUpSinkingCal(gp.wf_ref, n);

	switch(gp.wf_ref)
	{
	case CHANNEL_MAX_AMP_3:
		channelCalCurrent = 3000;
		break;
	case CHANNEL_MAX_AMP_6:
		channelCalCurrent = 6000;
		break;
	case CHANNEL_MAX_AMP_10:
		channelCalCurrent = 9000;
		break;
	default:
		channelCalCurrent = 15000;
	}

	amp = 	(uint32_t)amplitude * (uint32_t)dacCal;
	amp /= 	(uint32_t)channelCalCurrent;

	chan.amp = (uint16_t)amp;
	chan.source = false;
	chan.uncontrolled = false;

	return(chan);
}

static CHAN calculateChannel(int n, int krec, uint16_t amplitude, int8_t ampPercent, bool ussChannel, bool raw, bool activeRecovery)
// amplitude in uA
{
	CHAN 	chan;
	uint16_t dacCal;
	uint32_t amp;
	uint16_t channelCalCurrent;

	if (ussChannel)
	{
		chan.amp = 0;
		chan.uncontrolled = true;
		// chan.source applies only to controlled-current channels and therefore does not need to be set.
	}
	else {
		// section 4.1.3.4 of IPG Functional Spec
		if(activeRecovery && ((krec == 5 && amplitude < 50) || (krec == 4 && amplitude < 40) || (krec == 3 && amplitude < 30)))
		{
			chan.amp = 0;
			chan.uncontrolled = false;
		}
		else
		{
			// Configure the channel polarity
			if (ampPercent < 0)
			{
				ampPercent = -ampPercent;
				chan.source = false;
				dacCal = calLookUpSinkingCal(gp.wf_ref, n);
			}
			else
			{
				chan.source = true;
				dacCal = calLookUpSourcingCal(gp.wf_ref, n);
			}
			if (raw)
			{
				// In raw mode, we run the channel as if it had the ideal LSB size of CHANNEL_CAL_RAW_LSB.
				// This takes the place of actual calibration.
				dacCal = CHANNEL_CAL_CURRENT / CHANNEL_CAL_RAW_LSB;
			}

			switch(gp.wf_ref)
			{
			case CHANNEL_MAX_AMP_3:
				channelCalCurrent = 3000;
				break;
			case CHANNEL_MAX_AMP_6:
				channelCalCurrent = 6000;
				break;
			case CHANNEL_MAX_AMP_10:
				channelCalCurrent = 9000;
				break;
			default:
				channelCalCurrent = 15000;
			}

			// Calculate the channel amplitude
			// channel amplitude = [total_amplitude * (ampPercent/100)](uA) * [dacCal/calibrationCurrent](bits/uA) * [1/Krec](scale factor) = bits for MDAC
			// NOTE: because dacCal will be different for source and sink, the actual amplitude values will be different even when both are the same channel percentage

			amp = ((long)amplitude * ampPercent * dacCal) / ((long)krec * channelCalCurrent * PERCENT);

			chan.amp = (uint16_t)amp;
			chan.uncontrolled = false;
		}
	}

	return chan;
}

static uint8_t waveformToWav(enum WAVEFORM waveform)
{
	switch (waveform)
	{
	case WAVEFORM_RAM0:
		return WAV_RAM1;
	case WAVEFORM_RAM1:
		return WAV_RAM2;
	case WAVEFORM_RECT:
	default:
		return stimMdl_PgmNum();
	}
}

#ifndef STIM_NO_MITIGATION
static int gprgm_LoadActiveRecoveryPhase(int gpulse, uint16_t pulseWidth, uint16_t delay, bool chargeBalance, bool raw, bool pause, bool sync, bool cal)
{
	uint8_t 		phase;			// Phase to be written
	PHASE 			phx;			// Phase configuration structure
	uint16_t 		amplitude;
	int8_t const 	*ampPercent;
	bool const 		*ussChannel;
	int 			i;
	int 			krec;

	phase = stimMdl_NextPhase();

	krec = gp.pulse[gpulse].recRatio;

	phx.uSource 		= !gp.pulse[gpulse].recUSource;
	phx.pulseWidth 		= pulseWidth * krec;
	phx.delay 			= delay;
	phx.wav 			= waveformToWav(gp.pulse[gpulse].recWaveform);
	phx.hvOff 			= false;
	phx.chargeBalance 	= chargeBalance;		// cbs switches are typically on for active recovery
	phx.pause			= pause;
	phx.sync			= sync;
	phx.calibrate		= cal;
	phx.repetition		= 0;

	amplitude 	= gp.pulse[gpulse].amplitude;
	ampPercent 	= gp.pulse[gpulse].ampPercent;
	ussChannel 	= gp.pulse[gpulse].ussChannel;

	for (i = 0; i < STIM_CHANNELS; i++)
	{
		phx.chan[i] = calculateChannel(i, krec, amplitude, -(int)ampPercent[i], ussChannel[i], raw, true);
	}

	return stimConfigPhase(phase, &phx, true);
}

static int gprgm_LoadActiveRecoveryPhaseLast(int gpulse, uint16_t pulseWidth, uint16_t delay, bool chargeBalance, bool raw, bool pause, bool sync, bool cal)
{
	uint8_t 		phase;			// Phase to be written
	PHASE 			phx;			// Phase configuration structure
	uint16_t 		amplitude;
	int8_t const 	*ampPercent;
	bool const 		*ussChannel;
	int 			i;
	int 			krec;

	// recovery phase with short delay and cbc switches OPEN
	phase = stimMdl_NextPhase();

	krec = gp.pulse[gpulse].recRatio;

	phx.uSource 		= !gp.pulse[gpulse].recUSource;
	phx.pulseWidth 		= pulseWidth * krec;
	phx.delay 			= ACTIVE_RECOVERY_CBC_CORRECTION_PULSE_DELAY;
	phx.wav 			= waveformToWav(gp.pulse[gpulse].recWaveform);
	phx.hvOff 			= false;
	phx.chargeBalance 	= false;		// cbs switches OFF
	phx.pause			= pause;
	phx.sync			= sync;
	phx.calibrate		= cal;
	phx.repetition		= 0;

	amplitude 	= gp.pulse[gpulse].amplitude;
	ampPercent 	= gp.pulse[gpulse].ampPercent;
	ussChannel 	= gp.pulse[gpulse].ussChannel;

	for (i = 0; i < STIM_CHANNELS; i++)
	{
		phx.chan[i] = calculateChannel(i, krec, amplitude, -(int)ampPercent[i], ussChannel[i], raw, true);
	}

	stimConfigPhase(phase, &phx, true);

	// additional phase with short pulse where all inactive channels are sinking current to help prevent clock errors
	phase = stimMdl_NextPhase();

	krec = gp.pulse[gpulse].recRatio;

	phx.uSource 		= !gp.pulse[gpulse].recUSource;
	phx.pulseWidth 		= ACTIVE_RECOVERY_CBC_CORRECTION_PULSE_WIDTH;
	phx.delay 			= delay- (ACTIVE_RECOVERY_CBC_CORRECTION_PULSE_DELAY + ACTIVE_RECOVERY_CBC_CORRECTION_PULSE_WIDTH);	// compensate for delay time in the active recovery phase above and pulse width for this phase
	//phx.wav 			= waveformToWav(gp.pulse[gpulse].recWaveform);
	phx.wav 			= 5;  // use an unused waveform register that is always set to 255
	phx.hvOff 			= false;
	phx.chargeBalance 	= chargeBalance;		// cbs switches are typically on for active recovery
	phx.pause			= pause;
	phx.sync			= sync;
	phx.calibrate		= cal;
	phx.repetition		= 0;

	stimSetRectLevel(5, 255);

	for (i = 0; i < STIM_CHANNELS; i++)
	{
		if(gp.pulse[gpulse].ampPercent[i] == 0)
		{
			// unused channels sink a small amount of current
			phx.chan[i] = calculateChannelCBCSink(i, CBC_SINK_CURRENT);
		}
		else
		{
			phx.chan[i].amp = 0;	// per hardware teams instructions - sink current on all unused channels during recovery phase
			phx.chan[i].source = false;
			phx.chan[i].uncontrolled = false;
		}
	}

	return stimConfigPhase(phase, &phx, false);
}
#else
static int gprgm_LoadActiveRecoveryPhase(int gpulse, uint16_t pulseWidth, uint16_t delay, bool chargeBalance, bool raw, bool pause, bool sync, bool cal)
{
	uint8_t 		phase;			// Phase to be written
	PHASE 			phx;			// Phase configuration structure
	uint16_t 		amplitude;
	int8_t const 	*ampPercent;
	bool const 		*ussChannel;
	int 			i;
	int 			krec;

	phase = stimMdl_NextPhase();

	krec = gp.pulse[gpulse].recRatio;

	phx.uSource 		= !gp.pulse[gpulse].recUSource;
	phx.pulseWidth 		= pulseWidth * krec;
	phx.delay 			= delay;
	phx.wav 			= waveformToWav(gp.pulse[gpulse].recWaveform);
	phx.hvOff 			= false;
	phx.chargeBalance 	= chargeBalance;		// cbs switches are typically on for active recovery
	phx.pause			= pause;
	phx.sync			= sync;
	phx.calibrate		= cal;
	phx.repetition		= 0;

	amplitude 	= gp.pulse[gpulse].amplitude;
	ampPercent 	= gp.pulse[gpulse].ampPercent;
	ussChannel 	= gp.pulse[gpulse].ussChannel;

	for (i = 0; i < STIM_CHANNELS; i++)
	{
		phx.chan[i] = calculateChannel(i, krec, amplitude, -(int)ampPercent[i], ussChannel[i], raw, true);
	}

	return stimConfigPhase(phase, &phx, true);
}

#endif

static int gprgm_LoadStimPhase(int gpulse, uint16_t pulseWidth, uint16_t delay, bool raw, bool pause, bool sync, bool cal)
{
	uint8_t 		phase;			// Phase to be written
	PHASE 			phx;			// Phase configuration structure
	uint16_t 		amplitude;		// Amplitude of the pulse
	int8_t const 	*ampPercent;	// Channel amplitude percentage table
	bool const 		*ussChannel;	// Channel uncontrolled source/sink mode table
	int i;

	phase = stimMdl_NextPhase();

	phx.uSource 		= gp.pulse[gpulse].stimUSource;
	phx.pulseWidth 		= pulseWidth;
	phx.delay 			= delay;
	phx.wav 			= waveformToWav(gp.pulse[gpulse].stimWaveform);
	phx.hvOff 			= constStimPhasePSDisable();
	phx.chargeBalance 	= false;						// cbs switches are always off for stim phase
	phx.pause			= pause;
	phx.sync			= sync;
	phx.calibrate		= cal;
	phx.repetition		= 0;

	amplitude 	= gp.pulse[gpulse].amplitude;
	ampPercent 	= gp.pulse[gpulse].ampPercent;
	ussChannel 	= gp.pulse[gpulse].ussChannel;

	for (i = 0; i < STIM_CHANNELS; i++)
	{
		phx.chan[i] = calculateChannel(i, 1, amplitude, ampPercent[i], ussChannel[i], raw, false);
	}

	return stimConfigPhase(phase, &phx, true);
}

#ifndef STIM_NO_MITIGATION
static int gprgm_LoadPassiveRecoveryPhase(int gpulse, uint16_t pulseWidth, uint16_t delay, bool pause, bool sync, bool cal)
{
	uint8_t phase;	// Phase to be written
	PHASE phx;
	int i;

	phase = stimMdl_NextPhase();

	//phx.wav 			= stimMdl_PgmNum();		// set correct stim phase bank
	phx.wav				= 5;
	phx.pulseWidth 		= pulseWidth;
	phx.delay 			= delay;
	phx.uSource 		= gp.pulse[gpulse].recUSource;
	phx.hvOff 			= false;
	phx.chargeBalance 	= true;					// cbs switches are always on for passive recovery
	phx.pause			= pause;
	phx.sync			= sync;
	phx.calibrate		= cal;
	phx.repetition		= 0;

	stimSetRectLevel(5, 255);

	for (i = 0; i < STIM_CHANNELS; i++)
	{
		if(gp.pulse[gpulse].ampPercent[i] == 0)
		{
			// pull down on all unused channels before the CBC switches close during the delay part of the phase
			phx.chan[i] = calculateChannelCBCSink(i, CBC_SINK_CURRENT);
		}
		else
		{
			// used channels have a 0 amplitude
			phx.chan[i].amp = 0;
			phx.chan[i].source = false;
			phx.chan[i].uncontrolled = false;
		}
	}

	return stimConfigPhase(phase, &phx, false);
}
#else
static int gprgm_LoadPassiveRecoveryPhase(int gpulse, uint16_t pulseWidth, uint16_t delay, bool pause, bool sync, bool cal)
{
	uint8_t phase;	// Phase to be written
	PHASE phx;
	int i;

	phase = stimMdl_NextPhase();

	//phx.wav 			= stimMdl_PgmNum();
	phx.wav				= 2;
	phx.pulseWidth 		= pulseWidth;
	phx.delay 			= delay;
	phx.uSource 		= gp.pulse[gpulse].recUSource;
	phx.hvOff 			= false;
	phx.chargeBalance 	= true;					// cbs switches are always on for passive recovery
	phx.pause			= pause;
	phx.sync			= sync;
	phx.calibrate		= cal;
	phx.repetition		= 0;

	stimSetRectLevel(2, 0);

	for (i = 0; i < STIM_CHANNELS; i++)
	{
		// used channels have a 0 amplitude
		phx.chan[i].amp = 0;
		phx.chan[i].source = false;
		phx.chan[i].uncontrolled = false;
	}

	return stimConfigPhase(phase, &phx, false);
}

#endif


static int gprgm_LoadDelayPhase(uint16_t delay, uint8_t repetition, bool sync, bool cal)
{
	uint8_t phase;	// Phase to be written
	PHASE phx;
	int i;

	phase = stimMdl_NextPhase();

	//phx.wav 			= stimMdl_PgmNum();
	phx.wav 			= 2;
	phx.pulseWidth 		= SHORTEST_PULSE;
	if(delay < SHORTEST_PULSE)
	{
		phx.delay	= 0;
	}
	else
	{
		phx.delay 	= delay-SHORTEST_PULSE;
	}
	phx.uSource 		= false;
	phx.hvOff 			= false;
	phx.chargeBalance 	= false;				// cbs switches are always off for the delays
	phx.pause			= false;
	phx.sync			= sync;
	phx.calibrate		= cal;
	phx.repetition		= repetition;

	stimSetRectLevel(2, 0);

	for (i = 0; i < STIM_CHANNELS; i++)
	{
		phx.chan[i].amp = 0;
		phx.chan[i].source = false;
		phx.chan[i].uncontrolled = false;
	}

	return stimConfigPhase(phase, &phx, false);
}

static uint32_t getTotalPulseTime()
{
#ifndef STIM_NO_MITIGATION
	int32_t const ipd 			= constActiveInterphaseDelay();
	int32_t const cbc 			= constActiveCBCWidth();
	uint32_t time = 0;
	int i;

	for (i = 0; i < gp.numPulses; i++)
	{
		if(gp.pulse[i].recRatio == 0)
		{
			time += gp.pulse[i].pulseWidth;
			time += gp.pulse[i].interphaseDelay;
			time += gp.pulse[i].passiveWidth;
			time += gp.pulse[i].cbcWidth;
		}
		else
		{
			for (i = 0; i < gp.numPulses-1; i++)
			{
				time += gp.pulse[i].pulseWidth;
				time += (gp.pulse[i].pulseWidth * gp.pulse[i].recRatio);
				time += gp.pulse[i].interphaseDelay;
				time += ACTIVE_RECOVERY_HOLDOFF;
			}
			time += gp.pulse[gp.numPulses-1].pulseWidth;
			time += gp.pulse[gp.numPulses-1].interphaseDelay;
			time += (gp.pulse[gp.numPulses-1].pulseWidth * gp.pulse[i].recRatio);
			time += gp.pulse[gp.numPulses-1].cbcWidth;
		}
	}
	return time;
#else
	int32_t const ipd 			= constActiveInterphaseDelay();
	int32_t const cbc 			= constActiveCBCWidth();
	uint32_t time = 0;
	int i;

	for (i = 0; i < gp.numPulses; i++)
	{
		if(gp.pulse[i].recRatio == 0)
		{
			time += gp.pulse[i].pulseWidth;
			time += gp.pulse[i].interphaseDelay;
			time += gp.pulse[i].passiveWidth;
			time += gp.pulse[i].cbcWidth;
		}
		else
		{
			for (i = 0; i < gp.numPulses; i++)
			{
				time += gp.pulse[i].pulseWidth;
				time += (gp.pulse[i].pulseWidth * gp.pulse[i].recRatio);
				time += gp.pulse[i].interphaseDelay;
				time += gp.pulse[i].cbcWidth;
			}
		}
	}
	return time;
#endif
}

// ^---- STATIC
//-------------------------------------------------------------------------------------------------------
// v---- PUBLIC

// called at bootup or reset
void gprgmInit(void)
{
	memset(&gp, 0, sizeof(gp));
	stimMdl_Init();
}

void gprgmClear(void)
{
	memset(&gp, 0, sizeof(gp));
}

int gprgmRun(bool rampDisable)
{
	// program consists of the following phase types:
	// 		a. stimulation
	// 		b. recovery (either passive or active)
	//		c. repetition delay (max allowed delay of 65536 usec that repeats zero to 16 times)
	// 		d. repetition balance (balance required to get correct period)
	//		e. SYNC delay (fixed amount of time to allow interrupt processing of SYNC signal)
	// NOTE defn: "slack time" = repetition delay + repetition balance =  (period - sum of all excitation/recovery pulses including cbc and ipdelays)
	// pulse train: px_stim/px_recovery : delay_repeat : delay_balance : delay_sync
	// delay_sync must be larger than some minimum value required to do the interrupt processing

	uint32_t  	freq;				// Hz
	uint32_t 	period, repetition, balanceAfterReps;	// usec
	int32_t		slack;
	int 		i;
	uint16_t	recoveryWidth;

	if (gp.numPulses == 0)
	{
		DBG(f_gp, DBG_SEV_ERROR, 0, 0);
		stimStop();

		setResponseCode(RESP_PROGRAM_NOT_VALID);
		return(-1);
	}

	DBG(f_gp, DBG_SEV_NORM, 0, 0);

	// note: all times are in microseconds
	freq 	= constLookUpFrequency(gp.freqIdx);
	period = 1000000uL / freq;

	slack = period - STIM_SYNC_PHASE_DELAY_US; // remove the time needed for SYNC ISR processing phase

	// now remove the time for all the pulses
	slack -= getTotalPulseTime();
	if(slack < 0)
	{
		setResponseCode(RESP_PROGRAM_NOT_VALID);
		DBGI(f_gp, period);
		DBGI(f_gp, freq);

		// this should never happen if we did our earlier checks correctly
		return(-1);
	}

	// clock is 1 MHz and is not divided down so the max delay of 0xFFFF represents 65536 usec
	repetition 			= slack / 65536;
	balanceAfterReps 	= slack % 65536;

	// move to the next phase bank
	stimMdl_LoadProgram();

#ifndef STIM_NO_MITIGATION
	// build stim and recovery pulses
	for(i=0; i<gp.numPulses; i++)
	{
		if (gp.pulse[i].recRatio == 0)
		{
			// remove the pulse width of the recovery pulse from the stim interphase delay.
			if (gprgm_LoadStimPhase(i, gp.pulse[i].pulseWidth, gp.pulse[i].interphaseDelay - PASSIVE_RECOVERY_CBC_CORRECTION_PULSE_WIDTH - PASSIVE_RECOVERY_CBC_SWITCH_CLOSE_DELAY, false, singleStep, false, false) < 0)
			{
				return -1;
			}

			// note that only the last pulse will have a non-zero cbc width
			if(i == (gp.numPulses - 1))
			{
				recoveryWidth = gp.pulse[i].passiveWidth + gp.pulse[i].cbcWidth;
			}
			else
			{
				recoveryWidth = gp.pulse[i].passiveWidth;
			}
			if (gprgm_LoadPassiveRecoveryPhase(i, PASSIVE_RECOVERY_CBC_CORRECTION_PULSE_WIDTH, recoveryWidth, singleStep, false, false) < 0)
			{
				return -1;
			}
			DBG(f_gp, DBG_SEV_NORM, i, 0);
		}
		else
		{
			if (gprgm_LoadStimPhase(i, gp.pulse[i].pulseWidth, gp.pulse[i].interphaseDelay, false, singleStep, false, false) < 0)
			{
				return -1;
			}

			if(i == (gp.numPulses - 1))
			{
				// do something special on the last pulse for active recovery (we add an extra phase that sinks current to prep for the cbc delay)
				if (gprgm_LoadActiveRecoveryPhaseLast(i, gp.pulse[i].pulseWidth, gp.pulse[i].cbcWidth, true, false, singleStep, false, false) < 0)
				{
					return -1;
				}
			}
			else
			{
				if (gprgm_LoadActiveRecoveryPhase(i, gp.pulse[i].pulseWidth, ACTIVE_RECOVERY_HOLDOFF, false, false, singleStep, false, false) < 0)
				{
					return -1;
				}
			}
			DBG(f_gp, DBG_SEV_NORM, i, 0);
		}
	}
#else
	for(i=0; i<gp.numPulses; i++)
	{
		if (gp.pulse[i].recRatio == 0)
		{
			// remove the pulse width of the recovery pulse from the stim interphase delay.  The recovery pulse always has a SHORTEST_PULSE (2 usec) width.
			if (gprgm_LoadStimPhase(i, gp.pulse[i].pulseWidth, gp.pulse[i].interphaseDelay - SHORTEST_PULSE, false, singleStep, false, false) < 0)
			{
				return -1;
			}

			// note that only the last pulse will have a non-zero cbc width
			if(i == (gp.numPulses - 1))
			{
				recoveryWidth = gp.pulse[i].passiveWidth + gp.pulse[i].cbcWidth;
			}
			else
			{
				recoveryWidth = gp.pulse[i].passiveWidth;
			}
			if (gprgm_LoadPassiveRecoveryPhase(i, SHORTEST_PULSE, recoveryWidth, singleStep, false, false) < 0)
			{
				return -1;
			}
			DBG(f_gp, DBG_SEV_NORM, i, 0);
		}
		else
		{
			if (gprgm_LoadStimPhase(i, gp.pulse[i].pulseWidth, gp.pulse[i].interphaseDelay, false, singleStep, false, false) < 0)
			{
				return -1;
			}

			if (gprgm_LoadActiveRecoveryPhase(i, gp.pulse[i].pulseWidth, gp.pulse[i].cbcWidth, true, false, singleStep, false, false) < 0)
			{
				return -1;
			}
			DBG(f_gp, DBG_SEV_NORM, i, 0);
		}
	}
#endif
	// build slack repetition pulse with each repetition the max delay of 65536 (0xFFFF+1).  Note that performing one repetition requires a value of 0.
	if(repetition > 0)
	{
		DBGI(f_gp, repetition);
		if(gprgm_LoadDelayPhase(0xFFFF, repetition-1, false, false) < 0)
		{
			DBG(f_gp, DBG_SEV_ERROR, 0, 0);
			return(-1);
		}
	}

	if(balanceAfterReps > 0)
	{
		DBGI(f_gp, balanceAfterReps);

		// build slack balance pulse
		if(gprgm_LoadDelayPhase(balanceAfterReps, 0, false, false) < 0)
		{
			DBG(f_gp, DBG_SEV_ERROR, 0, 0);
			return(-1);
		}
	}

	// build SYNC pulse.  Set SYNC true so we get an interrupt on this delay
	if(gprgm_LoadDelayPhase(STIM_SYNC_PHASE_DELAY_US, 0, true, false) < 0)
	{
		DBG(f_gp, DBG_SEV_ERROR, 0, 0);
		return(-1);
	}

	if(rampDisable)
	{
		stimSetRectLevel(stimMdl_PgmNum(), 255);
	}
	else
	{
		stimSetRectLevel(stimMdl_PgmNum(), 0);
	}

	if(stimConfigSequencerRegisters(stimMdl_PgmNum(), stimMdl_FirstPhase(), stimMdl_NumPhases() ) < 0)
	{
		DBG(f_gp, DBG_SEV_ERROR, 0, 0);
		return(-1);
	}

#ifndef LEAKAGE_TEST
	// bring up the voltage to where we need to be
	if (gp.complianceDACValue > stimGetComplianceDAC())
	{
		if (stimSetComplianceDAC(gp.complianceDACValue, true) != 0)
		{
			return -1;
		}
	}
#else
	stimSetComplianceDAC(0x3F, true);
#endif

	DBG(f_gp, DBG_SEV_NORM, 0, 0);


	if(stimStart2(stimMdl_PgmNum(), false, true, gp.wf_ref) != 0)
	{
		logError(E_LOG_STIM_STARTUP_ERROR, 0);
		setResponseCode(RESP_STIM_FAILED_SEVERE_ERROR);
		return -1;
	}

	DBG(f_gp, DBG_SEV_NORM, 0, 0);

#ifndef LEAKAGE_TEST
	if (gp.complianceDACValue < stimGetComplianceDAC())
	{
		if (stimSetComplianceDAC(gp.complianceDACValue, false) != 0)
		{
			return -1;
		}
	}
#endif

	DBG(f_gp, DBG_SEV_NORM, 0, 0);
	return(0);
}
#if 0
int gprgmRunTitration()
{
	// program consists of the following phase types:
	// 		a. stimulation
	// 		b. recovery (either passive or active)
	//		c. repetition delay (max allowed delay of 65536 usec that repeats zero to 16 times)
	// 		d. repetition balance (balance required to get correct period)
	//		e. SYNC delay (fixed amount of time to allow interrupt processing of SYNC signal)
	// NOTE defn: "slack time" = repetition delay + repetition balance =  (period - sum of all excitation/recovery pulses including cbc and ipdelays)
	// pulse train: px_stim/px_recovery : delay_repeat : delay_balance : delay_sync
	// delay_sync must be larger than some minimum value required to do the interrupt processing

	uint32_t  	freq;				// Hz
	uint32_t 	period, repetition, balanceAfterReps;	// usec
	int32_t		slack;
	int 		i;

	// note: all times are in microseconds
	freq 	= constLookUpFrequency(gp.freqIdx);
	period = 1000000uL / freq;

	slack = period - STIM_SYNC_PHASE_DELAY_US; // remove the time needed for SYNC ISR processing phase

	// now remove the time for all the pulses
	slack -= getTotalPulseTime();
	if(slack < 0)
	{
		setResponseCode(RESP_PROGRAM_NOT_VALID);
		// this should never happen if we did our earlier checks correctly
		return(-1);
	}

	// clock is 1 MHz and is not divided down so the max delay of 0xFFFF represents 65536 usec
	repetition 			= slack / 65536;
	balanceAfterReps 	= slack % 65536;

	stimMdl_LoadProgram();

	for(i=0; i<gp.numPulses; i++)
	{
		if (gprgm_LoadStimPhase(i, gp.pulse[i].pulseWidth, gp.pulse[i].interphaseDelay, false, singleStep, false, false) < 0)
		{
			return -1;
		}

		if (gprgm_LoadActiveRecoveryPhase(i, gp.pulse[i].pulseWidth, gp.pulse[i].cbcWidth, true, false, singleStep, false, false) < 0)
		{
			return -1;
		}
	}

	// build slack repetition pulse with each repetition the max delay of 65536 (0xFFFF+1).  Note that performing one repetition requires a value of 0.
	if(repetition > 0)
	{
		if(gprgm_LoadDelayPhase(0xFFFF, repetition-1, false, false) < 0)
		{
			return(-1);
		}
	}

	if(balanceAfterReps > 0)
	{
		// build slack balance pulse
		if(gprgm_LoadDelayPhase(balanceAfterReps, 0, false, false) < 0)
		{
			DBG(f_gp, DBG_SEV_ERROR, 0, 0);
			return(-1);
		}
	}

	// build SYNC pulse.  Set SYNC true so we get an interrupt on this delay
	if(gprgm_LoadDelayPhase(STIM_SYNC_PHASE_DELAY_US, 0, true, false) < 0)
	{
		DBG(f_gp, DBG_SEV_ERROR, 0, 0);
		return(-1);
	}

	if(stimConfigSequencerRegisters(stimMdl_PgmNum(), stimMdl_FirstPhase(), stimMdl_NumPhases() ) < 0)
	{
		DBG(f_gp, DBG_SEV_ERROR, 0, 0);
		return(-1);
	}

	stimSetRectLevel(stimMdl_PgmNum(), 255);

	stimSetComplianceDAC(gp.complianceDACValue, false);

	DBG(f_gp, DBG_SEV_NORM, 0, 0);

	return(stimStart(stimMdl_PgmNum(), false, true));
}
#endif

int gprgmUpdateAmplitude(uint8_t level)
{
	return stimUpdateRectLevel(pgmNum, level);
}

int gprgmCheckDensities(void)
{
	LEAD_LIMIT_PARAMS const *ll;
	int numPulses;
	int pulse;
	int failureCode = 0;

	ll = constLeadLimits();
	numPulses = gprgmGetNumPulses();

	// Walk through each pulse in the program.
	for (pulse = 0; pulse < numPulses; pulse++)
	{
		uint16_t const 	amplitude 	= gp.pulse[pulse].amplitude;
		uint16_t const 	pulseWidth 	= gp.pulse[pulse].pulseWidth;
		int8_t const 	*ampPercent = gp.pulse[pulse].ampPercent;
		int chan;

		/*
		 * Check the total amplitude for the pulse.
		 * The hardware has a limit, by specification, of 30 mA total amplitude.
		 */
		if (amplitude > TP_MAX_PULSE_AMPLITUDE)
		{
			setResponseCode(RESP_PULSE_AMP_EXCEEDS_LIMITS);
			failureCode = 1;
			goto FAIL_RETURN;
		}

		// Walk through each channel in the pulse
		for (chan = 0; chan < NUM_CHANNELS; chan++)
		{
			/*
			 * Compute the channel current in uA.
			 */
			long const chanCurrent = labs((long)amplitude * (long)ampPercent[chan] / 100L);

			/*
			 * Check the channel current against the hardware current limit.
			 */
			if (chanCurrent > TP_MAX_CHANNEL_AMPLITUDE)
			{
				setResponseCode(RESP_PULSE_AMP_EXCEEDS_LIMITS);
				failureCode = 2;
				goto FAIL_RETURN;
			}

			/*
			 * Check the channel current against the lead current limit.
			 */
			if (chanCurrent > ll->currentLimit[chan])
			{
				setResponseCode(RESP_MOD_VIOLATES_CURR_DENSITY_LIMIT);
				failureCode = 3;
				goto FAIL_RETURN;
			}

			/*
			 * Check the channel charge against the charge limit.
			 * The chanCurrent is in uA*us = pC
			 * while the chargeLimit is in mA*us = nC, so
			 * scale the chargeLimit to pC before the comparison.
			 */
			if (chanCurrent * pulseWidth > ll->chargeLimit[chan] * 1000L)
			{
				setResponseCode(RESP_MOD_VIOLATES_CHRG_DENSITY_LIMIT);
				failureCode = 4;
				goto FAIL_RETURN;
			}
		}
	}

	return 0;

FAIL_RETURN:

	logError(E_LOG_ERR_CHECK_DENSITIES_FAILED, failureCode);
	return -1;
}




