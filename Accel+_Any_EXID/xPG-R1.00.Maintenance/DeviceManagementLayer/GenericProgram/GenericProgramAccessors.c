/*
 * GenericProgramAccessors.c
 *
 *  Created on: Jan 28, 2013
 *      Author: garnold
 */

#include "gprgm_def.h"
#include "GenericProgram.h"

extern GPRGM gp;
/*
 * Public API -- see GenericProgram.h for documentation.
 */

uint8_t gprgmGetFreqIdx(void)
{
	return gp.freqIdx;
}

/*
 * Public API -- see GenericProgram.h for documentation.
 */
uint8_t gprgmGetComplianceDACValue(void)
{
	return gp.complianceDACValue;
}

/*
 * Public API -- see GenericProgram.h for documentation.
 */
uint8_t gprgmGetNumPulses(void)
{
	return gp.numPulses;
}

/*
 * Public API -- see GenericProgram.h for documentation.
 */
uint8_t gprgmGetRampLevel(void)
{
	return gp.rampLevel;
}

/*
 * Public API -- see GenericProgram.h for documentation.
 */
void gprgmSetFreqIdx(uint8_t freqIdx)
{
	gp.freqIdx = freqIdx;
}

/*
 * Public API -- see GenericProgram.h for documentation.
 */
void gprgmSetComplianceDACValue(uint8_t n)
{
	gp.complianceDACValue = n;
}

/*
 * Public API -- see GenericProgram.h for documentation.
 */
void gprgmSetNumPulses(uint8_t n)
{
	gp.numPulses = n;
}

/*
 * Public API -- see GenericProgram.h for documentation.
 */
void gprgmSetRampLevel(uint8_t rampLevel)
{
	gp.rampLevel = rampLevel;
}


/*
 * Retrieve the amplitude field from a GPULSE.
 *
 * Public API -- see GenericProgram.h for documentation.
 */

uint16_t gprgmGetAmplitude(int gpulse)
{
	return gp.pulse[gpulse].amplitude;
}

/*
 * Retrieve the pulse width field from a GPULSE.
 *
 * Public API -- see GenericProgram.h for documentation.
 */

uint16_t gprgmGetPulseWidth(int gpulse)
{
	return gp.pulse[gpulse].pulseWidth;
}

/*
 * Retrieve the channel amplitude percentage field from a GPULSE.
 *
 * Public API -- see GenericProgram.h for documentation.
 */

int8_t const *gprgmGetAmpPercent(int gpulse)
{
	return gp.pulse[gpulse].ampPercent;
}

/*
 * Retrieve the voltage mode (uncontrolled source/sink) flags from a GPULSE.
 *
 * Public API -- see GenericProgram.h for documentation.
 */

bool const *gprgmGetUSSChannels(int gpulse)
{
	return gp.pulse[gpulse].ussChannel;
}

/*
 * Retrieve the recovery ratio field from a GPULSE.
 *
 * Public API -- see GenericProgram.h for documentation.
 */

uint8_t gprgmGetRecRatio(int gpulse)
{
	return gp.pulse[gpulse].recRatio;
}

/*
 * Retrieve the passive recovery width (passiveWidth) field from a GPULSE.
 *
 * Public API -- see GenericProgram.h for documentation.
 */

uint16_t gprgmGetPassiveWidth(int gpulse)
{
	return gp.pulse[gpulse].passiveWidth;
}

/*
 * Retrieve the charge balance correction width (cbcWidth) field from a GPULSE.
 *
 * Public API -- see GenericProgram.h for documentation.
 */

uint16_t gprgmGetCBCWidth(int gpulse)
{
	return gp.pulse[gpulse].cbcWidth;
}

/*
 * Retrieve the interphase delay (IPD) from a GPULSE.
 *
 * Public API -- see GenericProgram.h for documentation.
 */

uint16_t gprgmGetInterphaseDelay(int gpulse)
{
	return gp.pulse[gpulse].interphaseDelay;
}

/*
 * Public API -- see GenericProgram.h for documentation.
 */
bool gprgmGetStimUSource(int gpulse)
{
	return gp.pulse[gpulse].stimUSource;
}

/*
 * Public API -- see GenericProgram.h for documentation.
 */
bool gprgmGetRecUSource(int gpulse)
{
	return gp.pulse[gpulse].recUSource;
}

/*
 * Public API -- see GenericProgram.h for documentation.
 */
bool gprgmGetCBCUSource(int gpulse)
{
	return gp.pulse[gpulse].cbcUSource;
}

/*
 * Public API -- see GenericProgram.h for documentation.
 */
void gprgmSetAmplitude(int gpulse, uint16_t amp)
{
	gp.pulse[gpulse].amplitude = amp;
}

/*
 * Public API -- see GenericProgram.h for documentation.
 */
void gprgmSetPulseWidth(int gpulse, uint16_t pw)
{
	gp.pulse[gpulse].pulseWidth = pw;
}

/*
 * Public API -- see GenericProgram.h for documentation.
 */
void gprgmSetAmpPercent(int gpulse, int8_t const *ampPercent)
{
	memcpy(gp.pulse[gpulse].ampPercent, ampPercent, sizeof(gp.pulse[gpulse].ampPercent));
}

/*
 * Set the voltage mode (uncontrolled source/sink) flags in a GPULSE.
 *
 * Public API -- see GenericProgram.h for documentation.
 */
void gprgmSetUSSChannels(int gpulse, bool const *ussChannel)
{
	memcpy(gp.pulse[gpulse].ussChannel, ussChannel, sizeof(gp.pulse[gpulse].ussChannel));
}

/*
 * Public API -- see GenericProgram.h for documentation.
 */
void gprgmSetRecRatio(int gpulse, uint8_t krec)
{
	gp.pulse[gpulse].recRatio = krec;
}

/*
 * Public API -- see GenericProgram.h for documentation.
 */
void gprgmSetPassiveWidth(int gpulse, uint16_t us)
{
	gp.pulse[gpulse].passiveWidth = us;
}

/*
 * Public API -- see GenericProgram.h for documentation.
 */
void gprgmSetCBCWidth(int gpulse, uint16_t us)
{
	gp.pulse[gpulse].cbcWidth = us;
}

/*
 * Public API -- see GenericProgram.h for documentation.
 */
void gprgmSetInterphaseDelay(int gpulse, uint16_t ipd)
{
	gp.pulse[gpulse].interphaseDelay = ipd;
}

/*
 * Public API -- see GenericProgram.h for documentation.
 */
void gprgmSetStimUSource(int gpulse, bool isSource)
{
	gp.pulse[gpulse].stimUSource = isSource;
}

/*
 * Public API -- see GenericProgram.h for documentation.
 */
void gprgmSetRecUSource(int gpulse, bool isSource)
{
	gp.pulse[gpulse].recUSource = isSource;
}

/*
 * Public API -- see GenericProgram.h for documentation.
 */
void gprgmSetCBCUSource(int gpulse, bool isSource)
{
	gp.pulse[gpulse].cbcUSource = isSource;
}

/*
 * Public API -- see GenericProgram.h for documentation
 */
enum WAVEFORM gprgmGetStimWaveform(int gpulse)
{
	return gp.pulse[gpulse].stimWaveform;
}


/*
 * Public API -- see GenericProgram.h for documentation
 */
enum WAVEFORM gprgmGetRecWaveform(int gpulse)
{
	return gp.pulse[gpulse].recWaveform;
}


/*
 * Public API -- see GenericProgram.h for documentation
 */
void gprgmSetStimWaveform(int gpulse, enum WAVEFORM wf)
{
	gp.pulse[gpulse].stimWaveform = wf;
}


/*
 * Public API -- see GenericProgram.h for documentation
 */
void gprgmSetRecWaveform(int gpulse, enum WAVEFORM wf)
{
	gp.pulse[gpulse].recWaveform = wf;
}

void gprgmSetWaveformReferenceCurrent(CHANNEL_MAX_AMP_INDEX ref)
{
	gp.wf_ref = ref;
}

CHANNEL_MAX_AMP_INDEX gprgmGetWaveformReferenceCurrent()
{
	return gp.wf_ref;
}
