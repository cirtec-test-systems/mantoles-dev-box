/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *	
 *	@file Description: Data types and constants for the GPRGM generic program
 *	
 ***************************************************************************/



#ifndef GPRGM_DEF_H_
#define GPRGM_DEF_H_

#include "GenericProgram.h"
#include "prgmDef.h"
#include "ticks.h"
#include "stim.h"


#define PW_ACTIVE_CBC	100	// usec

/**
 * Generic pulse structure.
 *
 * Used to unify test pulses, test programs, and standard programs.
 */

typedef struct GPULSE {
	uint16_t amplitude;				///< Amplitude of the pulse, in microamps
	uint16_t pulseWidth;			///< Pulsewidth of the stimulation phase, in microseconds
	int8_t ampPercent[NUM_CHANNELS];	///< Percentage of each channel, -100..100

	// This field may be calculated automatically by Auto Uncontrolled Source/Sink.
	// Specify it only if not using gprgmAutoUncontrolledSourceSink().
	bool ussChannel[NUM_CHANNELS];		///< true if channel is in uncontrolled current mode (voltage mode), false if controlled current

	// These fields may be calculated automatically by Automatic Waveform Adjustment.
	// Specify them only if not using gprgmAutoWaveformAdjustment().
	uint16_t interphaseDelay;		///< Interphase delay (IPD)
	uint8_t recRatio;				///< Recovery ratio, 1..5 for active or 0 for passive
	uint16_t passiveWidth;			///< Width of passive recovery, if applicable, in microseconds
	uint16_t cbcWidth;				///< Charge balance correction phase width, if applicable, in microseconds

	bool stimUSource;				///< true if stim-phase uncontrolled-current channels should be sources; false if should be sinks
	bool recUSource;				///< true if recovery-phase uncontrolled-current channels should be sources; false if should be sinks
	bool cbcUSource;				///< true if CBC-phase uncontrolled-current channels should be sources; false if should be sinks

	enum WAVEFORM stimWaveform;
	enum WAVEFORM recWaveform;
} GPULSE;

/**
 * Generic program structure
 *
 * Used to unify test pulses, test programs, and standard programs.
 */

typedef struct GPRGM {
	uint8_t freqIdx;				///< Frequency as an index into the frequency table
	uint8_t rampLevel;				///< Ramping level (0-255)
	uint8_t numPulses;				///< Number of pulses in program

	GPULSE pulse[NUM_PULSES];

	uint8_t complianceDACValue;		///< Boost converter DAC value for the compliance voltage
	CHANNEL_MAX_AMP_INDEX wf_ref;
} GPRGM;


/**
 * The shortest stimulation phase the xPG can generate
 */
#define SHORTEST_PULSE			8	// usec  every phase must have a pulse with a duration greater than or equal to SHORTEST_PULSE
#define IMPEDANCE_PULSE_WIDTH	200	// usec
#define IMPEDANCE_DELAY			130 // usec  time into the impedance pulse that we start the measurement of the impedance voltage
#define CAP_CHECK_DELAY			250	// usec  time into the impedance pulse that we start the measurement of the cap voltage

#define PASSIVE_RECOVERY_CBC_CORRECTION_PULSE_WIDTH		8		// width of the "cbc drive to ground" pulse that is part of the passive recovery phase
#define PASSIVE_RECOVERY_CBC_SWITCH_CLOSE_DELAY			12		// it takes 3 clock cycles (at 250 KHz) to get the CBC switches closed
#define PASSIVE_RECOVERY_CBC_CORRECTION_AMP 			0x40	// amplitude of the "extra" phase (uA)

#define ACTIVE_RECOVERY_CBC_CORRECTION_PULSE_DELAY		8		// delay in the active recovery phase (usec)
#define ACTIVE_RECOVERY_CBC_CORRECTION_PULSE_WIDTH		8		// pulse width of "extra" phase that drives unused channels to ground (usec)
#define ACTIVE_RECOVERY_CBC_CORRECTION_AMP 				0x40	// amplitude of the "extra" phase (uA)
#define ACTIVE_RECOVERY_HOLDOFF							100		// usec delay after pulse that has no cbc (only last pulse has CBC)
/**
 * Timeout for Impedance ADC conversion
 *
 * TODO: Consider replacing this with a calculation based on program frequency.
 */
#define IMPEDANCE_TIMEOUT	0x7fff

/**
 * Frequency index to use during amplitude calibration operations
 */
#define CAL_FREQ_IDX		10

/**
 * Pulse width to use during amplitude calibration operations (microseconds)
 */
#define CAL_PULSE_WIDTH	160

/**
 * Inter-phase delay to use during amplitude calibration operations (microseconds)
 */
#define CAL_IPD			1

/**
 * Timeout for the calibration ADC conversion (ms)
 */
#define CAL_TIMEOUT		100			// Chosen somewhat arbitrarily

/**
 * Delay into the pulse at which the ADC should trigger for calibration operations (microseconds, multiple of 8)
 */
#define CAL_WHEN			16


/**
 * The fixed division ratio in the impedance measurement circuitry.
 *
 * The total gain of the measurement, from channels to ADC input, is
 * gain1 * gain2 * FIXED_DIVISOR.
 *
 * \todo: Confirm correct gain selection. Gain may need to be set
 *        in a piecewise-linear fashion in order to support accuracy
 *        over the full dynamic range.
 */
#if 0
// This is the calculated gain from the Saturn specification
#define FIXED_GAIN		(1 / 55.5)
#elif 1
// This is the average gain measured from 30 samples of Saturn 904.
#define FIXED_GAIN		0.022375
#else
// This is the gain calculated from the one Saturn chip impedance
// measurement was developed on.
#define FIXED_GAIN		0.026
#endif

/**
 * The volts per ADC least-significant bit.
 *
 * Defined as the ADC reference voltage divided by the full-scale value
 * equal to that reference voltage.
 */
#define LSB				(2.5 / 4095)

/**
 * The series capacitance of one DC blocking capacitor, in Farads.
 */
#define C_BLOCK			1.0e-6		// 1 uF

/**
 * The maximum output peak of the impedance measurement circuit, in V.
 *
 * The Saturn specification (EESP 0085) gives the DC bias of this output
 * as 1.250 V with a swing of +/- 125 mV around that bias voltage.
 */
#if 0		// TODO: Confirm correct selection
// This is from the Saturn specification
#define IMPEDANCE_SWING	0.125
#else
// However, after some improvements by ON, the actual output swing comes
// within one diode drop of the rails.
#define IMPEDANCE_SWING	(2.5/2 - 0.7)
#endif

/**
 * The minimum offset voltage for working impedance measurement circuitry,
 * in DAC units.
 *
 * \todo TODO Figure out if this is correct or if a smaller offset range should be used.
 */
#define IMP_OFFSET_MIN		(uint16_t)((4095 / 2 - IMPEDANCE_SWING / LSB))

/**
 * The maximum offset voltage for working impedance measurement circuitry,
 * in DAC units.
 *
 * \todo TODO Figure out if this is correct or if a smaller offset range should be used.
 */
#define IMP_OFFSET_MAX		(uint16_t)((4095 / 2 + IMPEDANCE_SWING / LSB))


/**
 * The maximum gain of the first-stage programmable gain amplifier (PGA)
 */
#define GAINA_MAX	127

/**
 * The maximum gain of the second-stage programmable gain amplifier (PGA)
 */
#define GAINB_MAX	7


/**
 * The LSB of the pulse guard timer, in microseconds
 */
#define PG_INCREMENT	128

/**
 * The pulse guard value to use for pulse guard self-calibration.
 *
 * This value was chosen by looking at the ratio between STIM_CLK and
 * the PGO and looking for the point of diminishing returns between
 * taking longer for auto-cal versus getting the ratio as close as
 * possible to 1.
 */
#define PG_VALUE		50

/**
 * The duration, in microseconds, for the pulseguard PG_VALUE
 */
#define PG_DURATION 	(PG_INCREMENT * PG_VALUE)

/**
 * The pulse width to use for pulse guard self-calibration.
 *
 * It needs to be longer than the worst-case mis-trimmed pulse guard oscillator
 */
#define PG_TRIM_PW		(PG_DURATION * 2)


#endif /* GPRGM_DEF_H_ */
