/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *	
 *	@file Description: stimMdl internal functions
 *	
 ***************************************************************************/

#include <stdbool.h>

#include "stim.h"
#include "PhaseCounter.h"

extern uint8_t g_nextPhaseBank;

unsigned 	pgmNum;	// stim asic bank 0 or 1
uint8_t 	firstPhase;
uint8_t 	numPhases;

// INITIALIZATION AT BOOTUP
// called from gprgmInit which is called on device driver initialization.  Done only once.
void stimMdl_Init(void)
{
	pgmNum = g_nextPhaseBank;

	firstPhase = 0;
	numPhases = 0;
}

// ACCESS ROUTINES
unsigned stimMdl_PgmNum(void)
{
	return pgmNum;
}

uint8_t stimMdl_FirstPhase(void)
{
	return firstPhase;
}

uint8_t stimMdl_NumPhases(void)
{
	return numPhases;
}


// ROUTINES TO BUILD A PROGRAM ONE PHASE AT A TIME
static uint8_t wrap(uint8_t phase)
{
	if (phase >= STIM_PHASES)
	{
		return phase - STIM_PHASES;
	}
	else
	{
		return phase;
	}
}

// called from the "Emitters" when you are building your phases in the stim asic
uint8_t stimMdl_NextPhase(void)
{
	uint8_t nextPhase;
	
	nextPhase = wrap(firstPhase + numPhases);
	numPhases++;
	return nextPhase;
}


// called from gprgmRun() when you are about to run a new program from the other bank in the stim asic
void stimMdl_LoadProgram(void)
{

	firstPhase 	= wrap(firstPhase + numPhases);
	numPhases 	= 0;

	pgmNum = g_nextPhaseBank;
}








