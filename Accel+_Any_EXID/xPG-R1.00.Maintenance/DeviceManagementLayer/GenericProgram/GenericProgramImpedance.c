/*
 * GenericProgramImpedance.c
 *
 *  Created on: Jan 28, 2013
 *      Author: garnold
 */
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <math.h>

#include "DeviceDriverLayer/Common/msp430f2618.h"
#include "DeviceDriverLayer/Common/bits.h"

#include "system.h"
#include "Common/Protocol/cmndBackgroundImpedance.h"
#include "bgImp.h"
#include "pgm.h"
#include "cal.h"
#include "const.h"
#include "lastResponse.h"

#include "GenericProgram.h"
#include "gprgm_def.h"
#include "PhaseCounter.h"
#include "stim.h"
#include "adc.h"
#include "log.h"
#include "delay.h"
#include "system.h"
#include "timer.h"
#include "Interrupt.h"
#include "cal.h"

//#define CHECK_IMP_EXEC_SPEED
//#define STIM_IMP_RAW_COUNTS
//#define FAST_BGI_CHECK

#define STIM_IMP_CALCULATE

#ifdef STIM_IMP_CALCULATE

#define STIM_IMP_GAIN_A_100		1
#define STIM_IMP_GAIN_B_100		7

#define STIM_IMP_GAIN_A_200		1
#define STIM_IMP_GAIN_B_200		7

#define STIM_IMP_GAIN_A_500		1
#define STIM_IMP_GAIN_B_500		7

#else

#define STIM_IMP_GAIN_A_100		16
#define STIM_IMP_GAIN_B_100		1

#define STIM_IMP_GAIN_A_200		8
#define STIM_IMP_GAIN_B_200		1

#define STIM_IMP_GAIN_A_500		4
#define STIM_IMP_GAIN_B_500		1


#define IMP_CALC_100UA_SLOPE_0_TO_400		6849	// divide this by 1000
#define IMP_CALC_100UA_INTERCEPT_0_TO_400	-612

#define IMP_CALC_100UA_SLOPE_400_AND_ABOVE		11702	// divide this by 1000
#define IMP_CALC_100UA_INTERCEPT_400_AND_ABOVE	-2755

#define IMP_CALC_200UA_SLOPE		8747	// divide this by 1000
#define IMP_CALC_200UA_INTERCEPT	-212

#define IMP_CALC_500UA_SLOPE		8472	// divide this by 1000
#define IMP_CALC_500UA_INTERCEPT	-89

#endif

#define STIM_IMP_MAX_IMPEDANCE				5000
#define STIM_IMP_MIN_CAP_ADC_COUNT_500UA 	40
#define STIM_IMP_MIN_CAP_ADC_COUNT_200UA 	16
#define STIM_IMP_MIN_CAP_ADC_COUNT_100UA 	8

extern PRGM_DEF 	pgm;
extern unsigned 	pgmNum;

extern uint16_t bimpImpedance[NUM_CHANNELS];
extern uint8_t  bimpDataValid;

#ifdef CHECK_IMP_EXEC_SPEED
static uint32_t tdelta(int16_t ticks_start, int16_t ticks_end)
{
	int32_t delta;

	if((delta = ticks_end - ticks_start) < 0)
	{
		// timer rolled at least once
		return(0);
	}
	delta *= 1000;
	delta /= 4096;
	return(delta);
}
#endif

// -------------------------------------------------------------------------------------------------------------------
static void configADC(int reps)
{
	ADC_CONFIG 	adcConfig;

	adcOn();

	adcConfig.firstInput 	= ADC_IMPEDANCE;
	adcConfig.numInputs 	= 1;						// Read impedance pin
	adcConfig.reps			= reps;						// This number of times
	adcConfig.seqType		= CONVERT_ALL;				// read from a single trigger event
	adcConfig.speed			= ADC_FAST;
	adcConfig.trigger		= TRIGGER_SOFTWARE;			// trigger in software via setting a register bit
	adcConfig.sampleTime	= ADC_SAMPLE_TIME_16;

	adcSetup(&adcConfig);
}

static int getADCValue(uint32_t *value)
{
	uint16_t const volatile *data = NULL;
#if 0
	uint32_t	voltage;
	int			i;
#endif
	adcConvert();

	if((data = adcResults(MS(CAL_TIMEOUT))) == NULL)
	{
		*value = 0;
		return(-1);
	}

#if 0
	voltage = 0;
	// ignore the first A/D value - it's often way off
	for(i=1; i<reps; i++)
	{
		voltage += (uint32_t)data[i];
		//DBGI(f_gp_impedance, data[i]);
	}
	voltage /= (uint32_t)(reps-1);
	//DBGI(f_gp_impedance, voltage);

	*value = voltage;
#else
	// assume two samples and we throw out the first and return the second sample
	*value = data[1];
#endif
	return(0);
}

static CHAN calculateChannel(int n, int16_t amplitude)
// amplitude in uA
{
	CHAN 	chan;
	uint16_t dacCal;
	uint64_t amp;

	if(amplitude == 0)
	{
		chan.amp = 0;
		chan.uncontrolled = false;
		return(chan);
	}

	if (amplitude < 0)
	{
		amplitude = -1 * amplitude;
		chan.source = false;
#ifndef DISABLE_CHANNEL_CAL_LOOKUP
		dacCal = calLookUpSinkingCal(CHANNEL_MAX_AMP_3, n);
#else
		//dacCal = 900;
#endif
	}
	else
	{
		chan.source = true;
#ifndef DISABLE_CHANNEL_CAL_LOOKUP
		dacCal = calLookUpSourcingCal(CHANNEL_MAX_AMP_3, n);
#else
		//dacCal = 900;
#endif
	}

	amp =  (uint64_t)amplitude * (uint64_t)dacCal;
	amp /= (uint64_t)CHANNEL_CAL_CURRENT_IMPEDANCE_MEASURE;

	chan.amp = (uint16_t)amp;
	chan.uncontrolled = false;

	//DBGI(f_gp_impedance, tdelta(ticks_a, ticks_b));

	return chan;
}

static uint8_t loadPhaseNew(int16_t amplitude, uint16_t pulseWidth, uint16_t delay, uint8_t srcChan, uint8_t sinkChan, bool cbc)
{
	uint8_t 		phase;			// Phase to be written
	PHASE 			phx;			// Phase configuration structure

	phase = stimMdl_NextPhase();

	phx.uSource 		= false;
	phx.pulseWidth 		= pulseWidth;
	phx.delay 			= delay;
	phx.wav 			= 0;
	//phx.hvOff 			= true;
	phx.hvOff 			= false;
	phx.chargeBalance 	= cbc;
	phx.pause			= false;
	phx.sync			= false;
	phx.calibrate		= false;
	phx.repetition		= 0;

	phx.chan[srcChan]  = calculateChannel(srcChan,  amplitude);
	phx.chan[sinkChan] = calculateChannel(sinkChan,-amplitude);

	stimConfigPhaseFast(phase, &phx, srcChan, sinkChan);

	return(phase);
}

static uint32_t	getADCOffset()
{
	uint32_t adcOffset;

	getADCValue(&adcOffset);

	return(adcOffset);
}

static int loadProgram(uint16_t amplitude, uint16_t pulseWidth, uint8_t ch1, uint8_t ch2, uint8_t *phaseRay)
{
	stimMdl_LoadProgram();

	// first pulse is ch1->ch2
	phaseRay[0] = loadPhaseNew(amplitude, pulseWidth, 100, ch1, ch2, false);
	phaseRay[1] = loadPhaseNew(amplitude, pulseWidth, 100, ch2, ch1, false);
	phaseRay[2] = loadPhaseNew(amplitude, pulseWidth, 100, ch1, ch2, false);

	// pulses 1 and 4 use phases 0 and 1
	// pulses 2 and 3 use phases 1 and 2

	stimSetRectLevel(0, 255);

	stimConfigSequencerRegisters(0, phaseRay[0], 2);
	stimConfigSequencerRegisters(1, phaseRay[1], 2);

	return(0);
}

static void prepareImpedance(uint16_t pulseWidth, uint16_t amplitude, uint8_t srcChan, uint8_t sinkChan)
{
	uint16_t	gainA, gainB, delay;

	delay = pulseWidth - 8;

	switch(amplitude)
	{
	case 500:
		gainA = STIM_IMP_GAIN_A_500;
		gainB = STIM_IMP_GAIN_B_500;
		break;
	case 200:
		gainA = STIM_IMP_GAIN_A_200;
		gainB = STIM_IMP_GAIN_B_200;
		break;
	case 100:
		gainA = STIM_IMP_GAIN_A_100;
		gainB = STIM_IMP_GAIN_B_100;
		break;
	default:
		return;
	}

	stimImpedanceOn();

	// the stim circuit should put out 1.25 volts when no S/H is active.  This would be an ADC count of 4096/2 = 2048 (since the ADC has a 2.5V reference)
	// the impedance registers expect the src and sink channels to start at 1 instead of zero
	// disable the S/H via phase=30
	stimImpedanceConfigure(srcChan+1, sinkChan+1, gainA, gainB, delay, 30);

	return;
}

static int captureImpedance(int sel, uint32_t *impedance, bool trigger)
{
	// make this function as fast as possible!
	int 		status = 0;
	uint32_t 	val = 0;

	(void)trigger;

	*impedance = 0;

	stimStartImpedanceMeasurement(sel);

#ifdef DRAIN_CAP_ATTEMPT
	// perform a dummy a/d operation to drain the voltage inside the a/d converter
	setTST_TRIG_HIGH();
	getADCValue(measureReps, &firstVal);
	setTST_TRIG_LOW();
#endif

#ifdef STIM_IMP_USE_SH
	if(stimImpedanceSync() != 0)
	{
		DBGI(f_gp_impedance, 0);
		status = -1;
		goto CLEANUP;
	}
#else
	DELAY_US(IMPEDANCE_DELAY);
#endif

#if 1
#if 0
	if(trigger)
	{
		setTST_TRIG_HIGH();
	}
#endif

	if(getADCValue(&val) != 0)
	{
		DBGI(f_gp_impedance, val);
		status = -1;
	}

#if 0
	if(trigger)
	{
		setTST_TRIG_LOW();
	}
#endif
#endif

#ifdef STIM_IMP_USE_SH
	if(stimImpedanceClearSH() != 0)
	{
		DBGI(f_gp_impedance, 0);
		status = -1;
	}
#endif

	stimWaitForStop();


	*impedance = val;

#if 0
	if(status != 0)
	{
		setResponseCode(RESP_BGND_IMPEDANCE_CHECK_FAILED);
	}
#endif

	return(status);
}

#ifdef ENABLE_OC_CHECK
static int captureCapVoltage(uint32_t *capVoltage)
{
	int 		status = 0;
	uint32_t 	val = 0;

	*capVoltage = 0;

	// use the first program to take the cap measurement
	stimStartImpedanceMeasurement(0);

	DELAY_US(CAP_CHECK_DELAY);

	if(getADCValue(2, &val) != 0)
	{
		DBGI(f_gp_impedance, val);
		status = -1;
	}

	stimWaitForStop();

	*capVoltage = val;

	if(status != 0)
	{
		setResponseCode(RESP_BGND_IMPEDANCE_CHECK_FAILED);
	}

	return(status);
}
#endif

static uint32_t calculateImpedance(uint32_t adcCount, uint16_t amplitude)
{

#ifdef STIM_IMP_RAW_COUNTS
	return(adcCount);
#else
#ifdef STIM_IMP_CALCULATE
	int32_t 	val;
	uint32_t 	scaledAdcCount;

	// voltage = adcCount (count) * 0.61 (mv/Count)
	scaledAdcCount = adcCount * 61;
	scaledAdcCount /= 100;

	switch(amplitude)
	{
	case 500:
		// per hardware team: r = 11.43 * (adcCountAverage - 35)
		val = 0;
		if(scaledAdcCount > 35)
		{
			val = scaledAdcCount - 35;
		}
		val *= 1143;
		val /=100;
		break;
	case 200:
		// per hardware team: r = 28.57 * (adcCountAverage - 14)
		val = 0;
		if(scaledAdcCount > 14)
		{
			val = scaledAdcCount - 14;
		}
		val *= 2857;
		val /=100;
		break;
	case 100:
		// per hardware team: r = 57.14 * (adcCountAverage - 7)
		val = 0;
		if(scaledAdcCount > 7)
		{
			val = scaledAdcCount - 7;
		}
		val *= 5714;
		val /=100;
		break;
	default:
		val = 0;
		break;
	}

	return(val);

#else
	int32_t	slope, intercept;
	int32_t	val;

	switch(amplitude)
	{
	case 500:
		slope 		= IMP_CALC_500UA_SLOPE;
		intercept 	= IMP_CALC_500UA_INTERCEPT;
		break;
	case 200:
		slope 		= IMP_CALC_200UA_SLOPE;
		intercept 	= IMP_CALC_200UA_INTERCEPT;
		break;
	case 100:
		// for 100 uA the graph is piece-wise linear
		if(adcCount <= 400)
		{
			slope 		= IMP_CALC_100UA_SLOPE_0_TO_400;
			intercept 	= IMP_CALC_100UA_INTERCEPT_0_TO_400;
		}
		else
		{
			slope 		= IMP_CALC_100UA_SLOPE_400_AND_ABOVE;
			intercept 	= IMP_CALC_100UA_INTERCEPT_400_AND_ABOVE;
		}
		break;
	default:
		return(0);
	}

	val = adcCount * slope / 1000;
	val += intercept;

	val < 0 ? val = 0 : val;

	return(val);
#endif
#endif
}

// FOREGROUND IMPEDANCE MEASUREMENT
int gprgmMeasureImpedance(uint16_t amplitude, uint8_t ch1, uint8_t ch2, bool oneShot, bool checkCap, uint32_t *impedancePtr, uint32_t *capVoltage)
{
	uint32_t 	adcOffset[5];
	uint32_t	impedance[4], impedanceVal;
	uint8_t		phase[4];
	int			status, i;
	uint16_t	pulseWidth = IMPEDANCE_PULSE_WIDTH;

	uint16_t interruptState;

	(void)capVoltage;
	(void)checkCap;

#ifdef CHECK_IMP_EXEC_SPEED
	int16_t		ticks_a, ticks_b, ticks_c, ticks_d, ticks_e, ticks_f, ticks_g, ticks_h, ticks_i, ticks_j;
#endif

	if(oneShot)
	{
		configADC(2);

		if(stimSetComplianceDAC(20, true) != 0)
		{
			DBGI(f_gp_impedance, 0);
			return -1;
		}
	}

	*impedancePtr = 0xFFFFFFFF;

	// the driver is 0 based indexing and generic program is 1 based indexing
	ch1 -= 1;
	ch2 -= 1;

	if((status = loadProgram(amplitude, pulseWidth, ch1, ch2, phase)) != 0)
	{
		goto CLEANUP;
	}
	prepareImpedance(pulseWidth, amplitude, ch1, ch2);

	stimPrepareToStart(CHANNEL_MAX_AMP_3);

	// -------  ENTER CRITICAL SECTION - MAKE SURE INTERRUPTS ARE DISABLED
	interruptState = interruptDisable();

#ifdef CHECK_IMP_EXEC_SPEED
	ticks_a = timerTicks();
#endif
	adcOffset[0] = getADCOffset();
#ifdef CHECK_IMP_EXEC_SPEED
	ticks_g = timerTicks();
#endif
	captureImpedance(0, &impedance[0], false);
#ifdef CHECK_IMP_EXEC_SPEED
	ticks_b = timerTicks();
#endif

	adcOffset[1] = getADCOffset();
	captureImpedance(1, &impedance[1], false);

#ifdef CHECK_IMP_EXEC_SPEED
	ticks_c = timerTicks();
#endif
	// flip the mux channels - they start at 1 not zero for this call
	stimImpedanceConfigureChannels(ch2+1, ch1+1);

#ifdef CHECK_IMP_EXEC_SPEED
	ticks_d = timerTicks();
#endif
	adcOffset[2] = getADCOffset();
	captureImpedance(1, &impedance[2], false);

#ifdef CHECK_IMP_EXEC_SPEED
	ticks_e = timerTicks();
#endif
	adcOffset[3] = getADCOffset();
	captureImpedance(0, &impedance[3], false);

#ifdef CHECK_IMP_EXEC_SPEED
	ticks_f = timerTicks();
#endif

#ifdef ENABLE_OC_CHECK
	if(checkCap)
	{
		uint32_t voltage;

		adcOffset[4] = getADCOffset(measureReps);
		captureCapVoltage(&voltage);

		*capVoltage = (voltage >= adcOffset[4]) ? voltage-adcOffset[4] : adcOffset[4]-voltage;
		DBGI(f_gp_impedance, *capVoltage);
	}
#endif

	// -------  END OF CRITICAL SECTION SO RESTORE INTERRUPTS
	interruptRestore(interruptState);

#ifdef CHECK_IMP_EXEC_SPEED
	DBGI(f_gp_impedance, tdelta(ticks_a, ticks_b));
	DBGI(f_gp_impedance, tdelta(ticks_b, ticks_c));
	DBGI(f_gp_impedance, tdelta(ticks_c, ticks_d));
	DBGI(f_gp_impedance, tdelta(ticks_d, ticks_e));
	DBGI(f_gp_impedance, tdelta(ticks_e, ticks_f));

	DBGI(f_gp_impedance, tdelta(ticks_a, ticks_g));
	DBGI(f_gp_impedance, tdelta(ticks_g, ticks_b));
#endif

	// calculate average
	impedanceVal = 0;
#if 1
	for(i=0; i<4; i++)
	{
		// calculate the a/d offset from baseline
		impedance[i] = (impedance[i] >= adcOffset[i]) ? impedance[i]-adcOffset[i] : adcOffset[i]-impedance[i];

		impedanceVal += impedance[i];

	}
	impedanceVal /= 4;
	//DBGI(f_gp_impedance, impedanceVal);

#else
	for(i=0; i<4; i++)
	{
		if(i==0 || i==3)
		{
			// use the first and last values only where the current is flowing in the same direction
			// calculate the a/d offset from baseline
			impedance[i] = (impedance[i] >= adcOffset[i]) ? impedance[i]-adcOffset[i] : adcOffset[i]-impedance[i];

			impedanceVal += impedance[i];
			DBGI(f_gp_impedance, impedance[i]);
		}

	}
	impedanceVal /= 2;
#endif

	*impedancePtr = calculateImpedance(impedanceVal, amplitude);

	//DBGI(f_gp_impedance, *impedancePtr);

CLEANUP:
	if(oneShot)
	{
		adcOff();
		stimImpedanceOff();
	}

	return(status);
}

#ifndef EPG
#ifndef DISABLE_BGI_CHECK
// BACKGROUND IMPEDANCE MEASUREMENT FOR IPG ONLY
int gprgmImpedanceAndOutputCapCheck(bool *result)
{

	BACKGROUND_IMPEDANCE_PARAMS bimpParams;

	uint16_t 	amplitude;
	int 		activeChannels[NUM_CHANNELS];
	uint8_t 	pulseMap, pulseNo, iChan, canChan;
	uint32_t	trueImpedance[NUM_CHANNELS+1];
	int			status = 0;
#ifdef ENABLE_OC_CHECK
	uint8_t		capVoltageThreshold;
	uint32_t	capVoltage[NUM_CHANNELS+1];
	uint32_t	disable;
#endif

#ifdef FAST_BGI_CHECK
	uint8_t		srcChan;
	uint16_t	pulseWidth;
	bool 		prepFlag;
	uint32_t 	adcOffset[5];
	uint32_t	impedance[4], impedanceVal;
	uint8_t		phase[4];
	int			i;
	int 	 	measureReps = 2;
	uint16_t 	interruptState;
	int8_t		prevChan;
	uint32_t	voltage;
#endif

#ifdef ENABLE_OC_CHECK
	disable = calGetGenCals()->tempStateCal[STIM_OUTPUT_CAP_CHECK];
#endif

	if(stimSetComplianceDAC(20, true) != 0)
	{
		DBGI(f_gp_impedance, 0);
		return -1;
	}

	if(bgImpGetBackgroundImpedanceParams(&bimpParams) == GEN_SUCCESS)
	{
		if(bimpParams.enabled == 0)
		{
			*result = true;
			return(0);
		}

		if(bimpParams.bgiAmplitude == 1)
		{
			amplitude 	 = 100;
#ifdef ENABLE_OC_CHECK
			capVoltageThreshold = STIM_IMP_MIN_CAP_ADC_COUNT_100UA;
#endif
		}
		else
		{
			amplitude = 200;
#ifdef ENABLE_OC_CHECK
			capVoltageThreshold = STIM_IMP_MIN_CAP_ADC_COUNT_200UA;
#endif
		}
#ifdef FAST_BGI_CHECK
		pulseWidth 	 = IMPEDANCE_PULSE_WIDTH;
#endif
	}
	else
	{
		// go ahead and run the check at the lowest amplitude if the parameters are corrupted
		amplitude = 100;
#ifdef ENABLE_OC_CHECK
		capVoltageThreshold = STIM_IMP_MIN_CAP_ADC_COUNT_100UA;
#endif

#ifdef FAST_BGI_CHECK
		pulseWidth = IMPEDANCE_PULSE_WIDTH;
#endif

		//return(-1);
	}

	// find all channels used for the current program
	memset(activeChannels, 0, sizeof(activeChannels));
	memset(trueImpedance, 0, sizeof(trueImpedance));
#ifdef ENABLE_OC_CHECK
	memset(capVoltage, 255, sizeof(capVoltage));		// note that a value of 0 for capVoltage indicates a bad capacitor so fill with max ADC count instead
#endif

	pulseMap = pgm_GetPulseMap();

	for(pulseNo = 0; pulseMap != 0; pulseMap = pulseMap >> 1, pulseNo++)
	{
		for(iChan=0; iChan<NUM_CHANNELS; iChan++)
		{
			if (pgm.pulseDef[pulseNo].electrodeAmpPercentage[iChan] != 0)
			{
				activeChannels[iChan] = 1;
			}
		}
	}

#ifdef FAST_BGI_CHECK
	canChan = 14;	// use zero based indexing
#else
	canChan = 15;	// the gprgmMeasureImpedance() function subtracts one from both channels so don't do it here
#endif

#ifdef FAST_BGI_CHECK
	prevChan = -1;
	prepFlag = false;
#endif

	configADC(2);

	for(iChan=1; iChan<=NUM_CHANNELS; iChan++)
	{

		// don't check the can channels
		if(iChan == 14 || iChan == 15) continue;
		if(activeChannels[iChan-1] == 0) continue;


#ifndef FAST_BGI_CHECK
		// the foreground command will take care of the channels-starting-at-one problem so set them back to original value
#ifdef ENABLE_OC_CHECK
		gprgmMeasureImpedance(amplitude, iChan, canChan, false, true, &trueImpedance[iChan], &capVoltage[iChan]);
		DBGI(f_gp_impedance, trueImpedance[iChan]);
		DBGI(f_gp_impedance, capVoltage[iChan]);

#else
		gprgmMeasureImpedance(amplitude, iChan, canChan, false, true, &trueImpedance[iChan], NULL);
		DBGI(f_gp_impedance, trueImpedance[iChan]);
#endif
		DELAY_MS(5);

#else
		srcChan = iChan-1; // 0 based indexing in driver so subtract one

		if(!prepFlag)
		{
			if((status = loadProgram(amplitude, pulseWidth, srcChan, canChan, phase)) != 0)
			{
				goto CLEANUP;
			}
			prepareImpedance(pulseWidth, amplitude, srcChan, canChan);

			stimPrepareToStart(CHANNEL_MAX_AMP_3);

			prepFlag = true;
		}

		if(prevChan != -1)
		{
			CHAN	chan;

			// turn off the previous channel
			stimSetChannelAmplitude(phase[0], prevChan, 0, false);
			stimSetChannelAmplitude(phase[1], prevChan, 0, false);
			stimSetChannelAmplitude(phase[2], prevChan, 0, false);

			chan = calculateChannel(srcChan, amplitude);

			// turn on the next channel
			stimSetChannelAmplitude(phase[0], srcChan, chan.amp, true);
			stimSetChannelAmplitude(phase[1], srcChan, chan.amp, false);
			stimSetChannelAmplitude(phase[2], srcChan, chan.amp, true);
		}

		// -------  ENTER CRITICAL SECTION - MAKE SURE INTERRUPTS ARE DISABLED
		interruptState = interruptDisable();


		adcOffset[0] = getADCOffset(measureReps);
		captureImpedance(0, measureReps, &impedance[0], false);

		adcOffset[1] = getADCOffset(measureReps);
		captureImpedance(1, measureReps, &impedance[1], false);

		// flip the mux channels - they start at 1 not zero for this call
		stimImpedanceConfigureChannels(canChan+1, srcChan+1);

		adcOffset[2] = getADCOffset(measureReps);
		captureImpedance(1, measureReps, &impedance[2], false);

		adcOffset[3] = getADCOffset(measureReps);
		captureImpedance(0, measureReps, &impedance[3], false);

		// now measure the capacitor voltage
		adcOffset[4] = getADCOffset(measureReps);
		captureCapVoltage(&voltage);

		capVoltage[iChan] = (voltage >= adcOffset[4]) ? voltage-adcOffset[4] : adcOffset[4]-voltage;
		DBGI(f_gp_impedance, capVoltage[iChan]);

		// -------  END OF CRITICAL SECTION SO RESTORE INTERRUPTS
		interruptRestore(interruptState);

		// calculate average
		impedanceVal = 0;

		for(i=0; i<4; i++)
		{
			// calculate the a/d offset from baseline
			impedance[i] = (impedance[i] >= adcOffset[i]) ? impedance[i]-adcOffset[i] : adcOffset[i]-impedance[i];

			impedanceVal += impedance[i];
			//DBGI(f_gp_impedance, impedance[i]);

		}
		impedanceVal /= 4;
		DBGI(f_gp_impedance, iChan);

		trueImpedance[iChan] = calculateImpedance(impedanceVal, amplitude);
		DBGI(f_gp_impedance, trueImpedance);

		prevChan = (int8_t)srcChan;
#endif

	}

	{
		bool disableActive = false;

		for(iChan=1; iChan<=NUM_CHANNELS; iChan++)
		{

			if(trueImpedance[iChan] > STIM_IMP_MAX_IMPEDANCE)
			{
				DBGI(f_gp_impedance, trueImpedance[iChan]);
				disableActive = true;
				logError(E_LOG_ERR_OPEN_CIRCUIT_CONDITION, iChan);
				// pgm_Disable assumes a zero indexed channel
				pgm_DisableProgramChannel(iChan-1, ACTERR_CH_IMPEDANCE_FAILURE_PROGRAMS_DISABLED);
				setResponseCode(RESP_BGND_IMPEDANCE_CHECK_FAILED);
			}
#ifdef ENABLE_OC_CHECK
			else if(!disable && capVoltage[iChan] < capVoltageThreshold)
			{
				DBGI(f_gp_impedance, capVoltage[iChan]);
				disableActive = true;
				logError(E_LOG_ERR_FAULTY_OUTPUT_CAPACITOR, iChan);
				// pgm_Disable assumes a zero indexed channel
				pgm_DisableProgramChannel(iChan-1, ACTERR_CH_IMPEDANCE_FAILURE_PROGRAMS_DISABLED);
				setResponseCode(RESP_BGND_IMPEDANCE_CHECK_FAILED);
			}
#endif
		}
		if(disableActive)
		{
			pgm_InvalidateSelectedProgram();
		}

		// copy the impedance values into the global structure so that they can be returned by the diagnostic check
		for(iChan=1; iChan<=NUM_CHANNELS; iChan++)
		{
			if(trueImpedance[iChan] > 0)
			{
				bimpImpedance[iChan-1] = trueImpedance[iChan];
			}
			bimpDataValid = 1;
		}
	}

#ifdef FAST_BGI_CHECK
CLEANUP:
#endif

	adcOff();
	stimImpedanceOff();

	return(status);
}
#endif
#endif


#ifdef EPG
#ifndef DISABLE_BGI_CHECK
// BACKGROUND IMPEDANCE MEASUREMENT FOR EPG ONLY
int gprgmEpgImpedanceAndOutputCapCheck(bool *result)
{
	BACKGROUND_IMPEDANCE_PARAMS bimpParams;

	uint16_t 	amplitude;
	uint8_t		activeChannels[NUM_CHANNELS+1];
	uint8_t		channelState[NUM_CHANNELS+1];
	uint8_t 	pulseMap, pulseNo, i, j, canChan;
	uint32_t	trueImpedance;
#ifdef ENABLE_OC_CHECK
	uint32_t	capVoltage;
	uint32_t 	disable;
	uint8_t		capVoltageThreshold;

	disable = calGetGenCals()->tempStateCal[STIM_OUTPUT_CAP_CHECK];
#endif

	configADC(2);

	if(stimSetComplianceDAC(20, true) != 0)
	{
		DBGI(f_gp_impedance, 0);
		return -1;
	}

	if(bgImpGetBackgroundImpedanceParams(&bimpParams) == GEN_SUCCESS)
	{
		if(bimpParams.enabled == 0)
		{
			*result = true;
			return(0);
		}

		if(bimpParams.bgiAmplitude == 1)
		{
			amplitude 	 = 100;
#ifdef ENABLE_OC_CHECK
			capVoltageThreshold = STIM_IMP_MIN_CAP_ADC_COUNT_100UA;
#endif
		}
		else
		{
			amplitude = 200;
#ifdef ENABLE_OC_CHECK
			capVoltageThreshold = STIM_IMP_MIN_CAP_ADC_COUNT_200UA;
#endif
		}
	}
	else
	{
		// go ahead and run the check at the lowest amplitude if the parameters are corrupted
		amplitude = 100;
#ifdef ENABLE_OC_CHECK
		capVoltageThreshold = STIM_IMP_MIN_CAP_ADC_COUNT_100UA;
#endif
	}

	// channels 12 and 13 are not available on the EPG
	// find all channels used by the currently selected program
	memset(activeChannels, 0, sizeof(activeChannels));
	memset(channelState, 0 , sizeof(channelState));

	pulseMap = pgm_GetPulseMap();
	for(pulseNo = 0; pulseMap != 0; pulseMap = pulseMap >> 1, pulseNo++)
	{
		for(i=0; i<NUM_CHANNELS; i++)
		{
			if (pgm.pulseDef[pulseNo].electrodeAmpPercentage[i] != 0)
			{
				activeChannels[i+1] = 1;
			}
		}
	}

	// channelState: 0 means not tested, 1 means channel passed, 2 means channel failed
	// the code below makes sure that every active channel in the program is checked
	canChan 	= 0xFF;

	for(i=1; i<=NUM_CHANNELS; i++)
	{
		if(activeChannels[i] == 0 || channelState[i] != 0) continue;	// this channel is not active or has been tested already

		if(canChan != 0xFF)
		// compare the ith channel against one that we know passed in a previous pass through these loops - our virtual can channel
		{
#ifdef ENABLE_OC_CHECK
			gprgmMeasureImpedance(amplitude, i, j, false, true, &trueImpedance, &capVoltage);
#else
			gprgmMeasureImpedance(amplitude, i, canChan, false, false, &trueImpedance, NULL);
#endif
			// save the impedance value in the global structure so it can be returned in the diagnostic command
			bimpImpedance[i-1] = trueImpedance;

			DELAY_MS(5);
			DBGI(f_gp_impedance, trueImpedance);

#ifdef ENABLE_OC_CHECK
			if(trueImpedance > STIM_IMP_MAX_IMPEDANCE  || (!disable && capVoltage < capVoltageThreshold))
#else
			if(trueImpedance > STIM_IMP_MAX_IMPEDANCE)
#endif
			{
				channelState[i] = 2;
				DBGI(f_gp_impedance, trueImpedance);

				logError(E_LOG_ERR_OPEN_CIRCUIT_CONDITION, i);
				//pgm_DisableProgramChannel(i, ACTERR_CH_IMPEDANCE_FAILURE_PROGRAMS_DISABLED);
				setResponseCode(RESP_BGND_IMPEDANCE_CHECK_FAILED);
			}
			else
			{
				channelState[i] = 1;
			}
		}
		else
		// no channel combinations have yet passed
		// check the ith channel against the jth channel until we either get a pass or all compares against the other active channels fail
		// if it passes then use the jth channel as the virtual can channel
		{
			for(j=1; j<=NUM_CHANNELS; j++)
			{
				if(activeChannels[j] == 0 || j == i || channelState[j] == 2) continue;		// this channel is not active, is the same as the channel under test, or has already failed

#ifdef ENABLE_OC_CHECK
				gprgmMeasureImpedance(amplitude, i, j, false, true, &trueImpedance, &capVoltage);
#else
				gprgmMeasureImpedance(amplitude, i, j, false, false, &trueImpedance, NULL);
#endif
				DELAY_MS(5);
				DBGI(f_gp_impedance, trueImpedance);

#ifdef ENABLE_OC_CHECK
				if(trueImpedance <= STIM_IMP_MAX_IMPEDANCE && (disable || capVoltage >= capVoltageThreshold))
#else
				if(trueImpedance <= STIM_IMP_MAX_IMPEDANCE )
#endif
				{
					// both channels passed so mark them
					channelState[i] = 1;
					channelState[j] = 1;

					canChan = j;

					// save the impedance value in the global structure so it can be returned in the diagnostic command
					bimpImpedance[i-1] = trueImpedance;
					bimpImpedance[j-1] = trueImpedance;

					break; // break out of the for(j= ...) loop
				}
			}

			// checks against all other channels failed so it's likely that this channel is bad
			if(canChan == 0xFF)
			{
				channelState[i] = 2;

				bimpImpedance[i-1] = trueImpedance;

				DBGI(f_gp_impedance, trueImpedance);
				logError(E_LOG_ERR_OPEN_CIRCUIT_CONDITION, i);
				setResponseCode(RESP_BGND_IMPEDANCE_CHECK_FAILED);
			}
		}
	}

	// evaluate the results
	*result = true;
	for(i=1; i<=NUM_CHANNELS; i++)
	{
		if(channelState[i] == 2)
		{
			*result = false;
			break;
		}
	}

	// indicate that we updated the global impedance data structure
	bimpDataValid = 1;

	adcOff();
	stimImpedanceOff();

	return(0);
}
#endif
#endif


