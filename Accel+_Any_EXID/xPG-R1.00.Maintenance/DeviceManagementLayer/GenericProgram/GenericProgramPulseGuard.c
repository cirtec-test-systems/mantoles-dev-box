

#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <math.h>

#include "DeviceDriverLayer/Common/msp430f2618.h"
#include "DeviceDriverLayer/Common/bits.h"

#include "const.h"
#include "Stim.h"
#include "GenericProgram.h"
#include "PhaseCounter.h"
#include "log.h"
#include "cal.h"
#include "pgm.h"
#include "delay.h"
#include "lastResponse.h"

extern unsigned 	pgmNum;

static CHAN calculateChannel(int n, int krec, uint16_t amplitude, int8_t ampPercent)
// amplitude in uA
{
	CHAN 	chan;
	uint16_t dacCal;
	uint32_t amp;

	// Configure the channel polarity
	if (ampPercent < 0)
	{
		ampPercent = -ampPercent;
		chan.source = false;
		dacCal = calLookUpSinkingCal(CHANNEL_MAX_AMP_15, n);
	}
	else
	{
		chan.source = true;
		dacCal = calLookUpSourcingCal(CHANNEL_MAX_AMP_15, n);
	}

	// Calculate the channel amplitude
	// channel amplitude = [total_amplitude * (ampPercent/100)](uA) * [dacCal/15000](bits/uA) * [1/Krec](scale factor) = bits for MDAC
	// NOTE: because dacCal will be different for source and sink, the actual amplitude values will be different even when both are the same channel percentage

	amp = ((long)amplitude * ampPercent * dacCal) / ((long)krec * CHANNEL_CAL_CURRENT * 100);

	chan.amp = (uint16_t)amp;
	chan.uncontrolled = false;

	return chan;
}

static int loadPhase(uint16_t amplitude, uint16_t pulseWidth, uint16_t delay, uint8_t src, uint8_t sink, bool cbc)
{
	uint8_t 		phase;			// Phase to be written
	PHASE 			phx;			// Phase configuration structure
	int				i;

	phase = stimMdl_NextPhase();

	phx.uSource 		= false;
	phx.pulseWidth 		= pulseWidth;
	phx.delay 			= delay;
	phx.wav 			= pgmNum;
	phx.hvOff 			= false;
	phx.chargeBalance 	= cbc;
	phx.pause			= false;
	phx.sync			= false;
	phx.calibrate		= false;
	phx.repetition		= 0;

	for (i = 0; i < STIM_CHANNELS; i++)
	{
		phx.chan[i].amp = 0;
		phx.chan[i].source = false;
		phx.chan[i].uncontrolled = false;
	}

	phx.chan[src] 	= calculateChannel(src,  1, amplitude, 100);
	phx.chan[sink] 	= calculateChannel(sink, 1, amplitude, -100);

	return stimConfigPhase(phase, &phx, true);
}

bool gprgmPulseGuardCheck(uint8_t src, uint8_t sink)
{
	uint16_t	amplitude, pulseWidth;
	uint16_t	pg;
	int 		count;
	bool		triggered = false;

	amplitude 	= AMPL_LOW_LIMIT_MIN;
	pulseWidth 	= 500; 	// usec

	stimMdl_LoadProgram();

	// load the stim phase
	if (loadPhase(amplitude, pulseWidth, 100, src, sink, false) < 0)
	{
		DBG(f_gp_pulseguard, DBG_SEV_ERROR, 0, 0);
		goto CLEANUP;
	}

	// load the recovery phase
	if (loadPhase(amplitude, pulseWidth, 200, sink, src, true ) < 0)
	{
		DBG(f_gp_pulseguard, DBG_SEV_ERROR, 0, 0);
		goto CLEANUP;
	}

	stimSetRectLevel(pgmNum, 255);

	if(stimConfigSequencerRegisters(pgmNum, stimMdl_FirstPhase(), stimMdl_NumPhases() ) < 0)
	{
		DBG(f_gp_pulseguard, DBG_SEV_ERROR, 0, 0);
		goto CLEANUP;
	}

	P2IE &= ~STIM_ERR;

	// set pulse guard to be 70% of the pulse width.  It should trip.
	pg = pulseWidth * 7;
	pg /= 10;
	pg /= PG_STEP;

	stimSetPulseGuard(stimMdl_FirstPhase(), pg);

	DELAY_MS(1);

	if(stimStart(pgmNum, false, true) != 0)
	{
		logError(E_LOG_STIM_STARTUP_ERROR, 0);
		DBG(f_gp_pulseguard, DBG_SEV_ERROR, 0, 0);
		setResponseCode(RESP_STIM_FAILED_SEVERE_ERROR);
		goto CLEANUP;
	}

	count = 0;

	while((P2IN & STIM_ERR) != 0 && count < 100)
	{
		DELAY_MS(1);
		count++;
	}

	if((P2IN & STIM_ERR) == 0)
	{
		DBG(f_gp_pulseguard, DBG_SEV_NORM, 0, 0);
		triggered = true;
	}
	else
	{
		DBG(f_gp_pulseguard, DBG_SEV_ERROR, 0, 0);
	}

	stimStop();

	stimClearFlags();

	P2IFG &= ~STIM_ERR;
	P2IE |= STIM_ERR;

CLEANUP:
	return(triggered);
}
