/*
 * GenericProgramAuto.c
 *
 *  Created on: Jan 28, 2013
 *      Author: garnold
 */


/*
 * Automatically configure the impedance-measurement gains.
 *
 * Public API -- see GenericProgram.h for documentation.
 */
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "lastresponse.h"
#include "cal.h"
#include "GenericProgram.h"
#include "const.h"
#include "gprgm_def.h"
#include "stim.h"
#include "log.h"
#include "cal.h"

float gprgm_ComputeVCap(int chan, float t);

extern GPRGM gp;


/* *
 * Requires the following GPRGM fields to be set:
 *
 *  - freqIdx
 *  - numPulses
 *  - pulseWidth (per pulse)
 *
 * Does not alter the GPRGM.
 *
 * \note The algorithm as defined in the SWEX 0071 Nov-2011 draft omits the recovery
 * 		 and CBC holdoff times. This implementation includes the missing terms.
 *
 * \return The largest recovery ratio the program can use.
 */
static int gprgm_ComputeMaxRecRatio(void)
{
#ifndef STIM_NO_MITIGATION
	int32_t const ipd 			= constActiveInterphaseDelay();
	int32_t const cbc 			= constActiveCBCWidth();

	int32_t krecMax;

	int32_t 		period;
	int32_t 		sum, pwsum;
	int32_t			balance;
	int const 		n 			= gp.numPulses;		// Number of pulses in the program, per the algorithm spec in SWEX 0071.
	int 			i;

	// this function sums up all the stimulation pulse widths and associated delays and determines what krec value we would need to squeeze in the recovery pulses
	period = 1000000uL / constLookUpFrequency(gp.freqIdx);

	sum 	= 0;
	pwsum 	= 0;

	// pulses before the last pulse do not have a cbc delay but have an active recovery holdoff instead
	for (i = 0; i < n-1; i++)
	{
		pwsum += gp.pulse[i].pulseWidth;

		sum += gp.pulse[i].pulseWidth;
		sum += ipd;
		sum += ACTIVE_RECOVERY_HOLDOFF;
	}

	// last pulse has the standard phase that contains the stimulation and interphase delay (ipd) but also...
	// last pulse has a recovery phase with a short delay followed by a phase that sinks all channels and contains the charge balance correction
	// so the last pulse actually has three phases (see GenericProgam.c : gprgm_LoadActiveRecoveryPhaseLast()
	//	1. stim + ipd
	//  2. recovery + ACTIVE_RECOVERY_CBC_CORRECTION_PULSE_DELAY
	//  3. "sink currents" pulse with width of ACTIVE_RECOVERY_CBC_CORRECTION_PULSE_WIDTH + cbc delay
	pwsum += gp.pulse[n-1].pulseWidth;
	// don't add the ACTIVE_RECOVERY_CBC_CORRECTION_PULSE_WIDTH to the pwsum but do add it to the overhead contained in "sum"

	// stimulation
	sum += gp.pulse[n-1].pulseWidth;
	sum += ipd;

	// recovery
	sum += ACTIVE_RECOVERY_CBC_CORRECTION_PULSE_DELAY;
	sum += ACTIVE_RECOVERY_CBC_CORRECTION_PULSE_WIDTH;
	sum += cbc;

	// extra phase for synchronizing hardware and software
	sum += STIM_SYNC_PHASE_DELAY_US;

	// balance is the time left over to squeeze in all the recovery pulses.  The sum of the recovery pulse widths for krec=1 would be pwsum
	balance = period - sum;
	if(balance <= 0)
	{
		return -1;
	}

	krecMax =  balance/ pwsum;
	if(krecMax == 0)
	{
		return -1;
	}

	krecMax = (krecMax > NUM_KREC) ? NUM_KREC : krecMax;

	return(krecMax);
#else
	int32_t const ipd 			= constActiveInterphaseDelay();
	int32_t const cbc 			= constActiveCBCWidth();

	int32_t krecMax;

	int32_t 		period;
	int32_t 		sum, pwsum;
	int32_t			balance;
	int const 		n 			= gp.numPulses;		// Number of pulses in the program, per the algorithm spec in SWEX 0071.
	int 			i;

	period = 1000000uL / constLookUpFrequency(gp.freqIdx);

	sum 	= 0;
	pwsum 	= 0;
	for (i = 0; i < n; i++)
	{
		pwsum += gp.pulse[i].pulseWidth;

		sum += gp.pulse[i].pulseWidth;
		sum += ipd;
		sum += cbc;
	}

	balance = period - sum - STIM_SYNC_PHASE_DELAY_US;
	if(balance <= 0)
	{
		return -1;
	}

	krecMax =  balance/ pwsum;
	if(krecMax == 0)
	{
		return -1;
	}

	krecMax = (krecMax > NUM_KREC) ? NUM_KREC : krecMax;

	return(krecMax);

#endif
}
/**
 * Finds the highest absolute current on any channel in a pulse as a percentage of the total amplitude.
 *
 * Used as a helper function for calculating the compliance voltage.
 *
 */
static uint16_t maxChannelCurrent(int gpulse)
{
	int8_t const *percent = gp.pulse[gpulse].ampPercent;
	int max;
	int i;

	max = 0;
	for (i = 0; i < NUM_CHANNELS; i++)
	{
		if (max < abs(percent[i]))
		{
			max = abs(percent[i]);
		}
	}

	return max;
}

CHANNEL_MAX_AMP_INDEX gprgm_FindReferenceCurrentIndex()
{
	CHANNEL_MAX_AMP_INDEX ampIndex;
	int i;
	uint16_t percentage = 0;

	uint32_t currentMax 	= 0;
	uint32_t maxAmplitude 	= 0;

	// find the maximum current for the entire program
	for(i=0; i<gp.numPulses; i++)
	{
		percentage = maxChannelCurrent(i);
		currentMax = (uint32_t)percentage * (uint32_t)gp.pulse[i].amplitude / (uint32_t)100;
		if(currentMax > maxAmplitude)
		{
			maxAmplitude = currentMax;
		}
	}

	if(maxAmplitude <= 3000)
	{
		ampIndex = CHANNEL_MAX_AMP_3;
	}
	else if(maxAmplitude <= 6000)
	{
		ampIndex = CHANNEL_MAX_AMP_6;
	}
	else if(maxAmplitude <= 9000)
	{
		ampIndex = CHANNEL_MAX_AMP_10;
	}
	else
	{
		ampIndex = CHANNEL_MAX_AMP_15;
	}
	return(ampIndex);
}

/**
 * Calculate the compliance voltage for one pulse, per "Compliance Voltage Calculation", section
 * 4.1.6 of the November 2011 draft functional spec.
 *
 * Uses the formula and quantities listed in the specification.
 *
 * @param gpulse 	The number of the pulse to consider
 * @return			The compliance voltage required by the pulse, in mV
 */
static uint32_t gprgm_PulseVoltage(int gpulse)
{
	// The following variables and constants correspond to those of section 4.1.6 of the spec,
	// with capitalization changes as needed to comply with the C coding standard.

	uint32_t  	iTotal 		= gp.pulse[gpulse].amplitude;					// Total current for the pulse (uA)
	uint32_t  	iChan 		= iTotal * maxChannelCurrent(gpulse) / 100; 	// Maximum current sourced or sunk by any channel during the phase (uA)
	uint32_t  	pwStim 		= gp.pulse[gpulse].pulseWidth;					// Pulse width of the stimulus phase (us)
	uint32_t  	sActive 	= gp.pulse[gpulse].recRatio == 0 ? 0 : 1;		// Active/passive recovery constant (unitless)
	uint32_t  	rTarget = calGetGenCals()->rTarget;							// Modeled load impedance, in ohms
	uint32_t	voltage;

/*
	uint32_t cHV = 9400;			// High-voltage supply output capacitance, in nF
	uint32_t const vOffset = 1000;	// Fixed voltage drop across sources or sinks, in mV
	uint32_t const rSource = 100;	// Source resistance across source or sinks, in ohms
	uint32_t const cChan = 1000;	// Capacitance of DC blocking capacitor, in nF
*/

	/*
	voltage = (1 + sActive) * (iTotal * pwStim) / cHV	// uA*usec/nF = 10e-12/1-e-9=10e-3 = 10e-3 =  mV
				+ 2*(vOffset + iChan*rSource/1000)		// uA*ohm/10e+3 10e-6/10e3 = mV
				+ 2*(iChan * pwStim)/cChan				// uA*usec/nF = 10e-6*10e-6/10e-9=10e-12/10e-9=10e-3 = mV	:: this is the voltage across output caps
				+ iChan*rTarget/1000;					// uA*ohm/10e+3 = 10e-6/10e+3 = 10e-3 = mV					:: this is the voltage across the load
	*/

	voltage =  (1 + sActive) * (iTotal * pwStim) / 9400L;	// uA*usec/nF = 10e-12/1-e-9=10e-3 = 10e-3 =  mV
	voltage += 	2000L + iChan / 5L;							// uA*ohm/10e+3 10e-6/10e3 = mV
	voltage +=  (iChan * pwStim) / 500L;					// uA*usec/nF = 10e-6*10e-6/10e-9=10e-12/10e-9=10e-3 = mV	:: this is the voltage across output caps
	voltage +=  iChan*rTarget / 1000L;						// uA*ohm/10e+3 = 10e-6/10e+3 = 10e-3 = mV					:: this is the voltage across the load

	return(voltage);

	// Calculation is done in mV, but some terms are (uA * ohm) = uV.
	// Scaling by 1000 is needed for those terms.
#if 0
																	// Unit correctness:
	return (1 + sActive) * (iTotal * pwStim) / cHV					// uA * us / nF = mV
			+ (1 + sControl) * (vOffset + iChan * rSource / 1000)	// mV + uA * ohm / 1000 = mV
			+ 2 * (iChan * pwStim) / cChan							// uA * us / nF = mV
			+ iChan * rTarget / 1000;								// uA * ohm * 1000 = mV
#endif
}


/**
 * Find the Boost DAC code that will generate the requested voltage.
 *
 * Uses the algorithm specified in "Compliance Voltage Calculation", section 4.1.6 of the
 * November 2011 draft functional spec.
 *
 * Looks up the requested voltage in the stimAsicHVCals table.
 *  - Returns the number of the table entry that has the lowest voltage at least as high
 *    as the requested voltage.
 *  - If the requested voltage is below the first entry, returns the first entry.
 *  - If the requested voltage is above the last entry, returns the last entry.
 *
 * @param mv	The desired boost converter output voltage, in mV
 * @return 		The Boost DAC code value for the corresponding boost converter output voltage.
 */
static uint8_t gprgm_FindBoostDACCode(uint32_t mv)
{
	uint16_t const * const hvCal = calGetHVCals()->stimAsicHVmVOutput;
	int i;

	for (i = 0; i < NUM_STIM_ASIC_HV_SETTINGS; i++)
	{
		if (mv < hvCal[i])
		{
			break;
		}
	}

	if(i == NUM_STIM_ASIC_HV_SETTINGS)
	{
		// the voltage is higher than the highest value in the LUT.  Use largest value available
		i--;
		logError(E_LOG_ERR_STIM_COMPLIANCE_VOLT_EXCEEDS_MAX, 0);
	}
	return i;
}
/**
 * Find the largest enabled recovery ratio equal to or less than the parameter.
 *
 * \param max	The limit on the recovery ratio that may be returned
 * \return		The largest enabled recover ratio equal to or less than the parameter, if there is one.
 * 				Otherwise, returns -1 and sets the last response code. (See getLastResponseCode().)
 */
static int gprgm_FindRecRatio(int max)
{
	uint16_t const enabled = constKrecsEnabledBitMap();
	uint16_t shift;
	int i;

	// Active recovery is disabled entirely
	if ((enabled & KREC_MASK) == 0)
	{
		setResponseCode(RESP_ACTIVE_RECOVERY_DISABLED);
		return -1;
	}

	for (i = max; i > 0; i--)
	{
		shift = 1 << (i - 1);
		if ((enabled & shift) != 0)
		{
			return i;
		}
	}

	setResponseCode(RESP_ACTIVE_RECOVERY_DISABLED);
	return -1;
}

static void gprgm_SelectInterphaseDelay(uint16_t ipd)
{
	int i;

	for (i = 0; i < NUM_PULSES; i++)
	{
		gp.pulse[i].interphaseDelay = ipd;
	}
}

/**
 * Set all GPULSE passive recovery widths to the same value.
 *
 * \param passiveWidth	The new passive recovery width
 */
static void gprgm_SelectPassiveWidth(uint16_t passiveWidth)
{
	int i;

	for (i = 0; i < NUM_PULSES; i++)
	{
		gp.pulse[i].passiveWidth = passiveWidth;
	}
}

/**
 * Set all GPULSE recovery ratios to the same value.
 *
 * \param recRatio	The new recovery ratio
 */
static void gprgm_SelectRecRatio(uint8_t recRatio)
{
	int i;

	for (i = 0; i < NUM_PULSES; i++)
	{
		gp.pulse[i].recRatio = recRatio;
	}
}


/**
 * Set the charge balance correction widths in all GPULSEs in the GPRGM.
 *
 * The last GPULSE in the program is set to use cbcWidth. The other GPULSEs
 * have their CBCs disabled (cbcWidth == 0).
 *
 * \param cbcWidth	The CBC width for the last pulse in the program.
 */
static void gprgm_SelectPassiveCBCWidth(uint16_t cbcWidth)
{
	int lastPulse;
	int i;

	lastPulse = gp.numPulses - 1;
	if (lastPulse >= 0)
	{
		for (i = 0; i < lastPulse; i++)
		{
			gp.pulse[i].cbcWidth = 0;
		}
		gp.pulse[lastPulse].cbcWidth = cbcWidth;
	}
}

/**
 * Set the charge balance correction widths in all GPULSEs in the GPRGM.
 *
 * All GPULSEs in the program are set to use the same CBC width.
 *
 * \param cbcWidth	The CBC width for all pulses in the program.
 */
static void gprgm_SelectActiveCBCWidth(uint16_t cbcWidth)
{
	int i;

	for (i = 0; i < NUM_PULSES; i++)
	{
		gp.pulse[i].cbcWidth = cbcWidth;
	}
}


/**
 * Determine whether the current GPRGM can use passive recovery.
 *
 * This is done by comparing the number of pulses to the frequency, in accordance
 * with "Switching between Passive and Active Recovery" in SWEX 0071, 4.1.3.1 in the 11/11 draft.
 *
 * \return true if the program may use passive recovery, false if it has to use active recovery.
 */
static bool gprgm_ProgramCanUsePassiveRecovery(void)
{
	uint16_t const freq 	= constLookUpFrequency(gp.freqIdx);
	uint16_t const thresh 	= constLookupPassiveRecoveryFrequencyThreshold(gp.numPulses);

	return freq <= thresh;
}

// ---------------------------------------------------------------------------------------------------------------------

/*
 * Automatically configure a program's waveform recovery options.
 *
 * Public API -- see GenericProgram.h for documentation.
 */
int gprgmAutoWaveformAdjustment(void)
{
	int recRatio;

	// An empty program needs no waveform adjustment
	if (gprgmGetNumPulses() == 0)
	{
		return 0;
	}

	if (gprgm_ProgramCanUsePassiveRecovery())
	{
		gprgm_SelectRecRatio(0);
		gprgm_SelectInterphaseDelay(constPassiveInterphaseDelay());
		gprgm_SelectPassiveWidth(constPassiveRecoveryWidth());
		gprgm_SelectPassiveCBCWidth(constPassiveCBCWidth());
	}
	else {

		int krecMax;
		if((krecMax = gprgm_ComputeMaxRecRatio()) < 0)
		{
			setResponseCode(RESP_PULSE_WIDTH_FREQ_CONFLICT);
			logError(E_LOG_ERR_STIM_PULSE_WIDTH_FREQ_CONFLICT,0);
			return -1;
		}
		else
		{
			if ((recRatio = gprgm_FindRecRatio(krecMax)) < 0)
			{
				return -1;
			}
			gprgm_SelectRecRatio(recRatio);
			gprgm_SelectInterphaseDelay(constActiveInterphaseDelay());
			gprgm_SelectActiveCBCWidth(constActiveCBCWidth());
		}
	}

	return 0;
}


/*
 * Automatically determine if a program is permitted to use uncontrolled source/sink channels,
 * and if so, reconfigure it accordingly.
 *
 * Public API -- see GenericProgram.h for documentation.
 */
int gprgmAutoUncontrolledSourceSink(void)
{
	/*
	 * Note to future implementors: The automatic uncontrolled source/sink algorithm
	 * in the November 2011 draft makes the USS determination *per phase*, while the
	 * current GPULSE structure has source/sink flags *per pulse*.  In order to
	 * correctly implement the algorithm it will be necessary to split the ussChannel flags
	 * into two arrays, one each for the stim and recovery phases.
	 */
	bool noUSS[NUM_CHANNELS];
	int i;

	for (i = 0; i < NUM_CHANNELS; i++)
	{
		noUSS[i] = false;
	}
	for (i = 0; i < gp.numPulses; i++)
	{
		memcpy(gp.pulse[i].ussChannel, noUSS, sizeof(gp.pulse[i].ussChannel));
	}

	return 0;
}


/*
 * Automatically calculate the compliance voltage needed for the GPRGM.
 *
 * Public API -- see GenericProgram.h for documentation.
 */
int gprgmAutoComplianceVoltage(void)
{
	uint32_t mv;	// Voltage for current pulse, in mV
	uint32_t maxv;	// Maximum voltage for all pulses, in mV
	int n;

	maxv = 0;
	for (n = 0; n < gp.numPulses; n++)
	{
		mv = gprgm_PulseVoltage(n);
		if (maxv < mv)
		{
			maxv = mv;
		}
	}

	gp.complianceDACValue = gprgm_FindBoostDACCode(maxv);

	// also set the reference current here
	gp.wf_ref = gprgm_FindReferenceCurrentIndex();

	return 0;
}

// ------------------------------------------------------
// added to calculate the compliance voltage without referencing the gp global generic program data structure
static uint16_t pulseVoltage(uint32_t amplitude_total, int8_t *percent, uint32_t pulseWidth, uint8_t krec)
{
	// The following variables and constants correspond to those of section 4.1.6 of the spec,
	// with capitalization changes as needed to comply with the C coding standard.

	uint32_t iTotal 		= amplitude_total;
	uint32_t iChan;
	uint32_t pwStim 		= pulseWidth;
	int 	 sActive 	= krec == 0 ? 0 : 1;

	// TODO: Confirm that these are not configurable
	uint32_t const cHV = 9400;							// High-voltage supply output capacitance, in nF
	uint32_t const vOffset = 1000;						// Fixed voltage drop across sources or sinks, in mV
	uint32_t const rSource = 100;						// Source resistance across source or sinks, in ohms
	uint32_t const cChan = 1000;						// Capacitance of DC blocking capacitor, in nF
	uint32_t const rTarget = calGetGenCals()->rTarget;	// Modeled load impedance, in ohms
	uint32_t voltage;

	uint16_t max;
	int i;

	if(percent == NULL)
	{
		iChan = iTotal;
	}
	else
	{
		max = 0;
		for (i = 0; i < NUM_CHANNELS; i++)
		{
			if (max < abs(percent[i]))
			{
				max = abs(percent[i]);
			}
		}

		iChan 	= iTotal * max;
		iChan 	/= 100;
	}

	voltage = (1 + sActive) * (iTotal * pwStim) / cHV	// uA*usec/nF = 10e-12/1-e-9=10e-3 = 10e-3 =  mV
				+ 2*(vOffset + iChan*rSource/1000)		// uA*ohm/10e+3 10e-6/10e3 = mV
				+ 2*(iChan * pwStim)/cChan				// uA*usec/nF = 10e-6*10e-6/10e-9=10e-12/10e-9=10e-3 = mV	:: this is the voltage across output caps
				+ iChan*rTarget/1000;					// uA*ohm/10e+3 = 10e-6/10e+3 = 10e-3 = mV					:: this is the voltage across the load

	return(voltage);

#if 0
	// Calculation is done in mV, but some terms are (uA * ohm) = uV.
	// Scaling by 1000 is needed for those terms.
																	// Unit correctness:
	return (1 + sActive) * (iTotal * pwStim) / cHV					// uA * us / nF = mV
			+ (1 + sControl) * (vOffset + iChan * rSource / 1000)	// mV + uA * ohm / 1000 = mV
			+ 2 * (iChan * pwStim) / cChan							// uA * us / nF = mV
			+ iChan * rTarget / 1000;								// uA * ohm * 1000 = mV
#endif
}

uint8_t gprgmComplianceVoltageIndex(uint32_t amplitude_total, int8_t *percent, uint32_t pulseWidth, uint8_t krec)
{
	uint16_t voltage;
	uint8_t	 index;

	voltage = pulseVoltage(amplitude_total, percent, pulseWidth, krec);
	index 	= gprgm_FindBoostDACCode(voltage);
	return(index);
}











