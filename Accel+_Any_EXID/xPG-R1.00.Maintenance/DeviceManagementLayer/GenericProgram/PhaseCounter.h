/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: Declarations of internal functions for stimMdl
 *	
 ***************************************************************************/

#ifndef PHASE_COUNTER_H_
#define PHASE_COUNTER_H_

#include <stdint.h>

void 		stimMdl_Init(void);
unsigned 	stimMdl_PgmNum(void);
uint8_t 	stimMdl_FirstPhase(void);
uint8_t 	stimMdl_NumPhases(void);
uint8_t 	stimMdl_NextPhase(void);
void 		stimMdl_LoadProgram(void);


#endif /*STIMMDL_INT_H_*/
