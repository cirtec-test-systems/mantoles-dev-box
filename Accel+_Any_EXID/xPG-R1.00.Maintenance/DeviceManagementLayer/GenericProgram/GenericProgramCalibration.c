/*
 * GenericProgramCalibration.c
 *
 *  Created on: Jan 28, 2013
 *      Author: garnold
 */

#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include <math.h>

#include "const.h"
#include "Stim.h"
#include "adc.h"
#include "gprgm_def.h"
#include "GenericProgram.h"
#include "PhaseCounter.h"
#include "log.h"
#include "cal.h"
#include "pgm.h"
#include "delay.h"
#include "Common/Protocol/cmndCalibrateChannel.h"
#include "lastResponse.h"

extern GPRGM 		gp;
extern unsigned 	pgmNum;

static void configADC()
{
	ADC_CONFIG 	adcConfig;

	adcOn();

	adcConfig.firstInput 	= ADC_CALIBRATION_RESIN;
	adcConfig.numInputs 	= 2;						// Read impedance pin
	adcConfig.reps			= 2;						// This number of times
	adcConfig.seqType		= CONVERT_ALL;				// read from a single trigger event
	adcConfig.speed			= ADC_FAST;
	adcConfig.trigger		= TRIGGER_SOFTWARE;			// trigger in software via setting a register bit
	adcConfig.sampleTime	= ADC_SAMPLE_TIME_16;

	adcSetup(&adcConfig);
}

static int getADCValue(uint32_t *value)
{
	uint16_t const volatile *data = NULL;

	adcConvert();

	if((data = adcResults(MS(CAL_TIMEOUT))) == NULL)
	{
		*value = 0;
		return(-1);
	}

	// find the difference between the two output signals - these are the voltages at each end of the calibration resistor
	// skip the first set of values and use the second set
	* value = data[2] > data[3] ? data[2]-data[3] : data[3]-data[2];

	return(0);
}

static CHAN calculateChannel(int n, uint16_t amplitude, int8_t ampPercent)
{
	CHAN 	chan;
	uint16_t dacCal;
	uint32_t amp;
	uint16_t channelCalCurrent;

	// Configure the channel polarity
	if (ampPercent < 0)
	{
		ampPercent = -ampPercent;
		chan.source = false;
		dacCal = calLookUpSinkingCal(gp.wf_ref, n);
	}
	else
	{
		chan.source = true;
		dacCal = calLookUpSourcingCal(gp.wf_ref, n);
	}

	switch(gp.wf_ref)
	{
		case CHANNEL_MAX_AMP_3:
			channelCalCurrent = 3000;
			break;
		case CHANNEL_MAX_AMP_6:
			channelCalCurrent = 6000;
			break;
		case CHANNEL_MAX_AMP_10:
			channelCalCurrent = 9000;
			break;
		default:
			channelCalCurrent = 15000;
	}

	amp = ((uint32_t)amplitude * (uint32_t)ampPercent * (uint32_t)dacCal) / ((uint32_t)channelCalCurrent * (uint32_t)100);

	chan.amp = (uint16_t)amp;
	chan.uncontrolled = false;

	return chan;
}

static int loadPhase(int gpulse, uint16_t pulseWidth, uint16_t delay)
{
	uint8_t 		phase;			// Phase to be written
	PHASE 			phx;			// Phase configuration structure
	int 			i, status;
	uint16_t 		amplitude;		// Amplitude of the pulse
	int8_t const 	*ampPercent;	// Channel amplitude percentage table

	phase = stimMdl_NextPhase();

	phx.uSource 		= false;
	phx.pulseWidth 		= pulseWidth;
	phx.delay 			= delay;
	phx.wav 			= pgmNum;
	phx.hvOff 			= false;
	phx.chargeBalance 	= false;
	phx.pause			= false;
	phx.sync			= false;
	phx.calibrate		= true;		// send current through calibration circuit
	phx.repetition		= 0;

	amplitude 	= gp.pulse[gpulse].amplitude;
	ampPercent 	= gp.pulse[gpulse].ampPercent;

	for (i = 0; i < STIM_CHANNELS; i++)
	{
		phx.chan[i] = calculateChannel(i, amplitude, ampPercent[i]);
		if(phx.chan[i].amp != 0)
		{
			DBGI(f_gp_calibration, (uint32_t)phx.chan[i].amp);
		}
	}

	DBGI(f_gp_calibration, 0);
	status = stimConfigPhase(phase, &phx, false);
	if(status != 0)
	{
		DBG(f_gp_calibration, DBG_SEV_ERROR, 0, 0);
	}
	DBGI(f_gp_calibration, 0);
	return(status);
}

int32_t gprgmCurrentBalanceCheck(int32_t *currentBalance)
{
	int 		status = 0;
	uint32_t 	adcCount;
	int64_t 	measured;

	uint16_t	pulseWidth, delay;
	uint8_t 	pulseMap, pulseNo;
	uint8_t 	voltageIndex;
	int			n;

	DBGI(f_gp_calibration, 0);

	configADC();

	for(n=0; n<4; n++)
	{
		currentBalance[n] = 0;
	}

	pulseWidth 	= 250; 	// usec
	delay		= 100;  // usec

	stimEnableCalibrator();

	pulseMap = pgm_GetPulseMap();

	for(pulseNo = 0; pulseMap != 0; pulseMap = pulseMap >> 1, pulseNo++)
	{
		stimMdl_LoadProgram();

		// do just the stimulation phase - no need to also do the recovery phase since we are running the current through an internal resistor
		if ((status = loadPhase(pulseNo, pulseWidth, delay)) < 0)
		{
			goto CLEANUP;
		}

		stimSetRectLevel(pgmNum, 255);

		if((status = stimConfigSequencerRegisters(pgmNum, stimMdl_FirstPhase(), stimMdl_NumPhases() )) < 0)
		{
			goto CLEANUP;
		}

		voltageIndex = gprgmComplianceVoltageIndex(gp.pulse[pulseNo].amplitude, NULL, pulseWidth, 1);
		DBGI(f_gp_calibration, voltageIndex);

		if (voltageIndex > stimGetComplianceDAC())
		{
			if (stimSetComplianceDAC(voltageIndex, true) != 0)
			{
				goto CLEANUP;
			}
		}
		else
		{
			if (stimSetComplianceDAC(voltageIndex, false) != 0)
			{
				goto CLEANUP;
			}
		}

		if(stimStart2(pgmNum, false, false, gp.wf_ref) != 0)
		{
			logError(E_LOG_STIM_STARTUP_ERROR, 0);
			DBG(f_gp_calibration, DBG_SEV_ERROR, 0, 0);
			setResponseCode(RESP_STIM_FAILED_SEVERE_ERROR);
			status = -1;
			goto CLEANUP;
		}

		DELAY_US(125);

		getADCValue(&adcCount);

#ifdef ORIGINAL_SRC_SINK_CAL
		// i = V/R = ((2.5/4095 [volts/bit] * voltage_delta [bits])/50 [ohms]) *1000000 [uAmps/Amp]
		measured = 2500000 * adcCount;
		measured /= 4095;
		measured /= 50;
#else
		// above calculations are simplified to a single multiply with the coefficient rounded down to integer
		measured = 12 * adcCount;
#endif
		currentBalance[pulseNo] = (int32_t)measured;

		stimWaitForStop();

		DBGI(f_gp_calibration, currentBalance[pulseNo]);
	}

CLEANUP:
	adcOff();
	stimDisableCalibrator();

	DBG(f_gp_calibration, DBG_SEV_ERROR, 0, 0);
	return(status);
}

#if 0
static int loadPhaseChannelCal(uint16_t amplitude, uint16_t pulseWidth, uint16_t delay, uint8_t channel, bool raw, bool src)
{
	uint8_t 		phase;			// Phase to be written
	PHASE 			phx;			// Phase configuration structure
	int8_t 			ampPercent;	// Channel amplitude percentage table
	int				i, status;

	phase = stimMdl_NextPhase();

	phx.uSource 		= false;
	phx.pulseWidth 		= pulseWidth;
	phx.delay 			= delay;
	phx.wav 			= pgmNum;
	phx.hvOff 			= false;
	phx.chargeBalance 	= false;
	phx.pause			= false;
	phx.sync			= false;
	phx.calibrate		= true;
	phx.repetition		= 0;

	for (i = 0; i < STIM_CHANNELS; i++)
	{
		phx.chan[i].amp = 0;
		phx.chan[i].source = false;
		phx.chan[i].uncontrolled = false;
	}

	// run single ended - do NOT set up both a src and sink.  Set up only one so current difference is routed through calibration resistor
	ampPercent = (src == true) ? 100 : -1 * 100;
	phx.chan[channel] = calculateChannel(channel, 1, amplitude, ampPercent, raw);

	status = stimConfigPhase(phase, &phx, false);
	if(status != 0)
	{
		DBG(f_gp_calibration, DBG_SEV_ERROR, 0, 0);
	}
	return(status);
}
#endif

int gprgmCalibrateChannel(CAL_CHAN_PARAMS const *p, CAL_CHAN_RESPONSE_DATA *r)
{
#ifndef ENABLE_CHANNEL_CAL

	r->amplitude 	= 1;
	r->chan 		= p->chan;
	r->flags 		= p->flags;

	return(-1);

#else
	bool raw;
	bool src;

	int status;
	ADC_CONFIG 	adcConfig;
	uint16_t const volatile *data = NULL;
	int 		n, reps = 2;
	int32_t 	sum;
	int32_t 	measured;

	uint16_t	pulseWidth, delay;
	uint8_t		voltageIndex;

	stimEnableCalibrator();

	if((p->flags & CAL_RAW) != 0)
	{
		raw = true;
	}
	else
	{
		raw = false;
	}

	if((p->flags & CAL_SRC) != 0)
	{
		src = true;
		stimSetCalibratorSinkMode(false);
	}
	else
	{
		src = false;
		stimSetCalibratorSinkMode(true);
	}

	adcConfig.firstInput 	= ADC_CALIBRATION_RESIN;	// Read the calibration pins
	adcConfig.numInputs 	= 8;						// Read both calibration pins
	adcConfig.reps			= reps;						// Read them reps times (2*reps total reads)
	adcConfig.seqType		= CONVERT_ALL;				// read from a single trigger event
	adcConfig.speed			= ADC_SLOW;
	adcConfig.trigger		= TRIGGER_SOFTWARE;
	adcConfig.sampleTime 	= ADC_SAMPLE_TIME_16;

	adcOn();
	adcSetup(&adcConfig);

	DBG(f_gp_calibration, DBG_SEV_NORM,0,0);

	pulseWidth 	= 2000; 	// usec
	delay		= 100;  	// usec

	stimMdl_LoadProgram();

	if ((status = loadPhaseChannelCal(p->amplitude, pulseWidth, delay, p->chan-1, raw, src)) < 0)
	{
		goto CLEANUP;
	}

	stimSetRectLevel(pgmNum, 255);

	if((status = stimConfigSequencerRegisters(pgmNum, stimMdl_FirstPhase(), stimMdl_NumPhases() )) < 0)
	{
		goto CLEANUP;
	}

	voltageIndex = gprgmComplianceVoltageIndex(p->amplitude, NULL, pulseWidth, 1);
	DBGI(f_gp_calibration, voltageIndex);

	if (voltageIndex > stimGetComplianceDAC())
	{
		if (stimSetComplianceDAC(voltageIndex, true) != 0)
		{
			goto CLEANUP;
		}
	}
	else
	{
		if (stimSetComplianceDAC(voltageIndex, false) != 0)
		{
			goto CLEANUP;
		}
	}

	stimStart(pgmNum, false, false);

	DELAY_US(10);

	adcConvert();

	data = adcResults(MS(CAL_TIMEOUT));



	sum = 0;
	// throw the first A/D sample
	for (n = 2; n < reps*2; n += 2)
	{
		sum = sum + data[n] - data[n + 1];
	}

	// i = V/R = ((2.5/4095 [volts/bit] * (voltage_delta_sum / number_of_samples) [bits])/50 [ohms]) *1000000 [uAmps/Amp]
	measured = 2500000 * sum;
	measured /= 4095;
	measured /= reps-1;
	measured /= 50;

	if(measured < 0)
	{
		measured *= -1;
	}

	r->amplitude = (uint16_t)measured;
	r->chan = p->chan;
	r->flags = p->flags;

	stimWaitForStop();

	DBGI(f_gp_calibration, r->amplitude);

	adcOff();
	stimDisableCalibrator();

	return 0;

CLEANUP:
	adcOff();
	DBG(f_gp_calibration, DBG_SEV_ERROR, 0, 0);
	return(status);
#endif
}


