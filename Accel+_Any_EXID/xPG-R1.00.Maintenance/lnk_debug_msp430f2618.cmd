/******************************************************************************/
/* lnk_ipg_msp430f2618.cmd - LINKER COMMAND FILE FOR LINKING MSP430F2618      */
/*                           PROGRAMS     									  */
/*                                                                            */
/*   Usage:  lnk430 <obj files...>    -o <out file> -m <map file> lnk.cmd     */
/*           cl430  <src files...> -z -o <out file> -m <map file> lnk.cmd     */
/*                                                                            */
/*----------------------------------------------------------------------------*/
/* These linker options are for command line linking only.  For IDE linking,  */
/* you should set your linker options in Project Properties                   */
/* -c                                               LINK USING C CONVENTIONS  */
/* -stack  0x0100                                   SOFTWARE STACK SIZE       */
/* -heap   0x0100                                   HEAP AREA SIZE            */
/*                                                                            */
/*----------------------------------------------------------------------------*/


/****************************************************************************/
/* SPECIFY THE SYSTEM MEMORY MAP                                            */
/****************************************************************************/

MEMORY
{
    SFR                     : origin = 0x0000, length = 0x0010
    PERIPHERALS_8BIT        : origin = 0x0010, length = 0x00F0
    PERIPHERALS_16BIT       : origin = 0x0100, length = 0x0100
    RAM                     : origin = 0x1100, length = 0x2000
    INFOA                   : origin = 0x10C0, length = 0x0040
    INFOB                   : origin = 0x1080, length = 0x0040
    INFOC                   : origin = 0x1040, length = 0x0040
    INFOD                   : origin = 0x1000, length = 0x0040

//	The region 0x3100 to 0x6FFF is reserved for the bootloader.
//	BOOTLOADER			()	: origin = 0x3100, length = 0x3f00

    PRGM_CONST				: origin = 0x7000, length = 0x0200
    PULSE_CONST				: origin = 0x7200, length = 0x0200
    TRIM_P					: origin = 0x7400, length = 0x0200
    TRIM_Z					: origin = 0x7600, length = 0x0200
    TRIM_S					: origin = 0x7800, length = 0x0200
    CHAN_CALS				: origin = 0x7A00, length = 0x0200
    GEN_CALS				: origin = 0x7C00, length = 0x0200
	HV_CALS					: origin = 0x7E00, length = 0x0200
    PRGM_CONST_BACKUP		: origin = 0x8000, length = 0x0200
    PULSE_CONST_BACKUP		: origin = 0x8200, length = 0x0200
    TRIM_P_BACKUP			: origin = 0x8400, length = 0x0200
    TRIM_Z_BACKUP			: origin = 0x8600, length = 0x0200
    TRIM_S_BACKUP			: origin = 0x8800, length = 0x0200
    CHAN_CALS_BACKUP		: origin = 0x8A00, length = 0x0200
    GEN_CALS_BACKUP			: origin = 0x8C00, length = 0x0200
	HV_CALS_BACKUP			: origin = 0x8E00, length = 0x0200

//							The starting address of the application is moved to
//							0x9000 so it starts on an a segment boundary and its
//							length is reduced so that it ends at 0xFE00.

	FLASH                   : origin = 0x9000, length = 0x6E00
	FLASH2                  : origin = 0x10000,length = 0x10000

//							Allowing 0xFE00 to 0xFFC0 makes it so that the interrupt
//							vectors need to be erased by the bootloader when writing
//							the 0xFE00 to 0xFFC0.  Not using those 1C0 bytes makes
//							the bootloader and its counterpart the PC Loader simpler.

    INT00                   : origin = 0xFFC0, length = 0x0002
    INT01                   : origin = 0xFFC2, length = 0x0002
    INT02                   : origin = 0xFFC4, length = 0x0002
    INT03                   : origin = 0xFFC6, length = 0x0002
    INT04                   : origin = 0xFFC8, length = 0x0002
    INT05                   : origin = 0xFFCA, length = 0x0002
    INT06                   : origin = 0xFFCC, length = 0x0002
    INT07                   : origin = 0xFFCE, length = 0x0002
    INT08                   : origin = 0xFFD0, length = 0x0002
    INT09                   : origin = 0xFFD2, length = 0x0002
    INT10                   : origin = 0xFFD4, length = 0x0002
    INT11                   : origin = 0xFFD6, length = 0x0002
    INT12                   : origin = 0xFFD8, length = 0x0002
    INT13                   : origin = 0xFFDA, length = 0x0002
    INT14                   : origin = 0xFFDC, length = 0x0002
    INT15                   : origin = 0xFFDE, length = 0x0002
    INT16                   : origin = 0xFFE0, length = 0x0002
    INT17                   : origin = 0xFFE2, length = 0x0002
    INT18                   : origin = 0xFFE4, length = 0x0002
    INT19                   : origin = 0xFFE6, length = 0x0002
    INT20                   : origin = 0xFFE8, length = 0x0002
    INT21                   : origin = 0xFFEA, length = 0x0002
    INT22                   : origin = 0xFFEC, length = 0x0002
    INT23                   : origin = 0xFFEE, length = 0x0002
    INT24                   : origin = 0xFFF0, length = 0x0002
    INT25                   : origin = 0xFFF2, length = 0x0002
    INT26                   : origin = 0xFFF4, length = 0x0002
    INT27                   : origin = 0xFFF6, length = 0x0002
    INT28                   : origin = 0xFFF8, length = 0x0002
    INT29                   : origin = 0xFFFA, length = 0x0002
    INT30                   : origin = 0xFFFC, length = 0x0002
    RESET                   : origin = 0xFFFE, length = 0x0002

// In order to get length checking of the NV space, it is set up as if it was
// a memory region at an impossible address.  The C code discards the upper
// bits of the address.
    NV : origin = 0x80000000, length = 0x8000
}

/****************************************************************************/
/* SPECIFY THE SECTIONS ALLOCATION INTO MEMORY                              */
/****************************************************************************/

SECTIONS
{
    .bss       : {} > RAM                /* GLOBAL & STATIC VARS              */
    .sysmem    : {} > RAM                /* DYNAMIC MEMORY ALLOCATION AREA    */
	.data	   : {} > RAM
    .stack     : {} > RAM (HIGH)         /* SOFTWARE SYSTEM STACK             */
	
    .text      : {}>> FLASH | FLASH2     /* CODE                              */
    .text:_isr : {} > FLASH              /* ISR CODE SPACE                    */
    .cinit     : {} > FLASH | FLASH2             /* INITIALIZATION TABLES             */
    .const     : {} > FLASH | FLASH2     /* CONSTANT DATA                     */
    .cio       : {} > RAM                /* C I/O BUFFER                      */
	
    .pinit     : {} > FLASH              /* C++ CONSTRUCTOR TABLES            */

	/*	We flag the rest of the sections as NOLOAD so that the new ELF-based TI libs do
	 *	NOT attempt to load the data. If an AVID board needs to get these sections initialized,
	 *	remove the NOLOAD's for a debug build
	 *	NOTE: NEVER remove the NOLOAD's for INFOA or INFOC, as they are very special cases
	 *	INFOA is calibration data, set only by the Elprotronic loader
	 *	INFOC is the Bootloader's Software Description Block, which is unknown to the application
	 */

    .infoA     : {} > INFOA, type = NOLOAD              /* MSP430 INFO FLASH MEMORY SEGMENTS */
    .infoB     : {} > INFOB
    .infoC     : {} > INFOC, type = NOLOAD
    .infoD     : {} > INFOD

    .chan_cals 			: {} >CHAN_CALS
    .gen_cals  			: {} >GEN_CALS
    .hv_cals   			: {} >HV_CALS
    .trim_s    			: {} >TRIM_S
    .trim_z    			: {} >TRIM_Z
    .trim_p    			: {} >TRIM_P
    .prgrm_const		: {} >PRGM_CONST
    .pulse_const		: {} >PULSE_CONST

    .chan_cals_backup	: {} >CHAN_CALS_BACKUP
    .gen_cals_backup  	: {} >GEN_CALS_BACKUP
    .hv_cals_backup   	: {} >HV_CALS_BACKUP
    .trim_s_backup    	: {} >TRIM_S_BACKUP
    .trim_z_backup    	: {} >TRIM_Z_BACKUP
    .trim_p_backup    	: {} >TRIM_P_BACKUP
    .prgrm_const_backup	: {} >PRGM_CONST_BACKUP
    .pulse_const_backup	: {} >PULSE_CONST_BACKUP


//     .int00   : {} > INT00                /* MSP430 INTERRUPT VECTORS          */
//     .int01   : {} > INT01
//     .int02   : {} > INT02
//     .int03   : {} > INT03
//     .int04   : {} > INT04
//     .int05   : {} > INT05
//     .int06   : {} > INT06
//     .int07   : {} > INT07
//     .int08   : {} > INT08
//     .int09   : {} > INT09
//     .int10   : {} > INT10
//     .int11   : {} > INT11
//     .int12   : {} > INT12
//     .int13   : {} > INT13
//     .int14   : {} > INT14
//     .int15   : {} > INT15
//     .int16   : {} > INT16
//     .int17   : {} > INT17
//     .int18   : {} > INT18
//     .int19   : {} > INT19
//     .int20   : {} > INT20
//     .int21   : {} > INT21
//     .int22   : {} > INT22
//     .int23   : {} > INT23
//     .int24   : {} > INT24
//     .int25   : {} > INT25
//     .int26   : {} > INT26
//     .int27   : {} > INT27
//     .int28   : {} > INT28
//     .int29   : {} > INT29
//     .int30   : {} > INT30
//     .reset   : {} > RESET              /* MSP430 RESET VECTOR               */ 
    
    RESERVED0    : { * ( .int00 ) } > INT00 type = VECT_INIT
    RESERVED1    : { * ( .int01 ) } > INT01 type = VECT_INIT
    RESERVED2    : { * ( .int02 ) } > INT02 type = VECT_INIT
    RESERVED3    : { * ( .int03 ) } > INT03 type = VECT_INIT
    RESERVED4    : { * ( .int04 ) } > INT04 type = VECT_INIT
    RESERVED5    : { * ( .int05 ) } > INT05 type = VECT_INIT
    RESERVED6    : { * ( .int06 ) } > INT06 type = VECT_INIT
    RESERVED7    : { * ( .int07 ) } > INT07 type = VECT_INIT
    RESERVED8    : { * ( .int08 ) } > INT08 type = VECT_INIT
    RESERVED9    : { * ( .int09 ) } > INT09 type = VECT_INIT
    RESERVED10   : { * ( .int10 ) } > INT10 type = VECT_INIT
    RESERVED11   : { * ( .int11 ) } > INT11 type = VECT_INIT
    RESERVED12   : { * ( .int12 ) } > INT12 type = VECT_INIT
    RESERVED13   : { * ( .int13 ) } > INT13 type = VECT_INIT
    DAC12        : { * ( .int14 ) } > INT14 type = VECT_INIT
    DMA          : { * ( .int15 ) } > INT15 type = VECT_INIT
    USCIAB1TX    : { * ( .int16 ) } > INT16 type = VECT_INIT
    USCIAB1RX    : { * ( .int17 ) } > INT17 type = VECT_INIT
    PORT1        : { * ( .int18 ) } > INT18 type = VECT_INIT
    PORT2        : { * ( .int19 ) } > INT19 type = VECT_INIT
    RESERVED20   : { * ( .int20 ) } > INT20 type = VECT_INIT
    ADC12        : { * ( .int21 ) } > INT21 type = VECT_INIT
    USCIAB0TX    : { * ( .int22 ) } > INT22 type = VECT_INIT
    USCIAB0RX    : { * ( .int23 ) } > INT23 type = VECT_INIT
    TIMERA1      : { * ( .int24 ) } > INT24 type = VECT_INIT
    TIMERA0      : { * ( .int25 ) } > INT25 type = VECT_INIT
    WDT          : { * ( .int26 ) } > INT26 type = VECT_INIT
    COMPARATORA   : { * ( .int27 ) } > INT27 type = VECT_INIT
    TIMERB1      : { * ( .int28 ) } > INT28 type = VECT_INIT
    TIMERB0      : { * ( .int29 ) } > INT29 type = VECT_INIT
    NMI          : { * ( .int30 ) } > INT30 type = VECT_INIT
    .reset       : {}               > RESET  /* MSP430 Reset vector         */

    // FRAM address space.
    // Set to NOLOAD so that FRAM contents do not appear in the output file.
    nv		 : {} > NV, type = NOLOAD

    UNION
    {
		GROUP
		{
			.flashBlockWrite: {flashBlockWrite.obj( .text ) }
		} load = FLASH, table(_flashBlockWriteTable)
		GROUP
		{
			.flashSegmentErase: {flashSegmentErase.obj( .text ) }
		} load = FLASH, table(_flashSegmentEraseTable)
		GROUP
		{
			.flashVerify: {flashVerify.obj( .text ) }
		} load = FLASH, table(_flashVerifyTable)
	} run = RAM
    .ovly > FLASH | FLASH2
}
/****************************************************************************/
/* INCLUDE PERIPHERALS MEMORY MAP                                           */
/****************************************************************************/

-l msp430f2618.cmd

