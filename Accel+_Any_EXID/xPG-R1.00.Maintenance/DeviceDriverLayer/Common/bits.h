/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Defines MSP430 port pin assignments  
 *	
 ***************************************************************************/

#ifndef BITS_H_
#define BITS_H_

/**
 * \page pinAssignments MSP430 Pin Assignments
 * 
 * The MSP430F2618 has eight I/O ports with eight pins each. The assignments
 * of these pins, grouped by subsystem, are listed here.
 * 
 * \subpage accelBits
 * 
 * \subpage miscBits
 * 
 * \subpage plutoBits
 * 
 * \subpage saturnBits
 * 
 * \subpage memBits
 * 
 * \subpage micsBits
 * 
 * \subpage unusedBits
 * 
 */

/**
 * \page saturnBits	Saturn pin assignments
 *  
 * As of 04/05/2011, these are the I/O signals to Saturn, the stim ASIC.
 * 
 * <table>
 * 	 <tr><th>Port.Bit</th> <th>Signal</th>          <th>Type</th></tr>
 *   <tr><td>(P2.5)</td>   <td>(HVDD_CONN)</td>     <td>(Output - to power asic also)</td></tr>
 *   <tr><td>P2.6</td>     <td>STIM_TFLG</td>       <td>Input (interrupt)</td></tr>
 *   <tr><td>P2.7</td>     <td>STIM_ERR</td>        <td>Input (interrupt)</td></tr>
 *   <tr><td>P3.0</td>     <td>STIM_SCK</td>        <td>Output (SPI SCK)</td></tr>
 *   <tr><td>P3.4</td>     <td>STIM_SDI</td>        <td>Output (SPI SDI)</td></tr>
 *   <tr><td>P3.5</td>     <td>STIM_SDO</td>        <td>Input  (SPI SDO)</td></tr>
 *   <tr><td>P4.0</td>     <td>STIM_STOP</td>       <td>Output</td></tr>
 *   <tr><td>P4.1</td>     <td>STIM_CONT</td>       <td>Output</td></tr>
 *   <tr><td>P4.2</td>     <td>STIM_RUN</td>        <td>Output</td></tr>
 *   <tr><td>P4.3</td>     <td>STIM_SEL</td>        <td>Output</td></tr>
 *   <tr><td>P4.4</td>     <td>STIM_SYNC</td>       <td>Input</td></tr>
 *   <tr><td>P4.5</td>     <td>STIM_PAUSE</td>      <td>Input</td></tr>
 *   <tr><td>P4.6</td>     <td>STIM_PROG</td>       <td>Input</td></tr>
 *   <tr><td>P4.7</td>     <td>STIM_CS</td>         <td>Output (SPI chip select)</td></tr>
 *   <tr><td>P5.0</td>     <td>STIM_RST</td>        <td>Output</td></tr>
 *   <tr><td>(P5.5)</td>   <td>(STIM_CLK)</td>      <td>(Output - special handling)</td></tr>
 *   <tr><td>P6.0</td>     <td>STIM_IMPM</td>       <td>Input, analog</td></tr>
 *   <tr><td>P6.1</td>     <td>STIM_CAL_RESIN</td>  <td>Input, analog (aka STIM_CAL1)</td></tr>
 *   <tr><td>P6.2</td>     <td>STIM_CAL_RESOUT</td> <td>Input, analog (aka STIM_CAL2)</td></tr>
 *   <tr><td>P6.3</td>     <td>STIM_CAL_CAPIN</td>  <td>Input, analog (aka STIM_CAL3)</td></tr>
 * </table>
 */

/**
 * \page plutoBits Pluto pin assignments
 *  
 * As of 4/5/2011, these are the I/O signals to Pluto, the power ASIC.
 *
 * Updated 3/28/2012 to include charge rate signal.
 * 
 * Since Pluto is always powered up, no need to worry that signals
 * can parasitically power the chip.
 * 
 * <table>
 * 	 <tr><th>Port.Bit</th>	<th>Signal</th>			<th>Type</th></tr>
 *   <tr><td>P1.3</td>		<td>PWR_CHARGE_RATE</td><td>Output (0=C/8, 1=C/4)</td></tr>
 *   <tr><td>P1.4</td>		<td>PWR_V_DET</td>		<td>Input (interrupt)</td></tr>
 *   <tr><td>P1.5</td>		<td>PWR_BRWN_OUT</td>	<td>Input (interrupt)</td></tr>
 *   <tr><td>P1.6</td>		<td>PWR_MAG_SW</td>		<td>Input (interrupt)</td></tr>
 *   <tr><td>P2.0</td>		<td>PWR_MAG_2S</td>		<td>Input (interrupt)</td></tr>
 *   <tr><td>P2.1</td>		<td>PWR_MAG_5S</td>		<td>Input (interrupt) (???)</td></tr>
 *   <tr><td>P2.2</td>		<td>PWR_CS</td>			<td>Output (SPI SCS)</td></tr>
 *   <tr><td>P2.3</td>		<td>PWR_UV_PROT</td>	<td>Output</td></tr>
 *   <tr><td>P2.4</td>		<td>PWR_BBC_CTL</td>	<td>Output</td></tr>
 *   <tr><td>P2.5</td>		<td>HVDD_CONN</td>		<td>Output (also to stim asic)</td></tr>
 *   <tr><td>P3.1</td>		<td>PWR_SDI</td>		<td>Output (SPI MOSI)</td></tr>
 *   <tr><td>P3.2</td>		<td>PWR_SDO</td>		<td>Input (SPI MISO)</td></tr>
 *   <tr><td>P3.3</td>		<td>PWR_SCK</td>		<td>Output (SPI SCK)</td></tr>
 *   <tr><td>P3.6</td>		<td>TETS_TXD</td>		<td>Output (Async TXD)</td></tr>
 *   <tr><td>P3.7</td>		<td>TETS_RXD</td>		<td>Input (Async RXD)</td></tr>
 *   <tr><td>P5.7</td>		<td>MICS_WU_EN</td>		<td>Output</td></tr>
 *   <tr><td>P6.4</td>		<td>PWR_ANA_OUT</td>	<td>Input (analog)</td></tr>
 *   <tr><td>P8.0</td>		<td>ASK_BLANK</td>		<td>Output</td></tr>
 *   <tr><td>P8.1</td>		<td>PWR_LDO_EN</td>		<td>Output</td></tr>
 *   <tr><td>P8.2</td>		<td>PWR_MAG_TO_OVRD</td><td>Output</td></tr>
 * </table>
 */

/**
 * \page micsBits ZL7010x MICS pin assignments
 * 
 * As of 7/25/2011, these are the I/O signals to the ZL7010x
 *
 * <table>
 * 	 <tr><th>Port.Bit</th>	<th>Signal</th>		<th>Type</th></tr>
 * 	 <tr><td>P1.2</td>		<td>MICS_IRQ</td>	<td>Input (interrupt)</td></tr>
 * 	 <tr><td>P5.1</td>		<td>MICS_SDI</td>	<td>Output (SPI MOSI)</td></tr>
 *	 <tr><td>P5.2</td>		<td>MICS_SDO</td>	<td>Input (SPI MISO)</td></tr>
 *   <tr><td>P5.3</td>		<td>MICS_SCK</td>	<td>Output (SPI SCK)</td></tr>
 *   <tr><td>P5.7</td>		<td>MICS_WU_EN</td>	<td>Output</td></tr>
 *   <tr><td>P7.0</td>		<td>MICS_PO4</td>	<td>Input</td></tr>
 *	 <tr><td>P7.6</td>		<td>MICS_CS</td>	<td>Output (SPI SCS)</td></tr>
 * </table>
 */

/**
 * \page accelBits Accelerometer pin assignments
 *  
 * As of 7/25/2011, these are the I/O signals to the accelerometer
 *
 * <table>
 * 	 <tr><th>Port.Bit</th>	<th>Signal</th>		<th>Type</th></tr>
 *   <tr><td>P1.0</td>		<td>ACC_RANGE</td>	<td>Output</td></tr>
 *   <tr><td>P1.1</td>		<td>ACC_PWR_DN</td>	<td>Output</td></tr>
 *   <tr><td>P6.5</td>		<td>ACC_Y</td>		<td>Analog input</td></tr>
 *   <tr><td>P6.6</td>		<td>ACC_X</td>		<td>Analog input</td></tr>
 *   <tr><td>P6.7</td>		<td>ACC_Z</td>		<td>Analog input</td></tr>
 * </table>
 */

/**
 * \page memBits Serial FRAM pin assignments
 *  
 * As of 7/25/2011, these are the I/O signals to the FM25V02-DG FRAM
 *
 * <table>
 * 	 <tr><th>Port.Bit</th>	<th>Signal</th>		<th>Type</th></tr>
 *   <tr><td>P8.3</td>		<td>MEM_CS</td>		<td>Output (low true)</td></tr>
 *   <tr><td>P8.4</td>		<td>MEM_SCK</td>	<td>Output (SPI SCK)</td></tr>
 *   <tr><td>P8.5</td>		<td>MEM_SDI</td>	<td>Output (SPI MOSI)</td></tr>
 *   <tr><td>P8.6</td>		<td>MEM_SDO</td>	<td>Input (SPI MISO)</td></tr>
 * </table>
 */

/**
 * \page miscBits Miscellaneous pin assignments
 *  
 * As of 7/25/2011, these are the MSP430 port pins that have miscellaneous
 * functions.
 *
 * <table>
 * 	 <tr><th>Port.Bit</th>	<th>Signal</th>	<th>Type</th></tr>
 *   <tr><td>P5.4</td>		<td>MCLK</td>		<td>Test point</td></tr>
 *   <tr><td>P5.6</td>		<td>ACLK</td>		<td>Test point</td></tr>
 *   <tr><td>P7.7</td>		<td>TST_TRIG</td>	<td>Test point</td></tr>
 *   <tr><td>P8.7</td>		<td>EPG_MODE</td>	<td>Input</td></tr>
 * </table>
 */

/**
 * \page unusedBits Unused pin assignments
 *  
 * As of 7/25/2011, these are the MSP430 port pins that are unused by the
 * IPG firmware.
 *
 * <table>
 * 	 <tr><th>Port.Bit</th>	<th>Signal</th>	<th>Type</th></tr>
 *   <tr><td>P1.7</td>		<td>n/a</td>		<td>No connect</td></tr>
 *   <tr><td>P7.1</td>		<td>n/a</td>		<td>No connect</td></tr>
 *   <tr><td>P7.2</td>		<td>n/a</td>		<td>No connect</td></tr>
 *   <tr><td>P7.3</td>		<td>n/a</td>		<td>No connect</td></tr>
 *   <tr><td>P7.4</td>		<td>n/a</td>		<td>No connect</td></tr>
 *   <tr><td>P7.5</td>		<td>n/a</td>		<td>No connect</td></tr>
 * </table>
 */

/*@-enummemuse@*/

/**
 * Port P1.  Interrupt capable.  IPG Only.
 */
enum P1_BITS {
	ACC_RANGE 		= 0x01,		///< Accelerometer range control
	ACC_PWR_DN 		= 0x02,		///< Accelerometer power down control
	MICS_IRQ 		= 0x04, 	///< Interrupt line from ZL7010x (MICS_IRQ on schematic)
	PWR_CHARGE_RATE	= 0x08,		///< IPG - Charge Rate (C/8 or C/4) (Note: Different for EPG)
	LED_GREEN		= 0x08,		///< EPG - Green LED (Note: Different for IPG)
	PWR_V_DET 		= 0x10, 	///< Pluto vrect comparator
	PWR_BRWN_OUT 	= 0x20,		///< Pluto brown out detection
	PWR_MAG_SW 		= 0x40,		///< Pluto magnet sensor (active low)
#ifdef EPG
	PWR_MICS_STRB	= 0x80		///< MICS Strobe Line Used for MICS Clock Check
#elif defined IPG_REV05
	P1_7_NC			= 0x80,		///< IPG - Unused (Note: Different for EPG)
 	LED_RED			= 0x80		///< EPG - Red LED (Note: Different for IPG)
#else
	PWR_MICS_STRB	= 0x80		///< MICS Strobe Line Used for MICS Clock Check
#endif
};


/**
 * Port P2.  Interrupt capable.
 */
enum P2_BITS {
	PWR_MAG_2S 	= 0x01, ///< Pluto magnet sensor 2 seconds (active high)
	PWR_MAG_5S	= 0x02, ///< Pluto magnet sensor 5 seconds (active high)
	PWR_CS		= 0x04, ///< Pluto SPI chip select
	PWR_UV_PROT	= 0x08,	///< Pluto undervoltage protect signal
	PWR_BBC_CTL	= 0x10, ///< Pluto pass switch/buck-boost ctrl signal
	HVDD_CONN	= 0x20,	///< High voltage connect, to Saturn and Pluto
	STIM_TFLG	= 0x40,	///< Saturn TFLG pin (active high)
	STIM_ERR	= 0x80	///< Saturn ERR_n pin (active low)
};

/**
 * Port P3.  USCI ports.
 */
enum P3_BITS {
	STIM_SCK 	= 0x01, ///< Saturn SCK input  (UCA0SCK)
	PWR_SDI 	= 0x02, ///< Pluto SPI MOSI (UCB0SIMO)
	PWR_SDO 	= 0x04,	///< Pluto SPI MISO (UCB0SOMI)
	PWR_SCK 	= 0x08, ///< Pluto SPI clock (UCB0SCK)
	STIM_SDI 	= 0x10, ///< Saturn SDI input  (UCA0MOSI)
	STIM_SDO 	= 0x20, ///< Saturn SDO output (UCA0MISO)
	TETS_TXD 	= 0x40,	///< Pluto TETS transmit data (UCA1TXD)
	TETS_RXD 	= 0x80	///< Pluto TETS receive data (UCA1RXD)
};

/**
 * Port P4 (Timer_B): Saturn control signals
 */
enum P4_BITS {
	STIM_STOP 	= 0x01,	///< Saturn STOP input
	STIM_CONT 	= 0x02,	///< Saturn CONT input
	STIM_RUN 	= 0x04,	///< Saturn RUN input
	STIM_SEL 	= 0x08,	///< Saturn SEL input
	STIM_SYNC 	= 0x10,	///< Saturn SYNC output
	STIM_PAUSE 	= 0x20,	///< Saturn PAUSE output
	STIM_PROG 	= 0x40,	///< Saturn PROG output
#ifdef EPG
	MICS_PO3	= 0x80	///< MICS Power Clock
#elif defined IPG_REV05
	STIM_CS 	= 0x80	///< Saturn SCSb input (SPI chip select, active low)
#else
	MICS_PO3	= 0x80	///< MICS Power Clock
#endif
};

/**
 * Port P5
 */
enum P5_BITS {
	STIM_RST	= 0x01,	///< Saturn RST_n input (active low)
	MICS_SDI	= 0x02,	///< ZL SPI MOSI (aka SIMO)
	MICS_SDO	= 0x04,	///< ZL MICS SPI MISO (aka SOMI)
	MICS_SCK	= 0x08,	///< ZL SPI CLK
	MCLK		= 0x10,	///< MCLK (test point only)
	STIM_CLK	= 0x20,	///< Saturn CLK input (MSP430 MCLK output)
	ACLK		= 0x40,	///< ACLK (test point only)
	MICS_WU_EN	= 0x80	///< ZL MICS_WU_EN signal, aka PWR_STROBE_IN on schematic
};

/**
 * Port P6: Analog inputs
 */
enum P6_BITS {
	STIM_IMPM 		= 0x01,	///< Saturn IMPM_OUT pin
	STIM_CAL_RESIN 	= 0x02,	///< Saturn CAL_RESIN pin 
	STIM_CAL_RESOUT	= 0x04, ///< Saturn CAL_RESOUT pin
	P6_3_NC 		= 0x08,	///< No Connect
	PWR_ANA_OUT 	= 0x10,	///< Input from Pluto's analog mux
	ACC_Y 			= 0x20,	///< Accelerometer Y output
	ACC_X 			= 0x40,	///< Accelerometer X output
	ACC_Z 			= 0x80	///< Accelerometer Z output
};

/**
 * Port P7
 */
enum P7_BITS {
#ifdef EPG
	MICS_PO4	= 0x01,	///< Input from ZL7010x's Programmable Output #4
#elif defined IPG_REV05
	MICS_PO4	= 0x01,	///< Input from ZL7010x's Programmable Output #4
#else
	LOAD_VRECT	= 0x01,	///< Should be configured as output low to disable
#endif
	P7_1_NC 	= 0x02,	///< Unused
	P7_2_NC 	= 0x04,	///< Unused
#ifdef EPG
	STIM_CS		= 0x08,	///< Saturn SCSb input (SPI chip select, active low)
#elif defined IPG_REV05
	P7_3_NC 	= 0x08,	///< Unused
#else
	STIM_CS		= 0x08,	///< Saturn SCSb input (SPI chip select, active low)
#endif
	P7_4_NC		= 0x10,	///< IPG - Unused (Note: Different for EPG)
	LED_STIM 	= 0x10,	///< EPG - Stim LED (Note: Different for IPG)
#ifdef EPG
	LED_RED		= 0x20, ///< EPG - Red LED (Note: Different for IPG)
#elif defined IPG_REV05
	P7_5_NC 	= 0x20,	///< Unused
#else
	P7_5_NC 	= 0x20,	///< IPG - Unused
#endif
	MICS_CS		= 0x40,	///< Chip select to ZL7010x
	TST_TRIG	= 0x80	///< Test trigger (test point only)
};

/**
 * Port P8
 */

enum P8_BITS {
	ASK_BLANK		= 0x01,	///< Pluto ASK blanking signal
	PWR_LDO_EN		= 0x02,	///< Pluto ldo_ctl 
	PWR_MAG_TO_OVRD	= 0x04,	///< Pluto magnet timeout override signal
	MEM_CS			= 0x08,	///< FRAM SPI chip select (low true)
	MEM_SCK			= 0x10,	///< FRAM SPI clock
	MEM_SDI			= 0x20,	///< FRAM SPI MOSI
	MEM_SDO			= 0x40,	///< FRAM SPI MISO
	EPG_MODE		= 0x80	///< EPG detect (1 for EPG, 0 for IPG)
};

#endif /*BITS_H_*/
