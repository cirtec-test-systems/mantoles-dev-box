/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Common driver routines for EPG/IPG
 *
 ***************************************************************************/

#include "system.h"
#include "Interrupt.h"
#include "msp430f2618.h"
#include "DeviceDriverLayer/Common/bits.h"
#include "mics.h"

bool isIPG(void)
{
	return (P8IN & EPG_MODE) == 0;
}

void setTST_TRIG_HIGH(void)
{
    P7OUT |= TST_TRIG;
}

void setTST_TRIG_LOW(void)
{
    P7OUT &= ~TST_TRIG;;
}

void setTST_TRIG_TOGGLE(void)
{
    P7OUT ^= TST_TRIG;
}

void resetSystem(void)
{
	void (* function_pointer)();
	function_pointer = (void (*)())RESET_VECTOR;

	interruptDisable();
	micsShutdown();

	function_pointer();
}
