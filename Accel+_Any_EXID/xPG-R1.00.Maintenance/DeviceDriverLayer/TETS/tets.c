/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: TETS communications Conductor
 *
 ***************************************************************************/

#include "DeviceDriverLayer/Common/msp430f2618.h"		// For interrupt vector
#include "delay.h"
#include "tets.h"
#include "Interrupt.h"
#include "system_events.h"
#include "EventQueue.h"

#include "DeviceDriverLayer/PowerASIC/pluto_bits.h"

#define DEFAULT_ASK_POLARITY true  // If true, sets the ASKP bit to flip polarity


/**
 * Initialize the TETS communications subsystem
 */

void tetsInit(void)
{
	pwrSetASKPolarity(DEFAULT_ASK_POLARITY);
}


/**
 * Detune TETS
 */
void tetsDetune(void)
{
	P3SEL &= ~TETS_TXD;			// select the pin as GPIO instead of the UART

	P3OUT &= ~TETS_TXD;			// set TETS_TXD low
	P3DIR |= TETS_TXD;			// set TETS_TXD as an output
	P3REN &= ~TETS_RXD;			// enable the pulldown resistor
}

#pragma vector = USCIAB1RX_VECTOR
interrupt void tetsHandleReceiveInt(void)
{
	interruptHandlerPrologue();
	_low_power_mode_off_on_exit();
}

#pragma vector = USCIAB1TX_VECTOR
interrupt void tetsHandleTransmitInt(void)
{
	interruptHandlerPrologue();
	_low_power_mode_off_on_exit();
}
