#include "DeviceDriverLayer/Common/msp430f2618.h"
#include "DeviceDriverLayer/Common/bits.h"

#include "a_Saturn.h"
#include "stimspi.h"
#include "a_RegisterIO.h"


void stimReadRegisters(uint16_t addr, void *buf, size_t len)
{
	stimSPI_Read(addr, buf, len);
}

int stimWriteRegisters(uint16_t addr, void const *buf, size_t len)
{
	return stimSPI_Write(addr, buf, len);
}

// mask operations

int sat_Set(uint16_t addr, uint8_t mask)
{
	return stimSPI_Put(addr, mask | stimSPI_Get(addr));
}

int sat_Clear(uint16_t addr, uint8_t mask)
{
	return stimSPI_Put(addr, ~mask & stimSPI_Get(addr));
}


// some shared functions that enable writes

int globalWriteEnable()
{
	if(stimSPI_Put(SAT_WE1, 0xFF) < 0)
	{
		return(-1);
	}
	if(stimSPI_Put(SAT_WE2, 0xFF) < 0)
	{
		return(-1);
	}
	if(stimSPI_Put(SAT_WE3, 0xFF) < 0)
	{
		return(-1);
	}
	if(stimSPI_Put(SAT_WE4, 0xFF) < 0)
	{
		return(-1);
	}
	if(stimSPI_Put(SAT_WE5, 0xFF) < 0)
	{
		return(-1);
	}
	return(0);
}

int globalWriteDisable()
{
	if(stimSPI_Put(SAT_WE1, 0x00) < 0)
	{
		return(-1);
	}
	stimSPI_Put(SAT_WE2, 0x00);
	stimSPI_Put(SAT_WE3, 0x00);
	stimSPI_Put(SAT_WE4, 0x00);
	stimSPI_Put(SAT_WE5, 0x00);

	return(0);
}

int globalRegisterValid()
{
	if(stimSPI_Put(SAT_RVLD1, 0xFF) < 0)
	{
		return(-1);
	}
	if(stimSPI_Put(SAT_RVLD2, 0xFF) < 0)
	{
		return(-1);
	}
	if(stimSPI_Put(SAT_RVLD3, 0xFF) < 0)
	{
		return(-1);
	}
	if(stimSPI_Put(SAT_RVLD4, 0xFF) < 0)
	{
		return(-1);
	}
	if(stimSPI_Put(SAT_RVLD5, 0x03) < 0)
	{
		return(-1);
	}
	return(0);
}

void RegisterClearFlags()
{
	uint8_t flags;

	flags = stimSPI_Get(SAT_FLG);
	if(flags != 0x00)
	{
		stimSPI_UnverifiedPut(SAT_FLG, flags);
	}
}







