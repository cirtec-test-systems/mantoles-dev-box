#include "Stim.h"
#include "PowerASIC.h"
#include "delay.h"
#include "error_code.h"
#include "log.h"
#include "lastresponse.h"
#include "watchdog.h"
#include "Clock.h"

#include "DeviceDriverLayer/Common/msp430f2618.h"
#include "DeviceDriverLayer/Common/bits.h"
#include "a_Saturn.h"
#include "stimspi.h"
#include "a_RegisterIO.h"
#include "a_Interrupt.h"
#include "system.h"

extern uint8_t g_nextPhaseBank;

uint8_t registerInit = 0;
uint16_t g_globalChannelScale = 1000;

static int applyTrimValues()
{
	int result = 0;
	STIM_TRIM_VALS vals;

	trimGetStimConstants(&vals);

	// remember the global gain value so we can use it when we load the amplitude register
	g_globalChannelScale = vals.STIM_GLOBAL_CHAN_SCALE;

	DBG(f_stim_startup, vals.STIM_BG_TRIM, vals.STIM_CASN_TRIM, vals.STIM_CASP_TRIM);

	if (stimSPI_Put(SAT_BG_CTRL, vals.STIM_BG_TRIM ) < 0)
	{
		result = -1;
	}
	if (stimSPI_Put(SAT_PGO_CTRL, vals.STIM_PGO_TRIM) < 0)
	{
		result = -1;
	}
	if (stimSPI_Put(SAT_CG_CTRL, (0x88 + vals.STIM_CASN_TRIM + vals.STIM_HVDDL_TRIM)) < 0)
	{
		result = -1;
	}
	if (stimSPI_Put(SAT_CM_CTRL, vals.STIM_CASP_TRIM) < 0)
	{
		result = -1;
	}

	return result;
}

static void engagePulldowns(void)
{
	P6SEL &= ~(STIM_IMPM | STIM_CAL_RESOUT | STIM_CAL_RESIN);

	// inputs to the MSP
	P2REN |= (STIM_TFLG | STIM_ERR);
    P4REN |= (STIM_SYNC | STIM_PAUSE | STIM_PROG);
    P6REN |= (STIM_IMPM | STIM_CAL_RESOUT | STIM_CAL_RESIN);
}

static void releasePulldowns(void)
{
	// inputs to the MSP
	P2REN &= ~(STIM_TFLG | STIM_ERR);
    P4REN &= ~(STIM_SYNC | STIM_PAUSE | STIM_PROG);

    P6DIR &= ~(STIM_IMPM | STIM_CAL_RESOUT | STIM_CAL_RESIN);
    P6REN &= ~(STIM_IMPM | STIM_CAL_RESOUT | STIM_CAL_RESIN);

    // route the pins to the A/D converter
    P6SEL |= STIM_IMPM | STIM_CAL_RESOUT | STIM_CAL_RESIN;
}


// -----------------------------------------------------------------------------------------------------------
int stimPowerOn(void)
{
	uint8_t bc_on;
	int i;
	int error = 0;

	clkOscillatorFaultInterruptDisable();

	// perform soft start by temporarily putting a voltage on the output lines
	P4DIR &= ~(STIM_STOP | STIM_CONT | STIM_RUN | STIM_SEL);
	P4REN |= (STIM_STOP | STIM_CONT | STIM_RUN | STIM_SEL);
	P4OUT |= (STIM_STOP | STIM_CONT | STIM_RUN | STIM_SEL);

	DELAY_MS(50);

	// Power and calibration values

	if(pwrSatEnableDVDD() < 0)
	{
		error = 1;
		goto EXIT_FAIL;
	}

	DELAY_MS(2);

	// remove the temporary voltage and configure the outputs correctly
	P4OUT &= ~(STIM_STOP | STIM_CONT | STIM_RUN | STIM_SEL);
	P4REN &= ~(STIM_STOP | STIM_CONT | STIM_RUN | STIM_SEL);
	P4DIR |= (STIM_STOP | STIM_CONT | STIM_RUN | STIM_SEL);

	// the inputs were tied to ground so release them
	releasePulldowns();

	P5OUT |= STIM_RST;

	DELAY_MS(2);

	P5OUT |= STIM_CLK;
	P5OUT &= ~STIM_CLK;
	P5OUT |= STIM_CLK;
	P5OUT &= ~STIM_CLK;

	P3OUT |= STIM_SCK;
	P3OUT &= ~STIM_SCK;
	P3OUT |= STIM_SCK;
	P3OUT &= ~STIM_SCK;

	DELAY_MS(2);

#ifdef EPG
	P7OUT |= STIM_CS;
#elif defined IPG_REV05
	P4OUT |= STIM_CS;
#else
	P7OUT |= STIM_CS;
#endif

	stimSPI_Resume();

	if(pwrSatEnableAVDD() < 0)
	{
		error = 2;
		goto EXIT_FAIL;
	}

	P2OUT |= HVDD_CONN;

	DELAY_MS(10);

	// set up write enables
	// for now, enable all the write and valid bits
	if(globalWriteEnable() < 0)
	{
		error = 3;
		goto EXIT_FAIL;
	}

	// clear the interrupt register
	RegisterClearFlags();

	// stimSPI_Put(SAT_WE5, PWRWE | AWE | TRIMWE);

	applyTrimValues();

	// Internal Circuits
	if(stimSPI_Put(SAT_ENABLE, BCE | BGE) < 0)
	{
		error = 6;
		goto EXIT_FAIL;
	}


	DELAY_MS(2);

	// stim asic spec says to write the boost voltage index twice with a delay between
	stimSPI_Put(SAT_BC_CTRL0, 0x0C);

	stimSPI_UnverifiedPut(SAT_BC_CTRL1, POR | ICC0 | ICC1);

	DELAY_US(20);

	stimSPI_UnverifiedPut(SAT_BC_CTRL1, POR | ICC0 | ICC1 | VBOOST_EN);


	i=0;
	do
	{
		if((bc_on = stimSPI_Get(SAT_BC_CTRL1) & BCG) == 0)
		{
			wdServiceWatchdog();
			DELAY_MS(1);
		}
		i++;
	}while(i < 10 && bc_on == 0);

	if(bc_on == 0)
	{
		DBG(f_stim_startup, DBG_SEV_ERROR,0,0);
		error = 7;
	}

	stimSPI_UnverifiedPut(SAT_VG_CTRL, HVDDL_EN);

	DELAY_MS(1);

	stimSPI_UnverifiedPut(SAT_VG_CTRL, HVDDL_EN | LS_RESET);

	//setTST_TRIG_HIGH();

	DELAY_US(10);

	stimSPI_UnverifiedPut(SAT_VG_CTRL, HVDDL_EN);

	DELAY_US(10);

	stimSPI_UnverifiedPut(SAT_VG_CTRL, HVDDL_EN | CAS_EN);

	DELAY_MS(1);

	globalRegisterValid();

	// turn on the interrupts
	RegisterClearFlags();

	stimConfigErrorIRQ();

	// disable the write to some of the registers
//	stimSPI_Put(SAT_WE4, 0x00);
//	stimSPI_Put(SAT_WE5, 0x00);

	// make sure that the g_nextPhaseBank is initialized
	g_nextPhaseBank = 0;

	P5OUT |= STIM_CLK;
	P5SEL |= STIM_CLK;

	clkClearOscillatorFault();
	clkOscillatorFaultInterruptEnable();

	// at this point we can set the HVDD value
	return(0);

EXIT_FAIL:
	clkClearOscillatorFault();
	clkOscillatorFaultInterruptEnable();

	logError(E_LOG_ERR_STIM_POWER_UP_FAILED, error);
	setResponseCode(RESP_STIM_POWER_ON_FAILED);

	DBGI(f_stim_startup, error);
	return(-1);
}


void stimPowerOff(void)
{
	// turn off interrupts and clear them
	P2IE &= ~STIM_ERR;
	P2IFG &= ~STIM_ERR;

	// PORT 2 OUTPUTS TO LOW
	P2OUT &= ~HVDD_CONN;

	// PORT 5 OUTPUTS TO LOW
	P5OUT &= ~STIM_RST;

	// disconnect the clock and set the ouput low
	P5OUT &= ~STIM_CLK;
	P5SEL &= ~STIM_CLK;

	// turn off SPI
	stimSPI_Suspend();

	// PORT 3 OUTPUTS TO LOW
	// set the other outputs low
	P3OUT &= ~(STIM_SCK | STIM_SDI);
	P3SEL &= ~(STIM_SDI | STIM_SDO | STIM_SCK);

#ifdef EPG
	P4OUT &= ~(STIM_RUN | STIM_STOP | STIM_CONT | STIM_SEL);
	P7OUT &= ~STIM_CS;
#elif defined IPG_REV05
	P4OUT &= ~(STIM_RUN | STIM_STOP | STIM_CONT | STIM_SEL | STIM_CS);
#else
	P4OUT &= ~(STIM_RUN | STIM_STOP | STIM_CONT | STIM_SEL);
	P7OUT &= ~STIM_CS;
#endif

	pwrSatDisableAVDD();
	pwrSatDisableDVDD();

	DELAY_MS(2);

	engagePulldowns();

	// make sure that the g_nextPhaseBank is initialized when we leave
	g_nextPhaseBank = 0;

}
