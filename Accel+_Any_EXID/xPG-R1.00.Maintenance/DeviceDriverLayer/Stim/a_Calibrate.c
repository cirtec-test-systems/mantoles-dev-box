#include "DeviceDriverLayer/Common/msp430f2618.h"
#include "DeviceDriverLayer/Common/bits.h"

#include "delay.h"

#include "a_Saturn.h"
#include "a_RegisterIO.h"
#include "stimspi.h"

int stimEnableCalibrator(void)
{
	if (sat_Set(SAT_ENABLE, CE) < 0)
	{
		return -1;
	}
	return 0;
}

int stimDisableCalibrator(void)
{
	int result = 0;

	if (sat_Clear(SAT_ENABLE, CE) < 0)
	{
		return -1;
	}

	return result;
}

int stimSetCalibratorSinkMode(bool sinkMode)
{
	int result;

	if (sinkMode)
	{
		result = sat_Set(SAT_CM_CTRL, CAL_STAT_SINK);
	}
	else
	{
		result = sat_Clear(SAT_CM_CTRL, CAL_STAT_SINK);
	}

	return result;
}

