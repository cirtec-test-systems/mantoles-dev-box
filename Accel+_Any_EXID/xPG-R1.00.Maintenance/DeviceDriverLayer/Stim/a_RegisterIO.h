#ifndef REGISTERIO_H_
#define REGISTERIO_H_

#include <stdbool.h>
#include <stdio.h>

int 	sat_Set(uint16_t addr, uint8_t mask);
int 	sat_Clear(uint16_t addr, uint8_t mask);

int 	sat_RevokeWE(void);
bool 	sat_IsAddressValid(uint16_t addr);
int 	sat_AnalogWE(void);
int 	sat_RevokeAnalogWE(void);
int 	sat_PowerWE(void);
int 	sat_RevokePowerWE(void);
int 	sat_EnableHVLDO(void);
int 	sat_DisableHVLDO(void);


int 	globalWriteEnable();
int 	globalWriteDisable();

int 	globalRegisterValid();
int 	globalRegisterInvalid();

void 	RegisterClearFlags();

#endif
