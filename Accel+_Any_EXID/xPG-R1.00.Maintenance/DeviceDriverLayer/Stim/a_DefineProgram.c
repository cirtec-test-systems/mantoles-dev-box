#include "DeviceDriverLayer/Common/msp430f2618.h"
#include "DeviceDriverLayer/Common/bits.h"
#include "lastresponse.h"
#include "delay.h"
#include "log.h"
#include "watchdog.h"

#include "Stim.h"
#include "a_Saturn.h"
#include "stimspi.h"
#include "a_RegisterIO.h"

#define BOOST_STEP_SIZE 10

extern uint16_t g_globalChannelScale;


static uint16_t phaseAddr(uint8_t phase)
{
	return SAT_PH1 + (SAT_PH2 - SAT_PH1) * phase;
}

static int sat_ConfigPulseGuard(uint8_t phase, uint8_t pg)
{
	return stimSPI_Put(phaseAddr(phase) + PHX_PG_TIMEOUT, pg);
}

static int sat_ClearAllAmplitudes(uint8_t phase)
{
	uint16_t addr = phaseAddr(phase) + PHX_AMP01;
	int i;

	// write zero into all 52 bytes that form the 26 amplitudes for the phase (each amplitude is two bytes)
	for(i=0; i<52; i++)
	{
		stimSPI_UnverifiedPut(addr++, 0);
	}
	return(0);
}

static int sat_ConfigAmplitude(uint8_t phase, int chan, uint16_t amp, bool source, bool uncontrolled, bool calibrator)
{
	uint16_t addr = phaseAddr(phase) + PHX_AMP01 + (chan * sizeof(uint16_t));
	int status;
	uint32_t bigamp;

	// apply the global scale factor that is part of the trim list
	bigamp = amp;
	bigamp *= (uint32_t)g_globalChannelScale;
	bigamp /= (uint32_t)1000;
	amp = (uint16_t)bigamp;

	if (uncontrolled)
	{
		amp |= AMP_USPNE;
	}
	else if (amp == 0)
	{
		amp = 0;					// Not uncontrolled and no amplitude, ergo don't enable it.
	}
	else if (source)
	{
		amp |= AMP_CHE;				// Channel enabled and sourcing current
	}
	else
	{
		amp |= AMP_CHE | AMP_SSE;	// Channel enabled and sinking current
	}

	if (calibrator)
	{
		amp |= AMP_CALE;
	}

	if((status = stimSPI_Write(addr, &amp, sizeof(amp))) != 0)
	{
		DBG(f_stim_defineprogram,DBG_SEV_ERROR, addr, 0);
	}

	return(status);
}

static int sat_ConfigPhase(uint8_t phase, unsigned wav, bool uSource, bool hvOff, bool chargeBalance, uint32_t stp, uint16_t dly, uint8_t repetition, bool pause, bool sync)
{
	uint16_t addr;
	PHX_CONTROL ctl = { 0, 0, 0, 0, 0, 0, 0, 0 };
	int status;

	ctl.ctl1 = wav;
	if (uSource)
	{
		ctl.ctl1 |= USPN;
	}
	if (hvOff)
	{
		ctl.ctl1 |= HVOFF;
	}
	if (chargeBalance)
	{
		ctl.ctl1 |= DPFCBS_EN | DPFE;
	}
	if(pause)
	{
		ctl.ctl2 |= PAUSE;
	}
	if(sync)
	{
		ctl.ctl2 |= SYNC;
	}

	ctl.ctl2 |= repetition;  // the repetition value is stored in the lowest 4 bits of the register so we can just "or" the value

	ctl.stpa = (stp & 0x0000ff);
	ctl.stpb = (stp & 0x00ff00) >> 8;
	ctl.stpc = (stp & 0xff0000) >> 16;
	ctl.dly = dly;
	ctl.cdv = CS;

	addr = phaseAddr(phase);
	if((status = stimSPI_Write(addr, &ctl, sizeof(ctl))) != 0)
	{
		DBG(f_stim_defineprogram,DBG_SEV_ERROR, addr, 0);
	}
	return(status);
}

// -----------------------------------------------------------------------------------------------------
// Driver internal API's
bool stimIsValueValid(uint16_t value)
{
	if (value == 0 && value <= 0xFF)
	{
		return false;
	}

	return true;
}

static int waitForVoltage()
{
	uint8_t bc_on = 0;
	int i;

	// bring up the high voltage to the correct compliance level
	i=0;
	do
	{
		bc_on = stimSPI_Get(SAT_BC_CTRL1) & BCG;
		if(bc_on == 0)
		{
			wdServiceWatchdog();
			DELAY_MS(1);
		}
		i++;
	}while(i < 200 && bc_on == 0);

	if(bc_on == 0)
	{
		setResponseCode(RESP_BOOST_CONVERTER_STARTUP_FAILED);

		logError(E_LOG_ERR_STIM_HV_BOOST_INIT_FAILED, 0);
		DBG(f_stim_defineprogram, DBG_SEV_ERROR, 0, 0);
		return(-1);
	}
	return(0);
}
// -----------------------------------------------------------------------------------------------------
// APP level API's
int stimConfigSequencerRegisters(uint8_t sel, uint8_t startPhase, uint8_t numPhase)
{
	uint8_t endPhase;

	endPhase = startPhase + numPhase - 1;
	if (endPhase >= STIM_PHASES)
	{
		endPhase -= STIM_PHASES;
	}

	if (sel == 0)
	{
		stimSPI_Put(SAT_SEQ0A, startPhase);
		stimSPI_Put(SAT_SEQ0B, endPhase);
	}
	else
	{
		stimSPI_Put(SAT_SEQ1A, startPhase);
		stimSPI_Put(SAT_SEQ1B, endPhase);
	}

	return(0);
}

int stimSetSourceSink(uint8_t phase, uint8_t srcChan, uint16_t srcAmp, uint8_t sinkChan, uint16_t sinkAmp)
{
	uint16_t 	addr;
	uint16_t	val;
	int			status;

	// set the amplitude value and control bits for source
	addr = phaseAddr(phase) + PHX_AMP01 + (srcChan * sizeof(uint16_t));
	val = srcAmp | AMP_CHE | AMP_SSE;
	if((status = stimSPI_UnverifiedWrite(addr, &val, sizeof(val))) != 0)
	{
		DBG(f_stim_defineprogram,DBG_SEV_ERROR, addr, 0);
	}

	// set the amplitude value and control bits for sink
	addr = phaseAddr(phase) + PHX_AMP01 + (sinkChan * sizeof(uint16_t));
	val = sinkAmp | AMP_CHE;
	if((status = stimSPI_UnverifiedWrite(addr, &val, sizeof(val))) != 0)
	{
		DBG(f_stim_defineprogram,DBG_SEV_ERROR, addr, 0);
	}

	return(status);
}

int stimConfigPhaseFast(uint8_t phase, PHASE const *phx, int chan1, int chan2)
{
	uint32_t stp, samples, delay;

	// Calculate the step size.
	if(phx->pulseWidth >= SMCLK_SCALE_FACTOR)
	{
		samples = phx->pulseWidth/SMCLK_SCALE_FACTOR;
	}
	else
	{
		// the asic does not like any phase pulse width to be zero so set to some non zero value
		samples = 1;
	}

	// the goal is to have the waveform counter increment by stp such that it rolls over after phx->pulseWidth micro-seconds.  Each bit in the stp register represents SMCLK_SCALE_FACTOR micro-seconds
	stp = (0x1000000 + samples - 1) / samples;

	// now calculate the delay value to load into the register
	delay = 25;

	// Set the control registers - note that delay is always one more than the value being set (a delay of zero results in a delay of one clock)
	if (sat_ConfigPhase(phase, phx->wav, phx->uSource, phx->hvOff, phx->chargeBalance, stp, (uint16_t)(delay - 1), phx->repetition, phx->pause, phx->sync) < 0)
	{
		goto fail;
	}

	sat_ClearAllAmplitudes(phase);

	if (sat_ConfigAmplitude(phase, chan1, phx->chan[chan1].amp, phx->chan[chan1].source, phx->chan[chan1].uncontrolled, phx->calibrate) < 0)
	{
		goto fail;
	}

	if (sat_ConfigAmplitude(phase, chan2, phx->chan[chan2].amp, phx->chan[chan2].source, phx->chan[chan2].uncontrolled, phx->calibrate) < 0)
	{
		goto fail;
	}

	// disable the puse guard
	if (sat_ConfigPulseGuard(phase, 0) < 0)
	{
		goto fail;
	}
	return 0;

fail:
	return -1;
}

int stimConfigPhase(uint8_t phase, PHASE const *phx, bool setPG)
{
	uint32_t stp, samples, delay;
	uint16_t pg;
	int i;

#if 1
	// Calculate the step size.
	if(phx->pulseWidth >= SMCLK_SCALE_FACTOR)
	{
		samples = phx->pulseWidth/SMCLK_SCALE_FACTOR;
	}
	else
	{
		// the asic does not like any phase pulse width to be zero so set to some non zero value
		samples = 2;
	}
#else
	samples = phx->pulseWidth/SMCLK_SCALE_FACTOR;
#endif

	// the goal is to have the waveform counter increment by stp such that it rolls over after phx->pulseWidth micro-seconds.  Each bit in the stp register represents SMCLK_SCALE_FACTOR micro-seconds
	stp = (0x1000000 + samples - 1) / samples;

	// now calculate the delay value to load into the register
	delay = phx->delay/SMCLK_SCALE_FACTOR;

	// Set the control registers - note that delay is always one more than the value being set (a delay of zero results in a delay of one clock)
	if (sat_ConfigPhase(phase, phx->wav, phx->uSource, phx->hvOff, phx->chargeBalance, stp, (uint16_t)(delay - 1), phx->repetition, phx->pause, phx->sync) < 0)
	{
		goto fail;
	}

	// Iterate through the channels and set the amplitude registers for each one
	for (i = 0; i < STIM_CHANNELS; i++)
	{
		if (sat_ConfigAmplitude(phase, i, phx->chan[i].amp, phx->chan[i].source, phx->chan[i].uncontrolled, phx->calibrate) < 0)
		{
			goto fail;
		}
	}

	if(setPG)
	{
		pg = phx->pulseWidth + 250;  // per requirement 617
		pg /= PG_STEP;

		// Set the pulse guard
#ifndef DISABLE_PULSE_GUARD
		if (sat_ConfigPulseGuard(phase, pg) < 0)
		{
			goto fail;
		}
#else
		if (sat_ConfigPulseGuard(phase, 0) < 0)
		{
			goto fail;
		}
#endif
	}
	else
	{
		// disable the puse guard
		if (sat_ConfigPulseGuard(phase, 0) < 0)
		{
			goto fail;
		}
	}

	return 0;

fail:
	return -1;
}

int stimSetChannelAmplitude(uint8_t phase, uint8_t channel, uint16_t amplitude, bool source)
{
	if (sat_ConfigAmplitude(phase, channel, amplitude, source, false, false) < 0)
	{
		return(-1);
	}
	return(0);
}

 int stimSetRectLevel(unsigned sel, uint8_t rampLevel)
{
	int result;
	result = stimSPI_Put(SAT_RECT1 + sel, rampLevel);
	return(result);
}

int stimSetComplianceDAC(uint8_t level, bool wait)
{
	int 	status = 0;
	uint8_t currentLevel;
	uint8_t 	i;

	uint8_t		steps, balance, nextLevel, prevLevel;

	currentLevel = stimSPI_Get(SAT_BC_CTRL0);

	if(level > currentLevel)
	{
		steps   = (level-currentLevel) / BOOST_STEP_SIZE;
		balance = (level-currentLevel) % BOOST_STEP_SIZE;

		nextLevel = currentLevel;
		prevLevel = currentLevel;
		for(i=0; i<steps; i++)
		{
			nextLevel += BOOST_STEP_SIZE;
			stimSPI_UnverifiedPut(SAT_BC_CTRL0, nextLevel);
//			DELAY_MS(1);
//			stimSPI_UnverifiedPut(SAT_BC_CTRL0, nextLevel);

			if(wait && nextLevel > prevLevel)
			{
				if(waitForVoltage() != 0)
				{
					DBG(f_stim_defineprogram,prevLevel,nextLevel,0);
					//return(-1);
				}
			}
			prevLevel = nextLevel;
		}

		if(balance != 0)
		{
			nextLevel += balance;
			stimSPI_UnverifiedPut(SAT_BC_CTRL0, nextLevel);
//			DELAY_MS(1);
//			stimSPI_UnverifiedPut(SAT_BC_CTRL0, nextLevel);

			if(wait)
			{
				if(waitForVoltage() != 0)
				{
					DBG(f_stim_defineprogram,prevLevel,nextLevel,0);
					//return(-1);
				}
			}
		}

#if 0
		for(i= currentLevel+1; i<=level; i++)
		{
			stimSPI_UnverifiedPut(SAT_BC_CTRL0, i);
		}

		if(wait)
		{
			if(waitForVoltage() != 0)
			{
				return(-1);
			}
		}
#endif
	}
	// if we're stepping down the voltage then it's real simple - just set the value into the register
	else
	{
		stimSPI_UnverifiedPut(SAT_BC_CTRL0, level);
	}

	return status;
}

uint8_t stimGetComplianceDAC(void)
{
	return stimSPI_Get(SAT_BC_CTRL0);
}

int stimPauseBitSet(uint8_t phase)
{
	uint16_t addr = phaseAddr(phase) + PHX_CTL2;
	sat_Set(addr, PAUSE);
	return(0);
}

int stimPauseBitClear(uint8_t phase)
{
	uint16_t addr = phaseAddr(phase) + PHX_CTL2;
	sat_Clear(addr, PAUSE);
	return(0);
}

void stimSetPulseGuard(uint8_t phase, uint8_t pg)
{
	sat_ConfigPulseGuard(phase, pg);
}







