#include "DeviceDriverLayer/Common/msp430f2618.h"
#include "DeviceDriverLayer/Common/bits.h"
#include "EventQueue.h"
#include "timeout.h"
#include "Interrupt.h"

#include "Stim.h"
#include "a_Saturn.h"
#include "stimspi.h"
#include "a_RegisterIO.h"
#include "a_Interrupt.h"
#include "log.h"
#include "Watchdog.h"

#include <intrinsics.h>

extern uint8_t	g_nextPhaseBank;
extern uint8_t  g_wfd_ctrl_val;

#ifdef UNIT_TESTING
extern volatile uint64_t TimerBTicks;
#endif

// variables set by application to ramp up and down
uint8_t		volatile g_stimISRFunc = 0;				// 1 - ramp up, 2 - ramp down 0 - no ramping

// variable used for synchronization between application and ISR for ramping and parameter changes
uint8_t 	volatile g_stimISRSync 	= 0;			// spin lock
uint8_t		volatile g_stimISRError  = 0;

// ----------------------------------------------------------------------------------------------

// ----------------------------------------------------------------------------------------------
// SYNC PULSE VIA TIMER B ISR
// The SYNC pulse comes in on Port 4.4 and is routed through Timer B Channel 4
// Note that Port 4 can not interrupt directly like ports 1 and 2 so you have to configure the timer in capture mode to get the interrupt
static inline void handleStimSyncInt(void)
{
	if(TBIV == TBIV_TBCCR4)
	{
		// -----------------------------------------------------------------------------------
		// STIM PROGRAM UPDATE < sync with stimUpdate() >
		if(g_stimISRFunc == 0)
			// we are making an update to the stim program so swap phase banks
		{
#if 0
			// this is all now handled at the application level in the stimUpdate function
			// toggle the select line
			if(g_nextPhaseBank == 0)
			{
				P4OUT &= ~STIM_SEL;
				g_nextPhaseBank = 1;
			}
			else
			{
				P4OUT |= STIM_SEL;
				g_nextPhaseBank = 0;
			}

			// clear and turn off interrupts
			TBCCTL4 &= ~CCIE;
			TBCCTL4 &= ~CCIFG;
#endif
			// set the synchronization variable so that the app layer can continue
			// we synchronize inside stimUpdate() function
			g_stimISRSync = 1;
		}
		// -----------------------------------------------------------------------------------
		// STIMULATION PERIOD / FREQUENCY MEASUREMENT < sync with stimVerifyPeriod() >
		else if(g_stimISRFunc == 3)
			// we are performing the FIRST measurement of a period measurement
		{
			g_stimISRSync = 1;		// sync with the application

			// clear the interrupt flag
			TBCCTL4 &= ~CCIFG;
		}
		else if(g_stimISRFunc == 4)
			// we are performing the SECOND measurement of a period measurement - we are done
		{
			g_stimISRSync = 1;		// sync with the application

			// clear and turn off interrupts for the timer channel and the roll-over
			TBCCTL4 &= ~CCIE;
			TBCCTL4 &= ~CCIFG;

#ifdef ENABLE_ROLL_OVER
			// clear and turn off the roll-over interrupt
			TBCTL &= ~TBIE;
			TBCTL &= ~TBIFG;
#endif

		}
	}
#ifdef ENABLE_ROLL_OVER
	// -----------------------------------------------------------------------------------
	// TRACK TIMER B ROLL OVERS
	else if(TBIV == TBIV_TBIFG)
		// we got a timer B counter roll-over
	{
		g_rollCounter++;

		TBCTL &= ~TBIFG;		// clear the interrupt

		what = BACK_TO_SLEEP;
	}
#endif

	return;
}

#pragma vector = TIMERB1_VECTOR
interrupt void stimHandleTimerInt(void)
{
	interruptHandlerPrologue();

#ifdef UNIT_TESTING
	if(TBIV == TBIV_TBIFG){
		TimerBTicks += 0x10000;
	}
#endif
	handleStimSyncInt();

	_low_power_mode_off_on_exit();
}

// STIM_ERR INTERRUPT
// ----------------------------------------------------------------------------------------------------
void stimConfigErrorIRQ(void)
{
	P2IE &= ~STIM_ERR;
	P2IES |= STIM_ERR;
	P2IFG &= ~STIM_ERR;
	if((P2IN & STIM_ERR) == 0)
	{
		P2IFG |= STIM_ERR;
	}
	P2IE |= STIM_ERR;
}

// STIM_ERR IRQ : STIM_ERR comes in on Port 2.7
// called from ISR contained in Interrupt/isr.c for port 2 interrupts
void stimHandleInterrupt(void)
{
	uint8_t flag;

	/*
	 * Multiple peripherals share this single interrupt, so make sure it is
	 * ours before acting.
	 */
	// stim asic asserts the signal low - so high means no interrupt event
	if ((P2IN & STIM_ERR) == 0)
	{
		// error interrupt
		flag = stimSPI_Get(SAT_FLG);

		if((flag & PGF) || (flag & PARF) || (flag & SEQF) || (flag & RVF))
			// we had an error that stopped stimulation
		{
			// sync with the application so we don't get locked in a spin loop
			g_stimISRError = 1;

			// reset the stim ISR flags
			g_stimISRFunc = 0;
		}

		dispPutSimpleEvent(E_STIM_ERR_IRQ);

		// clear the interrupt flag
		P2IFG &= ~STIM_ERR;
	}
}

void stimProcessEvent(EVENT event)
{
	static const struct {
		uint8_t flag;
		EVENT_ID event;
	} table[] = {
		{ PGF, E_STIM_PULSE_GUARD },
		{ PARF, E_STIM_PARITY },
		{ SEQF, E_STIM_SEQUENCER },
		{ ADRF, E_STIM_ADDRESS },
		{ RVF, E_STIM_REGISTER_VALID },
		{ WPF, E_STIM_WRITE_PROTECT }
	};

	uint8_t flags;
	int i;


DISPATCH_GEN_START(stimProcessEvent)
	if (event.eventID != E_STIM_ERR_IRQ)
	{
		return;
	}
DISPATCH_GEN_END

	/*
	 * As long as there are flags set, iterate through them and queue
	 * the corresponding events.
	 */
	if((flags = stimSPI_Get(SAT_FLG) & ~TFLG) != 0)
	{
		// Stop stimulation and clear possible error sources.

		DBG(f_stim_interrupt, DBG_SEV_NORM, flags,0);

		//cleanUpFaultState();

		/*
		 * For each flag in the flags table:
		 *   if the flag is set and not masked, dispatch the corresponding event
		 *   if the flag is set and masked, just increment the masked-flags counter
		 */
		for (i = 0; i < sizeof(table)/sizeof(table[0]); i++)
		{
			if ((flags & table[i].flag) != 0)
			{
				(void) dispPutSimpleEvent(table[i].event);
			}
		}

		stimSPI_UnverifiedPut(SAT_FLG, flags);
	}
}



