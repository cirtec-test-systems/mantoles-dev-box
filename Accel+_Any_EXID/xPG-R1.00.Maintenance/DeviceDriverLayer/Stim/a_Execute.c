#include "DeviceDriverLayer/Common/msp430f2618.h"
#include "DeviceDriverLayer/Common/bits.h"
#include "delay.h"
#include "timeout.h"
#include "Watchdog.h"
#include "log.h"

#include "Stim.h"
#include "a_Saturn.h"
#include "stimspi.h"
#include "a_RegisterIO.h"
#include "timer.h"
#include "Common/Protocol/response_code.h"
#include "lastresponse.h"
#include "system.h"

uint8_t	g_nextPhaseBank = 0;

// ---------------------  ISR related ------------------------------------------
// variables set by application to ramp up and down - set from the generic program layer
extern uint8_t	volatile g_stimISRFunc;

// variable used for synchronization between application and ISR for ramping and parameter changes
extern uint8_t 	volatile g_stimISRSync;
extern uint8_t 	volatile g_stimISRError;

// ---------------------- STIM ASIC state related -----------------------------------
uint8_t g_wfd_ctrl_val;

static void timerBInit()
{
	// run timer B at 125 KHz regardless of the SMCLK rate

#ifdef RUN_AT_8_MHZ
	// in this case the DCO is running at 8 MHz and the SMCLK is running at 1 MHz (see IOInitialization.c)
	TBCTL = TBCLGRP_0	// Each latch loads independently
		  | CNTL_0		// 16-bit counter
		  | TBSSEL_2	// SMCLK
		  | ID_3		// divided by 8 (SMCLK/8 = 125 kHz)
		  | MC_0		// Keep it turned off
		  | TBCLR;		// Reset the counters
#else
#ifdef RUN_SMCLK_AT_1_MHZ
	// in this case the DCO is running at 1 MHz and the SMCLK is running at 1 MHz
	TBCTL = TBCLGRP_0	// Each latch loads independently
		  | CNTL_0		// 16-bit counter
		  | TBSSEL_2	// SMCLK
		  | ID_3		// divided by 8 (SMCLK/8 = 125 kHz)
		  | MC_0		// Keep it turned off
		  | TBCLR;		// Reset the counters
#else
#ifdef RUN_SMCLK_AT_125_KHZ
	// in this case the DCO is running at 1 MHz and the SMCLK is running at 125 KHz
	TBCTL = TBCLGRP_0	// Each latch loads independently
		  | CNTL_0		// 16-bit counter
		  | TBSSEL_2	// SMCLK
		  | ID_0		// divided by 1 (SMCLK/1 = 125 kHz)
		  | MC_0		// Keep it turned off
		  | TBCLR;		// Reset the counters
#else
	// in this case the DCO is running at 1 MHz and the SMCLK is running at 250 KHz
	TBCTL = TBCLGRP_0	// Each latch loads independently
		  | CNTL_0		// 16-bit counter
		  | TBSSEL_2	// SMCLK
		  | ID_1		// divided by 2 (SMCLK/2 = 125 kHz)
		  | MC_0		// Keep it turned off
		  | TBCLR;		// Reset the counters
#endif
#endif
#endif

	// Timer_B4: SYNC signal
	TBCCTL4 = CM_1 	// Capture on rising edge
		   | CCIS_0	// Capture from CCI6A
		   | SCS	// Synchronous capture
		   | CAP;	// Capture mode


	P4SEL |= STIM_SYNC;
}

// ---------------------------------------------------------------------------------------------------
// PUBLIC API's
int stimUpdateRectLevel(unsigned sel, uint8_t rectLevel)
{
	uint8_t 	port1, port2;
	int16_t		timeout;
	bool		timeoutFlag = false;
	int 		status = 0;

#if 0
	stimSetRectLevel(sel, rectLevel);
	return(0);
#else

	if(P4IN & STIM_PROG == 0)
	{
		// a stim program is not running
		return(-1);
	}

	DBGI(f_stim_execute, (uint32_t)rectLevel);

	g_stimISRSync 	= 0;
	g_stimISRError 	= 0;
	g_stimISRFunc 	= 0;

	// turn off interrupts other than stim and remember the settings for later
	port1 = P1IE;
	port2 = P2IE;

	P1IE = 0x00;
	P2IE = STIM_ERR;

	// initialize the timer specifically for what we need in this function
	timerBInit();

	// --------------------------------------------------------------------------
	// get ready to sync this function with the stim ISR
	wdServiceWatchdog();

	// turn on the timer in continuous mode (bits 10). An interrupt will occur on rising edge of SYNC ... here we go
	TBCTL |= MC_2;
	
	// clear and enable timer B channel 4 interrupt (which is tied to SYNC)
	TBCCTL4 &= ~CCIFG;
	TBCCTL4 |= CCIE;


	timeout = timeoutSet(SYNC_RETRY_TIMEOUT_TICKS);

	while(g_stimISRSync == 0 && timeoutFlag == false && g_stimISRError == 0)
	{
		timeoutFlag = timeoutIsExpired(timeout);
	}

	wdServiceWatchdog();

	if(g_stimISRSync == 0)
		// failed to receive the SYNC interrupt - should never happen
	{
		if(timeoutFlag)
		{
			// the SYNC interrupt never occured so return an error
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}

		if(g_stimISRError != 0)
		{
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}

		if((P4IN & STIM_PROG) == 0)
		{
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}
		setResponseCode(RESP_STIM_FAILED_SEVERE_ERROR);
		status = -1;
		goto CLEANUP;
	}

	// got the SYNC interrupt
	// wait until the SYNC phase is over
	DELAY_US(50);

	// turn off continuous mode
	P4OUT &= ~STIM_CONT;

	// wait for the next SYNC
	g_stimISRSync 	= 0;

	timeout = timeoutSet(SYNC_RETRY_TIMEOUT_TICKS);

	while(g_stimISRSync == 0 && timeoutFlag == false && g_stimISRError == 0)
	{
		timeoutFlag = timeoutIsExpired(timeout);
	}

	wdServiceWatchdog();

	if(g_stimISRSync == 0)
		// failed to receive the SYNC interrupt - should never happen
	{
		if(timeoutFlag)
		{
			// the SYNC interrupt never occured so return an error
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}

		if(g_stimISRError != 0)
		{
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}

		if((P4IN & STIM_PROG) == 0)
		{
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}
		setResponseCode(RESP_STIM_FAILED_SEVERE_ERROR);
		status = -1;
		goto CLEANUP;
	}

	// make sure the period is over and stim asic is idle
	stimWaitForStop();

	if(stimSetRectLevel(sel, rectLevel) != 0)
	{
		logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
		DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		setResponseCode(RESP_STIM_FAILED_SEVERE_ERROR);
		status = -1;
		goto CLEANUP;
	}

	// re-enable continuous mode
	P4OUT |= STIM_CONT;

	P4OUT |= STIM_RUN;
	DELAY_US(4);
	P4OUT &= ~STIM_RUN;

CLEANUP:

	// clear and turn off interrupts
	TBCCTL4 &= ~CCIE;
	TBCCTL4 &= ~CCIFG;

	// turn off the timer (bits 00) to save power and disconnect the signal from the timer
	TBCTL &= ~(MC0 | MC1);
	P4SEL &= ~STIM_SYNC;

	// turn back on the other interrupts from the saved settings
	P1IE = port1;
	P2IE = port2;

	return(status);
#endif

#if 0
	// "delay" microseconds from the start of a program period
	// this will put us in the middle of a stimulation phase so we can take a calibration measurement using ADC
	g_stimISRSync 	= 0;
	g_stimISRError 	= 0;
	g_stimISRFunc = 3;

	// turn off all other interrupts and remember the settings for later
	port1 = P1IE;
	port2 = P2IE;

	P1IE = 0x00;
	P2IE = STIM_ERR;

	// initialize the timer specifically for what we need in this function
	timerBInit();

	// clear and enable timer B channel 4 interrupt (which is tied to SYNC)
	TBCCTL4 &= ~CCIFG;
	TBCCTL4 |= CCIE;

	// --------------------------------------------------------------------------
	// get ready to sync this function with the stim ISR
	wdServiceWatchdog();

	// turn on the timer in continuous mode (bits 10). An interrupt will occur on rising edge of SYNC ... here we go
	TBCTL |= MC_2;

	timeout = timeoutSet(SYNC_RETRY_TIMEOUT_TICKS);

	while(g_stimISRSync == 0 && timeoutFlag == false && g_stimISRError == 0)
	{
		timeoutFlag = timeoutIsExpired(timeout);
	}

	// process the time sensitive command first then check for errors
	if(g_stimISRSync != 0)
	{
		// looks like we SYNC'd so go ahead and update the rectangle register
		if(stimSetRectLevel(sel, rectLevel) != 0)
		{
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
			setResponseCode(RESP_STIM_FAILED_SEVERE_ERROR);
			status = -1;
		}
	}
	else
	{
		if(timeoutFlag)
		{
			// the SYNC interrupt never occured so return an error
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}

		if(g_stimISRError != 0)
		{
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}

		if((P4IN & STIM_PROG) == 0)
		{
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}
		setResponseCode(RESP_STIM_FAILED_SEVERE_ERROR);
		status = -1;
	}

	wdServiceWatchdog();

	// turn off the timer (bits 00) to save power and disconnect the signal from the timer
	TBCTL &= ~(MC0 | MC1);
	P4SEL &= ~STIM_SYNC;

	// turn back on the other interrupts from the saved settings
	P1IE = port1;
	P2IE = port2;

	return(status);

#endif
}

int stimVerifyPeriod(uint32_t *period)
{
	uint8_t 	port1, port2;
	uint32_t	time_1 = 0, time_2 = 0;
	uint32_t	timeDelta;
	int16_t		timeout;
	bool		timeoutFlag = false;
	int			status = 0;

	*period = 0;

	if(P4IN & STIM_PROG == 0)
	{
		// a stim program is not running
		return(-1);
	}

	g_stimISRSync 	= 0;
	g_stimISRError 	= 0;
	g_stimISRFunc = 3;

	// turn off all other interrupts and remember the settings for later
	port1 = P1IE;
	port2 = P2IE;

	P1IE = 0x00;
	P2IE = STIM_ERR;

	// initialize the timer specifically for what we need in this function
	timerBInit();
	
#ifdef ENABLE_ROLL_OVER
	// clear and enable the roll-over interrupt
	TBCTL &= ~TBIFG;
	TBCTL |= TBIE;
#endif

	// turn on the timer in continuous mode (bits 10). An interrupt will occur on rising edge of SYNC ... here we go
	TBCTL |= MC_2;
	
	// clear and enable timer B channel 4 interrupt (which is tied to SYNC)
	TBCCTL4 &= ~CCIFG;
	TBCCTL4 |= CCIE;

	// --------------------------------------------------------------------------
	// get ready to sync this function with the stim ISR
	wdServiceWatchdog();

	timeout = timeoutSet(SYNC_RETRY_TIMEOUT_TICKS);

	while(g_stimISRSync == 0 && timeoutFlag == false && g_stimISRError == 0)
	{
		timeoutFlag = timeoutIsExpired(timeout);
	}

	// process the time sensitive command first then check for errors
	if(g_stimISRSync != 0)
	{
		// looks like we SYNC'd so capture the first timestamp...
		time_1 = TBCCR4;
		g_stimISRSync 	= 0;
		g_stimISRFunc = 4;
	}
	else
	{
		if(timeoutFlag)
		{
			// the SYNC interrupt never occured so return an error
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}

		if(g_stimISRError != 0)
		{
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}

		if((P4IN & STIM_PROG) == 0)
		{
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}
		setResponseCode(RESP_STIM_FAILED_SEVERE_ERROR);
		status = -1;
		goto DONE;
	}

	wdServiceWatchdog();

	timeoutFlag = false;  // just to make sure
	timeout = timeoutSet(SYNC_RETRY_TIMEOUT_TICKS);

	while(g_stimISRSync == 0 && timeoutFlag == false && g_stimISRError == 0)
	{
		timeoutFlag = timeoutIsExpired(timeout);
	}

	// process the time sensitive command first then check for errors
	if(g_stimISRSync != 0)
	{
		// looks like we SYNC'd so capture the second timestamp
		time_2 = TBCCR4;
		g_stimISRSync 	= 0;
		g_stimISRFunc = 0;
	}
	else
	{
		if(timeoutFlag)
		{
			// the SYNC interrupt never occured so return an error
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}

		if(g_stimISRError != 0)
		{
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}

		if((P4IN & STIM_PROG) == 0)
		{
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}
		setResponseCode(RESP_STIM_FAILED_SEVERE_ERROR);
		status = -1;
		goto DONE;
	}

	wdServiceWatchdog();
	// --------------------------------------------------------------------------

	// note that the longest possible period is 1/2Hz = .5sec and the timer has a span of 65536/125KHz = .524 seconds
	// therefore we can handle the longest possible period
#ifdef ENABLE_ROLL_OVER
	if(g_rollCounter == 0)
	{
		timeDelta = time_2- time_1;
	}
	else if(g_rollCounter == 1)
	{
		timeDelta = time_2 + (0xFFFF - time_1);

	}
	else
	{
		timeDelta = time_2 + (0xFFFF - time_1) + (g_rollCounter - 1)*0xFFFF;
	}
#else
	if(time_2 > time_1)
	{
		timeDelta = time_2- time_1;
	}
	else
	{
		timeDelta = time_2 + (0xFFFF - time_1);
	}
#endif

	// period in microseconds = (time delta / counter frequency) *1000000 = (time delta/125000)*1000000 = time delta * 1000 / 125;

	*period = (timeDelta * 1000/125);

DONE:
	// turn off the timer (bits 00) to save power and disconnect the signal from the timer
	TBCTL &= ~(MC0 | MC1);
	P4SEL &= ~STIM_SYNC;

	// turn back on the other interrupts from the saved settings
	P1IE = port1;
	P2IE = port2;

	return(status);
}

void stimStrobe(void)
{
	P4OUT |= STIM_RUN;
	DELAY_US(10);
	P4OUT &= ~STIM_RUN;
}

// the stimUpdate function will toggle over to the g_nextPhaseBank inside the ISR (Interrupt.c)
// the ISR will update the g_nextPhaseBank value after the switch is completed
int stimUpdate(CHANNEL_MAX_AMP_INDEX wf_ref)
{
	uint8_t 	port1, port2;
	int16_t		timeout;
	bool		timeoutFlag = false;
	int 		status = 0;
	uint8_t		wfd_ctrl_val;

	DBG(f_stim_execute, DBG_SEV_NORM, 0, 0);

	if(P4IN & STIM_PROG == 0)
	{
		// a stim program is not running
		return(-1);
	}

	g_stimISRSync 	= 0;
	g_stimISRError 	= 0;
	g_stimISRFunc 	= 0;

	// turn off interrupts other than stim and remember the settings for later
	port1 = P1IE;
	port2 = P2IE;

	P1IE = 0x00;
	P2IE = STIM_ERR;

	// initialize the timer specifically for what we need in this function
	timerBInit();

	// turn on the timer in continuous mode (bits 10). An interrupt will occur on rising edge of SYNC ... here we go
	TBCTL |= MC_2;

	// clear and enable timer B channel 4 interrupt (which is tied to SYNC)
	TBCCTL4 &= ~CCIFG;
	TBCCTL4 |= CCIE;

	// --------------------------------------------------------------------------
	// get ready to sync this function with the stim ISR
	wdServiceWatchdog();

	timeout = timeoutSet(SYNC_RETRY_TIMEOUT_TICKS);

	while(g_stimISRSync == 0 && timeoutFlag == false && g_stimISRError == 0)
	{
		timeoutFlag = timeoutIsExpired(timeout);
	}

	wdServiceWatchdog();

	if(g_stimISRSync == 0)
		// failed to receive the SYNC interrupt - should never happen
	{
		if(timeoutFlag)
		{
			// the SYNC interrupt never occured so return an error
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}

		if(g_stimISRError != 0)
		{
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}

		if((P4IN & STIM_PROG) == 0)
		{
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}
		setResponseCode(RESP_STIM_FAILED_SEVERE_ERROR);
		status = -1;
		goto CLEANUP;
	}

	// got the SYNC interrupt
	// wait until the SYNC phase is over
	DELAY_US(50);

	// turn off continuous mode
	P4OUT &= ~STIM_CONT;

	// wait for the next SYNC
	g_stimISRSync 	= 0;

	timeout = timeoutSet(SYNC_RETRY_TIMEOUT_TICKS);

	while(g_stimISRSync == 0 && timeoutFlag == false && g_stimISRError == 0)
	{
		timeoutFlag = timeoutIsExpired(timeout);
	}

	wdServiceWatchdog();

	if(g_stimISRSync == 0)
		// failed to receive the SYNC interrupt - should never happen
	{
		if(timeoutFlag)
		{
			// the SYNC interrupt never occured so return an error
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}

		if(g_stimISRError != 0)
		{
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}

		if((P4IN & STIM_PROG) == 0)
		{
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}
		setResponseCode(RESP_STIM_FAILED_SEVERE_ERROR);
		status = -1;
		goto CLEANUP;
	}

	// make sure the period is over and stim asic is idle
	stimWaitForStop();

	// determine the correct waveform generator value
	switch(wf_ref)
	{
	case CHANNEL_MAX_AMP_3:
		wfd_ctrl_val = WFD_CTRL_3_MAMP;
		break;
	case CHANNEL_MAX_AMP_6:
		wfd_ctrl_val = WFD_CTRL_6_MAMP;
		break;
	case CHANNEL_MAX_AMP_10:
		wfd_ctrl_val = WFD_CTRL_10_MAMP;
		break;
	case CHANNEL_MAX_AMP_15:
		wfd_ctrl_val = WFD_CTRL_15_MAMP;
		break;
	default:
		wfd_ctrl_val = WFD_CTRL_15_MAMP;
		break;
	}

	// set the updated waveform DAQ value
	if(stimSPI_Put(SAT_WFD_CTRL, WFD_CLK_EN | wfd_ctrl_val) != 0)
	{
		logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
		DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		status = -1;
		goto CLEANUP;
	}

	// update the stim select line
	if(g_nextPhaseBank == 0)
	{
		P4OUT &= ~STIM_SEL;
		g_nextPhaseBank = 1;
	}
	else
	{
		P4OUT |= STIM_SEL;
		g_nextPhaseBank = 0;
	}

	// re-enable continuous mode
	P4OUT |= STIM_CONT;

	P4OUT |= STIM_RUN;
	DELAY_US(4);
	P4OUT &= ~STIM_RUN;

CLEANUP:

	// clear and turn off interrupts
	TBCCTL4 &= ~CCIE;
	TBCCTL4 &= ~CCIFG;

	// turn off the timer (bits 00) to save power and disconnect the signal from the timer
	TBCTL &= ~(MC0 | MC1);
	P4SEL &= ~STIM_SYNC;

	// turn back on the other interrupts from the saved settings
	P1IE = port1;
	P2IE = port2;

	return(status);
}

void stimPrepareToStart(CHANNEL_MAX_AMP_INDEX wf_ref)
{
	uint8_t	wfd_ctrl_val;

	switch(wf_ref)
	{
	case CHANNEL_MAX_AMP_3:
		wfd_ctrl_val = WFD_CTRL_3_MAMP;
		break;
	case CHANNEL_MAX_AMP_6:
		wfd_ctrl_val = WFD_CTRL_6_MAMP;
		break;
	case CHANNEL_MAX_AMP_10:
		wfd_ctrl_val = WFD_CTRL_10_MAMP;
		break;
	case CHANNEL_MAX_AMP_15:
		wfd_ctrl_val = WFD_CTRL_15_MAMP;
		break;
	default:
		wfd_ctrl_val = WFD_CTRL_15_MAMP;
		break;
	}
	g_wfd_ctrl_val = wfd_ctrl_val;
	// bring up waveform generator

	stimSPI_Put(SAT_WFD_CTRL, WFD_CLK_EN | wfd_ctrl_val);
	sat_Set(SAT_ENABLE, WGE | OE);
	DELAY_MS(2);

	// turn on the clock to the stim asic
	// clock is now turned on during the power on process - should we keep it there?
	//P5SEL |= STIM_CLK;
	//DELAY_MS(2);

	// clear and enable error interrupts
	RegisterClearFlags();

	P2IES |= STIM_ERR;
}

void stimStartImpedanceMeasurement(unsigned sel)
{
	P4OUT &= ~STIM_CONT;

	if(sel == 0)
	{
		P4OUT &= ~STIM_SEL;
	}
	else
	{
		P4OUT |= STIM_SEL;
	}

	DELAY_US(2);

	// strobe the run line to start stim
	P4OUT |= STIM_RUN;
	DELAY_US(4);
	P4OUT &= ~STIM_RUN;

}


// the stimStart2 function will set the g_nextPhaseBank to the opposite value of sel
int stimStart2(unsigned sel, bool ramping, bool continuous, CHANNEL_MAX_AMP_INDEX wf_ref)
{
	(void)ramping;

	if(P4IN & STIM_PROG)
	{
		return(stimUpdate(wf_ref));
	}

	stimPrepareToStart(wf_ref);

	if(continuous)
	{
		// put asic in continuous mode with this input signal
		P4OUT |= STIM_CONT;
	}
	else
	{
		P4OUT &= ~STIM_CONT;
	}

	//DELAY_MS(2);

	// set the initial SEL signal
	if(sel == 0)
	{
		P4OUT &= ~STIM_SEL;
	}
	else
	{
		P4OUT |= STIM_SEL;
	}

	DELAY_US(2);

	// make sure the next available phase bank is the one we are NOT currently running
	g_nextPhaseBank = sel == 0 ? 1: 0;

#ifndef LEAKAGE_TEST
	// strobe the run line to start stim
	P4OUT |= STIM_RUN;
	DELAY_US(4);
	P4OUT &= ~STIM_RUN;
#endif

	return(0);
}

int stimStart(unsigned sel, bool ramping, bool continuous)
{
	return(stimStart2(sel, ramping, continuous, CHANNEL_MAX_AMP_15));
}

#if 1
void stimStop()
{
	int16_t		timeout;
	bool		timeoutFlag = false;

	// turn off the timer (bits 00) to save power and disconnect the signal from the timer
	TBCTL &= ~(MC0 | MC1);
	P4SEL &= ~STIM_SYNC;

	// disable timer B channel 4 interrupt and make sure the interrupt flag is clear
	TBCCTL4 &= ~CCIE;
	TBCCTL4 &= ~CCIFG;

	// turn off continuous mode
	P4OUT &= ~STIM_CONT;

#ifndef LEAKAGE_TEST
	timeout = timeoutSet(STIM_TIMEOUT_1_SECOND);

	// wait for it to stop before returning
	while ((P4IN & STIM_PROG) != 0 && timeoutFlag == false)
	{
		timeoutFlag = timeoutIsExpired(timeout);

	}

	if(timeoutFlag)
	{
		DBGI(f_stim_execute,0);
	}

	P4OUT |= STIM_STOP;
	DELAY_MS(5);
	P4OUT &= ~STIM_STOP;
#endif

	setTST_TRIG_LOW();

}
#else
void stimStop()
{
	uint8_t 	port1, port2;
	int16_t		timeout;
	bool		timeoutFlag = false;

	DBG(f_stim_execute, DBG_SEV_NORM, 0, 0);

	g_stimISRSync 	= 0;
	g_stimISRError 	= 0;
	g_stimISRFunc 	= 0;

	// turn off interrupts other than stim and remember the settings for later
	port1 = P1IE;
	port2 = P2IE;

	P1IE = 0x00;
	P2IE = STIM_ERR;

	if(P4IN & STIM_PROG == 0)
	{
		// a stim program is not running
		goto CLEANUP;
	}

	// initialize the timer specifically for what we need in this function
	timerBInit();

	// clear and enable timer B channel 4 interrupt (which is tied to SYNC)
	TBCCTL4 &= ~CCIFG;
	TBCCTL4 |= CCIE;

	// --------------------------------------------------------------------------
	// get ready to sync this function with the stim ISR
	wdServiceWatchdog();

	// turn on the timer in continuous mode (bits 10). An interrupt will occur on rising edge of SYNC ... here we go
	TBCTL |= MC_2;

	timeout = timeoutSet(SYNC_RETRY_TIMEOUT_TICKS);

	while(g_stimISRSync == 0 && timeoutFlag == false && g_stimISRError == 0)
	{
		timeoutFlag = timeoutIsExpired(timeout);
	}

	wdServiceWatchdog();

	if(g_stimISRSync == 0)
		// failed to receive the SYNC interrupt - should never happen
	{
		if(timeoutFlag)
		{
			// the SYNC interrupt never occured so return an error
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}

		if(g_stimISRError != 0)
		{
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}

		if((P4IN & STIM_PROG) == 0)
		{
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}
		setResponseCode(RESP_STIM_FAILED_SEVERE_ERROR);
		goto CLEANUP;
	}

	// got the SYNC interrupt
	// wait until the SYNC phase is over
	DELAY_US(50);

	// turn off continuous mode
	P4OUT &= ~STIM_CONT;

	// wait for the next SYNC
	g_stimISRSync 	= 0;

	timeout = timeoutSet(SYNC_RETRY_TIMEOUT_TICKS);

	while(g_stimISRSync == 0 && timeoutFlag == false && g_stimISRError == 0)
	{
		timeoutFlag = timeoutIsExpired(timeout);
	}

	wdServiceWatchdog();

	if(g_stimISRSync == 0)
		// failed to receive the SYNC interrupt - should never happen
	{
		if(timeoutFlag)
		{
			// the SYNC interrupt never occured so return an error
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}

		if(g_stimISRError != 0)
		{
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}

		if((P4IN & STIM_PROG) == 0)
		{
			logError(E_LOG_STIM_SYNC_ERROR, __LINE__);
			DBG(f_stim_execute, DBG_SEV_ERROR, 0, 0);
		}
		setResponseCode(RESP_STIM_FAILED_SEVERE_ERROR);
		goto CLEANUP;
	}

	// make sure the period is over and stim asic is idle
	stimWaitForStop();

CLEANUP:

	P4OUT |= STIM_STOP;
	DELAY_MS(5);
	P4OUT &= ~STIM_STOP;

	// clear and turn off interrupts
	TBCCTL4 &= ~CCIE;
	TBCCTL4 &= ~CCIFG;

	// turn off the timer (bits 00) to save power and disconnect the signal from the timer
	TBCTL &= ~(MC0 | MC1);
	P4SEL &= ~STIM_SYNC;

	// turn back on the other interrupts from the saved settings
	P1IE = port1;
	P2IE = port2;

	return;
}

#endif

void stimWaitForStop()
{
	while ((P4IN & STIM_PROG) != 0);
}

void stimPauseBitWait()
{
	while((P4IN & STIM_PAUSE) == 0);
}

void stimClearFlags()
{
	uint8_t flags;

	flags = stimSPI_Get(SAT_FLG);
	if(flags != 0x00)
	{
		stimSPI_UnverifiedPut(SAT_FLG, flags);
	}
}




