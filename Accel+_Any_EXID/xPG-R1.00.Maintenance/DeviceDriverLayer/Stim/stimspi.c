/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: stimSPI (Stimulation SPI) module
 *	
 ***************************************************************************/
#include "stdbool.h"

#include "stimspi.h"

#include "error_code.h"
#include "log_events.h"
#include "log.h"
#include "lastresponse.h"
#include "DeviceDriverLayer/Common/msp430f2618.h"
#include "DeviceDriverLayer/Common/bits.h"
#include "EventQueue.h"
#include "a_Saturn.h"
#include "watchdog.h"
#include "delay.h"
#include "system.h"


static int RegisterWE(uint16_t adr, bool enable)
{
	int 	status;
	uint8_t val = 0;

	switch(adr)
	{
	case SAT_FLG:
		val = stimSPI_Get(SAT_WE5);
		status = (enable == true) ? stimSPI_Put(SAT_WE5, (FLGWE | val)) : stimSPI_Put(SAT_WE5, (~FLGWE & val));
		 break;

	case SAT_RVLD1:
	case SAT_RVLD2:
	case SAT_RVLD3:
	case SAT_RVLD4:
	case SAT_RVLD5:
		val = stimSPI_Get(SAT_WE5);
		status = enable == true ? stimSPI_Put(SAT_WE5, (RVLDWE | val)) : stimSPI_Put(SAT_WE5, (~RVLDWE & val));
		break;

	case SAT_ENABLE:
		val = stimSPI_Get(SAT_WE5);
		status = enable == true ? stimSPI_Put(SAT_WE5, (PWRWE | val)) : stimSPI_Put(SAT_WE5, (~PWRWE & val));
		 break;

	case SAT_SEQ0A:
	case SAT_SEQ0B:
		val = stimSPI_Get(SAT_WE5);
		status = enable == true ? stimSPI_Put(SAT_WE5, (SEQ0WE | val)) : stimSPI_Put(SAT_WE5, (~SEQ0WE & val));
		break;

	case SAT_SEQ1A:
	case SAT_SEQ1B:
		val = stimSPI_Get(SAT_WE5);
		status = enable == true ? stimSPI_Put(SAT_WE5, (SEQ1WE | val)) : stimSPI_Put(SAT_WE5, (~SEQ1WE & val));
		break;

	case SAT_RECT1:
	case SAT_RECT2:
	case SAT_RECT3:
	case SAT_RECT4:
	case SAT_RECT5:
		val = stimSPI_Get(SAT_WE4);
		status = enable == true ? stimSPI_Put(SAT_WE4, 0xFF) : stimSPI_Put(SAT_WE4, 0xC0 | val);	// keep RAM writes enabled
		break;

	case SAT_CIM0:
	case SAT_CIM1:
	case SAT_CIM_GCL:
	case SAT_CIM_GCU:
	case SAT_SH_PHASE:
	case SAT_SH_DELAYL:
	case SAT_SH_DELAYH:
	case SAT_HV_CTRL:
	case SAT_CM_CTRL:
	case SAT_VG_CTRL:
	case SAT_BC_CTRL0:
	case SAT_BC_CTRL1:
	case SAT_CG_CTRL:
	case SAT_WFD_CTRL:
		val = stimSPI_Get(SAT_WE5);
		status = enable == true ? stimSPI_Put(SAT_WE5, (AWE | val)) : stimSPI_Put(SAT_WE5, (~AWE & val));
		break;

	case SAT_BG_CTRL:
	case SAT_PGO_CTRL:
		val = stimSPI_Get(SAT_WE5);
		status = enable == true ? stimSPI_Put(SAT_WE5, (TRIMWE | val)) : stimSPI_Put(SAT_WE5, (~TRIMWE & val));
		break;

		default:
		break;
	}
	return(status);

}

inline static void stimHw_Send(uint8_t d)
{
	UCA0TXBUF = d;
}

inline static uint8_t stimHw_Recv(void)
{
	return UCA0RXBUF;
}

inline static uint8_t stimHw_IsBusy(void)
{
	return UCA0STAT & UCBUSY;
}

inline static uint8_t stimHw_IsTxReady(void)
{
	return 	UC0IFG & UCA0TXIFG;
}

inline static void stimHw_AssertSCS(void)
{
#ifdef EPG
	P7OUT &= ~STIM_CS;
#elif defined IPG_REV05
	P4OUT &= ~STIM_CS;
#else
	P7OUT &= ~STIM_CS;
#endif
}

inline static void stimHw_DeassertSCS(void)
{
#ifdef	EPG
	P7OUT |= STIM_CS;
#elif defined IPG_REV05
	P4OUT |= STIM_CS;
#else
	P7OUT |= STIM_CS;
#endif
}

/**
 * Set the CTL0 value for the Saturn SPI interface
 */
static void stimHw_SetCTL0(uint8_t d)
{
	UCA0CTL0 = d;
}

/**
 * Set the CTL1 value for the Saturn SPI interface
 */
static void stimHw_SetCTL1(uint8_t d)
{
	UCA0CTL1 = d;
}

/**
 * Set the BR (baud rate) value for the Saturn SPI interface
 */
static void stimHw_SetBR(uint16_t d)
{
	UCA0BR0 = d & 0xff;
	UCA0BR1 = (d >> 8) & 0xff;
}

/**
 * Wait for the SPI Busy flag to clear
 */

static inline void waitWhileBusy(void)
{
	while (stimHw_IsBusy())
	{
	}
}

/**
 * Wait for the SPI Busy flag to clear
 */

static inline void waitUntilTxReady(void)
{
	while (!stimHw_IsTxReady())
	{
	}
}

//----------------------------------------------------------------------------------------------------------------

/**
 * Set up the SPI hardware for communications with Saturn.
 *
 * This is "Resume" and not "Init" because we have to be able to power down
 * Saturn.  Hence, stimHw_Init sets up the SPI pins in a grounded state, and
 * only later is this function called to make them active.
 */

void stimSPI_Resume(void)
{
    // Set up MICS SPI chip select
#ifdef EPG
    P7OUT |= STIM_CS;
    P7SEL &= ~STIM_CS;
    P7DIR |= STIM_CS;
#elif defined IPG_REV05
    P4OUT |= STIM_CS;
    P4SEL &= ~STIM_CS;
    P4DIR |= STIM_CS;
#else
    P7OUT |= STIM_CS;
    P7SEL &= ~STIM_CS;
    P7DIR |= STIM_CS;
#endif

    // Put USCI_A0 (Saturn SPI) into reset and clear all other bits
    stimHw_SetCTL1(UCSWRST);
  
    // Configure USCI_A0

    stimHw_SetCTL0(UCCKPH 		// SPI mode 0: UCCKPH = 1, UCCKPL = 0
             		| UCMSB		// MSB first (standard SPI format)
             		| UCMST		// Master mode
             		| UCMODE_0	// UCMODEx = 00 (3-pin SPI)
             		| UCSYNC);	// Select synchronous mode for SPI

    stimHw_SetCTL1(UCSSEL_2		// UCSELx = 10 (Clock source is SMCLK)
  	  	     		| UCSWRST);	// Maintain USCI_A0 in reset
  
    stimHw_SetBR(1);  		// Running at SMCLK (1 MHz), so divide by 1

    // Switch the pins to SPI mode
    P3SEL |= STIM_SDI | STIM_SDO | STIM_SCK;
  
    // Release the USART from reset
    stimHw_SetCTL1(UCSSEL_2);
}


/**
 * Suspend the Saturn SPI interface by putting it into reset
 */

void stimSPI_Suspend(void)
{
    // Put USCI_A0 (Saturn SPI) into reset and clear all other bits
	stimHw_SetCTL1(UCSWRST);
}



/**
 * Sends one byte of data through SPI without chip select.  Saturn needs
 * this in order to clock the reset synchronizers and release the chip
 * from reset.  (It actually needs only two cycles, while this will send
 * eight, but it's easier to send a full byte than to bit-bang the
 * CLK line.)  
 */
  
void stimSPI_ClockThroughReset(void)
{
	stimHw_Send(0);			// Send one byte just to run SCK
	waitWhileBusy();		// Loop until the byte is sent
}


/**
 * Write one byte to the Saturn registers without readback verification
 *
 * The caller is responsible for detecting and handling address fault (ADRF)
 * exceptions.
 *
 * \param addr The Saturn register address to write
 * \param data The byte to write at the address
 */

void stimSPI_UnverifiedPut(uint16_t addr, uint8_t data)
{
	RegisterWE(addr, true);

	stimHw_AssertSCS();
	stimHw_Send((addr >> 8) & 0x7f); // Send address high byte with R/W = 0 (W)
	waitUntilTxReady();
	stimHw_Send(addr & 0xff);		// Send address low byte
	waitUntilTxReady();
	stimHw_Send(data);				// Send the data byte
	waitWhileBusy();				// Wait for completion before releasing chip select
	stimHw_DeassertSCS();

	RegisterWE(addr, false);
}


/**
 * stimSPI_Put() - Write one byte to the Saturn registers.
 *
 * Readback verification is performed.
 *
 * The caller is responsible for detecting and handling address fault (ADRF)
 * exceptions.
 *
 * \param addr The Saturn register address to write
 * \param data The byte to write at the address
 * \return 0 if readback verifies a correct write, -1 otherwise
 */

int stimSPI_Put(uint16_t addr, uint8_t data)
{
	int status 	= 0;
	int count	= 0;

	RegisterWE(addr, true);
	do
	{
		// Write the data
		stimSPI_UnverifiedPut(addr, data);

		// Verify the write
		if (data == stimSPI_Get(addr))
		{
			// Read-back verification succeeded
			break;
		}
		else
		{
			status = -1;
			count++;
		}
	}while(status == -1 && count < 3);
	RegisterWE(addr, false);

	if(status == -1)
		// catastrophic bus failure
	{
		uint8_t flags = 0;

		DBGI(f_stim_SPI, addr);
		DBGI(f_stim_SPI, data);

		flags = stimSPI_Get(SAT_FLG);
		DBGI(f_stim_SPI, flags);

		logError(E_LOG_ERR_STIM_DATA_READBACK, 1);

#ifndef DISABLE_STIM_SPI_RESET
		setTST_TRIG_HIGH();
		DELAY_US(4);
		setTST_TRIG_LOW();

		resetSystem();
#else
		setResponseCode(RESP_STIM_SETUP_ERROR_BAD_STIM_ASIC_READBACK);
		dispPutErrorEvent(ACTERR_STIM_ASIC_READ_BACK_ERROR);
#endif
		// maybe should perform a board reset here
	}
	return status;
}


/**
 * stimSPI_Get() - Read one byte from the Saturn registers.
 *
 * The caller is responsible for detecting and handling address fault (ADRF)
 * exceptions.
 *
 * \param addr The Saturn register address to read.
 * \return The byte read at address addr.
 */

uint8_t stimSPI_Get(uint16_t addr)
{
	stimHw_AssertSCS();
	stimHw_Send((addr >> 8) | 0x80);	// Read flag	
	waitUntilTxReady();
	stimHw_Send(addr & 0xff);			// Send the address second byte
	waitUntilTxReady();
	stimHw_Send(0x00);					// Send the dummy byte
	waitUntilTxReady();
	stimHw_Send(0x00);					// Clock out the data byte
	waitWhileBusy();
	stimHw_DeassertSCS();

	return stimHw_Recv();
}


/**
 * stimSPI_Read() - Read multiple bytes from Saturn
 *
 * The caller is responsible for detecting and handling address fault (ADRF)
 * exceptions.
 * 
 * \param addr The Saturn register address to be accessed.
 * \param data Pointer to data to be written
 * \param len  Number of bytes to write
 */

void stimSPI_Read(uint16_t addr, void *data, size_t len)
{
	uint8_t *p;
	
	stimHw_AssertSCS();
	stimHw_Send((addr >> 8) | 0x80);	// Address high byte 
	waitUntilTxReady();
	stimHw_Send(addr & 0xff);			// Address low byte
	waitUntilTxReady();
	stimHw_Send(0);						// Dummy byte
	waitUntilTxReady();
	for (p = data; len > 0; len--)
	{
		stimHw_Send(0);		// Clock out the received data byte
		waitWhileBusy()	;
		*p++ = stimHw_Recv();		// Read the data byte
	}
	stimHw_DeassertSCS();				// Release chip select
}


/**
 * sat_Write() - Writes multiple bytes to Saturn 
 *
 * Readback verification is performed.
 * 
 * The caller is responsible for detecting and handling address fault (ADRF)
 * exceptions.
 *
 * \param addr The Saturn register address to be accessed.
 * \param data Pointer to data to be written
 * \param len  Number of bytes to write
 * \return 0 if readback verification succeeds, -1 otherwise
 */


int stimSPI_Write(uint16_t addr, const void *data, size_t len)
{
	const uint8_t *p;
	size_t n;
	
	RegisterWE(addr, true);

	//
	// Write the data block
	//

	stimHw_AssertSCS();
	
	stimHw_Send((addr >> 8) & 0x7f); // Send address high byte with R/W = 0 (W)
	waitUntilTxReady();
		
	stimHw_Send(addr & 0xff);		// Send address low byte
	
	for (p = data, n = len; n > 0; n--, p++)
	{
		waitUntilTxReady();
		stimHw_Send(*p);			// Send the data byte
	}
	waitWhileBusy();		// Wait for completion before releasing chip select
		
	stimHw_DeassertSCS();

	//
	// Read back and verify the data block
	//

	stimHw_AssertSCS();
	
	stimHw_Send((addr >> 8) | 0x80);	// Address high byte 
	waitUntilTxReady();
		
	stimHw_Send(addr & 0xff);			// Address low byte
	waitUntilTxReady();
		
	stimHw_Send(0);						// Dummy byte
	waitUntilTxReady();
		
	for (p = data; len > 0; len--)
	{
		stimHw_Send(0);					// Clock out the received data byte
		waitWhileBusy();
		if (*p++ != stimHw_Recv())	// Read the data byte
		{
			break;
		}
	}
	
	stimHw_DeassertSCS();				// Release chip select


	RegisterWE(addr, false);

	// If we exited the loop early, the verify failed.
	if (len == 0)
	{
		// Read-back verification succeeded
		return 0;
	}
	else
	{
		uint8_t flags = 0;
		int i;
		uint8_t *ptr = (uint8_t *)data;

		flags = stimSPI_Get(SAT_FLG);
		DBGI(f_stim_SPI, flags);

		DBGI(f_stim_SPI, addr);
		DBGI(f_stim_SPI, (uint32_t)len);

		for(i = 0; i < len; i++)
		{
			DBGI(f_stim_SPI, ptr[0]);
		}

		logError(E_LOG_ERR_STIM_DATA_READBACK, 2);

#ifndef DISABLE_STIM_SPI_RESET
		setTST_TRIG_HIGH();
		DELAY_US(4);
		setTST_TRIG_LOW();

		resetSystem();
#else
		// Read-back verification failed.  Set Active Error and Response Code, and log it.
		setResponseCode(RESP_STIM_SETUP_ERROR_BAD_STIM_ASIC_READBACK);
		dispPutErrorEvent(ACTERR_STIM_ASIC_READ_BACK_ERROR);
#endif
		// send response to external and reset the board

		return -1;
	}

}

int stimSPI_UnverifiedWrite(uint16_t addr, const void *data, size_t len)
{
	const uint8_t *p;
	size_t n;

	RegisterWE(addr, true);

	//
	// Write the data block
	//

	stimHw_AssertSCS();

	stimHw_Send((addr >> 8) & 0x7f); // Send address high byte with R/W = 0 (W)
	waitUntilTxReady();

	stimHw_Send(addr & 0xff);		// Send address low byte

	for (p = data, n = len; n > 0; n--, p++)
	{
		waitUntilTxReady();
		stimHw_Send(*p);			// Send the data byte
	}
	waitWhileBusy();		// Wait for completion before releasing chip select

	stimHw_DeassertSCS();

	return(0);
}

