#include "DeviceDriverLayer/Common/msp430f2618.h"
#include "DeviceDriverLayer/Common/bits.h"
#include "log.h"
#include "delay.h"

#include "Stim.h"
#include "a_Saturn.h"
#include "stimspi.h"
#include "a_RegisterIO.h"
#include "timeout.h"
#include "timer.h"

static int sat_SetDelay(uint16_t delay)
{
	uint8_t data[2];

	data[0] = (uint8_t)(delay & 0x00FF);
	data[1] = (uint8_t)((delay >> 8) &0x00FF);

	stimSPI_Put(SAT_SH_DELAYL, data[0]);
	stimSPI_Put(SAT_SH_DELAYH, data[1]);

	return(0);
}

static int sat_SelectImpedanceChannels(uint8_t cim0, uint8_t cim1)
{
	uint8_t data[2];

	data[0] = cim0;
	data[1] = cim1;
	return stimSPI_Write(SAT_CIM0, &data, sizeof(data));
}

static int sat_SetImpedanceGains(uint8_t gainA, uint8_t gainB)
{
	uint8_t data[2];

	data[0] = gainA;
	data[1] = gainB;
	return stimSPI_Write(SAT_CIM_GCL, &data, sizeof(data));
}

// ---------------------------------------------------------------------------------------------------------

int stimImpedanceOn(void)
{
	int result = 0;

	result = sat_Set(SAT_ENABLE, (IME | HVE));

	return result;
}

int stimImpedanceOff(void)
{
	int result = 0;

	result = sat_Clear(SAT_ENABLE, (IME | HVE));

	return result;
}

int stimImpedanceConfigure(uint8_t chan_1, uint8_t chan_2, uint16_t gainA, uint16_t gainB, uint16_t delay, uint8_t phase)
{
	int 		result;
	uint16_t 	delay_scaled;

	if((result = sat_SelectImpedanceChannels(chan_1, chan_2)) == 0)
	{
		if((result = sat_SetImpedanceGains(gainA, gainB)) == 0)
		{
			// phase to take measurement on
			if((result = stimSPI_Put(SAT_SH_PHASE, phase)) == 0)
			{
				// delay inside of the measurement phase
				delay_scaled = delay/SMCLK_SCALE_FACTOR;
				result = sat_SetDelay(delay_scaled);
			}
		}
	}

	return result;
}

int stimImpedanceConfigureChannels(uint8_t chan_1, uint8_t chan_2)
{
	return(sat_SelectImpedanceChannels(chan_1, chan_2));
}

#if 0
int stimImpedanceConfigureGains(uint16_t gainA, uint16_t gainB)
{
	return(sat_SetImpedanceGains(gainA, gainB));
}

int	stimImpedanceConfigurePhase(uint8_t phase, uint16_t delay)
{
	int 		result;
	uint16_t 	delay_scaled;

	if((result = stimSPI_Put(SAT_SH_PHASE, phase)) == 0)
	{
		// delay inside of the measurement phase
		delay_scaled = delay/SMCLK_SCALE_FACTOR;
		result = sat_SetDelay(delay_scaled);
	}
	return(result);
}


int stimImpedanceSync()
{
	bool timeoutFlag = false;
	int16_t		timeout;
	uint8_t	flag = 0;

	timeout = timeoutSet(SYNC_RETRY_TIMEOUT_TICKS);
	do
	{
		timeoutFlag = timeoutIsExpired(timeout);
		if((P2IN & STIM_TFLG) != 0)
		{
			// check one more time
			flag = (P2IN & STIM_TFLG);
		}
	}while (flag == 0 && timeoutFlag == false);

	if(timeoutFlag)
	{
		return -1;
	}
	else
	{
		return 0;
	}
#if 0
	while ((P4IN & STIM_TFLG) == 0 && counter < 100)
	{
		counter++;
		DELAY_MS(1);
	}

	if(counter == 100)
	{
		DBG(f_stim_measureimpedance,DBG_SEV_ERROR, 0, 0);
		return(-1);
	}
#endif

}

int stimImpedanceClearSH()
{
	uint8_t flags;

	stimSPI_UnverifiedPut(SAT_FLG, TFLG);
	stimSPI_UnverifiedPut(SAT_FLG, TFLG);

	flags = stimSPI_Get(SAT_FLG);

	if(flags & TFLG != 0)
	{
		return -1;
	}
	return 0;
}
#endif


