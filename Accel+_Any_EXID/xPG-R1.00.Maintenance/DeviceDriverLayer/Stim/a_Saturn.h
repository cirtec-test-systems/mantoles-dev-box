/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Saturn (Stim ASIC) register addresses and bit assignments
 * 
 *  NOTE: THESE WERE TAKEN FROM A RELEASE VERSION OF EESP 0085 Stim 
 *  ASIC Specifications,Rev 1.7  THEY HAVE NOT BEEN VERIFIED AND MAY
 *  POSSIBLY CHANGE WITH ADDITIONAL SPINS OF THE STIM ASIC  
 *	
 ***************************************************************************/
 
#ifndef SATURNREGS_H_
#define SATURNREGS_H_


#define TIMERB_DIVISOR	8
#define TIMERB_HZ		(1000000uL / TIMERB_DIVISOR)

/// Number of timer ticks between the timer recycling and the edge of STIM_SEL.
#define SEL_DELAY	1

/// Number of timer ticks between the timer recycling and the rising edge of STIM_RUN.
#define RUN_DELAY	2

/// The timeout for waiting for a program to stop, in timer ticks.
#define PRGM_STOP_TIMEOUT	MS((1000 / PROGRAM_FREQUENCY_MIN) + 1)

/// The settling time for the pulse guard oscillator after a re-trim, in microseconds
#define PGO_SETTLING_TIME 	300
#define DEFAULT_BC_CTRL0 0x0b	///< Initial value of boost converter voltage register (BC_CTRL0)


//
// Saturn control register addresses
//

#define SAT_FLG		0x000
#define SAT_ENABLE		0x001
#define SAT_WE1		0x002
#define SAT_WE2		0x003
#define SAT_WE3		0x004
#define SAT_WE4		0x005
#define SAT_WE5		0x006
#define SAT_RVLD1	  	0x007
#define SAT_RVLD2	  	0x008
#define SAT_RVLD3	  	0x009
#define SAT_RVLD4	  	0x00a
#define SAT_RVLD5	  	0x00b
#define SAT_SEQ0A	  	0x00c
#define SAT_SEQ0B	  	0x00d
#define SAT_SEQ1A	  	0x00e
#define SAT_SEQ1B	  	0x00f
#define SAT_RECT1	  	0x010
#define SAT_RECT2	  	0x011
#define SAT_RECT3	  	0x012
#define SAT_RECT4	  	0x013
#define SAT_RECT5	  	0x014
#define SAT_RECT6	  	0x015
#define SAT_CIM0	  	0x016
#define SAT_CIM1	  	0x017
#define SAT_CIM_GCL	0x018
#define SAT_CIM_GCU	0x019
#define SAT_SH_PHASE  	0x01a
#define SAT_SH_DELAYL 	0x01b
#define SAT_SH_DELAYH 	0x01c
#define SAT_HV_CTRL	0x01d
#define SAT_CM_CTRL	0x01e
#define SAT_VG_CTRL	0x01f
#define SAT_BC_CTRL0	0x020
#define SAT_BC_CTRL1	0x021
#define SAT_BG_CTRL	0x022
#define SAT_PGO_CTRL	0x023
#define SAT_CG_CTRL	0x024
#define SAT_WFD_CTRL	0x025
#define SAT_ACCA		0x026
#define SAT_ACCB		0x027
#define SAT_ACCC		0x028
#define SAT_WDAC		0x029
#define SAT_SEQSTATE	0x02a
#define SAT_CPH	    0x02b
#define SAT_TESTEN		0x02c
#define SAT_TESTSEL	0x02d
#define SAT_ATSTCTRL0	0x02e
#define SAT_ATSTCTRL1	0x02f
#define SAT_ATSTCTRL2	0x030
#define SAT_ATSTCTRL	0x031


//
// Base addresses of phase register banks
// Each phase register bank is 64 bytes in length
//

enum {
	SAT_PH1 = 0x040,
	SAT_PH2 = 0x080,
	SAT_PH3 = 0x0c0,
	SAT_PH4 = 0x100,
	SAT_PH5 = 0x140,
	SAT_PH6 = 0x180,
	SAT_PH7 = 0x1c0,
	SAT_PH8 = 0x200,
	SAT_PH9 = 0x240,
	SAT_PH10 = 0x280,
	SAT_PH11 = 0x2c0,
	SAT_PH12 = 0x300,
	SAT_PH13 = 0x340,
	SAT_PH14 = 0x380,
	SAT_PH15 = 0x3c0,
	SAT_PH16 = 0x400,
	SAT_PH17 = 0x440,
	SAT_PH18 = 0x480,
	SAT_PH19 = 0x4c0,
	SAT_PH20 = 0x500,
	SAT_PH21 = 0x540,
	SAT_PH22 = 0x580,
	SAT_PH23 = 0x5c0,
	SAT_PH24 = 0x600
};


//
// Phase register offsets in bank
//

enum {
	PHX_CTL1 = 0x00,
	PHX_CTL2,
	PHX_STPA,
	PHX_STPB,
	PHX_STPC,
	PHX_OFSET,
	PHX_DLYL,
	PHX_DLYU,
	PHX_CDIV,
	PHX_AMP01L,
	PHX_AMP01U,
	PHX_AMP02L,
	PHX_AMP02U,
	PHX_AMP03L,
	PHX_AMP03U,
	PHX_AMP04L,
	PHX_AMP04U,
	PHX_AMP05L,
	PHX_AMP05U,
	PHX_AMP06L,
	PHX_AMP06U,
	PHX_AMP07L,
	PHX_AMP07U,
	PHX_AMP08L,
	PHX_AMP08U,
	PHX_AMP09L,
	PHX_AMP09U,
	PHX_AMP10L,
	PHX_AMP10U,
	PHX_AMP11L,
	PHX_AMP11U,
	PHX_AMP12L,
	PHX_AMP12U,
	PHX_AMP13L,
	PHX_AMP13U,
	PHX_AMP14L,
	PHX_AMP14U,
	PHX_AMP15L,
	PHX_AMP15U,
	PHX_AMP16L,
	PHX_AMP16U,
	PHX_AMP17L,
	PHX_AMP17U,
	PHX_AMP18L,
	PHX_AMP18U,
	PHX_AMP19L,
	PHX_AMP19U,
	PHX_AMP20L,
	PHX_AMP20U,
	PHX_AMP21L,
	PHX_AMP21U,
	PHX_AMP22L,
	PHX_AMP22U,
	PHX_AMP23L,
	PHX_AMP23U,
	PHX_AMP24L,
	PHX_AMP24U,
	PHX_AMP25L,
	PHX_AMP25U,
	PHX_AMP26L,
	PHX_AMP26U,
	PHX_PG_TIMEOUT,
	PHX_UNUSED01,
	PHX_UNUSED02,
	
	// aliases for 16-bit access
	PHX_AMP01 = PHX_AMP01L,
	PHX_AMP02 = PHX_AMP02L,
	PHX_AMP03 = PHX_AMP03L,
	PHX_AMP04 = PHX_AMP04L,
	PHX_AMP05 = PHX_AMP05L,
	PHX_AMP06 = PHX_AMP06L,
	PHX_AMP07 = PHX_AMP07L,
	PHX_AMP08 = PHX_AMP08L,
	PHX_AMP09 = PHX_AMP09L,
	PHX_AMP10 = PHX_AMP10L,
	PHX_AMP11 = PHX_AMP11L,
	PHX_AMP12 = PHX_AMP12L,
	PHX_AMP13 = PHX_AMP13L,
	PHX_AMP14 = PHX_AMP14L,
	PHX_AMP15 = PHX_AMP15L,
	PHX_AMP16 = PHX_AMP16L,
	PHX_AMP17 = PHX_AMP17L,
	PHX_AMP18 = PHX_AMP18L,
	PHX_AMP19 = PHX_AMP19L,
	PHX_AMP20 = PHX_AMP20L,
	PHX_AMP21 = PHX_AMP21L,
	PHX_AMP22 = PHX_AMP22L,
	PHX_AMP23 = PHX_AMP23L,
	PHX_AMP24 = PHX_AMP24L,
	PHX_AMP25 = PHX_AMP25L,
	PHX_AMP26 = PHX_AMP26L
};


//
// Phase register banks
//
// CAUTION: A quirk of the chip is that the multi-byte registers are not word-aligned.
// However, the MSP430 requires word alignment for 16-bit and 32-bit writes.
// To align the amplitude registers, the structure below starts with an pad byte that
// does not exist in the hardware. Watch out for that when downloading the structure
// to the ASIC.  The first byte of the structure should be skipped.
//
// Also, the registers are laid out such that the delay register and the amplitude
// registers cannot be simultaneously word-aligned.  For that reason, the delay registers
// are split into bytes.  The step register, a 24-bit register, is split for similar
// reasons plus the fact that a 24 bit variable is going to be quirky to access anyway.
// 
// Note, too, that the two unused bytes at the end of the phase register bank are not
// included in this structure.
// 

typedef struct PHX_CONTROL {
	uint8_t ctl1;
	uint8_t ctl2;
	uint8_t stpa;
	uint8_t stpb;
	uint8_t stpc;
	uint8_t ofst;
	uint16_t dly;
	uint8_t cdv;
} PHX_CONTROL;


//
// Saturn RAM bank addresses
//

enum {
	SAT_RAM1 = 0x800,
	SAT_RAM2 = 0x900
};	

#define RAM_SIZE 256

//
// CG_CTRL register bit assignments
//
#define CVNA0   0x01  ///< Current generator A control bit 0
#define CVNA1   0x02  ///< Current generator A control bit 1
// 0x04 unused
#define AE      0x08  ///< Current generator A enable
#define CVNB0   0x10  ///< Current generator B control bit 0
#define CVNB1   0x20  ///< Current generator B control bit 1
// 0x40 unused
#define BE      0x80  ///< Current generator B enable


//
// ENABLE register bit assignments
//
#define BCE	0x01	///< ENABLE register: Boost converter enable
#define BGE	0x02	///< ENABLE register: Bandgap enable
#define WGE	0x04	///< ENABLE register: Waveform generator enable
#define HVE	0x08	///< ENABLE register: High voltage LDO enable
#define SHE	0x10	///< ENABLE register: Sample/hold enable
#define IME	0x20	///< ENABLE register: Impedance measurement enable
#define CE		0x40	///< ENABLE register: Calibration enable
#define OE		0x80	///< ENABLE register: Pulse guard oscillator enable


//
//FLAG register bit assignments
//
#define PGF	0x01	///< FLG register: PGF (pulse guard fault)
#define PARF	0x02	///< FLG register: PARF (parity fault)
#define SEQF	0x04	///< FLG register: SEQF (SEQxx register fault)
#define ADRF	0x08	///< FLG register: ADRF (address fault)
#define RVF	0x10	///< FLG register: RVF (register valid fault)
#define WPF	0x20	///< FLG register: WPF (write protect fault)
// 0x40 unused
#define TFLG	0x80	///< FLG register: TFLG (timer flag)

//
// WE4 register bit assignments
//
#define RECT1WE  0x01 ///< WE4 register: rect reg 1 write enable
#define RECT2WE  0x02 ///< WE4 register: rect reg 2 write enable
#define RECT3WE  0x04 ///< WE4 register: rect reg 3 write enable
#define RECT4WE  0x08 ///< WE4 register: rect reg 4 write enable
#define RECT5WE  0x10 ///< WE4 register: rect reg 5 write enable
#define RECT6WE  0x20 ///< WE4 register: rect reg 6 write enable
#define RAM1WE   0x40 ///< WE4 register: shape ram 1 write enable
#define RAM2WE   0x80 ///< WE4 register: shape ram 2 write enable

//
// WE5 register bit assignments
//
#define SEQ0WE   0x01	///< WE5 register: SEQ0A/B write enable
#define SEQ1WE   0x02	///< WE5 register: SEQ1A/B write enable
#define PWRWE    0x04	///< WE5 register: ENABLE write enable
#define FLGWE    0x08	///< WE5 register: FLG write enable
#define RVLDWE   0x10	///< WE5 register: RVLD write enable
#define TESTCALWE 0x20 ///< WE5 register: Test register write enable
#define AWE      0x40 	///< WE5 register: Analog register write enable
#define TRIMWE   0x80 	///< WE5 register: Trim register write enable

//
// RVLD4 register bit assignments
//
#define RECT1V  0x01 ///< RVLD4 register: rect reg 1 valid
#define RECT2V  0x02 ///< RVLD4 register: rect reg 2 valid
#define RECT3V  0x04 ///< RVLD4 register: rect reg 3 valid
#define RECT4V  0x08 ///< RVLD4 register: rect reg 4 valid
#define RECT5V  0x10 ///< RVLD4 register: rect reg 5 valid
#define RECT6V  0x20 ///< RVLD4 register: rect reg 6 valid
#define RAM1V   0x40 ///< RVLD4 register: shape ram 1 valid
#define RAM2V   0x80 ///< RVLD4 register: shape ram 2 valid


//
// RVLD5 register bit assignments
//
#define SEQ0V	0x01
#define SEQ1V	0x02

//
// VG_CTRL register bit assignments
//
#define CAS_EN		0x01	///< VG_CTRL register: Cascode enable
#define HVDDL_EN	0x02	///< VG_CTRL register: Level shifter HVDD low enable
#define LS_RESET	0x04	///< VG_CTRL register: Level shifter reset
#define RAM1_EN	0x08	///< VG_CTRL register: RAM1 enable
#define RAM2_EN	0x10	///< VG_CTRL register: RAM2 enable

//
// WFD_CTRL register bit assignments
//
#define WFD_CLK_EN	0x08	///< WFD_CTRL register: Waveform DAC clock enable
#define WDO2		0x04	///< WFD_CTRL register: Waveform DAC reference current control, bit 2
#define WDO1		0x02	///< WFD_CTRL register: Waveform DAC reference current control, bit 1
#define WDO0		0x01	///< WFD_CTRL register: Waveform DAC reference current control, bit 0

#define WDO_0		0x00	///< WFD_CTRL register: Waveform DAC reference current control = 0
#define WDO_1		0x01	///< WFD_CTRL register: Waveform DAC reference current control = 1
#define WDO_2		0x02	///< WFD_CTRL register: Waveform DAC reference current control = 2
#define WDO_3		0x03	///< WFD_CTRL register: Waveform DAC reference current control = 3
#define WDO_4		0x04	///< WFD_CTRL register: Waveform DAC reference current control = 4
#define WDO_5		0x05	///< WFD_CTRL register: Waveform DAC reference current control = 5
#define WDO_6		0x06	///< WFD_CTRL register: Waveform DAC reference current control = 6
#define WDO_7		0x07	///< WFD_CTRL register: Waveform DAC reference current control = 7


//
// BC_CTRL1 register bit assignments
//
#define ICC0		0x01	///< BC_CTRL1 register: Input current control bit 0
#define ICC1		0x02	///< BC_CTRL1 register: Input current control bit 0
#define ICC2		0x04	///< BC_CTRL1 register: Input current control bit 0
#define POR		0x08	///< BC_CTRL1 register: Boost converter power-on reset
#define VBOOST_EN	0x10	///< BC_CTRL1 register: Boost converter enable
#define LVBAT		0x20	///< BC_CTRL1 register: Low battery indicator
#define BCG		0x80	///< BC_CTRL1 register: Boost converter voltage good

#define ICC_MASK   (ICC0 | ICC1 | ICC2)	// Mask for the ICC bits

#define ICC_50MA   0x00  ///< ICC setting for 50 mA
#define ICC_100MA  0x01  ///< ICC setting for 100 mA 
#define ICC_150MA  0x02  ///< ICC setting for 150 mA
#define ICC_200MA  0x03  ///< ICC setting for 200 mA
#define ICC_250MA  0x04  ///< ICC setting for 250 mA
#define ICC_300MA  0x05  ///< ICC setting for 300 mA


//
// CM_CTRL register bit assignments
//
#define CAL_STAT_EN	0x01	///< CM_CTRL register: Reserved bit. Not used.
#define CAL_RESET		0x02	///< CM_CTRL register: Reserved bit. Not used.
#define CAL_STAT_SINK	0x04	///< CM_CTRL register: Set high when calibrating sink currents.
#define CALM0			0x08	///< CM_CTRL register: Calibrate CASP bias voltage, bit 0
#define CALM1			0x10	///< CM_CTRL register: Calibrate CASP bias voltage, bit 1
#define CALM2			0x20	///< CM_CTRL register: Calibrate CASP bias voltage, bit 2
#define CALM3			0x40	///< CM_CTRL register: Reserved bit. Not used.
#define CALM4			0x80	///< CM_CTRL register: Reserved bit. Not used.

#define CASP_0	0
#define CASP_1	(CALM0)
#define CASP_2	(CALM1)
#define CASP_3	(CALM1 | CALM0)
#define CASP_4	(CALM2)
#define CASP_5	(CALM2 | CALM0)
#define CASP_6	(CALM2 | CALM1)
#define CASP_7	(CALM2 | CALM1 | CALM0)


//
// BG_CTRL register bit assignments
//

#define TC0		0x01
#define TC1		0x02
#define BGT0		0x04
#define BGT1		0x08
#define BGT2		0x10
#define BGT3		0x20
#define BGT4		0x40
#define BGT5		0x80

//
// PHx.CTL1 register bit assignments
//

#define DPFCBS_EN	0x10	///< controls channel impedance and internal signals in a complex way
#define USPN		0x20	///< uncontrolled src/sink polarity (0 = sink, 1 = source)
#define HVOFF		0x40	///< boost converter disabled during stim
#define DPFE		0x80	///< delay/pause flag enable

//
// PHx.CTL2 register bit assignments
//
#define RPT0	0x01
#define RPT1	0x02
#define RPT2	0x04
#define SYNC	0x10
#define PAUSE	0x20


//
// PHx.CDV register bit assignments
//

#define CS			0x80	///< Selects divided clock (0) or original clock (1)


//
// PHx AMPxU/L register pair bit assignments
//
// NOTE: AMPxU/L values are handled internally as 16-bit numbers, so the flags
//		below are defined in terms of 
//       the bit assignments listed here reference operations on an integer
//
#define AMP_AMP_MASK   0x03FF
#define AMP_SSE        0x0400  //source / sink enable
                                                // 0 = source, 1 = sink
#define AMP_CALE       0x0800  //calibration enable
#define AMP_CHE        0x1000  //channel enable
#define AMP_USPNE      0x2000  //uncontrolled src / sink enable
#define AMP_CBS        0x4000  //charge balance switch

/// @}

#endif // SATURNREGS_H_
