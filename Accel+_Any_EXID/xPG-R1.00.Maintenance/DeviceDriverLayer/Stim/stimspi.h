/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: stimSPI (Stimulation SPI) module prototypes
 *	
 ***************************************************************************/

#ifndef STIMSPI_H_
#define STIMSPI_H_

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

/// \defgroup stimSPI Stimulation ASIC SPI Interface
/// \ingroup stim
/// @{

void 	stimSPI_Resume(void);
void 	stimSPI_Suspend(void);
void 	stimSPI_ClockThroughReset(void);

uint8_t stimSPI_Get(uint16_t addr);
int 	stimSPI_Put(uint16_t addr, uint8_t data);
void 	stimSPI_UnverifiedPut(uint16_t addr, uint8_t data);

void 	stimSPI_Read(uint16_t addr, void *data, size_t len);
int 	stimSPI_Write(uint16_t addr, const void *data, size_t len);
int 	stimSPI_UnverifiedWrite(uint16_t addr, const void *data, size_t len);

int 	stimRegisterWE(uint16_t adr, bool enable);

/// @}

#endif /*STIMSPI_H_*/
