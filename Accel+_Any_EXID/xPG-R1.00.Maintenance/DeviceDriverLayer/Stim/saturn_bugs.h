/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Definitions controlling Saturn bug workarounds
 * 
 ***************************************************************************/
 
#ifndef SATURNBUGS_H_
#define SATURNBUGS_H_

/// \defgroup saturnBugs Saturn Bug Workaround Control
/// \ingroup stim
/// @{

/**
 * Configure which version of the Saturn silicon we are building for
 */ 

#ifndef SATURN_REV
#define SATURN_REV	904
#endif

/*
 * Now, use the Saturn version information to turn on the right set of
 * bug workarounds
 */

#if SATURN_REV == 900

#  define SATURN_BUG_VDDIO_LATCHUP	///< This mask set will latch up if VDDIO rises too quickly.  Workaround requires a slow-start circuit.
#  define SATURN_BUG_BROKEN_BCG	///< This mask set has an unusable BCG (boost converter good) bit
#  define SATURN_BUG_BROKEN_USINK	///< This mask set has broken uncontrolled sink switches
#  define SATURN_BUG_VOLTAGE_CLAMP	///< This mask set does not support the full boost converter range

#elif SATURN_REV == 904

#  define SATURN_BUG_BROKEN_BCG	///< This mask set has an unusable BCG (boost converter good) bit
#  define SATURN_BUG_COMMON_MODE	///< This mask set does not support the full common mode range for impedance measurement
#  define SATURN_BUG_IMPEDANCE_VDD	///< This mask set requires 3V on DVDD and AVDD for impedance measurement to work correctly

#else // SATURN_REV not recognized

#  error "Unknown SATURN_REV. See saturn_bugs.h"

#endif // SATURN_REV

/// @}

#endif // SATURNBUGS_H_
