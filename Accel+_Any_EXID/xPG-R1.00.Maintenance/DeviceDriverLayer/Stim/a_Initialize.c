#include <stdbool.h>

#include "Common/Protocol/trim_list.h"
#include "DeviceDriverLayer/Common/msp430f2618.h"
#include "DeviceDriverLayer/Common/bits.h"
#include "stimspi.h"
#include "system.h"

void stimInit(void)
{
	/*
	 * Set up P2. This one is all inputs.
	 */

	// ground the inputs
    P2OUT &= ~(STIM_TFLG | STIM_ERR);
    P2REN |= (STIM_TFLG | STIM_ERR);
    // configure as inputs
    P2DIR &= ~(STIM_TFLG | STIM_ERR);
    P2SEL &= ~(STIM_TFLG | STIM_ERR);

	/*
	 * Set up P4/Timer_B. This is part inputs and part outputs, but the entire
	 * port is dedicated to Saturn so we can set it directly instead of using
	 * read-modify-write.
	 */

    // ground the inputs and leave outputs float
    P4OUT = 0x00;
    P4REN = (STIM_SYNC | STIM_PAUSE | STIM_PROG);
    // configure as outputs

#ifdef EPG
    P4DIR = STIM_STOP | STIM_CONT | STIM_RUN | STIM_SEL;
    // the STIM_CS signal was moved to pin P7.3 on EPG Rev2
    P7DIR |= STIM_CS;
#elif defined IPG_REV05
    P4DIR = STIM_STOP | STIM_CONT | STIM_RUN | STIM_SEL | STIM_CS;
#else
    P4DIR = STIM_STOP | STIM_CONT | STIM_RUN | STIM_SEL;
    P7DIR |= STIM_CS;
#endif
    		// STIM_SYNC, STIM_PAUSE, and STIM_PROG remain inputs
    P4SEL = 0x00;

    /*
     * Set up P5.
     */

    // STIM_CLK is initially set as an output, but will switch to the SMCLK
    // float the signals
    P5OUT &= ~STIM_CLK & ~STIM_RST;
    P5REN &= ~STIM_CLK & ~STIM_RST;
    // configure as outputs
    P5SEL &= ~STIM_RST & ~STIM_CLK;
    P5DIR |= STIM_RST | STIM_CLK;

	/*
	 * Set up P3 and USCI_A0 (SPI bus)
	 *
	 * Suspend USCI_A0, the Saturn SPI port, by putting it into reset.
	 * Set up the SCK, SDI, and SDO lines as GPIOs so they can be kept low
	 * by allGPIOLow(). Switch them to SPI mode during the Saturn power-up
	 * sequence.
	 */

    // ground STIM_SDO
    P3OUT &= ~(STIM_SCK | STIM_SDI | STIM_SDO);
    P3REN |= STIM_SDO;
    P3REN &= ~(STIM_SCK | STIM_SDI);
    // configure as I/O
	P3SEL &= ~STIM_SCK & ~STIM_SDI & ~STIM_SDO;
	P3DIR |= STIM_SCK | STIM_SDI;				// Outputs
	P3DIR &= ~STIM_SDO;							// Input

    /*
     * Set up P6 for the analog inputs
     *
     * Initially set these up as GPIOs so we can use the pulldown resistors
     * to minimize current consumption.  Switch them to analog mode during
     * the Saturn power-up sequence.
     */

	// ground all three signals
    P6OUT &= ~(STIM_IMPM | STIM_CAL_RESOUT | STIM_CAL_RESIN);
    P6REN &= ~(STIM_IMPM | STIM_CAL_RESOUT | STIM_CAL_RESIN);
    // configure all three as inputs
    P6DIR &= ~(STIM_IMPM | STIM_CAL_RESOUT | STIM_CAL_RESIN);
    //P6SEL &= ~(STIM_IMPM | STIM_CAL_RESOUT | STIM_CAL_RESIN);
    P6SEL |= (STIM_IMPM | STIM_CAL_RESOUT | STIM_CAL_RESIN);

    stimSPI_Suspend();

    setTST_TRIG_LOW();
}

void 	stimUpdateTrimList(bool tlValid, TRIM_LIST const *tl)
{
	(void)tlValid;
	(void)tl;
}

