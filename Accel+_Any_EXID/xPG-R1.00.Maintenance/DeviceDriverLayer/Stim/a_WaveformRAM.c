#include "DeviceDriverLayer/Common/msp430f2618.h"
#include "DeviceDriverLayer/Common/bits.h"
#include "lastresponse.h"

#include "Stim.h"
#include "a_Saturn.h"
#include "stimspi.h"
#include "a_RegisterIO.h"


/**
 * Determine if RAM1 is in use by the stimulation hardware.
 *
 * The RAMs are single-port, so they can be accessed via SPI only if
 * they are not being used by the stimulation hardware, and vice versa.
 * Switching between SPI and stim is controlled by the register valid
 * (RVLD) bit for each RAM.
 *
 * This function checks the RVLD bit for RAM1 to determine if the
 * RAM is currently switched to the stimulation hardware.
 *
 * @return true if the RAM is in use by stimulation hardware, or false
 *         if is it accessible via SPI.
 */
#ifdef FIXME
static bool sat_IsRAM1InUse(void)
{
	return (stimSPI_Get(SAT_RVLD4) & RAM1V) != 0;
}

static bool sat_IsRAM2InUse(void)
{
	return (stimSPI_Get(SAT_RVLD4) & RAM2V) != 0;
}

#else
static bool sat_IsRAM1InUse(void)
{
	return false;
}

static bool sat_IsRAM2InUse(void)
{
	return false;
}
#endif

int stimLoadWaveform(int ram, uint8_t const waveform[256])
{
	int result;

	if (ram == 0)
	{
		if (sat_IsRAM1InUse())
		{
			setResponseCode(CMND_RESP_BUSY);
			return -1;
		}
		if (sat_Set(SAT_VG_CTRL, RAM1_EN) < 0)
		{
			return -1;
		}
		stimSPI_Put(SAT_WE5, AWE);
		result = stimSPI_Write(SAT_RAM1, waveform, RAM_SIZE);
		stimSPI_Put(SAT_WE5, RVLDWE);
	}
	else
	{
		if (sat_IsRAM2InUse())
		{
			setResponseCode(CMND_RESP_BUSY);
			return -1;
		}
		if (sat_Set(SAT_VG_CTRL, RAM2_EN) < 0)
		{
			return -1;
		}
		stimSPI_Put(SAT_WE5, AWE);
		result = stimSPI_Write(SAT_RAM2, waveform, RAM_SIZE);
		stimSPI_Put(SAT_WE5, RVLDWE);
	}
	return(result);
}
