/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: MICS Configurator module implementation
 *	
 ***************************************************************************/

#include <stdbool.h>

#include "mics_io.h"
#include "DeviceDriverLayer/Common/msp430f2618.h"
#include "DeviceDriverLayer/Common/bits.h"

void mics_AssertWU_EN(void)
{
    P5OUT |= MICS_WU_EN;
}

void mics_DeassertWU_EN(void)
{
    P5OUT &= ~MICS_WU_EN;
}

bool mics_GetWU_EN(void)
{
    return P5OUT & MICS_WU_EN;
}

void mics_ConfigureWU_EN(void)
{
    P5DIR |= MICS_WU_EN;
}

bool mics_IsIRQAsserted(void)
{
    return (P1IN & MICS_IRQ) != 0;
}

void mics_AssertIRQFlag(void)
{
    P1IFG |= MICS_IRQ;
}

void mics_ClearIRQFlag(void)
{
    P1IFG &= ~MICS_IRQ;
}

bool mics_GetIRQFlag(void)
{
    return P1IFG & MICS_IRQ;
}

void mics_ConfigureIRQEdge(void)
{
    // Configure interrupt for rising edge
    P1IES &= ~MICS_IRQ;
}

void mics_EnableIRQ(void)
{
    P1IE |= MICS_IRQ;
}

void mics_SetCTL0(uint8_t d)
{
    UCB1CTL0 = d;
}

void mics_SetCTL1(uint8_t d)
{
    UCB1CTL1 = d;
}

void mics_SetBR(uint16_t d)
{
    UCB1BR0 = d & 0xff;
    UCB1BR1 = (d >> 8) & 0xff;
}

void mics_ConfigureSCS(void)
{
    P7DIR |= MICS_CS;
}
