/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: MICS driver MAC command.
 *
 *	Split out from the other internal functions to simplify an issue with
 *	unit testing.
 *
 ***************************************************************************/

#ifndef MICS_MAC_H_
#define MICS_MAC_H_

#include <stdint.h>

void mics_MacCmd(void (*macCmd)(), uint16_t maxMacCmdTime);

#endif                          /*EXECUTEMICSMACCMDWAITCOMPLETE_H_ */

