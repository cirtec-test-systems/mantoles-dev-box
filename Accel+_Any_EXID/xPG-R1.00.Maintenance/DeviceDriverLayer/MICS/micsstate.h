/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: micsMdl (MICS Model) module
 *
 ***************************************************************************/

#ifndef MICSMDL_H_
#define MICSMDL_H_

#include <stdint.h>
#include <stdbool.h>

/**
 * Defines for MICS state (MicsPub.state)
 *
 * MICS_SLEEPING:   MICS chip is either sleeping for starting up (before
 *                  IRQ_RADIOREADY
 *
 * MICS_IDLE:       MICS chip is awake and idle
 *
 * MICS_SENDING_EMERGENCY:
 *                  Sending emergency transmission (not yet determined if this
 *                  feature will be used in the IPG.
 * MICS_IN_SESSION: In session, the link is ready to transmit and receive data
 *                  and housekeeping messages.
 *
 * MIC_SENDING_WAKEUP_RESPONSES:
 *                  MICS chip is awake and sending wakeup responses. It was
 *                  awakened via 2.45 GHz wakeup, rather than direct with
 *                  WU_EN.
 */

typedef enum MICS_STATE {
    MICS_SLEEPING,
    MICS_IDLE,
    MICS_SENDING_EMERGENCY,
    MICS_IN_SESSION,
    MICS_SENDING_WAKEUP_RESPONSES
} MICS_STATE;


/**
 * MICS SPI addressing configuration
 */

enum MICS_ADDR_CONFIG {
	MICS_ADDR_UNKNOWN,
	MICS_ADDR_7BIT,
	MICS_ADDR_8BIT
};

/**
 * EXID offset in received MICS message
 */
#define EXID_OFFSET    1

/**
 * Command token offset in received MICS message
 */
#define TOKEN_OFFSET	4

/**
 * Given a received packet, obtain a pointer to the EXID
 * 
 * \param pkt	Points to a command packet
 * \returns		Pointer to the EXID within the packet
 */
#define EXID(pkt)		((EXID_BYTES *) &(pkt)[EXID_OFFSET])

/**
 * Given a received packet, obtain the command token
 * 
 * \param pkt	Points to a command packet
 * \returns		The command token within the packet
 */
#define TOKEN(pkt)		(*(uint16_t *) &(pkt)[TOKEN_OFFSET])


void micsState_Init(void);
void micsState_SetState(MICS_STATE s);
MICS_STATE micsState_GetState(void);
void micsState_SetCommandDone(void);
void micsState_ClearCommandDone(void);
bool micsState_IsCommandDone(void);
bool micsState_Is102(void);
void micsState_Set102(void);
void micsState_Clear102(void);
void micsState_SetRxADCDecisionLevel(uint8_t n);
uint8_t micsState_GetRxADCDecisionLevel(void);
enum MICS_ADDR_CONFIG micsState_GetSPIConfig(void);
void micsState_SetSPIConfig(enum MICS_ADDR_CONFIG spiConf);
	
#endif                          /*MICSMDL_H_ */
