/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: MICS driver internal functions
 *
 ***************************************************************************/

#ifndef MICS_INT_H_
#define MICS_INT_H_

/// \defgroup MICS MICS
/// \ingroup deviceDriverLayer
/// @{

#include "mics.h"

//ZL70102 Design Manual says that direct wakeup can take up to 180 msecs
#define MAX_MS_FOR_MICS_WAKEUP 200

//ZL70102 DESIGN MANUAL says that CRC should take no more than 4 msecs
#define MAX_MS_FOR_CRC_CALC 10

//per ZL70101 Table 51 (copying registers to wakeup stack), this should
// only take 0.2 msecs? (measured 10/08/2010 - took 250 usecs)
// We need to process the irq in this time. Becuase of the interaction of irqs
// this can take a bit. Let 10ms go by before giving up.
#define TIMEOUT_MS_COPY_MICS_REGS  10

#define MICS_RX_BUFFER_SIZE 466 //!< Size of the msp430-side MICS receive buffer. Set to 15 bytes/block * 31 blocks/packet.
extern uint8_t volatile micsReceiveBuffer[MICS_RX_BUFFER_SIZE]; //!< The msp430-side MICS receive buffer.
extern int volatile micsReceiveBufferStart; //!< The index of the first byte in the receive buffer.
extern int volatile micsReceiveBufferEnd; //!< The index of the first byte after the data in the receive buffer. When start == end, there are no bytes in the buffer.
extern bool volatile micsRxNotEmptyInQueue; //!< This flag is used to determine if an RxNotEmpty event should be posted to the queue from the ISR. it is set when data is put in the buffer and cleared when it is removed.

void mics_Config(bool trimListValid, TRIM_LIST const *trimList,const XPG_MICS_ID *id);
int mics_ConfigRevision(void);
void mics_Reset(void);
int mics_Wake(void);
void mics_PrepWakeupStack(void);
void mics_Abort(void);
void mics_TxStart(void);

void mics_TuneTxAntenna(void);

void mics_ConfigBus(void);


/**
 * Send a housekeeping message. Section 8 of the ZL70102 design
 * manual for details.
 *
 * \param aData The data to write to the remote register.
 * \param aAddr The address of the remote register to write to.
 */
void mics_SendHousekeepingMessage(uint8_t aData, uint8_t aAddr);

/*
 * I/O and IRQ configuration functions
 */

void mics_ConfigureIRQ(void);
void mics_ConfigureIO(void);

int mics_enableStrobe();
int mics_disableStrobe();

/// @}

#endif                          /*IPGINITMICSCHIP_H_ */
