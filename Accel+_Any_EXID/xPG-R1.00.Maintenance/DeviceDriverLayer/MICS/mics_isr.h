/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Header file for theISR functions for the MICS device driver.
 * 
 *  This is a submodule of the MICS Conductor.  Handling interrupts from
 *  the ZL7010x is a complex operation, so to keep the code tidy, the
 *  various ISR subroutines are in their own submodule here.
 * 
 ***************************************************************************/

#ifndef MICS_ISR_H_
#define MICS_ISR_H_

#include <stdint.h>
#include <stdbool.h>

#define MICS_ISR_CLEAR_IRQ_RETRYS 128

void micsISR_RadioFail(void);
void micsISR_RxNotEmpty();
void micsISR_RadioReady(void);
void micsISR_LinkReady(void);
void micsISR_CommandDone(void);
void micsISR_ConnectionLost(void);

#endif /*MICS_ISR_H_*/
