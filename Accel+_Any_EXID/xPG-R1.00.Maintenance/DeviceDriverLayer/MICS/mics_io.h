/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: micsConfig (MICS Configurator) module prototypes
 *	
 ***************************************************************************/

#ifndef MICS_IO_H_
#define MICS_IO_H_

#include <stdbool.h>
#include <stdint.h>

/*
 * GPIO and interrupt-related getters, setters, and configurators
 */

void mics_AssertWU_EN(void);
void mics_DeassertWU_EN(void);
bool mics_GetWU_EN(void);
void mics_ConfigureWU_EN(void);

bool mics_IsIRQAsserted(void);

void mics_AssertIRQFlag(void);
void mics_ClearIRQFlag(void);
bool mics_GetIRQFlag(void);

void mics_EnableIRQ(void);
void mics_ConfigureIRQEdge(void);




/*
 * SPI getter and setter prototypes
 */

void mics_SetCTL1(uint8_t d);
void mics_SetCTL0(uint8_t d);
void mics_SetBR(uint16_t d);
void mics_ConfigureSCS(void);



/*
 * Inline SPI getters and setters.
 * 
 * These functions needed to be broken out from  
 * Determine how to handle inline functions. If compiling tests
 * (TEST defined) and using this file for mocking (TEST_INLINE_FUNCTIONS is
 * not defined), then we need to emit prototypes for mocking insted of
 * inline functions.
 * 
 * If we are not compiling tests (TEST not defined) or if the functions
 * themselves are being tested (TEST_INLINE_FUNCTIONS defined), then emit
 * the actual inline functions instead of the prototypes.
 */

#if defined(TEST) && !defined(TEST_INLINE_FUNCTIONS)

void mics_Send(uint8_t d);
uint8_t mics_Recv(void);
uint8_t mics_IsTxReady(void);
uint8_t mics_IsBusy(void);
void mics_AssertSCS(void);
void mics_DeassertSCS(void);

#else

#include "DeviceDriverLayer/Common/msp430f2618.h"
#include "DeviceDriverLayer/Common/bits.h"

void inline mics_Send(uint8_t d)
{
    UCB1TXBUF = d;
}

uint8_t inline mics_Recv(void)
{
    return UCB1RXBUF;
}

uint8_t inline mics_IsBusy(void)
{
    return UCB1STAT & UCBUSY;
}

uint8_t inline mics_IsTxReady(void)
{
    return UC1IFG & UCB1TXIFG;
}

void inline mics_AssertSCS(void)
{
    P7OUT &= ~MICS_CS;
}

void inline mics_DeassertSCS(void)
{
    P7OUT |= MICS_CS;
}

#endif                          // defined(TEST) && !defined(TEST_INLINE_FUNCTIONS)


#endif                          // MICS_IO_H_
