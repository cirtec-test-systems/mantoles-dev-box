/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: ISR functions for the MICS device driver.
 * 
 *  This is a submodule of the MICS Conductor.  Handling interrupts from
 *  the ZL7010x is a complex operation, so to keep the code tidy, the
 *  various ISR subroutines are in their own submodule here.
 * 
 ***************************************************************************/

#include <string.h>
#include "mics_isr.h"

#include "EventQueue.h"
#include "Interrupt.h"
#include "micsspi.h"
#include "mics_int.h"
#include "micsstate.h"
#include "mics_io.h"
#include "zl_bits.h"			// For IRQ_ defines
#include "zl.h"
#include "delay.h"
#include "MultiChannelTimer.h"
//#include "system_events.h"

/**
 * The RADIO FAIL irq indicates that the radio did not initialize properly
 * when waking up, either due to an error in the CRC check of the setup
 * registers or due to the inability to achieve the synthesizer lock.
 *
 * In the case of the CRC, we can limp along and continue the set-up of the
 * chip.  In the case of the synth lock fail, the radio will not be
 * operational.
 *
 * \todo TODO need to determine what to do in either case.
 *
 * \warning WARNING: Runs at interrupt level.
 */
void micsISR_RadioFail(void)
{
	uint16_t irqAuxStat;

    irqAuxStat = zl_GetIrqAuxStatus();

    /*
     * if CRC error interrupt, handle it. Normally, an implant application
     * should restore the relevant register settings in the MICS chip from
     * non-volatile memory, then invoke a function to copy the registers to
     * save the registers in the ZL7010x's wakeup stack and calculate the CRC.
     * For now, just tell the chip so skip the CRC.  Note that the implant can
     * be power-cycled to clear the CRC error and return the MICS chip to the
     * default register settings (leave it off for a few moments to make sure
     * power is completely drained.)
     *
     * TODO Need to handle CRC error?
     */
    if (irqAuxStat & IRQ_CRCERR)
    {
        //set-up to skip the CRC check for now
        zl_SkipCRC();
        zl_AckIrqAuxStatus(IRQ_CRCERR);
    }

    //if synth lock failed, clear the bit
    if (irqAuxStat & IRQ_SYNTHLOCKFAIL)
    {
	    //TODO what else can we do here?  Log error?
        zl_AckIrqAuxStatus(IRQ_SYNTHLOCKFAIL);
    }

    // Douse the TST_TRIG LED
    setTST_TRIG_LOW();

    // Cancel the error polling timer
    swTimerCancel(SWTMR_POLL_LINKSTATUS);
}


/**
 * The RX NOT EMPTY irq is issued when the mics chip has one or more blocks
 * of received data to read.
 *
 * \warning WARNING: Runs at interrupt level
 */
void micsISR_RxNotEmpty()
{
	int bytesToRead;
	int nextidx;

	while((bytesToRead = zl_GetBlocksToRead()) > 0)
	{
		bytesToRead *= MICS_BLOCK_SIZE; //The chip reports "blocks in the buffer"

		mics_AssertSCS();
		// Start the read from the receive buffer
		mics_Send(MICS_READ | TXRXBUFF);
		while (!mics_IsTxReady()) {}	//Wait until transmit is ready

		for (;bytesToRead > 0; bytesToRead--)
		{
			nextidx = (micsReceiveBufferEnd + 1) %  MICS_RX_BUFFER_SIZE;

			if(nextidx == micsReceiveBufferStart)
			{
				//perform flow control.

				// Added delay to ensure there is enough time between the clock going low, and cs going high (Tcch).
				DELAY_US(1);

				//de-select the mics chip on the SPI bus
				mics_DeassertSCS();

				//disable the interrupt so it doesn't recur. We will re-enable it when the buffer empties a bit.
				zl_DisableRxNotEmpty();

				if(micsRxNotEmptyInQueue == false)
				{
					//Notify anyone interested that the data is ready (notably the connection state machine).
					micsRxNotEmptyInQueue = true;
					dispPutSimpleEvent(E_MICS_INT_RX_NOT_EMPTY);
				}

				return;
			}

			mics_Send(0);
			while (mics_IsBusy()) {}	//Wait until MICS is not busy

			micsReceiveBuffer[micsReceiveBufferEnd] = mics_Recv();
			micsReceiveBufferEnd = nextidx;
		}

	    // Added delay to ensure there is enough time between the clock going low, and cs going high (Tcch).
	    DELAY_US(1);

		//de-select the mics chip on the SPI bus
		mics_DeassertSCS();

	    // And delay after deasserting CS to ensure that there is enough time between messages (Tcwh).
	    DELAY_US(1);
	}

	//Notify anyone interested that the data is ready (notably the connection state machine).
	if(micsRxNotEmptyInQueue == false)
	{
		micsRxNotEmptyInQueue = true;
		dispPutSimpleEvent(E_MICS_INT_RX_NOT_EMPTY);
	}
}


/**
 * Note if the MICS chip asserted IRQ_RADIOREADY at the end of its wakeup
 * sequence, the watchdog timer will be enabled (default wakeup setting).
 * In that case, the watchdog is left enabled so it will timeout in 4.37
 * seconds and assert IRQ_WDOG (see conn lost isr()), which will set the 
 * TASK_MICS_ABORT flag to tell the background job to put the mics chip
 * to sleep.
 * 
 * IRQ_RADIOREADY should only occur when the mics chip wakes up, because the
 * ABORT_LINK command is only used to put the chip to sleep.  Thus, 
 * ABORT_LINK should never cause the MICS chip to assert IRQ_RADIOREADY.
 *
 * \warning WARNING: Runs at interrupt level.
 */
void micsISR_RadioReady(void)
{
	uint8_t macCtrl;

    dispPutSimpleEvent(E_MICS_INT_RADIO_READY);

    // If we got this int because of a reset, we need to clear the reset bit.
    // On a wakeup, this register is cleared, but on a reset the value remains.
    macCtrl = micsSPI_Get(MAC_CTRL);
    micsSPI_Put(MAC_CTRL, macCtrl & ~ABORT_LINK);

    //if the chip was sleeping, IRQ_RADIOREADY due to wakeup, not abort link
    if (micsState_GetState() != MICS_SLEEPING)
    {
    	return;
    }
    
    /*
     * Initialize registers that need to be set every time the MICS chip
     * wakes up (because its "copy registers' command doesn't save them in
     * its wakeup stack):
     * 
     * IRQ_ENABLESET1 & IRQ_ENABLESET2:
     *    Enable MICS interrupts needed by this application.  Note the MICS
     *    chip enables the following interrupts by default: IRQ_RADIOFAIL,
     *    IRQ_RADIOREADY, IRQ_LINKREADY, IRQ_CLOST, IRQ_HKUSERDATA, 
     *    IRQ_HKUSERSTATUS and IRQ_HKREMOTEDONE.  These don't have to be
     *    re-enabled here.
     *
     * The IRQ_HKREMOTEDONE, IRQ_HKUSERSTATUS, and IRQ_HKUSERDATA interrupts
     * are not used by this application, so disable the interrupts.
     */
    zl_EnableCommandDone();
    zl_DisableHkRemoteDone();
    zl_DisableHkUserStatus();
    zl_DisableHkUserData();

    // TODO: This fails the SPI write because the value read at startup is invalid.
    // We only use the default (the value read at startup) at this time, so I am
    // disabling this call until we know how to handle this. --JA
    /*
     * Load the RXIFADCDECLEV register.  Zarlink recommends setting this register
     * during every wakeup because it needs to be trimmed at factory and is not
     * preserved in the wakeup registers or wakeup stack.
     */ 
    //zl_SetRxADCDecisionLevel(micsState_GetRxADCDecisionLevel());

    //see if radio ready was achieved by direct wakeup
    if (mics_GetWU_EN())
    {
    	//de-assert WU_EN to allow the chip to sleep when complete.
    	mics_DeassertWU_EN();

    	mics_enableStrobe();

        //update mics state after this direct wakeup
        micsState_SetState(MICS_IDLE);
    }
    else
    {
        //achieved radio ready due to 2.45 GHz wakeup
        //set state to 'sending wakeup responses (after 2.45 GHz wakeup
        // the mics chip is automatically sending responses
        micsState_SetState(MICS_SENDING_WAKEUP_RESPONSES);

        zl_EnableHkWrites_102();

        //TODO If the trim list contains entries that need to be written when the system comes out of sleep, do that here.
    }

}


/**
 * The LINK READY irq is issued when a base station has establish a mics
 * link with the ipg.
 *
 * \warning WARNING: Runs at interrupt level.
 */
void micsISR_LinkReady(void)
{
    EVENT pollEvent;

	/*
     * Enable MICS watchdog timer in case it wasn't enabled while starting
     * session.  This is needed if the session is lost, the MICS watchdog
     * will timeout and assert IRQ_WDOG to ensure the MICS channel won't be
     * occupied for more than 5 seconds (MICS standard).
     */
    zl_WatchdogOn();

    // Enable the IRQ that alerts to data received
    zl_EnableRxNotEmpty();

    // Set the new mics state
    micsState_SetState(MICS_IN_SESSION);

    // Notify upper layers that a session is open.
	dispPutSimpleEvent(E_MICS_INT_LINK_READY);

	// Enable the error polling timer
    pollEvent.eventID = E_POLL_LINK_ERRORS;
    swTimerSet(SWTMR_POLL_LINKSTATUS, 250, pollEvent, true);
}



/**
 * the command done irq is issued when an automatic command completes.
 * This can be a calibration, saving registers to the wakeup stack, 
 * calculation of the CRC, ...
 * 
 * \warning WARNING: Runs at interrupt level.
 */
void micsISR_CommandDone(void)
{
	micsState_SetCommandDone();
}


/**
 * The connection lost irq tells the ipg that the mics session has ended.
 * It can be caused by either an HK abort link write from the base station
 * or due to the mics chip watch dog timing out.  In either case, we set
 * a flag for the background job to shut down the mics chip.
 * 
 * Also, tally the reason for the connection being lost.  
 * 
 * \warning WARNING: Runs at interrupt level.
 */
void micsISR_ConnectionLost(void)
{
    uint16_t irqAuxStat;
    uint16_t retryCount;

    irqAuxStat = zl_GetIrqAuxStatus();

    //if abort link received via housekeeping
    if (irqAuxStat & IRQ_HKABORTLINK)
    {
        //clear bit in auxiliary status
        // (see ZL70101 Design Manual, rev 1.1 Table 43)
        zl_AckIrqAuxStatus(IRQ_HKABORTLINK);
        irqAuxStat = zl_GetIrqAuxStatus();

        for(retryCount=0; (irqAuxStat & IRQ_HKABORTLINK) && retryCount<MICS_ISR_CLEAR_IRQ_RETRYS; retryCount++)
        {
        	DELAY_US(10);
            zl_AckIrqAuxStatus(IRQ_HKABORTLINK);
            irqAuxStat = zl_GetIrqAuxStatus();
        }

		#ifdef VERBOSE_MICS_LOGGING
        	dispPutLogEvent(E_LOG_MICS_HK_ABORT_LINK, 0);
		#endif
    }

    /* If MICS watchdog timer, clear it (clears interrupt source), then clear
     * the bit in the auxiliary status.  Clearing the watchdog also ensures it
     * won't finish counting to ~5 seconds and force the MICS chip to sleep
     * without the firmware knowing about it (if it did, the firmware would
     * think the chip was awake when it was actually sleeping, which could
     * cause problems).
     *
     * Note if DEBUG is defined, the watchdog is also disabled.  That way, if
     * you're using a debugger, the watchdog won't force the chip back to
     * sleep while you're stepping through code.  If DEBUG isn't defined, the
     * watchdog is left enabled.  That way, if the micro-controller gets lost
     * and never puts the MICS chip back to sleep, it will go back to sleep on
     * its own (to help ensure it won't drain the battery).
     */

    //if watchdog timer, clear bit in auxiliary status
    if (irqAuxStat & IRQ_WDOG)
    {
    	zl_WatchdogClear();

        // (see ZL70101 Design Manual, rev 1.1 Table 43)
        zl_AckIrqAuxStatus(IRQ_WDOG);
        irqAuxStat = zl_GetIrqAuxStatus();

        for(retryCount=0; (irqAuxStat & IRQ_WDOG) && retryCount<MICS_ISR_CLEAR_IRQ_RETRYS; retryCount++)
        {
        	DELAY_US(10);
        	zl_WatchdogClear();
            zl_AckIrqAuxStatus(IRQ_WDOG);
            irqAuxStat = zl_GetIrqAuxStatus();
        }

        #ifdef VERBOSE_MICS_LOGGING
        	dispPutLogEvent(E_LOG_MICS_WATCHDOG_TIMEOUT, 0);
		#endif
    }


	dispPutSimpleEvent(E_MICS_INT_CLOST);

    /*
     * Send E_MICS_ABORT to put the MICS chip back to sleep. micsSleep is
     * not called directly because that causes race conditions when the
     * firmware is doing something with the MICS chip when IRQ_CLOST occurs.
     */
    dispPutSimpleEvent(E_MICS_ABORT);

    // Douse the TST_TRIG LED on connection lost
    setTST_TRIG_LOW();

    // Cancel the error polling timer
    swTimerCancel(SWTMR_POLL_LINKSTATUS);
}


