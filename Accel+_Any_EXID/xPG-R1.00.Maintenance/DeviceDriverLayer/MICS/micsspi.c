/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: micsSPI (MICS SPI) module
 *	
 ***************************************************************************/

#include "micsspi.h"

#include "mics_io.h"
#include "micsstate.h"
#include "Interrupt.h"
#include "DeviceDriverLayer/Common/msp430f2618.h"
#include "delay.h"
#include "zl_bits.h"
#include "DeviceDriverLayer/Common/bits.h"

#include "log.h"
#include "watchdog.h"
#include "EventQueue.h"

/**
 * micsSPI_Configure - initialize peripherals related to the Mics SPI interface.
 * 
 * On the MSP430 side, refer to the IPG schematic, the MSP430X2XX Family User's
 * Guide (slau144e.pdf) and the MSP430F2618 data sheet for information about
 * the USCI.
 * 
 * On the Zarlink side, refer to the ZL70101 DESIGN MANUAL (Dec 2007). Section
 * 6.2, Serial Peripheral Interface describes the SPI requirements for the 
 * chip.  This describes the clocking requirements and polarities, which 
 * determine the requirements for the set-up of the USCI registers in this
 * function.  
 * 
 * Note that the Zarlink ZL70101 SPI interface can run at up to 4 MHz.  In the 
 * IPG, we're using the SMCLK as the source for the USCI.  The SMCLK is also
 * used as the stim clock, so normally runs at 1 MHz. IF THE SMCLK FREQUENCY
 * IS CHANGED, MAY NEED TO CHANGE THE SET-UP FOR THE SPI.  The ZL70101 supports
 * SPI clocking speed of up to 4 MHz, but requires special set-up to do higher
 * than 2 MHz.  The default set-up is for maximum of 2 MHz.  Since we're 
 * running at 1 MHz, no need to change the set-up.  (See ZL70101 DESIGN MANUAL,
 * section 6.2.71 Interface Mode Register (reg_interface_mode).)
 */

void micsSPI_Configure(void)
{
    // Set up MICS SPI chip select
    mics_DeassertSCS();
    mics_ConfigureSCS();

    /* Select SPI function for USCI port pins.  Note in SPI mode, the
     * corresponding bits in SPI_USART_PDIR are ignored and the direction
     * of each pin is determined by the SPI configuration (see the I/O pin
     * schematics in the device-specific data sheet for the MSP430.)
     */
    P5SEL |= MICS_SDI | MICS_SDO | MICS_SCK;

    /* 
     * The MSP430 manual suggest that SWRST should be set before 
     * initializing or re-configuring the USART for SPI mode.
     */
    mics_SetCTL1(UCSWRST);

    /* Set USCI control:
     * 
     * - UCLK phase = captured on 1st edge, changed on 2nd. (UCCKPH = 1)
     * - UCLK polarity = inactive state is low.             (UCCKPL = 0)
     * - MSB first (UCMSB=1).
     * - Character length 8 bits (UC7BIT = 0).
     * - Master Mode Select (UCMST = 1).
     * - USCI Mode 3-pin SPI (UCMODE1, UCMODE0 = 0).
     * - Synchronous Mode Enable (UCSYNC = 1).
     */
    mics_SetCTL0(UCCKPH | UCMSB | UCMST | UCSYNC);

    /* continue USCI control: 
     * - select SMCLK as source
     * - keep in reset during set-up (UCSWRST = 1)
     */
    mics_SetCTL1(UCSSEL_2 | UCSWRST);

    // Set USART bit-rate control.
    mics_SetBR(1);

    //finally, release the USART from reset
    mics_SetCTL1(UCSSEL_2);

}

/**
 * Shut down SPI communications and configure the port pins to
 * prevent parasitic powering.
 */

void micsSPI_Suspend(void)
{
	P5OUT &= ~(MICS_SDI | MICS_SDO | MICS_SCK);	// Prepare to output 0 on the SPI pins
	P5DIR |= MICS_SDI | MICS_SDO | MICS_SCK;	// Set to output mode
	P5SEL &= ~(MICS_SDI | MICS_SDO | MICS_SCK);	// Change pins to GPIO

	//set CS low.
	P7OUT &= ~(MICS_CS);	// Prepare to output 0 on the SPI CS pin
	P7DIR |= MICS_CS;	// Set to output mode
	P7SEL &= ~(MICS_CS);	// Change pin to GPIO

	mics_SetCTL1(UCSWRST);						// Stop the SPI peripheral
}

static inline void waitForTxReady(void)
{
    while (!mics_IsTxReady())
    {
    }
}

static inline void waitWhileBusy(void)
{
    while (mics_IsBusy())
    {
    }
}


/**
 * Performs a read on the MICS chip over SPI without disabling interrupts first.
 *
 * \param registerAddress The address of the register to read.
 * \return The value of the register.
 */
static inline uint8_t micsSPI_GetWithoutDisableInturrupt(uint8_t registerAddress)
{
    uint8_t micsRegReadData;

	//select mics chip on SPI bus
	mics_AssertSCS();

	//transmit mics chip register address with read bit set
	mics_Send(MICS_READ | registerAddress);

	//wait until address byte is moved to TX shift register
	waitForTxReady();

	/*
	 * Handle 8-bit (page 2) addresses
	 */

	if ((registerAddress & MICS_PAGE_2) != 0)
	{
		mics_Send(MICS_PAGE_2 | MICS_M0);
		waitForTxReady();
	}

	/*
	 * Write dummy byte to SPI TX buffer, then wait for SPI interface to finish
	 * clocking the dummy byte.  The mics chip will be clocking in the register
	 * data at the same time.
	 */
	mics_Send(0);

	//wait until dummy byte is transmitted so return byte is clocked in
	waitWhileBusy();

    // Added delay to ensure there is enough time between the clock going low, and cs going high (Tcch).
    DELAY_US(1);

	//de-select the mics chip on the SPI bus
	mics_DeassertSCS();

    // And delay after deasserting CS to ensure that there is enough time between messages (Tcwh).
    DELAY_US(1);

	//save the register data read
	micsRegReadData = mics_Recv();

	return micsRegReadData;
}


/**
 * Writes to a register on the MICS chip through SPI. This method does not disable
 * interrupts, nor does it perform a readback check.
 *
 * \param registerAddress The address to write to.
 * \param registerValue The value to write to the address.
 */
static inline void micsSPI_PutWithoutChecksOrDisableInterrupt(uint8_t registerAddress, uint8_t registerValue)
{
	//select mics chip on SPI bus
	    mics_AssertSCS();

	    //write address byte to SPI transmit buffer
	    mics_Send(registerAddress & ~MICS_PAGE_2);
	    waitForTxReady();

		if (micsState_GetSPIConfig() == MICS_ADDR_8BIT)
		{
			// If we're in 8-bit mode, write the second byte.
		    mics_Send(registerAddress & MICS_PAGE_2);
		    waitForTxReady();
		}

	    //write desired value to SPI transmit buffer
	    mics_Send((uint8_t) registerValue);

	    //wait for SPI to be done
	    waitWhileBusy();

	    // Added delay to ensure there is enough time between the clock going low, and cs going high (Tcch).
	    DELAY_US(1);

	    //de-select the mics chip on the SPI bus
	    mics_DeassertSCS();

	    // And delay after deasserting CS to ensure that there is enough time between messages (Tcwh).
	    DELAY_US(1);
}

/**
 * read a register on the MICS chip via SPI
 *
 * This function automatically handles page 2 addresses (0x80 - 0xff)
 * on the ZL70102.  If ZL70101 code ever needs a page 2 address, it
 * must explicitly write the paging bit.
 *  
 * Note: the function blocks interrupts around the actual read, so
 * no other ISR can access the SPI bus during the read.
 * 
 * \param registerAddress register address in MICS chip (ZL70101)
 * 
 * \return value read from register in MICS chip
 */

uint8_t micsSPI_Get(uint8_t registerAddress)
{
    uint16_t originalGIE;
    uint8_t micsRegReadData;

    //save current global interrupt enable (GIE) setting, then disable all
    // interrupts so no other ISR will try to use the SPI bus.
    originalGIE = interruptDisable();

    micsRegReadData = micsSPI_GetWithoutDisableInturrupt(registerAddress);

    //if interrupts were enabled, re-enable them
    interruptRestore(originalGIE);

    //return the value read from the register
    return micsRegReadData;
}


/**
 *
 * Write a register on the MICS chip via SPI
 *
 * Note: the function blocks interrupts around the actual write, so
 * no other ISR can access the SPI bus during the write.
 *
 * \param registerAddress register address in MICS chip (ZL7010x)
 * \param registerValue   value to write to register
 */
void micsSPI_Put(uint8_t registerAddress, uint8_t registerValue)
{
    uint16_t originalGIE;
    uint8_t readValue;
    uint8_t retryCount = 3;

    //save current global interrupt enable (GIE) setting, then disable all
    // interrupts so no other ISR will try to use the SPI bus.
    originalGIE = interruptDisable();

    do
    {
    	retryCount--;
    	micsSPI_PutWithoutChecksOrDisableInterrupt(registerAddress, registerValue);
    	readValue = micsSPI_GetWithoutDisableInturrupt(registerAddress);
    } while(registerValue != readValue && retryCount > 0);


    if(registerValue != readValue)
    {
    	logNormal(E_LOG_MICS_SPIFAIL,
    			((uint32_t)registerAddress) << 16
    			| ((uint32_t)registerValue) << 8
    			| ((uint32_t)readValue));

    	//Do a hard-reset of the MICS chip
		micsShutdown();
    	dispPutSimpleEvent(E_MICS_FULL_RESET);
    }

    //if interrupts were enabled, re-enable them
    interruptRestore(originalGIE);

}


/**
 * 
 * Write a register on the MICS chip via SPI. Do not perform a readback check.
 *
 * This function serves the same purpose as micsSPI_Put, but does not perform
 * the readback check. Use this function when reading the register after writing
 * is not expected to return the value written.
 * 
 * \note the function blocks interrupts around the actual write, so
 * no other ISR can access the SPI bus during the write.
 * 
 * \param registerAddress register address in MICS chip (ZL7010x)
 * \param registerValue   value to write to register
 */
void micsSPI_PutWithoutCheck(uint8_t registerAddress, uint8_t registerValue)
{
    uint16_t originalGIE;

    //save current global interrupt enable (GIE) setting, then disable all
    // interrupts so no other ISR will try to use the SPI bus.
    originalGIE = interruptDisable();

    micsSPI_PutWithoutChecksOrDisableInterrupt(registerAddress, registerValue);

    //if interrupts were enabled, re-enable them
    interruptRestore(originalGIE);
}
