/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: micsMdl (MICS Model) module
 *
 ***************************************************************************/

#include "micsstate.h"
#include "mics.h"
#include "Common/Protocol/protocol.h"


static volatile MICS_STATE state;
static volatile bool commandDone;
static volatile bool isZL70102;
static uint8_t adcDecLev;
static volatile enum MICS_ADDR_CONFIG spiConf;

/**
 * Initialize the MICS Model class
 */

void micsState_Init(void)
{
    //zero-out flags
    micsState_ClearCommandDone();
    micsState_Clear102();

    // The MICS SPI address width is unknown
    micsState_SetSPIConfig(MICS_ADDR_UNKNOWN);

    //finish up by setting initial mics state to sleeping
    micsState_SetState(MICS_SLEEPING);
}

void micsState_SetState(MICS_STATE s)
{
    state = s;
}

MICS_STATE micsState_GetState(void)
{
    return state;
}


void micsState_SetCommandDone(void)
{
	commandDone = true;
}

void micsState_ClearCommandDone(void)
{
	commandDone = false;
}

bool micsState_IsCommandDone(void)
{
	return commandDone;
}

bool micsState_Is102(void)
{
	return isZL70102;
}

void micsState_Set102(void)
{
	isZL70102 = true;
}

void micsState_Clear102(void)
{
	isZL70102 = false;
}

void micsState_SetRxADCDecisionLevel(uint8_t n)
{
	adcDecLev = n;
}

uint8_t micsState_GetRxADCDecisionLevel(void)
{
	return adcDecLev;
}

enum MICS_ADDR_CONFIG micsState_GetSPIConfig(void)
{
	return spiConf;
}

void micsState_SetSPIConfig(enum MICS_ADDR_CONFIG conf)
{
	spiConf = conf;
}
