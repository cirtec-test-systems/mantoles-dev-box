/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: MICS driver (internal) calibration functions
 *
 ***************************************************************************/

#ifndef MICSCAL_H_
#define MICSCAL_H_

#include <stdint.h>

//original Zarlink dev kit code did 20 cycles of 2.45 GHz zero detect cal
#define CAL_245_GHZ_CYCLES 20

//ZL70101 Design Manual Table 67 says this cal is 735 usecs max
#define TIMEOUT_CAL_245_GHZ_ZD_MS 2

//ZL70101 Design Manual Table 67 indicates longest cal (except 24MHz osc)
//  is 17.3 msecs
#define TIMEOUT_GENERIC_CAL_MS 25

// ZL70102 Design Manual Table 80 indicates present-channel antenna tuning takes 75 ms with extended algorithm
#define TIMEOUT_ANTENNA_TUNE_MS 1000

#endif                          /*IPGMICSCAL_H_ */
