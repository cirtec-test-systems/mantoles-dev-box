/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: MICS driver internal functions
 * 
 ***************************************************************************/

#include <stdbool.h>
#include <string.h>

#include "mics_int.h"

#include "EventQueue.h"
#include "Interrupt.h"
#include "mics_cal.h"
#include "mics_mac.h"
#include "mics_isr.h"
#include "mics_io.h"
#include "micsstate.h"
#include "zl.h"
#include "zl_bits.h"
#include "system.h"
#include "timeout.h"
#include "Common/protocol/protocol.h"
#include "mics.h"
#include "micsspi.h"

#ifdef VERBOSE_MICS_LOGGING
#include "log.h"
#include "log_events.h"
#endif

//#define DEBUG_MICS
#if defined(DEBUG_MICS) || defined(PRINT_OUTPUT)
#include <stdio.h>
#endif

//#define DEBUG_ANT_TUNE
#ifdef TEST
#undef DEBUG_ANT_TUNE		// Don't enable the debugging printfs during TDD unit tests
#endif
#ifdef DEBUG_ANT_TUNE
#include <stdio.h>
#endif

uint8_t volatile micsReceiveBuffer[MICS_RX_BUFFER_SIZE]; //!< The msp430-side MICS receive buffer.
int volatile micsReceiveBufferStart = 0; //!< The index of the first byte in the receive buffer.
int volatile micsReceiveBufferEnd = 0; //!< The index of the first byte after the data in the receive buffer. When start == end, there are no bytes in the buffer.
bool volatile micsRxNotEmptyInQueue = false; //!< This flag is used to determine if an RxNotEmpty event should be posted to the queue from the ISR. it is set when data is put in the buffer and cleared when it is removed.

/**
 * mics_ConfigRevision
 * 
 * Read the ZL7010x revision.  Set the micsMdl 70102 flag and enable 8 bit
 * addresses on SPI write if this is a ZL70102.
 * 
 * The only revisions supported at this time is the ZL70102.
 * 
 * \returns	0 if the MICS chip self-identified as a '102, otherwise -1.
 */

int mics_ConfigRevision(void)
{
	uint8_t rev = zl_GetRevision();

	switch (rev)
	{
		case ZL70102:
			zl_ConfigInterfaceMode_102();		// Switch to 8-bit mode
			micsState_SetSPIConfig(MICS_ADDR_8BIT);
			micsState_Set102();
			break;
		default:
#ifdef DEBUG_MICS
			printf("mics_ConfigRevision: unknown revision %d\n", rev);
#endif
			return -1;
	}
	
	return 0;
}


/**
 * mics_Config
 *  
 * Configures the MICS chip through a series of register writes 
 * (via SPI) to prepare the chip for communication with external
 * devices.
 * 
 * This was originally based on a similar function in code provided
 * with the Zarlink Development Kit, in a function called ConfigMics.
 * 
 * The function also performs an automatic calibration for the MICS
 * chip for the 2.45 GHz zero detect level.
 */

void mics_Config(bool trimListValid, TRIM_LIST const *trimList,const XPG_MICS_ID *id)
{
	zl_Init_102();
	// Cal 2 MHz oscillator

	zl_SetMacMicsID(id);
    zl_SetWakeupMicsID(id);

    if (trimListValid)
    {
    	zl_ApplyTrimList(trimList);
    }
    else
    {
    	// Signal an error if corrupted
    	dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
    }
}



/**
 * Reset the ZL7010x.
 */

void mics_Reset(void)
{
	// Ensure MICS chip interrupt won't assert an int until chip re-awakened
    zl_DisableAll();

    // Attempt full chip reset via register write
    zl_Reset();

    // After a reset, figure out the right bus mode all over again
    micsState_SetSPIConfig(MICS_ADDR_UNKNOWN);

    // Force sleep state
    micsState_SetState(MICS_SLEEPING);
}	


/**
 * mics_Wake - normally performed when coming out of storage
 * mode, uses the 'direct wakeup' technique (WU_EN signal) to try to wakeup
 * the MICS chip (Zarlink ZL70102) so that it can be further configured for
 * normal communications.
 * 
 * The normal sequence is that the IRQ_RADIOREADY MICS interrupt will occur
 * following the WU_EN signal and will put the MICS chip state into the
 * 'MICS_IDLE' state. 
 * 
 * There are several abnormal cases:
 *   - if the MICS chip was already awake, the interrupt won't fire, so
 *     this function will time-out.  (The remedy is basically to reset the
 *     chip and start over.)  
 *   - the MICS chip itself can flag a couple of errors during the wake-up,
 *     such as an internal CRC error or its radio won't function (synth lock)
 * 
 * Refer the Zarlink ZL7010x Design Manual for further information on the
 * direct wake-up technique.
 * 
 * Note: it is likely that this sequence will be somewhat modified when we
 * have the power asic, since the MSP430 handles powering of the MICS chip
 * through the power asic.
 */

int mics_Wake(void)
{
    TIMEOUT timeout;
    int result;

    mics_disableStrobe();

    // assert the wakeup enable signal to the mics chip
    mics_AssertWU_EN();

	result = -1;
    for (timeout = timeoutSet(MS(MAX_MS_FOR_MICS_WAKEUP)); !timeoutIsExpired(timeout); )
    {
        //if the state changed, we've successfully powered up the chip
        if (micsState_GetState() != MICS_SLEEPING)
        {
            result = 0;
            break;
        }
    }

    return result;
}


/**
 * mics_PrepWakeupStack()
 *  
 * Issues command to MICS chip to perform a recalculation of its
 * CRC for the internal registers, then monitors for completion of
 * the recalculation.  
 * 
 * The Zarlink ZL7010x Design Manual suggests allowing 4 msecs
 * for this to complete.  Measured on 10/11/2010 and appeared to
 * take just 1.5 msecs?
 *
 * Refer to Zarlink ZL7010x Design Manual for information on the
 * sequence to perform this calculation. See section 7.2 and the 
 * description of the reg_wakeup_crcctrl register.
 * 
 * TODO need to log error if doesn't complete in time.
 */

void mics_PrepWakeupStack(void)
{
    TIMEOUT timeout;

    //tell mics chip to copy registers to wakeup stack
    mics_MacCmd(zl_CopyRegs, TIMEOUT_MS_COPY_MICS_REGS);

    //trigger a recalculation of the mics registers CRC 
    zl_CalcCRC();

    //log start msec time
    timeout = timeoutSet(MS(MAX_MS_FOR_CRC_CALC));

    //wait until CRCCTRL register indicates calculation has completed
    while (!zl_IsCRCDone())
    {
        //see if timed out waiting for calculation to complete
        if (timeoutIsExpired(timeout))
        {
            //TODO log some type of error to indicate timed out?
#ifdef PRINT_OUTPUT
        	printf("mics_PrepWakeupStack Error\n");
#endif
            break;              //quit waiting for CRC recalculation to complete
        }
    }

    //clear the CALC_CRC bit in CRC control (not self-clearing)
    zl_EndCalcCRC();
}



/**
 * mics_Abort()
 *  
 * Shuts down the mics chip after initialization or after a mics
 * session.
 */

void mics_Abort(void)
{
#ifdef VERBOSE_MICS_LOGGING
	logNormal(E_LOG_MICS_ABORT,0);
#endif

    //put mics chip to sleep (will also flush TX and RX buffers on chip)
	micsSleep();

	//clear local RX buffer as well
	micsReceiveBufferStart = micsReceiveBufferEnd = 0;
}


/**
 * mics_TxStart()
 *
 * Calls the right zl_TxStart_102 function.
 */
 
void mics_TxStart(void)
{
	zl_TxStart_102();
}

/**
 * Configure the MICS interrupt to latch on the rising edge of
 * the MICS interrupt (MICS_IRQ signal), then initialize the MICS
 * interrupt flag (if we already missed the rising edge, set the
 * flag) and enable the MICS interrupt.
 *
 * \note The MICS chip enables IRQ_RADIOFAIL, IRQ_RADIOREADY, IRQ_LINKREADY,
 *  	 IRQ_CLOST, IRQ_HKUSERDATA, IRQ_HKUSERSTATUS, and IRQ_HRREMOTEDONE by
 * 		 default.
 *
 * \note When the IRQ_RADIOREADY interrupt occurs, micsISR_RadioReady() will
 * 		 be called to complete initialization that must be done every time the
 * 		 MICS chip wakes up.
 */


void mics_ConfigureIRQ(void)
{
    // Configure interrupt for rising edge
    mics_ConfigureIRQEdge();

    // Clear the mics interrupt flag
    mics_ClearIRQFlag();

    // Paranoia: See if int signal is still asserted.  If so, force interrupt.
    if (mics_IsIRQAsserted())
    {
        mics_AssertIRQFlag();
    }
    //enable the interrupt
    mics_EnableIRQ();
}


/**
 * Initializes the control signals between the MSP430 and the
 * Zarlink ZL7010x MICS chip.
 *
 * On the MSP430 side, refer to the IPG schematic, the MSP430X2XX Family User's
 * Guide (slau144e.pdf) and the MSP430F2618 data sheet for information about
 * the setting up regular digital I/O ports.
 *
 * On the Zarlink side, refer to the ZL70102 DESIGN MANUAL (JAN 2011).
 * It describes the use of the wakeup enable (WU_EN) signal for direct
 * wakeup of the chip.
 */

void mics_ConfigureIO(void)
{
    // Initialize control of the wake-up enable signal
    mics_DeassertWU_EN();
    mics_ConfigureWU_EN();
}


/**
 * Tune the TX, MATCH1, and MATCH2 tuning capacitors.
 */
void mics_TuneTxAntenna(void)
{
	if (micsState_GetState() == MICS_SLEEPING)
	{
		// Can't tune up if the chip isn't awake
		return;
	}

	// Transmit tuning (TX, MATCH1, MATCH2)
	zl_ConfigAntTuneTx();
	if (isIPG())
	{
		zl_ConfigAntTuneGain_IPG();
	}
	else
	{
		zl_ConfigAntTuneGain_EPG();
	}

	zl_CalSelect_102(CAL_400_MHZ_TX_102);
	mics_MacCmd(zl_PerformCals, TIMEOUT_ANTENNA_TUNE_MS);

	// Receive tuning (RX)
	// Just make the TX and RX pins have the same capacitance in both RX and TX modes
	zl_SetTxCapRxMode_102(zl_GetTxCapTxMode_102());

#ifdef DEBUG_ANT_TUNE
	printf("caps: chan=%d TX=%02x MATCH1=%02x MATCH2=%02x\n",
			zl_GetWakeupChannel(), zl_GetTxCapTxMode_102(), zl_GetMatch1_102(), zl_GetMatch2_102());
	printf("levels: final=%d max=%0d\n",
			zl_GetTuneResult_102(), zl_GetTuneResultMax_102());
#endif
}


/**
 * Configures the bus into a known state, starting with an unknown ZL7010x
 * variant and an unknown bus mode (7-bit or 8-bit addressing).
 *
 * \warning WARNING: Runs at interrupt level
 */
void mics_ConfigBus(void)
{
	if (micsState_GetSPIConfig() == MICS_ADDR_UNKNOWN)
	{
		if (zl_Is8Bit_102())
		{
			micsState_SetSPIConfig(MICS_ADDR_8BIT);
		}
		else
		{
			micsState_SetSPIConfig(MICS_ADDR_7BIT);
		}
	}
}


void mics_SendHousekeepingMessage(uint8_t aData, uint8_t aAddr)
{
	micsSPI_Put(HK_TXDATA, aData);
	micsSPI_PutWithoutCheck(HK_TXADDR, aAddr);
}

static bool strobeEnabled = false;

int mics_enableStrobe()
{
	if(!strobeEnabled)
	{
		strobeEnabled = true;
		return pwrEnableStrobe();
	}
	else
	{
		return 0;
	}
}

int mics_disableStrobe()
{
	if(strobeEnabled)
	{
		strobeEnabled = false;
		return pwrDisableStrobe();
	}
	else
	{
		return 0;
	}
}
