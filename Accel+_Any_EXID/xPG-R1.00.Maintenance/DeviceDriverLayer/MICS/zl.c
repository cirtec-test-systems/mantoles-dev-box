/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: zl (ZL7010x accessor) module
 *	
 ***************************************************************************/

#include "zl.h"
#include "mics_io.h"
#include "micsspi.h"
#include "zl_bits.h"
#include "Common/Protocol/trim_list.h"
#include "Common/Protocol/protocol.h"
#include "delay.h"

// Define to turn on debugging for the power save timer.
// Causes the ZL70102 PO1 pin to output TX_MODE_B and the PO2 ping to
// output RX_MODE.  Monitor these pins with an oscilloscope.
//#define DEBUG_PWRSAVE

/**
 * Register initialization list for the ZL70102
 */

static const uint8_t init102[] = {
	// RESEND_TIME?
#ifdef EPG
    TXRFPWRDEFAULTSET, 0x05,
#else
    TXRFPWRDEFAULTSET, 0x3F,
#endif
    LNABIAS_102, 0x0F,
    RX_EN_CNT_102, 5,

    // Set up the internal strobe pulse width and frequency
    STROSCPWIDTH1_102, 0x06,
    STROSCPERIOD1_102, 3571 % 256,
    STROSCPERIOD2_102, 3571 / 256,
    
    // Disable the internal strobe oscillator. Use pluto's instead.
    WAKEUP_CTRL, (WAKEUP_CTRL_RESET_102 & ~ENAB_INT_STROBE_OSC),

    WAKEUP_WK_RX_CTRL2, 3,				// select select2 register
    WAKEUP_RX_BASEBAND_GAIN_SEL1, 0,	// reduce the baseband gain
    WAKEUP_SENS_SEQUENCE_CTRL, 1,		// enable sequencing register
    WAKEUP_WK_RX_CTRL2, 1,				// de-select select2 register

    // Set up lna gain switching
    WAKEUP_LNA_NEGTRIM_NOM1, 33,		// Increase medium LNA gain
    WAKEUP_LNA_NEGTRIM_NOM2, 33,		// Increase high LNA gain. Will be overwritten by factory cal.
    WAKEUP_LNABIAS_NOM1, 15,			// Increase LNA nom1 bias.
    WAKEUP_LNABIAS_NOM2, 15,			// Increase LNA nom2 bias.
    WAKEUP_WK_RX_CTRL2, 0x89			// Enable dynamic sensitivity algorithm.
};

bool volatile zl_receiveInterruptIsDisabled;


/**
 * initHelper
 * 
 * Loads the ZL7010x from a register initialization list
 */

static void initHelper(const uint8_t *p, int len)
{ 
	int i;

	for (i = 0; i < len; i += 2)
	{
		micsSPI_PutWithoutCheck(p[i], p[i+1]);
	}
}


/**
 * zl_Init_102
 * 
 * Initializes basic MICS chip registers related to the MICS
 * Transceiver.
 */

void zl_Init_102(void)
{
	initHelper(init102, sizeof(init102));
}


/**
 * Read the ZL7010x chip revision. Indicates whether this is a ZL70101 or ZL70102.
 */

uint8_t zl_GetRevision(void)
{
	return micsSPI_Get(MICS_REVISION);
}


/**
 * Read how many data blocks the ZL7010x has in its receive buffer.
 */

int zl_GetBlocksToRead(void)
{
	return micsSPI_Get(RXBUFF_USED);
}

int zl_GetBlocksToSend(void)
{
	return micsSPI_Get(TXBUFF_USED);
}


/**
 * Initialize the INTERFACE_MODE register on the ZL70102.
 */

void zl_ConfigInterfaceMode_102(void)
{
	micsSPI_Put(INTERFACE_MODE, MAX_SPI_CLOCK_1_MHZ | SPI_8BIT_ADDR_102);
}


/**
 * Read the IRQ_AUXSTATUS register on the ZL7010x.
 */

uint8_t zl_GetIrqAuxStatus(void)
{
	return micsSPI_Get(IRQ_AUXSTATUS);
}


/**
 * Read the IRQ_STATUS1 register on the ZL7010x.
 */

uint8_t zl_GetIrqStatus1(void)
{
	return micsSPI_Get(IRQ_STATUS1);
}


/**
 * Read the IRQ_STATUS2 register on the ZL7010x.
 */

uint8_t zl_GetIrqStatus2(void)
{
	return micsSPI_Get(IRQ_STATUS2);
}


/**
 * Acknowledge a ZL7010x auxiliary interrupt.
 */

void zl_AckIrqAuxStatus(uint8_t irq)
{
	micsSPI_PutWithoutCheck(IRQ_AUXSTATUS, ~irq);
}


/**
 * Acknowledge a ZL7010x group 1 interrupt
 */

void zl_AckIrqStatus1(uint8_t irq)
{
	micsSPI_PutWithoutCheck(IRQ_RAWSTATUS1, ~irq);
}


/**
 * Acknowledge a ZL7010x group 2 interrupt.
 */

void zl_AckIrqStatus2(uint8_t irq)
{
	micsSPI_PutWithoutCheck(IRQ_RAWSTATUS2, ~irq);
}


/**
 * Enable the IRQ_RXNOTEMPTY interrupt on the ZL7010x.
 */

void zl_EnableRxNotEmpty(void)
{
	micsSPI_PutWithoutCheck(IRQ_ENABLESET1, IRQ_RXNOTEMPTY);
	zl_receiveInterruptIsDisabled = false;
}


/**
 * Disable the IRQ_RXNOTEMPTY interrupt on the ZL7010x.
 */

void zl_DisableRxNotEmpty(void)
{
	micsSPI_PutWithoutCheck(IRQ_ENABLECLEAR1, IRQ_RXNOTEMPTY);
	zl_receiveInterruptIsDisabled = true;
}


/**
 * Enable the IRQ_COMMANDDONE interrupt on the ZL7010x.
 */

void zl_EnableCommandDone(void)
{
	micsSPI_PutWithoutCheck(IRQ_ENABLESET2, IRQ_COMMANDDONE);
}


/**
 * Disable the IRQ_HKREMOTEDONE interrupt on the ZL7010x.
 */
void zl_DisableHkRemoteDone(void)
{
	micsSPI_PutWithoutCheck(IRQ_ENABLECLEAR1, IRQ_HKREMOTEDONE);
}


/**
 * Disable the IRQ_HKUSERSTATUS interrupt on the ZL7010x.
 */
void zl_DisableHkUserStatus(void)
{
	micsSPI_PutWithoutCheck(IRQ_ENABLECLEAR1, IRQ_HKUSERSTATUS);
}


/**
 * Disable the IRQ_HKUSERDATA interrupt on the ZL7010x.
 */
void zl_DisableHkUserData(void)
{
	micsSPI_PutWithoutCheck(IRQ_ENABLECLEAR1, IRQ_HKUSERDATA);
}


/**
 * Disable all ZL7010x interrupt sources.
 */

void zl_DisableAll(void)
{
	micsSPI_PutWithoutCheck(IRQ_ENABLECLEAR1, 0xFF);
	micsSPI_PutWithoutCheck(IRQ_ENABLECLEAR2, 0xFF);
}



/**
 * Enable the ZL7010x internal watchdog.
 */

void zl_WatchdogOn(void)
{
#ifdef DEBUG		// TODO: Get rid of this?
	micsSPI_PutWithoutCheck(WDOG_CONTROL, WDOG_CLEAR_AND_DISAB);
#else
	micsSPI_PutWithoutCheck(WDOG_CONTROL, WDOG_ENAB);
#endif
}


/**
 * Clear the ZL7010x internal watchdog.
 */

void zl_WatchdogClear(void)
{
#ifdef DEBUG		// TODO: Get rid of this?
	micsSPI_PutWithoutCheck(WDOG_CONTROL, WDOG_CLEAR_AND_DISAB);
#else
	micsSPI_PutWithoutCheck(WDOG_CONTROL, WDOG_CLEAR);
#endif
}


/**
 * Write the MAC MICS ID registers on the ZL7010x.
 */

void zl_SetMacMicsID(const XPG_MICS_ID *id)
{
	micsSPI_Put(MAC_COMPANYID, id->micsIDCompanyId);
    micsSPI_Put(MAC_IMDTRANSID1, id->micsIDlsb);
    micsSPI_Put(MAC_IMDTRANSID2, id->micsIDmiddleByte);
    micsSPI_Put(MAC_IMDTRANSID3, id->micsIDmsb);
}


/**
 * Write the wakeup MICS ID registers on the ZL7010x.
 */

void zl_SetWakeupMicsID(const XPG_MICS_ID *id)
{
	micsSPI_Put(WAKEUP_COMPANYID, id->micsIDCompanyId);
    micsSPI_Put(WAKEUP_IMDTRANSID1, id->micsIDlsb);
    micsSPI_Put(WAKEUP_IMDTRANSID2, id->micsIDmiddleByte);
    micsSPI_Put(WAKEUP_IMDTRANSID3, id->micsIDmsb);
}


/**
 * Reset the ZL7010x.
 */

void zl_Reset(void)
{
	micsSPI_PutWithoutCheck(CHIP_RESET_ADDRESS, CHIP_RESET);
}


/**
 * Send a COPY_REGS command to the ZL7010x.
 */

void zl_CopyRegs(void)
{
	micsSPI_PutWithoutCheck(MAC_CTRL, COPY_REGS);
}


/**
 * Send a PERFORM_CALS command to the ZL7010x.
 */

void zl_PerformCals(void)
{
	micsSPI_PutWithoutCheck(MAC_CTRL, PERFORM_CALS);
}


/**
 * Send an ABORT_LINK command to the ZL7010x.
 */

void zl_AbortLink(void)
{
	micsSPI_PutWithoutCheck(MAC_CTRL, ABORT_LINK);
}


/**
 * Send a SKIP_CRC command to the ZL7010x.
 */

void zl_SkipCRC(void)
{
	micsSPI_PutWithoutCheck(MAC_CTRL, SKIP_CRC);
}


/**
 * Set the CRCCTRL register on the ZL7010x to CALC_CRC, initiating a wakeup stack CRC operation.
 */

void zl_CalcCRC(void)
{
	micsSPI_PutWithoutCheck(CRCCTRL, CALC_CRC);
}


/**
 * Read the CRCCTRL register on the ZL7010x and determine if the CRC operation is complete.
 */

bool zl_IsCRCDone(void)
{
	return (micsSPI_Get(CRCCTRL) & CALC_CRC_DONE) != 0;
}


/**
 * Clear the CRCCTRL register on the ZL7010x, ending the calculation of the wakeup stack CRC
 */

void zl_EndCalcCRC(void)
{
	micsSPI_PutWithoutCheck(CRCCTRL, 0);
}

/**
 * Set the ENAB_HK_WRITE bit in the INTERFACE_MODE register on the ZL70102.
 */

void zl_EnableHkWrites_102(void)
{
    micsSPI_Put(HK_MODE_102, HK_WRITE_ENAB | micsSPI_Get(HK_MODE_102));
}


/**
 * Clear the INITCOM register on the ZL7010x.
 */

void zl_ClearInitCom(void)
{
	micsSPI_Put(INITCOM, 0);
}


/**
 * Write the MAC_CHANNEL register on the ZL7010x.
 */

void zl_SetMacChannel(uint8_t chan)
{
	micsSPI_Put(MAC_CHANNEL, chan);
}


/**
 * Read the WAKEUP_CHANNEL register on the ZL7010x
 */

uint8_t zl_GetWakeupChannel(void)
{
	return micsSPI_Get(WAKEUP_CHANNEL);
}

/**
 * Set the CALSELECT1 and CALSELECT2 registers on a ZL70102.
 */

void zl_CalSelect_102(uint16_t cal)
{
	micsSPI_Put(CALSELECT1_102, (uint8_t)(cal & 0xff));
	micsSPI_Put(CALSELECT2_102, (uint8_t)((cal >> 8) & 0xff));
}


/**
 * Retrieve the RXIFADCDECLEV register, which has to be saved and restored
 * at wakeup because it isn't saved on the wakeup stack nor in the
 * wakeup registers.
 */

uint8_t zl_GetRxADCDecisionLevel(void)
{
	return micsSPI_Get(RXIFADCDECLEV);
}


/**
 * Set the RXIFADCDECLEV register, which has to be saved and restored
 * at wakeup because it isn't saved on the wakeup stack nor in the
 * wakeup registers.
 */

void zl_SetRxADCDecisionLevel(uint8_t n)
{
	micsSPI_Put(RXIFADCDECLEV, n);
}

/**
 * Start transmitting a data block to the ZL70102, assuming that all ZL70102
 * traffic is in 8-bit address mode.
 * 
 * WARNING: interrupts must already be disabled when calling this function,
 * in order to prevent SPI bus conflicts with interrupt-level code.  
 */
 
void zl_TxStart_102(void)
{
    mics_AssertSCS();
    mics_Send(TXRXBUFF);

    while (!mics_IsTxReady())
    {
       	//Wait until transmit is ready
    }

    mics_Send(0);
}


/**
 * Transmit one byte of a data block
 * 
 * WARNING: interrupts must already be disabled when calling this function,
 * in order to prevent SPI bus conflicts with interrupt-level code.
 * 
 * \param d	The byte to transmit  
 */
 
void zl_TxByte(uint8_t d)
{
	while (!mics_IsTxReady())
	{
	   	//Wait until transmit is ready
	}

	mics_Send(d);
}


/**
 * Finish transmitting a data block to the ZL7010x.
 * 
 * WARNING: interrupts must already be disabled when calling this function,
 * in order to prevent SPI bus conflicts with interrupt-level code.  
 */

void zl_TxFinish(void)
{
    while (mics_IsBusy())
    {
       	//Wait until MICS is ready
    }

    // Added delay to ensure there is enough time between the clock going low, and cs going high (Tcch).
    DELAY_US(1);

	//de-select the mics chip on the SPI bus
	mics_DeassertSCS();

    // And delay after deasserting CS to ensure that there is enough time between messages (Tcwh).
    DELAY_US(1);
}


/**
 * Validates a ZL7010x address
 */
#pragma diag_suppress 880 //parameter is required to meet the interface.
bool zl_IsAddressValid(uint16_t addr)
{
	//We use the upper byte for address as a mask. All of the lower-order bytes are valid.
	return true;
}
#pragma diag_default 880

/**
 * Applies the trims from the trim list
 */

void zl_ApplyTrimList(TRIM_LIST const *tl)
{
	const uint8_t *p;
	uint8_t val;
	int i;

	for (i = 0, p = tl->trims; i < tl->listLen; i++, p += 3)
	{
		if (p[0] != 0)
		{
			if(p[1] != 0)
			{
				val = micsSPI_Get(p[0]);
				val = (val & p[1]) | (p[2] & ~p[1]); //p[1] is an inverse mask where a 1 means don't write the bit.
			}
			else
			{
				val = p[2];
			}

			micsSPI_PutWithoutCheck(p[0], val);
		}
		else
		{
			uint8_t delayCount = p[2];

			//Can only use delay intrinsics with integer constants
			while (delayCount--)
			{
				DELAY_MS(1);
			}
		}
	}
}


/**
 * turn off the power save timer
 */
void zl_DisablePowerSaveTimer(void)
{
	micsSPI_Put(PWRSAVE, 0);
}


/**
 * Set up the high-level antenna tuning algorithm for TX tuning.
 */
void zl_ConfigAntTuneTx(void)
{
	micsSPI_Put(ANTTUNESEL1_102, ANTTUNE_PEAK_MATCH2 | ANTTUNE_ITERATIONS_5 | ANTTUNE_CAP_TX | ANTTUNE_CAP_MATCH1 | ANTTUNE_CAP_MATCH2);
	micsSPI_Put(ANTTUNESEL2_102, 0);
	micsSPI_Put(ANTTUNESEL3_102, ANTTUNE_ALG_EXTENDED | ANTTUNE_DELAY_60US);
}


/**
 * Set the antenna tuning peak detector gain and TX power level for an IPG.
 */
void zl_ConfigAntTuneGain_IPG(void)
{
	/*
	 * The IPG matching network doesn't put a lot of voltage on MATCH2, so it needs
	 * the peak detector gain turned up to max and the TX power turned up high.
	 */
	micsSPI_Put(TXPD_GAIN_CTRL_102, TXPD_GAIN_12);
	micsSPI_Put(TXRFPWRTUNEANTSET_102, 56);
}


/**
 * Set the antenna tuning peak detector gain and TX power level for an EPG.
 */
void zl_ConfigAntTuneGain_EPG(void)
{
	/*
	 * The EPG matching network puts a lot more voltage on MATCH2, so the peak
	 * detector gain has to be set lower than for the IPG.
	 */
	micsSPI_Put(TXPD_GAIN_CTRL_102, TXPD_GAIN_2);
	micsSPI_Put(TXRFPWRTUNEANTSET_102, 56);
}


/**
 * Determine whether this chip is a ZL70102.
 *
 * \return true if the chip self-identifies as a ZL70102, false otherwise
 */
bool zl_Is102(void)
{
	return micsSPI_Get(MICS_REVISION) == ZL70102;
}


/**
 * Determine whether the ZL70102 is running in 8-bit SPI mode
 *
 * \return true if the chip expects 8-bit addressing, false if it expects 7-bit.
 */
bool zl_Is8Bit_102(void)
{
	return (micsSPI_Get(INTERFACE_MODE) & SPI_8BIT_ADDR_102) != 0;
}


/**
 * Return the setting of the RX capacitor in receive mode
 *
 * @return RXRFANTTUNE_RX_MODE register value
 */
uint8_t zl_GetRxCapRxMode_102(void)
{
	return micsSPI_Get(RXRFANTTUNE_RX_MODE_102);
}


/**
 * Return the setting of the RX capacitor in transmit mode
 *
 * @return RXRFANTTUNE_TX_MODE register value
 */
uint8_t zl_GetRxCapTxMode_102(void)
{
	return micsSPI_Get(RXRFANTTUNE_TX_MODE_102);
}

/**
 * Return the setting of the TX capacitor in receive mode
 *
 * @return TXRFANTTUNE_RX_MODE register value
 */
uint8_t zl_GetTxCapRxMode_102(void)
{
	return micsSPI_Get(TXRFANTTUNE_RX_MODE_102);
}

/**
 * Return the setting of the TX capacitor in transmit mode
 *
 * @return TXRFANTTUNE_TX_MODE register value
 */
uint8_t zl_GetTxCapTxMode_102(void)
{
	return micsSPI_Get(TXRFANTTUNE_TX_MODE_102);
}

/**
 * Return the setting of the MATCH1 capacitor
 *
 * @return ANTMATCH1 register value
 */
uint8_t zl_GetMatch1_102(void)
{
	return micsSPI_Get(ANTMATCH1_102);
}

/**
 * Return the setting of the MATCH2 capacitor
 *
 * @return ANTMATCH2 register value
 */
uint8_t zl_GetMatch2_102(void)
{
	return micsSPI_Get(ANTMATCH2_102);
}


/**
 * Set the RX capacitor for receive mode
 *
 * \param cap RXRFANTTUNE_RX_MODE register value
 */
void zl_SetRxCapRxMode_102(uint8_t cap)
{
	micsSPI_Put(RXRFANTTUNE_RX_MODE_102, cap);
}


/**
 * Set the TX capacitor for transmit mode
 *
 * \param cap RXRFANTTUNE_TX_MODE register value
 */
void zl_SetRxCapTxMode_102(uint8_t cap)
{
	micsSPI_Put(RXRFANTTUNE_TX_MODE_102, cap);
}


/**
 * Set the TX capacitor for receive mode
 *
 * \param cap TXRFANTTUNE_RX_MODE register value
 */
void zl_SetTxCapRxMode_102(uint8_t cap)
{
	micsSPI_Put(TXRFANTTUNE_RX_MODE_102, cap);
}


/**
 * Set the TX capacitor for transmit mode
 *
 * \param cap TXRFANTTUNE_TX_MODE register value
 */
void zl_SetTxCapTxMode_102(uint8_t cap)
{
	micsSPI_Put(TXRFANTTUNE_TX_MODE_102, cap);
}


/**
 * Set the MATCH1 capacitor
 *
 * \param cap The new ANTMATCH1 register value
 */
void zl_SetMatch1_102(uint8_t cap)
{
	micsSPI_Put(ANTMATCH1_102, cap);
}


/**
 * Set the MATCH2 capacitor
 *
 * \param cap The new ANTMATCH2 register value
 */
void zl_SetMatch2_102(uint8_t cap)
{
	micsSPI_Put(ANTMATCH2_102, cap);
}


/**
 * Return the ADC value of the peak detector at the final cap values
 * chosen by the tuning algorithm.
 *
 * @return The value of the ANTTUNERES1 register
 */
uint8_t zl_GetTuneResult_102(void)
{
	return micsSPI_Get(ANTTUNERES1_102);
}

/**
 * Return the maximum ADC value seen during the high-level tuning
 * algorithm.
 *
 * @return The value of the ANTTUNERES2 register.
 */
uint8_t zl_GetTuneResultMax_102(void)
{
	return micsSPI_Get(ANTTUNERES2_102);
}

