/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: MICS driver MAC command.
 *
 *	Split out from the other internal functions to simplify an issue with
 *	unit testing.
 * 
 ***************************************************************************/

#include "mics_mac.h"
#include "micsspi.h"
#include "micsstate.h"

#include "timeout.h"

#include "log.h"
#include "log_events.h"
#include "EventQueue.h"

/**
 * mics_MacCmd()
 *  
 * Executes a MAC command and waits for completion or timeout.
 * 
 * \param macCmd - pointer to a MAC control function
 * 	 Examples: zl_PerformCals
 * 
 *   Refer to Zarlink ZL7010x Design Manual for description of MAC
 *   command.
 * 
 * \param maxMacCmdTime time limit (in msecs) for command to complete
 */
#include <stdio.h>
void mics_MacCmd(void (*macCmd)(void), uint16_t maxMacCmdTime)
{
	int16_t timeout;

    //clear IRQ_COMMANDDONE bit in IRQ status latch so can see when cmd completes
    micsState_ClearCommandDone();

    //start the command
    (*macCmd)();

    //get the starting time
    timeout = timeoutSet(MS(maxMacCmdTime));

    //wait for command to complete
    while (!micsState_IsCommandDone())
    {
        //if not done yet, see if we've timed out
        if (timeoutIsExpired(timeout))
        {
            //TODO need to log an error if this happens?
			#ifdef VERBOSE_MICS_LOGGING
        		dispPutLogEvent(E_LOG_MICS_MAC_CMND_TIMEOUT, 0);
			#endif

            break;
        }
    }
}
