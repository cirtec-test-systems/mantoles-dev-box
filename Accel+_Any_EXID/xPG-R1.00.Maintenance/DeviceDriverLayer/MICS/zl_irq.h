/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: ZL70102 IRQ bit definitions
 *	
 * This file contains definitions of just the IRQ flag bits used by the ZL70102.
 * For convenience, these bits are used in multiple places in the micsCond
 * (conductor) module, because that's more efficient and more convenient than
 * duplicating the work just to try to isolate them into the zl module.
 *
 * Note to help prevent programming errors, the addresses of all registers
 * specific to the ZL70101 end in "_101", and the addresses of all registers
 * specific to the ZL70102 all end in "_102". The other defines for each 
 * register only end in "_101" or "_102" when there's a conflict between the
 * ZL70101 and ZL70102, or potential for confusion (e.g. a register exists in
 * both, but some of it's bits only apply to the ZL70101 or ZL70102).
 * 
 * All registers specific to the ZL70101 have been removed as ZL70101 support
 * has been removed. ZL70102-specific registers have keep their indicator in
 * order to improve maintainability.
 *
 * This file is derived from the header file MicsHw.h provided by Zarlink in
 * the ZL7010X application development kit (ADK).
 * 
 * Zarilnk's copyright notice for MicsHw.h follows:
 * 
 * Copyright Zarlink Semiconductor (U.S.) Inc. 2007. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2007.
 * The use of the copyright notice is intended to provide notice that Zarlink
 * Semiconductor Inc owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Zarlink
 * Semiconductor Inc; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Zarlink Semiconductor Inc. This work is provided on a right to use basis
 * subject to additional restrictions set out in the applicable license or
 * other agreement.
 *
 ***************************************************************************/

#ifndef ZL_IRQ_H_
#define ZL_IRQ_H_

/* Defines for bits in interrupt group 1 registers (IRQ_RAWSTATUS1,
 * IRQ_STATUS1, IRQ_ENABLE1, IRQ_ENABLESET1, and IRQ_ENABLECLEAR1). Note the
 * MICS chip enables some interrupts by default when it wakes up or is reset,
 * as indicated. These interrupts should always be handled so if they ever
 * occur, they won't keep occurring (the ISR can just clear or disable them).
 */
#define IRQ_HKREMOTEDONE  0x01  /* enabled by default */
#define IRQ_CLOST         0x02  /* enabled by default */
#define IRQ_HKUSERSTATUS  0x04  /* enabled by default */
#define IRQ_HKUSERDATA    0x08  /* enabled by default */
#define IRQ_RXNOTEMPTY    0x10  /* disabled by default */
#define IRQ_TXFULL        0x20  /* disabled by default */
#define IRQ_TXHALFEMPTY   0x40  /* disabled by default */
#define IRQ_TXEMPTY       0x80  /* disabled by default */

/* Defines for bits in interrupt group 2 registers (IRQ_RAWSTATUS2,
 * IRQ_STATUS2, IRQ_ENABLE2, IRQ_ENABLESET2, and IRQ_ENABLECLEAR2). Note the
 * MICS chip enables some interrupts by default when it wakes up or is reset,
 * as indicated. These interrupts should always be handled so if they ever
 * occur, they won't keep occurring (the ISR can just clear or disable them).
 */
#define IRQ_COMMANDDONE     0x01  /* disabled by default */
#define IRQ_HKMESSREG       0x02  /* disabled by default */
#define IRQ_LINKREADY       0x04  /* enabled by default */
#define IRQ_RADIOREADY      0x08  /* enabled by default */
#define IRQ_RADIOFAIL       0x10  /* enabled by default */
#define IRQ_LINKQUALITY     0x20  /* disabled by default */
#define IRQ_TXBUFFOVERFLOW  0x40  /* disabled by default */

/* Defines for bits in auxiliary interrupt status register (IRQ_AUXSTATUS).
 */
#define IRQ_SYNTHLOCKFAIL  0x01
#define IRQ_CRCERR         0x02
#define IRQ_WDOG           0x04
#define IRQ_HKABORTLINK    0x08
#define IRQ_MAXBERR        0x10
#define IRQ_MAXRETRIES     0x20
#define IRQ_VREGFAIL_102   0x40  /* ZL70102 */

#endif /*ZL_IRQ_H_*/
