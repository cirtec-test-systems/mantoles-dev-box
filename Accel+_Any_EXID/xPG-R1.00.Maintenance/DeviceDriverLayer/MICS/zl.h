/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: zl (ZL7010x accessor) module
 *	
 ***************************************************************************/

#ifndef ZL_H_
#define ZL_H_

#include <stdint.h>
#include <stdbool.h>
#include "Common/Protocol/cmndSetXpgIdent.h"
#include "Common/Protocol/trim_list.h"

extern bool volatile zl_receiveInterruptIsDisabled;

void zl_Init_102(void);
uint8_t zl_GetRevision(void);
bool zl_Is102(void);
bool zl_Is8Bit_102(void);
int zl_GetBlocksToRead(void);
int zl_GetBlocksToSend(void);
void zl_ConfigInterfaceMode_102(void);
uint8_t zl_GetIrqAuxStatus(void);
uint8_t zl_GetIrqStatus1(void);
uint8_t zl_GetIrqStatus2(void);
void zl_AckIrqAuxStatus(uint8_t irq);
void zl_AckIrqStatus1(uint8_t irq);
void zl_AckIrqStatus2(uint8_t irq);
void zl_EnableRxNotEmpty(void);
void zl_DisableRxNotEmpty(void);
void zl_EnableCommandDone(void);
void zl_DisableHkRemoteDone(void);
void zl_DisableHkUserStatus(void);
void zl_DisableHkUserData(void);
void zl_DisableAll(void);
void zl_WatchdogOn(void);
void zl_WatchdogClear(void);
void zl_SetMacMicsID(const XPG_MICS_ID *id);
void zl_SetWakeupMicsID(const XPG_MICS_ID *id);
void zl_Reset(void);
void zl_CopyRegs(void);
void zl_PerformCals(void);
void zl_AbortLink(void);
void zl_SkipCRC(void);
void zl_CalcCRC(void);
bool zl_IsCRCDone(void);
void zl_EndCalcCRC(void);
void zl_EnableHkWrites_102(void);
void zl_ClearInitCom(void);
void zl_SetMacChannel(uint8_t chan);
uint8_t zl_GetWakeupChannel(void);
void zl_CalSelect_102(uint16_t cal);
uint8_t zl_GetRxADCDecisionLevel(void);
void zl_SetRxADCDecisionLevel(uint8_t n);
void zl_DisablePowerSaveTimer(void);

void zl_TxStart_102(void);
void zl_TxByte(uint8_t d);
void zl_TxFinish(void);

bool zl_IsAddressValid(uint16_t addr);
void zl_ApplyTrimList(TRIM_LIST const *tl);

void zl_ConfigAntTuneTx(void);
uint8_t zl_GetRxCapRxMode_102(void);
uint8_t zl_GetRxCapTxMode_102(void);
uint8_t zl_GetTxCapRxMode_102(void);
uint8_t zl_GetTxCapTxMode_102(void);
uint8_t zl_GetMatch1_102(void);
uint8_t zl_GetMatch2_102(void);
void zl_SetRxCapRxMode_102(uint8_t cap);
void zl_SetRxCapTxMode_102(uint8_t cap);
void zl_SetTxCapRxMode_102(uint8_t cap);
void zl_SetTxCapTxMode_102(uint8_t cap);
void zl_SetMatch1_102(uint8_t cap);
void zl_SetMatch2_102(uint8_t cap);
uint8_t zl_GetTuneResult_102(void);
uint8_t zl_GetTuneResultMax_102(void);
void zl_ConfigAntTuneGain_IPG(void);
void zl_ConfigAntTuneGain_EPG(void);

#endif /*ZL_H_*/
