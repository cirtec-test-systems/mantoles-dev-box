/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: ZL70101/ZL70102 register and bit definitions
 *	
 * This file contains defines and macros to interface to the MICS chip
 * (ZL70101, ZL70102, etc.).
 * 
 * Note to help prevent programming errors, the addresses of all registers
 * specific to the ZL70101 end in "_101", and the addresses of all registers
 * specific to the ZL70102 all end in "_102". The other defines for each 
 * register only end in "_101" or "_102" when there's a conflict between the
 * ZL70101 and ZL70102, or potential for confusion (e.g. a register exists in
 * both, but some of it's bits only apply to the ZL70101 or ZL70102).
 * 
 * All registers specific to the ZL70101 have been removed as ZL70101 support
 * has been removed. ZL70102-specific registers have keep their indicator in
 * order to improve maintainability.
 *
 * This file is derived from the header file MicsHw.h provided by Zarlink in
 * the ZL7010X application development kit (ADK).
 * 
 * 
 * Zarilnk's copyright notice for MicsHw.h follows:
 * 
 * Copyright Zarlink Semiconductor (U.S.) Inc. 2007. All rights reserved.
 * This copyrighted work constitutes an unpublished work created in 2007.
 * The use of the copyright notice is intended to provide notice that Zarlink
 * Semiconductor Inc owns a copyright in this unpublished work; the copyright
 * notice is not an admission that publication has occurred. This work contains
 * confidential, proprietary information and trade secrets of Zarlink
 * Semiconductor Inc; it may not be used, reproduced or transmitted, in whole
 * or in part, in any form or by any means without the prior written permission
 * of Zarlink Semiconductor Inc. This work is provided on a right to use basis
 * subject to additional restrictions set out in the applicable license or
 * other agreement.
 *
 ***************************************************************************/

#ifndef ZL_BITS_H
#define ZL_BITS_H

#include "zl_irq.h"

/**************************************************************************
 * Defines and Macros
 */
 

/* When reading a register from the ZL7010X via SPI, this must be OR'ed with
 * the register address sent to the ZL7010X to indicate a read operation. As
 * a result, the address sent to the ZL7010X via SPI is limited to 7 bits
 * (0 to 127), so the register page must be selected separately beforehand.
 * 
 * Note the ADK software handles the MICS_READ bit and the register paging
 * automatically, so you never need to specify MICS_READ in a register address
 * passed to the ADK software. If you do, the ADK software will think you're
 * trying to access a page 2 register (see MICS_PAGE_2).
 */
#define MICS_READ  0x80

/* When this bit is set in a register address passed to the ADK software, it
 * tells the software to access a register in page 2 instead of page 1. Thus,
 * the address range for page 2 registers is 128 to 255. The ADK software
 * handles the register paging automatically. Note for the ZL70101, page 2
 * is also called the test page.
 */
#define MICS_PAGE_2  0x80

/*
 * The M0 bit is used to enable 8-bit address mode during reads
 */
#define MICS_M0	0x40

/* Lists of registers a ZL7010X includes in its wakeup stack. When the
 * MAC_CTRL.COPY_REGS command is executed on the ZL7010X, it saves these
 * registers in its wakeup stack, so they'll be preserved while it's sleeping.
 * Note for page 2 registers, MIS_PAGE_2 (0x80) must be set in the address.
 * 
 * MICS_STACK_REGS_102:
 *     Registers the ZL70102 includes in its wakeup stack.
 */
#define MICS_STACK_REGS_102 \
    "\x02\x04\x05\x07\x25\x2B\x63\x64\x65\x66\x68\x69\x6D\x72\x73\x78\x79" \
    "\x7A\x7B\x7C\x7D\xAB\xBB\xF0\xF1\xF2\xF3\xF6\xF8\xF9\xFA\xFB\xFC\xFD\xFE"
    
/* Synthesizer startup times:
 * 
 * MICS_LONG_SYNTH_LOCK_TIME_US: If the synthesizer is enabled with
 * SYNTH_CT.SYNTH_CT_READY = 0 (so the ZL7010X will coarse tune SYNTH_CT),
 * it should lock within MICS_LONG_SYNTH_LOCK_TIME_US. After power-on/reset,
 * SYNTH_CT_READY is 0 by default.
 * 
 * MICS_SHORT_SYNTH_LOCK_TIME_US: If the synthesizer is enabled with
 * SYNTH_CT.SYNTH_CT_READY = 1 and SYNTH_CTRIM.SYNTH_CTRIM_READY = 1
 * (so the ZL7010X will use the current SYNTH_CT and SYNTH_CTRIM values),
 * the synthesizer should lock within MICS_SHORT_SYNTH_LOCK_TIME_US. If it
 * hasn't locked by that time, the ZL7010X will assert IRQ_SYNTHLOCKFAIL.
 * To trim SYNTH_CTRIM and set SYNTH_CTRIM.SYNTH_CTRIM_READY beforehand, call
 * MicsSynthCtrim(). To coarse tune SYNTH_CT and set SYNTH_CT.SYNTH_CT_READY
 * beforehand, call MicsCoarseTuneSynth() (note MicsCoarseTuneSynth() will
 * also trim SYNTH_CTRIM and set SYNTH_CTRIM_READY if not already done).
 */
#define MICS_LONG_SYNTH_LOCK_TIME_US   4350
#define MICS_SHORT_SYNTH_LOCK_TIME_US  1941

/* If the channel is changed while the synthesizer is enabled, this is the
 * maximum time it will take before the ZL7010X is ready to take another RSSI
 * sample. This gives the synthesizer ~900 us and the RSSI ~100 us to settle.
 */
#define MICS_CHAN_SETTLING_TIME_US  (900 + 100)

/* The power-up time for the internal ADC in the ZL7010X. After enabling the
 * ADC, you should wait this long before taking the first ADC sample.
 */
#define MICS_ADC_START_TIME_US  15

/**************************************************************************
 * Defines for page 1 registers in the MICS chip (register addresses & related).
 */
 
/* Number of blocks used in receive buffer.
 */
#define RXBUFF_USED        0x01  /* 1 (R) */
#define RXBUFF_USED_MASK   0x7F
#define RXBUFF_USED_RESET  0x00
#define RXBUFF_USED_MAX    64
/*
 * Receive block size, etc. Note if the MAC_CTRL.COPY_REGS command is executed,
 * the MICS chip will copy this to the wakeup stack, and when the chip wakes
 * up, it will restore the value preserved in the wakeup stack.
 */
#define RXBUFF_BSIZE            0x02  /* 2 (R/W) */
#define RXBUFF_BSIZE_RESET_102  0x1F  /* ZL70102 */
/* bits for receive block size */
#define RXBUFF_BSIZE_MASK    0x0F
#define RXBUFF_BSIZE_MAX     15
/* bit to improve error correction for packets with unused bytes */
#define IMPROVE_ERR_COR_102  0x10  /* ZL70102 */
  
/* Number of blocks used in transmit buffer.
 */
#define TXBUFF_USED        0x03  /* 3 (R) */
#define TXBUFF_USED_MASK   0x7F
#define TXBUFF_USED_RESET  0x00
#define TXBUFF_USED_MAX    64
/*
 * Transmit block size. Note if the MAC_CTRL.COPY_REGS command is executed,
 * the MICS chip will copy this to the wakeup stack, and when the chip wakes
 * up, it will restore the value preserved in the wakeup stack.
 */
#define TXBUFF_BSIZE        0x04  /* 4 (R/W) */
#define TXBUFF_BSIZE_MASK   0x0F
#define TXBUFF_BSIZE_RESET  15
#define TXBUFF_BSIZE_MAX    15
/*
 * Maximum number of blocks to transmit in a packet. Note if the
 * MAC_CTRL.COPY_REGS command is executed, the MICS chip will copy this to the
 * wakeup stack, and when the chip wakes up, it will restore the value
 * preserved in the wakeup stack.
 */
#define TXBUFF_MAXPACKSIZE        0x05  /* 5 (R/W) */
#define TXBUFF_MAXPACKSIZE_MASK   0x1F
#define TXBUFF_MAXPACKSIZE_RESET  31

/* For the ZL70102, this clears the error counters, etc.
 */
#define ERRCLR_102        0x06  /* ZL70102: 6 (R/W) */
#define ERRCLR_MASK_102   0x07
#define ERRCLR_RESET_102  0x07
/* bit to stop ECCORR counter (1 to stop, 0 to clear & restart) */
#define STOP_ECCORR       0x01  /* ZL70102 */
/* bit to stop CRCERR counter (1 to stop, 0 to clear & restart) */
#define STOP_CRCERR       0x02  /* ZL70102 */
/* bit to stop BLKCNT counter (1 to stop, 0 to clear & restart) */
#define STOP_BLKCNT       0x04  /* ZL70102 */


/* best value to use for different TX modulation modes */
#define TRAINWORD_2FSK       0xAA
#define TRAINWORD_4FSK       0xD8

/* Controls the number of training words transmitted at the beginning of each
 * MICS header. For the ZL70102, bits [7:2] determine the number of training
 * words, and bits [1:0] select the minimum number of training words. Note the
 * value in bits [7:2] is divided by 1 for 4FSK, 2 for 2FSK, and 4 for 2FSK-FB.
 * The macro TRAINNUM_102(numTrainWords, txMod) can be used to calculate the
 * value for bits [7:2] that will give the specified number of training words
 * for the specified TX modulation (TX_MOD_2FSK_FB, ...). Note "txMod" is OR'ed
 * with TX_MOD_MASK so you can pass a whole MAC_MODUSER value if desired.
 * 

 */
#define TRAINNUM                    0x07  /* 7 (R/W) */
#define TRAINNUM_MASK               0x03
#define TRAINNUM_RESET_102          0x31  /* ZL70102 */
/* bits for min # training words for ZL70102; # training words for ZL70101 */
#define TRAINNUM_MIN_MASK           0x03
#define TRAINNUM_MIN_1              0x00
#define TRAINNUM_MIN_2              0x01
#define TRAINNUM_MIN_3              0x02
#define TRAINNUM_MIN_4              0x03
/* for ZL70102, these bits determine # of training words (see comment above) */
#define TRAINNUM_MASK_102           (0xFC)  /* ZL70102 */
#define TRAINNUM_102(numTrainWords, txMod) \
    (((txMod & TX_MOD_MASK) == TX_MOD_2FSK_FB) ? (numTrainWords << 4) : \
    (((txMod & TX_MOD_MASK) == TX_MOD_2FSK) ? (numTrainWords << 3) : \
    (numTrainWords << 2)))

/* Sync pattern registers. The MSB of the sync pattern (SYNC5) is transmitted
 * first, so it is also the first to be received. 
 */
#define SYNC1        0x09  /* 9 (R/W) */
#define SYNC1_MASK   0xFF
#define SYNC1_RESET  0x9A
/**/
#define SYNC2        0x0A  /* 10 (R/W) */
#define SYNC2_MASK   0xFF
#define SYNC2_RESET  0x29
/**/
#define SYNC3        0x0B  /* 11 (R/W) */
#define SYNC3_MASK   0xFF
#define SYNC3_RESET  0x8F
/**/
#define SYNC4        0x0C  /* 12 (R/W) */
#define SYNC4_MASK   0xFF
#define SYNC4_RESET  0x4D
/**/
#define SYNC5        0x0D  /* 13 (R/W) */
#define SYNC5_MASK   0xFF
#define SYNC5_RESET  0xB1

/* Maximum number of block errors that can occur in a single packet before the
 * IRQ_MAXBERR interrupt is generated. Note the block errors count is started
 * over at the beginning each packet.
 */
#define MAXBERR        0x0E  /* 14 (R/W) */
#define MAXBERR_MASK   0x1F
#define MAXBERR_RESET  0x03
#define MAXBERR_MAX    0x1F

/* Number of blocks (not headers) corrected by the Reed-Solomon FEC. Note this
 * stops counting when it reaches 0xFF, and may be cleared by setting
 * CLEAR_ECCORR in the ERRCLR register.
 */
#define ECCERR        0x0F  /* 15 (R) */
#define ECCERR_RESET  0x00

/* Number of erroneous blocks and headers detected by the CRC decoder. Note
 * this stops counting when it reaches 0xFF, and may be cleared by setting
 * CLEAR_CRCERR in the ERRCLR register.
 */
#define CRCERR        0x10  /* 16 (R) */
#define CRCERR_RESET  0x00


/* bit to clear ECCORR counter (1 to clear, 0 to count) */
#define CLEAR_ECCERR      0x01
/* bit to clear CRCERR counter (1 to clear, 0 to count) */
#define CLEAR_CRCERR      0x02

/* bits for coarse control of on/off times for 2.45 GHz wakeup */
#define WU_ON_COARSE_MASK         0x03
#define WU_SHORT_OFF_COARSE_MASK  0x1C
#define WU_LONG_OFF_COARSE_MASK   0xE0

/* IMD transceiver ID (implant ID; 24 bits; 1 = LSB, 3 = MSB). Note if the MICS
 * chip is awakened by 2.45 GHz wakeup, it will copy WAKEUP_IMDTRANSID1/2/3
 * (preserved in the wakeup block) to MAC_IMDTRANDID1/2/3. In contrast, if
 * direct wakeup is used, or the chip is reset, MAC_IMDTRANSID1/2/3 will be 0.
 */
#define MAC_IMDTRANSID1        0x13  /* 19 (R/W) */
#define MAC_IMDTRANSID1_RESET  0x00
#define MAC_IMDTRANSID2        0x14  /* 20 (R/W) */
#define MAC_IMDTRANSID2_RESET  0x00
#define MAC_IMDTRANSID3        0x15  /* 21 (R/W) */
#define MAC_IMDTRANSID3_RESET  0x00

/* Company ID. Note if the MICS chip is awakened by 2.45 GHz wakeup, it will
 * copy WAKEUP_COMPANYID (preserved in the wakeup block) to MAC_COMPANYID. In
 * contrast, if direct wakeup is used, or the chip is reset, MAC_COMPANYID
 * will be 0.
 */
#define MAC_COMPANYID          0x16  /* 22 (R/W) */
#define MAC_COMPANYID_RESET    0x00

/* TX/RX modulation and user wakeup data. Note if the MICS chip is awakened by
 * 2.45 GHz wakeup, it will extract the TX and RX modulation settings from the
 * 2.45 GHz signal, swap them, place them in WAKEUP_MODUSER[6:0], and copy them
 * to MAC_MODUSER. In contrast, if direct wakeup is used, or the chip is reset,
 * MAC_MODUSER will be 0.
 */
#define MAC_MODUSER            0x17  /* 23 (R/W) */
#define MAC_MODUSER_MASK       0x7F
#define MAC_MODUSER_RESET      0x00
/* bits for TX modulation mode */
#define TX_MOD_MASK            0x03
#define TX_MOD_2FSK_FB         0x00
#define TX_MOD_2FSK            0x02
#define TX_MOD_4FSK            0x03
/* bits for RX modulation mode */
#define RX_MOD_MASK            0x0C
#define RX_MOD_2FSK_FB         0x00
#define RX_MOD_2FSK            0x08
#define RX_MOD_4FSK            0x0C
/* bits containing user-defined wakeup data (3 bits) */
#define USER_WAKEUP_DATA_MASK  0x70


/* bits for fine control of on/off times for 2.45 GHz wakeup */
#define WU_ON_FINE_MASK         0x0F
#define WU_SHORT_OFF_FINE_MASK  0x30
#define WU_LONG_OFF_FINE_MASK   0xC0

/* MICS channel. For the ZL70102, this will be 5 after a direct
 * wakeup, or the value copied from WAKEUP_CHANNEL after 2.45 GHz
 * wakeup.
 */
#define MAC_CHANNEL            0x19  /* 25 (R/W) */
#define MAC_CHANNEL_MASK       0x0F
#define MAC_CHANNEL_RESET_102  5  /* ZL70102 */
#define MAC_CHANNEL_MAX        11
/* maximum valid MICS channel (channels 0 to 9) */
#define MAX_MICS_CHAN          9

/* Register to control main watchdog timer (clear, disable, and enable).
 */
#define WDOG_CONTROL          0x1A  /* 26 (W), reg_mac_clearwdog */
#define WDOG_CLEAR            0xFF
#define WDOG_CLEAR_AND_DISAB  0x0F
#define WDOG_ENAB             0xF0

/* Housekeeping registers.
 */
#define HK_TXADDR            0x1C  /* 28 (R/W) */
#define HK_TXADDR_RESET      0x00
/**/ 
#define HK_TXDATA            0x1D  /* 29 (R/W) */
#define HK_TXDATA_RESET      0x00
/**/ 
#define HK_TXREPLY           0x1E  /* 30 (R) */
#define HK_TXREPLY_RESET     0x00
/**/ 
#define HK_USERDATA          0x1F  /* 31 (R/W) */
#define HK_USERDATA_RESET    0x00
/**/
#define HK_USERSTATUS        0x20  /* 32 (R/W) */
#define HK_USERSTATUS_RESET  0x00
/**/
#define HK_LASTADDRESS       0x21  /* 33 (R) */
#define HK_LASTADDRESS_RESET 0x00


/* For the ZL70102, this is the housekeeping mode control register.
 */
#define HK_MODE_102        0x22  /* ZL70102: 34 (R/W) */ 
#define HK_MODE_RESET_102  0x00
/* various HK_MODE_102 bits */
#define HK_ACCESS_PAGE_2   0x01
#define HK_WRITE_ENAB      0x02

/* Maximum number of retries that can occur before the IRQ_MAXRETRIES
 * interrupt is generated. Also contains a bit to clear the retries counter.
 */
#define MAXRETRIES           0x24  /* 36 (R/W) */
#define MAXRETRIES_RESET     0x0A
/* mask for bits containing maximum number of retries */
#define MAXRETRIES_MASK      0x7F
/* bit to clear retry counter (1 to clear, 0 to start counting retries) */
#define CLEAR_RETRY_COUNTER  0x80

/* Resend time and bit to disable the resend timer on a base station.
 * The resend time is in multiples of 0.427 ms. The default value (0x17) gives
 * a resend time of 9.829 ms. Note when the MICS chip is sending wakeup
 * responses (implant), the resend timer can't be disabled, and the actual
 * resend time varies randomly anywhere from 1 to 2 times the register setting.
 */
#define RESENDTIME          0x25  /* 37 (R/W) */
#define RESENDTIME_RESET    0x17  /* 9.829 ms */
/* bits containing resend time */
#define RESENDTIME_MASK     0x7F
#define RESENDTIME_MIN      0x00  /* 55 us gap between packets (base station) */
#define RESENDTIME_MAX      0x7F  /* about 54.2 ms */
/* bit to disable resend timer (base station only) */
#define RESENDTIME_DISAB    0x80
/* setting to ensure resend time >= specified microseconds */
#define RESENDTIME_US(us)   ((us / (427 + 1)) + 1)


/* Power save time and bits to set the power save time to one-time-only
 * and to control the length of the power save time.
 */

#define PWRSAVE		0x26
#define PWRSAVE_MASK	0x3F
#define PWRSAVE_SLOW	0x80
#define PWRSAVE_ONCE	0x40


/* MAC control register. Set a bit to start the corresponding command. Note
 * only one command can be performed at a time (except SKIP_CRC, which may be
 * set any time). When the command is done, the MICS chip automatically clears
 * the corresponding bit and asserts IRQ_COMMANDDONE (except for ABORT_LINK and
 * SKIP_CRC, which do not assert IRQ_COMMANDDONE, and must be cleared manually).
 */
#define MAC_CTRL            0x29  /* 41 (R/W) */
#define MAC_CTRL_MASK       0x7F
#define MAC_CTRL_RESET      0x00
/* MAC_CTRL bits (commands) */
#define FLUSH_LINK          0x01
#define ABORT_LINK          0x02
#define COPY_REGS           0x04
#define PERFORM_CALS        0x08
#define SKIP_CRC            0x10
#define FLUSH_LOCAL_TX_BUF  0x20
#define FLUSH_LOCAL_RX_BUF  0x40

/* MAC initiate communication register.
 */
#define INITCOM                         0x2A  /* 42 (R/W) */
#define INITCOM_MASK                    0x3F
#define INITCOM_RESET                   0x08
/* INITCOM bits */
#define TX_245_GHZ_WAKEUP               0x01
#define RX_WAKEUP_RESPONSES             0x02
#define START_SESSION                   0x04
#define IBS_FLAG                        0x08
#define ENAB_RESPONSES_WITH_WRONG_ADDR  0x10
#define INV_TX245_ON_OFF_KEYING         0x20
#define LOW_DUTY_CYCLE_MODE_102         0x40  /* ZL70102 (burst mode) */
#define LISTEN_MODE_102                 0x80  /* ZL70102 */
/* old name (kept for backwards compatibility with existing software) */
#define ENAB_ZL70100_RESPONSES          ENAB_RESPONSES_WITH_WRONG_ADDR

/* Interface mode control register. This controls the operation of the SPI
 * interface and various pins on the MICS chip.
 */
#define INTERFACE_MODE            0x2B  /* 43 (R/W) */
#define INTERFACE_MODE_RESET_102  0x60  /* ZL70102: max SPI clock = 4 MHz */
/* various INTERFACE_MODE bits */
#define ACCESS_PAGE_2             0x01
#define ACCESS_TEST_PAGE          0x01  /* page 2 is test page for ZL70101 */
#define MAC_BYPASS                0x02
#define RF_BYPASS                 0x04
#define SPI_8BIT_ADDR_102		   0x10	 /* ZL70102 */
/* bits to select max SPI clock speed (lower max speed saves some power) */
#define MAX_SPI_CLOCK_MASK        0x60
#define MAX_SPI_CLOCK_1_MHZ       0x00
#define MAX_SPI_CLOCK_2_MHZ       0x20  /* default after reset */
#define MAX_SPI_CLOCK_4_MHZ       0x60
/* bit to select loopback mode (1 = loop RX buf back into TX buf) */
#define LOOPBACK                  0x80

/* Transmit/receive buffer used to write data to the TX buffer and read data
 * from the RX buffer at the same time (not typically used).
 */
#define TXRXBUFF_BOTH        0x2C  /* 44 (R/W) */
#define TXRXBUFF_BOTH_RESET  0x00
/*
 * Transmit/receive buffer. Read to start reading data from RX buffer, or write
 * to start writing data to the TX buffer.
 */
#define TXRXBUFF             0x2D  /* 45 (R/W) */
#define TXRXBUFF_RESET       0x00

/*
 * Read to get the raw status for interrupt group 1 (status before masking).
 * If a bit is set, it indicates the corresponding interrupt condition. Write
 * to selectively clear interrupts. Writing 0 to a bit clears the corresponding
 * interrupt (writing 1 has no affect). Note for some interrupts, you need to
 * clear the interrupt source first.
 */
#define IRQ_RAWSTATUS1          0x2E  /* 46 (R/W) */
#define IRQ_RAWSTATUS1_RESET    0xC0
/*
 * Read to get the enable status for interrupt group 1. If a bit is set, it
 * indicates the corresponding interrupt is enabled (not masked).
 */
#define IRQ_ENABLE1             0x2F  /* 47 (R) */
#define IRQ_ENABLE1_RESET       0x0F
/*
 * Write to enable interrupts in interrupt group 1. Setting a bit enables the
 * corresponding interrupt (clearing a bit has no affect).
 */
#define IRQ_ENABLESET1          0x30  /* 48 (W) */
#define IRQ_ENABLESET1_RESET    0x0F
/*
 * Write to disable interrupts in interrupt group 1. Setting a bit disables the
 * corresponding interrupt (clearing a bit has no affect).
 */
#define IRQ_ENABLECLEAR1        0x31  /* 49 (W) */
#define IRQ_ENABLECLEAR1_RESET  0x00

/*
 * Read to get the raw status for interrupt group 2 (status before masking).
 * If a bit is set, it indicates the corresponding interrupt condition. Write
 * to selectively clear interrupts. Writing 0 to a bit clears the corresponding
 * interrupt (writing 1 has no affect). Note for some interrupts, you need to
 * clear the interrupt source first.
 */
#define IRQ_RAWSTATUS2          0x32  /* 50 (R/W) */
#define IRQ_RAWSTATUS2_RESET    0x00
/*
 * Read to get the enable status for interrupt group 2. If a bit is set, it
 * indicates the corresponding interrupt is enabled (not masked).
 */
#define IRQ_ENABLE2             0x33  /* 51 (R) */
#define IRQ_ENABLE2_RESET       0x1C
/*
 * Write to enable interrupts in interrupt group 2. Setting a bit enables the
 * corresponding interrupt (clearing a bit has no affect).
 */
#define IRQ_ENABLESET2          0x34  /* 52 (W) */
#define IRQ_ENABLESET2_RESET    0x1C
/*
 * Write to disable interrupts in interrupt group 2. Setting a bit disables the
 * corresponding interrupt (clearing a bit has no affect).
 */
#define IRQ_ENABLECLEAR2        0x35  /* 53 (W) */
#define IRQ_ENABLECLEAR2_RESET  0x00
/*
 * Read to get the status for interrupt group 1 and 2 (status after masking).
 * If a bit set, it indicates the corresponding interrupt condition (note the
 * bit will only be set if the corresponding interrupt is also enabled).
 */
#define IRQ_STATUS1        0x36  /* 54 (R) */
#define IRQ_STATUS1_RESET  0x00
/**/
#define IRQ_STATUS2        0x37  /* 55 (R) */
#define IRQ_STATUS2_RESET  0x00

/*
 * Read to get the raw status for auxiliary interrupts. If a bit is set, it
 * indicates the corresponding interrupt condition. Write to selectively clear
 * interrupts. Writing 0 to a bit clears the corresponding interrupt (writing
 * 1 has no affect). Note for some interrupts, you need to clear the interrupt
 * source first.
 */
#define IRQ_AUXSTATUS        0x38  /* 56 (R/W) */
#define IRQ_AUXSTATUS_RESET  0x00

/* Number of bytes of overflow while transmit buffer was full.
 */
#define TXBUFF_OVERFLOW        0x39  /* 57 (R/W) */
#define TXBUFF_OVERFLOW_RESET  0x00

/* Source for programmable output 0. If more than one bit is set, the output
 * is the OR of the specified sources. Note interrupts sources are for the
 * raw interrupts before any masking.
 */
#define PO0                     0x3A  /* 58 (R/W) */
#define PO0_RESET               0x00
/* bits to select sources for PO0 */
#define PO0_DTB_0               0x00
#define PO0_GPO_0               0x01
#define PO0_TX245               0x02
#define PO0_IRQ_RADIOREADY      0x04
#define PO0_IRQ_HKUSERSTATUS    0x08
#define PO0_IRQ_HKUSERDATA      0x10
#define PO0_IRQ_RXNOTEMPTY      0x20
#define PO0_IRQ_TXFULL          0x40
#define PO0_IRQ_TXEMPTY         0x80

/* Source for programmable output 1. If more than one bit is set, the output
 * is the OR of the specified sources. Note interrupts sources are for the
 * raw interrupts before any masking.
 */
#define PO1                     0x3B  /* 59 (R/W) */
#define PO1_RESET               0x00
/* bits to select sources for PO1 */
#define PO1_DTB_1               0x00
#define PO1_GPO_1               0x01
#define PO1_RX_MODE_B_102       0x02  /* ZL70102 */
#define PO1_TX_MODE_B           0x04
#define PO1_IRQ_RADIOREADY      0x08
#define PO1_IRQ_LINKREADY       0x10
#define PO1_IRQ_RXNOTEMPTY      0x20
#define PO1_IRQ_TXFULL          0x40
#define PO1_IRQ_TXHALFEMPTY_102 0x80  /* ZL70102 */

/* Source for programmable output 2. If more than one bit is set, the output
 * is the OR of the specified sources. Note interrupts sources are for the
 * raw interrupts before any masking.
 */
#define PO2                     0x3C  /* 60 (R/W) */
#define PO2_RESET               0x00
/* bits to select sources for PO1 */
#define PO2_DTB_2               0x00
#define PO2_GPO_2               0x01
#define PO2_RX_MODE             0x02
#define PO2_TX_MODE_102         0x04  /* ZL70102 */
#define PO2_IRQ_TXBUFFOVERFLOW  0x08
#define PO2_IRQ_CLOST           0x10
#define PO2_IRQ_RADIOFAIL       0x20
#define PO2_IRQ_LINKQUAL        0x40
#define PO2_IRQ_SPIERROR        0x80

/* Source for programmable output 3. If more than one bit is set, the output
 * is the OR of the specified sources.
 */
#define PO3                     0x3D  /* 61 (R/W) */
#define PO3_RESET               0x00
/* ZL70101 or ZL70102 */
#define PO3_DTB_3               0x00
#define PO3_GPO_3               0x01
#define PO3_CLK_6_MHZ           0x08
#define PO3_CLK_800_KHZ         0x10
#define PO3_CLK_400_KHZ         0x20
#define PO3_CLK_300_KHZ         0x80
#define PO3_CLK_200_KHZ         0x40
/* ZL70102 only */
#define PO3_TX_BEGIN_102        0x04  /* ZL70102 */
#define PO3_CLK_12_MHZ_102      0x18  /* ZL70102 */
#define PO3_CLK_3_MHZ_102       0xC8  /* ZL70102 */
#define PO3_CLK_2P4_MHZ_102     0x88  /* ZL70102, 2.4 MHz */
#define PO3_CLK_1P2_MHZ_102     0x48  /* ZL70102, 1.2 MHz */
#define PO3_CLK_600_KHZ_102     0x28  /* ZL70102 */
#define PO3_CLK_150_KHZ_102     0xF8  /* ZL70102 */

/* General purpose output values. The value written to each bit here can be
 * routed to a programmable output pin (see PO0, PO1, etc.).
 */
#define GPO        0x3E  /* 62 (R/W) */
#define GPO_MASK   0x0F
#define GPO_RESET  0x00
/* bits for general purpose output values (each output value = 0 or 1) */
#define GPO_0      0x01  /* bit value can be routed to PO0 */
#define GPO_1      0x02  /* bit value can be routed to PO1 */
#define GPO_2      0x04  /* bit value can be routed to PO2 */
#define GPO_3      0x08  /* bit value can be routed to PO3 */

/* General purpose inputs. This can be read to get the input on the
 * programmable input pins (PI0, PI1, and PI2). Each bit corresponds to a pin.
 */
#define GPI        0x3F  /* 63 (R) */
#define GPI_MASK   0x07
#define GPI_RESET  0x00
/* bits for general purpose inputs */
#define GPI_PI0    0x01
#define GPI_PI1    0x02
#define GPI_PI2    0x04

/* Company ID for 2.45 GHz wakeup. Note if the MICS chip is awakened by 2.45
 * GHz wakeup, it will copy WAKEUP_COMPANYID (preserved in the wakeup block)
 * to MAC_COMPANYID. In contrast, if direct wakeup is used, or the chip is
 * reset, MAC_COMPANYID will be 0.
 */
#define WAKEUP_COMPANYID        0x41  /* 65 (R/W) */
#define WAKEUP_COMPANYID_RESET  0x00

/* IMD transceiver ID for 2.45 GHz wakeup (implant ID; 24 bits; 1 = LSB,
 * 3 = MSB). Note if the MICS chip is awakened by 2.45 GHz wakeup, it will copy
 * WAKEUP_IMDTRANSID1/2/3 (preserved in wakeup block) to MAC_IMDTRANSDID1/2/3.
 * In contrast, if direct wakeup is used, or the chip is reset,
 * MAC_IMDTRANSID1/2/3 will be 0.
 */
#define WAKEUP_IMDTRANSID1        0x42  /* 66 (R/W) */
#define WAKEUP_IMDTRANSID1_RESET  0x00
#define WAKEUP_IMDTRANSID2        0x43  /* 67 (R/W) */
#define WAKEUP_IMDTRANSID2_RESET  0x00
#define WAKEUP_IMDTRANSID3        0x44  /* 68 (R/W) */
#define WAKEUP_IMDTRANSID3_RESET  0x00

/* The MICS channel extracted from the 2.45 GHz wakeup signal (read-only). For
 * a direct wakeup, this defaults to 5.
 */
#define WAKEUP_CHANNEL            0x45  /* 69 (R) */
#define WAKEUP_CHANNEL_MASK       0x0F
#define WAKEUP_CHANNEL_RESET      0

/* TX/RX modulation and user wakeup data extracted from the 2.45 GHz wakeup
 * signal (read-only). The bits are the same as for MAC_MODUSER, except for
 * WAKEUP_IBS_FLAG. If the MICS chip is awakened by 2.45 GHz wakeup, it will
 * clear WAKEUP_IBS_FLAG, extract the TX and RX modulation settings from the
 * 2.45 GHz signal, swap them, place them in WAKEUP_MODUSER[6:0], and copy them
 * to MAC_MODUSER. In contrast, if direct wakeup is used, the MICS chip will
 * set WAKEUP_IBS_FLAG and clear all other bits. The chip always copies
 * WAKEUP_IBS_FLAG to the IBS_FLAG in INITCOM (0 for 2.45 GHz wakeup, 1 for
 * direct wakeup).
 */
#define WAKEUP_MODUSER        0x46  /* 70 (R) */
#define WAKEUP_MODUSER_MASK   0xFF
#define WAKEUP_MODUSER_RESET  0x00
/* bit for IBS flag (all other bits are the same as for MAC_MODUSER) */
#define WAKEUP_IBS_FLAG       0x80

/* For the ZL70102, this is the device mode. This controls the startup
 * behavior of the ZL70102 when it's awakened.
 */
#define DEVICEMODE_102        0x47  /* ZL70102: 71 (R/W) */
#define DEVICEMODE_MASK_102   0x07
#define DEVICEMODE_RESET_102  0x00
/* control bits */
#define FAST_STARTUP          0x01
#define RSSI_MODE             0x02
#define USE_TRIM_SET_2        0x04
#define DISAB_COMPANY_ID      0x08

/* For the ZL70102, this is control register 1 for the wakeup RX (2.45 GHz).
 */
#define RX_CTRL1_102        0x48  /* ZL70102: 72 (R/W) */
#define RX_CTRL1_MASK_102   0xFF
#define RX_CTRL1_RESET_102  0x00

/* Wakeup control register.
 */
#define WAKEUP_CTRL                   0x4A  /* 74 (R/W) */
#define WAKEUP_CTRL_MASK              0xFF
#define WAKEUP_CTRL_RESET_102         0x87  /* ZL70102 */
/* control bits */
#define ENAB_INT_STROBE_OSC           0x01
#define ADD_MIN_VDDA_LOAD             0x02
#define ADD_MIN_VDDD_LOAD             0x04
#define LOOPBACK_245_GHZ              0x08
#define CLAMP_2V                      0x10
#define CLEAR_DROPOUT_COUNTERS        0x20
#define ENAB_245_GHZ_TEST_REGS        0x40
#define AUTO_MIN_VDDA_VDDD_LOADS_102  0x80  /* ZL70102 */

/* Antenna 2.45 GHz matching network register.
 */
#define ANT245TUNE            0x4D  /* 77 (R/W) */
#define ANT245TUNE_MASK       0x0F
#define ANT245TUNE_RESET_102  0x00  /* ZL70102 */

/* For the ZL70102, this controls the gain for the wakeup RX RSSI (2.45 GHz).
 */
#define RSSIGAIN_102        0x50  /* ZL70102: 80 (R/W) */
#define RSSIGAIN_MASK_102   0x03
#define RSSIGAIN_RESET_102  0x01

/* For the ZL70102, this controls the 2.45 GHz wakeup receiver performance.
 */
#define LNAFREQ1_102        0x51  /* ZL70102: 81 (R/W) */
#define LNAFREQ1_MASK_102   0x1F
#define LNAFREQ1_RESET_102  0x0F

/* Register to control the start-up time for the crystal oscillator (the time
 * from enabling the XO to starting the MAC).
 */
#define XOSU        0x54  /* 84 (R/W) */
#define XOSU_MASK   0xFF
#define XOSU_RESET  0x8C

/* Register to control wakeup CRC check.
 */
#define CRCCTRL        0x56  /* 86 (R/W) */
#define CRCCTRL_MASK   0x7F
#define CRCCTRL_RESET  0x00
/* bits to control wakeup CRC (W = writable, R = read-only) */
#define CALC_CRC       0x01  /* W */
#define CHECK_CRC      0x02  /* W */
#define CALC_CRC_DONE  0x04  /* R */
#define CRC_NOT_OK     0x08  /* R */
#define CRC_OK         0x10  /* R */
#define WU_EN_PIN      0x20  /* R */
#define IBS_PIN        0x40  /* R */

/* For the ZL70102, this is the trim code for the capacitors in the 2.45 GHz
 * wakeup detector. When the ZL70102 wakes up, it copies the value contained
 * in SYNTH_CTRIM[5:1] to RX_CTRIM_102[4:0].
 */
#define RX_CTRIM_102        0x59  /* ZL70102: 89 (R/W) */
#define RX_CTRIM_MASK_102   0x1F
#define RX_CTRIM_RESET_102  0x0A

/* For the ZL70102, these are the trim registers for VDDA and VDDD.
 */
#define VDDATRIM_102        0x5A  /* ZL70102: 90 (R/W) */
#define VDDATRIM_MASK_102   0x1F
#define VDDATRIM_RESET_102  0x0F
/**/
#define VDDDTRIM_102        0x5B  /* ZL70102: 91 (R/W) */
#define VDDDTRIM_MASK_102   0x1F
#define VDDDTRIM_RESET_102  0x0F

/* Output buffer control register for TESTIO pins. If a bit is 1, it enables
 * the output buffer for the corresponding TESTIO pin.
 */
#define TESTIOBUFEN         0x5C  /* 92 (R/W) */
#define TESTIOBUFEN_MASK    0x3F
#define TESTIOBUFEN_RESET   0x00
/* bits to enable the output buffer for each TESTIO pin */
#define TEST_IO_1_BUF_ENAB  0x01
#define TEST_IO_2_BUF_ENAB  0x02
#define TEST_IO_3_BUF_ENAB  0x04
#define TEST_IO_4_BUF_ENAB  0x08
#define TEST_IO_5_BUF_ENAB  0x10
#define TEST_IO_6_BUF_ENAB  0x20
/* default mask to enable TESTIO ouput buf(s) for external RSSI */
#ifndef TEST_IO_EXT_RSSI_BUF_ENAB
#define TEST_IO_EXT_RSSI_BUF_ENAB  (TEST_IO_5_BUF_ENAB | TEST_IO_6_BUF_ENAB)
#endif

/* Clamp control register for TESTIO pins. If a bit is 1, it enables the use
 * of the corresponding TESTIO pin by disabling its internal pull-down.
 */
#define TESTIOCLAMP        0x5D  /* 93 (R/W) */
#define TESTIOCLAMP_MASK   0x7F
#define TESTIOCLAMP_RESET  0x00
/* bits to enable the use of each TESTIO pin (1 = disable pull-down) */
#define TEST_IO_1_ENAB     0x01
#define TEST_IO_2_ENAB     0x02
#define TEST_IO_3_ENAB     0x04
#define TEST_IO_4_ENAB     0x08
#define TEST_IO_5_ENAB     0x10
#define TEST_IO_6_ENAB     0x20
/* bit to enable RF signals to TESTIO[5,6] (for external RSSI) */
#define TEST_IO_RF_ENAB    0x40

/* Core reset register (write 'r' to reset MAC and RF subsystems).
 */
#define CORE_RESET_ADDRESS   0x5E  /* 94 (X) */
#define CORE_RESET           'r'

/* Chip reset register (write 'R' to reset whole chip).
 */
#define CHIP_RESET_ADDRESS   0x5F  /* 95 (X) */
#define CHIP_RESET           'R'

/* TX IF control register.
 */
#define TXIFCTRL                     0x61  /* 97 (R/W) */
#define TXIFCTRL_MASK                0x1F
#define TXIFCTRL_RESET               0x00
/* miscellaneous TX IF control */
#define TX_IF_BLOCK_ENAB             0x01
#define TX_IF_OSC_OUTPUT_ENAB        0x02

/* For the ZL70102, this is the PA input amplitude and mixer control register.
 */
#define TXRF_SEL_CTRL_102        0x62  /* ZL70102: 98 (R/W) */
#define TXRF_SEL_CTRL_MASK_102   0xFF
#define TXRF_SEL_CTRL_RESET_102  0xFB
/* various power amplifier control bits */
#define PA_ENAB_I_PHASE          0x01
#define PA_ENAB_Q_PHASE          0x02
#define PA_LINEAR_MODE           0x04
/* power amplifier input amplitude drive level */
#define PA_INPUT_LEV_MASK        0xF8

/* TX power output trimming register.
 */
#define TXRFPWRDEFAULTSET            0x63  /* 99 (R/W) */
#define TXRFPWRDEFAULTSET_MASK_102   0xFF  /* ZL70102 */
#define TXRFPWRDEFAULTSET_RESET_102  0x38

/* RSSI trim.
 */
#define RSSITRIM_102    0x64  /* ZL70102: 100 (R/W) */
#define RSSITRIM_MASK   0x0F
#define RSSITRIM_RESET  0x07

/* RX LNA gain trimming register.
 */
#define RXRFLNAGAINTRIM        0x68  /* 104 (R/W) */
#define RXRFLNAGAINTRIM_MASK   0xFF
#define RXRFLNAGAINTRIM_RESET  0xFF

/* RX bias current register (reg_rf_rxbiastr).
 */
#define RXBIASTR               0x69  /* 105 (R/W) */
#define RXBIASTR_MASK          0xFF
#define RXBIASTR_RESET         0x6F
/* bits for RX RF mixer bias setting */
#define RF_MIXER_BIAS_MASK     0x03
#define RF_MIXER_BIAS_0        0x00
#define RF_MIXER_BIAS_1        0x01
#define RF_MIXER_BIAS_2        0x02
#define RF_MIXER_BIAS_3        0x03
/* bits for RX RF LNA bias setting */
#define RF_LNA_BIAS_MASK       0x0C
#define RF_LNA_BIAS_0          0x00
#define RF_LNA_BIAS_1          0x04
#define RF_LNA_BIAS_2          0x08
#define RF_LNA_BIAS_3          0x0C
/* bits for RX IF detector filter bias setting */
#define IF_DET_FILT_BIAS_MASK  0x30
#define IF_DET_FILT_BIAS_0     0x00
#define IF_DET_FILT_BIAS_1     0x10
#define IF_DET_FILT_BIAS_2     0x20
#define IF_DET_FILT_BIAS_3     0x30
/* bits for RX IF filter bias setting */
#define IF_FILT_BIAS_MASK      0xC0
#define IF_FILT_BIAS_0         0x00
#define IF_FILT_BIAS_1         0x40
#define IF_FILT_BIAS_2         0x80
#define IF_FILT_BIAS_3         0xC0

/* RX block control register. Setting a bit enables the corresponding block
 * of the 400 MHz receiver.
 */
#define RXGENCTRL        0x6A  /* 106 (R/W) */
#define RXGENCTRL_MASK   0x7F
#define RXGENCTRL_RESET  0x03
/* bits to enable blocks of 400 MHz receiver */
#define RX_DC_REMOVAL    0x01
#define RX_NORMAL_INPUT  0x02
#define RX_IF_LOOPBACK   0x04
#define RX_4FSK          0x08
#define RX_IF_XO_TUNE    0x10
#define RX_INT_RSSI      0x20
#define RX_EXT_RSSI      0x40

/* RX ADC decision level register.
 */
#define RXIFADCDECLEV        0x6B  /* 107 (R/W) */
#define RXIFADCDECLEV_MASK   0x1F
#define RXIFADCDECLEV_RESET  0x10

/* RX-IF filter tuning register.
 */
#define RXIFFILTERTUNE        0x6D  /* 109 (R/W) */
#define RXIFFILTERTUNE_MASK   0x2F
#define RXIFFILTERTUNE_RESET  0x20

/* RF general enables. Setting a bit enables the corresponding block of
 * the 400 MHz transceiver.
 */
#define RF_GENENABLES        0x6E  /* 110 (R/W) */
#define RF_GENENABLES_MASK   0x1F
#define RF_GENENABLES_RESET  0x00
/* bits to enable blocks of 400 MHz transceiver */
#define SYNTH                0x01
#define TX_IF                0x02
#define TX_RF                0x04
#define RX_IF                0x08
#define RX_RF                0x10
#define RSSI_RX_IF_102       0x20  /* ZL70102 */

#define RX_MODE				 0x19
#define TX_MODE				 0x07

/* Synthesizer channel selection register.
 */
#define SYNTH_CHANNEL            0x6F  /* 111 (R/W) */
#define SYNTH_CHANNEL_MASK       0x0F
#define SYNTH_CHANNEL_RESET_102  0x05  /* ZL70102 */

/* For the ZL70102, this is the TX power ramp time constant capacitance trim.
 */
#define RAMP_CTRIM_102        0x71  /* ZL70102: 113 (R/W) */
#define RAMP_CTRIM_MASK_102   0x07
#define RAMP_CTRIM_RESET_102  0x03

/* Trim register for 24 MHz crystal oscillator.
 */
#define XO_TRIM        0x72  /* 114 (R/W) */
#define XO_TRIM_MASK   0x3F
#define XO_TRIM_RESET  0x14

/* Control register for general purpose ADC.
 */
#define ADCCTRL                        0x74  /* 116, (R/W) */
#define ADCCTRL_MASK                   0x1F
#define ADCCTRL_RESET                  0x00
/* miscellaneous control bits */
#define ADC_ENAB                       0x01
#define ADC_START                      0x02
/* bits to select input */
#define ADC_INPUT_MASK                 0x1C
#define ADC_INPUT_TEST_IO_1            0x00
#define ADC_INPUT_TEST_IO_2            0x04
#define ADC_INPUT_TEST_IO_3            0x08
#define ADC_INPUT_TEST_IO_4            0x0C
#define ADC_INPUT_VSUP                 0x10
#define ADC_INPUT_400_MHZ_TX_PEAK      0x14  /* also see ANTMATCHSEL_102 */
#define ADC_INPUT_400_MHZ_RX_RSSI      0x18
#define ADC_INPUT_245_GHZ_RX_RSSI_102  0x1C  /* ZL70102 */
/* default input for external RSSI on a base station */
#ifndef ADC_INPUT_EXT_RSSI
#define ADC_INPUT_EXT_RSSI             ADC_INPUT_TEST_IO_1
#endif
/*
 * Output register for general purpose ADC.
 */
#define ADCOUTPUT        0x75  /* 117 (R) */
#define ADCOUTPUT_MASK   0x3F
#define ADCOUTPUT_RESET  0x00
/* bit indicating if ADC conversion is complete */
#define ADC_COMPLETE     0x20
/* bits for ADC result */
#define ADC_RESULT_MASK  0x1F

/* TX-IF amplitude control register (reg_rf_txifamp).
 */
#define TXIFAMP                    0x76  /* 118 (R/W) */
#define TXIFAMP_MASK_102           0xFF  /* ZL70102 */
#define TXIFAMP_RESET_102          0x01
/* bit for TX-IF frequency deviation (modulation depth) */
#define TX_IF_FREQ_DEV_MASK        0x01
#define TX_IF_FREQ_DEV_STANDARD    0x01
#define TX_IF_FREQ_DEV_90_PERCENT  0x00
/* bits for TX-IF output amplitude */
#define TX_IF_AMP_MASK             0x0E
#define TX_IF_AMP_500_MV           0x00
#define TX_IF_AMP_600_MV           0x02
#define TX_IF_AMP_700_MV           0x04
#define TX_IF_AMP_800_MV           0x06
#define TX_IF_AMP_900_MV           0x08
#define TX_IF_AMP_1000_MV          0x0A
#define TX_IF_AMP_1100_MV          0x0C
#define TX_IF_AMP_1200_MV          0x0E

/* Loop filter capacitor trim value.
 */
#define SYNTH_CTRIM        0x78  /* 120 (R/W) */
#define SYNTH_CTRIM_RESET  0x1A
/* bits containing the trim value */
#define SYNTH_CTRIM_MASK   0x3F
/* bit indicating the trim value is ready (trimming successful) */
#define SYNTH_CTRIM_READY  0x40

/* Synthesizer coarse tuning register.
 */
#define SYNTH_CT        0x79  /* 121 (R/W) */
#define SYNTH_CT_RESET  0x08
/* bits containing the synthesizer coarse tuning value */
#define SYNTH_CT_MASK   0x0F
/* bit to tell chip to use specified value instead of doing coarse tuning */
#define SYNTH_CT_READY  0x10


/* Revision number of the MICS chip. Note the ZL70100 didn't officially have
 * this register, but this address always reads 0 on a ZL70100, so you can
 * still reference it if desired.
 */
#define MICS_REVISION  0x7F  /* 127 (R) */
/* MICS chip revisions */
#define ZL70100        0
#define ZL70101        1
#define ZL70102        4

/**************************************************************************
 * Defines for page 2 registers in the ZL7010X (register addresses & related).
 * Note these addresses have MICS_PAGE_2 set (0x80) to tell the ADK software to
 * access page 2 instead of page 1. As a result, the address range for page 2
 * registers is 128 to 255. The ADK software handles the paging automatically.
 * Note for the ZL70101, page 2 is also called the test page.
 */
 
/* For the ZL70102, this is the training word used at the beginning
 * of each packet for the 4FSK modulation mode.
 */
#define TRAINWORD_102        0x80+0x11  /* ZL70102: 2-17 (R/W) */ 
#define TRAINWORD_MASK_102   0xFF
#define TRAINWORD_RESET_102  0xD8

/* For the ZL70102, this is the training word used at the beginning
 * of each packet for the 2FSK modulation modes.
 */
#define TRAINWORD2_102        0x80+0x12  /* ZL70102: 2-18 (R/W) */ 
#define TRAINWORD2_MASK_102   0xFF
#define TRAINWORD2_RESET_102  0xAA


/* For the ZL70102, allows for test/wake-up setups.
 */
#define MAC_SETTINGS				0x80+0x13
#define CONTINUOUS_TX				0x01
#define WHITENING_CONTINUOUS_TX		0x02
#define ENABLE_4FSK_DUMMY_HEADERS	0x04
#define ENABLE_PROLONGING_RX		0x08

/* Registers to select calibrations.
 * 
 * Defines to select group 1 calibrations. These correspond to the bits in the
 * calselect1 register:
 * 
 * CAL_WAKEUP_STROBE_OSC:
 *     Wakeup strobe oscillator tuning (done automatically at startup).
 * CAL_TX_IF_OSC:
 *     TX IF oscillator tuning (done automatically at startup).
 * CAL_FM_DETECT_AND_RX_IF:
 *     FM detector filter & RX IF filter tuning (done automatically at startup).
 * CAL_RX_ADC:
 *     RX ADC trimming (done automatically at startup).
 * CAL_400_MHZ_TX_102 (ZL70102):
 *     High level antenna tuning for 400 MHz TX.
 * CAL_400_MHZ_RX_102 (ZL70102):
 *     High level antenna tuning for 400 MHz RX.
 * CAL_245_GHZ_ANT_101 (ZL70101):
 *     2.45 GHz antenna tuning.
 * CAL_245_GHZ_ZERO_LEV_101 (ZL70101):
 *     2.45 GHz zero level trimming.
 * CAL_400_MHZ_ANT:
 *     400 MHz antenna tuning (also see ANTMATCHSEL_101/102).
 * CAL_24_MHZ_CRYSTAL_OSC:
 *     24.0 MHz crystal oscillator tuning.
 * 
 * Defines to select group 2 calibrations. These correspond to the bits in
 * the calselect2 register, except they are left-shifted 8 bits so they can
 * be OR'ed with the group 1 defines to form a 16 bit value (the lower 8 bits
 * apply to calselect1, and the upper 8 bits apply to calselect2).
 * 
 * CAL_2_MHZ_OSC_102 (ZL70102):
 *     For the 2.45 GHz receiver, calibrate frequency of 2MHz oscillator.
 * CAL_LNA_FREQ_102 (ZL70102):
 *     For the 2.45 GHz receiver, optimize the gain in the LNA for the
 *     frequency being used.
 */
#define CALSELECT1_102            0x80+0x14  /* ZL70102: 2-20 (R/W) */
#define CALSELECT2_102            0x80+0x15  /* ZL70102: 2-21 (R/W) */
#define CALSELECT1_RESET          0x0F
#define CALSELECT2_RESET          0x00
/* defines to select group 1 cals */
#define CAL_WAKEUP_STROBE_OSC     0x01
#define CAL_TX_IF_OSC             0x02
#define CAL_FM_DETECT_AND_RX_IF   0x04
#define CAL_RX_ADC                0x08
#define CAL_400_MHZ_TX_102        0x10  /* ZL70102 */
#define CAL_400_MHZ_RX_102        0x20  /* ZL70102 */
#define CAL_400_MHZ_ANT           0x40
#define CAL_24_MHZ_CRYSTAL_OSC    0x80
/* defines to select group 2 cals (shifted to OR with group 1 defines) */
#define CAL_2_MHZ_OSC_102         (0x01 << 8)  /* ZL70102 */
#define CAL_LNA_FREQ_102          (0x02 << 8)  /* ZL70102 */
/* value to select no calibrations */
#define CAL_NONE                  0x00

/* Antenna matching selection register. This tells the 400 MHz antenna
 * calibration (CAL_400_MHZ_ANT) which capacitor to vary and which peak
 * detector to sample (via internal ADC). Note this also selects which peak
 * peak detector is routed to the ADC_INPUT_400_MHZ_TX_PEAK input on the
 * internal ADC (see ADCCTRL).
 */
#define ANTMATCHSEL_102          0x80+0x16  /* ZL70102: 2-22 (R/W) */
#define ANTMATCHSEL_MASK         0x0F
#define ANTMATCHSEL_RESET        0x00
/* bits to select capacitor to vary */
#define CAP_MASK                 0x03  /* 0011 */
#define CAP_RF_TX                0x00  /* 0000 */
#define CAP_MATCH1               0x01  /* 0001 */
#define CAP_MATCH2               0x02  /* 0010 */
/* bits to select peak detector */
#define PEAK_DET_MASK            0x0C  /* 1100 */
#define PEAK_DET_RF_TX           0x00  /* 0000 */ 
#define PEAK_DET_MATCH1          0x04  /* 0100 */ 
#define PEAK_DET_MATCH2          0x08  /* 1000 */
/* defines to select same setting for capacitor and peak detector */
#define CAP_AND_PEAK_DET_MASK    (0x0F)
#define CAP_AND_PEAK_DET_RF_TX   (CAP_RF_TX | PEAK_DET_RF_TX)
#define CAP_AND_PEAK_DET_MATCH1  (CAP_MATCH1 | PEAK_DET_MATCH1)
#define CAP_AND_PEAK_DET_MATCH2  (CAP_MATCH2 | PEAK_DET_MATCH2)


/* High-level antenna tuning algorithm selection register
 */
#define ANTTUNESEL1_102		(0x80+0x17)

#define ANTTUNE_PEAK_TX		0x00
#define ANTTUNE_PEAK_MATCH1	0x40
#define ANTTUNE_PEAK_MATCH2	0x80
#define ANTTUNE_PEAK_RX		0xc0

#define ANTTUNE_ITERATIONS_0	(0 << 3)
#define ANTTUNE_ITERATIONS_1	(1 << 3)
#define ANTTUNE_ITERATIONS_2	(2 << 3)
#define ANTTUNE_ITERATIONS_3	(3 << 3)
#define ANTTUNE_ITERATIONS_4	(4 << 3)
#define ANTTUNE_ITERATIONS_5	(5 << 3)
#define ANTTUNE_ITERATIONS_6	(6 << 3)
#define ANTTUNE_ITERATIONS_7	(7 << 3)

#define ANTTUNE_CAP_TX			0x01
#define ANTTUNE_CAP_MATCH1		0x02
#define ANTTUNE_CAP_MATCH2		0x04

/* High-level antenna tuning algorithm channel selection register
 */
#define ANTTUNESEL2_102		(0x80+0x18)

#define ANTTUNE_CHANNEL_0		(1 << 0)
#define ANTTUNE_CHANNEL_1		(1 << 1)
#define ANTTUNE_CHANNEL_2		(1 << 2)
#define ANTTUNE_CHANNEL_3		(1 << 3)
#define ANTTUNE_CHANNEL_4		(1 << 4)
#define ANTTUNE_CHANNEL_5		(1 << 5)
#define ANTTUNE_CHANNEL_6		(1 << 6)
#define ANTTUNE_CHANNEL_7		(1 << 7)

/* High-level antenna tuning algorithm configuration and channel selection register
 */
#define ANTTUNESEL3_102		(0x80+0x19)

#define ANTTUNE_DELAY_15US		(0 << 5)
#define ANTTUNE_DELAY_30US		(1 << 5)
#define ANTTUNE_DELAY_60US		(2 << 5)
#define ANTTUNE_DELAY_100US	(3 << 5)

#define ANTTUNE_ALG_EXTENDED	(7 << 2)
#define ANTTUNE_ALG_STANDARD	(0 << 2)

#define ANTTUNE_CHANNEL_8		(1 << 0)
#define ANTTUNE_CHANNEL_9		(1 << 1)

/* High-level antenna tuning algorithm initial step size
 */
#define ANTTUNESTEP_102		(0x80+0x20)


/* Clock recovery control, control for PO4, etc.
 */
#define CLKRECCTRL_102          0x80+0x1F  /* ZL70102: 2-31 (R/W) */
#define CLKRECCTRL_MASK_102     0xFF
#define CLKRECCTRL_RESET        0x09
/* bits to control clock recovery */
#define CLK_RECOV_MASK          0x07
#define CLK_RECOV_DIGITAL_FILT  0x01
#define CLK_RECOV_RX_BIT_SAMP   0x02
#define CLK_RECOV_MANUAL        0x04
/* bits to select source for PO4 (note for ZL70101, bit 3 enables PO4) */
#define PO4_SOURCE_MASK         0x78
#define PO4_TX_MODE             0x08
#define PO4_TX_MODE_B           0x18
#define PO4_RX_MODE             0x28
#define PO4_RX_MODE_B           0x38
#define PO4_PO0                 0x48
#define PO4_PO1                 0x58
#define PO4_PO2                 0x68
#define PO4_PO3                 0x78
#define PO4_TX_OR_RX_MODE_102   0x30  /* ZL70102 */
#define PO4_TX_OR_RX_MODE_B_102 0x40  /* ZL70102 */
#define PO4_TX_BEGIN_102        0x70  /* ZL70102 */
/* bit for manual reset of corr block */
#define RESET_CORR_102          0x80  /* ZL70102 */

/* Selects the TX IF frequency output to the TX IF oscillator.
 */
#define TXIFFREQ_102             0x80+0x3E  /* ZL70102: 2-62 (R/W) */
#define TXIFFREQ_MASK_102        0x1F
#define TXIFFREQ_RESET           0x00
/* bits to select TX IF frequency output to TX IF oscillator */
#define TX_IF_FREQ_OUT_MASK      0x0F
/* ZL70102: if set, bits[3:0] select TX IF frequency output to TX IF osc */
#define TX_IF_FREQ_OUT_SEL_102   0x10  /* ZL70102 */

/* For the ZL70102, this controls various test switches.
 */
#define TESTCTRL1_102        0x80+0x40  /* ZL70102: 2-64 (R/W) */
#define TESTCTRL1_MASK_102   0xFF
#define TESTCTRL1_RESET_102  0x00
/* bit to enable writes to VDDATRIM_102 and VDDDTRIM_102 */
#define ENAB_VDD_WRITE       0x80

/* For the ZL70102, this controls the RX counters for 2.45 GHz wakeup.
 */
#define RX_EN_CNT_102           0x80+0x41  /* ZL70102: 2-65 (R/W) */
#define RX_EN_CNT_MASK_102      0xFF
#define RX_EN_CNT_RESET_102     0x00
/* bits for number of re-prolonged RX enables (retries) for 2.45 GHz RX */
#define RX_245_RETRIES_MASK     0x0F
/* bits for delay from detector enable to LNA enable (not used by default) */
#define RX_245_DET_TO_LNA_MASK  0xF0

/* For the ZL70102, this the wakeup RX test control register 1 (2.45 GHz).
 */
#define RX_TESTCTRL1_102        0x80+0x46  /* ZL70102: 2-70 (R/W) */
#define RX_TESTCTRL1_MASK_102   0xFF
#define RX_TESTCTRL1_RESET_102  0x00

/* For the ZL70102, this controls the wakeup LNA bias.
 */
#define LNABIAS_102        0x80+0x49  /* ZL70102: 2-73 (R/W) */
#define LNABIAS_MASK_102   0x0F
#define LNABIAS_RESET_102  0x0A

/* For the ZL70102, this controls the coarse offset trim for the 2.45 GHz
 * wakeup detector.
 */
#define DET_IOSTRIM_102        0x80+0x54  /* ZL70102: 2-84 (R/W) */
#define DET_IOSTRIM_MASK_102   0x3F
#define DET_IOSTRIM_RESET_102  0x20

/* For the ZL70102, this the fine trim for the 2.45 GHz wakeup RX RSSI offset.
 */
#define RSSIVOSTRIM_102        0x80+0x57  /* ZL70102: 2-87 (R/W) */
#define RSSIVOSTRIM_MASK_102   0x1F
#define RSSIVOSTRIM_RESET_102  0x10

/* For the ZL70102, this controls the wakeup strobe pulse width.
 */
#define STROSCPWIDTH1_102        0x80+0x5A  /* ZL70102: 2-90 (R/W) */
#define STROSCPWIDTH1_MASK_102   0x1F
#define STROSCPWIDTH1_RESET_102  0x0A

/* For the ZL70102, these control the wakeup strobe period. They are combined
 * into a 16 bit value (the first is the LSB, and the second is the MSB).
 */
#define STROSCPERIOD1_102        0x80+0x5C  /* ZL70102: 2-92 (R/W) */
#define STROSCPERIOD1_MASK_102   0xFF
#define STROSCPERIOD1_RESET_102  0xE0
/**/
#define STROSCPERIOD2_102        0x80+0x5D  /* ZL70102: 2-93 (R/W) */
#define STROSCPERIOD2_MASK_102   0xFF
#define STROSCPERIOD2_RESET_102  0x08

/* RX subsystem enable control register. This is a test register that normally
 * shouldn't be accessed. As of this writing, it's only used by 400 MHz wakeup
 * to reduce the power used to perform RSSI's on an implant.
 */
#define RXTESTENABLE        0x80+0x62  /* 2-98 (R/W) */
#define RXTESTENABLE_MASK   0xFF
#define RXTESTENABLE_RESET  0x00

/* Antenna tuning TX power output trimming register.
 */
#define TXRFPWRTUNEANTSET_102        0x80+0x76  /* ZL70102: 2-118 (R/W) */
#define TXRFPWRTUNEANTSET_RESET_102  0x38
#define TXRFPWRTUNEANTSET_MASK_102   0xFF

/* For the ZL70102, this register controls the voltage gain for the TX, Match1,
 * Match2 and RX peak detectors.
 */
#define TXPD_GAIN_CTRL_102        0x80+0x77  /* ZL70102: 2-119 (R/W) */
#define TXPD_GAIN_CTRL_RESET_102  0x00
#define TXPD_GAIN_CTRL_MASK_102   0x3F

#define TXPD_GAIN_1				0x00
#define TXPD_GAIN_2				0x01
#define TXPD_GAIN_3				0x03
#define TXPD_GAIN_4				0x07
#define TXPD_GAIN_6				0x0f
#define TXPD_GAIN_8				0x1f
#define TXPD_GAIN_12				0x3f


/* For the ZL70102, this controls the capacitor bank associated with the RFTX
 * pin used in TX mode.
 */
#define TXRFANTTUNE_TX_MODE_102        0x80+0x79  /* ZL70102: 2-121 (R/W) */
#define TXRFANTTUNE_TX_MODE_RESET_102  0x20
#define TXRFANTTUNE_TX_MODE_MASK_102   0x3F

/* For the ZL70102, this controls the capacitor bank associated with the RFTX
 * pin used in RX mode.
 */
#define TXRFANTTUNE_RX_MODE_102        0x80+0x7A  /* ZL70102: 2-122 (R/W) */
#define TXRFANTTUNE_RX_MODE_RESET_102  0x00
#define TXRFANTTUNE_RX_MODE_MASK_102   0x3F
 
/* For the ZL70102, this controls the capacitor bank associated with the RFRX
 * pin used in TX mode.
 */
#define RXRFANTTUNE_TX_MODE_102        0x80+0x7B  /* ZL70102: 2-123 (R/W) */
#define RXRFANTTUNE_TX_MODE_RESET_102  0x00
#define RXRFANTTUNE_TX_MODE_MASK_102   0x3F
 
/* For the ZL70102, this controls the capacitor bank associated with the RFRX
 * pin used in RX mode.
 */
#define RXRFANTTUNE_RX_MODE_102        0x80+0x7C  /* ZL70102: 2-124 (R/W) */
#define RXRFANTTUNE_RX_MODE_RESET_102  0x20
#define RXRFANTTUNE_RX_MODE_MASK_102   0x3F

/* For the ZL70102, this controls the capacitor bank associated with the
 * MATCH1 pin.
 */
#define ANTMATCH1_102        0x80+0x7D  /* ZL70102: 2-125 (R/W) */
#define ANTMATCH1_RESET_102  0x20
#define ANTMATCH1_MASK_102   0x3F

/* For the ZL70102, this controls the capacitor bank associated with the
 * MATCH2 pin.
 */
#define ANTMATCH2_102        0x80+0x7E  /* ZL70102: 2-126 (R/W) */
#define ANTMATCH2_RESET_102  0x20
#define ANTMATCH2_MASK_102   0x3F

/* For the ZL70102, this is the result ADC level from the high-level antenna
 * tuning algorithm.
 */
#define ANTTUNERES1_102		0x80+0x24
#define ANTTUNERES1_RESET_102	0x00
#define ANTTUNERES1_MASK_102	0x1F

/* For the ZL70102, this is the maximum ADC level seen during the high-level antenna
 * tuning algorithm.
 */
#define ANTTUNERES2_102		0x80+0x25
#define ANTTUNERES2_RESET_102	0x00
#define ANTTUNERES2_MASK_102	0x1F

/* For the ZL70102, this is the wakeup basebase gain value (default 4, set to
 * 0 for safety).
 */
#define WAKEUP_RX_BASEBAND_GAIN_SEL1	0x80+0x52

#define WAKEUP_WK_RX_CTRL2		0x80+0x56

#define WAKEUP_SENS_SEQUENCE_CTRL 0x80+0x48

#define WAKEUP_LNA_NEGTRIM_NOM1 0x80+0x4F
#define WAKEUP_LNA_NEGTRIM_NOM2 0x80+0x51
#define WAKEUP_LNABIAS_NOM1		0x80+0x4B
#define WAKEUP_LNABIAS_NOM2		0x80+0x4D

#endif 		/*ZL_BITS_H*/
