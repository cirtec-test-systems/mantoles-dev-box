/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: micsHw (MICS Hardware) module prototypes
 *
 ***************************************************************************/

#include <stdbool.h>
#include <string.h>

#include "mics.h"

#include "delay.h"
#include "EventQueue.h"
#include "Interrupt.h"
#include "mics_int.h"
#include "mics_io.h"
#include "mics_isr.h"
#include "micsspi.h"
#include "micsstate.h"
#include "PowerASIC.h"
#include "zl.h"
#include "zl_irq.h"
#include "zl_bits.h"
#include "Watchdog.h"
#include "timeout.h"
#include "trim.h"
#include "mics_mac.h"


#include "log.h"
#include "log_events.h"


#ifdef PRINT_OUTPUT
#include <stdio.h>
#endif

#define MICS_RSSI_TIMEOUT 100
#define MICS_CALMAXTIME 250

/*
 * Power up and initialize the MICS hardware.
 *
 * Public API.  See documentation in mics.h.
 */
int micsInit(bool trimListValid, TRIM_LIST const *trimList, XPG_MICS_ID const *xpgID)
{
	/*
     * Initialize the local state.
     *
     * Among other things, this sets the address width to MICS_ADDR_UNKNOWN.
     */

	micsState_Init();

    /*
     * Configure the hardware.
     */

    mics_ConfigureIO();

    /*
     * Disable the WU_EN strobe and turn the MICS power on.
     */

    if (mics_disableStrobe() < 0)
    {
#ifdef PRINT_OUTPUT
    printf("pwrCond_DisableStrobe error\n");
#endif
        return -1;
    }

    if (pwrMICSPowerOn() < 0)
    {
#ifdef PRINT_OUTPUT
    printf("pwrCond_MICSPowerOn error\n");
#endif
        return -1;
    }

    // give the chip a chance to come up
    DELAY_MS(180);

    /*
     * Set up the SPI interface and the interrupts
     */

    micsSPI_Configure();
    mics_ConfigureIRQ();

    /*
     * Initialize, then configure, the chip.
     *
     * It is not necessary to reset the chip if the power just turned on,
     * but if the power was already on (in the event of a watchdog reset or
     * when restarting via JTAG, for example), it is helpful to reset it
     * to put it in a known state.
     */

    (void) mics_Wake();
    DELAY_MS(1);	// Allow time for WU_EN low to propagate through Pluto
	mics_Reset();
    if (mics_Wake() < 0)
    {
#ifdef PRINT_OUTPUT
    	printf("mics_Wake failed\n");
#endif
        return -1;
    }
    if (mics_ConfigRevision() < 0)
    {
#ifdef PRINT_OUTPUT
    	printf("mics_ConfigRevision failed\n");
#endif
    	return -1;
    }

    mics_Config(trimListValid, trimList, xpgID);

    /*
     * Initialize the wakeup stack
     */

    mics_PrepWakeupStack();

    // TODO: It seems that this read gets a bogus value at startup
    // under some conditions. We do not use this register except to
    // write back the default value on RadioReady, so I am disabling
    // the read/write until we know how to handle this. --JA

	/*
	 * The RXIFADCDECLEV (Receiver IF ADC Decision Level) register needs to
	 * be saved now, after trim and auto-calibration, so it can be restored
	 * during each wakeup in the future. It is not saved by the on-chip wakeup
	 * registers or stack.
	 */

	//micsState_SetRxADCDecisionLevel(zl_GetRxADCDecisionLevel());


    /*
     * Put the chip to sleep
     */

    mics_Abort();

    /*
     * Request that the Power ASIC supply the wakeup strobe.
     */
	if (mics_enableStrobe() < 0)
	{
#ifdef PRINT_OUTPUT
		printf("pwrEnableStrobe error\n");
#endif
		return -1;
	}

	#ifdef VERBOSE_MICS_LOGGING
		logNormal(E_LOG_MICS_INITIALIZED, 0);
	#endif

	return 0;
}



/*
 * Send a single MICS packet.
 *
 * Public API.  See documentation in mics.h.
 */
void micsSendPacket(uint8_t const *packet, int size)
{
	uint16_t interruptState;
	int i;

	interruptState = interruptDisable();
	mics_TxStart();
	for (i = 0; i < size; i++)
	{
		zl_TxByte(*packet);
		packet++;
	}
	zl_TxFinish();
	interruptRestore(interruptState);
}


/*
 * Close a MICS session.
 *
 * Public API.  See documentation in mics.h.
 */
void micsDisconnect(void)
{
	#ifdef VERBOSE_MICS_LOGGING
		logNormal(E_LOG_MICS_DISCONNECT,0);
	#endif

	mics_Abort();
}

//These define the maximum number of times each interrupt may occur in a single call to the interrupt handler.
#define RX_NOT_EMPTY_MAX_COUNT 20
#define CLOST_MAX_COUNT 1
#define RADIO_FAIL_MAX_COUNT 1
#define RADIO_READY_MAX_COUNT 1
#define LINK_READY_MAX_COUNT 1
#define COMMAND_DONE_MAX_COUNT 1

/*
 * Public API -- see mics.h for documentation.
 */
void micsHandleInterrupt(void)
{
    uint8_t irqStat1;
    uint8_t irqStat2;
    uint8_t irqStat1After;
    uint8_t irqStat2After;
    uint16_t retryCount;

    // We have a problem where the interrupts appear not to
    // clear. A maximum number that each interrupt can occur before
    // we give up and reset the chip (dropping the session) is defined.
    // These variables track the number of times the events happened.
	unsigned int rxNotEmptyCount;
	unsigned int clostCount;
	unsigned int radioFailCount;
	unsigned int radioReadyCount;
	unsigned int linkReadyCount;
	unsigned int commandDoneCount;

	uint32_t logData;

	rxNotEmptyCount = 0;
	clostCount = 0;
	radioFailCount = 0;
	radioReadyCount = 0;
	linkReadyCount = 0;
	commandDoneCount = 0;

  /***************************************************************
   *
   * NOTE: In the new rev 2.1.0 Zarlink source code, there is an
   * inline call here to re-initialize the chip's INTERFACE_MODE
   * register.  This register controls the available max clock speed
   * for the chip's SPI interface, some other unused functions, and
   * the ability for the external device to access the registers
   * through the house-keeping (HK) interface.  Since we use the
   * default clock rate (max 2.4MHz), this re-init was not included
   * here.  May need to add it if we use a higher SPI clock rate, or
   * if we want to allow the HK write access.
   *
   ***************************************************************/

    /*
     * The MICS interrupt vector (port 1) is shared with other peripherals.
     * Before going any farther, check to see if this interrupt is for MICS,
     * and bail out if it is not.
     */
    if (!mics_GetIRQFlag())
    {
    	return;
    }

    /*
     * Check the bus for 7-bit versus 8-bit address mode.
     */
    mics_ConfigBus();

    /* Process MICS interrupts until the chip de-asserts the interrupt line.
     * That way, if an interrupt condition occurs after we read the statuses,
     * it will be detected and processed the next time through the loop.  This
     * also ensures there will always be a rising edge if an interrupt occurs
     * after we exit the ISR, so the MSP430 will always detect it and set
     * MICS_INT_FLAG again (the interrupt is edge triggered).
     */

    /* Note the interrupt flag is cleared first outside of the loop in case
     * micsIsIRQAsserted() returns false (should never happen, but it's best
     * to ensure the flag is cleared so the interrupt won't keep recurring.).
     */
    mics_ClearIRQFlag();

    //if interrupt is from mics chip, process it
    while (mics_IsIRQAsserted())
    {

        /* Get status for interrupt groups 1 & 2 from MICS chip and init
         * auxiliary status to 0 (if an interrupt flagged in group 1 or 2 has
         * an auxiliary status, it will be read from the MICS chip later).
         */
        irqStat1 = zl_GetIrqStatus1();
        irqStat2 = zl_GetIrqStatus2();

        //see if a group 1 interrupt is flagged
        if (irqStat1)
        {
            //RX buffer not empty
            if (irqStat1 & IRQ_RXNOTEMPTY)
            {
            	rxNotEmptyCount++;
            	micsISR_RxNotEmpty();
            }

            //connection lost (either IRQ_WDOG or IRQ_ABORTLINK)
            if (irqStat1 & IRQ_CLOST)
            {
				#ifdef VERBOSE_MICS_LOGGING
            		dispPutLogEvent(E_LOG_MICS_CONNECTION_LOST, 0);
				#endif

            	clostCount++;
                micsISR_ConnectionLost();
            }

            //clear any group 1 interrupt that were set in status
            // (see ZL70101 Design Manual, rev 1.1 Table 41)
            zl_AckIrqStatus1(irqStat1);
            irqStat1After = zl_GetIrqStatus1();

            // IRQs don't always clear, so we will retry a number of times before giving up.
            for(retryCount=0; (irqStat1After & irqStat1) && retryCount<MICS_ISR_CLEAR_IRQ_RETRYS; retryCount++)
			{
				DELAY_US(10);
				zl_AckIrqStatus1(irqStat1);
				irqStat1After = zl_GetIrqStatus1();
			}
        }

        //see if a group 2 interrupt is flagged
        if (irqStat2)
        {
            //radio failure (IRQ_CRCERR or IRQ_SYNTHLOCKFAIL)
            if (irqStat2 & IRQ_RADIOFAIL)
            {
				#ifdef VERBOSE_MICS_LOGGING
            		dispPutLogEvent(E_LOG_MICS_RADIO_FAILURE, 0);
				#endif

            	radioFailCount++;
                micsISR_RadioFail();
            }

            //radio ready (at end of wakeup, or after abort link)
            if (irqStat2 & IRQ_RADIOREADY)
            {
				#ifdef VERBOSE_MICS_LOGGING
            		dispPutLogEvent(E_LOG_MICS_RADIO_READY, 0);
				#endif

            	radioReadyCount++;
                micsISR_RadioReady();
            }

            //link ready (session established)
            if (irqStat2 & IRQ_LINKREADY)
            {
				#ifdef VERBOSE_MICS_LOGGING
            		dispPutLogEvent(E_LOG_MICS_LINK_READY, 0);
				#endif

            	linkReadyCount++;
            	micsISR_LinkReady();
            }

            //MAC_CTRL command complete
            if (irqStat2 & IRQ_COMMANDDONE)
            {
            	commandDoneCount++;
            	micsISR_CommandDone();

            }
            //clear any group 2 interrupts that were set in status
            // (see ZL70101 Design Manual, rev 1.1 Table 42)
            zl_AckIrqStatus2(irqStat2);
            irqStat2After = zl_GetIrqStatus2();

            // IRQs don't always clear, so we will retry a number of times before giving up.
            for(retryCount = 0; (irqStat2After & irqStat2) && retryCount < MICS_ISR_CLEAR_IRQ_RETRYS; retryCount++)
            {
            	DELAY_US(10);
            	zl_AckIrqStatus2(irqStat2);
            	irqStat2After = zl_GetIrqStatus2();
            }
        }

        // Check our occurrence limits here.
        if(	rxNotEmptyCount > RX_NOT_EMPTY_MAX_COUNT
        		|| clostCount > CLOST_MAX_COUNT
        		|| radioFailCount > RADIO_FAIL_MAX_COUNT
        		||	radioReadyCount > RADIO_READY_MAX_COUNT
        		|| linkReadyCount > LINK_READY_MAX_COUNT
        		|| commandDoneCount > COMMAND_DONE_MAX_COUNT )
        {
        	if(rxNotEmptyCount > RX_NOT_EMPTY_MAX_COUNT)
        	{
        		logData = ((uint32_t)rxNotEmptyCount << 8) | 1;
        	}
        	if(clostCount > CLOST_MAX_COUNT)
        	{
        		logData = ((uint32_t)clostCount << 8) | 2;
        	}
        	if(radioFailCount > RADIO_FAIL_MAX_COUNT)
        	{
        		logData = ((uint32_t)radioFailCount << 8) | 3;
        	}
        	if(radioReadyCount > RADIO_READY_MAX_COUNT)
        	{
        		logData = ((uint32_t)radioReadyCount << 8) | 4;
        	}
        	if(linkReadyCount > LINK_READY_MAX_COUNT)
        	{
        		logData = ((uint32_t)linkReadyCount << 8) | 5;
        	}
        	if(commandDoneCount > COMMAND_DONE_MAX_COUNT)
        	{
        		logData = ((uint32_t)commandDoneCount << 8) | 6;
        	}

        	logData |= ((uint32_t)irqStat1After << 16) | ((uint32_t)irqStat2After << 24);


        	logNormal(E_LOG_MICS_CANTCLEARIRQ, logData);


        	//Do a hard-reset of the MICS chip
        	micsShutdown();
        	dispPutSimpleEvent(E_MICS_FULL_RESET);

			// Stop processing the ISR no matter what.
        	mics_ClearIRQFlag();
        	return;
        }

        //clear the interrupt flag so interrupt won't recur when we exit ISR
        mics_ClearIRQFlag();



        /*
         * if IRQ_CLOST occurred (IRQ_WDOG) or IRQ_HKABORTLINK), stop waiting
         * for the mics chip to de-assert the interrupt.  After IRQ_CLOST on an
         * implant, the chip will never de-assert the interrupt line, so just
         * exit the ISR.  The interrupt won't recur because there is no rising
         * edge.  After the ISR exits, the abort mics function will be called by
         * micsCond_Run() to put the chip to sleep.
         *
         * This check to break out was not included in the new rev 2.1.0
         * Zarlink code, but I don't think it hurts to leave in, because what
         * other interrupts could occur if we had IRQ_CLOST?
         */
        if (irqStat1 & IRQ_CLOST)
        {
			#ifdef VERBOSE_MICS_LOGGING
        		dispPutLogEvent(E_LOG_MICS_CONNECTION_LOST, 0);
			#endif

            break;              //break out of while
        }

    }
}


/*
 * Public API -- see mics.h for documentation.
 */
int micsShutdown(void)
{
	// Turn off SPI and set the port pins low
	micsSPI_Suspend();
	mics_DeassertWU_EN();

	#ifdef VERBOSE_MICS_LOGGING
		logNormal(E_LOG_MICS_SHUTDOWN,0);
	#endif

	// Turn the power off last, to prevent parasitic powering.
	return pwrMICSPowerOff();
}


/*
 * Public API -- see mics.h for documentation.
 */
bool micsIsAddressValid(uint16_t addr)
{
	return zl_IsAddressValid(addr);
}

/*
 * Public API -- see mics.h for documentation.
 */
void micsPoke(uint8_t address, uint8_t data)
{
	micsSPI_PutWithoutCheck(address, data);
}

/*
 * Public API -- see mics.h for documentation.
 */
uint8_t micsPeek(uint8_t address)
{
	return micsSPI_Get(address);
}


/*
 * Public API -- see mics.h for documentation.
 */
void micsProcessEvent(EVENT event)
{
DISPATCH_GEN_START(micsProcessEvent)
	switch (event.eventID)
	{
	case E_MICS_ABORT:
	    mics_Abort();
		break;
	case E_MICS_TUNEUP:
		mics_TuneTxAntenna();
		break;
	default:
		break;
	}
DISPATCH_GEN_END
}


/*
 * Public API -- see mics.h for documentation
 */
bool micsIsAwake(void)
{
	return micsState_GetState() != MICS_SLEEPING;
}

/*
 * Enable or disable low-latency mode.
 *
 * Public API -- see mics.h for documentation
 */
void micsSetLowLatencyMode(bool lowLatency)
{
	(void)lowLatency;

	zl_DisablePowerSaveTimer();
}

void micsWaitForTxComplete(void)
{
	while(zl_GetBlocksToSend() > 0)
	{
	}
}

int micsReadFromBuffer(void *aBuff, int aOffset, int aMaxBytesToRead)
{
	uint16_t gie;
	uint8_t *out;
	uint8_t *maxout;
	int bytesread;

	//micsRxNotEmptyInQueue = false;

	gie = interruptDisable();

	out = ((uint8_t*)aBuff) + aOffset;
	maxout = out + aMaxBytesToRead;
	bytesread = 0;

	while(out < maxout && micsReceiveBufferStart != micsReceiveBufferEnd)
	{
		*out = micsReceiveBuffer[micsReceiveBufferStart];

		bytesread++;
		out++;
		micsReceiveBufferStart = (micsReceiveBufferStart + 1) % MICS_RX_BUFFER_SIZE;
	}

	interruptRestore(gie);

	return bytesread;
}

int micsReadBufferBytesReady()
{
	uint16_t gie;

	//micsRxNotEmptyInQueue = false;

	gie = interruptDisable();
	if(micsReceiveBufferStart <= micsReceiveBufferEnd)
	{
		interruptRestore(gie);
		return micsReceiveBufferEnd - micsReceiveBufferStart;
	}
	else
	{
		interruptRestore(gie);
		return (MICS_RX_BUFFER_SIZE - micsReceiveBufferStart) + micsReceiveBufferEnd;
	}
}

void micsDoneReadingFromBuffer()
{

	micsRxNotEmptyInQueue = false;	//enable this interrupt so we know when a tid is received
	if(zl_receiveInterruptIsDisabled)
	{
		zl_EnableRxNotEmpty();
	}
}

void micsConfigBuffers(uint8_t aBytesPerBlock, uint8_t aMaxBlocksPerPacket)
{
    micsSPI_Put(RXBUFF_BSIZE, IMPROVE_ERR_COR_102 | aBytesPerBlock);
    micsSPI_Put(TXBUFF_BSIZE, aBytesPerBlock);
    micsSPI_Put(TXBUFF_MAXPACKSIZE, aMaxBlocksPerPacket);
}

void micsManualWakeup()
{

	mics_disableStrobe();
	mics_AssertWU_EN();
}

void micsConfigureFor400mhzListen(const XPG_MICS_ID *id)
{
	// set modulation mode
	micsSPI_Put(MAC_MODUSER, TX_MOD_2FSK_FB | RX_MOD_2FSK_FB);

	// set set company id and TID
	zl_SetMacMicsID(id);

	// Configure rx block size
	micsConfigBuffers(3, MICS_MAXPACKSIZE_MAX);

	//clear rx buffer.
	micsFlushRxBuffer();

	//enable this interrupt so we know when a tid is received
    zl_EnableRxNotEmpty();

	// disable the transmitter and start listening
	micsSPI_Put(INITCOM, RX_WAKEUP_RESPONSES | IBS_FLAG | LISTEN_MODE_102 | ENAB_RESPONSES_WITH_WRONG_ADDR );

}

void micsFlushRxBuffer()
{
	//clear on-chip RX buffer
	micsSPI_PutWithoutCheck(MAC_CTRL, FLUSH_LOCAL_RX_BUF);

	//clear in-memory receive buffer
	micsReceiveBufferStart = micsReceiveBufferEnd = 0;
	micsRxNotEmptyInQueue = false;
	if(zl_receiveInterruptIsDisabled)
	{
		zl_EnableRxNotEmpty();
	}
}

void micsSetChannel(uint8_t aChan)
{
	//reset to get back to the command state

	zl_SetMacChannel(aChan);
}


void micsStop400mhzWakeupOperation()
{
	//reset INITCOM so we stop in phase 3
	micsSPI_Put(INITCOM, IBS_FLAG);

	//issue a reset.
	micsSPI_PutWithoutCheck(MAC_CTRL, ABORT_LINK);
}

//micsSleep is implemented in mics_int.c for historical reasons.

void micsSetMicsID(const XPG_MICS_ID *id)
{
	zl_SetMacMicsID(id);
}

void micsVerify400mhzWakeupChannel()
{
	//set the resend timer to the maximum value.
	micsSPI_Put(RESENDTIME, RESENDTIME_MAX);

	// set modulation mode
	micsSPI_Put(MAC_MODUSER, TX_MOD_2FSK_FB | RX_MOD_2FSK_FB);

	// Configure rx block size
	micsConfigBuffers(TXBUFF_BSIZE_MAX, MICS_MAXPACKSIZE_MAX);

	// Allow the channel to verify by putting the system in the normal
	// connection mode with the transmitter disabled. The session is
	//verified on link_ready.
	micsSPI_Put(INITCOM, LISTEN_MODE_102);
}

void mics400mhzConnect()
{
	micsSPI_Put(INITCOM, 0x00);
}


void micsSleep(void)
{
    /* if the MICS chip is already sleeping, do nothing.  This is done just to
     * be safe, in case the firmware calls micsSleep() more than once to put
     * the mics chip to sleep.
     */
    if (micsState_GetState() != MICS_SLEEPING)
    {
        //ensure MICS chip won't assert an interrupt until it is re-awakened
        zl_DisableAll();

        /* In INITCOM register, clear IBS_FLAG so the mics chip will go to sleep
         * when it executes ABORT_LINK, and clear all other bits to tell the mics
         * chip to stop any other operations.
         */
        zl_ClearInitCom();

        /* Tell the mics chip to execute the ABORT_LINK command.  Note the MICS
         * chip will immediately go to sleep.  There is no need to clear the
         * ABORT_LINK bit afterwards because it is cleared when the chip goes
         * to sleep.
         */
        zl_AbortLink();

        // Make sure the power save timer is not used during wakeup
        zl_DisablePowerSaveTimer();

        // Make sure the MICS strobe is set up
        mics_DeassertWU_EN();
        mics_enableStrobe();

        //set the state to sleeping
        micsState_SetState(MICS_SLEEPING);
    }
}


bool micsSpiTest()
{
	uint8_t test;

	micsSPI_PutWithoutCheck(HK_USERDATA, 0xAA);
	test = micsSPI_Get(HK_USERDATA);
	if(test != 0xAA)
	{
		return false;
	}

	micsSPI_PutWithoutCheck(HK_USERDATA, 0x55);
	test = micsSPI_Get(HK_USERDATA);
	if(test != 0x55)
	{
		return false;
	}

	return true;
}

uint8_t micsGetCurrentChannel()
{
	return micsSPI_Get(MAC_CHANNEL);
}

uint8_t micsMeasureRssi()
{
	// See section 12.1 of the Zarlink design manual for details.

	bool inSession;
	uint8_t adcout;
	uint8_t genenables;
	TIMEOUT timeout;

	// If we are not in session, enable the receiver. Otherwise, wait for the receiver to enable.
	inSession = false;
	if(micsState_GetState() == MICS_IN_SESSION)
	{
		inSession = true;
	}

	if(inSession)
	{
		// wait for the receiver to enable
		timeout = timeoutSet(MS(MICS_RSSI_TIMEOUT));

		do
		{
			genenables = micsSPI_Get(RF_GENENABLES);
		}
		while((genenables & RX_MODE) != RX_MODE && !timeoutIsExpired(timeout));
		if(timeoutIsExpired(timeout))
		{
			return 0;
		}

		micsSPI_Put(RF_GENENABLES, genenables | RSSI_RX_IF_102);
	}
	else
	{
		// enable the receiver
		genenables = micsSPI_Get(RF_GENENABLES);
		micsSPI_Put(RF_GENENABLES, SYNTH | RX_RF | RSSI_RX_IF_102);
	}

	//Enable the internal RSSI circuit
	micsSPI_Put(RXGENCTRL, RXGENCTRL_RESET | RX_INT_RSSI);
	DELAY_MS(5); //wait for the synthesizer to lock.

	// Enable ADC and select input to be converted
	micsSPI_Put(ADCCTRL, ADC_ENAB | ADC_INPUT_400_MHZ_RX_RSSI);
	DELAY_US(15);

	// Start conversion of selected input and read result.
	micsSPI_Put(ADCCTRL, ADC_ENAB | ADC_INPUT_400_MHZ_RX_RSSI | ADC_START);

	adcout = 0;
	timeout = timeoutSet(MS(MICS_RSSI_TIMEOUT));
	do
	{
		adcout = micsSPI_Get(ADCOUTPUT);
	}while((adcout & ADC_COMPLETE) == 0 && !timeoutIsExpired(timeout));

	// Turn off the ADC.
	micsSPI_Put(ADCCTRL, ADCCTRL_RESET);

	// Restore genenables and RSSI circuit.
	micsSPI_Put(RF_GENENABLES, genenables);
	micsSPI_Put(RXGENCTRL, RXGENCTRL_RESET);

	if(timeoutIsExpired(timeout))
	{
		return 0;
	}

	return adcout & ADC_RESULT_MASK;
}

void micsTransmitCw()
{
	// See section 12.3 of the Zarlink design manual.

	// Set TX IF modulation and output frequency
	micsSPI_Put(TXIFCTRL, TX_IF_BLOCK_ENAB);
	micsSPI_Put(TXIFFREQ_102, TX_IF_FREQ_OUT_SEL_102 | 0x0C);

	// Enable TX power amp and output of CW
	// NOTE: skip writing txrfpwrdefaultset because we use the values already there.
	micsSPI_Put(RF_GENENABLES, SYNTH);
	DELAY_MS(5); //wait for the synth to lock.
	micsSPI_Put(RF_GENENABLES, TX_MODE);
}

void micsTransmitModulated()
{
	micsSPI_Put(MAC_SETTINGS, WHITENING_CONTINUOUS_TX | CONTINUOUS_TX);
	micsSPI_Put(INITCOM, 0);
}

void micsDiagnostic24ReceiveMode()
{
	// Per Dave. From stage 3, we just turn on the receive blocks.
	micsSPI_Put(RF_GENENABLES, RX_MODE);
}


void micsDiagnostic400ReceiveMode()
{
	// Turn on the 2.45 ghz receiver This is based on p. 164 of the zarlink manual.
	// We don't enable the RSSI block because we aren't taking an RSSI measurement.
	micsSPI_Put(RX_TESTCTRL1_102, 0x01);
	micsSPI_Put(RX_CTRL1_102, 0x07);

}

static void micsMacRunCal()
{
	micsSPI_Put(MAC_CTRL, PERFORM_CALS);
}

static uint8_t micsTuneCap(MICS_CAP cap, MICS_CAP det)
{
	uint8_t result;

	// Mask to ensure a valid input
	cap = (MICS_CAP)((uint8_t)cap & CAP_MASK);
	det = (MICS_CAP)((uint8_t)det & CAP_MASK);

	// select the cap and detector.
	micsSPI_Put(ANTMATCHSEL_102, ((det << 2) & PEAK_DET_MASK) | (cap & CAP_MASK));

	// run the calibration
	mics_MacCmd(micsMacRunCal, MICS_CALMAXTIME);

	// read the result
	switch(cap)
	{
	case MICS_CAP_TXRF:
		result = micsSPI_Get(TXRFANTTUNE_TX_MODE_102);
		break;
	case MICS_CAP_ANTMATCH1:
		result = micsSPI_Get(ANTMATCH1_102);
		break;
	case MICS_CAP_ANTMATCH2:
		result = micsSPI_Get(ANTMATCH2_102);
		break;
	case MICS_CAP_RXRF:
		result = micsSPI_Get(RXRFANTTUNE_RX_MODE_102);
		break;
	}

	// return the result
	return result;
}


void micsAutotune(TRIM_LIST *trims)
{
	uint8_t calSelect;
	uint8_t txpdGain;
	uint8_t rxrf, txrf, antmatch1, antmatch2;

	//Clear these caps
	micsSPI_Put(TXRFANTTUNE_RX_MODE_102, 0);
	micsSPI_Put(RXRFANTTUNE_TX_MODE_102, 0);

	// Enable antenna network tuning
	calSelect = micsSPI_Get(CALSELECT1_102);
	micsSPI_Put(CALSELECT1_102, CALSELECT1_RESET | CAL_400_MHZ_ANT);
	txpdGain = micsSPI_Get(TXPD_GAIN_CTRL_102);

	// run the autotune
	txrf = micsTuneCap(MICS_CAP_TXRF, MICS_CAP_TXRF);
	rxrf = micsTuneCap(MICS_CAP_RXRF, MICS_CAP_RXRF);

	// We repeat these twice and take the second value
	micsSPI_Put(TXPD_GAIN_CTRL_102, 15);
	micsTuneCap(MICS_CAP_ANTMATCH1, MICS_CAP_ANTMATCH1);
	micsTuneCap(MICS_CAP_ANTMATCH2, MICS_CAP_ANTMATCH1);
	antmatch1 = micsTuneCap(MICS_CAP_ANTMATCH1, MICS_CAP_ANTMATCH1);
	antmatch2 = micsTuneCap(MICS_CAP_ANTMATCH2, MICS_CAP_ANTMATCH1);

	// clean up
	micsSPI_Put(TXPD_GAIN_CTRL_102, txpdGain);
	micsSPI_Put(CALSELECT1_102, calSelect);
	micsSPI_Put(ANTMATCHSEL_102, ANTMATCHSEL_RESET);

	// Save the wakeup stack.
    mics_PrepWakeupStack();

	// update the trim list.
	trimSetTrimValue(trims, TXRFANTTUNE_TX_MODE_102, 0, txrf);
	trimSetTrimValue(trims, TXRFANTTUNE_RX_MODE_102, 0, 0);
	trimSetTrimValue(trims, RXRFANTTUNE_TX_MODE_102, 0, 0);
	trimSetTrimValue(trims, RXRFANTTUNE_RX_MODE_102, 0, rxrf);
	trimSetTrimValue(trims, ANTMATCH1_102, 0, antmatch1);
	trimSetTrimValue(trims, ANTMATCH2_102, 0, antmatch2);
}

void micsResetWatchdog()
{
	zl_WatchdogClear();
}

int micsPollLinkStatus (uint8_t doLED)
{
    int error_sum = 0;

    // stop the error counters
    micsSPI_Put(ERRCLR_102, CLEAR_ECCERR + CLEAR_CRCERR);

    // poll the error counters
    error_sum = micsSPI_Get(CRCERR) + (micsSPI_Get(ECCERR) << 8);

    // reset and restart the error counters
    micsSPI_Put(ERRCLR_102, 0);

    if (doLED)
        if (error_sum)
            // signal errors exist
            setTST_TRIG_HIGH();
        else
            // signal no errors
            setTST_TRIG_LOW();

    return error_sum;
}
