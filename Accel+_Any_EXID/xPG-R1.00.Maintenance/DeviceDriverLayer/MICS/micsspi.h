/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: micsSPI (MICS SPI) module prototypes
 *	
 ***************************************************************************/

#ifndef MICSSPI_H_
#define MICSSPI_H_

#include <stdint.h>

void micsSPI_Configure(void);
void micsSPI_Suspend(void);
uint8_t micsSPI_Get(uint8_t registerAddress);
void micsSPI_Put(uint8_t registerAddress, uint8_t registerValue);
void micsSPI_PutWithoutCheck(uint8_t registerAddress, uint8_t registerValue);

#endif                          /*MICSSPI_H_ */
