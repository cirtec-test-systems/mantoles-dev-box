/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Driver routines for EPG LEDs
 *
 ***************************************************************************/

#ifndef LED_H_
#define LED_H_

#include <stdbool.h>

void ledInit(void);

void ledStimOn(void);
void ledStimOff(void);
bool ledIsStimLedOn(void);

void ledGreenOn(void);
void ledGreenOff(void);
bool ledIsGreenLedOn(void);

void ledRedOn(void);
void ledRedOff(void);
bool ledIsRedLedOn(void);

#endif /* LED_H_ */
