/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Header file for the flash deivce driver.
 *	
 ***************************************************************************/
#ifndef FLASH_H_
#define FLASH_H_

#include <stdint.h>
#include "Common/Protocol/response_code.h"

/// \defgroup flash	Flash
/// \ingroup deviceDriverLayer
/// @{

RESPONSE flashCopyDataToFlash(const void *pCopySource, uint16_t numSrcBytes, void *pFlashDest);
RESPONSE flashWriteWithinSegment(const void *pCopySource, uint16_t numSrcBytes, void *pFlashDest);

void flashEnableFlashAccessViolationFault(void);

/// @}

#endif                          /* FLASH_H_ */
