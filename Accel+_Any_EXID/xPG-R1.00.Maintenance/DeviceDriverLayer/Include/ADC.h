/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: ADC device driver header file
 *	
 ***************************************************************************/
#ifndef ADC_H_
#define ADC_H_

#include <stdint.h>

#include "DeviceDriverLayer/Common/msp430f2618.h"
#include "DeviceDriverLayer/Common/bits.h"

typedef enum ADC_SELECT {
// Note: These codes are the ADC channel numbers for each input.
	ADC_IMPEDANCE = 0,			///< ADC input select: STIM_IMPM signal, impedance measurement
	ADC_CALIBRATION_RESIN = 1,	///< ADC input select: STIM_RESIN signal, on-board amplitude calibrator
	ADC_CALIBRATION_RESOUT = 2,	///< ADC input select: STIM_RESOUT signal, on-board amplitude calibrator
	ADC_UNUSED_PIN = 3,			///< ADC input select: Unconnected pin (P6.3)
	ADC_POWER_ASIC = 4,			///< ADC input select: PWR_ANA_OUT signal from Power ASIC on-board analog mux
	ADC_ACC_Y = 5,				///< ADC input select: Accelerometer Y channel
	ADC_ACC_X = 6,				///< ADC input select: Accelerometer X channel
	ADC_ACC_Z = 7,				///< ADC input select: Accelerometer Z channel
	ADC_REFERENCE = 8,			///< ADC input select: Vref voltage. Note: ADC driver does not currently enable the Vref voltage.
	ADC_GROUND = 9,				///< ADC input select: Ground
	ADC_TEMPERATURE = 10,		///< ADC input select: Temperature sensor. Note: ADC driver does not currently enable the temperature sensor.
	ADC_HALF_DVDD = 11			///< ADC input select: 1/2 of xPG's DVDD supply rail.
} ADC_SELECT;

typedef enum ADC_TRIGGER {
// Note: These codes match the SHSx bits in the ADC12CTL1 register
	TRIGGER_SOFTWARE = 0,	///< Trigger the conversion with adcConvert().
	TRIGGER_TIMER_A1 = 1,	///< Trigger from Timer_A1's output unit
	TRIGGER_TIMER_B0 = 2,	///< Trigger from Timer_B0's output unit
	TRIGGER_TIMER_B1 = 3	///< Trigger from Timer_B1's output unit
} ADC_TRIGGER;

typedef enum ADC_SEQUENCE {
	CONVERT_EACH,		///< Each trigger event performs one conversion in the sequence.
	CONVERT_ALL			///< One trigger event runs the entire sequence of conversions.
} ADC_SEQUENCE;

typedef enum ADC_SPEED {
	ADC_FAST,
	ADC_SLOW
} ADC_SPEED;

#define ADC_SAMPLE_TIME_4	 (SHT0_0 | SHT1_0)
#define ADC_SAMPLE_TIME_8	 (SHT0_1 | SHT1_1)
#define	ADC_SAMPLE_TIME_16	 (SHT0_2 | SHT1_2)
#define	ADC_SAMPLE_TIME_32	 (SHT0_3 | SHT1_3)
#define	ADC_SAMPLE_TIME_64	 (SHT0_4 | SHT1_4)
#define	ADC_SAMPLE_TIME_96	 (SHT0_5 | SHT1_5)
#define	ADC_SAMPLE_TIME_128  (SHT0_6 | SHT1_6)
#define	ADC_SAMPLE_TIME_192  (SHT0_7 | SHT1_7)
#define	ADC_SAMPLE_TIME_256  (SHT0_8 | SHT1_8)
#define	ADC_SAMPLE_TIME_384  (SHT0_9 | SHT1_9)
#define	ADC_SAMPLE_TIME_512  (SHT0_10 | SHT1_10)
#define	ADC_SAMPLE_TIME_768  (SHT0_11 | SHT1_11)
#define	ADC_SAMPLE_TIME_1024  (SHT0_12 | SHT1_12)

#define MAX_ADC_RESULTS	16

typedef struct ADC_CONFIG {
	ADC_SELECT firstInput;		///< The ADC_SELECT value choosing the first input of the sequence to convert.
	int numInputs;				///< The number of inputs to convert. Range 1-11.
	int reps;					///< The number of times to convert the sequence. Range 1 to floor(MAX_ADC_RESULTS/numInputs).
	ADC_TRIGGER trigger;		///< The trigger source for the sequence
	ADC_SEQUENCE seqType;		///< Controls whether one trigger starts the whole sequence, or if one trigger is required for each measurement
	ADC_SPEED speed;			///< Controls whether a fast clock or a slow clock is used by the ADC.
	uint16_t sampleTime;
} ADC_CONFIG;


void 	adcOn();
void 	adcOff(void);
void 	adcResetReferenceCount();
void 	adcSetup(ADC_CONFIG const *config);
void 	adcConvert(void);
uint16_t const volatile *adcResults(int16_t timeout);

#endif /*ADC_H_*/
