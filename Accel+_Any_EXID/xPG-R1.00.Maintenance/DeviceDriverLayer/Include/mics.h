/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: mics device driver header file
 *
 ***************************************************************************/

#ifndef MICS_H_
#define MICS_H_

#include <stdbool.h>
#include <stdint.h>

#include "system_events.h"
#include "Common/Protocol/trim_list.h"

/// \defgroup MICS MICS
/// \ingroup deviceDriverLayer
/// @{

///This is a structure that contains the ID used in the MICS communication stream to
///identify this xPG.
typedef struct XPG_MICS_ID
{
  uint8_t micsIDmsb;         ///< msb of three-byte mics ID
  uint8_t micsIDmiddleByte;  ///< middle byte of three-byte mics ID
  uint8_t micsIDlsb;         ///< lsb of three-byte mics ID
  uint8_t micsIDCompanyId;   ///< mics company ID byte
} XPG_MICS_ID;


/**
 * Identifies a matching network capacitor for tuning.
 */
typedef enum {
	MICS_CAP_TXRF = 0,
	MICS_CAP_ANTMATCH1 = 1,
	MICS_CAP_ANTMATCH2 = 2,
	MICS_CAP_RXRF = 3
} MICS_CAP;



#define MICS_MAXPACKSIZE_MAX    31 //!< The largest allowed value for the max number of blocks per packet.

/**
 * Initializes the interface to the MICS chip (Zarlink ZL7010x),
 * the MICS chip itself and the state variables used for control
 * of MICS communication.
 *
 * This is the basic order of the initialization:
 * 	 -- Disable WU_EN strobe from Pluto
 *   -- Turn on power to radio
 *   -- SPI interface between MSP430 and MICS chip
 *   -- control signals to the MICS chip (just WU_EN)
 *   -- sub-system state (and other) variables
 *   -- direct wake-up of mics chip
 *   -- if direct wake-up successful, configuration of MICS
 *      chip registers plus auto calibrations
 *
 * After the configuration, the mics
 * chip is directed to calculate the new CRC of the registers
 * (which it uses to detect any register corruption while
 * asleep).  Finally, the chip is shut down into its low-power
 * state where it periodically checks for the 2.45 GHz wake-up
 * signal from an external controller.
 *
 * Refer to the Zarlink ZL7010x Design Manual for detailed
 * information regarding the init / configuration sequence.
 *
 * \note The packet size is not configurable.
 *
 * Puts the mics chip to sleep after initialization or after a mics
 * session.
 *
 * \param trimListValid - a boolean to indicate if the trim list (next parameter) is valid and or not.
 * \param trimList - a pointer to the trimlist to apply to the Zarlink chip.
 * \param id - the MICS ID to use
 * \return 0 for success or -1 for failure.
 */
int micsInit(bool trimListValid, TRIM_LIST const *trimList, XPG_MICS_ID const *id);

/**
 * Interrupt service routine for the Zarlink ZL7010x mics chip
 *
 * This routine is exported so that it can be called from the Port1/Port2
 * interrupt demultiplexer.
 *
 * The interrupt signal from the mics chip can be due to a large
 * number of sources, responding to different situations and
 * states of the chip and the mics link.
 * The MSP430 is signaled by a single interrupt line.  The
 * first step of the routine is to read two (or three) chip
 * registers to determine the source of the interrupt.
 * Following that, the routine is basically a big case statement...
 * routines are called based on the detected source of the interrupt.
 *
 * The routine is based on the interrupt service routine provided
 * with the Zarlink development kit.  It has been modified
 * extensively for the ipg.
 */
void micsHandleInterrupt(void);

/**
 * This function sends a MICS packet to an external device through the
 * MICS interface. Requires that the transport-layer framing is already
 * done in the buffer passed in.
 *
 * This function blocks until the packet is sent to the zarlink radio.
 * an E_MICS_PKT_SENT event is put into the event queue when packet
 * transmission is completed.
 *
 * \param packet 	Pointer to the data to send.
 * \param size 		Number of bytes to send. Must be a multiple of 15.
 */
void micsSendPacket(uint8_t const *packet, int size);

/**
 * This function returns true if the given address is a valid MICS address.
 * \param address - address of the MICS register to validate.
 */
bool micsIsAddressValid(uint16_t addr);


/**
 * Check whether the MICS chip is awake
 *
 * \return	true if the MICS chip is awake, false if it is sleeping
 * 			(and waiting for a wakeup packet)
 */
bool micsIsAwake(void);

/**
 * Close the current MICS session.
 *
 * Puts the MICS radio into its low-power sleep state.
 */
void micsDisconnect(void);

/**
 * Power down the MICS radio.
 *
 * \return 0 for success or -1 for failure.
 */
int micsShutdown(void);

/**
 * Write one byte directly to the MICS radio registers.
 *
 * \param address	The register address to write
 * \param data		The data to write
 */
void micsPoke(uint8_t address, uint8_t data);

/**
 * Read one byte directly from the MICS radio registers.
 *
 * \param address	The register address to read
 * \return			The data at the address
 */
uint8_t micsPeek(uint8_t address);

/**
 * Receive events from the event dispatcher and act on them accordingly.
 *
 * @param eventToProcess The event received from the event handler
 */
void micsProcessEvent(EVENT eventToProcess);


/**
 * Switch the MICS radio into or out of a low-latency mode, in which
 * the average round-trip time is substantially reduced but power
 * consumption significantly increased.
 */
void micsSetLowLatencyMode(bool lowLatency);


/**
 * Block until the transmit buffer is empty
 */
void micsWaitForTxComplete(void);

/**
 * Retrieves information from the MICS receive buffer. Removes those bytes from the buffer.
 * This function reads the smaller of aMaxBytesToRead and micsReadBufferBytesReady().
 * However, if an interrupt changes the value of micsReadBufferBytesReady(), while reading the new
 * value will be used.
 *
 * \note When the ZL7010x is configured to transfer 15 bytes per block, only the low-order
 * bit of the first byte in each block contains data. See the ZL70102 design manual for details.
 *
 * \warn You must call micsDoneReadingFromBuffer() to signal that you are no longer processing data from the MICS receive buffer.
 *
 * \param aBuff The buffer to read into. Ensure sizeof(aBuff) >= aMaxBytesToRead + aOffset.
 * \param aOffset The index of the first byte in aBuff to write to.
 * \param aMaxBytesToRead The maximum number of bytes to read before stopping.
 * \return The number of bytes read from the buffer.
 */
int micsReadFromBuffer(void *aBuff, int aOffset, int aMaxBytesToRead);

/**
 * Returns the number of bytes available in the MICS receive buffer.
 *
 * \warn You must call micsDoneReadingFromBuffer() to signal that you are no longer processing data from the MICS receive buffer.
 *
 */
int micsReadBufferBytesReady();

/**
 * Signals that you are done reading the MICS receive buffer. This signals to the driver
 * that you should receive a E_MICS_RX_NOTEMPTY event the next time new data is received.
 */
void micsDoneReadingFromBuffer();

/**
 * Configure the chip's block size for communication. The block size is used for both TX and RX.
 *
 * \param aBlockSize the nubmer of bytes per block. If set to 15 (the maximum) the first byte in each block only
 * 		contains 1 bit of information. See the ZL70102 documentation for details.
 * 	\param aMaxBlocksPerPacket The maximum number of blocks to transmit in a packet.
 */
void micsConfigBuffers(uint8_t aBytesPerBlock, uint8_t aMaxBlocksPerPacket);

/**
 * Disables the MICS wakeup strobe and holds the WU_EN pin high. This forces the ZL70102 to wake up
 * and go into phase 3 with the IBS flag set to 1.
 *
 * \warn you must wait for the radio_ready interrupt before sending the ZL70102 commands.
 */
void micsManualWakeup();

/**
 * Puts the chip into the correct mode to listen for 400MHz wakeup messages.
 *
 * \note This call does not set the channel. Call micsSetChannel() before calling this method.
 */
void micsConfigureFor400mhzListen(const XPG_MICS_ID *id);

/**
 * Empties the MICS on-chip RX buffer and the local receive buffer.
 */
void micsFlushRxBuffer();

/**
 * Set the channel to the specified value.
 *
 * \warn You cannot change channels after phase 3 (see ZL70102 documentation for more information).
 *
 * \param aChan the new channel.
 */
void micsSetChannel(uint8_t aChan);

/**
 * Aborts the 400mhz listen session.
 *
 * \note You must wait for radio_ready to issue new commands.
 */
void micsStop400mhzWakeupOperation();

/**
 * Put the MICS chip into a sleep state enabled to wakeup on a 2.45GHz wakeup packet.
 */
void micsSleep();

/**
 * Set the MICS chips transmission id.
 */
void micsSetMicsID(const XPG_MICS_ID *id);

/**
 * Puts the chip into a verify state. The chip should already be in
 *  the 400mhz listen state on the correct channel.
 */
void micsVerify400mhzWakeupChannel();

/**
 * Releases the MICS chip to send wakeup responses and establish a
 * link. The chip must already be configured to connect on the correct
 * channel and use the correct TID.
 */
void mics400mhzConnect();

/**
 * Does a write/read test over SPI to make sure we can talk to the MICS chip.
 * \returns true on success, false on failure.
 */
bool micsSpiTest();

/**
 * Rtreieves the current communications channel.
 */
uint8_t micsGetCurrentChannel();

/**
 * Takes an RSSI measurement. The chip must be in session, or able to receive commands over SPI.
 */
uint8_t micsMeasureRssi();

/**
 * Causes the chip to transmit in constant wave mode.
 *
 * \warn May only be called from stage 3.
 */
void micsTransmitCw();

/**
 * Causes the chip to transmit a modulated signal. No data is transmitted in this mode.
 *
 * \warn May only be called from stage 3.
 */
void micsTransmitModulated();

/**
 * Puts the MCIS radio into a receive mode. Communication cannot be performed in this mode.
 *
 * \warn May only be called from stage 3.
 */
void micsDiagnostic400ReceiveMode();

/**
 * Puts the wakeup radio into a receive mode. Communication cannot be performed in this mode.
 *
 * \warn May only be called from stage 3.
 */
void micsDiagnostic24ReceiveMode();

/**
 * Runs a calibration operation on the MICS antenna matching network.
 */
void micsAutotune();

/**
 * Resets the MICS chip's "main" 4.97s watchdog timer to keep the session alive. This only
 * needs to be called when the chip needs to remain awake when not in session.
 */
void micsResetWatchdog();

/**
 * Polls "reg_mac_crcerr" and "reg_mac_eccerr" values to determine link status.  The function
 * has the option to set TST_TRIG "high" if nonzero values appear in those registers (doLED > 0).
 * The function zeroes/resets the register counters before exiting.
 */
int micsPollLinkStatus(uint8_t doLED);

/// @}

#endif /* MICS_H_ */
