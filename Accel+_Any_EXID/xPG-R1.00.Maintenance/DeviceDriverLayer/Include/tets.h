/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: TETS communications Conductor
 * 
 ***************************************************************************/

#ifndef TETS_H_
#define TETS_H_

#include "Interrupt.h"

/// \defgroup tets TETS
/// \ingroup deviceDriverLayer
/// @{

/**
 * Initialize the TETS device driver and enable receive mode.
 */
void tetsInit(void);

/**
 * Detune the charging interface.
 */
void tetsDetune(void);

/*@unused@*/
interrupt void tetsHandleReceiveInt(void);

/*@unused@*/
interrupt void tetsHandleTransmitInt(void);

/// @}

#endif /*TETS_H_*/
