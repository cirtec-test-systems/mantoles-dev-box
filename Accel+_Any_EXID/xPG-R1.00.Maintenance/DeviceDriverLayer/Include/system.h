/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Common driver routines for EPG/IPG
 *
 ***************************************************************************/

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include <stdbool.h>

/**
 * Determine whether the system is an IPG (Implantable) or EPG (External)
 *
 * Reads register for PORT 8, Bit 7 to determine type of device
 *
 * \param none
 * \returns bool - true: It is an IPG, false: It is an EPG
 */
bool isIPG(void);

void setTST_TRIG_HIGH(void);
void setTST_TRIG_LOW(void);
void setTST_TRIG_TOGGLE(void);

void resetSystem(void);

#endif /* SYSTEM_H_ */
