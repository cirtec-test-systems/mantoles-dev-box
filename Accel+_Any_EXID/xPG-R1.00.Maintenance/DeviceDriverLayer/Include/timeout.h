/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: prototypes for timeout functions in the timer device driver.
 * 
 ***************************************************************************/

#ifndef TIMEOUT_H_
#define TIMEOUT_H_

#include <stdbool.h>
#include <stdint.h>

#include "ticks.h"

/// \defgroup timeout Timeout Functions
/// \ingroup timer
/// @{

typedef int16_t TIMEOUT;

TIMEOUT timeoutSet(int16_t ticks);
bool timeoutIsExpired(TIMEOUT t);

/// @}

#endif /*TIMEOUT_H_*/
