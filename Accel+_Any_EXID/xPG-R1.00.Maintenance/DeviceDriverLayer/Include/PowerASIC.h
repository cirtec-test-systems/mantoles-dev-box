/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Module to control the Power ASIC to power on/off
 *		various subsections of the xPG.
 *
 ***************************************************************************/

#ifndef POWERASIC_H_
#define POWERASIC_H_

#include <stdbool.h>
#include <stdint.h>

#include "Common/Protocol/trim_list.h"
#include "DeviceDriverLayer/Common/msp430f2618.h"		// For inline functions
#include "DeviceDriverLayer/Common/bits.h"				// For inline functions

/// \defgroup PowerASIC PowerASIC
/// \ingroup deviceDriverLayer
/// @{

int pwrEnableBrownOutDetection(void);
int pwrDisableBrownOutDetection(void);

// added to provide finer granularity of control over the stim asic power
int pwrSatEnableAVDD(void);
int pwrSatDisableAVDD(void);
int pwrSatEnableDVDD(void);
int pwrSatDisableDVDD(void);

int pwrMICSPowerOn(void);
int pwrMICSPowerOff(void);

int pwrLEDPowerOn(void);

int pwrEnableStrobe(void);
int pwrDisableStrobe(void);

bool pwrVerifyTrims(void);
int pluto_ApplyTrimList(TRIM_LIST const *tl);

/**
 * Turn on the buck/boost converter, which supplies the LDOs with
 * a stable power source over a wide range of battery voltages.
 * It also helps to stabilize the power supplies when stimulation
 * pulls high currents off the battery.
 */
void pwrBuckBoostOn(void);

/**
 * Turn off the buck/boost converter. The LDOs will be powered
 * directly from battery voltage, via the pass switch.
 * When running with very light loads, this saves power by not
 * powering the buck/boost.
 */
void pwrBuckBoostOff(void);


/**
 * Turns on the power supply voltage for the FRAM.
 *
 * \returns	OK_RETURN_VALUE(0) for success, or
 *          ERROR_RETURN_VALUE(-1) if there is an error.
 */
int pwrNVPowerOn(void);

/**
 * Turns off the power supply voltage for the FRAM.
 *
 * \returns	OK_RETURN_VALUE(0) for success, or
 *          ERROR_RETURN_VALUE(-1) if there is an error.
 */
int pwrNVPowerOff(void);


bool pwrIsAddressValid(uint16_t addr);

/**
 * Tell the power ASIC to enter storage mode.
 *
 * Other subsystems may need to be stopped before entering storage mode.  Higher-layer
 * code is required to take care of that is needed.
 */
void pwrEnterStorageMode(void);

int pwrInit(bool trimListValid, TRIM_LIST const *trimList);

/**
 * Handle the Port 1 interrupt signals for the Power ASIC.
 */
void pwrHandlePort1Interrupt(void);

/**
 * Handle the Port 2 interrupt signals for the Power ASIC.
 */
void pwrHandlePort2Interrupt(void);

/**
 * Read a register on the Power ASIC.
 *
 * \param address	The address to read
 * \return			The data at that address
 */
uint8_t pwrPeek(uint8_t address);

/**
 * Write a register on the Power ASIC.
 *
 * \param address
 * \param data
 * \return 0 for success or -1 for failure.
 */
int pwrPoke(uint8_t address, uint8_t data);


/*
 * Macros and inline functions
 *
 * Determine how to handle macro definitions. If compiling tests
 * (TEST defined) and using this file for mocking (TEST_INLINE_FUNCTIONS is
 * not defined), then we need to emit prototypes for mocking insted of
 * macro definitions.
 *
 * If we are not compiling tests (TEST not defined) or if the macros themselves
 * are being tested (TEST_INLINE_FUNCTIONS defined), then emit the actual
 * macros instead of the prototypes.
 */

#if defined(TEST) && !defined(TEST_INLINE_FUNCTIONS)

void pwrContinuousLDO(void);
void pwrIntermittentLDO(void);

#else

/**
 * Enable continuous LDO operation, providing full power to the MSP430
 *
 * This has to be an inline function because of critical timing requirements.
 */
/*@unused@*/
inline static void pwrContinuousLDO(void)
{
	P8OUT |= PWR_LDO_EN;
}

/**
 * Enable pulsed LDO operation, for reduced quiescent current
 *
 * This has to be an inline function because of critical timing requirements.
 */
/*@unused@*/
inline static void pwrIntermittentLDO(void)
{
	P8OUT &= ~PWR_LDO_EN;
}

#endif

int pwrSetASKPolarity(bool polarity);

//Battery interface

/**
 * This function reads and returns a battery voltage.
 * The reading can be compared to limits directly without conversion into
 * standard units.
 *
 * \param batteryAdc  - the battery voltage in ADC units to be used
 * with battery limits.
 */
int batteryGetBatteryADC(uint16_t *batteryAdc);


//Charging interface

//! These are the values that can be used to set the charge rate.
typedef enum CHARGE_RATE {
	CHARGE_RATE_C4,		//!< Charge at the high rate of C/4
	CHARGE_RATE_C8		//!< Charge at the lower rate of C/8
} CHARGE_RATE ;

//! These are returned by the charging hardware to indicate the state of the charger.
typedef enum CHARGE_STATE {
	 CHARGE_STATE_CHARGE_OFF=0,
	 CHARGE_STATE_CHARGE_PRE_CONDITION=1,
	 CHARGE_STATE_CHARGE_CONSTANT_CURRENT=2,
	 CHARGE_STATE_CHARGE_VOLTAGE=3,
	 CHARGE_STATE_CHARGE_COMPLETE=4,
	 CHARGE_STATE_DETECT_FAULT = 5,
	 CHARGE_STATE_VOLTAGE_DETECT_AND_OVERVOLT_FAULT = 6,
	 CHARGE_STATE_OVERVOLT_FAULT=7,
	 CHARGE_STATE_UNKNOWN=0xFF
}CHARGE_STATE;

/**
 * This function initializes the charging hardware.
 */
void chargeInit(void);

/**
 * This function enables the interrupt signal on the PWR_V_DET line (Rising Edge)
 * which indicates that there is a charger present.
 */
void chargeEnableChargerEngagedInterrupt(void);


/**
 * This function enables the interrupt signal on the PWR_V_DET line, but for falling edge
 * which indicates that the charger is no longer present.
 */
void chargeEnableChargerDisengagedInterrupt(void);

/**
 * This function returns the charging rate.
 */
CHARGE_RATE chargeGetChargeRate(void);

/**
 * This function sets the charging rate
 *
 * \param rate - Rate of charge, either CHARGE_RATE_C_4 or CHARGE_RATE_C_8
 *
 */
void chargeSetChargeRate(CHARGE_RATE rate);

/**
 * This function returns a bitfield with the charging state
 *
 * \return
 * This function returns a bitfield with the charging state.
 * The values are from the power ASIC.  Here are the values:
 *			000 CHARGE_OFF<p>
 *			001 CHARGE_PRE_CONDITION<p>
 *			010 CHARGE_CONSTANT_CURRENT<p>
 *			011 CHARGE_VOLTAGE<p>
 *			100 CHARGE_COMPLETE<p>
 *			101	CHARGE_STATE_DETECT_FAULT<p>
 *	 	 	110 CHARGE_STATE_VOLTAGE_DETECT_AND_OVERVOLT_FAULT<p>
 *	 	 	111 CHARGE_STATE_OVERVOLT_FAULT<p>
 */
CHARGE_STATE chargeGetChargeState(void);


/**
 * This function returns the temperature in the passed
 * parameter.
 *
 * \param return_temperature - the temperature, scaled to TBD
 * \param thermOffset - the thermistor offset reading
 * \param thermInput - the thermistor input reading
 * \param thermBias - the thermistor bias reading
 *
 * \return 0 if all is well, -1 if there is a problem with
 * reading the temperature.
 */
int chargeGetTemperature(uint16_t *return_temperature, uint16_t *thermOffset, uint16_t *thermInput, uint16_t *thermBias);

/**
 * This function returns the recharge voltage in mV in the passed
 * parameter.
 *
 * \param return_recharge - the recharge voltage in mV
 *
 * \return 0 if all is well, -1 if there is a problem
 *
 */
int chargeGetRechargeVoltage(uint16_t *return_recharge);

/**
 * This function returns boolean TRUE when there is a charger present.
 *
 * \return
 * 		True indicates that a charger is present. <P>
 * 		False indicates that there is no charger present.
 */
bool chargeChargerPresent(void);

/**
 * This function starts the charging process.
 */
int chargeStartChargingProcess(void);

/**
 * This function stops the charging process.
 */
int chargeStopChargingProcess(void);

/**
 * This function performs a diagnostic check on the temperature hardware.
 * If the temperature hardware passes the test, 0 is returned.  Otherwise -1.
 */
int chargeTemperatureDiagnosticCheck(void);

/**
 * This function combines the diagnostic check of the temperature hardware with
 * the acquisition and calculation of temperature.  This makes the period (every
 * 1s) check by charge manager more efficient by usin the same data for two
 * different calculations.
 */
int chargeGetTempAndDiagCheck(uint16_t *return_temperature, uint16_t *thermOffset, uint16_t *thermInput, uint16_t *thermBias);

/// @}

#endif		/* POWERASIC_H_ */
