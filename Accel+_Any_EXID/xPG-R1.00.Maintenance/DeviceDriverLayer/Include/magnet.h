/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Header file for the magnet device driver.
 *
 ***************************************************************************/
#ifndef MAGNET_H_
#define MAGNET_H_
#include <stdbool.h>

/// \defgroup magnet Magnet
/// \ingroup deviceDriverLayer
/// @{

void magnetInit(void);
bool magnetIsMagnetEngaged(void);

/// @}

#endif /*MAGNET_H_*/
