/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Non-volatile memory device driver prototypes.
 *	
 ***************************************************************************/
 
#ifndef NV_H_
#define NV_H_

#include <stddef.h>
#include <stdint.h>

/**
 * The type used for an address in NV memory.  The SCS implant has a 256 kbit
 * FRAM, so a uint16_t are sufficient, but a future device with a 1 Mbit or
 * larger FRAM will need to change this to a uint32_t along with the
 * corresponding changes in the serial protocol. 
 */
 
typedef uint16_t NVADDR;

/// Access address information for the NV address space.
#define NV_ADDR(X)	(NVADDR)((unsigned long)&(X) & (NVADDR)~0uL)

/// \defgroup nv NV
/// \ingroup deviceDriverLayer
/// @{

int nvInit(void);

void nvRead(NVADDR nvaddr, void *buf, size_t n);
uint16_t nvReadWord(NVADDR nvaddr);
int nvWrite(NVADDR nvaddr, const void *buf, size_t n);
int nvWriteWord(NVADDR nvaddr, uint16_t data);
int nvZeroFill(NVADDR nvaddr, size_t nSize);

int nvStart(void);
int nvStop(void);

/// @}

#endif /*NV_H_*/
