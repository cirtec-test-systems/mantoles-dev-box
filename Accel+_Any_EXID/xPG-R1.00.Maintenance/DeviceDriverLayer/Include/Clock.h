/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Clock device driver header
 *	
 ***************************************************************************/
 
#ifndef CLK_H_
#define CLK_H_

#include <stdbool.h>

/// \defgroup clock Clock
/// \ingroup deviceDriverLayer
/// @{

int clkInit(void);
void clk8MHz(void);
void clk1MHz(void);
void clkSleepAndEnableInterrupts(void);

void clkClearOscillatorFault(void);
bool clkIsOscillatorFault(void);
void clkOscillatorFaultInterruptDisable();
void clkOscillatorFaultInterruptEnable();

#ifdef PLAN_C
bool clkWaitForOscillatorFaultToClear(void);
#endif

/// @}

#endif /*CLK_H_*/
