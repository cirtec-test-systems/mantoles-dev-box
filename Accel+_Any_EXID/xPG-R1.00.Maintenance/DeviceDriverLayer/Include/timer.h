/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Timer device driver public interface
 *	
 ***************************************************************************/

#ifndef TIMER_H_
#define TIMER_H_

#include <stdint.h>

/// \defgroup timer Timer A
/// \ingroup deviceDriverLayer/// @{

/**
 * Configures Timer_A.
 */
void timerInit(void);

/**
 * Read out the timer's current tick count.
 *
 * One complexity here is that the timer, which is running from ACLK, is
 * asynchronous with the CPU, which is running from MCLK.  Reading it requires
 * either stopping the counter for the read, which would make the
 * time-of-day clock run slow, or doing multiple reads with majority logic.
 * As a simple solution, this code requires three successive reads of the
 * clock to return identical values.  This will work because MCLK is 8 MHz,
 * while the counter increments at 512 Hz.  Therefore, it is quite likely
 * that three reads can complete in the ~2 ms that the counter holds each value.
 *
 * @return The current Timer_A tick count.
 */
int16_t timerTicks(void);



/*
 * TETS Timer
 */

/**
 * Configure one of the timer channels for use as the TETS timer.
 */
void timerConfigureTETSTimer(void);

/**
 * Set the TETS Timer to generate an interrupt in a certain number of ticks
 * after now.
 *
 * \note The parameter is the numebr of ticks until the interrupt, not an
 * 		 absolute timer count.
 *
 * @param ticks The number of ticks until the event.
 */
void timerStartTETSTimer(uint16_t ticks);



/*
 * SWTimer (multi-channel timer)
 */

/**
 * Configure one timer channel for the multi-channel timer (SWTimer).
 */
void timerConfigureSWTimer(void);

/**
 * Set the TETS Timer to generate an E_SW_TIMER_TIMEOUT event at a certain
 * number of ticks.
 *
 * \note The parameter is the absolute timer value at which the event should
 * 		 be generated.
 *
 * \param timeout The timer count at which to generate the event, in ticks.
 */
void timerSetSWTimer(uint16_t timeout);

/**
 * Enable interrupts for the SWTimer channel.
 */
void timerEnableSWTimerInterrupt(void);

/**
 * Disable interrupts for the SWTimer channel.
 */
void timerDisableSWTimerInterrupt(void);

/*! Prevents processing of TimerA interrupts until the corresponding
 *  call to timerRestoreInterrupt.
 *
 *  \returns A flag indicating the state of the interrupt when the call was
 *  made. This value is suitable for passing to timerRestoreInterrupt().
 */
uint16_t timerInterruptDisable();


/*! Restore the processing of TimerA interrupts to a previous state.
 *  If an interrupt occurred while TimerA interrupts were disabled,
 *  they will be processed immediately.
 *
 *  \param interruptState the value returned from timerDisableInterrupt()
 *  		when the interrupt was disabled.
 */
void timerInterruptRestore(uint16_t interruptState);



/// @}

#endif /*TIMER_H_*/
