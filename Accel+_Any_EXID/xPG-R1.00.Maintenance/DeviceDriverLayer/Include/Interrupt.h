/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Interrupt-management inline functions 
 *	
 ***************************************************************************/

#ifndef INTERRUPT_H_
#define INTERRUPT_H_

#include <stdint.h>
#include <stdbool.h>

#include "Clock.h"
#include "led.h"
#include "mics.h"
#include "PowerASIC.h"
#include "system.h"

/// @defgroup interrupt	Interrupt Management
/// @ingroup deviceDriverLayer

/*
 * GCC, used for testing and code coverage, does not know, nor need to know,
 * the interrupt keyword, so #define it away when running in that environment.
 *
 * When building the xPG DLL for the xPG Simulator App the interrupt keyword
 * is not needed so #define it away when running in that environment.
 */

#if defined(__GNUC__) || defined(VS_WINDOWS_SIMULATOR)
#define interrupt /**/
#endif


void interruptEnable(void);
uint16_t interruptDisable(void);
void interruptRestore(uint16_t interruptState);
void interruptClearNMI(void);
bool interruptIsNMI(void);



/**
 * Enter the full power state at the start of every interrupt handler.
 *
 * \warning Must be called at the start of every interrupt handler.
 *
 * If the xPG is idle and sleeping, it turns down its power to a level too
 * low to sustain MSP430 operation.  Call this function at the start of
 * every interrupt handler to restore the MSP430 clock to full speed and
 * to tell the Power ASIC to supply full power from the LDO.
 */
#ifndef RUN_AT_8_MHZ
#define interruptHandlerPrologue()						\
do {													\
	pwrBuckBoostOn();									\
	pwrContinuousLDO();									\
} while (0)

#else
#define interruptHandlerPrologue()								\
		do {													\
			DCOCTL = CALDCO_8MHZ;  								\
		  	BCSCTL1 = XT2OFF | DIVA_0 | (CALBC1_8MHZ & 0x0F); 	\
		  	BCSCTL2 = DIVS_3 | DIVM_0; 							\
		  	pwrBuckBoostOn();									\
			pwrContinuousLDO();									\
		} while (0)
#endif

/**
 * Values useful in some interrupt handlers
 */
enum WHAT_TO_DO {
	WAKE_UP,     	//!< The function should execute _low_power_mode_off_on_exit()
	BACK_TO_SLEEP	//!< The function should execute interruptHandlerEpilogue()
};


/// @}

#endif /*INTERRUPT_H_*/
