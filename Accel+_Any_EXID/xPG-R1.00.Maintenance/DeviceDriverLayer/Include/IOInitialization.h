/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Header file for IO initialization functions.
 *	
 ***************************************************************************/
 
#ifndef IOINIT_H_
#define IOINIT_H_


#ifdef RUN_SMCLK_AT_1_MHZ

#define SMCLK_SCALE_FACTOR 1

#else
#ifdef RUN_SMCLK_AT_125_KHZ

#define SMCLK_SCALE_FACTOR 8

#else
// run SMCLK at default of 250 KHz
#define SMCLK_SCALE_FACTOR 4

#endif
#endif

void ioInit(void);

#endif
