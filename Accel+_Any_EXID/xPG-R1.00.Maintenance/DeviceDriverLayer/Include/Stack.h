/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Stack device driver module -- manages stack filling
 * 		  and integrity checks.
 *	
 ***************************************************************************/
 
#ifndef STACKCOND_H_
#define STACKCOND_H_

/// \defgroup stack	Stack
/// \ingroup deviceDriverLayer
/// @{

/**
 * Initialize the stack driver
 *
 * Side-effect: Fills the linker-allocated stack region with a sentinel value
 * so that stack usage can be checked.
 */
void stackInit(void);

/**
 * Check the stack for overflow
 *
 * Performs the check by looking for the sentinel value written by stackInit().
 *
 * @return -1 if the stack has overflowed; 0 otherwise.
 */
int stackCheck(void);

/// Stack fill value.
#define FILL_VALUE	0x5555

/// @}

#endif /*STACKCOND_H_*/
