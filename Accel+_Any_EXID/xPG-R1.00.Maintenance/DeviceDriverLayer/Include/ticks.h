/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Timer tick definitions
 * 
 ***************************************************************************/
 
#ifndef TICKS_H_
#define TICKS_H_

#ifndef ACLK_USES_LFXT1CLK

#define TICKS_HZ	(1000)	// when using VLOCLK, we track timers in MS.
#define MS(ms) (ms)

#else

#define TICKS_HZ	(32768 / 8)	// 4096 Hz
#define MS(ms)		(((ms) * ((unsigned long)TICKS_HZ) + 999uL) / 1000uL)

#endif

#endif
