/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Watchdog device driver header file.
 *	
 ***************************************************************************/

#ifndef WDHW_H_
#define WDHW_H_

#include <stdint.h>
#include <stdbool.h>

/// \defgroup watchdog Watchdog
/// \ingroup deviceDriverLayer
/// @{
#ifdef WDOG_USES_SMCLK
#define MAX_WATCHDOG_COUNT 128
#else
#define MAX_WATCHDOG_COUNT 5
#endif

void wdInit(void);
void wdDisable(void);
void wdEnable(void);
uint16_t wdSuspend(void);
void wdRestore(uint16_t originalWDTCTL);
void wdServiceWatchdog(void);
void wdClearWatchdogFault(void);
void wdClearWatchdogTimer(void);
bool wdIsWatchdogFault(void);
void wdClearResetFlag(void);
bool wdIsResetFlag(void);
void wdClearPowerOnResetFlag(void);
bool wdIsPowerOnResetFlag(void);

void wdResetXpg(void);

/// @}

#endif /*WDHW_H_*/
