/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Clock device driver.
 *	
 ***************************************************************************/

#include "Clock.h"
#include "delay.h"
#include "Interrupt.h"
#include "system.h"
#include <intrinsics.h>


#ifdef UNIT_TESTING
volatile uint64_t TimerBTicks = 0;
#endif

int clkInit(void)
{
	uint8_t counter = 0;
	uint16_t delayCycles = 100;
	int i = 0;
	bool sentry = true;
	int sentryCounter = 0;

	// Use the SVS comparator to wait for DVDD to rise high enough for 8 MHz operation.
	// See "MSP430 Software Coding Techniques", TI Application Report SLAA294a, section 3.5.

	// SVS is a secondary check of Vcc
	SVSCTL = VLD2;

	//Ensure register is set correctly
	while ((SVSCTL & SVSON) == 0)
	{
		// Wait for t-d(SVSon) and t-settle
		counter++;

		if (counter == 8000)
		{
			break;
		}
	}

	//Check if register values settled in
	if ((SVSCTL & SVSON) == 0)
	{
		return ERROR_RETURN_VALUE;
	}


	counter = 0;

	while ((SVSCTL & SVSFG) != 0)             // If Vcc < Vcc-min, SVSFG will
	{										  	// immediately be set again
		SVSCTL &= ~SVSFG;

		counter++;

		if (counter == 8000)
		{
			break;
		}
	}

	//Check if voltage stabilized
	if ((SVSCTL & SVSFG) != 0)
	{
		return ERROR_RETURN_VALUE;
	}

	//Turn the supply voltage supervisor back off
	SVSCTL &=  ~VLD2;

	//Reset the counter
	counter = 0;

	// Check the oscillator-fault flags to make sure the oscillators start and are stable.
	// See "MSP430 Software Coding Techniques", TI Application Report SLAA294a, section 3.4.

	//Wait a while to make sure the fault clears
	while (sentry && (delayCycles > 0))
	{
		delayCycles--;
		IFG1 &= ~OFIFG;         	// Clear OSCFault flag

		for (i = 0x47FF; i > 0; i--)
		{
			//now wait and see if the error sets again
		}

		counter++;

		//If the fault is still there, keep looping
		if (IFG1 & OFIFG)
		{
			sentry = true;
		}
		else
		{
			//The fault is cleared, but we want to make sure its stable
			//So wait 10 more turns before really calling it cleared
			sentryCounter++;

			if (sentryCounter > 10)
			{
				sentry = false;
			}
			else
			{
				sentry = true;
			}
		}

		if (counter == 8000)
		{
			break;
		}
	}


	//Check if we were able to get a stable clock, other signal error
	if ((IFG1 & OFIFG) != 0)
	{
		return ERROR_RETURN_VALUE;
	}

	DELAY_MS(10);

	// Enable the oscillator fault interrupt
	IE1 |= OFIE;

	return OK_RETURN_VALUE;
}

/**
 * Put the MSP430 into a low-power sleep mode to await the next interrupt.
 */
void clkOscillatorFaultInterruptDisable()
{
	IE1 &= ~OFIE;
}

void clkOscillatorFaultInterruptEnable()
{
	IE1 |= OFIE;
}

void clkSleepAndEnableInterrupts(void)
{
	interruptEnable();
}

/**
 * Slow MCLK to 1 MHz.  Used by the flash module.
 * 
 * Leaves all other clocks untouched.
 */
 
void clk1MHz(void)
{
	BCSCTL2 |= DIVM_3;
}

/**
 * Return MCLK to full speed, 8 MHz.  Used by the flash module.
 * 
 * Leaves all other clocks untouched.
 */

void clk8MHz(void)
{
	BCSCTL2 &= ~DIVM_3;
}


/**
 * Clear the oscillator fault interrupt flag
 */

void clkClearOscillatorFault(void)
{
	IFG1 &= ~OFIFG;
}

/**
 * Clear the oscillator fault interrupt flag
 */

bool clkIsOscillatorFault(void)
{
	return (IFG1 & OFIFG) != 0;
}

#ifdef PLAN_C
bool clkWaitForOscillatorFaultToClear(void)
{
	uint8_t counter = 0;
	uint16_t delayCycles = 100;
	int i = 0;
	bool sentry = true;
	int sentryCounter = 0;

	//Wait a while to make sure the fault clears
	while (sentry && (delayCycles > 0))
	{
		delayCycles--;
		IFG1 &= ~OFIFG;         	// Clear OSCFault flag

		for (i = 0x47FF; i > 0; i--)
		{
			//now wait and see if the error sets again
		}

		counter++;

		//If the fault is still there, keep looping
		if (IFG1 & OFIFG)
		{
			sentry = true;
		}
		else
		{
			//The fault is cleared, but we want to make sure its stable
			//So wait 10 more turns before really calling it cleared
			sentryCounter++;

			if (sentryCounter > 10)
			{
				sentry = false;
			}
			else
			{
				sentry = true;
			}
		}

		if (counter == 8000)
		{
			break;
		}
	}

	//Check if we were able to get a stable clock, other signal error
	if ((IFG1 & OFIFG) != 0)
	{
		return false;
	}

	return true;
}

#endif




#ifdef UNIT_TESTING

uint64_t getTimerBTicks()
{
	uint16_t a, b;

	do
	{
		a = TBR;
		b = TBR;
	} while(a != b);

	return TimerBTicks | a;
}
#endif

