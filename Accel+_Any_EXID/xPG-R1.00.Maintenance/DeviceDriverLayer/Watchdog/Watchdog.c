/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: tests for wdcond.c (wdCond module)
 *	
 ***************************************************************************/

#include <stdint.h>
#include "Watchdog.h"
#include "DeviceDriverLayer/Common/msp430f2618.h"
#include "log.h"
#include "log_events.h"
#include "DeviceDriverLayer/Interrupt/isr.h"
#include "Interrupt.h"
#include "delay.h"
#include "EventQueue.h"
#include "SystemHealthMonitor.h"
#include "StimManager.h"
#include "Stim.h"
#include "counter.h"
#include "pgm.h"

uint8_t volatile watchdogCounter = 0;

/**
 * Initializes the watchdog to the longest time - 1s.
 *
 */
void wdInit(void)
{
	//Enable watchdog interrupt
	IE1 |= WDTIE;

	// Set watchdog to longest time - Password | Set to Timer A | Reset Count | Turn Off | Interval Timer Mode
	#ifdef WDOG_USES_SMCLK
		WDTCTL = WDTPW | WDTCNTCL | WDTHOLD | WDTTMSEL;
	#else
		WDTCTL = WDTPW | WDTSSEL | WDTCNTCL | WDTHOLD | WDTTMSEL;
	#endif
}

/**
 * Services watchdog by only resetting the count
 *
 */
void wdServiceWatchdog(void) 
{
	watchdogCounter = 0;
}

/**
 * Resets the internal watchdog timer counter to zero
 *
 */
void wdClearWatchdogTimer(void)
{
	//Only clear the counter - Retain Current Setting | Password | Reset Count
	WDTCTL = (WDTCTL & 0x00FF) | (WDTPW | WDTCNTCL);
}


/**
 * Disables the watchdog while still retaining the settings.
 *
 */
void wdDisable(void)
{
	//Set the Hold Bit - Retain Current Setting | Password | Turn Off
	WDTCTL = (WDTCTL & 0x00FF) | (WDTPW | WDTHOLD);
}

/**
 * Enables the watchdog and continues to use already configured settings
 *
 */
void wdEnable(void)
{
#ifndef DISABLE_WD
	//Unset the Hold Bit - Retain Current Setting | Password | Turn On
	WDTCTL = ((WDTCTL & 0x00FF) | (WDTPW)) & (~WDTHOLD);
#endif
}

/**
 * Saves the current watch-dog-timer control settings and disables the wdt.
 * 
 * \returns original WDTCTL lsb or'd with WDT password to right back to 
 *          restore the settings
 *  
 */
uint16_t wdSuspend(void)
{
    uint16_t origWDTCTL;

    //Retrieve Current Watchdog Setting
    origWDTCTL = (WDTCTL & 0x00FF) | WDTPW;

    //Disable the watchdog
    wdDisable();

    //Return Original Watchdog Setting
    return origWDTCTL;
}

/**
 * Restores the watch-dog-timer control settings to the passed value.
 * 
 * \param original WDTCTL setting obtained by calling wdSuspend()
 *  
 */
void wdRestore(uint16_t originalWDTCTL)
{
	WDTCTL = originalWDTCTL;
}

/**
 * Clears the watchdog fault from the interrupt register
 */
void wdClearWatchdogFault(void)
{
	IFG1 &= ~WDTIFG;
}

/**
 * Check whether the watchdog fault flag is set
 */
bool wdIsWatchdogFault(void)
{
	return (IFG1 & WDTIFG) != 0;
}

/**
 * Clears the reset flag from the interrupt register
 *
 */
void wdClearResetFlag(void)
{
	IFG1 &= ~RSTIFG;
}

/**
 * Check whether the reset flag is set
 */
bool wdIsResetFlag(void)
{
	return (IFG1 & RSTIFG) != 0;
}

/**
 * Reset the xPG by turning on the watchdog and letting it expire.
 *
 */
void wdResetXpg(void)
{
	//Log the reset
	logNormal(E_LOG_RESET_XPG,0);

	// Turn off interrupts so nothing can wake the CPU from LPM4.
	_disable_interrupts();

	//Write wrong password to watchdog to set it off
	WDTCTL = 0;
}

/**
 * Reset the xPG by turning on the watchdog and letting it expire.
 *
 */
void wdResetXpgBecauseOfWatchdog(void)
{
#ifndef DISABLE_STIM_RECOVERY
	if (isStimOn())
	{
		//Stop Stimulation Gracefully
		stimStop();
		stimPowerOff();

		//Save of the currently selected program
		counterSet(COUNTER_SELECTED_PROGRAM, pgm_SelectedProgramNumber());
	}
	else
	{
		//Clear of the currently selected program
		counterSet(COUNTER_SELECTED_PROGRAM, 0);
	}
#endif

	// Turn off interrupts so nothing can wake the CPU from LPM4.
	_disable_interrupts();

	//Set the status register to indicate a reset has been commanded
	IFG1 |= RSTIFG;

	//Write wrong password to watchdog to set it off
	WDTCTL = 0;
}

/**
 * Clears the power on reset interrupt flag, PORIFG.
 */

void wdClearPowerOnResetFlag(void)
{
	IFG1 &= ~PORIFG;
}

/**
 * Clears the power on reset interrupt flag, PORIFG.
 */

bool wdIsPowerOnResetFlag(void)
{
	return (IFG1 & PORIFG) != 0;
}


/**
 * Watchdog interrupt handler
 */
#pragma vector = WDT_VECTOR
interrupt void isrWDT(void)
{
	interruptHandlerPrologue();

#ifndef SERVICE_WATCHDOG_TIMER
	//Put an event for the application to service the watchdog
	dispPutSimpleEvent(E_SERVICE_WATCHDOG);
#endif

	//Increment watchdog counter, this is reset by wsServicdeWatchdog
	watchdogCounter++;

	if (watchdogCounter == MAX_WATCHDOG_COUNT)
	{
		//Log the reset
		//Write the system health monitor's state to the log
		logNormal(E_LOG_ERR_WATCHDOG_RESET, shmGetCurrentState());

		//Dump the event queue to the log
		dispWriteQueueToLog();

		#ifndef DISABLE_STIM_RECOVERY
			wdResetXpgBecauseOfWatchdog();
		#else
			wdResetXpg();
		#endif
	}

	_low_power_mode_off_on_exit();
}
