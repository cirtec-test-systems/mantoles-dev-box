/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: fram module, part of nv memory subsystem
 *	
 ***************************************************************************/

//#include "system_errors.h"
#include "error_code.h"
#include "fram.h"
#include "fram_bits.h"
#include "nvhw.h"
#include "nvspi.h"
#include "delay.h"
#include "watchdog.h"

#define FRAM_REV_MASK 0x38

/**
 * Reads and returns the NV memory ID.  The manufacturer ID is returned in the
 * following order, from MSB to LSB:
 *  - JEDEC Manufacturer ID bank number
 *  - JEDEC Manufacturer ID
 *  - Device ID byte 1
 *  - Device ID byte 2
 * 
 * \returns A 32-bit combined manufacturer and device ID, as described above. 
 */
 
uint32_t framReadID(void)
{
	uint32_t id;
	uint8_t c;

	nvHw_AssertSCS();
	
	nvSPI_Put(FM_RDID);

	/*
	 * Count the leading 0x7f bytes to determine the bank number of the JEDEC
	 * manufacturer ID.  The count goes in the upper byte of the id.
	 * 
	 * There is one subtlety here: JEDEC banks start counting at 1, so the
	 * ID needs to start with 0x01 in the high byte.
	 */	

	id = 0x01000000uL;
	for (c = nvSPI_Get(); c == 0x7f; c = nvSPI_Get())
	{
		id += 0x01000000uL;
	}
	
	/*
	 * Now read the other three ID bytes and assemble them in the result.
	 */
 
	id |= ((uint32_t)c) << 16;		// Manufacturer ID, low byte
	id |= nvSPI_Get() << 8;			// Device ID byte 1
	id |= (uint32_t)(nvSPI_Get() & ~FRAM_REV_MASK);		// Device ID byte 2, with revision field masked off

	nvHw_DeassertSCS();
	
	return id;
}


/**
 * Reads a block of data from the FRAM.
 *
 * \param addr Address, in the FRAM address space, at which to start reading
 * \param buf  Address, in the MSP430 address space, at which to store the data
 * \param n    Number of bytes to read
 */ 
 
void framRead(uint16_t addr, void *buf, size_t n)
{
	uint8_t *p = buf;

	nvHw_AssertSCS();
	
	nvSPI_Put(FM_READ);
	nvSPI_Put((uint8_t)((addr >> 8) & 0xff));
	nvSPI_Put((uint8_t)(addr & 0xff));

	for ( ; n > 0; n--)
	{
		*p++ = nvSPI_Get();
	}
	
	nvHw_DeassertSCS();
}


/**
 * Writes a block of data to the FRAM.  Assumes writes are already enabled
 * (see #fram_WriteEnable and #fram_WriteDisable). 
 * 
 * \param addr Address, in FRAM address space, at which to start writing.
 * \param buf  Address, in MSP430 address space, of the data to write.
 * \param n	   Number of bytes to write.
 *
 * \returns	OK_RETURN_VALUE(0) for success, or
 *          ERROR_RETURN_VALUE(-1) if write verification failed.
 */

int framWrite(uint16_t addr, const void *buf, size_t n)
{
	const uint8_t *p;
	int result;
	int i;

	/*
	 * Write the data
	 */

	nvHw_AssertSCS();
	
	nvSPI_Put(FM_WRITE);
	nvSPI_Put((uint8_t)((addr >> 8) & 0xff));
	nvSPI_Put((uint8_t)(addr & 0xff));

	p = buf;
	for (i = n; i > 0; i--)
	{
		nvSPI_Put(*p++);
	}
	
	nvHw_DeassertSCS();
	
	/*
	 * Verify the data
	 */

	nvHw_AssertSCS();
	
	nvSPI_Put(FM_READ);
	nvSPI_Put((uint8_t)((addr >> 8) & 0xff));
	nvSPI_Put((uint8_t)(addr & 0xff));

	result = OK_RETURN_VALUE;
	p = buf;
	for (i = n; i > 0; i--)
	{
		if (*p++ != nvSPI_Get())
		{
			result = ERROR_RETURN_VALUE;
			break;
		}
	}
	
	nvHw_DeassertSCS();
	
	return result;
}

/**
 * Overwrites a block of data in the FRAM with zeros.  Assumes writes are already enabled
 * (see #framWriteEnable and #framWriteDisable).  A zero will be written to every location
 * starting at the address given by the addr parameter.  The size in bytes of the data area
 * to be overwritten is given by the n parameter
 *
 * \param addr Address, in FRAM address space, at which to start writing zeros.
 * \param n	   Number of bytes to write.
 *
 * \returns	OK_RETURN_VALUE(0) for success, or
 *          ERROR_RETURN_VALUE(-1) if write verification failed.
 */

int framZeroFill(uint16_t addr, size_t n)
{
	int result;
	int i;

	/*
	 * Write the data
	 */

	nvHw_AssertSCS();

	nvSPI_Put(FM_WRITE);
	nvSPI_Put((uint8_t)((addr >> 8) & 0xff));
	nvSPI_Put((uint8_t)(addr & 0xff));

	for (i = n; i > 0; i--)
	{
		wdServiceWatchdog();
		nvSPI_Put(0x00);
	}

	nvHw_DeassertSCS();

	/*
	 * Verify the data
	 */

	nvHw_AssertSCS();

	nvSPI_Put(FM_READ);
	nvSPI_Put((uint8_t)((addr >> 8) & 0xff));
	nvSPI_Put((uint8_t)(addr & 0xff));

	result = OK_RETURN_VALUE;
	for (i = n; i > 0; i--)
	{
		wdServiceWatchdog();

		if (0x00 != nvSPI_Get())
		{
			result = ERROR_RETURN_VALUE;
			break;
		}
	}

	nvHw_DeassertSCS();

	return result;
}

/**
 * Issue any single-byte command to the FRAM
 */
 
static void simpleCommand(uint8_t cmd)
{
	nvHw_AssertSCS();
	nvSPI_Put(cmd);
	nvHw_DeassertSCS();
}


/**
 * Issue the FRAM's write enable (WREN) command.
 */

void framWriteEnable(void)
{
	simpleCommand(FM_WREN);
}

/**
 * Issue the FRAM's write disable (WRDI) command.
 */

void framWriteDisable(void)
{
	simpleCommand(FM_WRDI);
}
