/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: definitions for accessing the FM25V02 serial FRAM
 *	
 ***************************************************************************/
 
#ifndef FRAM_BITS_H_
#define FRAM_BITS_H_

/// @addgroup fram
/// \ingroup nv
/// @{

/**
 * The Manufacturer and Device ID bytes from the FRAM.
 * The bytes have the following meaning, from MSB to LSB:
 *  - Byte 1: JEDEC Manufacturer ID bank number
 * 	- Byte 2: JEDEC Manufacturer ID
 *  - Byte 3: Device ID, byte 1
 *  - Byte 4: Device ID, byte 2
 * 
 * The correct values for these bytes is determined by reading the data sheet.
 */
#define FM25V02_ID	0x07c22200uL

/*
 * RAMTRON FM25V02 commands
 */

#define FM_WREN	0x06		//< Set Write Enable Latch command
#define FM_WRDI	0x04		//< Write Disable command
#define FM_RDSR	0x05		//< Read Status Register command
#define FM_WRSR	0x01		//< Write Status Register command
#define FM_READ	0x03		//< Read Memory Data command
#define FM_FSTRD	0x0b		//< Fast Read Memory Data command
#define FM_WRITE	0x02		//< Write Memory Data command
#define FM_SLEEP	0xb9		//< Enter Sleep Mode command
#define FM_RDID	0x9f		//< Read Device ID command
#define FM_SNR		0xc3		//< Read Serial Number command (if available)

/// @}

#endif /*FRAM_BITS_H_*/
