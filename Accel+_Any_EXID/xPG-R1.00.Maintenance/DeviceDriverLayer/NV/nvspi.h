/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: nvSPI (non-volatile memory SPI) module prototypes
 *	
 ***************************************************************************/

#ifndef NVSPI_H_
#define NVSPI_H_

#include <stdint.h>

/// @addgroup nvSPI nvSPI
/// \ingroup nv
/// @{

void nvSPI_PowerUp(void);
void nvSPI_PowerDown(void);

void nvSPI_Put(uint8_t d);
uint8_t nvSPI_Get(void);

/// @}

#endif /*NVSPI_H_*/
