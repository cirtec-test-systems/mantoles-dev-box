/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: non-volatile memory device driver module
 *	
 ***************************************************************************/
 
#include "error_code.h"
#include "NV.h"
#include "nvspi.h"
#include "fram.h"
#include "fram_bits.h"
#include "PowerASIC.h"
#include "delay.h"
#include "system.h"

//#define DEBUG
#ifdef DEBUG
#include <stdio.h>
#define DEBUG_PRINTF(X)		printf(X)
#else
#define DEBUG_PRINTF(X)
#endif

/**
 * Initialize the nonvolatile memory (FRAM) subsystem.
 * 
 * Turns on NVRAM power, then reads back and verifies the NVRAM ID as a
 * simple functional check.
 * 
 * \returns	OK_RETURN_VALUE(0) for success, or
 *          ERROR_RETURN_VALUE(-1) if there is an error.
 */

int nvInit(void)
{
	/*
	 * Turn on power, then wait for the FRAM to come up.
	 */
	if (pwrNVPowerOn() < 0)
	{
		DEBUG_PRINTF("nvInit: pwrNVPowerOn failed.\n");
		return ERROR_RETURN_VALUE;
	}
	nvSPI_PowerUp();
	DELAY_MS(1);

	/*
	 * Do a functional check by verifying the FRAM ID.
	 * 
	 */

	if (framReadID() != FM25V02_ID)
	{
		DEBUG_PRINTF("nvInit: FRAM ID did not match\n");
		nvSPI_PowerDown();
		(void) pwrNVPowerOff();
		return ERROR_RETURN_VALUE;
	}
	
	/*
	 * Turn the FRAM off to save power.
	 */
	nvSPI_PowerDown();
	if (pwrNVPowerOff() < 0)
	{
		DEBUG_PRINTF("nvInit: pwrNVPowerOff failed.\n");
		return ERROR_RETURN_VALUE;
	}

	return OK_RETURN_VALUE;

}


/**
 * Prepare the FRAM for a series of read and write operations (turn it on)
 *
 * \returns	OK_RETURN_VALUE(0) for success, or
 *          ERROR_RETURN_VALUE(-1) if there is an error.
 */

int nvStart(void)
{
	if (pwrNVPowerOn() < 0)	// Power up the FRAM
	{
		return ERROR_RETURN_VALUE;
	}
	nvSPI_PowerUp();
	DELAY_MS(1);					// Wait for the FRAM to initialize

	return OK_RETURN_VALUE;
}

/**
 * End a series of read and write operations (turn the FRAM off)
 *
 * \returns	OK_RETURN_VALUE(0) for success, or
 *          ERROR_RETURN_VALUE(-1) if there is an error.
 */

int nvStop(void)
{
	nvSPI_PowerDown();
	return pwrNVPowerOff();
}


/**
 * Read data from the FRAM.
 * 
 * \param nvaddr	Address of the data in the FRAM address space
 * \param buf		Address of the buffer to receive the data, in MSP430 address space
 * \param n			Number of bytes to read
 */

void nvRead(NVADDR nvaddr, void *buf, size_t n)
{
	framRead(nvaddr, buf, n);
}


/**
 * Read a 16-bit word from the FRAM.
 * 
 * \param nvaddr	The address of the word to read in the FRAM address space 
 * \return 			The word read from nvaddr
 */ 
 
uint16_t nvReadWord(NVADDR nvaddr)
{
	uint16_t d;
	
	nvRead(nvaddr, &d, sizeof(d));
	
	return d;
}


/**
 * Write data to the FRAM.
 * 
 * \param nvaddr	Address of the data in the FRAM address space
 * \param buf		Address of the buffer of data to write, in MSP430 address space
 * \param n			Number of bytes to read
 * 
 * \returns	OK_RETURN_VALUE(0) for success, or
 *          ERROR_RETURN_VALUE(-1) if write verification failed.
 */

int nvWrite(NVADDR nvaddr, const void *addr, size_t buf)
{
	int result;
	
	framWriteEnable();
	result = framWrite(nvaddr, addr, buf);
	framWriteDisable();
	
	return result;
}


/**
 * Write a 16-bit word to FRAM.
 * 
 * \param nvaddr	Address of the word to write in the FRAM address space
 * \param data		The word to write
 *
 * \returns	OK_RETURN_VALUE(0) for success, or
 *          ERROR_RETURN_VALUE(-1) if write verification failed.
 */

int nvWriteWord(NVADDR nvaddr, uint16_t data)
{
	return nvWrite(nvaddr, &data, sizeof(data));
}

/**
 * Fill a region of NV RAM with zeros.
 *
 * \param nvaddr	Address of the data in the FRAM address space
 * \param nSize		Number of bytes to write zero to
 *
 * \returns	OK_RETURN_VALUE(0) for success, or
 *          ERROR_RETURN_VALUE(-1) if write verification failed.
 */

int nvZeroFill(NVADDR nvaddr, size_t nSize)
{
	int result;

	framWriteEnable();
	result = framZeroFill(nvaddr, nSize);
	framWriteDisable();

	return result;
}

