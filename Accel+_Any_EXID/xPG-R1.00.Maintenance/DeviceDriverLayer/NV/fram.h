/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: fram module, part of nv module, prototypes
 *	
 ***************************************************************************/

#ifndef FRAM_H_
#define FRAM_H_

#include <stdint.h>
#include <stddef.h>

/// @addgroup fram
/// \ingroup nv
/// @{

uint32_t framReadID(void);
void framRead(uint16_t addr, void *buf, size_t n);
int framWrite(uint16_t addr, const void *buf, size_t n);
int framZeroFill(uint16_t addr, size_t n);
void framWriteEnable(void);
void framWriteDisable(void);

/// @}

#endif /*FRAM_H_*/
