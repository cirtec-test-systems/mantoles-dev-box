/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: nvHw (non-volatile memory hardware) module prototypes
 *	
 ***************************************************************************/

#ifndef NVHW_H_
#define NVHW_H_

#include <stdint.h>

/// \defgroup nvHw nvHw
/// \ingroup nv
/// @{

#if defined(TEST) && !defined(TEST_INLINE_FUNCTIONS)

void nvHw_AssertSCS(void);
void nvHw_DeassertSCS(void);

void nvHw_MOSIHigh(void);
void nvHw_MOSILow(void);
void nvHw_SCKHigh(void);
void nvHw_SCKLow(void);
uint8_t nvHw_MISO(void);
void nvHw_DisablePulldown(void);
void nvHw_EnablePulldown(void);

#else

#include "DeviceDriverLayer/Common/msp430f2618.h"
#include "DeviceDriverLayer/Common/bits.h"

/*@unused@*/
inline static void nvHw_AssertSCS(void)
{
	P8OUT &= ~MEM_CS;
}

/*@unused@*/
inline static void nvHw_DeassertSCS(void)
{
	P8OUT |= MEM_CS;
}

/*@unused@*/
inline static void nvHw_MOSIHigh(void)
{
	P8OUT |= MEM_SDI;
}

/*@unused@*/
inline static void nvHw_MOSILow(void)
{
	P8OUT &= ~MEM_SDI;
}

/*@unused@*/
inline static void nvHw_SCKHigh(void)
{
	P8OUT |= MEM_SCK;
}

/*@unused@*/
inline static void nvHw_SCKLow(void)
{
	P8OUT &= ~MEM_SCK;
}

/*@unused@*/
inline static uint8_t nvHw_MISO(void)
{
	return P8IN & MEM_SDO;
}

/*@unused@*/
inline static void nvHw_DisablePulldown(void)
{
	P8REN &= ~MEM_CS;
}

/*@unused@*/
inline static void nvHw_EnablePulldown(void)
{
	P8REN |= MEM_CS;
}

#endif  // defined(TEST) && !defined(TEST_INLINE_FUNCTIONS)

/// @}

#endif /*NVHW_H_*/
