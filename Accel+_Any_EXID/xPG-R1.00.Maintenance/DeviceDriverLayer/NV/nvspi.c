/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: nvSPI (non-volatile memory SPI) module
 *	
 ***************************************************************************/

#include "nvspi.h"
#include "nvhw.h"

// Suppress warnings about unreachable code and constant controlling expressions.
// The macros below cause these warnings, but the code is written this way on
// purpose in order to generate reasonably efficient bit-banging assembly.
#pragma diag_suppress=1465,238

/**
 * Helper macro that represents one iteration of the unrolled SPI put loop.
 */

#define PUT_HELPER(D, MASK) 	\
	do {						\
		if ((D) & (MASK))		\
		{						\
			nvHw_MOSIHigh();	\
		}						\
		else					\
		{						\
			nvHw_MOSILow();		\
		}						\
		nvHw_SCKHigh();			\
		nvHw_SCKLow();			\
	} while (0)


/**
 * Send one byte on the memory SPI interface.  Chip-select control is done
 * externally to this function.  This is only the shift register.
 * 
 * \param c  The byte to send
 */

void nvSPI_Put(uint8_t d)
{
	/*
	 * Unrolled loop, 8 iterations
	 */

	PUT_HELPER(d, 0x80);
	PUT_HELPER(d, 0x40);
	PUT_HELPER(d, 0x20);
	PUT_HELPER(d, 0x10);
	PUT_HELPER(d, 0x08);
	PUT_HELPER(d, 0x04);
	PUT_HELPER(d, 0x02);
	PUT_HELPER(d, 0x01);
}


/**
 * Helper macro that represents one iteration of the unrolled SPI put loop.
 */

#define GET_HELPER(D, MASK) 	\
	do {						\
		nvHw_SCKHigh();			\
		if (nvHw_MISO())		\
		{						\
			(D) |= (MASK);		\
		}						\
		nvHw_SCKLow();			\
	} while (0)


/**
 * Read one byte of data on the memory SPI interface.
 *
 * @return The data byte read from the FRAM SPI interface
 */
uint8_t nvSPI_Get(void)
{
	uint8_t d;

	/*
	 * Unrolled loop, 8 iterations
	 */

	d = 0;
	GET_HELPER(d, 0x80);
	GET_HELPER(d, 0x40);
	GET_HELPER(d, 0x20);
	GET_HELPER(d, 0x10);
	GET_HELPER(d, 0x08);
	GET_HELPER(d, 0x04);
	GET_HELPER(d, 0x02);
	GET_HELPER(d, 0x01);
	
	return d; 	 
}

/**
 * Enable the memory SPI interface, bringing it out of its low-power mode
 * and making it ready for communications.
 *
 * This is called "Resume" instead of "Init" because it is done repeatedly
 * as the NVRAM is powered up and down.
 */
void nvSPI_PowerUp(void)
{
	nvHw_DeassertSCS();
	nvHw_DisablePulldown();
}

/**
 * Disable the memory SPI interface, putting it into a low-power state that
 * will not parasitically power the memory.
 */
void nvSPI_PowerDown(void)
{
	nvHw_AssertSCS();		// SCS is asserted low, so this sets MEM_CS low.
	nvHw_EnablePulldown();
}
