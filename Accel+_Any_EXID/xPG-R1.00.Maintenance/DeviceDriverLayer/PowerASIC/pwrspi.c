/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Power ASIC SPI (pwrSPI) module implementation
 *	
 ***************************************************************************/

#include <stdbool.h>

#include "DeviceDriverLayer/Common/msp430f2618.h"
#include "pwrspi.h"
#include "delay.h"
#include "system.h"

//#define DEBUG_READBACK_MISMATCH
#ifdef DEBUG_READBACK_MISMATCH
#include <stdio.h>
#endif


static void pwrHw_Send(uint8_t d)
{
	UCB0TXBUF = d;
}

static uint8_t pwrHw_Recv(void)
{
	return UCB0RXBUF;
}

static uint8_t pwrHw_IsBusy(void)
{
	return UCB0STAT & UCBUSY;
}

static uint8_t pwrHw_IsTxReady(void)
{
	return 	UC0IFG & UCB0TXIFG;
}

static void pwrHw_AssertSCS(void)
{
	P2OUT &= ~PWR_CS;
}

static void pwrHw_DeassertSCS(void)
{
	P2OUT |= PWR_CS;
}


int pluto_Set(uint8_t addr, uint8_t mask)
{
	uint8_t x;

	x = pwrSPI_Get(addr) | mask;
	return pwrSPI_Put(addr, x);
}

int pluto_Clear(uint8_t addr, uint8_t mask)
{
	uint8_t x;

	x = pwrSPI_Get(addr) & ~mask;
	return pwrSPI_Put(addr, x);
}


void pwrSPI_Init(void)
{
	P2OUT |= PWR_CS;  // should be set to high
  	P2SEL &= ~PWR_CS; // set as normal IO
  	P2DIR |= PWR_CS;  // set as output line

  	UCB0CTL1 = UCSWRST;

    /*
     * SPI interface is :
     * Data changed on first clock edge   UCCKPH = 1
     * Clock polarity inactive is low     UCCKPL = 0
     * MSB fist                           UCMSB  = 1
     * 8-bit                              UC7BIT = 0
     * Master mode                        UCMST  = 1
     * Mode is 3-pin SPI                  UCMODE = 00
     * Synchronous mode                   UCSYNC = 1
     *
     * TODO: NOTE: clock-edge does not agree with power asic documentation,
     * EESP 0086 Power Asic Specification
     */

    UCB0CTL0 = UCCKPH | UCMSB | UCMST | UCSYNC;

    UCB0CTL1 = UCSWRST | UCSSEL_2;

	UCB0BR0 = 1 & 0xff;
	UCB0BR1 = (1 >> 8) & 0xff;								// Run at SMCLK, so divide by 1

    // initialize the SPI interface signals, PWR_SCK, PWR_SDO, PWR_SDI
	P3SEL |= PWR_SCK | PWR_SDI | PWR_SDO;

    // Complete the configuration by releasing reset
    UCB0CTL1 = UCSSEL_2;
}

int pwrSPI_Put(uint8_t addr, uint8_t data)
{
	// Write the data at the address

	pwrHw_AssertSCS();

	pwrHw_Send(addr | 0x40);		// Send the address byte

	while (!pwrHw_IsTxReady())
	{
		//Loop until ready to transmit
	}

	pwrHw_Send(data);		// Send the data byte

	while (pwrHw_IsBusy())
	{
		//Loop until hardware is not busy
	}

	pwrHw_DeassertSCS();

	DELAY_US(20);

	// Verify the data and return success (0) or failure (-1)
	{
		uint8_t d = pwrSPI_Get(addr);
		if (d == data)
		{
			return 0;
		}
		else
		{
			//setTST_TRIG_HIGH();
			DELAY_MS(1);
#ifdef DEBUG_READBACK_MISMATCH
			printf("pwrSPI FAIL addr %02x wrote %02x read %02x\n", addr, data, d);
#endif
			//setTST_TRIG_LOW();
			return -1;
		}
	}
}

uint8_t pwrSPI_Get(uint8_t addr)
{
	pwrHw_AssertSCS();

	pwrHw_Send(addr | 0x80);		// Send the address byte
	while (!pwrHw_IsTxReady())
	{
		//Loop until ready to transmit
	}

	//read spi rx register to clear it.
	(void) pwrHw_Recv();

	pwrHw_Send(0x00);		// Clock out the data byte
	while (pwrHw_IsBusy())
	{
		//Loop until hardware is not busy
	}

	pwrHw_DeassertSCS();

	// Return the data byte
	return pwrHw_Recv();
}


