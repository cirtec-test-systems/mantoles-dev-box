/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Power ASIC charger device driver. 
 *	
 ***************************************************************************/
#include <stdint.h>
#include "PowerASIC.h"
#include "ADC.h"
#include "cal.h"
#include "Common/Protocol/gen_cals.h"
#include "delay.h"
#include "DeviceDriverLayer/Common/msp430f2618.h"	// Only for interrupt vector definition
#include "error_code.h"
#include "EventQueue.h"
#include "pluto_bits.h"
#include "pwrspi.h"
#include "system.h"
#include "log.h"
#include "ticks.h"

static int chargeGetTemperatureSignals(uint16_t *therm_offset, uint16_t *therm_input, uint16_t *therm_bias);

#define TEMPERATURE_SCALING_FACTOR 11750L
#define MIN_RTHERM_THRESHOLD 11719

#define ADC_BUFFER_SIZE	16			///< Number of ADC samples to read and average
#define ADC_TIMEOUT		MS(250)		///< How many timer ticks to wait for the ADC conversion to complete

#define SETTLING_TIME_DELAY_MS 10		///< Time to wait between enabling Pluto's analog output and taking an ADC measurement.

static int plutoClearBufferSelect(void)
{
	uint8_t buffer = MUX0|MUX1|MUX2|BEN;

	return pluto_Clear(BUFCTL, buffer);
}

static int plutoSetBufferSelect(uint8_t bufferType)
{
	if (plutoClearBufferSelect() < 0)
	{
		logError(E_LOG_ERR_VOLTAGE_CHECK, __LINE__);
		return -1;
	}
	return pluto_Set(BUFCTL, bufferType);
}

static void pwrPrepareADCForMeasurement(void)
{
	ADC_CONFIG const adcConfig = {
		ADC_POWER_ASIC,		// Measure the PWR_ANA_OUT signal from the power ASIC
		1,					// PWR_ANA_OUT is the only channel to measure
		ADC_BUFFER_SIZE,	// Measure the channel repeatedly
		TRIGGER_SOFTWARE,	// Trigger the measurement with a function call.
		CONVERT_ALL,		// A single trigger runs the full measurement sequence.
		ADC_FAST,
		ADC_SAMPLE_TIME_16
	};

	adcOn();
	adcSetup(&adcConfig);
}

static int pwrReadADC(uint16_t *return_AdcAverage)
{
	uint16_t const volatile *results;
	int i;

	// Do the conversions
	adcConvert();
	results = adcResults(ADC_TIMEOUT);

	// If the conversion timed out, return failure.
	if (!results)
	{
		*return_AdcAverage = 0;
		return -1;
	}

	// Calculate the average and indicate success.
	*return_AdcAverage = 0;
	for (i = 0; i < ADC_BUFFER_SIZE; i++)
	{
		*return_AdcAverage += results[i];
	}
	*return_AdcAverage /= ADC_BUFFER_SIZE;

 	return 0;
}

static void pwrDisableADCAfterMeasurements(void)
{
	 adcOff();
}

/**
 * Measure one analog voltage from Pluto.
 *
 * Sets up Pluto's analog mux and mspbuf buffer, makes the measurement, and
 * returns the result.
 *
 * @param select	The Pluto BUFCTL value for the voltage to be measured
 * @param result	Pointer to where to put the ADC conversion result
 * @return 0 for success or -1 for failure.
 */

static int pwrReadOneVoltageWithADC(uint8_t bufferSelect, uint8_t deviceEnable, uint16_t *result)
{
	uint8_t enable0Register;

	enable0Register = pwrSPI_Get(ENABLE0);
	if (pwrSPI_Put(ENABLE0, (enable0Register | deviceEnable) ) < 0)
	{
		//clean up and return an error
		(void) pwrSPI_Put(ENABLE0, enable0Register);
		(void) plutoClearBufferSelect();
		logError(E_LOG_ERR_VOLTAGE_CHECK, __LINE__);
		return -1;
	}

	DELAY_MS(50);

	if (plutoSetBufferSelect(bufferSelect) < 0)
	{
		//clean up and return an error
		(void) pwrSPI_Put(ENABLE0, enable0Register);
		(void) plutoClearBufferSelect();
		logError(E_LOG_ERR_VOLTAGE_CHECK, __LINE__);
		return -1;
	}

	DELAY_MS(SETTLING_TIME_DELAY_MS);

	pwrPrepareADCForMeasurement();

	if (pwrReadADC(result) < 0)
	{
		(void) pwrSPI_Put(ENABLE0, enable0Register);
		(void) plutoClearBufferSelect();
		logError(E_LOG_ERR_VOLTAGE_CHECK, __LINE__);
		return -1;
	}

	pwrDisableADCAfterMeasurements();

	(void) pwrSPI_Put(ENABLE0, enable0Register);
	(void) plutoClearBufferSelect();

	return 0;
}

// ^------ STATIC FUNCTIONS
// --------------------------------------------------------------------

void chargeInit(void) 
{
	chargeEnableChargerEngagedInterrupt();	// Enable the charger interrupt
	//Read the charge state once
	chargeGetChargeState();
}

void chargeEnableChargerEngagedInterrupt(void)
{
	//Reset the corresponding bit to 0 for a Rising Edge interrupt
	P1IES &= ~PWR_V_DET;
	//Interrupt Enable
	P1IE |= PWR_V_DET;
}

void chargeEnableChargerDisengagedInterrupt(void)
{
	//Set the corresponding bit to 1 for a Falling Edge interrupt
	P1IES |= PWR_V_DET;
	//Interrupt Enable
	P1IE |= PWR_V_DET;
}

CHARGE_RATE chargeGetChargeRate(void) 
{
	if (P1IN & PWR_CHARGE_RATE) 
	{
		return CHARGE_RATE_C4;
	}
	else
	{
		return CHARGE_RATE_C8;
	}
}

void chargeSetChargeRate(CHARGE_RATE rate) 
{
	if (rate == CHARGE_RATE_C4)
	{
		P1OUT |= PWR_CHARGE_RATE;
	}
	else
	{
		P1OUT &= ~PWR_CHARGE_RATE;
	}
}

/**
 * Get the charging state from the ASIC status register
 */
CHARGE_STATE chargeGetChargeState(void)
{
	volatile uint8_t chargingState;
	
	//Read the status register twice for the current hardware revision.
	chargingState =  pwrSPI_Get(STATUS);
	chargingState =  pwrSPI_Get(STATUS);
	chargingState &= 0x07;

	return (CHARGE_STATE)(chargingState);
}

/**
 * Get the thermistor temperature per Pluto Spec 8.20
 * 
 * In order to measure IPG temperature via the thermistor, 
 * there are three measurements that need to be made:
 *
 *	1.	THERM_OFFSET
 *	2.	THERM_INPUT
 *	3.	THERM_BIAS
 *
 *	In order to make the measurement, you need to take the following steps:
 *
 *	1.	Enable the thermistor interface (set the THERM_EN bit in the ENABLE0 register)
 *	2.	Wait 50ms
 *	3.	Enable the buffer interface with the buffer set for THERM_OFFSET (write $0F to the BUFCTL register)
 *	4.	Wait 10ms
 *	5.	Make an ADC measurement on ADC4
 *	6.	 Set the buffer for THERM_INPUT (write $0E to the BUFCTL register)
 *	7.	Wait 10ms
 *	8.	Make an ADC measurement on ADC4
 *	9.	Set the buffer for THERM_BIAS (write $0D to the BUFCTL register)
 *	10.	Wait 10ms
 *	11.	Make an ADC measurement on ADC4
 *	12.	Disable the buffer by writing $00 to the BUFCTL register
 *	13.	Disable the thermistor interface by clearing the THERM_EN bit in the ENABLE0 register
 *	14.	Calculate the thermistor reading as follows:
 *
 * 	Scaled_RTherm = 156 * (THERM_INPUT - THERM_OFFSET) / (THERM_BIAS - THERM_INPUT)
 *	lookup the scaled value and return the temperature in centigrade.
 *
 *  
 * \return 0 when all is well, -1 when there is an error.
 *   
 */
int chargeGetTemperature(uint16_t *return_temperature, uint16_t *thermOffset, uint16_t *thermInput, uint16_t *thermBias)
{
	uint32_t numerator, denominator, scaled_rtherm;
	int ret;

	ret = chargeGetTemperatureSignals(thermOffset, thermInput, thermBias);
	
	if ( (ret != OK_RETURN_VALUE ) || (thermBias == thermInput))
	{
		//We can't read the temperature.
		dispPutErrorEvent(ACTERR_POWER_ASIC_ANALOG_SENSE_ERROR);

		scaled_rtherm = 0;
		ret = ERROR_RETURN_VALUE;
	}
	else 
	{
		//This is the Scaled_Rtherm value
		numerator =  (uint32_t)((uint32_t)TEMPERATURE_SCALING_FACTOR * ((uint32_t)*thermInput - (uint32_t)*thermOffset)) ;
		denominator = (uint32_t)((uint32_t)*thermBias - (uint32_t)*thermInput) ;
		scaled_rtherm = (uint32_t) ((uint32_t)numerator / (uint32_t)denominator);
	}

	if (scaled_rtherm <= MIN_RTHERM_THRESHOLD)
	{
		chargeGetTemperatureSignals(thermOffset, thermInput, thermBias);

		numerator =  (uint32_t)((uint32_t)TEMPERATURE_SCALING_FACTOR * ((uint32_t)*thermInput - (uint32_t)*thermOffset)) ;
		denominator = (uint32_t)((uint32_t)*thermBias - (uint32_t)*thermInput) ;
		scaled_rtherm = (uint32_t) ((uint32_t)numerator / (uint32_t)denominator);
	}

	//If we have it the abort threshold, go ahead and log the temperature
	if (scaled_rtherm <= MIN_RTHERM_THRESHOLD)
	{
		uint32_t logdata = 0;

		//Get the Data for Logging
		logdata += *thermOffset;
		logNormal(E_LOG_DIAGNOSTIC_VRECT_AND_TEMP,logdata);

		//Log the Current Temperature
		logdata = ((uint32_t)(*thermInput));
		logdata = logdata<<16;
		logdata += *thermBias;
		logNormal(E_LOG_DIAGNOSTIC_TEMPERATURE,logdata);
	}

	//Ensure the scaled RTHERM value can fit into a 16bit Number as we are using 32bit numbers for calculations
	if (scaled_rtherm < 0xFFFF)
	{
		*return_temperature = scaled_rtherm;
	}
	else
	{
		*return_temperature = 0;
		ret = ERROR_RETURN_VALUE;
	}

	return (ret);
}

bool chargeChargerPresent(void) 
{
	return (P1IN & PWR_V_DET) != 0;
}

/**
 * This function computes the recharge charge voltage as described
 * in section 8.14 of the Pluto Spec.
 * 
 * The recharge voltage is returned in mV.
 */

int chargeGetRechargeVoltage(uint16_t *return_recharge)
{
	uint32_t vRect_mv;
 	uint16_t adcAvg;

 	if (pwrReadOneVoltageWithADC(BUF_SELECT_VRECT_BY_5, BFG_EN, &adcAvg) != OK_RETURN_VALUE)
 	{
		dispPutErrorEvent(ACTERR_POWER_ASIC_ANALOG_SENSE_ERROR);
 		*return_recharge = 0;
 		return -1;
 	}

	//vrect is presented by the asic divided by 5.
	//the measurement is made with 12 bits (0-4095)
	//with a reference of 2.5V
	//5 * ADC/4095 * 2500mV
	vRect_mv = 12500uL * adcAvg;
	vRect_mv = (vRect_mv / 4095);

	*return_recharge = vRect_mv;

	return 0;

}

int chargeStartChargingProcess(void)
{
	uint8_t enable0Register;
	
	enable0Register = pwrSPI_Get(ENABLE0);
	return (pwrSPI_Put(ENABLE0, (CHARGE_EN | COMPOV_EN | enable0Register) ));
}

int chargeStopChargingProcess(void)
{
	uint8_t enable0Register;

	enable0Register = pwrSPI_Get(ENABLE0);
	enable0Register &= ~( CHARGE_EN |COMPOV_EN );
	return (pwrSPI_Put(ENABLE0, enable0Register ));	
}


int chargeTemperatureDiagnosticCheck(void)
{
	uint16_t therm_offset;
	uint16_t therm_input;
	uint16_t therm_bias;
	GEN_CALS const *calibrationData;
	uint8_t ret;

	if (calIsGenCalsValid())
	{
		calibrationData = calGetGenCals();
	}
	else
	{
		dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
		return ERROR_RETURN_VALUE;
	}


	//get the data to test
	ret = chargeGetTemperatureSignals(&therm_offset, &therm_input, &therm_bias);

	if (ret != OK_RETURN_VALUE)
	{
		dispPutErrorEvent(ACTERR_POWER_ASIC_ANALOG_SENSE_ERROR);
		return (ret);
	}

	//Compare the read values to the limits
	if ( (therm_offset > calibrationData->thermOffsetMaximum) ||
		 (therm_offset < calibrationData->thermOffsetMinimum) ||
		 (therm_input > calibrationData->thermInputMaximum) ||
		 (therm_input < calibrationData->thermInputMinimum) ||
		 (therm_bias > calibrationData->thermBiasMaximum) ||
		 (therm_bias < calibrationData->thermBiasMinimum) )
	{
		return (ERROR_RETURN_VALUE);
	}

	return (OK_RETURN_VALUE);
}


static int chargeGetTemperatureSignals(uint16_t *therm_offset, uint16_t *therm_input, uint16_t *therm_bias)
{
	//initialize the return variables.
	*therm_offset = 0;
	*therm_input = 0;
	*therm_bias = 0;

	pwrReadOneVoltageWithADC(BUF_SELECT_THERM_OFFSET, THERM_EN, therm_offset);

	pwrReadOneVoltageWithADC(BUF_SELECT_THERM_INPUT, THERM_EN, therm_input);

	pwrReadOneVoltageWithADC(BUF_SELECT_THERM_BIAS, THERM_EN, therm_bias);
	
	return OK_RETURN_VALUE;
}

/**
 *	1.	Write $01 to Power ASIC address $0E (turns on the fuel gauge enable bit in the ENABLE0 register).
 *	2.	Write $09 to Power ASIC address $0D (selects the battery voltage to be sent to the PWR_ANA_OUT net).
 *	3.	Wait 10ms.  (We may reduce this time after characterizing with an actual IPG board).
 *	4.	Perform A/D conversion on MSP430.
 *	5.	Write $00 to Power ASIC address $0E (disables the fuel gauge)
 *	6.	Write $00 to Power ASIC address $0D (puts the BUFCTL register back to its default state)
 */
int batteryGetBatteryADC(uint16_t *batteryAdc)
{
	return pwrReadOneVoltageWithADC(BUF_SELECT_BATT_GAUGE, BFG_EN, batteryAdc);
}



int chargeGetTempAndDiagCheck(uint16_t *return_temperature, uint16_t *thermOffset, uint16_t *thermInput, uint16_t *thermBias)
{
	GEN_CALS const *calibrationData;
	int ret = OK_RETURN_VALUE;
	uint32_t numerator, denominator, scaled_rtherm;

	if (calIsGenCalsValid())
	{
		calibrationData = calGetGenCals();
	}
	else
	{
		dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
		return ERROR_RETURN_VALUE;
	}

	//get the data to test
	ret = chargeGetTemperatureSignals(thermOffset, thermInput, thermBias);

	if ((ret != OK_RETURN_VALUE)|| (thermBias == thermInput))
	{
		*return_temperature = 0;
		dispPutErrorEvent(ACTERR_POWER_ASIC_ANALOG_SENSE_ERROR);

		return (ERROR_RETURN_VALUE);
	}

	//Compare the read values to the limits
	if ( ((uint32_t)*thermOffset > calibrationData->thermOffsetMaximum) ||
		 ((uint32_t)*thermOffset < calibrationData->thermOffsetMinimum) ||
		 ((uint32_t)*thermInput > calibrationData->thermInputMaximum) ||
		 ((uint32_t)*thermInput < calibrationData->thermInputMinimum) ||
		 ((uint32_t)*thermBias > calibrationData->thermBiasMaximum) ||
		 ((uint32_t)*thermBias < calibrationData->thermBiasMinimum) )
	{
		*return_temperature = 0;

		return (ERROR_RETURN_VALUE);
	}

	//This is the Scaled_Rtherm value
	numerator =  (uint32_t)((uint32_t)TEMPERATURE_SCALING_FACTOR * ((uint32_t)*thermInput - (uint32_t)*thermOffset)) ;
	denominator = (uint32_t)((uint32_t)*thermBias - (uint32_t)*thermInput) ;
	scaled_rtherm = (uint32_t) ((uint32_t)numerator / (uint32_t)denominator);

	if (scaled_rtherm <= MIN_RTHERM_THRESHOLD)
	{
		chargeGetTemperatureSignals(thermOffset, thermInput, thermBias);

		numerator =  (uint32_t)((uint32_t)TEMPERATURE_SCALING_FACTOR * ((uint32_t)*thermInput - (uint32_t)*thermOffset)) ;
		denominator = (uint32_t)((uint32_t)*thermBias - (uint32_t)*thermInput) ;
		scaled_rtherm = (uint32_t) ((uint32_t)numerator / (uint32_t)denominator);
	}

	//If we have it the abort threshold, go ahead and log the temperature
	if (scaled_rtherm <= MIN_RTHERM_THRESHOLD)
	{
		uint32_t logdata = 0;

		//Get the Data for Logging
		logdata += *thermOffset;
		logNormal(E_LOG_DIAGNOSTIC_VRECT_AND_TEMP,logdata);

		//Log the Current Temperature
		logdata = ((uint32_t)(*thermInput));
		logdata = logdata<<16;
		logdata += *thermBias;
		logNormal(E_LOG_DIAGNOSTIC_TEMPERATURE,logdata);
	}

	//Ensure the scaled RTHERM value can fit into a 16bit Number as we are using 32bit numbers for calculations
	if (scaled_rtherm < 0xFFFF)
	{
		*return_temperature = scaled_rtherm;
	}
	else
	{
		*return_temperature = 0;
		return ERROR_RETURN_VALUE;
	}

	return (OK_RETURN_VALUE);
}


