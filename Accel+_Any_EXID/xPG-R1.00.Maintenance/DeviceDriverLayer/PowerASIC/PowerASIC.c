/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Module to control the Power ASIC to power on/off
 *		various subsections of the xPG.
 *
 ***************************************************************************/

#include "PowerASIC.h"

#include "delay.h"
#include "error_code.h"
#include "EventQueue.h"
#include "mics.h"
#include "pwrspi.h"
#include "Common/Protocol/trim_list.h"
#include "DeviceDriverLayer/Common/bits.h"
#include "DeviceDriverLayer/Common/msp430f2618.h"

#include "pluto_bits.h"
#include "trim.h"
#include "system.h"

extern int pluto_Set(uint8_t addr, uint8_t mask);
extern int pluto_Clear(uint8_t addr, uint8_t mask);


/**
 * The Pluto ENABLE1 register controls the power supply to the various
 * subsystems of the IPG.  It also controls automatic strobing of hte WU_EN
 * signal to the MICS chip (STREN bit, PWR_MICS_STROBE signal).  The automatic
 * strobing must be disabled (STREN = 0) when the MSP430 needs to directly
 * control the WU_EN signal, such as when initializing the MICS chip.
 * Automatic strobing (STREN = 1) will generally only be done when the MICS
 * chip is sleeping, waiting for wake-up signal from an external controller.
 *
 * For more information, see <em>EESP 0086 Power ASIC Specification</em>,
 * a document called <em>Power ASIC registers.doc</em>, which has some
 * register addresses and bit assignments, and the IPG/EPG schematics.
 */

int pluto_ApplyTrimList(TRIM_LIST const *tl)
{
	uint16_t addr;
	const uint8_t *p;
	int result = 0;
	int i;
	uint8_t enable0Register;

	//Get the current ENABLE0 register, and or it Trim Write Enable
	enable0Register = pwrSPI_Get(ENABLE0);
	pwrSPI_Put(ENABLE0, (TWE | enable0Register));

	for (i = 0, p = tl->trims; i < tl->listLen; i++, p += 3)
	{
		addr = (p[1] << 8) + p[0];
		if (pwrSPI_Put(addr, p[2]) < 0)
		{
			result = ERROR_RETURN_VALUE;
		}
	}

	//Restore original trim write enable
	pwrSPI_Put(ENABLE0, enable0Register);

	return result;
}

int pwrInit(bool trimListValid, TRIM_LIST const *trimList)
{
	pwrSPI_Init();				// Initialize power ASIC SPI master

	// In case this was a warm reset, turn off any power supplies that might have been left on
	// by a warm reset.
	if (pwrSPI_Put(ENABLE1, 0) < 0)
	{
		return -1;
	}

	/*
	 *
	 * Configure the sleep-mode controller
	 *
	 * The ZL70102 needs 21.8 ms to go from the start of the WU_EN pulse to asserting
	 * IRQ, so 25 ms in high current mode (HCM) is sufficient.
	 *
	 * The low current mode (LCM) duty cycle is determined experimentally.
	 */
	if (pwrSPI_Put(CTRL2, LCM_DC_250 | HCM_PW_25) < 0)
	{
		return -1;
	}

	if (!trimListValid)
	{
		dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
		return -1;
	}

	if (pwrVerifyTrims() == false)
	{
		if (pluto_ApplyTrimList(trimList) < 0)
		{
			return -1;
		}
	}

	return 0;
}

int pwrSetASKPolarity(bool polarity)
{
	if (polarity)
	{
		return pluto_Set(BUFCTL, ASKP);
	}
	else
	{
		return pluto_Clear(BUFCTL, ASKP);
	}
}

int pwrNVPowerOn()
{
	return pluto_Set(ENABLE1, VD4EN);
}

int pwrNVPowerOff()
{
	return pluto_Clear(ENABLE1, VD4EN);
}


int pwrSatEnableAVDD(void)
{
	return pluto_Set(ENABLE1, LDOAEN);
}

int pwrSatDisableAVDD(void)
{
	return pluto_Clear(ENABLE1, LDOAEN);
}

int pwrSatEnableDVDD(void)
{
	return pluto_Set(ENABLE1, VD2EN);
}

int pwrSatDisableDVDD(void)
{
	return pluto_Clear(ENABLE1, VD2EN);
}


int pwrMICSPowerOn(void)
{
	return pluto_Set(ENABLE1, VD1EN);
}

int pwrMICSPowerOff(void)
{
	int res;

	//disable the MICS strobe (the only strobe)
	res = pluto_Clear(ENABLE1, STREN);

	if(res != 0)
	{
		return res;
	}

	return pluto_Clear(ENABLE1, VD1EN);
}

int pwrLEDPowerOn(void)
{
	return pluto_Set(ENABLE1, VD3EN);
}

int pwrEnableStrobe(void)
{
    //set up the strobe pulse width and frequency
	if (pwrSPI_Put(CTRL1, STROBE_FREQ_1_HZ | STROBE_PW_300_US) < 0)
	{
		return -1;
	}

	if (pluto_Set(ENABLE1, STREN) < 0)
	{
		return -1;
	}

	return 0;
}

int pwrDisableStrobe(void)
{
	return pluto_Clear(ENABLE1, STREN);
}

bool pwrIsAddressValid(uint16_t addr)
{
	return addr >= TRIM0 && addr <= SPI_CTL;
}



void pwrEnterStorageMode(void)
{
	//also, set all the power SPI lines low to prevent parasitic power
	P3OUT &= ~(PWR_SDI | PWR_SDO | PWR_SCK);
	P3DIR |= PWR_SDI | PWR_SDO | PWR_SCK;
	P3SEL &= ~(PWR_SDI | PWR_SDO | PWR_SCK);

	P2OUT &= ~(PWR_CS);
	P2DIR |= PWR_CS;
	P2SEL &= ~(PWR_CS);
	
	//According to hardware team, ensure PWR_LDO_EN is driven prior to going into storage mode
	//pwrIntermittentLDO();

	setTST_TRIG_HIGH();
	DELAY_US(4);
	setTST_TRIG_LOW();

	setTST_TRIG_HIGH();
	DELAY_US(4);
	setTST_TRIG_LOW();

	setTST_TRIG_HIGH();
	DELAY_US(4);
	setTST_TRIG_LOW();

	//Now assert VU low to make the power asic cut power.
	P2OUT &= ~PWR_UV_PROT;

	// We should not need to enter LPM4. The hardware should turn off,
	// but this is used in case it takes a few cycles for storage mode to happen
	// or if the charge coil is in place.

	_low_power_mode_4();
}

int pwrEnableBrownOutDetection()
{
	uint8_t enable0Register;
	int status;

	//Turn it on
	enable0Register = pwrSPI_Get(ENABLE0);
	status = pwrSPI_Put(ENABLE0, (enable0Register | BRWO_EN));

	//Enable the interrupt
	P1IE |= PWR_BRWN_OUT;

	return status;
}

int pwrDisableBrownOutDetection()
{
	uint8_t enable0Register;
	int status;

	//Turn it off
	enable0Register = pwrSPI_Get(ENABLE0);
	status = pwrSPI_Put(ENABLE0, (enable0Register &= ~BRWO_EN));

	//Disable the interrupt
	P1IE &= ~PWR_BRWN_OUT;

	return status;
}


void pwrHandlePort1Interrupt(void)
{
	EVENT event;
	bool lookAgain = true;

	/*
	 * The interrupt vector that leads to this function being called
	 * is shared with other peripherals, as well as being shared for
	 * multiple Power ASIC signals, so each bit has to be checked to
	 * determine which one(s) caused the interrupt.
	 */

	if ((P1IFG & PWR_V_DET) != 0)
	{
		while (lookAgain)
		{
			//Acknowledge the interrupt
			P1IFG &= ~PWR_V_DET;

			lookAgain = false;

			if ((P1IN & PWR_V_DET) != 0)
			{
				//Line is high
				//Set the corresponding bit to 1 for a Falling Edge interrupt
				P1IES |= PWR_V_DET;
				event.eventID =E_CHARGER_ENGAGED;

				if ((P1IN & PWR_V_DET) == 0)
				{
					//line is already low
					lookAgain = true;
				}
#ifdef UNIT_TESTING
                // label for unit test breakpoint tells if interrupt hit
                asm("pwrHandlePort1Interrupt_PWR_V_SET: ;");
                asm(" .global pwrHandlePort1Interrupt_PWR_V_SET");
#endif
			}
			else
			{
				//line is low
				//Reset the corresponding bit to 0 for a Rising Edge interrupt
				P1IES &= ~PWR_V_DET;
				event.eventID =E_CHARGER_DISENGAGED;

				if ((P1IN & PWR_V_DET) != 0)
				{
					//line is already high
					lookAgain=true;
				}
#ifdef UNIT_TESTING
                // label for unit test breakpoint tells if interrupt hit
                asm("pwrHandlePort1Interrupt_PWR_V_RESET: ;");
                asm(" .global pwrHandlePort1Interrupt_PWR_V_RESET");
#endif
			}
		}

		//Put the event into the event queue
    	dispPutEvent(event);
	}

	//Brown-out interrupt
	if ((P1IFG & PWR_BRWN_OUT) != 0)
	{
		event.eventID =E_BROWNOUT_DETECTED;
		dispPutEvent(event);
		P1IFG &= ~PWR_BRWN_OUT;
	}


	//Magnet interface
	//PWR_MAG_SW is an active low signal.
	if ((P1IFG & PWR_MAG_SW) != 0)
	{
		if ((P1IN & PWR_MAG_SW) != 0) 	//When it's high the magnet is engaged...
		{
			//Line is high - no magnet
			//Set the corresponding bit to 1 for a Falling Edge interrupt
			P1IES |= PWR_MAG_SW;
			event.eventID = E_MAGNET_ENGAGED;
		}
		else							//Else it's low, the magnet is disengaged
		{
			//line is low - magnet detected
			//Reset the corresponding bit to 0 for a Rising Edge interrupt
			P1IES &= ~PWR_MAG_SW;
			event.eventID = E_MAGNET_DISENGAGED;
		}
		//Put the event into the event queue
		dispPutEvent(event);
		//Acknowledge the interrupt
		P1IFG &= ~PWR_MAG_SW;
	}
}


void pwrHandlePort2Interrupt(void)
{
	/*
	 * The interrupt vector that leads to this function being called
	 * is shared with other peripherals, as well as being shared for
	 * multiple Power ASIC signals, so each bit has to be checked to
	 * determine which one(s) caused the interrupt.
	 */

	//This is now handled by MAGNET_ENGAGED and TIMER in MagnetMonitor
	//So just clear the magnet interrupts
	P2IFG &= ~(PWR_MAG_2S | PWR_MAG_5S);
}


uint8_t pwrPeek(uint8_t address)
{
	return pwrSPI_Get(address);
}

int pwrPoke(uint8_t address, uint8_t data)
{
	return pwrSPI_Put(address, data);
}


void pwrBuckBoostOn(void)
{
	P2OUT &= ~PWR_BBC_CTL;
}

void pwrBuckBoostOff(void)
{
	P2OUT |= PWR_BBC_CTL;
}


bool pwrVerifyTrims(void)
{
	uint16_t addr;
	const uint8_t *p;
	int i;
	TRIM_LIST const *tl = trimGetList(TRIM_PLUTO);

	for (i = 0, p = tl->trims; i < tl->listLen; i++, p += 3)
	{
		addr = (p[1] << 8) + p[0];

		if (pwrSPI_Get(addr) != p[2])
		{
			if (pwrSPI_Put(addr, p[2]) == ERROR_RETURN_VALUE);
			{
				return false;
			}
		}
	}

	return true;
}
