/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Power ASIC SPI module prototypes
 *	
 ***************************************************************************/
 
#ifndef PWRSPI_H_
#define PWRSPI_H_

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include "DeviceDriverLayer/Common/msp430f2618.h"
#include "DeviceDriverLayer/Common/bits.h"

/*
 * SPI Interface functions.
 */
int pluto_Set(uint8_t addr, uint8_t mask);
int pluto_Clear(uint8_t addr, uint8_t mask);

void pwrSPI_Init(void);
void pwrSPI_ClockThroughReset(void);
int pwrSPI_Put(uint8_t addr, uint8_t data);
uint8_t pwrSPI_Get(uint8_t addr);

void pwrSPI_SetPluto901(bool flag);

uint8_t pwrSPI_Get_902(uint8_t addr);

#endif /*PWRSPI_H_*/
