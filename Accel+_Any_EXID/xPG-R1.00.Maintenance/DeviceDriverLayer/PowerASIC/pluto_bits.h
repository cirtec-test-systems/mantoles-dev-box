/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Pluto register definitions (Internal file for power modules)
 *	
 ***************************************************************************/
 
#ifndef PLUTO_BITS_H_
#define PLUTO_BITS_H_

//TRIM0 register
//Recharge bandgap & buck/boost trim registers
#define TRIM0     0x01
#define BGT0   0x01   //adjustment of recharge bandgap
#define BGT1   0x02
#define BGT2   0x04
#define BGT3   0x08
#define IP0    0x10   //bbc peak current
#define IP1    0x20
#define IP2    0x40
#define IP3    0x80

//TRIM1 register
//Buck/boost trim registers
#define TRIM1     0x02
#define OCF0   0x01   //offset compensation
#define OCF1   0x02
#define VO0    0x04   //bbc output voltage
#define VO1    0x08
#define VO2    0x10
#define SEL0   0x20   //bbc switching frequency
#define OFF0   0x40   //bbc offset compensation disable

//TRIM2 register
//Low voltage CC bandgap offset trim register
#define TRIM2     0x03
//byte for band-gap offset - lower 7 bits

//TRIM3 register
//Low voltage CC bandgap offset & slope trim register
#define TRIM3     0x04
#define BGCC_SLOPE_TRIM_BIT_MASK    0x3F  //lower 6 bits
#define BGCC_OFFSET_TRIM  0x80            //8th bit for trim register 2

//TRIM4 register
//Low voltage bandgap offset trim register
#define TRIM4     0x05
//byte for adjustment of low voltage bandgap output to 1.25V

//TRIM5 register
//Low voltage bandgap offset & slope trim register
#define TRIM5     0x06
#define BGLV_SLOPE_TRIM_BIT_MASK    0x3F  //lower 6 bits
#define BGLV_OFFSET_TRIM  0x80            //8th bit for trim register 4

//TRIM6 register
//LDOA & LDOD trim registers
#define TRIM6     0x07
#define A0     0x01   //selection of VANA voltage
#define A1     0x02
#define A2     0x04
#define D0     0x08   //selection of VDIG voltage
#define D1     0x10
#define D2     0x20

//TRIM7 register
//Clock trim registers (1M & 20k)
#define TRIM7     0x08
#define M0     0x01   //adjustment of 1MHz clock frequency
#define M1     0x02
#define M2     0x04
#define M3     0x08
#define K0     0x10   //adjustment of 20kHz clock frequency
#define K1     0x20
#define K2     0x40
#define K3     0x80

//TRIM8 register
//Low voltage biasing current trim register
#define TRIM8     0x09
//byte - lower 6-bits adjustment of biasing currents

//TRIM9 register
//Digital calibration of on-chip temp monitor trim register
#define TRIM9     0x0A
//byte - lower 6-bits cal of on-chip temp monitor

//TRIM10 register
#define TRIM10    0x0B
//Adjustment of CVM registers
#define C0     0x01    //adjustment of charge threshold CCM to CVM
#define C1     0x02
#define C2     0x04
#define C3     0x08
#define O0     0x10    //adjustment of VBATR voltage in CVM
#define O1     0x20
#define O2     0x40
#define O3     0x80

//MAGCTL register
//magnet sensor control register
#define MAGCTL    0x0C
#define MAGSW_DISABLE  0x01  //magnet switch disable
#define MAGTIME_SEL_0  0x00  //magnet time select 0: no auto-turn-off
#define MAGTIME_SEL_1  0x02  //magnet time select 1: 2 sec turn-off
#define MAGTIME_SEL_2  0x04  //magnet time select 1: 5 sec turn-off
#define MAGTIME_SEL_3  0x06  //magnet time select 1: ~16ms turn-off (for testing)

//BUFCTL register
//MSP buffer control & ASK polarity control register
#define BUFCTL    0x0D
#define MUX0   0x01  //MSP430 buffer selection signals
#define MUX1   0x02
#define MUX2   0x04
#define BEN    0x08  //MSP430 buffer enable
#define ASKP   0x10  //ASK polarity

//hardware settings related to BUFCTL register
#define BUF_DISABLE             0x00         //buffer off
#define BUF_SELECT_BATT_GAUGE   (BEN | 0x01) //battery gauge input
#define BUF_SELECT_VRECT_BY_5   (BEN | 0x02) //rectified voltage div by 5
#define BUF_SELECT_TEMP         (BEN | 0x04) //pwr asic internal temp sensor
#define BUF_SELECT_THERM_BIAS   (BEN | 0x05) //thermistor bias node
#define BUF_SELECT_THERM_INPUT  (BEN | 0x06) //thermistor input node
#define BUF_SELECT_THERM_OFFSET (BEN | 0x07) //thermistor offset node

#define BUF_CNTL_MASK (BEN | MUX2 | MUX1 | MUX0)



//ENABLE0 register
//Enable 0 register
#define ENABLE0   	0x0E
#define BFG_EN   	0x01	//battery fuel gauge enable
#define BRWO_EN   	0x02	//brownout enable
#define CHARGE_EN  	0x04	//charge controller enable
#define COMPOV_EN  	0x08	//SPI enable signal to enable overvoltage check when charging is off
#define TEMP_EN   	0x10	//on-chip temperature monitor enable
#define THERM_EN   	0x20	//thermistor interface enable
#define TWE			0x40	//Trim register write enable

//ENABLE1 register
//Enable 1 register
#define ENABLE1   0x0F
#define VD1EN  0x01  //VDIG1 enable
#define VD2EN  0x02  //VDIG2 enable
#define VD3EN  0x04  //VDIG3 enable
#define VD4EN  0x08  //VDIG4 enable
#define LDOAEN 0x10  //LDOA enable
#define STREN  0x20  //pluto strobe signal generator enable


//assignment of auxiliary power supplies
#define VDIG1_DVDD_MICS    VD1EN
#define VDIG2_DVDD_STIM    VD2EN
#define VDIG3_DVDD_AUX1    VD3EN
#define VDIG4_DVDD_MEM     VD4EN
#define LDOA_AVDD          LDOAEN


//CTRL1 register
//Strobe mode control register
#define CTRL1     0x10

#define STROBE_PW0		0x01	///< Pluto CTRL1: strobe pulse width bit 0
#define STROBE_PW1		0x02	///< Pluto CTRL1: strobe pulse width bit 1
#define STROBE_PW2		0x04	///< Pluto CTRL1: strobe pulse width bit 2

#define STROBE_PW_50_US	000	///< Pluto CTRL1: strobe pulse width 50 us
#define STROBE_PW_100_US	001	///< Pluto CTRL1: strobe pulse width 100 us
#define STROBE_PW_150_US	002	///< Pluto CTRL1: strobe pulse width 150 us
#define STROBE_PW_200_US	003	///< Pluto CTRL1: strobe pulse width 200 us
#define STROBE_PW_250_US	004	///< Pluto CTRL1: strobe pulse width 250 us
#define STROBE_PW_300_US	005	///< Pluto CTRL1: strobe pulse width 300 us
#define STROBE_PW_350_US	006	///< Pluto CTRL1: strobe pulse width 350 us
#define STROBE_PW_400_US	007	///< Pluto CTRL1: strobe pulse width 400 us

#define STROBE_FREQ0	0x08	///< Pluto CTRL1: strobe frequency bit 0
#define STROBE_FREQ1	0x10	///< Pluto CTRL1: strobe frequency bit 1
#define STROBE_FREQ2	0x20	///< Pluto CTRL1: strobe frequency bit 2

#define STROBE_FREQ_025_HZ	000	///< Pluto CTRL1: strobe frequency 0.25 Hz
#define STROBE_FREQ_05_HZ	010	///< Pluto CTRL1: strobe frequency 0.5 Hz
#define STROBE_FREQ_1_HZ	020	///< Pluto CTRL1: strobe frequency 1 Hz
#define STROBE_FREQ_2_HZ	030	///< Pluto CTRL1: strobe frequency 2 Hz
#define STROBE_FREQ_4_HZ	040	///< Pluto CTRL1: strobe frequency 4 Hz
#define STROBE_FREQ_8_HZ	060	///< Pluto CTRL1: strobe frequency 8 Hz


//CTRL2 register
//High/Low current mode control register
#define CTRL2     0x11

#define HCM_PW0	001	///< Sleep high current pulse width, bit 0
#define HCM_PW1	002	///< Sleep high current pulse width, bit 1
#define HCM_PW2	004	///< Sleep high current pulse width, bit 2

#define HCM_PW_0_25	000	///< Sleep high current pulse width setting, 0.250 ms
#define HCM_PW_1		001	///< Sleep high current pulse width setting, 1.0 ms
#define HCM_PW_2_5		002	///< Sleep high current pulse width setting, 2.5 ms
#define HCM_PW_5		003	///< Sleep high current pulse width setting, 5.0 ms
#define HCM_PW_10		004	///< Sleep high current pulse width setting, 10 ms
#define HCM_PW_25		005	///< Sleep high current pulse width setting, 25 ms
#define HCM_PW_50		006	///< Sleep high current pulse width setting, 50 ms

#define LCM_DC0	010	///< Sleep low current duty cycle, bit 0
#define LCM_DC1	020	///< Sleep low current duty cycle, bit 1
#define LCM_DC2	040	///< Sleep low current duty cycle, bit 2

#define LCM_DC_8		000	///< Sleep low current setting, 8 uA
#define LCM_DC_16		010	///< Sleep low current setting, 16 uA
#define LCM_DC_32		020	///< Sleep low current setting, 32 uA
#define LCM_DC_64		030	///< Sleep low current setting, 64 uA
#define LCM_DC_125		040	///< Sleep low current setting, 125 uA
#define LCM_DC_250		050	///< Sleep low current setting, 250 uA
#define LCM_DC_500		060	///< Sleep low current setting, 500 uA
#define LCM_DC_1000	070	///< Sleep low current setting, 1 mA

//STATUS register
//Status register
#define STATUS    0x12
#define CHST0  0x01  //charge pump status (I think this is charge controller state?)
#define CHST1  0x02
#define CHST2  0x04
#define EEBUSY 0x08  //EEPROM busy
#define IBIAS  0x10  //Biased current enabled for EEPROM read/write
#define CLKEN  0x20  //output to analog for clock gating

//TEST0 register
//Analog test control 1 register
#define TEST0     0x13
#define BATCL  0x01
#define BBC    0x02
#define PASSW  0x04
#define BGLV   0x08
#define CURLV  0x10
#define CLK1M  0x20
#define CLK20  0x40
#define CLKTE  0x80

//TEST1 register
//Analog test control 2 register
#define TEST1     0x14
#define RECMP  0x01
#define RECCL  0x02
#define VDET   0x04
#define LDOR   0x08
#define BGRC   0x10
#define DMYSW  0x20
#define ASK    0x40
#define LDOD   0x80

//TEST2 register
//Analog test control 3 register
#define TEST2     0x15
#define CH0    0x01
#define CH1    0x02
#define CH2    0x04
#define CH3    0x08
#define CMP0   0x10
#define CMP1   0x20
#define CMP2   0x40
#define CMP3   0x80

//TEST3 register
//Analog test control 4 register
#define TEST3     0x16
#define HV0    0x01
#define HV1    0x02
#define HV2    0x04
#define HV3    0x08
#define MXA0   0x10
#define MXA1   0x20
#define MXA2   0x40
#define MXA3   0x80

//TEST4 register
//Analog test control 5 register
#define TEST4     0x17
//lower 5-bits some D1 mux select?

//TEST5 register
//Analog test control 6 register
#define TEST5     0x18
//lower 5-bits some D2 mux select?

#define TSTDIG2_VSS			0x00
#define TSTDIG2_CHARGE_ST0		0x01
#define TSTDIG2_CHARGE_ST2		0x02
#define TSTDIG2_BGLV_PWD_TST	0x03
#define TSTDIG2_BBC_T2			0x04
#define TSTDIG2_BBC_OCOUT		0x05
#define TSTDIG2_BBC_OC			0x06
#define TSTDIG2_BBC_IZERO		0x07
#define TSTDIG2_LDOA_EN		0x08
#define TSTDIG2_LDOD_STUPB		0x09
#define TSTDIG2_PORB_VRECH_VBBCO	0x0a
#define TSTDIG2_DIG_IBIAS_EN	0x0b
#define TSTDIG2_BRWO_EN		0x0c
#define TSTDIG2_COMPOV_EN		0x0d
#define TSTDIG2_TEMP_EN		0x0e
#define TSTDIG2_MSPBUF_EN		0x0f
#define TSTDIG2_CHARGE_EN_DEL	0x10
#define TSTDIG2_CLK20K_DIG		0x11
#define TSTDIG2_BRWO_COMP		0x12
#define TSTDIG2_LDOD_EN		0x13
#define TSTDIG2_PROTSW_OK		0x14
#define TSTDIG2_VSS_15			0x15
#define TSTDIG2_VSS_16			0x16
#define TSTDIG2_BAT_OK			0x17
#define TSTDIG2_PORB_VRECH_VBAT	0x18
#define TSTDIG2_VSS_19			0x19
#define TSTDIG2_CHARGE_COMP2_OUT	0x1a
#define TSTDIG2_VSS_1B			0x1b
#define TSTDIG2_STUP_PRECH		0x1c
#define TSTDIG2_CURLV_EN		0x1d
#define TSTDIG2_STUP_VBBCO_VALID_AUX 0x1e
#define TSTDIG2_PASSW_CL_INT	0x1f

//EE_CTL register
//EEPROM control 0 register
#define EE_CTL    0x19
#define PD     0x01
#define CSEL   0x02
#define R      0x04
#define W      0x08
#define GB0    0x10
#define GB1    0x20
#define EESTATUS 0x40
#define LOAD   0x80

//EE_DI0 register
//EEPROM Write Data 0
#define EE_DI0    0x1A

//EE_DI1 register
//EEPROM Write Data 1
#define EE_DI1    0x1B

//EE_DI2 register
//EEPROM Write Data 2
#define EE_DI2    0x1C

//EE_DI3 register
//EEPROM Write Data 3
#define EE_DI3    0x1D

//EE_DO register
//EEPROM Read Data
#define EE_DO     0x1E

//EE_A register
//EEPROM Address
#define EE_A      0x1F

//EE_MUX register
//EEPROM current cell mux
#define EE_MUX    0x20

//SPI_CTL register
//SPI Control bits
#define SPI_CTL   0x21
#define CPOL  0x01   //SPI clock polarity
#define CPHA  0x02   //SPI data phase

#endif /*PLUTO_BITS_H_*/
