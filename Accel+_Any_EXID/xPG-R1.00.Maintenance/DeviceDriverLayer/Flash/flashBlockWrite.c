/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Flash Block Write function.
 *
 ***************************************************************************/


#include <stdint.h>
#include <assert.h>

#include "Flash.h"
#include "flash_Int.h"
#include "flashWork.h"
#include "flashHw.h"

#include "Interrupt.h"
#include "Watchdog.h"
#include "delay.h"
#include "Common/Protocol/response_code.h"
#include "cpy_tbl.h"

//Timeout for the busy flag, 2ms or 2000us.
#define BYTE_TIMEOUT_US 2000
//Delay this number of us per loop while waiting.
#define BYTE_TIMEOUT_DELAY_US 10
//Count this many delay intervals
#define BYTE_TIMEOUT_INTERVALS (BYTE_TIMEOUT_US / BYTE_TIMEOUT_DELAY_US)

 /**
 *		flashCond_BlockWrite
 *
 * This function writes a block of RAM into FLASH.
 * The flash controller registers should be set-up prior to
 * calling the routine, with the exception of the BLKWRT and
 * WRT bits in FCTL1.
 *
 * This routine writes the block as 32 words and monitors the
 * WAIT signal (in FCTL3) as each word is written and finally
 * the BUSY signal (in FCTL3) as the block-write sequence is
 * terminated.
 *
 * Per the project's coding standard, a time-out is enacted
 * during the process, to allow some type of graceful recovery
 * should the WAIT or BUSY process not end properly.
 *
 * Note that this routine gets copied to RAM, and runs from that
 * location, since no accesses to flash memory are allowed while
 * erasing or writing.
 *
 * For more information see section 7.3.3 of the
 * MSP430x2xx Family Users Guide, "Writing Flash Memory", in
 * particular the section on page 7-13, "Block Write".
 *
 * For the flash controller clock source used the MCLK, from the DCO at 8MHz,
 * divided by 22.  This creates a flash controller timing generator of about
 * 364kHz.  Per the data sheet for the MSP430F2618-EP data sheet, flash memory
 * data, the time to write the block (using words) should be 30 + (21*31)+6
 * TFTGs.
 *
 *      687 * 2.75 usec 1.88 msec.
 *
 * For 8 blocks, that would be 8 * 1.88 msecs = 15 msecs.
 * Measured 13.4 msecs.
 *
 * 		for our timeout we are allowing for 2ms or 2000us per byte.
 *
 * \param pSourcePtr 	Pointer to the RAM buffer written to flash.
 * \param pDestPtr		Pointer to the location in FLASH for the buffer to be written.
 *
 *  \return Returns an error code for the whole process.
 *			GEN_SUCCESS = success,
 * 			FLASH_WORD_WRITE_ERROR_TIMEOUT = write process not completed in the allowed timeframe.
 *			FLASH_WORD_WRITE_ERROR_FAIL_FLAG = Hardware reports that the write failed.
 *			FLASH_WORD_WRITE_ERROR_ACCVIFG_FLAG  = Hardware reports an access violation.
 *
 */
RESPONSE flashBlockWrite(uint16_t * pSourcePtr, uint16_t * pDestPtr )
{
    volatile uint16_t timeoutCounter;

    RESPONSE returnCode = GEN_SUCCESS;
    uint16_t i;

    //prepare the hardware
    //Unlock the flash
    flashHw_UnlockFlash();

    //Prepare a block write.
    flashHw_StartBlockWrite();

    i = 0;
    while ((returnCode == GEN_SUCCESS) && (i < NUM_WORDS_IN_FLASH_BLOCK))
    {
        //set the destination value to the source value
        *pDestPtr = *pSourcePtr;

        //move to the next data item
        pDestPtr++;
        pSourcePtr++;

        timeoutCounter = BYTE_TIMEOUT_INTERVALS ;
        //wait for the write to finish
        while ((flashHw_GetWAITflag() == 0) && (timeoutCounter > 0))
        {
			DELAY_US(BYTE_TIMEOUT_DELAY_US);
			--timeoutCounter;
        }
        //either the WAIT bit is clear or timeoutCounter == 0
        if (timeoutCounter == 0)
        {
            returnCode = FLASH_WORD_WRITE_ERROR_TIMEOUT;
        }
        else if (flashHw_GetFAILflag())
        {
            returnCode = FLASH_WORD_WRITE_ERROR_FAIL_FLAG;
        }
        else if (flashHw_GetACCVIFGflag())
        {
            returnCode = FLASH_WORD_WRITE_ERROR_ACCVIFG_FLAG;
        }

        //Since we didn't return yet, go to the next data element and write it!
        i++;
    }

    //Complete the block write
    flashHw_EndBlockWrite();
    timeoutCounter = BYTE_TIMEOUT_INTERVALS;
    //wait for the write to finish
    while ((flashHw_GetWAITflag() == 0) && (timeoutCounter > 0))
    {
    	DELAY_US(BYTE_TIMEOUT_DELAY_US);
        --timeoutCounter;
    }

    //if we already have an error, then report the first error detected
    //otherwise look for errors in the flags
    if (returnCode == GEN_SUCCESS)
    {
        if (timeoutCounter == 0)
        {
            returnCode = FLASH_BLOCK_WRITE_ERROR_TIMEOUT;
        }
        if (flashHw_GetFAILflag())
        {
            returnCode = FLASH_BLOCK_WRITE_ERROR_FAIL_FLAG;
        }
        if (flashHw_GetACCVIFGflag())
        {
            returnCode = FLASH_BLOCK_WRITE_ERROR_ACCVIFG_FLAG;
        }
    }

    //Lock the flash - whether an error was detected or not
    flashHw_LockFlash();

    return returnCode;
}


