/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Header file for various #defines for working with
 *                     flash memory
 *	
 ***************************************************************************/
#ifndef FLASHWORK_H_
#define FLASHWORK_H_

/// \ingroup flashHw
/// @{

//THESE LIMITS OF WRITABLE FLASH ARE FOR DEVELOPMENT.  LOWEST_WRITABLE_FLASH_ADDR could be
//set to BOTTOM_OF_NORMAL_FLASH_ADDR, to allow writing to all addresses.
//See flashHw_VerifyAddress()
// THE HIGH LIMIT IS PRESENTLY SET FOR THE MSP430X2618
#define LOWEST_WRITABLE_FLASH_ADDR  0x06E00     ///< low address of writable flash 512-byte segments
#define HIGHEST_WRITABLE_FLASH_ADDR 0x08FFF     ///< high address of writable flash 512-byte segments

#define BOTTOM_OF_NORMAL_FLASH_ADDR 0x03100     ///< address of first byte of normal flash memory

#define SEGMENT_ADDRESS_MASK       0xFFE00      ///< used to mask off segment base address
#define SEGMENT_SIZE                   512      ///< number of bytes in 512-byte segment
#define NUM_WORDS_IN_FLASH_BLOCK 32     ///< number of words in 64-byte block
#define BLOCKS_PER_SEGMENT        8     ///< number of 64-byte blocks in a 512-byte segment

//ADDED FOR SPECIAL DATA THAT GETS SAVED TO INFO MEMORY
#define INFO_MEM_SEGMENT_D_BASE   0x1000        ///< base address of info memory segment D
#define INFO_MEM_SEGMENT_C_BASE   0x1040        ///< base address of info memory segment C
#define INFO_MEM_SEGMENT_B_BASE   0x1080        ///< base address of info memory segment B
#define INFO_MEM_SEGMENT_A_BASE   0x10C0        ///< base address of info memory segment A
                                               //   NOTE THAT INFO MEMORY SEGMENT A 
                                               //   CONTAINS MSP430 CALIBRATIONS AND
                                               //   SHOULD NOT BE USED FOR ANY DATA
                                               //   STORAGE
#define INFO_MEM_SEGMENT_A_TOP    0x10FF        ///< top address in info memory segment A

#define INFO_MEM_SEGMENT_SIZE         64        ///< number of bytes in info mem segment
#define INFO_SEG_ADDRESS_MASK    0xFFFC0        ///< used to mask off info segment base address

//THIS DIVIDER SETS THE CLOCK FOR THE FLASH CONTROLLER
// THE FLASH TIMING GENERATOR MUST RUN BETWEEN 257kHz & 476kHz
// I CHOSE TO RUN IT FROM THE 8 MHz MCLK.  THE DIVIDER SETTING IS
// DIVIDER+1, SO THIS IS 8MHz/22 = 364kHz, RIGHT IN MIDDLE
// 2mhz version
//#define FC_CK_DIVIDE          5        ///< divider for flash controller timing generator

// 1mhz version
//#define FC_CK_DIVIDE          2        ///< divider for flash controller timing generator

//8MHz version
#define FC_CK_DIVIDE          21        ///< divider for flash controller timing generator

//TO TRY TO MAKE CERTAIN THAT FLASH CONTROLLER REGISTERS ARE SET-UP
// CORRECTLY, THEY ARE CHECKED BEFORE KEY OPERATIONS IN THE ERASE
// AND WRITE PROCESSES.  THESE DEFINE THE EXPECTED VALUES FOR THE
// READ-BACKS.  (FOR EXAMPLE, PRIOR TO AN ERASE, WANT TO MAKE 
// SURE THAT THE MASS-ERASE MERAS BIT IN FCTL1 IS NOT SET.)

//THESE ARE THE EXPECTED FLASH CONTROLLER REGISTER SETTINGS
// PRIOR TO THE ERASE OPERATION.
#define FCTL1_ERASE_SETUP_READBACK FRKEY + ERASE        ///< expected value of FCTL1 prior to erase
#define FCTL2_ERASE_SETUP_READBACK FRKEY + FSSEL_1 + FC_CK_DIVIDE       ///< expected value of FCTL2 prior to erase
#define FCTL3_ERASE_SETUP_READBACK FRKEY + LOCKA + WAIT ///< expected value of FCTL3 prior to erase

//THESE ARE THE EXPECTED FLASH CONTROLLER REGISTER SETTINGS
// PRIOR TO THE BLOCK-WRITE OPERATION.
#define FCTL2_WRITE_SETUP_READBACK FRKEY + FSSEL_1 + FC_CK_DIVIDE       ///< expected value of FCTL2 prior to erase
#define FCTL3_WRITE_SETUP_READBACK FRKEY + LOCKA + WAIT ///< expected value of FCTL3 prior to erase

//WHEN PERFORMING THE FLASH OPERATIONS (SEGMENT ERASE OR WORD WRITE)
// THE 'BUSY' AND 'WAIT' BITS ARE CHECKED TO DETERMINE THE COMPLETION
// OF THE PROCESS.  THE CODING STANDARD FOR THE PROJECT SUGGESTS THAT
// SUCH OPERATIONS SHOULD BE GUARDED WITH A TIME-OUT, TO ENSURE THAT
// THE FIRMWARE DOESN'T GET STUCK IN AN INFINITE LOOP.
// THE MSP430X2XX FAMILY USER'S GUIDE AND THE DATA SHEET FOR THE
// SPECIFIC PROCESSOR DESCRIBE THE TIMING OF THE OPERATIONS.  THESE
// 'TFLG_' VALUES ARE USED TO CALCULATE A TIME-OUT COUNT FOR MONITORING
// THE PROCESSES.  
//THE SEGMENT ERASE IS THE LONGEST SINGLE PROCESS IN WORKING WITH THE
// FLASH MEMORY. THE DEVICE DATA SHEET LISTS THE NUMBER OF TIMING
// GENERATOR CYCLES. 
#define TFTG_CYCS_SEG_ERASE 4819        ///< number of timing generator cycles for segment erase

//THE ASSEMBLY CODE THAT MONITORS THE BUSY SIGNAL TAKES A CERTAIN NUMBER
// OF MCLK CYCLES TO EXECUTE.  THE CODE INCLUDES DECREMENTING THE TIMEOUT
// COUNTER, CHECKING IT FOR ZERO, CHECKING THE BUSY BIT, AND LOOPING.
#define MCLK_CYCS_BUSY_CHECK   8        ///< number of mclk cycles to test busy bit

//THE TIMEOUT VALUE IS THEN CALCULATED BASED ON THE SEGMENT ERASE CYCLES, THE
// AMOUNT OF TIME TO TEST THE BUSY BIT, AND THE CLOCK DIVIDER FOR GENERATING
// THE FLASH TIMING.
/** calculated timeout value for the segment erase */
#define SEG_ERASE_TIMEOUT_VAL (uint16_t)((TFTG_CYCS_SEG_ERASE/MCLK_CYCS_BUSY_CHECK) * (FC_CK_DIVIDE+1))
#define SEG_ERASE_TIMEOUT_MARGIN              200       ///< extra timeout margin for timing the segment erase

//THE WORD WRITE PROCESS IS MONITORED USING THE 'WAIT' BIT IN FCTL3.
// THE DEVICE DATA SHEET LISTS THREE DIFFERENT TIME INTERVALS FOR THE
// BLOCK WRITING OF A WORD OR BYTE.  THE FIRST WORD TAKES THE LONGEST,
// SINCE THERE IS A DELAY FOR THE PROGRAMMING VOLTAGE TO BE SET.  THE
// WORD WRITE PROCESS IS GUARDED WITH A SINGLE TIME-OUT VALUE, BASED 
// ON THE WORST-CASE, THE FIRST WORD IN THE BLOCK.  (THE FINAL INTERVAL
// STEP IN THE BLOCK WRITE MONITORS THE 'BUSY' BIT, TO SEE WHEN THE 
// PROGRAMMING VOLTAGE IS REMOVED.
//THE DEVICE DATA SHEET LISTS THE NUMBER OF FLASH TIMING GENERATOR
// CYCLES FOR THE FIRST WORD IN A BLOCK WRITE. 
#define TFTG_CYCS_1ST_WORD_WRITE  30    ///< number of flash timing generator cycles for first word in block write
//THE ASSEMBLY CODE THAT MONITORS THE WAIT SIGNAL TAKES A CERTAIN NUMBER
// OF MCLK CYCLES TO EXECUTE.  THE CODE INCLUDES DECREMENTING THE TIMEOUT
// COUNTER, CHECKING IT FOR ZERO, CHECKING THE WAIT BIT, AND LOOPING.
#define MCLK_CYCS_WAIT_CHECK       8    ///< number of mclk cycles to test wait bit

//THE TIMEOUT VALUE IS THEN CALCULATED BASED ON THE WORD WRITE CYCLES, THE
// AMOUNT OF TIME TO TEST THE WAIT BIT, AND THE CLOCK DIVIDER FOR GENERATING
// THE FLASH TIMING.
/** calculated timeout value for the word-write during block write */
#define WORD_WRITE_TIMEOUT_VAL (uint16_t)((TFTG_CYCS_1ST_WORD_WRITE * (FC_CK_DIVIDE+1))/MCLK_CYCS_WAIT_CHECK)
#define WORD_WRITE_TIMEOUT_MARGIN               6       ///< extra timeout margin for timing the word-write

/// @}

#endif                          /*FLASHWORK_H_ */
