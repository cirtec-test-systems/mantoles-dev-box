/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Flash segment erase function.
 *
 ***************************************************************************/


#include <stdint.h>
#include <assert.h>

#include "Flash.h"
#include "flash_Int.h"
#include "flashWork.h"
#include "flashHw.h"

#include "Interrupt.h"
#include "Watchdog.h"
#include "delay.h"
#include "Common/Protocol/response_code.h"
#include "cpy_tbl.h"


/**
 *		flashCond_SegmentErase
 *
 * Erases the segment associated with the address passed as a parameter.
 * This function runs out of RAM.
 *
 * \param pSegmentAddress a pointer to the address where the flash block will be erased.
 *
 * \returns
 */
RESPONSE flashSegmentErase(uint16_t * pSegmentAddress, uint16_t timeoutVar)
{

    *pSegmentAddress = 0;       //dummy write to erase the flash segment
    //loop until the FCTL3 BUSY bit is clear or timeoutVar is decremented to 0.
    while ((timeoutVar > 0) && (flashHw_GetBUSYflag()))
    {
        --timeoutVar;
    }
    if (timeoutVar == 0)
    {
        return (FLASH_ERASE_ERROR_TIMEOUT);
    }
    if (flashHw_GetFAILflag())
    {
        //erase Failure
        return (FLASH_ERASE_ERROR_FAIL_FLAG);
    }
    if (flashHw_GetACCVIFGflag())
    {
        //erase Failure
        return (FLASH_ERASE_ERROR_ACCVIFG_FLAG);
    }
    //if we didn't exit yet, then all is good.
    return (GEN_SUCCESS);
}

