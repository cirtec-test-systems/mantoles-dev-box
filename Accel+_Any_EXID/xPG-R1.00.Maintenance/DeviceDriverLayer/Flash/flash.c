/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Flash device driver functions.
 *	
 ***************************************************************************/


#include <stdint.h>
#include <assert.h>

#include "Flash.h"
#include "flash_Int.h"
#include "flashWork.h"
#include "flashHw.h"

#include "Interrupt.h"
#include "Watchdog.h"
#include "delay.h"
#include "Common/Protocol/response_code.h"
#include "cpy_tbl.h"
#include "DeviceDriverLayer/Common/msp430f2618.h"

//Use the definitions that are created by the linker and defined in lnk_ipg_msp430f2618.cmd
/*@-exportheadervar@*/
extern COPY_TABLE _flashBlockWriteTable;
extern COPY_TABLE _flashVerifyTable;
extern COPY_TABLE _flashSegmentEraseTable;
/*@=exportheadervar@*/

//Timeout for the busy flag, 2ms or 2000us.
#define BYTE_TIMEOUT_US 2000
//Delay this number of us per loop while waiting.
#define BYTE_TIMEOUT_DELAY_US 10
//Count this many delay intervals
#define BYTE_TIMEOUT_INTERVALS (BYTE_TIMEOUT_US / BYTE_TIMEOUT_DELAY_US)



/**
 *		flashCopyDataToFlash
 *
 * copies a block of data into flash memory
 * 
 * This is normally used to copy something like pulse/program definitions, or 
 * limit tables into flash memory.  This is used both for 'normal' flash memory,
 * which is divided up into 512-byte segments, and the three special 64-byte 
 * 'info segments' (SEGMENT D, SEGMENT C, & SEGMENT B).
 * 
 * These are the basic steps to copy a block of data into a normal 512-byte
 * flash memory segment:
 *  - the current contents of the segment are copied into a 512-byte RAM buffer
 *  - the new data is copied into the appropriate location in the RAM buffer
 *  - the flash segment is erased
 *  - the RAM buffer is copied into the flash segment (in 64-byte blocks)
 * 
 * The steps are slightly modified to copy a block of data into one of the
 * 64-byte INFO segments:
 *  - the current contents of the segment are copied into the 512-byte RAM
 *    buffer (only 64 bytes)
 *  - the new data is copied into the appropriate location in the RAM buffer
 *  - the info segment is erased
 *  - the RAM buffer is copied into the info segment (as one 64-byte block) 
 * 
 * Refer the the MSP430x2xx FAMILY USERS GUIDE, section 7, Flash Memory 
 * Controller for a thorough description of writing to flash memory.
 * 
 * \param  pCopySource  Pointer to the data to be copied
 * \param  numSrcBytes  Number of bytes to be copied
 * \param  pFlashDest   pointer to flash location to copy the data
 * 
 * \return Returns an error code for the whole process (GEN_SUCCESS = 0xFF = success)
 * 
 * CAVEATS:
 * - the routine disables the watch-dog timer if running, restoring its
 *   operation when done
 * - the routine disables interrupts.  The state of GIE is restored at the
 *   end of the routine
 * - data cannot overlap segments
 * 
 * 
 * 05/04/2011 - performed some white-box testing primarily to check new
 * boundaries for relocated data when switching from MSP430F2617 to 
 * MSP430F2618 on the proto ipg board.
 * 
 */
RESPONSE flashCopyDataToFlash(const void *pCopySource, uint16_t numSrcBytes, void *pFlashDest)
{

    /* base address of flash segment to write to */
    uint8_t *pFlashSegment;

    /* return code for routine */
    RESPONSE copyToFlashReturnCode;

    /* save original condition of watch-dog timer */
    uint16_t originalWDTCTL;

    /* save original condition of interrupt enable (GIE) */
    uint16_t originalIntrEnable;

    /* segment size, based on whether normal segment or info segment */
    uint16_t segmentSize;

    copyToFlashReturnCode = flashHw_VerifyAddress(pFlashDest);

    if (copyToFlashReturnCode == GEN_SUCCESS)
    {

        pFlashSegment = flashHw_GetSegment(pFlashDest);
        //are the starting and ending addresses in the same segment?
        if (pFlashSegment == (flashHw_GetSegment((uint8_t *)pFlashDest + numSrcBytes - 1)))
        {

            segmentSize = flashHw_GetSegmentSize(pFlashSegment);

            //disable the watchdog and save the settings                    
            originalWDTCTL = wdSuspend();

            //disable interrupts and save a token of the current state
            originalIntrEnable = interruptDisable();

            //erase the segment
            copyToFlashReturnCode = flashErase(pFlashSegment, segmentSize);

            if (copyToFlashReturnCode == GEN_SUCCESS)
            {

                //erased okay, so try to write the new segment of data (in RAM) to flash
                copyToFlashReturnCode = flashSegmentWrite(pCopySource, pFlashSegment, segmentSize, numSrcBytes);
            }
            //restore the condition of the watch-dog timer
            wdRestore(originalWDTCTL);

            //restore the interrupt enable
            interruptRestore(originalIntrEnable);

        }
        else
        {
            copyToFlashReturnCode = FLASH_ERROR_SEGMENT_BOUNDARY;
        }
    }

    return (copyToFlashReturnCode);
}

 /**
 *		flashErase
 *
 * Erases a 512-byte or 64-byte flash segment.  
 * 
 * Refer to the MSP430x2xx Family Users Guide, section 7.3.2 Erasing
 * Flash Memory.  In this routine, I initiated the erase from within
 * RAM, so we can guard the erase process with a timeout.  (If we 
 * perform the erase from within flash, the CPU is held during the
 * erase process, so we can't guard with a timeout.)
 *  
 * \param segmentBaseAddress address within the segment of flash to
 *                           be erased (normally the first location?).
 * \param segSize size (in bytes) of segment to be erased
 * 
 * \return  error code for the erase process (0xFF = success)
 * 
 * Tested this as a simulation on 02/19/2010.  Used 8MHz DCO MCLK
 * as the flash controller clock, divided by 22 for about 363kHz
 * (right in the middle of the allowed range of 257 to 476kHz). 
 * Per the MSP430f2618-EP data sheet, erasure of a segment should 
 * take 4819 clocks.  4819 * 2.75 usec = 13.2 msecs.  Measured
 * 13.2 msecs.  
 * For this entire routine (which includes checking that segment
 * is erased), measured approx 14.2 msecs.
 * 
 */
RESPONSE flashErase(const uint8_t * segmentBaseAddress, uint16_t segSize)
{

    /* return code for erase segment function */
    RESPONSE eFSReturnCode;

    /* pointer to flash segment to be erased */
    //uint8_t  *erasePtr;
    uint16_t *erasePtr;

    /* counter variable to guard time-out of erase process */
    uint16_t segEraseTimeOut;
    uint16_t i;

    //for test measurement of erase time
    //P6OUT |= BIT2;

    //flash segment erase starts by a write to an address in the segment    
    erasePtr = (uint16_t *) segmentBaseAddress;

    eFSReturnCode = GEN_SUCCESS;        //set temporary success

    //check if flash controller is currently busy (should never happen)
    if (flashHw_GetBUSYflag())
    {
        eFSReturnCode = FLASH_ERASE_ERROR_FC_BUSY;
    }
    else                        //looks okay to start erase
    {
        //copy the erase routine into RAM in preparation to execute the erasing code
        //from RAM
        copy_in(&_flashSegmentEraseTable);

        //set-up to erase the flash
        //set flash control 2 reg to MCLK divided by 22
        flashHw_InitializeClock();

        //set to erase a segment
	    flashHw_Erase();

        //unlock flash memory (keep info block protected)
        flashHw_UnlockFlash();

        //verify that flash controller registers are set-up correctly
        // CAN'T RISK FCTL1 MERAS BIT SET !!!!
        if ((flashHw_GetFCTL1() == FCTL1_ERASE_SETUP_READBACK) && (flashHw_GetFCTL2() == FCTL2_ERASE_SETUP_READBACK)
            && (flashHw_GetFCTL3() == FCTL3_ERASE_SETUP_READBACK))
        {
            //set the timeout value
            segEraseTimeOut = SEG_ERASE_TIMEOUT_VAL + SEG_ERASE_TIMEOUT_MARGIN;

            //do c-call to routine that does assy call to routine in RAM
            eFSReturnCode = flashSegmentErase(erasePtr, segEraseTimeOut);

            //see if erase mechanics were successful
            //first, see if timed out on the erase process
            if (eFSReturnCode == GEN_SUCCESS)
            {
                //verify that the segment was erased
                for (i = 0; i < segSize; i += 2)
                {
                    //check that each word in segment is 0xFFFF
                    if (*erasePtr != 0xFFFF)
                    {
                        eFSReturnCode = FLASH_ERASE_ERROR_BAD_ERASE;
                        break;
                    }
                    else
                    {
                        erasePtr++;
                    }
                }
            }
        }
        else                    //set-up failed
        {
            eFSReturnCode = FLASH_ERASE_ERROR_FC_SETUP;
        }

        //reset lock to protect flash
        flashHw_LockFlash();

        //toggle bit for end of test measurement
        //P6OUT &= ~BIT2;
        //__no_operation();
    }

    return eFSReturnCode;
}



/**
 *		flashSegmentWrite
 *
 * Re-writes RAM image back into flash.
 * 
 * This routine re-writes the RAM image back into flash.
 * It uses the block write technique.  See section 7.3.3 of the
 * MSP430x2xx Family Users Guide, "Writing Flash Memory", in 
 * particular the section on page 7-13, "Block Write".
 * In this routine, we write all eight 64-byte blocks, one at a
 * time, to complete the write of the 512-byte flash segment.
 * (If our target is one of the special 64-byte 'info segments',
 * we just write one 64-byte block.)
 * 
 * \param pFlashSegmentToWrite Address of base of segment to be written
 * \param segSize size of segment to be written, in bytes
 * 
 * \return Completion code for operation (0xFF = success)
 *
 * Tested on 02/19/2010 DJH
 * For the flash controller clock source used the MCLK, from the DCO at 8MHz,
 * divided by 22.  This creates a flash controller timing generator of about
 * 364kHz.  Per the data sheet for the MSP430F2618-EP data sheet, flash memory
 * data, the time to write the block (using words) should be 30 + (21*31)+6
 * TFTGs.
 * 
 *      687 * 2.75 usec 1.88 msec.  
 * 
 * For 8 blocks, that would be 8 * 1.88 msecs = 15 msecs.
 * Measured 13.4 msecs.
 * 
 * (Note that the MSP430X2618 data sheet says 30 tftgs for the first word, 
 * 21 tftgs for the next 31 words and 6 tftgs for the end of the sequence.
 * The MSP430X2XX Family User's Guide says 25, 18 and 6 for these values,
 * which comes out close to the 13 msecs I measured. ?  This was done on the
 * stim proto board with an MSP430F2617.)
 * 
 * Including the final check that the flash segment was identical to the
 * RAM image, the routine to write the entire segment took about 14.4 msecs.
 *  
 */
RESPONSE flashSegmentWrite(const uint8_t *pCopySource, uint8_t *pFlashSegmentToWrite, uint16_t segSize,
                                 uint16_t numSrcBytes)
{
    /* return code for the routine */
    RESPONSE wFSReturnCode;

    /* pointer to RAM buffer */
    uint16_t *ramSrcPtr;

    /* pointer to flag segment to write to */
    uint16_t *flashDestPtr;

    /* counter for the eight 64-byte blocks of the 512-byte segment */
    uint8_t blockCounter;

    /* number of blocks in the segment to be written */
    uint8_t blocksInSegment;

    blocksInSegment = segSize / (NUM_WORDS_IN_FLASH_BLOCK * 2);


    wFSReturnCode = GEN_SUCCESS;        //set temporary success

    //for test measurement of erase time
    //P6OUT |= BIT2;

    //check if flash controller is currently busy (should never happen)
    if (flashHw_GetBUSYflag())
    {
        wFSReturnCode = FLASH_WRITE_ERROR_FC_BUSY;
    }
    else                        //looks okay to start write
    {
        //Copy the code to write the flash into RAM.
        copy_in(&_flashBlockWriteTable);

        //partially set-up the flash controller 
        flashHw_InitializeClock();

        //unlock flash memory (keep info block protected)
        // goal is to set LOCK low.  
        // NOTE THAT WRITING 1 TO LOCKA CHANGES ITS STATE
        flashHw_UnlockFlash();

        //double check flash controller register set-up before going
        if ((flashHw_GetFCTL2() == FCTL2_WRITE_SETUP_READBACK) && (flashHw_GetFCTL3() == FCTL3_WRITE_SETUP_READBACK))
        {

            //do for all of the blocks in the segment
            blockCounter = 0;
            
            while ((blockCounter < blocksInSegment) && (wFSReturnCode == GEN_SUCCESS))
            {
                //calculate pointer in RAM image
                ramSrcPtr = (uint16_t *) pCopySource + (NUM_WORDS_IN_FLASH_BLOCK * blockCounter);

                //calculate pointer to flash segment
                flashDestPtr = (uint16_t *) pFlashSegmentToWrite + (NUM_WORDS_IN_FLASH_BLOCK * blockCounter);

                //call the routine IN RAM to copy the image to flash
                wFSReturnCode = flashBlockWrite(ramSrcPtr, flashDestPtr);
                
                //Go to the next block
                blockCounter++;
            }

            if (wFSReturnCode == GEN_SUCCESS)
            {

                //now verify that the entire segment was written correctly
                //Copy the verify code into RAM
                copy_in(&_flashVerifyTable);

                wFSReturnCode =
                    flashVerify((uint16_t *) pCopySource, (uint16_t *) pFlashSegmentToWrite, numSrcBytes);
            }

        }
        else
        {
            //error setting up for write
            wFSReturnCode = FLASH_WRITE_ERROR_FC_SETUP;
        }
    }

    flashHw_LockFlash();
    //toggle bit for end of test measurement
    //P6OUT &= ~BIT2;
    return wFSReturnCode;
}

/**
 * 	Write to data within a flash segment.
 *
 * 	This method handles making a copy of the flash segment's data, modifying it with the provided
 * 	data, erasing the segment, and writing the merged data back. If the write spans multiple segments,
 * 	multiple writes will be made automatically.
 *
 * 	\param pCopySource A pointer to the data to be copied.
 * 	\param numSrcBytes The number of bytes to copy.
 * 	\param pFlashDest The output location.
 */
RESPONSE flashWriteWithinSegment(const void *pCopySource, uint16_t numSrcBytes, void *pFlashDest)
{
	uint8_t *currentBlock;
	uint16_t currentBlockSize;
	uint16_t bytesToCopyThisBlock;
	uint8_t tmp[512];
	RESPONSE resp;

	while(numSrcBytes > 0)
	{
		currentBlock = flashHw_GetSegment((uint8_t*)pFlashDest);
		currentBlockSize = flashHw_GetSegmentSize((uint8_t*)pFlashDest);
		bytesToCopyThisBlock = currentBlockSize - (((uint8_t*)pFlashDest) - currentBlock);
		if(bytesToCopyThisBlock > numSrcBytes)
		{
			bytesToCopyThisBlock = numSrcBytes;
		}

		//copy the current data out
		memcpy(tmp, currentBlock, currentBlockSize);

		//copy the data
		memcpy(tmp + (((uint8_t*)pFlashDest) - currentBlock), pCopySource, bytesToCopyThisBlock);

		//Write it to flash.
		resp = flashCopyDataToFlash(tmp, currentBlockSize, currentBlock);
		if(resp != GEN_SUCCESS)
		{
			return resp;
		}

		//move on to the next.
		pCopySource = ((uint8_t*)pCopySource) + bytesToCopyThisBlock;
		pFlashDest = currentBlock + currentBlockSize;
		numSrcBytes -= bytesToCopyThisBlock;

	}

	return GEN_SUCCESS;
}


/**
 *		flashEnableFlashAccessViolationFault
 *
 * Enables the flash access violation fault
 *
 */
void flashEnableFlashAccessViolationFault(void)
{
	IE1 |= ACCVIE;
}
