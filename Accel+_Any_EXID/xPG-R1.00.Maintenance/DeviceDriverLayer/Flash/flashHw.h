/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Flash hardware layer.
 * 	This file contains setters and getters for the flash hardware registers.
 *	
 ***************************************************************************/

#ifndef FLASHHW_H_
#define FLASHHW_H_

#include "DeviceDriverLayer/Common/msp430f2618.h"
#include <stdint.h>

#include "Common/Protocol/response_code.h"

/// \ingroup flashHw
/// @{

/*
 * Macros.
 * 
 * If compiling unit tests (TEST defined) and using this file for mocking (TEST_INLINE_FUNCTIONS is
 * not defined), then we need to emit prototypes for mocking insted of
 * macro definitions.
 * 
 * If we are not compiling tests (TEST not defined) or if the macros themselves
 * are being tested (TEST_INLINE_FUNCTIONS defined), then emit the actual
 * macros instead of the prototypes.
 */
#if defined(TEST) && !defined(TEST_INLINE_FUNCTIONS)

//running unit tests - put in function headers that will be "Mocked"

void flashHw_SetFCTL4(uint16_t register_value);
void flashHw_SetFCTL3(uint16_t register_value);
void flashHw_SetFCTL2(uint16_t register_value);
void flashHw_SetFCTL1(uint16_t register_value);

uint16_t flashHw_GetFCTL4(void);
uint16_t flashHw_GetFCTL3(void);
uint16_t flashHw_GetFCTL2(void);
uint16_t flashHw_GetFCTL1(void);

void flashHw_EnableMarginalWriteMode(void);
void flashHw_EnableMarginalWrite1(void);
void flashHw_EnableMarginalWrite0(void);
void flashHw_DisableMarginalWriteMode(void);

void flashHw_UnlockFlash(void);
void flashHw_LockFlash(void);
void flashHw_StartBlockWrite(void);
void flashHw_EndBlockWrite(void);

uint16_t flashHw_GetWAITflag(void);
uint16_t flashHw_GetFAILflag(void);
uint16_t flashHw_GetACCVIFGflag(void);
uint16_t flashHw_GetBUSYflag(void);

#else
//Compiling and running the target code 
#define flashHw_SetFCTL4(register_value) FCTL4=register_value
#define flashHw_SetFCTL3(register_value) FCTL3=register_value
#define flashHw_SetFCTL2(register_value) FCTL2=register_value
#define flashHw_SetFCTL1(register_value) FCTL1=register_value

#define flashHw_GetFCTL4() FCTL4
#define flashHw_GetFCTL3() FCTL3
#define flashHw_GetFCTL2() FCTL2
#define flashHw_GetFCTL1() FCTL1

#define flashHw_ClearMarginalWriteModeBits() flashHw_SetFCTL4(FWKEY)

/**
 * After executing this routine the caller can execute
 * a CRC or memory compare to detect marginal writes of "1" bits.
 * 
 * Testing for marginal writes must occur at 1MHz and must
 * be executed from RAM.
 * 
 * Therefore, call flashCond_EnableMarginalWriteMode() before
 * calling this function. 
 * 
 */
#define flashHw_EnableMarginalWrite1() flashHw_SetFCTL4(FWKEY + MGR1)

/**
 * After executing this routine the caller can execute
 * a CRC or memory compare to detect marginal writes of "0" bits.
 * 
 * Testing for marginal writes must occur at 1MHz and must
 * be executed from RAM.
 * 
 * Therefore, call flashCond_EnableMarginalWriteMode() before
 * calling this function. 
 * 
 */
#define flashHw_EnableMarginalWrite0() flashHw_SetFCTL4(FWKEY + MGR0)




/**
 * Unlocks the flash to enable programming and erasing.
 * 
 */
#define flashHw_UnlockFlash() flashHw_SetFCTL3(FWKEY)

/**
 * Locks the flash.
 * This is usually performed after erasing or programming.
 * 
 */
#define flashHw_LockFlash() flashHw_SetFCTL3(FWKEY + LOCK)


/**
 * Start a block write.
 * 
 * The flash hardware must be configured with the correct clock rate
 * and unlocked for this to work properly.
 * 
 */
#define flashHw_StartBlockWrite() flashHw_SetFCTL1(FWKEY + BLKWRT + WRT)

/**
 * Finish a  block write.
 * 
 * This routine completes a block write.
 * 
 */
#define flashHw_EndBlockWrite() flashHw_SetFCTL1(FWKEY)

/**
 * This routine returns the WAIT bit of FCTL3. 
 * This bit is set when the software should wait during a block write.
 * 
 * \returns 0 if the flag is clear.  Non-zero if it is set.
 * 
 */
#define flashHw_GetWAITflag() (flashHw_GetFCTL3() & WAIT)

/**
 * This routine returns the FAIL bit of FCTL3. 
 * This bit is set when there is a flash memory write failure..
 * 
 * \returns 0 if the flag is clear.  Non-zero if it is set.
 * 
 */
#define flashHw_GetFAILflag() (flashHw_GetFCTL3() & FAIL)

/**
 * This routine returns the ACCVIFG bit of FCTL3. 
 * This bit is set when the there is a flash access violation.
 * 
 * \returns 0 if the flag is clear.  Non-zero if it is set.
 * 
 */
#define flashHw_GetACCVIFGflag() (flashHw_GetFCTL3() & ACCVIFG)

/**
 * This routine returns the ACCVIFG bit of FCTL3. 
 * This bit is set when the there is a flash access violation.
 * 
 * \returns 0 if the flag is clear.  Non-zero if it is set.
 * 
 */
#define flashHw_GetBUSYflag() (flashHw_GetFCTL3() & BUSY)

#endif                          /*defined(TEST) && !defined(TEST_INLINE_FUNCTIONS) */

//these definitions are for both configurations

RESPONSE flashHw_VerifyAddress(uint8_t * pFlashAddress);
uint8_t *flashHw_GetSegment(uint8_t * pFlashAddress);
uint16_t flashHw_GetSegmentSize(uint8_t * pFlashSegment);
void flashHw_InitializeClock(void) ;
void flashHw_Erase(void) ;

/// @}

#endif                          /*FLASHHW_H_ */
