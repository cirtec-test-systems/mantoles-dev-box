/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Flash verify function.
 *
 ***************************************************************************/


#include <stdint.h>
#include <assert.h>

#include "Flash.h"
#include "flash_Int.h"
#include "flashWork.h"
#include "flashHw.h"

#include "Interrupt.h"
#include "Watchdog.h"
#include "delay.h"
#include "Common/Protocol/response_code.h"
#include "cpy_tbl.h"

 /**
 *		flashCond_Verify
 *
 * This function performs a marginal write test of the specified segment
 * of flash memory.
 *
 * \param pSourcePtr 	Pointer to the RAM buffer written to flash.
 * \param pDestPtr		Pointer to the location in FLASH for the buffer to be written.
 * \param segSize		Size of the segment written.
 *
 *  \return Returns an error code for the whole process - GEN_SUCCESS = success,
 * 			FLASH_WRITE_ERROR_BAD_WRITE if an error is detected.
 *
 */
RESPONSE flashVerify(uint16_t * pSourcePtr, uint16_t * pDestPtr, uint16_t segSize)
{

    /** return code for the routine */
    RESPONSE wFSReturnCode;
    uint16_t i;
    uint16_t *pTempSourcePtr;
    uint16_t *pTempDestPtr;
    uint8_t testbit;

    wFSReturnCode = GEN_SUCCESS;

	//During marginal read mode, the flash access speed (MCLK) must be limited
	//to 1 MHz.  This is carried out in the flashCond_EnableMarginalWriteMode()
	//call.
    //set the MCLK clock to 1MHz, leaving SMCLK at 1MHz
    flashEnableMarginalWriteMode();


    //Start the marginal write test with 0
    testbit=0;
    while ((testbit <= 1) && (wFSReturnCode == GEN_SUCCESS))
    {
    	if (testbit==0)
    	{
    		flashHw_EnableMarginalWrite0();
    	}
    	else
    	{
    		flashHw_EnableMarginalWrite1();
    	}


	    //compare the source to the destination
	    pTempSourcePtr = pSourcePtr;
	    pTempDestPtr = pDestPtr;
	    i=0;
	    while ((i < segSize) && (wFSReturnCode == GEN_SUCCESS))
	    {
	        if (*pTempSourcePtr != *pTempDestPtr)
	        {
	            wFSReturnCode = FLASH_WRITE_ERROR_BAD_WRITE;
	        }
	        pTempSourcePtr++;
	        pTempDestPtr++;

	        i += sizeof(uint16_t);
	    }

	    //finished with the bit test, go to the next bit value (if any)
	    testbit++;
    }

    //Clear the marginal write bits and reset the clock.
    flashDisableMarginalWriteMode();

    return wFSReturnCode;

}



