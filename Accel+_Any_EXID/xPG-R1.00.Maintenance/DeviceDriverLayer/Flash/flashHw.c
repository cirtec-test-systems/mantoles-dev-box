/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Functions related to modification of flash memory.
 * This is part of the flash conductor module.
 * Since writing and erasing flash must be accomplished while running from RAM
 * the code in this file uses library functions (copy_in()) and linker symbols to copy
 * data from the program space (in flash) into RAM, and executes the functions
 * from RAM.
 *	
 ***************************************************************************/
#include "flashHw.h"
#include "flashWork.h"
#include "Common/Protocol/response_code.h"

/**
 * 	
 * Return a code indicating the validity of the passed address.
 * 
 * \param 	pFlashAddress - Address we are verifying
 * \return 	This function returns GEN_SUCCESS is valid, else FLASH_ERROR_INVALID_ADDRESS
 *  
 */
RESPONSE flashHw_VerifyAddress(uint8_t * pFlashAddress)
{
    if (((pFlashAddress >= (uint8_t *) LOWEST_WRITABLE_FLASH_ADDR)
         && (pFlashAddress <= (uint8_t *) HIGHEST_WRITABLE_FLASH_ADDR))
        || ((pFlashAddress >= (uint8_t *) INFO_MEM_SEGMENT_D_BASE)
            && (pFlashAddress <= (uint8_t *) INFO_MEM_SEGMENT_A_TOP)))
    {
        return (GEN_SUCCESS);
    }
    else
    {
        return (FLASH_ERROR_INVALID_ADDRESS);
    }
}

/**
 * 	
 * Return the segment of the address passed as a parameter.
 * 
 * \param 	pFlashSegment - Pointer to the address whose segment we return.
 * \return 	This function returns the segment where the passed address resides. 
 *  
 */
uint8_t *flashHw_GetSegment(uint8_t * pFlashAddress)
{

    if (pFlashAddress >= (uint8_t *) BOTTOM_OF_NORMAL_FLASH_ADDR)
    {
        //if in normal flash, calculate segment base address on 512-byte boundary
        return ((uint8_t *) ((uint32_t) pFlashAddress & (uint32_t) SEGMENT_ADDRESS_MASK));
    }
    else
    {
        //target is info block, so calculate based on info block (64-byte) boundary
        return ((uint8_t *) ((uint32_t) pFlashAddress & (uint32_t) INFO_SEG_ADDRESS_MASK));
    }
}

/**
 * 	
 * Return the segment size based on the address passed as a parameter.
 * 
 * \param 	pFlashSegment - Pointer to the address whose segment size we return.
 * \return 	This function returns the size of the segment where the address passed
 * 			as a parameter resides.
 *  
 */
uint16_t flashHw_GetSegmentSize(uint8_t * pFlashSegment)
{
    if (pFlashSegment >= (uint8_t *) BOTTOM_OF_NORMAL_FLASH_ADDR)
    {
        //target address is in the "normal" flash range.
        return (512);
    }
    else
    {
        //target address is in the info block
        return (64);
    }
}

/**
 * 	
 * Set the flash clock.
 *  
 */
void flashHw_InitializeClock(void) 
{
	//set flash control 2 reg to MCLK divided by 22
	FCTL2=FWKEY + FSSEL_1 + FC_CK_DIVIDE;
}

/**
 * 	
 * Erase the current segment,
 *  
 */
void flashHw_Erase(void) 
{
	//erase the segment
	FCTL1=FWKEY + ERASE;
}
