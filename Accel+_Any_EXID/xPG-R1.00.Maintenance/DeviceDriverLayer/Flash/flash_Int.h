/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: flash device driver internal macros.
 * 
 ***************************************************************************/
#ifndef FLASH_INT_H_
#define FLASH_INT_H_
#include "flashHw.h"
#include "Clock.h"

//This function should be copied into RAM and executed in RAM to work properly.
RESPONSE flashBlockWrite(uint16_t * pSourcePtr, uint16_t * pDestPtr);
RESPONSE flashErase(const uint8_t * segmentBaseAddress, uint16_t segSize);
RESPONSE flashSegmentErase(uint16_t * pSegmentAddress, uint16_t timeoutVar);
RESPONSE flashSegmentWrite(const uint8_t *pCopySource, uint8_t *pFlashSegmentToWrite, uint16_t segSize, uint16_t numSrcBytes);
RESPONSE flashVerify(uint16_t * pSourcePtr, uint16_t * pDestPtr, uint16_t segSize);

/*
 * Macros.
 * 
 * If compiling unit tests (TEST defined) and using this file for mocking (TEST_INLINE_FUNCTIONS is
 * not defined), then we need to emit prototypes for mocking insted of
 * macro definitions.
 * 
 * If we are not compiling tests (TEST not defined) or if the macros themselves
 * are being tested (TEST_INLINE_FUNCTIONS defined), then emit the actual
 * macros instead of the prototypes.
 */
#if defined(TEST) && !defined(TEST_INLINE_FUNCTIONS)
void flashDisableMarginalWriteMode(void);
void flashEnableMarginalWriteMode(void);

#else
/**
 * Disables Marginal Write mode of the flash.
 * 
 * After this routine is run, all processing happens at 8MHz rather
 * than the 1MHz set for marginal write testing.
 * 
 * Also, this function clears the marginal write bits.
 * 
 */
#define flashDisableMarginalWriteMode() \
	flashHw_ClearMarginalWriteModeBits();    \
	clk8MHz()

/**
 * Enables Marginal Write mode of the flash.
 * Testing for marginal writes must occur at 1MHz and must
 * be executed from RAM.
 * 
 * After this routine is run, all processing happens at 1MHz rather
 * than the normal 8MHz.
 * 
 */
#define flashEnableMarginalWriteMode() clk1MHz()
#endif                          /*defined(TEST) && !defined(TEST_INLINE_FUNCTIONS) */

#endif                          /*FLASH_INT_H_ */
