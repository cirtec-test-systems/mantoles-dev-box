/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Module to turn on and off the LEDs on the EPG.
 *
 ***************************************************************************/


#include "led.h"
#include "error_code.h"
#include "DeviceDriverLayer/Common/bits.h"
#include "DeviceDriverLayer/Common/msp430f2618.h"

#ifdef EPG

void ledInit(void)
{
	//Turn them all off
	ledStimOff();
	ledGreenOff();
	ledRedOff();
}

void ledStimOn(void)
{
	P7OUT |= LED_STIM;
	P7OUT &= ~LED_STIM;
	P7OUT |= LED_STIM;
}

void ledStimOff(void)
{
	P7OUT &= ~LED_STIM;
}

bool ledIsStimLedOn(void)
{
	return (P7OUT & LED_STIM) != 0;
}

void ledGreenOn(void)
{
	P1OUT |= LED_GREEN;
	P1OUT &= ~LED_GREEN;
	P1OUT |= LED_GREEN;
}

void ledGreenOff(void)
{
	P1OUT &= ~LED_GREEN;
}

bool ledIsGreenLedOn(void)
{
	return (P1OUT & LED_GREEN) != 0;
}

void ledRedOn(void)
{
	P7OUT |= LED_RED;
	P7OUT &= ~LED_RED;
	P7OUT |= LED_RED;
}

void ledRedOff(void)
{
	P7OUT &= ~LED_RED;
}

bool ledIsRedLedOn(void)
{
	return (P7OUT & LED_RED) != 0;
}

#endif  // #ifdef EPG
