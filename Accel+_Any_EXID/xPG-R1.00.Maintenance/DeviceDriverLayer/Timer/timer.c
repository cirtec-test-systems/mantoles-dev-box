/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: timerHw (timer hardware) module
 *	
 ***************************************************************************/
#include "DeviceDriverLayer/Common/msp430f2618.h"
#include "Interrupt.h"

#include "MultiChannelTimer.h"
#include "timer.h"
#include "ticks.h"
#include "EventQueue.h"

#include <intrinsics.h>

#ifdef UNIT_TESTING
    #pragma RETAIN(UT_USE_VLOCLK)
    #ifndef ACLK_USES_LFXT1CLK
		uint8_t UT_USE_VLOCLK = 1;
	#else
		uint8_t UT_USE_VLOCLK = 0;
	#endif
	bool taccr2Intr;
#endif

void timerInit(void)
{

#ifndef ACLK_USES_LFXT1CLK

	TACTL = TASSEL_1	// Select ACLK
			| ID_3		// Divide by 8
			| MC_2		// Continuous mode
			| TACLR;	// Reset the counter and prescaler

#else
#ifdef TIMERA_USES_SMCLK

	TACTL = TASSEL_2	// Select SMCLK
			| ID_3		// Divide by 8
			| MC_2		// Continuous mode
			| TACLR;	// Reset the counter and prescaler

#else

	TACTL = TASSEL_1	// Select ACLK
			| ID_3		// Divide by 8
			| MC_2		// Continuous mode
			| TACLR;	// Reset the counter and prescaler

#endif
#endif

	TACTL |= TAIE;
}
 
int16_t timerTicks(void)
{
	uint16_t a, b, c;

	do {
		a = TAR;
		b = TAR;
		c = TAR;
	} while (!(a == b && b == c));

	return a;
}

void timerConfigureTETSTimer(void)
{
	TACCTL1 = 0;
}

void timerStartTETSTimer(uint16_t ticks)
{
	// Set the timer for a delay from the current time.
	TACCR1 	= ticks;
	TACCTL1 |= CCIE;
}

void timerEnableSWTimerInterrupt(void)
{
	TACCTL2 |= CCIE;
}

void timerDisableSWTimerInterrupt(void)
{
	TACCTL2 &= ~CCIE;
}

void timerSetSWTimer(uint16_t timeout)
{
	TACCR2 = timeout;
}

void timerConfigureSWTimer(void)
{
	TACCTL2 = 0;
}

static inline enum WHAT_TO_DO handleMuxInt(void)
{
	//TAIV is not a bit-field
	switch (TAIV)
	{
		case TAIV_NONE:
			return BACK_TO_SLEEP;
		case TAIV_TACCR1:
			TACCTL1 &= ~CCIE;
			return BACK_TO_SLEEP;
		case TAIV_TACCR2:
			#ifdef UNIT_TESTING
				taccr2Intr = true;
			#endif
			swTimerInterrupt();
			return WAKE_UP;
		case TAIV_6:
			// Should never happen
			return BACK_TO_SLEEP;
		case TAIV_8:
			// Should never happen
			return BACK_TO_SLEEP;
		case TAIV_TAIFG:
			swTimerOverflow();				// This must be done at interrupt level
			return WAKE_UP;
		default:
			return BACK_TO_SLEEP;
	}
}

/**
 * Interrupt handler for the multiplexed A1 (TETS), A2 (swtmr module),
 * TAIV (also swtmr module), and TAIFLG (real-time clock) interrupts.
 *
 * This code is not extremely time-critical.
 */
#pragma vector = TIMERA1_VECTOR
interrupt void timer_HandleMuxInt(void)
{
	interruptHandlerPrologue();
	handleMuxInt();
	_low_power_mode_off_on_exit();
}

uint16_t timerInterruptDisable()
{
	uint16_t state;

	state = TACTL & TAIE;
	TACTL &= ~TAIE;
	return state;
}

void timerInterruptRestore(uint16_t interruptState)
{
	TACTL |= (interruptState & TAIE);
}

