/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: I/O initialization module
 *	
 ***************************************************************************/
 
#include <stddef.h>
#include <stdarg.h>

#include "IOInitialization.h"
#include "DeviceDriverLayer/Common/bits.h"
#include "DeviceDriverLayer/Common/msp430f2618.h"


/**
 * Loads initial register values at startup.  The most important purpose for
 * this is to quickly configure all I/O ports to non-floating values.  It is
 * also used to initialize critical peripherals (such as disabling the watchdog
 * timer), and to begin startup for slow-moving peripherals like LFXT1, so
 * that they can get going while the CPU is busy doing other startup work.
 *
 * Lumping together the port configurations for various subsystems isn't as
 * elegant as doing the configuration in each subsystem's module, but it will
 * let the MSP430 quickly stop its pins from floating, and it is more efficient
 * in both ROM and time than lots of bit sets and clears in the various
 * drivers.  In addition, there is a setup advantage, in that some of the
 * subsystems need to have configuration split into two sections, some done
 * before interrupts are enabled, and some after.  This function handles the
 * setup with interrupts globally disabled, and the subsystem Init functions,
 * called by Executor_Init(), handle setup after interrupts are enabled. 
 */   

void ioInit(void)
{
	/*
	 * Port initialization
	 *
	 * Configure all I/O ports early in the system startup so that no pins
	 * are floating any longer than necessary.  None of the peripherals other
	 * than Pluto are powered up yet, so their outputs should be set to 0
	 * and all inputs should be set to passive pull-down.  Pluto I/Os should
	 * be configured to their default state, whether that is high or low.
	 *
	 * Note that the STIM_ stuff is duplicative of stimHw_Init().  This is
	 * unfortunate, but reluctantly the least likely solution to cause trouble
	 * downstream.
	 *
	 * The Pluto configuration includes setting several important signals high.
	 *
	 *  - First, PWR_UV_PROT is set high.  There is also a pullup on this pin
	 *    provided by Pluto.  If that pin is set low, the xPG will immediately
	 *    power down and enter storage mode.
	 *  - Second, setting PWR_LDO_EN high puts Pluto's LDOD in continuous
	 *    operation. This gives us maximum current for bringing up the system.
	 *  - Third, PWR_MAG_TO_OVRD is set high, which disables the magnet sensor.
	 *    behavior.)
	 */

    //bring up the power asic to full power.
  	P2OUT = PWR_UV_PROT;
    P2DIR = PWR_UV_PROT;
    P8OUT = PWR_LDO_EN;
  	P8DIR = PWR_LDO_EN;
	/*
	 * Port P1.  Interrupt capable GPIO.
	 *
	 * ACC_RANGE 		Output, powered down
	 * ACC_PWR_DN 		Output, powered down
	 * MICS_IRQ 		Input, powered down
	 * PWR_CHARGE_RATE 	Output, 0 = use C/8 rate (also known as LED_GREEN)
	 * PWR_V_DET 		Input
	 * PWR_BRWN_OUT 	Input
	 * PWR_MAG_SW 		Input
	 * LED_RED			Output, red led for EPG
	 * PWR_MICS_STRB	Output, MICS clock strobe
	 */

	P1OUT = 0;
	P1SEL = 0;

	// Note that PWR_CHARGE_RATE and LED_GREEN are the same bit, which is used for different
	// purposes in the IPG and EPG. Both constants are included here to make it clear that this line
	// of code applies to both the IPG and EPG.
#ifdef EPG
	P1DIR = ACC_RANGE | ACC_PWR_DN | (PWR_CHARGE_RATE | LED_GREEN);
#elif defined IPG_REV05
	P1DIR = ACC_RANGE | ACC_PWR_DN | (PWR_CHARGE_RATE | LED_GREEN) | LED_RED;
#else
	P1DIR = ACC_RANGE | ACC_PWR_DN | PWR_CHARGE_RATE;
#endif
	P1REN = MICS_IRQ;

	/*
	 * Port P2.  Interrupt capable GPIO.
	 *
	 * PWR_MAG_2S 	Input
	 * PWR_MAG_5S	Input
	 * PWR_CS		Output (active low)
	 * PWR_UV_PROT	Output
	 * PWR_BBC_CTL	Output
	 * HVDD_CONN	Output
	 * STIM_TFLG	Input, powered down
	 * STIM_ERR		Input, powered down
	 */

  	P2OUT = PWR_CS | PWR_UV_PROT;
  	P2SEL = 0;
    P2DIR = PWR_CS | PWR_UV_PROT | PWR_BBC_CTL | HVDD_CONN;
    P2REN = STIM_TFLG | STIM_ERR;

	/*
	 * Port P3.  Serial ports.
	 *
	 * All of the SPI and UART configuration happens later. At this point,
	 * the pins are configured to idle or powered-down states, as appropriate.
	 *
	 * STIM_SCK 	Output, powered down
	 * PWR_SDI 		Output
	 * PWR_SDO 		Input
	 * PWR_SCK 		Output
	 * STIM_SDI 	Output, powered down
	 * STIM_SDO 	Input, powered down
	 * TETS_TXD 	Output, idle state high
	 * TETS_RXD 	Input
	 */

	P3OUT = TETS_TXD;
	P3SEL = 0;
	P3DIR = STIM_SCK | STIM_SDI | TETS_TXD;
    P3REN = STIM_SDO;

	/*
	 * Port P4 (Timer_B): Saturn control signals
	 *
	 * STIM_STOP 	Output, powered down
	 * STIM_CONT 	Output, powered down
	 * STIM_RUN 	Output, powered down
	 * STIM_SEL 	Output, powered down
	 * STIM_SYNC 	Input, powered down
	 * STIM_PAUSE 	Input, powered down
	 * STIM_PROG 	Input, powered down
	 * STIM_CS 		Output, powered down
	 * STIM_CS 		Output, powered down
	 * MICS_PO3		Output, powered down
	 */

    P4OUT = 0;
    P4SEL = 0;
#ifdef EPG
    P4DIR = STIM_STOP | STIM_CONT | STIM_RUN | STIM_SEL;
#elif defined IPG_REV05
    P4DIR = STIM_STOP | STIM_CONT | STIM_RUN | STIM_SEL | STIM_CS;
#else
    P4DIR = STIM_STOP | STIM_CONT | STIM_RUN | STIM_SEL;
#endif
    P4REN = STIM_SYNC | STIM_PAUSE | STIM_PROG;

	/*
	 * Port P5
	 *
	 * STIM_RST		Output, powered down
	 * MICS_SDI		Output, powered down
	 * MICS_SDO		Input, powered down
	 * MICS_SCK		Output, powered down
	 * MCLK			Unused
	 * STIM_CLK		Output, powered down
	 * ACLK			Unused
	 * MICS_WU_EN	Output, powered down
	 */

	P5OUT = 0;
	P5SEL  = 0;
	P5DIR = STIM_RST | MICS_SDI | MICS_SCK | STIM_CLK | MICS_WU_EN | MCLK;
	P5REN = MICS_SDO;

	/*
	 * Port P6: Analog inputs
	 *
	 * Configure all analog inputs as routed to the ADC12. That disables the
	 * CMOS logic buffer on those pins, allowing them to float without drawing
	 * excess power.
	 *
	 * STIM_IMPM 		Analog input, powered down
	 * STIM_CAL_RESIN 	Analog input, powered down
	 * STIM_CAL_RESOUT	Analog input, powered down
	 * P6_3_NC			Unused
	 * PWR_ANA_OUT		Analog input
	 * ACC_Y 			Analog input, powered down
	 * ACC_X 			Analog input, powered down
	 * ACC_Z 			Analog input, powered down
	 */

	P6OUT = 0;
	P6SEL = PWR_ANA_OUT;
	P6DIR = STIM_IMPM | STIM_CAL_RESIN | STIM_CAL_RESOUT | ACC_Y | ACC_X | ACC_Z;
    P6REN = P6_3_NC;

	/*
	 * Port P7
	 *
	 * MICS_PO4			Input, powered down
	 * P7_1_NC			Unused
	 * P7_2_NC			Unused
	 * P7_3_NC			Unused
	 * STIM_CS			Saturn SCSb input (SPI chip select, active low)
	 * LED_STIM			Output, stim led
	 * P7_5_NC			Unused
	 * LED_RED			EPG - Red LED
	 * MICS_CS			Output, powered down
	 * TST_TRIG			Unused
	 */

	P7OUT = 0;
	P7SEL = 0;
#ifdef EPG
	P7DIR = MICS_CS | TST_TRIG | STIM_CS | LED_STIM | LED_RED;
    P7REN = MICS_PO4 | P7_1_NC | P7_2_NC;
#elif defined IPG_REV05
	P7DIR = MICS_CS | TST_TRIG | LED_STIM;
	P7REN = MICS_PO4 | P7_1_NC | P7_2_NC | P7_3_NC | P7_5_NC;
#else
	P7DIR = LOAD_VRECT | MICS_CS | TST_TRIG | STIM_CS;
    P7REN = P7_1_NC | P7_2_NC | P7_5_NC;
#endif

	/*
	 * Port P8
	 *
	 * ASK_BLANK		Output
	 * PWR_LDO_EN		Output, initially 1
	 * PWR_MAG_TO_OVRD	Output, initially 0
	 * MEM_CS			Output, powered down
	 * MEM_SCK			Output, powered down
	 * MEM_SDI			Output, powered down
	 * MEM_SDO			Input, powered down
	 * EPG_MODE			Input
	 */

    P8OUT = PWR_LDO_EN;
    P8SEL = 0;
  	P8DIR = ASK_BLANK | PWR_LDO_EN | PWR_MAG_TO_OVRD | MEM_CS | MEM_SCK | MEM_SDI;
    P8REN = MEM_SDO;

	/*
	 * Clock startup
	 *
	 * Needs to happen after port setup so that the power ASIC is configured to supply
	 * enough power for the oscillators.
	 */

#ifndef RUN_AT_8_MHZ

    DCOCTL = CALDCO_1MHZ;
  	BCSCTL1 = XT2OFF | DIVA_0 | (CALBC1_1MHZ & 0x0F); // XT2 is off (high freq crystal), LFXT1 is low freq, ACLK is divided by 1

#ifdef RUN_SMCLK_AT_1_MHZ
  	BCSCTL2 = DIVS_0 | DIVM_0; // MCLK driven from DCO, MCLK divided by 1, SMCLK driven from DCO, SMCLK divided by 1, DCO internal resistor
#else
#ifdef RUN_SMCLK_AT_125_KHZ
  	BCSCTL2 = DIVS_3 | DIVM_0; // MCLK driven from DCO, MCLK divided by 1, SMCLK driven from DCO, SMCLK divided by 8, DCO internal resistor
#else
  	BCSCTL2 = DIVS_2 | DIVM_0; // MCLK driven from DCO, MCLK divided by 1, SMCLK driven from DCO, SMCLK divided by 4, DCO internal resistor
#endif
#endif

#else
  	// run the DCO at 8 MHz and the SMCLK at 1 MHz
	DCOCTL = CALDCO_8MHZ;  			// Configure DCO for factory-calibrated 8 MHz

	// bits:1000 0101
  	BCSCTL1 = XT2OFF 				// Turn off XT2 oscillator
  		  | DIVA_0					// ACLK = LFXT1 / 1 = 32.768 kHz
  		  | (CALBC1_8MHZ & 0x0F);	// Configure RSEL for factory-calibrated 8 MHz  0x0005

  	BCSCTL2 = DIVS_3	 			// SMCLK = DCO / 8 = 1 MHz
  			| DIVM_0;	 	 	 	// MCLK  = DCO / 1 = 8 MHz

#endif

#ifndef ACLK_USES_LFXT1CLK

  	BCSCTL3 = LFXT1S_2
          | XCAP_2;

#else
  	// bits: 0000 1000
  	BCSCTL3 = LFXT1S_0	 			// LFXT1 oscillator has 32768 Hz crystal and drives ACLK
          | XCAP_2;		 			// LFXT1 cap set to 8.5 pF
#endif

  	/*
  	 * LFXT1 capacitor setting is based on the recommendations
  	 * in Micro Crystal "Crystal Recommendations for TI MSP430 Microcontrollers"
  	 * http://www.ti.com/lit/ml/slaa367/slaa367.pdf
  	 */
}

