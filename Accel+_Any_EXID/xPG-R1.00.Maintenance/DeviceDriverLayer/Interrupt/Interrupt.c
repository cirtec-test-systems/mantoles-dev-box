/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Interrupt-management functions
 * 
 *  My original plan was to define these functions inline in the Interrupt.h
 *  header file, but CMock was unable to parse them correctly and crashed.
 *  Reluctantly, I took the drop in speed and made them non-inline.  Will
 *  have to reconsider inlining them -- or at least replacing disable() and
 *  enable() directly with their intrinsic equivalents -- once it's time to
 *  look at performance.   
 *	
 ***************************************************************************/

#include "Interrupt.h"
#include "DeviceDriverLayer/Common/msp430f2618.h"

#include <intrinsics.h>

/**
 * Enables interrupts, no questions asked.
 */
 
void interruptEnable(void)
{
	_enable_interrupts();
}

/**
 * Disables interrupts and returns a token that can be passed to interrupt_SetInterruptState()
 * to restore interrupts to their prior state.
 * 
 * \return  An opaque token representing the GIE state.
 */

uint16_t interruptDisable(void)
{
	uint16_t token;
	
	token = _get_SR_register() & GIE;
	_disable_interrupts();

	return token;	
}

/**
 * Restore interrupts to their state before a call to interruptDisable().
 * 
 * \param sr  The opaque token returned by interrupt_GetInterruptState().
 */
 
void interruptRestore(uint16_t interruptState)
{
	(void) _bis_SR_register(interruptState);
}

/**
 * Clears the interrupt status register after NMI is detected
 */

void interruptClearNMI(void)
{
	IFG1 &= ~NMIIFG;
}

/**
 * Check whether the NMI interrupt flag is set.
 */

bool interruptIsNMI(void)
{
	return (IFG1 & NMIIFG) != 0;
}

