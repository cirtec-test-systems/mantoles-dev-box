/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: General Interrupt Service Routines
 *	
 ***************************************************************************/

#include "isr.h"
#include "Interrupt.h"

#include "error_code.h"
#include "EventQueue.h"
#include "mics.h"
#include "PowerASIC.h"
#include "Stim.h"
#include "Watchdog.h"
#include "log.h"
#include "Clock.h"
#include "delay.h"
#include "system.h"

/*
 * Public API (sort of) -- see Interrupt.h for documentation.
 *
 * Routes the Port1 interrupt to the right places.
 */
#pragma vector = PORT1_VECTOR
interrupt void isrPort1(void)
{
	interruptHandlerPrologue();

	/*
	 * Note that the interrupt flag for each line is cleared by the handler
	 * responsible for it.
	 */
   	micsHandleInterrupt();
   	pwrHandlePort1Interrupt();

	_low_power_mode_off_on_exit();
}


/*
 * Public API (sort of) -- see Interrupt.h for documentation.
 *
 * Routes the Port2 interrupt to the right places.
 */
#pragma vector = PORT2_VECTOR
interrupt void isrPort2(void)
{
	interruptHandlerPrologue();
	/*
	 * Note that the interrupt flag for each line is cleared by the handler
	 * responsible for it.
	 */
	pwrHandlePort2Interrupt();
	stimHandleInterrupt();

	_low_power_mode_off_on_exit();
}



/*
 * Public API (sort of) -- see Interrupt.h for documentation.
 *
 * Handler for all unused interrupt vectors.
 */

// The correct thing to do for unused interrupt vectors is almost always either
// (1) log it and ignore it or (2) force a reset, equivalent to a watchdog reset.

/*@unused@*/
#pragma vector = RESERVED0_VECTOR
interrupt void isrUnusedReserved0(void)
{
	interruptHandlerPrologue();

	resetSystem();
}

/*@unused@*/
#pragma vector = RESERVED1_VECTOR
interrupt void isrUnusedReserved1(void)
{
	interruptHandlerPrologue();

	resetSystem();
}

/*@unused@*/
#pragma vector = RESERVED2_VECTOR
interrupt void isrUnusedReserved2(void)
{
	interruptHandlerPrologue();

	resetSystem();
}

/*@unused@*/
#pragma vector = RESERVED3_VECTOR
interrupt void isrUnusedReserved3(void)
{
	interruptHandlerPrologue();

	resetSystem();
}

/*@unused@*/
#pragma vector = RESERVED4_VECTOR
interrupt void isrUnusedReserved4(void)
{
	interruptHandlerPrologue();

	resetSystem();
}

/*@unused@*/
#pragma vector = RESERVED5_VECTOR
interrupt void isrUnusedReserved5(void)
{
	interruptHandlerPrologue();

	resetSystem();
}

/*@unused@*/
#pragma vector = RESERVED6_VECTOR
interrupt void isrUnusedReserved6(void)
{
	interruptHandlerPrologue();

	resetSystem();
}

/*@unused@*/
#pragma vector = RESERVED7_VECTOR
interrupt void isrUnusedReserved7(void)
{
	interruptHandlerPrologue();

	resetSystem();
}

/*@unused@*/
#pragma vector = RESERVED8_VECTOR
interrupt void isrUnusedReserved8(void)
{
	interruptHandlerPrologue();

	resetSystem();
}

/*@unused@*/
#pragma vector = RESERVED9_VECTOR
interrupt void isrUnusedReserved9(void)
{
	interruptHandlerPrologue();

	resetSystem();
}

/*@unused@*/
#pragma vector = RESERVED10_VECTOR
interrupt void isrUnusedReserved10(void)
{
	interruptHandlerPrologue();

	resetSystem();
}

/*@unused@*/
#pragma vector = RESERVED11_VECTOR
interrupt void isrUnusedReserved11(void)
{
	interruptHandlerPrologue();

	resetSystem();
}

/*@unused@*/
#pragma vector = RESERVED12_VECTOR
interrupt void isrUnusedReserved12(void)
{
	interruptHandlerPrologue();

	resetSystem();
}

/*@unused@*/
#pragma vector = RESERVED13_VECTOR
interrupt void isrUnusedReserved13(void)
{
	interruptHandlerPrologue();

	resetSystem();
}

/*@unused@*/
#pragma vector = DAC12_VECTOR
interrupt void isrUnusedDAC12(void)
{
	interruptHandlerPrologue();

	resetSystem();
}

/*@unused@*/
#pragma vector = DMA_VECTOR
interrupt void isrUnusedDMA(void)
{
	interruptHandlerPrologue();

	resetSystem();
}

// Vectors that are used
//#pragma vector = USCIAB1TX_VECTOR
//#pragma vector = USCIAB1RX_VECTOR
//#pragma vector = PORT1_VECTOR
//#pragma vector = PORT2_VECTOR

/*@unused@*/
#pragma vector = RESERVED20_VECTOR
interrupt void isrUnusedReserved20(void)
{
	interruptHandlerPrologue();

	resetSystem();
}

/*@unused@*/
#pragma vector = ADC12_VECTOR
interrupt void isrUnusedADC12(void)
{
	interruptHandlerPrologue();

	resetSystem();
}

/*@unused@*/
#pragma vector = USCIAB0TX_VECTOR
interrupt void isrUnusedUSCIAB0TX(void)
{
	interruptHandlerPrologue();

	resetSystem();
}

/*@unused@*/
#pragma vector = USCIAB0RX_VECTOR
interrupt void isrUnusedUSCIAB0RX(void)
{
	interruptHandlerPrologue();

	resetSystem();
}

// TimerA1 vector is used
//#pragma vector = TIMERA1_VECTOR

/*@unused@*/
#pragma vector = TIMERA0_VECTOR
interrupt void isrUnusedTimerA0(void)
{
	interruptHandlerPrologue();

	resetSystem();
}

/*@unused@*/
#pragma vector = COMPARATORA_VECTOR
interrupt void isrUnusedComparatorA(void)
{
	interruptHandlerPrologue();

	resetSystem();
}

/*@unused@*/
#pragma vector = TIMERB0_VECTOR
interrupt void isrUnusedTimerB0(void)
{
	interruptHandlerPrologue();

	resetSystem();
}

#pragma vector = NMI_VECTOR
interrupt void isrNMI(void)
{
	interruptHandlerPrologue();

	if ((IFG1 & OFIFG) != 0)
	{
		clkClearOscillatorFault();

		if (clkIsOscillatorFault())
		{
			dispPutErrorEvent(ACTERR_CLOCK_ERROR_MAJOR);
		}
	}

	if ((FCTL3 & ACCVIFG) != 0)
	{
		dispPutErrorEvent(ACTERR_INTERNAL_SW_ERROR_CAT1);
	}

	if ((IFG1 & NMIIFG) != 0)
	{
		dispPutErrorEvent(ACTERR_INTERNAL_SW_ERROR_CAT1);
	}

	_low_power_mode_off_on_exit();
}


