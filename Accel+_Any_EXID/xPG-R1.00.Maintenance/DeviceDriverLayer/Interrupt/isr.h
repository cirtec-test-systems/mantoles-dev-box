/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description:
 *	
 ***************************************************************************/



#ifndef INTERRUPT_ISR_H_
#define INTERRUPT_ISR_H_

/// @defgroup isr	General Interrupt Service Routines
/// @ingroup interrupt

#include "Interrupt.h"

/**
 * Demultiplex the Port 1 interrupt inputs.
 *
 * Each pin on GPIO Port 1 can be configured to generate an interrupt, but
 * all of those interrupts come through a single interrupt vector.  The port
 * is shared between multiple xPG peripherals, and hence multiple device drivers.
 *
 * This function handles the port 1 interrupt, checks the state of the pins,
 * and calls out to the appropriate driver or drivers to handle the interrupt.
 *
 * \warning This function is an interrupt handler, invoked by the hardware.
 * 			It is not to be called directly.
 */
/*@unused@*/
interrupt void isrPort1(void);

/**
 * Demultiplex the Port 2 interrupt inputs.
 *
 * Each pin on GPIO Port 2 can be configured to generate an interrupt, but
 * all of those interrupts come through a single interrupt vector.  The port
 * is shared between multiple xPG peripherals, and hence multiple device drivers.
 *
 * This function handles the port 2 interrupt, checks the state of the pins,
 * and calls out to the appropriate driver or drivers to handle the interrupt.
 *
 * \warning This function is an interrupt handler, invoked by the hardware.
 * 			It is not to be called directly.
 */
/*@unused@*/
interrupt void isrPort2(void);



/*
 * Function prototypes of various interrupt handlers, for test accessibility
 * and for keeping various code-checkers happy.
 */

interrupt void isrUnusedReserved0(void);
interrupt void isrUnusedReserved1(void);
interrupt void isrUnusedReserved2(void);
interrupt void isrUnusedReserved3(void);
interrupt void isrUnusedReserved4(void);
interrupt void isrUnusedReserved5(void);
interrupt void isrUnusedReserved6(void);
interrupt void isrUnusedReserved7(void);
interrupt void isrUnusedReserved8(void);
interrupt void isrUnusedReserved9(void);
interrupt void isrUnusedReserved10(void);
interrupt void isrUnusedReserved11(void);
interrupt void isrUnusedReserved12(void);
interrupt void isrUnusedReserved13(void);
interrupt void isrUnusedDAC12(void);
interrupt void isrUnusedDMA(void);
interrupt void isrUnusedReserved20(void);
interrupt void isrUnusedADC12(void);
interrupt void isrUnusedUSCIAB0TX(void);
interrupt void isrUnusedUSCIAB0RX(void);
interrupt void isrUnusedTimerA0(void);
interrupt void isrUnusedComparatorA(void);
interrupt void isrUnusedTimerB0(void);
interrupt void isrNMI(void);
interrupt void isrWDT(void);

#endif /* INTERRUPT_ISR_H_ */
