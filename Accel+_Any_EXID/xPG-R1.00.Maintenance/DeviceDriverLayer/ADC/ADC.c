/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: ADC device driver.
 *	
 ***************************************************************************/

#include <stddef.h>

#include "delay.h"
#include "ADC.h"
#include "lastresponse.h"
#include "timeout.h"
#include <Watchdog.h>

static uint16_t g_completionMask;
static uint8_t	g_referenceCount = 0;

void adcResetReferenceCount()
{
	ADC12CTL0 = 0;
	g_referenceCount = 0;
}

void adcOn()
{
	if(g_referenceCount == 0)
	{
		ADC12CTL0 = ADC12ON | REFON | REF2_5V;
	}
	g_referenceCount++;
	DELAY_MS(17); // per MSP430 user guide
}

void adcSetup(ADC_CONFIG const *config)
{
	int n;	// Number of the sequencer configuration register we're about to fill
	int i;	// Iterate through the number of inputs
	int j;	// Iterate through the number of reps

	// disable ADC
	ADC12CTL0 &= ~ENC;

	if (config->speed == ADC_SLOW)
	{
		ADC12CTL0 &= 0x00FF;  // clear the SHT0 and SHT1 sample time bits
		ADC12CTL0 |= config->sampleTime;

#ifdef RUN_AT_8_MHZ
		// in this case the DCO is running at 8 MHz and the SMCLK is running at 1 MHz (see IOInitialization.c)
		//Sample-and-hold pulse-mode select, Sequence-of-channels, Select clock SMCLK / 2
		ADC12CTL1 = SHP | CONSEQ_1 | ADC12DIV_1 | ADC12SSEL_3;
#else
		// in this case the DCO is running at 8 MHz.  Use the MCLK at 1 MHz as the input clock and divide it by two.
		ADC12CTL1 = SHP | CONSEQ_1 | ADC12DIV_0 | ADC12SSEL_3;
#endif

	}
	else
	{
		ADC12CTL0 &= 0x00FF;  // clear the SHT0 and SHT1 sample time bits
		ADC12CTL0 |= config->sampleTime;			// 128 cycles

		// Sample-and-hold pulse-mode select, Sequence-of-channels, Select clock  ADC12OSC / 1
		ADC12CTL1 = SHP | CONSEQ_1;
	}


	// configure trigger
	ADC12CTL1 = (ADC12CTL1 & ~SHS_3) | (config->trigger * SHS_1);

	if (config->seqType == CONVERT_ALL)
	{
		// Run the whole sequence from a single trigger
		ADC12CTL0 |= MSC;
	}
	else
	{
		// Require a trigger for each conversion in the sequence
		ADC12CTL0 &= ~MSC;
	}

	// Set up each conversion in the sequence
	n = 0;
	for (j = 0; j < config->reps; j++)
	{
		for (i = 0; i < config->numInputs; i++)
		{
			// select VREF and AVSS as VR+ and VR- reference voltages
			ADC12MCTL[n++] = (uint8_t)(SREF_1 | (ADC_SELECT)(config->firstInput + i));
		}
	}
	// indicate last conversion in the sequence (multiple inputs are converted sequentially so indicate last one)
	ADC12MCTL[n-1] |= EOS;

	g_completionMask = 1 << (n - 1);

	// clear interrupts
	ADC12IFG = 0;

	// Enable the conversion, arm the trigger, and lock in the settings.
	ADC12CTL0 |= ENC;

}

void adcConvert(void)
{
	//start by clearing the flag that indicates the measurement is completed.
	ADC12IFG 	= 0;

	// software start the conversion by turning on the bit in the register
	ADC12CTL0 	|= ADC12SC;

}


// poll the interrupt flag register until the very last conversion has completed
uint16_t const volatile *adcResults(int16_t timeout)
{
	TIMEOUT giveUp;

	giveUp = timeoutSet(timeout);
	while (!timeoutIsExpired(giveUp))
	{
		wdServiceWatchdog();
		if (ADC12IFG & g_completionMask != 0)
		{
			return (uint16_t *)ADC12MEM;
		}
	}

	setResponseCode(RESP_TIMED_OUT);

	return NULL;
}

void adcOff(void)
{
	g_referenceCount--;

	if(g_referenceCount == 0)
	{
		ADC12CTL0 = 0;
	}
}



