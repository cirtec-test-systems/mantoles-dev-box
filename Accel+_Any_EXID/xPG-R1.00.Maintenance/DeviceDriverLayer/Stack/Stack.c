/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Stack Conductor module -- manages stack filling
 * 		  and integrity checks.
 *	
 ***************************************************************************/

#include <stdint.h>

#include "Stack.h"
#include <intrinsics.h>

/*@-exportheadervar@*/
/// Linker-defined external symbol for the start (lowest address) of the stack space
extern uint16_t _stack[];
/*@=exportheadervar@*/

void stackInit(void)
{
	uint16_t *p;

	for (p = _stack; p < (uint16_t *)_get_SP_register(); p++)
	{
		*p = FILL_VALUE;
	}
}

int stackCheck(void)
{
	return _stack[0] == FILL_VALUE ? 0 : -1;
}
