/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Magnet device driver.
 *
 ***************************************************************************/
#include "magnet.h"
#include "DeviceDriverLayer/Common/bits.h"
#include "DeviceDriverLayer/Common/msp430f2618.h"
#include <stdbool.h>

void magnetInit(void)
{
	// Set up the magnet 2 second interrupt
	//P2IES &= ~PWR_MAG_2S;		// Rising edge interrupt
	P2IFG &= ~PWR_MAG_2S;		// Prevent a superfluous interrupt
	P2IE &= ~PWR_MAG_2S;		// Disable the interrupt

	//set up the magnet switch interrupt
	P1IES |= PWR_MAG_SW;		// Falling edge interrupt
	P1IFG &= ~PWR_MAG_SW;		// Prevent a superfluous interrupt
	P1IE |= PWR_MAG_SW;			// Enable the interrupt

}

/**
 * Returns whether or not the magnet is engaged.
 */

bool magnetIsMagnetEngaged(void)
{
	return ((P1IN & PWR_MAG_SW) != 0);
}

