HERE = File.expand_path(File.dirname(__FILE__)) + '/'

require 'rake'
require 'rake/clean'
require 'rake/testtask'
require 'rake/loaders/makefile'
require './rakefile_helper'
require "./vendor/cmock/lib/cmock.rb" 

include RakefileHelpers

# Load default configuration, for now
DEFAULT_CONFIG_FILE = 'gcc.yml'
configure_toolchain(DEFAULT_CONFIG_FILE)

OBJDIR = '../build'
INITINFO = File.join(OBJDIR, 'initial.info')
TESTINFO = File.join(OBJDIR, 'test.info')
TOTALINFO = File.join(OBJDIR, 'total.info')
MOCKDIR = '../mocks'
COVDIR = '../coverage'
DOCDIR = '../xPG_doc'
UNITYDIR = 'vendor/cmock/vendor/unity/src'
CMOCKDIR = 'vendor/cmock/src'
CONFIG = 'gcc.yml'
TOPLEVELDIRS = '{ApplicationLayer,Common,DataStore,DeviceDriverLayer,DeviceManagementLayer,EventDispatch,Include}'

SRC = FileList["#{TOPLEVELDIRS}/**/*.c", "#{CMOCKDIR}/*.c", "#{UNITYDIR}/*.c"]
HDRS = FileList["#{TOPLEVELDIRS}/**/*.h", "#{CMOCKDIR}/*.h", "#{UNITYDIR}/*.h"]

unity_o = File.join(OBJDIR, "unity.o")
cmock_o = File.join(OBJDIR, "cmock.o")

CLEAN.include(File.join(OBJDIR, '*.*'))
CLEAN.include(File.join(MOCKDIR, '*.*'))
CLOBBER.include(DOCDIR)
CLOBBER.include(OBJDIR)
CLOBBER.include(MOCKDIR)
CLOBBER.include(COVDIR)

#
# Set up configuration from YAML file
#

load_configuration($cfg_file)
test_defines = ['TEST']
$cfg['compiler']['defines']['items'] = [] if $cfg['compiler']['defines']['items'].nil?
$cfg['compiler']['defines']['items'] << 'TEST'

include_dirs = get_local_include_dirs

# Kluge up directory creation that does not output to stderr, so as to not
# screw up the TFS build server's scripts that equate anything written to
# stderr with a test failure.
file MOCKDIR do
	Dir.mkdir(MOCKDIR)
end
file OBJDIR do
	Dir.mkdir(OBJDIR)
end

# These are not run on the build server, so we can use the built-in behavior
directory DOCDIR
directory COVDIR


task :default => [:unit, :summary]

desc "Run all unit tests and the test summary"
task :all => [:clean, :unit, :summary]

task :unit => [ MOCKDIR, OBJDIR ]

task :old_unit => [ MOCKDIR, OBJDIR ] do
  run_tests(get_unit_test_files)
end

desc "Generate test summary"
task :summary do
  report_summary
end

desc "Select configuration"
task :config, :config_file do |t, args|
  configure_toolchain(args[:config_file])
end

desc "Generate doxygen documentation"
task :docs => DOCDIR do
	sh "doxygen doxyfile"
end

#
# Test coverage stuff using lcov.  The cygwin stuff is an almighty kluge and
# should be replaced by fixing lcov to run under native Windows.
#

cov_report = File.join(COVDIR, "index.html")

desc "Run test coverage"
task :coverage => [:clean, cov_report]

file TOTALINFO => [OBJDIR, INITINFO, TESTINFO] do
	execute "c:/cygwin/bin/bash -l -c \"cd #{pwd}; lcov -a #{INITINFO} -a #{TESTINFO} -d #{OBJDIR} -b . -o #{OBJDIR}/tmp0.info\""
	execute "c:/cygwin/bin/bash -l -c \"cd #{pwd}; lcov -r #{OBJDIR}/tmp0.info **/stdio.h -o #{OBJDIR}/tmp1.info\""
	execute "c:/cygwin/bin/bash -l -c \"cd #{pwd}; lcov -r #{OBJDIR}/tmp1.info */tests/* -o #{OBJDIR}/tmp2.info\""
	execute "c:/cygwin/bin/bash -l -c \"cd #{pwd}; lcov -r #{OBJDIR}/tmp2.info vendor/cmock/src/* -o #{OBJDIR}/tmp3.info\""
	execute "c:/cygwin/bin/bash -l -c \"cd #{pwd}; lcov -r #{OBJDIR}/tmp3.info vendor/cmock/vendor/unity/src/* -o #{OBJDIR}/tmp4.info\""
	execute "c:/cygwin/bin/bash -l -c \"cd #{pwd}; lcov -r #{OBJDIR}/tmp4.info /c/xpg/build/* -o #{OBJDIR}/tmp5.info\""
	execute "c:/cygwin/bin/bash -l -c \"cd #{pwd}; lcov -r #{OBJDIR}/tmp5.info */*/tests/* -o #{OBJDIR}/tmp6.info\""
	execute "c:/cygwin/bin/bash -l -c \"cd #{pwd}; lcov -r #{OBJDIR}/tmp6.info ./dispatchgen.inc -o #{OBJDIR}/tmp7.info\""
	execute "c:/cygwin/bin/bash -l -c \"cd #{pwd}; lcov -r #{OBJDIR}/tmp7.info /c/xpg/mocks/* -o #{TOTALINFO}\""
end

file INITINFO do
	execute "c:/cygwin/bin/bash -l -c \"cd #{pwd}; lcov -c -i -d #{OBJDIR} -b . -o #{INITINFO}\""
end

file TESTINFO => :unit do
	execute "c:/cygwin/bin/bash -l -c \"cd #{pwd}; lcov -c -d #{OBJDIR} -b . -o #{TESTINFO}\""
end

file cov_report => [COVDIR, TOTALINFO] do
	execute "c:/cygwin/bin/bash -l -c \"cd #{pwd}; genhtml -p /c/xpg/xPG -t 'xPG Firmware' -o #{COVDIR} #{TOTALINFO}\""
end

#
# Dependency-based builder written by SCT @ QiG.
# Functional replacement for the procedural builder in rakefile_helper.rb.
# Some functions from rakefile_helper are still used herein.
#

def find_source(hfile)
    base = File.basename(hfile, '.h')
    SRC.find { |s| File.basename(s, '.c') == base }
end

def find_header(hfile)
	base = File.basename(hfile)
	if (base =~ /Mock/)
        File.join(MOCKDIR, base)
    else
    	h = HDRS.find { |s| File.basename(s) == base }
    	if h.nil?
    		hfile
    	else
    		h
    	end
    end
end

def obj(fn)
	File.join(OBJDIR, File.basename(fn).ext('o'))
end

def mockh(fn)
	File.join(MOCKDIR, "Mock" + File.basename(fn))
end

def mockc(fn)
	mockh(fn).ext('c')
end

def link(exe_name, obj_list)
    linker = build_linker_fields
    cmd_str = "#{linker[:command]}#{linker[:options]}#{linker[:includes]} " +
        (obj_list.map{|obj|"#{obj} "}).join +
        $cfg['linker']['bin_files']['prefix'] + ' ' +
        $cfg['linker']['bin_files']['destination'] + exe_name
    execute(cmd_str)
end


file cmock_o do
	compile "#{CMOCKDIR}/cmock.c"
end

file unity_o do
	compile "#{UNITYDIR}/unity.c"
end


FileList['**/tests/test*.c'].each do |test_c|
	test_exe = obj(test_c).ext('exe')
	test_result = obj(test_c).ext('testresult')
	test_o = obj(test_c)
	runner_o = File.join(OBJDIR, File.basename(test_c, '.c') + "_Runner.o")
	runner_c = runner_o.ext('c')
	
	objs = FileList[test_o, runner_o, cmock_o]
	
	# Extract headers
    header_list = extract_headers(test_c)
    header_list.each do |header|
        if (header =~ /Mock/)
        	objs.add(obj(header))
        	file runner_o => File.join(MOCKDIR, header)
        elsif !find_source(header).nil?
        	objs.add(obj(header))
        end
    end

	file test_o => header_list.collect { |x| find_header(x) } do
		compile test_c
	end
	
	file runner_o => runner_c do
		compile runner_c
	end
	
	file runner_c => [test_c, CONFIG] do
		puts "Generating #{runner_c}"
        test_gen = UnityTestRunnerGenerator.new($cfg_file)
        test_gen.run(test_c, runner_c)
	end
	
	file test_exe => objs do
		link(test_exe, objs)
	end

	task :unit => test_result

	file test_result => test_exe do
      output = execute(test_exe)
      File.open(test_result, 'w') { |f| f.print output }
	end

end

FileList["#{TOPLEVELDIRS}/**/*.h"].each do |h|
	mock_h = mockh(h)
	mock_c = mockc(h)
	mock_o = obj(mock_h)
	
	file mock_h => [h, CONFIG] do
        @cmock ||= CMock.new($cfg_file)
	    @cmock.setup_mocks([h])
	end

	file mock_c => [h, CONFIG] do
        @cmock ||= CMock.new($cfg_file)
	    @cmock.setup_mocks([h])
	end
	
	file mock_o => [mock_h, mock_c] do
		compile mock_c
	end
end

FileList["#{TOPLEVELDIRS}/**/*.c"].exclude('**/tests/test*.c').each do |c|
	file obj(c) => c do
		compile c
	end
	
	file INITINFO => obj(c)
end


#
# Set up dependencies for compiling all C files.
#

# gcc with the -MD option will generate dependency makefiles with a .d suffix.
# Tell rake that these should be imported as makefile syntax.
module Rake
	Rake.application.add_loader('d', MakefileLoader.new)
end

# Import any dependencies gcc has generated.
#
# See http://mad-scientist.net/make/autodep.html

FileList["#{OBJDIR}/*.d"].each do |dep|
	import dep				# Import the dependency file directly
	
	# Read the file and set up empty tasks for the dependencies.
	# This prevents unnecessary "Don't know how to build task 'foo.h'" errors

# This doesn't work, and I don't have time to try to figure out why. :-(
#	s = IO.read(dep)		# Read the file into a string
#	s[/^[^:]*: */] = ''		# Remove the leading 'target.o: ' text.
#	s.delete! '\\'			# Remove any backslash line-continuation characters
#	s.split.each do |fn|	# Define tasks for the remaining filenames
#		file fn
#	end
	
end
