@:: /***************************************************************************/
@:: /*                        CONFIDENTIAL & PROPRIETARY                       */
@:: /*                             Template Rev 2.4                            */
@:: /***************************************************************************/
@:: 
@:: /****************************************************************************
@::  *	
@::  *	Copyright (c) 2011 QIG Group
@::  *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
@::  *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
@::  *	
@::  *	Project: 24-channel SCS IPG
@::  *	
@::  *	@file Description: Splint C static analysis tool driver batch file  
@::  *	
@::  ***************************************************************************/

@echo off

:: Runs the Splint static analysis tool on the xPG firmware source code
:: with a reasonable set of options.  The options set here will
:: undoubtedly evolve over time.

setlocal
setlocal enabledelayedexpansion

set SPLINTDIR=c:\splint-3.1.2
set SPLINT=%SPLINTDIR%\bin\splint.exe
set INCLUDE=c:/ti/ccsv5/tools/compiler/msp430_4.1.2/include

if exist %SPLINT% goto :run

@echo Splint is not installed.
@echo To install Splint:
@echo 1. Go to https://github.com/maoserr/splint_win32/downloads
@echo 2. Download split-3.1.2.msi.
@echo 3. Run it and install in %SPLINTDIR%  (splint.exe should be in %SPLINT%)
@echo Then re-run %0
goto done

:run

:: Splint needs to run at the project root, so use the batch file location
:: to change directories to the project root.
   
cd %~dp0..

:: Build the list of source files
rem set sources=
for /D %%d in (ApplicationLayer\* Common DataStore\* DeviceDriverLayer\* DeviceManagementLayer\* EventDispatch\*) do (
	for %%f in (%%d\*.c) do (
		set sources=!sources! %%f
	)
)

:: There is a bug in splint for Windows that causes it to view .\path\include-file.h and path\include-file.h as
:: different paths.
::
:: To work around this bug, the -redef option is enabled for Windows, even though that disables detection of
:: legitimate multiple definitions. The cygwin version of splint does not have the bug, so the cygwin version
:: of this script does not disable the redef option.

:: -paren-file-format selects gcc-format message syntax, for consistency with the CCS tools.

%SPLINT% -f Tools\splint-options -redef -paren-file-format %sources%

:done
