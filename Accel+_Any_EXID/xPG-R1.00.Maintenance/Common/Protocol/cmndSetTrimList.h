/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Define the Trim List packet structure
 *	
 ***************************************************************************/

#ifndef CMNDSETTRIMLIST_H_
#define CMNDSETTRIMLIST_H_

#include <stdint.h>

#include "trim_list.h"

/// \defgroup setTrimList Set Trim List command
/// \ingroup mics_protocol
/// Sets one of the IPG's trim lists, used to load ASIC trim registers during
/// startup.
///  
/// @{

typedef TRIM_LIST_CMD SET_TRIM_LIST_PARAMS;

/// @}

#endif /*CMNDSETTRIMLIST_H_*/
