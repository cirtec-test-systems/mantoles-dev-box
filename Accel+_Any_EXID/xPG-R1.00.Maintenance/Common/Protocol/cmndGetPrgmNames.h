/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.2                            */
/***************************************************************************/

/****************************************************************************
 *	QiG group Source File
 *	
 *	Copyright (c) 2010 QiG group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	QiG Part Number: Number
 *	Project: Project
 *	File Name: $RCSfile$
 *	@version: $Revision$
 *	@author: $Author$
 *	
 *	$Id$
 *	
 *	@file Description: Description
 *	
 ***************************************************************************/

#ifndef CMNDGETPRGMNAMES_H_
#define CMNDGETPRGMNAMES_H_

#include "cmndCommon.h"
#include "prgmDef.h"

/// \defgroup getPrgmNamesCmnd Get Program Names Command
/// \ingroup mics_protocol
/// Gets program map and program names for 10 patients programs.
///  
/// @{



/// Get Prgm Names - Response Data
/// - NOTE: Group individual byte sized parameters in pairs.   

typedef struct GET_PRGM_NAMES_RESPONSE_DATA
{
	/// bitmap indicating if programs P1 thru P10 are valid
    /// - values: see #_PROGRAM_BITMAP
    uint16_t selectableProgramMap;

    /// array of program names
    char programName[NUM_PRGM_DEFS][PRGM_NAME_CHARS];
} GET_PRGM_NAMES_RESPONSE_DATA;


/// @}

#endif // CMNDGETPRGMNAMES_H_
 
 
 
/**********************************************************************
 * Revision History
 *
 * $Log$
 *
 **********************************************************************/
