/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Define the structure for the Select Program command.
 *	
 ***************************************************************************/

#ifndef CMNDSLCTPRGM_H_
#define CMNDSLCTPRGM_H_

#include <stdint.h>

/// \defgroup slctPrgmCmnd Select Program Command
/// \ingroup mics_protocol
/// Specify which xPG Program is select to run when stimulation is active.
///  
/// @{

/// Select Program - Parameter Data
/// - NOTE: Group individual byte sized parameters in pairs.   
typedef struct SLCT_PRGM
{
	/// indicates which program number to run when stimulation is active.
	/// - valid values: see #_PROGRAM_NUMBER
	uint8_t program;

} SLCT_PRGM_PARAMS; 

/// @}

#endif // CMNDSLCTPRGM_H_
 
 
 
/**********************************************************************
 * Revision History
 *
 * $Log$
 *
 **********************************************************************/
