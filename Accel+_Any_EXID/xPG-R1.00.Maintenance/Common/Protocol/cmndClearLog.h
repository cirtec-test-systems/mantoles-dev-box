/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2011 QIG Group
 *	1771 E. 30th St. Cleveland, OH 44114
 *
 *	Project: SCS System - IPG Firmware
 *
 *	@file Description: Data structure definition for Open Log Command.
 *
 ***************************************************************************/
#ifndef CMNDCLEARLOG_H_
#define CMNDCLEARLOG_H_

/*
 * This command clears all event entries in the given log by writing zeros
 * over each memory location.  This ensures that the log entry data is erased.
 * The serial number for the log is reset to 0 and the log contains 0 log
 * entries after this command is executed.
 */

typedef struct CLEAR_LOG_PARAMS
{
	/// The identifying code for the counter to retrieve.
	uint8_t logNum;  		//Number of the Event Log.
							//0 = Major Event Log
							//1 = Normal Event Log
							//2 = Routine Event Log

} CLEAR_LOG_PARAMS;



#endif /* CMNDCLEARLOG_H_ */
