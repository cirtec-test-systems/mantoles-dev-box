/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: General Calibration data structure
 *	
 ***************************************************************************/

#ifndef GEN_CALS_H_
#define GEN_CALS_H_

#include <stdint.h>

#define NUM_BATT_CHARGE_LEVEL_STATE_THRESHOLDS 6

/// Data type for general calibrations
typedef struct GEN_CALS {
	/// Thermistor values for the thresholds between the 16 temperature states
	uint16_t tempStateCal[15];

	uint16_t temperatureMinimumThreshold; //nominal 10C
	uint16_t temperatureWarningThreshold; //nominal 40C
	uint16_t temperatureCriticalThreshold; //nominal 41C
	uint16_t temperatureAbortThreshold; //nominal 42C

	uint16_t thermOffsetMinimum;
	uint16_t thermOffsetMaximum;
	uint16_t thermBiasMinimum;
	uint16_t thermBiasMaximum;
	uint16_t thermInputMinimum;
	uint16_t thermInputMaximum;

	uint16_t batteryIncreasePreCharge; 			//nominal 30mV
	uint16_t batteryIncreaseConstantCurrent; 	//nominal 10mV

	/// Pluto on-chip temperature sensor values for the thresholds between the
	/// 16 temperature states
	uint16_t powerAsicTempStateCal[15];
	
	/// Thresholds between the seven battery states, in ADC units, with stim off
	uint16_t batteryStateCalStimOff[NUM_BATT_CHARGE_LEVEL_STATE_THRESHOLDS];
	
	/// Thresholds between the seven battery states, in ADC units, with stim on
	uint16_t batteryStateCalStimOn[NUM_BATT_CHARGE_LEVEL_STATE_THRESHOLDS];
	
	///  Modeled load impedance, in ohms.
	uint16_t rTarget;
} GEN_CALS;


#endif /*GEN_CALS_H_*/
