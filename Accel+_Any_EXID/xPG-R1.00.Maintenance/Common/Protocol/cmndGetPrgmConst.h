/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	Project: SCS System - IPG Firmware
 *	
 *	@file Description: Data structure for the Get Program Constants command.
 * 
 ***************************************************************************/

#ifndef CMNDGETPRGMCONST_H_
#define CMNDGETPRGMCONST_H_

#include "prgmConst.h"

/// \defgroup prgmConstCmnd Get Program Constants Command
/// \ingroup mics_protocol
/// Set / get the program constant parameters for the xPG.
///  
/// @{

/// The response for a Get Program Constants command is simply a PRGM_CONST
/// structure.

typedef struct GET_PRGM_CONST_RESPONSE_DATA {
	PRGM_CONST prgmConst;
} GET_PRGM_CONST_RESPONSE_DATA;

/// @}


#endif /*CMNDGETPRGMCONST_H_*/
