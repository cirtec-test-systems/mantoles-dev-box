/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	Project: SCS System - IPG Firmware
 *	
 *	@file Description: Header / get/set command data format for stim asic
 *                     channel calibrations
 *	
 ***************************************************************************/
 
#ifndef CMNDSTIMASICCHANNELCALS_H_
#define CMNDSTIMASICCHANNELCALS_H_

#include "prgmDef.h"       //for number of electrode channels

/// \defgroup getSetChannelCals Get/Set Channel Calibrations Commands 
/// \ingroup mics_protocol
/// Configure or retrieve the channel calibration information
///  
/// @{

/// Max channel cal value
#define MAX_CHANNEL_CAL 1023 		//set to full scale output
#define NUM_CHANNEL_CAL_TABLES 4	//number of calibration tables

/// Channel calibrations - parameter data structure
typedef struct CHANNEL_CALS
{
	uint16_t index;

	/// channel calibrations, channel sourcing current table 1
	uint16_t channelCalSourcing[NUM_CHANNELS];

	/// channel calibrations, channel sinking current table 1
	uint16_t channelCalSinking[NUM_CHANNELS];
} CHANNEL_CALS;

/// Channel calibrations - parameter data structure
typedef struct GET_CHANNEL_CALS_PARAMS
{
	uint16_t index;
} GET_CHANNEL_CALS_PARAMS;

/// @}

#endif /*CMNDSTIMASICCHANNELCALS_H_*/
