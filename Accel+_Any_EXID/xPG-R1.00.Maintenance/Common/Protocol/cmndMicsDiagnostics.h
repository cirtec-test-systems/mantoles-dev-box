/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2011 QIG Group
 *	1771 E. 30th St. Cleveland, OH 44114
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description:
 *
 ***************************************************************************/

#ifndef CMND_MICS_DIAGNOSTICS_H
#define CMND_MICS_DIAGNOSTICS_H


#define MICS_DIAGNOSTICS_MODE_RSSI 0
#define MICS_DIAGNOSTICS_MODE_TRANSMIT_CW 1
#define MICS_DIAGNOSTICS_MODE_TRANSMIT_MODULATED 2
#define MICS_DIAGNOSTICS_MODE_400_RECEIVE 3
#define MICS_DIAGNOSTICS_MODE_24_RECEIVE 4

#define MICS_DIAGNOSTICS_CURRENT_CHANNEL 255

typedef struct {
	uint8_t mode;
	uint8_t channel;
	uint16_t delay;
	uint16_t durration;
} MICS_DIAGNOSTICS_PARAMS;

#endif
