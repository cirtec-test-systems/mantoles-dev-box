/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Restore Program Defaults
 *	
 ***************************************************************************/

#ifndef CMNDRSTRPRGMDFLT_H_
#define CMNDRSTRPRGMDFLT_H_

/// \defgroup rstrPrgmDfltCmnd Restore Program Defaults Command
/// \ingroup mics_protocol
/// Restore the selected program(s) to its default program definition values.
///  
/// @{

/// Restore Program Defaults - Parameter Data
/// - NOTE: Group individual byte sized parameters in pairs.   
typedef struct RSTR_PRGM_DFLT
{
	/// indicates which program number to run when stimulation is active.
	/// - valid values: see #_PROGRAM_BITMAP
	uint16_t programMap;

} RSTR_PRGM_DFLT_PARAMS; 


/// @}

#endif // CMNDRSTRPRGMDFLT_H_
 
 
 
/**********************************************************************
 * Revision History
 *
 * $Log$
 *
 **********************************************************************/
