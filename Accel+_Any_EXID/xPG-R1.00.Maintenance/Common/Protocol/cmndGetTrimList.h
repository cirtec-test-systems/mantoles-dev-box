/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Define the Get Trim List packet structures
 *	
 ***************************************************************************/

#ifndef CMNDGETTRIMLIST_H_
#define CMNDGETTRIMLIST_H_

#include <stdint.h>

#include "trim_list.h"

/// \defgroup getTrimList Get Trim List command
/// \ingroup mics_protocol
/// Gets one of the IPG's trim lists, used to load ASIC trim registers during
/// startup.
///  
/// @{

typedef struct GET_TRIM_LIST_PARAMS {
	uint8_t listID;		///< Selects the trim list to retrieve
} GET_TRIM_LIST_PARAMS;

typedef TRIM_LIST_CMD GET_TRIM_LIST_RESPONSE_DATA;

/// @}

#endif /*CMNDGETTRIMLIST_H_*/
