/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Command tokens from SWEX0085.
 *	
 ***************************************************************************/

#ifndef COMMANDTOKENS_H_
#define COMMANDTOKENS_H_


/// \defgroup cmdTokens Command Tokens
/// \ingroup mics_protocol
/// Defines to represent Tokens identifying individual commands.
/// Token values limited to unsigned byte values 0x0001 - 0xFFFE. 
///  

// @{

/// enumerated list of command tokens 
typedef enum CMND_TOKEN
{
	/// Get xPG Status Command
	/// - Used By: CP PPC PoP IPG EPG
	/// - Sends: (no data - Command Only)
	/// - Returns: Operational status of xPG, primarily that req'd for PoP UI display.     
	/// 	- #_XPG_STATUS
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///
	/// @sa @link getXpgStatCmnd Get xPG Status Command @endlink
	TKN_GET_XPG_STATUS = 1,


	/// Stimulation Command
	/// - Used By: CP PPC PoP IPG EPG
	/// - Sends: On|Off
	///		- # _STIM
	/// - Returns: (no data)     
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link stimCmnd  Stimulation Command  @endlink
	TKN_STIM = 2,


	/// Select Program Command
	/// - Used By: CP PPC PoP IPG EPG
	/// - Sends: Program Number
	/// 	- #_SLCT_PRGM
	/// - Returns: (no data)     
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link slctPrgmCmnd  Select Program Command  @endlink
	TKN_SLCT_PRGM = 3,


	/// Increment Program Amplitude Command
	/// - Used By: CP PPC PoP IPG EPG
	/// - Sends: (no data - Command Only)
	/// - Returns: ALL INDIVIDUAL Virtual PULSE Step Indexes for presently selected program.      
	/// 	- #_INC_PRGM_AMPL_RESPONSE_DATA
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link incPrgmAmplCmnd  Increment Program Amplitude Command  @endlink
	TKN_INC_PRGM_AMPL = 4,


	/// Decrement Program Amplitude Command
	/// - Used By: CP PPC PoP IPG EPG
	/// - Sends: (no data - Command Only)
	/// - Returns: ALL INDIVIDUAL Virtual PULSE Step Indexes for presently selected program.      
	/// 	- #_DEC_PRGM_AMPL_RESPONSE_DATA
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link decPrgmAmplCmnd  Decrement Program Amplitude Command  @endlink
	TKN_DEC_PRGM_AMPL = 5,


	/// Get PoP Constants Command 
	/// - Used By: -- --- PoP IPG EPG
	/// - Sends: (no data - Command Only)
	/// - Returns: non-Dynamic PoP operational parameter values.      
	/// 	- #_GET_POP_CONST_RESPONSE_DATA
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link getPopConstCmnd  Get PoP Constants Command  @endlink
	TKN_GET_POP_CONST = 6,


	/// Get PPC Constants Command 
	/// - Used By: CP PPC --- IPG EPG
	/// - Sends: (no data - Command Only)
	/// - Returns: non-Dynamic PPC operational parameter values.      
	/// 	- #_GET_PPC_CONST_RESPONSE_DATA
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link getPpcConstCmnd  Get PPC Constants Command  @endlink
	TKN_GET_PPC_CONST = 7,


	/// Get Program Names command
	/// - Used By: CP PPC --- IPG EPG
	/// - Sends: (no data - Command Only)
	/// - Returns: Program valid map and program names for 10 patient programs
	/// - Response Codes:
	///             - #_CMND_RESP_COMMON
	///
	///  @sa @link getPrgmNamesCmnd Get Program Names Command @endlink
	TKN_GET_PRGM_NAMES = 8,


	/// Set Pulse Amplitude Command 
	/// - Used By: CP PPC --- IPG EPG
	/// - Sends: Pulse Index, amplitudeStepIndex
	/// 	- #_SET_PULSE_AMPL_PARAMS
	/// - Returns: Virtual PULSE Step Index for specified pulse of presently selected program.      
	/// 	- #_SET_PULSE_AMPL_RESPONSE_DATA
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link setPulsAmplCmnd  Set Pulse Amplitude Command  @endlink
	TKN_SET_PULSE_AMPL = 9,


	/// Increment Pulse Amplitude Command 
	/// - Used By: CP PPC --- IPG EPG
	/// - Sends: Pulse Index
	/// 	- #_INC_PULSE_AMPL_PARAMS
	/// - Returns: Individual PULSE Logical Step Indexes for specified pulse of presently selected program.     
	/// 	- #_INC_PULSE_AMPL_RESPONSE_DATA
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link incPulsAmplCmnd  Increment Pulse Amplitude Command  @endlink
	TKN_INC_PULSE_AMPL = 10,


	/// Decrement Pulse Amplitude Command 
	/// - Used By: CP PPC --- IPG EPG
	/// - Sends: Pulse Index
	/// 	- #_DEC_PULSE_AMPL_PARAMS
	/// - Returns: Individual PULSE Logical Step Indexes for specified pulse of presently selected program.     
	/// 	- #_DEC_PULSE_AMPL_RESPONSE_DATA
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link decPulsAmplCmnd  Decrement Pulse Amplitude Command  @endlink
	TKN_DEC_PULSE_AMPL = 11,


	/// Get Pulse Widths Command
	/// - Used By: CP PPC --- IPG EPG
	/// - Sends: (no data - Command Only)
	/// - Returns: ALL Individual Pulse Widths for presently selected program.      
	/// 	- #_GET_PULSE_WIDTHS_RESPONSE_DATA
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link getPulsWdthCmnd  Get Pulse Widths Command  @endlink
	TKN_GET_PULSE_WIDTHS = 12,


	/// Set Pulse Width Command  
	/// - Used By: CP PPC --- IPG EPG
	/// - Sends: Pulse Index, pulseWidth
	/// 	- #_SET_PULSE_WIDTH_PARAMS
	/// - Returns: (no data)     
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link setPulseWidthCmnd  Set Pulse Width Command  @endlink
	TKN_SET_PULSE_WIDTH = 13,


	/// Increment Pulse Width Command 
	/// - Used By: CP PPC --- IPG EPG
	/// - Sends: Pulse Index
	/// 	- #_INC_PULSE_WIDTH_PARAMS
	/// - Returns: Individual Pulse Width for specified pulse of presently selected program.      
	/// 	- #_INC_PULSE_WIDTH_RESPONSE_DATA
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link incPulsWdthCmnd  Increment Pulse Width Command  @endlink
	TKN_INC_PULSE_WIDTH = 14,


	/// Decrement Pulse Width Command 
	/// - Used By: CP PPC --- IPG EPG
	/// - Sends: Pulse Index
	/// 	- #_DEC_PULSE_WIDTH_PARAMS
	/// - Returns: Individual Pulse Width for specified pulse of presently selected program.      
	/// 	- #_DEC_PULSE_WIDTH_RESPONSE_DATA
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link decPulsWdthCmnd  Decrement Pulse Width Command  @endlink
	TKN_DEC_PULSE_WIDTH = 15,


	/// Get Program Frequency Command 
	/// - Used By: CP PPC --- IPG EPG
	/// - Sends: (no data - Command Only)
	/// - Returns: Frequency Table Index for presently selected program.     
	/// 	- #_GET_PRGM_FREQ_RESPONSE_DATA
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link getPrgmFreqCmnd  Get Program Frequency Command  @endlink
	TKN_GET_PRGM_FREQ = 16,


	/// Set Program Frequency Command 
	/// - Used By: CP PPC --- IPG EPG
	/// - Sends: Frequency Table Index
	/// 	- #_PRGM_FREQ_INDEX
	/// - Returns: (no data)     
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link setPrgmFreqCmnd  Set Program Frequency Command  @endlink
	TKN_SET_PRGM_FREQ = 17,


	/// Increment Program Frequency Command 
	/// - Used By: CP PPC --- IPG EPG
	/// - Sends: (no data - Command Only)
	/// - Returns: Frequency Table Index for presently selected program.     
	/// 	- #_INC_PRGM_FREQ_RESPONSE_DATA
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link incPrgmFreqCmnd  Increment Program Frequency Command  @endlink
	TKN_INC_PRGM_FREQ = 18,


	/// Decrement Program Frequency Command 
	/// - Used By: CP PPC --- IPG EPG
	/// - Sends: (no data - Command Only)
	/// - Returns: Frequency Table Index for presently selected program.     
	/// 	- #_DEC_PRGM_FREQ_RESPONSE_DATA
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link decPrgmFreqCmnd  Decrement Program Frequency Command  @endlink
	TKN_DEC_PRGM_FREQ = 19,


	/// Charging Command 
	/// - Used By: TEST
	/// - Sends: EVENT ID
	/// 	- #ACTIVE_ERROR_CODE
	/// - Returns: (no data)     
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link chrgCmnd  Charging Command  @endlink
	TKN_INJECT_EVENT = 20,


	/// Charging Control Command 
	/// - Used By: -- PPC --- IPG ---
	/// - Sends: Charger TETS Output Level
	/// 	- #_CHARGING_CONTROL_PARAMS
	/// - Returns: Charging State, Received TETS Output Level, IPG Temperature
	/// 	- #_CHARGING_CONTROL_RESPONSE_DATA
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link chrgCntrlCmnd  Charging Control Command  @endlink
	TKN_CHARGING_CONTROL = 21,

	/// Get xPG Identity Command 
	/// - Used By: CP PPC --- IPG EPG
	/// - Sends: (no data - Command Only)
	/// - Returns: Model Number, Serial Number, Firmware Version     
	/// 	- #_GET_XPG_IDENT_RESPONSE_DATA
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link getXpgIdentCmnd  Get xPG Identity Command  @endlink
	TKN_GET_XPG_IDENT = 22,

	/// Set xPG Identity Command
	TKN_SET_XPG_IDENT = 23,

	/// Restore Program Defaults Command 
	/// - Used By: CP PPC --- IPG EPG
	/// - Sends: Program Select Flags
	/// 	- #_RSTR_PRGM_DFLT
	/// - Returns: (no data)     
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link rstrPrgmDfltCmnd  Restore Program Defaults Command  @endlink
	TKN_RSTR_PRGM_DFLT = 24,

	/// Get Program Definition Command 
	/// - Used By: CP PPC --- IPG EPG
	/// - Sends: Program Number
	/// 	- #_GET_PRGM_DEF
	/// - Returns: Program Definition     
	/// 	- #_GET_PRGM_DEF_RESPONSE_DATA
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link getPrgmDefCmnd  Get Program Definition Command  @endlink
	TKN_GET_PRGM_DEF = 25,

	/// Set Program Definition Command 
	/// - Used By: CP --- --- IPG EPG
	/// - Sends: Program Number, Program & Pulse Definitions
	/// 	- #_SET_PRGM_DEF_PARAMS
	/// - Returns: (no data)     
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link setPrgmDefCmnd  Set Program Definition Command  @endlink
	TKN_SET_PRGM_DEF = 26,

	/// Get Program Constants Command 
	/// - Used By: CP PPC --- IPG EPG
	/// - Sends: (no data - Command Only)
	/// - Returns: Program Constants
	/// 	- #_GET_PRGM_CONST_RESPONSE_DATA
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link prgmConstCmnd  Get Program Constants Command  @endlink
	TKN_GET_PRGM_CONST = 27,

	/// Set Program Constants Command 
	/// - Used By: CP --- --- IPG EPG
	/// - Sends: Program Constants
	/// 	- #_SET_PRGM_CONST_PARAMS
	/// - Returns: (no data)     
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link prgmConstCmnd  Set Program Constants Command  @endlink
	TKN_SET_PRGM_CONST = 28,

	/// Get Configurable Device Parameters Command 
	/// - Used By: CP --- --- IPG EPG
	/// - Sends: (no data - Command Only)
	/// - Returns: Configurable Device Parameters
	/// 	- #_CONFIG_DEVICE_PARAMETERS
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link configDeviceParamsCmnd  Get Configurable Device Parameters Command  @endlink
	TKN_GET_CONFIG_DEVICE_PARAMS = 29,

	/// Set Configurable Device Parameters Command 
	/// - Used By: CP --- --- IPG EPG
	/// - Sends: Configurable Device Parameters
	/// 	- #_CONFIG_DEVICE_PARAMETERS
	/// - Returns: (no data)     
	/// - Response Codes:
	///		- #_CMND_RESP_COMMON   
	///   
	///  @sa @link configDeviceParamsCmnd  Set Configurable Device Parameters Command  @endlink
	TKN_SET_CONFIG_DEVICE_PARAMS = 30,

	/// Get Pulse Constants Command
	TKN_GET_PULSE_CONST = 31,
	
	/// Set Pulse Constants Command
	TKN_SET_PULSE_CONST = 32,

	/// Get Lead Limits Command
	TKN_GET_LEAD_LIMITS = 33,
	
	/// Set Lead Limits Command
	TKN_SET_LEAD_LIMITS = 34,

	/// Get Channel Cals Command
	TKN_GET_CHANNEL_CALS = 35,
	
	/// Set Channel Cals Command
	TKN_SET_CHANNEL_CALS = 36,

  	/// Reserved
	TKN_UNUSED_37 = 37,
  
  	/// Reserved
	TKN_UNUSED_38 = 38,
  
  	/// Reserved
	TKN_UNUSED_39 = 39,
 
  	/// Reserved
  	TKN_UNUSED_40 = 40,
  
  	/// Reserved
  	TKN_UNUSED_41 = 41,

	/// Reserved
	TKN_UNUSED_42 = 42,

	/// Get Stim ASIC HV Cals Command
	TKN_GET_STIM_ASIC_HV_CALS = 43,
	
	/// Set Stim ASIC HV Cals Command
	TKN_SET_STIM_ASIC_HV_CALS = 44,
	
	/// Reserved
	TKN_UNUSED_45 = 45,
	
	/// Reserved
	TKN_UNUSED_46 = 46,
	
	/// Get CP Data Command
	TKN_GET_CP_DATA = 47,
	
	/// Set CP Data Command
	TKN_SET_CP_DATA = 48,
	
	/// Open Log Command
	TKN_CLEAR_LOG = 49,
	
	/// Read Log Command
	TKN_READ_LOG = 50,
	
	/// Append Log Command
	TKN_APPEND_LOG = 51,
	
	/// Get Program Time Command
	TKN_UNUSED_52 = 52,
	
	/// Set Program Time Command
	TKN_UNUSED_53 = 53,
	
	/// Impedance Measurement Command
	TKN_IMP_MEAS = 54,
	
	/// Storage command
	TKN_STORAGE = 55,
  
	/// Short Echo Command (8 bytes of data)
	TKN_ECHO_SHORT = 56,
	
	/// Long Echo Command (256 bytes of data)
	TKN_ECHO_LONG = 57,
	
	/// Get General Calibrations command
	TKN_GET_GEN_CAL = 58,
	
	/// Set General Calibrations command
	TKN_SET_GEN_CAL = 59,
	
	/// Get Trim List command
	TKN_GET_TRIM_LIST = 60,
	
	/// Set Trim List command
	TKN_SET_TRIM_LIST = 61,
	
	/// Get Counter command
	TKN_GET_COUNTER = 62,
	
	/// Set Counter command
	TKN_SET_COUNTER = 63,
	
	/// Get Test Stimulation command
	TKN_GET_TEST_STIM = 64,

	/// Set Test Stimulation command
	TKN_SET_TEST_STIM = 65,

	/// Get Titration Stimulation command
	TKN_GET_TITR_STIM = 66,

	/// Set Titration Stimulation command
	TKN_SET_TITR_STIM = 67,

	/// Get Ramp Time command
	TKN_GET_RAMP_TIME = 68,

	/// Set Ramp Time command
	TKN_SET_RAMP_TIME = 69,

	/// Get VBAT command.  Used for testing, calibration, and troubleshooting.
	TKN_GET_VBAT = 70,

	///Set software description block
	TKN_SET_SDB = 71,

	///Get software description block
	TKN_GET_SDB = 72,

	///Write Memory
	TKN_WRITE_MEMORY = 73,

	///Read Memory
	TKN_READ_MEMORY = 74,

	///Reset xPG
	TKN_RESET_XPG = 75,

	///Erase flash block
	TKN_ERASE_FLASH_BLOCK = 76,

	///Write Interrupt Vectors
	TKN_WRITE_VECTORS = 77,

	///Read Interrupt Vectors
	TKN_READ_VECTORS = 78,

	///Diagnostic Data
	TKN_DIAG_DATA = 79,

	///Set Background Impedance Data
	TKN_SET_BG_IMPEDANCE = 80,

	///Get Background Impedance Data
	TKN_GET_BG_IMPEDANCE = 81,

	/// Set Test Waveform
	TKN_SET_TEST_WAVEFORM = 82,

	/// Calibrate Channel
 	TKN_CALIBRATE_CHANNEL = 83,

 	/// Get the log range
 	TKN_GET_LOG_RANGE = 84,

 	/// Get Antenna Tuning
 	TKN_GET_ANT_TUNE = 85,

 	/// Set Antenna Tuning
 	TKN_SET_ANT_TUNE = 86,

 	/// Set MICS Options
 	TKN_MICS_OPTIONS = 87,

 	// Pair the current PPC
 	TKN_PAIR_PPC = 88,

 	//Pair the next device as a PoP
 	TKN_PAIR_POP = 89,

 	//Stop the bootloader if it is running
 	TKN_STOP_BOOTLOADER = 90,

 	// Tune the MICS antenna
 	TKN_MICS_TUNE = 91,

 	//Perform a MICS diagnostic function
 	TKN_MICS_DIAGNOSTICS = 92,

	/// A command token that is, by definition, undefined.  Used for testing.
	/// \warning Do not implement this token in any way, shape, or form.
	///          Discard packets containing it exactly as you would discard
	///			 any other undefined command.
	TKN_X_UNDEFINED = 0x7faa

} CMND_TOKEN;

// @}

#endif // COMMANDTOKENS_H_
 
/**********************************************************************
 * Revision History
 *
 * $Log$
 *
 **********************************************************************/
