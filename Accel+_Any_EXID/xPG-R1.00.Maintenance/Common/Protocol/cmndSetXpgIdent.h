/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Define the XPG Identity structure.
 *	
 ***************************************************************************/
#ifndef CMNDSETXPGIDENT_H_
#define CMNDSETXPGIDENT_H_

#include "cmndCommon.h"
#include "mics.h"

/// \defgroup setXpgIdentCmnd Set xPG Identity Command 
/// \ingroup mics_protocol
/// Set information related to the mics ID, the model number and serial number.
///  
/// @{

///XPG Identity parameter data structure
typedef struct XPG_IDENTITY
{
  XPG_MICS_ID xpgMicsId;   ///< mics ID bytes
  char  xpgModelNumber[MODEL_NUMBER_CHARS];     ///< device's model number
  char  xpgSerialNumber[SERIAL_NUMBER_CHARS];   ///< device's serial number
} XPG_IDENTITY;



/// @}

#endif /*CMNDSETXPGIDENT_H_*/
 
 
 
/**********************************************************************
 * Revision History
 *
 * $Log$
 *
 **********************************************************************/
