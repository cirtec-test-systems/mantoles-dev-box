/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2011 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Define the Background Impedance Data Structures
 *
 ***************************************************************************/

#ifndef CMNDBACKGROUNDIMPEDANCE_H_
#define CMNDBACKGROUNDIMPEDANCE_H_

#define DEFAULT_IMPEDANCE_AMPLITUDE 0x1
#define DEFAULT_IMPEDANCE_PULSE_WIDTH 250
#define DEFAULT_IMPEDANCE_FREQUENCY_INDEX 30
#define DEFAULT_IMPEDANCE_PULSES 4
#define DEFAULT_IMPEDANCE_IMPEDANCE 10000
#define DEFAULT_OCC_PASS_FAIL_THRESHOLD  50

#define DEFAULT_IMPEDANCE_MAX_PULSES 16
#define DEFAULT_IMPEDANCE_MIN_PULSES 1
#define DEFAULT_IMPEDANCE_MAX_IMPEDANCE 26000
#define DEFAULT_IMPEDANCE_MIN_IMPEDANCE 1000
#define DEFAULT_IMPEDANCE_MIN_VOLTAGE 3000

typedef struct BACKGROUND_IMPEDANCE_PARAMS
{
// Proposed change
	uint8_t bgiAmplitude;		///< Background impedance: Amplitude of measurement pulse, in uA
	uint8_t  enabled;			///< Enable/disable background impedance measurement and output capacitor check (0 = disabled, 1 = enabled)
} BACKGROUND_IMPEDANCE_PARAMS;

#endif /* CMNDBACKGROUNDIMPEDANCE_H_ */
