/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.2                            */
/***************************************************************************/

/****************************************************************************
 *	QiG group Source File
 *	
 *	Copyright (c) 2010 QiG group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	QiG Part Number: Number
 *	Project: Project
 *	File Name: $RCSfile$
 *	@version: $Revision$
 *	@author: $Author$
 *	
 *	$Id$
 *	
 *	@file Description: Description
 *	
 ***************************************************************************/

#ifndef CMNDDECPRGMAMPL_H_
#define CMNDDECPRGMAMPL_H_

#include "prgmDef.h"

/// \defgroup decPrgmAmplCmnd Decrement Program Amplitude Command
/// \ingroup mics_protocol
/// Decrement the amplitude (step indexes) of the presently selected program.
///  
/// @{

/// Decrement Program Amplitude - Response Data
/// - NOTE: Group individual byte sized parameters in pairs.   

typedef struct DEC_PRGM_AMPL_RESPONSE_DATA
{
	/// bitmap indicating if pulse PA thru PD is defined for presently selected program. 
	/// - values: see #_PULSE_SLOT_BIT
	uint8_t definedPulseMap;
	/// virtual amplitude step indexes of individual pulses in presently selected program. 
	/// - values: see #_PULSE_VIRTUAL_AMPL_STEP_LIMIT
	int8_t  pulseVirtualStepIndex[NUM_PULSES];

} DEC_PRGM_AMPL_RESPONSE_DATA;

/// @}

#endif // CMNDDECPRGMAMPL_H_
 
 
 
/**********************************************************************
 * Revision History
 *
 * $Log$
 *
 **********************************************************************/
