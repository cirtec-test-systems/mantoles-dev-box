/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.2                            */
/***************************************************************************/

/****************************************************************************
 *	QiG group Source File
 *	
 *	Copyright (c) 2010 QiG group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	QiG Part Number: Number
 *	Project: Project
 *	File Name: $RCSfile$
 *	@version: $Revision$
 *	@author: $Author$
 *	
 *	$Id$
 *	
 *	@file Description: Description
 *	
 ***************************************************************************/

#ifndef XPG_STATUS_H_
#define XPG_STATUS_H_

#include "prgmDef.h"

/// \defgroup getXpgStatCmnd Get xPG Status Command
/// \ingroup mics_protocol
/// Returns all parameters required for the PoP UI Display, and most of the 
/// parameters required the other external devices. 
///  
/// @{

/// BitMapped enumeration of general XPG status
/// - bit 0: stimulation state         1 = active,         0 = inactive
/// - bit 1: ramping status            1 = ramping,        0 = not ramping
/// - bit 2: increment lockout active  1 = lockout active, 0 = lockout not active
/// - bits 3 to 7: unused = 0

#define GS_STIM_ACTIVE  	0x01
#define GS_RAMPING      	0x02
#define GS_INCR_LOCKOUT 	0x04
#define GS_BOOTLOADER		0x80

#define STIM_ASIC_HV_SETTING_MASK   0x3F  //bits5-0 set high voltage value
#define STIM_ASIC_HV_ON_BIT         0x80  //bit 7: stim asic boost converter is on


/// This is the enumerated designation of xPG's present battery level.
/// The numbers assigned are important.
/// These are the battery-state values presented to the outside world.

typedef enum XPG_BATT_STATE {
	XPG_BATT_EMPTY = 0,		///<battery is in state where it can't stim
	XPG_BATT_LOW = 1,		///<battery level is warning (<25%)
	XPG_BATT_MEDIUM = 2,	///<battery level is between 25% & 50%
	XPG_BATT_HIGH = 3,		///<battery level is between 50% & 75%
	XPG_BATT_FULL = 4		///<battery level is greater than 75%
} XPG_BATT_STATE;



/// xPG Status Data
/// - Returned as Data for Get xPG Status Command
/// - NOTE: Group individual byte sized parameters in pairs.   
typedef struct GET_XPG_STATUS_RESPONSE_DATA
{
  /// indicates whether/which error preventing normal xpg operation is in effect.
  /// zero - no error in operation.
  /// non-zero - number corresponding to error.
  uint8_t activeError;
  
  /// XPG general bit-mapped status byte 
  /// - values: see #_XPG_GENERAL_STATUS
  uint8_t generalStatus;
   
  /// bitmap indicating if program P1 thru P10 is selectable.
  /// - values: see #_PROGRAM_BITMAP
  uint16_t programSelectMap;
  
  /// number of the currently selected program.
  /// - values: see #_PROGRAM_NUMBER
  uint8_t selectedProgram;
  
  /// bitmap indicating if pulse PA thru PD is defined for presently selected program. 
  /// - values: see #_PULSE_SLOT_BIT
  uint8_t pulseSelectMap;

  /// virtual amplitude step indexes of individual pulses in presently selected program. 
  /// - values: see #_PULSE_VIRTUAL_AMPL_STEP_LIMIT
  int8_t  pulseVirtualAmplStepIndex[NUM_PULSES];

  /// the xPGs present battery state.
  /// - values: see #_XPG_BATT_STATE  
  uint8_t xpgBatteryState;
  
  /// the latest charging error the xPG encountered
  /// zero - no error in operation.
  /// non-zero - number corresponding to error.
  uint8_t chargingError;
  
  /// the current stimulation state
  uint8_t stimState;

  /// unused. Should be transmitted as zero by the xPG and ignored by external devices.
  uint8_t reserved;
        
} GET_XPG_STATUS_RESPONSE_DATA;

/// @}

#endif // XPG_STATUS_H_
