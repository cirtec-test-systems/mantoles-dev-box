/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Set Program Definition structure definition.
 *	
 ***************************************************************************/

#ifndef CMNDSETPRGMDEF_H_
#define CMNDSETPRGMDEF_H_

#include "prgmDef.h"

/// \defgroup setPrgmDefCmnd Set Program Definition Command
/// \ingroup mics_protocol
/// Set Program Definition for specified Program Number.
///  
/// @{

/// Set Program Definition - Parameter Data
/// - NOTE: Group individual byte sized parameters in pairs.   
typedef struct SET_PRGM_DEF_PARAMS
{
	/// indicates the program number to set program definition for.
	/// - valid values: see #_PROGRAM_NUMBER
	uint8_t program;

	/// filler byte to keep program def on normal memory boundaries
	uint8_t fillerByte;

	/// program definition for selected program number
	/// - see #_PRGM_DEF
	PRGM_DEF programDef;

} SET_PRGM_DEF_PARAMS;

/// @}

#endif // CMNDSETPRGMDEF_H_
 
 
 
/**********************************************************************
 * Revision History
 *
 * $Log$
 *
 **********************************************************************/
