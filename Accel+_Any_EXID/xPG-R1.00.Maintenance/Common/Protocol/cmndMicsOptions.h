/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description:
 *	
 ***************************************************************************/



#ifndef CMNDMICSOPTIONS_H_
#define CMNDMICSOPTIONS_H_

#include <stdint.h>

#define MICS_LOW_LATENCY	0x01				///< MICS_OPTIONS_PARAMS: Flag to configure MICS for low latency but higher power.

typedef struct MICS_OPTIONS_PARAMS {
	uint8_t	flags;			///< Bitmask flags for the MICS options
} MICS_OPTIONS_PARAMS;

#endif /* CMNDMICSOPTIONS_H_ */
