/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Get Test Stimulation command
 *	
 ***************************************************************************/

#ifndef CMNDGETTESTSTIM_H_
#define CMNDGETTESTSTIM_H_

#include <stdint.h>
#include "testStim.h"

/// \defgroup getTestStim Get Test Stimulation command
/// \ingroup mics_protocol
/// Get Test Stimulation settings.
///  
/// @{

/// Get Test Stimulation - Response Data
/// - NOTE: Group individual byte sized parameters in pairs.   

typedef TEST_STIM GET_TEST_STIM_RESPONSE_DATA;

/// @}

#endif /*CMNDGETTESTSTIM_H_*/
