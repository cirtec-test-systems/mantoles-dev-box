/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: MICS protocol constants
 *	
 ***************************************************************************/

#ifndef PROTOCOL_H_
#define PROTOCOL_H_

#define MICS_BLOCK_SIZE 	15
#define MICS_MAX_BLOCKS		31
#define MICS_PACKET_SIZE	(MICS_BLOCK_SIZE * MICS_MAX_BLOCKS)

#endif                          /*PROTOCOL_H_ */
