/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.2                            */
/***************************************************************************/

/****************************************************************************
 *	QiG group Source File
 *	
 *	Copyright (c) 2010 QiG group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	QiG Part Number: Number
 *	Project: Project
 *	File Name: $RCSfile$
 *	@version: $Revision$
 *	@author: $Author$
 *	
 *	$Id$
 *	
 *	@file Description: Description
 *	
 ***************************************************************************/

#ifndef CMNDGETPPCCONST_H_
#define CMNDGETPPCCONST_H_

#include "cmndCommon.h"

/// \defgroup getPpcConstCmnd Get PPC Constants Command 
/// \ingroup mics_protocol
/// Get non-dynamic xPG operational parameter constants integral to PPC User Interface operation.
///  
/// @{

/// Get PPC Constants - Response Data
/// - NOTE: Group individual byte sized parameters in pairs.   

typedef struct GET_PPC_CONST_RESPONSE_DATA
{
  /// characters for ppc personalized text
  /// - text gets displayed on PPC UI
  char   ppcText[PPC_TEXT_CHARS];  

  /// pulse width step size
  /// - values: see _PULSE_WIDTH_STEP_LIMIT
  uint16_t pulseWidthStep;

  /// number of amplitude steps for inc/dec program amplitude
  /// - see amplStepIndex.h for details
  uint8_t  amplitudeSteps;   

  /// ppc behavior options
  /// - values / format: see _PPC_OPTIONS_BITMAP
  uint8_t  PPCOptions;

  /// return ramp time so PPC can manage after stim started
  uint16_t rampTime;  
  
  /// return increment lockout time so that PPC knows if it needs to delay after increment commands
  uint16_t incrementLockoutMsecs;  
  
  uint8_t implantDateTime[8];  ///< this is actually in a microsoft date/time format
  
  uint8_t replacementIntervalMonths; ///<number of months after implant date
  
  uint8_t fillerByte;  ///<added since block ends on odd number

} GET_PPC_CONST_RESPONSE_DATA;

/// @}

#endif // CMNDGETPPCCONST_H_
 
 
 
/**********************************************************************
 * Revision History
 *
 * $Log$
 *
 **********************************************************************/
