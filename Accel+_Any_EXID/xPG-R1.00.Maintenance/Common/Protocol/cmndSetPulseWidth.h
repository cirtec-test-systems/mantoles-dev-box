/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Define the set pulse width parameter structure
 *	
 ***************************************************************************/

#ifndef CMNDSETPULSEWIDTH_H_
#define CMNDSETPULSEWIDTH_H_

/// \defgroup setPulseWidthCmnd Set Pulse Width Command
/// \ingroup mics_protocol
/// Set the individual width value for the specified pulse in the presently selected program.
///  
/// @{

/// Set Pulse Width - Parameter Data
/// - NOTE: Group individual byte sized parameters in pairs.   
typedef struct SET_PULSE_WIDTH_PARAMS
{
	/// indicates which pulse in presently selected program to set width for.
	/// - valid values: see #_PULSE_INDEX
	uint8_t pulseIndex;

	/// pad byte for word alignment
	uint8_t reserved;

	/// width value for specified individual pulse
	///	- valid values: #_PULSE_WIDTH_LIMIT 
	uint16_t pulseWidth;

} SET_PULSE_WIDTH_PARAMS; 


/// @}

#endif // CMNDSETPULSEWIDTH_H_
 
 
 
/**********************************************************************
 * Revision History
 *
 * $Log$
 *
 **********************************************************************/
