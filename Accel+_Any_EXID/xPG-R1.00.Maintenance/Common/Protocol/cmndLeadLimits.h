/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Data structure definition for lead limits (current
 *                     density and charge density).  This is used by the
 *                     Get / Set Lead Limits commands.
 *	
 ***************************************************************************/
#ifndef CMNDLEADLIMITS_H_
#define CMNDLEADLIMITS_H_

/// \defgroup getSetLeadLimits Get/Set Lead Limits Commands
/// \ingroup mics_protocol
/// @{

#include <stdint.h>
#include "prgmDef.h"       //for number of electrode channels

/// Max current density limit, in uA
#define MAX_CURRENT_DENSITY_LIMIT 15000 //set to max for channel

/// Max charge density limit, in mA X usec
#define MAX_CHARGE_DENSITY_LIMIT 22500  //set to max pulse (15mA @ 1500 usec)

//Set / Get the lead limits. (Current density and charge density.)

///Lead Limits - parameter data structure
typedef struct LEAD_LIMITS_PARAMS
{
  ///current density limit for each electrode channel, in uA
  uint16_t currentLimit[NUM_CHANNELS];
  ///charge density limit for each electrode channel, in mA X usec
  uint16_t chargeLimit[NUM_CHANNELS];
} LEAD_LIMIT_PARAMS;

/// @}

#endif /*CMNDLEADLIMITS_H_*/
