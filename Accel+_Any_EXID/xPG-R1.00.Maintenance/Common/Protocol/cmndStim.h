/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Stim Command
 *	
 ***************************************************************************/

#ifndef CMNDSTIM_H_
#define CMNDSTIM_H_

/// \defgroup stimCmnd Stimulation Command
/// \ingroup mics_protocol
/// Specify whether xPG stimulation is active or inactive.
///  
/// @{

/// enumerated designation of stim mode
enum STIM_STATE
{
	STIM_INACTIVE = 0,
	STIM_ACTIVE = 1,
	STIM_TITRATION = 2,
	STIM_TEST = 3,
	STIM_IMPEDANCE = 4
};


/// Stimulation Command - Parameter Data
/// - NOTE: Group individual byte sized parameters in pairs.   
typedef struct STIM
{
	/// indicates whether stimulation should be active or not.
	/// - valid values: see enum STIM_STATE
	uint8_t stimState;
} STIM_PARAMS; 

/// @}

#endif // CMNDSTIM_H_
