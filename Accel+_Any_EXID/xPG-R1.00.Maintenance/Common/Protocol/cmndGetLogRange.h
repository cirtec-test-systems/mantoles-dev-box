/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2011 QIG Group
 *	1771 E. 30th St. Cleveland, OH 44114
 *
 *	Project: SCS System - IPG Firmware
 *
 *	@file Description: Data structure definition for Get Log Range Command.
 *
 ***************************************************************************/

#ifndef CMNDGETLOGRANGE_H_
#define CMNDGETLOGRANGE_H_

typedef struct GET_LOG_RANGE_PARAMS
{
	/// The identifying code for the counter to retrieve.
	uint32_t logNum;  		//Number of the Event Log.
							//0 = Major Event Log
							//1 = Normal Event Log
} GET_LOG_RANGE_PARAMS;

typedef struct GET_LOG_RANGE_RESPONSE_DATA
{
	uint32_t logNum;		//Number of the Event Log returned.
	uint32_t minSerialNum;	//Serial number of the first log entry
	uint32_t maxSerialNum;	//Serial number of the last log entry
} GET_LOG_RANGE_RESPONSE_DATA;

#endif /* CMNDGETLOGRANGE_H_ */
