/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Header / data format for get/set stim asic high
 *                     voltage calibrations
 *	
 ***************************************************************************/
 
#ifndef CMNDSTIMASICHVCALS_H_
#define CMNDSTIMASICHVCALS_H_

#include <stdint.h>

/// \defgroup getSetstimAsicHvCals Get/Set Stim ASIC HV Calibrations Commands
/// \ingroup mics_protocol
/// @{

/*
 * Low and high limits of 0V & 26V per Jeff W., 02/08/2011 (need next spin
 * of stim asic and then testing to determine full expected range of boost
 * converter.)
 */
 
/// minimum stim asic high voltage calibration
#define MIN_STIM_ASIC_HV_CAL     0  	//minimum boost converter output, in mV

/// maximum stim asic high voltage calibration
#define MAX_STIM_ASIC_HV_CAL    26000  //maximum boost converter output, in mV

/// number of stim asic high voltage control settings
#define NUM_STIM_ASIC_HV_SETTINGS  64  //ref stim asic control register BC_CTRL0
//Set / Get the Stim Asic HV calibrations

///Stim Asic HV calibrations - parameter data structure
typedef struct HV_CAL_PARAMS
{
  ///stim asic hv control register calibration
  uint16_t stimAsicHVmVOutput[NUM_STIM_ASIC_HV_SETTINGS];
} HV_CAL_PARAMS;

/// @}

#endif /*CMNDSTIMASICHVCALS_H_*/
