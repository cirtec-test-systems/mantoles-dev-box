/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Define the Get CP Data packet structure
 *	
 ***************************************************************************/
 
#ifndef CMNDGETCPDATA_H_
#define CMNDGETCPDATA_H_

#include <stdint.h>

#include "cp_data.h"

/// \defgroup getCPData Get CP Data Command
/// \ingroup mics_protocol
/// Get a CP data block.
///  
/// @{

/// Get CP Data - Parameter Data
typedef struct GET_CP_DATA
{
	/// The data block number requested
	/// Range from 0 to NUM_CP_DATA
	uint8_t block;
} GET_CP_DATA_PARAMS; 


/// Get CP Data - Response Data

typedef struct GET_CP_DATA_RESPONSE_DATA
{
	/// The block number of the data returned
	uint8_t block;

    /// padding byte for word-alignment
	uint8_t filler;

	/// block of opaque data stored on behalf of the CP
	uint8_t data[CP_DATA_LEN];
} GET_CP_DATA_RESPONSE_DATA;

  
/// @}

#endif // CMNDGETCPDATA_H_
