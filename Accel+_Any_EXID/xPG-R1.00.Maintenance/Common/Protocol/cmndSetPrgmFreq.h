/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Define the program frequency command structure.
 *	
 ***************************************************************************/

#ifndef CMNDSETPRGMFREQ_H_
#define CMNDSETPRGMFREQ_H_

/// \defgroup setPrgmFreqCmnd Set Program Frequency Command
/// \ingroup mics_protocol
/// Specify the frequency table index to establish the frequency at which the presently selected program should run.
///  
/// @{

/// Set Program Frequency Command - Parameter Data
/// - NOTE: Group individual byte sized parameters in pairs.   
typedef struct PRGM_FREQ_INDEX
{
	/// index in program frequency table at which presently selected program should run.
	/// - valid values: #_FREQUENCY_INDEX_LIMIT
	uint8_t programFrequencyIndex;

} SET_PRGM_FREQ_PARAMS; 

/// @}

#endif // CMNDSETPRGMFREQ_H_
 
 
 
/**********************************************************************
 * Revision History
 *
 * $Log$
 *
 **********************************************************************/
