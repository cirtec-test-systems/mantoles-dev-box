/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.2                            */
/***************************************************************************/

/****************************************************************************
 *	QiG group Source File
 *	
 *	Copyright (c) 2010 QiG group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	QiG Part Number: Number
 *	Project: Project
 *	File Name: $RCSfile$
 *	@version: $Revision$
 *	@author: $Author$
 *	
 *	$Id$
 *	
 *	@file Description: Description
 *	
 ***************************************************************************/

#ifndef CMNDGETPOPCONST_H_
#define CMNDGETPOPCONST_H_

#include <stdint.h>

/// \defgroup getPopConstCmnd Get PoP Constants Command 
/// \ingroup mics_protocol
/// Get non-dynamic xPG operational parameter constants integral to PoP User Interface operation.
///  
/// @{

// no parameter data req'd for command


/// Get PoP Constants - Response Data
/// - NOTE: Group individual byte sized parameters in pairs.   

typedef struct GET_POP_CONST_RESPONSE_DATA
{

	/// number of steps over which the (hardware) output amplitude can be adjusted. 
	/// - values: see #_PULSE_OUTPUT_AMPL_STEP_LIMIT
	uint8_t amplitudeSteps;

  //added ramp time and increment lockout msecs to PoP constants
  uint8_t reservedPoPConst;
  
  uint16_t rampTime;  // return ramp time so PoP can manage after stim started
  
  uint16_t incrementLockoutMsecs;  // return increment lockout time so that PoP
                                 // knows if it needs to delay after increment
                                 // commands

} GET_POP_CONST_RESPONSE_DATA;

/// @}

#endif // CMNDGETPOPCONST_H_
 
 
 
/**********************************************************************
 * Revision History
 *
 * $Log$
 *
 **********************************************************************/
