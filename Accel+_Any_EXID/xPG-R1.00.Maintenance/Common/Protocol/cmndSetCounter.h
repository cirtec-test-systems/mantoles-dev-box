/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Data format for Set Counter command
 *	
 ***************************************************************************/

#ifndef CMNDSETCOUNTER_H_
#define CMNDSETCOUNTER_H_

#include <stdint.h>
#include "counter_id.h"

typedef struct SET_COUNTER_PARAMS {
	/// The identifying code for the counter to set.
	uint8_t eventCounterID;
	
	/// Pad byte to preserve word-alignment (always 0)
	uint8_t reserved;
	
	/// New event count for the counter
	uint32_t eventCount;
} SET_COUNTER_PARAMS;

#endif /*CMNDSETCOUNTER_H_*/
