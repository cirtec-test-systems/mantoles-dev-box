/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.2                            */
/***************************************************************************/

/****************************************************************************
 *	QiG group Source File
 *	
 *	Copyright (c) 2010 QiG group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	QiG Part Number: Number
 *	Project: Project
 *	File Name: $RCSfile$
 *	@version: $Revision$
 *	@author: $Author$
 *	
 *	$Id$
 *	
 *	@file Description: Description
 *	
 ***************************************************************************/

#ifndef CMNDDECPULSEAMPL_H_
#define CMNDDECPULSEAMPL_H_

/// \defgroup decPulsAmplCmnd Decrement (Individual) Pulse Amplitude Command
/// \ingroup mics_protocol
/// Decrement the individual amplitude step index for the specified pulse of the presently selected program.
///  
/// @{

/// Decrement Pulse Amplitude - Parameter Data
/// - NOTE: Group individual byte sized parameters in pairs.   
typedef struct DEC_PULSE_AMPL_PARAMS
{
	/// indicates which pulse in presently selected program to decrement amplitude for.
	/// - value: see #_PULSE_INDEX
	uint8_t pulseIndex;

} DEC_PULSE_AMPL_PARAMS; 


/// Decrement Pulse Amplitude - Response Data
/// - NOTE: Group individual byte sized parameters in pairs.   

typedef struct DEC_PULSE_AMPL_RESPONSE_DATA
{
	/// virtual amplitude step index of individual pulse in presently selected program. 
	/// - values: see #_PULSE_VIRTUAL_AMPL_STEP_LIMIT
	int8_t  pulseVirtualStepIndex;

} DEC_PULSE_AMPL_RESPONSE_DATA;

/// @}

#endif // CMNDDECPULSEAMPL_H_
 
 
 
/**********************************************************************
 * Revision History
 *
 * $Log$
 *
 **********************************************************************/
