/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.2                            */
/***************************************************************************/

/****************************************************************************
 *	QiG group Source File
 *	
 *	Copyright (c) 2010 QiG group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	QiG Part Number: Number
 *	Project: Project
 *	File Name: $RCSfile$
 *	@version: $Revision$
 *	@author: $Author$
 *	
 *	$Id$
 *	
 *	@file Description: Description
 *	
 ***************************************************************************/

#ifndef CMNDGETXPGIDENT_H_
#define CMNDGETXPGIDENT_H_

/// \defgroup getXpgIdentCmnd Get xPG Identity Command 
/// \ingroup mics_protocol
/// Get information related to the type of xPG, and its unique identification and versioning.
///  
/// @{


/// Get xPG Identity - Response Data
/// - NOTE: Group individual byte sized parameters in pairs.   

typedef struct GET_XPG_IDENT_RESPONSE_DATA
{
	/// alphanumeric identification of the responding xPG device model number (i.e., device type).
	///	- valid values: 8 - bit ASCII characters
	uint8_t modelNumber[MODEL_NUMBER_CHARS];

	/// unique alphanumeric identification of the responding xPG device.
	///	- valid values: 8 - bit ASCII characters
	uint8_t serialNumber[SERIAL_NUMBER_CHARS];

	/// unique alphanumeric identification of the responding xPG device firmware revision.
	///	- valid values: 8 - bit ASCII characters
	uint8_t firmwareVersion[FIRMWARE_VERSION_CHARS];

} GET_XPG_IDENT_RESPONSE_DATA;

/// @}

#endif // CMNDGETXPGIDENT_H_
 
 
 
/**********************************************************************
 * Revision History
 *
 * $Log$
 *
 **********************************************************************/
