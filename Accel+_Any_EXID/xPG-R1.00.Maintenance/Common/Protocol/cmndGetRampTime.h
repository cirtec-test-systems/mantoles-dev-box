/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Define the Get Ramp Time packet structures
 *	
 ***************************************************************************/

#ifndef CMNDGETRAMPTIME_H_
#define CMNDGETRAMPTIME_H_

#include <stdint.h>

/// \defgroup getRampTime Get Ramp Time
/// \ingroup mics_protocol
/// Returns the ramp time in milliseconds.
///  
/// @{

typedef struct GET_RAMP_TIME_RESPONSE_DATA {
	uint16_t rampTime;
} GET_RAMP_TIME_RESPONSE_DATA;

/// @}

#endif /*CMNDGETRAMPTIME_H_*/
