/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.2                            */
/***************************************************************************/

/****************************************************************************
 *	QiG group Source File
 *	
 *	Copyright (c) 2010 QiG group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	QiG Part Number: Number
 *	Project: Project
 *	File Name: $RCSfile$
 *	@version: $Revision$
 *	@author: $Author$
 *	
 *	$Id$
 *	
 *	@file Description: Description
 *	
 ***************************************************************************/

#ifndef CMNDDECPULSEWIDTH_H_
#define CMNDDECPULSEWIDTH_H_

/// \defgroup decPulsWdthCmnd Decrement (Individual) Pulse Width Command
/// \ingroup mics_protocol
/// Decrement the width for the specified pulse of the presently selected program by the widthStep value of the specified pulse in the presently selected program's definition.
///  
/// @{

/// Decrement Pulse Width - Parameter Data
/// - NOTE: Group individual byte sized parameters in pairs.   
typedef struct DEC_PULSE_WIDTH_PARAMS
{
	/// indicates which pulse in presently selected program to decrement width for.
	/// - value: see #_PULSE_INDEX
	uint8_t pulseIndex;

} DEC_PULSE_WIDTH_PARAMS; 


/// Decrement Pulse Width - Response Data
/// - NOTE: Group individual byte sized parameters in pairs.   

typedef struct DEC_PULSE_WIDTH_RESPONSE_DATA
{
	/// width value for specified individual pulse in presently selected program.
	///	- valid values: #_PULSE_WIDTH_LIMIT 
	uint16_t pulseWidth;

} DEC_PULSE_WIDTH_RESPONSE_DATA;

/// @}

#endif // CMNDDECPULSEWIDTH_H_
 
 
 
/**********************************************************************
 * Revision History
 *
 * $Log$
 *
 **********************************************************************/
