/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.2                            */
/***************************************************************************/

/****************************************************************************
 *	QiG group Source File
 *	
 *	Copyright (c) 2010 QiG group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	QiG Part Number: Number
 *	Project: Project
 *	File Name: $RCSfile$
 *	@version: $Revision$
 *	@author: $Author$
 *	
 *	$Id$
 *	
 *	@file Description: Description
 *	
 ***************************************************************************/

#ifndef CMNDCHRGCNTRL_H_
#define CMNDCHRGCNTRL_H_

#include <stdint.h>

/// \defgroup chrgCntrlCmnd Charging Control Command Command
/// \ingroup mics_protocol
/// Send the present charger output level and get back received level and other IPG operational parameters affecting IPG Battery charging control loop.
///  
/// @{

// 06/04/2010 new command format - no send parameters

/// Charging Control - Response Data
/// - NOTE: Group individual byte sized parameters in pairs.   

typedef struct CHARGING_CONTROL_RESPONSE_DATA
{
	/// warning bit, charge state, temperature state
	/// - value: see WARNING_CHARGE_STATE_TEMP_STATE_BITMAP
	uint8_t ChargeStateTempStateBitMap;

	/// IPG received level
	/// rectified voltage level = value * 50mV
	uint8_t voltageLevel;
} CHARGING_CONTROL_RESPONSE_DATA;

/// @}

#endif // CMNDCHRGCNTRL_H_
 
 
 
/**********************************************************************
 * Revision History
 *
 * $Log$
 *
 **********************************************************************/
