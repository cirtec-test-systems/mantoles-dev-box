/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.2                            */
/***************************************************************************/

/****************************************************************************
 *	QiG group Source File
 *	
 *	Copyright (c) 2010 QiG group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	QiG Part Number: Number
 *	Project: Project
 *	File Name: $RCSfile$
 *	@version: $Revision$
 *	@author: $Author$
 *	
 *	$Id$
 *	
 *	@file Description: Description
 *	
 ***************************************************************************/

#ifndef CMNDGETPULSEWIDTHS_H_
#define CMNDGETPULSEWIDTHS_H_

#include "prgmDef.h"

/// \defgroup getPulsWdthCmnd Get Pulse Widths Command
/// \ingroup mics_protocol
/// Get the pulse widths for the presently selected program.
///  
/// @{



/// Get Pulse Width - Response Data
/// - NOTE: Group individual byte sized parameters in pairs.   

typedef struct GET_PULSE_WIDTHS_RESPONSE_DATA
{
    /// bit map indicating if pulse PA thru PD is defined for the presently selected program.
    /// - values: see #_PULSE_SLOT_BIT
    uint8_t definedPulseMap;

    /// pad byte for word alignment
    uint8_t reserved;

	/// width values for each individual pulse in presently selected program.
	///	- valid values: #_PULSE_WIDTH_LIMIT 
	uint16_t pulseWidth[NUM_PULSES];
} GET_PULSE_WIDTHS_RESPONSE_DATA;

/// @}

#endif // CMNDGETPULSEWIDTHS_H_
 
 
 
/**********************************************************************
 * Revision History
 *
 * $Log$
 *
 **********************************************************************/
