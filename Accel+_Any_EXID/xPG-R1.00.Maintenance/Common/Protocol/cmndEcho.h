/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Define the Short and Long Echo packet structure
 *	
 ***************************************************************************/
 

#ifndef CMNDECHO_H_
#define CMNDECHO_H_

#include <stdint.h>

/**
 * The Echo Long packet is long, to require the use of multiple data blocks.
 * 
 * One could argue in favor of making it long enough to fill the maximum packet
 * length, but since no packets in the protocol need to be maximum-length, I did
 * not want echo to be the only reason why the packet buffers could not be
 * reduced to release RAM. 
 */
 
typedef uint8_t ECHO_LONG_PARAMS[256];


/**
 * The Echo Short packet is designed to fill a single data block.
 */
 
typedef uint8_t ECHO_SHORT_PARAMS[8];

#endif /*CMNDECHO_H_*/
