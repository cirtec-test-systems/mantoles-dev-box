/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.2                            */
/***************************************************************************/

/****************************************************************************
 *	QiG group Source File
 *	
 *	Copyright (c) 2010 QiG group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	QiG Part Number: Number
 *	Project: Project
 *	File Name: $RCSfile$
 *	@version: $Revision$
 *	@author: $Author$
 *	
 *	$Id$
 *	
 *	@file Description: Description
 *	
 ***************************************************************************/

#ifndef CMNDCHRG_H_
#define CMNDCHRG_H_

/// \defgroup chrgCmnd Charging Command
/// \ingroup mics_protocol
/// Specify whether IPG charging is active or inactive.
///  
/// @{

/// enumerated designation of charging mode
typedef enum CHARGING_MODE
{
	CHARGING_INACTIVE,
	CHARGING_ACTIVE
} CHARGING_MODE;

/// Charging Command - Parameter Data
/// - NOTE: Group individual byte sized parameters in pairs.   
typedef struct CHARGING_PARAMS
{
	/// indicates whether charging should be active or not.
	/// - valid values: see #_CHARGING_MODE
	uint8_t chargingActive;

} CMND_CHARGING_PARAMS; 


/// @}

#endif // CMNDCHARGING_H_
 
 
 
/**********************************************************************
 * Revision History
 *
 * $Log$
 *
 **********************************************************************/
