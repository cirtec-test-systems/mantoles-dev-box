/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	Project: SCS System - IPG Firmware
 *	
 *	@file Description: Data format for Set Counter command
 *	
 ***************************************************************************/

#ifndef CMNDGETCOUNTER_H_
#define CMNDGETCOUNTER_H_

#include <stdint.h>
#include "counter_id.h"

typedef struct GET_COUNTER_PARAMS {
	/// The identifying code for the counter to retrieve.
	uint8_t eventCounterID;
} GET_COUNTER_PARAMS;

typedef struct GET_COUNTER_RESPONSE_DATA {
	/// The identifying code for the counter to retrieve.
	uint8_t eventCounterID;
	
	/// Pad byte to preserve word-alignment (always 0)
	uint8_t reserved;
	
	/// New event count for the counter
	uint32_t eventCount;
} GET_COUNTER_RESPONSE_DATA;

#endif /*CMNDGETCOUNTER_H_*/
