/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Define the Get VBAT packet structures
 *	
 ***************************************************************************/

#ifndef CMNDGETVBAT_H_
#define CMNDGETVBAT_H_

#include <stdint.h>

/// \defgroup getVBAT Get VBAT extended command
/// \ingroup mics_protocol
/// Returns the VBAT value in internal units.  Used for calibrations.
///  
/// @{

typedef struct GET_VBAT_RESPONSE_DATA {
	uint16_t vbat;
} GET_VBAT_RESPONSE_DATA;

/// @}

#endif /*CMNDGETVBAT_H_*/
