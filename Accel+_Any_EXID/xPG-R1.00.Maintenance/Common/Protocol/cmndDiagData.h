/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2011 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Define the Diagnostic Data packet structures
 *
 ***************************************************************************/
#ifndef CMNDDIAGDATA_H_
#define CMNDDIAGDATA_H_

/*
 * Retrieve a diagnostic data block from the xPG. These data blocks contain
 * outputs from various automatic diagnostics performed by the xPG. Each
 * block holds the data from the most recent run of the diagnostic.
 */

//Types of diagnostic data commands
typedef enum DIAG_DATA_COMMAND
{
	DIAG_READ = 0,		//The data returned is that from the most recent automatic run, if any.
	DIAG_READ_CLEAR = 1	//Diagnostic data is cleared to all 0�s.
} DIAG_DATA_COMMAND;

//Types of data selection for diagnostic data command
typedef enum DIAG_DATA_SELECT
{
	DIAG_CAPACITOR = 0,			//Output capacitor check data.
	DIAG_BG_IMPEDANCE = 1,		//Background impedance check data.
	DIAG_VBAT = 2,				//Battery monitor voltage data.
	DIAG_THERMISTOR = 3			//Charging temperature data.
} DIAG_DATA_SELECT;

//Parameters for TKN_DIAG_DATA command token
typedef struct DIAG_DATA_PARAMS
{
	uint8_t command;	//DIAG_DATA_COMMMAND - A code for the operation to be performed
	uint8_t select;		//DIAG_DATA_SELECT - Selects the data to be read
} DIAG_DATA_PARAMS;

//Response for TKN_DIAG_DATA command token
typedef struct DIAG_DATA_RESPONSE_DATA
{
	uint8_t command;	//DIAG_DATA_COMMAND - The command code for the operation performed
	uint8_t select;		//DIAG_DATA_SELECT - The selection of the data read, as described above.
	uint8_t data[250];	//The diagnostic data. The actual data will be padded to fill this fixed-length field.
} DIAG_DATA_RESPONSE_DATA;

//Output Capacitor Check Data - Diagnostic data from an Output Capacitor Check.
typedef struct OUTPUT_CAPACITOR_CHECK_DATA
{
	uint16_t voltage[26];	//The voltage read on each channel during the Output Capacitor Check, in ADC units.
	uint8_t dataValid;		//If the Output Capacitor Check has been run since the last clear operation, dataValid = 1.
							//Otherwise, dataValid = 0.
} OUTPUT_CAPACITOR_CHECK_DATA;

//Background Impedance Check Data - Diagnostic data from a Background Impedance Check.
//Note: The xPG also supports an Impedance On Demand command. That data is returned in the response to that command
//      and does not affect the data here.
typedef struct BACKGROUND_IMPEDANCE_CHECK_DATA
{
	uint16_t impedance[26];	//The impedance read on each channel during the Background Impedance Check, in Ohms.
	uint8_t dataValid;		//If the Background Impedance Check has been run since the last clear operation, dataValid = 1.
							//Otherwise, dataValid = 0.
} BACKGROUND_IMPEDANCE_CHECK_DATA;

//Battery Monitor Voltage Data - Diagnostic data from the Battery Monitor�s last measurement of battery voltage.
//Note: The xPG also supports a Get VBAT command that returns the battery voltage on demand. The Get VBAT command
//      does not affect the value returned here.
typedef struct BATT_MON_VOLTAGE_DATA
{
	uint16_t voltage;		//The voltage read from the battery during the Battery Monitor�s last measurement, in ADC units.
							//If the voltage has not been read since the last clear operation, voltage = 0.
} BATT_MON_VOLTAGE_DATA;

//Temperature Data - Diagnostic data from the Charge Manager�s last measurement of temperature.
typedef struct TEMPERATURE_DATA
{
	uint16_t thermBias;		//The voltage read at the THERM_BIAS input, in ADC units.
	uint16_t thermInput;	//The voltage read at the THERM_INPUT input, in ADC units.
	uint16_t thermOffset;	//The voltage read at the THERM_OFFSET input, in ADC units.
} TEMPERATURE_DATA;

#endif /* CMNDDIAGDATA_H_ */
