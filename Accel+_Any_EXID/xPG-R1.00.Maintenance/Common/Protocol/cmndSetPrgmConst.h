/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Data structure for the Set Program Constants command.
 * 
 ***************************************************************************/

#ifndef CMNDSETPRGMCONST_H_
#define CMNDSETPRGMCONST_H_

#include "prgmConst.h"

/// \defgroup prgmConstCmnd Get Program Constants Command
/// \ingroup mics_protocol
/// Set / get the program constant parameters for the xPG.
///  
/// @{

/// The parameters for a Set Program Constants command are simply a PRGM_CONST
/// structure.

typedef struct SET_PRGM_CONST_PARAMS {
	PRGM_CONST prgmConst;
} SET_PRGM_CONST_PARAMS;

/// @}


#endif /*CMNDSETPRGMCONST_H_*/
