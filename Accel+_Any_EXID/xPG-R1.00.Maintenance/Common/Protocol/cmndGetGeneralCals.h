/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	Project: SCS System - IPG Firmware
 *	
 *	@file Description: Data format for Get General Calibrations command
 *	
 ***************************************************************************/

#ifndef CMNDGETGENERALCALS_H_
#define CMNDGETGENERALCALS_H_

#include "gen_cals.h"

typedef GEN_CALS GET_GENERAL_CALS_RESPONSE_DATA;

#endif /*CMNDGETGENERALCALS_H_*/
