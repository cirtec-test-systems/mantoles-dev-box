/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Set Titration Stimulation command
 *	
 ***************************************************************************/

#ifndef CMNDSETTITRSTIM_H_
#define CMNDSETTITRSTIM_H_

#include <stdint.h>
#include "titrStim.h"

/// \defgroup setTitrStim Set Titration Stimulation command
/// \ingroup mics_protocol
/// Set Titration Stimulation settings.
///  
/// @{

/// Set Titration Stimulation - Command Parameters
/// - NOTE: Group individual byte sized parameters in pairs.   

typedef TITR_STIM SET_TITR_STIM_PARAMS;

/// @}

#endif /*CMNDSETTITRSTIM_H_*/
