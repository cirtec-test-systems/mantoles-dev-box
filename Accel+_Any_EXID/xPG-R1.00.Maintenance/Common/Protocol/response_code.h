/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Header file listing the response codes to various
 *                     commands.  Many times the response codes are used as
 *                     return codes from functions to indicates that some
 *                     error has taken place.
 * 
 *                     The response codes are listed and detailed in the
 *                     MICS command list document, SWEX 0085.
 *	
 ***************************************************************************/
#ifndef RESPONSE_CODE_H_
#define RESPONSE_CODE_H_

typedef enum RESPONSE {
	/// Response codes common to all commands
	GEN_SUCCESS  						= 0xFF,  ///< Response for general successful completion of a command. Also known as "COMMAND ACCEPTED".

	CMND_RESP_INVALID_EXID				= 0x00,  ///< Received EXternal ID of device sending command is invalid.
	CMND_RESP_INVALID_CMND				= 0x01,  ///< Received Command Token in sent command is invalid/unknown.
	CMND_RESP_INVALID_PARAM				= 0x02,  ///< One or More Parameters in sent command is invalid/out of bounds.
	CMND_RESP_BUSY 						= 0x03,  ///< Command cannot be processed in current xPG state (such as stimulation on)

	//GENERAL REASONS COMMAND CANNOT RUN
 	RESP_CMND_NOT_VALID_WHILE_IDLE    	= 0x10,	 ///< Command cannot be processed while not stimulating
 	RESP_CMND_NOT_VALID_WHILE_RAMPING 	= 0x11,	 ///< Command cannot be processed while ramping
	RESP_CMND_LOCKED_OUT              	= 0x12,	 ///< Command is currently locked out, must wait until lockout time has elapsed
	
	//REASONS A PROGRAM CAN'T BE SELECTED OR STARTED
	RESP_PROGRAM_NOT_VALID            	= 0x20,	 ///< The command cannot be executed because the program is not valid
	RESP_PROGRAM_DISABLED             	= 0x21,  ///< The command cannot be executed because the program has been disabled
	RESP_BATTERY_TOO_LOW              	= 0x22,  ///< Stimulation cannot be started because the battery level is too low
	RESP_NO_PROGRAM_SELECTED          	= 0x24,	 ///< Stimulation cannot be started because no program is selected
	RESP_BOOST_CONVERTER_STARTUP_FAILED	= 0x25,	 ///< Stimulation cannot be started because the HV boost converter did not initialize
	RESP_PULSE_GUARD_CHECK_FAILED		= 0x26,  ///< Stimulation cannot be started because the pulse guard circuit check failed
	RESP_STIM_POWER_ON_FAILED			= 0x27,  ///< Stimulation cannot be started because the chip failed to power up correctly
	RESP_ACTIVE_RECOVERY_DISABLED		= 0x28,	 ///< Stimulation cannot be started because automatic waveform adjustment failed
	
	RESP_STIM_SETUP_ERROR_BAD_STIM_ASIC_READBACK   = 0x2E, ///< While attempting to set up stim ASIC, data read-back failed
	RESP_POWER_SETUP_ERROR_BAD_POWER_ASIC_READBACK = 0x2F, ///< While attempting to set up power ASIC, data read-back failed
	
	//REASONS STIM MODS CAN'T BE DONE
	RESP_ALL_AMPS_AT_MIN              	= 0x31,	///< Cannot decrement amplitude, all amplitudes already at minimum
	RESP_ALL_AMPS_AT_MAX              	= 0x32,	///< Cannot increment amplitude, all amplitudes already at maximum
	RESP_PULSE_NOT_VALID              	= 0x33,	///< Cannot modify pulse, because pulse is not defined in program
	RESP_PW_EXCEEDS_LIMITS            	= 0x34,	///< Cannot increment pulse width because increase would exceed pulse width limits
	RESP_MOD_VIOLATES_CHRG_DENSITY_LIMIT= 0x36,	///< Cannot modify pulse output because charge density limit would exceeded
	RESP_MOD_VIOLATES_CURR_DENSITY_LIMIT= 0x37, ///< Cannot modify pulse output because current density limit would be exceeded
	RESP_PULSE_WIDTH_FREQ_CONFLICT      = 0x38, ///< The requested modification will cause a pulse width frequency conflict
	RESP_FREQ_CHANGE_NOT_ALLOWED      	= 0x3A, ///< The requested frequency changes is not allowed per the program definition
	RESP_PULSE_AMP_EXCEEDS_LIMITS     	= 0x3B, ///< The requested pulse amplitude change would exceed programmed limits
	
	//Misc failures
	RESP_TIMED_OUT						= 0x40,	///< The operation timed out waiting for the hardware to return a response
	
	//Charging Control Error
	RESP_CHARGING_CONTROL_ERROR			= 0x55,	///< An error occurred while trying to read temperature or battery data

	//DIFFERENT THINGS CORRUPTED
	RESP_DATA_CORRUPTED               	= 0x60, ///< The requested data was detected as corrupt
	RESP_PROGRAM_DEF_CORRUPT          	= 0x61, ///< The requested program definition is corrupt
	RESP_PROGRAM_SETTINGS_CORRUPT     	= 0x62, ///< The requested program's active settings are corrupt
	RESP_PROGRAM_CONSTANTS_CORRUPT    	= 0x63, ///< The requested program constants are corrupt
	RESP_PULSE_CONSTANTS_CORRUPT      	= 0x64, ///< The requested pulse constants are corrupt
	RESP_CHANNEL_CALS_CORRUPT         	= 0x65, ///< The requested channel calibration is corrupt
	RESP_LEAD_LIMITS_CORRUPT          	= 0x66, ///< The requested lead limits are corrupt
	RESP_STIM_ASIC_HV_CALS_CORRUPT    	= 0x67, ///< The requested Stim ASIC HV calibrations are corrupt
	RESP_RAMP_TIME_CORRUPT            	= 0x68, ///< The requested ramp time is corrupt
	
	RESP_FEATURE_NOT_ENABLED          	= 0x70,	///< The requested feature is not presently enabled

	RESP_WRITE_FAILED					= 0x77,	///< A memory write (other than flash) failed.
	RESP_READ_FAILED					= 0x78,	///< A memory read failed.
	
	RESP_BGND_IMPEDANCE_CHECK_FAILED	= 0x81,	///< Pre-stimulation background impedance check failed
	RESP_BASIC_OUTPUT_CHECK_FAILED		= 0x82,	///< Pre-stimulation basic output check failed
	RESP_STIM_FAILED_UKNKNOWN_SW_ERROR  = 0x83, ///< Stimulation failed due to an unknown software error
	RESP_STIM_FAILED_SEVERE_ERROR		= 0x84,	///< Stimulation failed because a severe error has not been cleared
	
	//THERE ARE A NUMBER OF WAYS THAT THE FLASH ERASE AND WRITE PROCESS CAN
	// FAIL.  INCLUDED DIFFERENT ERROR CODES FOR THE VARIOUS ERROR SOURCES
	// SO THAT WE CAN IDENTIFY THE ROOT CAUSE DURING DEVELOPMENT.  FLASH
	// ERRORS WILL GENERALLY OCCUR DURING PROGRAMMING OPERATIONS FROM THE CP
	FLASH_ERROR_INVALID_ADDRESS         = 0xB0,  ///< illegal flash address for segment write
	FLASH_ERROR_SEGMENT_BOUNDARY        = 0xB1,  ///< requested flash write overlaps segment boundary
	
	FLASH_ERASE_ERROR_FC_BUSY           = 0xB3,  ///< flash controller unexpectedly busy, prep for segment erase
	FLASH_ERASE_ERROR_FC_SETUP          = 0xB4,  ///< firmware set-up error prep for segment erase
	FLASH_ERASE_ERROR_TIMEOUT           = 0xB5,  ///< busy bit timeout waiting for segment erase
	FLASH_ERASE_ERROR_FAIL_FLAG         = 0xB6,  ///< flash fail flag during segment erase
	FLASH_ERASE_ERROR_ACCVIFG_FLAG      = 0xB7,  ///< access violation during segment erase
	FLASH_ERASE_ERROR_BAD_ERASE         = 0xB8,  ///< after the segment erase, segment not erased
	
	FLASH_WRITE_ERROR_FC_BUSY           = 0xC1,  ///< flash controller unexpectedly busy, prep for block write
	FLASH_WRITE_ERROR_FC_SETUP       	= 0xC2,  ///< firmware set-up error prep for block write
	
	FLASH_WORD_WRITE_ERROR_TIMEOUT    	= 0xC3,  ///< wait bit timeout while writing flash word
	FLASH_WORD_WRITE_ERROR_FAIL_FLAG  	= 0xC4,  ///< flash fail flag while writing flash word
	FLASH_WORD_WRITE_ERROR_ACCVIFG_FLAG	= 0xC5,  ///< access violation while writing flash word
	
	FLASH_BLOCK_WRITE_ERROR_TIMEOUT		= 0xC6,  ///< busy bit timeout while ending block write
	FLASH_BLOCK_WRITE_ERROR_FAIL_FLAG 	= 0xC7,  ///< flash fail flag while ending block write
	FLASH_BLOCK_WRITE_ERROR_ACCVIFG_FLAG= 0xC8,  ///< access violation while ending block write
	
	FLASH_WRITE_ERROR_BAD_WRITE			= 0xC9   ///< after the segment write, dest did not match source
} RESPONSE;


// if there was a failure return status and the caller needs to make sure the error response has been set, then the caller can use this macro.
// this macro guarantees that a valid error code is set
#define RESPONSE_ERROR	(getLastResponseCode() == GEN_SUCCESS ? RESP_STIM_FAILED_UKNKNOWN_SW_ERROR : getLastResponseCode());

#endif /*RESPONSE_CODE_H_*/
