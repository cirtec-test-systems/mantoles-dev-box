/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.2                            */
/***************************************************************************/

/****************************************************************************
 *	QiG group Source File
 *	
 *	Copyright (c) 2010 QiG group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	QiG Part Number: Number
 *	Project: Project
 *	File Name: $RCSfile$
 *	@version: $Revision$
 *	@author: $Author$
 *	
 *	$Id$
 *	
 *	@file Description: Description
 *	
 ***************************************************************************/

#ifndef CMNDINCPRGMFREQ_H_
#define CMNDINCPRGMFREQ_H_


/// \defgroup incPrgmFreqCmnd Increment Program Frequency Command
/// \ingroup mics_protocol
/// Increment to the next allowed frequency (table index) for the presently selected program. Skip over any frequency table entries not allowed for that program as specified in its program definition.
///  
/// @{


/// Increment Program Frequency - Response Data
/// - NOTE: Group individual byte sized parameters in pairs.   

typedef struct INC_PRGM_FREQ_RESPONSE_DATA
{
	/// index in program frequency table at which presently selected program should run.
	/// - valid values: #_FREQUENCY_INDEX_LIMIT
	uint8_t programFrequencyIndex;

} INC_PRGM_FREQ_RESPONSE_DATA;

/// @}

#endif // CMNDINCPRGMFREQ_H_
 
 
 
/**********************************************************************
 * Revision History
 *
 * $Log$
 *
 **********************************************************************/
