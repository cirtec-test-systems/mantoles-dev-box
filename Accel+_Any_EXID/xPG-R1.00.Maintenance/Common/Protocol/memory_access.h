/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Common definitions for Read Memory and Write Memory commands.
 *	
 ***************************************************************************/


#ifndef MEMORY_ACCESS_H_
#define MEMORY_ACCESS_H_

#define MAX_MEMORY_WRITE_SIZE 64
#define MAX_MEMORY_READ_SIZE  64

#define ADDRESS_SPACE(A)	(((A) >> 24) & 0xFF)
#define ADDRESS_OFFSET(A)	((A) & 0x00FFFFFFuL)

#define ADDRESS_MSP430_FLASH	0x00000000uL
#define ADDRESS_MSP430_BYTE		0x01000000uL
#define ADDRESS_MSP430_WORD		0x02000000uL
#define ADDRESS_NVRAM			0x10000000uL
#define ADDRESS_POWER			0x20000000uL
#define ADDRESS_STIM			0x30000000uL
#define ADDRESS_MICS			0x40000000uL

#define ADDRESS_IS_FLASH(A) ((A <= 0x1FFFF && A >= 0x03100) || (A <= 0x010FF && A >= 0x01000))

#endif /* MEMORY_ACCESS_H_ */
