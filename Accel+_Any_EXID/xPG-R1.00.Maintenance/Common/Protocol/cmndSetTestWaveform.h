/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Set Test Waveform command.
 *
 *		Used to load the Stim ASIC waveform memory during test stimulation.
 *	
 ***************************************************************************/

#ifndef CMNDSETTESTWAVEFORM_H_
#define CMNDSETTESTWAVEFORM_H_

typedef struct SET_TEST_WAVEFORM_PARAMS {
	uint8_t ram;			///< Which waveform memory to write (0 or 1)
	uint8_t reserved;		///< Reserved for word alignment. Set to 0 on transmit.
	uint8_t waveform[256];	///< 256 bytes of waveform data
} SET_TEST_WAVEFORM_PARAMS;

#endif /* CMNDSETTESTWAVEFORM_H_ */
