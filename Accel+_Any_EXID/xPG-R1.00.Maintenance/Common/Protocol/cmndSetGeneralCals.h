/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Data format for Set General Calibration command
 *	
 ***************************************************************************/

#ifndef CMNDSETGENERALCALS_H_
#define CMNDSETGENERALCALS_H_

#include "gen_cals.h"

typedef GEN_CALS SET_GENERAL_CALS_PARAMS;

#endif /*CMNDSETGENERALCALS_H_*/
