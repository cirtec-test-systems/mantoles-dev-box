/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	Project: SCS System - IPG Firmware
 *	
 *	@file Description: Data structure for the Get Pulse Constants command.
 * 
 ***************************************************************************/

#ifndef CMNDGETPULSECONST_H_
#define CMNDGETPULSECONST_H_

#include "pulseConst.h"

/// \defgroup getPulseConstCmnd Get Pulse Constants Command
/// \ingroup mics_protocol
/// Set / get the pulse constant parameters for the xPG.
///  
/// @{

/// The response for a Get Pulse Constants command is simply a PULSE_CONST
/// structure.

typedef struct GET_PULSE_CONST_RESPONSE_DATA {
	PULSE_CONST pulseConst;
} GET_PULSE_CONST_RESPONSE_DATA;

/// @}


#endif /*CMNDGETPULSECONST_H_*/
