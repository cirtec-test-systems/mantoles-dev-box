/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2011 QIG Group
 *	1771 E. 30th St. Cleveland, OH 44114
 *
 *	Project: SCS System - IPG Firmware
 *
 *	@file Description: Data structure definition for Append Log Command.
 *
 ***************************************************************************/

#ifndef CMNDAPPENDLOG_H_
#define CMNDAPPENDLOG_H_


/*
* The append command allows the CP to add an entry to the log. This can be
* used, for example, to simulate log clearing by marking the point up to
* which all log entries have been reviewed. It may also be useful in xPG
* firmware development.
*/

typedef struct APPEND_LOG
{
	/// The identifying code for the counter to retrieve.
	uint8_t logNum;  		//Number of the Event Log.
							//0 = Major Event Log
							//1 = Normal Event Log
							//2 = Routine Event Log
	uint8_t reserved;		//Always zero
	uint16_t event;			//Log event identifier
	uint8_t eventData[4];	//Event data

} APPEND_LOG;

typedef struct APPEND_LOG_RESPONSE_DATA
{
	uint32_t firstSerial;	//Serial number of the new log entry

} APPEND_LOG_RESPONSE_DATA;

#endif /* CMNDAPPENDLOG_H_ */
