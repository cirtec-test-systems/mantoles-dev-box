/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Get Titration Stimulation command
 *	
 ***************************************************************************/

#ifndef CMNDGETTITRSTIM_H_
#define CMNDGETTITRSTIM_H_

#include <stdint.h>
#include "titrStim.h"

/// \defgroup getTitrStim Get Titration Stimulation command
/// \ingroup mics_protocol
/// Get Titration Stimulation settings.
///  
/// @{

/// Get Titration Stimulation - Response Data
/// - NOTE: Group individual byte sized parameters in pairs.   

typedef TITR_STIM GET_TITR_STIM_RESPONSE_DATA;

/// @}

#endif /*CMNDGETTITRSTIM_H_*/
