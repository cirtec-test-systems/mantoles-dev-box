/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Data structure definition for Read Log Command.
 *
 ***************************************************************************/

#ifndef CMNDREADLOG_H_
#define CMNDREADLOG_H_

#include "DataStore/Include/log.h"

/* Read or add to the xPG event logs. The xPG maintains three event logs,
 * representing three levels of expected frequency of events.  Each log
 * assigns each of its events a serial number. The serial number for an event
 * remains unchanged for as long the event remains in the log. Serial numbers
 * are 32 bits long. All of the events in the log at a given time have
 * sequential serial numbers. Serial numbers are unique within a given log,
 * but may be duplicated in different logs.  The open and read commands are
 * idempotent.  The xPG may respond to a read command with any number of
 * entries from 1 to the maximum. If the read command requests a serial
 * number that is not in the log, the xPG shall respond with an empty list
 * (Count = 0).
*/

#define MAX_LOG_ENTRIES_RESPONSE 42

typedef struct READ_LOG
{
	/// The identifying code for the counter to retrieve.
	uint8_t logNum;  		//Number of the Event Log.
							//0 = Major Event Log
							//1 = Normal Event Log
							//2 = Routine Event Log
	uint8_t reserved;		//Always zero.
	uint32_t serial;		//Serial number of the first event to return

} READ_LOG;

typedef struct READ_LOG_RESPONSE_DATA
{
	uint8_t logNum;			//Number of the Event Log returned.
	uint8_t count;			//Number of log entries returned (0-42)
	uint16_t reserved;		//For alignment, always zero
	uint32_t serial;		//Serial number of the first log entry
	LOG_EVENT_RECORD log[MAX_LOG_ENTRIES_RESPONSE];//See 9.32.4  Log Entry Structure:

} READ_LOG_RESPONSE_DATA;

#endif /* CMNDREADLOG_H_ */
