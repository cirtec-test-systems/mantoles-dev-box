/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.2                            */
/***************************************************************************/

/****************************************************************************
 *	QiG group Source File
 *
 *	Copyright (c) 2010 QiG group
 *	1771 E. 30th St. Cleveland, OH 44114
 *
 *	QiG Part Number: Number
 *	Project: Project
 *	File Name: $RCSfile$
 *	@version: $Revision$
 *	@author: $Author$
 *
 *	$Id$
 *
 *	@file Description: Description
 *
 ***************************************************************************/

/// \defgroup injectEvent Inject an event into the system for testing purposes
/// \ingroup mics_protocol
/// Inject an event or error event into the system to force error conditions for
/// testing purposes.
///
/// @{

/// Inject Event - Parameter Data
/// - NOTE: Group individual byte sized parameters in pairs.
typedef struct INJECT_EVENT_PARAMS
{
	/// specifies the event id and one data byte of the event intended to be published into the system
	uint16_t eventId;
	uint16_t eventData;
} INJECT_EVENT_PARAMS;


