/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Define the pulse amplitude structures.
 *	
 ***************************************************************************/

#ifndef CMNDSETPULSEAMPL_H_
#define CMNDSETPULSEAMPL_H_

/// \defgroup setPulsAmplCmnd Set (Individual) Pulse Amplitude Command
/// \ingroup mics_protocol
/// Set the individual amplitude step index for the specified pulse of the presently selected program.
///  
/// @{

/// Set Pulse Amplitude - Parameter Data
/// - NOTE: Group individual byte sized parameters in pairs.   
typedef struct SET_PULSE_AMPL_PARAMS
{
	/// indicates which pulse in presently selected program to set amplitude for.
	/// - value: see #_PULSE_INDEX
	uint8_t pulseIndex;

	/// displayed amplitude step index for specified individual pulse. 
	/// - value: see #_PULSE_DISPLAY_AMPL_STEP_LIMIT
	uint8_t displayAmplitudeStepIndex;

} SET_PULSE_AMPL_PARAMS; 


/// Set Pulse Amplitude - Response Data
/// - NOTE: Group individual byte sized parameters in pairs.   

typedef struct SET_PULSE_AMPL_RESPONSE_DATA
{
	/// virtual amplitude step index of individual pulse in presently selected program. 
	/// - values: see #_PULSE_VIRTUAL_AMPL_STEP_LIMIT
	uint8_t pulseAmplitudeStepIndex;

} SET_PULSE_AMPL_RESPONSE_DATA;

/// @}

#endif // CMNDSETPULSEAMPL_H_
 
 
 
/**********************************************************************
 * Revision History
 *
 * $Log$
 *
 **********************************************************************/
