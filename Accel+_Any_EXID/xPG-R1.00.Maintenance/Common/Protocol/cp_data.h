/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Define the Get CP Data packet structure
 *	
 ***************************************************************************/
 
#ifndef CP_DATA_H_
#define CP_DATA_H_

/// Length of a CP data block, in bytes
#define CP_DATA_LEN	256

/// Number of CP data blocks stored
#define NUM_CP_DATA	40

#endif /*CP_DATA_H_*/
