/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Data structure for the Set Program Constants command.
 * 
 ***************************************************************************/

#ifndef CMNDSETPULSECONST_H_
#define CMNDSETPULSECONST_H_

#include "pulseConst.h"

/// \defgroup SetPulseConstCmnd Set Pulse Constants Command
/// \ingroup mics_protocol
/// Set the pulse constant parameters for the xPG.
///  
/// @{

/// The parameters for a Set Program Constants command are simply a PULSE_CONST
/// structure.

typedef struct SET_PULSE_CONST_PARAMS {
	PULSE_CONST pulseConst;
} SET_PULSE_CONST_PARAMS;

/// @}


#endif /*CMNDSETPULSECONST_H_*/
