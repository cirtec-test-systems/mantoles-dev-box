/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Define the Read Memory Data Structures
 *
 ***************************************************************************/

#ifndef CMNDREADMEMORY_H_
#define CMNDREADMEMORY_H_

#include "memory_access.h"

typedef struct READ_MEMORY_PARAMS
{
	uint32_t address;	//The address at which to begin the write. See the
						//address space table above. Addresses in word-access
						//regions must be even.

	uint16_t length;	//The number of bytes to write. Lengths in word-access
						//regions must be even.
} READ_MEMORY_PARAMS;

typedef struct READ_MEMORY_RESPONSE_DATA
{
	uint32_t address;	//The address at which to begin the write. See the
						//address space table in SWEX 0085. Addresses in
						//word-access regions must be even.

	uint16_t length;	//The number of bytes to write. Lengths in word-access
						//regions must be even.

	uint8_t data[MAX_MEMORY_READ_SIZE];	//The data to write, starting at element 0 and
										//proceeding to element (length - 1).
} READ_MEMORY_RESPONSE_DATA;

#endif /* CMNDREADMEMORY_H_ */
