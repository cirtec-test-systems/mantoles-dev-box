/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.2                            */
/***************************************************************************/

/****************************************************************************
 *	QiG group Source File
 *	
 *	Copyright (c) 2010 QiG group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	QiG Part Number: Number
 *	Project: Project
 *	File Name: $RCSfile$
 *	@version: $Revision$
 *	@author: $Author$
 *	
 *	$Id$
 *	
 *	@file Description: Description
 *	
 ***************************************************************************/

#ifndef CMNDGETPRGMDEF_H_
#define CMNDGETPRGMDEF_H_

#include "prgmDef.h"

/// \defgroup getPrgmDefCmnd Get Program Definition Command
/// \ingroup mics_protocol
/// Get Program Definition for specified Program Number.
///  
/// @{

/// Get Program Definition - Parameter Data
/// - NOTE: Group individual byte sized parameters in pairs.   
typedef struct GET_PRGM_DEF
{
	/// indicates the program number to get program definition for.
	/// - valid values: see #_PROGRAM_NUMBER
	uint8_t program;
} GET_PRGM_DEF_PARAMS; 


/// Get Program Definition - Response Data
/// - NOTE: Group individual byte sized parameters in pairs.   

typedef struct GET_PRGM_DEF_RESPONSE_DATA
{
	/// indicates the program number to get program definition for.
	/// - valid values: see #_PROGRAM_NUMBER
	uint8_t program;

    /// filler byte to keep program def on normal memory boundaries
	uint8_t fillerByte;

	/// program definition for selected program number
	/// - see #_PRGM_DEF
	PRGM_DEF programDef;
} GET_PRGM_DEF_RESPONSE_DATA;

  
/// @}

#endif // CMNDGETPRGMDEF_H_
 
 
 
/**********************************************************************
 * Revision History
 *
 * $Log$
 *
 **********************************************************************/
