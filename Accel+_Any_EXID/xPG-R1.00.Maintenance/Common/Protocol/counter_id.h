/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Data format for Set Counter command
 *	
 ***************************************************************************/

#ifndef COUNTER_ID_H_
#define COUNTER_ID_H_

/**
 * List of eventCounterIDs and their values
 */

/*@-enummemuse@*/
typedef enum {
	COUNTER_STIM_RECOVER,				///< Number of times attempting to recover from reset while stimulating.
	COUNTER_PRECHARGE_COMPLETE,			///< Number of times charging has gone from pre-charge to constant current
	COUNTER_CHARGE_COMPLETE,			///< Number of times charging has reached the end of constant voltage state or charge complete stage
	COUNTER_STORAGE_MODE,				///< Number of times the system has come out of storage mode
	COUNTER_RUN_TIME_SECS_PRG_1,		///< The Cumulative Run-time of Program 1
	COUNTER_RUN_TIME_SECS_PRG_2,		///< The Cumulative Run-time of Program 2
	COUNTER_RUN_TIME_SECS_PRG_3,		///< The Cumulative Run-time of Program 3
	COUNTER_RUN_TIME_SECS_PRG_4,		///< The Cumulative Run-time of Program 4
	COUNTER_RUN_TIME_SECS_PRG_5,		///< The Cumulative Run-time of Program 5
	COUNTER_RUN_TIME_SECS_PRG_6,		///< The Cumulative Run-time of Program 6
	COUNTER_RUN_TIME_SECS_PRG_7,		///< The Cumulative Run-time of Program 7
	COUNTER_RUN_TIME_SECS_PRG_8,		///< The Cumulative Run-time of Program 8
	COUNTER_RUN_TIME_SECS_PRG_9,		///< The Cumulative Run-time of Program 9
	COUNTER_RUN_TIME_SECS_PRG_10,		///< The Cumulative Run-time of Program 10
	COUNTER_BATTERY_DEPLETION,			///< The Counter for number of times battery is depleted
	COUNTER_SELECTED_PROGRAM,			///< The Counter that saves the currently select program prior to a reset
	NUM_COUNTER							///< Total number of counters in the system
} COUNTER;


#endif /*COUNTER_ID_H_*/
