/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Define the Set Ramp Time packet structures
 *	
 ***************************************************************************/

#ifndef CMNDSETRAMPTIME_H_
#define CMNDSETRAMPTIME_H_

#include <stdint.h>

/// \defgroup setRampTime Set Ramp Time
/// \ingroup mics_protocol
/// Returns the ramp time in milliseconds.
///  
/// @{

typedef struct SET_RAMP_TIME_PARAMS {
	uint16_t rampTime;
} SET_RAMP_TIME_PARAMS;

/// @}

#endif /*CMNDSETRAMPTIME_H_*/
