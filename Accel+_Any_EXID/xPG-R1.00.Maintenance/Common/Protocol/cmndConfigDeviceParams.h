/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	Project: SCS System - IPG Firmware
 *	
 *	@file Description: Structure definition for configurable device parameters.
 *	                   This structure is used for the Get / Set Configurable
 *                     device commands.  These are used for pairing of the
 *                     patient's PoPs and PPC with their implant, for 
 *                     personalizing the banner on their PPC, for enabling or
 *                     disabling certain functions / features on their PPC
 *                     or ipg, for recording the date of implantation and 
 *                     the recommended replacement interval (ERI - elective
 *                     replacement interval), among other things.
 * 
 ***************************************************************************/
#ifndef CMNDCONFIGDEVICEPARAMS_H_
#define CMNDCONFIGDEVICEPARAMS_H_

#include <stdint.h>
#include "cmndCommon.h"

/// \defgroup configDeviceParamsCmnd Configurable Device Parameters Command
/// \ingroup mics_protocol
/// Set / get the configurable device parameters for the xPG.
///  
/// @{

/// number of bytes for the implantation date format (ERI)
#define NUM_BYTES_IMPLANT_DATE 8

/// limits for amplitude steps setting (normally set to default of 50)
#define MIN_AMPL_STEPS_SETTING   10
#define MAX_AMPL_STEPS_SETTING   50

// Flag bits for ipgMagnetOptions
#define MAGNET_OPTION_ENABLE_STOP 0x01		///< ipgMagnetOptions: Magnet Sense Stop Feature Enable


#define DEFAULT_PULSE_WIDTH_STEP 1

/// allowed pulse step width in microseconds
typedef enum PULSE_WIDTH_STEP_LIMIT
{
   PULSE_WIDTH_STEP_MIN =   1,
   PULSE_WIDTH_STEP_MAX = 200
} PULSE_WIDTH_STEP_LIMIT;

/// structure for comm and storage of external device ID
/// allowed values are 0x000000 to 0xFEFFFF
typedef struct EXID_BYTES
{
   uint8_t lsb;         ///< EXID least significant byte
   uint8_t mid; 		///< EXID middle byte
   uint8_t msb;         ///< EXID most significant byte
} EXID_BYTES;

/// Total number of paired devices.
#define NUM_PAIRINGS 3

///Configurable Device Parameters - parameter data structure
typedef struct CONFIG_DEVICE_PARAMETERS
{
	struct {
		/// boolean indicating whether or not PPC is enabled
		uint8_t enabled;
  		/// EXID of paired PPC
  		/// - values / format: see _EXID_BYTES
		EXID_BYTES exid;
	} pairing[NUM_PAIRINGS]; 

   /// characters for ppc personalized text
  /// - text gets displayed on PPC UI
  char ppcText[PPC_TEXT_CHARS];

  /// pulse width step size
  /// - values: see _PULSE_WIDTH_STEP_LIMIT
  uint16_t pulseWidthStep;

  ///number of amplitude steps for inc/dec program amplitude
  /// - see amplStepIndex.h for details
  uint8_t amplitudeSteps;

  /// ppc behavior options
  /// - values / format: see _PPC_OPTIONS_BITMAP
  uint8_t ppcOptions;

  /// ipg magnet options
  /// See MAGNET_OPTION_ENABLE_STOP, etc., above.
  uint8_t ipgMagnetOptions;

  uint8_t fillerByte;      ///< filler to end on even

  //03/28/2011 added implant date / time and replacement months
  // this is part of 'elective replacement', so an external (the PPC or CP)
  // informs the user when the implanted device is should be replaced
  // Note that the date/time format is actually a Microsoft date/time format
  // as a 64-bit number, indicating the number of 100 nsec increments since
  // some reference date.  The xPG simply handles it as an 8 byte array.
  
  uint8_t implantDateTime[NUM_BYTES_IMPLANT_DATE];  ///< this is actually in a microsoft date/time format
  
  uint8_t replacementIntervalMonths; ///<number of months after implant date
  
  uint8_t fillerByte2;   ///<filler to end on even
  
 
  //add battery & temp options here
} CONFIG_DEVICE_PARAMETERS;

/// @}

#endif // CMNDCONFIGDEVICEPARAMS_H_
