/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Define the Get VBAT packet structures
 *
 ***************************************************************************/

#ifndef CMNDIMPEDANCE_H_
#define CMNDIMPEDANCE_H_

/**
 * \defgroup ImpedanceCmnd Impedance Measurement command
 * \ingroup mics_protocol
 * Impedance Measurement
 *
 * Instructs the XPG to measures the impedance between the two selected
 * electrode channels per the specified parameters.
 *
 * @{
 */

/**
 * Impedance measurement command - parameters
 */

typedef struct IMPEDANCE_PARAMS {
	uint8_t channelA;			///< Channel for measurement (1-26, 0 for no connection, or 27 for internal voltage reference)
	uint8_t channelB;			///< Channel for measurement (1-26, 0 for no connection, or 27 for internal voltage reference)

	uint8_t pulseAmplitude;	///< Amplitude of measurement pulse
} IMPEDANCE_PARAMS;

/**
 * Impedance measurement command - response data
 */
typedef struct IMPEDANCE_RESPONSE_DATA {
	uint8_t channelA;			///< Channel for measurement (1-26, 0 for no connection, or 27 for internal voltage reference)
	uint8_t	channelB;			///< Channel for measurement (1-26, 0 for no connection, or 27 for internal voltage reference)

	uint16_t impedance;			///< Impedance reading. (ohms)
} IMPEDANCE_RESPONSE_DATA;

/// @}

#endif /* CMNDIMPEDANCE_H_ */
