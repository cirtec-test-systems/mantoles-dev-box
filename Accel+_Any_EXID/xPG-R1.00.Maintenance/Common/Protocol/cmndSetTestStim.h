/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Set Test Stimulation command
 *	
 ***************************************************************************/

#ifndef CMNDSETTESTSTIM_H_
#define CMNDSETTESTSTIM_H_

#include <stdint.h>
#include "testStim.h"

/// \defgroup setTestStim Set Test Stimulation command
/// \ingroup mics_protocol
/// Set Test Stimulation settings.
///  
/// @{

/// Set Test Stimulation - Command Parameters
/// - NOTE: Group individual byte sized parameters in pairs.   

typedef TEST_STIM SET_TEST_STIM_PARAMS;

/// @}

#endif /*CMNDSETTESTSTIM_H_*/
