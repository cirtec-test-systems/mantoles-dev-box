/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.2                            */
/***************************************************************************/

/****************************************************************************
 *	QiG group Source File
 *	
 *	Copyright (c) 2010 QiG group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	QiG Part Number: Number
 *	Project: Project
 *	File Name: $RCSfile$
 *	@version: $Revision$
 *	@author: $Author$
 *	
 *	$Id$
 *	
 *	@file Description: Description
 *	
 ***************************************************************************/

#ifndef CMNDGETPRGMFREQ_H_
#define CMNDGETPRGMFREQ_H_

/// \defgroup getPrgmFreqCmnd Get Program Frequency Command
/// \ingroup mics_protocol
/// Get the frequency (table index) for the presently selected program.
///  
/// @{

/// Get Program Frequency - Response Data
/// - NOTE: Group individual byte sized parameters in pairs.   

typedef struct GET_PRGM_FREQ_RESPONSE_DATA
{
	/// index in program frequency table at which presently selected program should run.
	/// - valid values: #_FREQUENCY_INDEX_LIMIT
	uint8_t programFrequencyIndex;

} GET_PRGM_FREQ_RESPONSE_DATA;

/// @}

#endif // CMNDGETPRGMFREQ_H_
 
 
 
/**********************************************************************
 * Revision History
 *
 * $Log$
 *
 **********************************************************************/
