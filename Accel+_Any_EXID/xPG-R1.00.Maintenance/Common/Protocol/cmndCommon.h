/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.2                            */
/***************************************************************************/

/****************************************************************************
 *	QiG group Source File
 *	
 *	Copyright (c) 2010 QiG group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	QiG Part Number: Number
 *	Project: Project
 *	File Name: $RCSfile$
 *	@version: $Revision$
 *	@author: $Author$
 *	
 *	$Id$
 *	
 *	@file Description: Description
 *	
 ***************************************************************************/

#ifndef CMND_COMMON_H_
#define CMND_COMMON_H_

#include <stdint.h>

/// \defgroup mics_protocol MICS Protocol

/// \defgroup CmndCommon Common Command Specifications 
/// \ingroup mics_protocol
///  
/// @{

/// number of characters allotted to represent model number of xPG device
/// - number of characters does not include null for string termination
#define MODEL_NUMBER_CHARS 10

/// number of characters allotted to represent serial number of xPG device
/// - number of characters does not include null for string termination
#define SERIAL_NUMBER_CHARS 20

/// number of characters allotted to represent firmware version of xPG device
/// - number of characters does not include null for string termination
#define FIRMWARE_VERSION_CHARS 10


/// number of characters allotted for 'PPC TEXT' displayed on PPC User Interface
/// - number of characters does not include null for string termination
#define  PPC_TEXT_CHARS 30 

/// BitMapped enumeration of PPC configuration options
typedef enum PPC_OPTIONS_BITMAP
{
	PPC_OPTION_INDV_PULSE_AMPL_MODS =  1 << 0,
	PPC_OPTION_PULSE_WIDTH_MODS     =  1 << 1,
	PPC_OPTION_PRGM_FREQ_MODS       =  1 << 2
} PPC_OPTIONS_BITMAP;


/// @}

#endif // CMND_COMMON_H_
 
 
 
/**********************************************************************
 * Revision History
 *
 * $Log$
 *
 **********************************************************************/
