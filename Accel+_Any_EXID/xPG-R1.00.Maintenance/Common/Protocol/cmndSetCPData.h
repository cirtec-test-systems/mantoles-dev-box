/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Define the Set CP Data packet structure
 *	
 ***************************************************************************/
 
#ifndef CMNDSETCPDATA_H_
#define CMNDSETCPDATA_H_

#include "cp_data.h"

/// \defgroup setCPData Set CP Data Command
/// \ingroup mics_protocol
/// Set a CP data block.
///  
/// @{

/// Set CP Data - Parameter Data
typedef struct SET_CP_DATA
{
	/// The data block number requested
	/// Range from 0 to NUM_CP_DATA
	uint8_t block;

    /// padding byte for word-alignment
	uint8_t filler;

	/// block of opaque data stored on behalf of the CP
	uint8_t data[CP_DATA_LEN];
} SET_CP_DATA_PARAMS; 

/// @}

#endif // CMNDSETCPDATA_H_
