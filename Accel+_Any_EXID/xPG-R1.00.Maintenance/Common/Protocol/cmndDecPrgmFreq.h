/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.2                            */
/***************************************************************************/

/****************************************************************************
 *	QiG group Source File
 *	
 *	Copyright (c) 2010 QiG group
 *	1771 E. 30th St. Cleveland, OH 44114
 *	
 *	QiG Part Number: Number
 *	Project: Project
 *	File Name: $RCSfile$
 *	@version: $Revision$
 *	@author: $Author$
 *	
 *	$Id$
 *	
 *	@file Description: Description
 *	
 ***************************************************************************/

#ifndef CMNDDECPRGMFREQ_H_
#define CMNDDECPRGMFREQ_H_

/// \defgroup decPrgmFreqCmnd Decrement Program Frequency Command
/// \ingroup mics_protocol
/// Decrement to the next allowed frequency (table index) for the presently selected program. Skip over any frequency table entries not allowed for that program as specified in its program definition.
///  
/// @{

/// Decrement Program Frequency - Response Data
/// - NOTE: Group individual byte sized parameters in pairs.   

typedef struct DEC_PRGM_FREQ_RESPONSE_DATA
{
	/// index in program frequency table at which presently selected program should run.
	/// - valid values: #_FREQUENCY_INDEX_LIMIT
	uint8_t programFrequencyIndex;

} DEC_PRGM_FREQ_RESPONSE_DATA;

/// @}

#endif // CMNDDECPRGMFREQ_H_
 
 
 
/**********************************************************************
 * Revision History
 *
 * $Log$
 *
 **********************************************************************/
