/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Define the Get Trim List packet structures
 *	
 ***************************************************************************/

#ifndef TRIM_LIST_H_
#define TRIM_LIST_H_

#include <stdint.h>

/// Number of 3-byte (address, data) tuples in a trim list
#define TRIM_LIST_ENTRIES			32
#define SATURN_TRIM_LIST_ENTRIES 	7
//Padding for storage to maintain compatibility with existing versions of the bootloader.
#define TRIM_LIST_PADDING_SIZE			324

/// Data type for a trim list
typedef struct TRIM_LIST {
	uint8_t listID;		///< Selects which trim list to update
	uint8_t listLen;	///< Number of (address, data) tuples in this trim list.

	/// Array of (address, data) tuples, each 3 bytes long. The 3-byte tuples are
	/// stored with byte alignment and no padding. This differs from the word
	/// alignment that is standard everywhere else in this protocol.
	uint8_t trims[TRIM_LIST_ENTRIES * 3];
	uint8_t padding[TRIM_LIST_PADDING_SIZE];
} TRIM_LIST;

/// Data type for a trim list
typedef struct TRIM_LIST_CMD {
	uint8_t listID;		///< Selects which trim list to update
	uint8_t listLen;	///< Number of (address, data) tuples in this trim list.

	/// Array of (address, data) tuples, each 3 bytes long. The 3-byte tuples are
	/// stored with byte alignment and no padding. This differs from the word
	/// alignment that is standard everywhere else in this protocol.
	uint8_t trims[TRIM_LIST_ENTRIES * 3];
} TRIM_LIST_CMD;

/// ID codes for the trim lists.
enum TRIM_LIST_ID {
	TRIM_PLUTO = 0,
	TRIM_ZL = 1,
	TRIM_SATURN = 2,
	TRIM_ZL_FACTORY = 3,
	NUM_TRIM_LISTS
};

#endif /*TRIM_LIST_H_*/
