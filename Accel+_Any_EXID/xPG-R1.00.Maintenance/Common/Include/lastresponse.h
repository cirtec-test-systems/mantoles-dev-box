/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Header file with the interface for reading and setting
 *	the most recent Response Code.
 *	
 ***************************************************************************/


#ifndef LASTRESPONSE_H
#define LASTRESPONSE_H

#include "Common/Protocol/response_code.h"

/**
 * Retrieve the most recent Response Code set when a function returns an error
 * indication.
 *
 * If no error has occurred since the Response Code was last cleared, returns
 * GEN_SUCCESS.
 *
 * \return the response code relevant to the most recent error
 */
RESPONSE getLastResponseCode(void);


/**
 * Set the response code returned to GEN_SUCCESS.
 */
void clearResponseCode(void);


/**
 * Set the Response Code that will be returned by getLastResponseCode().
 *
 * \param rc  The Response Code that should be reported by getLastResponseCode()
 */
void setResponseCode(RESPONSE rc);



#endif /* LASTRESPONSE_H */
