/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Data structure for pulse constants data block, used
 *                     by Get / Set Pulse Constants commands.
 *	
 ***************************************************************************/
 
#ifndef PULSECONST_H_
#define PULSECONST_H_

#include <stdint.h>

/// \defgroup pulseConst Pulse Constants
/// \ingroup mics_protocol
/// The pulse constants data structure for the xPG.
///  
/// @{

/// limits for interphase delays
typedef enum INTERPHASE_DELAY_LIMIT
{
   INTERPHASE_DELAY_MIN =   1,
   INTERPHASE_DELAY_MAX = 255
} INTERPHASE_DELAY_LIMIT;

/// limits for passive recovery width
typedef enum PASSIVE_RECOVERY_LIMIT
{
   PASSIVE_RECOVERY_WIDTH_MIN =    10,
   PASSIVE_RECOVERY_WIDTH_MAX = 15000
} PASSIVE_RECOVERY_LIMIT;

/// limits for holdoffs after recovery phases or CBC phases
typedef enum HOLDOFF_LIMIT
{
   HOLDOFF_MIN =     1,
   HOLDOFF_MAX = 15000
} HOLDOFF_LIMIT;

/// limits for active recovery width
typedef enum ACTIVE_RECOVERY_LIMIT
{
   ACTIVE_RECOVERY_WIDTH_MIN =    10,
   ACTIVE_RECOVERY_WIDTH_MAX = 15000
} ACTIVE_RECOVERY_LIMIT;

/// BitMapped enumeration of Krec options
#define NUM_KREC			5
#define KREC_MASK			0x1F

/// limits for increment lockout, in msecs
typedef enum INCREMENT_LOCKOUT_LIMIT
{
   INCREMENT_LOCKOUT_MIN =     0,
   INCREMENT_LOCKOUT_MAX =  2000
} INCREMENT_LOCKOUT_LIMIT;

///Pulse Constants - parameter data structure
typedef struct PULSE_CONST
{
  /// length of interphase delay when using passive recovery, in usecs
  /// - values: see _INTERPHASE_DELAY_LIMIT
  uint16_t  passiveInterphaseDelay;              

  /// length of recovery pulse when using passive recovery, in usecs
  /// - values: see _PASSIVE_RECOVERY_LIMIT
  uint16_t  passiveRecoveryWidth;    
  
  /// length of recovery holdoff when using passive recovery, in usecs
  /// - values: see _HOLDOFF_LIMIT
  uint16_t  passiveRecoveryHoldoff;          

  /// length of CBC phase when using passive recovery, in usecs
  /// - values: see _PASSIVE_RECOVERY_LIMIT
  uint16_t  passiveCBCWidth;              

  /// Global channel calibration
  uint16_t  globalChannelCal;

  /// length of interphase delay when using active recovery, in usecs
  /// - values: see _INTERPHASE_DELAY_LIMIT
  uint16_t  activeInterphaseDelay;              

  /// length of recovery holdoff when using active recovery, in usecs
  /// - values: see _HOLDOFF_LIMIT
  uint16_t  activeRecoveryHoldoff;          

  /// length of CBC phase when using active recovery, in usecs
  /// - values: see _ACTIVE_RECOVERY_LIMIT
  uint16_t  activeCBCWidth;              

  /// length of holdoff after CBC when using active recovery, in usecs
  /// - values: see _HOLDOFF_LIMIT
  uint16_t  activeCBCHoldoff;              

  /// bitmap indicating values of Krec enabled for active recovery
  /// - values / format: see _KREC_OPTIONS_BITMAP
  uint16_t  krecsEnabledBitMap;

  /// duration of increment lockout, in msecs
  /// - values: see _INCREMENT_LOCKOUT_LIMIT
  uint16_t  incrementLockoutMsecs;
  
  /// boolean indicating if disable pwr supply during stim phase feature is enabled
  /// - values: TRUE (0x01), FALSE (0x00)
  uint8_t   stimPhasePSDisable;

  /// boolean indicating whether uncontrolled source/sink feature is enabled
  /// - values: TRUE (0x01), FALSE (0x00)
  uint8_t   uncontrolledSrcSnkEnabled;
  
} PULSE_CONST;

/// @}


#endif /*PULSECONST_H_*/
