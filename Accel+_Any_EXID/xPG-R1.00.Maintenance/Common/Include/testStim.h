/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Test stimulation structure
 *	
 ***************************************************************************/
#ifndef TESTSTIM_H_
#define TESTSTIM_H_

#include <stdint.h>
#include "prgmDef.h"            //for NUM_CHANNELS

//for defining an electrode channel as uncontrolled
#define  UNCONTROLLED_SRC_SNK_CHAN        127

//parameter limits
#define  TP_MIN_PULSE_AMPLITUDE			   15
#define  TP_MAX_PULSE_AMPLITUDE         30000

#define  TP_MIN_CHANNEL_AMPLITUDE	        0
#define  TP_MAX_CHANNEL_AMPLITUDE       15000

#define  TP_MIN_INTERPHASE_DELAY            1
#define  TP_MAX_INTERPHASE_DELAY          255

#define  TP_MIN_RECOVERY_RATIO              0
#define  TP_MAX_RECOVERY_RATIO              5

#define  TP_MIN_PASSIVE_RECOVERY_WIDTH      2
#define  TP_MAX_PASSIVE_RECOVERY_WIDTH  15000

#define  TP_MIN_CBC_PULSE_WIDTH             2
#define  TP_MAX_CBC_PULSE_WIDTH         10000


#define USRC_STIM	0x01	///< Flag: If set, stim-phase uncontrolled-current channels are sources. Otherwise, they are sinks.
#define USRC_REC	0x02	///< Flag: If set, recovery-phase uncontrolled-current channels are sources. Otherwise, they are sinks.
#define USRC_CBC	0x04	///< Flag: If set, cbc-phase uncontrolled-current channels are sources. Otherwise, they are sinks.

#define WAVE_STIM_RECT	0x00
#define WAVE_STIM_RAM0	0x10
#define WAVE_STIM_RAM1	0x18
#define WAVE_STIM_MASK	(WAVE_STIM_RECT | WAVE_STIM_RAM0 | WAVE_STIM_RAM1)

#define WAVE_RECOVERY_RECT	0x00
#define WAVE_RECOVERY_RAM0	0x40
#define WAVE_RECOVERY_RAM1	0x60
#define WAVE_RECOVERY_MASK	(WAVE_RECOVERY_RECT | WAVE_RECOVERY_RAM0 | WAVE_RECOVERY_RAM1)

/// Test stimulation pulse structure
typedef struct TEST_PULSE {
    uint16_t amplitude;				///< Stim phase amplitude in microamps

    int8_t channelPercentage[NUM_CHANNELS];  ///< Amplitude percentage for each channel, or UNCONTROLLED_SRC_SNK_CHAN.

    uint16_t stimWidth; 			///< Stim phase pulse width in microseconds

    uint8_t interphaseDelay;  		///< Interphase delay (us)
    
    uint8_t recoveryRatio;  		///< Recovery ratio select
    								///< 0 = passive, 1-5 = recovery ratio

    uint16_t passiveRecoveryWidth;	///< Width of passive recovery (us). Used only if recoveryRatio == 0.

    uint16_t cbcWidth;        		///< Width of CBC (us). Set to 0 for no CBC phase.

    uint8_t flags;					///< Bitfield of flags controlling pulse options

    uint8_t reserved;				///< Pad byte reserved for future use. Must be 0 on transmit. Ignored on receive.

} TEST_PULSE;

/// Test stimulation structure
typedef struct TEST_STIM {
	uint8_t frequencyIdx;			///< Index into program frequency table
	
	uint8_t complianceVoltage;		///< Compliance voltage setting, in Saturn boost converter DAC units
	
	uint8_t numPulses;				///< Number of pulses in the program. 0 turns off stimulation.
	
	uint8_t reserved;				///< Word-alignment padding byte.
	
	TEST_PULSE pulse[NUM_PULSES];
} TEST_STIM;


#endif                          /*TESTSTIM_H_ */
