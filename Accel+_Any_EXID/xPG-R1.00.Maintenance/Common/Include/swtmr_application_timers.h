/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Header file where the software timers are defined.
 * Add an element to the SWTMR_ENUM enumeration and then call:\n 
 * swTimer_Set() to start the timer.\n
 * swTimer_Cancel() to stop the timer.\n
 *	
 ***************************************************************************/

#ifndef SWTMR_APPLICATION_TIMERS_H_
#define SWTMR_APPLICATION_TIMERS_H_

/// \addgroup swTimer 
/// @{

typedef enum {
    //All the virtual timers are defined here
	SWTMR_CHARGE_STATE_TIMER,	// This timer is used to ensure charging does not go on too long in any one state
	SWTMR_CHARGE_POLLING,		// This timer is used to periodically check the charge state
	SWTMR_READ_BATTERY,			// This timer is used to periodically check the battery state
	SWTMR_STIM_RAMPING,			// This timer will be used for both Stim Ramping and Stim Increment Lockout
	SWTMR_CHECK_HEALTH_DAILY,	// This timer will be used for daily checking of system health
	SWTMR_CHECK_HEALTH_STIM,	// This timer will be used by system health monitor for checking system health during stimulation
	SWTMR_SERVICE_WATCHDOG,		// This timer will be used to service the watchdog
	SWTMR_MICS_RESPONSE_TIMEOUT,// This timer will be used by command processor so not to wait forever for a MICS cmd response
	SWTMR_TETS_RESPONSE_TIMEOUT,// This timer will be used by command processor so not to wait forever for a TETS cmd response
	SWTMR_MICS_RXTX_TIMEOUT,	// This timer will be used by mics packet assembler/disassembler so to not wait forever sending/receiving packets
	SWTMR_MAGNET_TIMER,			// This timer will be used by magnet monitor for attempting to turn off stimulation prior to going into storage mode
	SWTMR_STIM_RUN_TIME,		// This timer will be used by the patient stimulation manager to help keep track of the cumulative run-time for each program
	SWTMR_LOW_RATE_TIMEOUT,		// This timer will be used if charging at a low rate and would like to attempt at high rate

	SWTMR_ARM_PAIR_TIMEOUT,		// This timer is used to time out while trying to pair an external via TETS
	SWTMR_ARM_STOP_TIMEOUT,		// This timer is used to time out while trying to stop stim via TETS
	SWTMR_STIM_CYCLE_CHECK,		// This timer will be used by the system health monitor to measure stim period during stimulation
	SWTMR_FLASH_STIM_LED,		// This timer will be used to flash the stim led
	SWTMR_FLASH_RED_LED,		// This timer will be used to flash the red (low batt) led
	SWTMR_FLASH_GREEN_LED,		// This timer will be used to flash the green (power on) led
	SWTMR_ONE_MINUTE_TEST,		// This timer is used to log the charging data (vrect and temperature)
	SWTMR_COMPLETING_CHARGING,	// This timer is used to introduce a delay before completing charging, so as to not restart charging

	SWTMR_MICS_CONNECTION,	//!< This timer is used to cause timeouts on transitions in the MICS 400MHz wakeup states.
	SWTMR_SWTIMER_TOGGLETEST,	//!< Used to test the software timer's functionality.

	SWTMR_MICS_TRIM_COPY,	//!< This timer is used to disable the MICS trim list copy back functionality in magnet monitor.

	SWTMR_POLL_LINKSTATUS,  //!< This timer is used to poll the CRCERR and ECCERR status during a session, optionally reporting errors via TST_TRIG LED

    SWTMR_NUM_TIMERS
} SWTMR_ENUM ;

/// @}

#endif /*SWTMR_APPLICATION_TIMERS_H_*/
