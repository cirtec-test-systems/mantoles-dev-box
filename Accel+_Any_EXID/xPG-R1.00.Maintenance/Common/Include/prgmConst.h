/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Data structure for the program constants.  This is 
 *	                   used by the Get / Set Program Constants command.
 * 
 ***************************************************************************/
 
#ifndef PGRMCONST_H_
#define PGRMCONST_H_

#include <stdint.h>
#include "prgmDef.h"

/// \defgroup prgmConst Program Constants Definition
/// Detail of the structure and elements of Program Constants definitions.
///
/// Note that Program Constants are not constant -- they can be changed by a
/// command from the CP.
///  
/// @{


/// All ramp times must be a multiple of 1000 ms.
#define RAMP_TIME_MULTIPLE 1000

/// The maximum ramp time
#define RAMP_TIME_MAX 8000

/**
 * Program Constants data structure
 * 
 * \warning This is a communications protocol data structure, so do not change
 * its binary layout without equivalent changes in the protocol spec and
 * external devices. 
 */

typedef struct PRGM_CONST
{
    uint16_t zero;				    				///< Unused. Must be zero.
    uint16_t prgmFreq[MAX_FREQUENCY_SELECTIONS];	///< program definition for selected program number
} PRGM_CONST;

/// @}

#endif /*PGRMCONST_H_*/
