/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: List of all event IDs in the system
 *	
 ***************************************************************************/

/*
 * This file contains the list of event identifiers for the EVENT_ID enum.
 *
 * It is included in system_events.h, where it is processed into the declaration
 * of the EVENT_ID enum.
 *
 * It is also included in EventQueue.c, where it is processed into a string
 * table of event names for debugging purposes.
 *
 * Every event identifier should be wrapped in the DEFINE_EVENT_ID macro.
 * The files that include this list define DEFINE_EVENT_ID as needed to
 * either define the event ID as an identifier or to use it as a string.
 *
 * Add any new event identifiers to the bottom of the list, and wrap them
 * in a DEFINE_EVENT_ID macro, like the others.
 *
 * Make sure there is a comma at the end of every line, even the last one.
 */

DEFINE_EVENT_ID( E_NO_EVENT ),               	//!< The null event.  Nothing to process.  Used as a token and an initialization.
DEFINE_EVENT_ID( E_ARM_PAIR_TIMEOUT ),			//!< This event published when timing out waiting to pair an external via charging
DEFINE_EVENT_ID( E_ARM_STOP_TIMEOUT ),			//!< This event published when timing out waiting to stop stim via charging
DEFINE_EVENT_ID( E_BATT_MEASURE_BATTERY ),
DEFINE_EVENT_ID( E_BROWNOUT_DETECTED ),
DEFINE_EVENT_ID( E_CHARGE_CONCUR_STATE_TO ),	//!< This event is for a timeout that occurs if in constant current too long.
DEFINE_EVENT_ID( E_CHARGE_LOGGING_TIMEOUT ),
DEFINE_EVENT_ID( E_CHARGE_LOW_RATE_TO ),		//!< Charging is proceeding at low rate and should try at high rate after this timeout
DEFINE_EVENT_ID( E_CHARGE_PRECON_STATE_TO ),	//!< This event is for a timeout that occurs if in pre-condition charge state too long.
DEFINE_EVENT_ID( E_CHARGER_DISENGAGED ),
DEFINE_EVENT_ID( E_CHARGER_ENGAGED ),
DEFINE_EVENT_ID( E_CHARGING_ALLOWED ),
DEFINE_EVENT_ID( E_CHARGING_COMPLETING),
DEFINE_EVENT_ID( E_CHARGING_NOT_ALLOWED ),
DEFINE_EVENT_ID( E_CHARGING_POLLING ),
DEFINE_EVENT_ID( E_CLOCK_VERIFY ),             	//!< Causes system health monitor to check clocks, stack, etc.
DEFINE_EVENT_ID( E_CMD_RESPONSE ),
DEFINE_EVENT_ID( E_ENTER_STORAGE_MODE ),
DEFINE_EVENT_ID( E_ERR_CHARGER_ERROR ),        	//!< Generic error for all charging error, then use charging error code
DEFINE_EVENT_ID( E_ERR_ERROR_DETECTED ),
DEFINE_EVENT_ID( E_ERR_EVENT_QUEUE_ERROR ),    	//!< Signaled by event dispatcher upon queue overflow
DEFINE_EVENT_ID( E_FLASH_STIM_LED ),
DEFINE_EVENT_ID( E_FLASH_GREEN_LED ),
DEFINE_EVENT_ID( E_FLASH_RED_LED ),
DEFINE_EVENT_ID( E_IMPEDANCE_COMMAND ),        	//!< Sent from the MICS module to the Impedance Manager when an On Demand Impedance Measurement should be performed
DEFINE_EVENT_ID( E_START_IMP_MEASUREMENT ),     //!< From the Stim Director to the Impedance Measurement module indicating the Impedance Manager should go into Impedance Measurement mode
DEFINE_EVENT_ID( E_INITIALIZE ),
DEFINE_EVENT_ID( E_LOG_EVENT),
DEFINE_EVENT_ID( E_MAGNET_TMR_SWIPE ),
DEFINE_EVENT_ID( E_MAGNET_TMR_STORAGE ),
DEFINE_EVENT_ID( E_MAGNET_DISENGAGED ),
DEFINE_EVENT_ID( E_MAGNET_ENGAGED ),
DEFINE_EVENT_ID( E_MAGNET_TOGGLE_STIM ),
DEFINE_EVENT_ID( E_MICS_ABORT ),
DEFINE_EVENT_ID( E_MICS_CMD ),
DEFINE_EVENT_ID( E_MICS_DATA_BLOCK ),
DEFINE_EVENT_ID( E_MICS_RC_TIMEOUT ),
DEFINE_EVENT_ID( E_MICS_SEND_FRAME ),
DEFINE_EVENT_ID( E_MICS_SESSION_CLOSED ),
DEFINE_EVENT_ID( E_MICS_SESSION_OPEN ),
DEFINE_EVENT_ID( E_MICS_TIMEOUT ),
DEFINE_EVENT_ID( E_MICS_TUNEUP ),
DEFINE_EVENT_ID( E_PRG_RUN_TIME_TIMER ),		//!< Timer used to track the cumulative run time of each program
DEFINE_EVENT_ID( E_PRGM_DEF_CHANGED ),
DEFINE_EVENT_ID( E_PUBLISH_BATT_VOLTAGE ),     	//!< Send out an event that contains the current battery voltage reading
DEFINE_EVENT_ID( E_RAMP_TIMER ),               	//!< The timed event that Patient Stim uses while it is in the Ramping state.
DEFINE_EVENT_ID( E_RECOVER_STIM ),             	//!< The system was reset during stimulation, attempt to recover.
DEFINE_EVENT_ID( E_SELECT_PROGRAM ),           	//!< Select a program for stimulation.
DEFINE_EVENT_ID( E_SERVICE_WATCHDOG ),         	//!< Causes system health monitor to tickle the watchdog
DEFINE_EVENT_ID( E_SET_FREQUENCY ),				//!< Sets program frequency to the specified index in the currently selected program
DEFINE_EVENT_ID( E_SET_PULSE_WIDTH ),			//!< Sets the width of the pulse at the specified index in the currently selected program
DEFINE_EVENT_ID( E_SET_TEST_STIM ),
DEFINE_EVENT_ID( E_SET_TEST_WAVEFORM ),
DEFINE_EVENT_ID( E_SET_TITRATION_STIM ),
DEFINE_EVENT_ID( E_SOFTWARE_RESET ),
DEFINE_EVENT_ID( E_START_PATIENT_STIM ),       	//!< From the Stim Director to the Patient Stim module indicating Patient Stim should be started
DEFINE_EVENT_ID( E_START_TEST_STIM ),          	//!< From the Stim Director to the Test Stim module indicating Test Stim should be started
DEFINE_EVENT_ID( E_START_TITRATION_STIM ),     	//!< From the Stim Director to the Titration Stim module indicating Titration Stim should be started
DEFINE_EVENT_ID( E_STIM_ADDRESS ),				//!< Saturn address error has occurred
DEFINE_EVENT_ID( E_STIM_COMMAND ),             	//!< Sent from MICS to the Stimulation module when a Stimulation command is received.  The data area of the structure will contain the parameter: Active, Inactive, Titration, Test Pulse
DEFINE_EVENT_ID( E_STIM_CYCLE_CHECK ),			//!< Causes health monitor to stop collecting stim period measurements, avg available measurements, and compare to desired frequency
DEFINE_EVENT_ID( E_STIM_DECREASE_FREQUENCY ),
DEFINE_EVENT_ID( E_STIM_DECREASE_PROGRAM_AMPLITUDE ),
DEFINE_EVENT_ID( E_STIM_DECREASE_PULSE_AMPLITUDE ),
DEFINE_EVENT_ID( E_STIM_DECREASE_PULSE_WIDTH ),
DEFINE_EVENT_ID( E_STIM_ERR_IRQ ),				//!< Saturn ERR_n error interrupt has happened
DEFINE_EVENT_ID( E_STIM_FAILED ),              	//!< This event is sent by the Stimulation sub-modules if they can't start stimulation for any reason
DEFINE_EVENT_ID( E_STIM_INCREASE_FREQUENCY ),
DEFINE_EVENT_ID( E_STIM_INCREASE_PROGRAM_AMPLITUDE ),
DEFINE_EVENT_ID( E_STIM_INCREASE_PULSE_AMPLITUDE ),
DEFINE_EVENT_ID( E_STIM_INCREASE_PULSE_WIDTH ),
DEFINE_EVENT_ID( E_STIM_LOCKOUT_TIMER ),       	//!< The timed event that Patient Stim uses while it is in the Increment Lockout state.
DEFINE_EVENT_ID( E_STIM_PARITY ),				//!< Saturn parity error has occurred
DEFINE_EVENT_ID( E_STIM_PULSE_GUARD	),			//!< Saturn pulse guard timeout has occurred
DEFINE_EVENT_ID( E_STIM_REGISTER_VALID ),		//!< Saturn register valid error has occurred
DEFINE_EVENT_ID( E_STIM_SEQUENCER ),			//!< Saturn sequencer error has occurred
DEFINE_EVENT_ID( E_STIM_SET_AMPLITUDE ),       	//!< Sets the apmlitude of the pulse at the specified index in the currently selected program
DEFINE_EVENT_ID( E_STIM_STARTED ),             	//!< This event indicates that Stimulation of some form ( Patient, Titration, Test) has started.  It is sent out by the Stimulation modules.
DEFINE_EVENT_ID( E_STIM_STOPPED ),             	//!< This event indicates that Stimulation stopped.  It is sent out by the Stimulation modules.
DEFINE_EVENT_ID( E_STIM_WRITE_PROTECT ),		//!< Saturn write protect error has occurred
DEFINE_EVENT_ID( E_STOP_STIM ),                	//!< Message letting the Stimulation module know that any active stimulation pulses/programs need to be stopped
DEFINE_EVENT_ID( E_STOP_STIM_SUBSYSTEM ),      	//!< This event from the Stim Director to any Stim subsystem that has a an active stimulation running.  Indicates that any active stimulation should be stopped by the Stim subsystem.
DEFINE_EVENT_ID( E_SW_TIMER_TIMEOUT ),
DEFINE_EVENT_ID( E_STATE_ENTER ),				//!< This event is used to handle entry to a new state on transition.
DEFINE_EVENT_ID( E_STATE_EXIT ),				//!< This event is used to handle exit of an old state on transition.
DEFINE_EVENT_ID( E_MICS_INT_LINK_READY ),		//!< Indicates the link_ready interrupt.
DEFINE_EVENT_ID( E_MICS_INT_CLOST ),			//!< Indicates that the MICS session was lost.
DEFINE_EVENT_ID( E_MICS_INT_RX_NOT_EMPTY ),		//!< Indicates that the MICS data receive buffer is not empty.
DEFINE_EVENT_ID( E_MICS_INT_RADIO_READY ),		//!< Indicates that the radio_ready interrupt was received.
DEFINE_EVENT_ID( E_MICS_CONNECTION_TIMEOUT ), //!< Indicates that the SWTMR_MICS_CONNECTION timer has expired.
DEFINE_EVENT_ID( E_MICS_INT_RX_OVERFLOW ),		//!< Indicates that the receive buffer overflowed, and the MICS chip is resetting.
DEFINE_EVENT_ID( E_MICS_FULL_RESET ),			//!< Indicates that the MICS chip has been reset and needs to be re-initialized.
DEFINE_EVENT_ID( E_SWTIMER_REARM ),
DEFINE_EVENT_ID( E_SWTIMER_TOGGLETEST ),
DEFINE_EVENT_ID(E_MICS_PERFORM_DIAGNOSTICS),	//!< Begins a MICS diagnostics operation.
DEFINE_EVENT_ID(E_MICS_PERFORM_DIAGNOSTICS_RUN), 	//!< Indicates that a MICS diagnostic mode requires action.
DEFINE_EVENT_ID(E_SWTMR_MICS_TRIM_COPY_EXPIRED), //!< Signals that the SWTMR_MICS_TRIM_COPY timer has expired.
DEFINE_EVENT_ID(E_CALIBRATE_VLO),
DEFINE_EVENT_ID(E_POLL_LINK_ERRORS),            //!< Perform link quality monitoring poll during a session.
// Note: The last line of the list must have a trailing comma.

