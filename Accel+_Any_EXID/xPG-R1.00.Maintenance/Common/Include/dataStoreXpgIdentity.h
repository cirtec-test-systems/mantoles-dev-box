/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Header file for XPG Identity structure.
 *
 ***************************************************************************/

#ifndef DATASTOREXPGIDENTITY_H_
#define DATASTOREXPGIDENTITY_H_

#include "Common/Protocol/cmndSetXpgIdent.h"
#include "Common/Protocol/cmndCommon.h"

/**
 * This first structure is to include the firmware version
 * that the bootloader expects to be in the data structure
 * stored in the data store.
 */
typedef struct DS_XPG_IDENTITY {
	XPG_IDENTITY xpgIdentity;
	char firmwareVersion[FIRMWARE_VERSION_CHARS];
} DS_XPG_IDENTITY;


#endif /* DATASTOREXPGIDENTITY_H_ */
