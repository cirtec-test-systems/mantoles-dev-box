/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Define the active error and charging error codes  
 *	
 ***************************************************************************/

#ifndef ERROR_CODE_H_
#define ERROR_CODE_H_

#define MAX_CAT1_ERROR 30
#define MAX_CAT2_ERROR 50
#define MAX_CAT3_ERROR 100

typedef enum ACTIVE_ERROR_CODE {
    ACTERR_NONE = 0,

    /// category 1 errors 1 thru 30
    ACTERR_CRITICAL_NV_DATA_CORRUPTED = 1,
    ACTERR_CLOCK_ERROR_MAJOR = 2,
    ACTERR_POWER_ASIC_ERROR_STIM_DISABLED = 3,
    ACTERR_DEPRECATED_RESERVED_04 = 4,
    ACTERR_XPG_IDENTITY_PARAMS_CORRUPTED = 5,
    ACTERR_MICS_TRANSCEIVER_ERROR = 6,
    ACTERR_POWER_ASIC_ANALOG_SENSE_ERROR = 7,
    ACTERR_BACKGROUND_IMPEDANCE_HW_ERROR = 8,
    ACTERR_CRITICAL_NV_HARDWARE_ERROR = 9,
    ACTERR_PULSE_CYCLE_ERROR = 10,

    ACTERR_INTERNAL_SW_ERROR_CAT1 = 30,

    /// category 2 errors 31 thru 50
    ACTERR_DEPRECATED_RESERVED_31 = 31,
    ACTERR_PROGRAM_DEFINITION_CORRUPTED = 32,
    ACTERR_OUTPUT_CH_CAP_FAILURE_PROGRAMS_DISABLED = 33,
    ACTERR_CH_IMPEDANCE_FAILURE_PROGRAMS_DISABLED = 34,
    ACTERR_CH_OUTPUT_CURRENT_FAILURE_PROGRAMS_DISABLED = 35,

    /// category 3 errors 51 thru 90
    ACTERR_PROGRAM_ACTIVE_SETTINGS_CORRUPTED = 51,
    ACTERR_DEPRECATED_RESERVED_52 = 52,
    ACTERR_STIM_ASIC_RUN_ERROR_PULSE_GUARD = 53,
    ACTERR_STIM_ASIC_RUN_ERROR_NON_PULSE_GUARD = 54,
    ACTERR_STIM_ASIC_READ_BACK_ERROR = 55,
    ACTERR_DEPRECATED_RESERVED_56 = 56,
    ACTERR_DEPRECATED_RESERVED_57 = 57,
    ACTERR_BROWN_OUT_DETECTED = 58,
    ACTERR_WATCHDOG_ERROR = 59,
    ACTERR_DEPRECATED_RESERVED_60 = 60,

    ACTERR_INTERNAL_SW_ERROR_CAT3 = 90,

    //special code for user action event
    ACTERR_STIM_SHUT_DOWN_WITH_EXTERNAL_MAGNET = 100
} ACTIVE_ERROR_CODE;

///
/// Charging errors
///
typedef enum CHARGE_ERROR_CODE {
    CHERR_NONE                                    = 0x00,
    CHERR_STOP_PRE_CHARGE_PHASE_INSUFF_V_INCREASE = 0x01,
    CHERR_STOP_PRE_CHARGE_PHASE_TIMEOUT 		  = 0x02,
    CHERR_STOP_CONST_I_PHASE_INSUFF_V_INCREASE    = 0x03,
    CHERR_STOP_CONST_I_PHASE_TIMEOUT              = 0x04,
    CHERR_STOP_CONST_V_PHASE_TIMEOUT              = 0x05,
    CHERR_STOP_TEMP_LIMIT_EXCEEDED                = 0x06,
    CHERR_THERMISTOR_ERROR                        = 0x07,
    CHERR_CHARGE_STATE_ERROR                      = 0x08,
    CHERR_CHARGE_NOT_ALLOWED                      = 0x09,
    CHERR_CHARGE_OVERVOLTAGE_ERROR				  = 0x0A
} CHARGE_ERROR_CODE;

#define ERROR_RETURN_VALUE -1
#define OK_RETURN_VALUE 0

#endif                          /*ERROR_CODE_H_ */
