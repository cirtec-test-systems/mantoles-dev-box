/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Log event definitions and their associated data.
 *
 ***************************************************************************/

#ifndef LOG_EVENTS_H
#define LOG_EVENTS_H

//!Log Event IDs that can be stored in log event records in the ERROR event log.
typedef enum LOG_EVENT_ID {
    E_LOG_ERR_NO_EVENT = 0,								//!< The null event.

    E_LOG_ERR_UNINTENDED_RESET = 1001,					//!< An unintended reset occurred.
    E_LOG_ERR_WATCHDOG_RESET = 1002,					//!< A watchdog reset occurred.

    E_LOG_ERR_VOLTAGE_CHECK = 1003,						//!< An error was detected during a voltage check.
    E_LOG_ERR_TEMPERATURE_CHECK = 1004,					//!< An error was detected during a temperature check.
    E_LOG_ERR_CLOCK_VERIFICATION = 1005,				//!< A timing error was detected during clock verification.
    E_LOG_ERR_PULSE_CYCLE_CHECK = 1006,					//!< A timing error was detected during pulse cycle verification

    E_LOG_ERR_STIM_DATA_READBACK = 1010,				//!< A Stimulation Data Read-back error was detected.
    E_LOG_ERR_STIM_PROGRAM_CORRUPTED = 1011,			//!< The diagnostic memory check detected corruption of the Stimulation Program Definition.
    E_LOG_ERR_STIM_ACTIVE_SETTINGS_CORRUPTED = 1012,	//!< The diagnostic memory check detected corruption of the Active Stimulation Settings.
    E_LOG_ERR_MICS_ID_CORRUPTED = 1013,					//!< The diagnostic memory check detected corruption of the MICS identity information.
    E_LOG_ERR_PROGRAM_CONSTANTS_CORRUPTED = 1014,		//!< The diagnostic memory check detected corruption of the Program Constants.
    E_LOG_ERR_PULSE_CONSTANTS_CORRUPTED = 1015,			//!< The diagnostic memory check detected corruption of the Pulse Constants.
    E_LOG_ERR_CHANNEL_CALIBRATION_CORRUPTED = 1016,		//!< The diagnostic memory check detected corruption of the Channel Calibration.
    E_LOG_ERR_LEAD_INFORMATION_CORRUPTED = 1017,		//!< The diagnostic memory check detected corruption of the Lead Information.
    E_LOG_ERR_CONFIG_DEVICE_PARAMS_CORRUPTED = 1018,	//!< The diagnostic memory check detected corruption of the Configurable Device Parameters.
    E_LOG_ERR_GENERAL_CALIBRATION_CORRUPTED = 1019,		//!< The diagnostic memory check detected corruption of the General Calibration Data.
    E_LOG_ERR_SATURN_TRIM_LIST_CORRUPTED = 1020,		//!< The Stimulation System trim list is corrupted.
    E_LOG_ERR_PLUTO_TRIM_LIST_CORRUPTED = 1021,			//!< The Power System trim list is corrupted.
    E_LOG_ERR_ZARLINK_TRIM_LIST_CORRUPTED = 1022,		//!< The Radio trim list is corrupted.
    E_LOG_ERR_PROGRAM_DATA_INTEGRITY_ERROR = 1023,		//!< The Program Data Integrity check failed
    E_LOG_ERR_HV_CALIBRATION_CORRUPTED = 1024,			//!< The diagnostic memory check detected corruption of the High Voltage Calibration
    E_LOG_ERR_BACKGROUND_IMP_PARAMS_CORRUPTED = 1025,	//!< The diagnostic memory check detected corruption of the Background Impedance Parameters
    E_LOG_ERR_RAMP_TIME_CORRUPTED = 1026,				//!< The diagnostic memory check detected corruption of the Ramp Time

    E_LOG_ERR_BROWNOUT = 1030,							//!< A Brownout was detected during Stimulation.

    E_LOG_ERR_PULSE_GUARD_TRIPPED = 1040,				//!< The pulse guard tripped.
    E_LOG_ERR_STIM_PARITY_ERROR = 1041,					//!< A stimulation system parity error was detected.
    E_LOG_ERR_STIM_SEQUENCER_ERROR = 1042,				//!< A stimulation system sequencing error
    E_LOG_SATURN_ADDRESS_ERROR = 1043, 					//!< A stimulation system address error
    E_LOG_STIM_REGISTER_VALID = 1044,					//!< A stimulation system register valid error
    E_LOG_STIM_WRITE_PROTECT_ERROR = 1045,				//!< A stimulation system write protect error
    E_LOG_STIM_SYNC_ERROR = 1046,						//!< Could not SYNC with the stimulation hardware to perform the operation
    E_LOG_STIM_STARTUP_ERROR = 1047,					//!< Could not start or update the stimulation program

    E_LOG_ERR_OPEN_CIRCUIT_CONDITION = 1050,			//!< The Impedance Measurement diagnostics detected an Open Circuit Condition.
    E_LOG_ERR_FAULTY_OUTPUT_CHANNEL = 1051,				//!< The Background Stimulation Output check detected a Faulty Channel.
    E_LOG_ERR_FAULTY_OUTPUT_CAPACITOR = 1052,			//!< The Output Capacitor check detected a Faulty Capacitor.
    E_LOG_ERR_BG_IMP_MEASUREMENT_FAILED = 1053,			//!< Background Impedance Measurement failed
    E_LOG_ERR_CHECK_DENSITIES_FAILED = 1054,			//!< The Check Densities routine failed
    E_LOG_ERR_STIM_POWER_UP_FAILED = 1055,				//!< The Stimulation hardware didn't power up or initialize correctly
    E_LOG_ERR_BASIC_OUTPUT_CHECK_FAILED = 1056,			//!< Basic Output Check failed

    E_LOG_ERR_THERMISTOR_FAULT = 1060,					//!< The Thermistor Diagnostic detects a fault in the temperature measurement circuit

    E_LOG_ERR_EVENT_LOG_CORRUPT = 1070,					//!< The Log Module detected a corrupt Event Log data structure in this Log
    E_LOG_ERR_STACK_EXCEEDED = 1071,					//!< The software has exceeded its stack limitations
    E_LOG_ERR_QUEUE_OVERFLOW = 1072,					//!< The software has detected a queue overflow
    E_LOG_ERR_QUEUE_CONTENTS = 1073,					//!< The contents of the queue when an error was encountered.

    E_LOG_NORMAL_UNKNOWN_ERROR = 1080,					//!< Unknown error has been published
    E_LOG_RESET_SYSTEM = 1081,							//!< System restarted after coming out of reset
    E_LOG_NMI = 1082,									//!< System restarted after receiving unknown interrupt
    E_LOG_UNKNOWN_RESET = 1083,							//!< System restarted for unknown reasons

    E_LOG_ERR_TEST_STIM_RANGE_CHECK_FAILED = 1090,		//!< Test stim range check failed
    E_LOG_ERR_RETRIEVE_PROGRAM_FAILED = 1091,			//!< Retrieving a program failed
    E_LOG_ERR_STIM_AUTO_WAVEFORM_FAILED = 1092,			//!< Stim auto-waveform failed
    E_LOG_ERR_STIM_COMPLIANCE_VOLT_EXCEEDS_MAX = 1093,	//!< Compliance voltage calculation indicates voltage higher than is available
    E_LOG_ERR_STIM_HV_BOOST_INIT_FAILED = 1094,			//!< Stim high voltage DAC initialization failed
    E_LOG_ERR_STIM_PERIOD_CHECK_FAILED = 1095,			//!< Stim period check failed
    E_LOG_ERR_STIM_PULSE_WIDTH_FREQ_CONFLICT = 1096,	//!< Stim calculated a pulse width - frequency conflict

    E_LOG_TIMESTAMP_LOG_LOW_BYTES = 2001,				//!< Embed timestamp low bytes in log
    E_LOG_REVIEW_MARK_LOG = 2002,						//!< Mark location in log
    E_LOG_POWER_UP = 2003,								//!< System is powered up or coming out of a reset
    E_LOG_CP_CONNECTED = 2004,							//!< CP has been connected to the system
    E_LOG_TIMESTAMP_LOG_HIGH_BYTES = 2005,				//!< Embed timestamp high bytes in log
    E_LOG_MICS_CONNECTION = 2006,						//!< MICS Connection established, data includes channel number

#ifdef VERBOSE_MICS_LOGGING
    E_LOG_MICS_DISCONNECT = 2007,						//!< MICS has been disconnected by the software
    E_LOG_MICS_SHUTDOWN = 2008,							//!< MICS has been shutdown
    E_LOG_MICS_ABORT = 2009,							//!< MICS transmit/receive has been aborted
    E_LOG_MICS_INITIALIZED = 2010,						//!< MICS driver has been initialized
    E_LOG_MICS_CONNECTION_LOST = 2011,					//!< MICS connection has been lost
    E_LOG_MICS_RADIO_FAILURE = 2012,					//!< MICS radio failure
    E_LOG_MICS_RADIO_READY = 2013,						//!< MICS radio is ready
    E_LOG_MICS_LINK_READY = 2014,						//!< MICS link is ready
    E_LOG_MICS_MAC_CMND_TIMEOUT = 2015,					//!< MICS MAC Command Timeout
    E_LOG_MICS_WATCHDOG_TIMEOUT = 2016,					//!< MICS Watchdog Timeout
    E_LOG_MICS_HK_ABORT_LINK = 2017,					//!< MICS Housekeeping Abort Link
#endif


    E_LOG_CHARGING_INITIATED = 2020,					//!< Charging of the battery has been initiated
    E_LOG_CHARGING_COMPLETED = 2021,					//!< Battery has been charged to full capacity
    E_LOG_CHARGING_INTERRUPTED = 2022,					//!< Battery charging has been interrupted prior to being fully charged
    E_LOG_CHARGER_DETECTED = 2023,						//!< Charger has been detected
    E_LOG_CHARGER_REMOVED = 2024,						//!< Charger has been removed
    E_LOG_CHARGING_ABORT_TEMP = 2025,					//!< Charging has been aborted due to high temperature
    E_LOG_CHARGING_ABORT_VOLTAGE = 2026,				//!< Charging has been aborted due to insufficient voltage rise
    E_LOG_CHARGING_ABORT_ERROR = 2027,					//!< Charging has been aborted due to error
    E_LOG_TETS_DETUNED = 2028,							//!< Charging has been disconnected from the system
    E_LOG_STIM_STOPPED_LOW_BATT = 2029,					//!< Stimulation was stopped due to low battery
    E_LOG_STIM_STOPPED_PWR_DOWN = 2030,					//!< Stimulation was stopped due to power down
    E_LOG_STIM_STOPPED_ERROR = 2031,					//!< Stimulation was stopped due to an error
    E_LOG_CHARGING_ABORT_OVER_VOLTAGE = 2032,			//!< Charging was stopped due to overvoltage error
    E_LOG_CHARGING_CRITICAL_TEMP = 2033,				//!< Charging was switched to C8 because of critical temperature
    E_LOG_SWITCH_CHARGING_MODE = 2034,					//!< Charging was switched to the mode specified

    E_LOG_STIM_STOPPED_CLOSE_SESSION = 2040,			//!< Stimulation was stopped to radio session closing
    E_LOG_PAIRING_CHANGED_MICS = 2041,					//!< Pairing information was changed via radio
    E_LOG_PAIRING_CHANGED_PPC = 2042,					//!< Pairing information was changed via charging system

    E_LOG_CAL_DATA_CORRUPTION_REPAIRED = 2050,			//!< Calibration data restored from backup
    E_LOG_CONST_DATA_CORRUPTION_REPAIRED = 2051,		//!< Program or Pulse constants data has been repaired
    E_LOG_HVCAL_DATA_CORRUPTION_REPAIRED = 2052,		//!< High Voltage Calibration data was restored from backup
    E_LOG_GENCAL_DATA_CORRUPTION_REPAIRED = 2053,		//!< General Calibration data was restored from backup
    E_LOG_XPGINFO_DATA_CORRUPTION_REPAIRED = 2054,		//!< System Information Data was restored from backup copy
    E_LOG_STIM_TRIM_LIST_REPAIRED = 2055,				//!< Stimulation configuration trim list data was restored from backup
    E_LOG_RADIO_TRIM_LIST_REPAIRED = 2056,				//!< Radio configuration trim list data was restored from backup
    E_LOG_DEFAULT_BACKROUND_IMPEDANCE_PARAMS = 2057,	//!< Default background impedance parameters have been restored
    E_LOG_POWER_TRIM_LIST_REPAIRED = 2058,				//!< Power configuration trim list data was restored from backup
    E_LOG_CDP_REPAIRED = 2059,							//!< Configurable device parameters was restored from backup
    E_LOG_RAMP_TIME_REPAIRED = 2060,					//!< Ramp time was restored from backup

    E_LOG_COMMAND_UNHANDLED = 2070,						//!< The command received was not supported
    E_LOG_PATIENT_STIM_STOPPED = 2071,					//!< Stimulation was stopped
    E_LOG_PATIENT_STIM_STARTED = 2072,					//!< Patient stimulation was started
    E_LOG_TITRATION_STARTED = 2073,						//!< Trial stimulation was started
    E_LOG_TEST_STIM_STARTED = 2074,						//!< Test stimulation was started
    E_LOG_STORAGE_MODE = 2075,							//!< System was commanded into storage mode
    E_LOG_SELECT_PROGRAM = 2076,						//!< Patient Program was selected
    E_LOG_INCR_PRGM_AMPLITUDE = 2077,					//!< Program amplitude was incremented
    E_LOG_SET_PULSE_AMPLITUDE = 2078,					//!< Pulse amplitude was incremented
    E_LOG_SET_PRGM_FREQUENCY = 2079,					//!< Program frequency was set
    E_LOG_RESTORE_PROGRAM_DEFAULTS = 2080,				//!< Active Program Settings were restored to default values
    E_LOG_RESET_XPG = 2081,								//!< System was commanded to reset
    E_LOG_IMPEDANCE_ON_DEMAND = 2082,					//!< Impedance measurement was requested
    E_LOG_CLEAR_LOG = 2083,								//!< Log was requested to be cleared
    E_LOG_TITRATION_PARAMS = 2084,						//!< Trial Stimulation parameters were modified
    E_LOG_SET_TEST_PARAMS = 2089,						//!< Test Stimulation parameters were modified
    E_LOG_DECR_PRGM_AMPLITUDE = 2090,					//!< Program amplitude was decremented

    E_LOG_MAGNET_STIM_STARTED = 2100,					//!< Patient stimulation was started via magnet
    E_LOG_MAGNET_STIM_STOPPED = 2101,					//!< Patient stimulation was stopped via magnet
    E_LOG_MAGNET_PWR_DOWN = 2102,						//!< System is about the powered down via magnet

    E_LOG_STIM_STOPPED_TETS = 2110,						//!< Stimulation is stopped via charging system command

    E_LOG_READ_ERROR_LOG = 2120,						//!< Error log was read
    E_LOG_READ_EVENT_LOG = 2121,						//!< Event log was read
    E_LOG_QUERY_PPC_CONSTANTS = 2122,					//!< PPC Constants were queried
    E_LOG_QUERY_PROGRAM_NAMES = 2123,					//!< Program names were queried
    E_LOG_QUERY_PROGRAM_CONSTANTS = 2124,				//!< Program constants were queried
    E_LOG_QUERY_XPG_IDENTITY = 2125,					//!< System Identity information was queried
    E_LOG_QUERY_GENERAL_CALIBRATION = 2126,				//!< General calibration parameters were queried
    E_LOG_QUERY_CURRENT_TO_VOLT_CNTR = 2127,			//!< Charging went from current mode to voltage mode counter was queried
    E_LOG_QUERY_CHARGE_COMPLETE_CNTR = 2128,			//!< Charging was done to completion counter was queried
    E_LOG_QUERY_STORAGE_MODE_CNTR = 2129,				//!< Storage mode counter was queried
    E_LOG_QUERY_CHANNEL_CALIBRATION = 2130,				//!< Channel calibration values were queried
    E_LOG_QUERY_HV_CALIBRATION = 2131,					//!< High Voltage calibration values were queried
    E_LOG_QUERY_TRIM_LISTS = 2132,						//!< Trim list values were queried
    E_LOG_QUERY_VBAT_DIAG = 2133,						//!< Battery diagnostic information was queried
    E_LOG_QUERY_BG_IMPEDANCE_DIAG = 2134,				//!< Background impedance diagnostic information was queried
    E_LOG_QUERY_OUTPUT_CAP_DIAG = 2135,					//!< Output capacitor check diagnostic information was queried
    E_LOG_QUERY_THERMISTOR_DIAG = 2136,					//!< Thermistor diagnostic information was queried
    E_LOG_QUERY_PROGRAM_DEF = 2137,						//!< Program definition was queried
    E_LOG_QUERY_PULSE_CONSTANTS = 2138,					//!< Pulse constants were queried
    E_LOG_QUERY_CDP = 2139,								//!< Configuration device parameters were queried
    E_LOG_QUERY_PULSE_WIDTHS = 2140,					//!< Pulse widths were queried
    E_LOG_QUERY_PRGM_FREQUENCY = 2141,					//!< Program frequency was queried
    E_LOG_QUERY_POP_CONSTANTS = 2142,					//!< POP Constants were queried
    E_LOG_QUERY_LEAD_LIMITS = 2143,						//!< Lead limits were queried
    E_LOG_QUERY_CP_DATA = 2144,							//!< Clinician Programmer data was queried
    E_LOG_QUERY_RAMP_TIME = 2145,						//!< Ramp Time was queried
    E_LOG_QUERY_TITRATION_STIM = 2146,					//!< Trial Stimulation parameters were queried
    E_LOG_QUERY_PRGM_RUN_TIME = 2147,					//!< Program run time was queried
    E_LOG_READ_MEMORY_REQUESTED = 2148,					//!< Direct memory read was performed
    E_LOG_QUERY_BG_IMPEDANCE_PARAMS = 2149,				//!< Background impedance parameters were queried

    E_LOG_SET_XPG_IDENTITY = 2201,						//!< System Identity Information changed
    E_LOG_SET_GENERAL_CALIBRATION = 2202,				//!< General calibration data changed
    E_LOG_RESET_CURRENT_TO_VOLT_CNTR = 2203,			//!< Charging mode change counter was reset
    E_LOG_RESET_CHARGE_COMPLETE_CNTR = 2204,			//!< Charging completed counter was reset
    E_LOG_RESET_STORAGE_MODE_CNTR = 2205,				//!< Storage mode counter was reset
    E_LOG_SET_CHANNEL_CALIBRATION = 2206,				//!< Channel calibration was changed
    E_LOG_SET_HV_CALIBRATION = 2207,					//!< High Voltage calibration was changed
    E_LOG_SET_STIM_TRIM_LIST = 2208,					//!< Stimulation configuration trim list was changed
    E_LOG_SET_RADIO_TRIM_LIST = 2209,					//!< Radio configuration trim list was changed
    E_LOG_SET_PROGRAM_DEFINITION = 2210,				//!< Program definition was changed
    E_LOG_SET_PROGRAM_CONSTANTS = 2211,					//!< Program constants were changed
    E_LOG_SET_PULSE_CONSTANTS = 2212,					//!< Pulse constants were changed
    E_LOG_SET_CDP = 2213,								//!< Config device parameters were changed
    E_LOG_SET_LEAD_LIMITS = 2214,						//!< Lead limits were changed
    E_LOG_SET_CP_DATA = 2215,							//!< Clinician Program data was changed
    E_LOG_SET_RAMP_TIME = 2216,							//!< Ramp Time was changed
    E_LOG_RESET_PROGRAM_TIME = 2217,					//!< Program Time was reset
    E_LOG_WRITE_MEMORY_REQUESTED = 2218,				//!< Direct write to memory was requested
    E_LOG_SET_BG_IMPEDANCE_PARAMS = 2219,				//!< Background Impedance Parameters were changed
    E_LOG_ACTIVE_ERROR = 2220,							//!< System Error was detected
    E_LOG_CHARGING_ERROR = 2221,						//!< Charging Error was detected
    E_LOG_SET_POWER_TRIM_LIST = 2222,					//!< Power configuration trim list was changed

    E_LOG_DIAGNOSTIC_VRECT_AND_TEMP = 2300,				//!< Diagnostic log of vrect and temperature during charging
    E_LOG_DIAGNOSTIC_TEMPERATURE = 2301,				//!< Diagnostic log of temperature data during charging
    E_LOG_DIAGNOSTIC_BATTERY_ADC = 2302,					//!< Diagnostic log of battery adc counts during charging

    E_LOG_MICS_CANTCLEARIRQ = 2303,						//!< The MICS ISR was unable to clear an interrupt bit.
    E_LOG_MICS_SPIFAIL = 2304,							//!< The MICS driver failed to write to a register via SPI. Data contains (lsb) read register value, written register value, register address (msb)

    E_LOG_VLO_CALIBRATION_FREQUENCY = 2305,				//!< The MSP430 VLO was calibrated and is running at the specified frequency

    E_LOG_SW_TIMER_RESET = 2306,						//!< The software timer caused the system to reset.

    E_LOG_MICS_TOOMUCH = 2307,							//!< More data than is allowed was received when in session.
    E_LOG_MICS_RSSI = 2308,								//!< RSSI Measurement Result

    E_LOG_SET_RADIO_FACTORY_TRIM_LIST = 2309,			//!< Set the factory trim list for MICS.

    E_LOG_SW_TIMER_POSTED_MISSED_EVENT = 2310,			//!< A missed expiration was detected.


#ifdef VERBOSE_MICS_LOGGING
    E_LOG_MICS_COMMAND_RECEIVED = 2400,					//!< A MICS Command was received, token is logged
    E_LOG_MICS_RESPONSE_SENT = 2401,					//!< A MICS Response was sent, token is logged
    E_LOG_MICS_PACKET_DUMPED = 2402,   					//!< A MICS Packet was dumped
    E_LOG_MICS_INVALID_EXID = 2403,						//!< A MICS Command was received from an unpaired programmer
    E_LOG_MICS_INVALID_TOKEN = 2404,					//!< A MICS Command with an invalid token was received
    E_LOG_MICS_CHECKSUM_FAILED = 2405,					//!< A MICS Command was received, but the checksum failed
    E_LOG_MICS_TIMEOUT = 2406,							//!< A MICSPAD Timeout occurred waiting for an intermediate block
    E_LOG_MICS_SESSION_DUMPED = 2407,					//!< A MICS Session was closed by the MICS PAD
#endif

#ifdef VERBOSE_TETS_LOGGING
   ,E_LOG_TETS_BYTE_RECEIVED = 2450,					//!< A byte was received over TETS, data has byte
   E_LOG_TETS_COMMAND_RECEIVED = 2451,					//!< A valid TETS command was received, data has command number
   E_LOG_TETS_RESPONSE_SENT = 2452,						//!< A response is being sent over TETS
   E_LOG_TETS_BYTE_SENT = 2453,							//!< A byte was sent over TETS, data has byte
   E_LOG_TETS_COMMAND_REJECTED = 2454,					//!< TETS command was rejected because message not formatted correctly
   E_LOG_TETS_TIMEOUT = 2455,							//!< A timeout occurred while sending or receiving data bytes
#endif

   E_LOG_DEBUG = 2500
    /*
     * NOTE: LOG IDs 3000-3999 are reserved for the CP
     */

} LOG_EVENT_ID ;


#endif /* LOG_EVENTS_H */
