/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2011 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: System event list
 *
 ***************************************************************************/

#ifndef SYSTEM_EVENTS_H
#define SYSTEM_EVENTS_H

#include <stdint.h>
#include "error_code.h"
#include "Common/Protocol/cmndStim.h"
#include "Common/Protocol/response_code.h"
#include "Common/Protocol/cmndTokens.h"
#include "Common/Protocol/protocol.h"
#include "Common/Protocol/cmndImpedance.h"
#include "Common/Protocol/cmndSetPrgmFreq.h"
#include "Common/Protocol/cmndSetPulseWidth.h"
#include "Common/Protocol/cmndSetPulseAmpl.h"
#include "Common/Protocol/cmndSetTestWaveform.h"
#include "Common/Protocol/cmndCalibrateChannel.h"
#include "Common/Protocol/cmndMicsDiagnostics.h"
#include "titrStim.h"
#include "testStim.h"
#include "MicsConnectionCommand.h"
#include "log_events.h"

//!Events handled by the system.
typedef enum EVENT_ID {

	//
	// Incorporate the list of event IDs as identifiers
	//
	// NOTE:
	//
	// If you want to add an event ID to the enum, please add it to system_event_list.inc.
	// Comments in that file will show you how to do it.
	//

#define DEFINE_EVENT_ID(NAME)	NAME
#include "system_event_list.inc"
#undef DEFINE_EVENT_ID	// Prevent redefinition errors should system_event_list.inc be included for another purpose in the same source unit.

	NUM_EVENTS          //!< A constant defining the number of events handled outside the main event loop.
} EVENT_ID;


///This type is for origin of commands
typedef enum CMD_ORIGIN {
    MICS,
    TETS
} CMD_ORIGIN;


///This structure used to send any response to a command
typedef struct DATA_E_CMD_RESPONSE {
    /// Indicates the response code
    RESPONSE responseCode;

    /// The command being responded to
    CMND_TOKEN commandToken;

    /// Indicates the source of the command
    CMD_ORIGIN commandSource;

    /// The size of the structure being handed back.
    uint16_t responseSize;
} DATA_E_CMD_REPONSE;

typedef struct LOG_EVENT_DATA {
	LOG_EVENT_ID logId;
	uint32_t data;
} LOG_EVENT_DATA;

///Defines the payload data types for an event
typedef union EVENT_DATA {
    uint8_t eTetsData;
    uint8_t pulseIndex;
    uint8_t selectedProgram;
    uint16_t eBatteryADCCounts;
    ACTIVE_ERROR_CODE eActiveErrorData;
    CHARGE_ERROR_CODE eChargeErrorData;
    DATA_E_CMD_REPONSE eCommandResponseData;
    enum STIM_STATE eStimMode;
    uint8_t micsDataBlock[MICS_BLOCK_SIZE];
    TITR_STIM const *titrParams;
    TEST_STIM const *testStim;
    SET_TEST_WAVEFORM_PARAMS const *waveformParams;
    IMPEDANCE_PARAMS const *impedanceParams;
    SET_PULSE_AMPL_PARAMS setPulseAmplParams;
    SET_PULSE_WIDTH_PARAMS setPulseWidthParams;
    SET_PRGM_FREQ_PARAMS setProgramFreqParams;
    LOG_EVENT_DATA logEventData;
    enum MICSCONN_TIMEOUT_PHASE micsConnectionTimeoutPhase;
    void *swTimerPointer;
    MICS_DIAGNOSTICS_PARAMS micsDiagnosticsParams;
} EVENT_DATA;

///Defines the event structure
typedef struct EVENT {
    EVENT_ID eventID;
    EVENT_DATA eventData;
} EVENT;


/**
 * Marks the start of code dispatchgen.tcl should scan for events to dispatch
 * to an event handler.
 *
 * DISPATCH_GEN_START and DISPATCH_GEN_END should surround state machine
 * dispatch tables so that dispatchgen.tcl can find the events used by that
 * state machine.
 *
 * The dispatchgen script does not parse C. Instead it scans for any tokens
 * matching system event names.  Therefore, there is no dependency on any
 * particular table format, and DISPATCH_GEN_START/END can even be used
 * around straight C code as well as around a table.  Any events named in
 * that code will be dispatched to the event handler named in
 * DISPATCH_GEN_START.
 *
 * The macro is used only as a marker to help dispatchgen.tcl scan for tokens
 * and does not compile into anything.
 *
 * \param EVENT_HANDLER_FUNCTION_NAME	The name of the event handler function
 * 										to which the events should be dispatched.
 */
#define DISPATCH_GEN_START(EVENT_HANDLER_FUNCTION_NAME)

#define DISPATCH_GEN_START_ONLY(EVENT_HANDLER_FUNCTION_NAME, CLASS)

/**
 * Marks the end of code dispatchgen.tcl should scan for events to dispatch
 * to an event handler.
 *
 * DISPATCH_GEN_START and DISPATCH_GEN_END should surround state machine
 * dispatch tables to that dispatchgen.tcl can find the events used by that
 * state machine.
 *
 * See #DISPATCH_GEN_START.
 */
#define DISPATCH_GEN_END

#endif                          /* SYSTEM_EVENTS_H */
