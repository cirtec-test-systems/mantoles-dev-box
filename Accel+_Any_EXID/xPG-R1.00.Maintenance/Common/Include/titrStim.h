/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Titration stimulation structure
 *	
 ***************************************************************************/
#ifndef TITRSTIM_H_
#define TITRSTIM_H_

#include <stdint.h>
#include "prgmDef.h"

/// Test stimulation pulse structure
typedef struct TITR_PULSE {
    uint16_t amplitude;				///< Stim phase amplitude in microamps

    uint16_t pulseWidth; 			///< Stim phase pulse width in microseconds

    int8_t channelPercentage[NUM_CHANNELS];  ///< Amplitude percentage for each channel, or UNCONTROLLED_SRC_SNK_CHAN.
} TITR_PULSE;

/// Test stimulation structure
typedef struct TITR_STIM {
	uint8_t frequencyIdx;			///< Index into program frequency table
	
	uint8_t numPulses;				///< Number of pulses in the program. 0 turns off stimulation.
	
	TITR_PULSE pulse[NUM_PULSES];
} TITR_STIM;


#endif                          /*TITRSTIM_H_ */
