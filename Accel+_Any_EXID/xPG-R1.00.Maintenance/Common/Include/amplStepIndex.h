/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Data structure and constants amplitude steps.
 *	
 ***************************************************************************/

#ifndef AMPLSTEPINDEX_H_
#define AMPLSTEPINDEX_H_

/// \defgroup amplStepIndex Amplitude Step Index (Virtual and Hardware Output) Constructs
/// 
///  
/// @{

/// default number of (hardware) output amplitude steps over which amplitudes can be incremented and decremented.
/// - NOTE: This value is used as the compile time default for an xPG operating constant which can be overwritten by an external device.
#define DEFAULT_AMPL_STEPS 50


/// (hardware) output pulse amplitude step index range
/// - These limits specify the step index value range for which an amplitude output value can be generated in hardware:\n
///		output = amplLowLimit + (stepSize * stepIndex)
/// - NOTE: The max value indicated here is a compile time default for illustration. \n
///		Runtime evaluation of the range requires replacing the DEFAULT_AMPL_STEPS value with the xPG device configuration constant "amplitudeSteps" as it can be overridden.
enum PULSE_OUTPUT_AMPL_STEP_LIMIT
{
	PULSE_OUTPUT_AMPL_STEP_MIN = 1,
	PULSE_OUTPUT_AMPL_STEP_MAX = DEFAULT_AMPL_STEPS
};


/// virtual pulse amplitude step index range
/// - These limits specify the range for virtual individual pulse step index values maintained internally by the xPG to allow determination of an overall program amplitude level indication.
/// - NOTE: The values indicated here are compile time defaults for illustration. \n
///		Runtime evaluation of the range requires replacing the DEFAULT_AMPL_STEPS value with the xPG device configuration constant "amplitudeSteps" as it can be overridden.
enum PULSE_VIRTUAL_AMPL_STEP_LIMIT
{
	PULSE_VIRTUAL_AMPL_STEP_MIN = 2 - DEFAULT_AMPL_STEPS,
	PULSE_VIRTUAL_AMPL_STEP_MAX = (DEFAULT_AMPL_STEPS * 2) - 1
};


/// individual pulse amplitude step index display range
/// - These limits specify the allowed display range of individual pulse amplitude step index values.
/// - NOTE: The max value indicated here is a compile time default for illustration. \n
///		Runtime evaluation of the range requires replacing the DEFAULT_AMPL_STEPS value with the xPG device configuration constant "amplitudeSteps" as it can be overridden.
enum PULSE_DISPLAY_AMPL_STEP_LIMIT
{
	PULSE_DISPLAY_AMPL_STEP_MIN = 1,
	PULSE_DISPLAY_AMPL_STEP_MAX = DEFAULT_AMPL_STEPS
};


/// program amplitude step index display range
/// - These limits specify the allowed display range of the prgram amplitude step index value.
/// - NOTE: The max value indicated here is a compile time default for illustration. \n
///		Runtime evaluation of the range requires replacing the DEFAULT_AMPL_STEPS value with the xPG device configuration constant "amplitudeSteps" as it can be overridden.
enum PROGRAM_DISPLAY_AMPL_STEP_LIMIT
{
	PROGRAM_DISPLAY_AMPL_STEP_MIN = 1,
	PROGRAM_DISPLAY_AMPL_STEP_MAX = (DEFAULT_AMPL_STEPS * 2) - 1
};


  
/// @}

#endif // AMPLSTEPINDEX_H_
 
 
 
/**********************************************************************
 * Revision History
 *
 * $Log$
 *
 **********************************************************************/
