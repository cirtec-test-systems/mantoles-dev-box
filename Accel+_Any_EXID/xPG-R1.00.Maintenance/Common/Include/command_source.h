/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Header file with definitions for who sent MICS commands.
 *	
 ***************************************************************************/

#ifndef COMMAND_SOURCE_H_
#define COMMAND_SOURCE_H_

/// command source as determined from EXID in message
/// The PPC, POP1, and POP2 values represent the order those fields appear in the
/// Get/Set Configurable Device Parameters commands.
typedef enum
{
  SOURCE_PPC = 0,	///< mics EXID matches paired PPC
  SOURCE_POP1 = 1,	///< mics EXID matches paired PoP1
  SOURCE_POP2 = 2,	///< mics EXID matches paired PoP2
  SOURCE_CP,		///< mics EXID indicates Clinician Programmer
  SOURCE_UNKNOWN	///< EXID not a CP and doesn't match
} COMMAND_SOURCE;

#endif /*COMMAND_SOURCE_H_*/
