/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: macros for cycle-counted delays
 *	
 ***************************************************************************/

#ifndef DELAY_H_
#define DELAY_H_

#include <intrinsics.h>

#define MCLK_MHZ		1			/* 8 MHz MCLK */

/*
 * Macros.
 * 
 * Determine how to handle macro definitions. If compiling tests
 * (TEST defined) and using this file for mocking (TEST_INLINE_FUNCTIONS is
 * not defined), then we need to emit prototypes for mocking insted of
 * macro definitions.
 * 
 * If we are not compiling tests (TEST not defined) or if the macros themselves
 * are being tested (TEST_INLINE_FUNCTIONS defined), then emit the actual
 * macros instead of the prototypes.
 */

#if defined(TEST) && !defined(TEST_INLINE_FUNCTIONS)

void DELAY_US(unsigned long T);
void DELAY_MS(unsigned long T);

#else

# define DELAY_US(T) _delay_cycles((unsigned long)(T) * (MCLK_MHZ))
# define DELAY_MS(T) _delay_cycles((unsigned long)(T) * (MCLK_MHZ) * 1000)

#endif

#endif /*DELAY_H_*/
