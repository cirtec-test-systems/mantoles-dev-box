/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Data structure and constants for program and pulse
 *                     definitions 
 * 
 ***************************************************************************/

#ifndef PRGMDEF_H_
#define PRGMDEF_H_

#include <stdint.h>
#include "amplStepIndex.h"

/// \defgroup prgmDef Program Definition
/// Detail of the structure and elements of program and pulse definitions
/// for Patient Stimulation
///  
/// @{

/// number of electrodes.
#define NUM_CHANNELS 26

/// number of program definitions allowed.
#define NUM_PRGM_DEFS 10

/// number of pulses allowed per program.
#define NUM_PULSES 4

/// number of waveforms permitted
/// currently, waveforms are held directly in Stim ASIC RAM. This number may change once real waveform support is added.
#define NUM_WAVEFORMS	2

/// allowed range of (hardware) output amplitude in integer microamps
typedef enum AMPL_LOW_LIMIT
{
	AMPL_LOW_LIMIT_MIN = 15,
	AMPL_LOW_LIMIT_MAX = 15000
} AMPL_LOW_LIMIT;

/// allowed range of (hardware) output amplitude in integer microamps
typedef enum BG_AMPL_LOW_LIMIT
{
	BG_AMPL_LOW_LIMIT_MIN = 1,
	BG_AMPL_LOW_LIMIT_MAX = 2
} BG_AMPL_LOW_LIMIT;

/// allowed output step amplitude in integer microamps
typedef enum AMPLITUDE_STEP_LIMIT
{
	AMPLITUDE_STEP_MIN = 15,
	AMPLITUDE_STEP_MAX = 1000
} AMPLITUDE_STEP_LIMIT;


/// allowed pulse width range in microseconds
typedef enum PULSE_WIDTH_LIMIT
{
	PULSE_WIDTH_MIN = 10, 
	PULSE_WIDTH_MAX = 1500
} PULSE_WIDTH_LIMIT;


/// allowed range of electrode output amplitude in integer percentage
typedef enum ELECTRODE_AMPL_PRCNT_LIMIT
{
	ELECTRODE_AMPL_PRCNT_MIN = -100,
	ELECTRODE_AMPL_PRCNT_MAX =  100
} ELECTRODE_AMPL_PRCNT_LIMIT;


/// number of characters alloted to pulse's name
/// - number of characters does not include null for string termination
#define PULSE_NAME_CHARS 16

/// \defgroup prgmFreq Program Frequency
/// 
///  
/// @{


/// allowed program frequency range in cycles per second
typedef enum PROGRAM_FREQUENCY_LIMIT
{
  PROGRAM_FREQUENCY_MIN =    2,
  PROGRAM_FREQUENCY_MAX = 2000
} PROGRAM_FREQUENCY_LIMIT;


/// total number of discrete program frequencies to select from.
#define MAX_FREQUENCY_SELECTIONS 64

/// calculated number of bytes req'd to hold Frequency Selection bits
/// - Must be computed from #MAX_FREQUENCY_SELECTIONS
#define PRGM_FREQ_BITMAP_BYTES		((MAX_FREQUENCY_SELECTIONS) / 8) 

/// @}


/// definition of an individual program pulse
typedef struct PULSE_DEF
{
	/// pulse's name to prompt understanding of its intended use.
	char pulseName[PULSE_NAME_CHARS];

	/// low limit for pulse amplitude in uA
	/// - value: see #_AMPL_LOW_LIMIT
	uint16_t amplitudeLowLimit;

	/// amplitude step size in uA
	/// - value: see #_AMPLITUDE_STEP_LIMIT
	uint16_t amplitudeStep;

	/// default amplitude step index for pulse
	/// - value: see #_PULSE_OUTPUT_AMPL_STEP_LIMIT
	int16_t  amplitudeStepIndex;               

    /// low limit for pulse width in usec
	/// - value: see #_PULSE_WIDTH_LIMIT
	uint16_t pulseWidthLowLimit;

	/// high limit for pulse width in usec
	/// - value: see #_PULSE_WIDTH_LIMIT
	uint16_t pulseWidthHighLimit;

	/// default pulse width for pulse
	/// - value: see #_PULSE_WIDTH_LIMIT
	uint16_t pulseWidth;

	/// signed percentage of current per electrode channel
	/// values: see #_ELECTRODE_AMPL_PRCNT_LIMIT
	int8_t electrodeAmpPercentage[NUM_CHANNELS];

} PULSE_DEF;


/// Number of characters alloted to program's name
/// This is just a bunch of bytes to the xPG. It does not care if it is NUL-terminated, 
/// space padded, or whatever.  The CP & PPC must agree on how it is formatted.
#define PRGM_NAME_CHARS 16


/**
 * definition of structure for complete program definition
 */
typedef struct PRGM_DEF
{
	/// Boolean indicating that the program definition is defined, complete, correct, and useable.
	uint8_t defined;

	/// Boolean indicating that the program definition is unuseable because of a non-definition issue.
	uint8_t disabled;

	/// program's name to prompt understanding of its intended use.
	char name[PRGM_NAME_CHARS];

	/// bitmap of the frequency table indexes indicating the frequencies this program can run at.
	uint8_t freqMap[PRGM_FREQ_BITMAP_BYTES]; 

	/// index into the program frequency table setting the default frequency the program should run at.
	/// - minimum 0
	/// - maximum (MAX_FREQUENCY_SELECTIONS - 1)
	uint8_t defaultFreq;

	/// bitmap of flags indicating which pulse definitions are to be used by this program. 
	/// - see #_PULSE_SLOT_BIT
	uint8_t pulseValid;

	/// individual pulse definitions for this program
	/// - see #_PULSE_DEF
	PULSE_DEF pulseDef[NUM_PULSES];
} PRGM_DEF;

/// Program number used, in the protocol, to indicate that no program is selected.
#define NULL_PROGRAM 0


/// @}

#endif /*PRGMDEF_H_*/
