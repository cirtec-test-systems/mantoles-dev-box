/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2011 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: This is the header file that contains the common
 *	(or shared) state machine data structure definitions.
 *
 ***************************************************************************/
#ifndef STATE_MACHINE_COMMON_H_
#define STATE_MACHINE_COMMON_H_

#include "system_events.h"

typedef struct STATE_MACHINE_TABLE_ENTRY
{
	int state;
	EVENT_ID eventId;
	void (*eventHandler)(EVENT eventToProcess);

} STATE_MACHINE_TABLE_ENTRY;


// Structure to hold handlers for the entry and exit conditions
// of each state in the State Machine
//
typedef struct STATE_MACHINE_ENTRY_EXIT_HANDLERS
{
	int state;
	void (*entryHandler)(void);
	void (*exitHandler)(void);
} STATE_MACHINE_ENTRY_EXIT_HANDLERS;

#endif /* STATE_MACHINE_COMMON_H_ */
