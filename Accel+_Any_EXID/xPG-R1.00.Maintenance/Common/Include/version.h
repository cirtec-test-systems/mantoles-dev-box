/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Header file with software version and MICS company ID.
 *	
 ***************************************************************************/

#ifndef VERSION_H_
#define VERSION_H_

#define FIRMWARE_VERSION	"R1.00.EX07"

/**
 * MICS company ID is assigned by Zarlink.
 * 
 * QiG Group's company ID is 187.
 */

#define DEFAULT_MICS_COMPANY_ID   187

#endif /*VERSION_H_*/
