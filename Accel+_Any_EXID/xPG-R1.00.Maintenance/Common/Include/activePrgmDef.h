/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *	
 *	@file Description: Data structure and constants for active program 
 *                     and active pulse definitions 
 * 
 ***************************************************************************/

#ifndef ACTIVE_PRGMDEF_H_
#define ACTIVE_PRGMDEF_H_

#include <stdint.h>
#include "prgmDef.h"
#include "error_code.h"

/// \defgroup activePgmDef Active Program Definitions
/// \ingroup activePgm
///
/// Detail of the structure and elements of Active Program and Pulse definitions.
///  
/// @{

/// definition of an individual active program pulse
typedef struct ACTIVE_PULSE_DEF
{
	/// the current active setting of the step index for the Pulse Amplitude.
	int16_t curAmplitudeStepIndex;

	/// the current virtual step index for the Pulse Amplitude
	/// this is used for the Inc/Dec Program Amplitude commands 
	int16_t curVirtualAmplitudeStepIndex;

	/// the current active setting of the Pulse Width
	uint16_t curPulseWidth;

} ACTIVE_PULSE_DEF;

/// Number of characters alloted to program's name
/// This is just a bunch of bytes to the xPG. It does not care if it is NUL-terminated, 
/// space padded, or whatever.  The CP & PPC must agree on how it is formatted.
#define PRGM_NAME_CHARS 16


/**
 * definition of structure for complete program definition
 */
typedef struct ACTIVE_PRGM_DEF
{
	/// the current active Frequency Index setting.
	uint8_t curFreqIndex;

	/// individual active pulse definitions for this program
	/// - see #_ACTIVE_PULSE_DEF
	ACTIVE_PULSE_DEF activePulseDef[NUM_PULSES];
} ACTIVE_PRGM_DEF;



/// @}

#endif /*ACTIVE_PRGMDEF_H_*/
