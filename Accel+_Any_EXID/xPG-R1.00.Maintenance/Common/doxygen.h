/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Manages the Doxygen-generated code documentation
 *					   This files is not involved in code generation.
 *	
 ***************************************************************************/

#ifndef DOXYGEN_H_
#define DOXYGEN_H_

/// \defgroup softwareUnits			Software Units

/// \defgroup deviceDriverLayer 	Device Driver Layer
/// \ingroup softwareUnits

/// \defgroup deviceDriverCommon 	Device Driver Common Code
/// \ingroup deviceDriverLayer

/// \defgroup deviceManagementLayer	Device Management Layer
/// \ingroup softwareUnits

/// \defgroup dataStore				Data Store
/// \ingroup softwareUnits

/// \defgroup dataStoreCommon		Data Store Common Code
/// \ingroup dataStore

/// \defgroup eventQueue			Event Queue
/// \ingroup softwareUnits

/// \defgroup eventDispatch			Event Dispatcher
/// \ingroup softwareUnits

/// \defgroup applicationLayer		Application Layer
/// \ingroup softwareUnits



#endif /* DOXYGEN_H_ */
