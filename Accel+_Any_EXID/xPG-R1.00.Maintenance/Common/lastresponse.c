/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Functions to make error handling easier
 *	
 ***************************************************************************/


#include "lastresponse.h"
#include "Common/Protocol/response_code.h"

static RESPONSE lastResponseCode;

// Public API. See header file for documentation.
RESPONSE getLastResponseCode(void)
{
	return lastResponseCode;
}

// Public API. See header file for documentation.
void setResponseCode(RESPONSE rc)
{
	lastResponseCode = rc;
}

// Public API. See header files for documentation.
void clearResponseCode(void)
{
	setResponseCode(GEN_SUCCESS);
}

