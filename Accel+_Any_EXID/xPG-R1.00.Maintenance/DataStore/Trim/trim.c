/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Manage the trim list data structures
 *	
 ***************************************************************************/

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "trim.h"
#include "mics.h"
#include "PowerASIC.h"
#include "Stim.h"
#include "EventQueue.h"
#include "Common/Protocol/cmndGetTrimList.h"
#include "Common/Protocol/cmndSetTrimList.h"
#include "Common/Protocol/response_code.h"
#include "Common/Protocol/trim_list.h"
#include "DataStore/Common/flashstore.h"


/**
 * Data type for the table of function pointers for processing the trim lists
 */

typedef struct ASIC {
	bool (*check)(uint16_t addr);
	void (*update)(bool valid, TRIM_LIST const *list);
} ASIC;

/**
 * Table of function pointers for processing the different trim lists.
 */

static ASIC asic[] = {
	{ pwrIsAddressValid,  NULL },				// TRIM_PLUTO = 0
	{ micsIsAddressValid, NULL },				// TRIM_ZL = 1
	{ stimIsValueValid, stimUpdateTrimList },	// TRIM_SATURN = 2
	{ micsIsAddressValid, NULL }				// TRIM_ZL_FACTORY = 3
};


bool trimIsListValid(enum TRIM_LIST_ID id)
{
	return flashIsTrimListValid(id);
}


bool trimIsListBackupValid(enum TRIM_LIST_ID id)
{
	return flashIsTrimListBackupValid(id);
}


RESPONSE trimSetList(const TRIM_LIST *list)
{
	enum TRIM_LIST_ID const id = (enum TRIM_LIST_ID)list->listID;
	RESPONSE result;
	bool valid;

	result = flashWriteTrimList(list);

	/*
	 * Confirm that the trim list was written correctly.
	 */

	valid = flashIsTrimListValid(id);

	if (!valid)
	{
		dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
	}

	/*
	 * Some device drivers (OK, one device driver) use the trim list at times
	 * other than startup and need to be notified when it is changed.
	 */

	if (asic[id].update)
	{
		asic[id].update(valid, flashTrimList(id));
	}

	return result;
}


RESPONSE trimSetPrimaryList(const TRIM_LIST *list)
{
	enum TRIM_LIST_ID const id = (enum TRIM_LIST_ID)list->listID;
	RESPONSE result;
	bool valid;

	result = flashWritePrimaryTrimList(list);

	/*
	 * Confirm that the trim list was written correctly.
	 */

	valid = flashIsTrimListValid(id);

	if (!valid)
	{
		dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
	}

	/*
	 * Some device drivers (OK, one device driver) use the trim list at times
	 * other than startup and need to be notified when it is changed.
	 */

	if (asic[id].update)
	{
		asic[id].update(valid, flashTrimList(id));
	}

	return result;
}

RESPONSE trimSetBackupList(const TRIM_LIST *list)
{
	enum TRIM_LIST_ID const id = (enum TRIM_LIST_ID)list->listID;
	RESPONSE result;
	bool valid;

	result = flashWriteBackupTrimList(list);

	/*
	 * Confirm that the trim list was written correctly.
	 */

	valid = flashIsTrimListBackupValid(id);

	if (!valid)
	{
		dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
	}

	return result;
}




TRIM_LIST const *trimGetList(enum TRIM_LIST_ID id)
{
	return flashTrimList(id);
}

TRIM_LIST const *trimGetListBackup(enum TRIM_LIST_ID id)
{
	return flashTrimListBackup(id);
}


bool trim_RangeCheckSetTrimList(SET_TRIM_LIST_PARAMS const * const p)
{
	uint16_t addr;
	int i;

	// listID: range 0 to (NUM_TRIM_LISTS - 1)
	if (p->listID >= NUM_TRIM_LISTS)
	{
		return false;
	}
	
	// listLen: range 0 to TRIM_LIST_ENTRIES
	if (p->listLen > TRIM_LIST_ENTRIES)
	{
		return false;
	}

	// trim list addresses: range is set by the ASIC in question
	for (i = 0; i < p->listLen * 3; i += 3)
	{
		addr = (p->trims[i+1] << 8) | p->trims[i];
		if (!asic[p->listID].check(addr))
		{
			return false;
		}
	}

	return true;
}

bool trim_RangeCheckSetSaturnTrimList(SET_TRIM_LIST_PARAMS const * const p)
{
	int i;

	if (p->listID != TRIM_SATURN)
	{
		return false;
	}

	// listLen: range 0 to TRIM_LIST_ENTRIES
	if (p->listLen > SATURN_TRIM_LIST_ENTRIES)
	{
		return false;
	}

	// trim list addresses: range is set by the ASIC in question
	for (i = 0; i < p->listLen; i++)
	{
		if (p->trims[i] == 0 || p->trims[i] == 0xFF)
		{
			return false;
		}
	}

	return true;
}



bool trim_RangeCheckGetTrimList(GET_TRIM_LIST_PARAMS const * const p)
{
	// listID: range 0 to (NUM_TRIM_LISTS - 1)
	return p->listID < NUM_TRIM_LISTS;
}

bool trim_RangeCheckGetSaturnTrimList(GET_TRIM_LIST_PARAMS const * const p)
{
	return p->listID == TRIM_SATURN;
}


int trimGetStimConstants(STIM_TRIM_VALS *stimTrimVals)
{
	const TRIM_LIST *stimTrimList = trimGetList(TRIM_SATURN);

	stimTrimVals->STIM_BG_TRIM = stimTrimList->trims[0];
	stimTrimVals->STIM_PGO_TRIM = stimTrimList->trims[1];
	stimTrimVals->STIM_CASN_TRIM = stimTrimList->trims[2];
	stimTrimVals->STIM_CASP_TRIM = stimTrimList->trims[3];
	stimTrimVals->STIM_HVDDL_TRIM = stimTrimList->trims[4];
	stimTrimVals->STIM_GLOBAL_CHAN_SCALE = ((stimTrimList->trims[5] << 8) | stimTrimList->trims[6]);

	return OK_RETURN_VALUE;
}


bool trimStimConstantsValid()
{
	return trimIsListValid(TRIM_SATURN);
}


int trimSetStimConstants(STIM_TRIM_VALS* stimTrimVals)
{
	TRIM_LIST stimTrimList;
	RESPONSE resp;

	stimTrimList.listID = TRIM_SATURN;
	stimTrimList.listLen = 5;

	stimTrimList.trims[0] = stimTrimVals->STIM_BG_TRIM;
	stimTrimList.trims[1] = stimTrimVals->STIM_PGO_TRIM;
	stimTrimList.trims[2] = stimTrimVals->STIM_CASN_TRIM;
	stimTrimList.trims[3] = stimTrimVals->STIM_CASP_TRIM;
	stimTrimList.trims[4] = stimTrimVals->STIM_HVDDL_TRIM;
	stimTrimList.trims[5] = (uint8_t)(stimTrimVals->STIM_GLOBAL_CHAN_SCALE >> 8);
	stimTrimList.trims[6] = (uint8_t)(stimTrimVals->STIM_GLOBAL_CHAN_SCALE & 0x00FF);

	resp = trimSetList(&stimTrimList);

	if (resp != GEN_SUCCESS)
	{
		return ERROR_RETURN_VALUE;
	}

	return OK_RETURN_VALUE;
}


bool trimSetTrimValue(TRIM_LIST *inMemoryCopy, uint8_t address1, uint8_t address2, uint8_t value)
{
	int i;

	//find the last instance of the address
	for(i=inMemoryCopy->listLen; i >= 0; i--)
	{
		if(inMemoryCopy->trims[i*3] == address1 && inMemoryCopy->trims[i*3+1] == address2)
		{
			break;
		}
	}

	if(i<0)
	{
		//The trim wasn't found. Add a new entry
		if(inMemoryCopy->listLen == TRIM_LIST_ENTRIES)
		{
			return false;
		}

		i=inMemoryCopy->listLen++;
		inMemoryCopy->trims[i*3] = address1;
		inMemoryCopy->trims[i*3+1] = address2;
	}

	//set the value.
	inMemoryCopy->trims[i*3+2] = value;

	return true;
}

