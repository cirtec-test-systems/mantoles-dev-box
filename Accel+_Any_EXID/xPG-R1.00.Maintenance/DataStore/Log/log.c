/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: Event log module.
 *
 ***************************************************************************/

#include "system_events.h"
#include "EventQueue.h"
#include "log.h"
#include "log_int.h"
#include "NV.h"
#include "MultiChannelTimer.h"

/**
 * Data structures for allocating space in the FRAM address space for
 * LOG_STATUS_T structures for each Event Log and the LOG_EVENT_RECORD
 * arrays for the two logs (error and action).
 *
 * \warning This is not a usable data structure in the MSP430 address space!
 * It overlaps other data structures.  Only use it to let the linker manage
 * the nv addresses for the nvCond_* and fram_* functions.  Do not try to
 * directly access this data!
 */
#pragma DATA_SECTION(nvLogStatus, "nv")
static const LOG_STATUS_T nvLogStatus[LOG_NUM_OF_EVENT_LOGS];

#pragma DATA_SECTION(eventErrorRecords, "nv")
static const LOG_EVENT_RECORD eventErrorRecords[LOG_NUM_OF_EVENTS_PER_ERROR_LOG];

#pragma DATA_SECTION(eventActionRecords, "nv")
static const LOG_EVENT_RECORD eventActionRecords[LOG_NUM_OF_EVENTS_PER_ACTION_LOG];


/**
 * Clear all the data from the event record log and initialize its data structures.
 *
 * \param logNum	The number of the log to clear and initialize.
 */
void logClear(uint8_t logNum)
{
	LOG_STATUS_T localLog;
	int status = OK_RETURN_VALUE;

	nvStart();	// power on the FRAM

	// call to event queue with NV RAM error if status is still non-zero (-1 equals nvWrite failed)
	if (status != OK_RETURN_VALUE)
	{
		EVENT errEvent;
		errEvent.eventID = E_ERR_ERROR_DETECTED ;
		errEvent.eventData.eActiveErrorData = ACTERR_CRITICAL_NV_HARDWARE_ERROR;
		dispPutEvent(errEvent);
	}

	// Set the initial values for the LOG_STATUS_T structure and store it in NV RAM
	localLog.topSerialNumber = 0;
	localLog.indexNextEvent = 0;

	status = nvWrite(NV_ADDR(nvLogStatus[logNum].topSerialNumber), (const void *)&localLog, sizeof(LOG_STATUS_T));

	// call to event queue with NV RAM error if status is still non-zero (-1 equals nvWrite failed)
	if (status != OK_RETURN_VALUE)
	{
		EVENT errEvent;
		errEvent.eventID = E_ERR_ERROR_DETECTED ;
		errEvent.eventData.eActiveErrorData = ACTERR_CRITICAL_NV_HARDWARE_ERROR;
		dispPutEvent(errEvent);
	}

	nvStop();	// power off the FRAM to conserve battery
}

/**
 * Returns the serial number of the first (i.e. oldest) event record in the log.
 *
 * \param logNum	The number of the log.
 */
uint32_t logGetFirstSerialNum(uint8_t logNum)
{
	LOG_STATUS_T localLog;
	uint16_t thisEventSize;

	thisEventSize = (LOG_ERROR_LOG_NUM == logNum) ? LOG_NUM_OF_EVENTS_PER_ERROR_LOG : LOG_NUM_OF_EVENTS_PER_ACTION_LOG;

	// read and validate a copy of the LOG_STATUS_T data structure into local memory
	logGetValidatedLogDataStructure(NV_ADDR(nvLogStatus[logNum]), &localLog, thisEventSize);

	return logCalcFirstSerialNum(&localLog, thisEventSize);
}

/**
 * Returns the serial number of the last (i.e. newest) event record in the log.
 *
 * \param logNum	The number of the log.
 */
uint32_t logGetLastSerialNum(uint8_t logNum)
{
	LOG_STATUS_T localLog;
	uint16_t thisEventSize;

	thisEventSize = (LOG_ERROR_LOG_NUM == logNum) ? LOG_NUM_OF_EVENTS_PER_ERROR_LOG : LOG_NUM_OF_EVENTS_PER_ACTION_LOG;

	// read a copy of the LOG_STATUS_T data structure into local memory
	logGetValidatedLogDataStructure(NV_ADDR(nvLogStatus[logNum]), &localLog, thisEventSize);

	return logCalcLastSerialNum(&localLog);
}


/**
 * Add an event record to the event log.
 *
 * \param logNum		The number of the log to add the event record to.
 * \param eventRecord	A pointer to the event record to add to the log.
 */
static void logAddEventRecord(uint8_t logNum, const LOG_EVENT_RECORD* eventRecord)
{
	LOG_STATUS_T localLog;
	int status;
	uint32_t thisEventLog;
	uint16_t thisEventSize;

	thisEventSize = (LOG_ERROR_LOG_NUM == logNum) ? LOG_NUM_OF_EVENTS_PER_ERROR_LOG : LOG_NUM_OF_EVENTS_PER_ACTION_LOG;

	//Power on FRAM for entire body of function
	nvStart();

	// read a copy of the LOG_STATUS_T data structure into local memory
	logGetValidatedLogDataStructureWithNVOn(NV_ADDR(nvLogStatus[logNum]), &localLog, thisEventSize);

	// store the given event record at the location pointed to
	// by the indexNext pointer
	//
	thisEventLog = (LOG_ERROR_LOG_NUM == logNum) ? NV_ADDR(eventErrorRecords[localLog.indexNextEvent]) : NV_ADDR(eventActionRecords[localLog.indexNextEvent]);
	status = nvWrite(thisEventLog, (const void *)eventRecord, sizeof(LOG_EVENT_RECORD));

	// call to event queue with NV RAM error if status is still non-zero (-1 equals nvWrite failed)
	if (status != OK_RETURN_VALUE)
	{
		EVENT errEvent;
		errEvent.eventID = E_ERR_ERROR_DETECTED ;
		errEvent.eventData.eActiveErrorData = ACTERR_CRITICAL_NV_HARDWARE_ERROR;
		dispPutEvent(errEvent);
	}

	localLog.indexNextEvent++;
	// if we are wrapping around in the event record buffer then indexNext = 0 and
	// add buffer size to topSerialNumber
	if (0 == (localLog.indexNextEvent % thisEventSize))
	{
		localLog.indexNextEvent = 0;
		localLog.topSerialNumber += thisEventSize;
	}

	status = nvWrite(NV_ADDR(nvLogStatus[logNum]), (const void *)&localLog, sizeof(LOG_STATUS_T));

	// call to event queue with NV RAM error if status is still non-zero (-1 equals nvWrite failed)
	if (status != OK_RETURN_VALUE)
	{
		EVENT errEvent;
		errEvent.eventID = E_ERR_ERROR_DETECTED ;
		errEvent.eventData.eActiveErrorData = ACTERR_CRITICAL_NV_HARDWARE_ERROR;
		dispPutEvent(errEvent);
	}

	nvStop();	// power off the FRAM to conserve battery
}

/**
 * Retrieves a buffer full of event records from the event log.  It will retrieve
 * up to 42 event records depending on how many event records are in the buffer and
 * which serial number is requested.  If the requested serial number is in the log it
 * will retrieve records from that serial number up to 42, or the end of the log, or the
 * size limit of the given buffer (whichever is the smallest).  If the serial number smaller
 * than any in the log it will retrieve event records from the first event in the log to 42,
 * or the end of the log, or the size limit of the given buffer (whichever is the smallest).
 * If the serial number requested is greater than any serial number in the log it will return
 * 0 event records.
 *
 * \param logNum			The number of the log to clear and initialize.
 * \param firstSerialNumToRead	The serial number of the first record requested.
 * \param eventRecordBuf	A buffer to store the event records in.  This should be big
 *							enough to hold 42 event records. If the buffer is not big
 *							enough to hold 42 event records then it will be filled with as
 *							many records as it can hold.
 * \param sizeOfBuf			The number of event records that eventRecordBuf can hold.
 * \param remainingEntriesCount The number of entries remaining in the event record buffer
 *							is returned in this variable.
 */
void logReadEventRecords(uint8_t logNum, uint32_t* firstSerialNumToRead, LOG_EVENT_RECORD* eventRecordBuf
						 , uint16_t* sizeOfBuf, uint16_t* remainingEntriesCount)
{
	LOG_STATUS_T localLog;
	uint32_t firstSerNumThisLog, lastSerNumThisLog;
	uint16_t numOfRecordsToRead, indexOfFirstRecToReturn, indexOfLastRecToReturn;
	uint16_t thisEventSize;
	uint32_t thisEventLog;

	thisEventSize = (LOG_ERROR_LOG_NUM == logNum) ? LOG_NUM_OF_EVENTS_PER_ERROR_LOG : LOG_NUM_OF_EVENTS_PER_ACTION_LOG;

	// read a copy of the LOG_STATUS_T data structure into local memory
	logGetValidatedLogDataStructure(NV_ADDR(nvLogStatus[logNum]), &localLog, thisEventSize);

	// Determine the first record to read.  Find it's serial number and index in the log.
	firstSerNumThisLog = logCalcFirstSerialNum(&localLog, thisEventSize);
	lastSerNumThisLog = logCalcLastSerialNum(&localLog);

	if (*firstSerialNumToRead > lastSerNumThisLog || firstSerNumThisLog == lastSerNumThisLog)
	{
		// don't read any records in the case just return and indicate
		// that there are no records in the return buffer
		*sizeOfBuf = 0;
		return;
	}

	// adjust the first serial number to read if necessary
	if (*firstSerialNumToRead < firstSerNumThisLog)
	{
		*firstSerialNumToRead = firstSerNumThisLog;
	}

	// Determine the max number of messages to read.
	// This is the smallest of: the number of the records that
	// will fit in the given eventRecordBuf or the start record
	// to the end of the log.
	numOfRecordsToRead = *sizeOfBuf;	// set to the size of the buffer passed in
	// check if there are enough records in the buffer to read
	if ( ((lastSerNumThisLog+1)-*firstSerialNumToRead) < numOfRecordsToRead )
	{
		numOfRecordsToRead = (lastSerNumThisLog+1)-*firstSerialNumToRead;
	}

	// Determine if the data we need to read wraps around in the buffer
	// i.e. can we read all the data at once or do we need to read it in 2
	// seperate chunks.
	// first calculate the index of the first and last records to return
	indexOfFirstRecToReturn = logGetIndexOfSerialNumber(&localLog, *firstSerialNumToRead, thisEventSize);
	indexOfLastRecToReturn = logGetIndexOfSerialNumber(&localLog, *firstSerialNumToRead+numOfRecordsToRead, thisEventSize);

	// getting ready to read the data from the FRAM
	nvStart();	// power on the FRAM
	if (indexOfFirstRecToReturn == indexOfLastRecToReturn)
	{
		// this should never happen at this point but if it does then it indicates that
		// we should not return any records.
		*sizeOfBuf = 0;
		nvStop();	// power off the FRAM to conserve battery
		return;
	}
	else if (indexOfFirstRecToReturn < indexOfLastRecToReturn)
	{
		//the event records we need are all in one block
		//read the data into the eventRecordBuf and return

		// read the event records into the buffer to return
		thisEventLog = (LOG_ERROR_LOG_NUM == logNum) ? NV_ADDR(eventErrorRecords[indexOfFirstRecToReturn]) : NV_ADDR(eventActionRecords[indexOfFirstRecToReturn]);
		nvRead(thisEventLog, (void *)eventRecordBuf, sizeof(LOG_EVENT_RECORD) * numOfRecordsToRead);
	}
	else
	{
		//the event records we need wrap around in the event record buffer
		//read the two sections into eventRecordBuf and return
		uint16_t numRecsInFirstSection, numRecsInSecondSection;

		numRecsInFirstSection = thisEventSize - indexOfFirstRecToReturn;
		numRecsInSecondSection = indexOfLastRecToReturn;

		// read the first section of the event records into the buffer to return
		thisEventLog = (LOG_ERROR_LOG_NUM == logNum) ? NV_ADDR(eventErrorRecords[indexOfFirstRecToReturn]) : NV_ADDR(eventActionRecords[indexOfFirstRecToReturn]);
		nvRead(thisEventLog, (void *)eventRecordBuf, sizeof(LOG_EVENT_RECORD) * numRecsInFirstSection);

		// read the second section of the event records into the buffer to return
		// this section will be at the top of the event record buffer so no need for the addition offset
		// to the starting address but we do need to use an extra offset for the location to write into
		// the eventRecordBuf
		thisEventLog = (LOG_ERROR_LOG_NUM == logNum) ? NV_ADDR(eventErrorRecords[0]) : NV_ADDR(eventActionRecords[0]);
		nvRead(thisEventLog, (void *)(eventRecordBuf + numRecsInFirstSection), sizeof(LOG_EVENT_RECORD) * numRecsInSecondSection);

	}
	nvStop();	// power off the FRAM to conserve battery (done reading from the FRAM)

	*sizeOfBuf = numOfRecordsToRead;
	//    remainingEntries = (last serial num in log) - (serial number of last record returned)
	*remainingEntriesCount = (lastSerNumThisLog) - (*firstSerialNumToRead + numOfRecordsToRead - 1);


}


/**
 * Write a single log entry to the NORMAL_LOG.
 *
 * The payload bytes are interpreted as a uint32_t.  To write other formats of data,
 * use #logAddEventRecord.
 *
 * This function is equivalent to setting up a LOG_EVENT_RECORD with the same parameters,
 * then calling #logAddEventRecord.
 *
 * \param logCode	The identifier of the new log entry.
 * \param data		The payload, if any, for the log entry. Should be 0 if there is no payload data.
 */
void logNormal(LOG_EVENT_ID logCode, uint32_t data)
{
	LOG_EVENT_RECORD eventRecord;

	eventRecord.logEventId = logCode;
	eventRecord.timeStamp = swTimerGetCurrentTime();

	*(uint32_t *)&eventRecord.logEventData = data;
	logAddEventRecord(LOG_NORMAL_LOG_NUM, &eventRecord);
}


/**
 * Write a single log entry to the ERROR_LOG.
 *
 * The payload bytes are interpreted as a uint32_t.  To write other formats of data,
 * use #logAddEventRecord.
 *
 * This function is equivalent to setting up a LOG_EVENT_RECORD with the same parameters,
 * then calling #logAddEventRecord.
 *
 * \param logCode	The identifier for the new log entry.
 * \param data		The payload, if any, for the log entry. Should be 0 if there is no payload data.
 */
void logError(LOG_EVENT_ID logCode, uint32_t data)
{
	LOG_EVENT_RECORD eventRecord;

	eventRecord.logEventId = logCode;
	eventRecord.timeStamp = swTimerGetCurrentTime();

	*(uint32_t *)&eventRecord.logEventData = data;

	logAddEventRecord(LOG_ERROR_LOG_NUM, &eventRecord);
}

#ifdef LOG_DEBUG
void logDebug(FILE_NUM filenum, uint32_t linenum,  uint8_t d1, uint8_t d2, uint8_t d3)
{
	LOG_EVENT_RECORD eventRecord;

	eventRecord.logEventId = E_LOG_DEBUG;
	eventRecord.timeStamp = linenum;
	eventRecord.logEventData[0] = (uint8_t)filenum;
	eventRecord.logEventData[1] = d1;
	eventRecord.logEventData[2] = d2;
	eventRecord.logEventData[3] = d3;

	logAddEventRecord(LOG_NORMAL_LOG_NUM, &eventRecord);
}

void logDebug24(FILE_NUM filenum, uint32_t linenum, uint32_t d)
{
	LOG_EVENT_RECORD eventRecord;

	eventRecord.logEventId = E_LOG_DEBUG;
	eventRecord.timeStamp = linenum | 0x80000000; 	// set MSB to indicate that we have a 24 bit value
	eventRecord.logEventData[0] = (uint8_t)filenum;
	eventRecord.logEventData[1] = d & 0x000000FF;
	eventRecord.logEventData[2] = (d & 0x0000FF00) >> 8;
	eventRecord.logEventData[3] = (d & 0x00FF0000) >> 16;

	logAddEventRecord(LOG_NORMAL_LOG_NUM, &eventRecord);
}

#endif




