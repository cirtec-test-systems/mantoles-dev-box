/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2011 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Event log module - internal definitions and functions.
 *
 ***************************************************************************/

#ifndef LOG_INT_H_
#define LOG_INT_H_

#include <stdint.h>
#include "log.h"

/// \defgroup logInt Log Internal Functions
/// \ingroup log
/// @{

#define LOG_NUM_OF_EVENT_LOGS 2
#define LOG_NUM_OF_EVENTS_PER_ERROR_LOG 512
#define LOG_NUM_OF_EVENTS_PER_ACTION_LOG 1400

// The data structure that keeps track of status of the event logs.  It keeps
// track of the serial number corresponding to the event record at the top of
// the log and the index that the next event record added will be stored at.
// This structure is stored in NV RAM right before the the array of event records.
typedef struct LOG_STATUS_T
{
    uint32_t topSerialNumber;
	uint16_t indexNextEvent;
} LOG_STATUS_T;




// function prototypes for internal helper functions
uint32_t logCalcFirstSerialNum(LOG_STATUS_T* logStruct, uint16_t nLogEntries);
uint32_t logCalcLastSerialNum(LOG_STATUS_T* logStruct);
uint16_t logGetIndexOfSerialNumber(LOG_STATUS_T* logStruct, uint32_t serialNumber, uint16_t nLogEntries);
void logGetValidatedLogDataStructure(uint16_t logBaseAddr, LOG_STATUS_T* logStruct, uint16_t nLogEntries);
void logGetValidatedLogDataStructureWithNVOn(uint16_t logBaseAddr, LOG_STATUS_T* logStruct, uint16_t nLogEntries);
/// @}

#endif /*LOG_INT_H_*/
