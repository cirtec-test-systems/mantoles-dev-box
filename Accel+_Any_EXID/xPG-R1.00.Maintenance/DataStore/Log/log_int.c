/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: Event log module.
 *
 ***************************************************************************/


#include "log.h"
#include "log_int.h"
#include "NV.h"
#include "log_events.h"
#include "MultiChannelTimer.h"

/**
 * Returns the serial number of the first (i.e. oldest) event record in the log.
 *
 * \param logStruct	A pointer to the LOG_T data structure for the log.
 * \param nLogEntries   The number of log entries in this particular log.
 */
uint32_t logCalcFirstSerialNum(LOG_STATUS_T* logStruct, uint16_t nLogEntries)
{
	// if the log records have not wrapped around at least once
	// then the first serial number and oldest record are 0 and at
	// the top of the buffer
	return (logStruct->topSerialNumber == 0)
				? (uint32_t)0
				: logStruct->topSerialNumber - nLogEntries + logStruct->indexNextEvent;

	// Pseudo code for the line above
	//if (logStruct->topSerialNumber == 0)
	//{
	//	// if the log records have not wrapped around at least once
	//	// then the first serial number and oldest record are 0 and at
	//	// the top of the buffer
	//	return (uint32_t)0;
	//}
	//else
	//{
	//	return logStruct->topSerialNumber - nLogEntries + logStruct->indexNextEvent;
	//}
}

/**
 * Returns the serial number of the last (i.e. most recent) event record in the log.
 *
 * \param logStruct	A pointer to the LOG_T data structure for the log.
 */
uint32_t logCalcLastSerialNum(LOG_STATUS_T* logStruct)
{
	// if the the log is empty. we'll have to just return zero
	return ((logStruct->topSerialNumber == 0) && (logStruct->indexNextEvent == 0))
					? (uint32_t)0
					: logStruct->topSerialNumber + logStruct->indexNextEvent - 1;

	//if ((logStruct->topSerialNumber == 0) && (logStruct->indexNextEvent == 0))
	//{
	//	// the the log is empty. we'll have to just return zero in this case
	//	return (uint32_t)0;
	//}
	//else
	//{
	//	return logStruct->topSerialNumber + logStruct->indexNextEvent - 1;
	//}
}

/**
 * Returns the index number of event record that corresponds to the given serial number.
 *   NOTE: this assumes the given serial number exists in the log
 *
 * \param logStruct		A pointer to the LOG_T data structure for the log.
 * \param serialNumber	The serial number to find the index for
 * \param nLogEntries   The number of log entries in this particular log.
 */
uint16_t logGetIndexOfSerialNumber(LOG_STATUS_T* logStruct, uint32_t serialNumber, uint16_t nLogEntries)
{
	return (serialNumber < logStruct->topSerialNumber)
				? serialNumber + nLogEntries - logStruct->topSerialNumber
				: serialNumber - logStruct->topSerialNumber;
}

/**
 * This read a copy of the LOG_T structure from NV RAM into local memory
 * and verifies the fields in it (to the best of its ability).
 * The only verification that can be done is to guarantee the indexNextEvent
 * value does not point past the bounds of the record event area.
 * If the indexNextEvent is invalid then a Corrupted Log event record will be
 * place in the log.
 *
 * \param logBaseAddr	The base address of the log get and validate data for.
 *						(since we don't have access to the base addresses here)
 * \param logStruct		A pointer to the LOG_T data structure for the log.
 * \param nLogEntries   The number of log entries in this particular log.
 *
 */
void logGetValidatedLogDataStructure(uint16_t logBaseAddr, LOG_STATUS_T* logStruct, uint16_t nLogEntries)
{
	nvStart();	// power on the FRAM
	nvRead(logBaseAddr, (void *)logStruct, sizeof(LOG_STATUS_T));

	// If the indexNextEvent is invalid then set it to zero
	//		some log event records might be lost but this is the
	//		best error handling we can do at this point
	if (logStruct->indexNextEvent >= nLogEntries)
	{
		LOG_EVENT_RECORD eventRecord;

		//This is the error correction but no need to do this since we will
		//change the value to 1 before we use it
		////logStruct->indexNextEvent = 0;

		// store a Log Corrupted event in this log
		eventRecord.timeStamp = swTimerGetCurrentTime();;
		eventRecord.logEventId = E_LOG_ERR_EVENT_LOG_CORRUPT;

		// since we know the indexNextEvent was just set to zero and because we want
		// to store this event in the first event record we can just hardcode the
		// additional offset to zero
		nvWrite( logBaseAddr + sizeof(LOG_STATUS_T), (const void *)&eventRecord, sizeof(LOG_EVENT_RECORD));
		logStruct->indexNextEvent = 1;
		// write the LOG_T structure to NV RAM
		nvWrite( logBaseAddr, (const void *)logStruct, sizeof(LOG_STATUS_T));
	}
	nvStop();	// power off the FRAM to conserve battery

	return;

}


/**
 * This read a copy of the LOG_T structure from NV RAM into local memory
 * and verifies the fields in it (to the best of its ability).
 * The only verification that can be done is to guarantee the indexNextEvent
 * value does not point past the bounds of the record event area.
 * If the indexNextEvent is invalid then a Corrupted Log event record will be
 * place in the log.
 *
 * \param logBaseAddr	The base address of the log get and validate data for.
 *						(since we don't have access to the base addresses here)
 * \param logStruct		A pointer to the LOG_T data structure for the log.
 * \param nLogEntries   The number of log entries in this particular log.
 *
 */
void logGetValidatedLogDataStructureWithNVOn(uint16_t logBaseAddr, LOG_STATUS_T* logStruct, uint16_t nLogEntries)
{
	nvRead(logBaseAddr, (void *)logStruct, sizeof(LOG_STATUS_T));

	// If the indexNextEvent is invalid then set it to zero
	//		some log event records might be lost but this is the
	//		best error handling we can do at this point
	if (logStruct->indexNextEvent >= nLogEntries)
	{
		LOG_EVENT_RECORD eventRecord;

		//This is the error correction but no need to do this since we will
		//change the value to 1 before we use it
		////logStruct->indexNextEvent = 0;

		// store a Log Corrupted event in this log
		eventRecord.timeStamp = swTimerGetCurrentTime();;
		eventRecord.logEventId = E_LOG_ERR_EVENT_LOG_CORRUPT;

		// since we know the indexNextEvent was just set to zero and because we want
		// to store this event in the first event record we can just hardcode the
		// additional offset to zero
		nvWrite( logBaseAddr + sizeof(LOG_STATUS_T), (const void *)&eventRecord, sizeof(LOG_EVENT_RECORD));
		logStruct->indexNextEvent = 1;
		// write the LOG_T structure to NV RAM
		nvWrite( logBaseAddr, (const void *)logStruct, sizeof(LOG_STATUS_T));
	}

	return;

}

