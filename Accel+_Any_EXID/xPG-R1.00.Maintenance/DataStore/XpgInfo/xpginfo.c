/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: xPG Information structure interface module.
 *	
 ***************************************************************************/

#include "xpginfo.h"
#include "DataStore/Common/flashstore.h"
#include "Common/Include/dataStoreXpgIdentity.h"
#include "EventQueue.h"
#include "error_code.h"

/*
 * Public API. See xpginfo.h for documentation.
 */

bool isXpgInfoValid(void)
{
	return flashIsXpgInfoValid();
}


/*
 * Public API. See xpginfo.h for documentation.
 */

bool isXpgInfoBackupValid(void)
{
	return flashIsXpgInfoBackupValid();
}


/*
 * Public API. See xpginfo.h for documentation.
 */

RESPONSE setXpgInfo(const DS_XPG_IDENTITY * pXpgIdentity)
{
	return flashWriteXpgInfo(pXpgIdentity);
}


/*
 * Public API. See xpginfo.h for documentation.
 */

RESPONSE setPrimaryXpgInfo(const DS_XPG_IDENTITY * pXpgIdentity)
{
	return flashWritePrimaryXpgInfo(pXpgIdentity);
}


/*
 * Public API. See xpginfo.h for documentation.
 */

const DS_XPG_IDENTITY *getXpgInfo(void)
{
	return flashXpgInfo();
}


/*
 * Public API. See xpginfo.h for documentation.
 */

const DS_XPG_IDENTITY *getXpgInfoBackup(void)
{
	return flashXpgInfoBackup();
}

/*
 * Public API. See xpginfo.h for documentation.
 */

int restorePrimaryXpgInfo(void)
{
	if (isXpgInfoBackupValid())
	{
		setPrimaryXpgInfo(getXpgInfoBackup());

		if (!isXpgInfoValid())
		{
			dispPutErrorEvent(ACTERR_XPG_IDENTITY_PARAMS_CORRUPTED);
			return ERROR_RETURN_VALUE;
		}
	}
	else
	{
		dispPutErrorEvent(ACTERR_XPG_IDENTITY_PARAMS_CORRUPTED);
		return ERROR_RETURN_VALUE;
	}

	return OK_RETURN_VALUE;
}
