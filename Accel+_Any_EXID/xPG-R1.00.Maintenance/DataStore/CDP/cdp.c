/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Defines and initializes data structure for the 
 *                     'Configurable Device Parameters', which mostly relate
 *                     to the patient's external controllers... the PoP(s) 
 *                     and PPC.
 *	
 ***************************************************************************/

#include <stddef.h>
#include <stdbool.h>
#include <string.h>

#include "cdp.h"

#include "Common/Protocol/cmndConfigDeviceParams.h"
#include "Common/Protocol/response_code.h"
#include "command_source.h"

#include "DataStore/Common/nvstore.h"

#include "EventQueue.h"

/**
 * defines storage and initialization for external config settings
 */

static CONFIG_DEVICE_PARAMETERS configDeviceParams;


/**
 * Initialize the configDeviceParams structure from NV memory
 */
 
int cdp_Init(void)
{
	if (nvReadConfigDeviceParams(&configDeviceParams) < 0)
	{
		/*
		 * If the CRC check failed, configDeviceParams probably has bogus
		 * data in it now.  Zero it out to prevent use of corrupted data.
		 *
		 * Note that the configDeviceParams is not given non-zero default
		 * values here, as there are no meaningful default values. The
		 * correct values for a particular use of an implant must be written
		 * by a Clinician Programmer.
		 *
		 * Also, the zeroed-out data is not written back to the NVRAM
		 * because that will attach a valid CRC to it, and on the next
		 * power-up, that zeroed-out data will be interpreted as being
		 * a valid CDP.  To avoid this, the zeroed-out data is kept only
		 * in the RAM cache, and the NVRAM copy is left bogus so that
		 * the IPG continues to know it has bad CDP data.
		 */
		memset(&configDeviceParams, 0, sizeof(configDeviceParams));

		dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);

#ifdef INITIALIZE_CDP
		{
			// Developers who don't have convenient tools to initialize this
			// structure may #define INITIALIZE_CDP in their builds to enable
			// this code.
			CONFIG_DEVICE_PARAMETERS cdp;

			cdp.amplitudeSteps = 50;
			cdp.pulseWidthStep = 10;
			cdp.ipgMagnetOptions = MAGNET_OPTION_ENABLE_STOP;
			cdp.replacementIntervalMonths = 60;
			cdp.ppcOptions = PPC_OPTION_INDV_PULSE_AMPL_MODS | PPC_OPTION_PULSE_WIDTH_MODS | PPC_OPTION_PRGM_FREQ_MODS;
			strncpy(cdp.ppcText, "DEFAULT CDP DATA USED", sizeof(cdp.ppcText));

			//Initialize Data Time to October 4, 2012
			cdp.implantDateTime[0] = 0x00;
			cdp.implantDateTime[1] = 0xae;
			cdp.implantDateTime[2] = 0x4d;
			cdp.implantDateTime[3] = 0xb2;
			cdp.implantDateTime[4] = 0x27;
			cdp.implantDateTime[5] = 0x70;
			cdp.implantDateTime[6] = 0xcf;
			cdp.implantDateTime[7] = 0x08;

			cdp.pairing[SOURCE_PPC].enabled = 1;
			cdp.pairing[SOURCE_PPC].exid.lsb = 0x67;
			cdp.pairing[SOURCE_PPC].exid.mid = 0x45;
			cdp.pairing[SOURCE_PPC].exid.msb = 0x03;

			cdp.pairing[SOURCE_POP1].enabled = 0;
			cdp.pairing[SOURCE_POP1].exid.lsb = 0x00;
			cdp.pairing[SOURCE_POP1].exid.mid = 0x00;
			cdp.pairing[SOURCE_POP1].exid.msb = 0x00;

			cdp.pairing[SOURCE_POP2].enabled = 0;
			cdp.pairing[SOURCE_POP2].exid.lsb = 0x00;
			cdp.pairing[SOURCE_POP2].exid.mid = 0x00;
			cdp.pairing[SOURCE_POP2].exid.msb = 0x00;

			//Write to NVRAM
			nvWriteConfigDeviceParams(&cdp);

			//Initialize RAM Copy
			configDeviceParams = cdp;
		}
#endif
		return -1;
	}
	else
	{
		return 0;
	}
}


/**
 * Copies the CONFIG_DEVICE_PARAMETERS structure from NV memory into RAM.
 * Used to get at the little-used fields of the structure, particularly
 * the PPC options.  Frequently-used fields have their own accessor functions
 * that use data cached in RAM.
 * 
 * \return -1 if the NV memory read failed (such as from a bad CRC), 0 otherwise
 */

int cdp_Get(CONFIG_DEVICE_PARAMETERS *cdp)
{
	return nvReadConfigDeviceParams(cdp);
}


/**
 * Checks an exid to determine if it is a known device.
 * The three-byte value is checked to see if it is from a
 * PoP (PoP1 or PoP2), or from a PPC, or from a CP. 
 *  
 * \param exid	A three-byte array containing the EXID to match.
 * 
 * \return A value from the COMMAND_SOURCE enum indicating a match or
 * SOURCE_UNKNOWN if there is no match.
 */

COMMAND_SOURCE cdp_IdentifyExid(const EXID_BYTES *exid)
{

/** For the "Any EXID" testing configuration, we skip the normal EXID checks and return "SOURCE_CP" for all EXIDs:
 *
 *     int i;
 *
 *	// If MSB is 0xff, then this is a CP.
 *	if (exid->msb == 0xff)
 *	{
 *		return SOURCE_CP;
 *	}
 *
 *	// Otherwise, check the paired devices
 *	for (i = 0; i < NUM_PAIRINGS; i++)
 *	{
 *		if (configDeviceParams.pairing[i].enabled && memcmp(exid, &configDeviceParams.pairing[i].exid, 3) == 0)
 *		{
 *			return (COMMAND_SOURCE)i;
 *		}
 *	}
 *
 *  // The exid is neither a CP nor a paired device.
 *  return SOURCE_UNKNOWN;
 *
 **/

    // Uncomment the block above and remove the following line to restore normal EXID checks
    return SOURCE_CP;

}


/**
 * Gets the Amplitude Steps Setting from the external
 * configuration data block
 * 
 * \return Amplitude Steps setting
 */

uint8_t cdp_GetAmplitudeSteps(void)
{
    return configDeviceParams.amplitudeSteps;
}

/**
 * Gets the Pulse Width Step Setting from the external
 * configuration data block
 * 
 * 
 * \return Pulse width step setting
 */

uint16_t cdp_GetPulseWidthStep(void)
{
    return configDeviceParams.pulseWidthStep;
}


/**
 * Gets the magnet options from the external
 * configuration data block
 *
 *
 * \return Magnet options
 */

uint8_t cdp_GetMagnetOptions(void)
{
	return configDeviceParams.ipgMagnetOptions;
}

/**
 * This function processes the GET CONFIGURABLE DEVICE
 * PARAMETERS command.
 *
 * \param param	  Pointer to parameter buffer; NULL for 
 * \param resp Pointer to first byte of response buffer
 * \param source	  Code indicating the device that sent the command
 *  
 * \return If param is NULL, returns the the number of blocks in the command.
 * 		   Otherwise, returns the number of bytes in the response.  
 */

RESPONSE cdp_GetConfigDeviceParams(CONFIG_DEVICE_PARAMETERS *resp)
{
	/*
	 * Retrieve the data block from nonvolatile memory.
	 */
	 
	if (nvReadConfigDeviceParams(resp) < 0)
	{ 
        // Data was corrupted, so return an error code.
        return RESP_DATA_CORRUPTED;
    }

	// Data good, so send the response.
    return GEN_SUCCESS;
}

/**
 * This function retrieves the cdp backup parameters.
 *
 * \param param	  Pointer to parameter buffer; NULL for
 * \param resp 	  Pointer to first byte of response buffer
 *
 * \return Response code
 */

RESPONSE cdp_GetConfigDeviceParamsBackup(CONFIG_DEVICE_PARAMETERS *resp)
{
	/*
	 * Retrieve the data block from nonvolatile memory.
	 */

	if (nvReadConfigDeviceParamsBackup(resp) < 0)
	{
        // Data was corrupted, so return an error code.
        return RESP_DATA_CORRUPTED;
    }

	// Data good, so send the response.
    return GEN_SUCCESS;
}


/**
 * This function checks candidate configurable device
 * parameters sent with the SET CONFIG DEVICE PARAMS
 * command.
 *
 * Checks that the PoP/PPC enabled bytes are true or false.
 * Checks that MSB of PoP & PPC EXIDs are not 0xFF (reserved
 * for CPs).
 * Checks that pulse width step is from 1 and 200 usecs.
 * Checks that amplitudeStep number is from 10 to 100.
 * Checks PPCOptions byte.
 * Checks magnetOptions byte.
 * 
 * In does not check the implantation date.
 * 
 * It checks the replacement interval just to see that it's not 0
 * 
 * \param cdp pointer to a CONFIG_DEVICE_PARAMETERS structure.
 * 
 * \return true if parameters in range, false if parameters out of range
 */

bool cdp_RangeCheck(CONFIG_DEVICE_PARAMETERS const *cdp)
{
	int i;
	
	/*
	 * Check each pairing enable flag, which must be either 0 (false) or 1 (true).
	 */ 

	for (i = 0; i < NUM_PAIRINGS; i++)
	{
	    if (cdp->pairing[i].enabled != 0 && cdp->pairing[i].enabled != 1)
    	{
        	return false;
    	}
    }

	/*
	 * Any enabled exids must not be a CP exid.  In other words, its MSB can't be 0xff.
	 */

	for (i = 0; i < NUM_PAIRINGS; i++)
	{
	    if (cdp->pairing[i].enabled && cdp->pairing[i].exid.msb == 0xFF)
    	{
        	return false;
    	}
	}
    
    /*
     * Check the pulse width step and amplitude steps values
     */

    if (cdp->pulseWidthStep < PULSE_WIDTH_STEP_MIN || cdp->pulseWidthStep > PULSE_WIDTH_STEP_MAX)
    {
        return false;
    }
    if (cdp->amplitudeSteps < MIN_AMPL_STEPS_SETTING || cdp->amplitudeSteps > MAX_AMPL_STEPS_SETTING)
    {
        return false;
    }

    /*
     * Check that magnet options are in range.
     *   /// bit0: Magnet Sense Stop Feature Enable (1=Enable, 0=Disable)
  	 * 	 /// bit1-7: Reserved
     */

    if ((cdp->ipgMagnetOptions & ~MAGNET_OPTION_ENABLE_STOP) != 0)
    {
        return false;
    }

	/*
	 * There are no checks on...
	 *    ppcText or ppcOptions, because they are just passthrough to the PPC.
     *    fillerByte or fillerByte2, because they are, well, filler.
     *    implantDateTime, which is a 64-bit integer in a Microsoft date/time format. 
     *         The xPG has no need to know.
     * 	  replacementIntervalMonths, because this is just passthrough to the PPC and CP.
     */

    return true;
}


/**
 * This function processes the SET CONFIGURABLE DEVICE
 * PARAMS command.
 *
 * This command is only allowed via MICS from a CP.  It is not allowed while
 * stimulation is active, because of the save to flash.
 * 
 * \param param	  Pointer to parameter buffer; NULL for 
 * \param resp Pointer to first byte of response buffer
 * \param source	  Code indicating the device that sent the command
 *  
 * \return If param is NULL, returns the the number of blocks in the command.
 * 		   Otherwise, returns the number of bytes in the response.
 */


RESPONSE cdp_SetConfigDeviceParams(CONFIG_DEVICE_PARAMETERS const *p)
{
    if (nvWriteConfigDeviceParams(p) < 0)
    {
    	return RESP_WRITE_FAILED;
    }
    
    // If that worked, update our working copy and return success
    configDeviceParams = *p;
	return GEN_SUCCESS;
}


/**
 * This function processes the SET CONFIGURABLE DEVICE
 * PARAMS command.
 *
 * This command is only allowed via MICS from a CP.  It is not allowed while
 * stimulation is active, because of the save to flash.
 *
 * \param param	  Pointer to parameter buffer; NULL for
 * \param resp Pointer to first byte of response buffer
 * \param source	  Code indicating the device that sent the command
 *
 * \return If param is NULL, returns the the number of blocks in the command.
 * 		   Otherwise, returns the number of bytes in the response.
 */

RESPONSE cdp_SetPrimaryConfigDeviceParams(CONFIG_DEVICE_PARAMETERS const *p)
{
    if (nvWritePrimaryConfigDeviceParams(p) < 0)
    {
    	return RESP_WRITE_FAILED;
    }

    // If that worked, update our working copy and return success
    configDeviceParams = *p;
	return GEN_SUCCESS;
}


/**
 * Returns the paired EXID for a given source.
 * 
 * \param source	The source for which to retrieve the EXID
 * \return 			A pointer to the EXID if the source parameter is a paired source, NULL otherwise.
 */
 
const EXID_BYTES *cdp_GetExid(COMMAND_SOURCE source)
{
	return &configDeviceParams.pairing[source].exid;
}


/**
 * Sets a paired EXID.
 * 
 * \param source	The source for which to set the EXID
 * \param exid		Pointer to the new EXID
 * \param enabled	Boolean flag: Should this EXID be enabled?
 * 
 * \return			0 if successful, -1 otherwise.
 */
int cdp_SetExid(COMMAND_SOURCE source, const EXID_BYTES *exid, bool enabled)
{
	CONFIG_DEVICE_PARAMETERS cdp;

	cdp = configDeviceParams;
	cdp.pairing[source].exid = *exid;
	cdp.pairing[source].enabled = enabled;

    if (nvWriteConfigDeviceParams(&cdp) < 0)
    {
    	return -1;
    }

	configDeviceParams = cdp;
	return 0;
}


/**
 * Tests whether a given source is paired.
 * 
 * \param source	The source to check
 * \return 			true if pairing is enabled for this source, false otherwise
 */

bool cdp_IsExidEnabled(COMMAND_SOURCE source)
{
	return configDeviceParams.pairing[source].enabled;
}


