/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: pgm (Stimulation Program) internal function prototypes
 *	
 ***************************************************************************/

#ifndef PGM_INT_H_
#define PGM_INT_H_

#include <stdbool.h>
#include <stdint.h>

/// \addgroup pgm_int	Pgm Internal Functions
/// \ingroup pgm
/// @{

bool pgm_IsProgramNumberValid(uint8_t n);
uint16_t pgm_BuildProgramMap(void);
uint16_t pgm_BuildEnabledProgramMap(void);

/// @}

#endif /*PGM_INT_H_*/
