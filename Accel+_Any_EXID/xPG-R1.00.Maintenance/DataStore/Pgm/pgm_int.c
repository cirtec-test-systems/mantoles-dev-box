/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: pgm (Stimulation Program) internal functions
 *	
 ***************************************************************************/

#include <stdbool.h>

#include "pgm_int.h"
#include "DataStore/Common/nvstore.h"

/**
 * Verify that an externally-provided (1-based) program number is valid.
 * Internal program numbers are always 0-based.
 * 
 * \param n  External (1-based) program number
 * \return true if the program number is in range, false otherwise
 */

bool pgm_IsProgramNumberValid(uint8_t n)
{
	return (n >= 1) && (n <= NUM_PRGM_DEFS);
}




/**
 * Build program map. Bits 0 thru 9 are for patient programs 1-10
 * Each defined (valid) program gets its bit set, each undefined program
 * does not.
 */

uint16_t pgm_BuildProgramMap(void)
{
    PRGM_DEF pd;
    int i;
    uint16_t map;

    map = 0;
    for (i = 0; i < NUM_PRGM_DEFS; i++)
    {
        if (nvReadPrgmDef(i, &pd) == 0 && pd.defined)
        {
            map |= (1 << i);
        }
    }

    return map;
}


/**
 * Build enabled program map. Bits 0 thru 9 are for patient programs 1-10
 * Each enabled program gets its bit set, each disabled program does not.
 */

uint16_t pgm_BuildEnabledProgramMap(void)
{
    PRGM_DEF pd;
    int i;
    uint16_t enabledMap;

    enabledMap = 0;
    for (i = 0; i < NUM_PRGM_DEFS; i++)
    {
        if (nvReadPrgmDef(i, &pd) == 0 && pd.disabled == 0)
        {
            enabledMap |= (1 << i);
        }
    }

    return enabledMap;
}
