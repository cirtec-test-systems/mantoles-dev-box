/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: pgm (Stimulation Program) module
 *	
 ***************************************************************************/

#include <stddef.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include "pgm.h"
#include "pgm_int.h"

#include "prgmDef.h"
#include "error_code.h"
#include "activePgm.h"
#include "Common/Protocol/cmndCommon.h"
#include "Common/Protocol/cmndGetPulseConst.h"
#include "Common/Protocol/cmndSetPulseConst.h"
#include "Common/Protocol/cmndSlctPrgm.h"
#include "Common/Protocol/response_code.h"
#include "DataStore/Common/nvstore.h"
#include "DataStore/Common/crc16.h"
#include "EventQueue.h"
#include "lastresponse.h"
#include "system_events.h"
#include "log.h"
#include "counter.h"
#include "StimManager.h"
#include "SystemHealthMonitor.h"

/*
 * Permit the test suite access to these internal structures
 */
 
#ifndef TEST
#define STATIC		static
#else
#define STATIC
#endif


/**
 * Store the current (selected) program definition.
 * Set to a null program when no program is selected.
 */

PRGM_DEF pgm;

/**
 * Currently selected program number, 0 if none
 */
 
STATIC uint8_t pgmNum;

/**
 * Bitmap of defined programs.
 */

STATIC uint16_t pgmMap;

/**
 * Bitmap of enabled programs.
 */

STATIC uint16_t pgmEnabledMap;

/**
 * Initialize the program variables, including the RAM copies of the program and pulse constants.
 */

void pgm_Init(void)
{
	memset(&pgm, 0, sizeof(pgm));
	pgmNum = 0;

	pgmMap = pgm_BuildProgramMap();
	pgmEnabledMap = pgm_BuildEnabledProgramMap();
}


/**
 * Returns the current selected program as a PRG_DEF structure.
 * It is checked to make sure a current program is selected.
 * If it is, the requested program definition is returned. 
 * 
 * \param param	prgDef pointer Program Definition structure
 *	                   for return data
 * 
  * \returns	OK_RETURN_VALUE(0) for success, or
 *          ERROR_RETURN_VALUE(-1) if there is not a program
 *			selected (current Program = 0) or if the function
 *          nvReadPrgmDef fails.
 */
int pgm_GetCurInternalPrgmDef(PRGM_DEF *prgDef)
{
	if (0 == pgmNum)
	{
		setResponseCode(RESP_NO_PROGRAM_SELECTED);
		return ERROR_RETURN_VALUE;
	}

	if (nvReadPrgmDef(pgmNum-1, prgDef) < 0)
	{
		if (prgDef->defined)
		{
			EVENT errorEvent;

			//Store active error is disabled byte
			prgDef->disabled = ACTERR_PROGRAM_DEFINITION_CORRUPTED;

			//Log the error
			logError(E_LOG_ERR_STIM_PROGRAM_CORRUPTED, pgmNum-1);

			errorEvent.eventID = E_ERR_ERROR_DETECTED;
			errorEvent.eventData.eActiveErrorData = ACTERR_PROGRAM_DEFINITION_CORRUPTED;
			dispPutEvent(errorEvent);
		}

		setResponseCode(RESP_PROGRAM_NOT_VALID);
		return ERROR_RETURN_VALUE;
	}

	return OK_RETURN_VALUE;
}

/**
 * Process the Get Program Definition command.
 * 
 * The program number is sent with the command.
 * It is checked to make sure it is in range.
 * If it is, the requested program definition is returned. 
 * 
 * \param param	pointer to the parameters field
 * \param resp	pointer to first byte of response buffer
 * \param source identifies the origin of the command
 * 
 * \return number of bytes in response message
 */

RESPONSE pgm_GetPrgmDef(GET_PRGM_DEF_PARAMS const *p, GET_PRGM_DEF_RESPONSE_DATA *r)
{
	r->program = p->program;
	r->fillerByte = 0;
	if (nvReadPrgmDef(p->program - 1, &r->programDef) < 0)
	{
		//Store active error is disabled byte
		r->programDef.disabled = ACTERR_PROGRAM_DEFINITION_CORRUPTED;

		//Write it back disabled
		nvWritePrgmDef(p->program - 1, &r->programDef);

		// When we set a Program Definition - initialize the Active Program Settings
		actPgmSetInitialActiveSettings(p->program, &r->programDef);

		//Log the error
		logError(E_LOG_ERR_STIM_PROGRAM_CORRUPTED, p->program - 1);

		//Signal error event
		dispPutErrorEvent(ACTERR_PROGRAM_DEFINITION_CORRUPTED);

		return RESP_PROGRAM_DEF_CORRUPT;
	}

	return GEN_SUCCESS;
}


/**
 * Range check a Get Program Definition command
 */
 
bool pgm_RangeCheckGetPrgmDef(GET_PRGM_DEF_PARAMS const *p)
{
	return pgm_IsProgramNumberValid(p->program);
}



/**
 * Process the Set Program Definition command.
 * 
 * The program number and definition are sent with the command.
 * They are checked to make sure all elements are in range.
 * 
 * This command is only allowed via mics, from a CP.
 * 
 * \param param	pointer to the parameters field
 * \param resp	pointer to first byte of response buffer
 * \param source identifies the origin of the command
 * 
 * \return number of bytes in response message
 */

RESPONSE pgm_SetPrgmDef(SET_PRGM_DEF_PARAMS const *p)
{
	if (isStimOn())
	{
		return CMND_RESP_BUSY;
	}

	// Turn off the program map bit now in case the write corrupts the program
	pgmMap &= ~(1 << (p->program - 1));
	pgmEnabledMap &= ~(1 << (p->program - 1));
	
	if (nvWritePrgmDef(p->program - 1, &p->programDef) < 0)
	{
		return RESP_WRITE_FAILED;
	}

	// If the number of the program being set is the same as the currently selected
	// program then update RAM cache copy of the program definition
	if (pgmNum == p->program)
	{
		pgm = p->programDef;
	}

	// When we set a Program Definition - initialize the Active Program Settings
	if (actPgmSetInitialActiveSettings(p->program, &(p->programDef)) < 0)
	{
		//Return appropriate response when active settings are corrupted
		return RESP_WRITE_FAILED;
	}

	// and set the Cumulative Run-time counter to 0
	if (counterSet((COUNTER)(COUNTER_RUN_TIME_SECS_PRG_1 + p->program - 1), 0) < 0)
	{
		return RESP_WRITE_FAILED;
	}

	// Set the program map bit if the program just written is defined
	if (p->programDef.defined)
	{
		pgmMap |= 1 << (p->program - 1);
	}

	// Set the program map bit if the program just written is defined
	if (p->programDef.disabled == 0)
	{
		pgmEnabledMap |= 1 << (p->program - 1);
	}

	dispPutSimpleEvent(E_PRGM_DEF_CHANGED);

	return GEN_SUCCESS;
}


static bool checkElectrodeRange(const int8_t e[NUM_CHANNELS])
{
	int pos;
	int neg;
	int i;
	
	pos = 0;
	neg = 0;
	
	for (i = 0; i < NUM_CHANNELS; i++)
	{
		if (e[i] < ELECTRODE_AMPL_PRCNT_MIN || e[i] > ELECTRODE_AMPL_PRCNT_MAX)
		{
			return false;
		}

		if (e[i] > 0)
		{
			pos += e[i];
		}
		else
		{
			neg += e[i];
		}
	} 

	if (pos != 100 || neg != -100)
	{
		return false;
	}
	
	return true;
}

static bool pulseRangeCheck(const PULSE_DEF *p)
{
	if (p->amplitudeLowLimit < AMPL_LOW_LIMIT_MIN)
	{
		return false;
	}
	if (p->amplitudeLowLimit > AMPL_LOW_LIMIT_MAX)
	{
		return false;
	}
	
	if (p->amplitudeStep < AMPLITUDE_STEP_MIN)
	{
		return false;
	} 
	if (p->amplitudeStep > AMPLITUDE_STEP_MAX)
	{
		return false;
	}

	if (p->amplitudeStepIndex < PULSE_OUTPUT_AMPL_STEP_MIN)
	{
		return false;
	}
	if (p->amplitudeStepIndex > PULSE_OUTPUT_AMPL_STEP_MAX)
	{
		return false;
	}
 		
	if (p->pulseWidthLowLimit < PULSE_WIDTH_MIN)
	{
		return false;
	}
	if (p->pulseWidthLowLimit > PULSE_WIDTH_MAX)
	{
		return false;
	}
	
	if (p->pulseWidthHighLimit < PULSE_WIDTH_MIN)
	{
		return false;
	}
	if (p->pulseWidthHighLimit > PULSE_WIDTH_MAX)
	{
		return false;
	}
		
	if (p->pulseWidth < PULSE_WIDTH_MIN)
	{
		return false;
	}
	if (p->pulseWidth > PULSE_WIDTH_MAX)
	{
		return false;
	}
	
	if (p->pulseWidth < p->pulseWidthLowLimit)
	{
		return false;
	}
	if (p->pulseWidth > p->pulseWidthHighLimit)
	{
		return false;
	}

	return checkElectrodeRange(p->electrodeAmpPercentage);
}


/**
 * Check all fields in a SET PRGM DEF command for valid values.
 * 
 * \param param  Pointer to the Set Program Definition parameters 
 * \return true if all fields have valid values, or false otherwise.
 */
 
bool pgm_RangeCheckSetPrgmDef(const SET_PRGM_DEF_PARAMS *p)
{
	int i;
	uint16_t mapIndex = 0;
	uint8_t freqBit = 0;
	const PRGM_DEF *pd = &p->programDef;
	
	if (!pgm_IsProgramNumberValid(p->program))
	{
		return false;
	}

	switch (pd->defined)
	{
		case false:
		case true:
			break;
		default:
			return false;
	}

	switch (pd->disabled)
	{
		case false:
			break;
		case true:
		default:
			return false;
	}

	if (pd->defaultFreq >= MAX_FREQUENCY_SELECTIONS)
	{
		return false;
	}
	
	//Is the requested frequency index valid for the programs frequency map
	mapIndex = pd->defaultFreq/8;					//index into the map
	freqBit  = pd->defaultFreq - (mapIndex*8);		//bit of interest

	if (!((pd->freqMap[mapIndex] >> freqBit) & 0x1))
	{
		return false;
	}

	switch (pd->pulseValid)
	{
		case 0x01:
		case 0x03:
		case 0x07:
		case 0x0F:
			// The only legal values, so continue
			break;
		default:
			return false;
	}

	for (i = 0; i < NUM_PULSES; i++)
	{
		if (((0x01 << i) & pd->pulseValid) != 0)
		{
			if (!pulseRangeCheck(&pd->pulseDef[i]))
			{
				return false;
			}
		}
	}
	
	return true;
}

/**
 * This function processes the SELECT PROGRAM command.
 *
 * The SELECT PROGRAM command was received.
 * The 'select byte' indicates the desired program to 
 * select, which can be from 1 thru 10, for the normal
 * patient programs, or the 'real time' program, for
 * the special CP program.
 * 
 * \param param pointer to first command parameter
 * \param resp pointer to first byte of response buffer
 * \param source type of source command came from
 * 
 * \return number of bytes in response message
 */

RESPONSE pgm_SelectProgram(uint8_t programNumber)
{
	PRGM_DEF pd;
	
	if (programNumber == 0)
	{
		//Unselect the new program
		memset(&pd, 0, sizeof(pd));
		pgmNum = programNumber;
		pgm = pd;

		return CMND_RESP_INVALID_PARAM;
	}
	else
	{
		if (!pgm_IsProgramNumberValid(programNumber))
		{
			//Unselect the new program
			memset(&pd, 0, sizeof(pd));
			pgmNum = 0;
			pgm = pd;

			return CMND_RESP_INVALID_PARAM;
		}
		
		if (nvReadPrgmDef(programNumber - 1, &pd) < 0)
		{
			//Store active error is disabled byte
			pd.disabled = ACTERR_PROGRAM_DEFINITION_CORRUPTED;

			//Write it back disabled
			nvWritePrgmDef(programNumber - 1, &pd);

			// When we set a Program Definition - initialize the Active Program Settings
			actPgmSetInitialActiveSettings(programNumber, &pd);

			//Log the error
			logError(E_LOG_ERR_STIM_PROGRAM_CORRUPTED, programNumber - 1);

			//Signal event
			dispPutErrorEvent(ACTERR_PROGRAM_DEFINITION_CORRUPTED);

			//Unselect the program
			memset(&pd, 0, sizeof(pd));
			pgmNum = 0;
			pgm = pd;

			return RESP_PROGRAM_DEF_CORRUPT;
		}

		if (!pd.defined)
		{
			//Unselect the program
			memset(&pd, 0, sizeof(pd));
			pgmNum = 0;
			pgm = pd;

			return RESP_PROGRAM_NOT_VALID;
		}

		if (pd.disabled > 0)
		{
			//Unselect the program
			memset(&pd, 0, sizeof(pd));
			pgmNum = 0;
			pgm = pd;

			return RESP_PROGRAM_DISABLED;
		}

		if (ERROR_RETURN_VALUE == actPgmCacheSettingsFromFramToRam(programNumber))
		{
			//Unselect the program
			memset(&pd, 0, sizeof(pd));
			pgmNum = 0;
			pgm = pd;

			return RESP_PROGRAM_SETTINGS_CORRUPT;
		}
	}
	
	// Select the new program
	pgmNum = programNumber;
	pgm = pd;

	return GEN_SUCCESS;
}


/**
 * Handles the Get Get Program Names command.
 *
 *  \param resp pointer to first byte of response buffer
 *
 *  \return number of bytes in response message
 *
 */

RESPONSE pgm_GetPrgmNames(GET_PRGM_NAMES_RESPONSE_DATA *r)
{
	int i;
	PRGM_DEF pd;
	RESPONSE resp = GEN_SUCCESS;
	bool atLeastOneEntry = false;

    // Clear out the struct by default
    memset(r, 0, sizeof(*r));

	for (i = 0; i < NUM_PRGM_DEFS; i++)
	{
		if (nvReadPrgmDef(i, &pd) == 0)
		{
			if (pd.defined)
			{
				memcpy(r->programName[i], pd.name, sizeof(r->programName[i]));
				r->selectableProgramMap |= (1<<i);

				atLeastOneEntry = true;
			}
		}
		else
		{
			resp = RESP_PROGRAM_DEF_CORRUPT;
		}
	}

	if (resp == RESP_PROGRAM_DEF_CORRUPT)
	{
		dispPutErrorEvent(ACTERR_PROGRAM_DEFINITION_CORRUPTED);

		//If we have at least one name in the list, send success
		if (atLeastOneEntry)
		{
			resp = GEN_SUCCESS;
		}
	}

    return resp;
}


/**
 * Returns true if the pulse number is valid for the current program. 
 */
 
bool pgm_IsPulseNumberValid(uint8_t pulse)
{
	return (pgm_GetPulseMap() & (1 << pulse)) != 0;
}


/**
 * Returns the High Pulse Width Limit for the requested pulse.  Value returned is in uSecs.
 */
 
uint16_t pgm_GetPulseWidthHighLimit(uint8_t pulseNo)
{
	return 	pgm_IsPulseNumberValid(pulseNo) ? pgm.pulseDef[pulseNo].pulseWidthHighLimit : 0;

}

/**
 * Returns the Low Pulse Width Limit for the requested pulse.  Value returned is in uSecs.
 */
 
uint16_t pgm_GetPulseWidthLowLimit(uint8_t pulseNo)
{
	return 	pgm_IsPulseNumberValid(pulseNo) ? pgm.pulseDef[pulseNo].pulseWidthLowLimit : 0;

}

/**
 * Returns the Low Pulse Amplitude Limit for the requested pulse.  Value returned is in uA.
 */
 
uint16_t pgm_GetPulseAmplitudeLowLimit(uint8_t pulseNo)
{
	return 	pgm_IsPulseNumberValid(pulseNo) ? pgm.pulseDef[pulseNo].amplitudeLowLimit : 0;

}

/**
 * Returns the Pulse Amplitude Step Value for the requested pulse.  Value returned is in uA.
 */
 
uint16_t pgm_GetPulseAmplitudeStep(uint8_t pulseNo)
{
	return 	pgm_IsPulseNumberValid(pulseNo) ? pgm.pulseDef[pulseNo].amplitudeStep : 0;

}

/**
 * Returns a word in which bits 0-3 indicate which of the four pulses of the current program are valid.
 */
 
uint8_t pgm_GetPulseMap(void)
{
	return 	pgm.defined ? pgm.pulseValid : 0;
}

/**
 * Returns a byte in which the bits indicate valid frequency indexes
 */

uint8_t pgm_GetFreqMapByte(uint8_t byteNo)
{
	if (byteNo < PRGM_FREQ_BITMAP_BYTES)
	{
		return 	pgm.defined ? pgm.freqMap[byteNo] : 0;
	}
	else
	{
		return 0;
	}
}


bool pgm_IsFrequencyValid(uint8_t freqIndex)
{

	uint64_t* validFrequencyMask = (uint64_t*)&pgm.freqMap;

	//Check to see if we are already at the max frequency
	if (freqIndex == MAX_FREQUENCY_SELECTIONS)
	{
		return false;
	}

	if (*validFrequencyMask & ((uint64_t)1 << freqIndex))
	{
		return true;
	}

	return false;
}


/**
 * Returns the bitmap of currently defined programs. Program 1 is bit 0, and program 10 is bit 9.
 */

uint16_t pgm_GetProgramMap(void)
{
	return pgmMap;
}


/**
 * Returns the bitmap of currently enabled programs. Program 1 is bit 0, and program 10 is bit 9.
 */

uint16_t pgm_GetProgramEnabledMap(void)
{
	return pgmEnabledMap;
}


/**
 * pgm_SelectedProgramNumber
 * 
 * Returns selected program number.  Normally this is the patient program
 * number (1-10).  If there is no program presently selected, NULL_PROGRAM
 * is returned. 
 * 
 * \return number of patient program that is presently selected
 */

uint8_t pgm_SelectedProgramNumber(void)
{
    return pgmNum;
}


/**
 * This function processes the DISABLE PROGRAM command.
 *
 * The DISABLE PROGRAM command was received.
 * The 'select byte' indicates the desired program to
 * select, which can be from 1 thru 10, for the normal
 * patient programs, or the 'real time' program, for
 * the special CP program.
 *
 * \param faultyChannel The channel that has an error
 * \param errorCode The reason to disable the channel - e.g.
 * 		  output capacitor check or impedance check failure
 *
 * \return number of bytes in response message
 */

void pgm_DisableProgramChannel(uint8_t faultyChannel, ACTIVE_ERROR_CODE errorCode)
{
	PRGM_DEF pd;
	int iterProgram = 0;
	uint16_t programMap = pgm_GetProgramMap();

	for (iterProgram = 0; iterProgram < NUM_PRGM_DEFS; iterProgram++)
	{
		if ((programMap >> (iterProgram)) & 0x1)
		{
			//Clear out the data so we can check it for 0
			memset(&pd, 0, sizeof(PRGM_DEF));

			if (nvReadPrgmDef(iterProgram, &pd) < 0)
			{
				//Basically couldn't read it out because of corruption, if structure was
				if (pd.defined)
				{
					EVENT errorEvent;

					//Store active error is disabled byte
					pd.disabled = ACTERR_PROGRAM_DEFINITION_CORRUPTED;

					//Log the error
					logError(E_LOG_ERR_STIM_PROGRAM_CORRUPTED, iterProgram);

					if (getActiveError() == ACTERR_NONE)
					{
						errorEvent.eventID = E_ERR_ERROR_DETECTED;
						errorEvent.eventData.eActiveErrorData = ACTERR_PROGRAM_DEFINITION_CORRUPTED;
						dispPutEvent(errorEvent);
					}
				}
			}

			if (pd.defined)
			{
				int iterChannel = 0;
				int iterPulse = 0;
				uint8_t pulseMap = pd.pulseValid;

				// for each pulse set the default active settings
				for(iterPulse = 0; 0 != pulseMap; pulseMap = pulseMap >> 1, iterPulse++)
				{
					for (iterChannel = 0; iterChannel < NUM_CHANNELS; iterChannel++)
					{
						if (pd.pulseDef[iterPulse].electrodeAmpPercentage[iterChannel] != 0)
						{
							if (iterChannel == faultyChannel)
							{
								EVENT errorEvent;

								//Disable the program
								pd.disabled = errorCode;

								//Write it back disabled
								nvWritePrgmDef(iterProgram, &pd);

								// When we set a Program Definition - initialize the Active Program Settings
								actPgmSetInitialActiveSettings(iterProgram + 1, &pd);

								if (getActiveError() == ACTERR_NONE)
								{
									//Signal an error
									errorEvent.eventID = E_ERR_ERROR_DETECTED;
									errorEvent.eventData.eActiveErrorData = errorCode;
									dispPutEvent(errorEvent);
								}

							}//End of conditional checking to see if channel matched disabled channel
						}//End of conditional for checking if channel is use
					}//End of iterating through channels
				}//End of iterating through pulses
			}//End of if program defined conditional
		}//End of if in program map conditional
	}//End of iterating through programs

	return;
}

void pgm_InvalidateSelectedProgram()
{
	PRGM_DEF pd;

	memset(&pd, 0, sizeof(pd));
	pgmNum = 0;
	pgm = pd;
}

