/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description:  Header file for the xPG Information structure interface.
 *	
 ***************************************************************************/



#ifndef XPGINFO_H_
#define XPGINFO_H_

#include <stdbool.h>

/// \defgroup xpgInfo XpgInfo
/// \ingroup dataStore
///
/// The XPG_IDENTITY structure.
///
/// @{

#include "Common/Protocol/cmndSetXpgIdent.h"
#include "Common/Protocol/response_code.h"
#include "Common/Include/dataStoreXpgIdentity.h"

/**
 * Verifies the CRC of the stored XPG_IDENTITY.
 *
 * \return  true if the XPG_IDENTITY passes the CRC check, false otherwise.
 */
bool isXpgInfoValid(void);

/**
 * Verifies the CRC of the stored XPG_IDENTITY backup.
 *
 * \return  true if the XPG_IDENTITY passes the CRC check, false otherwise.
 */
bool isXpgInfoBackupValid(void);

/**
 * Write the XPG_IDENTITY to long-term storage in flash.
 *
 * @param pXpgIdentity	Pointer to an XPG_IDENTITY structure to write to storage.
 * @return	GEN_SUCCESS for success, other response code for failure.
 */
RESPONSE setXpgInfo(const DS_XPG_IDENTITY * pXpgIdentity);

/**
 * Write the primary XPG_IDENTITY to long-term storage in flash.
 *
 * @param pXpgIdentity	Pointer to an XPG_IDENTITY structure to write to storage.
 * @return	GEN_SUCCESS for success, other response code for failure.
 */
RESPONSE setPrimaryXpgInfo(const DS_XPG_IDENTITY * pXpgIdentity);

/**
 * Attempt to restore the primary copy from the backup copy of the xpg identity.
 *
 * @return	OK_RETURN_VALUE for success, ERROR_RETURN_VALUE for failure.
 */
int restorePrimaryXpgInfo(void);

/**
 * Read the current XPG_IDENTITY structure.
 *
 * @return	pointer to the XPG_IDENTITY structure
 */
const DS_XPG_IDENTITY *getXpgInfo(void);

/**
 * Read the backup XPG_IDENTITY structure.
 *
 * @return	pointer to the XPG_IDENTITY structure
 */
const DS_XPG_IDENTITY *getXpgInfoBackup(void);

/// @}

#endif /* XPGINFO_H_ */
