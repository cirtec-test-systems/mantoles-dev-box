/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Header file for the Clinician Programmer data storage.
 *	
 ***************************************************************************/



#ifndef CPDATA_H_
#define CPDATA_H_

#include <stdint.h>

/// \defgroup cpData CPData
/// \ingroup dataStore
///
/// Clinician Programmer (CP) Data Blocks
///
/// This is raw storage for the CP's use provided by the xPG.
///
/// @{

/**
 * Write a Clinician Programmer data block.
 *
 * These blocks are mass storage for the Clinician Programmer and are not
 * processed by the xPG other than to store them on the CP's behalf.
 *
 * @param blockNum	Block number to write
 * @param buf		Data to write
 * @return 0 for success, -1 for error
 */
int cpDataWrite(int blockNum, const uint8_t *buf);

/**
 * Read a Clinician Programmer data block.
 *
 * These blocks are mass storage for the Clinician Programmer and are not
 * processed by the xPG other than to store them on the CP's behalf.
 *
 * @param blockNum	Block number to write
 * @param buf		Data to write
 * @return 0 for success, -1 for error
 */
int cpDataRead(int blockNum, uint8_t *buf);

/// @}

#endif /* CPDATA_H_ */
