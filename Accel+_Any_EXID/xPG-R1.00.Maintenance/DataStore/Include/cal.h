/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description:  Calibration Table interface functions.
 *	
 ***************************************************************************/



#ifndef CAL_H_
#define CAL_H_

#include <stdbool.h>
#include <stdint.h>

/// \defgroup cal Cal
/// \ingroup dataStore
///
/// xPG calibration tables.
///
/// @{

#include "Common/Protocol/cmndStimAsicHVCals.h"
#include "Common/Protocol/cmndGetSetChannelCals.h"
#include "Common/Protocol/gen_cals.h"
#include "Common/Protocol/response_code.h"
#include "stim.h"

typedef enum CHECK_DISABLE
{
	STIM_BASIC_OUTPUT_CHECK = 0,
	STIM_PULSE_GUARD_CHECK,
	STIM_PERIOD_CHECK,
	STIM_OUTPUT_CAP_CHECK
}CHECK_DISABLE;

#define CHANNEL_CAL_CURRENT_IMPEDANCE_MEASURE 3000uL

#define CHANNEL_CAL_CURRENT 15000uL	///< Channel (MDAC) calibration current in uA
#define CHANNEL_CAL_RAW_LSB	15uL  ///< The idealized current per LSB, used when calibrating in raw mode

/// Channel calibrations - parameter data structure
typedef struct CHANNEL_CALS_TABLE
{
	/// channel calibrations, channel sourcing current table 1
	uint16_t channelCalSourcing[NUM_CHANNELS];
	/// channel calibrations, channel sinking current table 1
	uint16_t channelCalSinking[NUM_CHANNELS];
} CHANNEL_CALS_TABLE;

typedef struct CHANNEL_CALIBRATION
{
	CHANNEL_CALS_TABLE channelCalTable[NUM_CHANNEL_CAL_TABLES];
} CHANNEL_CALIBRATION;


bool calIsChannelCalsValid(void);
bool calIsChannelCalsBackupValid(void);
RESPONSE calSetChannelCals(CHANNEL_CALS const *cal);
RESPONSE calSetPrimaryChannelCals(CHANNEL_CALIBRATION const *cal);
void calGetChannelCals(CHANNEL_CALS* cals, uint8_t tableIndex);
CHANNEL_CALIBRATION const *calGetChannelCalsBackup(void);
uint16_t calLookUpSourcingCal(CHANNEL_MAX_AMP_INDEX maxAmp, int chan);
uint16_t calLookUpSinkingCal(CHANNEL_MAX_AMP_INDEX maxAmp, int chan);

bool calIsHVCalsValid(void);
bool calIsHVCalsBackupValid(void);
RESPONSE calSetHVCals(HV_CAL_PARAMS const *cal);
RESPONSE calSetPrimaryHVCals(HV_CAL_PARAMS const *cal);
HV_CAL_PARAMS const *calGetHVCals(void);
HV_CAL_PARAMS const *calGetHVCalsBackup(void);

bool calIsGenCalsValid(void);
bool calIsGenCalsBackupValid(void);
RESPONSE calSetGenCals(const GEN_CALS *list);
RESPONSE calSetPrimaryGenCals(const GEN_CALS *list);
GEN_CALS const *calGetGenCals(void);
GEN_CALS const *calGetGenCalsBackup(void);

/// @}

#endif /* CAL_H_ */
