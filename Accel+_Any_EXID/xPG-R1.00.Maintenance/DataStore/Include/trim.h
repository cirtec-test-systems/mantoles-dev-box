/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Manage the trim list data structures
 *	
 ***************************************************************************/

#ifndef TRIM_H_
#define TRIM_H_

#include <stdbool.h>

/// \defgroup trim Trim
/// \ingroup dataStore
///
/// Trim lists.  Store (address, data) tuples for each of the xPG's ASICs,
/// listing any values that manufacturing test determined should be written
/// to the each ASIC's trim or tuning registers.
///
/// @{

#include "Common/Protocol/cmndGetTrimList.h"
#include "Common/Protocol/cmndSetTrimList.h"
#include "Common/Protocol/trim_list.h"
#include "Common/Protocol/response_code.h"

typedef struct STIM_TRIM_VALS
{
	uint8_t STIM_BG_TRIM;
    uint8_t STIM_PGO_TRIM;
    uint8_t STIM_CASN_TRIM;
    uint8_t STIM_CASP_TRIM;
    uint8_t STIM_HVDDL_TRIM;
    uint16_t STIM_GLOBAL_CHAN_SCALE;
} STIM_TRIM_VALS;

bool trimIsListValid(enum TRIM_LIST_ID id);
bool trimIsListBackupValid(enum TRIM_LIST_ID id);
RESPONSE trimSetList(const TRIM_LIST *list);
RESPONSE trimSetPrimaryList(const TRIM_LIST *list);
RESPONSE trimSetBackupList(const TRIM_LIST *list);
TRIM_LIST const *trimGetList(enum TRIM_LIST_ID id);
TRIM_LIST const *trimGetListBackup(enum TRIM_LIST_ID id);

bool trim_RangeCheckSetTrimList(SET_TRIM_LIST_PARAMS const * const p);
bool trim_RangeCheckGetTrimList(GET_TRIM_LIST_PARAMS const * const p);

int trimGetStimConstants(STIM_TRIM_VALS *stimTrimVals);
bool trimStimConstantsValid();
int trimSetStimConstants(STIM_TRIM_VALS *vals);

bool trim_RangeCheckSetSaturnTrimList(SET_TRIM_LIST_PARAMS const * const p);
bool trim_RangeCheckGetSaturnTrimList(GET_TRIM_LIST_PARAMS const * const p);

/**
 * Alters a trim list by updating the value set for a given address to the specified value.
 * If more than one entry with a matching address exists in the trim list, the last one is
 * updated. If no entry with a matching address exists, then the entry is added to the end
 * of the trim list (and the length increased by 1).
 *
 * \warn This funcation must be called on an in-ram copy of the trim list. It cannot be
 * called on the result of trimGetList() or trimGetListBackup() which both return pointers
 * to flash.
 *
 * \param inMemoryCopy A pointer to a trim list structure that is resident in ram.
 * \param address1 The value of the first address byte to search for.
 * \param address2 The value of the seccond address byte to search for.
 * \param value The new value for the specified address.
 * \returns True if the value was successfuly upddated/added.
 * 			False if the trim list is full and the value could not be added.
 */
bool trimSetTrimValue(TRIM_LIST *inMemoryCopy, uint8_t address1, uint8_t address2, uint8_t value);

/// @}

#endif /*TRIM_H_*/
