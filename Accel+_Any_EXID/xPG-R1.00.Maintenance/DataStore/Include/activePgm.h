/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *	
 *	Copyright (c) 2011 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *	
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: activePgm (Active Stimulation Program Settings) module prototypes
 *	
 ***************************************************************************/

#ifndef	ACTIVE_PGM_H_
#define ACTIVE_PGM_H_

#include <stdint.h>
#include <stdbool.h>
#include "Common/Protocol/response_code.h"

/// \defgroup activePgm ActivePgm
/// \ingroup dataStore
/// @{

#include "prgmDef.h"
#include "error_code.h"

void actPgmInit(void);
int actPgmSetInitialActiveSettings(uint8_t progNumber, const PRGM_DEF *prgm);
int actPgmCacheSettingsFromFramToRam(uint8_t progNumber);

uint8_t actPgmGetActiveFreq(void);
int16_t actPgmGetActiveAmpStepIndex(uint8_t pulseNumber);
int16_t actPgmGetActiveVirtualAmpStepIndex(uint8_t pulseNumber);
uint16_t actPgmGetActivePulseWidth(uint8_t pulseNumber);
uint16_t actPgmGetCalculatedPulseAmp(uint8_t pulseNumber);
RESPONSE actPgmSetActiveFreq(uint8_t aFreq);
RESPONSE actPgmSetActiveAmpStepIndex(uint8_t pulseNumber, int16_t aStepIndex);
RESPONSE actPgmSetActivePulseWidth(uint8_t pulseNumber, uint16_t aPulseWidth);
RESPONSE actPgmNormalIncActiveAmpStepIndex(uint8_t pulseNumber);
RESPONSE actPgmNormalDecActiveAmpStepIndex(uint8_t pulseNumber);
RESPONSE actPgmVirtualIncActiveAmpStepIndexes(void);
RESPONSE actPgmVirtualDecActiveAmpStepIndexes(void);

bool actPgmAllActivePulseAmpsAtMax(void);
bool actPgmAllActivePulseAmpsAtMin(void);
bool actPgmAnyActiveVirtualPulseAmpAtMax(void);
bool actPgmAnyActiveVirtualPulseAmpAtMin(void);

/// @}

#endif /*ACTIVE_PGM_H_*/
