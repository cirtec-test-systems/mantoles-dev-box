/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: pgm (Stimulation Program) module prototypes
 *	
 ***************************************************************************/

#ifndef PGM_H_
#define PGM_H_

#include <stdint.h>
#include <stdbool.h>

/// \defgroup pgm Pgm
/// \ingroup dataStore
///
/// Patient program storage.
///
/// @{

#include "prgmDef.h"
#include "Common/Protocol/cmndSetPrgmDef.h"
#include "Common/Protocol/cmndGetPrgmDef.h"
#include "Common/Protocol/cmndGetPrgmConst.h"
#include "Common/Protocol/cmndSetPrgmConst.h"
#include "Common/Protocol/cmndGetPrgmNames.h"
#include "Common/Protocol/response_code.h"
#include "error_code.h"

void pgm_Init(void);

bool pgm_IsPulseNumberValid(uint8_t pulseNo);

uint8_t pgm_GetPulseMap(void);
uint8_t pgm_GetFreqMapByte(uint8_t byteNo);
bool pgm_IsFrequencyValid(uint8_t freqIndex);
uint16_t pgm_GetProgramMap(void);
uint16_t pgm_GetProgramEnabledMap(void);
uint16_t pgm_GetPulseWidthHighLimit(uint8_t pulseNo);
uint16_t pgm_GetPulseWidthLowLimit(uint8_t pulseNo);
uint16_t pgm_GetPulseAmplitudeLowLimit(uint8_t pulseNo);
uint16_t pgm_GetPulseAmplitudeStep(uint8_t pulseNo);

uint8_t pgm_SelectedProgramNumber(void);

int pgm_GetCurInternalPrgmDef(PRGM_DEF *prgDef);

RESPONSE pgm_GetPrgmDef(GET_PRGM_DEF_PARAMS const *p, GET_PRGM_DEF_RESPONSE_DATA *r);
RESPONSE pgm_SetPrgmDef(SET_PRGM_DEF_PARAMS const *p);
bool pgm_RangeCheckGetPrgmDef(GET_PRGM_DEF_PARAMS const *p);
bool pgm_RangeCheckSetPrgmDef(SET_PRGM_DEF_PARAMS const *p);
RESPONSE pgm_SelectProgram(uint8_t programNumber);
RESPONSE pgm_GetPrgmNames(GET_PRGM_NAMES_RESPONSE_DATA *r);

void pgm_DisableProgramChannel(uint8_t faultyChannel, ACTIVE_ERROR_CODE errorCode);
void pgm_InvalidateSelectedProgram();

/// @}

#endif /*PGM_H_*/
