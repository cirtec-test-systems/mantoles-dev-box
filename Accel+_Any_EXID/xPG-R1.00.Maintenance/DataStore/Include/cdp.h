/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Defines and initializes data structure for the 
 *                     'Configurable Device Parameters', which mostly relate
 *                     to the patient's external controllers... the PoP(s) 
 *                     and PPC.
 *	
 ***************************************************************************/

#ifndef CDP_H_
#define CDP_H_

#include <stdint.h>
#include <stdbool.h>

/// \defgroup cdp CDP
/// \ingroup dataStore
///
/// Configurable Device Parameters table (universally known as "CDP").
///
/// @{

#include "amplStepIndex.h"
#include "Common/Protocol/cmndCommon.h"
#include "Common/Protocol/cmndConfigDeviceParams.h"
#include "Common/Protocol/response_code.h"
#include "command_source.h"

int cdp_Init(void);

int cdp_Get(CONFIG_DEVICE_PARAMETERS *cdp);

COMMAND_SOURCE cdp_IdentifyExid(const EXID_BYTES *exid);

uint8_t cdp_GetAmplitudeSteps(void);
uint16_t cdp_GetPulseWidthStep(void);
uint8_t cdp_GetMagnetOptions(void);

const EXID_BYTES *cdp_GetExid(COMMAND_SOURCE source);
int cdp_SetExid(COMMAND_SOURCE source, const EXID_BYTES *exid, bool enabled);
bool cdp_IsExidEnabled(COMMAND_SOURCE source);

bool cdp_RangeCheck(CONFIG_DEVICE_PARAMETERS const *param);

RESPONSE cdp_GetConfigDeviceParams(CONFIG_DEVICE_PARAMETERS *resp);
RESPONSE cdp_GetConfigDeviceParamsBackup(CONFIG_DEVICE_PARAMETERS *resp);

RESPONSE cdp_SetConfigDeviceParams(CONFIG_DEVICE_PARAMETERS const *param);
RESPONSE cdp_SetPrimaryConfigDeviceParams(CONFIG_DEVICE_PARAMETERS const *param);


/// @}

#endif //CDP_H_

