/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Defines and initializes data structure for the
 *                     'Background Impedance Parameters'
 *
 ***************************************************************************/

#ifndef BGIMP_H_
#define BGIMP_H_

#include <stdint.h>
#include <stdbool.h>

/// \defgroup bgImpg BgImp
/// \ingroup dataStore
///
/// Background Impedance Parameters.
///
/// @{

#include "Common/Protocol/cmndBackgroundImpedance.h"
#include "Common/Protocol/response_code.h"

int bgImpInit(void);

bool bgImpRangeCheck(BACKGROUND_IMPEDANCE_PARAMS const *param);
bool bgImpIsBgImpParamsValid(void);

RESPONSE bgImpGetBackgroundImpedanceParams(BACKGROUND_IMPEDANCE_PARAMS *bgImp);
RESPONSE bgImpSetBackgroundImpedanceParams(BACKGROUND_IMPEDANCE_PARAMS const *bgImp);

/// @}

#endif /* BGIMP_H_ */
