/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Event log module - public functions.
 *
 ***************************************************************************/

#ifndef LOG_H_
#define LOG_H_

#include <stdint.h>
#include "log_events.h"

/// \defgroup log Log
/// \ingroup dataStore
///
/// Event log system.
///
/// @{

#define LOG_ERROR_LOG_NUM 0		///< The index of the error log
#define LOG_NORMAL_LOG_NUM 1		///< The index of the standard log


#define BUILD_LOG_DATA(c0, c1, c2, c3) \
    (((uint32_t)(uint8_t)(c0) << 24) | \
    ((uint32_t)(uint8_t)(c1) << 16) | \
    ((uint32_t)(uint8_t)(c2) << 8) | \
    ((uint32_t)(uint8_t)(c3)))


/// This structure defines the format of the event records that are stored
/// in the event logs.  These are the same event records that are returned
/// in MICS Read Log command as defined in SWEX 0085.
typedef struct LOG_EVENT_RECORD
{
    uint32_t timeStamp;
	uint16_t logEventId;
	uint8_t	 logEventData[4];
} LOG_EVENT_RECORD;


// Log API for support of the log-related MICS commands
void logClear(uint8_t logNum);
uint32_t logGetFirstSerialNum(uint8_t logNum);
uint32_t logGetLastSerialNum(uint8_t logNum);
void logReadEventRecords(uint8_t logNum, uint32_t* firstSerialNumToRead, LOG_EVENT_RECORD* eventRecordBuf, uint16_t* sizeOfBuf, uint16_t* remainingEntriesCount);

// Log API for use by ordinary code
void logNormal(LOG_EVENT_ID logCode, uint32_t data);
void logError(LOG_EVENT_ID logCode, uint32_t data);

// add logging of debug messages using file and line number
typedef enum FILE_NUM {
	f_stim_activate,			//0
	f_stim_calibrate,			//1
	f_stim_defineprogram,		//2
	f_stim_execute,				//3
	f_stim_initialize,			//4
	f_stim_interrupt,			//5
	f_stim_measureimpedance,	//6
	f_stim_measureperiod,		//7
	f_stim_registerIO,			//8
	f_stim_startup,				//9
	f_stim_waveformRAM,			//10
	f_stim_SPI,					//11

	f_gp,						//12
	f_gp_accessors,				//13
	f_gp_auto,					//14
	f_gp_calibration,			//15
	f_gp_impedance,				//16
	f_gp_phasecounter,			//17
	f_gp_phaseloader,			//18

	f_sm_impedance_mgr,			//19
	f_sm_patient_stim_mgr,		//20
	f_sm_test_stim_mgr,			//21
	f_sm_titr_stim_mgr,			//22
	f_sm_director,				//23
	f_sm_pat_common,			//24
	f_sm_common,				//25

    f_cmdproc_commands,			//26
    f_cmdproc_commandprocessor,	//27
    f_cmdproc_micspad,			//28
    f_reserved,					//29
    f_cmdproc_mutators,			//30
    f_cmdproc_queries,			//31

    f_sys_health_mon,			//32
    f_magnet_mon,				//33
    f_battery_mon,				//34
    f_charge_mgr,				//35

    f_gp_pulseguard,			//36

    f_isr,						//37
    f_main,						//38

    f_mics_isr,					//39
    f_mics_spi,					//40
    f_mics_connection,			//41

    f_watchdog,					//42
    f_eventqueue,				//43

    f_sw_timer					//44

} FILE_NUM;

#define DBG_SEV_ERROR	99
#define DBG_SEV_NORM	0

#ifdef LOG_DEBUG

void logDebug(FILE_NUM filenum, uint32_t linenum, uint8_t d1, uint8_t d2, uint8_t d3);
#define DBG(file, d1, d2, d3) logDebug(file, __LINE__, d1, d2, d3)

void logDebug24(FILE_NUM filenum, uint32_t linenum, uint32_t d);
#define DBGI(file,d) logDebug24(file, __LINE__, d)

#else
#define DBG(file, d1, d2, d3)
#define DBGI(file, d)
#endif

/// @}

#endif /*LOG_H_*/
