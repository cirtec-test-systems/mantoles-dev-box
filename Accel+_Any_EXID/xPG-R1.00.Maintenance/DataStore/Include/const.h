/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Header file for the "programmable constants" interface.
 *	
 ***************************************************************************/

#ifndef CONST_H_
#define CONST_H_

#include <stdint.h>
#include <stdbool.h>

/// \defgroup const Const
/// \ingroup dataStore
///
/// Configurable "Constants".  These are constants in the sense that the
/// xPG firmware does not change them during normal operation.  Unlike true
/// constants, they are configurable by external devices.  The name of these
/// variable "constants" is an unfortunate quirk of history.
///
/// @{

#include "prgmConst.h"
#include "pulseConst.h"
#include "Common/Protocol/cmndLeadLimits.h"
#include "Common/Protocol/response_code.h"

/**
 * Initialize the programmable "constants", including the RAM caches.
 *
 * The constants of which we speak are actually nonvolatile configuration
 * parameters.  They can change at runtime, so they are not constant.
 *
 * The "constants" are stored in NVRAM, so local copies are cached in RAM.
 * This function reads the master copies from NVRAM, verifies the CRCs,
 * and generally sets up for business.
 */
void constInit(void);

/**
 * Checks the in-memory lead limits structures for corruption.
 *
 * \returns  true if the variables are usable and uncorrupted, false otherwise
 */
bool constIsLeadLimitsValid(void);

/**
 * Checks the flash memory "constant" structures for corruption.
 *
 * \returns  true if the variables are usable and uncorrupted, false otherwise
 */
bool constIsFlashConstantCacheValid(void);

/**
 * Checks the in-memory "constant" structures for corruption.
 *
 * \returns  true if the variables are usable and uncorrupted, false otherwise
 */
bool constIsRampTimeValid(void);

/**
 * Checks the in-memory "constant" structures for corruption.
 *
 * \returns  true if the variables are usable and uncorrupted, false otherwise
 */
bool constIsSystemStateValid(void);

/**
 * Checks the program constant structures for corruption.
 *
 * \returns  true if the variables are usable and uncorrupted, false otherwise
 */
bool constIsPrgmConstValid(void);

/**
 * Checks the program constant backup structure for corruption.
 *
 * \returns  true if the variables are usable and uncorrupted, false otherwise
 */
bool constIsPrgmConstBackupValid(void);

/**
 * Process the Get Program Constants command.
 *
 * \return pointer to the parameters field
 */
const PRGM_CONST *constGetPrgmConst(void);

/**
 * Return a pointer to program constants backup.
 *
 * \return pointer to the parameters field
 */
const PRGM_CONST *constGetPrgmConstBackup(void);

/**
 * Process the Set Primary and Backup Program Constants
 * command.
 *
 * The program constants block is sent with the command.
 * It are checked to make sure all elements are in range.
 *
 * This command is only allowed via mics, from a CP.
 *
 * This command is not allowed while stimulation is
 * active.
 *
 * \param param	pointer to the parameters field
 * \param resp	pointer to first byte of response buffer
 *
 * \return number of bytes in response message
 */
RESPONSE constSetPrgmConst(const PRGM_CONST *p);

/**
 * Process the Set Primary Program Constants command.
 *
 * The program constants block is sent with the command.
 * It are checked to make sure all elements are in range.
 *
 * This command is only allowed via mics, from a CP.
 *
 * This command is not allowed while stimulation is
 * active.
 *
 * \param param	pointer to the parameters field
 * \param resp	pointer to first byte of response buffer
 *
 * \return number of bytes in response message
 */
RESPONSE constSetPrimaryPrgmConst(const PRGM_CONST *p);

/**
 * Check all fields in a PRGM_CONST for legal values.
 *
 * The rampTime must be in range and a multiple of RAMP_TIME_MULTIPLE.
 * The prgmFreq table must be in range and monotonically ascending.
 *
 * \param p 	Pointer to the PRGM_CONST to check
 * \returns		true if all fields in the structre are well-formed and in range, or false otherwise.
 */
bool constRangeCheckPrgmConst(const PRGM_CONST *p);

/**
 * Checks the pulse constant structures for corruption.
 *
 * \returns  true if the variables are usable and uncorrupted, false otherwise
 */
bool constIsPulseConstValid(void);

/**
 * Checks the pulse constant backup structures for corruption.
 *
 * \returns  true if the variables are usable and uncorrupted, false otherwise
 */
bool constIsPulseConstBackupValid(void);

/**
 * This function processes the GET PULSE CONSTS command.
 *
 * \return Pointer to parameter buffer;
 */
const PULSE_CONST *constGetPulseConst(void);

/**
 * This function retrieves the PULSE CONSTS Data.
 *
 * \return Pointer to parameter buffer;
 */
const PULSE_CONST *constGetPulseConstBackup(void);

/**
 * This function processes the SET PULSE CONSTANTS command.
 *
 * The pulse constants are sent with the command.
 *
 * This command should not be called while stimulation is active
 *
 * \param p pointer to new PULSE_CONST
 * \return GEN_SUCCESS if successful, otherwise another response code
 */
RESPONSE constSetPulseConst(const PULSE_CONST *p);

/**
 * This function writes the primary copy of the pulse constants.
 *
 * The pulse constants are sent with the command.
 *
 * This command should not be called while stimulation is active
 *
 * \param p pointer to new PULSE_CONST
 * \return GEN_SUCCESS if successful, otherwise another response code
 */
RESPONSE constSetPrimaryPulseConst(const PULSE_CONST *p);

/**
 *
 * constRangeCheckPulseConst
 *
 * Tests to see if the proposed pulse constants are within their allowed ranges.
 *
 * \param puc - pulse constants structure pointer to proposed pulse constants parameters
 *
 * \return true if all parameters are within allowed ranges, false otherwise
 */
bool constRangeCheckPulseConst(const PULSE_CONST *puc);

/**
 * Access the ramp time for command-processing purposes.
 *
 * \warning This function is intended for use by the Get Ramp Time MICS command.
 * It directly accesses NVRAM instead of using the in-memory copy of the ramp time
 * and is therefore slow and power-hungry.  All callers except the Get Ramp Time
 * MICS command should use constRampTime() instead.
 *
 * \return a response code if there is anything wrong, otherwise GEN_SUCCESS.
 */
RESPONSE constGetRampTime(uint16_t *t);

/**
 * Access the ramp time backup
 *
 * \warning This function is intended for use by the Get Ramp Time MICS command.
 * It directly accesses NVRAM instead of using the in-memory copy of the ramp time
 * and is therefore slow and power-hungry.  All callers except the Get Ramp Time
 * MICS command should use constRampTime() instead.
 *
 * \return a response code if there is anything wrong, otherwise GEN_SUCCESS.
 */
RESPONSE constGetRampTimeBackup(uint16_t *t);

/**
 * Set the ramp time for command-processing purposes.
 *
 * \return a response code if there is anything wrong, otherwise GEN_SUCCESS.
 */
RESPONSE constSetRampTime(uint16_t t);

/**
 * Set the primary copy of the ramp time
 *
 * \return a response code if there is anything wrong, otherwise GEN_SUCCESS.
 */
RESPONSE constSetPrimaryRampTime(uint16_t t);

/**
 * Range-check a ramp time.
 *
 * Ramp times are required to be no more than 8000 ms, and must be a multiple of
 * 1000 ms.
 *
 * @param t	The proposed ramp time to range-check
 * @return true if in-range, false otherwise
 */
bool constRangeCheckRampTime(uint16_t t);

/**
 * Process the SET LEAD LIMITS command.
 *
 * The lead limits (current density and charge density)
 * are sent with the command.
 * These are checked to make sure they are in range.
 * This command is only allowed via mics, from a CP.
 * This command is not allowed while stimulation is
 * active, since we have to save the settings to flash
 * memory.
 * If everything is okay, the values are saved to their
 * appropriate location in flash memory.
 *
 * \param param pointer to first command parameter
 * \param resp pointer to first byte of response buffer
 * \param source type of source command came from
 *
 * \return number of bytes in response message
 */
RESPONSE constSetLeadLimits(LEAD_LIMIT_PARAMS *llp);

/**
 * Process the GET LEAD LIMITS command.
 *
 * \param param		Pointer to parameter buffer; NULL for command length in blocks
 * \param resp		Pointer to the response buffer
 * \param source	Code indicating the device that sent the command
 *
 * \return If param is NULL, returns the the number of blocks in the command.
 * 		   Otherwise, returns the number of bytes in the response.
 */
RESPONSE constGetLeadLimits(LEAD_LIMIT_PARAMS *r);

/**
 *
 * leadLimitParamsInRange
 *
 * Checks to see if the proposed lead limits are within
 * the allowed ranges.  Checks the current density limits
 * for all 26 electrode channels and then the charge
 * density limits for all 26 electrode channels.
 *
 * \param pLLParams pointer to proposed lead limits data block
 *
 * \return TRUE  if all proposed lead limits are within the allowed range
 *         FALSE if any one of the proposed lead limits are out of range
 */
bool constRangeCheckLeadLimits(LEAD_LIMIT_PARAMS const *p);




/**
 * Returns the stimulation ramp time setting.
 *
 * Uses the cached copy in RAM. Use this function for all accesses except
 * Set Ramp Time command handling.
 *
 * \note Does not verify correctness of the value. Use constIsConstantCacheValid()
 * for verification if needed.
 *
 * @return The current ramp time setting.
 */
uint16_t constRampTime(void);

/**
 * Returns the current Lead Limits.
 *
 * Uses the cached copy in RAM. Use this function for all accesses except
 * Set Lead Limits command handling.
 *
 * \note Does not verify correctness of the value. Use constIsConstantCacheValid()
 * for verification if needed.
 *
 * @return A pointer to the cached copy of Lead Limits.
 */
LEAD_LIMIT_PARAMS const *constLeadLimits(void);

/**
 * Returns the frequency for the requested frequency
 * table index.
 *
 * \note Does not verify correctness of the value. Use constIsConstantCacheValid()
 * for verification if needed.
 *
 * \param prgmFreqIndex index into frequency table. (0-63)
 *
 * \return frequency table entry at requested index, in Hertz
 */
uint16_t constLookUpFrequency(const uint8_t prgmFreqIndex);

/**
 * Returns the current value for the increment lockout interval.
 *
 * \note Does not verify correctness of the value. Use constIsConstantCacheValid()
 * for verification if needed.
 *
 * \return increment lockout interval, in milliseconds
 */
uint16_t constLockoutTime(void);

/**
 * Returns the passive interphase delay from the pulse constants.
 *
 * Reads the value from a cached copy in RAM, for speed.
 *
 * \note Does not verify correctness of the value. Use constIsConstantCacheValid()
 * for verification if needed.
 *
 * \return increment lockout interval, in milliseconds
 */
uint16_t constPassiveInterphaseDelay(void);

/**
 * Returns the passive recovery width from the pulse constants.
 *
 * Reads the value from a cached copy in RAM, for speed.
 *
 * \note Does not verify correctness of the value. Use constIsConstantCacheValid()
 * for verification if needed.
 *
 * \return increment lockout interval, in milliseconds
 */
uint16_t constPassiveRecoveryWidth(void);

/**
 * Returns the passive charge balance correction (CBC) width from the pulse constants.
 *
 * Reads the value from a cached copy in RAM, for speed.
 *
 * \note Does not verify correctness of the value. Use constIsConstantCacheValid()
 * for verification if needed.
 *
 * \return increment lockout interval, in milliseconds
 */
uint16_t constPassiveCBCWidth(void);

/**
 * Returns the active interphase delay from the pulse constants.
 *
 * Reads the value from a cached copy in RAM, for speed.
 *
 * \note Does not verify correctness of the value. Use constIsConstantCacheValid()
 * for verification if needed.
 *
 * \return increment lockout interval, in milliseconds
 */
uint16_t constActiveInterphaseDelay(void);

/**
 * Returns the active recovery holdoff from the pulse constants.
 *
 * Reads the value from a cached copy in RAM, for speed.
 *
 * \note Does not verify correctness of the value. Use constIsConstantCacheValid()
 * for verification if needed.
 *
 * \return increment lockout interval, in milliseconds
 */
uint16_t constActiveRecoveryHoldoff(void);

/**
 * Returns the active charge balance correction (CBC) width from the pulse constants.
 *
 * Reads the value from a cached copy in RAM, for speed.
 *
 * \note Does not verify correctness of the value. Use constIsConstantCacheValid()
 * for verification if needed.
 *
 * \return increment lockout interval, in milliseconds
 */
uint16_t constActiveCBCWidth(void);

/**
 * Returns the active charge balance correction (CBC) holdoff from the pulse constants.
 *
 * Reads the value from a cached copy in RAM, for speed.
 *
 * \note Does not verify correctness of the value. Use constIsConstantCacheValid()
 * for verification if needed.
 *
 * \return increment lockout interval, in milliseconds
 */
uint16_t constActiveCBCHoldoff(void);

/**
 * Returns the recovery ratio bitmap from the pulse constants.
 *
 * Reads the value from a cached copy in RAM, for speed.
 *
 * \note Does not verify correctness of the value. Use constIsConstantCacheValid()
 * for verification if needed.
 *
 * \return increment lockout interval, in milliseconds
 */
uint16_t constKrecsEnabledBitMap(void);

/**
 * Returns the stimulus phase power supply disable flag from the pulse constants.
 *
 * Reads the value from a cached copy in RAM, for speed.
 *
 * \note Does not verify correctness of the value. Use constIsConstantCacheValid()
 * for verification if needed.
 *
 * \return increment lockout interval, in milliseconds
 */
bool constStimPhasePSDisable(void);

/**
 * Returns the uncontrolled source/sink enable flag from the pulse constants.
 *
 * Reads the value from a cached copy in RAM, for speed.
 *
 * \note Does not verify correctness of the value. Use constIsConstantCacheValid()
 * for verification if needed.
 *
 * \return increment lockout interval, in milliseconds
 */
bool constUncontrolledSrcSnkEnabled(void);

/**
 * Looks up an entry in the table of frequencies below while passive recovery is permitted
 * for programs with a given number of pulses.
 *
 * \param numPulses	The number of pulses in the program. Range must be 1 <= numPulses < NUM_PULSES.
 * \return the passive recovery frequency threshold for that number of pulses.
 */
uint16_t constLookupPassiveRecoveryFrequencyThreshold(uint8_t numPulses);

uint16_t constGetGlobalChannelCalibration(void);
/*
 * Define the bits for the system state
 */
typedef enum SYSTEM_STATE
{
	SYS_STATE_DEPLETION_FLAG = 0x0001,
	SYS_STATE_RESERVED_02	 = 0x0002,
	SYS_STATE_RESERVED_03	 = 0x0004,
	SYS_STATE_RESERVED_04	 = 0x0008,
	SYS_STATE_RESERVED_05	 = 0x0010,
	SYS_STATE_RESERVED_06	 = 0x0020,
	SYS_STATE_RESERVED_07	 = 0x0040,
	SYS_STATE_RESERVED_08	 = 0x0080,
	SYS_STATE_RESERVED_09	 = 0x0100,
	SYS_STATE_RESERVED_10	 = 0x0200,
	SYS_STATE_RESERVED_11	 = 0x0400,
	SYS_STATE_RESERVED_12	 = 0x0800,
	SYS_STATE_RESERVED_13	 = 0x1000,
	SYS_STATE_RESERVED_14	 = 0x2000,
	SYS_STATE_RESERVED_15	 = 0x4000
} SYSTEM_STATE;

/**
 * Access the system state.
 *
 * \return OK_RETURN_VALUE or ERROR_RETURN_VALUE.
 */
int constGetSystemState(uint16_t *t);

/**
 * Set the system state.
 *
 * \return OK_RETURN_VALUE or ERROR_RETURN_VALUE
 */
int constSetSystemState(uint16_t t);

/**
 * Get the system state bit status.
 *
 * \return true if bit is set, false otherwise
 */
bool constGetSystemStateBit(SYSTEM_STATE bit);

/**
 * Set the system state bit state.
 *
 * \return OK_RETURN_VALUE or ERROR_RETURN_VALUE
 */
int constResetSystemStateBit(SYSTEM_STATE bit);

/**
 * Set the system state bit state.
 *
 * \return OK_RETURN_VALUE or ERROR_RETURN_VALUE
 */
int constSetSystemStateBit(SYSTEM_STATE bit);

#ifdef TEST
/*
 * The unit tests require access to certain internal data structures
 * of this module.  Declare them with extern linkage here, where they
 * are visible to both the tests and module, to prevent type conflicts. 
 */

#include "prgmConst.h"
#include "pulseConst.h"

extern const PRGM_CONST defPrgmConst;
extern PRGM_CONST prgmConst;
extern uint16_t prgmConstCRC;
	
extern const PULSE_CONST defPulseConst;
extern PULSE_CONST pulseConst;
extern uint16_t pulseConstCRC;

extern uint16_t rampTime;
extern uint16_t rampTimeCRC;

extern LEAD_LIMIT_PARAMS leadLimits;
extern uint16_t leadLimitsCRC;

#endif

/// @}

#endif /*CONST_H_*/
