/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: counter class
 *	
 ***************************************************************************/

#ifndef COUNTER_H_
#define COUNTER_H_

#include <stdint.h>
#include <stdbool.h>

/// \defgroup counter Counters
///
/// Event counters.
///
/// \ingroup dataStore
/// @{

#include "Common/Protocol/counter_id.h"
#include "Common/Protocol/cmndGetCounter.h"
#include "Common/Protocol/cmndSetCounter.h"
#include "Common/Protocol/response_code.h"


/**
 * Increment a counter.
 *
 * \param id	The ID of the counter to increment
 * \returns		0 for success or -1 for failure.
 */
int counterIncrement(COUNTER id);

/**
 * Add a value to a counter.  The addend can be a negative
 * number equivalent to subtracting from the counter
 *
 * \param id	The ID of the counter to add to.
 * \addend		The value to add to the counter.
 * \returns		0 for success or -1 for failure.
 */
int counterAddTo(COUNTER id, int32_t addend);

/**
 * Set a counter.
 *
 * \param id	The ID of the counter to add to.
 * \cntVal		The value to set the counter to.
 * \returns		0 for success or -1 for failure.
 */
int counterSet(COUNTER id, uint32_t cntVal);

/**
 * Handle the Set Counter command
 */
RESPONSE counterCmdSetCounter(SET_COUNTER_PARAMS const *p);

/**
 * Return the counter in response to a Get Counter command
 */
RESPONSE counterCmdGetCounter(GET_COUNTER_PARAMS const *p, GET_COUNTER_RESPONSE_DATA *r);

/**
 * Range check the Get Counter command parameters
 */
bool counterRangeCheckGetCounter(GET_COUNTER_PARAMS const *p);

/**
 * Range check the Set Counter command parameters
 */
bool counterRangeCheckSetCounter(SET_COUNTER_PARAMS const *p);

/// @}

#endif /*COUNTER_H_*/
