/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: "Programmable constant" management.
 *
 *		Reads the constants from NV memory and caches them in RAM for
 *		quick access.  Caching is managed internally by this module.
 *		Functions are provided for updating the NV and RAM copies in
 *		parallel, and for validating the data in cache.
 *
 ***************************************************************************/


#include "const.h"
#include "DataStore/Common/flashstore.h"
#include "DataStore/Common/crc16.h"

#include "Common/Protocol/response_code.h"

#include "StimManager.h"


/*
 * Public API. See documentation in const.h.
 */
bool constIsFlashConstantCacheValid(void)
{
	if (!flashIsPrgmConstValid())
	{
		return false;
	}

	if (!flashIsPulseConstValid())
	{
		return false;
	}

	return true;
}


/*
 * Public API. See documentation in const.h.
 */
bool constIsPrgmConstValid(void)
{
	return flashIsPrgmConstValid();
}


/*
 * Public API. See documentation in const.h.
 */
bool constIsPrgmConstBackupValid(void)
{
	return flashIsPrgmConstBackupValid();
}

/*
 * Public API. See documentation in const.h.
 */
const PRGM_CONST *constGetPrgmConst(void)
{
	return flashPrgmConst();
}


/*
 * Public API. See documentation in const.h.
 */
const PRGM_CONST *constGetPrgmConstBackup(void)
{
	return flashPrgmConstBackup();
}


/*
 * Public API. See documentation in const.h.
 */
RESPONSE constSetPrgmConst(const PRGM_CONST *p)
{
	if (isStimOn())
	{
		return CMND_RESP_BUSY;
	}

	return flashWritePrgmConst(p);
}


/*
 * Public API. See documentation in const.h.
 */
RESPONSE constSetPrimaryPrgmConst(const PRGM_CONST *p)
{
	if (isStimOn())
	{
		return CMND_RESP_BUSY;
	}

	return flashWritePrimaryPrgmConst(p);
}


/*
 * Public API. See documentation in const.h.
 */
bool constRangeCheckPrgmConst(const PRGM_CONST *pc)
{
	int i;

	if (pc->zero != 0)
	{
		return false;
	}

	if (pc->prgmFreq[0] < PROGRAM_FREQUENCY_MIN)
	{
		return false;
	}
	if (pc->prgmFreq[MAX_FREQUENCY_SELECTIONS - 1] > PROGRAM_FREQUENCY_MAX)
	{
		return false;
	}

	// Check for a monotonically increasing frequency table
	for (i = 1; i < MAX_FREQUENCY_SELECTIONS; i++)
	{
		if (pc->prgmFreq[i-1] > pc->prgmFreq[i])
		{
			return false;
		}
	}

	return true;
}


/*
 * Public API. See documentation in const.h.
 */
bool constIsPulseConstValid(void)
{
	return flashIsPulseConstValid();
}


/*
 * Public API. See documentation in const.h.
 */
bool constIsPulseConstBackupValid(void)
{
	return flashIsPulseConstBackupValid();
}


/*
 * Public API. See documentation in const.h.
 */
const PULSE_CONST *constGetPulseConst(void)
{
	return flashPulseConst();
}


/*
 * Public API. See documentation in const.h.
 */
const PULSE_CONST *constGetPulseConstBackup(void)
{
	return flashPulseConstBackup();
}


/*
 * Public API. See documentation in const.h.
 */
RESPONSE constSetPulseConst(const PULSE_CONST *p)
{
	return flashWritePulseConst(p);
}


/*
 * Public API. See documentation in const.h.
 */
RESPONSE constSetPrimaryPulseConst(const PULSE_CONST *p)
{
	return flashWritePrimaryPulseConst(p);
}


/*
 * Public API. See documentation in const.h.
 */
bool constRangeCheckPulseConst(const PULSE_CONST *puc)
{
    //start by checking the passive recovery interphase delay
    if (puc->passiveInterphaseDelay < INTERPHASE_DELAY_MIN || puc->passiveInterphaseDelay > INTERPHASE_DELAY_MAX)
    {
        //passive recovery interphase delay not in range
        return false;
    }

    //check the passive recovery width
    if (puc->passiveRecoveryWidth < PASSIVE_RECOVERY_WIDTH_MIN
    		|| puc->passiveRecoveryWidth > PASSIVE_RECOVERY_WIDTH_MAX)
    {
        //passive recovery width not in range
        return false;
    }

    //check passive recovery holdoff
    if (puc->passiveRecoveryHoldoff < HOLDOFF_MIN || puc->passiveRecoveryHoldoff > HOLDOFF_MAX)
    {
        //passive recovery holdoff out of range
        return false;
    }

    //check passive CBC width
    if (puc->passiveCBCWidth < PASSIVE_RECOVERY_WIDTH_MIN || puc->passiveCBCWidth > PASSIVE_RECOVERY_WIDTH_MAX)
    {
        //passive recovery CBC width out of range
        return false;
    }

    //check active interphase delay
    if (puc->activeInterphaseDelay < INTERPHASE_DELAY_MIN || puc->activeInterphaseDelay > INTERPHASE_DELAY_MAX)
    {
        //active interphase delay out of range
        return false;
    }

    //check active recovery holdoff
    if (puc->activeRecoveryHoldoff < HOLDOFF_MIN || puc->activeRecoveryHoldoff > HOLDOFF_MAX)
    {
        //active recovery holdoff out of range
        return false;
    }

    //check active CBC width
    if (puc->activeCBCWidth < ACTIVE_RECOVERY_WIDTH_MIN || puc->activeCBCWidth > ACTIVE_RECOVERY_WIDTH_MAX)
    {
        //active recovery CBC width out of range
        return false;
    }

    //check active CBC holdoff
    if (puc->activeCBCHoldoff < HOLDOFF_MIN || puc->activeCBCHoldoff > HOLDOFF_MAX)
    {
        //active recovery CBC holdoff out of range
        return false;
    }

    //check KREC bitmask
    if (puc->krecsEnabledBitMap == 0 || puc->krecsEnabledBitMap > 0x001F)
    {
        //krec bitmask out of range
        return false;
    }

    //check increment lockout
    if (puc->incrementLockoutMsecs > INCREMENT_LOCKOUT_MAX)
    {
        //increment lockout out of range
        return false;
    }

    //check disable stim power supply feature setting
    if (puc->stimPhasePSDisable != 0 && puc->stimPhasePSDisable != 1)
    {
        //disable stim power supply setting not in range
        return false;
    }

    //check uncontrolled src/snk setting
    if (puc->uncontrolledSrcSnkEnabled != 0 && puc->uncontrolledSrcSnkEnabled != 1)
    {
        //uncontrolled src/snk setting not in range
        return false;
    }

    return true;
}


/*
 * Public API. See documentation in const.h.
 */
uint16_t constLookUpFrequency(const uint8_t prgmFreqIndex)
{
    //return the frequency based on the index
    return constGetPrgmConst()->prgmFreq[prgmFreqIndex];
}


/*
 * Public API. See documentation in const.h.
 */
uint16_t constLockoutTime(void)
{
    return constGetPulseConst()->incrementLockoutMsecs;
}


/*
 * Public API. See documentation in const.h.
 */
uint16_t constPassiveInterphaseDelay(void)
{
	return constGetPulseConst()->passiveInterphaseDelay;
}

/*
 * Public API. See documentation in const.h.
 */
uint16_t constPassiveRecoveryWidth(void)
{
	return constGetPulseConst()->passiveRecoveryWidth;
}

/*
 * Public API. See documentation in const.h.
 */
uint16_t constPassiveCBCWidth(void)
{
	return constGetPulseConst()->passiveCBCWidth;
}

/*
 * Public API. See documentation in const.h.
 */
uint16_t constActiveInterphaseDelay(void)
{
	return constGetPulseConst()->activeInterphaseDelay;
}

/*
 * Public API. See documentation in const.h.
 */
uint16_t constActiveRecoveryHoldoff(void)
{
	return constGetPulseConst()->activeRecoveryHoldoff;
}

/*
 * Public API. See documentation in const.h.
 */
uint16_t constActiveCBCWidth(void)
{
	return constGetPulseConst()->activeCBCWidth;
}

/*
 * Public API. See documentation in const.h.
 */
uint16_t constActiveCBCHoldoff(void)
{
	return constGetPulseConst()->activeCBCHoldoff;
}

/*
 * Public API. See documentation in const.h.
 */
uint16_t constKrecsEnabledBitMap(void)
{
	return constGetPulseConst()->krecsEnabledBitMap;
}

/*
 * Public API. See documentation in const.h.
 */
bool constStimPhasePSDisable(void)
{
	return constGetPulseConst()->stimPhasePSDisable;
}

/*
 * Public API. See documentation in const.h.
 */
bool constUncontrolledSrcSnkEnabled(void)
{
	return constGetPulseConst()->uncontrolledSrcSnkEnabled;
}


uint16_t constGetGlobalChannelCalibration(void)
{
    return constGetPulseConst()->globalChannelCal;
}

