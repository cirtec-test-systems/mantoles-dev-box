/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: "Programmable constant" management.
 *
 *		Reads the constants from NV memory and caches them in RAM for
 *		quick access.  Caching is managed internally by this module.
 *		Functions are provided for updating the NV and RAM copies in
 *		parallel, and for validating the data in cache.
 *	
 ***************************************************************************/

#include <string.h>

#include "const.h"
#include "DataStore/Common/nvstore.h"
#include "DataStore/Common/crc16.h"

#include "Common/Protocol/response_code.h"

#include "EventQueue.h"

#include "log_events.h"
#include "log.h"

/*
 * Permit the test suite access to these internal structures
 */
 
#ifndef TEST
#define STATIC		static
#else
#define STATIC
#endif


/**
 * Cache the current ramp time setting.
 */

STATIC uint16_t rampTime;
STATIC uint16_t rampTimeCRC;

/**
 * Cache the lead limits
 */

STATIC LEAD_LIMIT_PARAMS leadLimits;
STATIC uint16_t leadLimitsCRC;

/**
 * Cache the system state
 */

STATIC uint16_t systemState;
STATIC uint16_t systemStateCRC;


/*
 * Public API. See documentation in const.h.
 */
void constInit(void)
{
	//
	// Initialize ramp time
	//
	if (nvReadRampTime(&rampTime, &rampTimeCRC) != OK_RETURN_VALUE)
	{
		dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
	}

	//
	// Initialize lead limits
	//
	if (nvReadLeadLimits(&leadLimits, &leadLimitsCRC) != OK_RETURN_VALUE)
	{
		//Log the lead limits corruption
		logError(E_LOG_ERR_LEAD_INFORMATION_CORRUPTED,0);

		dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
	}

	//
	// Initialize the system state
	//
	if (nvReadSystemState(&systemState, &systemStateCRC) != OK_RETURN_VALUE)
	{
		//If its corrupted, just reset the state...this is for internal use only
		constSetSystemState(0x0);
	}
}


/*
 * Public API. See documentation in const.h.
 */
bool constIsLeadLimitsValid(void)
{
	if (crc16(&leadLimits, sizeof(leadLimits)) != leadLimitsCRC)
	{
		return false;
	}

	return true;
}

bool constIsRampTimeValid(void)
{
	// The ramp time was originally part of the program constants and
	// needs to be verified at the same times as prgmConst.
	if (crc16(&rampTime, sizeof(rampTime)) != rampTimeCRC)
	{
		return false;
	}

	return true;
}

/*
 * Public APIs for System State. See documentation in const.h.
 */
bool constIsSystemStateValid(void)
{
	if (crc16(&systemState, sizeof(systemState)) != systemStateCRC)
	{
		return false;
	}

	return true;
}

int constGetSystemState(uint16_t *t)
{
	if (nvReadSystemState(t, NULL) < 0)
	{
		return ERROR_RETURN_VALUE;
	}

	return OK_RETURN_VALUE;
}

int constSetSystemState(uint16_t t)
{
	if (nvWriteSystemState(t) < 0)
	{
		return ERROR_RETURN_VALUE;
	}

	// Update the working copy in RAM
	systemState = t;
	systemStateCRC = crc16(&systemState, sizeof(systemState));

	return OK_RETURN_VALUE;
}

bool constGetSystemStateBit(SYSTEM_STATE bit)
{
	//Copy the status byte
	uint16_t byte = systemState;

	//And the byte with the bit
	byte &= bit;

	//If the bit is set, return true
	if (byte != 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

int constSetSystemStateBit(SYSTEM_STATE bit)
{
	//Copy the status byte
	uint16_t byte = systemState;

	//Or the bit with the byte
	byte |= bit;

	if (nvWriteSystemState(byte) < 0)
	{
		return ERROR_RETURN_VALUE;
	}

	// Update the working copy in RAM
	systemState = byte;
	systemStateCRC = crc16(&systemState, sizeof(systemState));

	return OK_RETURN_VALUE;
}

int constResetSystemStateBit(SYSTEM_STATE bit)
{
	//Copy the status byte
	uint16_t byte = systemState;

	//Or the bit with the byte
	byte &= ~bit;

	if (nvWriteSystemState(byte) < 0)
	{
		return ERROR_RETURN_VALUE;
	}

	// Update the working copy in RAM
	systemState = byte;
	systemStateCRC = crc16(&systemState, sizeof(systemState));

	return OK_RETURN_VALUE;
}



RESPONSE constGetRampTime(uint16_t *t)
{
	if (nvReadRampTime(t, NULL) < 0)
	{
		return RESP_READ_FAILED;
	}
	return GEN_SUCCESS;
}

RESPONSE constGetRampTimeBackup(uint16_t *t)
{
	if (nvReadRampTimeBackup(t, NULL) < 0)
	{
		return RESP_READ_FAILED;
	}
	return GEN_SUCCESS;
}

/*
 * Public API. See documentation in const.h.
 */
RESPONSE constSetRampTime(uint16_t t)
{
	if (nvWriteRampTime(t) < 0)
	{
		return RESP_WRITE_FAILED;
	}

	// Update the working copy in RAM
	rampTime = t;
	rampTimeCRC = crc16(&rampTime, sizeof(rampTime));

	return GEN_SUCCESS;
}

/*
 * Public API. See documentation in const.h.
 */
RESPONSE constSetPrimaryRampTime(uint16_t t)
{
	if (nvWritePrimaryRampTime(t) < 0)
	{
		return RESP_WRITE_FAILED;
	}

	// Update the working copy in RAM
	rampTime = t;
	rampTimeCRC = crc16(&rampTime, sizeof(rampTime));

	return GEN_SUCCESS;
}


/*
 * Public API. See documentation in const.h.
 */
bool constRangeCheckRampTime(uint16_t t)
{
	return t <= RAMP_TIME_MAX && (t % RAMP_TIME_MULTIPLE) == 0;
}

/*
 * Public API. See documentation in const.h.
 */
RESPONSE constSetLeadLimits(LEAD_LIMIT_PARAMS *llp)
{
	// Write it to FRAM

	if (nvWriteLeadLimits(llp) < 0)
	{
		return RESP_WRITE_FAILED;
	}

	// Write it to the cache

	memcpy(&leadLimits, llp, sizeof(leadLimits));
	leadLimitsCRC = crc16(&leadLimits, sizeof(leadLimits));

	return GEN_SUCCESS;
}

/*
 * Public API. See documentation in const.h.
 */
RESPONSE constGetLeadLimits(LEAD_LIMIT_PARAMS *r)
{
	if (nvReadLeadLimits(r, NULL) < 0)
	{
		dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
		return RESP_LEAD_LIMITS_CORRUPT;
	}

	return GEN_SUCCESS;
}

/*
 * Public API. See documentation in const.h.
 */
bool constRangeCheckLeadLimits(const LEAD_LIMIT_PARAMS *p)
{
    uint8_t i;

    //first see if the current limits are in the allowed range
    for (i = 0; i < NUM_CHANNELS; i++)
    {
        if (p->currentLimit[i] > MAX_CURRENT_DENSITY_LIMIT)
        {
            return false;
        }
    }

    //if current limits in range, see if charge limits are in range
    for (i = 0; i < NUM_CHANNELS; i++)
    {
        if (p->chargeLimit[i] > MAX_CHARGE_DENSITY_LIMIT)
        {
            return false;
        }
    }

    return true;
}



/*
 * Public API. See documentation in const.h.
 */
uint16_t constRampTime(void)
{
    return rampTime;
}


/*
 * Public API. See documentation in const.h.
 */
LEAD_LIMIT_PARAMS const *constLeadLimits(void)
{
	return &leadLimits;
}



/*
 * Public API. See const.h for documentation.
 */
uint16_t constLookupPassiveRecoveryFrequencyThreshold(uint8_t numPulses)
{
	// TODO: This table should be factory-configurable, not hard-coded.
	static uint16_t const thresh[NUM_PULSES] = { 175, 105, 75, 60 };
	return thresh[numPulses - 1];
}


