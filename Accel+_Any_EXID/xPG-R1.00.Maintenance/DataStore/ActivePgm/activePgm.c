/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: activePgm (Active Stimulation Program Settings) module prototypes
 *	
 ***************************************************************************/


#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "activePgm.h"
#include "activePrgmDef.h"
#include "pgm.h"
#include "log.h"
#include "log_events.h"
#include "prgmDef.h"
#include "error_code.h"
#include "EventQueue.h"
#include "DataStore/Common/nvstore.h"
#include "Common/Protocol/response_code.h"

/**
 * Store the current active settings for each program definition.
 */
static ACTIVE_PRGM_DEF activePgm;

/**
 * This function initializes the Active Program Settings data structures.
 * 
 */
void actPgmInit(void)
{
	memset(&activePgm, 0, sizeof(activePgm));

	return;

}

/**
 * This function initializes the Active Program Settings data structure in
 * NV RAM to the default values from the PRGM_DEF structure.  This is normally
 * called when a Program Definition is being set in NV RAM.  This clears 
 * the Active Error Code for the Program Definition.
 * 
 * \param progNumber - the number of the program definition to retrieve from [1..10]
 *
 * \return	OK_RETURN_VALUE(0) for success, or
 *          ERROR_RETURN_VALUE(-1) if there was a failure
 */
int actPgmSetInitialActiveSettings(uint8_t progNumber, const PRGM_DEF *prgm)
{
	uint8_t pulseMap, pulseNo;
	ACTIVE_PRGM_DEF actPrgDef;

	if (NULL_PROGRAM == progNumber)
	{
		return (ERROR_RETURN_VALUE);
	}

	// Set all the values in the local Active Program Definition
	actPrgDef.curFreqIndex = prgm->defaultFreq;
	
	// for each pulse set the default active settings
	pulseMap = prgm->pulseValid;
	for(pulseNo = 0; 0 != pulseMap; pulseMap = pulseMap >> 1, pulseNo++)
	{
		actPrgDef.activePulseDef[pulseNo].curAmplitudeStepIndex = prgm->pulseDef[pulseNo].amplitudeStepIndex;
		actPrgDef.activePulseDef[pulseNo].curVirtualAmplitudeStepIndex = prgm->pulseDef[pulseNo].amplitudeStepIndex;
		actPrgDef.activePulseDef[pulseNo].curPulseWidth = prgm->pulseDef[pulseNo].pulseWidth;
	}

	// If the program number requested is the same as the currently selected program then write the active
	// program settings that are cached in RAM too
	if ( progNumber  == pgm_SelectedProgramNumber() )
	{
		// Set all the values in the local Active Program Definition
		actPgmSetActiveFreq(prgm->defaultFreq);

		// for each pulse set the default active settings
		pulseMap = prgm->pulseValid;
		for(pulseNo = 0; 0 != pulseMap; pulseMap = pulseMap >> 1, pulseNo++)
		{
			actPgmSetActiveAmpStepIndex(pulseNo, prgm->pulseDef[pulseNo].amplitudeStepIndex);  // this function takes care of the virtual index too
			actPgmSetActivePulseWidth(pulseNo, prgm->pulseDef[pulseNo].pulseWidth);
		}
	}

	// Write the active program structure to NV RAM
	return ( nvWriteActPrgmDef(progNumber-1, &actPrgDef));
}


/**
 * This function reads the Active Program settings for the requested program that
 * are store in NV RAM and stores them in the cached RAM memory area for the currently
 * selected Active Program settings.  If the Active Program Settings stored in NV RAM
 * are corrupted then the Active Settings will be set to their default values from the
 * Program Definition.
 *
 * \param progNumber - the number of the program definition to retrieve from [1..10]
 *
 * \return	OK_RETURN_VALUE(0) for success, or
 *          ERROR_RETURN_VALUE(-1) if there was a failure
 */
int actPgmCacheSettingsFromFramToRam(uint8_t progNumber)
{
	PRGM_DEF pd;

	if (NULL_PROGRAM == progNumber)
	{
		return (ERROR_RETURN_VALUE);
	}

	if (ERROR_RETURN_VALUE == nvReadActPrgmDef(progNumber-1, &activePgm))
	{
		// if the Active Program Settings store in NV RAM are corrupted then restore
		// the default settings from the Program Definition
		nvReadPrgmDef(progNumber - 1, &pd);					//get program def
		actPgmSetInitialActiveSettings(progNumber, &pd);	//restore the active settings to defaults
		nvReadActPrgmDef(progNumber-1, &activePgm);

		//Signal event
		dispPutErrorEvent(ACTERR_PROGRAM_ACTIVE_SETTINGS_CORRUPTED);

		// log error
		logError(E_LOG_ERR_STIM_ACTIVE_SETTINGS_CORRUPTED,0);

		return ERROR_RETURN_VALUE;
	}

	return (OK_RETURN_VALUE);

}

/**
 * Returns the active Frequency for the current program.
 * 
 * \return the active Frequency
 */
uint8_t actPgmGetActiveFreq(void)
{
	return (activePgm.curFreqIndex);
}

/**
 * Returns the active Amplitude Step Index for current program
 * and the requested pulse number.
 * 
 * \param pulseNumber - the number of the pulse definition to retrieve from  [0..3]
 * 
 * \return the active Amplitude Step Index [1..50]
 */
int16_t actPgmGetActiveAmpStepIndex(uint8_t pulseNumber)
{
	return (activePgm.activePulseDef[pulseNumber].curAmplitudeStepIndex);
}

/**
 * Returns the active Virtual Amplitude Step Index for the current program
 * and the requested pulse number.
 * 
 * \param pulseNumber - the number of the pulse definition to retrieve from  [0..3]
 * 
 * \return the active Virtual Amplitude Step Index [-48..99]
 */
int16_t actPgmGetActiveVirtualAmpStepIndex(uint8_t pulseNumber)
{
	return (activePgm.activePulseDef[pulseNumber].curVirtualAmplitudeStepIndex);
}

/**
 * Returns the active Pulse Width for the current program
 * and the requested pulse number.
 * 
 * \param pulseNumber - the number of the pulse definition to retrieve from  [0..3]
 * 
 * \return the active Pulse Width
 */
uint16_t actPgmGetActivePulseWidth(uint8_t pulseNumber)
{
	return (activePgm.activePulseDef[pulseNumber].curPulseWidth);
}

/**
 * Calculates and returns the current Pulse Amplitude for the current selected
 * program number and pulse number.
 * 
 * \param progNumber - the number of the program definition to retrieve from [1..10]
 * \param pulseNumber - the number of the pulse definition to retrieve from  [0..3]
 * 
 * \return the calculated Pulse Amplitude
 */

uint16_t actPgmGetCalculatedPulseAmp(uint8_t pulseNumber)
{

	return ((uint16_t)pgm_GetPulseAmplitudeLowLimit(pulseNumber) + 
		(pgm_GetPulseAmplitudeStep(pulseNumber) * (actPgmGetActiveAmpStepIndex(pulseNumber) - 1)));
}


/**
 * Sets the active Frequency for the current program
 * and updates the Active Program settings in NV RAM.
 * 
 * \param aFreq - the active Frequency to set
 * 
 * \return	OK_RETURN_VALUE(0) for success, or
 *          ERROR_RETURN_VALUE(-1) if there was a failure
 */
RESPONSE actPgmSetActiveFreq(uint8_t aFreq)
{
	uint8_t curSelectedProgIndex;
	ACTIVE_PRGM_DEF lActPrgm;

	curSelectedProgIndex = pgm_SelectedProgramNumber() - 1;

	// Read the current active settings for the current selected program
	if ( ERROR_RETURN_VALUE == nvReadActPrgmDef(curSelectedProgIndex, &lActPrgm) )
	{
		return (RESP_READ_FAILED);
	}

	// Double check we are trying to set a valid frequency
	if (!(pgm_IsFrequencyValid(aFreq)))
	{
		return (RESP_FREQ_CHANGE_NOT_ALLOWED);
	}

	activePgm.curFreqIndex = aFreq;
	lActPrgm.curFreqIndex = aFreq;

	if ( ERROR_RETURN_VALUE == nvWriteActPrgmDef(curSelectedProgIndex, &lActPrgm) )
	{
		return (RESP_WRITE_FAILED);
	}
	else
	{
		return (GEN_SUCCESS);
	}
}

/**
 * Sets the active Amplitude Step Index for the current program and requested pulse number
 * and updates the Active Program settings in NV RAM.
 * 
 * \param pulseNumber - the number of the pulse definition to set to  [0..3]
 * \param aStepIndex - the active Pulse Amplitude Step Index to set
 * 
 * \return	OK_RETURN_VALUE(0) for success, or
 *          ERROR_RETURN_VALUE(-1) if there was a failure
 */
RESPONSE actPgmSetActiveAmpStepIndex(uint8_t pulseNumber, int16_t aStepIndex)
{
	ACTIVE_PRGM_DEF lActPrgm;
	uint8_t curSelectedProgIndex;

	curSelectedProgIndex = pgm_SelectedProgramNumber() - 1;
	// Read the current active settings for the current selected program
	if ( ERROR_RETURN_VALUE == nvReadActPrgmDef(curSelectedProgIndex, &lActPrgm) )
	{
		return (RESP_READ_FAILED);
	}

	if ( (aStepIndex < PULSE_OUTPUT_AMPL_STEP_MIN) || (aStepIndex > PULSE_OUTPUT_AMPL_STEP_MAX) )
	{
		return (RESP_PULSE_AMP_EXCEEDS_LIMITS);
	}

	activePgm.activePulseDef[pulseNumber].curAmplitudeStepIndex = aStepIndex;
	activePgm.activePulseDef[pulseNumber].curVirtualAmplitudeStepIndex = aStepIndex;
	lActPrgm.activePulseDef[pulseNumber].curAmplitudeStepIndex = aStepIndex;
	lActPrgm.activePulseDef[pulseNumber].curVirtualAmplitudeStepIndex = aStepIndex;

	if ( ERROR_RETURN_VALUE == nvWriteActPrgmDef(curSelectedProgIndex, &lActPrgm) )
	{
		return (RESP_WRITE_FAILED);
	}
	else
	{
		return (GEN_SUCCESS);
	}

}

/**
 * Sets the active Pulse Width for the current program and requested pulse number
 * and updates the Active Program settings in NV RAM.
 * 
 * \param pulseNumber - the number of the pulse definition to set to  [0..3]
 * \param aPulseWidth - the active Pulse Width to set
 * 
 * \return	OK_RETURN_VALUE(0) for success, or
 *          ERROR_RETURN_VALUE(-1) if there was a failure
 */
RESPONSE actPgmSetActivePulseWidth(uint8_t pulseNumber, uint16_t aPulseWidth)
{
	ACTIVE_PRGM_DEF lActPrgm;
	uint8_t curSelectedProgIndex;

	curSelectedProgIndex = pgm_SelectedProgramNumber() - 1;
	// Read the current active settings for the current selected program
	if ( ERROR_RETURN_VALUE == nvReadActPrgmDef(curSelectedProgIndex, &lActPrgm) )
	{
		return (RESP_READ_FAILED);
	}

	activePgm.activePulseDef[pulseNumber].curPulseWidth = aPulseWidth;
	lActPrgm.activePulseDef[pulseNumber].curPulseWidth = aPulseWidth;

	if ( ERROR_RETURN_VALUE == nvWriteActPrgmDef(curSelectedProgIndex, &lActPrgm) )
	{
		return (RESP_WRITE_FAILED);
	}
	else
	{
		return (GEN_SUCCESS);
	}

}

/**
 * Increments the active Amplitude Step Index and adjusts the Virtual Amplitude Step Index
 * accordingly.  This 'Normal' function is used when adjusting only one pulse amplitude.
 * Operates on the currently selected program
 * and updates the Active Program settings in NV RAM.
 *
 * \param pulseNumber - the number of the pulse definition to set to  [0..3]
 * 
 * \return	A Response code based on the success or failure of the routine
 */
RESPONSE actPgmNormalIncActiveAmpStepIndex(uint8_t pulseNumber)
{
	ACTIVE_PRGM_DEF lActPrgm;
	ACTIVE_PULSE_DEF *apd;
	uint8_t curSelectedProgIndex;
	RESPONSE retResp;

	curSelectedProgIndex = pgm_SelectedProgramNumber() - 1;
	// Read the current active settings for the current selected program
	if ( ERROR_RETURN_VALUE == nvReadActPrgmDef(curSelectedProgIndex, &lActPrgm) )
	{
		return (RESP_READ_FAILED);
	}

	apd = &(activePgm.activePulseDef[pulseNumber]);

	// if the virtual index is below one just increment it
	if (apd->curVirtualAmplitudeStepIndex < PULSE_OUTPUT_AMPL_STEP_MIN)
	{
		apd->curVirtualAmplitudeStepIndex++;
		lActPrgm.activePulseDef[pulseNumber].curVirtualAmplitudeStepIndex = apd->curVirtualAmplitudeStepIndex;
		retResp = GEN_SUCCESS;
	}
	// if the pulse index is at its high limit then don't increment it
	else if (apd->curAmplitudeStepIndex >= PULSE_OUTPUT_AMPL_STEP_MAX)
	{
		retResp = RESP_PULSE_AMP_EXCEEDS_LIMITS;
	}
	else
	{
		// otherwise increment both amplitude indexes
		apd->curAmplitudeStepIndex++;
		apd->curVirtualAmplitudeStepIndex++;
		lActPrgm.activePulseDef[pulseNumber].curAmplitudeStepIndex = apd->curAmplitudeStepIndex;
		lActPrgm.activePulseDef[pulseNumber].curVirtualAmplitudeStepIndex = apd->curVirtualAmplitudeStepIndex;
		retResp = GEN_SUCCESS;
	}

	if ( ERROR_RETURN_VALUE == nvWriteActPrgmDef(curSelectedProgIndex, &lActPrgm) )
	{
		retResp = RESP_WRITE_FAILED;
	}

	return (retResp);

}

/**
 * Decrements the active Amplitude Step Index and adjusts the Virtual Amplitude Step Index
 * accordingly.  This 'Normal' function is used when adjusting only one pulse amplitude.
 * Operates on the currently selected program
 * and updates the Active Program settings in NV RAM.
 * 
 * \param pulseNumber - the number of the pulse definition to set to  [0..3]
 * 
 * \return	A Response code based on the success or failure of the routine
 */
RESPONSE actPgmNormalDecActiveAmpStepIndex(uint8_t pulseNumber)
{
	ACTIVE_PRGM_DEF lActPrgm;
	ACTIVE_PULSE_DEF *apd;
	uint8_t curSelectedProgIndex;
	RESPONSE retResp;

	curSelectedProgIndex = pgm_SelectedProgramNumber() - 1;
	// Read the current active settings for the current selected program
	if ( ERROR_RETURN_VALUE == nvReadActPrgmDef(curSelectedProgIndex, &lActPrgm) )
	{
		return (RESP_READ_FAILED);
	}

	apd = &(activePgm.activePulseDef[pulseNumber]);

	// if the virtual index is above the MAX just decrement it
	if (apd->curVirtualAmplitudeStepIndex > PULSE_OUTPUT_AMPL_STEP_MAX)
	{
		apd->curVirtualAmplitudeStepIndex--;
		lActPrgm.activePulseDef[pulseNumber].curVirtualAmplitudeStepIndex = apd->curVirtualAmplitudeStepIndex;
		retResp = GEN_SUCCESS;
	}
	// if the pulse index is at its low limit then don't decrement it
	else if (apd->curAmplitudeStepIndex <= PULSE_OUTPUT_AMPL_STEP_MIN)
	{
		retResp = RESP_PULSE_AMP_EXCEEDS_LIMITS;
	}
	else
	{
		// otherwise decrement both indexes
		apd->curAmplitudeStepIndex--;
		apd->curVirtualAmplitudeStepIndex--;
		lActPrgm.activePulseDef[pulseNumber].curAmplitudeStepIndex = apd->curAmplitudeStepIndex;
		lActPrgm.activePulseDef[pulseNumber].curVirtualAmplitudeStepIndex = apd->curVirtualAmplitudeStepIndex;
		retResp = GEN_SUCCESS;
	}

	if ( ERROR_RETURN_VALUE == nvWriteActPrgmDef(curSelectedProgIndex, &lActPrgm) )
	{
		retResp = RESP_WRITE_FAILED;
	}

	return (retResp);

}

/**
 * Increments the active Amplitude Step Index and adjusts the Virtual Amplitude Step Index
 * accordingly.  This 'Virtual' function is used when adjusting Program Amplitude.
 * Operates on the currently selected program
 * and updates the Active Program settings in NV RAM.
 *
 * \param pulseNumber - the number of the pulse definition to set to  [0..3]
 * 
 * \return	OK_RETURN_VALUE(0) for success, or
 *          ERROR_RETURN_VALUE(-1) if the pulse index is at its high limit
 */
RESPONSE actPgmVirtualIncActiveAmpStepIndexes()
{
	ACTIVE_PRGM_DEF lActPrgm;
	uint8_t curSelectedProgIndex;
	ACTIVE_PULSE_DEF *apd;
	uint8_t pulseNo;

	curSelectedProgIndex = pgm_SelectedProgramNumber() - 1;
	// Read the current active settings for the current selected program
	if ( ERROR_RETURN_VALUE == nvReadActPrgmDef(curSelectedProgIndex, &lActPrgm) )
	{
		return (RESP_READ_FAILED);
	}

	for(pulseNo=0; pulseNo<NUM_PULSES; pulseNo++) 
	{
		if ( !pgm_IsPulseNumberValid(pulseNo))
		{
			//we are done - no more valid pulses - break out of the for loop
			break;
		}
		else
		{
			apd = &(activePgm.activePulseDef[pulseNo]);

			// if the virtual index is below one just increment it
			if (apd->curVirtualAmplitudeStepIndex < PULSE_OUTPUT_AMPL_STEP_MIN)
			{
				apd->curVirtualAmplitudeStepIndex++;
				lActPrgm.activePulseDef[pulseNo].curVirtualAmplitudeStepIndex = apd->curVirtualAmplitudeStepIndex;
				continue;
			}

			// if the pulse index is at its high limit then don't increment it
			// but increment the virtual index as long as the virtual index doesn't
			// go over the virtual high limit
			if (apd->curAmplitudeStepIndex >= PULSE_OUTPUT_AMPL_STEP_MAX)
			{
				if (apd->curVirtualAmplitudeStepIndex < PULSE_VIRTUAL_AMPL_STEP_MAX)
				{
					 apd->curVirtualAmplitudeStepIndex++;
					 lActPrgm.activePulseDef[pulseNo].curVirtualAmplitudeStepIndex = apd->curVirtualAmplitudeStepIndex;
				}
				continue;
			}

			// otherwise increment both indexes
			apd->curAmplitudeStepIndex++;
			apd->curVirtualAmplitudeStepIndex++;
			lActPrgm.activePulseDef[pulseNo].curAmplitudeStepIndex = apd->curAmplitudeStepIndex;
			lActPrgm.activePulseDef[pulseNo].curVirtualAmplitudeStepIndex = apd->curVirtualAmplitudeStepIndex;
		}
	}

	// Write out the modified values to the NV RAM
	if ( ERROR_RETURN_VALUE == nvWriteActPrgmDef(curSelectedProgIndex, &lActPrgm) )
	{
		return (RESP_WRITE_FAILED);
	}

	return (GEN_SUCCESS);

}

/**
 * Decrements the active Amplitude Step Index and adjusts the Virtual Amplitude Step Index
 * accordingly for each valid pulse in the program.  
 * This 'Virtual' function is used when adjusting Program Amplitude.
 * Operates on the currently selected program
 * and updates the Active Program settings in NV RAM.
 * 
 * \param pulseNumber - the number of the pulse definition to set to  [0..3]
 * 
 * \return	OK_RETURN_VALUE(0) for success, or
 *          ERROR_RETURN_VALUE(-1) if there was a failure
 */
RESPONSE actPgmVirtualDecActiveAmpStepIndexes()
{
	ACTIVE_PRGM_DEF lActPrgm;
	ACTIVE_PULSE_DEF *apd;
	uint8_t pulseNo;
	uint8_t curSelectedProgIndex;

	curSelectedProgIndex = pgm_SelectedProgramNumber() - 1;
	// Read the current active settings for the current selected program
	if ( ERROR_RETURN_VALUE == nvReadActPrgmDef(curSelectedProgIndex, &lActPrgm) )
	{
		return (RESP_READ_FAILED);
	}

	for(pulseNo=0; pulseNo<NUM_PULSES; pulseNo++) 
	{
		if ( !pgm_IsPulseNumberValid(pulseNo))
		{
			//we are done - no more valid pulses - break out of the for loop
			break;
		}
		else
		{
			apd = &(activePgm.activePulseDef[pulseNo]);

			// if the virtual index is above the max just decrement it
			if (apd->curVirtualAmplitudeStepIndex > PULSE_OUTPUT_AMPL_STEP_MAX)
			{
				apd->curVirtualAmplitudeStepIndex--;
				lActPrgm.activePulseDef[pulseNo].curVirtualAmplitudeStepIndex = apd->curVirtualAmplitudeStepIndex;
				continue;
			}

			// if the pulse index is at its low limit then don't decrement it
			// but decrement the virtual index as long as the virtual index doesn't
			// go under the virtual low limit
			if (apd->curAmplitudeStepIndex <= PULSE_OUTPUT_AMPL_STEP_MIN)
			{
				if (apd->curVirtualAmplitudeStepIndex > PULSE_VIRTUAL_AMPL_STEP_MIN)
				{
					 apd->curVirtualAmplitudeStepIndex--;
					 lActPrgm.activePulseDef[pulseNo].curVirtualAmplitudeStepIndex = apd->curVirtualAmplitudeStepIndex;
				}
				continue;
			}

			// otherwise decrement both indexes
			apd->curAmplitudeStepIndex--;
			apd->curVirtualAmplitudeStepIndex--;
			lActPrgm.activePulseDef[pulseNo].curAmplitudeStepIndex = apd->curAmplitudeStepIndex;
			lActPrgm.activePulseDef[pulseNo].curVirtualAmplitudeStepIndex = apd->curVirtualAmplitudeStepIndex;
		}
	}

	// Write out the modified values to the NV RAM
	if ( ERROR_RETURN_VALUE == nvWriteActPrgmDef(curSelectedProgIndex, &lActPrgm) )
	{
		return (RESP_WRITE_FAILED);
	}

	return (GEN_SUCCESS);

}

/**
 * Determines if all the valid pulses in a program are at their max step index.
 * Operates on the currently selected program.
 * 
 * \return	true if all valid pulses in the program are at their max step index
 *			false if any valid pulse is below the max step index
 */
bool actPgmAllActivePulseAmpsAtMax()
{
	uint8_t pulseNo;

	for(pulseNo = 0; pulseNo<NUM_PULSES; pulseNo++) 
	{
		if (pgm_IsPulseNumberValid(pulseNo))
		{
			if (activePgm.activePulseDef[pulseNo].curAmplitudeStepIndex
					< PULSE_OUTPUT_AMPL_STEP_MAX)
			{
				// we found a pulse amplitude index not at max
				return (false);
			}
		}
		else
		{
			break;
		}
	}

	return (true);

}

/**
 * Determines if all the valid pulses in a program are at their min step index.
 * Operates on the currently selected program.
 * 
 * \return	true if all valid pulses in the program are at their min step index
 *			false if any valid pulse is above the min step index
 */
bool actPgmAllActivePulseAmpsAtMin()
{
	uint8_t pulseNo;

	for(pulseNo = 0; pulseNo<NUM_PULSES; pulseNo++) 
	{
		if (pgm_IsPulseNumberValid(pulseNo))
		{
			if (activePgm.activePulseDef[pulseNo].curAmplitudeStepIndex
					> PULSE_OUTPUT_AMPL_STEP_MIN)
			{
				// we found a pulse amplitude index not at min
				return (false);
			}
		}
		else
		{
			break;
		}
	}

	return (true);

}

/**
 * Determines if any the valid pulses in a program are at their max virtual step index.
 * Operates on the currently selected program.
 * 
 * \return	true if any valid pulse in the program is at its max virtual step index
 *			false if all valid pulses are below the max virtual step index
 */
bool actPgmAnyActiveVirtualPulseAmpAtMax()
{
	uint8_t pulseNo;

	for(pulseNo = 0; pulseNo<NUM_PULSES; pulseNo++) 
	{
		if (pgm_IsPulseNumberValid(pulseNo))
		{
			if (activePgm.activePulseDef[pulseNo].curVirtualAmplitudeStepIndex
					>= PULSE_VIRTUAL_AMPL_STEP_MAX)
			{
				// we found a virtual pulse amplitude index at max
				return (true);
			}
		}
		else
		{
			break;
		}
	}

	return (false);

}

/**
 * Determines if any the valid pulses in a program are at their min virtual step index.
 * Operates on the currently selected program.
 * 
 * \return	true if any valid pulse in the program is at its min virtual step index
 *			false if all valid pulses are above their min virtual step index
 */
bool actPgmAnyActiveVirtualPulseAmpAtMin()
{
	uint8_t pulseNo;

	for(pulseNo = 0; pulseNo<NUM_PULSES; pulseNo++) 
	{
		if (pgm_IsPulseNumberValid(pulseNo))
		{
			if (activePgm.activePulseDef[pulseNo].curVirtualAmplitudeStepIndex
					<= PULSE_VIRTUAL_AMPL_STEP_MIN)
			{
				// we found a virtual pulse amplitude index at min
				return (true);
			}
		}
		else
		{
			break;
		}
	}

	return (false);

}


