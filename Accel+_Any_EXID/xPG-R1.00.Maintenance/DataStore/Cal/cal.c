/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description:
 *	
 ***************************************************************************/

#include "cal.h"
#include "DataStore/Common/flashstore.h"

bool calIsChannelCalsValid(void)
{
	return flashIsChannelCalsValid();
}

bool calIsChannelCalsBackupValid(void)
{
	return flashIsChannelCalsBackupValid();
}

RESPONSE calSetChannelCals(const CHANNEL_CALS * cal)
{
	CHANNEL_CALIBRATION channelCals = *flashChannelCals();

	memcpy(&(channelCals.channelCalTable[cal->index].channelCalSinking), (void*)cal->channelCalSinking, sizeof(uint16_t)*NUM_CHANNELS);
	memcpy(&(channelCals.channelCalTable[cal->index].channelCalSourcing), (void*)cal->channelCalSourcing, sizeof(uint16_t)*NUM_CHANNELS);

	return flashWriteChannelCals(&channelCals);
}

RESPONSE calSetPrimaryChannelCals(const CHANNEL_CALIBRATION * cal)
{
	return flashWritePrimaryChannelCals(cal);
}

void calGetChannelCals(CHANNEL_CALS* cals, uint8_t tableIndex)
{
	CHANNEL_CALIBRATION channelCals = *flashChannelCals();

	cals->index = tableIndex;
	memcpy(cals->channelCalSourcing, &(channelCals.channelCalTable[tableIndex]), sizeof(CHANNEL_CALS_TABLE));

	return;
}

const CHANNEL_CALIBRATION *calGetChannelCalsBackup(void)
{
	return flashChannelCalsBackup();
}

uint16_t calLookUpSourcingCal(CHANNEL_MAX_AMP_INDEX ref, int chan)
{
	return flashChannelCals()->channelCalTable[ref].channelCalSourcing[chan];
}

uint16_t calLookUpSinkingCal(CHANNEL_MAX_AMP_INDEX ref, int chan)
{
	return flashChannelCals()->channelCalTable[ref].channelCalSinking[chan];
}

bool calIsHVCalsValid(void)
{
	return flashIsHVCalsValid();
}

bool calIsHVCalsBackupValid(void)
{
	return flashIsHVCalsBackupValid();
}

RESPONSE calSetHVCals(const HV_CAL_PARAMS * cal)
{
	return flashWriteHVCals(cal);
}

RESPONSE calSetPrimaryHVCals(const HV_CAL_PARAMS * cal)
{
	return flashWritePrimaryHVCals(cal);
}

const HV_CAL_PARAMS *calGetHVCals(void)
{
	return flashHVCals();
}

const HV_CAL_PARAMS *calGetHVCalsBackup(void)
{
	return flashHVCalsBackup();
}

bool calIsGenCalsValid(void)
{
	return flashIsGenCalsValid();
}

bool calIsGenCalsBackupValid(void)
{
	return flashIsGenCalsBackupValid();
}

RESPONSE calSetGenCals(const GEN_CALS *list)
{
	return flashWriteGenCals(list);
}

RESPONSE calSetPrimaryGenCals(const GEN_CALS *list)
{
	return flashWritePrimaryGenCals(list);
}

GEN_CALS const *calGetGenCals(void)
{
	return flashGenCals();
}

GEN_CALS const *calGetGenCalsBackup(void)
{
	return flashGenCalsBackup();
}
