/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: DataStore internal functions to access flash storage
 *	
 ***************************************************************************/



#ifndef FLASHSTORE_H_
#define FLASHSTORE_H_

#include <stdbool.h>

/// \defgroup flashStore Flash Store
/// \ingroup dataStoreCommon
/// @{

#include "Common/Protocol/cmndStimAsicHVCals.h"
#include "Common/Protocol/cmndGetSetChannelCals.h"
#include "Common/Protocol/cmndSetXpgIdent.h"
#include "Common/Protocol/trim_list.h"
#include "Common/Protocol/gen_cals.h"
#include "Common/Protocol/response_code.h"
#include "dataStoreXpgIdentity.h"
#include "prgmConst.h"
#include "pulseConst.h"
#include "cal.h"

/**
 * Checks the channel calibration structure in flash for validity.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \returns false if the channel calibration structure is corrupted, true otherwise.
 */
bool flashIsChannelCalsValid(void);

/**
 * Checks the backup channel calibration structure in flash for validity.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \returns false if the channel calibration structure is corrupted, true otherwise.
 */
bool flashIsChannelCalsBackupValid(void);

/**
 * Write a channel primary and backup calibration structure to permanent storage in flash.
 *
 * Does an intermediate copy to a buffer on the stack in order to attach the CRC.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \param cal Points to the channel calibration data to be stored.
 * \returns A response code from the MICS protocol.
 */
RESPONSE flashWriteChannelCals(const CHANNEL_CALIBRATION * cal);

/**
 * Write a primary channel calibration structure to permanent storage in flash.
 *
 * Does an intermediate copy to a buffer on the stack in order to attach the CRC.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \param cal Points to the channel calibration data to be stored.
 * \returns A response code from the MICS protocol.
 */
RESPONSE flashWritePrimaryChannelCals(const CHANNEL_CALIBRATION * cal);

 /**
  * Return a pointer to the channel calibration structure in flash.
  *
  * \warning This is a back-end function for the datastore. No code outside of the
  * datastore should call it. Use a public datastore API instead.
  *
  * \returns pointer to channel calibration structure
  */
const CHANNEL_CALIBRATION *flashChannelCals(void);

/**
 * Return a pointer to the backup channel calibration structure in flash.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \returns pointer to channel calibration structure
 */
const CHANNEL_CALIBRATION *flashChannelCalsBackup(void);

/**
 * Checks the high voltage (HV) calibration structure in flash for validity.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \returns false if the HV calibration structure is corrupted, true otherwise.
 */
bool flashIsHVCalsValid(void);

/**
 * Checks the high voltage (HV) calibration backup structure in flash for validity.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \returns false if the HV calibration structure is corrupted, true otherwise.
 */
bool flashIsHVCalsBackupValid(void);

/**
 * Write a primary and backup high voltage (HV) calibration structure to permanent
 * storage in flash.
 *
 * Does an intermediate copy to a buffer on the stack in order to attach the CRC.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \param cal Points to the HV calibration data to be stored.
 * \returns A response code from the MICS protocol.
 */
RESPONSE flashWriteHVCals(const HV_CAL_PARAMS * cal);

/**
 * Write a primary high voltage (HV) calibration structure to permanent
 * storage in flash.
 *
 * Does an intermediate copy to a buffer on the stack in order to attach the CRC.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \param cal Points to the HV calibration data to be stored.
 * \returns A response code from the MICS protocol.
 */
RESPONSE flashWritePrimaryHVCals(const HV_CAL_PARAMS * cal);

/**
 * Return a pointer to the high voltage (HV) calibration structure in flash.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \returns pointer to HV calibration structure
 */
const HV_CAL_PARAMS *flashHVCals(void);

/**
 * Return a pointer to the high voltage (HV) calibration backup structure in flash.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \returns pointer to HV calibration structure
 */
const HV_CAL_PARAMS *flashHVCalsBackup(void);

/**
 * Checks the general calibrations data structure in flash for validity.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \returns true if the calibrations are uncorrupted, false otherwise.
 */
bool flashIsGenCalsValid(void);

/**
 * Checks the general calibrations backup data structure in flash for validity.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \returns true if the calibrations are uncorrupted, false otherwise.
 */
bool flashIsGenCalsBackupValid(void);

/**
 * Write a trim list to permanent storage in flash.
 *
 * Does an intermediate copy to a buffer on the stack in order to attach the CRC.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \param list  Points to the HV calibration data to be stored.
 * 				The listID member identifies which trim list is to be written.
 * \returns A MICS protocol response code.
 */
RESPONSE flashWriteGenCals(const GEN_CALS *list);

/**
 * Write a primary general calibration permanent storage in flash.
 *
 * Does an intermediate copy to a buffer on the stack in order to attach the CRC.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \param list  Points to the HV calibration data to be stored.
 * 				The listID member identifies which trim list is to be written.
 * \returns A MICS protocol response code.
 */
RESPONSE flashWritePrimaryGenCals(const GEN_CALS *list);

/**
 * Return a pointer to the primary copy of the general calibrations data
 * structure in flash.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \returns pointer to the data structure.
 */
GEN_CALS const *flashGenCals(void);

/**
 * Return a pointer to the backup copy of the general calibrations data
 * structure in flash.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \returns pointer to the data structure.
 */
GEN_CALS const *flashGenCalsBackup(void);

/**
 * Checks the xPG Info structure in flash for validity.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \returns false if the structure is corrupted, true otherwise.
 */
bool flashIsXpgInfoValid(void);

/**
 * Checks the xPG Info backup structure in flash for validity.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \returns false if the structure is corrupted, true otherwise.
 */
bool flashIsXpgInfoBackupValid(void);

/**
 * This function writes the xpg info block values to flash.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \param pXpgIdentity	Pointer to the buffer with the info block values
 *
 * \return Returns an error code for the whole process (0xFF = success)
 */
RESPONSE flashWriteXpgInfo(const DS_XPG_IDENTITY * pXpgIdentity);

/**
 * This function writes the only the primary xpg info block values to flash.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \param pXpgIdentity	Pointer to the buffer with the info block values
 *
 * \return Returns an error code for the whole process (0xFF = success)
 */
RESPONSE flashWritePrimaryXpgInfo(const DS_XPG_IDENTITY * pXpgIdentity);

/**
 * Return a pointer to the xPG Info structure in flash.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \returns pointer to xPG Info structure
 */
const DS_XPG_IDENTITY *flashXpgInfo(void);

/**
 * Return a pointer to the backup xPG Info structure in flash.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \returns pointer to xPG Info structure
 */
const DS_XPG_IDENTITY *flashXpgInfoBackup(void);

/**
 * Checks a trim list in flash for validity.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \param id  ID code for the trim list
 *
 * \returns true if the trim list is uncorrupted, false otherwise.
 */
bool flashIsTrimListValid(enum TRIM_LIST_ID id);

/**
 * Checks a trim list backup storage in flash for validity.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \param id  ID code for the trim list
 *
 * \returns true if the trim list is uncorrupted, false otherwise.
 */
bool flashIsTrimListBackupValid(enum TRIM_LIST_ID id);

/**
 * Return a pointer to a trim list structure in flash.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \param id ID of the trim list
 *
 * \returns pointer to trim list, or NULL for an illegal trim list id
 */
RESPONSE flashWriteTrimList(const TRIM_LIST *list);

/**
 * Writes the primary trim list to flash without writing the backup.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \param list pointer to the trim list
 *
 * \returns pointer to trim list, or NULL for an illegal trim list id
 */
RESPONSE flashWritePrimaryTrimList(const TRIM_LIST *list);

/**
 * Writes the trim list data to the backup store without writing to the primary.
 */
RESPONSE flashWriteBackupTrimList(const TRIM_LIST *list);

/**
 * Write a trim list to permanent storage in flash.
 *
 * Does an intermediate copy to a buffer on the stack in order to attach the CRC.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \param list  Points to the HV calibration data to be stored.
 * 				The listID member identifies which trim list is to be written.
 * \returns A response code from the MICS protocol.
 */
TRIM_LIST const *flashTrimList(enum TRIM_LIST_ID id);

/**
 * Write a trim list backup to permanent storage in flash.
 *
 * Does an intermediate copy to a buffer on the stack in order to attach the CRC.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \param list  Points to the HV calibration data to be stored.
 * 				The listID member identifies which trim list is to be written.
 * \returns A response code from the MICS protocol.
 */
TRIM_LIST const *flashTrimListBackup(enum TRIM_LIST_ID id);

/**
 * Checks the program constants data structure in flash for validity.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \returns true if the program constants are uncorrupted, false otherwise.
 */
bool flashIsPrgmConstValid(void);


/**
 * Checks the program constants backup data structure in flash for validity.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \returns true if the program constants backup is uncorrupted, false otherwise.
 */
bool flashIsPrgmConstBackupValid(void);


/**
 * Return a pointer to a program constants structure in flash.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \param prgmConst pointer to the program constants structure in flash
 *
 * \returns Response Code
 */
RESPONSE flashWritePrgmConst(const PRGM_CONST *prgmConst);

/**
 * Write the primary program constants structure to flash
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \param prgmConst pointer to the program constants structure in flash
 *
 * \returns Response Code
 */
RESPONSE flashWritePrimaryPrgmConst(const PRGM_CONST *prgmConst);

/**
 * Return a pointer to the program constants data structure in flash.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \returns pointer to the data structure.
 */
PRGM_CONST const *flashPrgmConst(void);


/**
 * Return a pointer to the program constants backup data structure in flash.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \returns pointer to the backup data structure.
 */
PRGM_CONST const *flashPrgmConstBackup(void);


/**
 * Checks the pulse constants data structure in flash for validity.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \returns true if the pulse constants are uncorrupted, false otherwise.
 */
bool flashIsPulseConstValid(void);

/**
 * Checks the pulse constants backup data structure in flash for validity.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \returns true if the pulse constants are uncorrupted, false otherwise.
 */
bool flashIsPulseConstBackupValid(void);

/**
 * Return a pointer to a pulse constants structure in flash.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \param pulseConst pointer to the pulse constants structure in flash
 *
 * \returns pointer to trim list, or NULL for an illegal trim list id
 */
RESPONSE flashWritePulseConst(const PULSE_CONST *pulseConst);

/**
 * Writes the primary copy of the pulse constants to flash
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \param pulseConst pointer to the pulse constants structure in flash
 *
 * \returns pointer to trim list, or NULL for an illegal trim list id
 */
RESPONSE flashWritePrimaryPulseConst(const PULSE_CONST *pulseConst);

/**
 * Return a pointer to the pulse constants data structure in flash.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \returns pointer to the data structure.
 */
PULSE_CONST const *flashPulseConst(void);

/**
 * Return a pointer to the pulse constants backup data structure in flash.
 *
 * \warning This is a back-end function for the datastore. No code outside of the
 * datastore should call it. Use a public datastore API instead.
 *
 * \returns pointer to the data structure.
 */
PULSE_CONST const *flashPulseConstBackup(void);

/// @}

#endif /* FLASHSTORE_H_ */
