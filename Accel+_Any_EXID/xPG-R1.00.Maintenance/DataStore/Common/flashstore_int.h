/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Header file for Flash storage module internal functions.
 *	
 ***************************************************************************/



#ifndef FLASHSTORE_INT_H_
#define FLASHSTORE_INT_H_

#include <stdint.h>
#include "Common/Protocol/cmndGetSetChannelCals.h"
#include "Common/Protocol/cmndStimAsicHVCals.h"
#include "Common/Protocol/cmndSetXpgIdent.h"
#include "dataStoreXpgIdentity.h"
#include "Common/Protocol/trim_list.h"
#include "Common/Protocol/gen_cals.h"
#include "Common/Protocol/response_code.h"
#include "prgmConst.h"
#include "pulseConst.h"
#include "cal.h"

/**
 * define storage for channel calibrations
 */
typedef struct F_CHANNEL_CALS {
	CHANNEL_CALIBRATION channelCals;     	///< source and sink channel calibrations
    uint16_t crc;               			///< crc for channel calibrations
} F_CHANNEL_CALS;

/**
 * define storage for stim asic high voltage calibrations
 */
typedef struct F_HV_CALS {
    HV_CAL_PARAMS hvCals;       ///< stim asic boost converter calibrations
    uint16_t crc;               ///< crc for calibration data block
} F_HV_CALS;

/**
 * define storage for XPG Identity data structure
 */
typedef struct F_XPG_IDENTITY {
    DS_XPG_IDENTITY xpgIdentity;   ///< Identity information
    uint16_t crc;                  ///< crc for xPG Identity data block
} F_XPG_IDENTITY;

/**
 * define storage for the Trim List data structure
 */
typedef struct F_TRIM_LIST {
	TRIM_LIST tl;
	uint16_t crc;
} F_TRIM_LIST;

/**
 * define storage for the General Calibrations data structure
 */
typedef struct F_GEN_CALS {
	GEN_CALS gc;
	uint16_t crc;
} F_GEN_CALS;

/**
 * define storage for the Program Constants data structure
 */
typedef struct F_PRGM_CONST {
	PRGM_CONST pc;
	uint16_t crc;
} F_PRGM_CONST;

/**
 * define storage for the Pulse Constants data structure
 */
typedef struct F_PULSE_CONST {
	PULSE_CONST pc;
	uint16_t crc;
} F_PULSE_CONST;

RESPONSE flashGenericWrite(const void *dest, const void *data, void *buf, size_t len, uint16_t *pcrc);


#endif /* FLASHSTORE_INT_H_ */
