/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description:  Flash storage module internal functions.
 *	
 ***************************************************************************/

#include <stdint.h>
#include <string.h>

#include "flashstore_int.h"
#include "Flash.h"
#include "StimManager.h"
#include "DataStore/Common/crc16.h"


/**
 * Writes a block of data to flash.
 *
 * \param dest	The address in flash to write
 * \param data 	The data to write to flash
 * \param buf	A temporary buffer large enough to hold the written data and CRC.
 * \param len 	The length of the structure to write to flash, not including the CRC.
 * \param crc	A pointer to the CRC portion of the temporary buffer
 */

RESPONSE flashGenericWrite(const void *dest, const void *data, void *buf, size_t len, uint16_t *pcrc)
{
    //stim must not be on (can't save to flash)
    if (isStimOn())
    {
        return CMND_RESP_BUSY;
    }

    memcpy(buf, data, len);
    *pcrc = crc16(buf, len);
    return flashCopyDataToFlash(buf, len + 2, (void *)dest);
}

