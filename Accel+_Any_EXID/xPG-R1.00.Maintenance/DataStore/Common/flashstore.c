/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Flash storage module functions.
 *	
 ***************************************************************************/
//When the next line is not commented out, the image does not include
//initialization for the xPG ID

#include <stddef.h>
#include <stdio.h>

#include "flashstore.h"
#include "flashinit.h"
#include "flashstore_int.h"
#include "version.h"
#include "DataStore/Common/crc16.h"
#include "StimManager.h"
#include "dataStoreXpgIdentity.h"


#ifdef TEST
#define STATIC
#else
#define STATIC	static
#endif


/**
 * allocates and initializes the channel calibrations
 *
 * The first 26 values are the calibrations for when the channel
 * is sourcing current.  They are initialized to the full-scale value.
 * The next 26 values are the calibrations for when the channel
 * is sinking current.  They are initialized to the full-scale value.
 */
#pragma DATA_SECTION(fChannelCals, ".chan_cals")
#pragma DATA_SECTION(fChannelCalsBackup, ".chan_cals_backup")
#ifdef INITIALIZE_CHANNEL_CALS
STATIC F_CHANNEL_CALS const fChannelCals = CHANNEL_CALS_INIT_DATA;
STATIC F_CHANNEL_CALS const fChannelCalsBackup = CHANNEL_CALS_INIT_DATA;
#else
STATIC F_CHANNEL_CALS const fChannelCals;
STATIC F_CHANNEL_CALS const fChannelCalsBackup;
#endif

/**
 * The stim asic hv calibrations list the voltage output (in mV)
 * for the 64 settings of the BC_CTRL0 register.
 *
 * These default values are straight from the stim asic spec,
 * EESP 0085 Table 36
 */
#pragma DATA_SECTION(fHVCals, ".hv_cals")
#pragma DATA_SECTION(fHVCalsBackup, ".hv_cals_backup")
#ifdef INITIALIZE_HV_CALS
STATIC F_HV_CALS const fHVCals = HV_CALS_INIT_DATA;
STATIC F_HV_CALS const fHVCalsBackup = HV_CALS_INIT_DATA;
#else
STATIC F_HV_CALS const fHVCals;
STATIC F_HV_CALS const fHVCalsBackup;
#endif

/**
 * initialized ipg identity storage
 */
#pragma DATA_SECTION(fXpgIdentity, ".infoD")
#pragma DATA_SECTION(fXpgIdentityBackup, ".infoB")
#ifdef INITIALIZE_XPG_INFO
/*@-stringliteralnoroom@*/
STATIC F_XPG_IDENTITY const fXpgIdentity = XPG_INFO_INIT_DATA;
STATIC F_XPG_IDENTITY const fXpgIdentityBackup = XPG_INFO_INIT_DATA;
/*@=stringliteralnoroom@*/
#else
STATIC F_XPG_IDENTITY const fXpgIdentity;
STATIC F_XPG_IDENTITY const fXpgIdentityBackup;
#endif

// Suppress warning about uninitialized const variables.
// These variables are set during manufacturing.
#pragma diag_suppress=1110

/// Trim List for the ZL7010x MICS radio chip
#pragma DATA_SECTION(fTrimZL, ".trim_z")
#pragma DATA_SECTION(fTrimZLbackup, ".trim_z_backup")
#ifdef INITIALIZE_TRIMS
STATIC F_TRIM_LIST const fTrimZL = TRIM_Z_INIT_DATA;
STATIC F_TRIM_LIST const fTrimZLbackup = TRIM_Z_INIT_DATA;
#else
STATIC F_TRIM_LIST const fTrimZL;
STATIC F_TRIM_LIST const fTrimZLbackup;
#endif

/// Trim List for the Saturn stimulation ASIC
#pragma DATA_SECTION(fTrimSaturn, ".trim_s")
#pragma DATA_SECTION(fTrimSaturnBackup, ".trim_s_backup")
#ifdef INITIALIZE_TRIMS
STATIC F_TRIM_LIST const fTrimSaturn = TRIM_S_INIT_DATA;
STATIC F_TRIM_LIST const fTrimSaturnBackup = TRIM_P_INIT_DATA;
#else
STATIC F_TRIM_LIST const fTrimSaturn;
STATIC F_TRIM_LIST const fTrimSaturnBackup;
#endif

/// Trim List for the Pluto power ASIC
#pragma DATA_SECTION(fTrimPluto, ".trim_p")
#pragma DATA_SECTION(fTrimPlutoBackup, ".trim_p_backup")
#ifdef INITIALIZE_TRIMS
STATIC F_TRIM_LIST const fTrimPluto = TRIM_P_INIT_DATA;
STATIC F_TRIM_LIST const fTrimPlutoBackup = TRIM_P_INIT_DATA;
#else
STATIC F_TRIM_LIST const fTrimPluto;
STATIC F_TRIM_LIST const fTrimPlutoBackup;
#endif

/// General calibrations data structure
#pragma DATA_SECTION(fGenCals, ".gen_cals")
#pragma DATA_SECTION(fGenCalsBackup, ".gen_cals_backup")
#ifdef INITIALIZE_GEN_CALS
#ifdef EPG
STATIC F_GEN_CALS const fGenCals = GEN_CAL_INIT_DATA_EPG;
STATIC F_GEN_CALS const fGenCalsBackup = GEN_CAL_INIT_DATA_EPG;
#else
STATIC F_GEN_CALS const fGenCals = GEN_CAL_INIT_DATA;
STATIC F_GEN_CALS const fGenCalsBackup = GEN_CAL_INIT_DATA;
#endif
#else
STATIC F_GEN_CALS const fGenCals;
STATIC F_GEN_CALS const fGenCalsBackup;
#endif

/// Program Constants data structure
#pragma DATA_SECTION(fPrgmConst, ".prgrm_const")
#pragma DATA_SECTION(fPrgmConstBackup, ".prgrm_const_backup")
#ifdef INITIALIZE_CONSTANTS
STATIC F_PRGM_CONST const fPrgmConst = PRGM_CONST_INIT_DATA;
STATIC F_PRGM_CONST const fPrgmConstBackup = PRGM_CONST_INIT_DATA;
#else
STATIC F_PRGM_CONST const fPrgmConst;
STATIC F_PRGM_CONST const fPrgmConstBackup;
#endif

/// Pulse Constants data structure
#pragma DATA_SECTION(fPulseConst, ".pulse_const")
#pragma DATA_SECTION(fPulseConstBackup, ".pulse_const_backup")
#ifdef INITIALIZE_CONSTANTS
STATIC F_PULSE_CONST const fPulseConst = PULSE_CONST_INIT_DATA;
STATIC F_PULSE_CONST const fPulseConstBackup = PULSE_CONST_INIT_DATA;
#else
STATIC F_PULSE_CONST const fPulseConst;
STATIC F_PULSE_CONST const fPulseConstBackup;
#endif


/*
 * DataStore backend API. See flashstore.h for documentation.
 */
bool flashIsChannelCalsValid(void)
{
    return crc16(&fChannelCals, sizeof(F_CHANNEL_CALS)) == 0;
}


/*
 * DataStore backend API. See flashstore.h for documentation.
 */
bool flashIsChannelCalsBackupValid(void)
{
    return crc16(&fChannelCalsBackup, sizeof(F_CHANNEL_CALS)) == 0;
}


/*
 * DataStore backend API. See flashstore.h for documentation.
 */
RESPONSE flashWritePrimaryChannelCals(const CHANNEL_CALIBRATION * cal)
{
    F_CHANNEL_CALS ccs;
	return flashGenericWrite(&fChannelCals, cal, &ccs.channelCals, sizeof(ccs.channelCals), &ccs.crc);
}


/*
 * DataStore backend API. See flashstore.h for documentation.
 */
RESPONSE flashWriteChannelCals(const CHANNEL_CALIBRATION * cal)
{
    F_CHANNEL_CALS ccs;
    RESPONSE response;

    response = flashGenericWrite(&fChannelCals, cal, &ccs.channelCals, sizeof(ccs.channelCals), &ccs.crc);

    if (response != GEN_SUCCESS)
    {
    	return response;
    }
    else
    {
    	response = flashGenericWrite(&fChannelCalsBackup, cal, &ccs.channelCals, sizeof(ccs.channelCals), &ccs.crc);
    }

	return response;
}


/*
 * DataStore backend API. See flashstore.h for documentation.
 */
const CHANNEL_CALIBRATION *flashChannelCals(void)
{
    return &fChannelCals.channelCals;
}


/*
 * DataStore backend API. See flashstore.h for documentation.
 */
const CHANNEL_CALIBRATION *flashChannelCalsBackup(void)
{
    return &fChannelCalsBackup.channelCals;
}


/*
 * DataStore backend API. See flashstore.h for documentation.
 */
bool flashIsHVCalsValid(void)
{
    return crc16(&fHVCals, sizeof(fHVCals)) == 0;
}

/*
 * DataStore backend API. See flashstore.h for documentation.
 */
bool flashIsHVCalsBackupValid(void)
{
    return crc16(&fHVCalsBackup, sizeof(fHVCals)) == 0;
}

/*
 * DataStore backend API. See flashstore.h for documentation.
 */
RESPONSE flashWriteHVCals(const HV_CAL_PARAMS *cal)
{
    F_HV_CALS hvs;
    RESPONSE response;

    response = flashGenericWrite(&fHVCals, cal, &hvs.hvCals, sizeof(hvs.hvCals), &hvs.crc);

    if (response == GEN_SUCCESS)
    {
    	response = flashGenericWrite(&fHVCalsBackup, cal, &hvs.hvCals, sizeof(hvs.hvCals), &hvs.crc);
    }
    else
    {
    	return response;
    }

	return response;
}

/*
 * DataStore backend API. See flashstore.h for documentation.
 */
RESPONSE flashWritePrimaryHVCals(const HV_CAL_PARAMS *cal)
{
    F_HV_CALS hvs;

	return flashGenericWrite(&fHVCals, cal, &hvs.hvCals, sizeof(hvs.hvCals), &hvs.crc);
}

/*
 * DataStore backend API. See flashstore.h for documentation.
 */
const HV_CAL_PARAMS *flashHVCals(void)
{
    return &fHVCals.hvCals;
}

/*
 * DataStore backend API. See flashstore.h for documentation.
 */
const HV_CAL_PARAMS *flashHVCalsBackup(void)
{
    return &fHVCalsBackup.hvCals;
}

/*
 * DataStore backend API. See flashstore.h for documentation.
 */
bool flashIsXpgInfoValid(void)
{
	return crc16(&fXpgIdentity, sizeof(fXpgIdentity)) == 0;
}

/*
 * DataStore backend API. See flashstore.h for documentation.
 */
bool flashIsXpgInfoBackupValid(void)
{
	return crc16(&fXpgIdentityBackup, sizeof(fXpgIdentityBackup)) == 0;
}

/*
 * DataStore backend API. See flashstore.h for documentation.
 */
RESPONSE flashWriteXpgInfo(const DS_XPG_IDENTITY *p)
{
    F_XPG_IDENTITY f;
    RESPONSE response = GEN_SUCCESS;

    response = flashGenericWrite(&fXpgIdentity, p, &f.xpgIdentity, sizeof(f.xpgIdentity), &f.crc);
    if (response != GEN_SUCCESS)
    {
    	return response;
    }

    return flashGenericWrite(&fXpgIdentityBackup, p, &f.xpgIdentity, sizeof(f.xpgIdentity), &f.crc);
}

/*
 * DataStore backend API. See flashstore.h for documentation.
 */
RESPONSE flashWritePrimaryXpgInfo(const DS_XPG_IDENTITY *p)
{
    F_XPG_IDENTITY f;

	return flashGenericWrite(&fXpgIdentity, p, &f.xpgIdentity, sizeof(f.xpgIdentity), &f.crc);
}


/*
 * DataStore backend API. See flashstore.h for documentation.
 */
const DS_XPG_IDENTITY *flashXpgInfo(void)
{
    return &fXpgIdentity.xpgIdentity;
}

/*
 * DataStore backend API. See flashstore.h for documentation.
 */
const DS_XPG_IDENTITY *flashXpgInfoBackup(void)
{
    return &fXpgIdentityBackup.xpgIdentity;
}

/**
 * Lookup table for the trim list structures in flash.
 *
 * This speeds the process of translating from TRIM_LIST_ID to the
 * corresponding trim list.
 */

static F_TRIM_LIST const * const ftl[] = {
	&fTrimPluto,	// TRIM_PLUTO = 0
	&fTrimZL,		// TRIM_ZL = 1
	&fTrimSaturn	// TRIM_SATURN = 2
};

static F_TRIM_LIST const * const ftlBackup[] = {
	&fTrimPlutoBackup,	// TRIM_PLUTO = 0
	&fTrimZLbackup,		// TRIM_ZL = 1
	&fTrimSaturnBackup	// TRIM_SATURN = 2
};

/*
 * DataStore backend API. See flashstore.h for documentation.
 */
bool flashIsTrimListValid(enum TRIM_LIST_ID id)
{
    return crc16(ftl[id], sizeof(F_TRIM_LIST)) == 0;
}


bool flashIsTrimListBackupValid(enum TRIM_LIST_ID id)
{
    return crc16(ftlBackup[id], sizeof(F_TRIM_LIST)) == 0;
}

/*
 * DataStore backend API. See flashstore.h for documentation.
 */
TRIM_LIST const *flashTrimList(enum TRIM_LIST_ID id)
{
	return &ftl[id]->tl;
}

TRIM_LIST const *flashTrimListBackup(enum TRIM_LIST_ID id)
{
	return &ftlBackup[id]->tl;
}

/*
 * DataStore backend API. See flashstore.h for documentation.
 */
RESPONSE flashWriteTrimList(const TRIM_LIST *list)
{
    F_TRIM_LIST f;

    RESPONSE response;

	//Write the primary first.  If successful, attempt to write the backup
	response = flashGenericWrite(ftl[list->listID], list, &f.tl, sizeof(f.tl), &f.crc);

	if (response == GEN_SUCCESS)
	{
		response = flashGenericWrite(ftlBackup[list->listID], list, &f.tl, sizeof(f.tl), &f.crc);
	}
	else
	{
		//Return the primary write failure response
		return response;
	}

	//Return the backup write response
	return response;
}

/*
 * DataStore backend API. See flashstore.h for documentation.
 */
RESPONSE flashWritePrimaryTrimList(const TRIM_LIST *list)
{
    F_TRIM_LIST f;

	return 	flashGenericWrite(ftl[list->listID], list, &f.tl, sizeof(f.tl), &f.crc);
}


RESPONSE flashWriteBackupTrimList(const TRIM_LIST *list)
{
    F_TRIM_LIST f;

	return 	flashGenericWrite(ftlBackup[list->listID], list, &f.tl, sizeof(f.tl), &f.crc);
}


/*
 * DataStore backend API. See flashstore.h for documentation.
 */
bool flashIsGenCalsValid(void)
{
    return crc16(&fGenCals, sizeof(fGenCals)) == 0;
}


/*
 * DataStore backend API. See flashstore.h for documentation.
 */
bool flashIsGenCalsBackupValid(void)
{
    return crc16(&fGenCalsBackup, sizeof(fGenCals)) == 0;
}


/*
 * DataStore backend API. See flashstore.h for documentation.
 */
RESPONSE flashWriteGenCals(const GEN_CALS *genCals)
{
    F_GEN_CALS f;
    RESPONSE response;

    response = flashGenericWrite(&fGenCals, genCals, &f.gc, sizeof(f.gc), &f.crc);

    if (response == GEN_SUCCESS)
    {
    	response = flashGenericWrite(&fGenCalsBackup, genCals, &f.gc, sizeof(f.gc), &f.crc);
    }
    else
    {
    	return response;
    }

	return response;
}

/*
 * DataStore backend API. See flashstore.h for documentation.
 */
RESPONSE flashWritePrimaryGenCals(const GEN_CALS *genCals)
{
    F_GEN_CALS f;

    return flashGenericWrite(&fGenCals, genCals, &f.gc, sizeof(f.gc), &f.crc);
}

/*
 * DataStore backend API. See flashstore.h for documentation.
 */
GEN_CALS const *flashGenCals(void)
{
	return &fGenCals.gc;
}


/*
 * DataStore backend API. See flashstore.h for documentation.
 */
GEN_CALS const *flashGenCalsBackup(void)
{
	return &fGenCalsBackup.gc;
}


/*
 * DataStore backend API. See flashstore.h for documentation.
 */
bool flashIsPrgmConstValid(void)
{
    return crc16(&fPrgmConst, sizeof(fPrgmConst)) == 0;
}


/*
 * DataStore backend API. See flashstore.h for documentation.
 */
bool flashIsPrgmConstBackupValid(void)
{
    return crc16(&fPrgmConstBackup, sizeof(fPrgmConstBackup)) == 0;
}


/*
 * DataStore backend API. See flashstore.h for documentation.
 */
RESPONSE flashWritePrgmConst(const PRGM_CONST *prgmConst)
{
	F_PRGM_CONST f;
	RESPONSE response;

	//Write the primary first.  If successful, attempt to write the backup
	response = flashGenericWrite(&fPrgmConst, prgmConst, &f.pc, sizeof(f.pc), &f.crc);

	if (response == GEN_SUCCESS)
	{
		response = flashGenericWrite(&fPrgmConstBackup, prgmConst, &f.pc, sizeof(f.pc), &f.crc);
	}
	else
	{
		//Return the primary write failure response
		return response;
	}

	//Return the backup write response
	return response;
}

/*
 * DataStore backend API. See flashstore.h for documentation.
 */
RESPONSE flashWritePrimaryPrgmConst(const PRGM_CONST *prgmConst)
{
	F_PRGM_CONST f;

	return flashGenericWrite(&fPrgmConst, prgmConst, &f.pc, sizeof(f.pc), &f.crc);
}

/*
 * DataStore backend API. See flashstore.h for documentation.
 */
PRGM_CONST const *flashPrgmConst(void)
{
	return &fPrgmConst.pc;
}


/*
 * DataStore backend API. See flashstore.h for documentation.
 */
PRGM_CONST const *flashPrgmConstBackup(void)
{
	return &fPrgmConstBackup.pc;
}


/*
 * DataStore backend API. See flashstore.h for documentation.
 */
bool flashIsPulseConstValid(void)
{
    return crc16(&fPulseConst, sizeof(fPulseConst)) == 0;
}


/*
 * DataStore backend API. See flashstore.h for documentation.
 */
bool flashIsPulseConstBackupValid(void)
{
    return crc16(&fPulseConstBackup, sizeof(fPulseConstBackup)) == 0;
}


/*
 * DataStore backend API. See flashstore.h for documentation.
 */
RESPONSE flashWritePulseConst(const PULSE_CONST *pulseConst)
{
	F_PULSE_CONST f;
	RESPONSE response;

	//Write the primary first.  If successful, attempt to write the backup
	response = flashGenericWrite(&fPulseConst, pulseConst, &f.pc, sizeof(f.pc), &f.crc);

	if (response == GEN_SUCCESS)
	{
		response = flashGenericWrite(&fPulseConstBackup, pulseConst, &f.pc, sizeof(f.pc), &f.crc);
	}
	else
	{
		//Return the primary write failure response
		return response;
	}

	//Return the secondary write response
	return response;
}

/*
 * DataStore backend API. See flashstore.h for documentation.
 */
RESPONSE flashWritePrimaryPulseConst(const PULSE_CONST *pulseConst)
{
	F_PULSE_CONST f;

	return flashGenericWrite(&fPulseConst, pulseConst, &f.pc, sizeof(f.pc), &f.crc);
}

/*
 * DataStore backend API. See flashstore.h for documentation.
 */
PULSE_CONST const *flashPulseConst(void)
{
	return &fPulseConst.pc;
}


/*
 * DataStore backend API. See flashstore.h for documentation.
 */
PULSE_CONST const *flashPulseConstBackup(void)
{
	return &fPulseConstBackup.pc;
}
