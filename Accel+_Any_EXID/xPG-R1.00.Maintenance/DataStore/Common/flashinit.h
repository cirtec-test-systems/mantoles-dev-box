/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Provide initialization data for flash storage
 *					   structures for debugging.
 *
 ***************************************************************************/

#ifndef FLASHINIT_H_
#define FLASHINIT_H_

#define CHANNEL_CALS_INIT_DATA 																								\
{																															\
  {																															\
   {																														\
    {																														\
     {674,674,674,674,674,674,674,674,674,674,674,674,674,674,674,674,674,674,674,674,674,674,674,674,674,674},				\
     {655,655,655,655,655,655,655,655,655,655,655,655,655,655,655,655,655,655,655,655,655,655,655,655,655,655}				\
    },																														\
    {																														\
     {751,751,751,751,751,751,751,751,751,751,751,751,751,751,751,751,751,751,751,751,751,751,751,751,751,751},				\
     {730,730,730,730,730,730,730,730,730,730,730,730,730,730,730,730,730,730,730,730,730,730,730,730,730,730}          	\
    },																														\
    {																														\
     {813,813,813,813,813,813,813,813,813,813,813,813,813,813,813,813,813,813,813,813,813,813,813,813,813,813},          	\
     {789,789,789,789,789,789,789,789,789,789,789,789,789,789,789,789,789,789,789,789,789,789,789,789,789,789}				\
    },																														\
    {																														\
     {865,865,865,865,865,865,865,865,865,865,865,865,865,865,865,865,865,865,865,865,865,865,865,865,865,865},				\
     {840,840,840,840,840,840,840,840,840,840,840,840,840,840,840,840,840,840,840,840,840,840,840,840,840,840}				\
    },																														\
   },																														\
  },																														\
  0x9D52                																									\
}


#define HV_CALS_INIT_DATA 		\
   {							\
    {							\
     {							\
		0,						\
		0,						\
		0,						\
		0,						\
		0,						\
		0,						\
		0,						\
		0,						\
		0,						\
		0,						\
		0,						\
		4178,					\
		4573,					\
		4923,					\
		5312,					\
		5667,					\
		6087,					\
		6442,					\
		6832,					\
		7188,					\
		7598,					\
		7954,					\
		8346,					\
		8702,					\
		9120,					\
		9476,					\
		9867,					\
		10224,					\
		10636,					\
		10991,					\
		11384,					\
		11742,					\
		12153,					\
		12507,					\
		12898,					\
		13254,					\
		13665,					\
		14020,					\
		14412,					\
		14768,					\
		15187,					\
		15541,					\
		15932,					\
		16288,					\
		16701,					\
		17055,					\
		17449,					\
		17805,					\
		18232,					\
		18585,					\
		18976,					\
		19331,					\
		19744,					\
		20098,					\
		20490,					\
		20847,					\
		21267,					\
		21620,					\
		22013,					\
		22368,					\
		22782,					\
		23136,					\
		23530,					\
		23887,					\
      }							\
     },							\
    0xAFBE                      \
}

#define XPG_INFO_INIT_DATA							\
{													\
	{												\
     {												\
      {0x00, 0x30, 0x00, DEFAULT_MICS_COMPANY_ID},  \
      "2408",              							\
      "0001",     		 							\
     },												\
     FIRMWARE_VERSION								\
    },												\
    0x973B	                      					\
}

//	New trim list copied from production device 03 ED 66
// reg_rf_txrfanttune_txmode (0xF9)
// reg_rf_txrfanttune_rxmode
// reg_rf_rxrfanttune_txmode
// reg_rf_rxrfanttune_rxmode
// reg_antmatch1
// reg_antmatch2
#ifdef MICS_TUNE_AVID
#define TRIM_Z_INIT_DATA	\
{							\
	{						\
		TRIM_ZL,            \
		17,					\
		{0xC8, 0x00, 0x03,	\
		 0xD6, 0x00, 0x89,	\
		 0xC0, 0x7F, 0x80,	\
		 0x5A, 0x00, 0x0B,	\
		 0x5B, 0x00, 0x0B,	\
		 0xC0, 0x7F, 0x00,	\
		 0x51, 0x00, 0x08,	\
		 0x4D, 0x00, 0x02,	\
		 0x72, 0x00, 0x13,	\
		 0xCF, 0x00, 0x22,	\
		 0xD1, 0x00, 0x2E,	\
		 0xF9, 0x00, 0x08,  \
		 0xFA, 0x00, 0x00,  \
		 0xFB, 0x00, 0x00,  \
		 0xFC, 0x00, 0x08,  \
		 0xFD, 0x00, 0x33,  \
		 0xFE, 0x00, 0x3E}  \
	},						\
	0x16B3					\
}
#else
#ifdef MICS_TUNE_IPG
#define TRIM_Z_INIT_DATA	\
{							\
	{						\
		TRIM_ZL,			\
		6,					\
		{0xF9, 0x00, 0x08,  \
		 0xFA, 0x00, 0x08,  \
		 0xFB, 0x00, 0x00,  \
		 0xFC, 0x00, 0x00,  \
		 0xFD, 0x00, 0x1A,  \
		 0xFE, 0x00, 0x3A}  \
	},						\
	0x9448					\
}
#else
#ifdef MICS_TUNE_EPG
#define TRIM_Z_INIT_DATA	\
{							\
	{						\
		TRIM_ZL,			\
		6,					\
		{0xF9, 0x00, 0x0A,  \
		 0xFA, 0x00, 0x0A,  \
		 0xFB, 0x00, 0x00,  \
		 0xFC, 0x00, 0x00,  \
		 0xFD, 0x00, 0x04,  \
		 0xFE, 0x00, 0x06}  \
	},						\
	0xD8FB					\
}
#else
#ifdef INITIALIZE_TRIMS
#error Must compile with one of the MICS_TUNE_* directives.
#endif
#endif
#endif
#endif

#define TRIM_S_INIT_DATA								\
{														\
	{													\
		TRIM_SATURN,									\
		7,												\
		{0x70, 0x50, 0x03, 0x18, 0x30, 0x03, 0xE8}		\
	},													\
	0x0F07												\
}

#define TRIM_P_INIT_DATA	\
{							\
	{						\
		TRIM_PLUTO,			\
		0,					\
		{0}					\
	},						\
	0xEB6C					\
}

#define PRGM_CONST_INIT_DATA							\
{														\
	{													\
     0,                      							\
     {													\
       /*  0 */ 2, 3, 4, 6, 8, 10, 12, 14,       				\
       /*  8 */ 16, 18, 20, 22, 24, 26, 28, 30,  				\
       /* 16 */ 35, 40, 45, 50, 55, 60, 65, 70,  				\
       /* 24 */ 75, 80, 85, 90, 95, 100, 110, 120,       		\
       /* 32 */ 130, 140, 150, 160, 170, 180, 190, 200,  		\
       /* 40 */ 225, 250, 275, 300, 325, 350, 375, 400,  		\
       /* 48 */ 500, 600, 700, 800, 900, 1000, 1100, 1200,       \
       /* 56 */ 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000   \
      },												\
	},													\
    0xF345												\
}

#define PULSE_CONST_INIT_DATA	\
{								\
	{							\
		100,                    \
		2000,                   \
		1,                      \
		2000,                   \
		1000,                   \
		100,                    \
		1,                      \
		100,                    \
		1,                      \
		0x001F,                 \
		250,                    \
		true,                   \
		true                    \
	},							\
	0x095B						\
}

#define DEFAULT_MINIMUM_TEMP_THRESH 	51950
#define DEFAULT_WARNING_TEMP_THRESH		12765
#define DEFAULT_CRITICAL_TEMP_THRESH 	12229
#define DEFAULT_ABORT_TEMP_THRESH 		11719

#define DEFAULT_THERM_BIAS_MAXIMUM		3400
#define DEFAULT_THERM_BIAS_MINIMUM  	1000
#define DEFAULT_THERM_INPUT_MAXIMUM 	3100
#define DEFAULT_THERM_INPUT_MINIMUM 	 900
#define DEFAULT_THERM_OFFSET_MAXIMUM 	 800
#define DEFAULT_THERM_OFFSET_MINIMUM 	 400

//nominal 10mV
#define DEFAULT_BATT_INCREASE_CONST_CURRENT 8

//nominal 30mV
#define DEFAULT_BATT_INCREASE_PRE_CHARGE 	25

#define GEN_CAL_INIT_DATA					\
{											\
	{										\
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},	\
		DEFAULT_MINIMUM_TEMP_THRESH,		\
		DEFAULT_WARNING_TEMP_THRESH,		\
		DEFAULT_CRITICAL_TEMP_THRESH,		\
		DEFAULT_ABORT_TEMP_THRESH,			\
		DEFAULT_THERM_OFFSET_MINIMUM,		\
		DEFAULT_THERM_OFFSET_MAXIMUM,		\
		DEFAULT_THERM_BIAS_MINIMUM,			\
		DEFAULT_THERM_BIAS_MAXIMUM,			\
		DEFAULT_THERM_INPUT_MINIMUM,		\
		DEFAULT_THERM_INPUT_MAXIMUM,		\
		DEFAULT_BATT_INCREASE_PRE_CHARGE,	\
		DEFAULT_BATT_INCREASE_CONST_CURRENT,\
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},	\
		{3194,3112,3055,3022,2989,2211},	\
		{3174,3092,3034,3002,2969,2211},	\
		750									\
	},										\
	0x6A21									\
}

#define GEN_CAL_INIT_DATA_EPG				\
{											\
	{										\
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},	\
		DEFAULT_MINIMUM_TEMP_THRESH,		\
		DEFAULT_WARNING_TEMP_THRESH,		\
		DEFAULT_CRITICAL_TEMP_THRESH,		\
		DEFAULT_ABORT_TEMP_THRESH,			\
		0,									\
		4095,								\
		0,									\
		4095,								\
		0,									\
		4095,								\
		DEFAULT_BATT_INCREASE_PRE_CHARGE,	\
		DEFAULT_BATT_INCREASE_CONST_CURRENT,\
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},	\
		{2273,2158,2060,1896,1777,1761},	\
		{2252,2138,2039,1875,1777,1761},	\
		750									\
	},										\
	0x2FBB									\
}

#endif /* FLASHINIT_H_ */
