/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Header file / function prototype for routine
 *	for calculating CRC on a data block.
 *	
 ***************************************************************************/
#ifndef CCRC16_H_
#define CCRC16_H_

#include <stdint.h>

/// \defgroup crc CRC
/// \ingroup dataStoreCommon
/// @{

uint16_t crc16(const void *buf, uint16_t len);

/// @}

#endif /*CRC16_H_*/
