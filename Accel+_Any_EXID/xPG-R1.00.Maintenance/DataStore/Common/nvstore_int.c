/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Non-volatile memory storage module internal functions.
 *	
 ***************************************************************************/

#include "nvstore_int.h"
#include "DataStore/Common/crc16.h"

/**
 * Write a block of data to FRAM, with CRC
 * 
 * \param nvData	Address in NVRAM at which to store the data
 * \param nvCRC		Address in NVRAM at which to store the data's CRC
 * \param buf		Pointer to the data to be stored
 * \param len		Length of the data to be stored
 * \returns 	-1 if the write failed, or 0 if successful
 */
 
int nvMdl_ProtectedWrite(NVADDR nvData, NVADDR nvCRC, const void *buf, size_t len)
{
	uint16_t crc;
	int result;

	result = 0;
	
	if (nvStart() < 0)
	{
		result = -1;
	}
	else if (nvWrite(nvData, buf, len) < 0)
	{
		result = -1;
	}
	else
	{
		crc = crc16(buf, len);
		if (nvWriteWord(nvCRC, crc))
		{
			result = -1;
		}
	}
	if (nvStop() < 0)
	{
		result = -1;
	}

	return result;
}

/**
 * Read a block of data from FRAM, with CRC verification
 * 
 * \param nvData	Address in NVRAM from which to read the data
 * \param nvCRC		Address in NVRAM from which to read the data's CRC
 * \param buf		Pointer to a buffer to receive the data 
 * \param len		Length of the data to be read
 * \param pcrc		Pointer to a location to store the data's CRC. May be NULL.
 * \returns 	-1 if the write failed, or 0 if successful
 */
 
int nvMdl_ProtectedRead(NVADDR nvData, NVADDR nvCRC, void *buf, size_t len, uint16_t *pcrc)
{
	uint16_t computedCRC, memoryCRC;
	int result;
	
	result = 0;
	
	if (nvStart() < 0)
	{
		result = -1;
	}
	else
	{
		nvRead(nvData, buf, len);
		memoryCRC = nvReadWord(nvCRC);
		computedCRC = crc16(buf, len);
		if (pcrc)
		{
			*pcrc = memoryCRC;
		}
		if (computedCRC != memoryCRC)
		{
			result = -1;
		}
	}
	if (nvStop() < 0)
	{
		result = -1;
	}
	
	return result;
}


/**
 * Write a block of data to FRAM, without CRC
 * 
 * \param nvData	Address in NVRAM at which to store the data
 * \param buf		Pointer to the data to be stored
 * \param len		Length of the data to be stored
 * \returns 	-1 if the write failed, or 0 if successful
 */
 
int nvMdl_BasicWrite(NVADDR nvData, const void *buf, size_t len)
{
	int result;

	result = 0;
	
	if (nvStart() < 0)
	{
		result = -1;
	}
	else if (nvWrite(nvData, buf, len) < 0)
	{
		result = -1;
	}
	if (nvStop() < 0)
	{
		result = -1;
	}

	return result;
}

/**
 * Read a block of data from FRAM, with CRC verification
 * 
 * \param nvData	Address in NVRAM from which to read the data
 * \param buf		Pointer to a buffer to receive the data 
 * \param len		Length of the data to be read
 * \returns 	-1 if the write failed, or 0 if successful
 */

int nvMdl_BasicRead(NVADDR nvData, void *buf, size_t len)
{
	int result;
	
	result = 0;
	
	if (nvStart() < 0)
	{
		result = -1;
	}
	else
	{
		nvRead(nvData, buf, len);
	}
	if (nvStop() < 0)
	{
		result = -1;
	}
	
	return result;
}


