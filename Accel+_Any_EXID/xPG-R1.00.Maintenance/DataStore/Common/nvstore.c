/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Non-volatile memory storage module functions.
 *	
 ***************************************************************************/

#include "nvstore.h"
#include "nvstore_int.h"
#include "NV.h"

#ifdef TEST
#define STATIC
#else
#define STATIC	static
#endif

/*
 * Suppress the "const variable...has no intializer" warnings.
 * The variables below represent memory allocations in the nvram, not actual
 * C variables.  They cannot use C initializing expressions.
 */
#pragma diag_suppress=1110

/**
 * Data structure for allocating space in the FRAM address space for
 * PRGM_DEF storage.
 * 
 * \warning This is not a usable data structure in the MSP430 address space!
 * It overlaps other data structures.  Only use it to let the linker manage
 * the nv addresses for the nvCond_* and fram_* functions.  Do not try to
 * directly access this data! 
 */

#pragma DATA_SECTION(nvPrgmDef, "nv")
STATIC const NV_PRGM_DEF nvPrgmDef[NUM_PRGM_DEFS];

/**
 * Data structure for allocating space in the FRAM address space for
 * CONFIG_DEVICE_PARAMETERS storage.
 *
 * \warning This is not a usable data structure in the MSP430 address space!
 * It overlaps other data structures.  Only use it to let the linker manage
 * the nv addresses for the nvCond_* and fram_* functions.  Do not try to
 * directly access this data!
 */

#pragma DATA_SECTION(nvConfigDeviceParams, "nv")
STATIC const NV_CONFIG_DEVICE_PARAMS nvConfigDeviceParams;
 
/**
 * Data structure for allocating space in the FRAM address space for
 * CONFIG_DEVICE_PARAMETERS backup storage.
 *
 * \warning This is not a usable data structure in the MSP430 address space!
 * It overlaps other data structures.  Only use it to let the linker manage
 * the nv addresses for the nvCond_* and fram_* functions.  Do not try to
 * directly access this data!
 */

#pragma DATA_SECTION(nvConfigDeviceParamsBackup, "nv")
STATIC const NV_CONFIG_DEVICE_PARAMS nvConfigDeviceParamsBackup;

/**
 * Data structure for allocating space in the FRAM address space for
 * lead limits storage.
 *
 * \warning This is not a usable data structure in the MSP430 address space!
 * It overlaps other data structures.  Only use it to let the linker manage
 * the nv addresses for the nvCond_* and fram_* functions.  Do not try to
 * directly access this data!
 */

#pragma DATA_SECTION(nvLeadLimits, "nv")
STATIC const NV_LEAD_LIMITS nvLeadLimits;

/**
 * Storage space in the FRAM address space for opaque data from the CP.
 *
 * \warning This is not a usable data structure in the MSP430 address space!
 * It overlaps other data structures.  Only use it to let the linker manage
 * the nv addresses for the nvCond_* and fram_* functions.  Do not try to
 * directly access this data! 
 */

#pragma DATA_SECTION(nvCPData, "nv")
STATIC const uint8_t nvCPData[NUM_CP_DATA][CP_DATA_LEN];

/**
 * Storage in the FRAM address space for persistent counters.
 *
 * \warning This is not a usable data structure in the MSP430 address space!
 * It overlaps other data structures.  Only use it to let the linker manage
 * the nv addresses for the nvCond_* and fram_* functions.  Do not try to
 * directly access this data! 
 */

#pragma DATA_SECTION(nvCounter, "nv")
STATIC const uint32_t nvCounter[NUM_COUNTER];

/**
 * Storage in the FRAM for the Ramp Time value
 * 
 * \warning This is not a usable data structure in the MSP430 address space!
 * It overlaps other data structures.  Only use it to let the linker manage
 * the nv addresses for the nvCond_* and fram_* functions.  Do not try to
 * directly access this data! 
 */

#pragma DATA_SECTION(nvRampTime, "nv")
STATIC const NV_RAMP_TIME nvRampTime;

/**
 * Storage in the FRAM for the backup Ramp Time value
 *
 * \warning This is not a usable data structure in the MSP430 address space!
 * It overlaps other data structures.  Only use it to let the linker manage
 * the nv addresses for the nvCond_* and fram_* functions.  Do not try to
 * directly access this data!
 */

#pragma DATA_SECTION(nvRampTimeBackup, "nv")
STATIC const NV_RAMP_TIME nvRampTimeBackup;

/**
 * Storage in the FRAM for the Background Impedance parameters
 *
 * \warning This is not a usable data structure in the MSP430 address space!
 * It overlaps other data structures.  Only use it to let the linker manage
 * the nv addresses for the nvCond_* and fram_* functions.  Do not try to
 * directly access this data!
 */

#pragma DATA_SECTION(nvBackgroundImpedanceParams, "nv")
STATIC const NV_BACKGROUND_IMPEDANCE_PARAMS nvBackgroundImpedanceParams;

/**
 * Data structure for allocating space in the FRAM address space for
 * ACTIVE_PRGM_DEF storage.
 *
 * \warning This is not a usable data structure in the MSP430 address space!
 * It overlaps other data structures.  Only use it to let the linker manage
 * the nv addresses for the nvCond_* and fram_* functions.  Do not try to
 * directly access this data!
 */

#pragma DATA_SECTION(nvActPrgmDef, "nv")
STATIC const NV_ACTIVE_PRGM_DEF nvActPrgmDef[NUM_PRGM_DEFS];

/**
 * Data structure for allocating space in the FRAM address space for
 * a bitfield that can be used for storing persistent states
 *
 * \warning This is not a usable data structure in the MSP430 address space!
 * It overlaps other data structures.  Only use it to let the linker manage
 * the nv addresses for the nvCond_* and fram_* functions.  Do not try to
 * directly access this data!
 */

#pragma DATA_SECTION(nvSystemState, "nv")
STATIC const NV_SYSTEM_STATE nvSystemState;


/**
 * Write a PRGM_DEF to an array in FRAM.
 * 
 * \param pd	Pointer to the PRGM_DEF to be written
 * \param n		Array index at which to store the PRGM_DEF
 * \returns 	-1 if the write failed, or 0 if successful
 */
  
int nvWritePrgmDef(int n, const PRGM_DEF *pd)
{
	return nvMdl_ProtectedWrite(NV_ADDR(nvPrgmDef[n].prgmDef), NV_ADDR(nvPrgmDef[n].crc), pd, sizeof(*pd));
}


/**
 * Read a PRGM_DEF from an array in FRAM.
 * 
 * \param pd	Pointer to where the PRGM_DEF should be stored
 * \param n		Array index from which to read the PRGM_DEF
 * \returns		-1 if CRC failed, or 0 if successful
 */

int nvReadPrgmDef(int n, PRGM_DEF *pd)
{
	return nvMdl_ProtectedRead(NV_ADDR(nvPrgmDef[n].prgmDef), NV_ADDR(nvPrgmDef[n].crc), pd, sizeof(*pd), NULL);
}


/**
 * Write a ACTIVE_PRGM_DEF to an array in FRAM.
 *
 * \param pd	Pointer to the ACTIVE_PRGM_DEF to be written
 * \param n		Array index at which to store the PRGM_DEF
 * \returns 	-1 if the write failed, or 0 if successful
 */

int nvWriteActPrgmDef(int n, const ACTIVE_PRGM_DEF *apd)
{
	return nvMdl_ProtectedWrite(NV_ADDR(nvActPrgmDef[n].activePrgmDef), NV_ADDR(nvActPrgmDef[n].crc), apd, sizeof(*apd));
}


/**
 * Read a ACTIVE_PRGM_DEF from an array in FRAM.
 *
 * \param pd	Pointer to where the ACTIVE_PRGM_DEF should be stored
 * \param n		Array index from which to read the PRGM_DEF
 * \returns		-1 if CRC failed, or 0 if successful
 */

int nvReadActPrgmDef(int n, ACTIVE_PRGM_DEF *apd)
{
	return nvMdl_ProtectedRead(NV_ADDR(nvActPrgmDef[n].activePrgmDef), NV_ADDR(nvActPrgmDef[n].crc), apd, sizeof(*apd), NULL);
}


/**
 * Write a CONFIG_DEVICE_PARAMETERS to FRAM.
 * 
 * \param pc	Pointer to the CONFIG_DEVICE_PARAMETERS to be written
 * \returns 	-1 if FRAM readback verification failed, or 0 if successful
 */
  
int nvWriteConfigDeviceParams(const CONFIG_DEVICE_PARAMETERS *cdp)
{
	int response;

	response = nvMdl_ProtectedWrite(NV_ADDR(nvConfigDeviceParams.configDeviceParams), NV_ADDR(nvConfigDeviceParams.crc), cdp, sizeof(*cdp));

	if (response == OK_RETURN_VALUE)
	{
		response = nvMdl_ProtectedWrite(NV_ADDR(nvConfigDeviceParamsBackup.configDeviceParams), NV_ADDR(nvConfigDeviceParamsBackup.crc), cdp, sizeof(*cdp));
	}
	else
	{
		return response;
	}

	return response;
}

/**
 * Write a CONFIG_DEVICE_PARAMETERS primary copy to FRAM.
 *
 * \param pc	Pointer to the CONFIG_DEVICE_PARAMETERS to be written
 * \returns 	-1 if FRAM readback verification failed, or 0 if successful
 */

int nvWritePrimaryConfigDeviceParams(const CONFIG_DEVICE_PARAMETERS *cdp)
{
	return nvMdl_ProtectedWrite(NV_ADDR(nvConfigDeviceParams.configDeviceParams), NV_ADDR(nvConfigDeviceParams.crc), cdp, sizeof(*cdp));
}

/**
 * Read a CONFIG_DEVICE_PARAMETERS from FRAM.
 * 
 * \param pd	Pointer to where the CONFIG_DEVICE_PARAMETERS should be stored
 * \returns		-1 if CRC failed, or 0 if successful
 */

int nvReadConfigDeviceParams(CONFIG_DEVICE_PARAMETERS *cdp)
{
	return nvMdl_ProtectedRead(NV_ADDR(nvConfigDeviceParams.configDeviceParams), NV_ADDR(nvConfigDeviceParams.crc), cdp, sizeof(*cdp), NULL);
}

/**
 * Read a CONFIG_DEVICE_PARAMETERS backup copy from FRAM.
 *
 * \param pd	Pointer to where the CONFIG_DEVICE_PARAMETERS should be stored
 * \returns		-1 if CRC failed, or 0 if successful
 */

int nvReadConfigDeviceParamsBackup(CONFIG_DEVICE_PARAMETERS *cdp)
{
	return nvMdl_ProtectedRead(NV_ADDR(nvConfigDeviceParamsBackup.configDeviceParams), NV_ADDR(nvConfigDeviceParamsBackup.crc), cdp, sizeof(*cdp), NULL);
}


/**
 * Write a LEAD_LIMIT_PARAMS to FRAM.
 * 
 * \param llp	Pointer to the LEAD_LIMIT_PARAMS to be written
 * \returns 	-1 if FRAM readback verification failed, or 0 if successful
 */
  
int nvWriteLeadLimits(const LEAD_LIMIT_PARAMS *llp)
{
	return nvMdl_ProtectedWrite(NV_ADDR(nvLeadLimits.leadLimits), NV_ADDR(nvLeadLimits.crc), llp, sizeof(*llp));
}

/**
 * Read a LEAD_LIMIT_PARAMS from FRAM.
 * 
 * \param pd	Pointer to where the LEAD_LIMIT_PARAMS should be stored
 * \param pcrc	Pointer to where the CRC can be stored, or NULL for no CRC copy.
 * \returns		-1 if CRC failed, or 0 if successful
 */

int nvReadLeadLimits(LEAD_LIMIT_PARAMS *llp, uint16_t *pcrc)
{
	return nvMdl_ProtectedRead(NV_ADDR(nvLeadLimits.leadLimits), NV_ADDR(nvLeadLimits.crc), llp, sizeof(*llp), pcrc);
}


/**
 * Write a block of opaque CP data to an array in FRAM.
 * 
 * There is no CRC on this data.
 * 
 * \param n		Number of the block to write
 * \param buf	Pointer to the data to be written
 * \returns 	-1 if the write failed, or 0 if successful
 */
  
int nvWriteCPData(int n, const uint8_t *buf)
{
	return nvMdl_BasicWrite(NV_ADDR(nvCPData[n]), buf, CP_DATA_LEN);
}

/**
 * Read a LEAD_LIMIT_PARAMS from FRAM.
 * 
 * There is no CRC on this data.
 * 
 * \param n		Number of the block to read 
 * \param buf	Pointer to where the data should be stored
 * \returns		-1 if power management fails, or 0 if successful
 */

int nvReadCPData(int n, uint8_t *buf)
{
	return nvMdl_BasicRead(NV_ADDR(nvCPData[n]), buf, CP_DATA_LEN);
}


/**
 * Read a counter value from FRAM.
 * 
 * There is no CRC on this data.
 * 
 * \param id	The counter ID
 * \param count	Pointer to where the count should be stored
 * \returns		-1 if power management fails, or 0 if successful
 */
 
int nvReadCounter(COUNTER id, uint32_t *count)
{
	return nvMdl_BasicRead(NV_ADDR(nvCounter[id]), count, sizeof(*count));
}

/**
 * Write a counter value to FRAM.
 * 
 * There is no CRC on this data.
 * 
 * \param id	The counter ID
 * \param count	Pointer to where the count should be stored
 * \returns		-1 if power management fails, or 0 if successful
 */
 
int nvWriteCounter(COUNTER id, uint32_t count)
{
	return nvMdl_BasicWrite(NV_ADDR(nvCounter[id]), &count, sizeof(count));
}

/**
 * Write a Ramp Time to FRAM.
 * 
 * \param rtp	Pointer to the ramp time uint16_t to be written
 * \returns 	ERROR_RETURN_VALUE if FRAM readback verification failed, or OK_RETURN_VALUE if successful
 */
  
int nvWriteRampTime(const uint16_t rampTime)
{
	int response;

	response = nvMdl_ProtectedWrite(NV_ADDR(nvRampTime.rampTime), NV_ADDR(nvRampTime.crc), &rampTime, sizeof(rampTime));

	if (response == OK_RETURN_VALUE)
	{
		response = nvMdl_ProtectedWrite(NV_ADDR(nvRampTimeBackup.rampTime), NV_ADDR(nvRampTimeBackup.crc), &rampTime, sizeof(rampTime));
	}
	else
	{
		return response;
	}

	return response;
}

/**
 * Write a Ramp Time to FRAM.
 *
 * \param rtp	Pointer to the ramp time uint16_t to be written
 * \returns 	ERROR_RETURN_VALUE if FRAM readback verification failed, or OK_RETURN_VALUE if successful
 */

int nvWritePrimaryRampTime(const uint16_t rampTime)
{
	return nvMdl_ProtectedWrite(NV_ADDR(nvRampTime.rampTime), NV_ADDR(nvRampTime.crc), &rampTime, sizeof(rampTime));
}

/**
 * Read the Ramp Time from FRAM.
 * 
 * \param rtp	Pointer to where the SET_RAMP_TIME_RESPONSE_DATA should be stored
 * \returns		ERROR_RETURN_VALUE if CRC failed, or OK_RETURN_VALUE if successful
 */

int nvReadRampTime(uint16_t *rampTime, uint16_t *pcrc)
{
	return nvMdl_ProtectedRead(NV_ADDR(nvRampTime.rampTime), NV_ADDR(nvRampTime.crc), rampTime, sizeof(*rampTime), pcrc);
}

/**
 * Read the Ramp Time from FRAM.
 *
 * \param rtp	Pointer to where the SET_RAMP_TIME_RESPONSE_DATA should be stored
 * \returns		ERROR_RETURN_VALUE if CRC failed, or OK_RETURN_VALUE if successful
 */

int nvReadRampTimeBackup(uint16_t *rampTime, uint16_t *pcrc)
{
	return nvMdl_ProtectedRead(NV_ADDR(nvRampTimeBackup.rampTime), NV_ADDR(nvRampTimeBackup.crc), rampTime, sizeof(*rampTime), pcrc);
}

/**
 * Write a BACKGROUND_IMPEDANCE_PARAMS to FRAM.
 *
 * \param pc	Pointer to the BACKGROUND_IMPEDANCE_PARAMS to be written
 * \returns 	-1 if FRAM readback verification failed, or 0 if successful
 */

int nvWriteBackgroundImpedanceParams(const BACKGROUND_IMPEDANCE_PARAMS *bgImpedanceParams)
{
	return nvMdl_ProtectedWrite(NV_ADDR(nvBackgroundImpedanceParams.backgroundImpedanceParams), NV_ADDR(nvBackgroundImpedanceParams.crc), bgImpedanceParams, sizeof(*bgImpedanceParams));
}


/**
 * Read a BACKGROUND_IMPEDANCE_PARAMS from FRAM.
 *
 * \param pd	Pointer to where the BACKGROUND_IMPEDANCE_PARAMS should be stored
 * \returns		-1 if CRC failed, or 0 if successful
 */

int nvReadBackgroundImpedanceParams(BACKGROUND_IMPEDANCE_PARAMS *bgImpedanceParams, uint16_t *pcrc)
{
	return nvMdl_ProtectedRead(NV_ADDR(nvBackgroundImpedanceParams.backgroundImpedanceParams), NV_ADDR(nvBackgroundImpedanceParams.crc), bgImpedanceParams, sizeof(*bgImpedanceParams), pcrc);
}

/**
 * Increment a counter value in FRAM.
 * 
 * There is no CRC on this data.
 * 
 * \param id	The counter ID
 * \param count	Pointer to where the count should be stored
 * \returns		-1 if power management fails, or 0 if successful
 */

int nvIncrementCounter(COUNTER id)
{
	uint32_t count;
	int result = 0;
	
	if (nvStart() < 0)
	{
		result = -1;
	}
	else
	{
		nvRead(NV_ADDR(nvCounter[id]), &count, sizeof(count));
		if (count < UINT32_MAX)
		{
			count++;
		}
		if (nvWrite(NV_ADDR(nvCounter[id]), &count, sizeof(count)) < 0)
		{
			result = -1;
		}
	}

	if (nvStop() < 0)
	{
		result = -1;
	}
	
	return result;
}


/**
 * Write a system state to FRAM.
 *
 * \param systemState	Pointer to the NV_SYSTEM_ST to be written
 * \returns 			-1 if FRAM readback verification failed, or 0 if successful
 */

int nvWriteSystemState(const uint16_t systemState)
{
	return nvMdl_ProtectedWrite(NV_ADDR(nvSystemState.systemState), NV_ADDR(nvSystemState.crc), &systemState, sizeof(systemState));
}

/**
 * Read system state from FRAM.
 *
 * \param pd	Pointer to where the LEAD_LIMIT_PARAMS should be stored
 * \param pcrc	Pointer to where the CRC can be stored, or NULL for no CRC copy.
 * \returns		-1 if CRC failed, or 0 if successful
 */

int nvReadSystemState(uint16_t *systemState, uint16_t *pcrc)
{
	return nvMdl_ProtectedRead(NV_ADDR(nvSystemState.systemState), NV_ADDR(nvSystemState.crc), systemState, sizeof(*systemState), pcrc);
}
