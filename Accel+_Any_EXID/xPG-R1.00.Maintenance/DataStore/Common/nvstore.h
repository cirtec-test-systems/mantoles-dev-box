/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Non-volatile memory storage module functions.
 *	
 ***************************************************************************/

#ifndef NVSTORE_H_
#define NVSTORE_H_

#include <stdbool.h>
#include <stdint.h>

/// \defgroup nvStore Nonvolatile Store
/// \ingroup dataStoreCommon
/// @{

#include "prgmDef.h"
#include "activePrgmDef.h"
#include "prgmConst.h"
#include "pulseConst.h"
#include "Common/Protocol/cmndConfigDeviceParams.h"
#include "Common/Protocol/cmndBackgroundImpedance.h"
#include "Common/Protocol/cmndLeadLimits.h"
#include "Common/Protocol/cmndSetRampTime.h"
#include "Common/Protocol/cmndGetRampTime.h"
#include "Common/Protocol/counter_id.h"

int nvWritePrgmDef(int n, const PRGM_DEF *pd);
int nvReadPrgmDef(int n, PRGM_DEF *pd);
int nvWriteActPrgmDef(int n, const ACTIVE_PRGM_DEF *apd);
int nvReadActPrgmDef(int n, ACTIVE_PRGM_DEF *apd);

int nvWriteConfigDeviceParams(const CONFIG_DEVICE_PARAMETERS *cdp);
int nvWritePrimaryConfigDeviceParams(const CONFIG_DEVICE_PARAMETERS *cdp);
int nvReadConfigDeviceParams(CONFIG_DEVICE_PARAMETERS *cdp);
int nvReadConfigDeviceParamsBackup(CONFIG_DEVICE_PARAMETERS *cdp);

int nvWriteLeadLimits(const LEAD_LIMIT_PARAMS *llp);
int nvReadLeadLimits(LEAD_LIMIT_PARAMS *llp, uint16_t *pcrc);

int nvWriteCPData(int n, const uint8_t *buf);
int nvReadCPData(int n, uint8_t *buf);

int nvReadCounter(COUNTER id, uint32_t *count);
int nvWriteCounter(COUNTER id, uint32_t count);
int nvIncrementCounter(COUNTER id);

int nvWriteRampTime(const uint16_t rampTime);
int nvWritePrimaryRampTime(const uint16_t rampTime);
int nvReadRampTime(uint16_t *rampTime, uint16_t *pcrc);
int nvReadRampTimeBackup(uint16_t *rampTime, uint16_t *pcrc);

int nvWriteBackgroundImpedanceParams(const BACKGROUND_IMPEDANCE_PARAMS *bgImpedanceParams);
int nvReadBackgroundImpedanceParams(BACKGROUND_IMPEDANCE_PARAMS *bgImpedanceParams, uint16_t *pcrc);

int nvWriteSystemState(const uint16_t systemState);
int nvReadSystemState(uint16_t *systemState, uint16_t *pcrc);

/// @}

#endif /*NVSTORE_H_*/
