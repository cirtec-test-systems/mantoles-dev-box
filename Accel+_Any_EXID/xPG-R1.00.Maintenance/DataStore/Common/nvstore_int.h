/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: Header file for Non-volatile memory storage module
 *	internal functions.
 *	
 ***************************************************************************/

#ifndef NVSTORE_INT_H_
#define NVSTORE_INT_H_

#include <stdint.h>
#include <stddef.h>

/// \defgroup nvStoreInt Nonvolatile Store Internals
/// \ingroup nvStore
/// @{

#include "NV.h"
#include "prgmDef.h"
#include "activePrgmDef.h"
#include "Common/Protocol/cmndConfigDeviceParams.h"
#include "Common/Protocol/cmndBackgroundImpedance.h"
#include "Common/Protocol/cmndLeadLimits.h"
#include "Common/Protocol/cp_data.h"
#include "Common/Protocol/counter_id.h"

typedef struct NV_PRGM_DEF {
	PRGM_DEF prgmDef;
	uint16_t crc;
} NV_PRGM_DEF;

typedef struct NV_ACTIVE_PRGM_DEF {
	ACTIVE_PRGM_DEF activePrgmDef;
	uint16_t crc;
} NV_ACTIVE_PRGM_DEF;

typedef struct NV_CONFIG_DEVICE_PARAMS {
	CONFIG_DEVICE_PARAMETERS configDeviceParams;
	uint16_t crc;
} NV_CONFIG_DEVICE_PARAMS;

typedef struct NV_LEAD_LIMITS {
	LEAD_LIMIT_PARAMS leadLimits;
	uint16_t crc;
} NV_LEAD_LIMITS;

typedef struct NV_RAMP_TIME {
	uint16_t rampTime;
	uint16_t crc;
} NV_RAMP_TIME;

typedef struct NV_BACKGROUND_IMPEDANCE_PARAMS {
	BACKGROUND_IMPEDANCE_PARAMS backgroundImpedanceParams;
	uint16_t crc;
} NV_BACKGROUND_IMPEDANCE_PARAMS;

typedef struct NV_SYSTEM_STATE {
	uint16_t systemState;
	uint16_t crc;
} NV_SYSTEM_STATE;

#ifdef TEST
// Link to the FRAM data structures needed for address calculations
extern const NV_PRGM_DEF nvPrgmDef[NUM_PRGM_DEFS];
extern const NV_CONFIG_DEVICE_PARAMS nvConfigDeviceParams;
extern const NV_LEAD_LIMITS nvLeadLimits;
extern const uint8_t nvCPData[NUM_CP_DATA][CP_DATA_LEN];
extern const uint32_t nvCounter[NUM_COUNTER];
#endif


int nvMdl_ProtectedRead(NVADDR nvData, NVADDR nvCRC, void *buf, size_t len, uint16_t *pcrc);
int nvMdl_ProtectedWrite(NVADDR nvData, NVADDR nvCRC, const void *buf, size_t len);
int nvMdl_BasicRead(NVADDR nvData, void *buf, size_t len);
int nvMdl_BasicWrite(NVADDR nvData, const void *buf, size_t len);

/// @}

#endif /*NVSTORE_INT_H_*/
