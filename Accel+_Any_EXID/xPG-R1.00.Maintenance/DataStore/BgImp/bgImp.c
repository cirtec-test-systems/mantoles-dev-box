/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Defines and initializes data structure for the
 *                     Background Impedance parameters
 *
 ***************************************************************************/

#include <stddef.h>
#include <stdbool.h>
#include <string.h>

#include "bgImp.h"
#include "prgmDef.h"

#include "Common/Protocol/cmndBackgroundImpedance.h"
#include "Common/Protocol/response_code.h"

#include "DataStore/Common/nvstore.h"
#include "log.h"
#include "log_events.h"
#include "DataStore/Common/crc16.h"


/**
 * defines storage and initialization for Background Impedance Parameters
 */

static BACKGROUND_IMPEDANCE_PARAMS backgroundImpedanceParams;
static uint16_t backgroundImpedanceParamsCRC;

/**
 * Initialize the configDeviceParams structure from NV memory
 */

int bgImpInit(void)
{
	if (nvReadBackgroundImpedanceParams(&backgroundImpedanceParams, &backgroundImpedanceParamsCRC) < 0)
	{
		// If the CRC check failed, configDeviceParams probably has bogus data
		// in it now.  Zero it out to make sure we don't use corrupted data.
		memset(&backgroundImpedanceParams, 0, sizeof(backgroundImpedanceParams));

		//Now set it to default parameters
		backgroundImpedanceParams.enabled = 1;
		backgroundImpedanceParams.bgiAmplitude = DEFAULT_IMPEDANCE_AMPLITUDE;

		nvWriteBackgroundImpedanceParams(&backgroundImpedanceParams);

		logNormal(E_LOG_DEFAULT_BACKROUND_IMPEDANCE_PARAMS,0);

		return -1;
	}
	else
	{
		return 0;
	}
}


/**
 * This function processes the GET BACKGROUND IMPEDANCE
 * PARAMETERS command.
 *
 * \param resp 	  Pointer to first byte of buffer
 *
 * \return Response Code
 */

RESPONSE bgImpGetBackgroundImpedanceParams(BACKGROUND_IMPEDANCE_PARAMS *bgImp)
{
	/*
	 * Retrieve the data block from nonvolatile memory.
	 */

	if (nvReadBackgroundImpedanceParams(bgImp, NULL) < 0)
	{
        // Data was corrupted, so return an error code.
        return RESP_DATA_CORRUPTED;
    }

	// Data good, so send the response.
    return GEN_SUCCESS;
}

/**
 * This function processes the GET BACKGROUND IMPEDANCE
 * PARAMETERS command.
 *
 * \param resp 	  Pointer to first byte of buffer
 *
 * \return Response Code
 */
bool bgImpIsBgImpParamsValid(void)
{
	if (crc16(&backgroundImpedanceParams, sizeof(backgroundImpedanceParams)) != backgroundImpedanceParamsCRC)
	{
		return false;
	}

	return true;
}

/**
 * This function processes the SET BACKGROUND IMPEDANCE
 * PARAMS command.
 *
 * \param param	  Pointer to parameter buffer; NULL for
 *
 * \return Response Code
 */

RESPONSE bgImpSetBackgroundImpedanceParams(BACKGROUND_IMPEDANCE_PARAMS const *bgImp)
{
    if (nvWriteBackgroundImpedanceParams(bgImp) < 0)
    {
    	return RESP_WRITE_FAILED;
    }

    // If that worked, update our working copy and return success
    backgroundImpedanceParams = *bgImp;

	return GEN_SUCCESS;
}


/**
 * This function checks candidate background impedance
 * parameters sent with the SET BACKGROND IMPEDANCE PARAMS
 * command.
 *
 * \param cdp pointer to a BACKGROUND_IMPEDANCE_PARAMS structure.
 *
 * \return true if parameters in range, false if parameters out of range
 */

bool bgImpRangeCheck(BACKGROUND_IMPEDANCE_PARAMS const *bgImp)
{
	//Check enabled byte
	if (bgImp->enabled > 1)
	{
		return false;
	}

	//Check pulse amplitude
	if (bgImp->bgiAmplitude < BG_AMPL_LOW_LIMIT_MIN || bgImp->bgiAmplitude > BG_AMPL_LOW_LIMIT_MAX)
	{
		return false;
	}

    return true;
}
