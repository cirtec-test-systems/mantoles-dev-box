/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description:  Interface to the Clinician Programmer data block.
 *	
 ***************************************************************************/


#include "cpdata.h"
#include "DataStore/Common/nvstore.h"

/**
 * Write a Clinician Programmer data block.
 *
 * These blocks are mass storage for the Clinician Programmer and are not
 * processed by the xPG other than to store them on the CP's behalf.
 *
 * @param blockNum	Block number to write
 * @param buf		Data to write
 * @return 0 for success, -1 for error
 */
int cpDataWrite(int blockNum, const uint8_t *buf)
{
	return nvWriteCPData(blockNum, buf);
}

/**
 * Read a Clinician Programmer data block.
 *
 * These blocks are mass storage for the Clinician Programmer and are not
 * processed by the xPG other than to store them on the CP's behalf.
 *
 * @param blockNum	Block number to write
 * @param buf		Data to write
 * @return 0 for success, -1 for error
 */
int cpDataRead(int blockNum, uint8_t *buf)
{
	return nvReadCPData(blockNum, buf);
}


