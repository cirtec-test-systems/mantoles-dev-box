/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *	
 *	@file Description: counter class
 *	
 ***************************************************************************/

#include "counter.h"

#include "DataStore/Common/nvstore.h"

/*
 * Public API. For documentation, see counter.h
 */
bool counterRangeCheckGetCounter(GET_COUNTER_PARAMS const *p)
{
	return p->eventCounterID < NUM_COUNTER;
}


/*
 * Public API. For documentation, see counter.h
 */
RESPONSE counterCmdGetCounter(GET_COUNTER_PARAMS const *p, GET_COUNTER_RESPONSE_DATA *r)
{
	r->eventCounterID = p->eventCounterID;
	r->reserved = 0;
	if (nvReadCounter((COUNTER)p->eventCounterID, &r->eventCount) < 0)
	{
		return RESP_POWER_SETUP_ERROR_BAD_POWER_ASIC_READBACK;
	}

	return GEN_SUCCESS;
}


/*
 * Public API. For documentation, see counter.h
 */
bool counterRangeCheckSetCounter(SET_COUNTER_PARAMS const *p)
{
	// Not necessary to range-check p->eventCounter because all values are valid

	return p->eventCounterID < NUM_COUNTER;
}


/*
 * Public API. For documentation, see counter.h
 */
RESPONSE counterCmdSetCounter(SET_COUNTER_PARAMS const *p)
{
	if (nvWriteCounter((COUNTER)p->eventCounterID, p->eventCount) < 0)
	{
		return RESP_POWER_SETUP_ERROR_BAD_POWER_ASIC_READBACK;
	}
	
	return GEN_SUCCESS;
}


/*
 * Public API. For documentation, see counter.h
 */
int counterIncrement(COUNTER id)
{
	return nvIncrementCounter(id);
}

/*
 * Public API. For documentation, see counter.h
 */
int counterAddTo(COUNTER id, int32_t addend)
 {
	uint32_t curVal;
	
	nvReadCounter(id, &curVal);
	return nvWriteCounter(id, curVal + addend);
}

/*
 * Public API. For documentation, see counter.h
 */
int counterSet(COUNTER id, uint32_t cntVal)
 {
	return nvWriteCounter(id, cntVal);
}

