/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *	
 *	@file Description: Event dispatch table
 *	
 ***************************************************************************/

#include <stddef.h>
#include <stdint.h>

#include "BatteryMonitor.h"
#include "ChargeManager.h"
#include "MagnetMonitor.h"
#include "SystemHealthMonitor.h"
#include "CommandProcessor.h"
#include "MicsPAD.h"
#include "mics.h"
#include "EventQueue.h"
#include "Stim.h"
#include "MultiChannelTimer.h"
#include "system_events.h"
#include "LEDManager.h"
#include "StimManager.h"
#include "MicsConnection.h"

#ifdef DEBUG_DISPATCHER
#include <stdio.h>
#endif


/**
 * Event handler for all events with a bogus event ID.  This handler is
 * called from the default case of the big event dispatcher switch statement.
 *
 * Since the dispatchgen script generates a case statement for every
 * event in the system_event_list, events end up here only if their ID is
 * a bogus enum value that isn't in the system_event_list.
 *
 * @param event The event with the bogus event ID.
 */

#ifdef UNIT_TESTING
// Save 32-bit time of undefinedEvent logged with bogus event ID
uint32_t undefinedEvent_Time = 0;       // 32-bit time of undefinedEvent from TimerA
uint32_t undefinedEvent_TimeB = 0;      // 32-bit time of undefinedEvent from TimerB
uint8_t undefinedEvent_ID = 0;          // eventID of undefinedEvent
// from "swTimer.c"
extern uint32_t ExtendedTimeValue(void);
// from "timer.c"
extern uint64_t getTimerBTicks(void);
#endif // UNIT_TESTING

void undefinedEvent(const EVENT event)
{
	(void) event;
	// TODO: Log the undefined event. It's an internal software error or maybe a hardware failure.
#ifdef UNIT_TESTING
    undefinedEvent_Time = ExtendedTimeValue();
    undefinedEvent_TimeB = (uint32_t)getTimerBTicks(); // get TimerB
    undefinedEvent_ID = event.eventID;
// Label defined for unit test breakpoint to retrieve undefinedEvent time and ID
    asm("undefinedEvent_STOP: ;");
    asm(" .global undefinedEvent_STOP");
#endif // UNIT_TESTING
}


/*
 * The table itself is automatically generated.
 *
 * This C file provides the #include directives needed to make the dispatcher
 * work.  The include file holds the automatically-generated code.
 *
 * See dispatchgen.tcl for details on the automatic code generation process.
 */

#include "dispatchgen.inc"
