/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *	
 *	@file Description: Header file containing the dispatcher table.
 *	
 ***************************************************************************/



#ifndef EVENTDISPATCHER_H_
#define EVENTDISPATCHER_H_

#include "system_events.h"

void dispatchEvent ( EVENT event ) ;

#endif /* EVENTDISPATCHER_H_ */
