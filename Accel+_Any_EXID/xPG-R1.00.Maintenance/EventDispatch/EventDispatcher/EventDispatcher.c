/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *	
 *	@file Description: Event Dispatcher module.
 *	
 ***************************************************************************/

#include "EventDispatcher.h"

#include "dispatchtable.h"
#include "Interrupt.h"
#include "EventQueue.h"
#include "StimManager.h"
#include "ChargeManager.h"
#include "system.h"
#include "PowerASIC.h"
#include "mics.h"
#include "delay.h"
#include "adc.h"
#include "MultiChannelTimer.h"

#ifdef DEBUG_LOW_POWER_MODE
uint32_t lastEvent;
#endif

#ifdef UNIT_TESTING
// If running unit tests, don't go to low power mode 3
bool Unit_Testing_LowPower = 1;         // false if unit tests not using LPM3
#endif

bool eventLoop(void)
{
    EVENT event;

   (void) interruptDisable();				// Prevent a race condition

	event = dispGetEvent();

	if (event.eventID == E_NO_EVENT)
	{

		interruptEnable(); // the _low_power intrinsics enable interrupts
		// ---------------------------------------
		// if we are stimulating then we can go into LPM1 and keep both SMCLK and ACLK running
		// if we are not stimulating then put the MSP into LPM3 and keep only ACLK running
#ifdef UNIT_TESTING
		if(!Unit_Testing_LowPower && !micsIsAwake())
#else
#ifndef EPG
		if(!chrgMgrIsCharging() && !micsIsAwake())
#else
		if(!micsIsAwake())
#endif
#endif
		{
			adcResetReferenceCount();

			if(stimState() == STIM_INACTIVE)
			{
				if (isIPG())
				{
					pwrBuckBoostOff();
					pwrIntermittentLDO();
				}

				_low_power_mode_3();
			}
			else
			{
#ifdef RUN_AT_8_MHZ
				// drop the DCO to 1MHZ and leave the SMCLK at 1 MHZ.  Note that the InterruptHandlerPrologue sets the DCO back to 8MHZ so the code executes at that clock speed
				DCOCTL = CALDCO_1MHZ;
			  	BCSCTL1 = XT2OFF | DIVA_0 | (CALBC1_1MHZ & 0x0F); // XT2 is off (high freq crystal), LFXT1 is low freq, ACLK is divided by 1
			  	BCSCTL2 = DIVS_0 | DIVM_0; // MCLK driven from DCO, MCLK divided by 1, SMCLK driven from DCO, SMCLK divided by 1, DCO internal resistor
#endif
			  	DELAY_US(20);
				_low_power_mode_1();
			}
		}
	}
	else
	{
#ifdef DEBUG_LOW_POWER_MODE
		lastEvent = event.eventID;
#endif

		interruptEnable();
		dispatchEvent(event);
	}

	return true;
}
