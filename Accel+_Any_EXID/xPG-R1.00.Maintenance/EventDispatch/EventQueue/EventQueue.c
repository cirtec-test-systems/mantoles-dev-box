/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: EventQueue software unit functions
 *
 ***************************************************************************/

#include "system_events.h"
#include <stdint.h>
#include <stddef.h>
#include "EventQueue.h"
#include "Interrupt.h"
#include "log.h"
#include "counter.h"
#include "system.h"

//! The size of the event queue
#define EVM_QUEUE_DEPTH 32

#if defined(DEBUG_DISPATCHER) || defined(PRINT_OUTPUT_DISPATCH)
#include <stdio.h>

char const *const eventStrings[] = {
#define DEFINE_EVENT_ID(X)	#X
#include "system_event_list.inc"
#undef DEFINE_EVENT_ID
};
#endif

/*@-initallelements -fullinitblock@*/
//! Instantiate an event queue for the system events.
//! The queue is pre-loaded with the E_INITIALIZE event to tell all state machines to
//! initialize themselves.
static EVENT EVM_queue[EVM_QUEUE_DEPTH] = { {E_INITIALIZE} };
/*@=initallelements =fullinitblock@*/

//! Instantiate an index into the system event queue
static uint8_t EVM_queue_write_index = 1;
static uint8_t EVM_queue_read_index = 0;
static uint8_t EVM_queue_size = 1;

/**
 *	This function adds an event into the event queue.
 *
 *	Normal processing is as follows:
 *
 * -	Store IRQ Status
 * -	Mask IRQ's
 * -	If EVM_queue_size >= EVM_QUEUE_DEPTH then there is an error:
 * 		-		Clear EVM_queue_write_index
 * 		-		Clear EVM_queue_read_index
 * 		-		Set EVM_queue_size to 1
 * 		-		Log the queue error
 * 		- 		Set the first queue member to EVENT_error
 * 		- 		Restore interrupts and return
 * - 	Else there is no error:
 * 		-		Queue[EVM_queue_write_index] = value to put into queue
 * 		-		Increment EVM_queue_write_index
 * 		- 		Increment EVM_queue_size
 * 		-		Restore IRQ's
 * 
 * 	Modifies the following global items:
 * -		EVM_queue
 * -		EVM_queue_size
 * -		EVM_queue_write_index
 * -		EVM_queue_read_index (if queue is full and there is an error)
 *
 *	\param EVENT_ID event - The event to put into the event queue
 *
 * 	\returns
 *		- NO_ERROR - Everything is fine.
 *		- ERROR - No room in the event queue.
 *
 */

void dispPutEvent(EVENT event)
{
    uint16_t interruptState;
    uint8_t i = 0;

    interruptState = interruptDisable();

    if (EVM_queue_size >= EVM_QUEUE_DEPTH)

    {
    	//Log the Error
    	logError(E_LOG_ERR_QUEUE_OVERFLOW,
    	    			((uint32_t)EVM_queue_read_index)
    	    			| ((uint32_t)EVM_queue_write_index) << 8
    	    			| ((uint32_t)EVM_queue_size) << 16);

    	for (i = 0; i < EVM_QUEUE_DEPTH; i++)
    	{
    		logError(E_LOG_ERR_QUEUE_OVERFLOW,EVM_queue[i].eventID);
    	}

    	//The queue is full, reset the xpg
    	resetSystem();
    }
    else
    {
        EVM_queue[EVM_queue_write_index] = event;

        EVM_queue_write_index = (EVM_queue_write_index + 1) % EVM_QUEUE_DEPTH;;
        EVM_queue_size++;
    }

    interruptRestore(interruptState);
}

/**
 * This function returns the most recent event from the event queue.
 *
 * Processing is as follows:
 * -	Store IRQ Status
 * - 	Mask IRQ's
 * -	If EVM_queue_size = 0 then there is an error, return the value EVENT_no_event
 * - 	Obtain EVM_queue[EVM_queue_read_index] into a temporary location
 * -	Increment EVM_queue_read_index
 * - 	Decrement EVM_queue_size
 * -	Restore IRQ's
 * -	Return the temporary value
 *
 *	The following globals are changed:
 * -		EVM_queue
 * -		EVM_queue_read_index
 * - 		EVM_queue_size
 *
 * \param void
 *
 * \returns
 *		EVENT_no_event is returned when there are no events in the queue,
 *		or the top-event in the event queue is returned.
 *
 */
EVENT dispGetEvent(void)
{
    EVENT event;
    uint16_t interruptState;

    interruptState = interruptDisable();

    if (EVM_queue_size == 0)
    {
        event.eventID = E_NO_EVENT;
    }
    else
    {
        event = EVM_queue[EVM_queue_read_index];
        EVM_queue_read_index = (EVM_queue_read_index + 1) % EVM_QUEUE_DEPTH;;

        EVM_queue_size--;
    }

    interruptRestore(interruptState);

    return event;
}


/**
 * Queue a simple event with no arguments.
 *
 * Use this function to keep code concise and reduce duplication.
 *
 * @param eventId	The EVENT_ID of the event to queue
 * @return 0 for success or -1 for error.
 */
void dispPutSimpleEvent(EVENT_ID eventId)
{
	EVENT event;
	event.eventID = eventId;
	dispPutEvent(event);
}

/**
 * Queue an error event with no arguments.
 *
 * Use this function to keep code concise and reduce duplication.
 *
 * @param eventId	The ACTIVE_ERROR_CODE of the error event to queue
 * @return 0 for success or -1 for error.
 */
void dispPutErrorEvent(ACTIVE_ERROR_CODE errorCode)
{
	EVENT errorEvent;

	errorEvent.eventID = E_ERR_ERROR_DETECTED;
	errorEvent.eventData.eActiveErrorData = errorCode;

	dispPutEvent(errorEvent);
}

/**
 * Queue a log event with log ID as argument.
 *
 * Use this function to keep code concise and reduce duplication.
 * Also allows logging from code that needs to execute fast, such
 * as ISRs.
 *
 * @param logId	The LOG_EVENT_ID to be queued
 * @return 0 for success or -1 for error.
 */
void dispPutLogEvent(LOG_EVENT_ID logID, uint32_t data)
{
	EVENT logEvent;

	logEvent.eventID = E_LOG_EVENT;
	logEvent.eventData.logEventData.logId = logID;
	logEvent.eventData.logEventData.data = data;

	dispPutEvent(logEvent);
}

void dispWriteQueueToLog()
{
	int i;

	logError(E_LOG_ERR_QUEUE_CONTENTS,
							((uint32_t)EVM_queue_read_index)
							| ((uint32_t)EVM_queue_write_index) << 8
							| ((uint32_t)EVM_queue_size) << 16);

	for (i = 0; i < EVM_QUEUE_DEPTH; i++)
	{
		logError(E_LOG_ERR_QUEUE_CONTENTS,EVM_queue[i].eventID);
	}
}
