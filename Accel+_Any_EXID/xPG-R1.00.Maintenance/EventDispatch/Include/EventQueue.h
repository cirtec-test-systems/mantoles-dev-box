/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: Header file for the event queue module.
 *
 ***************************************************************************/

#ifndef EVENTQUEUE_H
#define EVENTQUEUE_H

#include <stdint.h>

/// \defgroup eventQueue		Event Queue
/// @{

#include "system_events.h"
#include "error_code.h"
#include "log_events.h"

void dispPutEvent(EVENT event);
EVENT dispGetEvent(void);
void dispPutSimpleEvent(EVENT_ID eventId);
void dispPutErrorEvent(ACTIVE_ERROR_CODE errorCode);
void dispPutLogEvent(LOG_EVENT_ID logID, uint32_t data);
void dispWriteQueueToLog();

#if defined(DEBUG_DISPATCHER) || defined(PRINT_OUTPUT_XX)
extern char const *const eventStrings[];
#endif

/// @}

#endif                          /* EVENTQUEUE_H */
