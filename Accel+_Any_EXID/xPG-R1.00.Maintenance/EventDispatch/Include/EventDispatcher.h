/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *	
 *	@file Description: Header file for the event loop.
 *	
 ***************************************************************************/



#ifndef EVENTLOOP_H_
#define EVENTLOOP_H_

#include <stdbool.h>

/// \defgroup eventDispatch		Event Dispatcher
/// @{

bool eventLoop(void);

/// @}

#endif /* EVENTLOOP_H_ */
