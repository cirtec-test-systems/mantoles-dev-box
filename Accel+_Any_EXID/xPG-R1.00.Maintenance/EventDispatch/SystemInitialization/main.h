/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *	
 *	@file Description: main module.
 *
 *		This header file is primarily used for unit test access.
 *	
 ***************************************************************************/

#ifndef MAIN_H_
#define MAIN_H_

#ifdef TEST
#define main	altMain
#endif

/// \defgroup systemInitialization	System Initialization
/// \ingroup applicationLayer
/// @{

int _system_pre_init(void);
int main(void);

/// @}

#endif /* MAIN_H_ */
