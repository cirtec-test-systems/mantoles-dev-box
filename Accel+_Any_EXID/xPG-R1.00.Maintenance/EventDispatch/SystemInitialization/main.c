/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description:  This file contains the main loop system initialization.
 *
 ***************************************************************************/

#include "main.h"

#include "datastore.h"
#include "EventDispatcher.h"
#include "init.h"
#include "Interrupt.h"
#include "IOInitialization.h"
#include "Watchdog.h"
#include "log.h"
#include "ticks.h"

#ifdef PRINT_OUTPUT
#include <stdio.h>
#endif


/*@unused@*/
/**
 * This function is hooked by the C startup code early in the startup
 * sequence and can alter application initialization.
 *
 * Disables the watchdog timer so that C initialization can take as long as
 * necessary without causing a watchdog timeout.
 *
 * \return 1, which tells cstartup to perform variable initialization in
 *         the .data and .bss segments.
 */
int _system_pre_init(void)
{
	wdDisable(); // Stop Watchdog timer

    // Configure all MCU pins to fail-safe, low-current states
    // Start the crystal oscillator now (because it takes a long time to start)
	ioInit();

	/*==================================*/
	/* Choose if segment initialization */
	/* should be done or not. */
	/* Return: 0 to omit initialization */
	/* 1 to run initialization */
	/*==================================*/
	return (1);
}

/**
 * Perform all initialization necessary to bring the system up to a running
 * state.
 *
 * The general initialization strategy is bottom-up:
 * Initialize the port pins, then the device drivers, then the
 * device management layer, then the application layer.
 *
 * @return 0 for success or -1 for any severe initialization failure.
 */
static void sysInit(void)
{
	// The rest of initialization should be done with interrupts enabled.
	// Some device drivers need interrupts in order to complete their initialization.
	interruptEnable();

    // Call all of the device driver layer initialization functions.
	initDeviceDriverLayer();

	// Initialize Data Store.
	datastoreInit();

    // Call all of the device management layer initialization functions.
	initDeviceManagementLayer();

	// The application layer is initialized by an E_INITIALIZE event that is
	// automatically put in the event queue during dispatch.c's variable
	// initialization.

	// At this point, the system is initialized and ready to run.
#ifdef PRINT_OUTPUT
    printf("System started\n");
#endif

#ifndef ACLK_USES_LFXT1CLK
    DBGI(f_main, TICKS_HZ);
#endif

}


/**
 * 	The main loop for the xPG System.
 *	This function initializes each of the hardware subsystems, and enters the main processing
 *	loop.
 *	
 *	The main processing loop uses the Event Manager functions to look for
 *	and process events when they are detected.
 *	
 *	\note Since eventLoop() always returns true, main() is an infinite loop and never returns.
 *		  During testing, a false return from eventLoop() can be simulated when it is necessary
 *		  to force the infinite loop to terminate.  In such a case, main() will return.
 *
 * 	\return Never returns.
 */
int main(void)
{
    sysInit();
    while (eventLoop())
    {
    }
    return 0;	// Assuage the compiler and prevent warnings. Function never returns in the real world.
}

