/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *	
 *	@file Description: SysInit internal functions
 *	
 ***************************************************************************/



#ifndef INIT_H_
#define INIT_H_

/// \defgroup systemInitializationInternals Initializer Internals
/// \ingroup systemInitialization
/// @{

void initDeviceDriverLayer(void);
void initDeviceManagementLayer(void);

/// @}

#endif /* INIT_H_ */
