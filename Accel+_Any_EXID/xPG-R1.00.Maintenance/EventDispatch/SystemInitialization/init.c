/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *	
 *	@file Description: Initialization module.
 *	
 ***************************************************************************/

#include "init.h"

#include "Clock.h"
#include "datastore.h"
#include "error_code.h"
#include "EventQueue.h"
#include "GenericProgram.h"
#include "IOInitialization.h"
#include "Interrupt.h"
#include "led.h"
#include "magnet.h"
#include "mics.h"
#include "MultiChannelTimer.h"
#include "NV.h"
#include "PowerASIC.h"
#include "Stack.h"
#include "Stim.h"
#include "system.h"
#include "tets.h"
#include "timer.h"
#include "trim.h"
#include "xpginfo.h"
#include "Watchdog.h"
#include "log.h"


//#define DEBUG_INIT_FAILURES
#if defined(DEBUG_INIT_FAILURES) && !defined(TEST)
#include <stdio.h>
#define FAIL_PRINTF(txt)	printf("ERROR: %s\n", txt)
#else
#define FAIL_PRINTF(txt)
#endif


/**
 * Initialize the device drivers.
 *
 * This is done by calling each driver's init function.
 *
 */
void initDeviceDriverLayer(void)
{
	// Bring the clocks up first. Everything needs a stable clock.
	if (ERROR_RETURN_VALUE == clkInit())
	{
		dispPutErrorEvent(ACTERR_CLOCK_ERROR_MAJOR);
		FAIL_PRINTF("clkInit() failed");
	}

	// Initialize power next, because everything needs it.
	if (pwrInit(trimIsListValid(TRIM_PLUTO), trimGetList(TRIM_PLUTO)) < 0)
	{
		dispPutErrorEvent(ACTERR_POWER_ASIC_ERROR_STIM_DISABLED);
		FAIL_PRINTF("pwrInit() failed");
	}

	// Initialize the stack so that overflows during initialization can be detected.
	stackInit();

	// Initialize nv (FRAM) next, because it is needed for logging if anything else fails.
	if (nvInit() < 0)
	{
		dispPutErrorEvent(ACTERR_CRITICAL_NV_HARDWARE_ERROR);
		FAIL_PRINTF("nvInit() failed");
	}

	// Initialize timer next, because lots of other services need the timer.
	timerInit();

	// Initialize miscellaneous services in no particular order.
	chargeInit();
	magnetInit();

	//See if primary copy of xpg identity is valid, if not try the secondary.
	if (!isXpgInfoValid())
	{
		(void) restorePrimaryXpgInfo();
	}

	if (micsInit(trimIsListValid(TRIM_ZL), trimGetList(TRIM_ZL), &getXpgInfo()->xpgIdentity.xpgMicsId) < 0)
	{
		dispPutErrorEvent(ACTERR_MICS_TRANSCEIVER_ERROR);
		FAIL_PRINTF("micsInit() failed");
	}

	tetsInit();

#ifdef EPG
	// Initialize the LEDs, but only on an EPG.
	if (!isIPG())
	{
		ledInit();
	}
#endif

	// Stim may safely be initialized last. Nothing depends on it until MICS commands
	// to turn on stim are received and processed.
	stimInit();

	// Initialize the watchdog.
	wdInit();
}

/**
 * Initialize the device management layer.
 *
 * This is done by calling each modules's init function.  Note that some
 * modules may be initialized via the E_INITIALIZE event instead.
 */
void initDeviceManagementLayer(void)
{
 	// Initialize Device Management Layer
	swTimerInit();			//Start the system timer which is required for the event dispatcher.
	gprgmInit();			// Initialize the Generic Program
}

