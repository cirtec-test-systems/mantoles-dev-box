/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: LED Manager Module.
 *
 ***************************************************************************/


//#define LEDMGR_PRINT_OUTPUT	// to turn on/off print debug output for LED Manager
#ifdef LEDMGR_PRINT_OUTPUT
#include <stdio.h>
#endif

#include <stdbool.h>
#include "state_machine_common.h"
#include "system_events.h"
#include "EventQueue.h"
#include "swtmr_application_timers.h"
#include "log.h"
#include "log_events.h"
#include "system.h"
#include "PowerASIC.h"
#include "led.h"
#include "Common/Protocol/cmndGetXpgStatus.h"
#include "BatteryMonitor.h"
#include "StimManager.h"
#include "LEDManager.h"
#include "MultiChannelTimer.h"

#ifdef EPG
//Define the states of the state machine
typedef enum LEDMGR_STATE
{
	LEDMGR_STATE_INITIAL,	//Before initialization
	LEDMGR_STATE_PWR_ON,	//LED is power off
	LEDMGR_STATE_ALL,		//React in any state
	LEDMGR_STATE_DISABLED 	//Used to disable the state machine
} LEDMGR_STATE;

/*Event Handers*/
static void eventHandlerE_INITIALIZE(EVENT event);
static void eventHandlerE_STIM_STARTED(EVENT event);
static void eventHandlerE_STIM_STOPPED(EVENT event);
static void eventHandlerE_FLASH_STIM_LED(EVENT event);
static void eventHandlerE_FLASH_RED_LED(EVENT event);
static void eventHandlerE_FLASH_GREEN_LED(EVENT event);
static void eventHandlerE_RESET_LED_STATES(EVENT event);

// Define the State Machine Table
static STATE_MACHINE_TABLE_ENTRY const ledMgrStateTable[] =
{
//   There is one table entry for every event processed in each state
//   CURRENT STATE				EVENT ID				EVENT HANDLER				        END STATE
//   ---------------------------------------------------------------------------------------------------------------------
DISPATCH_GEN_START_ONLY(ledManagerProcessEvent, EPG)
	{LEDMGR_STATE_INITIAL,		E_INITIALIZE,			eventHandlerE_INITIALIZE			/* LEDMGR_STATE_PWR_ON 	*/},
	{LEDMGR_STATE_PWR_ON,		E_STIM_STARTED,			eventHandlerE_STIM_STARTED			/* LEDMGR_STATE_PWR_ON  */},
	{LEDMGR_STATE_PWR_ON,		E_STIM_STOPPED,			eventHandlerE_STIM_STOPPED			/* LEDMGR_STATE_PWR_ON	*/},
	{LEDMGR_STATE_PWR_ON,		E_FLASH_STIM_LED,		eventHandlerE_FLASH_STIM_LED		/* LEDMGR_STATE_PWR_ON  */},
	{LEDMGR_STATE_PWR_ON,		E_FLASH_RED_LED,		eventHandlerE_FLASH_RED_LED			/* LEDMGR_STATE_PWR_ON  */},
	{LEDMGR_STATE_PWR_ON,		E_FLASH_GREEN_LED,		eventHandlerE_FLASH_GREEN_LED		/* LEDMGR_STATE_PWR_ON  */},
	{LEDMGR_STATE_PWR_ON,		E_CALIBRATE_VLO,		eventHandlerE_RESET_LED_STATES		/* LEDMGR_STATE_PWR_ON  */}
DISPATCH_GEN_END
};

#define LEDMGR_EVENT_TABLE_LEN (sizeof(ledMgrStateTable)/sizeof(ledMgrStateTable[0]))

//Initialize the state machine and necessary
static LEDMGR_STATE ledMgrCurrentState = LEDMGR_STATE_INITIAL;

/**
 * Main Event Handler that processes all incoming events
 *
 * \param event - The event to process.
 */
void ledManagerProcessEvent(EVENT eventToProcess)
{
	int i = 0;
	EVENT_ID eventId = eventToProcess.eventID;

	#ifdef LEDMGR_PRINT_OUTPUT
		printf("LED Manager - ProcessEvent\n");
	#endif


	// Iterate through the state machine table until find the current state.
	for(i=0; i < LEDMGR_EVENT_TABLE_LEN; i++)
	{
		if((ledMgrStateTable[i].state == ledMgrCurrentState || ledMgrStateTable[i].state == LEDMGR_STATE_ALL)  && ledMgrStateTable[i].eventId == eventId)
		{
			break;
		}
	}

	//Now call the appropriate event handler
	if(i != LEDMGR_EVENT_TABLE_LEN)
	{
		ledMgrStateTable[i].eventHandler(eventToProcess);
	}

	return;
}

/**
 * The event handler for the E_INITIALIZE event.
 *
 * \param event - The event to handle.
 */
static void eventHandlerE_INITIALIZE(EVENT event)
{
	#ifdef LEDMGR_PRINT_OUTPUT
		printf("LED Manager - eventHandlerE_INITIALIZE\n");
	#endif

	(void)event;

	if (isIPG())
	{
		ledMgrCurrentState = LEDMGR_STATE_DISABLED;
	}
	else
	{
		pwrLEDPowerOn();

		if (battMonGetXpgBattState() >= XPG_BATT_MEDIUM)
		{
			EVENT flashGreenEvent;

			//Flash the green led on
			ledGreenOn();

			//Turn it off after a period of time
			flashGreenEvent.eventID = E_FLASH_GREEN_LED;
			swTimerSet( SWTMR_FLASH_GREEN_LED, LED_ON_TIME, flashGreenEvent, false);
		}
		else
		{
			EVENT flashRedEvent;

			//Flash the red led on
			ledRedOn();

			//Turn it off after a period of time
			flashRedEvent.eventID = E_FLASH_RED_LED;
			swTimerSet( SWTMR_FLASH_RED_LED, LED_ON_TIME/2, flashRedEvent, false);
		}

		ledMgrCurrentState = LEDMGR_STATE_PWR_ON;
	}

	return;
}

/**
 * The event handler for the E_STIM_STARTED event.
 *
 * \param event - The event to handle.
 */
static void eventHandlerE_STIM_STARTED(EVENT event)
{
	EVENT flashStimEvent;

	#ifdef LEDMGR_PRINT_OUTPUT
		printf("LED Manager - eventHandlerE_STIM_STARTED\n");
	#endif

	(void)event;

	ledStimOn();

	//Turn it back off again
	flashStimEvent.eventID = E_FLASH_STIM_LED;
	swTimerSet( SWTMR_FLASH_STIM_LED, LED_ON_TIME, flashStimEvent, false);

	return;
}

/**
 * The event handler for the E_STIM_STOPPED event.
 *
 * \param event - The event to handle.
 */
static void eventHandlerE_STIM_STOPPED(EVENT event)
{
	#ifdef LEDMGR_PRINT_OUTPUT
		printf("LED Manager - eventHandlerE_STIM_STOPPED\n");
	#endif

	(void)event;

	ledStimOff();
	swTimerCancel(SWTMR_FLASH_STIM_LED);

	return;
}

/**
 * The event handler for the E_FLASH_STIM_LED event.
 *
 * \param event - The event to handle.
 */
static void eventHandlerE_FLASH_STIM_LED(EVENT event)
{
	(void)event;

	if (ledIsStimLedOn())
	{
		//Turn it off
		ledStimOff();

		//Turn it back on again
		if (isStimOn())
		{
			EVENT flashStimEvent;

			flashStimEvent.eventID = E_FLASH_STIM_LED;
			swTimerSet( SWTMR_FLASH_STIM_LED, LED_FLASH_PERIOD - LED_ON_TIME, flashStimEvent, false);
		}
	}
	else if (isStimOn())
	{
		EVENT flashStimEvent;

		//Turn it on
		ledStimOn();

		//Turn it back off again
		flashStimEvent.eventID = E_FLASH_STIM_LED;
		swTimerSet( SWTMR_FLASH_STIM_LED, LED_ON_TIME, flashStimEvent, false);
	}

	return;
}

/**
 * The event handler for the E_FLASH_RED_LED event.
 *
 * \param event - The event to handle.
 */
static void eventHandlerE_FLASH_RED_LED(EVENT event)
{
	(void)event;

	if (ledIsRedLedOn())
	{
		//Turn it off
		ledRedOff();

		if (battMonGetXpgBattState() >= XPG_BATT_MEDIUM)
		{
			EVENT flashGreenEvent;

			//If the battery state has changed, now flash green
			ledGreenOn();

			//Turn it back off again
			flashGreenEvent.eventID = E_FLASH_GREEN_LED;
			swTimerSet( SWTMR_FLASH_GREEN_LED, LED_ON_TIME, flashGreenEvent, false);
		}
		else
		{
			//Turn it back on again
			EVENT flashRedEvent;

			flashRedEvent.eventID = E_FLASH_RED_LED;
			swTimerSet( SWTMR_FLASH_RED_LED, LED_FLASH_PERIOD/2 - LED_ON_TIME/4, flashRedEvent, false);
		}
	}
	else
	{
		if (battMonGetXpgBattState() >= XPG_BATT_MEDIUM)
		{
			EVENT flashGreenEvent;

			//If the battery state has changed, now flash green
			ledGreenOn();

			//Turn it back off again
			flashGreenEvent.eventID = E_FLASH_GREEN_LED;
			swTimerSet( SWTMR_FLASH_GREEN_LED, LED_ON_TIME, flashGreenEvent, false);
		}
		else
		{
			EVENT flashRedEvent;

			//Turn it back on again
			ledRedOn();

			flashRedEvent.eventID = E_FLASH_RED_LED;
			swTimerSet( SWTMR_FLASH_RED_LED, LED_ON_TIME/4, flashRedEvent, false);
		}
	}

	return;
}

/**
 * The event handler for the E_FLASH_GREEN_LED event.
 *
 * \param event - The event to handle.
 */
static void eventHandlerE_FLASH_GREEN_LED(EVENT event)
{
	(void)event;

	if (ledIsGreenLedOn())
	{
		//Turn it off
		ledGreenOff();

		if (battMonGetXpgBattState() >= XPG_BATT_MEDIUM)
		{
			//Turn it back on again
			EVENT flashGreenEvent;
			flashGreenEvent.eventID = E_FLASH_GREEN_LED;

			swTimerSet( SWTMR_FLASH_GREEN_LED, LED_FLASH_PERIOD - LED_ON_TIME, flashGreenEvent, false);
		}
		else
		{
			EVENT flashRedEvent;

			//If the battery state has changed, now flash red
			ledRedOn();

			//Turn it back off again
			flashRedEvent.eventID = E_FLASH_RED_LED;
			swTimerSet( SWTMR_FLASH_RED_LED, LED_ON_TIME/4, flashRedEvent, false);
		}
	}
	else
	{
		if (battMonGetXpgBattState() >= XPG_BATT_MEDIUM)
		{
			EVENT flashGreenEvent;

			//If the battery state has changed, now flash green
			ledGreenOn();

			//Turn it back off again
			flashGreenEvent.eventID = E_FLASH_GREEN_LED;
			swTimerSet( SWTMR_FLASH_GREEN_LED, LED_ON_TIME, flashGreenEvent, false);
		}
		else
		{
			EVENT flashRedEvent;

			//Turn it back on again
			ledRedOn();

			flashRedEvent.eventID = E_FLASH_RED_LED;
			swTimerSet( SWTMR_FLASH_RED_LED, LED_ON_TIME/4, flashRedEvent, false);
		}
	}

	return;
}

/**
 * The event periodically ensures all of the LEDS are in the correct state.
 *
 * \param event - The event to handle.
 */
static void eventHandlerE_RESET_LED_STATES(EVENT event)
{
	(void)event;

	//Turn them all off
	ledGreenOff();
	ledRedOff();
	ledStimOff();

	//Cancel any timers
	swTimerCancel(SWTMR_FLASH_GREEN_LED);
	swTimerCancel(SWTMR_FLASH_RED_LED);
	swTimerCancel(SWTMR_FLASH_STIM_LED);

	//First handle the power LEDs
	if (battMonGetXpgBattState() >= XPG_BATT_MEDIUM)
	{
		EVENT flashGreenEvent;

		//Flash the green led on
		ledGreenOn();

		//Turn it off after a period of time
		flashGreenEvent.eventID = E_FLASH_GREEN_LED;
		swTimerSet( SWTMR_FLASH_GREEN_LED, LED_ON_TIME, flashGreenEvent, false);
	}
	else
	{
		EVENT flashRedEvent;

		//Flash the red led on
		ledRedOn();

		//Turn it off after a period of time
		flashRedEvent.eventID = E_FLASH_RED_LED;
		swTimerSet( SWTMR_FLASH_RED_LED, LED_ON_TIME/2, flashRedEvent, false);
	}

	//Now handle the stim LED
	if (isStimOn())
	{
		EVENT flashStimEvent;

		//Turn it on
		ledStimOn();

		//Turn it back off again
		flashStimEvent.eventID = E_FLASH_STIM_LED;
		swTimerSet( SWTMR_FLASH_STIM_LED, LED_ON_TIME, flashStimEvent, false);
	}
}

#endif // #ifdef EPG
