/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: Battery Monitor Module.
 *
 ***************************************************************************/

// #define BM_PRINT_OUTPUT	/ to turn on/off print debug output for Battery Monitor module
#ifdef BM_PRINT_OUTPUT
#include <stdio.h>
#endif

#include <stdbool.h>
#include <stddef.h>

#include "BatteryMonitor.h"
#include "state_machine_common.h"
#include "PowerASIC.h"
#include "MultiChannelTimer.h"
#include "EventQueue.h"
#include "system_events.h"
#include "Common/Protocol/gen_cals.h"
#include "cal.h"
#include "StimManager.h"
#include "ChargeManager.h"
#include "EventQueue.h"
#include "log.h"
#include "log_events.h"
#include "const.h"
#include "counter.h"
#include "Common/Protocol/counter_id.h"
#include "system.h"
#include "led.h"

//////////////////////////////////////////////////////////////////////////////////////////////
// Battery Monitor States
//////////////////////////////////////////////////////////////////////////////////////////////
typedef enum BATTMON_STATE {
	 BATTMON_STATE_IDLE				=0,
	 BATTMON_STATE_CHARGING			=1,
	 BATTMON_STATE_STIM_BATT_HIGH	=2,
	 BATTMON_STATE_STIM_BATT_LOW	=3,
	 BATTMON_STATE_STIM_BATT_CRIT	=4,
	 BATTMON_STATE_ALL				=5,
	 BATTMON_STATE_UNKNOWN			=6
} BATTMON_STATE;

#ifdef BM_PRINT_OUTPUT
static char *battMonStrings[7] = 
{
	"BATTMON_STATE_IDLE",
	"BATTMON_STATE_CHARGING",
	"BATTMON_STATE_STIM_BATT_HIGH",
	"BATTMON_STATE_STIM_BATT_LOW",
	"BATTMON_STATE_STIM_BATT_CRIT",
	"BATTMON_STATE_ALL",
	"BATTMON_STATE_UNKNOWN"
};
#endif

// Battery Charge Level State Thresholds - used as indexes into the GEN_CALS batteryStateCalStimOff & batteryStateCalStimOn values
enum BATT_CHARGE_THRESHOLD {
	 BATT_CHARGE_THRESHOLD_FULL=0,
	 BATT_CHARGE_THRESHOLD_HIGH=1,
	 BATT_CHARGE_THRESHOLD_LOW=2,
	 BATT_CHARGE_THRESHOLD_WARNING=3,
	 BATT_CHARGE_THRESHOLD_CRITICAL=4,
	 BATT_CHARGE_THRESHOLD_CUTOFF=5
};

//////////////////////////////////////////////////////////////////////////////////////////////
// Event handlers and table for the Battery Monitor
//////////////////////////////////////////////////////////////////////////////////////////////
static void eventHandlerE_INITIALIZE(EVENT event);
static void eventHandlerE_BATT_MEASURE_BATTERY(EVENT event);
static void eventHandlerE_STIM_STARTED(EVENT event);
static void eventHandlerE_STIM_STOPPED(EVENT event);
static void eventHandlerE_CHARGER_ENGAGED(EVENT event);
static void eventHandlerE_CHARGER_DISENGAGED(EVENT event);

/////////////////////////////////////////////////////////////////////////////////////
// Module state variables
static BATTMON_STATE battMonStateMachineState=BATTMON_STATE_UNKNOWN;
static BATT_CHARGE_STATE battChargeState=BATT_CHARGE_STATE_UNKNOWN;	// variable to store the current battery charge state in
static uint16_t battVoltageAdcCounts = 0xFFFF;						// keep the most recent battery voltage adc counts reading here
static const uint16_t *currentBatteryThresholdTable;

/////////////////////////////////////////////////////////////////////////////////////
//	Transition functions - Forward declarations
static BATTMON_STATE battMonGetNextStateFromBatteryState(void);
static void battMonScheduleNextSample(void);
static void readBatteryVoltage(void);
static void calcAndStoreBatteryState(void);
static void enterStorageMode(void);
static void sendBatteryVoltageEvent(void);
static void sendStopStimEvent(void);
static void verifyAndLoadBatteryCalTable(bool stimOnTable);

#define BATT_MON_EVENT_TABLE_LEN 17
#define NUM_BATT_MEASURMENTS 3

static STATE_MACHINE_TABLE_ENTRY const battMonStateTable[BATT_MON_EVENT_TABLE_LEN] =
{
// there is one table entry for every event processed in each state
//   CURRENT STATE					EVENT ID				EVENT HANDLER
//   --------------------------------------------------------------------------------------------
DISPATCH_GEN_START(battMonProcessEvent)
	{BATTMON_STATE_UNKNOWN,			E_INITIALIZE,			eventHandlerE_INITIALIZE			},

	{BATTMON_STATE_IDLE,			E_BATT_MEASURE_BATTERY,	eventHandlerE_BATT_MEASURE_BATTERY	},
	{BATTMON_STATE_IDLE,			E_STIM_STARTED,			eventHandlerE_STIM_STARTED			},
	{BATTMON_STATE_IDLE,			E_CHARGER_ENGAGED,		eventHandlerE_CHARGER_ENGAGED		},

	{BATTMON_STATE_CHARGING,		E_BATT_MEASURE_BATTERY,	eventHandlerE_BATT_MEASURE_BATTERY	},
	{BATTMON_STATE_CHARGING,		E_CHARGER_DISENGAGED,	eventHandlerE_CHARGER_DISENGAGED	},
	{BATTMON_STATE_CHARGING,		E_STIM_STARTED,			eventHandlerE_STIM_STARTED			},
	{BATTMON_STATE_CHARGING,		E_STIM_STOPPED,			eventHandlerE_STIM_STOPPED			},

	{BATTMON_STATE_STIM_BATT_HIGH,	E_BATT_MEASURE_BATTERY,	eventHandlerE_BATT_MEASURE_BATTERY	},
	{BATTMON_STATE_STIM_BATT_HIGH,	E_STIM_STOPPED,			eventHandlerE_STIM_STOPPED			},
	{BATTMON_STATE_STIM_BATT_HIGH,	E_CHARGER_ENGAGED,		eventHandlerE_CHARGER_ENGAGED		},

	{BATTMON_STATE_STIM_BATT_LOW,	E_BATT_MEASURE_BATTERY,	eventHandlerE_BATT_MEASURE_BATTERY	},
	{BATTMON_STATE_STIM_BATT_LOW,	E_STIM_STOPPED,			eventHandlerE_STIM_STOPPED			},
	{BATTMON_STATE_STIM_BATT_LOW,	E_CHARGER_ENGAGED,		eventHandlerE_CHARGER_ENGAGED		},

	{BATTMON_STATE_STIM_BATT_CRIT,	E_BATT_MEASURE_BATTERY,	eventHandlerE_BATT_MEASURE_BATTERY	},
	{BATTMON_STATE_STIM_BATT_CRIT,	E_STIM_STOPPED,			eventHandlerE_STIM_STOPPED			},
	{BATTMON_STATE_STIM_BATT_CRIT,	E_CHARGER_ENGAGED,		eventHandlerE_CHARGER_ENGAGED		}
DISPATCH_GEN_END
};

/**
 * The event processor for the Battery Monitor.  The Dispatcher sends
 * any events that Battery Monitor is subscribed on to this routine.
 *
 * \param eventToProcess - The event to process.
 */
void battMonProcessEvent(EVENT eventToProcess) {

	int i;

	EVENT_ID eventId = eventToProcess.eventID;

	// lookup event handler and call it
	for(i=0; i < BATT_MON_EVENT_TABLE_LEN; i++)
	{
		if((battMonStateTable[i].state == battMonStateMachineState || battMonStateTable[i].state == BATTMON_STATE_ALL)  && battMonStateTable[i].eventId == eventId)
		{
			break;
		}
	}

	if(i != BATT_MON_EVENT_TABLE_LEN)
	{
		// We have found an event handler for this State / Event combination
		// Call the handler for the event
		battMonStateTable[i].eventHandler(eventToProcess);
	}
}


/**
 * The event handler for the E_INITIALIZE event.
 *
 * \param event - The event to handle.
 */
static void eventHandlerE_INITIALIZE(EVENT event)
{
	(void) event;		// Parameter not used - prevents compiler warning.

	#ifdef BM_PRINT_OUTPUT
		//printf("BattMon - E_INITIALIZE\n");
		printf("BattMon::%s - E_INITIALIZE\n", battMonStrings[battMonStateMachineState]);
	#endif

	battMonStateMachineState = BATTMON_STATE_IDLE;

	verifyAndLoadBatteryCalTable(false);

	// Get a battery voltage reading and calculate the current battery charge state
	readBatteryVoltage();
	calcAndStoreBatteryState();

	// Determine which state to go to:
	//   Charging if the charger is engaged
	//   else StimOn if Stimulation is on
	//   else Idle
#ifndef EPG
	if (chrgMgrIsChrgMgrOn())
	{
		battMonStateMachineState = BATTMON_STATE_CHARGING;
	}
	else if (isStimOn())
	{
		battMonStateMachineState = battMonGetNextStateFromBatteryState();
	}
	else 
	{
		battMonStateMachineState = BATTMON_STATE_IDLE;
	}
#else
	if (isStimOn())
	{
		battMonStateMachineState = battMonGetNextStateFromBatteryState();
	}
	else
	{
		battMonStateMachineState = BATTMON_STATE_IDLE;
	}
#endif

	battMonScheduleNextSample();
}

/**
 * The event handler for the E_BATT_MEASURE_BATTERY event.
 *
 * \param event - The event to handle.
 */
static void eventHandlerE_BATT_MEASURE_BATTERY(EVENT event)
{
	(void) event;		// Parameter not used - prevents compiler warning.

	#ifdef BM_PRINT_OUTPUT
		printf("BattMon::%s - E_BATT_MEASURE_BATTERY\n", battMonStrings[battMonStateMachineState]);
	#endif

	readBatteryVoltage();
	calcAndStoreBatteryState();

	// The E_BATT_MEASURE_BATTERY handler will do things differently depending on the current
	// state of Battery Monitor
	switch (battMonStateMachineState)
	{
		case BATTMON_STATE_IDLE:
			if (BATT_CHARGE_STATE_CUTOFF == battChargeState)
			{
				//Tick the battery depletion counter
				if (!constGetSystemStateBit(SYS_STATE_DEPLETION_FLAG))
				{
					counterIncrement(COUNTER_BATTERY_DEPLETION);

					//Sets a flag that prevents erroneous ticking of
					//the battery depletion counter for the same
					//depletion event.
					constSetSystemStateBit(SYS_STATE_DEPLETION_FLAG);
				}

				DBGI(f_battery_mon,0);
				enterStorageMode();
			}
			break;

		case BATTMON_STATE_CHARGING:
			sendBatteryVoltageEvent();
			break;

		case BATTMON_STATE_STIM_BATT_HIGH:
		case BATTMON_STATE_STIM_BATT_LOW:
			// The next state is dependent on the current battery charge state
			battMonStateMachineState = battMonGetNextStateFromBatteryState();
			if ( (BATT_CHARGE_STATE_STIM_CUTOFF == battChargeState) || (BATT_CHARGE_STATE_CUTOFF == battChargeState) )
			{
				sendStopStimEvent();
			}
			break;

		case BATTMON_STATE_STIM_BATT_CRIT:
			if ( (BATT_CHARGE_STATE_STIM_CUTOFF == battChargeState) || (BATT_CHARGE_STATE_CUTOFF == battChargeState) )
			{
				sendStopStimEvent();
			}
			break;

		default:
			break;
	}

	battMonScheduleNextSample();
}

/**
 * The event handler for the E_STIM_STARTED event.
 *
 * \param event - The event to handle.
 */
static void eventHandlerE_STIM_STARTED(EVENT event)
{
	#ifdef BM_PRINT_OUTPUT
		printf("BattMon::%s - E_STIM_STARTED\n", battMonStrings[battMonStateMachineState]);
	#endif

	(void) event;		// Parameter not used - prevents compiler warning.

	verifyAndLoadBatteryCalTable(true);

	if (BATTMON_STATE_IDLE == battMonStateMachineState)
	{
		// only change states if we are in the Idle state - then we are going into StimOn
		readBatteryVoltage();
		calcAndStoreBatteryState();

		battMonStateMachineState = battMonGetNextStateFromBatteryState();
	}

	battMonScheduleNextSample();
}

/**
 * The event handler for the E_STIM_STOPPED event.
 *
 * \param event - The event to handle.
 */
static void eventHandlerE_STIM_STOPPED(EVENT event)
{
	#ifdef BM_PRINT_OUTPUT
		printf("BattMon::%s - E_STIM_STOPPED\n", battMonStrings[battMonStateMachineState]);
	#endif

	(void) event;		// Parameter not used - prevents compiler warning.

	verifyAndLoadBatteryCalTable(false);

	if ( (BATTMON_STATE_STIM_BATT_HIGH == battMonStateMachineState) ||
		 (BATTMON_STATE_STIM_BATT_LOW  == battMonStateMachineState) ||
		 (BATTMON_STATE_STIM_BATT_CRIT == battMonStateMachineState) )
	{
		// only change states if we are in the Idle state
		battMonStateMachineState = BATTMON_STATE_IDLE;
	}

	//Update battery state
	readBatteryVoltage();
	calcAndStoreBatteryState();

	battMonScheduleNextSample();

	return;
}


/**
 * The event handler for the E_CHARGER_ENGAGED event.
 *
 * \param event - The event to handle.
 */
static void eventHandlerE_CHARGER_ENGAGED(EVENT event)
{
	#ifdef BM_PRINT_OUTPUT
		printf("BattMon::%s - E_CHARGER_ENGAGED\n", battMonStrings[battMonStateMachineState]);
	#endif

	(void) event;		// Parameter not used - prevents compiler warning.

	// The only thing the E_CHARGER_ENGAGED event needs to do is to
	// move to the Charging State.  It does this regardless of which
	// state the Battery Monitor is currently in
	battMonStateMachineState = BATTMON_STATE_CHARGING;

	//Update Battery State
	readBatteryVoltage();
	calcAndStoreBatteryState();

	battMonScheduleNextSample();

	return;
}

/**
 * The event handler for the E_CHARGER_DISENGAGED event.
 *
 * \param event - The event to handle.
 */
static void eventHandlerE_CHARGER_DISENGAGED(EVENT event)
{
	#ifdef BM_PRINT_OUTPUT
		printf("BattMon::%s - E_CHARGER_DISENGAGED\n", battMonStrings[battMonStateMachineState]);
	#endif

	(void) event;				// Parameter not used - prevents compiler warning.

	readBatteryVoltage();
	calcAndStoreBatteryState();

	battMonStateMachineState = battMonGetNextStateFromBatteryState();

	// Leave the Charging State. If Stim is ON go to the StimOn states
	// depending on what the current state of the battery is
	// Else go to the Idle State
	if ( !isStimOn() )
	{
		battMonStateMachineState = BATTMON_STATE_IDLE;
	}

	battMonScheduleNextSample();

	return;
}


/**
 * Setups up the timer to send us the next E_BATT_MEASURE_BATTERY event
 * after the specified period
 */
static void battMonScheduleNextSample(void)
{
	EVENT event;

	event.eventID = E_BATT_MEASURE_BATTERY;

	switch (battMonStateMachineState)
	{
		case BATTMON_STATE_IDLE:
			swTimerSet( SWTMR_READ_BATTERY, SWTMR_01_HOUR_TIMEOUT, event, false ) ;
			break;
		case BATTMON_STATE_CHARGING:
			swTimerSet( SWTMR_READ_BATTERY, SWTMR_05_MINUTE_TIMEOUT, event, false ) ;
			break;
		case BATTMON_STATE_STIM_BATT_HIGH:
			swTimerSet( SWTMR_READ_BATTERY, SWTMR_04_HOUR_TIMEOUT, event, false ) ;
			break;
		case BATTMON_STATE_STIM_BATT_LOW:
			swTimerSet( SWTMR_READ_BATTERY, SWTMR_01_HOUR_TIMEOUT, event, false ) ;
			break;
		case BATTMON_STATE_STIM_BATT_CRIT:
			swTimerSet( SWTMR_READ_BATTERY, SWTMR_15_MINUTE_TIMEOUT, event, false ) ;
			break;
		default:
			break;
	}

	return;
}

/**
 * Go out to the ADC module and get a battery voltage reading.  If there
 * is an ADC error then send out the appropiate error event.
 */
static void readBatteryVoltage(void)
{
	uint16_t newBattCounts = 0;

#ifdef EPG
	bool isStimLEDOn = ledIsStimLedOn();
	bool isGreenLEDOn = ledIsGreenLedOn();
	bool isRedLEDOn = ledIsRedLedOn();

	//Make sure the LEDs are off during measurement...
	if (!isIPG())
	{
		if (isStimLEDOn)
		{
			ledStimOff();
		}

		if (isGreenLEDOn)
		{
			ledGreenOff();
		}
		else if (isRedLEDOn)
		{
			ledRedOff();
		}
	}
#endif

	//Take measurements
	if ( OK_RETURN_VALUE == batteryGetBatteryADC(&newBattCounts) )
	{
		uint16_t additionalBattCounts = 0;
		int i = 0;

		for (i=0; i<NUM_BATT_MEASURMENTS; i++)
		{
			batteryGetBatteryADC(&additionalBattCounts);

			if (additionalBattCounts > newBattCounts)
			{
				newBattCounts = additionalBattCounts;
			}
		}

		// the adc says we got a good battery voltage reading
		battVoltageAdcCounts = newBattCounts;
	}
	else
	{
		// else the adc reported an error - send out an error event for the other modules
		// use the last good battery voltage reading and keep the battery in its current charge state
		EVENT lEvent;

		lEvent.eventID = E_ERR_ERROR_DETECTED;
		lEvent.eventData.eActiveErrorData = ACTERR_POWER_ASIC_ANALOG_SENSE_ERROR;
		dispPutEvent(lEvent);
	}

	//If this is an EPG and battery voltage dips below 2.5V, turn the BBC on.
	if (!isIPG() && (battVoltageAdcCounts < 2080))
	{
		pwrBuckBoostOn();
	}

#ifdef EPG
	//Turn any LEDs back on
	if (!isIPG())
	{
		if (isStimLEDOn)
		{
			ledStimOn();
		}

		if (isGreenLEDOn)
		{
			ledGreenOn();
		}
		else if (isRedLEDOn)
		{
			ledRedOn();
		}
	}
#endif
	return;
}

/**
 * Take the current voltage reading and use Battery Specification Tables
 * to determine the current Battery Charge State.  To meet the requirements 
 * from the functional spec. when the chargers is engaged the battery state won't
 * be allowed to go to a lower charge level state and when the charger isn't 
 * engaged it won't be allowed to go to a high charge level state.
 */
static void calcAndStoreBatteryState(void)
{
	BATT_CHARGE_STATE newBattChargeState;

	#ifdef BM_PRINT_OUTPUT
		printf("BattMon::%s - ADC Counts:%x\n", battMonStrings[battMonStateMachineState], battVoltageAdcCounts);
	#endif

	if (battVoltageAdcCounts > currentBatteryThresholdTable[BATT_CHARGE_THRESHOLD_FULL])
	{
		newBattChargeState = BATT_CHARGE_STATE_FULL;
	}
	else if (battVoltageAdcCounts > currentBatteryThresholdTable[BATT_CHARGE_THRESHOLD_HIGH])
	{
		newBattChargeState = BATT_CHARGE_STATE_HIGH;
	}
	else if (battVoltageAdcCounts > currentBatteryThresholdTable[BATT_CHARGE_THRESHOLD_LOW])
	{
		newBattChargeState = BATT_CHARGE_STATE_LOW;
	}
	else if (battVoltageAdcCounts > currentBatteryThresholdTable[BATT_CHARGE_THRESHOLD_WARNING])
	{
		newBattChargeState = BATT_CHARGE_STATE_WARNING;
	}
	else if (battVoltageAdcCounts > currentBatteryThresholdTable[BATT_CHARGE_THRESHOLD_CRITICAL])
	{
		newBattChargeState = BATT_CHARGE_STATE_CRITICAL;
	}
	else if (battVoltageAdcCounts > currentBatteryThresholdTable[BATT_CHARGE_THRESHOLD_CUTOFF])
	{
		newBattChargeState = BATT_CHARGE_STATE_STIM_CUTOFF;
	}
	else
	{
		newBattChargeState = BATT_CHARGE_STATE_CUTOFF;
	}

	// If the current battery charge state is BATT_CHARGE_STATE_UNKNOW the ignore the hysterisis routine.
	// Just set the battery charge level state to the new value and exit
	if (BATT_CHARGE_STATE_UNKNOWN == battChargeState)
	{
		battChargeState = newBattChargeState;
		return;
	}
	else
	{
		// the hysterisis routine
#ifndef EPG
		// implement hysteresis on battery charge level states (requirements 1031 & 1032)
		// note that a lower enum value is equivalent to a high battery charge level
		if ( chrgMgrIsCharging() )
		{
			if (newBattChargeState < battChargeState)
			{
				// then it is ok to go to the new state (i.e. a higher battery charge level)
				battChargeState = newBattChargeState;

				//Reset a flag such that we will tick the battery depletion counter
				//next time the battery reaches its cutoff threshold
				if (constGetSystemStateBit(SYS_STATE_DEPLETION_FLAG) && battChargeState != BATT_CHARGE_STATE_CUTOFF)
				{
					constResetSystemStateBit(SYS_STATE_DEPLETION_FLAG);
				}
			}
			// else don't change the battery charge state
		}
		else
		{
#endif
			// charging is not engaged
			if (newBattChargeState > battChargeState)
			{
				// then it is ok to go to the new state (i.e. a lower battery charge level)
				battChargeState = newBattChargeState;

				//Reset a flag such that we will tick the battery depletion counter
				//next time the battery reaches its cutoff threshold
				if (constGetSystemStateBit(SYS_STATE_DEPLETION_FLAG) && battChargeState != BATT_CHARGE_STATE_CUTOFF)
				{
					constResetSystemStateBit(SYS_STATE_DEPLETION_FLAG);
				}
			}
			// else don't change the battery charge state
#ifndef EPG
		}
#endif
	}

	return;
}

/**
 * Increment the "Storage Mode because of BATT_CUTOFF" counter. The enter Storage Mode.  
 * it isn't decided yet whether this module should do this or just send a message
 * to the some other module.
 */
static void enterStorageMode(void)
{
	EVENT event;

	#ifdef BM_PRINT_OUTPUT
		printf("BattMon::%s - Enter Storage Mode\n", battMonStrings[battMonStateMachineState]);
	#endif
	
	// The Battery Monitor has detected that the battery charge level is too low to operate any more
	event.eventID = E_ENTER_STORAGE_MODE;
	dispPutEvent(event);

	return;
}

/**
 * Send the E_PUBLISH_BATT_VOLTAGE event with the current battery
 * voltage as data.
 */
static void sendBatteryVoltageEvent(void)
{
	EVENT event;

	#ifdef BM_PRINT_OUTPUT
		printf("BattMon::%s - Send Battery Voltage Event\n", battMonStrings[battMonStateMachineState]);
	#endif

	// publish the current battery voltage - the Charger module needs this information during charging
	event.eventID = E_PUBLISH_BATT_VOLTAGE;
	event.eventData.eBatteryADCCounts = battVoltageAdcCounts;
	dispPutEvent(event);

	return;
}

/**
 * Send the E_STOP_STIM event.
 */
static void sendStopStimEvent(void)
{
	EVENT event;

	#ifdef BM_PRINT_OUTPUT
		printf("BattMon::%s - Send Stop Stim Event\n", battMonStrings[battMonStateMachineState]);
	#endif

	// Send a stop stimulation event
	event.eventID = E_STOP_STIM;
	dispPutEvent(event);

	logNormal(E_LOG_STIM_STOPPED_LOW_BATT,0);
		
	return;
}

/**
 * Returns the most recently sampled Battery Voltage
 *
 * \return The most recently sampled Battery Voltage.
 *
 */
uint16_t battMonGetLastSampledBatteryVoltage(void)
{
	return battVoltageAdcCounts;
}


/**
 * Clears the most recently sampled Battery Voltage to 0.
 *
 * \return void
 *
 */
void battMonClearLastSampledBatteryVoltage(void)
{
	battVoltageAdcCounts = 0;

	return;
}

/**
 * Returns the most recently sampled Battery Charge State
 * according to specification in SWEX 0085.
 *
 * \return The most recently sampled Battery Charge State.
 *
 */
XPG_BATT_STATE battMonGetXpgBattState(void)
{
	switch (battChargeState)
	{
		case BATT_CHARGE_STATE_FULL:
			return XPG_BATT_FULL;

		case BATT_CHARGE_STATE_HIGH:
			return XPG_BATT_HIGH;

		case BATT_CHARGE_STATE_LOW:
			return XPG_BATT_MEDIUM;

		case BATT_CHARGE_STATE_WARNING:
			return XPG_BATT_LOW;

		case BATT_CHARGE_STATE_CRITICAL:
		case BATT_CHARGE_STATE_STIM_CUTOFF:
		case BATT_CHARGE_STATE_CUTOFF:
		case BATT_CHARGE_STATE_UNKNOWN:
		default:
			return XPG_BATT_EMPTY;
	}
}

/**
 * Get an updated reading of Battery Voltage from the ADC
 * and then returns that value.
 *
 * \return A currently sampled reading of the battery voltage
 *         in adc counts.
 *
 */
uint16_t battMonGetCurrentBatteryVoltage(void)
{
	// get an updated battery voltage reading from the ADC
	readBatteryVoltage();
	return battVoltageAdcCounts;
}

/**
 * Verifies the Battery Threshold Table in Flash.  If the table
 * is corrupted then an error is signaled to the System Health
 * Monitor.  The local pointer is set to the correct threshold
 * table depending on whether Stimulation is on or off.  The
 * local pointer is updated even if the data is corrupted.
 *
 * \param stimOnTable - Indicates which Battery Threshold Table the
 * 				   		local pointer should point to.
 *
 */
static void verifyAndLoadBatteryCalTable(bool stimOnTable)
{

	if (!calIsGenCalsValid() )
	{
		dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
	}

	// get the set of threshold levels depending on whether Stim is On or Off
	if ( stimOnTable )
	{
		currentBatteryThresholdTable = calGetGenCals()->batteryStateCalStimOn;
	}
	else
	{
		currentBatteryThresholdTable = calGetGenCals()->batteryStateCalStimOff;
	}

	return;

}


/**
 * Determines if the Battery Charge Level is high enough to
 * allow safe Stimulation.
 * (i.e. at a state above BATT_CHARGE_STATE_STIM_CUTOFF)
 *
 * \return - TRUE if the Battery Charge state is higher
 *           than BATT_CHARGE_STATE_STIM_CUTOFF.
 *           FALSE if the Battery Charge state is:
 *              BATT_CHARGE_STATE_STIM_CUTOFF OR
 *              BATT_CHARGE_STATE_CUTOFF OR
 *              BATT_CHARGE_STATE_UNKNOWN
 */
bool battMonBatteryLevelOkToStim(void)
{
	if ( ( BATT_CHARGE_STATE_STIM_CUTOFF == battChargeState ) ||
		 ( BATT_CHARGE_STATE_CUTOFF == battChargeState ) ||
		 ( BATT_CHARGE_STATE_UNKNOWN == battChargeState )
		)
	{
		return (false);
	}
	else
	{
		return (true);
	}
}


/**
 * This function will determine the StimOn sub-state that the Battery
 * Monitor should move to based on the current battery charge state.
 * It is assumed that this function will only be called when the Battery
 * Monitor is entering the StimOn super-state.  This function will rely
 * on the global variable for the current battery charge state.
 */
static BATTMON_STATE battMonGetNextStateFromBatteryState(void)
{
	switch (battChargeState)
	{
		case BATT_CHARGE_STATE_FULL:
		case BATT_CHARGE_STATE_HIGH:
			return BATTMON_STATE_STIM_BATT_HIGH;
		case BATT_CHARGE_STATE_LOW:
		case BATT_CHARGE_STATE_WARNING:
			return BATTMON_STATE_STIM_BATT_LOW;
		case BATT_CHARGE_STATE_CRITICAL:
			return BATTMON_STATE_STIM_BATT_CRIT;
		case BATT_CHARGE_STATE_STIM_CUTOFF:
		case BATT_CHARGE_STATE_CUTOFF:
		case BATT_CHARGE_STATE_UNKNOWN:
			return BATTMON_STATE_STIM_BATT_CRIT;
		default:
			//The code should never get here but it is good to cover the unexpected with a default case.
			return BATTMON_STATE_STIM_BATT_CRIT;
	}
}

#ifdef VS_WINDOWS_SIMULATOR
// Functions to support the Visual Studio C# Simulator
__declspec(dllexport) int getBattMonState(void)
{
	return battMonStateMachineState;
}

__declspec(dllexport) int getBattChargeState(void)
{
	return battChargeState;
}
#endif
