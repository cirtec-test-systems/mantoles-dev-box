/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: System Health Monitor Module.
 *
 ***************************************************************************/

//#define SHM_PRINT_OUTPUT	// to turn on/off print debug output for System Health Monitor module
#ifdef SHM_PRINT_OUTPUT
#include <stdio.h>
#endif

#include <stdbool.h>
#include <string.h>

#include "state_machine_common.h"
#include "system_events.h"
#include "log_events.h"
#include "error_code.h"
#include "EventQueue.h"
#include "MultiChannelTimer.h"
#include "Watchdog.h"
#include "log.h"
#include "pgm.h"
#include "DataStore/Pgm/pgm_int.h"
#include "activePgm.h"
#include "GenericProgram.h"
#include "SystemHealthMonitor.h"
#include "swtmr_application_timers.h"
#include "PowerASIC.h"
#include "Stack.h"
#include "Common/Protocol/cmndGetPrgmDef.h"
#include "Common/Protocol/cmndConfigDeviceParams.h"
#include "Common/Protocol/cmndBackgroundImpedance.h"
#include "counter.h"
#include "cal.h"
#include "xpginfo.h"
#include "trim.h"
#include "Clock.h"
#include "Interrupt.h"
#include "PowerASIC.h"
#include "Flash.h"
#include "const.h"
#include "cdp.h"
#include "xpginfo.h"
#include "Stim.h"
#include "ticks.h"
#include "StimManager.h"
#include "Common/Protocol/cmndStim.h"
#include "delay.h"
#include "bgImp.h"
#include "system.h"

typedef enum MEMORY_CHECKS
{
	PRGM_CONST_CHECK = 0,			//Check of program constants
	CHAN_CALS_CHECK = 1,			//Check of channel calibrations
	HV_CALS_CHECK = 2,				//Check of high voltage calibration
	GEN_CALS_CHECK = 3,				//Check of general calibrations
	XPG_INFO_CHECK = 4,				//Check of xpg information
	TRIM_P_CHECK = 5,				//Check of pluto trims
	TRIM_Z_CHECK = 6,				//Check of zarlink trims
	TRIM_S_CHECK = 7,				//Check of saturn trims
	PULSE_CONST_CHECK = 8,			//Check of pulse constants
	CDP_CHECK = 9,					//Check the configurable device parameters
	RAMP_TIME_CHECK = 10,			//Check the ramp time
	LEAD_LIMITS_CHECK = 11,			//Check the lead limits
	BG_IMP_CHECK = 12,				//Check the background impedance parameters
	NUM_CHECKS = 13
} MEMORY_CHECKS;


//Define the states of the state machine
typedef enum SHM_STATE
{
	SHM_STATE_INITIAL				=0,	//Before initialization
	SHM_STATE_IDLE   				=1,	//Nothing going on, but powered up
	SHM_STATE_STIM   				=2,	//Actively Stimulating
	SHM_STATE_CHARGING				=3,	//Actively Charging
	SHM_STATE_CHARGING_STIM   		=4,	//Actively Changing and Stimulating
	SHM_STATE_ACTIVE_ERROR			=5, //Active Error has Occurred
	SHM_STATE_ERROR_CHARGING		=6, //Actively Charging when Active Error has Occurred
	SHM_STATE_STOPPING_STIM_STORAGE	=7,	//Actively Trying to Stop Stimulation Before Storage
	SHM_STATE_STOPPING_STIM_RESET	=8,	//Actively Trying to Stop Stimulation Before Reset
	SHM_STATE_STORAGE				=9,	//Storage Mode has been called => May Never Get Here
	SHM_STATE_ALL    				=10	//Represents every state to minimize code bloat
} SHM_STATE;

/*Event Handers*/
static void eventHandlerE_INITIALIZE(EVENT event);
static void eventHandlerE_CLOCK_VERIFY(EVENT event);
static void eventHandlerE_SERVICE_WATCHDOG(EVENT event);
static void eventHandlerE_STIM_STARTED(EVENT event);
static void eventHandlerE_STIM_STOPPED(EVENT event);
static void eventHandlerE_CHARGER_ENGAGED(EVENT event);
static void eventHandlerE_ERR_CHARGE_ERROR(EVENT event);
static void eventHandlerE_ERR_ERROR_DETECTED(EVENT event);
static void eventHandlerE_ENTER_STORAGE_MODE(EVENT event);
static void eventHandlerE_STOPPING_STIM(EVENT event);
static void eventHandlerE_CHARGER_DISENGAGED(EVENT event);
static void eventHandlerE_SOFTWARE_RESET(EVENT event);
static void eventHandlerE_BROWNOUT_DETECTED(EVENT event);
static void eventHandlerE_STIM_PULSE_GUARD(EVENT event);
static void eventHandlerE_ERROR_STOP_STIM(EVENT event);
static void eventHandlerE_PRGM_DEF_CHANGED(EVENT event);
static void eventHandlerE_LOG_EVENT(EVENT event);


/*Transition Functions*/
static int initializePolling(void);
static int serviceWatchdog(void);
static int startWatchdog(void);
static int stopWatchdog(void);
static int performIdleSystemChecks(void);
static int performStimulatingSystemChecks(void);
static int performStackCheck(void);
static bool isActive(void);
static int processSevereError(ACTIVE_ERROR_CODE errorCode);
static int processMajorError(ACTIVE_ERROR_CODE errorCode);
static int processMinorError(ACTIVE_ERROR_CODE errorCode);
static int processMemoryError(ACTIVE_ERROR_CODE errorCode);
static bool checkProgramDataIntegrity(void);
static bool checkIsMicrocontrollerClockOK(void);

static ACTIVE_ERROR_CODE getMajorError(void);


#define SHM_EVENT_TABLE_LEN 50

// Define the State Machine Table
static STATE_MACHINE_TABLE_ENTRY const shmStateTable[SHM_EVENT_TABLE_LEN] =
{
//   There is one table entry for every event processed in each state
//   CURRENT STATE			EVENT ID					EVENT HANDLER				        END STATE
//   ---------------------------------------------------------------------------------------------------------------------
DISPATCH_GEN_START(systemHealthMonitorProcessEvent)
	{SHM_STATE_INITIAL,					E_INITIALIZE,			eventHandlerE_INITIALIZE			/* SHM_STATE_IDLE  			*/},
	{SHM_STATE_IDLE,					E_CLOCK_VERIFY,			eventHandlerE_CLOCK_VERIFY			/* SHM_STATE_IDLE  			*/},
	{SHM_STATE_IDLE,					E_STIM_STARTED,			eventHandlerE_STIM_STARTED			/* SHM_STATE_STIM  			*/},
	{SHM_STATE_IDLE,					E_CHARGER_ENGAGED,		eventHandlerE_CHARGER_ENGAGED		/* SHM_STATE_CHARGING		*/},
	{SHM_STATE_IDLE,					E_ENTER_STORAGE_MODE,	eventHandlerE_ENTER_STORAGE_MODE	/* SHM_STATE_STORAGE		*/},
	{SHM_STATE_IDLE,					E_ERR_ERROR_DETECTED,	eventHandlerE_ERR_ERROR_DETECTED	/* MULTIPLE					*/},
	{SHM_STATE_CHARGING,				E_CHARGER_DISENGAGED,	eventHandlerE_CHARGER_DISENGAGED	/* SHM_STATE_IDLE			*/},
	{SHM_STATE_CHARGING,				E_CLOCK_VERIFY,			eventHandlerE_CLOCK_VERIFY			/* SHM_STATE_CHARGING		*/},
	{SHM_STATE_CHARGING,				E_SERVICE_WATCHDOG,		eventHandlerE_SERVICE_WATCHDOG		/* SHM_STATE_CHARGING		*/},
	{SHM_STATE_CHARGING,				E_STIM_STARTED,			eventHandlerE_STIM_STARTED			/* SHM_STATE_CHARGING_STIM	*/},
	{SHM_STATE_CHARGING,				E_ERR_ERROR_DETECTED,	eventHandlerE_ERR_ERROR_DETECTED	/* MULTIPLE					*/},
	{SHM_STATE_STIM,					E_STIM_STOPPED,			eventHandlerE_STIM_STOPPED			/* SHM_STATE_IDLE  			*/},
	{SHM_STATE_STIM,					E_SERVICE_WATCHDOG,		eventHandlerE_SERVICE_WATCHDOG		/* SHM_STATE_STIM  			*/},
	{SHM_STATE_STIM,       				E_CLOCK_VERIFY,         eventHandlerE_CLOCK_VERIFY		    /* SHM_STATE_STIM  			*/},
	{SHM_STATE_STIM,					E_CHARGER_ENGAGED,		eventHandlerE_CHARGER_ENGAGED		/* SHM_STATE_CHARGING_STIM	*/},
	{SHM_STATE_STIM,					E_ENTER_STORAGE_MODE,	eventHandlerE_STOPPING_STIM			/* SHM_STATE_STOPPING_STIM	*/},
	{SHM_STATE_STIM,					E_ERR_ERROR_DETECTED,	eventHandlerE_ERR_ERROR_DETECTED	/* SHM_STATE_ACTIVE_ERROR	*/},
	{SHM_STATE_STIM,					E_BROWNOUT_DETECTED,	eventHandlerE_BROWNOUT_DETECTED		/* SHM_STATE_STIM			*/},
	{SHM_STATE_STIM,					E_STIM_PULSE_GUARD, 	eventHandlerE_STIM_PULSE_GUARD		/* SHM_STATE_IDLE			*/},
	{SHM_STATE_STIM,					E_STIM_PARITY,			eventHandlerE_ERROR_STOP_STIM		/* SHM_STATE_IDLE			*/},
	{SHM_STATE_STIM,					E_STIM_SEQUENCER, 		eventHandlerE_ERROR_STOP_STIM		/* SHM_STATE_IDLE			*/},
	{SHM_STATE_STIM,					E_STIM_ADDRESS, 		eventHandlerE_ERROR_STOP_STIM		/* SHM_STATE_IDLE			*/},
	{SHM_STATE_STIM,					E_STIM_REGISTER_VALID,	eventHandlerE_ERROR_STOP_STIM		/* SHM_STATE_IDLE			*/},
	{SHM_STATE_STIM,					E_STIM_WRITE_PROTECT,	eventHandlerE_ERROR_STOP_STIM		/* SHM_STATE_IDLE			*/},
	{SHM_STATE_CHARGING_STIM,			E_CLOCK_VERIFY,			eventHandlerE_CLOCK_VERIFY			/* SHM_STATE_CHARGING_STIM	*/},
	{SHM_STATE_CHARGING_STIM,			E_SERVICE_WATCHDOG,		eventHandlerE_SERVICE_WATCHDOG		/* SHM_STATE_CHARGING_STIM  */},
	{SHM_STATE_CHARGING_STIM,			E_STIM_STOPPED,			eventHandlerE_STIM_STOPPED			/* SHM_STATE_CHARGING		*/},
	{SHM_STATE_CHARGING_STIM,			E_CHARGER_DISENGAGED,	eventHandlerE_CHARGER_DISENGAGED	/* SHM_STATE_STIM			*/},
	{SHM_STATE_CHARGING_STIM,			E_ERR_ERROR_DETECTED,	eventHandlerE_ERR_ERROR_DETECTED	/* MULTIPLE					*/},
	{SHM_STATE_CHARGING_STIM,			E_BROWNOUT_DETECTED,	eventHandlerE_BROWNOUT_DETECTED		/* SHM_STATE_CHARGING_STIME */},
	{SHM_STATE_CHARGING_STIM,			E_STIM_PULSE_GUARD,		eventHandlerE_STIM_PULSE_GUARD		/* SHM_STATE_CHARGING		*/},
	{SHM_STATE_CHARGING_STIM,			E_STIM_PARITY,			eventHandlerE_ERROR_STOP_STIM		/* SHM_STATE_CHARGING		*/},
	{SHM_STATE_CHARGING_STIM,			E_STIM_SEQUENCER,		eventHandlerE_ERROR_STOP_STIM		/* SHM_STATE_CHARGING		*/},
	{SHM_STATE_CHARGING_STIM,			E_STIM_ADDRESS, 		eventHandlerE_ERROR_STOP_STIM		/* SHM_STATE_IDLE			*/},
	{SHM_STATE_CHARGING_STIM,			E_STIM_REGISTER_VALID,	eventHandlerE_ERROR_STOP_STIM		/* SHM_STATE_IDLE			*/},
	{SHM_STATE_CHARGING_STIM,			E_STIM_WRITE_PROTECT,	eventHandlerE_ERROR_STOP_STIM		/* SHM_STATE_IDLE			*/},
	{SHM_STATE_ERROR_CHARGING,			E_SERVICE_WATCHDOG,		eventHandlerE_SERVICE_WATCHDOG		/* SHM_STATE_ERROR_CHARGING */},
	{SHM_STATE_ERROR_CHARGING,			E_CHARGER_DISENGAGED,	eventHandlerE_CHARGER_DISENGAGED	/* SHM_STATE_ERROR			*/},
	{SHM_STATE_ERROR_CHARGING,			E_ERR_ERROR_DETECTED,	eventHandlerE_ERR_ERROR_DETECTED	/* SHM_STATE_ERROR			*/},
	{SHM_STATE_ACTIVE_ERROR,			E_CHARGER_ENGAGED,		eventHandlerE_CHARGER_ENGAGED		/* SHM_STATE_ERROR_CHARGING */},
	{SHM_STATE_ACTIVE_ERROR,			E_ENTER_STORAGE_MODE,	eventHandlerE_ENTER_STORAGE_MODE	/* SHM_STATE_STORAGE_MODE	*/},
	{SHM_STATE_STOPPING_STIM_RESET,		E_STIM_STOPPED,			eventHandlerE_SOFTWARE_RESET		/* SHM_STATE_STORAGE_MODE	*/},
	{SHM_STATE_STOPPING_STIM_STORAGE,	E_STIM_STOPPED,			eventHandlerE_ENTER_STORAGE_MODE	/* SHM_STATE_STORAGE_MODE 	*/},
	{SHM_STATE_ALL,         			E_ERR_CHARGER_ERROR,   	eventHandlerE_ERR_CHARGE_ERROR      /* MULTIPLE    				*/},
	{SHM_STATE_ALL,         			E_ERR_EVENT_QUEUE_ERROR,eventHandlerE_ERR_ERROR_DETECTED    /* MULTIPLE  				*/},
	{SHM_STATE_ALL,						E_SOFTWARE_RESET,		eventHandlerE_SOFTWARE_RESET		/* MULTIPLE					*/},
	{SHM_STATE_ALL,						E_PRGM_DEF_CHANGED,		eventHandlerE_PRGM_DEF_CHANGED		/* MULTIPLE					*/},
	{SHM_STATE_ALL,						E_LOG_EVENT,			eventHandlerE_LOG_EVENT				/* MULTIPLE					*/}
DISPATCH_GEN_END
};

//Initialize the state machine and necessary
static SHM_STATE shmCurrentState = SHM_STATE_INITIAL;

//Private Variables
static bool chargePermission = true;
static bool stimPermission = true;
static ACTIVE_ERROR_CODE cat1ActiveErrorCode = ACTERR_NONE;
static ACTIVE_ERROR_CODE cat2ActiveErrorCode = ACTERR_NONE;
static ACTIVE_ERROR_CODE cat3ActiveErrorCode = ACTERR_NONE;
static CHARGE_ERROR_CODE chargeError = CHERR_NONE;
static bool stackErrorAlreadyLogged = false;

//Main Event Handler that processes all incoming events
void systemHealthMonitorProcessEvent(EVENT eventToProcess)
{
	int i = 0;
	EVENT_ID eventId = eventToProcess.eventID;

	#ifdef SHM_PRINT_OUTPUT
		printf("System Health Monitor - ProcessEvent\n");
	#endif


	// Iterate through the state machine table until find the current state.
	for(i=0; i < SHM_EVENT_TABLE_LEN; i++)
	{
		if((shmStateTable[i].state == shmCurrentState || shmStateTable[i].state == SHM_STATE_ALL)  && shmStateTable[i].eventId == eventId)
		{
			break;
		}
	}

	//Now call the appropriate event handler
	if(i != SHM_EVENT_TABLE_LEN)
	{
		shmStateTable[i].eventHandler(eventToProcess);
	}

	return;
}

/*
 *
 * EVENT HANDLER FUNCTIONS
 *
 */

//Transition Function for E_INITIALIZE Event
static void eventHandlerE_INITIALIZE(EVENT event)
{
	GET_COUNTER_PARAMS selectedProgramCounterParams;
	GET_COUNTER_RESPONSE_DATA selectedProgramCounterResp;

	(void) event;	// Event carries no data, so parameter unused.

	#ifdef SHM_PRINT_OUTPUT
		printf("System Health Monitor - E_INITIALIZE\n");
	#endif

	//Initialize stack check, timers, watchdog, etc.
	initializePolling();					//Sets timers for checks

	//Get any Category 2 errors that may still exist
	cat2ActiveErrorCode = getMajorError();

	//Check to see if reset occurred because of power on
	DBGI(f_sys_health_mon, (uint32_t)IFG1);

	if (wdIsResetFlag())
	{
		logError(E_LOG_ERR_WATCHDOG_RESET, 0);
		dispPutErrorEvent(ACTERR_WATCHDOG_ERROR);
	}
	else if (wdIsPowerOnResetFlag())
	{
		counterIncrement(COUNTER_STORAGE_MODE);
		logNormal(E_LOG_POWER_UP, 0);
	}
	else if (interruptIsNMI())
	{
		logError(E_LOG_NMI, 0);
	}
	else
	{
		logError(E_LOG_RESET_SYSTEM, 0);
	}

	//Enable flash access violation hardware faults
	flashEnableFlashAccessViolationFault();

	//Disable brownout detection until stim starts
	pwrDisableBrownOutDetection();

#ifndef DISABLE_STIM_RECOVERY
	//Add checking of stimulation recovery...
	selectedProgramCounterParams.eventCounterID = COUNTER_SELECTED_PROGRAM;
	counterCmdGetCounter(&selectedProgramCounterParams, &selectedProgramCounterResp);

	if (selectedProgramCounterResp.eventCount != 0 && wdIsResetFlag())
	{
		GET_COUNTER_PARAMS recoveryCounterParams;
		GET_COUNTER_RESPONSE_DATA recoverCounterResp;

		counterIncrement(COUNTER_STIM_RECOVER);

		recoveryCounterParams.eventCounterID = COUNTER_STIM_RECOVER;
		counterCmdGetCounter(&recoveryCounterParams, &recoverCounterResp);

		if (recoverCounterResp.eventCounterID < MAX_STIM_RECOVERY)
		{
			EVENT recoveryEvent;

			recoveryEvent.eventID = E_RECOVER_STIM;
			recoveryEvent.eventData.selectedProgram = selectedProgramCounterResp.eventCount;

			dispPutEvent(recoveryEvent);
		}
		else
		{
			counterSet(COUNTER_SELECTED_PROGRAM,0);
			counterSet(COUNTER_STIM_RECOVER,0);
		}
	}
#endif

	//Clear all the other faults
	wdClearResetFlag();
	interruptClearNMI();
	wdClearPowerOnResetFlag();
	wdClearWatchdogFault();

	//Set the state
	shmCurrentState = SHM_STATE_IDLE;

	return;
}

//Transition Function for E_CLOCK_VERIFY Event while idle
static void eventHandlerE_CLOCK_VERIFY(EVENT event)
{
	#ifdef SHM_PRINT_OUTPUT
		printf("System Health Monitor - E_CLOCK_VERIFY\n");
	#endif

	(void)event;

	//Feed the dog
	if (isActive())
	{
		serviceWatchdog();
	}

	//Perform checks
	if (shmCurrentState == SHM_STATE_STIM)
	{
		performStimulatingSystemChecks();
	}
	else
	{
		performIdleSystemChecks();
	}

	//NOTE: No change in state associated with this event

	return;
}


//Transition Function for E_SERVICE_WATCHDOG Event
static void eventHandlerE_SERVICE_WATCHDOG(EVENT event)
{
	#ifdef SHM_PRINT_OUTPUT
		printf("System Health Monitor - E_SERVICE_WATCHDOG\n");
	#endif

	(void)event;

	//Tickle the watchdog
	serviceWatchdog();

	//Make sure stack is no blown
	performStackCheck();

	return;
}

//Transition Function for E_STIM_ON Event
static void eventHandlerE_STIM_STARTED(EVENT event)
{
	EVENT clockCheckEvent;

	#ifdef SHM_PRINT_OUTPUT
		printf("System Health Monitor - E_STIM_STARTED\n");
	#endif

	(void)event;

	//Clear indication that stimulation was shutdown with magnet
	if (cat3ActiveErrorCode == ACTERR_STIM_SHUT_DOWN_WITH_EXTERNAL_MAGNET)
	{
		cat3ActiveErrorCode = ACTERR_NONE;
	}

	//Perform stimulation checks...
	performStimulatingSystemChecks();

	//Change state to stim
	if (shmCurrentState == SHM_STATE_CHARGING)
	{
		serviceWatchdog();
		shmCurrentState = SHM_STATE_CHARGING_STIM;
	}
	else
	{
		startWatchdog();
		shmCurrentState = SHM_STATE_STIM;
	}

	//pwrEnableBrownOutDetection();
	pwrDisableBrownOutDetection();


	//Set up to do clock checks at least every hour
	clockCheckEvent.eventID = E_CLOCK_VERIFY;
	swTimerSet( SWTMR_CHECK_HEALTH_STIM, SWTMR_01_HOUR_TIMEOUT,   clockCheckEvent,     true );

	return;
}

//Transition Function for E_STIM_OFF Event
static void eventHandlerE_STIM_STOPPED(EVENT event)
{
	#ifdef SHM_PRINT_OUTPUT
		printf("System Health Monitor - E_STIM_STOPPED\n");
	#endif

	(void)event;

	//Stop the hourly checks...
	swTimerCancel(SWTMR_CHECK_HEALTH_STIM);
	pwrDisableBrownOutDetection();

	//Go back to idle
	if (shmCurrentState == SHM_STATE_CHARGING_STIM)
	{
		serviceWatchdog();
		shmCurrentState = SHM_STATE_CHARGING;
	}
	else
	{
		stopWatchdog();
		shmCurrentState = SHM_STATE_IDLE;
	}

	//Perform checks while idle
	performIdleSystemChecks();

	return;
}

//Transition Function for E_ERR_ERROR_DETECTED Event
static void eventHandlerE_ERR_ERROR_DETECTED(EVENT event)
{
	ACTIVE_ERROR_CODE newErrorID = event.eventData.eActiveErrorData;
	
	#ifdef SHM_PRINT_OUTPUT
		printf("System Health Monitor - E_ERR_ERROR_DETECTED\n");
	#endif

	logNormal(E_LOG_ACTIVE_ERROR,newErrorID);

	if (newErrorID <= MAX_CAT1_ERROR)						//Category 1 - SEVERE
	{
		if (newErrorID == ACTERR_CRITICAL_NV_DATA_CORRUPTED     ||
			newErrorID == ACTERR_XPG_IDENTITY_PARAMS_CORRUPTED    )
		{
			processMemoryError(newErrorID);
		}
		else
		{
		#ifdef PLAN_C
			if (newErrorID == ACTERR_CLOCK_ERROR_MAJOR)
			{
				_disable_interrupts();

				if (clkIsOscillatorFault())
				{
					//Switch to VLO clocks to power off main crystal
					BCSCTL3 = LFXT1S_2 | XCAP_2;
					_bis_SR_register(OSCOFF);

					DELAY_MS(1);

					//Switch back to crystal
					_bis_SR_register(_get_SR_register() & ~OSCOFF);
					BCSCTL3 = LFXT1S_0 | XCAP_2;

					//Now wait for the oscillator to stabilize
					if (clkWaitForOscillatorFaultToClear() == false)
					{
						processSevereError(newErrorID);
					}
				}

				_enable_interrupts();
			}
			else
			{
				processSevereError(newErrorID);
			}
		#else
			processSevereError(newErrorID);
		#endif
		}
	}
	else if (newErrorID <= MAX_CAT2_ERROR)					//Category 2 - MAJOR
	{
		processMajorError(newErrorID);
	}
	else if (newErrorID <= MAX_CAT3_ERROR)					//Category 3 - MINOR
	{
		processMinorError(newErrorID);
	}
	else
	{
		//If we get an invalid error code...do nothing except log it
		logError(E_LOG_NORMAL_UNKNOWN_ERROR, newErrorID);
	}

	return;
}

//Transition Function for E_ERR_CHARGE_ERROR Event
static void eventHandlerE_ERR_CHARGE_ERROR(EVENT event)
{
	#ifdef SHM_PRINT_OUTPUT
		printf("System Health Monitor - E_ERR_CHARGE_ERROR\n");
	#endif

	if (chargeError != event.eventData.eChargeErrorData)
	{
		logNormal(E_LOG_CHARGING_ERROR,event.eventData.eChargeErrorData);

		if (event.eventData.eChargeErrorData == CHERR_THERMISTOR_ERROR)
		{
			logError(E_LOG_ERR_THERMISTOR_FAULT,0);
		}
	}

	chargeError = event.eventData.eChargeErrorData;

	return;
}

//Transition Function for E_STOPPING_STIM Event
static void eventHandlerE_STOPPING_STIM(EVENT event)
{
	(void)event;

	return;
}

//Transition Function for E_CHARGER_ENGAGED Event
static void eventHandlerE_CHARGER_ENGAGED(EVENT event)
{
	#ifdef SHM_PRINT_OUTPUT
		printf("System Health Monitor - eventHandlerE_CHARGER_ENGAGED\n");
	#endif

	(void)event;

	//On charge begin, reset the error
	chargeError = CHERR_NONE;

	if (shmCurrentState == SHM_STATE_IDLE)
	{
		shmCurrentState = SHM_STATE_CHARGING;
	}
	else if (shmCurrentState == SHM_STATE_STIM)
	{
		shmCurrentState = SHM_STATE_CHARGING_STIM;
	}
	else if (shmCurrentState == SHM_STATE_ACTIVE_ERROR)
	{
		if (chargePermission)
		{
			shmCurrentState = SHM_STATE_ERROR_CHARGING;
		}
		else
		{
			//Tell Charge Manager to stop charging!
			dispPutSimpleEvent(E_CHARGING_NOT_ALLOWED);
		}
	}

	if (!pwrVerifyTrims())
	{
		TRIM_LIST const *trimList = trimGetList(TRIM_PLUTO);

		if (pluto_ApplyTrimList(trimList) < 0)
		{
			dispPutErrorEvent(ACTERR_POWER_ASIC_ERROR_STIM_DISABLED);
		}
	}

	//Start the watchdog
	if (chargePermission)
	{
		startWatchdog();
	}

	return;
}

//Transition Function for E_CHARGER_DISENGAGED Event
static void eventHandlerE_CHARGER_DISENGAGED(EVENT event)
{
	#ifdef SHM_PRINT_OUTPUT
	printf("System Health Monitor - eventHandlerE_CHARGER_DISENGAGED\n");
	#endif

	(void)event;

	if (shmCurrentState == SHM_STATE_CHARGING)
	{
		//Stop the watchdog
		stopWatchdog();

		shmCurrentState = SHM_STATE_IDLE;
	}
	else if (shmCurrentState == SHM_STATE_CHARGING_STIM)
	{
		//Service the watchdog
		serviceWatchdog();

		shmCurrentState = SHM_STATE_STIM;
	}
	else if (shmCurrentState == SHM_STATE_ERROR_CHARGING)
	{
		//Stop the watchdog
		stopWatchdog();

		shmCurrentState = SHM_STATE_ACTIVE_ERROR;
	}

	return;
}

//Transition Function for E_ENTER_STORAGE_MODE Event
static void eventHandlerE_ENTER_STORAGE_MODE(EVENT event)
{
	#ifdef SHM_PRINT_OUTPUT
		printf("System Health Monitor - eventHandlerE_ENTER_STORAGE_MODE\n");
	#endif

	(void)event;

	if (shmCurrentState == SHM_STATE_STIM || shmCurrentState == SHM_STATE_CHARGING_STIM)
	{
		EVENT stimoffEvent;

		//Change the state
		shmCurrentState = SHM_STATE_STOPPING_STIM_STORAGE;

		//Make sure stim is off
		stimoffEvent.eventID = E_STOP_STIM ;
		dispPutEvent(stimoffEvent);

		logNormal(E_LOG_STIM_STOPPED_PWR_DOWN,0);
	}
	else
	{
		//Stop listening to any events...
		shmCurrentState = SHM_STATE_STORAGE;

		//Enter storage mode
		logNormal(E_LOG_STORAGE_MODE,0);

		//Stop charging
		chargeStopChargingProcess();

		//Shut down MICS chip
		micsShutdown();

		//Let the Power ASIC know to shutdown
		pwrEnterStorageMode();
	}

	return;
}

//Transition Function for E_SOFTWARE_RESET Event
static void eventHandlerE_SOFTWARE_RESET(EVENT event)
{
	#ifdef SHM_PRINT_OUTPUT
		printf("System Health Monitor - eventHandlerE_SOFTWARE_RESET\n");
	#endif

	(void)event;

	if (shmCurrentState == SHM_STATE_STIM || shmCurrentState == SHM_STATE_CHARGING_STIM)
	{
		EVENT stimoffEvent;

		//Change the state
		shmCurrentState = SHM_STATE_STOPPING_STIM_RESET;

		//Make sure stim is off
		stimoffEvent.eventID = E_STOP_STIM ;
		dispPutEvent(stimoffEvent);
	}
	else
	{
		//Stop listening to any events...
		shmCurrentState = SHM_STATE_STORAGE;

		//Disconnect Mics
		micsDisconnect();
		micsShutdown();

		//Reset Processor
		wdResetXpg();
	}
	return;
}

//Transition Function for E_BROWNOUT_DETECTED Event
static void eventHandlerE_BROWNOUT_DETECTED(EVENT event)
{
	#ifdef SHM_PRINT_OUTPUT
		printf("System Health Monitor - eventHandlerE_BROWNOUT_DETECTED\n");
	#endif

	(void)event;

	if (false == checkProgramDataIntegrity())
	{
		EVENT stimoffEvent;
		stimoffEvent.eventID = E_STOP_STIM ;
		dispPutEvent(stimoffEvent);
		dispPutErrorEvent(ACTERR_BROWN_OUT_DETECTED);
		logNormal(E_LOG_STIM_STOPPED_ERROR,ACTERR_BROWN_OUT_DETECTED);
	}

	logError(E_LOG_ERR_BROWNOUT, 0);

	return;
}

//Transition Function for E_STIM_PULSE_GUARD event
static void eventHandlerE_STIM_PULSE_GUARD(EVENT event)
{
	EVENT stimoffEvent;
	EVENT errorEvent;
	(void)event;

#ifdef SHM_PRINT_OUTPUT
	printf("System Health Monitor - eventHandlerE_STIM_PULSE_GUARD\n");
#endif

	stimoffEvent.eventID = E_STOP_STIM ;
	dispPutEvent(stimoffEvent);
	logNormal(E_LOG_STIM_STOPPED_ERROR,ACTERR_STIM_ASIC_RUN_ERROR_PULSE_GUARD);

	//Change state accordingly
	if (shmCurrentState == SHM_STATE_CHARGING_STIM)
	{
		shmCurrentState = SHM_STATE_CHARGING;
	}
	else if (shmCurrentState == SHM_STATE_STIM)
	{
		stopWatchdog();
		shmCurrentState = SHM_STATE_IDLE;
	}

	//Stop the hourly checks...
	swTimerCancel(SWTMR_CHECK_HEALTH_STIM);

	//Turn off brownout detection
	pwrDisableBrownOutDetection();

	//Log the error
	logError(E_LOG_ERR_PULSE_GUARD_TRIPPED, 0);

	errorEvent.eventID = E_ERR_ERROR_DETECTED;
	errorEvent.eventData.eActiveErrorData = ACTERR_STIM_ASIC_RUN_ERROR_PULSE_GUARD;
	dispPutEvent(errorEvent);

	return;
}

//Transition Function for E_STIM_PARITY event
static void eventHandlerE_ERROR_STOP_STIM(EVENT event)
{
	EVENT stimoffEvent;

#ifdef SHM_PRINT_OUTPUT
	printf("System Health Monitor - eventHandlerE_ERROR_STOP_STIM\n");
#endif

	stimoffEvent.eventID = E_STOP_STIM ;
	dispPutEvent(stimoffEvent);
	logNormal(E_LOG_STIM_STOPPED_ERROR,0);

	//Stop the hourly checks...
	swTimerCancel(SWTMR_CHECK_HEALTH_STIM);

	//Turn off brownout detection
	pwrDisableBrownOutDetection();

	switch (event.eventID)
	{
	case E_STIM_PARITY:
		logError(E_LOG_ERR_STIM_PARITY_ERROR, 0);
		break;
	case E_STIM_SEQUENCER:
		logError(E_LOG_ERR_STIM_SEQUENCER_ERROR, 0);
		break;
	case E_STIM_ADDRESS:
		logError(E_LOG_SATURN_ADDRESS_ERROR, 0);
		break;
	case E_STIM_REGISTER_VALID:
		logError(E_LOG_STIM_REGISTER_VALID, 0);
		break;
	case E_STIM_WRITE_PROTECT:
		logError(E_LOG_STIM_WRITE_PROTECT_ERROR, 0);
		break;
	default:
		break;
	}

	//Change state accordingly
	if (shmCurrentState == SHM_STATE_CHARGING_STIM)
	{
		shmCurrentState = SHM_STATE_CHARGING;
	}
	else if (shmCurrentState == SHM_STATE_STIM)
	{
		stopWatchdog();
		shmCurrentState = SHM_STATE_IDLE;
	}

	return;
}


//Transition Function for E_PRGRM_DEF_CHANGED event
static void eventHandlerE_PRGM_DEF_CHANGED(EVENT event)
{
	//Not using any event data
	(void)event;

	#ifdef SHM_PRINT_OUTPUT
		printf("System Health Monitor - eventHandlerE_PRGM_DEF_CHANGED\n");
	#endif

	//Get any Category 2 errors that may still exist
	cat2ActiveErrorCode = getMajorError();

	return;
}

//Transition Function for E_LOG_EVENT event
static void eventHandlerE_LOG_EVENT(EVENT event)
{
	logNormal(event.eventData.logEventData.logId,event.eventData.logEventData.data);

	return;
}

/*
 *
 * INTERNAL FUNCTIONS
 *
 */
//Sets up the polling rates for clock verification
static int initializePolling(void)
{
	EVENT event;
	event.eventID = E_CLOCK_VERIFY;

	#ifdef SHM_PRINT_OUTPUT
		printf("System Health Monitor - initializePolling\n");
	#endif

	//Set up to do checks at least every 24 hours
	swTimerSet( SWTMR_CHECK_HEALTH_DAILY, SWTMR_24_HOUR_TIMEOUT,   event,     true );

	return OK_RETURN_VALUE;
}


//Starts the Watchdog Timer and servicing watchdog
static int startWatchdog(void)
{
#ifdef SERVICE_WATCHDOG_TIMER
	EVENT wdEvent;

	//Set up to feed the watchdog timer
	wdEvent.eventID = E_SERVICE_WATCHDOG;
	swTimerSet( SWTMR_SERVICE_WATCHDOG, WATCHDOG_SERVICE_RATE, wdEvent, true );
#endif
	//Service watchdog to clear timer...then start the Watchdog
	wdServiceWatchdog();
	wdClearWatchdogTimer();
	wdEnable();

	return OK_RETURN_VALUE;

}


//Feed the watchdog timer
static int serviceWatchdog(void)
{
	#ifdef SHM_PRINT_OUTPUT
		printf("System Health Monitor - serviceWatchdog()\n");
	#endif

	#ifndef VS_WINDOWS_SIMULATOR
		wdServiceWatchdog();
	#endif

	return OK_RETURN_VALUE;
}


//Stops the Watchdog Timer and stops servicing watchdog
static int stopWatchdog(void)
{
	//Stop the Watchdog
	wdDisable();

#ifdef SERVICE_WATCHDOG_TIMER
	//Stop the polling timer
	swTimerCancel( SWTMR_SERVICE_WATCHDOG );
#endif
	return OK_RETURN_VALUE;
}

//System checks on an idle system
static int performIdleSystemChecks(void)
{
	EVENT errorEvent;

	#ifdef SHM_PRINT_OUTPUT
		printf("System Health Monitor - performIdleSystemChecks()\n");
	#endif

	//Ensure stack is not blown
	performStackCheck();

	if (!checkIsMicrocontrollerClockOK())
	{
		//Now place it in the log
		logError(E_LOG_ERR_CLOCK_VERIFICATION, 0);

		//Signal an active error
		errorEvent.eventID = E_ERR_ERROR_DETECTED;
		errorEvent.eventData.eActiveErrorData = ACTERR_CLOCK_ERROR_MAJOR;
		dispPutEvent(errorEvent);
	}

	return OK_RETURN_VALUE;
}

//System checks while stimulation is on
static int performStimulatingSystemChecks(void)
{
	#ifdef SHM_PRINT_OUTPUT
		printf("System Health Monitor - performStimulatingSystemChecks()\n");
	#endif

	//Ensure stack is not blown
	performStackCheck();

	return OK_RETURN_VALUE;
}

//System checks while stimulation is on
static int performStackCheck(void)
{
	#ifdef SHM_PRINT_OUTPUT
		printf("System Health Monitor - performStackCheck()\n");
	#endif

	//Always be checking the stack
	if (stackCheck() == ERROR_RETURN_VALUE && !stackErrorAlreadyLogged)
	{
		EVENT errorEvent;

		//Now place it in the log
		logError(E_LOG_ERR_STACK_EXCEEDED, 0);

		//Signal an active error
		errorEvent.eventID = E_ERR_ERROR_DETECTED;
		errorEvent.eventData.eActiveErrorData = ACTERR_INTERNAL_SW_ERROR_CAT1;
		dispPutEvent(errorEvent);

		//Set a flag to stop logging it again
		stackErrorAlreadyLogged = true;
	}

	return OK_RETURN_VALUE;
}

//Checks current program to ensure settings are within range
static bool checkProgramDataIntegrity()
{
	uint8_t numberOfPulses = gprgmGetNumPulses();
	uint8_t i = 0;
	RESPONSE response;
	GET_PRGM_DEF_PARAMS prgmDefParams;
	GET_PRGM_DEF_RESPONSE_DATA prgmDefResponse;

	prgmDefParams.program = pgm_SelectedProgramNumber();
	response = pgm_GetPrgmDef(&prgmDefParams, &prgmDefResponse);

	if (false == pgm_IsProgramNumberValid( pgm_SelectedProgramNumber() ))
	{
		return false;
	}

	if (GEN_SUCCESS != response)
	{
		return false;
	}

	if (prgmDefResponse.programDef.disabled > 0)
	{
		return false;
	}

	if (false == pgm_RangeCheckSetPrgmDef((SET_PRGM_DEF_PARAMS*)&prgmDefResponse))
	{
		return false;
	}

	if (actPgmGetActiveFreq() > PROGRAM_FREQUENCY_MAX)
	{
		return false;
	}

	for (i = 0; i < numberOfPulses; i++)
	{
		if (actPgmGetActiveAmpStepIndex(i) > 50 || actPgmGetActiveAmpStepIndex(i) < 1)
		{
			return false;
		}

		if (actPgmGetActiveVirtualAmpStepIndex(i) > 99 || actPgmGetActiveVirtualAmpStepIndex(i) < -48)
		{
			return false;
		}

		if (actPgmGetActivePulseWidth(i) > PULSE_WIDTH_MAX || actPgmGetActivePulseWidth(i) <  PULSE_WIDTH_MIN)
		{
			return false;
		}

		if (actPgmGetCalculatedPulseAmp(i) < AMPL_LOW_LIMIT_MIN)
		{
			return false;
		}
	}

	return true;
}


//Determine if currently charging or stimulating...
static bool isActive(void)
{
	//Return true if charging or stimulating
	if (   shmCurrentState == SHM_STATE_CHARGING
		|| shmCurrentState == SHM_STATE_CHARGING_STIM
		|| shmCurrentState == SHM_STATE_STIM
		|| shmCurrentState == SHM_STATE_ERROR_CHARGING)
	{
		return true;
	}
	else
	{
		return false;
	}
}


//Process Category 1 Error
static int processSevereError(ACTIVE_ERROR_CODE errorCode)
{
	cat1ActiveErrorCode = errorCode;

	serviceWatchdog();

	//Set permissions
	stimPermission = false;

	//According to SWEX 0091, Upon these two errors, disable charging
	if (errorCode == ACTERR_POWER_ASIC_ERROR_STIM_DISABLED || errorCode == ACTERR_POWER_ASIC_ANALOG_SENSE_ERROR)
	{
		chargePermission = false;
	}

	if (shmCurrentState == SHM_STATE_STIM)
	{
		EVENT stimoffEvent;

		//Stop the watchdog
		stopWatchdog();

		//Change the state
		shmCurrentState = SHM_STATE_ACTIVE_ERROR;

		//Make sure stim is off
		stimoffEvent.eventID = E_STOP_STIM ;
		dispPutEvent(stimoffEvent);

		logNormal(E_LOG_STIM_STOPPED_ERROR,errorCode);
	}
	else if (shmCurrentState == SHM_STATE_CHARGING_STIM)
	{
		EVENT stimoffEvent;

		//Change the state
		if (chargePermission)
		{
			//Inform Charge Manager to stop charging
			dispPutSimpleEvent(E_CHARGING_NOT_ALLOWED);

			shmCurrentState = SHM_STATE_ERROR_CHARGING;
		}
		else
		{
			shmCurrentState = SHM_STATE_ACTIVE_ERROR;

			stopWatchdog();
		}

		//Make sure stim is off
		stimoffEvent.eventID = E_STOP_STIM ;
		dispPutEvent(stimoffEvent);

		logNormal(E_LOG_STIM_STOPPED_ERROR,errorCode);
	}
	else if (shmCurrentState == SHM_STATE_IDLE)
	{
		shmCurrentState = SHM_STATE_ACTIVE_ERROR;

		stopWatchdog();
	}
	else if (shmCurrentState == SHM_STATE_CHARGING)
	{
		//Change the state
		if (chargePermission)
		{
			//Inform Charge Manager to stop charging
			dispPutSimpleEvent(E_CHARGING_NOT_ALLOWED);
			shmCurrentState = SHM_STATE_ERROR_CHARGING;
		}
		else
		{
			stopWatchdog();
			shmCurrentState = SHM_STATE_ACTIVE_ERROR;
		}
	}

	return OK_RETURN_VALUE;
}


//Process Category 2 Error
static int processMajorError(ACTIVE_ERROR_CODE errorCode)
{
	cat2ActiveErrorCode = errorCode;

	if (shmCurrentState == SHM_STATE_STIM)
	{
		EVENT stimoffEvent;

		stopWatchdog();

		//Change the state
		shmCurrentState = SHM_STATE_IDLE;

		//Make sure stim is off
		stimoffEvent.eventID = E_STOP_STIM ;
		dispPutEvent(stimoffEvent);

		logNormal(E_LOG_STIM_STOPPED_ERROR,errorCode);
	}
	else if (shmCurrentState == SHM_STATE_CHARGING_STIM)
	{
		EVENT stimoffEvent;

		//Change the state
		shmCurrentState = SHM_STATE_CHARGING;

		//Make sure stim is off
		stimoffEvent.eventID = E_STOP_STIM ;
		dispPutEvent(stimoffEvent);

		logNormal(E_LOG_STIM_STOPPED_ERROR,errorCode);
	}

	return OK_RETURN_VALUE;
}


//Process Category 3 Error
static int processMinorError(ACTIVE_ERROR_CODE errorCode)
{
	cat3ActiveErrorCode = errorCode;

	if (shmCurrentState == SHM_STATE_STIM)
	{
		EVENT stimoffEvent;

		stopWatchdog();

		//Change the state
		shmCurrentState = SHM_STATE_IDLE;

		//Make sure stim is off
		stimoffEvent.eventID = E_STOP_STIM ;
		dispPutEvent(stimoffEvent);

		logNormal(E_LOG_STIM_STOPPED_ERROR,errorCode);

	}
	else if (shmCurrentState == SHM_STATE_CHARGING_STIM)
	{
		EVENT stimoffEvent;

		//Change the state
		shmCurrentState = SHM_STATE_CHARGING;

		//Make sure stim is off
		stimoffEvent.eventID = E_STOP_STIM ;
		dispPutEvent(stimoffEvent);

		logNormal(E_LOG_STIM_STOPPED_ERROR,errorCode);
	}

	return OK_RETURN_VALUE;
}


//Process Category 1 Memory Error
static int processMemoryError(ACTIVE_ERROR_CODE errorCode)
{
	//This variable will store whether we were able to recover from error
	bool recovered[NUM_CHECKS] = {true,true,true,true,true,true,true,true,true,true,true,true,true};
	int iter = 0;
	bool failure = false;
	bool anythingRepaired = false;

	serviceWatchdog();

	//Check Channel Calibration Data
	if (!constIsPrgmConstValid())
	{
		//Log Error
		logError(E_LOG_ERR_PROGRAM_CONSTANTS_CORRUPTED, 0);

		//Attempt to Restore
		if (constIsPrgmConstBackupValid())
		{
			//Get the backup and set it into primary
			constSetPrimaryPrgmConst(constGetPrgmConstBackup());

			//See if primary copy is now valid
			if (!constIsPrgmConstValid())
			{
				recovered[PRGM_CONST_CHECK] = false;
			}
			else
			{
				anythingRepaired = true;
				logNormal(E_LOG_CONST_DATA_CORRUPTION_REPAIRED,0);
			}
		}
		else
		{
			recovered[PRGM_CONST_CHECK] = false;
		}
	}

	//Check Channel Calibration Data
	if (!constIsPulseConstValid())
	{
		//Log Error
		logError(E_LOG_ERR_PULSE_CONSTANTS_CORRUPTED, 0);

		//Attempt to Restore
		if (constIsPulseConstBackupValid())
		{
			//Get the backup and set it into primary
			constSetPrimaryPulseConst(constGetPulseConstBackup());

			//See if primary copy is now valid
			if (!constIsPulseConstValid())
			{
				recovered[PULSE_CONST_CHECK] = false;
			}
			else
			{
				anythingRepaired = true;
				logNormal(E_LOG_CONST_DATA_CORRUPTION_REPAIRED,0);
			}
		}
		else
		{
			recovered[PULSE_CONST_CHECK] = false;
		}
	}

	//Check Channel Calibration Data
	if (!calIsChannelCalsValid())
	{
		//Log Error
		logError(E_LOG_ERR_CHANNEL_CALIBRATION_CORRUPTED, 0);

		//Check the backup, restore if backup is good
		if (calIsChannelCalsBackupValid())
		{
			calSetPrimaryChannelCals(calGetChannelCalsBackup());

			//See if now valid
			if (!calIsChannelCalsValid())
			{
				recovered[CHAN_CALS_CHECK] = false;
			}
			else
			{
				anythingRepaired = true;
				logNormal(E_LOG_CAL_DATA_CORRUPTION_REPAIRED,0);
			}
		}
		else
		{
			recovered[CHAN_CALS_CHECK] = false;
		}
	}

	//Check High Voltage Calibration Data
	if (!calIsHVCalsValid())
	{
		//Log Error
		logError(E_LOG_ERR_HV_CALIBRATION_CORRUPTED, 0);

		//Check the backup, restore if backup is good
		if (calIsHVCalsBackupValid())
		{
			calSetPrimaryHVCals(calGetHVCalsBackup());

			//See if now valid
			if (!calIsHVCalsValid())
			{
				recovered[HV_CALS_CHECK] = false;
			}
			else
			{
				anythingRepaired = true;
				logNormal(E_LOG_HVCAL_DATA_CORRUPTION_REPAIRED,0);
			}
		}
		else
		{
			recovered[HV_CALS_CHECK] = false;
		}
	}

	//Check General Calibration Data
	if (!calIsGenCalsValid())
	{
		//Log Error
		logError(E_LOG_ERR_GENERAL_CALIBRATION_CORRUPTED, 0);

		if (calIsGenCalsBackupValid())
		{
			calSetPrimaryGenCals(calGetGenCalsBackup());

			//See if now valid
			if (!calIsGenCalsValid())
			{
				recovered[GEN_CALS_CHECK] = false;
			}
			else
			{
				anythingRepaired = true;
				logNormal(E_LOG_GENCAL_DATA_CORRUPTION_REPAIRED,0);
			}
		}
		else
		{
			recovered[GEN_CALS_CHECK] = false;
		}
	}

	//Check XPG Information Data
	if (!isXpgInfoValid())
	{
		//Log Error
		logError(E_LOG_ERR_MICS_ID_CORRUPTED, 0);

		//Attempt to Restore
		if (isXpgInfoBackupValid())
		{
			setPrimaryXpgInfo(getXpgInfoBackup());

			//See if now valid
			if (!isXpgInfoValid())
			{
				recovered[XPG_INFO_CHECK] = false;
			}
			else
			{
				anythingRepaired = true;
				logNormal(E_LOG_XPGINFO_DATA_CORRUPTION_REPAIRED,0);
			}
		}
		else
		{
			recovered[XPG_INFO_CHECK] = false;
		}

	}

	//Check Power ASIC Trims Data
	if (!trimIsListValid(TRIM_PLUTO))
	{
		//Log Error
		logError(E_LOG_ERR_PLUTO_TRIM_LIST_CORRUPTED, 0);

		//Attempt to Restore
		if (trimIsListBackupValid(TRIM_PLUTO))
		{
			trimSetPrimaryList(trimGetListBackup(TRIM_PLUTO));

			//See if primary copy is now valid
			if (!trimIsListValid(TRIM_PLUTO))
			{
				recovered[TRIM_P_CHECK] = false;
			}
			else
			{
				anythingRepaired = true;
				logNormal(E_LOG_POWER_TRIM_LIST_REPAIRED,0);
			}
		}
		else
		{
			recovered[TRIM_P_CHECK] = false;
		}
	}

	//Check Zarlink Trims Data
	if (!trimIsListValid(TRIM_ZL))
	{
		//Log Error
		logError(E_LOG_ERR_ZARLINK_TRIM_LIST_CORRUPTED, 0);

		//Attempt to Restore
		if (trimIsListBackupValid(TRIM_ZL))
		{
			//NOTE: the backup contains the factory values. That is what we fall back to here.
			trimSetPrimaryList(trimGetListBackup(TRIM_ZL));

			//See if primary copy is now valid
			if (!trimIsListValid(TRIM_ZL))
			{
				recovered[TRIM_Z_CHECK] = false;
			}
			else
			{
				anythingRepaired = true;
				logNormal(E_LOG_RADIO_TRIM_LIST_REPAIRED,0);
			}
		}
		else
		{
			recovered[TRIM_Z_CHECK] = false;
		}
	}

	//Check Stim ASIC Trims Data
	if (!trimIsListValid(TRIM_SATURN))
	{
		//Log Error
		logError(E_LOG_ERR_SATURN_TRIM_LIST_CORRUPTED, 0);

		//Attempt to Restore
		if (trimIsListBackupValid(TRIM_SATURN))
		{
			trimSetPrimaryList(trimGetListBackup(TRIM_SATURN));

			//See if primary copy is now valid
			if (!trimIsListValid(TRIM_SATURN))
			{
				recovered[TRIM_S_CHECK] = false;
			}
			else
			{
				anythingRepaired = true;
				logNormal(E_LOG_STIM_TRIM_LIST_REPAIRED,0);
			}
		}
		else
		{
			recovered[TRIM_S_CHECK] = false;
		}
	}

	//Check the configurable device parameters
	{
		CONFIG_DEVICE_PARAMETERS cdp;

		if (cdp_GetConfigDeviceParams(&cdp) != GEN_SUCCESS)
		{
			//Log Error
			logError(E_LOG_ERR_CONFIG_DEVICE_PARAMS_CORRUPTED, 0);

			if (cdp_GetConfigDeviceParamsBackup(&cdp) == GEN_SUCCESS)
			{
				if (cdp_SetPrimaryConfigDeviceParams(&cdp) != GEN_SUCCESS)
				{
					recovered[CDP_CHECK] = false;
				}
				else
				{
					anythingRepaired = true;
					logNormal(E_LOG_CDP_REPAIRED,0);
				}
			}
			else
			{
				recovered[CDP_CHECK] = false;
			}
		}
	}

	//Check the configurable device parameters
	if (!constIsRampTimeValid())
	{
		uint16_t rampTime;

		logNormal(E_LOG_ERR_RAMP_TIME_CORRUPTED,0);

		if (constGetRampTimeBackup(&rampTime) == GEN_SUCCESS)
		{
			if (constSetPrimaryRampTime(rampTime) != GEN_SUCCESS)
			{
				recovered[RAMP_TIME_CHECK] = false;
			}
			else
			{
				anythingRepaired = true;
				logNormal(E_LOG_RAMP_TIME_REPAIRED,0);
			}
		}
		else
		{
			recovered[RAMP_TIME_CHECK] = false;
		}
	}

	//Cannot be recovered, but check to see if lead limits are corrupted
	if (!constIsLeadLimitsValid())
	{
		recovered[LEAD_LIMITS_CHECK] = false;
	}

	//Cannot be recovered, but check to see if background impedance parameters are corrupted
	if (!bgImpIsBgImpParamsValid())
	{
		recovered[BG_IMP_CHECK] = false;
	}


	//See if there were any failures
	for (iter = 0; iter < NUM_CHECKS; iter++)
	{
		if (recovered[iter] == false)
		{
			failure = true;
		}
	}

	//Depending on whether we recovered from the error or not, change state
	if (!failure && !anythingRepaired)
	{
		//Do not need to change state...
		cat1ActiveErrorCode = ACTERR_NONE;
		stimPermission = true;
	}
	else
	{
		//Set the active error code..
		cat1ActiveErrorCode = errorCode;

		//Change state...stop stim - most likely stim is already stopped, but make sure
		if (shmCurrentState == SHM_STATE_STIM || shmCurrentState == SHM_STATE_CHARGING_STIM)
		{
			EVENT stimoffEvent;

			//Make sure stim is off
			stimoffEvent.eventID = E_STOP_STIM ;
			dispPutEvent(stimoffEvent);

			logNormal(E_LOG_STIM_STOPPED_ERROR, errorCode);
		}

		//Set stim permission to false
		stimPermission = false;

		//Go into error state
		if (shmCurrentState == SHM_STATE_STIM)
		{
			stopWatchdog();
			shmCurrentState = SHM_STATE_ACTIVE_ERROR;
		}
		else if (shmCurrentState == SHM_STATE_CHARGING_STIM || shmCurrentState == SHM_STATE_ERROR_CHARGING)
		{
			shmCurrentState = SHM_STATE_ERROR_CHARGING;
		}
	}

	return OK_RETURN_VALUE;
}


//Find out if there are any category 2 errors
static ACTIVE_ERROR_CODE getMajorError(void)
{
	ACTIVE_ERROR_CODE activeError = ACTERR_NONE;
	RESPONSE response;
	GET_PRGM_DEF_PARAMS pgrmParams;
	GET_PRGM_DEF_RESPONSE_DATA pgrmData;

	//Scan through the programs to see if there is a category 2 error
	uint16_t programMap = pgm_GetProgramMap();
	uint8_t  pgNum = 0;

	for(pgNum  = 1; pgNum <= NUM_PRGM_DEFS; pgNum++)
	{
		if ((programMap >> (pgNum-1)) & 0x1)
		{
			pgrmParams.program = pgNum;
			response = pgm_GetPrgmDef(&pgrmParams, &pgrmData);

			if (response != GEN_SUCCESS)
			{
				activeError = ACTERR_PROGRAM_DEFINITION_CORRUPTED;
				break;
			}
			else if (pgrmData.programDef.disabled > 0)
			{
				activeError = (ACTIVE_ERROR_CODE)pgrmData.programDef.disabled;
				break;
			}
		}
	}

	return activeError;
}

/*
 *
 * PUBLIC API FUNCTIONS
 *
 */

//Returns the current stimulation permission state
bool getStimulationPermission(void)
{
	return stimPermission;
}

//Returns the current charging permission state
bool getChargingPermission(void)
{
	return chargePermission;
}

//Returns the highest severity active error
uint8_t getActiveError(void)
{
	ACTIVE_ERROR_CODE activeError = ACTERR_NONE;

	//If there is a category 1 active error its simple
	if (cat1ActiveErrorCode != ACTERR_NONE)
	{
		activeError = cat1ActiveErrorCode;
	}
	else if (cat2ActiveErrorCode != ACTERR_NONE)
	{
		activeError = cat2ActiveErrorCode;
	}
	else if (cat3ActiveErrorCode != ACTERR_NONE)
	{
		activeError = cat3ActiveErrorCode;
	}

	return activeError;
}

//Returns the current charge error
uint8_t getChargeError(void)
{
	return chargeError;
}

//Checks to see if there is an existing oscillator fault
static bool checkIsMicrocontrollerClockOK(void)
{
#ifdef PLAN_C
	return clkWaitForOscillatorFaultToClear();
#else
	return !clkIsOscillatorFault();
#endif
}

void clearCategory3Error(void)
{
	if (cat3ActiveErrorCode != ACTERR_STIM_SHUT_DOWN_WITH_EXTERNAL_MAGNET)
	{
		cat3ActiveErrorCode = ACTERR_NONE;
	}

	return;
}

int shmGetCurrentState()
{
	return (int)shmCurrentState;
}
