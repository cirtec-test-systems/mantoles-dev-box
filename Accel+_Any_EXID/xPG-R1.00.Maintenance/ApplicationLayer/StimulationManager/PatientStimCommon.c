/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: Common Routines for Patient Stimulation Manager Module
 *					   and other Stimulation state machines.
 *
 ***************************************************************************/

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "PatientStimCommon.h"
#include "StimulationCommon.h"
#include "Common/Protocol/response_code.h"
#include "Stim.h"
#include "GenericProgram.h"
#include "pgm.h"
#include "activePgm.h"
#include "prgmDef.h"
#include "error_code.h"
#include "lastresponse.h"
#include "cdp.h"
#include "log.h"
#include "log_events.h"
#include "const.h"
#include "EventQueue.h"
#include "watchdog.h"
#include "system.h"
#include "cal.h"

extern PRGM_DEF 	pgm;

static int retrieveProgram(void)
{
	PRGM_DEF prgDef;
	PULSE_DEF *curPulseDef;
	uint8_t pulseMap, pulseNo;
	uint16_t calcPulseAmp;


	//Clear out data in structure to ensure data is blank and check data has been retrieved
	memset(&prgDef, 0, sizeof(PRGM_DEF));

	// Get the current selected Program Definition from NV RAM
	if (OK_RETURN_VALUE != pgm_GetCurInternalPrgmDef(&prgDef))
	{
		logError(E_LOG_ERR_RETRIEVE_PROGRAM_FAILED, 0);
		// last response code is set by pgm_GetCurInternalPrgmDef()
		return (ERROR_RETURN_VALUE);
	}

	if (prgDef.disabled)
	{
		setResponseCode(RESP_PROGRAM_DISABLED);
		return (ERROR_RETURN_VALUE);
	}

	// else continue filling in the generic program
	// note: The ramp value is filled in by initializeRamping() and incrementNextRampLevel() in PatientStimManager
	//       The compliance DAC value is calculated and set by gprgmAutoComplianceVoltage() which is called from startPatientStimulation()

	gprgmSetFreqIdx( actPgmGetActiveFreq() );	// Use the Active Frequency Index setting

	// Setup the each of the pulse definitions
	pulseMap = pgm_GetPulseMap();
	for(pulseNo = 0; 0 != pulseMap; pulseMap = pulseMap >> 1, pulseNo++)
	{
		curPulseDef = &(prgDef.pulseDef[pulseNo]);
		gprgmSetNumPulses(pulseNo+1);								// Adjust this everytime we find a pulse
		calcPulseAmp = actPgmGetCalculatedPulseAmp(pulseNo);
		gprgmSetAmplitude(pulseNo, calcPulseAmp);					//
		gprgmSetPulseWidth(pulseNo, actPgmGetActivePulseWidth(pulseNo));		//
		gprgmSetAmpPercent(pulseNo, curPulseDef->electrodeAmpPercentage);

		// Assume passive recovery for now - Automatic Waveform Adjustment will change these if Active Recovery is used
		gprgmSetInterphaseDelay(pulseNo, constPassiveInterphaseDelay());
		gprgmSetRecRatio(pulseNo,0);	//Set to Passive Recovery (0) as a default but gprgmAutoWaveformAdjustment() which is called in
										//startPatientStimulationwill determine the correct recovery type and ratio to use
		gprgmSetCBCWidth(pulseNo, constPassiveCBCWidth());


	}

	return(OK_RETURN_VALUE);
}

int performAutoSetupAndChecks(void)
{
	int status = 0;

	if ( (status = gprgmCheckDensities()) == 0 )
	{
		if ( (status = gprgmAutoWaveformAdjustment()) == 0 )
		{
			if ( (status = gprgmAutoUncontrolledSourceSink()) == 0 ) // see if I can remove this
			{
				status =  gprgmAutoComplianceVoltage();
			}
		}
	}

	return(status);
}

static int activateProgramChange(void)
{

	int status;

	if (  (status = retrieveProgram()) == 0)
	{
		if ( (status = performAutoSetupAndChecks()) == 0 )
		{
			if ( (status = gprgmRun(true)) != 0 )
			{
				// send an event to stim manager state machine to reset the state to idle and do cleanup
				dispPutSimpleEvent(E_STOP_STIM);
				DBG(f_sm_pat_common, DBG_SEV_ERROR, 0, 0);
			}
		}
		else
		{
			DBG(f_sm_pat_common, DBG_SEV_ERROR, 0, 0);
		}
	}
	else
	{
		DBG(f_sm_pat_common, DBG_SEV_ERROR, 0, 0);
	}
	return (status);
}

int startPatientStimulation(bool rampDisable)
{
	int status;

	DBG(f_sm_pat_common, DBG_SEV_NORM, 0, 0);

	if (  (status = retrieveProgram()) == 0)
	{
		if ( (status = performAutoSetupAndChecks()) == 0 )
		{
			if((status =  gprgmRun(rampDisable)) != 0)
			{
				DBG(f_sm_pat_common, DBG_SEV_ERROR, 0, 0);
			}
		}
		else
		{
			DBG(f_sm_pat_common, DBG_SEV_ERROR, 0, 0);
		}
	}
	else
	{
		DBG(f_sm_pat_common, DBG_SEV_ERROR, 0, 0);
	}

	return (status);
}

// ----------------------------------------------------------------------------------------------------------
int performDiagnosticOutputCheck()
{
	return(0);
}

int performBasicOutputCheck(void)
{
#ifdef ENABLE_BASIC_OUTPUT_CHECK
	int32_t balance[4];
	int i;
	int32_t	val;

	uint32_t disable = calGetGenCals()->tempStateCal[STIM_BASIC_OUTPUT_CHECK];

	if(disable != 0)
	{
		return(0);
	}

	// start the program running in calibration mode
	if(gprgmCurrentBalanceCheck(balance) == 0)
	{
		for(i=0; i<4; i++)
		{
			val = (balance[i] < 0) ? -1 *balance[i] : balance[i];
			DBGI(f_sm_pat_common, val);

			if ( balance[i] > CURRENT_BALANCE_THRESHOLD)
			{
				logError(E_LOG_ERR_BASIC_OUTPUT_CHECK_FAILED, balance[i]);
				DBG(f_sm_pat_common,DBG_SEV_ERROR, 0, 0);

				setResponseCode(RESP_BASIC_OUTPUT_CHECK_FAILED);
				return(-1);
			}
		}
	}
	else
	{
		logError(E_LOG_ERR_BASIC_OUTPUT_CHECK_FAILED, 0);
		DBG(f_sm_pat_common,DBG_SEV_ERROR, 0, 0);

		return(0);
	}

	DBG(f_sm_pat_common,DBG_SEV_NORM, 0, 0);
#endif
	return(0);
}

int performPulseGuardCheck()
{
#ifndef DISABLE_PULSE_GUARD
	uint8_t	i;
	uint8_t	src 	= 30;
	uint8_t sink 	= 30;

	uint32_t disable = calGetGenCals()->tempStateCal[STIM_PULSE_GUARD_CHECK];

	if(disable != 0)
	{
		return(0);
	}

	for(i=0; i<NUM_CHANNELS; i++)
	{
		if (pgm.pulseDef[0].electrodeAmpPercentage[i] != 0)
		{
			if(pgm.pulseDef[0].electrodeAmpPercentage[i] > 0)
			{
				src = i;
			}
			else
			{
				sink = i;
			}
			if(src != 30 && sink != 30)
			{
				if(gprgmPulseGuardCheck(src,sink) != true)
				{
					DBG(f_sm_pat_common,DBG_SEV_ERROR, 0, 0);
					setResponseCode(RESP_PULSE_GUARD_CHECK_FAILED);
					return(-1);
				}
				else
				{
					DBG(f_sm_pat_common,DBG_SEV_ERROR, 0, 0);
					return(0);
				}
			}
		}
	}

	DBG(f_sm_pat_common,DBG_SEV_ERROR, 0, 0);
	setResponseCode(RESP_PULSE_GUARD_CHECK_FAILED);
	return(-1);
#else
	return OK_RETURN_VALUE;
#endif
}

int performImpedanceCheck()
{
#ifndef DISABLE_BGI_CHECK
#ifdef EPG
	bool result;

	if(!isIPG())
	{
		if(gprgmEpgImpedanceAndOutputCapCheck(&result) == 0)
		{
			if(result == true)
			{
				// return OK if the impedance check passed
				return(0);
			}
			else
			{
				// return a fail if the impedance check failed
				return(-1);
			}
		}
		else
		{
			// return fail if the impedance check did not execute correctly
			return(-1);
		}
	}
	else
	{
		// return OK if this is not an EPG
		return(0);
	}
#else
	return(0);
#endif
#else
	return(0);
#endif
}

// ------------------------------------------------------------------------------------------------------
RESPONSE decrementFrequency(void)
{
	bool found = false;
	uint8_t originalFrequencyIndex = actPgmGetActiveFreq();
	uint8_t newFrequencyIndex = originalFrequencyIndex;

	while (found == false)
	{
		//Check to see if we are already at the min frequency
		if (newFrequencyIndex == 0)
		{
			return RESP_FREQ_CHANGE_NOT_ALLOWED;
		}

		newFrequencyIndex--;

		if (pgm_IsFrequencyValid(newFrequencyIndex))
		{
			found = true;
			break;
		}
	};

	if (!found)
	{
		return RESP_FREQ_CHANGE_NOT_ALLOWED;
	}

	actPgmSetActiveFreq(newFrequencyIndex);

	if ( ERROR_RETURN_VALUE == activateProgramChange() )
	{
		// set the frequency index back to what it was previously
		actPgmSetActiveFreq(originalFrequencyIndex);

		// activateProgramChange() or its subordinates set the last response code
		return RESPONSE_ERROR;
	}

	return (GEN_SUCCESS);
}

RESPONSE decrementProgramAmplitude(void)
{
	if ( actPgmAllActivePulseAmpsAtMin() || actPgmAnyActiveVirtualPulseAmpAtMin() )
	{
		return (RESP_ALL_AMPS_AT_MIN);
	}

	actPgmVirtualDecActiveAmpStepIndexes();
		
	if ( ERROR_RETURN_VALUE == activateProgramChange() )
	{
		actPgmVirtualIncActiveAmpStepIndexes();

		// activateProgramChange() or its subordinates set the last response code
		return RESPONSE_ERROR;
	}

	return (GEN_SUCCESS);
}

RESPONSE decrementPulseAmplitude(uint8_t pulseNo)
{
	RESPONSE retResp;
	
	// check if the pulse is valid
	if ( !pgm_IsPulseNumberValid(pulseNo) )
	{
		return (RESP_PULSE_NOT_VALID);
	}


	// actPgmNormalDecActiveAmpStepIndex will decrement the pulse and virtual pulse
	// indexes as appropiate or will return -1 if the pulse index is at its low limit
	retResp = actPgmNormalDecActiveAmpStepIndex(pulseNo);
	if (GEN_SUCCESS != retResp)
	{
		//then return whatever error response was generated by actPgmNormalIncActiveAmpStepIndex
		return (retResp);
	}

	if ( ERROR_RETURN_VALUE == activateProgramChange() )
	{
		actPgmNormalIncActiveAmpStepIndex(pulseNo);

		// activateProgramChange() or its subordinates set the last response code
		return RESPONSE_ERROR;
	}

	return (GEN_SUCCESS);

}

RESPONSE decrementPulseWidth(uint8_t pulseNo)
{
	uint16_t newPulseWidth, lastPulseWidth;
	
	// check if the pulse is valid
	if ( !pgm_IsPulseNumberValid(pulseNo) )
	{
		//then pulseNo is not a valid pulse
		return (RESP_PULSE_NOT_VALID);
	}

	// check if the new pulse width will violate pulseWidthHighLimit
	lastPulseWidth = actPgmGetActivePulseWidth(pulseNo);
	newPulseWidth = lastPulseWidth - cdp_GetPulseWidthStep();
	// if (newPulseWidth > curPulseWidth) in case newPulseWidth would wrapped around indicating its less than zero
	if ( (newPulseWidth < pgm_GetPulseWidthLowLimit(pulseNo)) || (newPulseWidth > lastPulseWidth) )
	{
		return (RESP_PW_EXCEEDS_LIMITS);
	}

	actPgmSetActivePulseWidth(pulseNo, newPulseWidth);
	if ( ERROR_RETURN_VALUE == activateProgramChange() )
	{
		// activateProgramChange() or its subordinates set the last response code
		// Set the pulse width back to last value if activateProgram Change fails
		actPgmSetActivePulseWidth(pulseNo, lastPulseWidth);
		return RESPONSE_ERROR;
	}

	return (GEN_SUCCESS);

}

RESPONSE incrementFrequency(void)
{
	bool found = false;
	uint8_t originalFrequencyIndex = actPgmGetActiveFreq();
	uint8_t newFrequencyIndex = originalFrequencyIndex;

	while (found == false)
	{
		newFrequencyIndex++;

		//Check to see if we are already at the max frequency
		if (newFrequencyIndex == MAX_FREQUENCY_SELECTIONS)
		{
			return RESP_FREQ_CHANGE_NOT_ALLOWED;
		}

		if (pgm_IsFrequencyValid(newFrequencyIndex))
		{
			found = true;
			break;
		}
	};

	if (!found)
	{
		return RESP_FREQ_CHANGE_NOT_ALLOWED;
	}

	actPgmSetActiveFreq(newFrequencyIndex);

	if ( ERROR_RETURN_VALUE == activateProgramChange() )
	{
		// set the frequency index back to what it was previously
		actPgmSetActiveFreq(originalFrequencyIndex);

		// activateProgramChange() or its subordinates set the last response code
		return RESPONSE_ERROR;
	}

	return (GEN_SUCCESS);
}

RESPONSE incrementProgramAmplitude(void)
{

	if ( actPgmAllActivePulseAmpsAtMax() || actPgmAnyActiveVirtualPulseAmpAtMax() )
	{
		return (RESP_ALL_AMPS_AT_MAX);
	}

	actPgmVirtualIncActiveAmpStepIndexes();
		
	if ( ERROR_RETURN_VALUE == activateProgramChange() )
	{
		actPgmVirtualDecActiveAmpStepIndexes();

		// activateProgramChange() or its subordinates set the last response code
		return RESPONSE_ERROR;
	}

	return (GEN_SUCCESS);
}

RESPONSE incrementPulseAmplitude(uint8_t pulseNo)
{
	RESPONSE retResp;

	// check if the pulse is valid
	if ( !pgm_IsPulseNumberValid(pulseNo) )
	{
		return (RESP_PULSE_NOT_VALID);
	}

	// actPgmNormalIncActiveAmpStepIndex will increment the pulse and virtual pulse
	// indexes as appropiate or will return -1 if the pulse index is at its high limit
	retResp = actPgmNormalIncActiveAmpStepIndex(pulseNo);
	if (GEN_SUCCESS != retResp)
	{
		//then return whatever error response was generated by actPgmNormalIncActiveAmpStepIndex
		return (retResp);
	}

	if ( ERROR_RETURN_VALUE == activateProgramChange() )
	{
		actPgmNormalDecActiveAmpStepIndex(pulseNo);

		// activateProgramChange() or its subordinates set the last response code
		return RESPONSE_ERROR;
	}

	return (GEN_SUCCESS);
}

RESPONSE incrementPulseWidth(uint8_t pulseNo)
{
	uint16_t newPulseWidth, lastPulseWidth;

	// check if the pulse is valid
	if ( !pgm_IsPulseNumberValid(pulseNo) )
	{
		return (RESP_PULSE_NOT_VALID);
	}

	// check if the new pulse width will violate pulseWidthHighLimit
	lastPulseWidth = actPgmGetActivePulseWidth(pulseNo);
	newPulseWidth = lastPulseWidth + cdp_GetPulseWidthStep();
	if ( newPulseWidth > pgm_GetPulseWidthHighLimit(pulseNo) )
	{
		//then pulseNo is not a valid pulse
		return (RESP_PW_EXCEEDS_LIMITS);
	}

	actPgmSetActivePulseWidth(pulseNo, newPulseWidth);
	if ( ERROR_RETURN_VALUE == activateProgramChange() )
	{
		// activateProgramChange() or its subordinates set the last response code

		// Set the pulse width back to last value if activateProgram Change fails
		actPgmSetActivePulseWidth(pulseNo, lastPulseWidth);

		return RESPONSE_ERROR;
	}

	return (GEN_SUCCESS);
}

RESPONSE setStimPulseAmplitude(uint8_t pulseNo, int ampStepIndex)
{
	RESPONSE retResp;
	int16_t lastAmpStepIndex;

	// check if the pulse is valid
	if ( !pgm_IsPulseNumberValid(pulseNo) )
	{
		return (RESP_PULSE_NOT_VALID);
	}

	lastAmpStepIndex = actPgmGetActiveAmpStepIndex(pulseNo);

  	retResp = actPgmSetActiveAmpStepIndex(pulseNo, ampStepIndex);
	if (GEN_SUCCESS != retResp)
	{
		//then return whatever error response was generated by actPgmNormalIncActiveAmpStepIndex
		return (retResp);
	}

	if ( ERROR_RETURN_VALUE == activateProgramChange() )
	{
		actPgmSetActiveAmpStepIndex(pulseNo, lastAmpStepIndex);

		// activateProgramChange() or its subordinates set the last response code
		return RESPONSE_ERROR;
	}

	return (GEN_SUCCESS);

}

RESPONSE setStimPulseWidth(uint8_t pulseNo, uint16_t pulseWidth)
{
	RESPONSE retResp;
	uint16_t lastPulseWidth;

	// check if the pulse is valid
	if ( !pgm_IsPulseNumberValid(pulseNo) )
	{
		return (RESP_PULSE_NOT_VALID);
	}

	lastPulseWidth = actPgmGetActivePulseWidth(pulseNo);

	if ( pulseWidth > pgm_GetPulseWidthHighLimit(pulseNo) ||
		 pulseWidth < pgm_GetPulseWidthLowLimit(pulseNo))
	{
		//then pulseNo is not a valid pulse
		return (RESP_PW_EXCEEDS_LIMITS);
	}

  	retResp = actPgmSetActivePulseWidth(pulseNo, pulseWidth);
	if (GEN_SUCCESS != retResp)
	{
		//then return whatever error response was generated by actPgmNormalIncActiveAmpStepIndex
		return (retResp);
	}

	if ( ERROR_RETURN_VALUE == activateProgramChange() )
	{
		actPgmSetActivePulseWidth(pulseNo, lastPulseWidth);

		// activateProgramChange() or its subordinates set the last response code
		return RESPONSE_ERROR;
	}

	return (GEN_SUCCESS);

}

RESPONSE setStimFrequency(uint8_t freqIndex)
{
	RESPONSE retResp;
	uint8_t	 lastFreqIndex;

	lastFreqIndex = actPgmGetActiveFreq();

	if (!pgm_IsFrequencyValid(freqIndex))
	{
		return RESP_FREQ_CHANGE_NOT_ALLOWED;
	}

  	retResp = actPgmSetActiveFreq(freqIndex);

  	if (GEN_SUCCESS != retResp)
	{
		//then return whatever error response was generated by actPgmSetActiveFreq
		return (retResp);
	}

	if ( ERROR_RETURN_VALUE == activateProgramChange() )
	{
		actPgmSetActiveFreq(lastFreqIndex);

		// activateProgramChange() or its subordinates set the last response code
		return RESPONSE_ERROR;
	}

	return (GEN_SUCCESS);
}
