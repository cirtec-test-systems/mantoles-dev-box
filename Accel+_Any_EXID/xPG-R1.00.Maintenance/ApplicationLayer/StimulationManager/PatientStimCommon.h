/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: Common Header file for the Patient Stimulation Manager module.
 *
 ***************************************************************************/
#ifndef PATIENT_STIM_COMMON_H_
#define PATIENT_STIM_COMMON_H_

#include <stdint.h>
#include <stdbool.h>


#include "Common/Protocol/response_code.h"

#define CURRENT_BALANCE_THRESHOLD 1000 // uAmps

int 	startPatientStimulation(bool rampDisable);
int 	stopPatientStimulation(void);

int 	performAutoSetupAndChecks(void);
int 	performDiagnosticOutputCheck();
int 	performBasicOutputCheck(void);
int 	performPulseGuardCheck();
int 	performImpedanceCheck();

RESPONSE decrementFrequency(void);
RESPONSE decrementProgramAmplitude(void);
RESPONSE decrementPulseAmplitude(uint8_t pulseNo);
RESPONSE decrementPulseWidth(uint8_t pulseNo);
RESPONSE incrementFrequency(void);
RESPONSE incrementProgramAmplitude(void);
RESPONSE incrementPulseAmplitude(uint8_t pulseNo);
RESPONSE incrementPulseWidth(uint8_t pulseNo);
RESPONSE setStimPulseAmplitude(uint8_t pulseNo, int ampStepIndex);
RESPONSE setStimPulseWidth(uint8_t pulseNo, uint16_t pulseWidth);
RESPONSE setStimFrequency(uint8_t freqIndex);

#define STIMSM_RESP_ERROR	(getLastResponseCode() == GEN_SUCCESS ? RESP_STIM_FAILED_UKNKNOWN_SW_ERROR : getLastResponseCode());

#endif
