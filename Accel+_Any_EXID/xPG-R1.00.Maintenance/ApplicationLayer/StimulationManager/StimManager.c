

#include "state_machine_common.h"
#include "Common/Protocol/response_code.h"
#include "Common/Protocol/cmndStim.h"
#include "string.h"

#include "system_events.h"
#include "const.h"
#include "lastresponse.h"
#include "cal.h"
#include "log.h"
#include "pgm.h"
#include "activePgm.h"
#include "counter.h"
#include "delay.h"
#include "EventQueue.h"

#include "StimManager.h"
#include "BatteryMonitor.h"
#include "PatientStimCommon.h"
#include "SystemHealthMonitor.h"

#include "GenericProgram.h"
#include "CommandProcessor.h"
#include "MultiChannelTimer.h"

#include "stim.h"
#include "Watchdog.h"
#include "clock.h"
#include "timer.h"

#include "system.h"

#define MINIMUM_INCREMENT_LOCKOUT 100	//100 ms
#define RAMP_STEPS	16

extern MICS_RX_BUFFER micsRxBuffer;
extern MICS_TX_BUFFER micsTxBuffer;

typedef enum STIMSM_STATE
{
	 STIMSM_IDLE		= 0,
	 STIMSM_TITR		= 1,
	 STIMSM_TEST		= 2,
	 STIMSM_IMPEDANCE	= 3,
	 STIMSM_CHAN_CAL	= 4,
	 STIMSM_RAMPING		= 5,
	 STIMSM_ACTIVE		= 6,
	 STIMSM_LOCKOUT		= 7,
	 STIMSM_ANY  		= 8
} STIMSM_STATE;

static void stimSmInit(EVENT event);
static void stimSmRecover(EVENT event);
static void stimSmSelect(EVENT event);
static void stimSmStart(EVENT event);
static void stimSmStop(EVENT event);
static void stimSmStopActive(EVENT event);
static void stimSmRampDone(EVENT event);
static void stimSmIncRunTime(EVENT event);
static void stimSmSetAmplitude(EVENT event);
static void stimSmSetPulseWidth(EVENT event);
static void stimSmSetFrequency(EVENT event);
static void stimSmIncProgAmplitude(EVENT event);
static void stimSmDecProgAmplitude(EVENT event);
static void stimSmIncPulseAmplitude(EVENT event);
static void stimSmDecPulseAmplitude(EVENT event);
static void stimSmIncPulseWidth(EVENT event);
static void stimSmDecPulseWidth(EVENT event);
static void stimSmIncFrequency(EVENT event);
static void stimSmDecFrequency(EVENT event);
static void stimSmLockoutDone(EVENT event);
static void stimSmSetTitr(EVENT event);
static void stimSmSetTest(EVENT event);
static void stimSmSetTestWF(EVENT event);
static void stimSmImpedance(EVENT event);
static void stimSmToggleStim(EVENT event);
static void stimSmMICSClosed(EVENT event);
static void stimSmInternalAbort(EVENT event);


#define STIM_EVENT_TABLE_LEN 31
static STATE_MACHINE_TABLE_ENTRY const stimStateTable[STIM_EVENT_TABLE_LEN] =
{
DISPATCH_GEN_START(stimSmProcessEvent)
	{STIMSM_IDLE,			E_INITIALIZE,						stimSmInit						},
	{STIMSM_IDLE,			E_RECOVER_STIM,						stimSmRecover					},
	{STIMSM_IDLE,			E_STIM_COMMAND,						stimSmStart						},

	{STIMSM_RAMPING,		E_STIM_COMMAND,						stimSmStopActive				},
	{STIMSM_RAMPING,		E_RAMP_TIMER,						stimSmRampDone					},

	{STIMSM_ACTIVE,			E_STIM_COMMAND,						stimSmStopActive				},


	{STIMSM_LOCKOUT,		E_STIM_COMMAND,						stimSmStopActive				},
	{STIMSM_LOCKOUT,		E_STIM_LOCKOUT_TIMER,				stimSmLockoutDone				},

	{STIMSM_TITR,			E_STIM_COMMAND,						stimSmStop						},
	{STIMSM_ANY,			E_SET_TITRATION_STIM,				stimSmSetTitr					},

	{STIMSM_TEST,			E_STIM_COMMAND,						stimSmStop						},
	{STIMSM_ANY,			E_SET_TEST_STIM,					stimSmSetTest					},
	{STIMSM_ANY,			E_SET_TEST_WAVEFORM,				stimSmSetTestWF					},

	{STIMSM_IMPEDANCE,		E_STIM_COMMAND,						stimSmStop						},
	{STIMSM_ANY,			E_IMPEDANCE_COMMAND,				stimSmImpedance					},

	{STIMSM_ANY,			E_SELECT_PROGRAM,					stimSmSelect					},

	{STIMSM_ANY,			E_PRG_RUN_TIME_TIMER,				stimSmIncRunTime				},

	{STIMSM_ANY,			E_STIM_SET_AMPLITUDE,				stimSmSetAmplitude				},
	{STIMSM_ANY,			E_SET_PULSE_WIDTH,					stimSmSetPulseWidth				},
	{STIMSM_ANY,			E_SET_FREQUENCY,					stimSmSetFrequency				},
	{STIMSM_ANY,			E_STIM_INCREASE_PROGRAM_AMPLITUDE,	stimSmIncProgAmplitude			},
	{STIMSM_ANY,			E_STIM_DECREASE_PROGRAM_AMPLITUDE,	stimSmDecProgAmplitude			},
	{STIMSM_ANY,			E_STIM_INCREASE_PULSE_AMPLITUDE,	stimSmIncPulseAmplitude			},
	{STIMSM_ANY,			E_STIM_DECREASE_PULSE_AMPLITUDE,	stimSmDecPulseAmplitude			},
	{STIMSM_ANY,			E_STIM_INCREASE_PULSE_WIDTH,		stimSmIncPulseWidth				},
	{STIMSM_ANY,			E_STIM_DECREASE_PULSE_WIDTH,		stimSmDecPulseWidth				},
	{STIMSM_ANY,			E_STIM_INCREASE_FREQUENCY,			stimSmIncFrequency				},
	{STIMSM_ANY,			E_STIM_DECREASE_FREQUENCY,			stimSmDecFrequency				},

	{STIMSM_ANY,			E_MAGNET_TOGGLE_STIM,				stimSmToggleStim				},
	{STIMSM_ANY,			E_MICS_SESSION_CLOSED,				stimSmMICSClosed				},
	{STIMSM_ANY,			E_STOP_STIM,						stimSmInternalAbort				}
	DISPATCH_GEN_END
};

static STIMSM_STATE stimSmState = STIMSM_IDLE;
static int 		g_rampTime;
static uint8_t	g_nsteps;

static uint16_t g_ramp = 0;

//------------------------------------------------------------------------------------------------
// STATIC FUNCIONS

static int LockAndLoad(EVENT event)
{

	if(event.eventData.eStimMode == STIM_ACTIVE)
	{
		if(!getStimulationPermission())
		{
			return -1;
		}

		if (pgm_SelectedProgramNumber() == 0)
		{
			setResponseCode(RESP_NO_PROGRAM_SELECTED);
			return -1;
		}
	}

	if (!battMonBatteryLevelOkToStim())
	{
		logNormal(E_LOG_STIM_STOPPED_LOW_BATT, 0);
		setResponseCode(RESP_BATTERY_TOO_LOW);
		return -1;
	}

	if (!constIsLeadLimitsValid())
	{
		logError(E_LOG_ERR_LEAD_INFORMATION_CORRUPTED, 0);
		dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
		setResponseCode(RESP_LEAD_LIMITS_CORRUPT);
		return -1;
	}

	if (!calIsChannelCalsValid())
	{
		logError(E_LOG_ERR_CHANNEL_CALIBRATION_CORRUPTED,0);
		dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
		setResponseCode(RESP_CHANNEL_CALS_CORRUPT);
		return -1;
	}

	if (!constIsPulseConstValid())
	{
		logError(E_LOG_ERR_PULSE_CONSTANTS_CORRUPTED, 0);
		dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
		setResponseCode(RESP_PULSE_CONSTANTS_CORRUPT);
		return -1;
	}

	if (!constIsPrgmConstValid())
	{
		logError(E_LOG_ERR_PROGRAM_CONSTANTS_CORRUPTED, 0);
		dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
		setResponseCode(RESP_PROGRAM_CONSTANTS_CORRUPT);
		return -1;
	}

	if (!calIsHVCalsValid())
	{
		logError(E_LOG_ERR_HV_CALIBRATION_CORRUPTED, 0);
		dispPutErrorEvent(ACTERR_CRITICAL_NV_DATA_CORRUPTED);
		setResponseCode(RESP_STIM_ASIC_HV_CALS_CORRUPT);
		return -1;
	}

	if (stimPowerOn() < 0)
	{
		DBGI(f_sm_patient_stim_mgr, 0);
		return -1;
	}

	if(event.eventData.eStimMode == STIM_TITRATION)
	{
		// ramp up the stim asic compliance voltage prior to receiving titration set commands
		stimSetComplianceDAC(63, false);
	}

	return 0;
}

static int UnlockAndUnload()
{
	wdDisable();

	swTimerCancel(SWTMR_STIM_RUN_TIME);
	swTimerCancel(SWTMR_STIM_RAMPING);

	stimStop();
	stimPowerOff();

	g_ramp = 0;
	g_nsteps = 0;
	g_rampTime = 0;

	return 0;
}

static bool startIncrementLockoutTimer(void)
{
	uint16_t lockoutTimer;
	EVENT event;

	lockoutTimer = constLockoutTime();

	if (MINIMUM_INCREMENT_LOCKOUT > lockoutTimer)
	{
		return (false);
	}
	else
	{
		// Repurpose the SWTMR_STIM_RAMPING as the Increment Lockout timer since they won't be active at the
		// same time (they are active in different states
		event.eventID = E_STIM_LOCKOUT_TIMER;
		swTimerSet(SWTMR_STIM_RAMPING, MS(lockoutTimer), event, false ) ;

		return (true);
	}
}

static void startCumulativeRunTimeTimer(void)
{
	EVENT event;

	event.eventID = E_PRG_RUN_TIME_TIMER;
	swTimerSet(SWTMR_STIM_RUN_TIME, SWTMR_15_MINUTE_TIMEOUT, event, true);
}

static bool initializeRamping(void)
{
	EVENT event;
	uint32_t timeout;
	uint16_t lRampTime;

	constGetRampTime(&lRampTime);
	g_rampTime = lRampTime;
	if(lRampTime < 1000)
	{
		// set timer in one shot mode to deliver a single event to the state machine
		lRampTime = 0;
		timeout = 10; // 10 ticks
		event.eventID = E_RAMP_TIMER;
		swTimerSet( SWTMR_STIM_RAMPING, timeout, event, false ) ;

		g_nsteps 	= 1;
	}
	else
	{
		// configure to ramp up the amplitude
		uint32_t 	n;
		uint32_t	freq, period, interval;

		freq 	= constLookUpFrequency(gprgmGetFreqIdx());
		period = 1000000uL / freq;		// period is in microseconds
		n = (uint32_t)lRampTime * 1000;  	// ramp time is in milliseconds
		n /= period;

		g_nsteps = RAMP_STEPS;
		if(n <= RAMP_STEPS)
		{
			g_nsteps = n;
		}
		interval = (uint32_t)lRampTime/(uint32_t)g_nsteps; // interval in milliseconds

		timeout = interval * (uint32_t)TICKS_HZ;
		timeout /= 1000;
		event.eventID = E_RAMP_TIMER;
		swTimerSet( SWTMR_STIM_RAMPING, timeout, event, true ) ;
	}

	g_ramp = 0;	// initialize the value placed into the rectangle register of stim asic

	if(lRampTime == 0 )
	{
		return true;
	}
	return false;
}

static int updateTitrationProgram(EVENT event)
{
	int i;
	int status = 0;

	gprgmClear();

	gprgmSetFreqIdx(event.eventData.titrParams->frequencyIdx);
	gprgmSetNumPulses(event.eventData.titrParams->numPulses);
	gprgmSetRampLevel(RAMP_MAX);												//no ramp for titration

	for (i=0; i<event.eventData.titrParams->numPulses ; i++)
	{
		gprgmSetAmplitude(i,event.eventData.titrParams->pulse[i].amplitude);
		gprgmSetPulseWidth(i,event.eventData.titrParams->pulse[i].pulseWidth);
		gprgmSetAmpPercent(i,event.eventData.titrParams->pulse[i].channelPercentage);
	}

	gprgmSetWaveformReferenceCurrent(gprgm_FindReferenceCurrentIndex());

	// always use the max stim asic compliance voltage
	gprgmSetComplianceDACValue(63);

	if ( (status = gprgmCheckDensities()) == 0 )
	{
		if ( (status = gprgmAutoWaveformAdjustment()) == 0 )
		{
			if ( (status = gprgmRun(true)) != 0 )
			{
				DBG(f_sm_titr_stim_mgr, DBG_SEV_ERROR, 0, 0);
			}
		}
		else
		{
			DBG(f_sm_titr_stim_mgr, DBG_SEV_ERROR, 0, 0);
		}
	}
	else
	{
		DBG(f_sm_titr_stim_mgr, DBG_SEV_ERROR, 0, 0);
	}

	return (status);
}

static bool testStimRangeCheckSet(SET_TEST_STIM_PARAMS const *p)
{
	int i = 0;

	if (p->frequencyIdx > MAX_FREQUENCY_SELECTIONS-1)
	{
		return false;
	}

	if (p->complianceVoltage > COMPLIANCE_MAX)
	{
		return false;
	}

	if (p->numPulses > NUM_PULSES)
	{
		return false;
	}

	for (i = 0; i < p->numPulses; i++)
	{
		if (p->pulse[i].stimWidth < PULSE_WIDTH_MIN || p->pulse[i].stimWidth > PULSE_WIDTH_MAX)
		{
			return false;
		}

		if (p->pulse[i].amplitude < TP_MIN_PULSE_AMPLITUDE || p->pulse[i].amplitude > TP_MAX_PULSE_AMPLITUDE)
		{
			return false;
		}

		if (p->pulse[i].interphaseDelay < TP_MIN_INTERPHASE_DELAY || p->pulse[i].interphaseDelay > TP_MAX_INTERPHASE_DELAY)
		{
			return false;
		}

		if (p->pulse[i].recoveryRatio > TP_MAX_RECOVERY_RATIO)
		{
			return false;
		}

		if (p->pulse[i].recoveryRatio == 0)
		{
			if (p->pulse[i].passiveRecoveryWidth < TP_MIN_PASSIVE_RECOVERY_WIDTH || p->pulse[i].passiveRecoveryWidth > TP_MAX_PASSIVE_RECOVERY_WIDTH)
			{
				return false;
			}
		}

		if (p->pulse[i].cbcWidth != 0)
		{
			if (p->pulse[i].cbcWidth < TP_MIN_CBC_PULSE_WIDTH || p->pulse[i].cbcWidth > TP_MAX_CBC_PULSE_WIDTH)
			{
				return false;
			}
		}

	}

	return true;
}

static int testStimCmdSet(SET_TEST_STIM_PARAMS const *p)
{
#if 1
	int n;
	int status = 0;

	if (false == testStimRangeCheckSet(p))
	{
		logError(E_LOG_ERR_TEST_STIM_RANGE_CHECK_FAILED, 0);
		DBG(f_sm_test_stim_mgr, 0,0,0);
		setResponseCode(CMND_RESP_INVALID_PARAM);
		return -1;
	}

	// Set up to use the last response code for error handling
	clearResponseCode();

	// Clear the gprgm
	gprgmClear();

	// Configure the program parameters
	gprgmSetFreqIdx(p->frequencyIdx);
	gprgmSetComplianceDACValue(p->complianceVoltage);
	gprgmSetNumPulses(p->numPulses);
	gprgmSetRampLevel(RAMP_MAX);

	// Configure the pulse parameters
	for (n = 0; n < p->numPulses; n++)
	{
		bool uss[NUM_CHANNELS];
		int8_t ampPercent[NUM_CHANNELS];
		int i;

		// Translate from having the uncontrolled source/sink flag as a reserved
		// "percentage" value to having percentage and USS ("voltage mode") in
		// separate arrays.
		for (i = 0; i < NUM_CHANNELS; i++)
		{
			if (p->pulse[n].channelPercentage[i] == UNCONTROLLED_SRC_SNK_CHAN)
			{
				uss[i] = true;
				ampPercent[i] = 0;
			}
			else
			{
				uss[i] = false;
				ampPercent[i] = p->pulse[n].channelPercentage[i];
			}
		}

		// Set up the GPRGM pulse parameters
		gprgmSetAmplitude(n, p->pulse[n].amplitude);
		gprgmSetPulseWidth(n, p->pulse[n].stimWidth);
		gprgmSetAmpPercent(n, ampPercent);
		gprgmSetUSSChannels(n, uss);
		gprgmSetInterphaseDelay(n, p->pulse[n].interphaseDelay);
		gprgmSetRecRatio(n, p->pulse[n].recoveryRatio);

		if(p->pulse[n].recoveryRatio == 0)
		{
			// set the passive recovery width only if we are doing a passive recovery
			gprgmSetPassiveWidth(n, p->pulse[n].passiveRecoveryWidth);
		}
		else
		{
			gprgmSetPassiveWidth(n, 0);
		}

		gprgmSetCBCWidth(n, p->pulse[n].cbcWidth);

		if ((p->pulse[n].flags & USRC_STIM) != 0)
		{
			gprgmSetStimUSource(n, true);
		}
		if ((p->pulse[n].flags & USRC_REC) != 0)
		{
			gprgmSetRecUSource(n, true);
		}
		if ((p->pulse[n].flags & USRC_CBC) != 0)
		{
			gprgmSetCBCUSource(n, true);
		}

		gprgmSetWaveformReferenceCurrent(gprgm_FindReferenceCurrentIndex());

		switch (p->pulse[n].flags & WAVE_STIM_MASK)
		{
		case WAVE_STIM_RAM0:
			gprgmSetStimWaveform(n, WAVEFORM_RAM0);
			break;
		case WAVE_STIM_RAM1:
			gprgmSetStimWaveform(n, WAVEFORM_RAM1);
			break;
		case WAVE_STIM_RECT:
		default:
			gprgmSetStimWaveform(n, WAVEFORM_RECT);
			break;
		}

		switch (p->pulse[n].flags & WAVE_RECOVERY_MASK)
		{
		case WAVE_RECOVERY_RAM0:
			gprgmSetRecWaveform(n, WAVEFORM_RAM0);
			break;
		case WAVE_RECOVERY_RAM1:
			gprgmSetRecWaveform(n, WAVEFORM_RAM1);
			break;
		case WAVE_RECOVERY_RECT:
		default:
			gprgmSetRecWaveform(n, WAVEFORM_RECT);
			break;
		}
	}

	if (p->numPulses == 0)
	{
		(void) stimStop();
	}
	else if ((status = gprgmCheckDensities()) < 0)
	{
		DBG(f_sm_test_stim_mgr, DBG_SEV_ERROR, 0, 0);
	}
	else
	{
		if ( (status = gprgmRun(true)) != 0 )
		{
			DBG(f_sm_test_stim_mgr, DBG_SEV_ERROR, 0, 0);
		}
	}

	return status;
#endif
}

//------------------------------------------------------------------------------------------------
// TRANSITION FUNCIONS

static void stimSmInit(EVENT event)
{
	(void) event;

	setResponseCode(GEN_SUCCESS);
	stimSmState = STIMSM_IDLE;

	// no MICS response issued
}

// restart stim due to a recovery event
static void stimSmRecover(EVENT event)
{
	RESPONSE repStatus;
	EVENT     tevent;
	bool	rampDisable;

	DBG(f_sm_patient_stim_mgr, DBG_SEV_NORM,0,0);
	switch (stimSmState)
	{
		case STIMSM_IDLE :
			DBG(f_sm_patient_stim_mgr, DBG_SEV_NORM,0,0);

			repStatus = pgm_SelectProgram(event.eventData.selectedProgram);

			if(repStatus == GEN_SUCCESS)
			{
				tevent.eventData.eStimMode = STIM_ACTIVE;
				if(LockAndLoad(tevent) == 0)
				{
					rampDisable = initializeRamping();
					if (startPatientStimulation(rampDisable) == 0)
					{
						DBG(f_sm_patient_stim_mgr, DBG_SEV_NORM,0,0);

						startCumulativeRunTimeTimer();
						stimSmState = STIMSM_RAMPING;
						dispPutSimpleEvent(E_STIM_STARTED);
					}
					else
					{
						DBG(f_sm_patient_stim_mgr, DBG_SEV_ERROR,0,0);
						repStatus = STIMSM_RESP_ERROR;
						UnlockAndUnload();
						stimSmState = STIMSM_IDLE;
					}
				}
			}
			break;
		default:
			repStatus = CMND_RESP_BUSY;
			break;
	}
}

static void stimSmSelect(EVENT event)
{
	RESPONSE repStatus;

	DBG(f_sm_patient_stim_mgr, DBG_SEV_NORM,0,0);
	switch (stimSmState)
	{
		case STIMSM_IDLE :
			DBG(f_sm_patient_stim_mgr, DBG_SEV_NORM,0,0);
			repStatus = pgm_SelectProgram(event.eventData.selectedProgram);
			break;
		default:
			repStatus = CMND_RESP_BUSY;
			break;
	}
	sendCommandResponseBasic(repStatus);
}

static void stimSmStart(EVENT event)
{
	RESPONSE repStatus = GEN_SUCCESS;
	uint8_t	 stimStopped = 0;
	uint8_t	 stimStarted = 0;
	bool		 rampDisable;

	if(event.eventData.eStimMode == STIM_INACTIVE)
	{
		// always allow the stim subsystem to be stopped and powered off if requested
		UnlockAndUnload();
		stimStopped = 1;
		stimSmState = STIMSM_IDLE;
	}
	else
	{
		if(LockAndLoad(event) != 0)
		{
			DBG(f_sm_patient_stim_mgr, DBG_SEV_ERROR,0,0);
			repStatus = STIMSM_RESP_ERROR;
			UnlockAndUnload();
			stimSmState = STIMSM_IDLE;
		}
		else
		{
			switch(event.eventData.eStimMode)
			{
			case STIM_ACTIVE :
				if (performBasicOutputCheck() == 0 )
				{
					if(performPulseGuardCheck() == 0)
					{
						if(performImpedanceCheck() == 0)
						{
							rampDisable = initializeRamping();
							if (startPatientStimulation(rampDisable) == 0)
							{
								DBG(f_sm_patient_stim_mgr, DBG_SEV_NORM,0,0);

								startCumulativeRunTimeTimer();
								stimStarted = 1;

								stimSmState = STIMSM_RAMPING;
							}
							else
							{
								DBG(f_sm_patient_stim_mgr, DBG_SEV_ERROR,0,0);
								repStatus = STIMSM_RESP_ERROR;
								UnlockAndUnload();
								stimSmState = STIMSM_IDLE;
							}
						}
						else
						{
							DBG(f_sm_patient_stim_mgr, DBG_SEV_ERROR,0,0);
							repStatus = STIMSM_RESP_ERROR;
							UnlockAndUnload();
							stimSmState = STIMSM_IDLE;
						}
					}
					else
					{
						// pulse guard failure
						DBG(f_sm_patient_stim_mgr, DBG_SEV_ERROR,0,0);
						repStatus = STIMSM_RESP_ERROR;
						UnlockAndUnload();
						stimSmState = STIMSM_IDLE;

						// reset the board on a pulse guard failure.
						resetSystem();
					}
				}
				else
				{
					DBG(f_sm_patient_stim_mgr, DBG_SEV_ERROR,0,0);
					repStatus = STIMSM_RESP_ERROR;
					UnlockAndUnload();
					stimSmState = STIMSM_IDLE;
				}
				break;
			case STIM_TITRATION :
				DBG(f_sm_patient_stim_mgr, DBG_SEV_NORM,0,0);
				stimStarted = 1;
				stimSmState = STIMSM_TITR;
				break;
			case STIM_TEST :
				DBG(f_sm_patient_stim_mgr, DBG_SEV_NORM,0,0);
				stimStarted = 1;
				stimSmState = STIMSM_TEST;
				break;
			case STIM_IMPEDANCE :
				DBG(f_sm_patient_stim_mgr, DBG_SEV_NORM,0,0);
				stimStarted = 1;
				stimSmState = STIMSM_IMPEDANCE;
				break;
			default:
				DBG(f_sm_patient_stim_mgr, DBG_SEV_ERROR,0,0);
				UnlockAndUnload();
				repStatus = RESP_STIM_FAILED_UKNKNOWN_SW_ERROR;
				break;
			}
		}
	}

	sendCommandResponseBasic(repStatus);

	if(stimStopped == 1)
	{
		dispPutSimpleEvent(E_STIM_STOPPED);
	}

	if(stimStarted == 1)
	{
		dispPutSimpleEvent(E_STIM_STARTED);
	}
}

static void stimSmStop(EVENT event)
{
	if(event.eventData.eStimMode == STIM_INACTIVE)
	{
		DBG(f_sm_patient_stim_mgr, DBG_SEV_NORM,0,0);
		UnlockAndUnload();
		sendCommandResponseBasic(GEN_SUCCESS);
		dispPutSimpleEvent(E_STIM_STOPPED);
		stimSmState = STIMSM_IDLE;
	}
	else
	{
		sendCommandResponseBasic(CMND_RESP_BUSY);
	}
}

static void stimSmInternalAbort(EVENT event)
{
	(void)event;

	DBG(f_sm_patient_stim_mgr, DBG_SEV_NORM,0,0);
	UnlockAndUnload();
	dispPutSimpleEvent(E_STIM_STOPPED);
	stimSmState = STIMSM_IDLE;

	// no MICS response issued
}

static void stimSmStopActive(EVENT event)
{
	uint8_t  curPrgNumber;
	uint32_t time;

#ifndef DISABLE_BGI_CHECK
	bool	result;
#endif

	if(event.eventData.eStimMode == STIM_INACTIVE)
	{
		stimStop();
		wdDisable();

		DBG(f_sm_patient_stim_mgr, DBG_SEV_NORM,0,0);

		curPrgNumber = pgm_SelectedProgramNumber() - 1;
		time = swTimerGetRemainingTime(SWTMR_STIM_RUN_TIME);

		//Counter runs for 15 minutes, so total run time is 15mins*60sec - remaining time.
		counterAddTo( (COUNTER)(COUNTER_RUN_TIME_SECS_PRG_1 + curPrgNumber), (15*60 - (time / SWTMR_01_SECOND_TIMEOUT)));
		swTimerCancel(SWTMR_STIM_RUN_TIME);

#ifndef EPG
#ifndef DISABLE_BGI_CHECK
		gprgmImpedanceAndOutputCapCheck(&result);
#endif
#endif

		UnlockAndUnload();
		sendCommandResponseBasic(GEN_SUCCESS);
		dispPutSimpleEvent(E_STIM_STOPPED);
		stimSmState = STIMSM_IDLE;
	}
	else
	{
		sendCommandResponseBasic(CMND_RESP_BUSY);
	}
}

static void stimSmRampDone(EVENT event)
{
	uint32_t  	freq;				// Hz
	uint32_t 	period;
	uint32_t	measuredPeriod;
	int32_t		timeDelta, allowedDelta;
	uint32_t 	disable;

	(void) event;

	if(g_rampTime != 0)
	{
		g_ramp += (uint16_t)255/(uint16_t)g_nsteps;
		g_ramp = g_ramp > 255 ? 255 : g_ramp;

		if(gprgmUpdateAmplitude((uint8_t)g_ramp) != 0)
		{
			// stim program had an error and stopped so clean up
			DBG(f_sm_patient_stim_mgr, DBG_SEV_ERROR, 0, 0);
			UnlockAndUnload();
			setResponseCode(RESP_STIM_FAILED_SEVERE_ERROR);
			dispPutSimpleEvent(E_STIM_STOPPED);
			stimSmState = STIMSM_IDLE;
			return;
		}

		if(g_ramp < 255) return;
	}
	// done with ramping so reset the local static variable
	g_ramp = 0;
	swTimerCancel(SWTMR_STIM_RAMPING);

	freq 	= constLookUpFrequency(gprgmGetFreqIdx());
	period = 1000000uL / freq;

	disable = calGetGenCals()->tempStateCal[STIM_PERIOD_CHECK];

	if(disable != 0)
	{
		return;
	}

	if(stimVerifyPeriod(&measuredPeriod) == 0)
	{
		timeDelta = period - measuredPeriod;
		timeDelta = timeDelta < 0 ? timeDelta * (-1) : timeDelta;

		// allow the period to be off by up to 20%
		allowedDelta = period/5;

		if(timeDelta > allowedDelta)
		{
			logError(E_LOG_ERR_STIM_PERIOD_CHECK_FAILED,0);
			DBGI(f_sm_patient_stim_mgr, measuredPeriod);
			DBGI(f_sm_patient_stim_mgr, period);
			UnlockAndUnload();
			setResponseCode(RESP_STIM_FAILED_SEVERE_ERROR);
			stimSmState = STIMSM_IDLE;
		}
		else
		{
			DBGI(f_sm_patient_stim_mgr, measuredPeriod);
			setResponseCode(GEN_SUCCESS);
			stimSmState = STIMSM_ACTIVE;
		}
	}
	else
	{
		DBG(f_sm_patient_stim_mgr, DBG_SEV_ERROR, 0, 0);
		UnlockAndUnload();
		setResponseCode(RESP_STIM_FAILED_SEVERE_ERROR);
		stimSmState = STIMSM_IDLE;
	}

	// no MICS response issued
}

static void stimSmIncRunTime(EVENT event)
{
	uint8_t curPrgNumber;
	(void) event;

	// add 15 minutes (in seconds) to the cumulative run-time counter for the
	// currently running program.
	//     local curPrgNumber will be converted to [0..9]
	curPrgNumber = pgm_SelectedProgramNumber() - 1;
	counterAddTo( (COUNTER)(COUNTER_RUN_TIME_SECS_PRG_1 + curPrgNumber), 15 * 60);

	// no MICS response issued
}

static void stimSmSetAmplitude(EVENT event)
{
	RESPONSE repStatus;
	(void) event;

	switch (stimSmState)
	{
		case STIMSM_IDLE :
			repStatus = RESP_CMND_NOT_VALID_WHILE_IDLE;
			break;
		case STIMSM_RAMPING :
			repStatus = RESP_CMND_NOT_VALID_WHILE_RAMPING;
			break;
		case STIMSM_ACTIVE :
			if (event.eventData.setPulseAmplParams.displayAmplitudeStepIndex >
						actPgmGetActiveAmpStepIndex(event.eventData.setPulseAmplParams.pulseIndex))
			{
				repStatus = setStimPulseAmplitude(	event.eventData.setPulseAmplParams.pulseIndex,
													event.eventData.setPulseAmplParams.displayAmplitudeStepIndex);
				// if startIncrementLockoutTimer() returns true then the increment lockout
				// timer has been started else don't change state
				if(repStatus == GEN_SUCCESS)
				{
					if (startIncrementLockoutTimer() )
					{
						stimSmState = STIMSM_LOCKOUT;
					}
				}
			}
			else
			{
				repStatus = setStimPulseAmplitude(	event.eventData.setPulseAmplParams.pulseIndex,
													event.eventData.setPulseAmplParams.displayAmplitudeStepIndex);
			}
			break;
		case STIMSM_LOCKOUT :
			if (event.eventData.setPulseAmplParams.displayAmplitudeStepIndex >
						actPgmGetActiveAmpStepIndex(event.eventData.setPulseAmplParams.pulseIndex))
			{
				repStatus = RESP_CMND_LOCKED_OUT;
			}
			else
			{
				repStatus = setStimPulseAmplitude(	event.eventData.setPulseAmplParams.pulseIndex,
													event.eventData.setPulseAmplParams.displayAmplitudeStepIndex);
			}
			break;

		default :
			repStatus = RESP_STIM_FAILED_UKNKNOWN_SW_ERROR;
			break;
	}

	micsTxBuffer.txData.setPulseAmplResponseData.pulseAmplitudeStepIndex = (uint8_t)actPgmGetActiveAmpStepIndex(event.eventData.pulseIndex);

	sendCommandResponse(repStatus, micsRxBuffer.token, sizeof(micsTxBuffer.txData.setPulseAmplResponseData));
}

static void stimSmSetPulseWidth(EVENT event)
{
	RESPONSE repStatus;

	switch (stimSmState)
	{
		case STIMSM_IDLE :
			repStatus = RESP_CMND_NOT_VALID_WHILE_IDLE;
			break;
		case STIMSM_RAMPING :
			repStatus = RESP_CMND_NOT_VALID_WHILE_RAMPING;
			break;
		case STIMSM_ACTIVE :
			if (event.eventData.setPulseWidthParams.pulseWidth >
					actPgmGetActivePulseWidth(event.eventData.setPulseWidthParams.pulseIndex))
			{
				repStatus = setStimPulseWidth(	event.eventData.setPulseWidthParams.pulseIndex,
												event.eventData.setPulseWidthParams.pulseWidth);
				// if startIncrementLockoutTimer() returns true then the increment lockout
				// timer has been started else don't change state
				if(repStatus == GEN_SUCCESS)
				{
					if (startIncrementLockoutTimer() )
					{
						stimSmState = STIMSM_LOCKOUT;
					}
				}
			}
			else
			{
				repStatus = setStimPulseWidth(	event.eventData.setPulseWidthParams.pulseIndex,
												event.eventData.setPulseWidthParams.pulseWidth);
			}
			break;
		case STIMSM_LOCKOUT :
			if (event.eventData.setPulseWidthParams.pulseWidth >
					actPgmGetActivePulseWidth(event.eventData.setPulseWidthParams.pulseIndex))
			{
				repStatus = RESP_CMND_LOCKED_OUT;
			}
			else
			{
				repStatus = setStimPulseWidth(	event.eventData.setPulseWidthParams.pulseIndex,
												event.eventData.setPulseWidthParams.pulseWidth);
			}
			break;

		default :
			repStatus = RESP_STIM_FAILED_UKNKNOWN_SW_ERROR;
			break;
	}

	sendCommandResponseBasic(repStatus);
}

static void stimSmSetFrequency(EVENT event)
{
	RESPONSE repStatus;

	switch (stimSmState)
	{
		case STIMSM_IDLE :
			repStatus = RESP_CMND_NOT_VALID_WHILE_IDLE;
			break;
		case STIMSM_RAMPING :
			repStatus = RESP_CMND_NOT_VALID_WHILE_RAMPING;
			break;
		case STIMSM_ACTIVE :
			if (event.eventData.setProgramFreqParams.programFrequencyIndex > actPgmGetActiveFreq())
			{
				repStatus = setStimFrequency(event.eventData.setProgramFreqParams.programFrequencyIndex);
				// if startIncrementLockoutTimer() returns true then the increment lockout
				// timer has been started else don't change state
				if(repStatus == GEN_SUCCESS)
				{
					if (startIncrementLockoutTimer() )
					{
						stimSmState = STIMSM_LOCKOUT;
					}
				}
			}
			else
			{
				repStatus = setStimFrequency(event.eventData.setProgramFreqParams.programFrequencyIndex);
			}
			break;
		case STIMSM_LOCKOUT :
			// if the new freq index is greater than the current active freq index
			if (event.eventData.setProgramFreqParams.programFrequencyIndex > actPgmGetActiveFreq())
			{
				repStatus = RESP_CMND_LOCKED_OUT;
			}
			else
			{
				repStatus = setStimFrequency(event.eventData.setProgramFreqParams.programFrequencyIndex);
			}
			break;

		default :
			repStatus = RESP_STIM_FAILED_UKNKNOWN_SW_ERROR;
			break;
	}
	sendCommandResponseBasic(repStatus);
}

static void stimSmIncProgAmplitude(EVENT event)
{
	RESPONSE repStatus;
	uint8_t pulseNo;

	(void) event;

	switch (stimSmState)
	{
		case STIMSM_IDLE :
			repStatus = RESP_CMND_NOT_VALID_WHILE_IDLE;
			break;
		case STIMSM_RAMPING :
			repStatus = RESP_CMND_NOT_VALID_WHILE_RAMPING;
			break;
		case STIMSM_ACTIVE :
			repStatus = incrementProgramAmplitude();
			if (GEN_SUCCESS == repStatus)
			{
				// if startIncrementLockoutTimer() returns true then the increment lockout
				// timer has been started else don't change state
				if (startIncrementLockoutTimer() )
				{
					stimSmState = STIMSM_LOCKOUT;
				}
			}
			break;
		case STIMSM_LOCKOUT :
			repStatus = RESP_CMND_LOCKED_OUT;
			break;

		default :
			repStatus = RESP_STIM_FAILED_UKNKNOWN_SW_ERROR;
			break;
	}

	// Get the current pulse width for this pulse and put it in the response data buffer
	micsTxBuffer.txData.incrPrgmAmplResponseData.definedPulseMap = pgm_GetPulseMap();
	for(pulseNo=0; pulseNo<NUM_PULSES; pulseNo++)
	{
		micsTxBuffer.txData.incrPrgmAmplResponseData.pulseVirtualStepIndex[pulseNo] =
			(int8_t)actPgmGetActiveVirtualAmpStepIndex(pulseNo);
	}
	sendCommandResponse(repStatus, micsRxBuffer.token, sizeof(micsTxBuffer.txData.incrPrgmAmplResponseData));
}

static void stimSmDecProgAmplitude(EVENT event)
{
	RESPONSE repStatus;
	uint8_t pulseNo;

	(void) event;

	switch (stimSmState)
	{
		case STIMSM_IDLE :
			repStatus = RESP_CMND_NOT_VALID_WHILE_IDLE;
			break;
		case STIMSM_RAMPING :
			repStatus = RESP_CMND_NOT_VALID_WHILE_RAMPING;
			break;
		case STIMSM_ACTIVE :
		case STIMSM_LOCKOUT :
			repStatus = decrementProgramAmplitude();
			break;

		default :
			repStatus = RESP_STIM_FAILED_UKNKNOWN_SW_ERROR;
			break;
	}

	// Get the current pulse width for this pulse and put it in the response data buffer
	micsTxBuffer.txData.decrPrgmAmplResponseData.definedPulseMap = pgm_GetPulseMap();
	for(pulseNo=0; pulseNo<NUM_PULSES; pulseNo++)
	{
		micsTxBuffer.txData.decrPrgmAmplResponseData.pulseVirtualStepIndex[pulseNo] =
			(int8_t)actPgmGetActiveVirtualAmpStepIndex(pulseNo);
	}
	sendCommandResponse(repStatus, micsRxBuffer.token, sizeof(micsTxBuffer.txData.decrPrgmAmplResponseData));
}

static void stimSmIncPulseAmplitude(EVENT event)
{
	RESPONSE repStatus;

	switch (stimSmState)
	{
		case STIMSM_IDLE :
			repStatus = RESP_CMND_NOT_VALID_WHILE_IDLE;
			break;
		case STIMSM_RAMPING :
			repStatus = RESP_CMND_NOT_VALID_WHILE_RAMPING;
			break;
		case STIMSM_ACTIVE :
			repStatus = incrementPulseAmplitude(event.eventData.pulseIndex);
			if (GEN_SUCCESS == repStatus)
			{
				// if startIncrementLockoutTimer() returns true then the increment lockout
				// timer has been started else don't change state
				if (startIncrementLockoutTimer() )
				{
					stimSmState = STIMSM_LOCKOUT;
				}
			}
			break;
		case STIMSM_LOCKOUT :
			repStatus = RESP_CMND_LOCKED_OUT;
			break;

		default :
			repStatus = RESP_STIM_FAILED_UKNKNOWN_SW_ERROR;
			break;
	}

	// Get the current pulse amplitude step index for this pulse and put it in the response data buffer
	micsTxBuffer.txData.incrPulseAmplResponseData.pulseVirtualStepIndex
		= (int8_t)actPgmGetActiveVirtualAmpStepIndex(event.eventData.pulseIndex);
	sendCommandResponse(repStatus, micsRxBuffer.token, sizeof(micsTxBuffer.txData.incrPulseAmplResponseData));
}

static void stimSmDecPulseAmplitude(EVENT event)
{
	RESPONSE repStatus;

	switch (stimSmState)
	{
		case STIMSM_IDLE :
			repStatus = RESP_CMND_NOT_VALID_WHILE_IDLE;
			break;
		case STIMSM_RAMPING :
			repStatus = RESP_CMND_NOT_VALID_WHILE_RAMPING;
			break;
		case STIMSM_ACTIVE :
		case STIMSM_LOCKOUT :
			repStatus = decrementPulseAmplitude(event.eventData.pulseIndex);
			break;

		default :
			repStatus = RESP_STIM_FAILED_UKNKNOWN_SW_ERROR;
			break;
	}

	// Get the current pulse width for this pulse and put it in the response data buffer
	micsTxBuffer.txData.decrPulseAmplResponseData.pulseVirtualStepIndex =
			(int8_t)actPgmGetActiveVirtualAmpStepIndex(event.eventData.pulseIndex);
	sendCommandResponse(repStatus, micsRxBuffer.token, sizeof(micsTxBuffer.txData.decrPulseAmplResponseData));
}

static void stimSmIncPulseWidth(EVENT event)
{
	RESPONSE repStatus;

	switch (stimSmState)
	{
		case STIMSM_IDLE :
			repStatus = RESP_CMND_NOT_VALID_WHILE_IDLE;
			break;
		case STIMSM_RAMPING :
			repStatus = RESP_CMND_NOT_VALID_WHILE_RAMPING;
			break;
		case STIMSM_ACTIVE :
			repStatus = incrementPulseWidth(event.eventData.pulseIndex);
			if (GEN_SUCCESS == repStatus)
			{
				// if startIncrementLockoutTimer() returns true then the increment lockout
				// timer has been started else don't change state
				if (startIncrementLockoutTimer() )
				{
					stimSmState = STIMSM_LOCKOUT;
				}
			}
			break;
		case STIMSM_LOCKOUT :
			repStatus = RESP_CMND_LOCKED_OUT;
			break;

		default :
			repStatus = RESP_STIM_FAILED_UKNKNOWN_SW_ERROR;
			break;
	}

	// Get the current pulse width for this pulse and put it in the response data buffer
	micsTxBuffer.txData.incrPulseWidthResponseData.pulseWidth = actPgmGetActivePulseWidth(event.eventData.pulseIndex);
	sendCommandResponse(repStatus, micsRxBuffer.token, sizeof(micsTxBuffer.txData.incrPulseWidthResponseData));
}

static void stimSmDecPulseWidth(EVENT event)
{
	RESPONSE repStatus;

	switch (stimSmState)
	{
		case STIMSM_IDLE :
			repStatus = RESP_CMND_NOT_VALID_WHILE_IDLE;
			break;
		case STIMSM_RAMPING :
			repStatus = RESP_CMND_NOT_VALID_WHILE_RAMPING;
			break;
		case STIMSM_ACTIVE :
		case STIMSM_LOCKOUT :
			repStatus = decrementPulseWidth(event.eventData.pulseIndex);
			break;

		default :
			repStatus = RESP_STIM_FAILED_UKNKNOWN_SW_ERROR;
			break;
	}

	// Get the current pulse width for this pulse and put it in the response data buffer
	micsTxBuffer.txData.decrPulseWidthResponseData.pulseWidth = actPgmGetActivePulseWidth(event.eventData.pulseIndex);
	sendCommandResponse(repStatus, micsRxBuffer.token, sizeof(micsTxBuffer.txData.decrPulseWidthResponseData));
}

static void stimSmIncFrequency(EVENT event)
{
	RESPONSE repStatus;

	(void) event;	// Event carries no data, so parameter unused.

	switch (stimSmState)
	{
		case STIMSM_IDLE :
			repStatus = RESP_CMND_NOT_VALID_WHILE_IDLE;
			break;
		case STIMSM_RAMPING :
			repStatus = RESP_CMND_NOT_VALID_WHILE_RAMPING;
			break;
		case STIMSM_ACTIVE :
			repStatus = incrementFrequency();
			if (GEN_SUCCESS == repStatus)
			{
				// if startIncrementLockoutTimer() returns true then the increment lockout
				// timer has been started else don't change state
				if (startIncrementLockoutTimer() )
				{
					stimSmState = STIMSM_LOCKOUT;
				}
			}
			break;
		case STIMSM_LOCKOUT :
			repStatus = RESP_CMND_LOCKED_OUT;
			break;

		default :
			repStatus = RESP_STIM_FAILED_UKNKNOWN_SW_ERROR;
			break;
	}

	// Get the current frequency index and put it in the response data buffer
	micsTxBuffer.txData.incrPrgmFreqResponseData.programFrequencyIndex = actPgmGetActiveFreq();
	sendCommandResponse(repStatus, micsRxBuffer.token, sizeof(micsTxBuffer.txData.incrPrgmFreqResponseData));
}

static void stimSmDecFrequency(EVENT event)
{
	RESPONSE repStatus;

	(void) event;

	switch (stimSmState)
	{
		case STIMSM_IDLE :
			repStatus = RESP_CMND_NOT_VALID_WHILE_IDLE;
			break;
		case STIMSM_RAMPING :
			repStatus = RESP_CMND_NOT_VALID_WHILE_RAMPING;
			break;
		case STIMSM_ACTIVE :
		case STIMSM_LOCKOUT :
			repStatus = decrementFrequency();
			break;

		default :
			repStatus = RESP_STIM_FAILED_UKNKNOWN_SW_ERROR;
			break;
	}

	// Get the current frequency index and put it in the response data buffer
	micsTxBuffer.txData.decrPrgmFreqResponseData.programFrequencyIndex = actPgmGetActiveFreq();
	sendCommandResponse(repStatus, micsRxBuffer.token, sizeof(micsTxBuffer.txData.decrPrgmFreqResponseData));
}

static void stimSmLockoutDone(EVENT event)
{
	(void) event;

	switch (stimSmState)
	{
		case STIMSM_LOCKOUT :
			swTimerCancel(SWTMR_STIM_RAMPING);
			stimSmState = STIMSM_ACTIVE;
			break;

		default:
			break;
	};

	// no MICS response issued
}

static void stimSmSetTitr(EVENT event)
{
	RESPONSE 	repStatus;

	#ifdef CMND_PERF_MON
		uint32_t	startTime, endTime, delta;
	#endif

	(void) event;

	switch (stimSmState)
	{
		case STIMSM_TITR :
			#ifdef CMND_PERF_MON
				startTime = timerTicks();
			#endif

			if (updateTitrationProgram(event) == 0)
			{
				DBG(f_sm_titr_stim_mgr,DBG_SEV_NORM,0,0);
				repStatus = GEN_SUCCESS;
			}
			else
			{
				DBG(f_sm_titr_stim_mgr,DBG_SEV_ERROR,0,0);
				repStatus = STIMSM_RESP_ERROR;
				UnlockAndUnload();
				stimSmState = STIMSM_IDLE;
			}

			#ifdef CMND_PERF_MON
				endTime = timerTicks();
				if((delta = endTime - startTime) > 0)
				{
					DBGI(f_sm_titr_stim_mgr, delta);
					DBGI(f_sm_titr_stim_mgr, TICKS_HZ);
				}
			#endif
			break;

		default:
			repStatus = RESP_FEATURE_NOT_ENABLED;
			DBG(f_sm_titr_stim_mgr,DBG_SEV_ERROR,0,0);
			break;
	}

	sendCommandResponseBasic(repStatus);
}

static void stimSmSetTest(EVENT event)
{
	RESPONSE 	repStatus;

	(void) event;

	switch (stimSmState)
	{
		case STIMSM_TEST :
			if (testStimCmdSet(event.eventData.testStim) == 0)
			{
				DBG(f_sm_test_stim_mgr,DBG_SEV_NORM,0,0);
				repStatus = GEN_SUCCESS;
			}
			else
			{
				DBGI(f_sm_test_stim_mgr,repStatus);
				repStatus = STIMSM_RESP_ERROR;
				UnlockAndUnload();
				stimSmState = STIMSM_IDLE;
			}
			break;

		default:
			repStatus = RESP_FEATURE_NOT_ENABLED;
			DBG(f_sm_test_stim_mgr,DBG_SEV_ERROR,0,0);
			break;
	}

	sendCommandResponseBasic(repStatus);
}

static void stimSmSetTestWF(EVENT event)
{
	(void) event;
}

static void stimSmImpedance(EVENT event)
{
	IMPEDANCE_PARAMS const *p;
	uint32_t	impedance;
	uint16_t	amplitude;

	switch (stimSmState)
	{
		case STIMSM_IMPEDANCE :

			DBG(f_sm_impedance_mgr,DBG_SEV_NORM,0,0);

			p = event.eventData.impedanceParams;
			if(p->pulseAmplitude == 1)
			{
				amplitude = 200;
			}
			else
			{
				amplitude = 500;
			}
			if(gprgmMeasureImpedance(amplitude, p->channelA, p->channelB, true, false, &impedance, NULL) == 0)
			{
				micsTxBuffer.txData.impedanceResponseData.channelA 		= p->channelA;
				micsTxBuffer.txData.impedanceResponseData.channelB 		= p->channelB;
				micsTxBuffer.txData.impedanceResponseData.impedance 	= impedance;

				sendCommandResponse(GEN_SUCCESS, micsRxBuffer.token, sizeof(micsTxBuffer.txData.impedanceResponseData));
			}
			else
			{
				micsTxBuffer.txData.impedanceResponseData.channelA 	= 0;
				micsTxBuffer.txData.impedanceResponseData.channelB 	= 0;

				sendCommandResponse(RESP_BGND_IMPEDANCE_CHECK_FAILED, micsRxBuffer.token, sizeof(micsTxBuffer.txData.impedanceResponseData));
			}
			break;

		default:
			DBG(f_sm_impedance_mgr,DBG_SEV_NORM,0,0);

			micsTxBuffer.txData.impedanceResponseData.channelA 	= 0;
			micsTxBuffer.txData.impedanceResponseData.channelB 	= 0;
			sendCommandResponse(RESP_FEATURE_NOT_ENABLED, micsRxBuffer.token, sizeof(micsTxBuffer.txData.impedanceResponseData));
			break;
	}
}

static void stimSmToggleStim(EVENT event)
{
	switch (stimSmState)
	{
		case STIMSM_IDLE :
			event.eventData.eStimMode = STIM_ACTIVE;
			stimSmStart(event);
			break;
		default:
			event.eventData.eStimMode = STIM_INACTIVE;
			stimSmStop(event);
			break;
	}

	// no MICS response issued
}

static void stimSmMICSClosed(EVENT event)
{
	(void) event;

	switch (stimSmState)
	{
		case STIMSM_TEST :
		case STIMSM_TITR:
		case STIMSM_IMPEDANCE:
			UnlockAndUnload();
			dispPutSimpleEvent(E_STIM_STOPPED);
			stimSmState = STIMSM_IDLE;
			break;
		default:
			break;
	}

	// no MICS response issued
}

//------------------------------------------------------------------------------------------------
// EVENT LOOP

void stimSmProcessEvent(EVENT eventToProcess)
{

	int 		i;
	EVENT_ID 	eventId = eventToProcess.eventID;

	clearResponseCode();

	for(i=0; i < STIM_EVENT_TABLE_LEN; i++)
	{
		if((stimStateTable[i].state == stimSmState || stimStateTable[i].state == STIMSM_ANY)  && stimStateTable[i].eventId == eventId)
		{
			break;
		}
	}
	if(i != STIM_EVENT_TABLE_LEN)
	{
		stimStateTable[i].eventHandler(eventToProcess);
	}
}

//------------------------------------------------------------------------------------------------
// SYNCHRONOUS API's

int isStimRamping(void)
{
	if ( stimSmState == STIMSM_RAMPING)
	{
		return (1);
	}
	else
	{
		return (0);
	}
}


int isStimLockedout(void)
{
	if ( stimSmState == STIMSM_LOCKOUT)
	{
		return (1);
	}
	else
	{
		return (0);
	}
}

RESPONSE titrStimCmdGet(GET_TITR_STIM_RESPONSE_DATA *r)
{
	int n = 0;

	if (stimSmState != STIMSM_TITR)
	{
		return RESP_FEATURE_NOT_ENABLED;
	}

	// Clear the response buffer
	memset(r, 0, sizeof(*r));

	// Copy the program settings from GPRGM into the reply packet
	r->frequencyIdx = gprgmGetFreqIdx();
	r->numPulses = gprgmGetNumPulses();

	// Copy the pulse settings from GPRGM into the reply packet
	for (n = 0; n < r->numPulses; n++)
	{
		bool const *uss = gprgmGetUSSChannels(n);
		int8_t const *ampPercent = gprgmGetAmpPercent(n);
		int i;

		r->pulse[n].amplitude = gprgmGetAmplitude(n);
		r->pulse[n].pulseWidth = gprgmGetPulseWidth(n);
		for (i = 0; i < NUM_CHANNELS; i++)
		{
			r->pulse[n].channelPercentage[i] = uss[i] ? UNCONTROLLED_SRC_SNK_CHAN : ampPercent[i];
		}
	}

	return GEN_SUCCESS;
}

RESPONSE testStimCmdGet(GET_TEST_STIM_RESPONSE_DATA *r)
{
	int n;

	if (stimSmState != STIMSM_TEST)
	{
		return RESP_FEATURE_NOT_ENABLED;
	}

	memset(r, 0, sizeof(*r));

	r->frequencyIdx = gprgmGetFreqIdx();
	r->complianceVoltage = gprgmGetComplianceDACValue();
	r->numPulses = gprgmGetNumPulses();
	r->reserved = 0;

	for (n = 0; n < r->numPulses; n++)
	{
		bool const *uss = gprgmGetUSSChannels(n);
		int8_t const *ampPercent = gprgmGetAmpPercent(n);
		int i;

		r->pulse[n].amplitude = gprgmGetAmplitude(n);
		r->pulse[n].stimWidth = gprgmGetPulseWidth(n);
		for (i = 0; i < NUM_CHANNELS; i++)
		{
			r->pulse[n].channelPercentage[i] = uss[i] ? UNCONTROLLED_SRC_SNK_CHAN : ampPercent[i];
		}
		r->pulse[n].recoveryRatio = gprgmGetRecRatio(n);
		r->pulse[n].interphaseDelay = gprgmGetInterphaseDelay(n);
		r->pulse[n].passiveRecoveryWidth = gprgmGetPassiveWidth(n);
		r->pulse[n].cbcWidth = gprgmGetCBCWidth(n);
		if (gprgmGetStimUSource(n))
		{
			r->pulse[n].flags |= USRC_STIM;
		}
		if (gprgmGetRecUSource(n))
		{
			r->pulse[n].flags |= USRC_REC;
		}
		if (gprgmGetCBCUSource(n))
		{
			r->pulse[n].flags |= USRC_CBC;
		}

		switch (gprgmGetStimWaveform(n))
		{
		case WAVEFORM_RAM0:
			r->pulse[n].flags |= WAVE_STIM_RAM0;
			break;
		case WAVEFORM_RAM1:
			r->pulse[n].flags |= WAVE_STIM_RAM1;
			break;
		case WAVEFORM_RECT:
		default:
			r->pulse[n].flags |= WAVE_STIM_RECT;
			break;
		}

		switch (gprgmGetRecWaveform(n))
		{
		case WAVEFORM_RAM0:
			r->pulse[n].flags |= WAVE_RECOVERY_RAM0;
			break;
		case WAVEFORM_RAM1:
			r->pulse[n].flags |= WAVE_RECOVERY_RAM1;
			break;
		case WAVEFORM_RECT:
		default:
			r->pulse[n].flags |= WAVE_RECOVERY_RECT;
			break;
		}
	}

	return GEN_SUCCESS;
}

RESPONSE testStimCmdCalibrateChannel(CAL_CHAN_PARAMS const *p, CAL_CHAN_RESPONSE_DATA *r)
{
	setResponseCode(GEN_SUCCESS);

	// make sure a test program is not executing
	stimStop();

	if (stimSmState != STIMSM_TEST)
	{
		return RESP_FEATURE_NOT_ENABLED;
	}

	if (p->chan < 1 || p->chan > NUM_CHANNELS)
	{
		return CMND_RESP_INVALID_PARAM;
	}
	if (p->amplitude > TP_MAX_CHANNEL_AMPLITUDE)
	{
		return CMND_RESP_INVALID_PARAM;
	}

	gprgmCalibrateChannel(p,r);

	return getLastResponseCode();
}

bool isStimOn(void)
{
	if (stimSmState != STIMSM_IDLE)
	{
		return (true);
	}
	else
	{
		return (false);
	}
}

enum STIM_STATE stimState(void)
{
	enum STIM_STATE state;

	switch (stimSmState)
	{
		case STIMSM_IDLE :
			state = STIM_INACTIVE;
			break;
		case STIMSM_ACTIVE:
		case STIMSM_RAMPING:
		case STIMSM_LOCKOUT:
			state = STIM_ACTIVE;
			break;
		case STIMSM_TITR:
			state = STIM_TITRATION;
			break;
		case STIMSM_TEST:
			state = STIM_TEST;
			break;
		case STIMSM_IMPEDANCE:
			state = STIM_IMPEDANCE;
			break;
		default:
			state = STIM_INACTIVE;
			break;
	}
	return(state);
}

