/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: This module contains some common code that most of
 *                     the Stimulation state machines use.
 *
 ***************************************************************************/

#include <string.h>

#include "StimulationCommon.h"
#include "EventQueue.h"
#include "CommandProcessor.h"
#include "DeviceManagementLayer/CommandProcessor/MICSCommandTable.h"
#include "Common/protocol/cmndBackgroundImpedance.h"
#include "bgImp.h"

#include "BatteryMonitor.h"
#include "lastresponse.h"
#include "Stim.h"
#include "GenericProgram.h"
#include "pgm.h"
#include "activePgm.h"
#include "const.h"
#include "system.h"

#include "log.h"
#include "log_events.h"

#include "DeviceDriverLayer/Stim/saturn_bugs.h"	// Needed for Saturn 904 bug workaround. Remove this when Saturn 904 support is removed.   (see bug 1311)


// static data structures to store the most recent Output Capacitor Check and Background Impedance Measurement data in
static uint16_t occVoltages[NUM_CHANNELS];
static uint8_t  occDataValid = 0;

uint16_t bimpImpedance[NUM_CHANNELS] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
uint8_t  bimpDataValid = 0;

#define DIAG_OUTPUT_LEVEL 2000						//(see bug 1312) - Level and thresholds for Diagnostic Output Check need to be reviewed
#define DIAG_OUTPUT_LEVEL_THRESHOLD_HIGH 2500		//                 and refined if necessary.
#define DIAG_OUTPUT_LEVEL_THRESHOLD_LOW 1500		//


void getOutputCapCheckData(bool clear, OUTPUT_CAPACITOR_CHECK_DATA *occRespData)
{
	int i;

	occRespData->dataValid = occDataValid;
	memcpy(occRespData->voltage, occVoltages, sizeof(occVoltages));

	if (clear)
	{
		occDataValid = 0;

		for(i=0; i<NUM_CHANNELS; i++)
		{
			occVoltages[i] = 0;
		}
	}
}


void getBackgroundImpedanceData(bool clear, BACKGROUND_IMPEDANCE_CHECK_DATA *bimpRespData)
{
	int i;

	bimpRespData->dataValid = bimpDataValid;
	memcpy(bimpRespData->impedance, bimpImpedance, sizeof(bimpImpedance));

	if (clear)
	{
		bimpDataValid = 0;

		for(i=0; i<NUM_CHANNELS; i++)
		{
			bimpImpedance[i] = 0;
		}
	}
}








