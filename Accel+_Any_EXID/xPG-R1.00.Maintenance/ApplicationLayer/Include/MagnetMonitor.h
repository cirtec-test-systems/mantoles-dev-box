/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: Header file for the Magnet Monitor.  Part of the System
 *	Health Monitor.
 *
 ***************************************************************************/

#ifndef MAGNETMONITOR_H_
#define MAGNETMONITOR_H_

#include "system_events.h"
#include <stdbool.h>

/// \defgroup systemHealthMonitor	System Health Monitor
/// \ingroup applicationLayer
/// @{

#define MINIMUM_BUTTON_PRESS_TIME SWTMR_01_SECOND_TIMEOUT

// Public functions for System Health Monitor
void magnetMonitorProcessEvent(EVENT eventToProcess);

/// @}

#endif /* MAGNETMONITOR_H_ */
