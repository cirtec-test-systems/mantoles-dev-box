#ifndef STIM_MANAGER_H_
#define STIM_MANAGER_H_

#include <stdbool.h>
#include <stdint.h>
#include "Common/Protocol/cmndStim.h"
#include "Common/Protocol/cmndGetTestStim.h"
#include "Common/Protocol/cmndGetTitrStim.h"
#include "Common/Protocol/cmndCalibrateChannel.h"
#include "Common/Protocol/response_code.h"
#include "system_events.h"

void 	stimSmProcessEvent(EVENT eventToProcess);
int 	isStimRamping(void);
int 	isStimLockedout(void);
bool 	isStimOn(void);
enum STIM_STATE stimState(void);

RESPONSE titrStimCmdGet(GET_TITR_STIM_RESPONSE_DATA *r);
RESPONSE testStimCmdGet(GET_TEST_STIM_RESPONSE_DATA *r);
RESPONSE testStimCmdCalibrateChannel(CAL_CHAN_PARAMS const *p, CAL_CHAN_RESPONSE_DATA *r);

#endif
