/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2011 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: This is the LED Manager header file.
 *
 ***************************************************************************/

#ifndef LEDMANAGER_H_
#define LEDMANAGER_H_

#include "system_events.h"
#include <stdbool.h>

/// \defgroup LED Manager
/// \ingroup applicationLayer
/// @{

#define LED_FLASH_PERIOD SWTMR_02_SECOND_TIMEOUT
#define LED_ON_TIME SWTMR_02_SECOND_TIMEOUT/10

// Public functions for LED Manager
void ledManagerProcessEvent(EVENT eventToProcess);

/// @}

#endif /* LEDMANAGER_H_ */
