/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: Header file for the Battery Monitor Module.
 *
 ***************************************************************************/
#ifndef BATTERYMONITOR_H_
#define BATTERYMONITOR_H_

#include <stdbool.h>
#include <stdint.h>

/// \defgroup batteryMonitor	Battery Monitor
/// \ingroup applicationLayer
/// @{

#include "system_events.h"
#include "Common/Protocol/cmndGetXpgStatus.h"

//////////////////////////////////////////////////////////////////////////////////////////////
// Battery Charge Level States
//////////////////////////////////////////////////////////////////////////////////////////////
// Note the calcAndStoreBatteryState routine relies on these charge level states being in
// order with the highest charge levels being the lowest enum values.
typedef enum BATT_CHARGE_STATE {
	 BATT_CHARGE_STATE_FULL=0,
	 BATT_CHARGE_STATE_HIGH=1,
	 BATT_CHARGE_STATE_LOW=2,
	 BATT_CHARGE_STATE_WARNING=3,
	 BATT_CHARGE_STATE_CRITICAL=4,
	 BATT_CHARGE_STATE_STIM_CUTOFF=5,
	 BATT_CHARGE_STATE_CUTOFF=6,
	 BATT_CHARGE_STATE_UNKNOWN=0xFF
} BATT_CHARGE_STATE;

// Public functions for Battery Monitor
XPG_BATT_STATE battMonGetXpgBattState(void);
uint16_t battMonGetCurrentBatteryVoltage(void);
uint16_t battMonGetLastSampledBatteryVoltage(void);
void battMonClearLastSampledBatteryVoltage(void);
bool battMonBatteryLevelOkToStim(void);

void battMonProcessEvent(EVENT eventToProcess);

/// @}

#endif /*BATTERYMONITOR_H_*/
