/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2011 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description:  System Health Monitor header file.
 *
 ***************************************************************************/

#ifndef SYSTEMHEALTHMONITOR_H_
#define SYSTEMHEALTHMONITOR_H_

#include "system_events.h"
#include <stdbool.h>
#include "MultiChannelTimer.h"

#define WATCHDOG_SERVICE_RATE MS(1000)
#define MAX_STIM_RECOVERY 10

// Public functions for System Health Monitor
void systemHealthMonitorProcessEvent(EVENT eventToProcess);
bool getStimulationPermission(void);
bool getChargingPermission(void);
uint8_t getActiveError(void);
uint8_t getChargeError(void);
void clearCategory3Error(void);
int shmGetCurrentState();

/// @}

#endif /*SYSTEMHEALTHMONITOR_H_*/
