/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: This module contains some common code that most of
 *                     the Stimulation state machines use.
 *
 ***************************************************************************/
#ifndef STIM_COMMON_H_
#define STIM_COMMON_H_

#include <stdbool.h>

#include "system_events.h"
#include "Common/Protocol/cmndDiagData.h"
#include "Common/Protocol/response_code.h"

void 	getOutputCapCheckData(bool clear, OUTPUT_CAPACITOR_CHECK_DATA *occRespData);
void 	getBackgroundImpedanceData(bool clear, BACKGROUND_IMPEDANCE_CHECK_DATA *bimpRespData);




#endif
