/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: Header file for the charge manager.
 *
 ***************************************************************************/

#ifndef CHARGEMANAGERPROTO_H_
#define CHARGEMANAGERPROTO_H_

#include "system_events.h"
#include <stdbool.h>

void chrgMgrProcessEvent(EVENT eventToProcess);
int  chrgMgrGetChrgAndTempState(uint8_t * chrgStateAndTemperature);
void chrgMgrGetLastTemperatureDataReading(uint16_t *temp, uint16_t *offset, uint16_t *input, uint16_t *bias);
void chrgMgrClearLastTemperatureDataReading(void);
bool chrgMgrIsChrgMgrOn(void);
bool chrgMgrIsCharging(void);
bool chrgMgrIsVrectPresent(void);
bool chrgMgrIsChargerEngaged(void);
#endif /* CHARGEMANAGERPROTO_H_ */
