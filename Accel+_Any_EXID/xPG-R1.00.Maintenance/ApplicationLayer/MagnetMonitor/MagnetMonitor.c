/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS xPG
 *
 *	@file Description: Magnet Monitor Module.
 *
 ***************************************************************************/

#include <stdbool.h>

#include "MagnetMonitor.h"
#include "state_machine_common.h"
#include "system_events.h"
#include "cdp.h"
#include "pgm.h"
#include "EventQueue.h"
#include "StimManager.h"
#include "MultiChannelTimer.h"
#include "swtmr_application_timers.h"
#include "log.h"
#include "log_events.h"
#include "system.h"
#include "magnet.h"
#include "trim.h"

//#define MAGMON_PRINT_OUTPUT	// to turn on/off print debug output for System Health Monitor module
#ifdef MAGMON_PRINT_OUTPUT
#include <stdio.h>
#endif

//Define the states of the state machine
typedef enum MAGMON_STATE
{
	MAGMON_STATE_INITIAL			=0,	//Before initialization
	MAGMON_MAGNET_DISENGAGED		=1,	//Magnet not currently being detected
	MAGMON_MAGNET_ENGAGED			=2,	//Magnet is current detected
	MAGMON_WAITING					=3,	//Magnet limbo state - maybe swipe, maybe towards storage mode
	MAGMON_WAITING_FOR_STORAGE		=4	//Magnet is about to cause system to go into storage mode
} MAGMON_STATE;



/*Event Handers*/
static void eventHandlerE_INITIALIZE(EVENT event);
static void eventHandlerE_MAGNET_ENGAGED(EVENT event);
static void eventHandlerE_MAGNET_DISENGAGED(EVENT event);
static void eventHandlerE_MAGNET_TIMER_ENGAGED(EVENT event);
static void eventHandlerE_MAGNET_TIMER_WAITING(EVENT event);

#define MAGMON_EVENT_TABLE_LEN 6

// Define the State Machine Table
static STATE_MACHINE_TABLE_ENTRY const magmonStateTable[MAGMON_EVENT_TABLE_LEN] =
{
//   There is one table entry for every event processed in each state
//   CURRENT STATE				EVENT ID				EVENT HANDLER				        END STATE
//   ---------------------------------------------------------------------------------------------------------------------
DISPATCH_GEN_START(magnetMonitorProcessEvent)
	{MAGMON_STATE_INITIAL,		E_INITIALIZE,			eventHandlerE_INITIALIZE			/* MAGMON_MAGNET_DISENGAGED 	*/},
	{MAGMON_MAGNET_DISENGAGED,	E_MAGNET_ENGAGED,		eventHandlerE_MAGNET_ENGAGED		/* MAGMON_MAGNET_ENGAGED		*/},
	{MAGMON_MAGNET_ENGAGED,		E_MAGNET_TMR_SWIPE,		eventHandlerE_MAGNET_TIMER_ENGAGED	/* MULTIPLE						*/},
	{MAGMON_WAITING,			E_MAGNET_TMR_STORAGE,	eventHandlerE_MAGNET_TIMER_WAITING	/* MAGMON_WAITING_FOR_STORAGE	*/},
	{MAGMON_WAITING,			E_MAGNET_DISENGAGED,	eventHandlerE_MAGNET_DISENGAGED		/* MAGMON_MAGNET_DISENGAGED		*/},
	{MAGMON_WAITING_FOR_STORAGE,E_MAGNET_DISENGAGED,	eventHandlerE_MAGNET_DISENGAGED		/* MAGMON_MAGNET_DISENGAGED		*/}
DISPATCH_GEN_END
};

//Initialize the state machine and necessary
static MAGMON_STATE magmonCurrentState = MAGMON_STATE_INITIAL;

#define MICS_TRIM_COPY_TIMEOUT (30*TICKS_HZ)
static bool canResetMicsTrims;


/**
 * Main Event Handler that processes all incoming events
 *
 * \param event - The event to process.
 */
void magnetMonitorProcessEvent(EVENT eventToProcess)
{
	int i = 0;
	EVENT_ID eventId = eventToProcess.eventID;

	#ifdef MAGMON_PRINT_OUTPUT
		switch (eventId)
		{
			case E_INITIALIZE:
				printf("Magnet Monitor ProcessEvent - E_INITIALIZE RECEIVED.\n");
				break;
			case E_MAGNET_ENGAGED:
				printf("Magnet Monitor ProcessEvent - E_MAGNET_ENGAGED RECEIVED.\n");
				break;
			case E_MAGNET_TMR_SWIPE:
				printf("Magnet Monitor ProcessEvent - E_MAGNET_TMR_SWIPE RECEIVED.\n");
				break;
			case E_MAGNET_TMR_STORAGE:
				printf("Magnet Monitor ProcessEvent - E_MAGNET_TMR_STORAGE RECEIVED.\n");
				break;
			case E_MAGNET_DISENGAGED:
				printf("Magnet Monitor ProcessEvent - E_MAGNET_DISENGAGED RECEIVED.\n");
				break;
			default:
				printf("Magnet Monitor ProcessEvent - UNKNOWN EVENT RECEIVED\n");
		}
	#endif

DISPATCH_GEN_START(magnetMonitorProcessEvent)
	if(eventId == E_SWTMR_MICS_TRIM_COPY_EXPIRED)
	{
		canResetMicsTrims = false;
	}
DISPATCH_GEN_END

	// Iterate through the state machine table until find the current state.
	for(i=0; i < MAGMON_EVENT_TABLE_LEN; i++)
	{
		if((magmonStateTable[i].state == magmonCurrentState)  && magmonStateTable[i].eventId == eventId)
		{
			break;
		}
	}

	//Now call the appropriate event handler
	if(i != MAGMON_EVENT_TABLE_LEN)
	{
		magmonStateTable[i].eventHandler(eventToProcess);
	}

	return;
}

/**
 * The event handler for the E_INITIALIZE event.
 *
 * \param event - The event to handle.
 */
static void eventHandlerE_INITIALIZE(EVENT event)
{
	EVENT timerEvent;

	#ifdef MAGMON_PRINT_OUTPUT
		printf("Magnet Monitor - eventHandlerE_INITIALIZE\n");
	#endif

	(void)event;

	if(isIPG())
	{
		canResetMicsTrims = true;
		timerEvent.eventID = E_SWTMR_MICS_TRIM_COPY_EXPIRED;
		swTimerSet(SWTMR_MICS_TRIM_COPY, MICS_TRIM_COPY_TIMEOUT, timerEvent, false);
	}
	else
	{
		canResetMicsTrims = false;
	}


	magmonCurrentState = MAGMON_MAGNET_DISENGAGED;

	return;
}

/**
 * The event handler for the E_MAGNET_ENGAGED event.
 *
 * \param event - The event to handle.
 */
static void eventHandlerE_MAGNET_ENGAGED(EVENT event)
{
	#ifdef MAGMON_PRINT_OUTPUT
		printf("Magnet Monitor - eventHandlerE_MAGNET_ENGAGED\n");
	#endif

	(void)event;

	if (isIPG())
	{
		//Wait for 500ms - basically waiting for mag switch to settle
		EVENT magnetSwipeEvent;
		magnetSwipeEvent.eventID = E_MAGNET_TMR_SWIPE;
		swTimerSet(SWTMR_MAGNET_TIMER, SWTMR_01_SECOND_TIMEOUT/2, magnetSwipeEvent, false );

		magmonCurrentState = MAGMON_MAGNET_ENGAGED;
	}
	else
	{
		//This is the case where the EPG quick stop button is pressed.
		//The EPG quick stop button is connected to the magnet switch.
		//Thus, we are using the magnet monitor for the quick stop button.
		if (isStimOn())
		{
			EVENT magnetSwipeEvent;
			magnetSwipeEvent.eventID = E_MAGNET_TMR_SWIPE;
			swTimerSet(SWTMR_MAGNET_TIMER, MINIMUM_BUTTON_PRESS_TIME, magnetSwipeEvent, false );

			//Note: We set a timer here for a minimum button press time, so as to avoid pressing
			//      a button by mistake.
			magmonCurrentState = MAGMON_MAGNET_ENGAGED;
		}
		else
		{
			//If stimulation is not on, then the quick stop button has no function
			magmonCurrentState = MAGMON_MAGNET_DISENGAGED;
		}


	}

	return;
}

/**
 * The event handler for the E_MAGNET_DISENGAGED event.
 *
 * \param event - The event to handle.
 */
static void eventHandlerE_MAGNET_DISENGAGED(EVENT event)
{
	#ifdef MAGMON_PRINT_OUTPUT
		printf("Magnet Monitor - eventHandlerE_MAGNET_DISENGAGED\n");
	#endif

	(void)event;

	if (magmonCurrentState == MAGMON_WAITING)
	{
		EVENT stimToggleEvent;
		stimToggleEvent.eventID = E_MAGNET_TOGGLE_STIM;

		//Cancel any active timers
		swTimerCancel(SWTMR_MAGNET_TIMER);


		if(pgm_SelectedProgramNumber() == 0 && canResetMicsTrims)
		{
			// If there is no program selected, this is the signal to copy the MICS backup trims
			// (factory cal) back to the primary set.

			//NOTE: the backup contains the factory values. That is what we fall back to here.
			trimSetPrimaryList(trimGetListBackup(TRIM_ZL));
		}
		else if (!isIPG() || (cdp_GetMagnetOptions() & MAGNET_OPTION_ENABLE_STOP))
		{
			if (isStimOn())
			{
				//Toggle stim
				logNormal(E_LOG_MAGNET_STIM_STOPPED,0);
				dispPutEvent(stimToggleEvent);
				dispPutErrorEvent(ACTERR_STIM_SHUT_DOWN_WITH_EXTERNAL_MAGNET);
			}
			else if (pgm_SelectedProgramNumber() != 0)
			{
				logNormal(E_LOG_MAGNET_STIM_STARTED,0);
				dispPutEvent(stimToggleEvent);
			}
		}
	}

	magmonCurrentState = MAGMON_MAGNET_DISENGAGED;

	return;
}

/**
 * The event handler for the E_MAGNET_TIMER event.
 *
 * \param event - The event to handle.
 */
static void eventHandlerE_MAGNET_TIMER_WAITING(EVENT event)
{
	#ifdef MAGMON_PRINT_OUTPUT
		printf("Magnet Monitor - eventHandlerE_MAGNET_TIMER_WAITING\n");
	#endif

	(void)event;

	if (isStimOn())
	{
		EVENT stimOffEvent;
		stimOffEvent.eventID = E_STOP_STIM;
		dispPutEvent(stimOffEvent);

		logNormal(E_LOG_STIM_STOPPED_PWR_DOWN,0);
	}

	logNormal(E_LOG_MAGNET_PWR_DOWN,0);

	magmonCurrentState = MAGMON_WAITING_FOR_STORAGE;

	return;
}


/**
 * The event handler for the E_MAGNET_TIMER event.
 *
 * \param event - The event to handle.
 */
static void eventHandlerE_MAGNET_TIMER_ENGAGED(EVENT event)
{
	#ifdef MAGMON_PRINT_OUTPUT
		printf("Magnet Monitor - eventHandlerE_MAGNET_TIMER_ENGAGED\n");
	#endif

	(void)event;

	//Check to see if still magnet swipe has occurred or magnet still engaged
	if (magnetIsMagnetEngaged())
	{
		if (isIPG())
		{
			//Wait another 4s, if this timer goes off, we are going into storage mode
			EVENT magnetStorageEvent;
			magnetStorageEvent.eventID = E_MAGNET_TMR_STORAGE;
			swTimerSet(SWTMR_MAGNET_TIMER, SWTMR_04_SECOND_TIMEOUT, magnetStorageEvent, false );

			magmonCurrentState = MAGMON_WAITING;
		}
		else
		{
			if (isStimOn())
			{
				EVENT stimOffEvent;
				stimOffEvent.eventID = E_STOP_STIM;
				dispPutEvent(stimOffEvent);

				logNormal(E_LOG_STIM_STOPPED_PWR_DOWN,0);
			}

			//Go to disengaged state for EPG and wait for next button press...handles stuck button case
			magmonCurrentState = MAGMON_MAGNET_DISENGAGED;
		}
	}
	else
	{
		if (isIPG())
		{
			EVENT stimToggleEvent;
			stimToggleEvent.eventID = E_MAGNET_TOGGLE_STIM;

			if(pgm_SelectedProgramNumber() == 0 && canResetMicsTrims)
			{
				// If there is no program selected, this is the signal to copy the MICS backup trims
				// (factory cal) back to the primary set.

				//NOTE: the backup contains the factory values. That is what we fall back to here.
				trimSetPrimaryList(trimGetListBackup(TRIM_ZL));
			}
			else if (!isIPG() || (cdp_GetMagnetOptions() & MAGNET_OPTION_ENABLE_STOP))
			{
				if (isStimOn())
				{
					logNormal(E_LOG_MAGNET_STIM_STOPPED,0);
					dispPutEvent(stimToggleEvent);
				}
				else if (pgm_SelectedProgramNumber() != 0)
				{
					logNormal(E_LOG_MAGNET_STIM_STARTED,0);
					dispPutEvent(stimToggleEvent);
				}
			}
		}

		//In both cases, EPG and IPG, go to disengaged state
		magmonCurrentState = MAGMON_MAGNET_DISENGAGED;
	}

	return;
}

