/***************************************************************************/
/*                        CONFIDENTIAL & PROPRIETARY                       */
/*                             Template Rev 2.4                            */
/***************************************************************************/

/****************************************************************************
 *
 *	Copyright (c) 2012 QIG Group
 *	Cleveland - 1771 E. 30th St. Cleveland, OH 44114
 *	Denver - 12202 Airport Way, Suite 160 Broomfield, CO 80021
 *
 *	Project: 24-channel SCS IPG
 *
 *	@file Description: power charger functions.
 *
 ***************************************************************************/
#ifndef EPG

#ifdef UNIT_TESTING
#define STATIC
#else
#define STATIC static
#endif

//#define CHRG_MGR_PRINT_OUTPUT
#ifdef CHRG_MGR_PRINT_OUTPUT
#include <stdio.h>
#endif

#define CHRG_MGR_TEMP_LOGGING

#include "ChargeManager.h"
#include "BatteryMonitor.h"
#include "SystemHealthMonitor.h"
#include "system_events.h"
#include "MultiChannelTimer.h"
#include "state_machine_common.h"
#include "log.h"
#include "cal.h"
#include "tets.h"
#include "counter.h"
#include "log_events.h"
#include "error_code.h"
#include "EventQueue.h"
#include "PowerASIC.h"
#include "system.h"
#include "Watchdog.h"

//State Machine States
typedef enum CM_STATE {
	STATE_CM_INITIAL,
	STATE_CM_NOT_CHARGING,
	STATE_CM_CHARGING_C4,
	STATE_CM_CHARGING_C8,
	STATE_CM_COMPLETING_CHARGING,
	STATE_CM_CRITICAL_TEMPERATURE,
	STATE_CM_CHARGING_NOT_ALLOWED,
	STATE_CM_ALL
} CM_STATE;

//Temperature States
typedef enum CM_TEMPERATURE_STATE {
	CM_TEMPERATURE_STATE_TOO_COLD,
	CM_TEMPERATURE_STATE_OK,
	CM_TEMPERATURE_STATE_WARNING,
	CM_TEMPERATURE_STATE_CRITICAL,
	CM_TEMPERATURE_STATE_ABORT
} CM_TEMPERATURE_STATE;

//Charging states for PPC...DEFINED IN PPC
typedef enum PPC_CHARGING_STATE {
	PPC_CHARGE_OFF 						= 0x0,
	PPC_CHARGE_PRE_CONDITION 			= 0x1,
	PPC_CHARGE_CONSTANT_CURRENT_FAST 	= 0x2,
	PPC_CHARGE_CONSTANT_CURRENT_SLOW	= 0x3,
	PPC_CHARGE_COMPLETE		 			= 0x4,
	PPC_CHARGE_ERROR  					= 0x5,
	PPC_CHARGE_CONSTANT_VOLTAGE_FAST 	= 0x6,
	PPC_CHARGE_CONSTANT_VOLTAGE_SLOW 	= 0x7
} PPC_CHARGING_STATE;


//Event Handlers
static void eventHandlerE_INITIALIZE(EVENT event);
static void eventHandlerE_CHARGER_ENGAGED(EVENT event);
static void eventHandlerE_CHARGING_POLLING_TIMEOUT_C8(EVENT event);
static void eventHandlerE_CHARGING_POLLING_TIMEOUT_C4(EVENT event);
static void eventHandlerE_CHARGING_C8_TIMEOUT(EVENT event);
static void eventHandlerE_LOG_TEMPERATURE_DATA(EVENT event);
static void eventHandlerE_CHARGING_COMPLETE(EVENT event);
static void eventHandlerE_CRITICAL_TEMPERATURE_POLLING(EVENT event);
static void eventHandlerE_PUBLISH_BATT_VOLTAGE(EVENT event);
static void eventHandlerE_CHARGING_MODE_TIMEOUT(EVENT event);

//Helper Functions
static void chrgMgrChargeC8(void);
static void chrgMgrChargeC4(void);
static void chrgMgrStopCharging(void);
static void chrgMgrStartC8Timer(void);
static void chrgMgrStartPollingTimer(void);
static void chrgMgrCheckChargingModeTimer(void);
static void chrgMgrCompleteCharging(void);
static void chrgMgrSetChargingError(CHARGE_ERROR_CODE chargingErrorCode);
static bool chrgMgrIsChrgComplete(void);
static uint16_t chrgMgrGetVrect(void);
static CM_TEMPERATURE_STATE chrgMgrGetTemperatureState(void);

//Static Variable
STATIC CM_STATE chrgMgrStateMachineState=STATE_CM_INITIAL;
STATIC CHARGE_STATE chargingMode = CHARGE_STATE_CHARGE_OFF;
static uint8_t c4retryCounter = 0;
static uint8_t c8retryCounter = 0;
static uint8_t c4retryResetCounter = 0;
static uint8_t c8retryResetCounter = 0;
static uint8_t overvoltageChargeCompleteRetryCounter = 0;
static uint8_t overvoltageRetryCounter = 0;
static GEN_CALS const *calibrationData;
static uint16_t thermBias = 0;
static uint16_t thermOffset = 0;
static uint16_t thermInput = 0;
static uint16_t resistanceTemp = 0;
static uint16_t batteryADCCounts = 0;
STATIC uint16_t batteryIncreaseRetryCounter = 0;
STATIC bool thermistorCheckPassed = true;
STATIC CHARGE_STATE chargeCompleteChargeControllerState = CHARGE_STATE_CHARGE_OFF;
STATIC bool isChargerEngaged = false;

//Define State Machine Table
static STATE_MACHINE_TABLE_ENTRY const stateTable[] =
{
// there is one table entry for every event processed in each state
//   CURRENT STATE					EVENT ID					EVENT HANDLER									END STATE
//   --------------------------------------------------------------------------------------------------------------------------------------------
DISPATCH_GEN_START_ONLY(chrgMgrProcessEvent, IPG)
	{STATE_CM_INITIAL,				E_INITIALIZE,				eventHandlerE_INITIALIZE						/*STATE_CM_NOT_CHARGING		*/	},
	{STATE_CM_NOT_CHARGING,			E_CHARGER_ENGAGED,			eventHandlerE_CHARGER_ENGAGED					/*STATE_CM_CHARGING_C4		*/	},
	{STATE_CM_CHARGING_C4,			E_CHARGING_POLLING,			eventHandlerE_CHARGING_POLLING_TIMEOUT_C4		/*MULTIPLE					*/	},
	{STATE_CM_COMPLETING_CHARGING,	E_CHARGING_COMPLETING,		eventHandlerE_CHARGING_COMPLETE,				/*NOT CHARGING				*/  },
	{STATE_CM_CHARGING_C8,			E_CHARGE_LOW_RATE_TO,		eventHandlerE_CHARGING_C8_TIMEOUT,				/*STATE_CM_CHARGING_C4_TEMP */	},
	{STATE_CM_CHARGING_C8,			E_CHARGING_POLLING,			eventHandlerE_CHARGING_POLLING_TIMEOUT_C8,		/*MULTIPLE					*/	},
	{STATE_CM_CHARGING_C4,			E_CHARGE_LOGGING_TIMEOUT,	eventHandlerE_LOG_TEMPERATURE_DATA,				/*SAME						*/	},
	{STATE_CM_CHARGING_C8,			E_CHARGE_LOGGING_TIMEOUT,	eventHandlerE_LOG_TEMPERATURE_DATA,				/*SAME						*/	},
	{STATE_CM_CHARGING_C4,			E_PUBLISH_BATT_VOLTAGE,		eventHandlerE_PUBLISH_BATT_VOLTAGE,				/*SAME						*/	},
	{STATE_CM_CHARGING_C8,			E_PUBLISH_BATT_VOLTAGE,		eventHandlerE_PUBLISH_BATT_VOLTAGE,				/*SAME						*/	},
	{STATE_CM_CHARGING_C4,			E_CHARGE_CONCUR_STATE_TO,	eventHandlerE_CHARGING_MODE_TIMEOUT,			/*SAME						*/	},
	{STATE_CM_CHARGING_C8,			E_CHARGE_PRECON_STATE_TO,	eventHandlerE_CHARGING_MODE_TIMEOUT,			/*SAME						*/	},
	{STATE_CM_CRITICAL_TEMPERATURE,	E_CHARGING_POLLING,			eventHandlerE_CRITICAL_TEMPERATURE_POLLING,		/*MULTIPLE					*/  }
	DISPATCH_GEN_END
};

#define NUM_STATE_TABLE_ENTRIES 	(sizeof(stateTable)/sizeof(stateTable[0]))
#define POLLING_TIMEOUT				SWTMR_01_SECOND_TIMEOUT
#define LOW_CHARGING_RATE_TIMEOUT	2*SWTMR_60_SECOND_TIMEOUT
#define CHARGING_COMPLETE_WAIT_TIME	SWTMR_30_SECOND_TIMEOUT
#define C4_VRECT_THRESHOLD			6000
#define C8_VRECT_THRESHOLD			5700
#define C4_MAX_RETRIES				10
#define C4_MAX_RETRY_RESET			5
#define C8_MAX_RETRIES				10
#define C8_MAX_RETRY_RESET			5
#define LOGGING_FREQUENCY			SWTMR_60_SECOND_TIMEOUT
#define BATT_INCREASE_RETRIES		12
#define OVERVOLT_CC_MAX_RETRIES		10
#define OVERVOLT_MAX_RETRIES		4
#define VRECT_PRESENCE_THRESHOLD	3000
#define BATT_VOLTAGE_FULL_ADC_CNTS  3272

/* Processes all subscribed events */
void chrgMgrProcessEvent(EVENT eventToProcess)
{
	int i;

	EVENT_ID eventId = eventToProcess.eventID;

	// lookup event handler and call it
	for (i = 0; i < NUM_STATE_TABLE_ENTRIES; i++)
	{
		if (stateTable[i].state == chrgMgrStateMachineState && stateTable[i].eventId == eventId)
		{
			// We have found an event handler for this State / Event combination
			// Call the handler for the event
			stateTable[i].eventHandler(eventToProcess);
			break;
		}
	}
}

/*
 * Event Handlers
 */

/* Initializes the state machine */
static void eventHandlerE_INITIALIZE(EVENT event)
{
	(void)event;

	//Nothing to do for now except change state
	#ifdef CHRG_MGR_PRINT_OUTPUT
		printf("CHRGMGR::NOT CHARGING.\n");
	#endif

	if(!isIPG())
	{
		chrgMgrStateMachineState = STATE_CM_CHARGING_NOT_ALLOWED;
		return;
	}

	//Check if the calibration values are valid, if not, don't allow charging
	if (calIsGenCalsValid())
	{
		calibrationData = calGetGenCals();
	}
	else
	{
		chrgMgrSetChargingError(CHERR_CHARGE_NOT_ALLOWED);
	}

	//Perform a diagnostic check, set an error if fails
	if (chargeGetTempAndDiagCheck(&resistanceTemp, &thermOffset, &thermInput, &thermBias) != OK_RETURN_VALUE)
	{
		thermistorCheckPassed = false;
		chrgMgrSetChargingError(CHERR_THERMISTOR_ERROR);
	}
	else
	{
		thermistorCheckPassed = true;
	}

	//See if there are any active errors that would prevent charging
	if (!getChargingPermission())
	{
		chrgMgrSetChargingError(CHERR_CHARGE_NOT_ALLOWED);
	}

	isChargerEngaged = false;
	chrgMgrStateMachineState = STATE_CM_NOT_CHARGING;

	return;
}

/* Called when the charger is engaged */
static void eventHandlerE_CHARGER_ENGAGED(EVENT event)
{
	CM_TEMPERATURE_STATE temperatureState;

	(void)event;

	//Indicate that we see the charger has been engaged.
#ifndef DISABLE_LOG_CHARGING_INFO
	logNormal(E_LOG_CHARGER_DETECTED,0);
#endif

	//Perform a diagnostic check, set an error if fails
	if (chargeGetTempAndDiagCheck(&resistanceTemp, &thermOffset, &thermInput, &thermBias) != OK_RETURN_VALUE)
	{
		thermistorCheckPassed = false;
		chrgMgrSetChargingError(CHERR_THERMISTOR_ERROR);
	}
	else
	{
		thermistorCheckPassed = true;
	}

	temperatureState = chrgMgrGetTemperatureState();

	//First check temperature and ensure we are not at the abort threshold
	if ((temperatureState == CM_TEMPERATURE_STATE_ABORT || temperatureState == CM_TEMPERATURE_STATE_TOO_COLD) && thermistorCheckPassed)
	{
		//Detune the charger
		tetsDetune();

		// Cycle the state machine
		#ifdef CHRG_MGR_PRINT_OUTPUT
			printf("CHRGMGR::NOT CHARGING DUE TO HIGH TEMPERATURE.\n");
		#endif

		//Set an error
		chrgMgrSetChargingError(CHERR_STOP_TEMP_LIMIT_EXCEEDED);

		//Log the error
		logNormal(E_LOG_CHARGING_ABORT_TEMP,0);

		// Set at timer at the desired rate to poll the temperature
		chrgMgrStartPollingTimer();

		//Set this to true so that 400MHz wakeup runs.
		isChargerEngaged = true;

		//Cycle the state machine
		chrgMgrStateMachineState = STATE_CM_CRITICAL_TEMPERATURE;

		return;
	}

	//See if there are any active errors that would prevent charging
	if (!getChargingPermission())
	{
		isChargerEngaged = false;
		chrgMgrStateMachineState = STATE_CM_NOT_CHARGING;

		return;
	}

	// Enable the VDET LOW IRQ / Disable the VDET HIGH IRQ
	chargeEnableChargerDisengagedInterrupt();

	// Set at timer at the desired rate to poll the charge controller
	chrgMgrStartPollingTimer();

	// Set the charger engaged variable
	isChargerEngaged = true;

	// Set a time to perform logging appropriately
	#ifdef CHRG_MGR_TEMP_LOGGING
	{
		EVENT loggingEvent;

		loggingEvent.eventID = E_CHARGE_LOGGING_TIMEOUT;
		swTimerSet(SWTMR_ONE_MINUTE_TEST, LOGGING_FREQUENCY, loggingEvent, true);
	}
	#endif

	// Zero out some counters
	c4retryCounter = 0;
	c8retryCounter = 0;
	c4retryResetCounter = 0;
	c8retryResetCounter = 0;
	batteryIncreaseRetryCounter = 0;
	overvoltageChargeCompleteRetryCounter = 0;
	overvoltageRetryCounter = 0;

	if (temperatureState == CM_TEMPERATURE_STATE_CRITICAL || !thermistorCheckPassed)
	{
		//Getting too hot to maintain charging at C4...switch to C8
		chrgMgrStartC8Timer();

		#ifdef CHRG_MGR_PRINT_OUTPUT
			printf("CHRGMGR::TEMPERATURE TOO HIGH, SWITCHING TO C8.\n");
		#endif

#ifndef DISABLE_LOG_CHARGING_INFO
		logNormal(E_LOG_CHARGING_INITIATED, CHARGE_RATE_C8);
#endif
		// Start charging at the low rate because we can't charge at the high rate
		chrgMgrChargeC8();
		chrgMgrStateMachineState = STATE_CM_CHARGING_C8;
	}
	else
	{
		#ifdef CHRG_MGR_PRINT_OUTPUT
			printf("CHRGMGR::CHARGING_C4.\n");
		#endif

#ifndef DISABLE_LOG_CHARGING_INFO
		logNormal(E_LOG_CHARGING_INITIATED, CHARGE_RATE_C4);
#endif
		// Start charging at the high rate
		chrgMgrChargeC4();
		chrgMgrStateMachineState = STATE_CM_CHARGING_C4;
	}

	//Check the charge mode and start the appropriate timer...
	chargingMode = CHARGE_STATE_CHARGE_OFF;
	chrgMgrCheckChargingModeTimer();

	//Reset the charge complete reason, only used for logging
	chargeCompleteChargeControllerState = CHARGE_STATE_CHARGE_OFF;

	//Get initial battery voltage
	batteryADCCounts = battMonGetCurrentBatteryVoltage();

	return;
}

/* Polls the charge controller while charging at high rate */
static void eventHandlerE_CHARGING_POLLING_TIMEOUT_C4(EVENT event)
{
	uint16_t vrect = chrgMgrGetVrect();
	bool chargeControllerOn = chrgMgrIsChrgMgrOn();
	bool isChargerPresent = chargeChargerPresent();
	CM_TEMPERATURE_STATE temperatureState;

	(void)event;

	//First check is charging is complete, if complete, we're done
	if (chrgMgrIsChrgComplete())
	{
		chrgMgrCompleteCharging();

		return;
	}

	//Perform a diagnostic check, set an error if fails
	if (chargeGetTempAndDiagCheck(&resistanceTemp, &thermOffset, &thermInput, &thermBias) != OK_RETURN_VALUE)
	{
		thermistorCheckPassed = false;
		chrgMgrSetChargingError(CHERR_THERMISTOR_ERROR);
	}
	else
	{
		thermistorCheckPassed = true;
	}

	temperatureState = chrgMgrGetTemperatureState();

	//Now see where we are temperature wise
	if (thermistorCheckPassed)
	{
		if (temperatureState == CM_TEMPERATURE_STATE_CRITICAL || temperatureState == CM_TEMPERATURE_STATE_TOO_COLD)
		{
			//Getting too hot to maintain charging at C4...switch to C8
			chrgMgrStartC8Timer();

			// Start charging at the low rate because we can't charge at the high rate
			chrgMgrStopCharging();
			chrgMgrChargeC8();

			#ifdef CHRG_MGR_PRINT_OUTPUT
				printf("CHRGMGR::TEMPERATURE TOO HIGH, SWITCHING TO C8.\n");
			#endif

			//Get additional chance to plow through overvoltage errors
			overvoltageChargeCompleteRetryCounter = 0;
#ifndef DISABLE_LOG_CHARGING_INFO
			logNormal(E_LOG_CHARGING_CRITICAL_TEMP,0);
#endif

			//Check to make sure correct charging mode timer is running
			chrgMgrCheckChargingModeTimer();

			// Cycle the state machine
			chrgMgrStateMachineState = STATE_CM_CHARGING_C8;

			return;
		}
		else if (temperatureState == CM_TEMPERATURE_STATE_ABORT || temperatureState == CM_TEMPERATURE_STATE_TOO_COLD)
		{
			//Tell the charge controller to stop
			chrgMgrStopCharging();

			//Detune the charger
			tetsDetune();

			// Cycle the state machine
			#ifdef CHRG_MGR_PRINT_OUTPUT
				printf("CHRGMGR::NOT CHARGING DUE TO HIGH TEMPERATURE.\n");
			#endif

			//Set an error
			chrgMgrSetChargingError(CHERR_STOP_TEMP_LIMIT_EXCEEDED);

			//Log the error
			logNormal(E_LOG_CHARGING_ABORT_TEMP,0);

			//Cycle the state machine
			chrgMgrStateMachineState = STATE_CM_CRITICAL_TEMPERATURE;

			return;
		}
	}
	else
	{
		//Thermistor check failed, go to C8
		chrgMgrStartC8Timer();

		// Start charging at the low rate because we can't charge at the high rate
		chrgMgrStopCharging();
		chrgMgrChargeC8();

		#ifdef CHRG_MGR_PRINT_OUTPUT
			printf("CHRGMGR::THERMISTOR ERROR, SWITCHING TO C8.\n");
		#endif

		//Get additional chance to plow through overvoltage errors
		overvoltageChargeCompleteRetryCounter = 0;

		// Cycle the state machine
		chrgMgrStateMachineState = STATE_CM_CHARGING_C8;

		return;
	}


	//Now check if we are in a state we should be charging..only get here
	//if temperature is okay and charging has not completeted.
	if (isChargerPresent && vrect > C4_VRECT_THRESHOLD)
	{
		//Conditions are right for charging, but charge controller is not on
		if (!chargeControllerOn)
		{
			//Increment a retry counter
			c4retryCounter++;

			//If we are not done retrying, then try, try again
			if (c4retryCounter < C4_MAX_RETRIES)
			{
				// Start charging at the high rate
				chrgMgrStopCharging();
				chrgMgrChargeC4();

				#ifdef CHRG_MGR_PRINT_OUTPUT
					printf("CHRGMGR::RESTART CHARGING AT C4.\n");
				#endif
			}
			else
			{
				//In this case, VDET is High, VRECT is sufficient for Charging
				//However, we have exceeded retries, so now try charging at C/8
				chrgMgrStartC8Timer();

				// Start charging at the low rate because we can't charge at the high rate
				chrgMgrStopCharging();
				chrgMgrChargeC8();

				#ifdef CHRG_MGR_PRINT_OUTPUT
					printf("CHRGMGR::TRYING CHARGING_C8.\n");
				#endif

				//Get additional chance to plow through overvoltage errors
				overvoltageChargeCompleteRetryCounter = 0;
#ifndef DISABLE_LOG_CHARGING_INFO
				logNormal(E_LOG_SWITCH_CHARGING_MODE, CHARGE_RATE_C8);
#endif
				// Cycle the state machine
				chrgMgrStateMachineState = STATE_CM_CHARGING_C8;
			}

		}
		else
		{
			//This is the happy case, increment a counter
			//Charge controller is still on...so stay here for now

			//#ifdef CHRG_MGR_PRINT_OUTPUT
			//	printf("CHRGMGR::CHARGING_C4.\n");
			//#endif

			//We have successfully charged at C4 for 1s, increment a success counter
			c4retryResetCounter++;

			//Now, if we have been charging for a while successfully, reset the retry counter
			if (c4retryResetCounter == C4_MAX_RETRY_RESET)
			{
				//See if the charge mode has changed, if it has change timers
				chrgMgrCheckChargingModeTimer();

				//Reset some counters
				c4retryCounter = 0;
				c4retryResetCounter = 0;
			}
		}
	}
	else
	{
		//This is the case where either VDET is LOW or VRECT is below threshold
		c4retryCounter++;

		//Now we are trying to go to C/8 because too many attempts to try at C/4
		if (c4retryCounter > C4_MAX_RETRIES)
		{
			chrgMgrStartC8Timer();

			// Start charging at the low rate
			chrgMgrStopCharging();
			chrgMgrChargeC8();

			// Cycle the state machine
			#ifdef CHRG_MGR_PRINT_OUTPUT
				printf("CHRGMGR::TRYING CHARGING_C8.\n");
			#endif

#ifndef DISABLE_LOG_CHARGING_INFO
			logNormal(E_LOG_SWITCH_CHARGING_MODE, CHARGE_RATE_C8);
#endif
			chrgMgrStateMachineState = STATE_CM_CHARGING_C8;
		}
		else
		{
			//This is the case where VDET is low or VRECT is below threshold
			//But we have not exceeded maximum retries (time)
			#ifdef CHRG_MGR_PRINT_OUTPUT
				printf("CHRGMGR::C4 WAITING FOR VDET TO GO HIGH.\n");
			#endif
		}
	}

	return;
}

/* Polls the charge controller while charging at low rate */
static void eventHandlerE_CHARGING_POLLING_TIMEOUT_C8(EVENT event)
{
	uint16_t vrect = chrgMgrGetVrect();
	bool chargeControllerOn = chrgMgrIsChrgMgrOn();
	bool isChargerPresent = chargeChargerPresent();
	CM_TEMPERATURE_STATE temperatureState;

	(void)event;

	//First check is charging is complete, if complete, we're done
	if (chrgMgrIsChrgComplete())
	{
		chrgMgrCompleteCharging();

		return;
	}

	//Perform a diagnostic check, set an error if fails
	if (chargeGetTempAndDiagCheck(&resistanceTemp, &thermOffset, &thermInput, &thermBias) != OK_RETURN_VALUE)
	{
		thermistorCheckPassed = false;
		chrgMgrSetChargingError(CHERR_THERMISTOR_ERROR);
	}
	else
	{
		thermistorCheckPassed = true;
	}

	temperatureState = chrgMgrGetTemperatureState();

	//Now see where we are temperature wise
	if (thermistorCheckPassed)
	{
		if (temperatureState == CM_TEMPERATURE_STATE_ABORT || temperatureState == CM_TEMPERATURE_STATE_TOO_COLD)
		{
			//Tell the charge controller to stop
			chrgMgrStopCharging();

			//Detune the charger
			tetsDetune();

			// Cycle the state machine
			#ifdef CHRG_MGR_PRINT_OUTPUT
				printf("CHRGMGR::NOT CHARGING DUE TO HIGH TEMPERATURE.\n");
			#endif

			logNormal(E_LOG_CHARGING_ABORT_TEMP,0);

			chrgMgrStateMachineState = STATE_CM_CRITICAL_TEMPERATURE;

			//Signal and error
			chrgMgrSetChargingError(CHERR_STOP_TEMP_LIMIT_EXCEEDED);

			return;
		}
	}

	//Conditions are right for charging, now check if we really are
	if (isChargerPresent && vrect > C8_VRECT_THRESHOLD)
	{
		//We are at C8, and VDET is HIGH, and VRECT is Over Threshold

		//Now check if the charge controller is one, if not turn it on
		if (!chargeControllerOn)
		{
			//We should be able to charge, but are not, so try charging
			//again at C8
			c8retryCounter++;

			//Have not exceeded retries, now try C8 again
			if (c8retryCounter < C8_MAX_RETRIES)
			{
				// Start charging at the high rate
				chrgMgrStopCharging();
				chrgMgrChargeC8();

				#ifdef CHRG_MGR_PRINT_OUTPUT
					printf("CHRGMGR::RESTART CHARGING AT C8.\n");
				#endif
			}
			else
			{
				//Cannot charge consistently at C8 although
				//VDET is high and VRECT is over threshold

				//Cancel the polling timer
				swTimerCancel(SWTMR_CHARGE_POLLING);
				swTimerCancel(SWTMR_LOW_RATE_TIMEOUT);
				swTimerCancel(SWTMR_ONE_MINUTE_TEST);
				swTimerCancel(SWTMR_CHARGE_STATE_TIMER);

				//Tell the charge controller to stop
				chrgMgrStopCharging();

				//Re-enable the VDET HIGH IRQ
				chargeEnableChargerEngagedInterrupt();

				// Cycle the state machine
				#ifdef CHRG_MGR_PRINT_OUTPUT
					printf("CHRGMGR::NOT CHARGING.\n");
				#endif

#ifndef DISABLE_LOG_CHARGING_INFO
				logNormal(E_LOG_CHARGING_INTERRUPTED, 1);
#endif
				isChargerEngaged = false;
				chrgMgrStateMachineState = STATE_CM_NOT_CHARGING;
			}
		}
		else
		{
			//VDET is HIGH, VRECT is Good, and Charging Controller Stayed On
			//This is the happy state, consistently charging at C8
			//#ifdef CHRG_MGR_PRINT_OUTPUT
			//	printf("CHRGMGR::CHARGING_C8.\n");
			//#endif

			//We have successfully charged at C8 for 1s, increment a success counter
			c8retryResetCounter++;

			//Now, if we have been charging for a while successfully, reset the retry counter
			if (c8retryResetCounter == C8_MAX_RETRY_RESET)
			{
				//See if the charge mode has changed, if it has change timers
				chrgMgrCheckChargingModeTimer();

				//Reset the counters
				c8retryCounter = 0;
				c8retryResetCounter = 0;
			}
		}
	}
	else
	{
		//In this case, VDET is LOW or VRECT is Not High Enough
		//Increment retry counter
		c8retryCounter++;

		//Exceeded retries at C8, Give Up
		if (c8retryCounter > C8_MAX_RETRIES)
		{
			//Cancel the polling timer
			swTimerCancel(SWTMR_CHARGE_POLLING);
			swTimerCancel(SWTMR_LOW_RATE_TIMEOUT);
			swTimerCancel(SWTMR_ONE_MINUTE_TEST);
			swTimerCancel(SWTMR_CHARGE_STATE_TIMER);

			//Tell the charge controller to stop
			chrgMgrStopCharging();

			//Re-enable the VDET HIGH IRQ
			chargeEnableChargerEngagedInterrupt();

			// Cycle the state machine
			#ifdef CHRG_MGR_PRINT_OUTPUT
				printf("CHRGMGR::NOT CHARGING.\n");
			#endif

#ifndef DISABLE_LOG_CHARGING_INFO
			logNormal(E_LOG_CHARGING_INTERRUPTED, 2);
#endif

			isChargerEngaged = false;
			chrgMgrStateMachineState = STATE_CM_NOT_CHARGING;
		}
		else
		{
			#ifdef CHRG_MGR_PRINT_OUTPUT
				printf("CHRGMGR::C8 WAITING FOR VDET TO GO HIGH.\n");
			#endif
		}
	}

	return;
}

/* Checks after 10 minutes of charging at low rate */
static void eventHandlerE_CHARGING_C8_TIMEOUT(EVENT event)
{
	CM_TEMPERATURE_STATE temperatureState;
	(void)event;

	//Perform a diagnostic check, set an error if fails
	if (chargeGetTempAndDiagCheck(&resistanceTemp, &thermOffset, &thermInput, &thermBias) != OK_RETURN_VALUE)
	{
		chrgMgrSetChargingError(CHERR_THERMISTOR_ERROR);
		thermistorCheckPassed = false;
	}
	else
	{
		thermistorCheckPassed = true;
	}

	temperatureState = chrgMgrGetTemperatureState();

	//If the temperature is okay, see if you can go back to C4
	if ((temperatureState == CM_TEMPERATURE_STATE_OK) && thermistorCheckPassed)
	{
		// Start charging at the high rate
		chrgMgrStopCharging();
		chrgMgrChargeC4();

		// Cycle the state machine
		#ifdef CHRG_MGR_PRINT_OUTPUT
			printf("CHRGMGR::SWITCHING TO C4 (10 MIN T/O).\n");
		#endif

#ifndef DISABLE_LOG_CHARGING_INFO
		logNormal(E_LOG_SWITCH_CHARGING_MODE, CHARGE_RATE_C4);
#endif

		//Get additional chance to plow through overvoltage errors
		overvoltageChargeCompleteRetryCounter = 0;

		chrgMgrStateMachineState = STATE_CM_CHARGING_C4;
	}
	else
	{
		//Wait another 10 minutes and stay charging at C8
		chrgMgrStartC8Timer();
	}

	// Zero out some counters
	c4retryCounter = 0;
	c8retryCounter = 0;
	c4retryResetCounter = 0;
	c8retryResetCounter = 0;

	return;
}

//Basically transitions us from waiting for charging to complete, to finishing
static void eventHandlerE_CHARGING_COMPLETE(EVENT event)
{
	uint16_t battVoltageAdcCounts;

	(void)event;

	#ifdef CHRG_MGR_PRINT_OUTPUT
		printf("CHRGMGR::CHARGING COMPLETED.\n");
	#endif

	//Let everybody know charging is disengaged
	dispPutSimpleEvent(E_CHARGER_DISENGAGED);
	isChargerEngaged = false;

	//Service the watchdog
	wdServiceWatchdog();

	// Zero out some counters
	c4retryCounter = 0;
	c8retryCounter = 0;
	c4retryResetCounter = 0;
	c8retryResetCounter = 0;
	batteryIncreaseRetryCounter = 0;

	//Cancel any outstanding timers
	swTimerCancel(SWTMR_CHARGE_POLLING);

	//Detune the charger
	tetsDetune();

	//Re-enable the VDET HIGH IRQ
	chargeEnableChargerEngagedInterrupt();

	//Increment charge-complete counter
	counterIncrement(COUNTER_CHARGE_COMPLETE);

	//Log charging completed
#ifndef DISABLE_LOG_CHARGING_INFO
	logNormal(E_LOG_CHARGING_COMPLETED,chargeCompleteChargeControllerState);
#endif
	//Cycle the state machine
	isChargerEngaged = false;
	chrgMgrStateMachineState = STATE_CM_NOT_CHARGING;

	//Log the battery voltage
	(void) batteryGetBatteryADC(&battVoltageAdcCounts) ;
#ifndef DISABLE_LOG_CHARGING_INFO
	logNormal(E_LOG_DIAGNOSTIC_BATTERY_ADC,battVoltageAdcCounts);
#endif

	return;
}

//Wait for the temperature to come down, then go into the not charging state so PPC can try again
static void eventHandlerE_CRITICAL_TEMPERATURE_POLLING(EVENT event)
{
	CM_TEMPERATURE_STATE temperatureState;

	(void)event;

	chargeGetTempAndDiagCheck(&resistanceTemp, &thermOffset, &thermInput, &thermBias);
	temperatureState = chrgMgrGetTemperatureState();

	//Once the temperature goes back down, then you can try again
	if (temperatureState == CM_TEMPERATURE_STATE_WARNING || temperatureState == CM_TEMPERATURE_STATE_OK)
	{
		//Cancel any outstanding timers
		swTimerCancel(SWTMR_CHARGE_POLLING);

		// Zero out some counters
		c4retryCounter = 0;
		c8retryCounter = 0;
		c4retryResetCounter = 0;
		c8retryResetCounter = 0;
		batteryIncreaseRetryCounter = 0;

		//Re-enable the VDET HIGH IRQ
		chargeEnableChargerEngagedInterrupt();

		//Cycle the state machine
		isChargerEngaged = false;
		chrgMgrStateMachineState = STATE_CM_NOT_CHARGING;

		//Let everybody know charging is disengaged
		dispPutSimpleEvent(E_CHARGER_DISENGAGED);
	}

	return;
}

//Reads the battery voltage and ensures it is increasing
static void eventHandlerE_PUBLISH_BATT_VOLTAGE(EVENT event)
{
	uint16_t newBatteryCount = event.eventData.eBatteryADCCounts;
	bool battIncreaseFailure = false;
	CHARGE_STATE chrgState = chargeGetChargeState();
	int batteryIncreaseCounts = 0;

	//Calculate the battery increase
	batteryIncreaseCounts = newBatteryCount - batteryADCCounts;

	//Check if we have a negative increase, if so set to zero
	if (batteryIncreaseCounts < 0)
	{
		batteryIncreaseCounts = 0;
	}

	//Depending on which charge state, increases have different minimum values
	if (chrgState == CHARGE_STATE_CHARGE_PRE_CONDITION)
	{
		//First check if the minimum increase is zero, if it is just return
		//because we have effectively disabled the check
		if (calibrationData->batteryIncreasePreCharge == 0)
		{
			return;
		}

		//If the increase is less than the minimum, we have an increase failure
		if (batteryIncreaseCounts < calibrationData->batteryIncreasePreCharge)
		{
			battIncreaseFailure = true;
		}
	}
	else
	{
		//First check if the minimum increase is zero, if it is just return
		//because we have effectively disabled the check
		if (calibrationData->batteryIncreaseConstantCurrent == 0)
		{
			return;
		}

		//If the increase is less than the minimum, we have an increase failure
		if (batteryIncreaseCounts < calibrationData->batteryIncreaseConstantCurrent)
		{
			battIncreaseFailure = true;
		}
	}

	//Increment the counter...if we have a failure, stop charging
	if (battIncreaseFailure)
	{
		//Increment the counter
		batteryIncreaseRetryCounter++;

		//If we reach our retry count, basically fail
		if (batteryIncreaseRetryCounter == BATT_INCREASE_RETRIES)
		{
			//Figure out what error to send
			if (chrgState == CHARGE_STATE_CHARGE_PRE_CONDITION)
			{
				chrgMgrSetChargingError(CHERR_STOP_PRE_CHARGE_PHASE_INSUFF_V_INCREASE);
			}
			else
			{
				chrgMgrSetChargingError(CHERR_STOP_CONST_I_PHASE_INSUFF_V_INCREASE);
			}

			//Cancel the polling timer
			swTimerCancel(SWTMR_CHARGE_POLLING);
			swTimerCancel(SWTMR_LOW_RATE_TIMEOUT);
			swTimerCancel(SWTMR_ONE_MINUTE_TEST);
			swTimerCancel(SWTMR_CHARGE_STATE_TIMER);

			//Tell the charge controller to stop
			chrgMgrStopCharging();

			//Re-enable the VDET HIGH IRQ
			chargeEnableChargerEngagedInterrupt();

			// Cycle the state machine
			#ifdef CHRG_MGR_PRINT_OUTPUT
				printf("CHRGMGR::CHARGING ABORTED DUE TO INSUFFICIENT INCREASE - NOT CHARGING.\n");
			#endif

#ifndef DISABLE_LOG_CHARGING_INFO
			logNormal(E_LOG_CHARGING_ABORT_VOLTAGE, 0);
#endif

			isChargerEngaged = false;
			chrgMgrStateMachineState = STATE_CM_NOT_CHARGING;
		}
	}
	else
	{
		//We have an increase in batt voltage...Reset the counter
		batteryIncreaseRetryCounter = 0;

		//Set the new stored count to last read count
		batteryADCCounts = newBatteryCount;
	}

	return;
}

//Handle timeout events if spent too long in a given phase
static void eventHandlerE_CHARGING_MODE_TIMEOUT(EVENT event)
{
	(void)event;

	//Figure out what error to send
	if (chargingMode == CHARGE_STATE_CHARGE_PRE_CONDITION)
	{
		chrgMgrSetChargingError(CHERR_STOP_PRE_CHARGE_PHASE_TIMEOUT);
	}
	else if (chargingMode == CHARGE_STATE_CHARGE_CONSTANT_CURRENT)
	{
		chrgMgrSetChargingError(CHERR_STOP_CONST_I_PHASE_TIMEOUT);
	}

	//Cancel the polling timer
	swTimerCancel(SWTMR_CHARGE_POLLING);
	swTimerCancel(SWTMR_LOW_RATE_TIMEOUT);
	swTimerCancel(SWTMR_ONE_MINUTE_TEST);
	swTimerCancel(SWTMR_CHARGE_STATE_TIMER);

	//Tell the charge controller to stop
	chrgMgrStopCharging();

	//Re-enable the VDET HIGH IRQ
	chargeEnableChargerEngagedInterrupt();

	// Cycle the state machine
	#ifdef CHRG_MGR_PRINT_OUTPUT
		printf("CHRGMGR::CHARGING ABORTED DUE TO TOO LONG IN PHASE - NOT CHARGING.\n");
	#endif

#ifndef DISABLE_LOG_CHARGING_INFO
	logNormal(E_LOG_CHARGING_ABORT_ERROR, 0);
#endif

	isChargerEngaged = false;
	chrgMgrStateMachineState = STATE_CM_NOT_CHARGING;

	return;
}

//For debugging purposes, this logs temperature, vrect, and vbat on a timer event.
static void eventHandlerE_LOG_TEMPERATURE_DATA(EVENT event)
{
	uint16_t rechargePower = 0;
	uint32_t logdata;
	uint16_t battVoltageAdcCounts=0;

	(void)event;

	//Get the Data for Logging
	chargeGetRechargeVoltage(&rechargePower);

	//Log the Current VRECT
	logdata = ((uint32_t)(rechargePower));
	logdata = logdata<<16;
	logdata += thermOffset;
	logNormal(E_LOG_DIAGNOSTIC_VRECT_AND_TEMP,logdata);

	//Log the Current Temperature
	logdata = ((uint32_t)(thermInput));
	logdata = logdata<<16;
	logdata += thermBias;
	logNormal(E_LOG_DIAGNOSTIC_TEMPERATURE,logdata);
	logNormal(E_LOG_DIAGNOSTIC_TEMPERATURE,(uint32_t)chrgMgrGetTemperatureState());

	//Log the Battery Information
	(void) batteryGetBatteryADC(&battVoltageAdcCounts) ;
	logNormal(E_LOG_DIAGNOSTIC_BATTERY_ADC,battVoltageAdcCounts);

	return;
}

/*
 * Helper Functions
 */

/* Initiate charging to the high rate. */
static void chrgMgrChargeC4()
{
	//Start Charging at Low Rate
	chargeSetChargeRate(CHARGE_RATE_C4);

	//Start the Charging Process
	chargeStartChargingProcess();

	return;
}

/* Initiate charging to the low rate. */
static void chrgMgrChargeC8()
{
	//Start Charging at Low Rate
	chargeSetChargeRate(CHARGE_RATE_C8);

	//Start the Charging Process
	chargeStartChargingProcess();

	return;
}

/* Stops charging by letting the charge controller know */
static void chrgMgrStopCharging()
{
	chargeStopChargingProcess();

	return;
}

/* Go get the current vrect value */
static uint16_t chrgMgrGetVrect()
{
	uint16_t rechargePower = 0;

	chargeGetRechargeVoltage(&rechargePower);

	return rechargePower;
}

/* See if charge completed as expected */
static bool chrgMgrIsChrgComplete()
{
	CHARGE_STATE newChargeState = chargeGetChargeState();

	if (newChargeState == CHARGE_STATE_CHARGE_VOLTAGE || newChargeState == CHARGE_STATE_CHARGE_COMPLETE)
	{
		//Check if the battery is really full, if not return false
		if (battMonGetCurrentBatteryVoltage() >= BATT_VOLTAGE_FULL_ADC_CNTS)
		{
			chargeCompleteChargeControllerState = newChargeState;
			return true;
		}
		else
		{
			return false;
		}
	}
	else if (newChargeState == CHARGE_STATE_OVERVOLT_FAULT || newChargeState == CHARGE_STATE_VOLTAGE_DETECT_AND_OVERVOLT_FAULT)
	{
		overvoltageChargeCompleteRetryCounter++;

		if (overvoltageChargeCompleteRetryCounter > OVERVOLT_CC_MAX_RETRIES)
		{
			chargeCompleteChargeControllerState = newChargeState;
			return true;
		}
		else
		{
			return false;
		}

	}
	else
	{
		return false;
	}
}

/*Get the temperature state for determining if and what rate to charge*/
static CM_TEMPERATURE_STATE chrgMgrGetTemperatureState()
{
	if (resistanceTemp == 0)
	{
		return CM_TEMPERATURE_STATE_CRITICAL;
	}
	if ( resistanceTemp > calibrationData->temperatureMinimumThreshold)
	{
		return CM_TEMPERATURE_STATE_TOO_COLD;
	}
	else if (resistanceTemp >= calibrationData->temperatureWarningThreshold)
	{
		return CM_TEMPERATURE_STATE_OK;
	}
	else if (resistanceTemp >= calibrationData->temperatureCriticalThreshold)
	{
		return CM_TEMPERATURE_STATE_WARNING;
	}
	else if (resistanceTemp >= calibrationData->temperatureAbortThreshold)
	{
		return CM_TEMPERATURE_STATE_CRITICAL;
	}
	else
	{
		return CM_TEMPERATURE_STATE_ABORT;
	}
}

//Starts the timer that runs when charging at C8 that initiates an
//attempt to charge at C4
static void chrgMgrStartC8Timer()
{
	//Start a 10 minute timer to see if we can return to C/4
	EVENT tenMinuteEvent;

	//Set at timer at the desired rate to poll the charge controller
	tenMinuteEvent.eventID = E_CHARGE_LOW_RATE_TO;
	swTimerSet( SWTMR_LOW_RATE_TIMEOUT, LOW_CHARGING_RATE_TIMEOUT, tenMinuteEvent, false );

	return;
}

//Starts the generic polling timer
static void chrgMgrStartPollingTimer()
{
	EVENT pollingEvent;

	// Set at timer at the desired rate to poll the charge controller
	pollingEvent.eventID = E_CHARGING_POLLING;
	swTimerSet( SWTMR_CHARGE_POLLING, POLLING_TIMEOUT, pollingEvent, true );

	return;
}


//Handling of charging completion
static void chrgMgrCompleteCharging()
{
	EVENT chargingCompletingEvent;

	//Cancel the polling timer
	swTimerCancel(SWTMR_CHARGE_POLLING);
	swTimerCancel(SWTMR_LOW_RATE_TIMEOUT);
	swTimerCancel(SWTMR_ONE_MINUTE_TEST);
	swTimerCancel(SWTMR_CHARGE_STATE_TIMER);

	//Tell the charge controller to stop
	chrgMgrStopCharging();

	// Cycle the state machine
	#ifdef CHRG_MGR_PRINT_OUTPUT
		printf("CHRGMGR::WAITING FOR CHARGING COMPLETE FINISHED.\n");
	#endif

	//Start a new timer to wait for PPC to stop pumping out power
	chargingCompletingEvent.eventID = E_CHARGING_COMPLETING;
	swTimerSet( SWTMR_COMPLETING_CHARGING, CHARGING_COMPLETE_WAIT_TIME, chargingCompletingEvent, false );

	chrgMgrStateMachineState = STATE_CM_COMPLETING_CHARGING;

	return;
}


/*
 * Public APIs
 */

/* Give the PPC what it needs to keep charging */
int chrgMgrGetChrgAndTempState(uint8_t * chrgStateAndTemperature)
{
	CHARGE_STATE chrgState = chargeGetChargeState();
	CM_TEMPERATURE_STATE temperatureState = chrgMgrGetTemperatureState();
	PPC_CHARGING_STATE ppcChrgState = PPC_CHARGE_OFF;

	if (!thermistorCheckPassed)
	{
		temperatureState = CM_TEMPERATURE_STATE_OK;
	}

	//If its complete, tell the PPC that charging is complete
	if (chrgMgrIsChrgComplete())
	{
		ppcChrgState = PPC_CHARGE_COMPLETE;

		//Potentially the PPC may know before the charge manager, handle this case
		if (chrgMgrStateMachineState != STATE_CM_COMPLETING_CHARGING)
		{
			chrgMgrCompleteCharging();
		}
	}
	//If charging is not going as expected, just report that charging is off
	else if (chrgState != CHARGE_STATE_CHARGE_CONSTANT_CURRENT && chrgState != CHARGE_STATE_CHARGE_PRE_CONDITION)
	{
		//First check if we have already detected charge completed - then report charge completed
		if (chrgMgrStateMachineState == STATE_CM_COMPLETING_CHARGING)
		{
			ppcChrgState = PPC_CHARGE_COMPLETE;
		}
		else if (chrgState == CHARGE_STATE_VOLTAGE_DETECT_AND_OVERVOLT_FAULT || chrgState ==  CHARGE_STATE_OVERVOLT_FAULT)
		{
			//Increment a counter to see if we should tell the PPC that we cannot charge
			overvoltageRetryCounter++;

			//If we keep getting overvoltage errors, then tell the PPC we cannot charge
			if (overvoltageRetryCounter == OVERVOLT_MAX_RETRIES)
			{
				overvoltageRetryCounter = 0;
				ppcChrgState = PPC_CHARGE_OFF;
			}
			else
			{
				//If we have not reached the max overvoltage count, then just report
				//we are charging at the rate defined by what state we are in...
				if (chrgMgrStateMachineState == STATE_CM_CHARGING_C4)
				{
					ppcChrgState = PPC_CHARGE_CONSTANT_CURRENT_FAST;
				}
				else
				{
					ppcChrgState = PPC_CHARGE_CONSTANT_CURRENT_SLOW;
				}
			}
		}
		else
		{
			ppcChrgState = PPC_CHARGE_OFF;
		}
	}
	else if (chrgState == CHARGE_STATE_CHARGE_CONSTANT_CURRENT)
	{
		//Reset the overvoltage counter because now we are charging
		overvoltageRetryCounter = 0;

		if (chrgMgrStateMachineState == STATE_CM_CHARGING_C4)
		{
			ppcChrgState = PPC_CHARGE_CONSTANT_CURRENT_FAST;
		}
		else
		{
			ppcChrgState = PPC_CHARGE_CONSTANT_CURRENT_SLOW;
		}
	}
	else if (chrgState == CHARGE_STATE_CHARGE_PRE_CONDITION)
	{
		ppcChrgState = PPC_CHARGE_PRE_CONDITION;
	}

	#ifdef CHRG_MGR_PRINT_OUTPUT
		printf("CHRGMGR::PPC REQ CHARGING STATE=0x%x.\n", chrgState);
	#endif

	//Set the value that will be used for charging state and temperature...
	*chrgStateAndTemperature = ((((uint8_t)ppcChrgState) << 4) | temperatureState);

    return OK_RETURN_VALUE;
}

/* Send a charging error event */
static void chrgMgrSetChargingError(CHARGE_ERROR_CODE chargingErrorCode)
{
	EVENT chargingErrorEvent;

	// Publish a charging error
	chargingErrorEvent.eventID = E_ERR_CHARGER_ERROR;
	chargingErrorEvent.eventData.eChargeErrorData = chargingErrorCode ;
	dispPutEvent(chargingErrorEvent);

	return;
}

/* Returns the last temperature data read. */
void chrgMgrGetLastTemperatureDataReading(uint16_t *temp, uint16_t *offset, uint16_t *input, uint16_t *bias)
{
	*temp = resistanceTemp;
	*offset = thermOffset;
	*input = thermInput;
	*bias = thermBias;

	return;
}

/* Clears the last temperature data reading. */
void chrgMgrClearLastTemperatureDataReading(void)
{
	resistanceTemp = 0;
	thermOffset = 0;
	thermInput = 0;
	thermBias = 0;

	return;
}

/* Start the appropriate 1 hour or 8 hour timer depending on charge mode */
static void chrgMgrCheckChargingModeTimer()
{
	CHARGE_STATE newChargingMode = chargeGetChargeState();

	//First check in the charging mode has changed
	if (newChargingMode != chargingMode)
	{
		//Cancel any running timers, because mode has changed
		swTimerCancel(SWTMR_CHARGE_STATE_TIMER);

		//First check if we are even charging
		if (newChargingMode != CHARGE_STATE_CHARGE_CONSTANT_CURRENT && chargingMode != CHARGE_STATE_CHARGE_PRE_CONDITION)
		{
			if (chargingMode == CHARGE_STATE_CHARGE_PRE_CONDITION)
			{
				counterIncrement(COUNTER_PRECHARGE_COMPLETE);
			}

			chargingMode = CHARGE_STATE_CHARGE_OFF;
		}
		else if (newChargingMode == CHARGE_STATE_CHARGE_CONSTANT_CURRENT)
		{
			//Start a new timer, because the state changed
			EVENT constCurrentTimeoutEvent;
			constCurrentTimeoutEvent.eventID = E_CHARGE_CONCUR_STATE_TO;
			swTimerSet(SWTMR_CHARGE_STATE_TIMER, SWTMR_08_HOUR_TIMEOUT, constCurrentTimeoutEvent, false );

			if (chargingMode == CHARGE_STATE_CHARGE_PRE_CONDITION)
			{
				counterIncrement(COUNTER_PRECHARGE_COMPLETE);
			}

			//Change the stored mode
			chargingMode = CHARGE_STATE_CHARGE_CONSTANT_CURRENT;
		}
		else if (newChargingMode == CHARGE_STATE_CHARGE_PRE_CONDITION)
		{
			//Start a new timer, because the state changed
			EVENT preConditionTimeoutEvent;
			preConditionTimeoutEvent.eventID = E_CHARGE_PRECON_STATE_TO;
			swTimerSet(SWTMR_CHARGE_STATE_TIMER, SWTMR_01_HOUR_TIMEOUT, preConditionTimeoutEvent, false );

			//Change the store mode
			chargingMode = CHARGE_STATE_CHARGE_PRE_CONDITION;
		}
	}

	return;
}

/* See if the charge manager is on */
bool chrgMgrIsChrgMgrOn()
{
	CHARGE_STATE newChargeState = chargeGetChargeState();

	if (newChargeState == CHARGE_STATE_CHARGE_PRE_CONDITION || newChargeState == CHARGE_STATE_CHARGE_CONSTANT_CURRENT)
	{
		return true;
	}
	else
	{
		if (newChargeState == CHARGE_STATE_VOLTAGE_DETECT_AND_OVERVOLT_FAULT || newChargeState == CHARGE_STATE_OVERVOLT_FAULT)
		{
			logError(E_LOG_CHARGING_ABORT_OVER_VOLTAGE,0);
		}

		return false;
	}
}

/* Returns whether we are trying to charge or not */
bool chrgMgrIsCharging()
{
	//If there is some vrect or we are trying to charge, then say we're charging
	if (chrgMgrStateMachineState != STATE_CM_NOT_CHARGING && chrgMgrStateMachineState != STATE_CM_CHARGING_NOT_ALLOWED)
	{
		return true;
	}

	return false;
}

bool chrgMgrIsVrectPresent(void)
{
	uint16_t vrect;

	vrect = chrgMgrGetVrect();
	return (vrect >= VRECT_PRESENCE_THRESHOLD);
}

bool chrgMgrIsChargerEngaged(void)
{
	return isChargerEngaged;
}

#endif
